.class public Lcom/ibm/icu/impl/OlsonTimeZone;
.super Lcom/ibm/icu/util/BasicTimeZone;
.source "OlsonTimeZone.java"


# static fields
.field private static final ASSERT:Z = false

.field private static final DEBUG:Z

.field private static final SECONDS_PER_DAY:I = 0x15180

.field private static final UNSIGNED_BYTE_MASK:I = 0xff

.field static final serialVersionUID:J = -0x572e1120b9848270L


# instance fields
.field private finalMillis:D

.field private finalYear:I

.field private finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

.field private transient finalZoneWithStartYear:Lcom/ibm/icu/util/SimpleTimeZone;

.field private transient firstFinalTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

.field private transient firstTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

.field private transient firstTZTransitionIdx:I

.field private transient historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

.field private transient initialRule:Lcom/ibm/icu/util/InitialTimeZoneRule;

.field private transitionCount:I

.field private transient transitionRulesInitialized:Z

.field private transitionTimes:[I

.field private typeCount:I

.field private typeData:[B

.field private typeOffsets:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 674
    const-string/jumbo v0, "olson"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/ibm/icu/impl/OlsonTimeZone;->DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 456
    invoke-direct {p0}, Lcom/ibm/icu/util/BasicTimeZone;-><init>()V

    .line 659
    const v0, 0x7fffffff

    iput v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    .line 666
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalMillis:D

    .line 672
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    .line 463
    invoke-direct {p0}, Lcom/ibm/icu/impl/OlsonTimeZone;->constructEmpty()V

    .line 464
    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/util/UResourceBundle;Lcom/ibm/icu/util/UResourceBundle;)V
    .locals 2
    .param p1, "top"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p2, "res"    # Lcom/ibm/icu/util/UResourceBundle;

    .prologue
    .line 360
    invoke-direct {p0}, Lcom/ibm/icu/util/BasicTimeZone;-><init>()V

    .line 659
    const v0, 0x7fffffff

    iput v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    .line 666
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalMillis:D

    .line 672
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    .line 361
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/impl/OlsonTimeZone;->construct(Lcom/ibm/icu/util/UResourceBundle;Lcom/ibm/icu/util/UResourceBundle;)V

    .line 362
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 5
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 466
    invoke-direct {p0}, Lcom/ibm/icu/util/BasicTimeZone;-><init>()V

    .line 659
    const v2, 0x7fffffff

    iput v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    .line 666
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalMillis:D

    .line 672
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    .line 467
    const-string/jumbo v2, "com/ibm/icu/impl/data/icudt40b"

    const-string/jumbo v3, "zoneinfo"

    sget-object v4, Lcom/ibm/icu/impl/ICUResourceBundle;->ICU_DATA_CLASS_LOADER:Ljava/lang/ClassLoader;

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v1

    .line 468
    .local v1, "top":Lcom/ibm/icu/util/UResourceBundle;
    invoke-static {p1}, Lcom/ibm/icu/impl/ZoneMeta;->openOlsonResource(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .line 469
    .local v0, "res":Lcom/ibm/icu/util/UResourceBundle;
    invoke-direct {p0, v1, v0}, Lcom/ibm/icu/impl/OlsonTimeZone;->construct(Lcom/ibm/icu/util/UResourceBundle;Lcom/ibm/icu/util/UResourceBundle;)V

    .line 470
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v2, :cond_0

    .line 471
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v2, p1}, Lcom/ibm/icu/util/SimpleTimeZone;->setID(Ljava/lang/String;)V

    .line 473
    :cond_0
    invoke-super {p0, p1}, Lcom/ibm/icu/util/BasicTimeZone;->setID(Ljava/lang/String;)V

    .line 474
    return-void
.end method

.method private construct(Lcom/ibm/icu/util/UResourceBundle;Lcom/ibm/icu/util/UResourceBundle;)V
    .locals 20
    .param p1, "top"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p2, "res"    # Lcom/ibm/icu/util/UResourceBundle;

    .prologue
    .line 366
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 367
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 369
    :cond_1
    sget-boolean v2, Lcom/ibm/icu/impl/OlsonTimeZone;->DEBUG:Z

    if-eqz v2, :cond_2

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "OlsonTimeZone("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 382
    :cond_2
    invoke-virtual/range {p2 .. p2}, Lcom/ibm/icu/util/UResourceBundle;->getSize()I

    move-result v19

    .line 383
    .local v19, "size":I
    const/4 v2, 0x3

    move/from16 v0, v19

    if-lt v0, v2, :cond_3

    const/4 v2, 0x6

    move/from16 v0, v19

    if-le v0, v2, :cond_4

    .line 385
    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Invalid Format"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 389
    :cond_4
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/ibm/icu/util/UResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v17

    .line 390
    .local v17, "r":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual/range {v17 .. v17}, Lcom/ibm/icu/util/UResourceBundle;->getIntVector()[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    .line 392
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    array-length v2, v2

    if-ltz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    array-length v2, v2

    const/16 v4, 0x7fff

    if-le v2, v4, :cond_6

    .line 393
    :cond_5
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Invalid Format"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 395
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    array-length v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    .line 398
    const/4 v2, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/ibm/icu/util/UResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v17

    .line 399
    invoke-virtual/range {v17 .. v17}, Lcom/ibm/icu/util/UResourceBundle;->getIntVector()[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    .line 400
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    array-length v2, v2

    const/4 v4, 0x2

    if-lt v2, v4, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    array-length v2, v2

    const/16 v4, 0x7ffe

    if-gt v2, v4, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    array-length v2, v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_8

    .line 401
    :cond_7
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Invalid Format"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 403
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    array-length v2, v2

    shr-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeCount:I

    .line 406
    const/4 v2, 0x2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/ibm/icu/util/UResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v17

    .line 407
    invoke-virtual/range {v17 .. v17}, Lcom/ibm/icu/util/UResourceBundle;->getBinary()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    .line 408
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    array-length v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    if-eq v2, v4, :cond_9

    .line 409
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Invalid Format"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 413
    :cond_9
    const/4 v2, 0x5

    move/from16 v0, v19

    if-lt v0, v2, :cond_a

    .line 414
    const/4 v2, 0x3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/ibm/icu/util/UResourceBundle;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 415
    .local v18, "ruleid":Ljava/lang/String;
    const/4 v2, 0x4

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/ibm/icu/util/UResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v17

    .line 416
    invoke-virtual/range {v17 .. v17}, Lcom/ibm/icu/util/UResourceBundle;->getIntVector()[I

    move-result-object v16

    .line 418
    .local v16, "data":[I
    if-eqz v16, :cond_c

    move-object/from16 v0, v16

    array-length v2, v0

    const/4 v4, 0x2

    if-ne v2, v4, :cond_c

    .line 419
    const/4 v2, 0x0

    aget v2, v16, v2

    mul-int/lit16 v3, v2, 0x3e8

    .line 426
    .local v3, "rawOffset":I
    const/4 v2, 0x1

    aget v2, v16, v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    .line 429
    const/4 v2, 0x1

    aget v2, v16, v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v2, v4, v5}, Lcom/ibm/icu/impl/Grego;->fieldsToDay(III)J

    move-result-wide v4

    const-wide/32 v6, 0x5265c00

    mul-long/2addr v4, v6

    long-to-double v4, v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalMillis:D

    .line 432
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/OlsonTimeZone;->loadRule(Lcom/ibm/icu/util/UResourceBundle;Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v17

    .line 435
    invoke-virtual/range {v17 .. v17}, Lcom/ibm/icu/util/UResourceBundle;->getIntVector()[I

    move-result-object v16

    .line 436
    move-object/from16 v0, v16

    array-length v2, v0

    const/16 v4, 0xb

    if-ne v2, v4, :cond_b

    .line 439
    new-instance v2, Lcom/ibm/icu/util/SimpleTimeZone;

    const-string/jumbo v4, ""

    const/4 v5, 0x0

    aget v5, v16, v5

    const/4 v6, 0x1

    aget v6, v16, v6

    const/4 v7, 0x2

    aget v7, v16, v7

    const/4 v8, 0x3

    aget v8, v16, v8

    mul-int/lit16 v8, v8, 0x3e8

    const/4 v9, 0x4

    aget v9, v16, v9

    const/4 v10, 0x5

    aget v10, v16, v10

    const/4 v11, 0x6

    aget v11, v16, v11

    const/4 v12, 0x7

    aget v12, v16, v12

    const/16 v13, 0x8

    aget v13, v16, v13

    mul-int/lit16 v13, v13, 0x3e8

    const/16 v14, 0x9

    aget v14, v16, v14

    const/16 v15, 0xa

    aget v15, v16, v15

    mul-int/lit16 v15, v15, 0x3e8

    invoke-direct/range {v2 .. v15}, Lcom/ibm/icu/util/SimpleTimeZone;-><init>(ILjava/lang/String;IIIIIIIIIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    .line 454
    .end local v3    # "rawOffset":I
    .end local v16    # "data":[I
    .end local v18    # "ruleid":Ljava/lang/String;
    :cond_a
    return-void

    .line 448
    .restart local v3    # "rawOffset":I
    .restart local v16    # "data":[I
    .restart local v18    # "ruleid":Ljava/lang/String;
    :cond_b
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Invalid Format"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 451
    .end local v3    # "rawOffset":I
    :cond_c
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Invalid Format"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private constructEmpty()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 347
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    .line 348
    const/4 v0, 0x1

    iput v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeCount:I

    .line 349
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    iput-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    .line 350
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    .line 352
    return-void

    .line 349
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private dstOffset(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 580
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    shl-int/lit8 v1, p1, 0x1

    add-int/lit8 v1, v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method private getHistoricalOffset(JZII[I)V
    .locals 17
    .param p1, "date"    # J
    .param p3, "local"    # Z
    .param p4, "NonExistingTimeOpt"    # I
    .param p5, "DuplicatedTimeOpt"    # I
    .param p6, "offsets"    # [I

    .prologue
    .line 492
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    if-eqz v13, :cond_16

    .line 493
    const-wide/16 v14, 0x3e8

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v14, v15}, Lcom/ibm/icu/impl/OlsonTimeZone;->myFloorDivide(JJ)J

    move-result-wide v10

    .line 496
    .local v10, "sec":J
    const/4 v5, 0x0

    .line 497
    .local v5, "i":I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    add-int/lit8 v5, v13, -0x1

    :goto_0
    if-lez v5, :cond_3

    .line 498
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    aget v12, v13, v5

    .line 499
    .local v12, "transition":I
    if-eqz p3, :cond_2

    .line 500
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    add-int/lit8 v14, v5, -0x1

    aget-byte v13, v13, v14

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/ibm/icu/impl/OlsonTimeZone;->getInt(B)I

    move-result v13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/ibm/icu/impl/OlsonTimeZone;->zoneOffset(I)I

    move-result v8

    .line 501
    .local v8, "offsetBefore":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    add-int/lit8 v14, v5, -0x1

    aget-byte v13, v13, v14

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/ibm/icu/impl/OlsonTimeZone;->getInt(B)I

    move-result v13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/ibm/icu/impl/OlsonTimeZone;->dstOffset(I)I

    move-result v13

    if-eqz v13, :cond_4

    const/4 v3, 0x1

    .line 503
    .local v3, "dstBefore":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    aget-byte v13, v13, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/ibm/icu/impl/OlsonTimeZone;->getInt(B)I

    move-result v13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/ibm/icu/impl/OlsonTimeZone;->zoneOffset(I)I

    move-result v7

    .line 504
    .local v7, "offsetAfter":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    aget-byte v13, v13, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/ibm/icu/impl/OlsonTimeZone;->getInt(B)I

    move-result v13

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/ibm/icu/impl/OlsonTimeZone;->dstOffset(I)I

    move-result v13

    if-eqz v13, :cond_5

    const/4 v2, 0x1

    .line 506
    .local v2, "dstAfter":Z
    :goto_2
    if-eqz v3, :cond_6

    if-nez v2, :cond_6

    const/4 v4, 0x1

    .line 507
    .local v4, "dstToStd":Z
    :goto_3
    if-nez v3, :cond_7

    if-eqz v2, :cond_7

    const/4 v9, 0x1

    .line 509
    .local v9, "stdToDst":Z
    :goto_4
    sub-int v13, v7, v8

    if-ltz v13, :cond_d

    .line 511
    and-int/lit8 v13, p4, 0x3

    const/4 v14, 0x1

    if-ne v13, v14, :cond_0

    if-nez v4, :cond_1

    :cond_0
    and-int/lit8 v13, p4, 0x3

    const/4 v14, 0x3

    if-ne v13, v14, :cond_8

    if-eqz v9, :cond_8

    .line 513
    :cond_1
    add-int/2addr v12, v8

    .line 542
    .end local v2    # "dstAfter":Z
    .end local v3    # "dstBefore":Z
    .end local v4    # "dstToStd":Z
    .end local v7    # "offsetAfter":I
    .end local v8    # "offsetBefore":I
    .end local v9    # "stdToDst":Z
    :cond_2
    :goto_5
    int-to-long v14, v12

    cmp-long v13, v10, v14

    if-ltz v13, :cond_15

    .line 560
    .end local v12    # "transition":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    aget-byte v13, v13, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/ibm/icu/impl/OlsonTimeZone;->getInt(B)I

    move-result v6

    .line 561
    .local v6, "index":I
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/ibm/icu/impl/OlsonTimeZone;->rawOffset(I)I

    move-result v14

    mul-int/lit16 v14, v14, 0x3e8

    aput v14, p6, v13

    .line 562
    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/ibm/icu/impl/OlsonTimeZone;->dstOffset(I)I

    move-result v14

    mul-int/lit16 v14, v14, 0x3e8

    aput v14, p6, v13

    .line 568
    .end local v5    # "i":I
    .end local v6    # "index":I
    .end local v10    # "sec":J
    :goto_6
    return-void

    .line 501
    .restart local v5    # "i":I
    .restart local v8    # "offsetBefore":I
    .restart local v10    # "sec":J
    .restart local v12    # "transition":I
    :cond_4
    const/4 v3, 0x0

    goto :goto_1

    .line 504
    .restart local v3    # "dstBefore":Z
    .restart local v7    # "offsetAfter":I
    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    .line 506
    .restart local v2    # "dstAfter":Z
    :cond_6
    const/4 v4, 0x0

    goto :goto_3

    .line 507
    .restart local v4    # "dstToStd":Z
    :cond_7
    const/4 v9, 0x0

    goto :goto_4

    .line 514
    .restart local v9    # "stdToDst":Z
    :cond_8
    and-int/lit8 v13, p4, 0x3

    const/4 v14, 0x1

    if-ne v13, v14, :cond_9

    if-nez v9, :cond_a

    :cond_9
    and-int/lit8 v13, p4, 0x3

    const/4 v14, 0x3

    if-ne v13, v14, :cond_b

    if-eqz v4, :cond_b

    .line 516
    :cond_a
    add-int/2addr v12, v7

    .line 517
    goto :goto_5

    :cond_b
    and-int/lit8 v13, p4, 0xc

    const/16 v14, 0xc

    if-ne v13, v14, :cond_c

    .line 518
    add-int/2addr v12, v8

    .line 519
    goto :goto_5

    .line 522
    :cond_c
    add-int/2addr v12, v7

    .line 524
    goto :goto_5

    .line 526
    :cond_d
    and-int/lit8 v13, p5, 0x3

    const/4 v14, 0x1

    if-ne v13, v14, :cond_e

    if-nez v4, :cond_f

    :cond_e
    and-int/lit8 v13, p5, 0x3

    const/4 v14, 0x3

    if-ne v13, v14, :cond_10

    if-eqz v9, :cond_10

    .line 528
    :cond_f
    add-int/2addr v12, v7

    .line 529
    goto :goto_5

    :cond_10
    and-int/lit8 v13, p5, 0x3

    const/4 v14, 0x1

    if-ne v13, v14, :cond_11

    if-nez v9, :cond_12

    :cond_11
    and-int/lit8 v13, p5, 0x3

    const/4 v14, 0x3

    if-ne v13, v14, :cond_13

    if-eqz v4, :cond_13

    .line 531
    :cond_12
    add-int/2addr v12, v8

    .line 532
    goto :goto_5

    :cond_13
    and-int/lit8 v13, p5, 0xc

    const/4 v14, 0x4

    if-ne v13, v14, :cond_14

    .line 533
    add-int/2addr v12, v8

    .line 534
    goto :goto_5

    .line 537
    :cond_14
    add-int/2addr v12, v7

    goto :goto_5

    .line 497
    .end local v2    # "dstAfter":Z
    .end local v3    # "dstBefore":Z
    .end local v4    # "dstToStd":Z
    .end local v7    # "offsetAfter":I
    .end local v8    # "offsetBefore":I
    .end local v9    # "stdToDst":Z
    :cond_15
    add-int/lit8 v5, v5, -0x1

    goto/16 :goto_0

    .line 565
    .end local v5    # "i":I
    .end local v10    # "sec":J
    .end local v12    # "transition":I
    :cond_16
    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/ibm/icu/impl/OlsonTimeZone;->rawOffset(I)I

    move-result v14

    mul-int/lit16 v14, v14, 0x3e8

    aput v14, p6, v13

    .line 566
    const/4 v13, 0x1

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/ibm/icu/impl/OlsonTimeZone;->dstOffset(I)I

    move-result v14

    mul-int/lit16 v14, v14, 0x3e8

    aput v14, p6, v13

    goto :goto_6
.end method

.method private getInt(B)I
    .locals 1
    .param p1, "val"    # B

    .prologue
    .line 487
    and-int/lit16 v0, p1, 0xff

    return v0
.end method

.method private declared-synchronized initTransitionRules()V
    .locals 28

    .prologue
    .line 917
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionRulesInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_0

    .line 1034
    :goto_0
    monitor-exit p0

    return-void

    .line 921
    :cond_0
    const/4 v4, 0x0

    :try_start_1
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->initialRule:Lcom/ibm/icu/util/InitialTimeZoneRule;

    .line 922
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

    .line 923
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstFinalTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

    .line 924
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    .line 925
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransitionIdx:I

    .line 926
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZoneWithStartYear:Lcom/ibm/icu/util/SimpleTimeZone;

    .line 928
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/impl/OlsonTimeZone;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, "(STD)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    .line 929
    .local v19, "stdName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/impl/OlsonTimeZone;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, "(DST)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    .line 932
    .local v15, "dstName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    if-lez v4, :cond_2

    .line 939
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    const/4 v5, 0x0

    aget-byte v4, v4, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/ibm/icu/impl/OlsonTimeZone;->getInt(B)I

    move-result v26

    .line 940
    .local v26, "typeIdx":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    mul-int/lit8 v5, v26, 0x2

    aget v4, v4, v5

    mul-int/lit16 v6, v4, 0x3e8

    .line 941
    .local v6, "raw":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    mul-int/lit8 v5, v26, 0x2

    add-int/lit8 v5, v5, 0x1

    aget v4, v4, v5

    mul-int/lit16 v7, v4, 0x3e8

    .line 942
    .local v7, "dst":I
    new-instance v5, Lcom/ibm/icu/util/InitialTimeZoneRule;

    if-nez v7, :cond_7

    move-object/from16 v4, v19

    :goto_1
    invoke-direct {v5, v4, v6, v7}, Lcom/ibm/icu/util/InitialTimeZoneRule;-><init>(Ljava/lang/String;II)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->initialRule:Lcom/ibm/icu/util/InitialTimeZoneRule;

    .line 944
    const/16 v23, 0x1

    .local v23, "transitionIdx":I
    :goto_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    move/from16 v0, v23

    if-ge v0, v4, :cond_1

    .line 945
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransitionIdx:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransitionIdx:I

    .line 946
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    aget-byte v4, v4, v23

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/ibm/icu/impl/OlsonTimeZone;->getInt(B)I

    move-result v4

    move/from16 v0, v26

    if-eq v0, v4, :cond_8

    .line 950
    :cond_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    move/from16 v0, v23

    if-ne v0, v4, :cond_9

    .line 989
    .end local v6    # "raw":I
    .end local v7    # "dst":I
    .end local v23    # "transitionIdx":I
    .end local v26    # "typeIdx":I
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->initialRule:Lcom/ibm/icu/util/InitialTimeZoneRule;

    if-nez v4, :cond_3

    .line 991
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-int/lit16 v6, v4, 0x3e8

    .line 992
    .restart local v6    # "raw":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    mul-int/lit16 v7, v4, 0x3e8

    .line 993
    .restart local v7    # "dst":I
    new-instance v4, Lcom/ibm/icu/util/InitialTimeZoneRule;

    if-nez v7, :cond_f

    .end local v19    # "stdName":Ljava/lang/String;
    :goto_4
    move-object/from16 v0, v19

    invoke-direct {v4, v0, v6, v7}, Lcom/ibm/icu/util/InitialTimeZoneRule;-><init>(Ljava/lang/String;II)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->initialRule:Lcom/ibm/icu/util/InitialTimeZoneRule;

    .line 996
    .end local v6    # "raw":I
    .end local v7    # "dst":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v4, :cond_6

    .line 998
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalMillis:D

    double-to-long v0, v4

    move-wide/from16 v20, v0

    .line 1000
    .local v20, "startTime":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v4}, Lcom/ibm/icu/util/SimpleTimeZone;->useDaylightTime()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1009
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v4}, Lcom/ibm/icu/util/SimpleTimeZone;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/util/SimpleTimeZone;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZoneWithStartYear:Lcom/ibm/icu/util/SimpleTimeZone;

    .line 1012
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZoneWithStartYear:Lcom/ibm/icu/util/SimpleTimeZone;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Lcom/ibm/icu/util/SimpleTimeZone;->setStartYear(I)V

    .line 1014
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZoneWithStartYear:Lcom/ibm/icu/util/SimpleTimeZone;

    const/4 v5, 0x0

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1, v5}, Lcom/ibm/icu/util/SimpleTimeZone;->getNextTransition(JZ)Lcom/ibm/icu/util/TimeZoneTransition;

    move-result-object v27

    .line 1015
    .local v27, "tzt":Lcom/ibm/icu/util/TimeZoneTransition;
    invoke-virtual/range {v27 .. v27}, Lcom/ibm/icu/util/TimeZoneTransition;->getTo()Lcom/ibm/icu/util/TimeZoneRule;

    move-result-object v9

    .line 1016
    .local v9, "firstFinalRule":Lcom/ibm/icu/util/TimeZoneRule;
    invoke-virtual/range {v27 .. v27}, Lcom/ibm/icu/util/TimeZoneTransition;->getTime()J

    move-result-wide v20

    .line 1022
    .end local v27    # "tzt":Lcom/ibm/icu/util/TimeZoneTransition;
    :goto_5
    const/16 v18, 0x0

    .line 1023
    .local v18, "prevRule":Lcom/ibm/icu/util/TimeZoneRule;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    if-lez v4, :cond_4

    .line 1024
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    move-object/from16 v0, p0

    iget v10, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    add-int/lit8 v10, v10, -0x1

    aget-byte v5, v5, v10

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/ibm/icu/impl/OlsonTimeZone;->getInt(B)I

    move-result v5

    aget-object v18, v4, v5

    .line 1026
    :cond_4
    if-nez v18, :cond_5

    .line 1028
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->initialRule:Lcom/ibm/icu/util/InitialTimeZoneRule;

    move-object/from16 v18, v0

    .line 1030
    :cond_5
    new-instance v4, Lcom/ibm/icu/util/TimeZoneTransition;

    move-wide/from16 v0, v20

    move-object/from16 v2, v18

    invoke-direct {v4, v0, v1, v2, v9}, Lcom/ibm/icu/util/TimeZoneTransition;-><init>(JLcom/ibm/icu/util/TimeZoneRule;Lcom/ibm/icu/util/TimeZoneRule;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstFinalTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

    .line 1033
    .end local v9    # "firstFinalRule":Lcom/ibm/icu/util/TimeZoneRule;
    .end local v18    # "prevRule":Lcom/ibm/icu/util/TimeZoneRule;
    .end local v20    # "startTime":J
    :cond_6
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionRulesInitialized:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 917
    .end local v15    # "dstName":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .restart local v6    # "raw":I
    .restart local v7    # "dst":I
    .restart local v15    # "dstName":Ljava/lang/String;
    .restart local v19    # "stdName":Ljava/lang/String;
    .restart local v26    # "typeIdx":I
    :cond_7
    move-object v4, v15

    .line 942
    goto/16 :goto_1

    .line 944
    .restart local v23    # "transitionIdx":I
    :cond_8
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_2

    .line 954
    :cond_9
    :try_start_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    new-array v0, v4, [J

    move-object/from16 v22, v0

    .line 955
    .local v22, "times":[J
    const/16 v26, 0x0

    :goto_6
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeCount:I

    move/from16 v0, v26

    if-ge v0, v4, :cond_e

    .line 957
    const/16 v16, 0x0

    .line 958
    .local v16, "nTimes":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransitionIdx:I

    move/from16 v23, v0

    move/from16 v17, v16

    .end local v16    # "nTimes":I
    .local v17, "nTimes":I
    :goto_7
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    move/from16 v0, v23

    if-ge v0, v4, :cond_a

    .line 959
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    aget-byte v4, v4, v23

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/ibm/icu/impl/OlsonTimeZone;->getInt(B)I

    move-result v4

    move/from16 v0, v26

    if-ne v0, v4, :cond_11

    .line 960
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    aget v4, v4, v23

    int-to-long v4, v4

    const-wide/16 v10, 0x3e8

    mul-long v24, v4, v10

    .line 961
    .local v24, "tt":J
    move-wide/from16 v0, v24

    long-to-double v4, v0

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalMillis:D

    cmpg-double v4, v4, v10

    if-gez v4, :cond_11

    .line 963
    add-int/lit8 v16, v17, 0x1

    .end local v17    # "nTimes":I
    .restart local v16    # "nTimes":I
    aput-wide v24, v22, v17

    .line 958
    .end local v24    # "tt":J
    :goto_8
    add-int/lit8 v23, v23, 0x1

    move/from16 v17, v16

    .end local v16    # "nTimes":I
    .restart local v17    # "nTimes":I
    goto :goto_7

    .line 967
    :cond_a
    if-lez v17, :cond_c

    .line 968
    move/from16 v0, v17

    new-array v8, v0, [J

    .line 969
    .local v8, "startTimes":[J
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v17

    invoke-static {v0, v4, v8, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 971
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    mul-int/lit8 v5, v26, 0x2

    aget v4, v4, v5

    mul-int/lit16 v6, v4, 0x3e8

    .line 972
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    mul-int/lit8 v5, v26, 0x2

    add-int/lit8 v5, v5, 0x1

    aget v4, v4, v5

    mul-int/lit16 v7, v4, 0x3e8

    .line 973
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    if-nez v4, :cond_b

    .line 974
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeCount:I

    new-array v4, v4, [Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    .line 976
    :cond_b
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    new-instance v4, Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    if-nez v7, :cond_d

    move-object/from16 v5, v19

    :goto_9
    const/4 v9, 0x2

    invoke-direct/range {v4 .. v9}, Lcom/ibm/icu/util/TimeArrayTimeZoneRule;-><init>(Ljava/lang/String;II[JI)V

    aput-object v4, v10, v26

    .line 955
    .end local v8    # "startTimes":[J
    :cond_c
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_6

    .restart local v8    # "startTimes":[J
    :cond_d
    move-object v5, v15

    .line 976
    goto :goto_9

    .line 982
    .end local v8    # "startTimes":[J
    .end local v17    # "nTimes":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    move-object/from16 v0, p0

    iget v5, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransitionIdx:I

    aget-byte v4, v4, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/ibm/icu/impl/OlsonTimeZone;->getInt(B)I

    move-result v26

    .line 983
    new-instance v4, Lcom/ibm/icu/util/TimeZoneTransition;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransitionIdx:I

    aget v5, v5, v10

    int-to-long v10, v5

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->initialRule:Lcom/ibm/icu/util/InitialTimeZoneRule;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    aget-object v12, v12, v26

    invoke-direct {v4, v10, v11, v5, v12}, Lcom/ibm/icu/util/TimeZoneTransition;-><init>(JLcom/ibm/icu/util/TimeZoneRule;Lcom/ibm/icu/util/TimeZoneRule;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

    goto/16 :goto_3

    .end local v22    # "times":[J
    .end local v23    # "transitionIdx":I
    .end local v26    # "typeIdx":I
    :cond_f
    move-object/from16 v19, v15

    .line 993
    goto/16 :goto_4

    .line 1018
    .end local v6    # "raw":I
    .end local v7    # "dst":I
    .end local v19    # "stdName":Ljava/lang/String;
    .restart local v20    # "startTime":J
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZoneWithStartYear:Lcom/ibm/icu/util/SimpleTimeZone;

    .line 1019
    new-instance v9, Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v4}, Lcom/ibm/icu/util/SimpleTimeZone;->getID()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v4}, Lcom/ibm/icu/util/SimpleTimeZone;->getRawOffset()I

    move-result v11

    const/4 v12, 0x0

    const/4 v4, 0x1

    new-array v13, v4, [J

    const/4 v4, 0x0

    aput-wide v20, v13, v4

    const/4 v14, 0x2

    invoke-direct/range {v9 .. v14}, Lcom/ibm/icu/util/TimeArrayTimeZoneRule;-><init>(Ljava/lang/String;II[JI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .restart local v9    # "firstFinalRule":Lcom/ibm/icu/util/TimeZoneRule;
    goto/16 :goto_5

    .end local v9    # "firstFinalRule":Lcom/ibm/icu/util/TimeZoneRule;
    .end local v20    # "startTime":J
    .restart local v6    # "raw":I
    .restart local v7    # "dst":I
    .restart local v17    # "nTimes":I
    .restart local v19    # "stdName":Ljava/lang/String;
    .restart local v22    # "times":[J
    .restart local v23    # "transitionIdx":I
    .restart local v26    # "typeIdx":I
    :cond_11
    move/from16 v16, v17

    .end local v17    # "nTimes":I
    .restart local v16    # "nTimes":I
    goto/16 :goto_8
.end method

.method private static loadRule(Lcom/ibm/icu/util/UResourceBundle;Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;
    .locals 2
    .param p0, "top"    # Lcom/ibm/icu/util/UResourceBundle;
    .param p1, "ruleid"    # Ljava/lang/String;

    .prologue
    .line 678
    const-string/jumbo v1, "Rules"

    invoke-virtual {p0, v1}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .line 679
    .local v0, "r":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v0, p1}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    .line 680
    return-object v0
.end method

.method private static final myFloorDivide(JJ)J
    .locals 4
    .param p0, "numerator"    # J
    .param p2, "denominator"    # J

    .prologue
    const-wide/16 v2, 0x1

    .line 697
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    div-long v0, p0, p2

    :goto_0
    return-wide v0

    :cond_0
    add-long v0, p0, v2

    div-long/2addr v0, p2

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method private rawOffset(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 576
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    shl-int/lit8 v1, p1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method private zoneOffset(I)I
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 571
    shl-int/lit8 p1, p1, 0x1

    .line 572
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    aget v0, v0, p1

    iget-object v1, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    add-int/lit8 v2, p1, 0x1

    aget v1, v1, v2

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 197
    invoke-super {p0}, Lcom/ibm/icu/util/BasicTimeZone;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/impl/OlsonTimeZone;

    .line 198
    .local v0, "other":Lcom/ibm/icu/impl/OlsonTimeZone;
    iget-object v1, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v1, :cond_0

    .line 199
    iget-object v1, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {p0}, Lcom/ibm/icu/impl/OlsonTimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ibm/icu/util/SimpleTimeZone;->setID(Ljava/lang/String;)V

    .line 200
    iget-object v1, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v1}, Lcom/ibm/icu/util/SimpleTimeZone;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/util/SimpleTimeZone;

    iput-object v1, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    .line 202
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    invoke-virtual {v1}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    check-cast v1, [I

    iput-object v1, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    .line 203
    iget-object v1, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    invoke-virtual {v1}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    check-cast v1, [B

    iput-object v1, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    .line 204
    iget-object v1, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    invoke-virtual {v1}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    check-cast v1, [I

    iput-object v1, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    .line 205
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 703
    invoke-super {p0, p1}, Lcom/ibm/icu/util/BasicTimeZone;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 707
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 705
    check-cast v0, Lcom/ibm/icu/impl/OlsonTimeZone;

    .line 707
    .local v0, "z":Lcom/ibm/icu/impl/OlsonTimeZone;
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    iget-object v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/Utility;->arrayEquals([BLjava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    iget v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    iget v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-nez v2, :cond_2

    iget-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    iget-object v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v2, v3}, Lcom/ibm/icu/util/SimpleTimeZone;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    iget v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeCount:I

    iget v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeCount:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    iget-object v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/Utility;->arrayEquals([ILjava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    iget-object v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/Utility;->arrayEquals([ILjava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    iget-object v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/Utility;->arrayEquals([BLjava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getDSTSavings()I
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v0}, Lcom/ibm/icu/util/SimpleTimeZone;->getDSTSavings()I

    move-result v0

    .line 289
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/ibm/icu/util/BasicTimeZone;->getDSTSavings()I

    move-result v0

    goto :goto_0
.end method

.method public getNextTransition(JZ)Lcom/ibm/icu/util/TimeZoneTransition;
    .locals 17
    .param p1, "base"    # J
    .param p3, "inclusive"    # Z

    .prologue
    const-wide/16 v14, 0x3e8

    const/4 v11, 0x0

    .line 760
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/impl/OlsonTimeZone;->initTransitionRules()V

    .line 762
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v12, :cond_2

    .line 763
    if-eqz p3, :cond_1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstFinalTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

    invoke-virtual {v12}, Lcom/ibm/icu/util/TimeZoneTransition;->getTime()J

    move-result-wide v12

    cmp-long v12, p1, v12

    if-nez v12, :cond_1

    .line 764
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstFinalTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

    .line 803
    :cond_0
    :goto_0
    return-object v11

    .line 765
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstFinalTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

    invoke-virtual {v12}, Lcom/ibm/icu/util/TimeZoneTransition;->getTime()J

    move-result-wide v12

    cmp-long v12, p1, v12

    if-ltz v12, :cond_2

    .line 766
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v12}, Lcom/ibm/icu/util/SimpleTimeZone;->useDaylightTime()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 768
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZoneWithStartYear:Lcom/ibm/icu/util/SimpleTimeZone;

    move-wide/from16 v0, p1

    move/from16 v2, p3

    invoke-virtual {v11, v0, v1, v2}, Lcom/ibm/icu/util/SimpleTimeZone;->getNextTransition(JZ)Lcom/ibm/icu/util/TimeZoneTransition;

    move-result-object v11

    goto :goto_0

    .line 775
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    if-eqz v12, :cond_0

    .line 777
    move-object/from16 v0, p0

    iget v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    add-int/lit8 v10, v11, -0x1

    .line 778
    .local v10, "ttidx":I
    :goto_1
    move-object/from16 v0, p0

    iget v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransitionIdx:I

    if-lt v10, v11, :cond_3

    .line 779
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    aget v11, v11, v10

    int-to-long v12, v11

    mul-long v8, v12, v14

    .line 780
    .local v8, "t":J
    cmp-long v11, p1, v8

    if-gtz v11, :cond_3

    if-nez p3, :cond_4

    cmp-long v11, p1, v8

    if-nez v11, :cond_4

    .line 784
    .end local v8    # "t":J
    :cond_3
    move-object/from16 v0, p0

    iget v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    add-int/lit8 v11, v11, -0x1

    if-ne v10, v11, :cond_5

    .line 785
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstFinalTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

    goto :goto_0

    .line 778
    .restart local v8    # "t":J
    :cond_4
    add-int/lit8 v10, v10, -0x1

    goto :goto_1

    .line 786
    .end local v8    # "t":J
    :cond_5
    move-object/from16 v0, p0

    iget v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransitionIdx:I

    if-ge v10, v11, :cond_6

    .line 787
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

    goto :goto_0

    .line 790
    :cond_6
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    add-int/lit8 v13, v10, 0x1

    aget-byte v12, v12, v13

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/ibm/icu/impl/OlsonTimeZone;->getInt(B)I

    move-result v12

    aget-object v5, v11, v12

    .line 791
    .local v5, "to":Lcom/ibm/icu/util/TimeZoneRule;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    aget-byte v12, v12, v10

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/ibm/icu/impl/OlsonTimeZone;->getInt(B)I

    move-result v12

    aget-object v4, v11, v12

    .line 792
    .local v4, "from":Lcom/ibm/icu/util/TimeZoneRule;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    add-int/lit8 v12, v10, 0x1

    aget v11, v11, v12

    int-to-long v12, v11

    mul-long v6, v12, v14

    .line 795
    .local v6, "startTime":J
    invoke-virtual {v4}, Lcom/ibm/icu/util/TimeZoneRule;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5}, Lcom/ibm/icu/util/TimeZoneRule;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-virtual {v4}, Lcom/ibm/icu/util/TimeZoneRule;->getRawOffset()I

    move-result v11

    invoke-virtual {v5}, Lcom/ibm/icu/util/TimeZoneRule;->getRawOffset()I

    move-result v12

    if-ne v11, v12, :cond_7

    invoke-virtual {v4}, Lcom/ibm/icu/util/TimeZoneRule;->getDSTSavings()I

    move-result v11

    invoke-virtual {v5}, Lcom/ibm/icu/util/TimeZoneRule;->getDSTSavings()I

    move-result v12

    if-ne v11, v12, :cond_7

    .line 797
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7, v11}, Lcom/ibm/icu/impl/OlsonTimeZone;->getNextTransition(JZ)Lcom/ibm/icu/util/TimeZoneTransition;

    move-result-object v11

    goto/16 :goto_0

    .line 800
    :cond_7
    new-instance v11, Lcom/ibm/icu/util/TimeZoneTransition;

    invoke-direct {v11, v6, v7, v4, v5}, Lcom/ibm/icu/util/TimeZoneTransition;-><init>(JLcom/ibm/icu/util/TimeZoneRule;Lcom/ibm/icu/util/TimeZoneRule;)V

    goto/16 :goto_0
.end method

.method public getOffset(IIIIII)I
    .locals 8
    .param p1, "era"    # I
    .param p2, "year"    # I
    .param p3, "month"    # I
    .param p4, "day"    # I
    .param p5, "dayOfWeek"    # I
    .param p6, "milliseconds"    # I

    .prologue
    .line 126
    if-ltz p3, :cond_0

    const/16 v0, 0xb

    if-le p3, v0, :cond_1

    .line 127
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Month is not in the legal range: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :cond_1
    invoke-static {p2, p3}, Lcom/ibm/icu/impl/Grego;->monthLength(II)I

    move-result v7

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/ibm/icu/impl/OlsonTimeZone;->getOffset(IIIIIII)I

    move-result v0

    return v0
.end method

.method public getOffset(IIIIIII)I
    .locals 8
    .param p1, "era"    # I
    .param p2, "year"    # I
    .param p3, "month"    # I
    .param p4, "dom"    # I
    .param p5, "dow"    # I
    .param p6, "millis"    # I
    .param p7, "monthLength"    # I

    .prologue
    .line 138
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    if-ltz p3, :cond_1

    const/16 v0, 0xb

    if-gt p3, v0, :cond_1

    const/4 v0, 0x1

    if-lt p4, v0, :cond_1

    if-gt p4, p7, :cond_1

    const/4 v0, 0x1

    if-lt p5, v0, :cond_1

    const/4 v0, 0x7

    if-gt p5, v0, :cond_1

    if-ltz p6, :cond_1

    const v0, 0x5265c00

    if-ge p6, v0, :cond_1

    const/16 v0, 0x1c

    if-lt p7, v0, :cond_1

    const/16 v0, 0x1f

    if-le p7, v0, :cond_2

    .line 149
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 152
    :cond_2
    if-nez p1, :cond_3

    .line 153
    neg-int p2, p2

    .line 156
    :cond_3
    iget v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    if-le p2, v0, :cond_4

    .line 158
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/ibm/icu/util/SimpleTimeZone;->getOffset(IIIIII)I

    move-result v0

    .line 166
    :goto_0
    return v0

    .line 162
    :cond_4
    invoke-static {p2, p3, p4}, Lcom/ibm/icu/impl/Grego;->fieldsToDay(III)J

    move-result-wide v0

    const-wide/32 v4, 0x5265c00

    mul-long/2addr v0, v4

    int-to-long v4, p6

    add-long v2, v0, v4

    .line 164
    .local v2, "time":J
    const/4 v0, 0x2

    new-array v7, v0, [I

    .line 165
    .local v7, "offsets":[I
    const/4 v4, 0x1

    const/4 v5, 0x3

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/ibm/icu/impl/OlsonTimeZone;->getHistoricalOffset(JZII[I)V

    .line 166
    const/4 v0, 0x0

    aget v0, v7, v0

    const/4 v1, 0x1

    aget v1, v7, v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getOffset(JZ[I)V
    .locals 9
    .param p1, "date"    # J
    .param p3, "local"    # Z
    .param p4, "offsets"    # [I

    .prologue
    .line 215
    long-to-double v0, p1

    iget-wide v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalMillis:D

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/ibm/icu/util/SimpleTimeZone;->getOffset(JZ[I)V

    .line 221
    :goto_0
    return-void

    .line 218
    :cond_0
    const/4 v5, 0x4

    const/16 v6, 0xc

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, Lcom/ibm/icu/impl/OlsonTimeZone;->getHistoricalOffset(JZII[I)V

    goto :goto_0
.end method

.method public getOffsetFromLocal(JII[I)V
    .locals 9
    .param p1, "date"    # J
    .param p3, "nonExistingTimeOpt"    # I
    .param p4, "duplicatedTimeOpt"    # I
    .param p5, "offsets"    # [I

    .prologue
    .line 230
    long-to-double v0, p1

    iget-wide v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalMillis:D

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v0, :cond_0

    .line 231
    iget-object v1, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/ibm/icu/util/SimpleTimeZone;->getOffsetFromLocal(JII[I)V

    .line 235
    :goto_0
    return-void

    .line 233
    :cond_0
    const/4 v4, 0x1

    move-object v1, p0

    move-wide v2, p1

    move v5, p3

    move v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/ibm/icu/impl/OlsonTimeZone;->getHistoricalOffset(JZII[I)V

    goto :goto_0
.end method

.method public getPreviousTransition(JZ)Lcom/ibm/icu/util/TimeZoneTransition;
    .locals 17
    .param p1, "base"    # J
    .param p3, "inclusive"    # Z

    .prologue
    const-wide/16 v14, 0x3e8

    const/4 v11, 0x0

    .line 810
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/impl/OlsonTimeZone;->initTransitionRules()V

    .line 812
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v12, :cond_3

    .line 813
    if-eqz p3, :cond_1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstFinalTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

    invoke-virtual {v12}, Lcom/ibm/icu/util/TimeZoneTransition;->getTime()J

    move-result-wide v12

    cmp-long v12, p1, v12

    if-nez v12, :cond_1

    .line 814
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstFinalTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

    .line 854
    :cond_0
    :goto_0
    return-object v11

    .line 815
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstFinalTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

    invoke-virtual {v12}, Lcom/ibm/icu/util/TimeZoneTransition;->getTime()J

    move-result-wide v12

    cmp-long v12, p1, v12

    if-lez v12, :cond_3

    .line 816
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v11}, Lcom/ibm/icu/util/SimpleTimeZone;->useDaylightTime()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 818
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZoneWithStartYear:Lcom/ibm/icu/util/SimpleTimeZone;

    move-wide/from16 v0, p1

    move/from16 v2, p3

    invoke-virtual {v11, v0, v1, v2}, Lcom/ibm/icu/util/SimpleTimeZone;->getPreviousTransition(JZ)Lcom/ibm/icu/util/TimeZoneTransition;

    move-result-object v11

    goto :goto_0

    .line 820
    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstFinalTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

    goto :goto_0

    .line 825
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    if-eqz v12, :cond_0

    .line 827
    move-object/from16 v0, p0

    iget v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    add-int/lit8 v10, v12, -0x1

    .line 828
    .local v10, "ttidx":I
    :goto_1
    move-object/from16 v0, p0

    iget v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransitionIdx:I

    if-lt v10, v12, :cond_4

    .line 829
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    aget v12, v12, v10

    int-to-long v12, v12

    mul-long v8, v12, v14

    .line 830
    .local v8, "t":J
    cmp-long v12, p1, v8

    if-gtz v12, :cond_4

    if-eqz p3, :cond_5

    cmp-long v12, p1, v8

    if-nez v12, :cond_5

    .line 834
    .end local v8    # "t":J
    :cond_4
    move-object/from16 v0, p0

    iget v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransitionIdx:I

    if-lt v10, v12, :cond_0

    .line 837
    move-object/from16 v0, p0

    iget v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransitionIdx:I

    if-ne v10, v11, :cond_6

    .line 838
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->firstTZTransition:Lcom/ibm/icu/util/TimeZoneTransition;

    goto :goto_0

    .line 828
    .restart local v8    # "t":J
    :cond_5
    add-int/lit8 v10, v10, -0x1

    goto :goto_1

    .line 841
    .end local v8    # "t":J
    :cond_6
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    aget-byte v12, v12, v10

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/ibm/icu/impl/OlsonTimeZone;->getInt(B)I

    move-result v12

    aget-object v5, v11, v12

    .line 842
    .local v5, "to":Lcom/ibm/icu/util/TimeZoneRule;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    add-int/lit8 v13, v10, -0x1

    aget-byte v12, v12, v13

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/ibm/icu/impl/OlsonTimeZone;->getInt(B)I

    move-result v12

    aget-object v4, v11, v12

    .line 843
    .local v4, "from":Lcom/ibm/icu/util/TimeZoneRule;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    aget v11, v11, v10

    int-to-long v12, v11

    mul-long v6, v12, v14

    .line 846
    .local v6, "startTime":J
    invoke-virtual {v4}, Lcom/ibm/icu/util/TimeZoneRule;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5}, Lcom/ibm/icu/util/TimeZoneRule;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-virtual {v4}, Lcom/ibm/icu/util/TimeZoneRule;->getRawOffset()I

    move-result v11

    invoke-virtual {v5}, Lcom/ibm/icu/util/TimeZoneRule;->getRawOffset()I

    move-result v12

    if-ne v11, v12, :cond_7

    invoke-virtual {v4}, Lcom/ibm/icu/util/TimeZoneRule;->getDSTSavings()I

    move-result v11

    invoke-virtual {v5}, Lcom/ibm/icu/util/TimeZoneRule;->getDSTSavings()I

    move-result v12

    if-ne v11, v12, :cond_7

    .line 848
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7, v11}, Lcom/ibm/icu/impl/OlsonTimeZone;->getPreviousTransition(JZ)Lcom/ibm/icu/util/TimeZoneTransition;

    move-result-object v11

    goto/16 :goto_0

    .line 851
    :cond_7
    new-instance v11, Lcom/ibm/icu/util/TimeZoneTransition;

    invoke-direct {v11, v6, v7, v4, v5}, Lcom/ibm/icu/util/TimeZoneTransition;-><init>(JLcom/ibm/icu/util/TimeZoneRule;Lcom/ibm/icu/util/TimeZoneRule;)V

    goto/16 :goto_0
.end method

.method public getRawOffset()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 241
    const/4 v1, 0x2

    new-array v0, v1, [I

    .line 242
    .local v0, "ret":[I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3, v4, v0}, Lcom/ibm/icu/impl/OlsonTimeZone;->getOffset(JZ[I)V

    .line 243
    aget v1, v0, v4

    return v1
.end method

.method public getTimeZoneRules()[Lcom/ibm/icu/util/TimeZoneRule;
    .locals 14

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 861
    invoke-direct {p0}, Lcom/ibm/icu/impl/OlsonTimeZone;->initTransitionRules()V

    .line 862
    const/4 v10, 0x1

    .line 863
    .local v10, "size":I
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    if-eqz v0, :cond_1

    .line 866
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    array-length v0, v0

    if-ge v6, v0, :cond_1

    .line 867
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    aget-object v0, v0, v6

    if-eqz v0, :cond_0

    .line 868
    add-int/lit8 v10, v10, 0x1

    .line 866
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 872
    .end local v6    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v0, :cond_2

    .line 873
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v0}, Lcom/ibm/icu/util/SimpleTimeZone;->useDaylightTime()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 874
    add-int/lit8 v10, v10, 0x2

    .line 880
    :cond_2
    :goto_1
    new-array v9, v10, [Lcom/ibm/icu/util/TimeZoneRule;

    .line 881
    .local v9, "rules":[Lcom/ibm/icu/util/TimeZoneRule;
    const/4 v7, 0x0

    .line 882
    .local v7, "idx":I
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "idx":I
    .local v8, "idx":I
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->initialRule:Lcom/ibm/icu/util/InitialTimeZoneRule;

    aput-object v0, v9, v7

    .line 884
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    if-eqz v0, :cond_5

    .line 885
    const/4 v6, 0x0

    .restart local v6    # "i":I
    move v7, v8

    .end local v8    # "idx":I
    .restart local v7    # "idx":I
    :goto_2
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    array-length v0, v0

    if-ge v6, v0, :cond_6

    .line 886
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    aget-object v0, v0, v6

    if-eqz v0, :cond_3

    .line 887
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "idx":I
    .restart local v8    # "idx":I
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->historicRules:[Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    aget-object v0, v0, v6

    aput-object v0, v9, v7

    move v7, v8

    .line 885
    .end local v8    # "idx":I
    .restart local v7    # "idx":I
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 876
    .end local v6    # "i":I
    .end local v7    # "idx":I
    .end local v9    # "rules":[Lcom/ibm/icu/util/TimeZoneRule;
    :cond_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .restart local v8    # "idx":I
    .restart local v9    # "rules":[Lcom/ibm/icu/util/TimeZoneRule;
    :cond_5
    move v7, v8

    .line 892
    .end local v8    # "idx":I
    .restart local v7    # "idx":I
    :cond_6
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v0, :cond_7

    .line 893
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v0}, Lcom/ibm/icu/util/SimpleTimeZone;->useDaylightTime()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 894
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZoneWithStartYear:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v0}, Lcom/ibm/icu/util/SimpleTimeZone;->getTimeZoneRules()[Lcom/ibm/icu/util/TimeZoneRule;

    move-result-object v11

    .line 896
    .local v11, "stzr":[Lcom/ibm/icu/util/TimeZoneRule;
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "idx":I
    .restart local v8    # "idx":I
    aget-object v0, v11, v4

    aput-object v0, v9, v7

    .line 897
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "idx":I
    .restart local v7    # "idx":I
    aget-object v0, v11, v5

    aput-object v0, v9, v8

    .line 904
    .end local v11    # "stzr":[Lcom/ibm/icu/util/TimeZoneRule;
    :cond_7
    :goto_3
    return-object v9

    .line 900
    :cond_8
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "idx":I
    .restart local v8    # "idx":I
    new-instance v0, Lcom/ibm/icu/util/TimeArrayTimeZoneRule;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lcom/ibm/icu/impl/OlsonTimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "(STD)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v2}, Lcom/ibm/icu/util/SimpleTimeZone;->getRawOffset()I

    move-result v2

    new-array v4, v4, [J

    iget-wide v12, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalMillis:D

    double-to-long v12, v12

    aput-wide v12, v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/util/TimeArrayTimeZoneRule;-><init>(Ljava/lang/String;II[JI)V

    aput-object v0, v9, v7

    move v7, v8

    .end local v8    # "idx":I
    .restart local v7    # "idx":I
    goto :goto_3
.end method

.method public hasSameRules(Lcom/ibm/icu/util/TimeZone;)Z
    .locals 4
    .param p1, "other"    # Lcom/ibm/icu/util/TimeZone;

    .prologue
    const/4 v1, 0x0

    .line 307
    invoke-super {p0, p1}, Lcom/ibm/icu/util/BasicTimeZone;->hasSameRules(Lcom/ibm/icu/util/TimeZone;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 339
    :cond_0
    :goto_0
    return v1

    .line 311
    :cond_1
    instance-of v2, p1, Lcom/ibm/icu/impl/OlsonTimeZone;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 317
    check-cast v0, Lcom/ibm/icu/impl/OlsonTimeZone;

    .line 318
    .local v0, "o":Lcom/ibm/icu/impl/OlsonTimeZone;
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-nez v2, :cond_3

    .line 319
    iget-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    const v3, 0x7fffffff

    if-ne v2, v3, :cond_0

    .line 332
    :cond_2
    iget v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    iget v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    iget-object v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeCount:I

    iget v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeCount:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    iget-object v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    iget-object v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 339
    const/4 v1, 0x1

    goto :goto_0

    .line 323
    :cond_3
    iget-object v2, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    iget v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    iget-object v3, v0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v2, v3}, Lcom/ibm/icu/util/SimpleTimeZone;->hasSameRules(Lcom/ibm/icu/util/TimeZone;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 725
    iget v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    iget v3, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    ushr-int/lit8 v3, v3, 0x4

    iget v4, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    add-int/2addr v3, v4

    xor-int/2addr v2, v3

    iget v3, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    ushr-int/lit8 v3, v3, 0x6

    iget v4, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeCount:I

    add-int/2addr v3, v4

    xor-int/2addr v2, v3

    int-to-long v4, v2

    iget v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeCount:I

    ushr-int/lit8 v2, v2, 0x8

    int-to-long v2, v2

    iget-wide v6, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalMillis:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    add-long/2addr v6, v2

    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    int-to-long v2, v2

    add-long/2addr v2, v6

    invoke-super {p0}, Lcom/ibm/icu/util/BasicTimeZone;->hashCode()I

    move-result v6

    int-to-long v6, v6

    add-long/2addr v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    .line 731
    .local v1, "ret":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 732
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    aget v2, v2, v0

    iget-object v3, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    aget v3, v3, v0

    ushr-int/lit8 v3, v3, 0x8

    xor-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 731
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 725
    .end local v0    # "i":I
    .end local v1    # "ret":I
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v2}, Lcom/ibm/icu/util/SimpleTimeZone;->hashCode()I

    move-result v2

    goto :goto_0

    .line 734
    .restart local v0    # "i":I
    .restart local v1    # "ret":I
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 735
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    aget v2, v2, v0

    iget-object v3, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    aget v3, v3, v0

    ushr-int/lit8 v3, v3, 0x8

    xor-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 734
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 737
    :cond_2
    const/4 v0, 0x0

    :goto_3
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 738
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    aget-byte v2, v2, v0

    and-int/lit16 v2, v2, 0xff

    add-int/2addr v1, v2

    .line 737
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 740
    :cond_3
    return v1
.end method

.method public inDaylightTime(Ljava/util/Date;)Z
    .locals 6
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 296
    const/4 v3, 0x2

    new-array v0, v3, [I

    .line 297
    .local v0, "temp":[I
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5, v2, v0}, Lcom/ibm/icu/impl/OlsonTimeZone;->getOffset(JZ[I)V

    .line 298
    aget v3, v0, v1

    if-eqz v3, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public setID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 477
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/util/SimpleTimeZone;->setID(Ljava/lang/String;)V

    .line 480
    :cond_0
    invoke-super {p0, p1}, Lcom/ibm/icu/util/BasicTimeZone;->setID(Ljava/lang/String;)V

    .line 481
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionRulesInitialized:Z

    .line 482
    return-void
.end method

.method public setRawOffset(I)V
    .locals 7
    .param p1, "offsetMillis"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 173
    invoke-virtual {p0}, Lcom/ibm/icu/impl/OlsonTimeZone;->getRawOffset()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 194
    :goto_0
    return-void

    .line 176
    :cond_0
    new-instance v0, Lcom/ibm/icu/util/GregorianCalendar;

    sget-object v2, Lcom/ibm/icu/util/ULocale;->ROOT:Lcom/ibm/icu/util/ULocale;

    invoke-direct {v0, v2}, Lcom/ibm/icu/util/GregorianCalendar;-><init>(Lcom/ibm/icu/util/ULocale;)V

    .line 177
    .local v0, "cal":Lcom/ibm/icu/util/GregorianCalendar;
    invoke-virtual {v0, p0}, Lcom/ibm/icu/util/GregorianCalendar;->setTimeZone(Lcom/ibm/icu/util/TimeZone;)V

    .line 178
    invoke-virtual {v0, v3}, Lcom/ibm/icu/util/GregorianCalendar;->get(I)I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .line 181
    .local v1, "tmpFinalYear":I
    iget v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    if-le v2, v1, :cond_1

    .line 182
    iput v1, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    .line 183
    invoke-static {v1, v6, v3}, Lcom/ibm/icu/impl/Grego;->fieldsToDay(III)J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    mul-long/2addr v2, v4

    long-to-double v2, v2

    iput-wide v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalMillis:D

    .line 185
    :cond_1
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-nez v2, :cond_2

    .line 187
    new-instance v2, Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {p0}, Lcom/ibm/icu/impl/OlsonTimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/ibm/icu/util/SimpleTimeZone;-><init>(ILjava/lang/String;)V

    iput-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    .line 193
    :goto_1
    iput-boolean v6, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionRulesInitialized:Z

    goto :goto_0

    .line 189
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v2, p1}, Lcom/ibm/icu/util/SimpleTimeZone;->setRawOffset(I)V

    .line 190
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    iget v3, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    invoke-virtual {v2, v3}, Lcom/ibm/icu/util/SimpleTimeZone;->setStartYear(I)V

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v5, 0x2c

    const/16 v6, 0x5d

    const/16 v4, 0x5b

    .line 585
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 586
    .local v0, "buf":Ljava/lang/StringBuffer;
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 587
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 588
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "transitionCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget v3, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 589
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, ",typeCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget v3, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 590
    const-string/jumbo v2, ",transitionTimes="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 591
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    if-eqz v2, :cond_3

    .line 592
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 593
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 594
    if-lez v1, :cond_0

    .line 595
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 597
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    aget v2, v2, v1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 593
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 599
    :cond_1
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 603
    .end local v1    # "i":I
    :goto_1
    const-string/jumbo v2, ",typeOffsets="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 604
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    if-eqz v2, :cond_5

    .line 605
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 606
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 607
    if-lez v1, :cond_2

    .line 608
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 610
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeOffsets:[I

    aget v2, v2, v1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 606
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 601
    .end local v1    # "i":I
    :cond_3
    const-string/jumbo v2, "null"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 612
    .restart local v1    # "i":I
    :cond_4
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 616
    .end local v1    # "i":I
    :goto_3
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, ",finalYear="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget v3, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 617
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, ",finalMillis="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-wide v4, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalMillis:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 618
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, ",finalZone="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 619
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 621
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 614
    :cond_5
    const-string/jumbo v2, "null"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3
.end method

.method public useDaylightTime()Z
    .locals 14

    .prologue
    const-wide/32 v12, 0x15180

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 255
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const/4 v9, 0x0

    invoke-static {v10, v11, v9}, Lcom/ibm/icu/impl/Grego;->timeToFields(J[I)[I

    move-result-object v0

    .line 256
    .local v0, "fields":[I
    aget v6, v0, v8

    .line 258
    .local v6, "year":I
    iget v9, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalYear:I

    if-le v6, v9, :cond_2

    .line 259
    iget-object v9, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->finalZone:Lcom/ibm/icu/util/SimpleTimeZone;

    invoke-virtual {v9}, Lcom/ibm/icu/util/SimpleTimeZone;->useDaylightTime()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 277
    :cond_0
    :goto_0
    return v7

    :cond_1
    move v7, v8

    .line 259
    goto :goto_0

    .line 263
    :cond_2
    invoke-static {v6, v8, v7}, Lcom/ibm/icu/impl/Grego;->fieldsToDay(III)J

    move-result-wide v10

    mul-long v4, v10, v12

    .line 264
    .local v4, "start":J
    add-int/lit8 v9, v6, 0x1

    invoke-static {v9, v8, v7}, Lcom/ibm/icu/impl/Grego;->fieldsToDay(III)J

    move-result-wide v10

    mul-long v2, v10, v12

    .line 268
    .local v2, "limit":J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v9, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionCount:I

    if-ge v1, v9, :cond_3

    .line 269
    iget-object v9, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    aget v9, v9, v1

    int-to-long v10, v9

    cmp-long v9, v10, v2

    if-ltz v9, :cond_4

    :cond_3
    move v7, v8

    .line 277
    goto :goto_0

    .line 272
    :cond_4
    iget-object v9, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->transitionTimes:[I

    aget v9, v9, v1

    int-to-long v10, v9

    cmp-long v9, v10, v4

    if-ltz v9, :cond_5

    iget-object v9, p0, Lcom/ibm/icu/impl/OlsonTimeZone;->typeData:[B

    aget-byte v9, v9, v1

    invoke-direct {p0, v9}, Lcom/ibm/icu/impl/OlsonTimeZone;->dstOffset(I)I

    move-result v9

    if-nez v9, :cond_0

    .line 268
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
