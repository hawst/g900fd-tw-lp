.class public Lcom/ibm/icu/impl/RuleCharacterIterator;
.super Ljava/lang/Object;
.source "RuleCharacterIterator.java"


# static fields
.field public static final DONE:I = -0x1

.field public static final PARSE_ESCAPES:I = 0x2

.field public static final PARSE_VARIABLES:I = 0x1

.field public static final SKIP_WHITESPACE:I = 0x4


# instance fields
.field private buf:[C

.field private bufPos:I

.field private isEscaped:Z

.field private pos:Ljava/text/ParsePosition;

.field private sym:Lcom/ibm/icu/text/SymbolTable;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/ibm/icu/text/SymbolTable;Ljava/text/ParsePosition;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "sym"    # Lcom/ibm/icu/text/SymbolTable;
    .param p3, "pos"    # Ljava/text/ParsePosition;

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    if-eqz p1, :cond_0

    invoke-virtual {p3}, Ljava/text/ParsePosition;->getIndex()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 105
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 107
    :cond_1
    iput-object p1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->text:Ljava/lang/String;

    .line 108
    iput-object p2, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->sym:Lcom/ibm/icu/text/SymbolTable;

    .line 109
    iput-object p3, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    .line 111
    return-void
.end method

.method private _advance(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 333
    iget-object v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    if-eqz v0, :cond_1

    .line 334
    iget v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->bufPos:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->bufPos:I

    .line 335
    iget v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->bufPos:I

    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 336
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    invoke-virtual {v1}, Ljava/text/ParsePosition;->getIndex()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 340
    iget-object v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    invoke-virtual {v0}, Ljava/text/ParsePosition;->getIndex()I

    move-result v0

    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->text:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 341
    iget-object v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->text:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    goto :goto_0
.end method

.method private _current()I
    .locals 5

    .prologue
    .line 320
    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    if-eqz v1, :cond_0

    .line 321
    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    array-length v3, v3

    iget v4, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->bufPos:I

    invoke-static {v1, v2, v3, v4}, Lcom/ibm/icu/text/UTF16;->charAt([CIII)I

    move-result v1

    .line 324
    :goto_0
    return v1

    .line 323
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    invoke-virtual {v1}, Ljava/text/ParsePosition;->getIndex()I

    move-result v0

    .line 324
    .local v0, "i":I
    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->text:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->text:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method


# virtual methods
.method public atEnd()Z
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    invoke-virtual {v0}, Ljava/text/ParsePosition;->getIndex()I

    move-result v0

    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->text:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPos(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "p"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 215
    if-nez p1, :cond_0

    .line 216
    new-array p1, v3, [Ljava/lang/Object;

    .end local p1    # "p":Ljava/lang/Object;
    iget-object v2, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    aput-object v2, p1, v4

    new-array v2, v3, [I

    iget-object v3, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    invoke-virtual {v3}, Ljava/text/ParsePosition;->getIndex()I

    move-result v3

    aput v3, v2, v4

    iget v3, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->bufPos:I

    aput v3, v2, v5

    aput-object v2, p1, v5

    .line 223
    :goto_0
    return-object p1

    .restart local p1    # "p":Ljava/lang/Object;
    :cond_0
    move-object v2, p1

    .line 218
    check-cast v2, [Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, [Ljava/lang/Object;

    .line 219
    .local v0, "a":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    aput-object v2, v0, v4

    .line 220
    aget-object v2, v0, v5

    check-cast v2, [I

    move-object v1, v2

    check-cast v1, [I

    .line 221
    .local v1, "v":[I
    iget-object v2, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    invoke-virtual {v2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v2

    aput v2, v1, v4

    .line 222
    iget v2, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->bufPos:I

    aput v2, v1, v5

    goto :goto_0
.end method

.method public inVariable()Z
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEscaped()Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->isEscaped:Z

    return v0
.end method

.method public jumpahead(I)V
    .locals 3
    .param p1, "count"    # I

    .prologue
    .line 283
    if-gez p1, :cond_0

    .line 284
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 286
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    if-eqz v1, :cond_3

    .line 287
    iget v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->bufPos:I

    add-int/2addr v1, p1

    iput v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->bufPos:I

    .line 288
    iget v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->bufPos:I

    iget-object v2, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    array-length v2, v2

    if-le v1, v2, :cond_1

    .line 289
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 291
    :cond_1
    iget v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->bufPos:I

    iget-object v2, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    array-length v2, v2

    if-ne v1, v2, :cond_2

    .line 292
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    .line 301
    :cond_2
    return-void

    .line 295
    :cond_3
    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    invoke-virtual {v1}, Ljava/text/ParsePosition;->getIndex()I

    move-result v1

    add-int v0, v1, p1

    .line 296
    .local v0, "i":I
    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    invoke-virtual {v1, v0}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 297
    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->text:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_2

    .line 298
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1
.end method

.method public lookahead()Ljava/lang/String;
    .locals 5

    .prologue
    .line 270
    iget-object v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    if-eqz v0, :cond_0

    .line 271
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    iget v2, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->bufPos:I

    iget-object v3, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    array-length v3, v3

    iget v4, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->bufPos:I

    sub-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    .line 273
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->text:Ljava/lang/String;

    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    invoke-virtual {v1}, Ljava/text/ParsePosition;->getIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public next(I)I
    .locals 9
    .param p1, "options"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 129
    const/4 v0, -0x1

    .line 130
    .local v0, "c":I
    iput-boolean v7, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->isEscaped:Z

    .line 133
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/ibm/icu/impl/RuleCharacterIterator;->_current()I

    move-result v0

    .line 134
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v3

    invoke-direct {p0, v3}, Lcom/ibm/icu/impl/RuleCharacterIterator;->_advance(I)V

    .line 136
    const/16 v3, 0x24

    if-ne v0, v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    if-nez v3, :cond_4

    and-int/lit8 v3, p1, 0x1

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->sym:Lcom/ibm/icu/text/SymbolTable;

    if-eqz v3, :cond_4

    .line 138
    iget-object v3, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->sym:Lcom/ibm/icu/text/SymbolTable;

    iget-object v4, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->text:Ljava/lang/String;

    iget-object v5, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    iget-object v6, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->text:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-interface {v3, v4, v5, v6}, Lcom/ibm/icu/text/SymbolTable;->parseReference(Ljava/lang/String;Ljava/text/ParsePosition;I)Ljava/lang/String;

    move-result-object v1

    .line 141
    .local v1, "name":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 175
    .end local v1    # "name":Ljava/lang/String;
    :cond_1
    return v0

    .line 144
    .restart local v1    # "name":Ljava/lang/String;
    :cond_2
    iput v7, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->bufPos:I

    .line 145
    iget-object v3, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->sym:Lcom/ibm/icu/text/SymbolTable;

    invoke-interface {v3, v1}, Lcom/ibm/icu/text/SymbolTable;->lookup(Ljava/lang/String;)[C

    move-result-object v3

    iput-object v3, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    .line 146
    iget-object v3, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    if-nez v3, :cond_3

    .line 147
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "Undefined variable: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 151
    :cond_3
    iget-object v3, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    array-length v3, v3

    if-nez v3, :cond_0

    .line 152
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    goto :goto_0

    .line 157
    .end local v1    # "name":Ljava/lang/String;
    :cond_4
    and-int/lit8 v3, p1, 0x4

    if-eqz v3, :cond_5

    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 162
    :cond_5
    const/16 v3, 0x5c

    if-ne v0, v3, :cond_1

    and-int/lit8 v3, p1, 0x2

    if-eqz v3, :cond_1

    .line 163
    new-array v2, v8, [I

    aput v7, v2, v7

    .line 164
    .local v2, "offset":[I
    invoke-virtual {p0}, Lcom/ibm/icu/impl/RuleCharacterIterator;->lookahead()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/ibm/icu/impl/Utility;->unescapeAt(Ljava/lang/String;[I)I

    move-result v0

    .line 165
    aget v3, v2, v7

    invoke-virtual {p0, v3}, Lcom/ibm/icu/impl/RuleCharacterIterator;->jumpahead(I)V

    .line 166
    iput-boolean v8, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->isEscaped:Z

    .line 167
    if-gez v0, :cond_1

    .line 168
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Invalid escape"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public setPos(Ljava/lang/Object;)V
    .locals 5
    .param p1, "p"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 232
    check-cast p1, [Ljava/lang/Object;

    .end local p1    # "p":Ljava/lang/Object;
    move-object v0, p1

    check-cast v0, [Ljava/lang/Object;

    .line 233
    .local v0, "a":[Ljava/lang/Object;
    aget-object v2, v0, v3

    check-cast v2, [C

    check-cast v2, [C

    iput-object v2, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->buf:[C

    .line 234
    aget-object v2, v0, v4

    check-cast v2, [I

    move-object v1, v2

    check-cast v1, [I

    .line 235
    .local v1, "v":[I
    iget-object v2, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    aget v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 236
    aget v2, v1, v4

    iput v2, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->bufPos:I

    .line 237
    return-void
.end method

.method public skipIgnored(I)V
    .locals 2
    .param p1, "options"    # I

    .prologue
    .line 248
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_0

    .line 250
    :goto_0
    invoke-direct {p0}, Lcom/ibm/icu/impl/RuleCharacterIterator;->_current()I

    move-result v0

    .line 251
    .local v0, "a":I
    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 255
    .end local v0    # "a":I
    :cond_0
    return-void

    .line 252
    .restart local v0    # "a":I
    :cond_1
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->_advance(I)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 310
    iget-object v1, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->pos:Ljava/text/ParsePosition;

    invoke-virtual {v1}, Ljava/text/ParsePosition;->getIndex()I

    move-result v0

    .line 311
    .local v0, "b":I
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->text:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const/16 v2, 0x7c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/ibm/icu/impl/RuleCharacterIterator;->text:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
