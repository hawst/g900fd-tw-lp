.class final Lcom/ibm/icu/impl/UBiDiProps$IsAcceptable;
.super Ljava/lang/Object;
.source "UBiDiProps.java"

# interfaces
.implements Lcom/ibm/icu/impl/ICUBinary$Authenticate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/impl/UBiDiProps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "IsAcceptable"
.end annotation


# instance fields
.field private final this$0:Lcom/ibm/icu/impl/UBiDiProps;


# direct methods
.method private constructor <init>(Lcom/ibm/icu/impl/UBiDiProps;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ibm/icu/impl/UBiDiProps$IsAcceptable;->this$0:Lcom/ibm/icu/impl/UBiDiProps;

    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/impl/UBiDiProps;Lcom/ibm/icu/impl/UBiDiProps$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/ibm/icu/impl/UBiDiProps;
    .param p2, "x1"    # Lcom/ibm/icu/impl/UBiDiProps$1;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/ibm/icu/impl/UBiDiProps$IsAcceptable;-><init>(Lcom/ibm/icu/impl/UBiDiProps;)V

    return-void
.end method


# virtual methods
.method public isDataVersionAcceptable([B)Z
    .locals 5
    .param p1, "version"    # [B

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 89
    aget-byte v2, p1, v1

    if-ne v2, v0, :cond_0

    aget-byte v2, p1, v4

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    const/4 v2, 0x3

    aget-byte v2, p1, v2

    if-ne v2, v4, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
