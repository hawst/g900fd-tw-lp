.class public Lcom/ibm/icu/math/BigDecimal;
.super Ljava/lang/Number;
.source "BigDecimal.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# static fields
.field private static final MaxArg:I = 0x3b9ac9ff

.field private static final MaxExp:I = 0x3b9ac9ff

.field private static final MinArg:I = -0x3b9ac9ff

.field private static final MinExp:I = -0x3b9ac9ff

.field public static final ONE:Lcom/ibm/icu/math/BigDecimal;

.field public static final ROUND_CEILING:I = 0x2

.field public static final ROUND_DOWN:I = 0x1

.field public static final ROUND_FLOOR:I = 0x3

.field public static final ROUND_HALF_DOWN:I = 0x5

.field public static final ROUND_HALF_EVEN:I = 0x6

.field public static final ROUND_HALF_UP:I = 0x4

.field public static final ROUND_UNNECESSARY:I = 0x7

.field public static final ROUND_UP:I = 0x0

.field public static final TEN:Lcom/ibm/icu/math/BigDecimal;

.field public static final ZERO:Lcom/ibm/icu/math/BigDecimal;

.field private static bytecar:[B = null

.field private static bytedig:[B = null

.field private static final isneg:B = -0x1t

.field private static final ispos:B = 0x1t

.field private static final iszero:B = 0x0t

.field private static final plainMC:Lcom/ibm/icu/math/MathContext;

.field private static final serialVersionUID:J = 0x726d636b3a313030L


# instance fields
.field private exp:I

.field private form:B

.field private ind:B

.field private mant:[B


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 275
    new-instance v0, Lcom/ibm/icu/math/BigDecimal;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/ibm/icu/math/BigDecimal;-><init>(J)V

    sput-object v0, Lcom/ibm/icu/math/BigDecimal;->ZERO:Lcom/ibm/icu/math/BigDecimal;

    .line 285
    new-instance v0, Lcom/ibm/icu/math/BigDecimal;

    const-wide/16 v2, 0x1

    invoke-direct {v0, v2, v3}, Lcom/ibm/icu/math/BigDecimal;-><init>(J)V

    sput-object v0, Lcom/ibm/icu/math/BigDecimal;->ONE:Lcom/ibm/icu/math/BigDecimal;

    .line 295
    new-instance v0, Lcom/ibm/icu/math/BigDecimal;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lcom/ibm/icu/math/BigDecimal;-><init>(I)V

    sput-object v0, Lcom/ibm/icu/math/BigDecimal;->TEN:Lcom/ibm/icu/math/BigDecimal;

    .line 368
    new-instance v0, Lcom/ibm/icu/math/MathContext;

    invoke-direct {v0, v4, v4}, Lcom/ibm/icu/math/MathContext;-><init>(II)V

    sput-object v0, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    .line 379
    const/16 v0, 0xbe

    new-array v0, v0, [B

    sput-object v0, Lcom/ibm/icu/math/BigDecimal;->bytecar:[B

    .line 380
    invoke-static {}, Lcom/ibm/icu/math/BigDecimal;->diginit()[B

    move-result-object v0

    sput-object v0, Lcom/ibm/icu/math/BigDecimal;->bytedig:[B

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1044
    invoke-direct {p0}, Ljava/lang/Number;-><init>()V

    .line 414
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/ibm/icu/math/BigDecimal;->form:B

    .line 1045
    return-void
.end method

.method public constructor <init>(D)V
    .locals 1
    .param p1, "num"    # D

    .prologue
    .line 838
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1, p2}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/ibm/icu/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 840
    return-void
.end method

.method public constructor <init>(I)V
    .locals 7
    .param p1, "num"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x1

    .line 856
    invoke-direct {p0}, Ljava/lang/Number;-><init>()V

    .line 414
    iput-byte v6, p0, Lcom/ibm/icu/math/BigDecimal;->form:B

    .line 858
    const/4 v0, 0x0

    .line 860
    .local v0, "i":I
    const/16 v2, 0x9

    if-gt p1, v2, :cond_5

    .line 861
    const/16 v2, -0x9

    if-lt p1, v2, :cond_5

    .line 865
    if-nez p1, :cond_1

    .line 867
    sget-object v2, Lcom/ibm/icu/math/BigDecimal;->ZERO:Lcom/ibm/icu/math/BigDecimal;

    iget-object v2, v2, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    iput-object v2, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 868
    iput-byte v6, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 926
    :cond_0
    :goto_0
    return-void

    .line 870
    :cond_1
    if-ne p1, v4, :cond_2

    .line 872
    sget-object v2, Lcom/ibm/icu/math/BigDecimal;->ONE:Lcom/ibm/icu/math/BigDecimal;

    iget-object v2, v2, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    iput-object v2, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 873
    iput-byte v4, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    goto :goto_0

    .line 875
    :cond_2
    if-ne p1, v5, :cond_3

    .line 877
    sget-object v2, Lcom/ibm/icu/math/BigDecimal;->ONE:Lcom/ibm/icu/math/BigDecimal;

    iget-object v2, v2, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    iput-object v2, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 878
    iput-byte v5, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    goto :goto_0

    .line 882
    :cond_3
    new-array v2, v4, [B

    iput-object v2, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 883
    if-lez p1, :cond_4

    .line 885
    iget-object v2, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    int-to-byte v3, p1

    aput-byte v3, v2, v6

    .line 886
    iput-byte v4, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    goto :goto_0

    .line 890
    :cond_4
    iget-object v2, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    neg-int v3, p1

    int-to-byte v3, v3

    aput-byte v3, v2, v6

    .line 891
    iput-byte v5, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    goto :goto_0

    .line 900
    :cond_5
    if-lez p1, :cond_6

    .line 902
    iput-byte v4, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 903
    neg-int p1, p1

    .line 910
    :goto_1
    move v1, p1

    .line 911
    .local v1, "mun":I
    const/16 v0, 0x9

    .line 912
    :goto_2
    div-int/lit8 v1, v1, 0xa

    .line 913
    if-nez v1, :cond_7

    .line 918
    rsub-int/lit8 v2, v0, 0xa

    new-array v2, v2, [B

    iput-object v2, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 919
    rsub-int/lit8 v2, v0, 0xa

    add-int/lit8 v0, v2, -0x1

    .line 920
    :goto_3
    iget-object v2, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    rem-int/lit8 v3, p1, 0xa

    int-to-byte v3, v3

    neg-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v0

    .line 921
    div-int/lit8 p1, p1, 0xa

    .line 922
    if-eqz p1, :cond_0

    .line 919
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 906
    .end local v1    # "mun":I
    :cond_6
    iput-byte v5, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    goto :goto_1

    .line 911
    .restart local v1    # "mun":I
    :cond_7
    add-int/lit8 v0, v0, -0x1

    goto :goto_2
.end method

.method public constructor <init>(J)V
    .locals 11
    .param p1, "num"    # J

    .prologue
    const/4 v4, 0x0

    const-wide/16 v8, 0xa

    const-wide/16 v6, 0x0

    .line 944
    invoke-direct {p0}, Ljava/lang/Number;-><init>()V

    .line 414
    iput-byte v4, p0, Lcom/ibm/icu/math/BigDecimal;->form:B

    .line 946
    const/4 v0, 0x0

    .line 951
    .local v0, "i":I
    cmp-long v1, p1, v6

    if-lez v1, :cond_0

    .line 953
    const/4 v1, 0x1

    iput-byte v1, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 954
    neg-long p1, p1

    .line 961
    :goto_0
    move-wide v2, p1

    .line 962
    .local v2, "mun":J
    const/16 v0, 0x12

    .line 963
    :goto_1
    div-long/2addr v2, v8

    .line 964
    cmp-long v1, v2, v6

    if-nez v1, :cond_2

    .line 969
    rsub-int/lit8 v1, v0, 0x13

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 970
    rsub-int/lit8 v1, v0, 0x13

    add-int/lit8 v0, v1, -0x1

    .line 971
    :goto_2
    iget-object v1, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    rem-long v4, p1, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    neg-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v1, v0

    .line 972
    div-long/2addr p1, v8

    .line 973
    cmp-long v1, p1, v6

    if-nez v1, :cond_3

    .line 977
    return-void

    .line 957
    .end local v2    # "mun":J
    :cond_0
    cmp-long v1, p1, v6

    if-nez v1, :cond_1

    .line 958
    iput-byte v4, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    goto :goto_0

    .line 960
    :cond_1
    const/4 v1, -0x1

    iput-byte v1, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    goto :goto_0

    .line 962
    .restart local v2    # "mun":J
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 970
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 1039
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/ibm/icu/math/BigDecimal;-><init>([CII)V

    .line 1040
    return-void
.end method

.method public constructor <init>(Ljava/math/BigDecimal;)V
    .locals 1
    .param p1, "bd"    # Ljava/math/BigDecimal;

    .prologue
    .line 475
    invoke-virtual {p1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/ibm/icu/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 476
    return-void
.end method

.method public constructor <init>(Ljava/math/BigInteger;)V
    .locals 1
    .param p1, "bi"    # Ljava/math/BigInteger;

    .prologue
    .line 500
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/ibm/icu/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 501
    return-void
.end method

.method public constructor <init>(Ljava/math/BigInteger;I)V
    .locals 3
    .param p1, "bi"    # Ljava/math/BigInteger;
    .param p2, "scale"    # I

    .prologue
    .line 530
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/ibm/icu/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 531
    if-gez p2, :cond_0

    .line 532
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Negative scale: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 533
    :cond_0
    neg-int v0, p2

    iput v0, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 534
    return-void
.end method

.method public constructor <init>([C)V
    .locals 2
    .param p1, "inchars"    # [C

    .prologue
    .line 556
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lcom/ibm/icu/math/BigDecimal;-><init>([CII)V

    .line 557
    return-void
.end method

.method public constructor <init>([CII)V
    .locals 23
    .param p1, "inchars"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 583
    invoke-direct/range {p0 .. p0}, Ljava/lang/Number;-><init>()V

    .line 414
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-byte v0, v1, Lcom/ibm/icu/math/BigDecimal;->form:B

    .line 589
    const/4 v14, 0x0

    .line 590
    .local v14, "i":I
    const/16 v19, 0x0

    .line 591
    .local v19, "si":C
    const/4 v11, 0x0

    .line 592
    .local v11, "eneg":Z
    const/16 v16, 0x0

    .line 593
    .local v16, "k":I
    const/4 v10, 0x0

    .line 594
    .local v10, "elen":I
    const/4 v15, 0x0

    .line 595
    .local v15, "j":I
    const/16 v20, 0x0

    .line 596
    .local v20, "sj":C
    const/4 v9, 0x0

    .line 597
    .local v9, "dvalue":I
    const/16 v18, 0x0

    .line 604
    .local v18, "mag":I
    if-gtz p3, :cond_0

    .line 605
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/math/BigDecimal;->bad([C)V

    .line 609
    :cond_0
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-byte v0, v1, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 610
    aget-char v21, p1, p2

    const/16 v22, 0x2d

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_3

    .line 612
    add-int/lit8 p3, p3, -0x1

    .line 613
    if-nez p3, :cond_1

    .line 614
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/math/BigDecimal;->bad([C)V

    .line 615
    :cond_1
    const/16 v21, -0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-byte v0, v1, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 616
    add-int/lit8 p2, p2, 0x1

    .line 628
    :cond_2
    :goto_0
    const/4 v12, 0x0

    .line 629
    .local v12, "exotic":Z
    const/4 v13, 0x0

    .line 630
    .local v13, "hadexp":Z
    const/4 v7, 0x0

    .line 631
    .local v7, "d":I
    const/4 v8, -0x1

    .line 632
    .local v8, "dotoff":I
    const/16 v17, -0x1

    .line 633
    .local v17, "last":I
    move/from16 v2, p3

    .local v2, "$1":I
    move/from16 v14, p2

    :goto_1
    if-lez v2, :cond_16

    .line 634
    aget-char v19, p1, v14

    .line 635
    const/16 v21, 0x30

    move/from16 v0, v19

    move/from16 v1, v21

    if-lt v0, v1, :cond_5

    .line 636
    const/16 v21, 0x39

    move/from16 v0, v19

    move/from16 v1, v21

    if-gt v0, v1, :cond_5

    .line 638
    move/from16 v17, v14

    .line 639
    add-int/lit8 v7, v7, 0x1

    .line 633
    :goto_2
    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 619
    .end local v2    # "$1":I
    .end local v7    # "d":I
    .end local v8    # "dotoff":I
    .end local v12    # "exotic":Z
    .end local v13    # "hadexp":Z
    .end local v17    # "last":I
    :cond_3
    aget-char v21, p1, p2

    const/16 v22, 0x2b

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_2

    .line 621
    add-int/lit8 p3, p3, -0x1

    .line 622
    if-nez p3, :cond_4

    .line 623
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/math/BigDecimal;->bad([C)V

    .line 624
    :cond_4
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 642
    .restart local v2    # "$1":I
    .restart local v7    # "d":I
    .restart local v8    # "dotoff":I
    .restart local v12    # "exotic":Z
    .restart local v13    # "hadexp":Z
    .restart local v17    # "last":I
    :cond_5
    const/16 v21, 0x2e

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_7

    .line 644
    if-ltz v8, :cond_6

    .line 645
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/math/BigDecimal;->bad([C)V

    .line 646
    :cond_6
    sub-int v8, v14, p2

    .line 647
    goto :goto_2

    .line 649
    :cond_7
    const/16 v21, 0x65

    move/from16 v0, v19

    move/from16 v1, v21

    if-eq v0, v1, :cond_9

    .line 650
    const/16 v21, 0x45

    move/from16 v0, v19

    move/from16 v1, v21

    if-eq v0, v1, :cond_9

    .line 652
    invoke-static/range {v19 .. v19}, Ljava/lang/Character;->isDigit(C)Z

    move-result v21

    if-nez v21, :cond_8

    .line 653
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/math/BigDecimal;->bad([C)V

    .line 655
    :cond_8
    const/4 v12, 0x1

    .line 656
    move/from16 v17, v14

    .line 657
    add-int/lit8 v7, v7, 0x1

    .line 658
    goto :goto_2

    .line 662
    :cond_9
    sub-int v21, v14, p2

    add-int/lit8 v22, p3, -0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_a

    .line 663
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/math/BigDecimal;->bad([C)V

    .line 664
    :cond_a
    const/4 v11, 0x0

    .line 665
    add-int/lit8 v21, v14, 0x1

    aget-char v21, p1, v21

    const/16 v22, 0x2d

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_f

    .line 667
    const/4 v11, 0x1

    .line 668
    add-int/lit8 v16, v14, 0x2

    .line 676
    :goto_3
    sub-int v21, v16, p2

    sub-int v10, p3, v21

    .line 677
    if-nez v10, :cond_11

    const/16 v21, 0x1

    move/from16 v22, v21

    :goto_4
    const/16 v21, 0x9

    move/from16 v0, v21

    if-le v10, v0, :cond_12

    const/16 v21, 0x1

    :goto_5
    or-int v21, v21, v22

    if-eqz v21, :cond_b

    .line 678
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/math/BigDecimal;->bad([C)V

    .line 679
    :cond_b
    move v3, v10

    .local v3, "$2":I
    move/from16 v15, v16

    :goto_6
    if-lez v3, :cond_14

    .line 680
    aget-char v20, p1, v15

    .line 681
    const/16 v21, 0x30

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_c

    .line 682
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/math/BigDecimal;->bad([C)V

    .line 683
    :cond_c
    const/16 v21, 0x39

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_13

    .line 685
    invoke-static/range {v20 .. v20}, Ljava/lang/Character;->isDigit(C)Z

    move-result v21

    if-nez v21, :cond_d

    .line 686
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/math/BigDecimal;->bad([C)V

    .line 687
    :cond_d
    const/16 v21, 0xa

    invoke-static/range {v20 .. v21}, Ljava/lang/Character;->digit(CI)I

    move-result v9

    .line 688
    if-gez v9, :cond_e

    .line 689
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/math/BigDecimal;->bad([C)V

    .line 693
    :cond_e
    :goto_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v21, v0

    mul-int/lit8 v21, v21, 0xa

    add-int v21, v21, v9

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 679
    add-int/lit8 v3, v3, -0x1

    add-int/lit8 v15, v15, 0x1

    goto :goto_6

    .line 671
    .end local v3    # "$2":I
    :cond_f
    add-int/lit8 v21, v14, 0x1

    aget-char v21, p1, v21

    const/16 v22, 0x2b

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_10

    .line 672
    add-int/lit8 v16, v14, 0x2

    goto :goto_3

    .line 674
    :cond_10
    add-int/lit8 v16, v14, 0x1

    goto :goto_3

    .line 677
    :cond_11
    const/16 v21, 0x0

    move/from16 v22, v21

    goto :goto_4

    :cond_12
    const/16 v21, 0x0

    goto :goto_5

    .line 692
    .restart local v3    # "$2":I
    :cond_13
    add-int/lit8 v9, v20, -0x30

    goto :goto_7

    .line 696
    :cond_14
    if-eqz v11, :cond_15

    .line 697
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v21, v0

    move/from16 v0, v21

    neg-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 698
    :cond_15
    const/4 v13, 0x1

    .line 704
    .end local v3    # "$2":I
    :cond_16
    if-nez v7, :cond_17

    .line 705
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/math/BigDecimal;->bad([C)V

    .line 706
    :cond_17
    if-ltz v8, :cond_18

    .line 707
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v21, v0

    add-int v21, v21, v8

    sub-int v21, v21, v7

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 710
    :cond_18
    add-int/lit8 v4, v17, -0x1

    .local v4, "$3":I
    move/from16 v14, p2

    :goto_8
    if-gt v14, v4, :cond_1b

    .line 711
    aget-char v19, p1, v14

    .line 712
    const/16 v21, 0x30

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_19

    .line 714
    add-int/lit8 p2, p2, 0x1

    .line 715
    add-int/lit8 v8, v8, -0x1

    .line 716
    add-int/lit8 v7, v7, -0x1

    .line 710
    :goto_9
    add-int/lit8 v14, v14, 0x1

    goto :goto_8

    .line 719
    :cond_19
    const/16 v21, 0x2e

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_1a

    .line 721
    add-int/lit8 p2, p2, 0x1

    .line 722
    add-int/lit8 v8, v8, -0x1

    .line 723
    goto :goto_9

    .line 725
    :cond_1a
    const/16 v21, 0x39

    move/from16 v0, v19

    move/from16 v1, v21

    if-gt v0, v1, :cond_1d

    .line 740
    :cond_1b
    new-array v0, v7, [B

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 741
    move/from16 v15, p2

    .line 742
    if-eqz v12, :cond_20

    .line 744
    move v5, v7

    .local v5, "$4":I
    const/4 v14, 0x0

    :goto_a
    if-lez v5, :cond_22

    .line 745
    if-ne v14, v8, :cond_1c

    .line 746
    add-int/lit8 v15, v15, 0x1

    .line 747
    :cond_1c
    aget-char v20, p1, v15

    .line 748
    const/16 v21, 0x39

    move/from16 v0, v20

    move/from16 v1, v21

    if-gt v0, v1, :cond_1e

    .line 749
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v21, v0

    add-int/lit8 v22, v20, -0x30

    move/from16 v0, v22

    int-to-byte v0, v0

    move/from16 v22, v0

    aput-byte v22, v21, v14

    .line 757
    :goto_b
    add-int/lit8 v15, v15, 0x1

    .line 744
    add-int/lit8 v5, v5, -0x1

    add-int/lit8 v14, v14, 0x1

    goto :goto_a

    .line 729
    .end local v5    # "$4":I
    :cond_1d
    const/16 v21, 0xa

    move/from16 v0, v19

    move/from16 v1, v21

    invoke-static {v0, v1}, Ljava/lang/Character;->digit(CI)I

    move-result v21

    if-nez v21, :cond_1b

    .line 732
    add-int/lit8 p2, p2, 0x1

    .line 733
    add-int/lit8 v8, v8, -0x1

    .line 734
    add-int/lit8 v7, v7, -0x1

    goto :goto_9

    .line 752
    .restart local v5    # "$4":I
    :cond_1e
    const/16 v21, 0xa

    invoke-static/range {v20 .. v21}, Ljava/lang/Character;->digit(CI)I

    move-result v9

    .line 753
    if-gez v9, :cond_1f

    .line 754
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/math/BigDecimal;->bad([C)V

    .line 755
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v21, v0

    int-to-byte v0, v9

    move/from16 v22, v0

    aput-byte v22, v21, v14

    goto :goto_b

    .line 763
    .end local v5    # "$4":I
    :cond_20
    move v6, v7

    .local v6, "$5":I
    const/4 v14, 0x0

    :goto_c
    if-lez v6, :cond_22

    .line 764
    if-ne v14, v8, :cond_21

    .line 765
    add-int/lit8 v15, v15, 0x1

    .line 766
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v21, v0

    aget-char v22, p1, v15

    add-int/lit8 v22, v22, -0x30

    move/from16 v0, v22

    int-to-byte v0, v0

    move/from16 v22, v0

    aput-byte v22, v21, v14

    .line 767
    add-int/lit8 v15, v15, 0x1

    .line 763
    add-int/lit8 v6, v6, -0x1

    add-int/lit8 v14, v14, 0x1

    goto :goto_c

    .line 778
    .end local v6    # "$5":I
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aget-byte v21, v21, v22

    if-nez v21, :cond_25

    .line 780
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-byte v0, v1, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 782
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v21, v0

    if-lez v21, :cond_23

    .line 783
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 784
    :cond_23
    if-eqz v13, :cond_24

    .line 786
    sget-object v21, Lcom/ibm/icu/math/BigDecimal;->ZERO:Lcom/ibm/icu/math/BigDecimal;

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 787
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 804
    :cond_24
    :goto_d
    return-void

    .line 794
    :cond_25
    if-eqz v13, :cond_24

    .line 796
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-byte v0, v1, Lcom/ibm/icu/math/BigDecimal;->form:B

    .line 798
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v22, v0

    add-int v21, v21, v22

    add-int/lit8 v18, v21, -0x1

    .line 799
    const v21, -0x3b9ac9ff

    move/from16 v0, v18

    move/from16 v1, v21

    if-ge v0, v1, :cond_26

    const/16 v21, 0x1

    move/from16 v22, v21

    :goto_e
    const v21, 0x3b9ac9ff

    move/from16 v0, v18

    move/from16 v1, v21

    if-le v0, v1, :cond_27

    const/16 v21, 0x1

    :goto_f
    or-int v21, v21, v22

    if-eqz v21, :cond_24

    .line 800
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/math/BigDecimal;->bad([C)V

    goto :goto_d

    .line 799
    :cond_26
    const/16 v21, 0x0

    move/from16 v22, v21

    goto :goto_e

    :cond_27
    const/16 v21, 0x0

    goto :goto_f
.end method

.method private static final allzero([BI)Z
    .locals 3
    .param p0, "array"    # [B
    .param p1, "start"    # I

    .prologue
    .line 4278
    const/4 v1, 0x0

    .line 4279
    .local v1, "i":I
    if-gez p1, :cond_0

    .line 4280
    const/4 p1, 0x0

    .line 4281
    :cond_0
    array-length v2, p0

    add-int/lit8 v0, v2, -0x1

    .local v0, "$25":I
    move v1, p1

    :goto_0
    if-gt v1, v0, :cond_2

    .line 4282
    aget-byte v2, p0, v1

    if-eqz v2, :cond_1

    .line 4283
    const/4 v2, 0x0

    .line 4286
    :goto_1
    return v2

    .line 4281
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4286
    :cond_2
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private bad([C)V
    .locals 3
    .param p1, "s"    # [C

    .prologue
    .line 3896
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Not a number: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private badarg(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "pos"    # I
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 3905
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Bad argument "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "to"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static final byteaddsub([BI[BIIZ)[B
    .locals 17
    .param p0, "a"    # [B
    .param p1, "avlen"    # I
    .param p2, "b"    # [B
    .param p3, "bvlen"    # I
    .param p4, "m"    # I
    .param p5, "reuse"    # Z

    .prologue
    .line 3969
    const/4 v11, 0x0

    .line 3970
    .local v11, "op":I
    const/4 v7, 0x0

    .line 3972
    .local v7, "dp90":I
    const/4 v8, 0x0

    .line 3978
    .local v8, "i":I
    move-object/from16 v0, p0

    array-length v2, v0

    .line 3979
    .local v2, "alength":I
    move-object/from16 v0, p2

    array-length v4, v0

    .line 3980
    .local v4, "blength":I
    add-int/lit8 v3, p1, -0x1

    .line 3981
    .local v3, "ap":I
    add-int/lit8 v5, p3, -0x1

    .line 3982
    .local v5, "bp":I
    move v9, v5

    .line 3983
    .local v9, "maxarr":I
    if-ge v9, v3, :cond_0

    .line 3984
    move v9, v3

    .line 3985
    :cond_0
    const/4 v13, 0x0

    check-cast v13, [B

    .line 3986
    .local v13, "reb":[B
    if-eqz p5, :cond_1

    .line 3987
    add-int/lit8 v14, v9, 0x1

    if-ne v14, v2, :cond_1

    .line 3988
    move-object/from16 v13, p0

    .line 3989
    :cond_1
    if-nez v13, :cond_2

    .line 3990
    add-int/lit8 v14, v9, 0x1

    new-array v13, v14, [B

    .line 3992
    :cond_2
    const/4 v12, 0x0

    .line 3993
    .local v12, "quickm":Z
    const/4 v14, 0x1

    move/from16 v0, p4

    if-ne v0, v14, :cond_8

    .line 3994
    const/4 v12, 0x1

    .line 3999
    :cond_3
    :goto_0
    const/4 v6, 0x0

    .line 4000
    .local v6, "digit":I
    move v11, v9

    :goto_1
    if-ltz v11, :cond_c

    .line 4001
    if-ltz v3, :cond_5

    .line 4003
    if-ge v3, v2, :cond_4

    .line 4004
    aget-byte v14, p0, v3

    add-int/2addr v6, v14

    .line 4005
    :cond_4
    add-int/lit8 v3, v3, -0x1

    .line 4007
    :cond_5
    if-ltz v5, :cond_7

    .line 4009
    if-ge v5, v4, :cond_6

    .line 4011
    if-eqz v12, :cond_a

    .line 4013
    if-lez p4, :cond_9

    .line 4014
    aget-byte v14, p2, v5

    add-int/2addr v6, v14

    .line 4021
    :cond_6
    :goto_2
    add-int/lit8 v5, v5, -0x1

    .line 4024
    :cond_7
    const/16 v14, 0xa

    if-ge v6, v14, :cond_b

    .line 4025
    if-ltz v6, :cond_b

    .line 4027
    int-to-byte v14, v6

    aput-byte v14, v13, v11

    .line 4028
    const/4 v6, 0x0

    .line 4000
    :goto_3
    add-int/lit8 v11, v11, -0x1

    goto :goto_1

    .line 3996
    .end local v6    # "digit":I
    :cond_8
    const/4 v14, -0x1

    move/from16 v0, p4

    if-ne v0, v14, :cond_3

    .line 3997
    const/4 v12, 0x1

    goto :goto_0

    .line 4016
    .restart local v6    # "digit":I
    :cond_9
    aget-byte v14, p2, v5

    sub-int/2addr v6, v14

    .line 4017
    goto :goto_2

    .line 4019
    :cond_a
    aget-byte v14, p2, v5

    mul-int v14, v14, p4

    add-int/2addr v6, v14

    goto :goto_2

    .line 4031
    :cond_b
    add-int/lit8 v7, v6, 0x5a

    .line 4032
    sget-object v14, Lcom/ibm/icu/math/BigDecimal;->bytedig:[B

    aget-byte v14, v14, v7

    aput-byte v14, v13, v11

    .line 4033
    sget-object v14, Lcom/ibm/icu/math/BigDecimal;->bytecar:[B

    aget-byte v6, v14, v7

    goto :goto_3

    .line 4037
    :cond_c
    if-nez v6, :cond_d

    .line 4058
    .end local v13    # "reb":[B
    :goto_4
    return-object v13

    .line 4043
    .restart local v13    # "reb":[B
    :cond_d
    const/4 v10, 0x0

    check-cast v10, [B

    .line 4044
    .local v10, "newarr":[B
    if-eqz p5, :cond_e

    .line 4045
    add-int/lit8 v14, v9, 0x2

    move-object/from16 v0, p0

    array-length v15, v0

    if-ne v14, v15, :cond_e

    .line 4046
    move-object/from16 v10, p0

    .line 4047
    :cond_e
    if-nez v10, :cond_f

    .line 4048
    add-int/lit8 v14, v9, 0x2

    new-array v10, v14, [B

    .line 4049
    :cond_f
    const/4 v14, 0x0

    int-to-byte v15, v6

    aput-byte v15, v10, v14

    .line 4051
    const/16 v14, 0xa

    if-ge v9, v14, :cond_10

    .line 4052
    add-int/lit8 v1, v9, 0x1

    .local v1, "$24":I
    const/4 v8, 0x0

    :goto_5
    if-lez v1, :cond_11

    .line 4053
    add-int/lit8 v14, v8, 0x1

    aget-byte v15, v13, v8

    aput-byte v15, v10, v14

    .line 4052
    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 4057
    .end local v1    # "$24":I
    :cond_10
    const/4 v14, 0x0

    const/4 v15, 0x1

    add-int/lit8 v16, v9, 0x1

    move/from16 v0, v16

    invoke-static {v13, v14, v10, v15, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_11
    move-object v13, v10

    .line 4058
    goto :goto_4
.end method

.method private checkdigits(Lcom/ibm/icu/math/BigDecimal;I)V
    .locals 3
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "dig"    # I

    .prologue
    .line 4108
    if-nez p2, :cond_1

    .line 4119
    :cond_0
    return-void

    .line 4111
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v0, v0

    if-le v0, p2, :cond_2

    .line 4112
    iget-object v0, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    invoke-static {v0, p2}, Lcom/ibm/icu/math/BigDecimal;->allzero([BI)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4113
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Too many digits: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4114
    :cond_2
    if-eqz p1, :cond_0

    .line 4116
    iget-object v0, p1, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v0, v0

    if-le v0, p2, :cond_0

    .line 4117
    iget-object v0, p1, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    invoke-static {v0, p2}, Lcom/ibm/icu/math/BigDecimal;->allzero([BI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4118
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Too many digits: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static final clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;
    .locals 2
    .param p0, "dec"    # Lcom/ibm/icu/math/BigDecimal;

    .prologue
    .line 4094
    new-instance v0, Lcom/ibm/icu/math/BigDecimal;

    invoke-direct {v0}, Lcom/ibm/icu/math/BigDecimal;-><init>()V

    .line 4095
    .local v0, "copy":Lcom/ibm/icu/math/BigDecimal;
    iget-byte v1, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    iput-byte v1, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 4096
    iget v1, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    iput v1, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 4097
    iget-byte v1, p0, Lcom/ibm/icu/math/BigDecimal;->form:B

    iput-byte v1, v0, Lcom/ibm/icu/math/BigDecimal;->form:B

    .line 4098
    iget-object v1, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    iput-object v1, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 4099
    return-object v0
.end method

.method private static final diginit()[B
    .locals 5

    .prologue
    .line 4066
    const/4 v1, 0x0

    .line 4067
    .local v1, "op":I
    const/4 v0, 0x0

    .line 4068
    .local v0, "digit":I
    const/16 v3, 0xbe

    new-array v2, v3, [B

    .line 4069
    .local v2, "work":[B
    const/4 v1, 0x0

    :goto_0
    const/16 v3, 0xbd

    if-gt v1, v3, :cond_1

    .line 4070
    add-int/lit8 v0, v1, -0x5a

    .line 4071
    if-ltz v0, :cond_0

    .line 4073
    rem-int/lit8 v3, v0, 0xa

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    .line 4074
    sget-object v3, Lcom/ibm/icu/math/BigDecimal;->bytecar:[B

    div-int/lit8 v4, v0, 0xa

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    .line 4069
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4078
    :cond_0
    add-int/lit8 v0, v0, 0x64

    .line 4079
    rem-int/lit8 v3, v0, 0xa

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    .line 4080
    sget-object v3, Lcom/ibm/icu/math/BigDecimal;->bytecar:[B

    div-int/lit8 v4, v0, 0xa

    add-int/lit8 v4, v4, -0xa

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    goto :goto_1

    .line 4083
    :cond_1
    return-object v2
.end method

.method private dodivide(CLcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;I)Lcom/ibm/icu/math/BigDecimal;
    .locals 30
    .param p1, "code"    # C
    .param p2, "rhs"    # Lcom/ibm/icu/math/BigDecimal;
    .param p3, "set"    # Lcom/ibm/icu/math/MathContext;
    .param p4, "scale"    # I

    .prologue
    .line 3613
    const/16 v27, 0x0

    .line 3614
    .local v27, "thisdigit":I
    const/4 v15, 0x0

    .line 3615
    .local v15, "i":I
    const/16 v28, 0x0

    .line 3616
    .local v28, "v2":B
    const/4 v12, 0x0

    .line 3617
    .local v12, "ba":I
    const/16 v18, 0x0

    .line 3618
    .local v18, "mult":I
    const/16 v26, 0x0

    .line 3619
    .local v26, "start":I
    const/16 v23, 0x0

    .line 3620
    .local v23, "padding":I
    const/4 v13, 0x0

    .line 3621
    .local v13, "d":I
    const/16 v22, 0x0

    .line 3622
    .local v22, "newvar1":[B
    const/16 v16, 0x0

    .line 3623
    .local v16, "lasthave":B
    const/4 v10, 0x0

    .line 3624
    .local v10, "actdig":I
    const/16 v21, 0x0

    .line 3626
    .local v21, "newmant":[B
    move-object/from16 v0, p3

    iget-boolean v6, v0, Lcom/ibm/icu/math/MathContext;->lostDigits:Z

    if-eqz v6, :cond_0

    .line 3627
    move-object/from16 v0, p3

    iget v6, v0, Lcom/ibm/icu/math/MathContext;->digits:I

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v6}, Lcom/ibm/icu/math/BigDecimal;->checkdigits(Lcom/ibm/icu/math/BigDecimal;I)V

    .line 3628
    :cond_0
    move-object/from16 v17, p0

    .line 3631
    .local v17, "lhs":Lcom/ibm/icu/math/BigDecimal;
    move-object/from16 v0, p2

    iget-byte v6, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    if-nez v6, :cond_1

    .line 3632
    new-instance v6, Ljava/lang/ArithmeticException;

    const-string/jumbo v7, "Divide by 0"

    invoke-direct {v6, v7}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 3633
    :cond_1
    move-object/from16 v0, v17

    iget-byte v6, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    if-nez v6, :cond_4

    .line 3635
    move-object/from16 v0, p3

    iget v6, v0, Lcom/ibm/icu/math/MathContext;->form:I

    if-eqz v6, :cond_2

    .line 3636
    sget-object v6, Lcom/ibm/icu/math/BigDecimal;->ZERO:Lcom/ibm/icu/math/BigDecimal;

    .line 3889
    :goto_0
    return-object v6

    .line 3637
    :cond_2
    const/4 v6, -0x1

    move/from16 v0, p4

    if-ne v0, v6, :cond_3

    move-object/from16 v6, v17

    .line 3638
    goto :goto_0

    .line 3639
    :cond_3
    move-object/from16 v0, v17

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/ibm/icu/math/BigDecimal;->setScale(I)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v6

    goto :goto_0

    .line 3643
    :cond_4
    move-object/from16 v0, p3

    iget v0, v0, Lcom/ibm/icu/math/MathContext;->digits:I

    move/from16 v24, v0

    .line 3644
    .local v24, "reqdig":I
    if-lez v24, :cond_7

    .line 3646
    move-object/from16 v0, v17

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v6, v6

    move/from16 v0, v24

    if-le v6, v0, :cond_5

    .line 3647
    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-direct {v6, v0}, Lcom/ibm/icu/math/BigDecimal;->round(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v17

    .line 3648
    :cond_5
    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v6, v6

    move/from16 v0, v24

    if-le v6, v0, :cond_6

    .line 3649
    invoke-static/range {p2 .. p2}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-direct {v6, v0}, Lcom/ibm/icu/math/BigDecimal;->round(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object p2

    .line 3668
    :cond_6
    :goto_1
    move-object/from16 v0, v17

    iget v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move-object/from16 v0, p2

    iget v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    sub-int/2addr v6, v7

    move-object/from16 v0, v17

    iget-object v7, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v7, v7

    add-int/2addr v6, v7

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v7, v7

    sub-int v19, v6, v7

    .line 3670
    .local v19, "newexp":I
    if-gez v19, :cond_c

    .line 3671
    const/16 v6, 0x44

    move/from16 v0, p1

    if-eq v0, v6, :cond_c

    .line 3673
    const/16 v6, 0x49

    move/from16 v0, p1

    if-ne v0, v6, :cond_b

    .line 3674
    sget-object v6, Lcom/ibm/icu/math/BigDecimal;->ZERO:Lcom/ibm/icu/math/BigDecimal;

    goto :goto_0

    .line 3653
    .end local v19    # "newexp":I
    :cond_7
    const/4 v6, -0x1

    move/from16 v0, p4

    if-ne v0, v6, :cond_8

    .line 3654
    invoke-virtual/range {v17 .. v17}, Lcom/ibm/icu/math/BigDecimal;->scale()I

    move-result p4

    .line 3656
    :cond_8
    move-object/from16 v0, v17

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v0, v6

    move/from16 v24, v0

    .line 3658
    move-object/from16 v0, v17

    iget v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    neg-int v6, v6

    move/from16 v0, p4

    if-eq v0, v6, :cond_9

    .line 3659
    add-int v6, v24, p4

    move-object/from16 v0, v17

    iget v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int v24, v6, v7

    .line 3660
    :cond_9
    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    sub-int v6, v24, v6

    move-object/from16 v0, p2

    iget v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    sub-int v24, v6, v7

    .line 3661
    move-object/from16 v0, v17

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v6, v6

    move/from16 v0, v24

    if-ge v0, v6, :cond_a

    .line 3662
    move-object/from16 v0, v17

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v0, v6

    move/from16 v24, v0

    .line 3663
    :cond_a
    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v6, v6

    move/from16 v0, v24

    if-ge v0, v6, :cond_6

    .line 3664
    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v0, v6

    move/from16 v24, v0

    goto :goto_1

    .line 3676
    .restart local v19    # "newexp":I
    :cond_b
    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p3

    invoke-direct {v6, v0, v7}, Lcom/ibm/icu/math/BigDecimal;->finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v6

    goto/16 :goto_0

    .line 3680
    :cond_c
    new-instance v25, Lcom/ibm/icu/math/BigDecimal;

    invoke-direct/range {v25 .. v25}, Lcom/ibm/icu/math/BigDecimal;-><init>()V

    .line 3681
    .local v25, "res":Lcom/ibm/icu/math/BigDecimal;
    move-object/from16 v0, v17

    iget-byte v6, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    move-object/from16 v0, p2

    iget-byte v7, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    mul-int/2addr v6, v7

    int-to-byte v6, v6

    move-object/from16 v0, v25

    iput-byte v6, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 3682
    move/from16 v0, v19

    move-object/from16 v1, v25

    iput v0, v1, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 3683
    add-int/lit8 v6, v24, 0x1

    new-array v6, v6, [B

    move-object/from16 v0, v25

    iput-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 3687
    add-int v6, v24, v24

    add-int/lit8 v20, v6, 0x1

    .line 3688
    .local v20, "newlen":I
    move-object/from16 v0, v17

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move/from16 v0, v20

    invoke-static {v6, v0}, Lcom/ibm/icu/math/BigDecimal;->extend([BI)[B

    move-result-object v2

    .line 3689
    .local v2, "var1":[B
    move/from16 v3, v20

    .line 3691
    .local v3, "var1len":I
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 3692
    .local v4, "var2":[B
    move/from16 v5, v20

    .line 3695
    .local v5, "var2len":I
    const/4 v6, 0x0

    aget-byte v6, v4, v6

    mul-int/lit8 v6, v6, 0xa

    add-int/lit8 v11, v6, 0x1

    .line 3696
    .local v11, "b2b":I
    array-length v6, v4

    const/4 v7, 0x1

    if-le v6, v7, :cond_d

    .line 3697
    const/4 v6, 0x1

    aget-byte v6, v4, v6

    add-int/2addr v11, v6

    .line 3700
    :cond_d
    const/4 v14, 0x0

    .line 3702
    .local v14, "have":I
    :goto_2
    const/16 v27, 0x0

    .line 3705
    :cond_e
    :goto_3
    if-ge v3, v5, :cond_12

    .line 3769
    :cond_f
    if-eqz v14, :cond_1b

    const/4 v6, 0x1

    move v7, v6

    :goto_4
    if-eqz v27, :cond_1c

    const/4 v6, 0x1

    :goto_5
    or-int/2addr v6, v7

    if-eqz v6, :cond_1e

    .line 3771
    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move/from16 v0, v27

    int-to-byte v7, v0

    aput-byte v7, v6, v14

    .line 3772
    add-int/lit8 v14, v14, 0x1

    .line 3773
    add-int/lit8 v6, v24, 0x1

    if-ne v14, v6, :cond_1d

    .line 3795
    :cond_10
    :goto_6
    if-nez v14, :cond_11

    .line 3796
    const/4 v14, 0x1

    .line 3798
    :cond_11
    const/16 v6, 0x49

    move/from16 v0, p1

    if-ne v0, v6, :cond_21

    const/4 v6, 0x1

    move v7, v6

    :goto_7
    const/16 v6, 0x52

    move/from16 v0, p1

    if-ne v0, v6, :cond_22

    const/4 v6, 0x1

    :goto_8
    or-int/2addr v6, v7

    if-eqz v6, :cond_2b

    .line 3800
    move-object/from16 v0, v25

    iget v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/2addr v6, v14

    move/from16 v0, v24

    if-le v6, v0, :cond_23

    .line 3801
    new-instance v6, Ljava/lang/ArithmeticException;

    const-string/jumbo v7, "Integer overflow"

    invoke-direct {v6, v7}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 3707
    :cond_12
    if-ne v3, v5, :cond_19

    .line 3710
    move v8, v3

    .local v8, "$22":I
    const/4 v15, 0x0

    :goto_9
    if-lez v8, :cond_18

    .line 3712
    array-length v6, v4

    if-ge v15, v6, :cond_16

    .line 3713
    aget-byte v28, v4, v15

    .line 3716
    :goto_a
    aget-byte v6, v2, v15

    move/from16 v0, v28

    if-lt v6, v0, :cond_f

    .line 3718
    aget-byte v6, v2, v15

    move/from16 v0, v28

    if-le v6, v0, :cond_17

    .line 3735
    const/4 v6, 0x0

    aget-byte v12, v2, v6

    .line 3745
    .end local v8    # "$22":I
    :cond_13
    :goto_b
    mul-int/lit8 v6, v12, 0xa

    div-int v18, v6, v11

    .line 3746
    if-nez v18, :cond_14

    .line 3747
    const/16 v18, 0x1

    .line 3748
    :cond_14
    add-int v27, v27, v18

    .line 3750
    move/from16 v0, v18

    neg-int v6, v0

    const/4 v7, 0x1

    invoke-static/range {v2 .. v7}, Lcom/ibm/icu/math/BigDecimal;->byteaddsub([BI[BIIZ)[B

    move-result-object v2

    .line 3751
    const/4 v6, 0x0

    aget-byte v6, v2, v6

    if-nez v6, :cond_e

    .line 3755
    add-int/lit8 v9, v3, -0x2

    .local v9, "$23":I
    const/16 v26, 0x0

    :goto_c
    move/from16 v0, v26

    if-gt v0, v9, :cond_15

    .line 3756
    aget-byte v6, v2, v26

    if-eqz v6, :cond_1a

    .line 3761
    :cond_15
    if-eqz v26, :cond_e

    .line 3764
    const/4 v6, 0x0

    move/from16 v0, v26

    invoke-static {v2, v0, v2, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/16 :goto_3

    .line 3715
    .end local v9    # "$23":I
    .restart local v8    # "$22":I
    :cond_16
    const/16 v28, 0x0

    goto :goto_a

    .line 3710
    :cond_17
    add-int/lit8 v8, v8, -0x1

    add-int/lit8 v15, v15, 0x1

    goto :goto_9

    .line 3727
    :cond_18
    add-int/lit8 v27, v27, 0x1

    .line 3728
    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move/from16 v0, v27

    int-to-byte v7, v0

    aput-byte v7, v6, v14

    .line 3729
    add-int/lit8 v14, v14, 0x1

    .line 3730
    const/4 v6, 0x0

    const/4 v7, 0x0

    aput-byte v7, v2, v6

    goto/16 :goto_6

    .line 3740
    .end local v8    # "$22":I
    :cond_19
    const/4 v6, 0x0

    aget-byte v6, v2, v6

    mul-int/lit8 v12, v6, 0xa

    .line 3741
    const/4 v6, 0x1

    if-le v3, v6, :cond_13

    .line 3742
    const/4 v6, 0x1

    aget-byte v6, v2, v6

    add-int/2addr v12, v6

    goto :goto_b

    .line 3758
    .restart local v9    # "$23":I
    :cond_1a
    add-int/lit8 v3, v3, -0x1

    .line 3755
    add-int/lit8 v26, v26, 0x1

    goto :goto_c

    .line 3769
    .end local v9    # "$23":I
    :cond_1b
    const/4 v6, 0x0

    move v7, v6

    goto/16 :goto_4

    :cond_1c
    const/4 v6, 0x0

    goto/16 :goto_5

    .line 3775
    :cond_1d
    const/4 v6, 0x0

    aget-byte v6, v2, v6

    if-eqz v6, :cond_10

    .line 3779
    :cond_1e
    if-ltz p4, :cond_1f

    .line 3780
    move-object/from16 v0, v25

    iget v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    neg-int v6, v6

    move/from16 v0, p4

    if-gt v6, v0, :cond_10

    .line 3783
    :cond_1f
    const/16 v6, 0x44

    move/from16 v0, p1

    if-eq v0, v6, :cond_20

    .line 3784
    move-object/from16 v0, v25

    iget v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-lez v6, :cond_10

    .line 3786
    :cond_20
    move-object/from16 v0, v25

    iget v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, v25

    iput v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 3789
    add-int/lit8 v5, v5, -0x1

    .line 3790
    goto/16 :goto_2

    .line 3798
    :cond_21
    const/4 v6, 0x0

    move v7, v6

    goto/16 :goto_7

    :cond_22
    const/4 v6, 0x0

    goto/16 :goto_8

    .line 3803
    :cond_23
    const/16 v6, 0x52

    move/from16 v0, p1

    if-ne v0, v6, :cond_2c

    .line 3806
    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    const/4 v7, 0x0

    aget-byte v6, v6, v7

    if-nez v6, :cond_24

    .line 3807
    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p3

    invoke-direct {v6, v0, v7}, Lcom/ibm/icu/math/BigDecimal;->finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v6

    goto/16 :goto_0

    .line 3808
    :cond_24
    const/4 v6, 0x0

    aget-byte v6, v2, v6

    if-nez v6, :cond_25

    .line 3809
    sget-object v6, Lcom/ibm/icu/math/BigDecimal;->ZERO:Lcom/ibm/icu/math/BigDecimal;

    goto/16 :goto_0

    .line 3810
    :cond_25
    move-object/from16 v0, v17

    iget-byte v6, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    move-object/from16 v0, v25

    iput-byte v6, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 3813
    add-int v6, v24, v24

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v0, v17

    iget-object v7, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v7, v7

    sub-int v23, v6, v7

    .line 3814
    move-object/from16 v0, v25

    iget v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    sub-int v6, v6, v23

    move-object/from16 v0, v17

    iget v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/2addr v6, v7

    move-object/from16 v0, v25

    iput v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 3818
    move v13, v3

    .line 3819
    add-int/lit8 v15, v13, -0x1

    :goto_d
    const/4 v6, 0x1

    if-lt v15, v6, :cond_26

    move-object/from16 v0, v25

    iget v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move-object/from16 v0, v17

    iget v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-ge v6, v7, :cond_28

    const/4 v6, 0x1

    :goto_e
    move-object/from16 v0, v25

    iget v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v29, v0

    move/from16 v0, v29

    if-ge v7, v0, :cond_29

    const/4 v7, 0x1

    :goto_f
    and-int/2addr v6, v7

    if-nez v6, :cond_2a

    .line 3826
    :cond_26
    array-length v6, v2

    if-ge v13, v6, :cond_27

    .line 3828
    new-array v0, v13, [B

    move-object/from16 v22, v0

    .line 3829
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v22

    invoke-static {v2, v6, v0, v7, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3830
    move-object/from16 v2, v22

    .line 3832
    :cond_27
    move-object/from16 v0, v25

    iput-object v2, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 3833
    const/4 v6, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6}, Lcom/ibm/icu/math/BigDecimal;->finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v6

    goto/16 :goto_0

    .line 3819
    :cond_28
    const/4 v6, 0x0

    goto :goto_e

    :cond_29
    const/4 v7, 0x0

    goto :goto_f

    .line 3820
    :cond_2a
    aget-byte v6, v2, v15

    if-nez v6, :cond_26

    .line 3822
    add-int/lit8 v13, v13, -0x1

    .line 3823
    move-object/from16 v0, v25

    iget v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v0, v25

    iput v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 3819
    add-int/lit8 v15, v15, -0x1

    goto :goto_d

    .line 3843
    :cond_2b
    const/4 v6, 0x0

    aget-byte v6, v2, v6

    if-eqz v6, :cond_2c

    .line 3845
    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    add-int/lit8 v7, v14, -0x1

    aget-byte v16, v6, v7

    .line 3846
    rem-int/lit8 v6, v16, 0x5

    if-nez v6, :cond_2c

    .line 3847
    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    add-int/lit8 v7, v14, -0x1

    add-int/lit8 v29, v16, 0x1

    move/from16 v0, v29

    int-to-byte v0, v0

    move/from16 v29, v0

    aput-byte v29, v6, v7

    .line 3853
    :cond_2c
    if-ltz p4, :cond_2f

    .line 3856
    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v6, v6

    if-eq v14, v6, :cond_2d

    .line 3858
    move-object/from16 v0, v25

    iget v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move-object/from16 v0, v25

    iget-object v7, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v7, v7

    sub-int/2addr v7, v14

    sub-int/2addr v6, v7

    move-object/from16 v0, v25

    iput v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 3860
    :cond_2d
    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v6, v6

    move-object/from16 v0, v25

    iget v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    neg-int v7, v7

    sub-int v7, v7, p4

    sub-int v10, v6, v7

    .line 3861
    move-object/from16 v0, p3

    iget v6, v0, Lcom/ibm/icu/math/MathContext;->roundingMode:I

    move-object/from16 v0, v25

    invoke-direct {v0, v10, v6}, Lcom/ibm/icu/math/BigDecimal;->round(II)Lcom/ibm/icu/math/BigDecimal;

    .line 3864
    move-object/from16 v0, v25

    iget v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v0, p4

    neg-int v7, v0

    if-eq v6, v7, :cond_2e

    .line 3866
    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v0, v25

    iget-object v7, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v7, v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v6, v7}, Lcom/ibm/icu/math/BigDecimal;->extend([BI)[B

    move-result-object v6

    move-object/from16 v0, v25

    iput-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 3867
    move-object/from16 v0, v25

    iget v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, v25

    iput v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 3869
    :cond_2e
    const/4 v6, 0x1

    move-object/from16 v0, v25

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6}, Lcom/ibm/icu/math/BigDecimal;->finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v6

    goto/16 :goto_0

    .line 3873
    :cond_2f
    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v6, v6

    if-ne v14, v6, :cond_30

    .line 3875
    move-object/from16 v0, v25

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/ibm/icu/math/BigDecimal;->round(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    .line 3876
    move/from16 v14, v24

    .line 3889
    :goto_10
    const/4 v6, 0x1

    move-object/from16 v0, v25

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6}, Lcom/ibm/icu/math/BigDecimal;->finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v6

    goto/16 :goto_0

    .line 3880
    :cond_30
    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    const/4 v7, 0x0

    aget-byte v6, v6, v7

    if-nez v6, :cond_31

    .line 3881
    sget-object v6, Lcom/ibm/icu/math/BigDecimal;->ZERO:Lcom/ibm/icu/math/BigDecimal;

    goto/16 :goto_0

    .line 3885
    :cond_31
    new-array v0, v14, [B

    move-object/from16 v21, v0

    .line 3886
    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    const/4 v7, 0x0

    const/16 v29, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v29

    invoke-static {v6, v7, v0, v1, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3887
    move-object/from16 v0, v21

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    goto :goto_10
.end method

.method private static final extend([BI)[B
    .locals 3
    .param p0, "inarr"    # [B
    .param p1, "newlen"    # I

    .prologue
    const/4 v2, 0x0

    .line 3917
    array-length v1, p0

    if-ne v1, p1, :cond_0

    .line 3922
    .end local p0    # "inarr":[B
    :goto_0
    return-object p0

    .line 3919
    .restart local p0    # "inarr":[B
    :cond_0
    new-array v0, p1, [B

    .line 3920
    .local v0, "newarr":[B
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 3922
    goto :goto_0
.end method

.method private finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;
    .locals 12
    .param p1, "set"    # Lcom/ibm/icu/math/MathContext;
    .param p2, "strip"    # Z

    .prologue
    const v11, 0x3b9ac9ff

    const/4 v6, 0x1

    const v10, -0x3b9ac9ff

    const/4 v7, 0x0

    .line 4307
    const/4 v1, 0x0

    .line 4308
    .local v1, "d":I
    const/4 v2, 0x0

    .line 4309
    .local v2, "i":I
    const/4 v4, 0x0

    .line 4310
    .local v4, "newmant":[B
    const/4 v3, 0x0

    .line 4311
    .local v3, "mag":I
    const/4 v5, 0x0

    .line 4313
    .local v5, "sig":I
    iget v8, p1, Lcom/ibm/icu/math/MathContext;->digits:I

    if-eqz v8, :cond_0

    .line 4314
    iget-object v8, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v8, v8

    iget v9, p1, Lcom/ibm/icu/math/MathContext;->digits:I

    if-le v8, v9, :cond_0

    .line 4315
    invoke-direct {p0, p1}, Lcom/ibm/icu/math/BigDecimal;->round(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    .line 4319
    :cond_0
    if-eqz p2, :cond_2

    .line 4320
    iget v8, p1, Lcom/ibm/icu/math/MathContext;->form:I

    if-eqz v8, :cond_2

    .line 4322
    iget-object v8, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v1, v8

    .line 4324
    add-int/lit8 v2, v1, -0x1

    :goto_0
    if-lt v2, v6, :cond_1

    .line 4325
    iget-object v8, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    aget-byte v8, v8, v2

    if-eqz v8, :cond_6

    .line 4331
    :cond_1
    iget-object v8, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v8, v8

    if-ge v1, v8, :cond_2

    .line 4333
    new-array v4, v1, [B

    .line 4334
    iget-object v8, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4335
    iput-object v4, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 4339
    :cond_2
    iput-byte v7, p0, Lcom/ibm/icu/math/BigDecimal;->form:B

    .line 4342
    iget-object v8, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v0, v8

    .local v0, "$26":I
    const/4 v2, 0x0

    :goto_1
    if-lez v0, :cond_e

    .line 4343
    iget-object v8, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    aget-byte v8, v8, v2

    if-eqz v8, :cond_d

    .line 4347
    if-lez v2, :cond_3

    .line 4349
    iget-object v8, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v8, v8

    sub-int/2addr v8, v2

    new-array v4, v8, [B

    .line 4350
    iget-object v8, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    iget-object v9, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v9, v9

    sub-int/2addr v9, v2

    invoke-static {v8, v2, v4, v7, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4351
    iput-object v4, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 4354
    :cond_3
    iget v8, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    iget-object v9, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v9, v9

    add-int v3, v8, v9

    .line 4355
    if-lez v3, :cond_7

    .line 4357
    iget v8, p1, Lcom/ibm/icu/math/MathContext;->digits:I

    if-le v3, v8, :cond_4

    .line 4358
    iget v8, p1, Lcom/ibm/icu/math/MathContext;->digits:I

    if-eqz v8, :cond_4

    .line 4359
    iget v8, p1, Lcom/ibm/icu/math/MathContext;->form:I

    int-to-byte v8, v8

    iput-byte v8, p0, Lcom/ibm/icu/math/BigDecimal;->form:B

    .line 4360
    :cond_4
    add-int/lit8 v8, v3, -0x1

    if-gt v8, v11, :cond_8

    .line 4403
    :cond_5
    :goto_2
    return-object p0

    .line 4327
    .end local v0    # "$26":I
    :cond_6
    add-int/lit8 v1, v1, -0x1

    .line 4328
    iget v8, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 4324
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 4364
    .restart local v0    # "$26":I
    :cond_7
    const/4 v8, -0x5

    if-ge v3, v8, :cond_8

    .line 4365
    iget v8, p1, Lcom/ibm/icu/math/MathContext;->form:I

    int-to-byte v8, v8

    iput-byte v8, p0, Lcom/ibm/icu/math/BigDecimal;->form:B

    .line 4367
    :cond_8
    add-int/lit8 v3, v3, -0x1

    .line 4368
    if-ge v3, v10, :cond_b

    move v8, v6

    :goto_3
    if-le v3, v11, :cond_c

    :goto_4
    or-int/2addr v6, v8

    if-eqz v6, :cond_5

    .line 4371
    iget-byte v6, p0, Lcom/ibm/icu/math/BigDecimal;->form:B

    const/4 v7, 0x2

    if-ne v6, v7, :cond_a

    .line 4373
    rem-int/lit8 v5, v3, 0x3

    .line 4374
    if-gez v5, :cond_9

    .line 4375
    add-int/lit8 v5, v5, 0x3

    .line 4376
    :cond_9
    sub-int/2addr v3, v5

    .line 4378
    if-lt v3, v10, :cond_a

    .line 4379
    if-le v3, v11, :cond_5

    .line 4382
    :cond_a
    new-instance v6, Ljava/lang/ArithmeticException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v8, "Exponent Overflow: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_b
    move v8, v7

    .line 4368
    goto :goto_3

    :cond_c
    move v6, v7

    goto :goto_4

    .line 4342
    :cond_d
    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 4390
    :cond_e
    iput-byte v7, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 4392
    iget v6, p1, Lcom/ibm/icu/math/MathContext;->form:I

    if-eqz v6, :cond_10

    .line 4393
    iput v7, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 4402
    :cond_f
    :goto_5
    sget-object v6, Lcom/ibm/icu/math/BigDecimal;->ZERO:Lcom/ibm/icu/math/BigDecimal;

    iget-object v6, v6, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    iput-object v6, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    goto :goto_2

    .line 4394
    :cond_10
    iget v6, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-lez v6, :cond_11

    .line 4395
    iput v7, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    goto :goto_5

    .line 4398
    :cond_11
    iget v6, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-ge v6, v10, :cond_f

    .line 4399
    new-instance v6, Ljava/lang/ArithmeticException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v8, "Exponent Overflow: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget v8, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method private intcheck(II)I
    .locals 4
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3545
    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->intValueExact()I

    move-result v0

    .line 3547
    .local v0, "i":I
    if-ge v0, p1, :cond_0

    move v3, v1

    :goto_0
    if-le v0, p2, :cond_1

    :goto_1
    or-int/2addr v1, v3

    if-eqz v1, :cond_2

    .line 3548
    new-instance v1, Ljava/lang/ArithmeticException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Conversion overflow: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move v3, v2

    .line 3547
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    .line 3549
    :cond_2
    return v0
.end method

.method private layout()[C
    .locals 20

    .prologue
    .line 3415
    const/4 v10, 0x0

    .line 3416
    .local v10, "i":I
    const/4 v15, 0x0

    .line 3417
    .local v15, "sb":Ljava/lang/StringBuffer;
    const/4 v9, 0x0

    .line 3418
    .local v9, "euse":I
    const/16 v16, 0x0

    .line 3419
    .local v16, "sig":I
    const/4 v8, 0x0

    .line 3420
    .local v8, "csign":C
    const/4 v14, 0x0

    .line 3423
    .local v14, "rec":[C
    const/4 v11, 0x0

    .line 3424
    .local v11, "len":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    new-array v7, v0, [C

    .line 3425
    .local v7, "cmant":[C
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v3, v0

    .local v3, "$18":I
    const/4 v10, 0x0

    :goto_0
    if-lez v3, :cond_0

    .line 3426
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v17, v0

    aget-byte v17, v17, v10

    add-int/lit8 v17, v17, 0x30

    move/from16 v0, v17

    int-to-char v0, v0

    move/from16 v17, v0

    aput-char v17, v7, v10

    .line 3425
    add-int/lit8 v3, v3, -0x1

    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 3430
    :cond_0
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/math/BigDecimal;->form:B

    move/from16 v17, v0

    if-eqz v17, :cond_9

    .line 3432
    new-instance v15, Ljava/lang/StringBuffer;

    .end local v15    # "sb":Ljava/lang/StringBuffer;
    array-length v0, v7

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0xf

    move/from16 v0, v17

    invoke-direct {v15, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 3433
    .restart local v15    # "sb":Ljava/lang/StringBuffer;
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 3434
    const/16 v17, 0x2d

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3435
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v17, v0

    array-length v0, v7

    move/from16 v18, v0

    add-int v17, v17, v18

    add-int/lit8 v9, v17, -0x1

    .line 3437
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/math/BigDecimal;->form:B

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_5

    .line 3439
    const/16 v17, 0x0

    aget-char v17, v7, v17

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3440
    array-length v0, v7

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_2

    .line 3441
    const/16 v17, 0x2e

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v17

    const/16 v18, 0x1

    array-length v0, v7

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v7, v1, v2}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 3463
    :cond_2
    :goto_1
    if-eqz v9, :cond_3

    .line 3465
    if-gez v9, :cond_8

    .line 3467
    const/16 v8, 0x2d

    .line 3468
    neg-int v9, v9

    .line 3472
    :goto_2
    const/16 v17, 0x45

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 3474
    :cond_3
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    move/from16 v0, v17

    new-array v14, v0, [C

    .line 3475
    const/16 v17, 0x0

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v18

    const/16 v19, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v15, v0, v1, v14, v2}, Lcom/ibm/icu/impl/Utility;->getChars(Ljava/lang/StringBuffer;II[CI)V

    move-object v7, v14

    .line 3535
    .end local v7    # "cmant":[C
    :cond_4
    :goto_3
    return-object v7

    .line 3445
    .restart local v7    # "cmant":[C
    :cond_5
    rem-int/lit8 v16, v9, 0x3

    .line 3446
    if-gez v16, :cond_6

    .line 3447
    add-int/lit8 v16, v16, 0x3

    .line 3448
    :cond_6
    sub-int v9, v9, v16

    .line 3449
    add-int/lit8 v16, v16, 0x1

    .line 3450
    array-length v0, v7

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    if-lt v0, v1, :cond_7

    .line 3452
    const/16 v17, 0x0

    array-length v0, v7

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v15, v7, v0, v1}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 3453
    array-length v0, v7

    move/from16 v17, v0

    sub-int v4, v16, v17

    .local v4, "$19":I
    :goto_4
    if-lez v4, :cond_2

    .line 3454
    const/16 v17, 0x30

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3453
    add-int/lit8 v4, v4, -0x1

    goto :goto_4

    .line 3460
    .end local v4    # "$19":I
    :cond_7
    const/16 v17, 0x0

    move/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v15, v7, v0, v1}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    move-result-object v17

    const/16 v18, 0x2e

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v17

    array-length v0, v7

    move/from16 v18, v0

    sub-int v18, v18, v16

    move-object/from16 v0, v17

    move/from16 v1, v16

    move/from16 v2, v18

    invoke-virtual {v0, v7, v1, v2}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 3471
    :cond_8
    const/16 v8, 0x2b

    goto :goto_2

    .line 3480
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v17, v0

    if-nez v17, :cond_a

    .line 3482
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    move/from16 v17, v0

    if-gez v17, :cond_4

    .line 3484
    array-length v0, v7

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    new-array v14, v0, [C

    .line 3485
    const/16 v17, 0x0

    const/16 v18, 0x2d

    aput-char v18, v14, v17

    .line 3486
    const/16 v17, 0x0

    const/16 v18, 0x1

    array-length v0, v7

    move/from16 v19, v0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v7, v0, v14, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v7, v14

    .line 3487
    goto/16 :goto_3

    .line 3491
    :cond_a
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_c

    const/4 v13, 0x1

    .line 3495
    .local v13, "needsign":I
    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v17, v0

    array-length v0, v7

    move/from16 v18, v0

    add-int v12, v17, v18

    .line 3497
    .local v12, "mag":I
    const/16 v17, 0x1

    move/from16 v0, v17

    if-ge v12, v0, :cond_e

    .line 3499
    add-int/lit8 v17, v13, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v18, v0

    sub-int v11, v17, v18

    .line 3500
    new-array v14, v11, [C

    .line 3501
    if-eqz v13, :cond_b

    .line 3502
    const/16 v17, 0x0

    const/16 v18, 0x2d

    aput-char v18, v14, v17

    .line 3503
    :cond_b
    const/16 v17, 0x30

    aput-char v17, v14, v13

    .line 3504
    add-int/lit8 v17, v13, 0x1

    const/16 v18, 0x2e

    aput-char v18, v14, v17

    .line 3505
    neg-int v5, v12

    .local v5, "$20":I
    add-int/lit8 v10, v13, 0x2

    :goto_6
    if-lez v5, :cond_d

    .line 3506
    const/16 v17, 0x30

    aput-char v17, v14, v10

    .line 3505
    add-int/lit8 v5, v5, -0x1

    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 3491
    .end local v5    # "$20":I
    .end local v12    # "mag":I
    .end local v13    # "needsign":I
    :cond_c
    const/4 v13, 0x0

    goto :goto_5

    .line 3509
    .restart local v5    # "$20":I
    .restart local v12    # "mag":I
    .restart local v13    # "needsign":I
    :cond_d
    const/16 v17, 0x0

    add-int/lit8 v18, v13, 0x2

    sub-int v18, v18, v12

    array-length v0, v7

    move/from16 v19, v0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v7, v0, v14, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v7, v14

    .line 3510
    goto/16 :goto_3

    .line 3513
    .end local v5    # "$20":I
    :cond_e
    array-length v0, v7

    move/from16 v17, v0

    move/from16 v0, v17

    if-le v12, v0, :cond_11

    .line 3515
    add-int v11, v13, v12

    .line 3516
    new-array v14, v11, [C

    .line 3517
    if-eqz v13, :cond_f

    .line 3518
    const/16 v17, 0x0

    const/16 v18, 0x2d

    aput-char v18, v14, v17

    .line 3519
    :cond_f
    const/16 v17, 0x0

    array-length v0, v7

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v7, v0, v14, v13, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3520
    array-length v0, v7

    move/from16 v17, v0

    sub-int v6, v12, v17

    .local v6, "$21":I
    array-length v0, v7

    move/from16 v17, v0

    add-int v10, v13, v17

    :goto_7
    if-lez v6, :cond_10

    .line 3521
    const/16 v17, 0x30

    aput-char v17, v14, v10

    .line 3520
    add-int/lit8 v6, v6, -0x1

    add-int/lit8 v10, v10, 0x1

    goto :goto_7

    :cond_10
    move-object v7, v14

    .line 3524
    goto/16 :goto_3

    .line 3528
    .end local v6    # "$21":I
    :cond_11
    add-int/lit8 v17, v13, 0x1

    array-length v0, v7

    move/from16 v18, v0

    add-int v11, v17, v18

    .line 3529
    new-array v14, v11, [C

    .line 3530
    if-eqz v13, :cond_12

    .line 3531
    const/16 v17, 0x0

    const/16 v18, 0x2d

    aput-char v18, v14, v17

    .line 3532
    :cond_12
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v7, v0, v14, v13, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3533
    add-int v17, v13, v12

    const/16 v18, 0x2e

    aput-char v18, v14, v17

    .line 3534
    add-int v17, v13, v12

    add-int/lit8 v17, v17, 0x1

    array-length v0, v7

    move/from16 v18, v0

    sub-int v18, v18, v12

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v7, v12, v14, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v7, v14

    .line 3535
    goto/16 :goto_3
.end method

.method private round(II)Lcom/ibm/icu/math/BigDecimal;
    .locals 12
    .param p1, "len"    # I
    .param p2, "mode"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x5

    const/4 v11, 0x0

    .line 4146
    const/4 v5, 0x0

    .line 4147
    .local v5, "reuse":Z
    const/4 v7, 0x0

    .line 4149
    .local v7, "first":B
    const/4 v8, 0x0

    .line 4150
    .local v8, "newmant":[B
    iget-object v0, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v0, v0

    sub-int v6, v0, p1

    .line 4151
    .local v6, "adjust":I
    if-gtz v6, :cond_1

    .line 4265
    :cond_0
    return-object p0

    .line 4154
    :cond_1
    iget v0, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/2addr v0, v6

    iput v0, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 4155
    iget-byte v10, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 4156
    .local v10, "sign":I
    iget-object v9, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 4157
    .local v9, "oldmant":[B
    if-lez p1, :cond_4

    .line 4160
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 4161
    iget-object v0, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    invoke-static {v9, v11, v0, v11, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4162
    const/4 v5, 0x1

    .line 4163
    aget-byte v7, v9, p1

    .line 4177
    :goto_0
    const/4 v4, 0x0

    .line 4179
    .local v4, "increment":I
    const/4 v0, 0x4

    if-ne p2, v0, :cond_6

    .line 4181
    if-lt v7, v1, :cond_2

    .line 4182
    move v4, v10

    .line 4237
    :cond_2
    :goto_1
    if-eqz v4, :cond_3

    .line 4239
    iget-byte v0, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    if-nez v0, :cond_10

    .line 4242
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->ONE:Lcom/ibm/icu/math/BigDecimal;

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    iput-object v0, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 4243
    int-to-byte v0, v4

    iput-byte v0, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 4263
    :cond_3
    :goto_2
    iget v0, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    const v1, 0x3b9ac9ff

    if-le v0, v1, :cond_0

    .line 4264
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Exponent Overflow: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4167
    .end local v4    # "increment":I
    :cond_4
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->ZERO:Lcom/ibm/icu/math/BigDecimal;

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    iput-object v0, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 4168
    iput-byte v11, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 4169
    const/4 v5, 0x0

    .line 4170
    if-nez p1, :cond_5

    .line 4171
    aget-byte v7, v9, v11

    goto :goto_0

    .line 4173
    :cond_5
    const/4 v7, 0x0

    goto :goto_0

    .line 4184
    .restart local v4    # "increment":I
    :cond_6
    const/4 v0, 0x7

    if-ne p2, v0, :cond_7

    .line 4187
    invoke-static {v9, p1}, Lcom/ibm/icu/math/BigDecimal;->allzero([BI)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4188
    new-instance v0, Ljava/lang/ArithmeticException;

    const-string/jumbo v1, "Rounding necessary"

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4190
    :cond_7
    if-ne p2, v1, :cond_9

    .line 4192
    if-le v7, v1, :cond_8

    .line 4193
    move v4, v10

    goto :goto_1

    .line 4195
    :cond_8
    if-ne v7, v1, :cond_2

    .line 4196
    add-int/lit8 v0, p1, 0x1

    invoke-static {v9, v0}, Lcom/ibm/icu/math/BigDecimal;->allzero([BI)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4197
    move v4, v10

    goto :goto_1

    .line 4199
    :cond_9
    const/4 v0, 0x6

    if-ne p2, v0, :cond_c

    .line 4201
    if-le v7, v1, :cond_a

    .line 4202
    move v4, v10

    goto :goto_1

    .line 4204
    :cond_a
    if-ne v7, v1, :cond_2

    .line 4206
    add-int/lit8 v0, p1, 0x1

    invoke-static {v9, v0}, Lcom/ibm/icu/math/BigDecimal;->allzero([BI)Z

    move-result v0

    if-nez v0, :cond_b

    .line 4207
    move v4, v10

    goto :goto_1

    .line 4209
    :cond_b
    iget-object v0, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    iget-object v1, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-byte v0, v0, v1

    rem-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 4210
    move v4, v10

    goto/16 :goto_1

    .line 4213
    :cond_c
    if-eq p2, v3, :cond_2

    .line 4215
    if-nez p2, :cond_d

    .line 4217
    invoke-static {v9, p1}, Lcom/ibm/icu/math/BigDecimal;->allzero([BI)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4218
    move v4, v10

    goto/16 :goto_1

    .line 4220
    :cond_d
    const/4 v0, 0x2

    if-ne p2, v0, :cond_e

    .line 4222
    if-lez v10, :cond_2

    .line 4223
    invoke-static {v9, p1}, Lcom/ibm/icu/math/BigDecimal;->allzero([BI)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4224
    move v4, v10

    goto/16 :goto_1

    .line 4226
    :cond_e
    const/4 v0, 0x3

    if-ne p2, v0, :cond_f

    .line 4228
    if-gez v10, :cond_2

    .line 4229
    invoke-static {v9, p1}, Lcom/ibm/icu/math/BigDecimal;->allzero([BI)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4230
    move v4, v10

    goto/16 :goto_1

    .line 4233
    :cond_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Bad round value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4248
    :cond_10
    iget-byte v0, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    const/4 v1, -0x1

    if-ne v0, v1, :cond_11

    .line 4249
    neg-int v4, v4

    .line 4250
    :cond_11
    iget-object v0, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    iget-object v1, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v1, v1

    sget-object v2, Lcom/ibm/icu/math/BigDecimal;->ONE:Lcom/ibm/icu/math/BigDecimal;

    iget-object v2, v2, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    invoke-static/range {v0 .. v5}, Lcom/ibm/icu/math/BigDecimal;->byteaddsub([BI[BIIZ)[B

    move-result-object v8

    .line 4251
    array-length v0, v8

    iget-object v1, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v1, v1

    if-le v0, v1, :cond_12

    .line 4254
    iget v0, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 4256
    iget-object v0, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    iget-object v1, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v1, v1

    invoke-static {v8, v11, v0, v11, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/16 :goto_2

    .line 4259
    :cond_12
    iput-object v8, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    goto/16 :goto_2
.end method

.method private round(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;
    .locals 2
    .param p1, "set"    # Lcom/ibm/icu/math/MathContext;

    .prologue
    .line 4126
    iget v0, p1, Lcom/ibm/icu/math/MathContext;->digits:I

    iget v1, p1, Lcom/ibm/icu/math/MathContext;->roundingMode:I

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/math/BigDecimal;->round(II)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(D)Lcom/ibm/icu/math/BigDecimal;
    .locals 2
    .param p0, "dub"    # D

    .prologue
    .line 3336
    new-instance v0, Lcom/ibm/icu/math/BigDecimal;

    new-instance v1, Ljava/lang/Double;

    invoke-direct {v1, p0, p1}, Ljava/lang/Double;-><init>(D)V

    invoke-virtual {v1}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/ibm/icu/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static valueOf(J)Lcom/ibm/icu/math/BigDecimal;
    .locals 2
    .param p0, "lint"    # J

    .prologue
    .line 3351
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/ibm/icu/math/BigDecimal;->valueOf(JI)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(JI)Lcom/ibm/icu/math/BigDecimal;
    .locals 6
    .param p0, "lint"    # J
    .param p2, "scale"    # I

    .prologue
    .line 3378
    const/4 v0, 0x0

    .line 3380
    .local v0, "res":Lcom/ibm/icu/math/BigDecimal;
    const-wide/16 v2, 0x0

    cmp-long v2, p0, v2

    if-nez v2, :cond_0

    .line 3381
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->ZERO:Lcom/ibm/icu/math/BigDecimal;

    .line 3390
    :goto_0
    if-nez p2, :cond_3

    move-object v1, v0

    .line 3396
    .end local v0    # "res":Lcom/ibm/icu/math/BigDecimal;
    .local v1, "res":Lcom/ibm/icu/math/BigDecimal;
    :goto_1
    return-object v1

    .line 3382
    .end local v1    # "res":Lcom/ibm/icu/math/BigDecimal;
    .restart local v0    # "res":Lcom/ibm/icu/math/BigDecimal;
    :cond_0
    const-wide/16 v2, 0x1

    cmp-long v2, p0, v2

    if-nez v2, :cond_1

    .line 3383
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->ONE:Lcom/ibm/icu/math/BigDecimal;

    goto :goto_0

    .line 3384
    :cond_1
    const-wide/16 v2, 0xa

    cmp-long v2, p0, v2

    if-nez v2, :cond_2

    .line 3385
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->TEN:Lcom/ibm/icu/math/BigDecimal;

    goto :goto_0

    .line 3387
    :cond_2
    new-instance v0, Lcom/ibm/icu/math/BigDecimal;

    .end local v0    # "res":Lcom/ibm/icu/math/BigDecimal;
    invoke-direct {v0, p0, p1}, Lcom/ibm/icu/math/BigDecimal;-><init>(J)V

    .restart local v0    # "res":Lcom/ibm/icu/math/BigDecimal;
    goto :goto_0

    .line 3392
    :cond_3
    if-gez p2, :cond_4

    .line 3393
    new-instance v2, Ljava/lang/NumberFormatException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "Negative scale: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3394
    :cond_4
    invoke-static {v0}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    .line 3395
    neg-int v2, p2

    iput v2, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move-object v1, v0

    .line 3396
    .end local v0    # "res":Lcom/ibm/icu/math/BigDecimal;
    .restart local v1    # "res":Lcom/ibm/icu/math/BigDecimal;
    goto :goto_1
.end method


# virtual methods
.method public abs()Lcom/ibm/icu/math/BigDecimal;
    .locals 1

    .prologue
    .line 1068
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    invoke-virtual {p0, v0}, Lcom/ibm/icu/math/BigDecimal;->abs(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public abs(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;
    .locals 2
    .param p1, "set"    # Lcom/ibm/icu/math/MathContext;

    .prologue
    .line 1089
    iget-byte v0, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1090
    invoke-virtual {p0, p1}, Lcom/ibm/icu/math/BigDecimal;->negate(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    .line 1091
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/ibm/icu/math/BigDecimal;->plus(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    goto :goto_0
.end method

.method public add(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;
    .locals 1
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;

    .prologue
    .line 1113
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/math/BigDecimal;->add(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public add(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;
    .locals 22
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "set"    # Lcom/ibm/icu/math/MathContext;

    .prologue
    .line 1140
    const/4 v15, 0x0

    .line 1141
    .local v15, "newlen":I
    const/16 v19, 0x0

    .line 1142
    .local v19, "tlen":I
    const/4 v6, 0x0

    .line 1143
    .local v6, "mult":I
    const/16 v18, 0x0

    .line 1144
    .local v18, "t":[B
    const/4 v12, 0x0

    .line 1145
    .local v12, "ia":I
    const/4 v13, 0x0

    .line 1146
    .local v13, "ib":I
    const/4 v10, 0x0

    .line 1147
    .local v10, "ea":I
    const/4 v11, 0x0

    .line 1148
    .local v11, "eb":I
    const/4 v8, 0x0

    .line 1149
    .local v8, "ca":B
    const/4 v9, 0x0

    .line 1151
    .local v9, "cb":B
    move-object/from16 v0, p2

    iget-boolean v7, v0, Lcom/ibm/icu/math/MathContext;->lostDigits:Z

    if-eqz v7, :cond_0

    .line 1152
    move-object/from16 v0, p2

    iget v7, v0, Lcom/ibm/icu/math/MathContext;->digits:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v7}, Lcom/ibm/icu/math/BigDecimal;->checkdigits(Lcom/ibm/icu/math/BigDecimal;I)V

    .line 1153
    :cond_0
    move-object/from16 v14, p0

    .line 1157
    .local v14, "lhs":Lcom/ibm/icu/math/BigDecimal;
    iget-byte v7, v14, Lcom/ibm/icu/math/BigDecimal;->ind:B

    if-nez v7, :cond_1

    .line 1158
    move-object/from16 v0, p2

    iget v7, v0, Lcom/ibm/icu/math/MathContext;->form:I

    if-eqz v7, :cond_1

    .line 1159
    invoke-virtual/range {p1 .. p2}, Lcom/ibm/icu/math/BigDecimal;->plus(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v7

    .line 1346
    :goto_0
    return-object v7

    .line 1160
    :cond_1
    move-object/from16 v0, p1

    iget-byte v7, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    if-nez v7, :cond_2

    .line 1161
    move-object/from16 v0, p2

    iget v7, v0, Lcom/ibm/icu/math/MathContext;->form:I

    if-eqz v7, :cond_2

    .line 1162
    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lcom/ibm/icu/math/BigDecimal;->plus(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v7

    goto :goto_0

    .line 1165
    :cond_2
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/math/MathContext;->digits:I

    move/from16 v16, v0

    .line 1166
    .local v16, "reqdig":I
    if-lez v16, :cond_4

    .line 1168
    iget-object v7, v14, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v7, v7

    move/from16 v0, v16

    if-le v7, v0, :cond_3

    .line 1169
    invoke-static {v14}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-direct {v7, v0}, Lcom/ibm/icu/math/BigDecimal;->round(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v14

    .line 1170
    :cond_3
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v7, v7

    move/from16 v0, v16

    if-le v7, v0, :cond_4

    .line 1171
    invoke-static/range {p1 .. p1}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-direct {v7, v0}, Lcom/ibm/icu/math/BigDecimal;->round(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object p1

    .line 1175
    :cond_4
    new-instance v17, Lcom/ibm/icu/math/BigDecimal;

    invoke-direct/range {v17 .. v17}, Lcom/ibm/icu/math/BigDecimal;-><init>()V

    .line 1184
    .local v17, "res":Lcom/ibm/icu/math/BigDecimal;
    iget-object v2, v14, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 1185
    .local v2, "usel":[B
    iget-object v7, v14, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v3, v7

    .line 1186
    .local v3, "usellen":I
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 1187
    .local v4, "user":[B
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v5, v7

    .line 1189
    .local v5, "userlen":I
    iget v7, v14, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move-object/from16 v0, p1

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ne v7, v0, :cond_7

    .line 1192
    iget v7, v14, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move-object/from16 v0, v17

    iput v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 1264
    :cond_5
    :goto_1
    iget-byte v7, v14, Lcom/ibm/icu/math/BigDecimal;->ind:B

    if-nez v7, :cond_f

    .line 1265
    const/4 v7, 0x1

    move-object/from16 v0, v17

    iput-byte v7, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 1268
    :goto_2
    iget-byte v7, v14, Lcom/ibm/icu/math/BigDecimal;->ind:B

    const/16 v20, -0x1

    move/from16 v0, v20

    if-ne v7, v0, :cond_10

    const/4 v7, 0x1

    :goto_3
    move-object/from16 v0, p1

    iget-byte v0, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_11

    const/16 v20, 0x1

    :goto_4
    move/from16 v0, v20

    if-ne v7, v0, :cond_12

    .line 1269
    const/4 v6, 0x1

    .line 1342
    :cond_6
    :goto_5
    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/ibm/icu/math/BigDecimal;->byteaddsub([BI[BIIZ)[B

    move-result-object v7

    move-object/from16 v0, v17

    iput-object v7, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 1346
    const/4 v7, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v7}, Lcom/ibm/icu/math/BigDecimal;->finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v7

    goto/16 :goto_0

    .line 1194
    :cond_7
    iget v7, v14, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move-object/from16 v0, p1

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-le v7, v0, :cond_b

    .line 1196
    iget v7, v14, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/2addr v7, v3

    move-object/from16 v0, p1

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v20, v0

    sub-int v15, v7, v20

    .line 1200
    add-int v7, v5, v16

    add-int/lit8 v7, v7, 0x1

    if-lt v15, v7, :cond_9

    .line 1201
    if-lez v16, :cond_9

    .line 1204
    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 1205
    iget v7, v14, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move-object/from16 v0, v17

    iput v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 1206
    iget-byte v7, v14, Lcom/ibm/icu/math/BigDecimal;->ind:B

    move-object/from16 v0, v17

    iput-byte v7, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 1207
    move/from16 v0, v16

    if-ge v3, v0, :cond_8

    .line 1209
    iget-object v7, v14, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move/from16 v0, v16

    invoke-static {v7, v0}, Lcom/ibm/icu/math/BigDecimal;->extend([BI)[B

    move-result-object v7

    move-object/from16 v0, v17

    iput-object v7, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 1210
    move-object/from16 v0, v17

    iget v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    sub-int v20, v16, v3

    sub-int v7, v7, v20

    move-object/from16 v0, v17

    iput v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 1212
    :cond_8
    const/4 v7, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v7}, Lcom/ibm/icu/math/BigDecimal;->finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v7

    goto/16 :goto_0

    .line 1215
    :cond_9
    move-object/from16 v0, p1

    iget v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move-object/from16 v0, v17

    iput v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 1216
    add-int/lit8 v7, v16, 0x1

    if-le v15, v7, :cond_a

    .line 1217
    if-lez v16, :cond_a

    .line 1220
    sub-int v7, v15, v16

    add-int/lit8 v19, v7, -0x1

    .line 1221
    sub-int v5, v5, v19

    .line 1222
    move-object/from16 v0, v17

    iget v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int v7, v7, v19

    move-object/from16 v0, v17

    iput v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 1223
    add-int/lit8 v15, v16, 0x1

    .line 1225
    :cond_a
    if-le v15, v3, :cond_5

    .line 1226
    move v3, v15

    goto/16 :goto_1

    .line 1229
    :cond_b
    move-object/from16 v0, p1

    iget v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/2addr v7, v5

    iget v0, v14, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v20, v0

    sub-int v15, v7, v20

    .line 1230
    add-int v7, v3, v16

    add-int/lit8 v7, v7, 0x1

    if-lt v15, v7, :cond_d

    .line 1231
    if-lez v16, :cond_d

    .line 1234
    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 1235
    move-object/from16 v0, p1

    iget v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move-object/from16 v0, v17

    iput v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 1236
    move-object/from16 v0, p1

    iget-byte v7, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    move-object/from16 v0, v17

    iput-byte v7, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 1237
    move/from16 v0, v16

    if-ge v5, v0, :cond_c

    .line 1239
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move/from16 v0, v16

    invoke-static {v7, v0}, Lcom/ibm/icu/math/BigDecimal;->extend([BI)[B

    move-result-object v7

    move-object/from16 v0, v17

    iput-object v7, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 1240
    move-object/from16 v0, v17

    iget v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    sub-int v20, v16, v5

    sub-int v7, v7, v20

    move-object/from16 v0, v17

    iput v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 1242
    :cond_c
    const/4 v7, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v7}, Lcom/ibm/icu/math/BigDecimal;->finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v7

    goto/16 :goto_0

    .line 1245
    :cond_d
    iget v7, v14, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move-object/from16 v0, v17

    iput v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 1246
    add-int/lit8 v7, v16, 0x1

    if-le v15, v7, :cond_e

    .line 1247
    if-lez v16, :cond_e

    .line 1250
    sub-int v7, v15, v16

    add-int/lit8 v19, v7, -0x1

    .line 1251
    sub-int v3, v3, v19

    .line 1252
    move-object/from16 v0, v17

    iget v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int v7, v7, v19

    move-object/from16 v0, v17

    iput v7, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 1253
    add-int/lit8 v15, v16, 0x1

    .line 1255
    :cond_e
    if-le v15, v5, :cond_5

    .line 1256
    move v5, v15

    goto/16 :goto_1

    .line 1267
    :cond_f
    iget-byte v7, v14, Lcom/ibm/icu/math/BigDecimal;->ind:B

    move-object/from16 v0, v17

    iput-byte v7, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    goto/16 :goto_2

    .line 1268
    :cond_10
    const/4 v7, 0x0

    goto/16 :goto_3

    :cond_11
    const/16 v20, 0x0

    goto/16 :goto_4

    .line 1272
    :cond_12
    const/4 v6, -0x1

    .line 1277
    move-object/from16 v0, p1

    iget-byte v7, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    if-eqz v7, :cond_6

    .line 1279
    if-ge v3, v5, :cond_13

    const/4 v7, 0x1

    :goto_6
    iget-byte v0, v14, Lcom/ibm/icu/math/BigDecimal;->ind:B

    move/from16 v20, v0

    if-nez v20, :cond_14

    const/16 v20, 0x1

    :goto_7
    or-int v7, v7, v20

    if-eqz v7, :cond_15

    .line 1281
    move-object/from16 v18, v2

    .line 1282
    move-object v2, v4

    .line 1283
    move-object/from16 v4, v18

    .line 1284
    move/from16 v19, v3

    .line 1285
    move v3, v5

    .line 1286
    move/from16 v5, v19

    .line 1287
    move-object/from16 v0, v17

    iget-byte v7, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    neg-int v7, v7

    int-to-byte v7, v7

    move-object/from16 v0, v17

    iput-byte v7, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    goto/16 :goto_5

    .line 1279
    :cond_13
    const/4 v7, 0x0

    goto :goto_6

    :cond_14
    const/16 v20, 0x0

    goto :goto_7

    .line 1289
    :cond_15
    if-gt v3, v5, :cond_6

    .line 1294
    const/4 v12, 0x0

    .line 1295
    const/4 v13, 0x0

    .line 1296
    array-length v7, v2

    add-int/lit8 v10, v7, -0x1

    .line 1297
    array-length v7, v4

    add-int/lit8 v11, v7, -0x1

    .line 1299
    :goto_8
    if-gt v12, v10, :cond_16

    .line 1300
    aget-byte v8, v2, v12

    .line 1312
    :goto_9
    if-gt v13, v11, :cond_18

    .line 1313
    aget-byte v9, v4, v13

    .line 1316
    :goto_a
    if-eq v8, v9, :cond_19

    .line 1318
    if-ge v8, v9, :cond_6

    .line 1320
    move-object/from16 v18, v2

    .line 1321
    move-object v2, v4

    .line 1322
    move-object/from16 v4, v18

    .line 1323
    move/from16 v19, v3

    .line 1324
    move v3, v5

    .line 1325
    move/from16 v5, v19

    .line 1326
    move-object/from16 v0, v17

    iget-byte v7, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    neg-int v7, v7

    int-to-byte v7, v7

    move-object/from16 v0, v17

    iput-byte v7, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    goto/16 :goto_5

    .line 1303
    :cond_16
    if-le v13, v11, :cond_17

    .line 1305
    move-object/from16 v0, p2

    iget v7, v0, Lcom/ibm/icu/math/MathContext;->form:I

    if-eqz v7, :cond_6

    .line 1306
    sget-object v7, Lcom/ibm/icu/math/BigDecimal;->ZERO:Lcom/ibm/icu/math/BigDecimal;

    goto/16 :goto_0

    .line 1310
    :cond_17
    const/4 v8, 0x0

    goto :goto_9

    .line 1315
    :cond_18
    const/4 v9, 0x0

    goto :goto_a

    .line 1331
    :cond_19
    add-int/lit8 v12, v12, 0x1

    .line 1332
    add-int/lit8 v13, v13, 0x1

    .line 1333
    goto :goto_8
.end method

.method public byteValueExact()B
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2189
    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->intValueExact()I

    move-result v0

    .line 2190
    .local v0, "num":I
    const/16 v3, 0x7f

    if-le v0, v3, :cond_0

    move v3, v1

    :goto_0
    const/16 v4, -0x80

    if-ge v0, v4, :cond_1

    :goto_1
    or-int/2addr v1, v3

    if-eqz v1, :cond_2

    .line 2191
    new-instance v1, Ljava/lang/ArithmeticException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Conversion overflow: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move v3, v2

    .line 2190
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    .line 2192
    :cond_2
    int-to-byte v1, v0

    return v1
.end method

.method public compareTo(Lcom/ibm/icu/math/BigDecimal;)I
    .locals 1
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;

    .prologue
    .line 1367
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/math/BigDecimal;->compareTo(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)I
    .locals 9
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "set"    # Lcom/ibm/icu/math/MathContext;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1403
    const/4 v3, 0x0

    .line 1404
    .local v3, "thislength":I
    const/4 v1, 0x0

    .line 1407
    .local v1, "i":I
    iget-boolean v4, p2, Lcom/ibm/icu/math/MathContext;->lostDigits:Z

    if-eqz v4, :cond_0

    .line 1408
    iget v4, p2, Lcom/ibm/icu/math/MathContext;->digits:I

    invoke-direct {p0, p1, v4}, Lcom/ibm/icu/math/BigDecimal;->checkdigits(Lcom/ibm/icu/math/BigDecimal;I)V

    .line 1410
    :cond_0
    iget-byte v4, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    iget-byte v7, p1, Lcom/ibm/icu/math/BigDecimal;->ind:B

    if-ne v4, v7, :cond_2

    move v4, v5

    :goto_0
    iget v7, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    iget v8, p1, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-ne v7, v8, :cond_3

    move v7, v5

    :goto_1
    and-int/2addr v4, v7

    if-eqz v4, :cond_a

    .line 1413
    iget-object v4, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v3, v4

    .line 1414
    iget-object v4, p1, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v4, v4

    if-ge v3, v4, :cond_4

    .line 1415
    iget-byte v4, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    neg-int v4, v4

    int-to-byte v6, v4

    .line 1444
    :cond_1
    :goto_2
    return v6

    :cond_2
    move v4, v6

    .line 1410
    goto :goto_0

    :cond_3
    move v7, v6

    goto :goto_1

    .line 1416
    :cond_4
    iget-object v4, p1, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v4, v4

    if-le v3, v4, :cond_5

    .line 1417
    iget-byte v6, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    goto :goto_2

    .line 1420
    :cond_5
    iget v4, p2, Lcom/ibm/icu/math/MathContext;->digits:I

    if-gt v3, v4, :cond_6

    move v4, v5

    :goto_3
    iget v7, p2, Lcom/ibm/icu/math/MathContext;->digits:I

    if-nez v7, :cond_7

    :goto_4
    or-int/2addr v4, v5

    if-eqz v4, :cond_c

    .line 1422
    move v0, v3

    .local v0, "$6":I
    const/4 v1, 0x0

    :goto_5
    if-lez v0, :cond_1

    .line 1423
    iget-object v4, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    aget-byte v4, v4, v1

    iget-object v5, p1, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    aget-byte v5, v5, v1

    if-ge v4, v5, :cond_8

    .line 1424
    iget-byte v4, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    neg-int v4, v4

    int-to-byte v6, v4

    goto :goto_2

    .end local v0    # "$6":I
    :cond_6
    move v4, v6

    .line 1420
    goto :goto_3

    :cond_7
    move v5, v6

    goto :goto_4

    .line 1425
    .restart local v0    # "$6":I
    :cond_8
    iget-object v4, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    aget-byte v4, v4, v1

    iget-object v5, p1, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    aget-byte v5, v5, v1

    if-le v4, v5, :cond_9

    .line 1426
    iget-byte v6, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    goto :goto_2

    .line 1422
    :cond_9
    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1436
    .end local v0    # "$6":I
    :cond_a
    iget-byte v4, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    iget-byte v6, p1, Lcom/ibm/icu/math/BigDecimal;->ind:B

    if-ge v4, v6, :cond_b

    .line 1437
    const/4 v6, -0x1

    goto :goto_2

    .line 1438
    :cond_b
    iget-byte v4, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    iget-byte v6, p1, Lcom/ibm/icu/math/BigDecimal;->ind:B

    if-le v4, v6, :cond_c

    move v6, v5

    .line 1439
    goto :goto_2

    .line 1442
    :cond_c
    invoke-static {p1}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v2

    .line 1443
    .local v2, "newrhs":Lcom/ibm/icu/math/BigDecimal;
    iget-byte v4, v2, Lcom/ibm/icu/math/BigDecimal;->ind:B

    neg-int v4, v4

    int-to-byte v4, v4

    iput-byte v4, v2, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 1444
    invoke-virtual {p0, v2, p2}, Lcom/ibm/icu/math/BigDecimal;->add(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v4

    iget-byte v6, v4, Lcom/ibm/icu/math/BigDecimal;->ind:B

    goto :goto_2
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "rhsobj"    # Ljava/lang/Object;

    .prologue
    .line 2220
    check-cast p1, Lcom/ibm/icu/math/BigDecimal;

    .end local p1    # "rhsobj":Ljava/lang/Object;
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/math/BigDecimal;->compareTo(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)I

    move-result v0

    return v0
.end method

.method public divide(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;
    .locals 3
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;

    .prologue
    .line 1468
    const/16 v0, 0x44

    sget-object v1, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    const/4 v2, -0x1

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/ibm/icu/math/BigDecimal;->dodivide(CLcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;I)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public divide(Lcom/ibm/icu/math/BigDecimal;I)Lcom/ibm/icu/math/BigDecimal;
    .locals 3
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "round"    # I

    .prologue
    const/4 v1, 0x0

    .line 1504
    new-instance v0, Lcom/ibm/icu/math/MathContext;

    invoke-direct {v0, v1, v1, v1, p2}, Lcom/ibm/icu/math/MathContext;-><init>(IIZI)V

    .line 1505
    .local v0, "set":Lcom/ibm/icu/math/MathContext;
    const/16 v1, 0x44

    const/4 v2, -0x1

    invoke-direct {p0, v1, p1, v0, v2}, Lcom/ibm/icu/math/BigDecimal;->dodivide(CLcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;I)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v1

    return-object v1
.end method

.method public divide(Lcom/ibm/icu/math/BigDecimal;II)Lcom/ibm/icu/math/BigDecimal;
    .locals 4
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "scale"    # I
    .param p3, "round"    # I

    .prologue
    const/4 v1, 0x0

    .line 1544
    if-gez p2, :cond_0

    .line 1545
    new-instance v1, Ljava/lang/ArithmeticException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Negative scale: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1546
    :cond_0
    new-instance v0, Lcom/ibm/icu/math/MathContext;

    invoke-direct {v0, v1, v1, v1, p3}, Lcom/ibm/icu/math/MathContext;-><init>(IIZI)V

    .line 1547
    .local v0, "set":Lcom/ibm/icu/math/MathContext;
    const/16 v1, 0x44

    invoke-direct {p0, v1, p1, v0, p2}, Lcom/ibm/icu/math/BigDecimal;->dodivide(CLcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;I)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v1

    return-object v1
.end method

.method public divide(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;
    .locals 2
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "set"    # Lcom/ibm/icu/math/MathContext;

    .prologue
    .line 1568
    const/16 v0, 0x44

    const/4 v1, -0x1

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/ibm/icu/math/BigDecimal;->dodivide(CLcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;I)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public divideInteger(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;
    .locals 3
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;

    .prologue
    .line 1589
    const/16 v0, 0x49

    sget-object v1, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    const/4 v2, 0x0

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/ibm/icu/math/BigDecimal;->dodivide(CLcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;I)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public divideInteger(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;
    .locals 2
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "set"    # Lcom/ibm/icu/math/MathContext;

    .prologue
    .line 1614
    const/16 v0, 0x49

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/ibm/icu/math/BigDecimal;->dodivide(CLcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;I)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public doubleValue()D
    .locals 2

    .prologue
    .line 2242
    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 11
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2274
    const/4 v2, 0x0

    .line 2275
    .local v2, "i":I
    const/4 v3, 0x0

    .line 2276
    .local v3, "lca":[C
    const/4 v4, 0x0

    .line 2278
    .local v4, "rca":[C
    if-nez p1, :cond_1

    .line 2308
    :cond_0
    :goto_0
    return v8

    .line 2280
    :cond_1
    instance-of v6, p1, Lcom/ibm/icu/math/BigDecimal;

    if-eqz v6, :cond_0

    move-object v5, p1

    .line 2282
    check-cast v5, Lcom/ibm/icu/math/BigDecimal;

    .line 2283
    .local v5, "rhs":Lcom/ibm/icu/math/BigDecimal;
    iget-byte v6, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    iget-byte v9, v5, Lcom/ibm/icu/math/BigDecimal;->ind:B

    if-ne v6, v9, :cond_0

    .line 2285
    iget-object v6, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v6, v6

    iget-object v9, v5, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v9, v9

    if-ne v6, v9, :cond_2

    move v6, v7

    :goto_1
    iget v9, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    iget v10, v5, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-ne v9, v10, :cond_3

    move v9, v7

    :goto_2
    and-int/2addr v9, v6

    iget-byte v6, p0, Lcom/ibm/icu/math/BigDecimal;->form:B

    iget-byte v10, v5, Lcom/ibm/icu/math/BigDecimal;->form:B

    if-ne v6, v10, :cond_4

    move v6, v7

    :goto_3
    and-int/2addr v6, v9

    if-eqz v6, :cond_5

    .line 2289
    iget-object v6, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v0, v6

    .local v0, "$8":I
    const/4 v2, 0x0

    :goto_4
    if-lez v0, :cond_6

    .line 2290
    iget-object v6, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    aget-byte v6, v6, v2

    iget-object v9, v5, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    aget-byte v9, v9, v2

    if-ne v6, v9, :cond_0

    .line 2289
    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .end local v0    # "$8":I
    :cond_2
    move v6, v8

    .line 2285
    goto :goto_1

    :cond_3
    move v9, v8

    goto :goto_2

    :cond_4
    move v6, v8

    goto :goto_3

    .line 2297
    :cond_5
    invoke-direct {p0}, Lcom/ibm/icu/math/BigDecimal;->layout()[C

    move-result-object v3

    .line 2298
    invoke-direct {v5}, Lcom/ibm/icu/math/BigDecimal;->layout()[C

    move-result-object v4

    .line 2299
    array-length v6, v3

    array-length v9, v4

    if-ne v6, v9, :cond_0

    .line 2302
    array-length v1, v3

    .local v1, "$9":I
    const/4 v2, 0x0

    :goto_5
    if-lez v1, :cond_6

    .line 2303
    aget-char v6, v3, v2

    aget-char v9, v4, v2

    if-ne v6, v9, :cond_0

    .line 2302
    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .end local v1    # "$9":I
    :cond_6
    move v8, v7

    .line 2308
    goto :goto_0
.end method

.method public floatValue()F
    .locals 1

    .prologue
    .line 2328
    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public format(II)Ljava/lang/String;
    .locals 7
    .param p1, "before"    # I
    .param p2, "after"    # I

    .prologue
    const/4 v3, -0x1

    .line 2394
    const/4 v5, 0x1

    const/4 v6, 0x4

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    invoke-virtual/range {v0 .. v6}, Lcom/ibm/icu/math/BigDecimal;->format(IIIIII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public format(IIIIII)Ljava/lang/String;
    .locals 28
    .param p1, "before"    # I
    .param p2, "after"    # I
    .param p3, "explaces"    # I
    .param p4, "exdigits"    # I
    .param p5, "exformint"    # I
    .param p6, "exround"    # I

    .prologue
    .line 2509
    const/4 v15, 0x0

    .line 2510
    .local v15, "mag":I
    const/16 v23, 0x0

    .line 2511
    .local v23, "thisafter":I
    const/4 v14, 0x0

    .line 2512
    .local v14, "lead":I
    const/16 v18, 0x0

    .line 2513
    .local v18, "newmant":[B
    const/4 v12, 0x0

    .line 2514
    .local v12, "chop":I
    const/16 v16, 0x0

    .line 2515
    .local v16, "need":I
    const/16 v20, 0x0

    .line 2517
    .local v20, "oldexp":I
    const/16 v21, 0x0

    .line 2518
    .local v21, "p":I
    const/16 v17, 0x0

    .line 2519
    .local v17, "newa":[C
    const/4 v13, 0x0

    .line 2520
    .local v13, "i":I
    const/16 v22, 0x0

    .line 2524
    .local v22, "places":I
    const/16 v24, -0x1

    move/from16 v0, p1

    move/from16 v1, v24

    if-ge v0, v1, :cond_a

    const/16 v24, 0x1

    move/from16 v25, v24

    :goto_0
    if-nez p1, :cond_b

    const/16 v24, 0x1

    :goto_1
    or-int v24, v24, v25

    if-eqz v24, :cond_0

    .line 2525
    const-string/jumbo v24, "format"

    const/16 v25, 0x1

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/math/BigDecimal;->badarg(Ljava/lang/String;ILjava/lang/String;)V

    .line 2526
    :cond_0
    const/16 v24, -0x1

    move/from16 v0, p2

    move/from16 v1, v24

    if-ge v0, v1, :cond_1

    .line 2527
    const-string/jumbo v24, "format"

    const/16 v25, 0x2

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/math/BigDecimal;->badarg(Ljava/lang/String;ILjava/lang/String;)V

    .line 2528
    :cond_1
    const/16 v24, -0x1

    move/from16 v0, p3

    move/from16 v1, v24

    if-ge v0, v1, :cond_c

    const/16 v24, 0x1

    move/from16 v25, v24

    :goto_2
    if-nez p3, :cond_d

    const/16 v24, 0x1

    :goto_3
    or-int v24, v24, v25

    if-eqz v24, :cond_2

    .line 2529
    const-string/jumbo v24, "format"

    const/16 v25, 0x3

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/math/BigDecimal;->badarg(Ljava/lang/String;ILjava/lang/String;)V

    .line 2530
    :cond_2
    const/16 v24, -0x1

    move/from16 v0, p4

    move/from16 v1, v24

    if-ge v0, v1, :cond_3

    .line 2531
    const-string/jumbo v24, "format"

    const/16 v25, 0x4

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/math/BigDecimal;->badarg(Ljava/lang/String;ILjava/lang/String;)V

    .line 2533
    :cond_3
    const/16 v24, 0x1

    move/from16 v0, p5

    move/from16 v1, v24

    if-ne v0, v1, :cond_e

    .line 2544
    :cond_4
    :goto_4
    const/16 v24, 0x4

    move/from16 v0, p6

    move/from16 v1, v24

    if-eq v0, v1, :cond_5

    .line 2546
    const/16 v24, -0x1

    move/from16 v0, p6

    move/from16 v1, v24

    if-ne v0, v1, :cond_10

    .line 2547
    const/16 p6, 0x4

    .line 2555
    :cond_5
    :goto_5
    invoke-static/range {p0 .. p0}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v19

    .line 2570
    .local v19, "num":Lcom/ibm/icu/math/BigDecimal;
    const/16 v24, -0x1

    move/from16 v0, p4

    move/from16 v1, v24

    if-ne v0, v1, :cond_11

    .line 2571
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, v19

    iput-byte v0, v1, Lcom/ibm/icu/math/BigDecimal;->form:B

    .line 2591
    :goto_6
    if-ltz p2, :cond_7

    .line 2595
    :cond_6
    :goto_7
    move-object/from16 v0, v19

    iget-byte v0, v0, Lcom/ibm/icu/math/BigDecimal;->form:B

    move/from16 v24, v0

    if-nez v24, :cond_15

    .line 2596
    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v24, v0

    move/from16 v0, v24

    neg-int v0, v0

    move/from16 v23, v0

    .line 2610
    :goto_8
    move/from16 v0, v23

    move/from16 v1, p2

    if-ne v0, v1, :cond_19

    .line 2647
    :cond_7
    :goto_9
    invoke-direct/range {v19 .. v19}, Lcom/ibm/icu/math/BigDecimal;->layout()[C

    move-result-object v11

    .line 2651
    .local v11, "a":[C
    if-lez p1, :cond_1e

    .line 2654
    array-length v6, v11

    .local v6, "$11":I
    const/16 v21, 0x0

    :goto_a
    if-lez v6, :cond_8

    .line 2655
    aget-char v24, v11, v21

    const/16 v25, 0x2e

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_1c

    .line 2663
    :cond_8
    move/from16 v0, v21

    move/from16 v1, p1

    if-le v0, v1, :cond_9

    .line 2664
    const-string/jumbo v24, "format"

    const/16 v25, 0x1

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/math/BigDecimal;->badarg(Ljava/lang/String;ILjava/lang/String;)V

    .line 2665
    :cond_9
    move/from16 v0, v21

    move/from16 v1, p1

    if-ge v0, v1, :cond_1e

    .line 2667
    array-length v0, v11

    move/from16 v24, v0

    add-int v24, v24, p1

    sub-int v24, v24, v21

    move/from16 v0, v24

    new-array v0, v0, [C

    move-object/from16 v17, v0

    .line 2668
    sub-int v7, p1, v21

    .local v7, "$12":I
    const/4 v13, 0x0

    :goto_b
    if-lez v7, :cond_1d

    .line 2669
    const/16 v24, 0x20

    aput-char v24, v17, v13

    .line 2668
    add-int/lit8 v7, v7, -0x1

    add-int/lit8 v13, v13, 0x1

    goto :goto_b

    .line 2524
    .end local v6    # "$11":I
    .end local v7    # "$12":I
    .end local v11    # "a":[C
    .end local v19    # "num":Lcom/ibm/icu/math/BigDecimal;
    :cond_a
    const/16 v24, 0x0

    move/from16 v25, v24

    goto/16 :goto_0

    :cond_b
    const/16 v24, 0x0

    goto/16 :goto_1

    .line 2528
    :cond_c
    const/16 v24, 0x0

    move/from16 v25, v24

    goto/16 :goto_2

    :cond_d
    const/16 v24, 0x0

    goto/16 :goto_3

    .line 2534
    :cond_e
    const/16 v24, 0x2

    move/from16 v0, p5

    move/from16 v1, v24

    if-eq v0, v1, :cond_4

    .line 2535
    const/16 v24, -0x1

    move/from16 v0, p5

    move/from16 v1, v24

    if-ne v0, v1, :cond_f

    .line 2536
    const/16 p5, 0x1

    goto/16 :goto_4

    .line 2539
    :cond_f
    const-string/jumbo v24, "format"

    const/16 v25, 0x5

    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/math/BigDecimal;->badarg(Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_4

    .line 2549
    :cond_10
    :try_start_0
    new-instance v24, Lcom/ibm/icu/math/MathContext;

    const/16 v25, 0x9

    const/16 v26, 0x1

    const/16 v27, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, p6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/math/MathContext;-><init>(IIZI)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_5

    .line 2551
    :catch_0
    move-exception v5

    .line 2552
    .local v5, "$10":Ljava/lang/IllegalArgumentException;
    const-string/jumbo v24, "format"

    const/16 v25, 0x6

    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/math/BigDecimal;->badarg(Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_5

    .line 2572
    .end local v5    # "$10":Ljava/lang/IllegalArgumentException;
    .restart local v19    # "num":Lcom/ibm/icu/math/BigDecimal;
    :cond_11
    move-object/from16 v0, v19

    iget-byte v0, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    move/from16 v24, v0

    if-nez v24, :cond_12

    .line 2573
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, v19

    iput-byte v0, v1, Lcom/ibm/icu/math/BigDecimal;->form:B

    goto/16 :goto_6

    .line 2576
    :cond_12
    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v24, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    add-int v15, v24, v25

    .line 2577
    move/from16 v0, p4

    if-le v15, v0, :cond_13

    .line 2578
    move/from16 v0, p5

    int-to-byte v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v19

    iput-byte v0, v1, Lcom/ibm/icu/math/BigDecimal;->form:B

    goto/16 :goto_6

    .line 2580
    :cond_13
    const/16 v24, -0x5

    move/from16 v0, v24

    if-ge v15, v0, :cond_14

    .line 2581
    move/from16 v0, p5

    int-to-byte v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, v19

    iput-byte v0, v1, Lcom/ibm/icu/math/BigDecimal;->form:B

    goto/16 :goto_6

    .line 2583
    :cond_14
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, v19

    iput-byte v0, v1, Lcom/ibm/icu/math/BigDecimal;->form:B

    goto/16 :goto_6

    .line 2597
    :cond_15
    move-object/from16 v0, v19

    iget-byte v0, v0, Lcom/ibm/icu/math/BigDecimal;->form:B

    move/from16 v24, v0

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_16

    .line 2598
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    add-int/lit8 v23, v24, -0x1

    goto/16 :goto_8

    .line 2600
    :cond_16
    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v24, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    add-int v24, v24, v25

    add-int/lit8 v24, v24, -0x1

    rem-int/lit8 v14, v24, 0x3

    .line 2601
    if-gez v14, :cond_17

    .line 2602
    add-int/lit8 v14, v14, 0x3

    .line 2603
    :cond_17
    add-int/lit8 v14, v14, 0x1

    .line 2604
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    if-lt v14, v0, :cond_18

    .line 2605
    const/16 v23, 0x0

    goto/16 :goto_8

    .line 2607
    :cond_18
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    sub-int v23, v24, v14

    goto/16 :goto_8

    .line 2612
    :cond_19
    move/from16 v0, v23

    move/from16 v1, p2

    if-ge v0, v1, :cond_1a

    .line 2615
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v24, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    add-int v25, v25, p2

    sub-int v25, v25, v23

    invoke-static/range {v24 .. v25}, Lcom/ibm/icu/math/BigDecimal;->extend([BI)[B

    move-result-object v18

    .line 2616
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 2617
    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v24, v0

    sub-int v25, p2, v23

    sub-int v24, v24, v25

    move/from16 v0, v24

    move-object/from16 v1, v19

    iput v0, v1, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 2618
    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v24, v0

    const v25, -0x3b9ac9ff

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_7

    .line 2619
    new-instance v24, Ljava/lang/ArithmeticException;

    new-instance v25, Ljava/lang/StringBuffer;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v26, "Exponent Overflow: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 2625
    :cond_1a
    sub-int v12, v23, p2

    .line 2626
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    if-le v12, v0, :cond_1b

    .line 2629
    sget-object v24, Lcom/ibm/icu/math/BigDecimal;->ZERO:Lcom/ibm/icu/math/BigDecimal;

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 2630
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, v19

    iput-byte v0, v1, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 2631
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, v19

    iput v0, v1, Lcom/ibm/icu/math/BigDecimal;->exp:I

    goto/16 :goto_7

    .line 2636
    :cond_1b
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    sub-int v16, v24, v12

    .line 2637
    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v20, v0

    .line 2638
    move-object/from16 v0, v19

    move/from16 v1, v16

    move/from16 v2, p6

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/math/BigDecimal;->round(II)Lcom/ibm/icu/math/BigDecimal;

    .line 2641
    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move/from16 v24, v0

    sub-int v24, v24, v20

    move/from16 v0, v24

    if-ne v0, v12, :cond_6

    goto/16 :goto_9

    .line 2657
    .restart local v6    # "$11":I
    .restart local v11    # "a":[C
    :cond_1c
    aget-char v24, v11, v21

    const/16 v25, 0x45

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_8

    .line 2654
    add-int/lit8 v6, v6, -0x1

    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_a

    .line 2672
    .restart local v7    # "$12":I
    :cond_1d
    const/16 v24, 0x0

    array-length v0, v11

    move/from16 v25, v0

    move/from16 v0, v24

    move-object/from16 v1, v17

    move/from16 v2, v25

    invoke-static {v11, v0, v1, v13, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2673
    move-object/from16 v11, v17

    .line 2678
    .end local v6    # "$11":I
    .end local v7    # "$12":I
    :cond_1e
    if-lez p3, :cond_22

    .line 2681
    array-length v0, v11

    move/from16 v24, v0

    add-int/lit8 v8, v24, -0x1

    .local v8, "$13":I
    array-length v0, v11

    move/from16 v24, v0

    add-int/lit8 v21, v24, -0x1

    :goto_c
    if-lez v8, :cond_1f

    .line 2682
    aget-char v24, v11, v21

    const/16 v25, 0x45

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_20

    .line 2687
    :cond_1f
    if-nez v21, :cond_23

    .line 2689
    array-length v0, v11

    move/from16 v24, v0

    add-int v24, v24, p3

    add-int/lit8 v24, v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [C

    move-object/from16 v17, v0

    .line 2690
    const/16 v24, 0x0

    const/16 v25, 0x0

    array-length v0, v11

    move/from16 v26, v0

    move/from16 v0, v24

    move-object/from16 v1, v17

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-static {v11, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2691
    add-int/lit8 v9, p3, 0x2

    .local v9, "$14":I
    array-length v13, v11

    :goto_d
    if-lez v9, :cond_21

    .line 2692
    const/16 v24, 0x20

    aput-char v24, v17, v13

    .line 2691
    add-int/lit8 v9, v9, -0x1

    add-int/lit8 v13, v13, 0x1

    goto :goto_d

    .line 2681
    .end local v9    # "$14":I
    :cond_20
    add-int/lit8 v8, v8, -0x1

    add-int/lit8 v21, v21, -0x1

    goto :goto_c

    .line 2695
    .restart local v9    # "$14":I
    :cond_21
    move-object/from16 v11, v17

    .line 2716
    .end local v8    # "$13":I
    .end local v9    # "$14":I
    :cond_22
    :goto_e
    new-instance v24, Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-direct {v0, v11}, Ljava/lang/String;-><init>([C)V

    return-object v24

    .line 2699
    .restart local v8    # "$13":I
    :cond_23
    array-length v0, v11

    move/from16 v24, v0

    sub-int v24, v24, v21

    add-int/lit8 v22, v24, -0x2

    .line 2700
    move/from16 v0, v22

    move/from16 v1, p3

    if-le v0, v1, :cond_24

    .line 2701
    const-string/jumbo v24, "format"

    const/16 v25, 0x3

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/math/BigDecimal;->badarg(Ljava/lang/String;ILjava/lang/String;)V

    .line 2702
    :cond_24
    move/from16 v0, v22

    move/from16 v1, p3

    if-ge v0, v1, :cond_22

    .line 2704
    array-length v0, v11

    move/from16 v24, v0

    add-int v24, v24, p3

    sub-int v24, v24, v22

    move/from16 v0, v24

    new-array v0, v0, [C

    move-object/from16 v17, v0

    .line 2705
    const/16 v24, 0x0

    const/16 v25, 0x0

    add-int/lit8 v26, v21, 0x2

    move/from16 v0, v24

    move-object/from16 v1, v17

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-static {v11, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2706
    sub-int v10, p3, v22

    .local v10, "$15":I
    add-int/lit8 v13, v21, 0x2

    :goto_f
    if-lez v10, :cond_25

    .line 2707
    const/16 v24, 0x30

    aput-char v24, v17, v13

    .line 2706
    add-int/lit8 v10, v10, -0x1

    add-int/lit8 v13, v13, 0x1

    goto :goto_f

    .line 2710
    :cond_25
    add-int/lit8 v24, v21, 0x2

    move/from16 v0, v24

    move-object/from16 v1, v17

    move/from16 v2, v22

    invoke-static {v11, v0, v1, v13, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2711
    move-object/from16 v11, v17

    goto :goto_e
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 2737
    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public intValue()I
    .locals 1

    .prologue
    .line 2755
    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    return v0
.end method

.method public intValueExact()I
    .locals 10

    .prologue
    const/16 v9, 0x9

    const/4 v6, 0x0

    .line 2773
    const/4 v5, 0x0

    .line 2775
    .local v5, "useexp":I
    const/4 v1, 0x0

    .line 2776
    .local v1, "i":I
    const/4 v4, 0x0

    .line 2780
    .local v4, "topdig":I
    iget-byte v7, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    if-nez v7, :cond_1

    move v3, v6

    .line 2829
    :cond_0
    :goto_0
    return v3

    .line 2783
    :cond_1
    iget-object v7, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v7, v7

    add-int/lit8 v2, v7, -0x1

    .line 2784
    .local v2, "lodigit":I
    iget v7, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-gez v7, :cond_5

    .line 2786
    iget v7, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/2addr v2, v7

    .line 2788
    iget-object v7, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    add-int/lit8 v8, v2, 0x1

    invoke-static {v7, v8}, Lcom/ibm/icu/math/BigDecimal;->allzero([BI)Z

    move-result v7

    if-nez v7, :cond_2

    .line 2789
    new-instance v6, Ljava/lang/ArithmeticException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v8, "Decimal part non-zero: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 2790
    :cond_2
    if-gez v2, :cond_3

    move v3, v6

    .line 2791
    goto :goto_0

    .line 2792
    :cond_3
    const/4 v5, 0x0

    .line 2801
    :goto_1
    const/4 v3, 0x0

    .line 2802
    .local v3, "result":I
    add-int v0, v2, v5

    .local v0, "$16":I
    const/4 v1, 0x0

    :goto_2
    if-gt v1, v0, :cond_7

    .line 2803
    mul-int/lit8 v3, v3, 0xa

    .line 2804
    if-gt v1, v2, :cond_4

    .line 2805
    iget-object v7, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    aget-byte v7, v7, v1

    add-int/2addr v3, v7

    .line 2802
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2796
    .end local v0    # "$16":I
    .end local v3    # "result":I
    :cond_5
    iget v7, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/2addr v7, v2

    if-le v7, v9, :cond_6

    .line 2797
    new-instance v6, Ljava/lang/ArithmeticException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v8, "Conversion overflow: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 2798
    :cond_6
    iget v5, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    goto :goto_1

    .line 2810
    .restart local v0    # "$16":I
    .restart local v3    # "result":I
    :cond_7
    add-int v7, v2, v5

    if-ne v7, v9, :cond_9

    .line 2814
    const v7, 0x3b9aca00

    div-int v4, v3, v7

    .line 2815
    iget-object v7, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    aget-byte v7, v7, v6

    if-eq v4, v7, :cond_9

    .line 2818
    const/high16 v7, -0x80000000

    if-ne v3, v7, :cond_8

    .line 2819
    iget-byte v7, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    const/4 v8, -0x1

    if-ne v7, v8, :cond_8

    .line 2820
    iget-object v7, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    aget-byte v6, v7, v6

    const/4 v7, 0x2

    if-eq v6, v7, :cond_0

    .line 2822
    :cond_8
    new-instance v6, Ljava/lang/ArithmeticException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v8, "Conversion overflow: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 2827
    :cond_9
    iget-byte v6, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    const/4 v7, 0x1

    if-eq v6, v7, :cond_0

    .line 2829
    neg-int v3, v3

    goto/16 :goto_0
.end method

.method public longValue()J
    .locals 2

    .prologue
    .line 2847
    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public longValueExact()J
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/16 v13, 0x12

    const/4 v12, 0x0

    .line 2865
    const/4 v1, 0x0

    .line 2866
    .local v1, "cstart":I
    const/4 v8, 0x0

    .line 2868
    .local v8, "useexp":I
    const/4 v2, 0x0

    .line 2869
    .local v2, "i":I
    const-wide/16 v6, 0x0

    .line 2871
    .local v6, "topdig":J
    iget-byte v9, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    if-nez v9, :cond_1

    .line 2925
    :cond_0
    :goto_0
    return-wide v4

    .line 2873
    :cond_1
    iget-object v9, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v9, v9

    add-int/lit8 v3, v9, -0x1

    .line 2874
    .local v3, "lodigit":I
    iget v9, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-gez v9, :cond_5

    .line 2876
    iget v9, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/2addr v3, v9

    .line 2878
    if-gez v3, :cond_2

    .line 2879
    const/4 v1, 0x0

    .line 2882
    :goto_1
    iget-object v9, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    invoke-static {v9, v1}, Lcom/ibm/icu/math/BigDecimal;->allzero([BI)Z

    move-result v9

    if-nez v9, :cond_3

    .line 2883
    new-instance v9, Ljava/lang/ArithmeticException;

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v11, "Decimal part non-zero: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 2881
    :cond_2
    add-int/lit8 v1, v3, 0x1

    goto :goto_1

    .line 2884
    :cond_3
    if-ltz v3, :cond_0

    .line 2886
    const/4 v8, 0x0

    .line 2899
    :goto_2
    const-wide/16 v4, 0x0

    .line 2900
    .local v4, "result":J
    add-int v0, v3, v8

    .local v0, "$17":I
    const/4 v2, 0x0

    :goto_3
    if-gt v2, v0, :cond_7

    .line 2901
    const-wide/16 v10, 0xa

    mul-long/2addr v4, v10

    .line 2902
    if-gt v2, v3, :cond_4

    .line 2903
    iget-object v9, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    aget-byte v9, v9, v2

    int-to-long v10, v9

    add-long/2addr v4, v10

    .line 2900
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2890
    .end local v0    # "$17":I
    .end local v4    # "result":J
    :cond_5
    iget v9, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    iget-object v10, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v10, v10

    add-int/2addr v9, v10

    if-le v9, v13, :cond_6

    .line 2891
    new-instance v9, Ljava/lang/ArithmeticException;

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v11, "Conversion overflow: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 2892
    :cond_6
    iget v8, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    goto :goto_2

    .line 2908
    .restart local v0    # "$17":I
    .restart local v4    # "result":J
    :cond_7
    add-int v9, v3, v8

    if-ne v9, v13, :cond_9

    .line 2910
    const-wide v10, 0xde0b6b3a7640000L

    div-long v6, v4, v10

    .line 2911
    iget-object v9, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    aget-byte v9, v9, v12

    int-to-long v10, v9

    cmp-long v9, v6, v10

    if-eqz v9, :cond_9

    .line 2914
    const-wide/high16 v10, -0x8000000000000000L

    cmp-long v9, v4, v10

    if-nez v9, :cond_8

    .line 2915
    iget-byte v9, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    const/4 v10, -0x1

    if-ne v9, v10, :cond_8

    .line 2916
    iget-object v9, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    aget-byte v9, v9, v12

    const/16 v10, 0x9

    if-eq v9, v10, :cond_0

    .line 2918
    :cond_8
    new-instance v9, Ljava/lang/ArithmeticException;

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v11, "Conversion overflow: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 2923
    :cond_9
    iget-byte v9, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    const/4 v10, 0x1

    if-eq v9, v10, :cond_0

    .line 2925
    neg-long v4, v4

    goto/16 :goto_0
.end method

.method public max(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;
    .locals 1
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;

    .prologue
    .line 1633
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/math/BigDecimal;->max(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public max(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;
    .locals 1
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "set"    # Lcom/ibm/icu/math/MathContext;

    .prologue
    .line 1660
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/math/BigDecimal;->compareTo(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 1661
    invoke-virtual {p0, p2}, Lcom/ibm/icu/math/BigDecimal;->plus(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    .line 1663
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1, p2}, Lcom/ibm/icu/math/BigDecimal;->plus(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    goto :goto_0
.end method

.method public min(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;
    .locals 1
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;

    .prologue
    .line 1682
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/math/BigDecimal;->min(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public min(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;
    .locals 1
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "set"    # Lcom/ibm/icu/math/MathContext;

    .prologue
    .line 1709
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/math/BigDecimal;->compareTo(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)I

    move-result v0

    if-gtz v0, :cond_0

    .line 1710
    invoke-virtual {p0, p2}, Lcom/ibm/icu/math/BigDecimal;->plus(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    .line 1712
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1, p2}, Lcom/ibm/icu/math/BigDecimal;->plus(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    goto :goto_0
.end method

.method public movePointLeft(I)Lcom/ibm/icu/math/BigDecimal;
    .locals 3
    .param p1, "n"    # I

    .prologue
    .line 2953
    invoke-static {p0}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    .line 2954
    .local v0, "res":Lcom/ibm/icu/math/BigDecimal;
    iget v1, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    sub-int/2addr v1, p1

    iput v1, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 2955
    sget-object v1, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/math/BigDecimal;->finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v1

    return-object v1
.end method

.method public movePointRight(I)Lcom/ibm/icu/math/BigDecimal;
    .locals 3
    .param p1, "n"    # I

    .prologue
    .line 2982
    invoke-static {p0}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    .line 2983
    .local v0, "res":Lcom/ibm/icu/math/BigDecimal;
    iget v1, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/2addr v1, p1

    iput v1, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 2984
    sget-object v1, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/math/BigDecimal;->finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v1

    return-object v1
.end method

.method public multiply(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;
    .locals 1
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;

    .prologue
    .line 1735
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/math/BigDecimal;->multiply(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public multiply(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;
    .locals 15
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "set"    # Lcom/ibm/icu/math/MathContext;

    .prologue
    .line 1758
    const/4 v10, 0x0

    .line 1759
    .local v10, "multer":[B
    const/4 v3, 0x0

    .line 1761
    .local v3, "multand":[B
    const/4 v8, 0x0

    .line 1764
    .local v8, "acclen":I
    const/4 v11, 0x0

    .line 1765
    .local v11, "n":I
    const/4 v5, 0x0

    .line 1766
    .local v5, "mult":B
    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/ibm/icu/math/MathContext;->lostDigits:Z

    if-eqz v2, :cond_0

    .line 1767
    move-object/from16 v0, p2

    iget v2, v0, Lcom/ibm/icu/math/MathContext;->digits:I

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v2}, Lcom/ibm/icu/math/BigDecimal;->checkdigits(Lcom/ibm/icu/math/BigDecimal;I)V

    .line 1768
    :cond_0
    move-object v9, p0

    .line 1771
    .local v9, "lhs":Lcom/ibm/icu/math/BigDecimal;
    const/4 v12, 0x0

    .line 1772
    .local v12, "padding":I
    move-object/from16 v0, p2

    iget v13, v0, Lcom/ibm/icu/math/MathContext;->digits:I

    .line 1773
    .local v13, "reqdig":I
    if-lez v13, :cond_4

    .line 1775
    iget-object v2, v9, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v2, v2

    if-le v2, v13, :cond_1

    .line 1776
    invoke-static {v9}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Lcom/ibm/icu/math/BigDecimal;->round(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v9

    .line 1777
    :cond_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v2, v2

    if-le v2, v13, :cond_2

    .line 1778
    invoke-static/range {p1 .. p1}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Lcom/ibm/icu/math/BigDecimal;->round(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object p1

    .line 1795
    :cond_2
    :goto_0
    iget-object v2, v9, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v2, v2

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v6, v6

    if-ge v2, v6, :cond_6

    .line 1797
    iget-object v10, v9, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 1798
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 1807
    :goto_1
    array-length v2, v10

    array-length v6, v3

    add-int/2addr v2, v6

    add-int/lit8 v4, v2, -0x1

    .line 1809
    .local v4, "multandlen":I
    const/4 v2, 0x0

    aget-byte v2, v10, v2

    const/4 v6, 0x0

    aget-byte v6, v3, v6

    mul-int/2addr v2, v6

    const/16 v6, 0x9

    if-le v2, v6, :cond_7

    .line 1810
    add-int/lit8 v8, v4, 0x1

    .line 1815
    :goto_2
    new-instance v14, Lcom/ibm/icu/math/BigDecimal;

    invoke-direct {v14}, Lcom/ibm/icu/math/BigDecimal;-><init>()V

    .line 1816
    .local v14, "res":Lcom/ibm/icu/math/BigDecimal;
    new-array v1, v8, [B

    .line 1821
    .local v1, "acc":[B
    array-length v7, v10

    .local v7, "$7":I
    const/4 v11, 0x0

    :goto_3
    if-lez v7, :cond_8

    .line 1822
    aget-byte v5, v10, v11

    .line 1823
    if-eqz v5, :cond_3

    .line 1826
    array-length v2, v1

    const/4 v6, 0x1

    invoke-static/range {v1 .. v6}, Lcom/ibm/icu/math/BigDecimal;->byteaddsub([BI[BIIZ)[B

    move-result-object v1

    .line 1829
    :cond_3
    add-int/lit8 v4, v4, -0x1

    .line 1821
    add-int/lit8 v7, v7, -0x1

    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 1785
    .end local v1    # "acc":[B
    .end local v4    # "multandlen":I
    .end local v7    # "$7":I
    .end local v14    # "res":Lcom/ibm/icu/math/BigDecimal;
    :cond_4
    iget v2, v9, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-lez v2, :cond_5

    .line 1786
    iget v2, v9, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/2addr v12, v2

    .line 1787
    :cond_5
    move-object/from16 v0, p1

    iget v2, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-lez v2, :cond_2

    .line 1788
    move-object/from16 v0, p1

    iget v2, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/2addr v12, v2

    goto :goto_0

    .line 1802
    :cond_6
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 1803
    iget-object v3, v9, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    goto :goto_1

    .line 1812
    .restart local v4    # "multandlen":I
    :cond_7
    move v8, v4

    goto :goto_2

    .line 1833
    .restart local v1    # "acc":[B
    .restart local v7    # "$7":I
    .restart local v14    # "res":Lcom/ibm/icu/math/BigDecimal;
    :cond_8
    iget-byte v2, v9, Lcom/ibm/icu/math/BigDecimal;->ind:B

    move-object/from16 v0, p1

    iget-byte v6, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    mul-int/2addr v2, v6

    int-to-byte v2, v2

    iput-byte v2, v14, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 1834
    iget v2, v9, Lcom/ibm/icu/math/BigDecimal;->exp:I

    move-object/from16 v0, p1

    iget v6, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/2addr v2, v6

    sub-int/2addr v2, v12

    iput v2, v14, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 1838
    if-nez v12, :cond_9

    .line 1839
    iput-object v1, v14, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 1842
    :goto_4
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-direct {v14, v0, v2}, Lcom/ibm/icu/math/BigDecimal;->finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v2

    return-object v2

    .line 1841
    :cond_9
    array-length v2, v1

    add-int/2addr v2, v12

    invoke-static {v1, v2}, Lcom/ibm/icu/math/BigDecimal;->extend([BI)[B

    move-result-object v2

    iput-object v2, v14, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    goto :goto_4
.end method

.method public negate()Lcom/ibm/icu/math/BigDecimal;
    .locals 1

    .prologue
    .line 1862
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    invoke-virtual {p0, v0}, Lcom/ibm/icu/math/BigDecimal;->negate(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public negate(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;
    .locals 3
    .param p1, "set"    # Lcom/ibm/icu/math/MathContext;

    .prologue
    .line 1883
    iget-boolean v1, p1, Lcom/ibm/icu/math/MathContext;->lostDigits:Z

    if-eqz v1, :cond_0

    .line 1884
    const/4 v1, 0x0

    check-cast v1, Lcom/ibm/icu/math/BigDecimal;

    iget v2, p1, Lcom/ibm/icu/math/MathContext;->digits:I

    invoke-direct {p0, v1, v2}, Lcom/ibm/icu/math/BigDecimal;->checkdigits(Lcom/ibm/icu/math/BigDecimal;I)V

    .line 1885
    :cond_0
    invoke-static {p0}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    .line 1886
    .local v0, "res":Lcom/ibm/icu/math/BigDecimal;
    iget-byte v1, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    neg-int v1, v1

    int-to-byte v1, v1

    iput-byte v1, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 1887
    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/ibm/icu/math/BigDecimal;->finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v1

    return-object v1
.end method

.method public plus()Lcom/ibm/icu/math/BigDecimal;
    .locals 1

    .prologue
    .line 1908
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    invoke-virtual {p0, v0}, Lcom/ibm/icu/math/BigDecimal;->plus(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public plus(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;
    .locals 2
    .param p1, "set"    # Lcom/ibm/icu/math/MathContext;

    .prologue
    .line 1932
    iget-boolean v0, p1, Lcom/ibm/icu/math/MathContext;->lostDigits:Z

    if-eqz v0, :cond_0

    .line 1933
    const/4 v0, 0x0

    check-cast v0, Lcom/ibm/icu/math/BigDecimal;

    iget v1, p1, Lcom/ibm/icu/math/MathContext;->digits:I

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/math/BigDecimal;->checkdigits(Lcom/ibm/icu/math/BigDecimal;I)V

    .line 1935
    :cond_0
    iget v0, p1, Lcom/ibm/icu/math/MathContext;->form:I

    if-nez v0, :cond_3

    .line 1936
    iget-byte v0, p0, Lcom/ibm/icu/math/BigDecimal;->form:B

    if-nez v0, :cond_3

    .line 1938
    iget-object v0, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v0, v0

    iget v1, p1, Lcom/ibm/icu/math/MathContext;->digits:I

    if-gt v0, v1, :cond_2

    .line 1943
    .end local p0    # "this":Lcom/ibm/icu/math/BigDecimal;
    :cond_1
    :goto_0
    return-object p0

    .line 1940
    .restart local p0    # "this":Lcom/ibm/icu/math/BigDecimal;
    :cond_2
    iget v0, p1, Lcom/ibm/icu/math/MathContext;->digits:I

    if-eqz v0, :cond_1

    .line 1943
    :cond_3
    invoke-static {p0}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/ibm/icu/math/BigDecimal;->finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object p0

    goto :goto_0
.end method

.method public pow(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;
    .locals 1
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;

    .prologue
    .line 1974
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/math/BigDecimal;->pow(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public pow(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;
    .locals 12
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "set"    # Lcom/ibm/icu/math/MathContext;

    .prologue
    .line 2010
    const/4 v7, 0x0

    .line 2011
    .local v7, "workdigits":I
    const/4 v0, 0x0

    .line 2015
    .local v0, "L":I
    const/4 v1, 0x0

    .line 2016
    .local v1, "i":I
    iget-boolean v9, p2, Lcom/ibm/icu/math/MathContext;->lostDigits:Z

    if-eqz v9, :cond_0

    .line 2017
    iget v9, p2, Lcom/ibm/icu/math/MathContext;->digits:I

    invoke-direct {p0, p1, v9}, Lcom/ibm/icu/math/BigDecimal;->checkdigits(Lcom/ibm/icu/math/BigDecimal;I)V

    .line 2018
    :cond_0
    const v9, -0x3b9ac9ff

    const v10, 0x3b9ac9ff

    invoke-direct {p1, v9, v10}, Lcom/ibm/icu/math/BigDecimal;->intcheck(II)I

    move-result v3

    .line 2019
    .local v3, "n":I
    move-object v2, p0

    .line 2021
    .local v2, "lhs":Lcom/ibm/icu/math/BigDecimal;
    iget v4, p2, Lcom/ibm/icu/math/MathContext;->digits:I

    .line 2022
    .local v4, "reqdig":I
    if-nez v4, :cond_2

    .line 2024
    iget-byte v9, p1, Lcom/ibm/icu/math/BigDecimal;->ind:B

    const/4 v10, -0x1

    if-ne v9, v10, :cond_1

    .line 2025
    new-instance v9, Ljava/lang/ArithmeticException;

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v11, "Negative power: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {p1}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 2026
    :cond_1
    const/4 v7, 0x0

    .line 2045
    :goto_0
    new-instance v8, Lcom/ibm/icu/math/MathContext;

    iget v9, p2, Lcom/ibm/icu/math/MathContext;->form:I

    const/4 v10, 0x0

    iget v11, p2, Lcom/ibm/icu/math/MathContext;->roundingMode:I

    invoke-direct {v8, v7, v9, v10, v11}, Lcom/ibm/icu/math/MathContext;-><init>(IIZI)V

    .line 2047
    .local v8, "workset":Lcom/ibm/icu/math/MathContext;
    sget-object v5, Lcom/ibm/icu/math/BigDecimal;->ONE:Lcom/ibm/icu/math/BigDecimal;

    .line 2048
    .local v5, "res":Lcom/ibm/icu/math/BigDecimal;
    if-nez v3, :cond_5

    move-object v9, v5

    .line 2069
    :goto_1
    return-object v9

    .line 2030
    .end local v5    # "res":Lcom/ibm/icu/math/BigDecimal;
    .end local v8    # "workset":Lcom/ibm/icu/math/MathContext;
    :cond_2
    iget-object v9, p1, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v9, v9

    iget v10, p1, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/2addr v9, v10

    if-le v9, v4, :cond_3

    .line 2031
    new-instance v9, Ljava/lang/ArithmeticException;

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v11, "Too many digits: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {p1}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 2034
    :cond_3
    iget-object v9, v2, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v9, v9

    if-le v9, v4, :cond_4

    .line 2035
    invoke-static {v2}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v9

    invoke-direct {v9, p2}, Lcom/ibm/icu/math/BigDecimal;->round(Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v2

    .line 2038
    :cond_4
    iget-object v9, p1, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v9, v9

    iget v10, p1, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int v0, v9, v10

    .line 2039
    add-int v9, v4, v0

    add-int/lit8 v7, v9, 0x1

    goto :goto_0

    .line 2050
    .restart local v5    # "res":Lcom/ibm/icu/math/BigDecimal;
    .restart local v8    # "workset":Lcom/ibm/icu/math/MathContext;
    :cond_5
    if-gez v3, :cond_6

    .line 2051
    neg-int v3, v3

    .line 2052
    :cond_6
    const/4 v6, 0x0

    .line 2053
    .local v6, "seenbit":Z
    const/4 v1, 0x1

    .line 2054
    :goto_2
    add-int/2addr v3, v3

    .line 2055
    if-gez v3, :cond_7

    .line 2057
    const/4 v6, 0x1

    .line 2058
    invoke-virtual {v5, v2, v8}, Lcom/ibm/icu/math/BigDecimal;->multiply(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v5

    .line 2060
    :cond_7
    const/16 v9, 0x1f

    if-ne v1, v9, :cond_9

    .line 2067
    iget-byte v9, p1, Lcom/ibm/icu/math/BigDecimal;->ind:B

    if-gez v9, :cond_8

    .line 2068
    sget-object v9, Lcom/ibm/icu/math/BigDecimal;->ONE:Lcom/ibm/icu/math/BigDecimal;

    invoke-virtual {v9, v5, v8}, Lcom/ibm/icu/math/BigDecimal;->divide(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v5

    .line 2069
    :cond_8
    const/4 v9, 0x1

    invoke-direct {v5, p2, v9}, Lcom/ibm/icu/math/BigDecimal;->finish(Lcom/ibm/icu/math/MathContext;Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v9

    goto :goto_1

    .line 2062
    :cond_9
    if-nez v6, :cond_a

    .line 2053
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2064
    :cond_a
    invoke-virtual {v5, v5, v8}, Lcom/ibm/icu/math/BigDecimal;->multiply(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v5

    goto :goto_3
.end method

.method public remainder(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;
    .locals 3
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;

    .prologue
    .line 2091
    const/16 v0, 0x52

    sget-object v1, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    const/4 v2, -0x1

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/ibm/icu/math/BigDecimal;->dodivide(CLcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;I)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public remainder(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;
    .locals 2
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "set"    # Lcom/ibm/icu/math/MathContext;

    .prologue
    .line 2118
    const/16 v0, 0x52

    const/4 v1, -0x1

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/ibm/icu/math/BigDecimal;->dodivide(CLcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;I)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public scale()I
    .locals 1

    .prologue
    .line 3000
    iget v0, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-ltz v0, :cond_0

    .line 3001
    const/4 v0, 0x0

    .line 3002
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    neg-int v0, v0

    goto :goto_0
.end method

.method public setScale(I)Lcom/ibm/icu/math/BigDecimal;
    .locals 1
    .param p1, "scale"    # I

    .prologue
    .line 3033
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/math/BigDecimal;->setScale(II)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public setScale(II)Lcom/ibm/icu/math/BigDecimal;
    .locals 7
    .param p1, "scale"    # I
    .param p2, "round"    # I

    .prologue
    .line 3073
    const/4 v2, 0x0

    .line 3074
    .local v2, "padding":I
    const/4 v0, 0x0

    .line 3077
    .local v0, "newlen":I
    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->scale()I

    move-result v1

    .line 3078
    .local v1, "ourscale":I
    if-ne v1, p1, :cond_0

    .line 3079
    iget-byte v4, p0, Lcom/ibm/icu/math/BigDecimal;->form:B

    if-nez v4, :cond_0

    .line 3108
    .end local p0    # "this":Lcom/ibm/icu/math/BigDecimal;
    :goto_0
    return-object p0

    .line 3081
    .restart local p0    # "this":Lcom/ibm/icu/math/BigDecimal;
    :cond_0
    invoke-static {p0}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v3

    .line 3082
    .local v3, "res":Lcom/ibm/icu/math/BigDecimal;
    if-gt v1, p1, :cond_3

    .line 3085
    if-nez v1, :cond_2

    .line 3086
    iget v4, v3, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int v2, v4, p1

    .line 3089
    :goto_1
    iget-object v4, v3, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    iget-object v5, v3, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v5, v5

    add-int/2addr v5, v2

    invoke-static {v4, v5}, Lcom/ibm/icu/math/BigDecimal;->extend([BI)[B

    move-result-object v4

    iput-object v4, v3, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 3090
    neg-int v4, p1

    iput v4, v3, Lcom/ibm/icu/math/BigDecimal;->exp:I

    .line 3107
    :cond_1
    :goto_2
    const/4 v4, 0x0

    iput-byte v4, v3, Lcom/ibm/icu/math/BigDecimal;->form:B

    move-object p0, v3

    .line 3108
    goto :goto_0

    .line 3088
    :cond_2
    sub-int v2, p1, v1

    goto :goto_1

    .line 3094
    :cond_3
    if-gez p1, :cond_4

    .line 3095
    new-instance v4, Ljava/lang/ArithmeticException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "Negative scale: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 3097
    :cond_4
    iget-object v4, v3, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v4, v4

    sub-int v5, v1, p1

    sub-int v0, v4, v5

    .line 3098
    invoke-direct {v3, v0, p2}, Lcom/ibm/icu/math/BigDecimal;->round(II)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v3

    .line 3101
    iget v4, v3, Lcom/ibm/icu/math/BigDecimal;->exp:I

    neg-int v5, p1

    if-eq v4, v5, :cond_1

    .line 3103
    iget-object v4, v3, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    iget-object v5, v3, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v5, v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v4, v5}, Lcom/ibm/icu/math/BigDecimal;->extend([BI)[B

    move-result-object v4

    iput-object v4, v3, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 3104
    iget v4, v3, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v3, Lcom/ibm/icu/math/BigDecimal;->exp:I

    goto :goto_2
.end method

.method public shortValueExact()S
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3126
    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->intValueExact()I

    move-result v0

    .line 3127
    .local v0, "num":I
    const/16 v3, 0x7fff

    if-le v0, v3, :cond_0

    move v3, v1

    :goto_0
    const/16 v4, -0x8000

    if-ge v0, v4, :cond_1

    :goto_1
    or-int/2addr v1, v3

    if-eqz v1, :cond_2

    .line 3128
    new-instance v1, Ljava/lang/ArithmeticException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Conversion overflow: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move v3, v2

    .line 3127
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    .line 3129
    :cond_2
    int-to-short v1, v0

    return v1
.end method

.method public signum()I
    .locals 1

    .prologue
    .line 3147
    iget-byte v0, p0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    return v0
.end method

.method public subtract(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;
    .locals 1
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;

    .prologue
    .line 2140
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->plainMC:Lcom/ibm/icu/math/MathContext;

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/math/BigDecimal;->subtract(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public subtract(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;
    .locals 2
    .param p1, "rhs"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "set"    # Lcom/ibm/icu/math/MathContext;

    .prologue
    .line 2161
    iget-boolean v1, p2, Lcom/ibm/icu/math/MathContext;->lostDigits:Z

    if-eqz v1, :cond_0

    .line 2162
    iget v1, p2, Lcom/ibm/icu/math/MathContext;->digits:I

    invoke-direct {p0, p1, v1}, Lcom/ibm/icu/math/BigDecimal;->checkdigits(Lcom/ibm/icu/math/BigDecimal;I)V

    .line 2166
    :cond_0
    invoke-static {p1}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    .line 2167
    .local v0, "newrhs":Lcom/ibm/icu/math/BigDecimal;
    iget-byte v1, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    neg-int v1, v1

    int-to-byte v1, v1

    iput-byte v1, v0, Lcom/ibm/icu/math/BigDecimal;->ind:B

    .line 2168
    invoke-virtual {p0, v0, p2}, Lcom/ibm/icu/math/BigDecimal;->add(Lcom/ibm/icu/math/BigDecimal;Lcom/ibm/icu/math/MathContext;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v1

    return-object v1
.end method

.method public toBigDecimal()Ljava/math/BigDecimal;
    .locals 3

    .prologue
    .line 3172
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->unscaledValue()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->scale()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ljava/math/BigDecimal;-><init>(Ljava/math/BigInteger;I)V

    return-object v0
.end method

.method public toBigInteger()Ljava/math/BigInteger;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 3190
    const/4 v2, 0x0

    .line 3191
    .local v2, "res":Lcom/ibm/icu/math/BigDecimal;
    const/4 v0, 0x0

    .line 3192
    .local v0, "newlen":I
    const/4 v1, 0x0

    .line 3194
    .local v1, "newmant":[B
    iget v3, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-ltz v3, :cond_0

    move v3, v4

    :goto_0
    iget-byte v6, p0, Lcom/ibm/icu/math/BigDecimal;->form:B

    if-nez v6, :cond_1

    :goto_1
    and-int/2addr v3, v4

    if-eqz v3, :cond_2

    .line 3195
    move-object v2, p0

    .line 3219
    :goto_2
    new-instance v3, Ljava/math/BigInteger;

    new-instance v4, Ljava/lang/String;

    invoke-direct {v2}, Lcom/ibm/icu/math/BigDecimal;->layout()[C

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>([C)V

    invoke-direct {v3, v4}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    return-object v3

    :cond_0
    move v3, v5

    .line 3194
    goto :goto_0

    :cond_1
    move v4, v5

    goto :goto_1

    .line 3196
    :cond_2
    iget v3, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-ltz v3, :cond_3

    .line 3198
    invoke-static {p0}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v2

    .line 3199
    iput-byte v5, v2, Lcom/ibm/icu/math/BigDecimal;->form:B

    goto :goto_2

    .line 3204
    :cond_3
    iget v3, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    neg-int v3, v3

    iget-object v4, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v4, v4

    if-lt v3, v4, :cond_4

    .line 3205
    sget-object v2, Lcom/ibm/icu/math/BigDecimal;->ZERO:Lcom/ibm/icu/math/BigDecimal;

    goto :goto_2

    .line 3208
    :cond_4
    invoke-static {p0}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v2

    .line 3209
    iget-object v3, v2, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v3, v3

    iget v4, v2, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int v0, v3, v4

    .line 3210
    new-array v1, v0, [B

    .line 3211
    iget-object v3, v2, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    invoke-static {v3, v5, v1, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3212
    iput-object v1, v2, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    .line 3213
    iput-byte v5, v2, Lcom/ibm/icu/math/BigDecimal;->form:B

    .line 3214
    iput v5, v2, Lcom/ibm/icu/math/BigDecimal;->exp:I

    goto :goto_2
.end method

.method public toBigIntegerExact()Ljava/math/BigInteger;
    .locals 3

    .prologue
    .line 3237
    iget v0, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-gez v0, :cond_0

    .line 3240
    iget-object v0, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    iget-object v1, p0, Lcom/ibm/icu/math/BigDecimal;->mant:[B

    array-length v1, v1

    iget v2, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/ibm/icu/math/BigDecimal;->allzero([BI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3241
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Decimal part non-zero: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3243
    :cond_0
    invoke-virtual {p0}, Lcom/ibm/icu/math/BigDecimal;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public toCharArray()[C
    .locals 1

    .prologue
    .line 3259
    invoke-direct {p0}, Lcom/ibm/icu/math/BigDecimal;->layout()[C

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3282
    new-instance v0, Ljava/lang/String;

    invoke-direct {p0}, Lcom/ibm/icu/math/BigDecimal;->layout()[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method public unscaledValue()Ljava/math/BigInteger;
    .locals 2

    .prologue
    .line 3299
    const/4 v0, 0x0

    .line 3300
    .local v0, "res":Lcom/ibm/icu/math/BigDecimal;
    iget v1, p0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    if-ltz v1, :cond_0

    .line 3301
    move-object v0, p0

    .line 3307
    :goto_0
    invoke-virtual {v0}, Lcom/ibm/icu/math/BigDecimal;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v1

    return-object v1

    .line 3304
    :cond_0
    invoke-static {p0}, Lcom/ibm/icu/math/BigDecimal;->clone(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    .line 3305
    const/4 v1, 0x0

    iput v1, v0, Lcom/ibm/icu/math/BigDecimal;->exp:I

    goto :goto_0
.end method
