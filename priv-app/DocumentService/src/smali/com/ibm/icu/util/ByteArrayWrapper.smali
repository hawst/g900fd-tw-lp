.class public Lcom/ibm/icu/util/ByteArrayWrapper;
.super Ljava/lang/Object;
.source "ByteArrayWrapper.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public bytes:[B

.field public size:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 3
    .param p1, "source"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    .line 81
    iget v0, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    .line 82
    iget-object v0, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    const/4 v1, 0x0

    iget v2, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    invoke-virtual {p1, v0, v1, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 83
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 3
    .param p1, "bytesToAdopt"    # [B
    .param p2, "size"    # I

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    if-nez p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    if-ltz p2, :cond_1

    array-length v0, p1

    if-le p2, v0, :cond_2

    .line 68
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "illegal size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_2
    iput-object p1, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    .line 71
    iput p2, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    .line 72
    return-void
.end method

.method private static final copyBytes([BI[BII)V
    .locals 3
    .param p0, "src"    # [B
    .param p1, "srcoff"    # I
    .param p2, "tgt"    # [B
    .param p3, "tgtoff"    # I
    .param p4, "length"    # I

    .prologue
    .line 279
    const/16 v2, 0x40

    if-ge p4, v2, :cond_0

    .line 280
    move v0, p1

    .local v0, "i":I
    move v1, p3

    .local v1, "n":I
    :goto_0
    add-int/lit8 p4, p4, -0x1

    if-ltz p4, :cond_1

    .line 281
    aget-byte v2, p0, v0

    aput-byte v2, p2, v1

    .line 280
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 285
    .end local v0    # "i":I
    .end local v1    # "n":I
    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 287
    :cond_1
    return-void
.end method


# virtual methods
.method public final append([BII)Lcom/ibm/icu/util/ByteArrayWrapper;
    .locals 3
    .param p1, "src"    # [B
    .param p2, "start"    # I
    .param p3, "limit"    # I

    .prologue
    .line 168
    sub-int v0, p3, p2

    .line 169
    .local v0, "len":I
    iget v1, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    add-int/2addr v1, v0

    invoke-virtual {p0, v1}, Lcom/ibm/icu/util/ByteArrayWrapper;->ensureCapacity(I)Lcom/ibm/icu/util/ByteArrayWrapper;

    .line 170
    iget-object v1, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    iget v2, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    invoke-static {p1, p2, v1, v2, v0}, Lcom/ibm/icu/util/ByteArrayWrapper;->copyBytes([BI[BII)V

    .line 171
    iget v1, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    .line 172
    return-object p0
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 255
    if-ne p0, p1, :cond_0

    const/4 v3, 0x0

    .line 263
    :goto_0
    return v3

    :cond_0
    move-object v2, p1

    .line 256
    check-cast v2, Lcom/ibm/icu/util/ByteArrayWrapper;

    .line 257
    .local v2, "that":Lcom/ibm/icu/util/ByteArrayWrapper;
    iget v3, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    iget v4, v2, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    if-ge v3, v4, :cond_1

    iget v1, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    .line 258
    .local v1, "minSize":I
    :goto_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    if-ge v0, v1, :cond_3

    .line 259
    iget-object v3, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    aget-byte v3, v3, v0

    iget-object v4, v2, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    aget-byte v4, v4, v0

    if-eq v3, v4, :cond_2

    .line 260
    iget-object v3, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    aget-byte v3, v3, v0

    and-int/lit16 v3, v3, 0xff

    iget-object v4, v2, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    aget-byte v4, v4, v0

    and-int/lit16 v4, v4, 0xff

    sub-int/2addr v3, v4

    goto :goto_0

    .line 257
    .end local v0    # "i":I
    .end local v1    # "minSize":I
    :cond_1
    iget v1, v2, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    goto :goto_1

    .line 258
    .restart local v0    # "i":I
    .restart local v1    # "minSize":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 263
    :cond_3
    iget v3, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    iget v4, v2, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    sub-int/2addr v3, v4

    goto :goto_0
.end method

.method public ensureCapacity(I)Lcom/ibm/icu/util/ByteArrayWrapper;
    .locals 4
    .param p1, "capacity"    # I

    .prologue
    const/4 v3, 0x0

    .line 120
    iget-object v1, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    array-length v1, v1

    if-ge v1, p1, :cond_1

    .line 121
    :cond_0
    new-array v0, p1, [B

    .line 122
    .local v0, "newbytes":[B
    iget-object v1, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    iget v2, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    invoke-static {v1, v3, v0, v3, v2}, Lcom/ibm/icu/util/ByteArrayWrapper;->copyBytes([BI[BII)V

    .line 123
    iput-object v0, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    .line 125
    .end local v0    # "newbytes":[B
    :cond_1
    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 218
    if-ne p0, p1, :cond_1

    .line 230
    :cond_0
    :goto_0
    return v3

    .line 219
    :cond_1
    if-nez p1, :cond_2

    move v3, v4

    goto :goto_0

    .line 221
    :cond_2
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/ibm/icu/util/ByteArrayWrapper;

    move-object v2, v0

    .line 222
    .local v2, "that":Lcom/ibm/icu/util/ByteArrayWrapper;
    iget v5, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    iget v6, v2, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    if-eq v5, v6, :cond_3

    move v3, v4

    goto :goto_0

    .line 223
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v5, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    if-ge v1, v5, :cond_0

    .line 224
    iget-object v5, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    aget-byte v5, v5, v1

    iget-object v6, v2, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    aget-byte v6, v6, v1
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v5, v6, :cond_4

    move v3, v4

    goto :goto_0

    .line 223
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 228
    .end local v1    # "i":I
    .end local v2    # "that":Lcom/ibm/icu/util/ByteArrayWrapper;
    :catch_0
    move-exception v3

    move v3, v4

    .line 230
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 239
    iget-object v2, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    array-length v1, v2

    .line 240
    .local v1, "result":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    if-ge v0, v2, :cond_0

    .line 241
    mul-int/lit8 v2, v1, 0x25

    iget-object v3, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    aget-byte v3, v3, v0

    add-int v1, v2, v3

    .line 240
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 243
    :cond_0
    return v1
.end method

.method public final releaseBytes()[B
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    .line 191
    .local v0, "result":[B
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    .line 192
    const/4 v1, 0x0

    iput v1, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    .line 193
    return-object v0
.end method

.method public final set([BII)Lcom/ibm/icu/util/ByteArrayWrapper;
    .locals 1
    .param p1, "src"    # [B
    .param p2, "start"    # I
    .param p3, "limit"    # I

    .prologue
    .line 141
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    .line 142
    invoke-virtual {p0, p1, p2, p3}, Lcom/ibm/icu/util/ByteArrayWrapper;->append([BII)Lcom/ibm/icu/util/ByteArrayWrapper;

    .line 143
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 203
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 204
    .local v1, "result":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->size:I

    if-ge v0, v2, :cond_1

    .line 205
    if-eqz v0, :cond_0

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 206
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/util/ByteArrayWrapper;->bytes:[B

    aget-byte v2, v2, v0

    and-int/lit16 v2, v2, 0xff

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 204
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 208
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
