.class Lcom/ibm/icu/lang/UCharacterNameIterator;
.super Ljava/lang/Object;
.source "UCharacterNameIterator.java"

# interfaces
.implements Lcom/ibm/icu/util/ValueIterator;


# static fields
.field private static GROUP_LENGTHS_:[C

.field private static GROUP_OFFSETS_:[C


# instance fields
.field private m_algorithmIndex_:I

.field private m_choice_:I

.field private m_current_:I

.field private m_groupIndex_:I

.field private m_limit_:I

.field private m_name_:Lcom/ibm/icu/impl/UCharacterName;

.field private m_start_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x21

    .line 208
    new-array v0, v1, [C

    sput-object v0, Lcom/ibm/icu/lang/UCharacterNameIterator;->GROUP_OFFSETS_:[C

    .line 210
    new-array v0, v1, [C

    sput-object v0, Lcom/ibm/icu/lang/UCharacterNameIterator;->GROUP_LENGTHS_:[C

    return-void
.end method

.method protected constructor <init>(Lcom/ibm/icu/impl/UCharacterName;I)V
    .locals 2
    .param p1, "name"    # Lcom/ibm/icu/impl/UCharacterName;
    .param p2, "choice"    # I

    .prologue
    const/4 v0, -0x1

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    iput v0, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_groupIndex_:I

    .line 204
    iput v0, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_algorithmIndex_:I

    .line 164
    if-nez p1, :cond_0

    .line 165
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "UCharacterName name argument cannot be null. Missing unames.icu?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_0
    iput-object p1, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_name_:Lcom/ibm/icu/impl/UCharacterName;

    .line 169
    iput p2, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_choice_:I

    .line 170
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_start_:I

    .line 171
    const/high16 v0, 0x110000

    iput v0, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_limit_:I

    .line 172
    iget v0, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_start_:I

    iput v0, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    .line 173
    return-void
.end method

.method private iterateExtended(Lcom/ibm/icu/util/ValueIterator$Element;I)Z
    .locals 3
    .param p1, "result"    # Lcom/ibm/icu/util/ValueIterator$Element;
    .param p2, "limit"    # I

    .prologue
    .line 318
    :goto_0
    iget v1, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    if-ge v1, p2, :cond_1

    .line 319
    iget-object v1, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_name_:Lcom/ibm/icu/impl/UCharacterName;

    iget v2, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    invoke-virtual {v1, v2}, Lcom/ibm/icu/impl/UCharacterName;->getExtendedOr10Name(I)Ljava/lang/String;

    move-result-object v0

    .line 320
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 321
    iget v1, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    iput v1, p1, Lcom/ibm/icu/util/ValueIterator$Element;->integer:I

    .line 322
    iput-object v0, p1, Lcom/ibm/icu/util/ValueIterator$Element;->value:Ljava/lang/Object;

    .line 323
    const/4 v1, 0x0

    .line 327
    .end local v0    # "name":Ljava/lang/String;
    :goto_1
    return v1

    .line 325
    .restart local v0    # "name":Ljava/lang/String;
    :cond_0
    iget v1, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    goto :goto_0

    .line 327
    .end local v0    # "name":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private iterateGroup(Lcom/ibm/icu/util/ValueIterator$Element;I)Z
    .locals 6
    .param p1, "result"    # Lcom/ibm/icu/util/ValueIterator$Element;
    .param p2, "limit"    # I

    .prologue
    const/4 v3, 0x0

    .line 262
    iget v4, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_groupIndex_:I

    if-gez v4, :cond_0

    .line 263
    iget-object v4, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_name_:Lcom/ibm/icu/impl/UCharacterName;

    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    invoke-virtual {v4, v5}, Lcom/ibm/icu/impl/UCharacterName;->getGroup(I)I

    move-result v4

    iput v4, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_groupIndex_:I

    .line 267
    :cond_0
    :goto_0
    iget v4, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_groupIndex_:I

    iget-object v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_name_:Lcom/ibm/icu/impl/UCharacterName;

    iget v5, v5, Lcom/ibm/icu/impl/UCharacterName;->m_groupcount_:I

    if-ge v4, v5, :cond_7

    iget v4, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    if-ge v4, p2, :cond_7

    .line 269
    iget v4, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    invoke-static {v4}, Lcom/ibm/icu/impl/UCharacterName;->getCodepointMSB(I)I

    move-result v2

    .line 270
    .local v2, "startMSB":I
    iget-object v4, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_name_:Lcom/ibm/icu/impl/UCharacterName;

    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_groupIndex_:I

    invoke-virtual {v4, v5}, Lcom/ibm/icu/impl/UCharacterName;->getGroupMSB(I)I

    move-result v1

    .line 271
    .local v1, "gMSB":I
    if-ne v2, v1, :cond_3

    .line 272
    add-int/lit8 v4, p2, -0x1

    invoke-static {v4}, Lcom/ibm/icu/impl/UCharacterName;->getCodepointMSB(I)I

    move-result v4

    if-ne v2, v4, :cond_2

    .line 275
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/lang/UCharacterNameIterator;->iterateSingleGroup(Lcom/ibm/icu/util/ValueIterator$Element;I)Z

    move-result v3

    .line 304
    .end local v1    # "gMSB":I
    .end local v2    # "startMSB":I
    :cond_1
    :goto_1
    return v3

    .line 279
    .restart local v1    # "gMSB":I
    .restart local v2    # "startMSB":I
    :cond_2
    invoke-static {v1}, Lcom/ibm/icu/impl/UCharacterName;->getGroupLimit(I)I

    move-result v4

    invoke-direct {p0, p1, v4}, Lcom/ibm/icu/lang/UCharacterNameIterator;->iterateSingleGroup(Lcom/ibm/icu/util/ValueIterator$Element;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 283
    iget v4, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_groupIndex_:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_groupIndex_:I

    goto :goto_0

    .line 285
    :cond_3
    if-le v2, v1, :cond_4

    .line 288
    iget v4, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_groupIndex_:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_groupIndex_:I

    goto :goto_0

    .line 291
    :cond_4
    invoke-static {v1}, Lcom/ibm/icu/impl/UCharacterName;->getGroupMin(I)I

    move-result v0

    .line 292
    .local v0, "gMIN":I
    if-le v0, p2, :cond_5

    .line 293
    move v0, p2

    .line 295
    :cond_5
    iget v4, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_choice_:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_6

    .line 296
    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/lang/UCharacterNameIterator;->iterateExtended(Lcom/ibm/icu/util/ValueIterator$Element;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 300
    :cond_6
    iput v0, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    goto :goto_0

    .line 304
    .end local v0    # "gMIN":I
    .end local v1    # "gMSB":I
    .end local v2    # "startMSB":I
    :cond_7
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private iterateSingleGroup(Lcom/ibm/icu/util/ValueIterator$Element;I)Z
    .locals 9
    .param p1, "result"    # Lcom/ibm/icu/util/ValueIterator$Element;
    .param p2, "limit"    # I

    .prologue
    .line 226
    sget-object v4, Lcom/ibm/icu/lang/UCharacterNameIterator;->GROUP_OFFSETS_:[C

    monitor-enter v4

    .line 227
    :try_start_0
    sget-object v5, Lcom/ibm/icu/lang/UCharacterNameIterator;->GROUP_LENGTHS_:[C

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 228
    :try_start_1
    iget-object v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_name_:Lcom/ibm/icu/impl/UCharacterName;

    iget v6, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_groupIndex_:I

    sget-object v7, Lcom/ibm/icu/lang/UCharacterNameIterator;->GROUP_OFFSETS_:[C

    sget-object v8, Lcom/ibm/icu/lang/UCharacterNameIterator;->GROUP_LENGTHS_:[C

    invoke-virtual {v3, v6, v7, v8}, Lcom/ibm/icu/impl/UCharacterName;->getGroupLengths(I[C[C)I

    move-result v0

    .line 230
    .local v0, "index":I
    :goto_0
    iget v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    if-ge v3, p2, :cond_3

    .line 231
    iget v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    invoke-static {v3}, Lcom/ibm/icu/impl/UCharacterName;->getGroupOffset(I)I

    move-result v2

    .line 232
    .local v2, "offset":I
    iget-object v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_name_:Lcom/ibm/icu/impl/UCharacterName;

    sget-object v6, Lcom/ibm/icu/lang/UCharacterNameIterator;->GROUP_OFFSETS_:[C

    aget-char v6, v6, v2

    add-int/2addr v6, v0

    sget-object v7, Lcom/ibm/icu/lang/UCharacterNameIterator;->GROUP_LENGTHS_:[C

    aget-char v7, v7, v2

    iget v8, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_choice_:I

    invoke-virtual {v3, v6, v7, v8}, Lcom/ibm/icu/impl/UCharacterName;->getGroupName(III)Ljava/lang/String;

    move-result-object v1

    .line 235
    .local v1, "name":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    iget v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_choice_:I

    const/4 v6, 0x2

    if-ne v3, v6, :cond_1

    .line 237
    iget-object v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_name_:Lcom/ibm/icu/impl/UCharacterName;

    iget v6, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    invoke-virtual {v3, v6}, Lcom/ibm/icu/impl/UCharacterName;->getExtendedName(I)Ljava/lang/String;

    move-result-object v1

    .line 239
    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 240
    iget v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    iput v3, p1, Lcom/ibm/icu/util/ValueIterator$Element;->integer:I

    .line 241
    iput-object v1, p1, Lcom/ibm/icu/util/ValueIterator$Element;->value:Ljava/lang/Object;

    .line 242
    const/4 v3, 0x0

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 248
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "offset":I
    :goto_1
    return v3

    .line 244
    .restart local v1    # "name":Ljava/lang/String;
    .restart local v2    # "offset":I
    :cond_2
    :try_start_3
    iget v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    goto :goto_0

    .line 246
    .end local v0    # "index":I
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "offset":I
    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v3

    .line 247
    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v3

    .line 246
    .restart local v0    # "index":I
    :cond_3
    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 247
    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 248
    const/4 v3, 0x1

    goto :goto_1
.end method


# virtual methods
.method public next(Lcom/ibm/icu/util/ValueIterator$Element;)Z
    .locals 7
    .param p1, "element"    # Lcom/ibm/icu/util/ValueIterator$Element;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 37
    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    iget v6, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_limit_:I

    if-lt v5, v6, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v3

    .line 41
    :cond_1
    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_choice_:I

    if-eq v5, v4, :cond_6

    .line 42
    iget-object v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_name_:Lcom/ibm/icu/impl/UCharacterName;

    invoke-virtual {v5}, Lcom/ibm/icu/impl/UCharacterName;->getAlgorithmLength()I

    move-result v1

    .line 43
    .local v1, "length":I
    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_algorithmIndex_:I

    if-ge v5, v1, :cond_6

    .line 44
    :goto_1
    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_algorithmIndex_:I

    if-ge v5, v1, :cond_3

    .line 46
    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_algorithmIndex_:I

    if-ltz v5, :cond_2

    iget-object v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_name_:Lcom/ibm/icu/impl/UCharacterName;

    iget v6, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_algorithmIndex_:I

    invoke-virtual {v5, v6}, Lcom/ibm/icu/impl/UCharacterName;->getAlgorithmEnd(I)I

    move-result v5

    iget v6, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    if-ge v5, v6, :cond_3

    .line 49
    :cond_2
    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_algorithmIndex_:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_algorithmIndex_:I

    goto :goto_1

    .line 56
    :cond_3
    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_algorithmIndex_:I

    if-ge v5, v1, :cond_6

    .line 60
    iget-object v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_name_:Lcom/ibm/icu/impl/UCharacterName;

    iget v6, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_algorithmIndex_:I

    invoke-virtual {v5, v6}, Lcom/ibm/icu/impl/UCharacterName;->getAlgorithmStart(I)I

    move-result v2

    .line 61
    .local v2, "start":I
    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    if-ge v5, v2, :cond_5

    .line 64
    move v0, v2

    .line 65
    .local v0, "end":I
    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_limit_:I

    if-gt v5, v2, :cond_4

    .line 66
    iget v0, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_limit_:I

    .line 68
    :cond_4
    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/lang/UCharacterNameIterator;->iterateGroup(Lcom/ibm/icu/util/ValueIterator$Element;I)Z

    move-result v5

    if-nez v5, :cond_5

    .line 69
    iget v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    move v3, v4

    .line 70
    goto :goto_0

    .line 74
    .end local v0    # "end":I
    :cond_5
    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    iget v6, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_limit_:I

    if-ge v5, v6, :cond_0

    .line 80
    iget v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    iput v3, p1, Lcom/ibm/icu/util/ValueIterator$Element;->integer:I

    .line 81
    iget-object v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_name_:Lcom/ibm/icu/impl/UCharacterName;

    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_algorithmIndex_:I

    iget v6, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    invoke-virtual {v3, v5, v6}, Lcom/ibm/icu/impl/UCharacterName;->getAlgorithmName(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/ibm/icu/util/ValueIterator$Element;->value:Ljava/lang/Object;

    .line 84
    const/4 v3, -0x1

    iput v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_groupIndex_:I

    .line 85
    iget v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    move v3, v4

    .line 86
    goto :goto_0

    .line 91
    .end local v1    # "length":I
    .end local v2    # "start":I
    :cond_6
    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_limit_:I

    invoke-direct {p0, p1, v5}, Lcom/ibm/icu/lang/UCharacterNameIterator;->iterateGroup(Lcom/ibm/icu/util/ValueIterator$Element;I)Z

    move-result v5

    if-nez v5, :cond_7

    .line 92
    iget v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    move v3, v4

    .line 93
    goto :goto_0

    .line 95
    :cond_7
    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_choice_:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 96
    iget v5, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_limit_:I

    invoke-direct {p0, p1, v5}, Lcom/ibm/icu/lang/UCharacterNameIterator;->iterateExtended(Lcom/ibm/icu/util/ValueIterator$Element;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 97
    iget v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    move v3, v4

    .line 98
    goto/16 :goto_0
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 112
    iget v0, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_start_:I

    iput v0, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    .line 113
    iput v1, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_groupIndex_:I

    .line 114
    iput v1, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_algorithmIndex_:I

    .line 115
    return-void
.end method

.method public setRange(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    const/high16 v1, 0x110000

    .line 134
    if-lt p1, p2, :cond_0

    .line 135
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "start or limit has to be valid Unicode codepoints and start < limit"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_0
    if-gez p1, :cond_1

    .line 139
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_start_:I

    .line 145
    :goto_0
    if-le p2, v1, :cond_2

    .line 146
    iput v1, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_limit_:I

    .line 151
    :goto_1
    iget v0, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_start_:I

    iput v0, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_current_:I

    .line 152
    return-void

    .line 142
    :cond_1
    iput p1, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_start_:I

    goto :goto_0

    .line 149
    :cond_2
    iput p2, p0, Lcom/ibm/icu/lang/UCharacterNameIterator;->m_limit_:I

    goto :goto_1
.end method
