.class Lcom/ibm/icu/lang/UCharacter$StringContextIterator;
.super Ljava/lang/Object;
.source "UCharacter.java"

# interfaces
.implements Lcom/ibm/icu/impl/UCaseProps$ContextIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/lang/UCharacter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StringContextIterator"
.end annotation


# instance fields
.field protected cpLimit:I

.field protected cpStart:I

.field protected dir:I

.field protected index:I

.field protected limit:I

.field protected s:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 4338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4339
    iput-object p1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->s:Ljava/lang/String;

    .line 4340
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->limit:I

    .line 4341
    iput v1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->index:I

    iput v1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpLimit:I

    iput v1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpStart:I

    .line 4342
    iput v1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->dir:I

    .line 4343
    return-void
.end method


# virtual methods
.method public getCPLimit()I
    .locals 1

    .prologue
    .line 4418
    iget v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpLimit:I

    return v0
.end method

.method public getCPStart()I
    .locals 1

    .prologue
    .line 4410
    iget v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpStart:I

    return v0
.end method

.method public moveToLimit()V
    .locals 1

    .prologue
    .line 4367
    iget v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->limit:I

    iput v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpLimit:I

    iput v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpStart:I

    .line 4368
    return-void
.end method

.method public next()I
    .locals 3

    .prologue
    .line 4441
    iget v1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->dir:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->index:I

    iget-object v2, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->s:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 4442
    iget-object v1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->s:Ljava/lang/String;

    iget v2, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->index:I

    invoke-static {v1, v2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 4443
    .local v0, "c":I
    iget v1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->index:I

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->index:I

    .line 4450
    .end local v0    # "c":I
    :goto_0
    return v0

    .line 4445
    :cond_0
    iget v1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->dir:I

    if-gez v1, :cond_1

    iget v1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->index:I

    if-lez v1, :cond_1

    .line 4446
    iget-object v1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->s:Ljava/lang/String;

    iget v2, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->index:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 4447
    .restart local v0    # "c":I
    iget v1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->index:I

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->index:I

    goto :goto_0

    .line 4450
    .end local v0    # "c":I
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public nextCaseMapCP()I
    .locals 6

    .prologue
    const v5, 0xdfff

    .line 4384
    iget v2, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpLimit:I

    iput v2, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpStart:I

    .line 4385
    iget v2, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpLimit:I

    iget v3, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->limit:I

    if-ge v2, v3, :cond_2

    .line 4386
    iget-object v2, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->s:Ljava/lang/String;

    iget v3, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpLimit:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpLimit:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 4387
    .local v0, "c":I
    const v2, 0xd800

    if-le v2, v0, :cond_0

    if-gt v0, v5, :cond_1

    .line 4389
    :cond_0
    const v2, 0xdbff

    if-gt v0, v2, :cond_1

    iget v2, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpLimit:I

    iget v3, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->limit:I

    if-ge v2, v3, :cond_1

    const v2, 0xdc00

    iget-object v3, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->s:Ljava/lang/String;

    iget v4, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpLimit:I

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .local v1, "c2":C
    if-gt v2, v1, :cond_1

    if-gt v1, v5, :cond_1

    .line 4393
    iget v2, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpLimit:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpLimit:I

    .line 4394
    int-to-char v2, v0

    invoke-static {v2, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v0

    .line 4401
    .end local v0    # "c":I
    .end local v1    # "c2":C
    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public reset(I)V
    .locals 1
    .param p1, "direction"    # I

    .prologue
    const/4 v0, 0x0

    .line 4423
    if-lez p1, :cond_0

    .line 4425
    const/4 v0, 0x1

    iput v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->dir:I

    .line 4426
    iget v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpLimit:I

    iput v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->index:I

    .line 4436
    :goto_0
    return-void

    .line 4427
    :cond_0
    if-gez p1, :cond_1

    .line 4429
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->dir:I

    .line 4430
    iget v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->cpStart:I

    iput v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->index:I

    goto :goto_0

    .line 4433
    :cond_1
    iput v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->dir:I

    .line 4434
    iput v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->index:I

    goto :goto_0
.end method

.method public setLimit(I)V
    .locals 1
    .param p1, "lim"    # I

    .prologue
    .line 4356
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->s:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gt p1, v0, :cond_0

    .line 4357
    iput p1, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->limit:I

    .line 4361
    :goto_0
    return-void

    .line 4359
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->s:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->limit:I

    goto :goto_0
.end method
