.class public final Lcom/ibm/icu/lang/UCharacter;
.super Ljava/lang/Object;
.source "UCharacter.java"

# interfaces
.implements Lcom/ibm/icu/lang/UCharacterEnums$ECharacterCategory;
.implements Lcom/ibm/icu/lang/UCharacterEnums$ECharacterDirection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/lang/UCharacter$StringContextIterator;,
        Lcom/ibm/icu/lang/UCharacter$HangulSyllableType;,
        Lcom/ibm/icu/lang/UCharacter$NumericType;,
        Lcom/ibm/icu/lang/UCharacter$LineBreak;,
        Lcom/ibm/icu/lang/UCharacter$SentenceBreak;,
        Lcom/ibm/icu/lang/UCharacter$WordBreak;,
        Lcom/ibm/icu/lang/UCharacter$GraphemeClusterBreak;,
        Lcom/ibm/icu/lang/UCharacter$JoiningGroup;,
        Lcom/ibm/icu/lang/UCharacter$JoiningType;,
        Lcom/ibm/icu/lang/UCharacter$DecompositionType;,
        Lcom/ibm/icu/lang/UCharacter$EastAsianWidth;,
        Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;
    }
.end annotation


# static fields
.field private static final APPLICATION_PROGRAM_COMMAND_:I = 0x9f

.field private static final BLOCK_MASK_:I = 0x1ff00

.field private static final BLOCK_SHIFT_:I = 0x8

.field private static final CJK_IDEOGRAPH_COMPLEX_EIGHT_:I = 0x634c

.field private static final CJK_IDEOGRAPH_COMPLEX_FIVE_:I = 0x4f0d

.field private static final CJK_IDEOGRAPH_COMPLEX_FOUR_:I = 0x8086

.field private static final CJK_IDEOGRAPH_COMPLEX_HUNDRED_:I = 0x4f70

.field private static final CJK_IDEOGRAPH_COMPLEX_NINE_:I = 0x7396

.field private static final CJK_IDEOGRAPH_COMPLEX_ONE_:I = 0x58f9

.field private static final CJK_IDEOGRAPH_COMPLEX_SEVEN_:I = 0x67d2

.field private static final CJK_IDEOGRAPH_COMPLEX_SIX_:I = 0x9678

.field private static final CJK_IDEOGRAPH_COMPLEX_TEN_:I = 0x62fe

.field private static final CJK_IDEOGRAPH_COMPLEX_THOUSAND_:I = 0x4edf

.field private static final CJK_IDEOGRAPH_COMPLEX_THREE_:I = 0x53c3

.field private static final CJK_IDEOGRAPH_COMPLEX_TWO_:I = 0x8cb3

.field private static final CJK_IDEOGRAPH_COMPLEX_ZERO_:I = 0x96f6

.field private static final CJK_IDEOGRAPH_EIGHTH_:I = 0x516b

.field private static final CJK_IDEOGRAPH_FIFTH_:I = 0x4e94

.field private static final CJK_IDEOGRAPH_FIRST_:I = 0x4e00

.field private static final CJK_IDEOGRAPH_FOURTH_:I = 0x56d8

.field private static final CJK_IDEOGRAPH_HUNDRED_:I = 0x767e

.field private static final CJK_IDEOGRAPH_HUNDRED_MILLION_:I = 0x5104

.field private static final CJK_IDEOGRAPH_NINETH_:I = 0x4e5d

.field private static final CJK_IDEOGRAPH_SECOND_:I = 0x4e8c

.field private static final CJK_IDEOGRAPH_SEVENTH_:I = 0x4e03

.field private static final CJK_IDEOGRAPH_SIXTH_:I = 0x516d

.field private static final CJK_IDEOGRAPH_TEN_:I = 0x5341

.field private static final CJK_IDEOGRAPH_TEN_THOUSAND_:I = 0x824c

.field private static final CJK_IDEOGRAPH_THIRD_:I = 0x4e09

.field private static final CJK_IDEOGRAPH_THOUSAND_:I = 0x5343

.field private static final DECOMPOSITION_TYPE_MASK_:I = 0x1f

.field private static final DELETE_:I = 0x7f

.field private static final EAST_ASIAN_MASK_:I = 0xe0000

.field private static final EAST_ASIAN_SHIFT_:I = 0x11

.field public static final FOLD_CASE_DEFAULT:I = 0x0

.field public static final FOLD_CASE_EXCLUDE_SPECIAL_I:I = 0x1

.field private static final FRACTION_DEN_MASK:I = 0x7

.field private static final FRACTION_DEN_OFFSET:I = 0x2

.field private static final FRACTION_NUM_SHIFT:I = 0x3

.field private static final GCB_MASK:I = 0x3e0

.field private static final GCB_SHIFT:I = 0x5

.field private static final IDEOGRAPHIC_NUMBER_ZERO_:I = 0x3007

.field private static final LARGE_EXP_MASK:I = 0xf

.field private static final LARGE_EXP_OFFSET:I = 0x2

.field private static final LARGE_EXP_OFFSET_EXTRA:I = 0x12

.field private static final LARGE_MANT_SHIFT:I = 0x4

.field private static final LAST_CHAR_MASK_:I = 0xffff

.field private static final LB_MASK:I = 0x3f00000

.field private static final LB_SHIFT:I = 0x14

.field private static final LB_VWORD:I = 0x2

.field public static final MAX_CODE_POINT:I = 0x10ffff

.field public static final MAX_HIGH_SURROGATE:C = '\udbff'

.field public static final MAX_LOW_SURROGATE:C = '\udfff'

.field public static final MAX_RADIX:I = 0x24

.field public static final MAX_SURROGATE:C = '\udfff'

.field public static final MAX_VALUE:I = 0x10ffff

.field public static final MIN_CODE_POINT:I = 0x0

.field public static final MIN_HIGH_SURROGATE:C = '\ud800'

.field public static final MIN_LOW_SURROGATE:C = '\udc00'

.field public static final MIN_RADIX:I = 0x2

.field public static final MIN_SUPPLEMENTARY_CODE_POINT:I = 0x10000

.field public static final MIN_SURROGATE:C = '\ud800'

.field public static final MIN_VALUE:I = 0x0

.field static NAME_:Lcom/ibm/icu/impl/UCharacterName; = null

.field private static final NARROW_NO_BREAK_SPACE_:I = 0x202f

.field private static final NO_BREAK_SPACE_:I = 0xa0

.field public static final NO_NUMERIC_VALUE:D = -1.23456789E8

.field private static final NUMERIC_TYPE_MASK_:I = 0xe0

.field private static final NUMERIC_TYPE_SHIFT_:I = 0x5

.field static PNAMES_:Lcom/ibm/icu/impl/UPropertyAliases; = null

.field private static final PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

.field private static final PROPERTY_INITIAL_VALUE_:I

.field private static final PROPERTY_TRIE_DATA_:[C

.field private static final PROPERTY_TRIE_INDEX_:[C

.field public static final REPLACEMENT_CHAR:I = 0xfffd

.field private static final SB_MASK:I = 0xf8000

.field private static final SB_SHIFT:I = 0xf

.field private static final SCRIPT_MASK_:I = 0xff

.field public static final SUPPLEMENTARY_MIN_VALUE:I = 0x10000

.field public static final TITLECASE_NO_BREAK_ADJUSTMENT:I = 0x200

.field public static final TITLECASE_NO_LOWERCASE:I = 0x100

.field private static final UNIT_SEPARATOR_:I = 0x1f

.field private static final WB_MASK:I = 0x7c00

.field private static final WB_SHIFT:I = 0xa

.field private static final ZERO_WIDTH_NO_BREAK_SPACE_:I = 0xfeff

.field private static final gBdp:Lcom/ibm/icu/impl/UBiDiProps;

.field private static final gCsp:Lcom/ibm/icu/impl/UCaseProps;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 6154
    sput-object v3, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    .line 6159
    sput-object v3, Lcom/ibm/icu/lang/UCharacter;->PNAMES_:Lcom/ibm/icu/impl/UPropertyAliases;

    .line 6164
    :try_start_0
    new-instance v3, Lcom/ibm/icu/impl/UPropertyAliases;

    invoke-direct {v3}, Lcom/ibm/icu/impl/UPropertyAliases;-><init>()V

    sput-object v3, Lcom/ibm/icu/lang/UCharacter;->PNAMES_:Lcom/ibm/icu/impl/UPropertyAliases;

    .line 6165
    invoke-static {}, Lcom/ibm/icu/impl/UCharacterName;->getInstance()Lcom/ibm/icu/impl/UCharacterName;

    move-result-object v3

    sput-object v3, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6197
    :try_start_1
    invoke-static {}, Lcom/ibm/icu/impl/UCharacterProperty;->getInstance()Lcom/ibm/icu/impl/UCharacterProperty;

    move-result-object v3

    sput-object v3, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    .line 6198
    sget-object v3, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    iget-object v3, v3, Lcom/ibm/icu/impl/UCharacterProperty;->m_trieIndex_:[C

    sput-object v3, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_TRIE_INDEX_:[C

    .line 6199
    sget-object v3, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    iget-object v3, v3, Lcom/ibm/icu/impl/UCharacterProperty;->m_trieData_:[C

    sput-object v3, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_TRIE_DATA_:[C

    .line 6200
    sget-object v3, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    iget v3, v3, Lcom/ibm/icu/impl/UCharacterProperty;->m_trieInitialValue_:I

    sput v3, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_INITIAL_VALUE_:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 6234
    :try_start_2
    invoke-static {}, Lcom/ibm/icu/impl/UCaseProps;->getSingleton()Lcom/ibm/icu/impl/UCaseProps;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    .line 6238
    .local v1, "csp":Lcom/ibm/icu/impl/UCaseProps;
    :goto_0
    sput-object v1, Lcom/ibm/icu/lang/UCharacter;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    .line 6242
    :try_start_3
    invoke-static {}, Lcom/ibm/icu/impl/UBiDiProps;->getSingleton()Lcom/ibm/icu/impl/UBiDiProps;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v0

    .line 6246
    .local v0, "bdp":Lcom/ibm/icu/impl/UBiDiProps;
    :goto_1
    sput-object v0, Lcom/ibm/icu/lang/UCharacter;->gBdp:Lcom/ibm/icu/impl/UBiDiProps;

    .line 6247
    return-void

    .line 6166
    .end local v0    # "bdp":Lcom/ibm/icu/impl/UBiDiProps;
    .end local v1    # "csp":Lcom/ibm/icu/impl/UCaseProps;
    :catch_0
    move-exception v2

    .line 6168
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Ljava/util/MissingResourceException;

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, ""

    const-string/jumbo v6, ""

    invoke-direct {v3, v4, v5, v6}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 6202
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 6204
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/util/MissingResourceException;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, ""

    const-string/jumbo v6, ""

    invoke-direct {v3, v4, v5, v6}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 6235
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v2

    .line 6236
    .local v2, "e":Ljava/io/IOException;
    invoke-static {}, Lcom/ibm/icu/impl/UCaseProps;->getDummy()Lcom/ibm/icu/impl/UCaseProps;

    move-result-object v1

    .restart local v1    # "csp":Lcom/ibm/icu/impl/UCaseProps;
    goto :goto_0

    .line 6243
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 6244
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-static {}, Lcom/ibm/icu/impl/UBiDiProps;->getDummy()Lcom/ibm/icu/impl/UBiDiProps;

    move-result-object v0

    .restart local v0    # "bdp":Lcom/ibm/icu/impl/UBiDiProps;
    goto :goto_1
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 6491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6492
    return-void
.end method

.method static access$000()Lcom/ibm/icu/impl/UCharacterProperty;
    .locals 1

    .prologue
    .line 159
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    return-object v0
.end method

.method public static charCount(I)I
    .locals 1
    .param p0, "cp"    # I

    .prologue
    .line 5634
    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v0

    return v0
.end method

.method public static final codePointAt(Ljava/lang/CharSequence;I)I
    .locals 4
    .param p0, "seq"    # Ljava/lang/CharSequence;
    .param p1, "index"    # I

    .prologue
    .line 5684
    add-int/lit8 v2, p1, 0x1

    .end local p1    # "index":I
    .local v2, "index":I
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 5685
    .local v0, "c1":C
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->isHighSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5686
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 5687
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 5688
    .local v1, "c2":C
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->isLowSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5689
    invoke-static {v0, v1}, Lcom/ibm/icu/lang/UCharacter;->toCodePoint(CC)I

    move-result v0

    .line 5693
    .end local v0    # "c1":C
    .end local v1    # "c2":C
    :cond_0
    return v0
.end method

.method public static final codePointAt([CI)I
    .locals 4
    .param p0, "text"    # [C
    .param p1, "index"    # I

    .prologue
    .line 5707
    add-int/lit8 v2, p1, 0x1

    .end local p1    # "index":I
    .local v2, "index":I
    aget-char v0, p0, p1

    .line 5708
    .local v0, "c1":C
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->isHighSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5709
    array-length v3, p0

    if-ge v2, v3, :cond_0

    .line 5710
    aget-char v1, p0, v2

    .line 5711
    .local v1, "c2":C
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->isLowSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5712
    invoke-static {v0, v1}, Lcom/ibm/icu/lang/UCharacter;->toCodePoint(CC)I

    move-result v0

    .line 5716
    .end local v0    # "c1":C
    .end local v1    # "c2":C
    :cond_0
    return v0
.end method

.method public static final codePointAt([CII)I
    .locals 4
    .param p0, "text"    # [C
    .param p1, "index"    # I
    .param p2, "limit"    # I

    .prologue
    .line 5730
    if-ge p1, p2, :cond_0

    array-length v3, p0

    if-le p2, v3, :cond_1

    .line 5731
    :cond_0
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v3}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v3

    .line 5733
    :cond_1
    add-int/lit8 v2, p1, 0x1

    .end local p1    # "index":I
    .local v2, "index":I
    aget-char v0, p0, p1

    .line 5734
    .local v0, "c1":C
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->isHighSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 5735
    if-ge v2, p2, :cond_2

    .line 5736
    aget-char v1, p0, v2

    .line 5737
    .local v1, "c2":C
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->isLowSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 5738
    invoke-static {v0, v1}, Lcom/ibm/icu/lang/UCharacter;->toCodePoint(CC)I

    move-result v0

    .line 5742
    .end local v0    # "c1":C
    .end local v1    # "c2":C
    :cond_2
    return v0
.end method

.method public static final codePointBefore(Ljava/lang/CharSequence;I)I
    .locals 3
    .param p0, "seq"    # Ljava/lang/CharSequence;
    .param p1, "index"    # I

    .prologue
    .line 5780
    add-int/lit8 p1, p1, -0x1

    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 5781
    .local v1, "c2":C
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->isLowSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5782
    if-lez p1, :cond_0

    .line 5783
    add-int/lit8 p1, p1, -0x1

    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 5784
    .local v0, "c1":C
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->isHighSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5785
    invoke-static {v0, v1}, Lcom/ibm/icu/lang/UCharacter;->toCodePoint(CC)I

    move-result v1

    .line 5789
    .end local v0    # "c1":C
    .end local v1    # "c2":C
    :cond_0
    return v1
.end method

.method public static final codePointBefore([CI)I
    .locals 3
    .param p0, "text"    # [C
    .param p1, "index"    # I

    .prologue
    .line 5803
    add-int/lit8 p1, p1, -0x1

    aget-char v1, p0, p1

    .line 5804
    .local v1, "c2":C
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->isLowSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5805
    if-lez p1, :cond_0

    .line 5806
    add-int/lit8 p1, p1, -0x1

    aget-char v0, p0, p1

    .line 5807
    .local v0, "c1":C
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->isHighSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5808
    invoke-static {v0, v1}, Lcom/ibm/icu/lang/UCharacter;->toCodePoint(CC)I

    move-result v1

    .line 5812
    .end local v0    # "c1":C
    .end local v1    # "c2":C
    :cond_0
    return v1
.end method

.method public static final codePointBefore([CII)I
    .locals 3
    .param p0, "text"    # [C
    .param p1, "index"    # I
    .param p2, "limit"    # I

    .prologue
    .line 5826
    if-le p1, p2, :cond_0

    if-gez p2, :cond_1

    .line 5827
    :cond_0
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v2}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v2

    .line 5829
    :cond_1
    add-int/lit8 p1, p1, -0x1

    aget-char v1, p0, p1

    .line 5830
    .local v1, "c2":C
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->isLowSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5831
    if-le p1, p2, :cond_2

    .line 5832
    add-int/lit8 p1, p1, -0x1

    aget-char v0, p0, p1

    .line 5833
    .local v0, "c1":C
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->isHighSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5834
    invoke-static {v0, v1}, Lcom/ibm/icu/lang/UCharacter;->toCodePoint(CC)I

    move-result v1

    .line 5838
    .end local v0    # "c1":C
    .end local v1    # "c2":C
    :cond_2
    return v1
.end method

.method public static codePointCount(Ljava/lang/CharSequence;II)I
    .locals 5
    .param p0, "text"    # Ljava/lang/CharSequence;
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    .line 5948
    if-ltz p1, :cond_0

    if-lt p2, p1, :cond_0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-le p2, v2, :cond_1

    .line 5949
    :cond_0
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "start ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, ") or limit ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, ") invalid or out of range 0, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 5954
    :cond_1
    sub-int v1, p2, p1

    .line 5955
    .local v1, "len":I
    :cond_2
    :goto_0
    if-le p2, p1, :cond_4

    .line 5956
    add-int/lit8 p2, p2, -0x1

    invoke-interface {p0, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 5957
    .local v0, "ch":C
    :cond_3
    const v2, 0xdc00

    if-lt v0, v2, :cond_2

    const v2, 0xdfff

    if-gt v0, v2, :cond_2

    if-le p2, p1, :cond_2

    .line 5958
    add-int/lit8 p2, p2, -0x1

    invoke-interface {p0, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 5959
    const v2, 0xd800

    if-lt v0, v2, :cond_3

    const v2, 0xdbff

    if-gt v0, v2, :cond_3

    .line 5960
    add-int/lit8 v1, v1, -0x1

    .line 5961
    goto :goto_0

    .line 5965
    .end local v0    # "ch":C
    :cond_4
    return v1
.end method

.method public static codePointCount([CII)I
    .locals 5
    .param p0, "text"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    .line 5978
    if-ltz p1, :cond_0

    if-lt p2, p1, :cond_0

    array-length v2, p0

    if-le p2, v2, :cond_1

    .line 5979
    :cond_0
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "start ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, ") or limit ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, ") invalid or out of range 0, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    array-length v4, p0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 5984
    :cond_1
    sub-int v1, p2, p1

    .line 5985
    .local v1, "len":I
    :cond_2
    :goto_0
    if-le p2, p1, :cond_4

    .line 5986
    add-int/lit8 p2, p2, -0x1

    aget-char v0, p0, p2

    .line 5987
    .local v0, "ch":C
    :cond_3
    const v2, 0xdc00

    if-lt v0, v2, :cond_2

    const v2, 0xdfff

    if-gt v0, v2, :cond_2

    if-le p2, p1, :cond_2

    .line 5988
    add-int/lit8 p2, p2, -0x1

    aget-char v0, p0, p2

    .line 5989
    const v2, 0xd800

    if-lt v0, v2, :cond_3

    const v2, 0xdbff

    if-gt v0, v2, :cond_3

    .line 5990
    add-int/lit8 v1, v1, -0x1

    .line 5991
    goto :goto_0

    .line 5995
    .end local v0    # "ch":C
    :cond_4
    return v1
.end method

.method public static digit(I)I
    .locals 3
    .param p0, "ch"    # I

    .prologue
    .line 3084
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getProperty(I)I

    move-result v0

    .line 3085
    .local v0, "props":I
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->getNumericType(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 3086
    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->getUnsignedValue(I)I

    move-result v1

    .line 3088
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public static digit(II)I
    .locals 4
    .param p0, "ch"    # I
    .param p1, "radix"    # I

    .prologue
    .line 3059
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getProperty(I)I

    move-result v0

    .line 3061
    .local v0, "props":I
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->getNumericType(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 3062
    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->getUnsignedValue(I)I

    move-result v1

    .line 3066
    .local v1, "value":I
    :goto_0
    if-ltz v1, :cond_1

    if-ge v1, p1, :cond_1

    .end local v1    # "value":I
    :goto_1
    return v1

    .line 3064
    :cond_0
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getEuropeanDigit(I)I

    move-result v1

    .restart local v1    # "value":I
    goto :goto_0

    .line 3066
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public static foldCase(II)I
    .locals 1
    .param p0, "ch"    # I
    .param p1, "options"    # I

    .prologue
    .line 4896
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    invoke-virtual {v0, p0, p1}, Lcom/ibm/icu/impl/UCaseProps;->fold(II)I

    move-result v0

    return v0
.end method

.method public static foldCase(IZ)I
    .locals 1
    .param p0, "ch"    # I
    .param p1, "defaultmapping"    # Z

    .prologue
    .line 4837
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {p0, v0}, Lcom/ibm/icu/lang/UCharacter;->foldCase(II)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static final foldCase(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "options"    # I

    .prologue
    .line 4915
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 4918
    .local v3, "result":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 4919
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_3

    .line 4920
    invoke-static {p0, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 4921
    .local v0, "c":I
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 4922
    sget-object v4, Lcom/ibm/icu/lang/UCharacter;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    invoke-virtual {v4, v0, v3, p1}, Lcom/ibm/icu/impl/UCaseProps;->toFullFolding(ILjava/lang/StringBuffer;I)I

    move-result v0

    .line 4925
    if-gez v0, :cond_1

    .line 4927
    xor-int/lit8 v0, v0, -0x1

    .line 4933
    :cond_0
    const v4, 0xffff

    if-gt v0, v4, :cond_2

    .line 4934
    int-to-char v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 4928
    :cond_1
    const/16 v4, 0x1f

    if-gt v0, v4, :cond_0

    goto :goto_0

    .line 4936
    :cond_2
    invoke-static {v3, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 4939
    .end local v0    # "c":I
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static foldCase(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "defaultmapping"    # Z

    .prologue
    .line 4858
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {p0, v0}, Lcom/ibm/icu/lang/UCharacter;->foldCase(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static forDigit(II)C
    .locals 1
    .param p0, "digit"    # I
    .param p1, "radix"    # I

    .prologue
    .line 5503
    invoke-static {p0, p1}, Ljava/lang/Character;->forDigit(II)C

    move-result v0

    return v0
.end method

.method public static getAge(I)Lcom/ibm/icu/util/VersionInfo;
    .locals 2
    .param p0, "ch"    # I

    .prologue
    .line 5128
    if-ltz p0, :cond_0

    const v0, 0x10ffff

    if-le p0, v0, :cond_1

    .line 5129
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Codepoint out of bounds"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5131
    :cond_1
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/UCharacterProperty;->getAge(I)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v0

    return-object v0
.end method

.method public static getCharFromExtendedName(Ljava/lang/String;)I
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4117
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    if-nez v0, :cond_0

    .line 4118
    new-instance v0, Ljava/util/MissingResourceException;

    const-string/jumbo v1, "Could not load unames.icu"

    const-string/jumbo v2, ""

    const-string/jumbo v3, ""

    invoke-direct {v0, v1, v2, v3}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 4120
    :cond_0
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p0}, Lcom/ibm/icu/impl/UCharacterName;->getCharFromName(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getCharFromName(Ljava/lang/String;)I
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4070
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    if-nez v0, :cond_0

    .line 4071
    new-instance v0, Ljava/util/MissingResourceException;

    const-string/jumbo v1, "Could not load unames.icu"

    const-string/jumbo v2, ""

    const-string/jumbo v3, ""

    invoke-direct {v0, v1, v2, v3}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 4073
    :cond_0
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Lcom/ibm/icu/impl/UCharacterName;->getCharFromName(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getCharFromName1_0(Ljava/lang/String;)I
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4089
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    if-nez v0, :cond_0

    .line 4090
    new-instance v0, Ljava/util/MissingResourceException;

    const-string/jumbo v1, "Could not load unames.icu"

    const-string/jumbo v2, ""

    const-string/jumbo v3, ""

    invoke-direct {v0, v1, v2, v3}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 4092
    :cond_0
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/ibm/icu/impl/UCharacterName;->getCharFromName(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getCodePoint(C)I
    .locals 2
    .param p0, "char16"    # C

    .prologue
    .line 4323
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->isLegal(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4324
    return p0

    .line 4326
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Illegal codepoint"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getCodePoint(CC)I
    .locals 2
    .param p0, "lead"    # C
    .param p1, "trail"    # C

    .prologue
    .line 4307
    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4308
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v0

    return v0

    .line 4310
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Illegal surrogate characters"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getCombiningClass(I)I
    .locals 2
    .param p0, "ch"    # I

    .prologue
    .line 3874
    if-ltz p0, :cond_0

    const v0, 0x10ffff

    if-le p0, v0, :cond_1

    .line 3875
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Codepoint out of bounds"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3877
    :cond_1
    invoke-static {p0}, Lcom/ibm/icu/impl/NormalizerImpl;->getCombiningClass(I)I

    move-result v0

    return v0
.end method

.method public static getDirection(I)I
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 3830
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->gBdp:Lcom/ibm/icu/impl/UBiDiProps;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/UBiDiProps;->getClass(I)I

    move-result v0

    return v0
.end method

.method public static getDirectionality(I)B
    .locals 1
    .param p0, "cp"    # I

    .prologue
    .line 5903
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getDirection(I)I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method private static getEuropeanDigit(I)I
    .locals 6
    .param p0, "ch"    # I

    .prologue
    const v5, 0xff21

    const/16 v4, 0x7a

    const/16 v1, 0x61

    const/16 v3, 0x5a

    const/16 v0, 0x41

    .line 6505
    if-le p0, v4, :cond_0

    if-lt p0, v5, :cond_2

    :cond_0
    if-lt p0, v0, :cond_2

    if-le p0, v3, :cond_1

    if-lt p0, v1, :cond_2

    :cond_1
    const v2, 0xff5a

    if-gt p0, v2, :cond_2

    const v2, 0xff31

    if-le p0, v2, :cond_3

    const v2, 0xff41

    if-ge p0, v2, :cond_3

    .line 6508
    :cond_2
    const/4 v0, -0x1

    .line 6519
    :goto_0
    return v0

    .line 6510
    :cond_3
    if-gt p0, v4, :cond_5

    .line 6512
    add-int/lit8 v2, p0, 0xa

    if-gt p0, v3, :cond_4

    :goto_1
    sub-int v0, v2, v0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 6515
    :cond_5
    const v0, 0xff3a

    if-gt p0, v0, :cond_6

    .line 6516
    add-int/lit8 v0, p0, 0xa

    sub-int/2addr v0, v5

    goto :goto_0

    .line 6519
    :cond_6
    add-int/lit8 v0, p0, 0xa

    const v1, 0xff41

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public static getExtendedName(I)Ljava/lang/String;
    .locals 4
    .param p0, "ch"    # I

    .prologue
    .line 4029
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    if-nez v0, :cond_0

    .line 4030
    new-instance v0, Ljava/util/MissingResourceException;

    const-string/jumbo v1, "Could not load unames.icu"

    const-string/jumbo v2, ""

    const-string/jumbo v3, ""

    invoke-direct {v0, v1, v2, v3}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 4032
    :cond_0
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    const/4 v1, 0x2

    invoke-virtual {v0, p0, v1}, Lcom/ibm/icu/impl/UCharacterName;->getName(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getExtendedNameIterator()Lcom/ibm/icu/util/ValueIterator;
    .locals 4

    .prologue
    .line 5107
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    if-nez v0, :cond_0

    .line 5108
    new-instance v0, Ljava/util/MissingResourceException;

    const-string/jumbo v1, "Could not load unames.icu"

    const-string/jumbo v2, ""

    const-string/jumbo v3, ""

    invoke-direct {v0, v1, v2, v3}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 5110
    :cond_0
    new-instance v0, Lcom/ibm/icu/lang/UCharacterNameIterator;

    sget-object v1, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacterNameIterator;-><init>(Lcom/ibm/icu/impl/UCharacterName;I)V

    return-object v0
.end method

.method public static getHanNumericValue(I)I
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 4957
    sparse-switch p0, :sswitch_data_0

    .line 5003
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 4961
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4964
    :sswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 4967
    :sswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 4970
    :sswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 4973
    :sswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 4976
    :sswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 4979
    :sswitch_6
    const/4 v0, 0x6

    goto :goto_0

    .line 4982
    :sswitch_7
    const/4 v0, 0x7

    goto :goto_0

    .line 4985
    :sswitch_8
    const/16 v0, 0x8

    goto :goto_0

    .line 4988
    :sswitch_9
    const/16 v0, 0x9

    goto :goto_0

    .line 4991
    :sswitch_a
    const/16 v0, 0xa

    goto :goto_0

    .line 4994
    :sswitch_b
    const/16 v0, 0x64

    goto :goto_0

    .line 4997
    :sswitch_c
    const/16 v0, 0x3e8

    goto :goto_0

    .line 4999
    :sswitch_d
    const/16 v0, 0x2710

    goto :goto_0

    .line 5001
    :sswitch_e
    const v0, 0x5f5e100

    goto :goto_0

    .line 4957
    nop

    :sswitch_data_0
    .sparse-switch
        0x3007 -> :sswitch_0
        0x4e00 -> :sswitch_1
        0x4e03 -> :sswitch_7
        0x4e09 -> :sswitch_3
        0x4e5d -> :sswitch_9
        0x4e8c -> :sswitch_2
        0x4e94 -> :sswitch_5
        0x4edf -> :sswitch_c
        0x4f0d -> :sswitch_5
        0x4f70 -> :sswitch_b
        0x5104 -> :sswitch_e
        0x516b -> :sswitch_8
        0x516d -> :sswitch_6
        0x5341 -> :sswitch_a
        0x5343 -> :sswitch_c
        0x53c3 -> :sswitch_3
        0x56d8 -> :sswitch_4
        0x58f9 -> :sswitch_1
        0x62fe -> :sswitch_a
        0x634c -> :sswitch_8
        0x67d2 -> :sswitch_7
        0x7396 -> :sswitch_9
        0x767e -> :sswitch_b
        0x8086 -> :sswitch_4
        0x824c -> :sswitch_d
        0x8cb3 -> :sswitch_2
        0x9678 -> :sswitch_6
        0x96f6 -> :sswitch_0
    .end sparse-switch
.end method

.method public static getISOComment(I)Ljava/lang/String;
    .locals 5
    .param p0, "ch"    # I

    .prologue
    .line 4047
    if-ltz p0, :cond_0

    const v1, 0x10ffff

    if-le p0, v1, :cond_1

    .line 4048
    :cond_0
    const/4 v0, 0x0

    .line 4055
    :goto_0
    return-object v0

    .line 4050
    :cond_1
    sget-object v1, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    if-nez v1, :cond_2

    .line 4051
    new-instance v1, Ljava/util/MissingResourceException;

    const-string/jumbo v2, "Could not load unames.icu"

    const-string/jumbo v3, ""

    const-string/jumbo v4, ""

    invoke-direct {v1, v2, v3, v4}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v1

    .line 4053
    :cond_2
    sget-object v1, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    const/4 v2, 0x3

    invoke-virtual {v1, p0, v2}, Lcom/ibm/icu/impl/UCharacterName;->getGroupName(II)Ljava/lang/String;

    move-result-object v0

    .line 4055
    .local v0, "result":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getIntPropertyMaxValue(I)I
    .locals 5
    .param p0, "type"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v0, -0x1

    const/4 v2, 0x2

    .line 5442
    if-gez p0, :cond_1

    .line 5495
    :cond_0
    :goto_0
    return v0

    .line 5445
    :cond_1
    const/16 v3, 0x31

    if-ge p0, v3, :cond_2

    move v0, v1

    .line 5446
    goto :goto_0

    .line 5448
    :cond_2
    const/16 v3, 0x1000

    if-lt p0, v3, :cond_0

    .line 5451
    const/16 v3, 0x1015

    if-ge p0, v3, :cond_0

    .line 5452
    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 5456
    :pswitch_0
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->gBdp:Lcom/ibm/icu/impl/UBiDiProps;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/UBiDiProps;->getMaxValue(I)I

    move-result v0

    goto :goto_0

    .line 5458
    :pswitch_1
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, v4}, Lcom/ibm/icu/impl/UCharacterProperty;->getMaxValues(I)I

    move-result v0

    const v1, 0x1ff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    goto :goto_0

    .line 5462
    :pswitch_2
    const/16 v0, 0xff

    goto :goto_0

    .line 5465
    :pswitch_3
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, v2}, Lcom/ibm/icu/impl/UCharacterProperty;->getMaxValues(I)I

    move-result v0

    and-int/lit8 v0, v0, 0x1f

    goto :goto_0

    .line 5467
    :pswitch_4
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, v4}, Lcom/ibm/icu/impl/UCharacterProperty;->getMaxValues(I)I

    move-result v0

    const/high16 v1, 0xe0000

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x11

    goto :goto_0

    .line 5469
    :pswitch_5
    const/16 v0, 0x1d

    goto :goto_0

    .line 5471
    :pswitch_6
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, v2}, Lcom/ibm/icu/impl/UCharacterProperty;->getMaxValues(I)I

    move-result v0

    const/high16 v1, 0x3f00000

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x14

    goto :goto_0

    .line 5473
    :pswitch_7
    const/4 v0, 0x3

    goto :goto_0

    .line 5475
    :pswitch_8
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, v4}, Lcom/ibm/icu/impl/UCharacterProperty;->getMaxValues(I)I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    goto :goto_0

    .line 5477
    :pswitch_9
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_a
    move v0, v1

    .line 5480
    goto :goto_0

    :pswitch_b
    move v0, v2

    .line 5483
    goto :goto_0

    .line 5485
    :pswitch_c
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, v2}, Lcom/ibm/icu/impl/UCharacterProperty;->getMaxValues(I)I

    move-result v0

    and-int/lit16 v0, v0, 0x3e0

    shr-int/lit8 v0, v0, 0x5

    goto :goto_0

    .line 5487
    :pswitch_d
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, v2}, Lcom/ibm/icu/impl/UCharacterProperty;->getMaxValues(I)I

    move-result v0

    const v1, 0xf8000

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0xf

    goto :goto_0

    .line 5489
    :pswitch_e
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, v2}, Lcom/ibm/icu/impl/UCharacterProperty;->getMaxValues(I)I

    move-result v0

    and-int/lit16 v0, v0, 0x7c00

    shr-int/lit8 v0, v0, 0xa

    goto/16 :goto_0

    .line 5452
    nop

    :pswitch_data_0
    .packed-switch 0x1000
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_b
        :pswitch_2
        :pswitch_2
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public static getIntPropertyMinValue(I)I
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 5411
    const/4 v0, 0x0

    return v0
.end method

.method public static getIntPropertyValue(II)I
    .locals 6
    .param p0, "ch"    # I
    .param p1, "type"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v0, 0x1

    const/4 v2, 0x5

    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 5258
    if-gez p1, :cond_1

    .line 5351
    :cond_0
    :goto_0
    return v1

    .line 5261
    :cond_1
    const/16 v5, 0x31

    if-ge p1, v5, :cond_3

    .line 5262
    invoke-static {p0, p1}, Lcom/ibm/icu/lang/UCharacter;->hasBinaryProperty(II)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 5264
    :cond_3
    const/16 v5, 0x1000

    if-lt p1, v5, :cond_0

    .line 5267
    const/16 v5, 0x1015

    if-ge p1, v5, :cond_c

    .line 5269
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 5271
    :pswitch_0
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getDirection(I)I

    move-result v1

    goto :goto_0

    .line 5273
    :pswitch_1
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->idOf(I)I

    move-result v1

    goto :goto_0

    .line 5275
    :pswitch_2
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getCombiningClass(I)I

    move-result v1

    goto :goto_0

    .line 5277
    :pswitch_3
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, p0, v3}, Lcom/ibm/icu/impl/UCharacterProperty;->getAdditional(II)I

    move-result v0

    and-int/lit8 v1, v0, 0x1f

    goto :goto_0

    .line 5280
    :pswitch_4
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, p0, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->getAdditional(II)I

    move-result v0

    const/high16 v1, 0xe0000

    and-int/2addr v0, v1

    shr-int/lit8 v1, v0, 0x11

    goto :goto_0

    .line 5283
    :pswitch_5
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v1

    goto :goto_0

    .line 5285
    :pswitch_6
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->gBdp:Lcom/ibm/icu/impl/UBiDiProps;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/UBiDiProps;->getJoiningGroup(I)I

    move-result v1

    goto :goto_0

    .line 5287
    :pswitch_7
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->gBdp:Lcom/ibm/icu/impl/UBiDiProps;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/UBiDiProps;->getJoiningType(I)I

    move-result v1

    goto :goto_0

    .line 5289
    :pswitch_8
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, p0, v3}, Lcom/ibm/icu/impl/UCharacterProperty;->getAdditional(II)I

    move-result v0

    const/high16 v1, 0x3f00000

    and-int/2addr v0, v1

    shr-int/lit8 v1, v0, 0x14

    goto :goto_0

    .line 5291
    :pswitch_9
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/UCharacterProperty;->getProperty(I)I

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->getNumericType(I)I

    move-result p1

    .line 5292
    if-le p1, v4, :cond_4

    .line 5294
    const/4 p1, 0x3

    :cond_4
    move v1, p1

    .line 5296
    goto :goto_0

    .line 5298
    :pswitch_a
    invoke-static {p0}, Lcom/ibm/icu/lang/UScript;->getScript(I)I

    move-result v1

    goto :goto_0

    .line 5301
    :pswitch_b
    const/16 v5, 0x1100

    if-lt p0, v5, :cond_0

    .line 5303
    const/16 v5, 0x11ff

    if-gt p0, v5, :cond_a

    .line 5305
    const/16 v5, 0x115f

    if-gt p0, v5, :cond_6

    .line 5307
    const/16 v3, 0x115f

    if-eq p0, v3, :cond_5

    const/16 v3, 0x1159

    if-le p0, v3, :cond_5

    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v3

    if-ne v3, v2, :cond_0

    :cond_5
    move v1, v0

    .line 5308
    goto/16 :goto_0

    .line 5310
    :cond_6
    const/16 v0, 0x11a7

    if-gt p0, v0, :cond_8

    .line 5312
    const/16 v0, 0x11a2

    if-le p0, v0, :cond_7

    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v0

    if-ne v0, v2, :cond_0

    :cond_7
    move v1, v3

    .line 5313
    goto/16 :goto_0

    .line 5317
    :cond_8
    const/16 v0, 0x11f9

    if-le p0, v0, :cond_9

    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v0

    if-ne v0, v2, :cond_0

    :cond_9
    move v1, v4

    .line 5318
    goto/16 :goto_0

    .line 5321
    :cond_a
    const v0, 0xac00

    sub-int/2addr p0, v0

    if-ltz p0, :cond_0

    .line 5323
    const/16 v0, 0x2ba4

    if-ge p0, v0, :cond_0

    .line 5325
    rem-int/lit8 v0, p0, 0x1c

    if-nez v0, :cond_b

    const/4 v0, 0x4

    :goto_2
    move v1, v0

    goto/16 :goto_0

    :cond_b
    move v0, v2

    goto :goto_2

    .line 5333
    :pswitch_c
    add-int/lit16 v0, p1, -0x100c

    add-int/lit8 v0, v0, 0x2

    invoke-static {p0, v0}, Lcom/ibm/icu/impl/NormalizerImpl;->quickCheck(II)I

    move-result v1

    goto/16 :goto_0

    .line 5335
    :pswitch_d
    invoke-static {p0}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(I)I

    move-result v0

    shr-int/lit8 v1, v0, 0x8

    goto/16 :goto_0

    .line 5337
    :pswitch_e
    invoke-static {p0}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(I)I

    move-result v0

    and-int/lit16 v1, v0, 0xff

    goto/16 :goto_0

    .line 5339
    :pswitch_f
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, p0, v3}, Lcom/ibm/icu/impl/UCharacterProperty;->getAdditional(II)I

    move-result v0

    and-int/lit16 v0, v0, 0x3e0

    shr-int/lit8 v1, v0, 0x5

    goto/16 :goto_0

    .line 5341
    :pswitch_10
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, p0, v3}, Lcom/ibm/icu/impl/UCharacterProperty;->getAdditional(II)I

    move-result v0

    const v1, 0xf8000

    and-int/2addr v0, v1

    shr-int/lit8 v1, v0, 0xf

    goto/16 :goto_0

    .line 5343
    :pswitch_11
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, p0, v3}, Lcom/ibm/icu/impl/UCharacterProperty;->getAdditional(II)I

    move-result v0

    and-int/lit16 v0, v0, 0x7c00

    shr-int/lit8 v1, v0, 0xa

    goto/16 :goto_0

    .line 5348
    :cond_c
    const/16 v0, 0x2000

    if-ne p1, v0, :cond_0

    .line 5349
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->getMask(I)I

    move-result v1

    goto/16 :goto_0

    .line 5269
    :pswitch_data_0
    .packed-switch 0x1000
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public static getMirror(I)I
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 3863
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->gBdp:Lcom/ibm/icu/impl/UBiDiProps;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/UBiDiProps;->getMirror(I)I

    move-result v0

    return v0
.end method

.method public static getName(I)Ljava/lang/String;
    .locals 4
    .param p0, "ch"    # I

    .prologue
    .line 3962
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    if-nez v0, :cond_0

    .line 3963
    new-instance v0, Ljava/util/MissingResourceException;

    const-string/jumbo v1, "Could not load unames.icu"

    const-string/jumbo v2, ""

    const-string/jumbo v3, ""

    invoke-direct {v0, v1, v2, v3}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 3965
    :cond_0
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/ibm/icu/impl/UCharacterName;->getName(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "separator"    # Ljava/lang/String;

    .prologue
    .line 3976
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 3977
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/lang/UCharacter;->getName(I)Ljava/lang/String;

    move-result-object v3

    .line 3986
    :goto_0
    return-object v3

    .line 3980
    :cond_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 3981
    .local v2, "sb":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 3982
    invoke-static {p0, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 3983
    .local v0, "cp":I
    if-eqz v1, :cond_1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3984
    :cond_1
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->getName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3981
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_1

    .line 3986
    .end local v0    # "cp":I
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static getName1_0(I)Ljava/lang/String;
    .locals 4
    .param p0, "ch"    # I

    .prologue
    .line 4002
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    if-nez v0, :cond_0

    .line 4003
    new-instance v0, Ljava/util/MissingResourceException;

    const-string/jumbo v1, "Could not load unames.icu"

    const-string/jumbo v2, ""

    const-string/jumbo v3, ""

    invoke-direct {v0, v1, v2, v3}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 4005
    :cond_0
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/ibm/icu/impl/UCharacterName;->getName(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getName1_0Iterator()Lcom/ibm/icu/util/ValueIterator;
    .locals 3

    .prologue
    .line 5079
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    if-nez v0, :cond_0

    .line 5080
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Could not load unames.icu"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5082
    :cond_0
    new-instance v0, Lcom/ibm/icu/lang/UCharacterNameIterator;

    sget-object v1, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacterNameIterator;-><init>(Lcom/ibm/icu/impl/UCharacterName;I)V

    return-object v0
.end method

.method public static getNameIterator()Lcom/ibm/icu/util/ValueIterator;
    .locals 3

    .prologue
    .line 5051
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    if-nez v0, :cond_0

    .line 5052
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Could not load unames.icu"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5054
    :cond_0
    new-instance v0, Lcom/ibm/icu/lang/UCharacterNameIterator;

    sget-object v1, Lcom/ibm/icu/lang/UCharacter;->NAME_:Lcom/ibm/icu/impl/UCharacterName;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacterNameIterator;-><init>(Lcom/ibm/icu/impl/UCharacterName;I)V

    return-object v0
.end method

.method private static getNumericType(I)I
    .locals 1
    .param p0, "props"    # I

    .prologue
    .line 6529
    and-int/lit16 v0, p0, 0xe0

    shr-int/lit8 v0, v0, 0x5

    return v0
.end method

.method public static getNumericValue(I)I
    .locals 13
    .param p0, "ch"    # I

    .prologue
    const/16 v9, 0x9

    const/4 v12, 0x4

    const/4 v7, -0x2

    .line 3109
    sget-object v8, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v8, p0}, Lcom/ibm/icu/impl/UCharacterProperty;->getProperty(I)I

    move-result v6

    .line 3110
    .local v6, "props":I
    invoke-static {v6}, Lcom/ibm/icu/lang/UCharacter;->getNumericType(I)I

    move-result v4

    .line 3112
    .local v4, "numericType":I
    if-nez v4, :cond_1

    .line 3113
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getEuropeanDigit(I)I

    move-result v5

    .line 3167
    :cond_0
    :goto_0
    return v5

    .line 3115
    :cond_1
    if-eq v4, v12, :cond_2

    const/4 v8, 0x6

    if-lt v4, v8, :cond_3

    :cond_2
    move v5, v7

    .line 3116
    goto :goto_0

    .line 3119
    :cond_3
    invoke-static {v6}, Lcom/ibm/icu/impl/UCharacterProperty;->getUnsignedValue(I)I

    move-result v5

    .line 3121
    .local v5, "numericValue":I
    if-lt v4, v12, :cond_0

    .line 3129
    shr-int/lit8 v1, v5, 0x4

    .line 3130
    .local v1, "mant":I
    and-int/lit8 v0, v5, 0xf

    .line 3131
    .local v0, "exp":I
    if-nez v1, :cond_4

    .line 3132
    const/4 v1, 0x1

    .line 3133
    add-int/lit8 v0, v0, 0x12

    .line 3139
    :goto_1
    if-le v0, v9, :cond_6

    move v5, v7

    .line 3140
    goto :goto_0

    .line 3134
    :cond_4
    if-le v1, v9, :cond_5

    move v5, v7

    .line 3135
    goto :goto_0

    .line 3137
    :cond_5
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 3143
    :cond_6
    int-to-long v2, v1

    .line 3146
    .local v2, "numValue":J
    :goto_2
    if-lt v0, v12, :cond_7

    .line 3147
    long-to-double v8, v2

    const-wide v10, 0x40c3880000000000L    # 10000.0

    mul-double/2addr v8, v10

    double-to-long v2, v8

    .line 3148
    add-int/lit8 v0, v0, -0x4

    .line 3149
    goto :goto_2

    .line 3150
    :cond_7
    packed-switch v0, :pswitch_data_0

    .line 3164
    :goto_3
    const-wide/32 v8, 0x7fffffff

    cmp-long v8, v2, v8

    if-gtz v8, :cond_8

    .line 3165
    long-to-int v5, v2

    goto :goto_0

    .line 3152
    :pswitch_0
    long-to-double v8, v2

    const-wide v10, 0x408f400000000000L    # 1000.0

    mul-double/2addr v8, v10

    double-to-long v2, v8

    .line 3153
    goto :goto_3

    .line 3155
    :pswitch_1
    long-to-double v8, v2

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    mul-double/2addr v8, v10

    double-to-long v2, v8

    .line 3156
    goto :goto_3

    .line 3158
    :pswitch_2
    long-to-double v8, v2

    const-wide/high16 v10, 0x4024000000000000L    # 10.0

    mul-double/2addr v8, v10

    double-to-long v2, v8

    .line 3159
    goto :goto_3

    :cond_8
    move v5, v7

    .line 3167
    goto :goto_0

    .line 3150
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static final getProperty(I)I
    .locals 4
    .param p0, "ch"    # I

    .prologue
    const v2, 0xdbff

    .line 6546
    const v1, 0xd800

    if-lt p0, v1, :cond_0

    if-le p0, v2, :cond_1

    const/high16 v1, 0x10000

    if-ge p0, v1, :cond_1

    .line 6551
    :cond_0
    :try_start_0
    sget-object v1, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_TRIE_DATA_:[C

    sget-object v2, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_TRIE_INDEX_:[C

    shr-int/lit8 v3, p0, 0x5

    aget-char v2, v2, v3

    shl-int/lit8 v2, v2, 0x2

    and-int/lit8 v3, p0, 0x1f

    add-int/2addr v2, v3

    aget-char v1, v1, v2
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6577
    :goto_0
    return v1

    .line 6554
    :catch_0
    move-exception v0

    .line 6555
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    sget v1, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_INITIAL_VALUE_:I

    goto :goto_0

    .line 6558
    .end local v0    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_1
    if-gt p0, v2, :cond_2

    .line 6560
    sget-object v1, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_TRIE_DATA_:[C

    sget-object v2, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_TRIE_INDEX_:[C

    shr-int/lit8 v3, p0, 0x5

    add-int/lit16 v3, v3, 0x140

    aget-char v2, v2, v3

    shl-int/lit8 v2, v2, 0x2

    and-int/lit8 v3, p0, 0x1f

    add-int/2addr v2, v3

    aget-char v1, v1, v2

    goto :goto_0

    .line 6565
    :cond_2
    const v1, 0x10ffff

    if-gt p0, v1, :cond_3

    .line 6569
    sget-object v1, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    iget-object v1, v1, Lcom/ibm/icu/impl/UCharacterProperty;->m_trie_:Lcom/ibm/icu/impl/CharTrie;

    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v2

    and-int/lit16 v3, p0, 0x3ff

    int-to-char v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/ibm/icu/impl/CharTrie;->getSurrogateValue(CC)C

    move-result v1

    goto :goto_0

    .line 6577
    :cond_3
    sget v1, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_INITIAL_VALUE_:I

    goto :goto_0
.end method

.method public static getPropertyEnum(Ljava/lang/String;)I
    .locals 1
    .param p0, "propertyAlias"    # Ljava/lang/String;

    .prologue
    .line 4185
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PNAMES_:Lcom/ibm/icu/impl/UPropertyAliases;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/UPropertyAliases;->getPropertyEnum(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getPropertyName(II)Ljava/lang/String;
    .locals 1
    .param p0, "property"    # I
    .param p1, "nameChoice"    # I

    .prologue
    .line 4159
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PNAMES_:Lcom/ibm/icu/impl/UPropertyAliases;

    invoke-virtual {v0, p0, p1}, Lcom/ibm/icu/impl/UPropertyAliases;->getPropertyName(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getPropertyValueEnum(ILjava/lang/String;)I
    .locals 1
    .param p0, "property"    # I
    .param p1, "valueAlias"    # Ljava/lang/String;

    .prologue
    .line 4293
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PNAMES_:Lcom/ibm/icu/impl/UPropertyAliases;

    invoke-virtual {v0, p0, p1}, Lcom/ibm/icu/impl/UPropertyAliases;->getPropertyValueEnum(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getPropertyValueName(III)Ljava/lang/String;
    .locals 3
    .param p0, "property"    # I
    .param p1, "value"    # I
    .param p2, "nameChoice"    # I

    .prologue
    const/16 v2, 0x1002

    .line 4240
    if-eq p0, v2, :cond_0

    const/16 v1, 0x1010

    if-eq p0, v1, :cond_0

    const/16 v1, 0x1011

    if-ne p0, v1, :cond_1

    :cond_0
    invoke-static {v2}, Lcom/ibm/icu/lang/UCharacter;->getIntPropertyMinValue(I)I

    move-result v1

    if-lt p1, v1, :cond_1

    invoke-static {v2}, Lcom/ibm/icu/lang/UCharacter;->getIntPropertyMaxValue(I)I

    move-result v1

    if-gt p1, v1, :cond_1

    if-ltz p2, :cond_1

    const/4 v1, 0x2

    if-ge p2, v1, :cond_1

    .line 4251
    :try_start_0
    sget-object v1, Lcom/ibm/icu/lang/UCharacter;->PNAMES_:Lcom/ibm/icu/impl/UPropertyAliases;

    invoke-virtual {v1, p0, p1, p2}, Lcom/ibm/icu/impl/UPropertyAliases;->getPropertyValueName(III)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 4258
    :goto_0
    return-object v1

    .line 4254
    :catch_0
    move-exception v0

    .line 4255
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    goto :goto_0

    .line 4258
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_1
    sget-object v1, Lcom/ibm/icu/lang/UCharacter;->PNAMES_:Lcom/ibm/icu/impl/UPropertyAliases;

    invoke-virtual {v1, p0, p1, p2}, Lcom/ibm/icu/impl/UPropertyAliases;->getPropertyValueName(III)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getStringPropertyValue(III)Ljava/lang/String;
    .locals 2
    .param p0, "propertyEnum"    # I
    .param p1, "codepoint"    # I
    .param p2, "nameChoice"    # I

    .prologue
    const/4 v1, 0x1

    .line 5364
    if-ltz p0, :cond_0

    const/16 v0, 0x31

    if-lt p0, v0, :cond_1

    :cond_0
    const/16 v0, 0x1000

    if-lt p0, v0, :cond_2

    const/16 v0, 0x1015

    if-ge p0, v0, :cond_2

    .line 5366
    :cond_1
    invoke-static {p1, p0}, Lcom/ibm/icu/lang/UCharacter;->getIntPropertyValue(II)I

    move-result v0

    invoke-static {p0, v0, p2}, Lcom/ibm/icu/lang/UCharacter;->getPropertyValueName(III)Ljava/lang/String;

    move-result-object v0

    .line 5385
    :goto_0
    return-object v0

    .line 5368
    :cond_2
    const/16 v0, 0x3000

    if-ne p0, v0, :cond_3

    .line 5369
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->getUnicodeNumericValue(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5372
    :cond_3
    packed-switch p0, :pswitch_data_0

    .line 5387
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Illegal Property Enum"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5373
    :pswitch_0
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->getAge(I)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/util/VersionInfo;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5374
    :pswitch_1
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->getISOComment(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5375
    :pswitch_2
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->getMirror(I)I

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5376
    :pswitch_3
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/ibm/icu/lang/UCharacter;->foldCase(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5377
    :pswitch_4
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->toLowerCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5378
    :pswitch_5
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->getName(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5379
    :pswitch_6
    invoke-static {p1, v1}, Lcom/ibm/icu/lang/UCharacter;->foldCase(IZ)I

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5380
    :pswitch_7
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->toLowerCase(I)I

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5381
    :pswitch_8
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->toTitleCase(I)I

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5382
    :pswitch_9
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->toUpperCase(I)I

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5383
    :pswitch_a
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/ibm/icu/lang/UCharacter;->toTitleCase(Ljava/lang/String;Lcom/ibm/icu/text/BreakIterator;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5384
    :pswitch_b
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->getName1_0(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5385
    :pswitch_c
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->toUpperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 5372
    :pswitch_data_0
    .packed-switch 0x4000
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static getType(I)I
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 3287
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getProperty(I)I

    move-result v0

    and-int/lit8 v0, v0, 0x1f

    return v0
.end method

.method public static getTypeIterator()Lcom/ibm/icu/util/RangeValueIterator;
    .locals 2

    .prologue
    .line 5026
    new-instance v0, Lcom/ibm/icu/lang/UCharacterTypeIterator;

    sget-object v1, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-direct {v0, v1}, Lcom/ibm/icu/lang/UCharacterTypeIterator;-><init>(Lcom/ibm/icu/impl/UCharacterProperty;)V

    return-object v0
.end method

.method public static getUnicodeNumericValue(I)D
    .locals 14
    .param p0, "ch"    # I

    .prologue
    const-wide v4, -0x3e6290cbac000000L    # -1.23456789E8

    const/4 v12, 0x4

    .line 3191
    sget-object v9, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v9, p0}, Lcom/ibm/icu/impl/UCharacterProperty;->getProperty(I)I

    move-result v8

    .line 3192
    .local v8, "props":I
    invoke-static {v8}, Lcom/ibm/icu/lang/UCharacter;->getNumericType(I)I

    move-result v6

    .line 3194
    .local v6, "numericType":I
    if-eqz v6, :cond_0

    const/4 v9, 0x6

    if-lt v6, v9, :cond_1

    .line 3252
    :cond_0
    :goto_0
    return-wide v4

    .line 3198
    :cond_1
    invoke-static {v8}, Lcom/ibm/icu/impl/UCharacterProperty;->getUnsignedValue(I)I

    move-result v7

    .line 3200
    .local v7, "numericValue":I
    if-ge v6, v12, :cond_2

    .line 3202
    int-to-double v4, v7

    goto :goto_0

    .line 3203
    :cond_2
    if-ne v6, v12, :cond_4

    .line 3207
    shr-int/lit8 v3, v7, 0x3

    .line 3208
    .local v3, "numerator":I
    and-int/lit8 v9, v7, 0x7

    add-int/lit8 v0, v9, 0x2

    .line 3210
    .local v0, "denominator":I
    if-nez v3, :cond_3

    .line 3211
    const/4 v3, -0x1

    .line 3213
    :cond_3
    int-to-double v10, v3

    int-to-double v12, v0

    div-double v4, v10, v12

    goto :goto_0

    .line 3219
    .end local v0    # "denominator":I
    .end local v3    # "numerator":I
    :cond_4
    shr-int/lit8 v2, v7, 0x4

    .line 3220
    .local v2, "mant":I
    and-int/lit8 v1, v7, 0xf

    .line 3221
    .local v1, "exp":I
    if-nez v2, :cond_5

    .line 3222
    const/4 v2, 0x1

    .line 3223
    add-int/lit8 v1, v1, 0x12

    .line 3230
    :goto_1
    int-to-double v4, v2

    .line 3233
    .local v4, "numValue":D
    :goto_2
    if-lt v1, v12, :cond_6

    .line 3234
    const-wide v10, 0x40c3880000000000L    # 10000.0

    mul-double/2addr v4, v10

    .line 3235
    add-int/lit8 v1, v1, -0x4

    .line 3236
    goto :goto_2

    .line 3224
    .end local v4    # "numValue":D
    :cond_5
    const/16 v9, 0x9

    if-gt v2, v9, :cond_0

    .line 3227
    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 3237
    .restart local v4    # "numValue":D
    :cond_6
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 3245
    :pswitch_0
    const-wide/high16 v10, 0x4024000000000000L    # 10.0

    mul-double/2addr v4, v10

    .line 3246
    goto :goto_0

    .line 3239
    :pswitch_1
    const-wide v10, 0x408f400000000000L    # 1000.0

    mul-double/2addr v4, v10

    .line 3240
    goto :goto_0

    .line 3242
    :pswitch_2
    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v10

    .line 3243
    goto :goto_0

    .line 3237
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static getUnicodeVersion()Lcom/ibm/icu/util/VersionInfo;
    .locals 1

    .prologue
    .line 3946
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    iget-object v0, v0, Lcom/ibm/icu/impl/UCharacterProperty;->m_unicodeVersion_:Lcom/ibm/icu/util/VersionInfo;

    return-object v0
.end method

.method public static hasBinaryProperty(II)Z
    .locals 2
    .param p0, "ch"    # I
    .param p1, "property"    # I

    .prologue
    .line 5161
    if-ltz p0, :cond_0

    const v0, 0x10ffff

    if-le p0, v0, :cond_1

    .line 5162
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Codepoint out of bounds"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5164
    :cond_1
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->PROPERTY_:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, p0, p1}, Lcom/ibm/icu/impl/UCharacterProperty;->hasBinaryProperty(II)Z

    move-result v0

    return v0
.end method

.method public static isBMP(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 3771
    if-ltz p0, :cond_0

    const v0, 0xffff

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBaseForm(I)Z
    .locals 3
    .param p0, "ch"    # I

    .prologue
    const/4 v1, 0x1

    .line 3803
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v0

    .line 3805
    .local v0, "cat":I
    const/16 v2, 0x9

    if-eq v0, v2, :cond_0

    const/16 v2, 0xb

    if-eq v0, v2, :cond_0

    const/16 v2, 0xa

    if-eq v0, v2, :cond_0

    if-eq v0, v1, :cond_0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    const/4 v2, 0x4

    if-eq v0, v2, :cond_0

    const/4 v2, 0x5

    if-eq v0, v2, :cond_0

    const/4 v2, 0x6

    if-eq v0, v2, :cond_0

    const/4 v2, 0x7

    if-eq v0, v2, :cond_0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isDefined(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 3303
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDigit(I)Z
    .locals 2
    .param p0, "ch"    # I

    .prologue
    .line 3320
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isHighSurrogate(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 5599
    const v0, 0xd800

    if-lt p0, v0, :cond_0

    const v0, 0xdbff

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isISOControl(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 3335
    if-ltz p0, :cond_1

    const/16 v0, 0x9f

    if-gt p0, v0, :cond_1

    const/16 v0, 0x1f

    if-le p0, v0, :cond_0

    const/16 v0, 0x7f

    if-lt p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isIdentifierIgnorable(I)Z
    .locals 4
    .param p0, "ch"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3615
    const/16 v2, 0x9f

    if-gt p0, v2, :cond_3

    .line 3616
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->isISOControl(I)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v2, 0x9

    if-lt p0, v2, :cond_0

    const/16 v2, 0xd

    if-le p0, v2, :cond_2

    :cond_0
    const/16 v2, 0x1c

    if-lt p0, v2, :cond_1

    const/16 v2, 0x1f

    if-le p0, v2, :cond_2

    .line 3620
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 3616
    goto :goto_0

    .line 3620
    :cond_3
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v2

    const/16 v3, 0x10

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public static isJavaIdentifierPart(I)Z
    .locals 1
    .param p0, "cp"    # I

    .prologue
    .line 3421
    int-to-char v0, p0

    invoke-static {v0}, Ljava/lang/Character;->isJavaIdentifierPart(C)Z

    move-result v0

    return v0
.end method

.method public static isJavaIdentifierStart(I)Z
    .locals 1
    .param p0, "cp"    # I

    .prologue
    .line 3409
    int-to-char v0, p0

    invoke-static {v0}, Ljava/lang/Character;->isJavaIdentifierStart(C)Z

    move-result v0

    return v0
.end method

.method public static isJavaLetter(I)Z
    .locals 1
    .param p0, "cp"    # I

    .prologue
    .line 3385
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->isJavaIdentifierStart(I)Z

    move-result v0

    return v0
.end method

.method public static isJavaLetterOrDigit(I)Z
    .locals 1
    .param p0, "cp"    # I

    .prologue
    .line 3397
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->isJavaIdentifierPart(I)Z

    move-result v0

    return v0
.end method

.method public static isLegal(I)Z
    .locals 3
    .param p0, "ch"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3894
    if-gez p0, :cond_1

    .line 3906
    :cond_0
    :goto_0
    return v1

    .line 3897
    :cond_1
    const v2, 0xd800

    if-ge p0, v2, :cond_2

    move v1, v0

    .line 3898
    goto :goto_0

    .line 3900
    :cond_2
    const v2, 0xdfff

    if-le p0, v2, :cond_0

    .line 3903
    invoke-static {p0}, Lcom/ibm/icu/impl/UCharacterUtility;->isNonCharacter(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3906
    const v2, 0x10ffff

    if-gt p0, v2, :cond_3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static isLegal(Ljava/lang/String;)Z
    .locals 4
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 3924
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 3926
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 3928
    invoke-static {p0, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 3929
    .local v0, "codepoint":I
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->isLegal(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 3930
    const/4 v3, 0x0

    .line 3936
    .end local v0    # "codepoint":I
    :goto_1
    return v3

    .line 3932
    .restart local v0    # "codepoint":I
    :cond_0
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->isSupplementary(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3933
    add-int/lit8 v1, v1, 0x1

    .line 3926
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3936
    .end local v0    # "codepoint":I
    :cond_2
    const/4 v3, 0x1

    goto :goto_1
.end method

.method public static isLetter(I)Z
    .locals 2
    .param p0, "ch"    # I

    .prologue
    const/4 v0, 0x1

    .line 3349
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v1

    shl-int v1, v0, v1

    and-int/lit8 v1, v1, 0x3e

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLetterOrDigit(I)Z
    .locals 2
    .param p0, "ch"    # I

    .prologue
    const/4 v0, 0x1

    .line 3367
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v1

    shl-int v1, v0, v1

    and-int/lit16 v1, v1, 0x23e

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLowSurrogate(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 5609
    const v0, 0xdc00

    if-lt p0, v0, :cond_0

    const v0, 0xdfff

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLowerCase(I)Z
    .locals 2
    .param p0, "ch"    # I

    .prologue
    .line 3440
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMirrored(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 3844
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->gBdp:Lcom/ibm/icu/impl/UBiDiProps;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/UBiDiProps;->isMirrored(I)Z

    move-result v0

    return v0
.end method

.method public static isPrintable(I)Z
    .locals 2
    .param p0, "ch"    # I

    .prologue
    .line 3783
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v0

    .line 3785
    .local v0, "cat":I
    if-eqz v0, :cond_0

    const/16 v1, 0xf

    if-eq v0, v1, :cond_0

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    const/16 v1, 0x11

    if-eq v0, v1, :cond_0

    const/16 v1, 0x12

    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSpace(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    const/16 v0, 0x20

    .line 3266
    if-gt p0, v0, :cond_1

    if-eq p0, v0, :cond_0

    const/16 v0, 0x9

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSpaceChar(I)Z
    .locals 2
    .param p0, "ch"    # I

    .prologue
    const/4 v0, 0x1

    .line 3495
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v1

    shl-int v1, v0, v1

    and-int/lit16 v1, v1, 0x7000

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupplementary(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 3758
    const/high16 v0, 0x10000

    if-lt p0, v0, :cond_0

    const v0, 0x10ffff

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final isSupplementaryCodePoint(I)Z
    .locals 1
    .param p0, "cp"    # I

    .prologue
    .line 5588
    const/high16 v0, 0x10000

    if-lt p0, v0, :cond_0

    const v0, 0x10ffff

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final isSurrogatePair(CC)Z
    .locals 1
    .param p0, "high"    # C
    .param p1, "low"    # C

    .prologue
    .line 5621
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->isHighSurrogate(C)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->isLowSurrogate(C)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isTitleCase(I)Z
    .locals 2
    .param p0, "ch"    # I

    .prologue
    .line 3517
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUAlphabetic(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 5176
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/ibm/icu/lang/UCharacter;->hasBinaryProperty(II)Z

    move-result v0

    return v0
.end method

.method public static isULowercase(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 5188
    const/16 v0, 0x16

    invoke-static {p0, v0}, Lcom/ibm/icu/lang/UCharacter;->hasBinaryProperty(II)Z

    move-result v0

    return v0
.end method

.method public static isUUppercase(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 5200
    const/16 v0, 0x1e

    invoke-static {p0, v0}, Lcom/ibm/icu/lang/UCharacter;->hasBinaryProperty(II)Z

    move-result v0

    return v0
.end method

.method public static isUWhiteSpace(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 5213
    const/16 v0, 0x1f

    invoke-static {p0, v0}, Lcom/ibm/icu/lang/UCharacter;->hasBinaryProperty(II)Z

    move-result v0

    return v0
.end method

.method public static isUnicodeIdentifierPart(I)Z
    .locals 3
    .param p0, "ch"    # I

    .prologue
    const/4 v0, 0x1

    .line 3551
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v1

    shl-int v1, v0, v1

    const v2, 0x40077e

    and-int/2addr v1, v2

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->isIdentifierIgnorable(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUnicodeIdentifierStart(I)Z
    .locals 2
    .param p0, "ch"    # I

    .prologue
    const/4 v0, 0x1

    .line 3589
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v1

    shl-int v1, v0, v1

    and-int/lit16 v1, v1, 0x43e

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isUpperCase(I)Z
    .locals 2
    .param p0, "ch"    # I

    .prologue
    const/4 v0, 0x1

    .line 3643
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final isValidCodePoint(I)Z
    .locals 1
    .param p0, "cp"    # I

    .prologue
    .line 5578
    if-ltz p0, :cond_0

    const v0, 0x10ffff

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWhitespace(I)Z
    .locals 2
    .param p0, "ch"    # I

    .prologue
    const/4 v0, 0x1

    .line 3473
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v1

    shl-int v1, v0, v1

    and-int/lit16 v1, v1, 0x7000

    if-eqz v1, :cond_0

    const/16 v1, 0xa0

    if-eq p0, v1, :cond_0

    const/16 v1, 0x202f

    if-eq p0, v1, :cond_0

    const v1, 0xfeff

    if-ne p0, v1, :cond_2

    :cond_0
    const/16 v1, 0x9

    if-lt p0, v1, :cond_1

    const/16 v1, 0xd

    if-le p0, v1, :cond_2

    :cond_1
    const/16 v1, 0x1c

    if-lt p0, v1, :cond_3

    const/16 v1, 0x1f

    if-gt p0, v1, :cond_3

    :cond_2
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static offsetByCodePoints(Ljava/lang/CharSequence;II)I
    .locals 8
    .param p0, "text"    # Ljava/lang/CharSequence;
    .param p1, "index"    # I
    .param p2, "codePointOffset"    # I

    .prologue
    const v7, 0xdfff

    const v6, 0xdc00

    const v5, 0xdbff

    const v4, 0xd800

    .line 6055
    if-ltz p1, :cond_0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-le p1, v3, :cond_1

    .line 6056
    :cond_0
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "index ( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ") out of range 0, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 6060
    :cond_1
    if-gez p2, :cond_5

    .line 6061
    :cond_2
    add-int/lit8 p2, p2, 0x1

    if-gtz p2, :cond_9

    .line 6062
    add-int/lit8 p1, p1, -0x1

    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 6063
    .local v0, "ch":C
    :cond_3
    if-lt v0, v6, :cond_2

    if-gt v0, v7, :cond_2

    if-lez p1, :cond_2

    .line 6064
    add-int/lit8 p1, p1, -0x1

    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 6065
    if-lt v0, v4, :cond_4

    if-le v0, v5, :cond_3

    .line 6066
    :cond_4
    add-int/lit8 p2, p2, 0x1

    if-lez p2, :cond_3

    .line 6067
    add-int/lit8 v3, p1, 0x1

    .line 6087
    .end local v0    # "ch":C
    :goto_0
    return v3

    .line 6073
    :cond_5
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .local v2, "limit":I
    move v1, p1

    .line 6074
    .end local p1    # "index":I
    .local v1, "index":I
    :cond_6
    add-int/lit8 p2, p2, -0x1

    if-ltz p2, :cond_8

    .line 6075
    add-int/lit8 p1, v1, 0x1

    .end local v1    # "index":I
    .restart local p1    # "index":I
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .restart local v0    # "ch":C
    move v1, p1

    .line 6076
    .end local p1    # "index":I
    .restart local v1    # "index":I
    :goto_1
    if-lt v0, v4, :cond_6

    if-gt v0, v5, :cond_6

    if-ge v1, v2, :cond_6

    .line 6077
    add-int/lit8 p1, v1, 0x1

    .end local v1    # "index":I
    .restart local p1    # "index":I
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 6078
    if-lt v0, v6, :cond_7

    if-le v0, v7, :cond_a

    .line 6079
    :cond_7
    add-int/lit8 p2, p2, -0x1

    if-gez p2, :cond_a

    .line 6080
    add-int/lit8 v3, p1, -0x1

    goto :goto_0

    .end local v0    # "ch":C
    .end local p1    # "index":I
    .restart local v1    # "index":I
    :cond_8
    move p1, v1

    .end local v1    # "index":I
    .end local v2    # "limit":I
    .restart local p1    # "index":I
    :cond_9
    move v3, p1

    .line 6087
    goto :goto_0

    .restart local v0    # "ch":C
    .restart local v2    # "limit":I
    :cond_a
    move v1, p1

    .end local p1    # "index":I
    .restart local v1    # "index":I
    goto :goto_1
.end method

.method public static offsetByCodePoints([CIIII)I
    .locals 8
    .param p0, "text"    # [C
    .param p1, "start"    # I
    .param p2, "count"    # I
    .param p3, "index"    # I
    .param p4, "codePointOffset"    # I

    .prologue
    const v7, 0xdfff

    const v6, 0xdc00

    const v5, 0xdbff

    const v4, 0xd800

    .line 6102
    add-int v2, p1, p2

    .line 6103
    .local v2, "limit":I
    if-ltz p1, :cond_0

    if-lt v2, p1, :cond_0

    array-length v3, p0

    if-gt v2, v3, :cond_0

    if-lt p3, p1, :cond_0

    if-le p3, v2, :cond_1

    .line 6104
    :cond_0
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "index ( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ") out of range "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, " in array 0, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    array-length v5, p0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 6110
    :cond_1
    if-gez p4, :cond_5

    .line 6111
    :cond_2
    add-int/lit8 p4, p4, 0x1

    if-gtz p4, :cond_a

    .line 6112
    add-int/lit8 p3, p3, -0x1

    aget-char v0, p0, p3

    .line 6113
    .local v0, "ch":C
    if-ge p3, p1, :cond_3

    .line 6114
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "index ( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ") < start ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 6118
    :cond_3
    if-lt v0, v6, :cond_2

    if-gt v0, v7, :cond_2

    if-le p3, p1, :cond_2

    .line 6119
    add-int/lit8 p3, p3, -0x1

    aget-char v0, p0, p3

    .line 6120
    if-lt v0, v4, :cond_4

    if-le v0, v5, :cond_3

    .line 6121
    :cond_4
    add-int/lit8 p4, p4, 0x1

    if-lez p4, :cond_3

    .line 6122
    add-int/lit8 v3, p3, 0x1

    .line 6146
    .end local v0    # "ch":C
    :goto_0
    return v3

    :cond_5
    move v1, p3

    .line 6128
    .end local p3    # "index":I
    .local v1, "index":I
    :cond_6
    add-int/lit8 p4, p4, -0x1

    if-ltz p4, :cond_9

    .line 6129
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "index":I
    .restart local p3    # "index":I
    aget-char v0, p0, v1

    .line 6130
    .restart local v0    # "ch":C
    if-le p3, v2, :cond_7

    .line 6131
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "index ( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ") > limit ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_7
    move v1, p3

    .line 6135
    .end local p3    # "index":I
    .restart local v1    # "index":I
    if-lt v0, v4, :cond_6

    if-gt v0, v5, :cond_6

    if-ge v1, v2, :cond_6

    .line 6136
    add-int/lit8 p3, v1, 0x1

    .end local v1    # "index":I
    .restart local p3    # "index":I
    aget-char v0, p0, v1

    .line 6137
    if-lt v0, v6, :cond_8

    if-le v0, v7, :cond_7

    .line 6138
    :cond_8
    add-int/lit8 p4, p4, -0x1

    if-gez p4, :cond_7

    .line 6139
    add-int/lit8 v3, p3, -0x1

    goto :goto_0

    .end local v0    # "ch":C
    .end local p3    # "index":I
    .restart local v1    # "index":I
    :cond_9
    move p3, v1

    .end local v1    # "index":I
    .restart local p3    # "index":I
    :cond_a
    move v3, p3

    .line 6146
    goto :goto_0
.end method

.method public static final toChars(I[CI)I
    .locals 2
    .param p0, "cp"    # I
    .param p1, "dst"    # [C
    .param p2, "dstIndex"    # I

    .prologue
    .line 5852
    if-ltz p0, :cond_1

    .line 5853
    const/high16 v0, 0x10000

    if-ge p0, v0, :cond_0

    .line 5854
    int-to-char v0, p0

    aput-char v0, p1, p2

    .line 5855
    const/4 v0, 0x1

    .line 5860
    :goto_0
    return v0

    .line 5857
    :cond_0
    const v0, 0x10ffff

    if-gt p0, v0, :cond_1

    .line 5858
    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v0

    aput-char v0, p1, p2

    .line 5859
    add-int/lit8 v0, p2, 0x1

    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->getTrailSurrogate(I)C

    move-result v1

    aput-char v1, p1, v0

    .line 5860
    const/4 v0, 0x2

    goto :goto_0

    .line 5863
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public static final toChars(I)[C
    .locals 4
    .param p0, "cp"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5875
    if-ltz p0, :cond_1

    .line 5876
    const/high16 v0, 0x10000

    if-ge p0, v0, :cond_0

    .line 5877
    new-array v0, v3, [C

    int-to-char v1, p0

    aput-char v1, v0, v2

    .line 5880
    :goto_0
    return-object v0

    .line 5879
    :cond_0
    const v0, 0x10ffff

    if-gt p0, v0, :cond_1

    .line 5880
    const/4 v0, 0x2

    new-array v0, v0, [C

    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v1

    aput-char v1, v0, v2

    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->getTrailSurrogate(I)C

    move-result v1

    aput-char v1, v0, v3

    goto :goto_0

    .line 5886
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public static final toCodePoint(CC)I
    .locals 1
    .param p0, "high"    # C
    .param p1, "low"    # C

    .prologue
    .line 5646
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v0

    return v0
.end method

.method public static toLowerCase(I)I
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 3666
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/UCaseProps;->tolower(I)I

    move-result v0

    return v0
.end method

.method public static toLowerCase(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 4581
    new-instance v2, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;

    invoke-direct {v2, p1}, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;-><init>(Ljava/lang/String;)V

    .line 4582
    .local v2, "iter":Lcom/ibm/icu/lang/UCharacter$StringContextIterator;
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 4583
    .local v3, "result":Ljava/lang/StringBuffer;
    const/4 v0, 0x1

    new-array v5, v0, [I

    .line 4586
    .local v5, "locCache":[I
    if-nez p0, :cond_0

    .line 4587
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object p0

    .line 4589
    :cond_0
    aput v4, v5, v4

    .line 4591
    :goto_0
    invoke-virtual {v2}, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->nextCaseMapCP()I

    move-result v1

    .local v1, "c":I
    if-ltz v1, :cond_4

    .line 4592
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/impl/UCaseProps;->toFullLower(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[I)I

    move-result v1

    .line 4595
    if-gez v1, :cond_2

    .line 4597
    xor-int/lit8 v1, v1, -0x1

    .line 4603
    :cond_1
    const v0, 0xffff

    if-gt v1, v0, :cond_3

    .line 4604
    int-to-char v0, v1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 4598
    :cond_2
    const/16 v0, 0x1f

    if-gt v1, v0, :cond_1

    goto :goto_0

    .line 4606
    :cond_3
    invoke-static {v3, v1}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 4609
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toLowerCase(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 4480
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/ibm/icu/lang/UCharacter;->toLowerCase(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toLowerCase(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "locale"    # Ljava/util/Locale;
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 4569
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/ibm/icu/lang/UCharacter;->toLowerCase(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toString(I)Ljava/lang/String;
    .locals 2
    .param p0, "ch"    # I

    .prologue
    .line 3684
    if-ltz p0, :cond_0

    const v1, 0x10ffff

    if-le p0, v1, :cond_1

    .line 3685
    :cond_0
    const/4 v1, 0x0

    .line 3695
    :goto_0
    return-object v1

    .line 3688
    :cond_1
    const/high16 v1, 0x10000

    if-ge p0, v1, :cond_2

    .line 3689
    int-to-char v1, p0

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 3692
    :cond_2
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 3693
    .local v0, "result":Ljava/lang/StringBuffer;
    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3694
    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->getTrailSurrogate(I)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3695
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static toTitleCase(I)I
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 3719
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/UCaseProps;->totitle(I)I

    move-result v0

    return v0
.end method

.method public static toTitleCase(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;Lcom/ibm/icu/text/BreakIterator;)Ljava/lang/String;
    .locals 1
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "titleIter"    # Lcom/ibm/icu/text/BreakIterator;

    .prologue
    .line 4658
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/ibm/icu/lang/UCharacter;->toTitleCase(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;Lcom/ibm/icu/text/BreakIterator;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toTitleCase(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;Lcom/ibm/icu/text/BreakIterator;I)Ljava/lang/String;
    .locals 24
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "titleIter"    # Lcom/ibm/icu/text/BreakIterator;
    .param p3, "options"    # I

    .prologue
    .line 4686
    new-instance v5, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;-><init>(Ljava/lang/String;)V

    .line 4687
    .local v5, "iter":Lcom/ibm/icu/lang/UCharacter$StringContextIterator;
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v6, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 4688
    .local v6, "result":Ljava/lang/StringBuffer;
    const/4 v3, 0x1

    new-array v8, v3, [I

    .line 4689
    .local v8, "locCache":[I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v21

    .line 4691
    .local v21, "srcLength":I
    if-nez p0, :cond_0

    .line 4692
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object p0

    .line 4694
    :cond_0
    const/4 v3, 0x0

    const/4 v7, 0x0

    aput v7, v8, v3

    .line 4696
    if-nez p2, :cond_1

    .line 4697
    invoke-static/range {p0 .. p0}, Lcom/ibm/icu/text/BreakIterator;->getWordInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/BreakIterator;

    move-result-object p2

    .line 4699
    :cond_1
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/BreakIterator;->setText(Ljava/lang/String;)V

    .line 4703
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/util/ULocale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v7, "nl"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    .line 4704
    .local v18, "isDutch":Z
    const/4 v15, 0x1

    .line 4707
    .local v15, "FirstIJ":Z
    const/16 v20, 0x0

    .line 4708
    .local v20, "prev":I
    const/16 v19, 0x1

    .line 4711
    .local v19, "isFirstIndex":Z
    :goto_0
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_13

    .line 4713
    if-eqz v19, :cond_b

    .line 4714
    const/16 v19, 0x0

    .line 4715
    invoke-virtual/range {p2 .. p2}, Lcom/ibm/icu/text/BreakIterator;->first()I

    move-result v17

    .line 4719
    .local v17, "index":I
    :goto_1
    const/4 v3, -0x1

    move/from16 v0, v17

    if-eq v0, v3, :cond_2

    move/from16 v0, v17

    move/from16 v1, v21

    if-le v0, v1, :cond_3

    .line 4720
    :cond_2
    move/from16 v17, v21

    .line 4736
    :cond_3
    move/from16 v0, v20

    move/from16 v1, v17

    if-ge v0, v1, :cond_a

    .line 4738
    move/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->setLimit(I)V

    .line 4739
    invoke-virtual {v5}, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->nextCaseMapCP()I

    move-result v4

    .line 4740
    .local v4, "c":I
    move/from16 v0, p3

    and-int/lit16 v3, v0, 0x200

    if-nez v3, :cond_c

    sget-object v3, Lcom/ibm/icu/lang/UCharacter;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    invoke-virtual {v3, v4}, Lcom/ibm/icu/impl/UCaseProps;->getType(I)I

    move-result v3

    if-nez v3, :cond_c

    .line 4741
    :cond_4
    invoke-virtual {v5}, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->nextCaseMapCP()I

    move-result v4

    if-ltz v4, :cond_5

    sget-object v3, Lcom/ibm/icu/lang/UCharacter;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    invoke-virtual {v3, v4}, Lcom/ibm/icu/impl/UCaseProps;->getType(I)I

    move-result v3

    if-eqz v3, :cond_4

    .line 4742
    :cond_5
    invoke-virtual {v5}, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->getCPStart()I

    move-result v23

    .line 4743
    .local v23, "titleStart":I
    move/from16 v0, v20

    move/from16 v1, v23

    if-ge v0, v1, :cond_6

    .line 4745
    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4751
    :cond_6
    :goto_2
    move/from16 v0, v23

    move/from16 v1, v17

    if-ge v0, v1, :cond_a

    .line 4752
    const/4 v15, 0x1

    .line 4754
    sget-object v3, Lcom/ibm/icu/lang/UCharacter;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    move-object/from16 v7, p0

    invoke-virtual/range {v3 .. v8}, Lcom/ibm/icu/impl/UCaseProps;->toFullTitle(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[I)I

    move-result v4

    .line 4758
    :cond_7
    :goto_3
    if-gez v4, :cond_e

    .line 4760
    xor-int/lit8 v4, v4, -0x1

    .line 4761
    const v3, 0xffff

    if-gt v4, v3, :cond_d

    .line 4762
    int-to-char v3, v4

    invoke-virtual {v6, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 4777
    :cond_8
    :goto_4
    move/from16 v0, p3

    and-int/lit16 v3, v0, 0x100

    if-eqz v3, :cond_10

    .line 4780
    invoke-virtual {v5}, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->getCPLimit()I

    move-result v22

    .line 4781
    .local v22, "titleLimit":I
    move/from16 v0, v22

    move/from16 v1, v17

    if-ge v0, v1, :cond_7

    .line 4783
    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    .line 4785
    .local v16, "appendStr":Ljava/lang/String;
    if-eqz v18, :cond_9

    const/16 v3, 0x49

    if-ne v4, v3, :cond_9

    const-string/jumbo v3, "j"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 4786
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "J"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const/4 v7, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v16

    .line 4788
    :cond_9
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4789
    invoke-virtual {v5}, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->moveToLimit()V

    .line 4807
    .end local v4    # "c":I
    .end local v16    # "appendStr":Ljava/lang/String;
    .end local v22    # "titleLimit":I
    .end local v23    # "titleStart":I
    :cond_a
    move/from16 v20, v17

    .line 4808
    goto/16 :goto_0

    .line 4717
    .end local v17    # "index":I
    :cond_b
    invoke-virtual/range {p2 .. p2}, Lcom/ibm/icu/text/BreakIterator;->next()I

    move-result v17

    .restart local v17    # "index":I
    goto/16 :goto_1

    .line 4748
    .restart local v4    # "c":I
    :cond_c
    move/from16 v23, v20

    .restart local v23    # "titleStart":I
    goto :goto_2

    .line 4764
    :cond_d
    invoke-static {v6, v4}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 4766
    :cond_e
    const/16 v3, 0x1f

    if-le v4, v3, :cond_8

    .line 4770
    const v3, 0xffff

    if-gt v4, v3, :cond_f

    .line 4771
    int-to-char v3, v4

    invoke-virtual {v6, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 4773
    :cond_f
    invoke-static {v6, v4}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 4792
    :cond_10
    invoke-virtual {v5}, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->nextCaseMapCP()I

    move-result v10

    .local v10, "nc":I
    if-ltz v10, :cond_a

    .line 4793
    if-eqz v18, :cond_12

    const/16 v3, 0x4a

    if-eq v10, v3, :cond_11

    const/16 v3, 0x6a

    if-ne v10, v3, :cond_12

    :cond_11
    const/16 v3, 0x49

    if-ne v4, v3, :cond_12

    const/4 v3, 0x1

    if-ne v15, v3, :cond_12

    .line 4794
    const/16 v4, 0x4a

    .line 4795
    const/4 v15, 0x0

    .line 4796
    goto/16 :goto_3

    .line 4798
    :cond_12
    sget-object v9, Lcom/ibm/icu/lang/UCharacter;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    move-object v11, v5

    move-object v12, v6

    move-object/from16 v13, p0

    move-object v14, v8

    invoke-virtual/range {v9 .. v14}, Lcom/ibm/icu/impl/UCaseProps;->toFullLower(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[I)I

    move-result v4

    .line 4800
    goto/16 :goto_3

    .line 4809
    .end local v4    # "c":I
    .end local v10    # "nc":I
    .end local v17    # "index":I
    .end local v23    # "titleStart":I
    :cond_13
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static toTitleCase(Ljava/lang/String;Lcom/ibm/icu/text/BreakIterator;)Ljava/lang/String;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "breakiter"    # Lcom/ibm/icu/text/BreakIterator;

    .prologue
    .line 4503
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/ibm/icu/lang/UCharacter;->toTitleCase(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;Lcom/ibm/icu/text/BreakIterator;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toTitleCase(Ljava/util/Locale;Ljava/lang/String;Lcom/ibm/icu/text/BreakIterator;)Ljava/lang/String;
    .locals 1
    .param p0, "locale"    # Ljava/util/Locale;
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "breakiter"    # Lcom/ibm/icu/text/BreakIterator;

    .prologue
    .line 4634
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/ibm/icu/lang/UCharacter;->toTitleCase(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;Lcom/ibm/icu/text/BreakIterator;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toUpperCase(I)I
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 3742
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/UCaseProps;->toupper(I)I

    move-result v0

    return v0
.end method

.method public static toUpperCase(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 4528
    new-instance v2, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;

    invoke-direct {v2, p1}, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;-><init>(Ljava/lang/String;)V

    .line 4529
    .local v2, "iter":Lcom/ibm/icu/lang/UCharacter$StringContextIterator;
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 4530
    .local v3, "result":Ljava/lang/StringBuffer;
    const/4 v0, 0x1

    new-array v5, v0, [I

    .line 4533
    .local v5, "locCache":[I
    if-nez p0, :cond_0

    .line 4534
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object p0

    .line 4536
    :cond_0
    aput v4, v5, v4

    .line 4538
    :goto_0
    invoke-virtual {v2}, Lcom/ibm/icu/lang/UCharacter$StringContextIterator;->nextCaseMapCP()I

    move-result v1

    .local v1, "c":I
    if-ltz v1, :cond_4

    .line 4539
    sget-object v0, Lcom/ibm/icu/lang/UCharacter;->gCsp:Lcom/ibm/icu/impl/UCaseProps;

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/impl/UCaseProps;->toFullUpper(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[I)I

    move-result v1

    .line 4542
    if-gez v1, :cond_2

    .line 4544
    xor-int/lit8 v1, v1, -0x1

    .line 4550
    :cond_1
    const v0, 0xffff

    if-gt v1, v0, :cond_3

    .line 4551
    int-to-char v0, v1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 4545
    :cond_2
    const/16 v0, 0x1f

    if-gt v1, v0, :cond_1

    goto :goto_0

    .line 4553
    :cond_3
    invoke-static {v3, v1}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 4556
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toUpperCase(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 4468
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/ibm/icu/lang/UCharacter;->toUpperCase(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toUpperCase(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "locale"    # Ljava/util/Locale;
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 4516
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/ibm/icu/lang/UCharacter;->toUpperCase(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
