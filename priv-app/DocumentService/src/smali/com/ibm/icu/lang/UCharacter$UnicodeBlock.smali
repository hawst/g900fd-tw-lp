.class public final Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;
.super Ljava/lang/Character$Subset;
.source "UCharacter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/lang/UCharacter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UnicodeBlock"
.end annotation


# static fields
.field public static final AEGEAN_NUMBERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final AEGEAN_NUMBERS_ID:I = 0x77

.field public static final ALPHABETIC_PRESENTATION_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ALPHABETIC_PRESENTATION_FORMS_ID:I = 0x50

.field public static final ANCIENT_GREEK_MUSICAL_NOTATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ANCIENT_GREEK_MUSICAL_NOTATION_ID:I = 0x7e

.field public static final ANCIENT_GREEK_NUMBERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ANCIENT_GREEK_NUMBERS_ID:I = 0x7f

.field public static final ANCIENT_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ANCIENT_SYMBOLS_ID:I = 0xa5

.field public static final ARABIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ARABIC_ID:I = 0xc

.field public static final ARABIC_PRESENTATION_FORMS_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ARABIC_PRESENTATION_FORMS_A_ID:I = 0x51

.field public static final ARABIC_PRESENTATION_FORMS_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ARABIC_PRESENTATION_FORMS_B_ID:I = 0x55

.field public static final ARABIC_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ARABIC_SUPPLEMENT_ID:I = 0x80

.field public static final ARMENIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ARMENIAN_ID:I = 0xa

.field public static final ARROWS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ARROWS_ID:I = 0x2e

.field public static final BALINESE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final BALINESE_ID:I = 0x93

.field public static final BASIC_LATIN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final BASIC_LATIN_ID:I = 0x1

.field public static final BENGALI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final BENGALI_ID:I = 0x10

.field private static final BLOCKS_:[Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final BLOCK_ELEMENTS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final BLOCK_ELEMENTS_ID:I = 0x35

.field public static final BOPOMOFO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final BOPOMOFO_EXTENDED:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final BOPOMOFO_EXTENDED_ID:I = 0x43

.field public static final BOPOMOFO_ID:I = 0x40

.field public static final BOX_DRAWING:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final BOX_DRAWING_ID:I = 0x34

.field public static final BRAILLE_PATTERNS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final BRAILLE_PATTERNS_ID:I = 0x39

.field public static final BUGINESE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final BUGINESE_ID:I = 0x81

.field public static final BUHID:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final BUHID_ID:I = 0x64

.field public static final BYZANTINE_MUSICAL_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final BYZANTINE_MUSICAL_SYMBOLS_ID:I = 0x5b

.field public static final CARIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CARIAN_ID:I = 0xa8

.field public static final CHAM:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CHAM_ID:I = 0xa4

.field public static final CHEROKEE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CHEROKEE_ID:I = 0x20

.field public static final CJK_COMPATIBILITY:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CJK_COMPATIBILITY_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CJK_COMPATIBILITY_FORMS_ID:I = 0x53

.field public static final CJK_COMPATIBILITY_ID:I = 0x45

.field public static final CJK_COMPATIBILITY_IDEOGRAPHS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CJK_COMPATIBILITY_IDEOGRAPHS_ID:I = 0x4f

.field public static final CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT_ID:I = 0x5f

.field public static final CJK_RADICALS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CJK_RADICALS_SUPPLEMENT_ID:I = 0x3a

.field public static final CJK_STROKES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CJK_STROKES_ID:I = 0x82

.field public static final CJK_SYMBOLS_AND_PUNCTUATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CJK_SYMBOLS_AND_PUNCTUATION_ID:I = 0x3d

.field public static final CJK_UNIFIED_IDEOGRAPHS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A_ID:I = 0x46

.field public static final CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B_ID:I = 0x5e

.field public static final CJK_UNIFIED_IDEOGRAPHS_ID:I = 0x47

.field public static final COMBINING_DIACRITICAL_MARKS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final COMBINING_DIACRITICAL_MARKS_ID:I = 0x7

.field public static final COMBINING_DIACRITICAL_MARKS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final COMBINING_DIACRITICAL_MARKS_SUPPLEMENT_ID:I = 0x83

.field public static final COMBINING_HALF_MARKS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final COMBINING_HALF_MARKS_ID:I = 0x52

.field public static final COMBINING_MARKS_FOR_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final COMBINING_MARKS_FOR_SYMBOLS_ID:I = 0x2b

.field public static final CONTROL_PICTURES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CONTROL_PICTURES_ID:I = 0x31

.field public static final COPTIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final COPTIC_ID:I = 0x84

.field public static final COUNT:I = 0xac

.field public static final COUNTING_ROD_NUMERALS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final COUNTING_ROD_NUMERALS_ID:I = 0x9a

.field public static final CUNEIFORM:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CUNEIFORM_ID:I = 0x98

.field public static final CUNEIFORM_NUMBERS_AND_PUNCTUATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CUNEIFORM_NUMBERS_AND_PUNCTUATION_ID:I = 0x99

.field public static final CURRENCY_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CURRENCY_SYMBOLS_ID:I = 0x2a

.field public static final CYPRIOT_SYLLABARY:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CYPRIOT_SYLLABARY_ID:I = 0x7b

.field public static final CYRILLIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CYRILLIC_EXTENDED_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CYRILLIC_EXTENDED_A_ID:I = 0x9e

.field public static final CYRILLIC_EXTENDED_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CYRILLIC_EXTENDED_B_ID:I = 0xa0

.field public static final CYRILLIC_ID:I = 0x9

.field public static final CYRILLIC_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CYRILLIC_SUPPLEMENTARY:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final CYRILLIC_SUPPLEMENTARY_ID:I = 0x61

.field public static final CYRILLIC_SUPPLEMENT_ID:I = 0x61

.field public static final DESERET:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final DESERET_ID:I = 0x5a

.field public static final DEVANAGARI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final DEVANAGARI_ID:I = 0xf

.field public static final DINGBATS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final DINGBATS_ID:I = 0x38

.field public static final DOMINO_TILES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final DOMINO_TILES_ID:I = 0xab

.field public static final ENCLOSED_ALPHANUMERICS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ENCLOSED_ALPHANUMERICS_ID:I = 0x33

.field public static final ENCLOSED_CJK_LETTERS_AND_MONTHS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ENCLOSED_CJK_LETTERS_AND_MONTHS_ID:I = 0x44

.field public static final ETHIOPIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ETHIOPIC_EXTENDED:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ETHIOPIC_EXTENDED_ID:I = 0x85

.field public static final ETHIOPIC_ID:I = 0x1f

.field public static final ETHIOPIC_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ETHIOPIC_SUPPLEMENT_ID:I = 0x86

.field public static final GENERAL_PUNCTUATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final GENERAL_PUNCTUATION_ID:I = 0x28

.field public static final GEOMETRIC_SHAPES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final GEOMETRIC_SHAPES_ID:I = 0x36

.field public static final GEORGIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final GEORGIAN_ID:I = 0x1d

.field public static final GEORGIAN_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final GEORGIAN_SUPPLEMENT_ID:I = 0x87

.field public static final GLAGOLITIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final GLAGOLITIC_ID:I = 0x88

.field public static final GOTHIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final GOTHIC_ID:I = 0x59

.field public static final GREEK:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final GREEK_EXTENDED:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final GREEK_EXTENDED_ID:I = 0x27

.field public static final GREEK_ID:I = 0x8

.field public static final GUJARATI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final GUJARATI_ID:I = 0x12

.field public static final GURMUKHI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final GURMUKHI_ID:I = 0x11

.field public static final HALFWIDTH_AND_FULLWIDTH_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final HALFWIDTH_AND_FULLWIDTH_FORMS_ID:I = 0x57

.field public static final HANGUL_COMPATIBILITY_JAMO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final HANGUL_COMPATIBILITY_JAMO_ID:I = 0x41

.field public static final HANGUL_JAMO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final HANGUL_JAMO_ID:I = 0x1e

.field public static final HANGUL_SYLLABLES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final HANGUL_SYLLABLES_ID:I = 0x4a

.field public static final HANUNOO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final HANUNOO_ID:I = 0x63

.field public static final HEBREW:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final HEBREW_ID:I = 0xb

.field public static final HIGH_PRIVATE_USE_SURROGATES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final HIGH_PRIVATE_USE_SURROGATES_ID:I = 0x4c

.field public static final HIGH_SURROGATES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final HIGH_SURROGATES_ID:I = 0x4b

.field public static final HIRAGANA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final HIRAGANA_ID:I = 0x3e

.field public static final IDEOGRAPHIC_DESCRIPTION_CHARACTERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final IDEOGRAPHIC_DESCRIPTION_CHARACTERS_ID:I = 0x3c

.field public static final INVALID_CODE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final INVALID_CODE_ID:I = -0x1

.field public static final IPA_EXTENSIONS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final IPA_EXTENSIONS_ID:I = 0x5

.field public static final KANBUN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final KANBUN_ID:I = 0x42

.field public static final KANGXI_RADICALS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final KANGXI_RADICALS_ID:I = 0x3b

.field public static final KANNADA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final KANNADA_ID:I = 0x16

.field public static final KATAKANA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final KATAKANA_ID:I = 0x3f

.field public static final KATAKANA_PHONETIC_EXTENSIONS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final KATAKANA_PHONETIC_EXTENSIONS_ID:I = 0x6b

.field public static final KAYAH_LI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final KAYAH_LI_ID:I = 0xa2

.field public static final KHAROSHTHI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final KHAROSHTHI_ID:I = 0x89

.field public static final KHMER:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final KHMER_ID:I = 0x24

.field public static final KHMER_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final KHMER_SYMBOLS_ID:I = 0x71

.field public static final LAO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LAO_ID:I = 0x1a

.field public static final LATIN_1_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LATIN_1_SUPPLEMENT_ID:I = 0x2

.field public static final LATIN_EXTENDED_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LATIN_EXTENDED_ADDITIONAL:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LATIN_EXTENDED_ADDITIONAL_ID:I = 0x26

.field public static final LATIN_EXTENDED_A_ID:I = 0x3

.field public static final LATIN_EXTENDED_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LATIN_EXTENDED_B_ID:I = 0x4

.field public static final LATIN_EXTENDED_C:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LATIN_EXTENDED_C_ID:I = 0x94

.field public static final LATIN_EXTENDED_D:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LATIN_EXTENDED_D_ID:I = 0x95

.field public static final LEPCHA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LEPCHA_ID:I = 0x9c

.field public static final LETTERLIKE_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LETTERLIKE_SYMBOLS_ID:I = 0x2c

.field public static final LIMBU:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LIMBU_ID:I = 0x6f

.field public static final LINEAR_B_IDEOGRAMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LINEAR_B_IDEOGRAMS_ID:I = 0x76

.field public static final LINEAR_B_SYLLABARY:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LINEAR_B_SYLLABARY_ID:I = 0x75

.field public static final LOW_SURROGATES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LOW_SURROGATES_ID:I = 0x4d

.field public static final LYCIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LYCIAN_ID:I = 0xa7

.field public static final LYDIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final LYDIAN_ID:I = 0xa9

.field public static final MAHJONG_TILES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final MAHJONG_TILES_ID:I = 0xaa

.field public static final MALAYALAM:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final MALAYALAM_ID:I = 0x17

.field public static final MATHEMATICAL_ALPHANUMERIC_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final MATHEMATICAL_ALPHANUMERIC_SYMBOLS_ID:I = 0x5d

.field public static final MATHEMATICAL_OPERATORS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final MATHEMATICAL_OPERATORS_ID:I = 0x2f

.field public static final MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A_ID:I = 0x66

.field public static final MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B_ID:I = 0x69

.field public static final MISCELLANEOUS_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final MISCELLANEOUS_SYMBOLS_AND_ARROWS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final MISCELLANEOUS_SYMBOLS_AND_ARROWS_ID:I = 0x73

.field public static final MISCELLANEOUS_SYMBOLS_ID:I = 0x37

.field public static final MISCELLANEOUS_TECHNICAL:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final MISCELLANEOUS_TECHNICAL_ID:I = 0x30

.field public static final MODIFIER_TONE_LETTERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final MODIFIER_TONE_LETTERS_ID:I = 0x8a

.field public static final MONGOLIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final MONGOLIAN_ID:I = 0x25

.field public static final MUSICAL_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final MUSICAL_SYMBOLS_ID:I = 0x5c

.field public static final MYANMAR:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final MYANMAR_ID:I = 0x1c

.field public static final NEW_TAI_LUE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final NEW_TAI_LUE_ID:I = 0x8b

.field public static final NKO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final NKO_ID:I = 0x92

.field public static final NO_BLOCK:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final NUMBER_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final NUMBER_FORMS_ID:I = 0x2d

.field public static final OGHAM:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final OGHAM_ID:I = 0x22

.field public static final OLD_ITALIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final OLD_ITALIC_ID:I = 0x58

.field public static final OLD_PERSIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final OLD_PERSIAN_ID:I = 0x8c

.field public static final OL_CHIKI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final OL_CHIKI_ID:I = 0x9d

.field public static final OPTICAL_CHARACTER_RECOGNITION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final OPTICAL_CHARACTER_RECOGNITION_ID:I = 0x32

.field public static final ORIYA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final ORIYA_ID:I = 0x13

.field public static final OSMANYA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final OSMANYA_ID:I = 0x7a

.field public static final PHAGS_PA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final PHAGS_PA_ID:I = 0x96

.field public static final PHAISTOS_DISC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final PHAISTOS_DISC_ID:I = 0xa6

.field public static final PHOENICIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final PHOENICIAN_ID:I = 0x97

.field public static final PHONETIC_EXTENSIONS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final PHONETIC_EXTENSIONS_ID:I = 0x72

.field public static final PHONETIC_EXTENSIONS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final PHONETIC_EXTENSIONS_SUPPLEMENT_ID:I = 0x8d

.field public static final PRIVATE_USE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final PRIVATE_USE_AREA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final PRIVATE_USE_AREA_ID:I = 0x4e

.field public static final PRIVATE_USE_ID:I = 0x4e

.field public static final REJANG:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final REJANG_ID:I = 0xa3

.field public static final RUNIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final RUNIC_ID:I = 0x23

.field public static final SAURASHTRA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SAURASHTRA_ID:I = 0xa1

.field public static final SHAVIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SHAVIAN_ID:I = 0x79

.field public static final SINHALA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SINHALA_ID:I = 0x18

.field public static final SMALL_FORM_VARIANTS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SMALL_FORM_VARIANTS_ID:I = 0x54

.field public static final SPACING_MODIFIER_LETTERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SPACING_MODIFIER_LETTERS_ID:I = 0x6

.field public static final SPECIALS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SPECIALS_ID:I = 0x56

.field public static final SUNDANESE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SUNDANESE_ID:I = 0x9b

.field public static final SUPERSCRIPTS_AND_SUBSCRIPTS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SUPERSCRIPTS_AND_SUBSCRIPTS_ID:I = 0x29

.field public static final SUPPLEMENTAL_ARROWS_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SUPPLEMENTAL_ARROWS_A_ID:I = 0x67

.field public static final SUPPLEMENTAL_ARROWS_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SUPPLEMENTAL_ARROWS_B_ID:I = 0x68

.field public static final SUPPLEMENTAL_MATHEMATICAL_OPERATORS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SUPPLEMENTAL_MATHEMATICAL_OPERATORS_ID:I = 0x6a

.field public static final SUPPLEMENTAL_PUNCTUATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SUPPLEMENTAL_PUNCTUATION_ID:I = 0x8e

.field public static final SUPPLEMENTARY_PRIVATE_USE_AREA_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SUPPLEMENTARY_PRIVATE_USE_AREA_A_ID:I = 0x6d

.field public static final SUPPLEMENTARY_PRIVATE_USE_AREA_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SUPPLEMENTARY_PRIVATE_USE_AREA_B_ID:I = 0x6e

.field public static final SYLOTI_NAGRI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SYLOTI_NAGRI_ID:I = 0x8f

.field public static final SYRIAC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final SYRIAC_ID:I = 0xd

.field public static final TAGALOG:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final TAGALOG_ID:I = 0x62

.field public static final TAGBANWA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final TAGBANWA_ID:I = 0x65

.field public static final TAGS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final TAGS_ID:I = 0x60

.field public static final TAI_LE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final TAI_LE_ID:I = 0x70

.field public static final TAI_XUAN_JING_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final TAI_XUAN_JING_SYMBOLS_ID:I = 0x7c

.field public static final TAMIL:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final TAMIL_ID:I = 0x14

.field public static final TELUGU:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final TELUGU_ID:I = 0x15

.field public static final THAANA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final THAANA_ID:I = 0xe

.field public static final THAI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final THAI_ID:I = 0x19

.field public static final TIBETAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final TIBETAN_ID:I = 0x1b

.field public static final TIFINAGH:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final TIFINAGH_ID:I = 0x90

.field public static final UGARITIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final UGARITIC_ID:I = 0x78

.field public static final UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS_ID:I = 0x21

.field public static final VAI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final VAI_ID:I = 0x9f

.field public static final VARIATION_SELECTORS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final VARIATION_SELECTORS_ID:I = 0x6c

.field public static final VARIATION_SELECTORS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final VARIATION_SELECTORS_SUPPLEMENT_ID:I = 0x7d

.field public static final VERTICAL_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final VERTICAL_FORMS_ID:I = 0x91

.field public static final YIJING_HEXAGRAM_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final YIJING_HEXAGRAM_SYMBOLS_ID:I = 0x74

.field public static final YI_RADICALS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final YI_RADICALS_ID:I = 0x49

.field public static final YI_SYLLABLES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

.field public static final YI_SYLLABLES_ID:I = 0x48

.field private static mref:Ljava/lang/ref/SoftReference;


# instance fields
.field private m_id_:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x61

    .line 961
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "NO_BLOCK"

    invoke-direct {v0, v1, v4}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->NO_BLOCK:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 967
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "BASIC_LATIN"

    invoke-direct {v0, v1, v5}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BASIC_LATIN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 972
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LATIN_1_SUPPLEMENT"

    invoke-direct {v0, v1, v6}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LATIN_1_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 977
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LATIN_EXTENDED_A"

    invoke-direct {v0, v1, v7}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LATIN_EXTENDED_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 982
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LATIN_EXTENDED_B"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LATIN_EXTENDED_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 987
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "IPA_EXTENSIONS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->IPA_EXTENSIONS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 992
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SPACING_MODIFIER_LETTERS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SPACING_MODIFIER_LETTERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 997
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "COMBINING_DIACRITICAL_MARKS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->COMBINING_DIACRITICAL_MARKS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1003
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "GREEK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GREEK:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1008
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CYRILLIC"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CYRILLIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1013
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ARMENIAN"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ARMENIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1018
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "HEBREW"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HEBREW:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1023
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ARABIC"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ARABIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1028
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SYRIAC"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SYRIAC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1033
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "THAANA"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->THAANA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1038
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "DEVANAGARI"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->DEVANAGARI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1043
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "BENGALI"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BENGALI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1048
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "GURMUKHI"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GURMUKHI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1053
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "GUJARATI"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GUJARATI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1058
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ORIYA"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ORIYA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1063
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "TAMIL"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TAMIL:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1068
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "TELUGU"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TELUGU:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1073
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "KANNADA"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KANNADA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1078
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "MALAYALAM"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MALAYALAM:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1083
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SINHALA"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SINHALA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1088
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "THAI"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->THAI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1093
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LAO"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LAO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1098
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "TIBETAN"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TIBETAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1103
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "MYANMAR"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MYANMAR:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1108
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "GEORGIAN"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GEORGIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1113
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "HANGUL_JAMO"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HANGUL_JAMO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1118
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ETHIOPIC"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ETHIOPIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1123
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CHEROKEE"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CHEROKEE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1128
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1133
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "OGHAM"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->OGHAM:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1138
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "RUNIC"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->RUNIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1143
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "KHMER"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KHMER:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1148
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "MONGOLIAN"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MONGOLIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1153
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LATIN_EXTENDED_ADDITIONAL"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LATIN_EXTENDED_ADDITIONAL:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1158
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "GREEK_EXTENDED"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GREEK_EXTENDED:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1163
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "GENERAL_PUNCTUATION"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GENERAL_PUNCTUATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1168
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SUPERSCRIPTS_AND_SUBSCRIPTS"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUPERSCRIPTS_AND_SUBSCRIPTS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1173
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CURRENCY_SYMBOLS"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CURRENCY_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1180
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "COMBINING_MARKS_FOR_SYMBOLS"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->COMBINING_MARKS_FOR_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1185
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LETTERLIKE_SYMBOLS"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LETTERLIKE_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1190
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "NUMBER_FORMS"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->NUMBER_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1195
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ARROWS"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ARROWS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1200
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "MATHEMATICAL_OPERATORS"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MATHEMATICAL_OPERATORS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1205
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "MISCELLANEOUS_TECHNICAL"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MISCELLANEOUS_TECHNICAL:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1210
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CONTROL_PICTURES"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CONTROL_PICTURES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1215
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "OPTICAL_CHARACTER_RECOGNITION"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->OPTICAL_CHARACTER_RECOGNITION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1220
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ENCLOSED_ALPHANUMERICS"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ENCLOSED_ALPHANUMERICS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1225
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "BOX_DRAWING"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BOX_DRAWING:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1230
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "BLOCK_ELEMENTS"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BLOCK_ELEMENTS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1235
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "GEOMETRIC_SHAPES"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GEOMETRIC_SHAPES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1240
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "MISCELLANEOUS_SYMBOLS"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MISCELLANEOUS_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1245
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "DINGBATS"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->DINGBATS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1250
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "BRAILLE_PATTERNS"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BRAILLE_PATTERNS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1255
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CJK_RADICALS_SUPPLEMENT"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_RADICALS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1260
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "KANGXI_RADICALS"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KANGXI_RADICALS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1265
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "IDEOGRAPHIC_DESCRIPTION_CHARACTERS"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->IDEOGRAPHIC_DESCRIPTION_CHARACTERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1270
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CJK_SYMBOLS_AND_PUNCTUATION"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_SYMBOLS_AND_PUNCTUATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1275
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "HIRAGANA"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HIRAGANA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1280
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "KATAKANA"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KATAKANA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1285
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "BOPOMOFO"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BOPOMOFO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1290
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "HANGUL_COMPATIBILITY_JAMO"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HANGUL_COMPATIBILITY_JAMO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1295
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "KANBUN"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KANBUN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1300
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "BOPOMOFO_EXTENDED"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BOPOMOFO_EXTENDED:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1305
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ENCLOSED_CJK_LETTERS_AND_MONTHS"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ENCLOSED_CJK_LETTERS_AND_MONTHS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1310
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CJK_COMPATIBILITY"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_COMPATIBILITY:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1315
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1320
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CJK_UNIFIED_IDEOGRAPHS"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1325
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "YI_SYLLABLES"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->YI_SYLLABLES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1330
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "YI_RADICALS"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->YI_RADICALS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1335
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "HANGUL_SYLLABLES"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HANGUL_SYLLABLES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1340
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "HIGH_SURROGATES"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HIGH_SURROGATES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1345
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "HIGH_PRIVATE_USE_SURROGATES"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HIGH_PRIVATE_USE_SURROGATES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1350
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LOW_SURROGATES"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LOW_SURROGATES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1360
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "PRIVATE_USE_AREA"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->PRIVATE_USE_AREA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1370
    sget-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->PRIVATE_USE_AREA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->PRIVATE_USE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1375
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CJK_COMPATIBILITY_IDEOGRAPHS"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1380
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ALPHABETIC_PRESENTATION_FORMS"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ALPHABETIC_PRESENTATION_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1385
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ARABIC_PRESENTATION_FORMS_A"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ARABIC_PRESENTATION_FORMS_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1390
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "COMBINING_HALF_MARKS"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->COMBINING_HALF_MARKS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1395
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CJK_COMPATIBILITY_FORMS"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_COMPATIBILITY_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1400
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SMALL_FORM_VARIANTS"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SMALL_FORM_VARIANTS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1405
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ARABIC_PRESENTATION_FORMS_B"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ARABIC_PRESENTATION_FORMS_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1410
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SPECIALS"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SPECIALS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1415
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "HALFWIDTH_AND_FULLWIDTH_FORMS"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HALFWIDTH_AND_FULLWIDTH_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1420
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "OLD_ITALIC"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->OLD_ITALIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1425
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "GOTHIC"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GOTHIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1430
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "DESERET"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->DESERET:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1435
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "BYZANTINE_MUSICAL_SYMBOLS"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BYZANTINE_MUSICAL_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1440
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "MUSICAL_SYMBOLS"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MUSICAL_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1445
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "MATHEMATICAL_ALPHANUMERIC_SYMBOLS"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MATHEMATICAL_ALPHANUMERIC_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1450
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1456
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1461
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "TAGS"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TAGS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1470
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CYRILLIC_SUPPLEMENTARY"

    invoke-direct {v0, v1, v3}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CYRILLIC_SUPPLEMENTARY:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1476
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CYRILLIC_SUPPLEMENT"

    invoke-direct {v0, v1, v3}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CYRILLIC_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1481
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "TAGALOG"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TAGALOG:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1486
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "HANUNOO"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HANUNOO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1491
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "BUHID"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BUHID:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1496
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "TAGBANWA"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TAGBANWA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1501
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1506
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SUPPLEMENTAL_ARROWS_A"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUPPLEMENTAL_ARROWS_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1511
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SUPPLEMENTAL_ARROWS_B"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUPPLEMENTAL_ARROWS_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1516
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1521
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SUPPLEMENTAL_MATHEMATICAL_OPERATORS"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUPPLEMENTAL_MATHEMATICAL_OPERATORS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1526
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "KATAKANA_PHONETIC_EXTENSIONS"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KATAKANA_PHONETIC_EXTENSIONS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1531
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "VARIATION_SELECTORS"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->VARIATION_SELECTORS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1536
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SUPPLEMENTARY_PRIVATE_USE_AREA_A"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUPPLEMENTARY_PRIVATE_USE_AREA_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1541
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SUPPLEMENTARY_PRIVATE_USE_AREA_B"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUPPLEMENTARY_PRIVATE_USE_AREA_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1547
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LIMBU"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LIMBU:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1552
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "TAI_LE"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TAI_LE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1557
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "KHMER_SYMBOLS"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KHMER_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1563
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "PHONETIC_EXTENSIONS"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->PHONETIC_EXTENSIONS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1569
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "MISCELLANEOUS_SYMBOLS_AND_ARROWS"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MISCELLANEOUS_SYMBOLS_AND_ARROWS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1574
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "YIJING_HEXAGRAM_SYMBOLS"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->YIJING_HEXAGRAM_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1579
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LINEAR_B_SYLLABARY"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LINEAR_B_SYLLABARY:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1584
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LINEAR_B_IDEOGRAMS"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LINEAR_B_IDEOGRAMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1589
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "AEGEAN_NUMBERS"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->AEGEAN_NUMBERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1594
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "UGARITIC"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->UGARITIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1599
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SHAVIAN"

    const/16 v2, 0x79

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SHAVIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1604
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "OSMANYA"

    const/16 v2, 0x7a

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->OSMANYA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1609
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CYPRIOT_SYLLABARY"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CYPRIOT_SYLLABARY:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1614
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "TAI_XUAN_JING_SYMBOLS"

    const/16 v2, 0x7c

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TAI_XUAN_JING_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1620
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "VARIATION_SELECTORS_SUPPLEMENT"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->VARIATION_SELECTORS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1628
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ANCIENT_GREEK_MUSICAL_NOTATION"

    const/16 v2, 0x7e

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ANCIENT_GREEK_MUSICAL_NOTATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1633
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ANCIENT_GREEK_NUMBERS"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ANCIENT_GREEK_NUMBERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1638
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ARABIC_SUPPLEMENT"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ARABIC_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1643
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "BUGINESE"

    const/16 v2, 0x81

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BUGINESE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1648
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CJK_STROKES"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_STROKES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1653
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "COMBINING_DIACRITICAL_MARKS_SUPPLEMENT"

    const/16 v2, 0x83

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->COMBINING_DIACRITICAL_MARKS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1658
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "COPTIC"

    const/16 v2, 0x84

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->COPTIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1663
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ETHIOPIC_EXTENDED"

    const/16 v2, 0x85

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ETHIOPIC_EXTENDED:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1668
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ETHIOPIC_SUPPLEMENT"

    const/16 v2, 0x86

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ETHIOPIC_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1673
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "GEORGIAN_SUPPLEMENT"

    const/16 v2, 0x87

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GEORGIAN_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1678
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "GLAGOLITIC"

    const/16 v2, 0x88

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GLAGOLITIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1683
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "KHAROSHTHI"

    const/16 v2, 0x89

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KHAROSHTHI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1688
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "MODIFIER_TONE_LETTERS"

    const/16 v2, 0x8a

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MODIFIER_TONE_LETTERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1693
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "NEW_TAI_LUE"

    const/16 v2, 0x8b

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->NEW_TAI_LUE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1698
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "OLD_PERSIAN"

    const/16 v2, 0x8c

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->OLD_PERSIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1703
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "PHONETIC_EXTENSIONS_SUPPLEMENT"

    const/16 v2, 0x8d

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->PHONETIC_EXTENSIONS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1708
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SUPPLEMENTAL_PUNCTUATION"

    const/16 v2, 0x8e

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUPPLEMENTAL_PUNCTUATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1713
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SYLOTI_NAGRI"

    const/16 v2, 0x8f

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SYLOTI_NAGRI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1718
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "TIFINAGH"

    const/16 v2, 0x90

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TIFINAGH:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1723
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "VERTICAL_FORMS"

    const/16 v2, 0x91

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->VERTICAL_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1728
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "NKO"

    const/16 v2, 0x92

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->NKO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1732
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "BALINESE"

    const/16 v2, 0x93

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BALINESE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1736
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LATIN_EXTENDED_C"

    const/16 v2, 0x94

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LATIN_EXTENDED_C:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1740
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LATIN_EXTENDED_D"

    const/16 v2, 0x95

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LATIN_EXTENDED_D:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1744
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "PHAGS_PA"

    const/16 v2, 0x96

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->PHAGS_PA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1748
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "PHOENICIAN"

    const/16 v2, 0x97

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->PHOENICIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1752
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CUNEIFORM"

    const/16 v2, 0x98

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CUNEIFORM:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1756
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CUNEIFORM_NUMBERS_AND_PUNCTUATION"

    const/16 v2, 0x99

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CUNEIFORM_NUMBERS_AND_PUNCTUATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1760
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "COUNTING_ROD_NUMERALS"

    const/16 v2, 0x9a

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->COUNTING_ROD_NUMERALS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1766
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SUNDANESE"

    const/16 v2, 0x9b

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUNDANESE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1772
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LEPCHA"

    const/16 v2, 0x9c

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LEPCHA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1778
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "OL_CHIKI"

    const/16 v2, 0x9d

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->OL_CHIKI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1784
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CYRILLIC_EXTENDED_A"

    const/16 v2, 0x9e

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CYRILLIC_EXTENDED_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1790
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "VAI"

    const/16 v2, 0x9f

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->VAI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1796
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CYRILLIC_EXTENDED_B"

    const/16 v2, 0xa0

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CYRILLIC_EXTENDED_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1802
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "SAURASHTRA"

    const/16 v2, 0xa1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SAURASHTRA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1808
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "KAYAH_LI"

    const/16 v2, 0xa2

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KAYAH_LI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1814
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "REJANG"

    const/16 v2, 0xa3

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->REJANG:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1820
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CHAM"

    const/16 v2, 0xa4

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CHAM:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1826
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "ANCIENT_SYMBOLS"

    const/16 v2, 0xa5

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ANCIENT_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1832
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "PHAISTOS_DISC"

    const/16 v2, 0xa6

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->PHAISTOS_DISC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1838
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LYCIAN"

    const/16 v2, 0xa7

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LYCIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1844
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "CARIAN"

    const/16 v2, 0xa8

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CARIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1850
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "LYDIAN"

    const/16 v2, 0xa9

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LYDIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1856
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "MAHJONG_TILES"

    const/16 v2, 0xaa

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MAHJONG_TILES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1862
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "DOMINO_TILES"

    const/16 v2, 0xab

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->DOMINO_TILES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1866
    new-instance v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    const-string/jumbo v1, "INVALID_CODE"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->INVALID_CODE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1979
    const/16 v0, 0xac

    new-array v0, v0, [Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    sget-object v1, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->NO_BLOCK:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v1, v0, v4

    sget-object v1, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BASIC_LATIN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v1, v0, v5

    sget-object v1, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LATIN_1_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v1, v0, v6

    sget-object v1, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LATIN_EXTENDED_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LATIN_EXTENDED_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->IPA_EXTENSIONS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SPACING_MODIFIER_LETTERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->COMBINING_DIACRITICAL_MARKS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GREEK:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CYRILLIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ARMENIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HEBREW:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ARABIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SYRIAC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->THAANA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->DEVANAGARI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BENGALI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GURMUKHI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GUJARATI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ORIYA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TAMIL:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TELUGU:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KANNADA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MALAYALAM:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SINHALA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->THAI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LAO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TIBETAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MYANMAR:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GEORGIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HANGUL_JAMO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ETHIOPIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CHEROKEE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->OGHAM:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->RUNIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KHMER:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MONGOLIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LATIN_EXTENDED_ADDITIONAL:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GREEK_EXTENDED:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GENERAL_PUNCTUATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUPERSCRIPTS_AND_SUBSCRIPTS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CURRENCY_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->COMBINING_MARKS_FOR_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LETTERLIKE_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->NUMBER_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ARROWS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MATHEMATICAL_OPERATORS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MISCELLANEOUS_TECHNICAL:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CONTROL_PICTURES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->OPTICAL_CHARACTER_RECOGNITION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ENCLOSED_ALPHANUMERICS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BOX_DRAWING:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BLOCK_ELEMENTS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GEOMETRIC_SHAPES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MISCELLANEOUS_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->DINGBATS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BRAILLE_PATTERNS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_RADICALS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KANGXI_RADICALS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->IDEOGRAPHIC_DESCRIPTION_CHARACTERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_SYMBOLS_AND_PUNCTUATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HIRAGANA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KATAKANA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BOPOMOFO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HANGUL_COMPATIBILITY_JAMO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KANBUN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BOPOMOFO_EXTENDED:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ENCLOSED_CJK_LETTERS_AND_MONTHS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_COMPATIBILITY:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->YI_SYLLABLES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->YI_RADICALS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HANGUL_SYLLABLES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HIGH_SURROGATES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HIGH_PRIVATE_USE_SURROGATES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LOW_SURROGATES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->PRIVATE_USE_AREA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ALPHABETIC_PRESENTATION_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ARABIC_PRESENTATION_FORMS_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->COMBINING_HALF_MARKS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_COMPATIBILITY_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SMALL_FORM_VARIANTS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ARABIC_PRESENTATION_FORMS_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SPECIALS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HALFWIDTH_AND_FULLWIDTH_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->OLD_ITALIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GOTHIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->DESERET:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BYZANTINE_MUSICAL_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MUSICAL_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MATHEMATICAL_ALPHANUMERIC_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TAGS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    sget-object v1, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CYRILLIC_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v1, v0, v3

    const/16 v1, 0x62

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TAGALOG:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->HANUNOO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BUHID:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TAGBANWA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUPPLEMENTAL_ARROWS_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUPPLEMENTAL_ARROWS_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUPPLEMENTAL_MATHEMATICAL_OPERATORS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KATAKANA_PHONETIC_EXTENSIONS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->VARIATION_SELECTORS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUPPLEMENTARY_PRIVATE_USE_AREA_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUPPLEMENTARY_PRIVATE_USE_AREA_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LIMBU:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TAI_LE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KHMER_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->PHONETIC_EXTENSIONS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MISCELLANEOUS_SYMBOLS_AND_ARROWS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->YIJING_HEXAGRAM_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LINEAR_B_SYLLABARY:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LINEAR_B_IDEOGRAMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->AEGEAN_NUMBERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->UGARITIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SHAVIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->OSMANYA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CYPRIOT_SYLLABARY:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TAI_XUAN_JING_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->VARIATION_SELECTORS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ANCIENT_GREEK_MUSICAL_NOTATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ANCIENT_GREEK_NUMBERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ARABIC_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BUGINESE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CJK_STROKES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->COMBINING_DIACRITICAL_MARKS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->COPTIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ETHIOPIC_EXTENDED:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ETHIOPIC_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GEORGIAN_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->GLAGOLITIC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x89

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KHAROSHTHI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MODIFIER_TONE_LETTERS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->NEW_TAI_LUE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->OLD_PERSIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->PHONETIC_EXTENSIONS_SUPPLEMENT:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUPPLEMENTAL_PUNCTUATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SYLOTI_NAGRI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x90

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->TIFINAGH:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x91

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->VERTICAL_FORMS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x92

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->NKO:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x93

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BALINESE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x94

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LATIN_EXTENDED_C:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x95

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LATIN_EXTENDED_D:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x96

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->PHAGS_PA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x97

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->PHOENICIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x98

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CUNEIFORM:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x99

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CUNEIFORM_NUMBERS_AND_PUNCTUATION:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->COUNTING_ROD_NUMERALS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SUNDANESE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LEPCHA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->OL_CHIKI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CYRILLIC_EXTENDED_A:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->VAI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CYRILLIC_EXTENDED_B:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->SAURASHTRA:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->KAYAH_LI:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->REJANG:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CHAM:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->ANCIENT_SYMBOLS:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->PHAISTOS_DISC:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LYCIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->CARIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->LYDIAN:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->MAHJONG_TILES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    const/16 v1, 0xab

    sget-object v2, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->DOMINO_TILES:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BLOCKS_:[Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 2096
    const/16 v0, 0xac

    sget-object v1, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BLOCKS_:[Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    array-length v1, v1

    if-eq v0, v1, :cond_0

    .line 2097
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "UnicodeBlock fields are inconsistent!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2099
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "id"    # I

    .prologue
    .line 2115
    invoke-direct {p0, p1}, Ljava/lang/Character$Subset;-><init>(Ljava/lang/String;)V

    .line 2116
    iput p2, p0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->m_id_:I

    .line 2117
    return-void
.end method

.method public static final forName(Ljava/lang/String;)Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;
    .locals 7
    .param p0, "blockName"    # Ljava/lang/String;

    .prologue
    .line 1931
    const/4 v2, 0x0

    .line 1932
    .local v2, "m":Ljava/util/Map;
    sget-object v4, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->mref:Ljava/lang/ref/SoftReference;

    if-eqz v4, :cond_0

    .line 1933
    sget-object v4, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->mref:Ljava/lang/ref/SoftReference;

    invoke-virtual {v4}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "m":Ljava/util/Map;
    check-cast v2, Ljava/util/Map;

    .line 1935
    .restart local v2    # "m":Ljava/util/Map;
    :cond_0
    if-nez v2, :cond_2

    .line 1936
    new-instance v2, Ljava/util/HashMap;

    .end local v2    # "m":Ljava/util/Map;
    sget-object v4, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BLOCKS_:[Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    array-length v4, v4

    invoke-direct {v2, v4}, Ljava/util/HashMap;-><init>(I)V

    .line 1937
    .restart local v2    # "m":Ljava/util/Map;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v4, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BLOCKS_:[Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 1938
    sget-object v4, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BLOCKS_:[Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aget-object v0, v4, v1

    .line 1939
    .local v0, "b":Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;
    const/16 v4, 0x1001

    invoke-virtual {v0}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->getID()I

    move-result v5

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Lcom/ibm/icu/lang/UCharacter;->getPropertyValueName(III)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->trimBlockName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1940
    .local v3, "name":Ljava/lang/String;
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1937
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1942
    .end local v0    # "b":Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;
    .end local v3    # "name":Ljava/lang/String;
    :cond_1
    new-instance v4, Ljava/lang/ref/SoftReference;

    invoke-direct {v4, v2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    sput-object v4, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->mref:Ljava/lang/ref/SoftReference;

    .line 1944
    .end local v1    # "i":I
    :cond_2
    invoke-static {p0}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->trimBlockName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1945
    .restart local v0    # "b":Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;
    if-nez v0, :cond_3

    .line 1946
    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    .line 1948
    :cond_3
    return-object v0
.end method

.method public static getInstance(I)Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;
    .locals 1
    .param p0, "id"    # I

    .prologue
    .line 1882
    if-ltz p0, :cond_0

    sget-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BLOCKS_:[Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    array-length v0, v0

    if-ge p0, v0, :cond_0

    .line 1883
    sget-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->BLOCKS_:[Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    aget-object v0, v0, p0

    .line 1885
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->INVALID_CODE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    goto :goto_0
.end method

.method static idOf(I)I
    .locals 2
    .param p0, "ch"    # I

    .prologue
    .line 1913
    if-ltz p0, :cond_0

    const v0, 0x10ffff

    if-le p0, v0, :cond_1

    .line 1914
    :cond_0
    const/4 v0, -0x1

    .line 1917
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lcom/ibm/icu/lang/UCharacter;->access$000()Lcom/ibm/icu/impl/UCharacterProperty;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->getAdditional(II)I

    move-result v0

    const v1, 0x1ff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    goto :goto_0
.end method

.method public static of(I)Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;
    .locals 2
    .param p0, "ch"    # I

    .prologue
    .line 1897
    const v0, 0x10ffff

    if-le p0, v0, :cond_0

    .line 1898
    sget-object v0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->INVALID_CODE:Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    .line 1901
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/ibm/icu/lang/UCharacter;->access$000()Lcom/ibm/icu/impl/UCharacterProperty;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->getAdditional(II)I

    move-result v0

    const v1, 0x1ff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->getInstance(I)Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;

    move-result-object v0

    goto :goto_0
.end method

.method private static trimBlockName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1953
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 1954
    .local v3, "upper":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 1955
    .local v2, "result":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 1956
    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1957
    .local v0, "c":C
    const/16 v4, 0x20

    if-eq v0, v4, :cond_0

    const/16 v4, 0x5f

    if-eq v0, v4, :cond_0

    const/16 v4, 0x2d

    if-eq v0, v4, :cond_0

    .line 1958
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1955
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1961
    .end local v0    # "c":C
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public getID()I
    .locals 1

    .prologue
    .line 1971
    iget v0, p0, Lcom/ibm/icu/lang/UCharacter$UnicodeBlock;->m_id_:I

    return v0
.end method
