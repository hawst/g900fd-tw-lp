.class public final Lcom/ibm/icu/lang/UCharacterCategory;
.super Ljava/lang/Object;
.source "UCharacterCategory.java"

# interfaces
.implements Lcom/ibm/icu/lang/UCharacterEnums$ECharacterCategory;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    return-void
.end method

.method public static toString(I)Ljava/lang/String;
    .locals 1
    .param p0, "category"    # I

    .prologue
    .line 40
    packed-switch p0, :pswitch_data_0

    .line 100
    const-string/jumbo v0, "Unassigned"

    :goto_0
    return-object v0

    .line 42
    :pswitch_0
    const-string/jumbo v0, "Letter, Uppercase"

    goto :goto_0

    .line 44
    :pswitch_1
    const-string/jumbo v0, "Letter, Lowercase"

    goto :goto_0

    .line 46
    :pswitch_2
    const-string/jumbo v0, "Letter, Titlecase"

    goto :goto_0

    .line 48
    :pswitch_3
    const-string/jumbo v0, "Letter, Modifier"

    goto :goto_0

    .line 50
    :pswitch_4
    const-string/jumbo v0, "Letter, Other"

    goto :goto_0

    .line 52
    :pswitch_5
    const-string/jumbo v0, "Mark, Non-Spacing"

    goto :goto_0

    .line 54
    :pswitch_6
    const-string/jumbo v0, "Mark, Enclosing"

    goto :goto_0

    .line 56
    :pswitch_7
    const-string/jumbo v0, "Mark, Spacing Combining"

    goto :goto_0

    .line 58
    :pswitch_8
    const-string/jumbo v0, "Number, Decimal Digit"

    goto :goto_0

    .line 60
    :pswitch_9
    const-string/jumbo v0, "Number, Letter"

    goto :goto_0

    .line 62
    :pswitch_a
    const-string/jumbo v0, "Number, Other"

    goto :goto_0

    .line 64
    :pswitch_b
    const-string/jumbo v0, "Separator, Space"

    goto :goto_0

    .line 66
    :pswitch_c
    const-string/jumbo v0, "Separator, Line"

    goto :goto_0

    .line 68
    :pswitch_d
    const-string/jumbo v0, "Separator, Paragraph"

    goto :goto_0

    .line 70
    :pswitch_e
    const-string/jumbo v0, "Other, Control"

    goto :goto_0

    .line 72
    :pswitch_f
    const-string/jumbo v0, "Other, Format"

    goto :goto_0

    .line 74
    :pswitch_10
    const-string/jumbo v0, "Other, Private Use"

    goto :goto_0

    .line 76
    :pswitch_11
    const-string/jumbo v0, "Other, Surrogate"

    goto :goto_0

    .line 78
    :pswitch_12
    const-string/jumbo v0, "Punctuation, Dash"

    goto :goto_0

    .line 80
    :pswitch_13
    const-string/jumbo v0, "Punctuation, Open"

    goto :goto_0

    .line 82
    :pswitch_14
    const-string/jumbo v0, "Punctuation, Close"

    goto :goto_0

    .line 84
    :pswitch_15
    const-string/jumbo v0, "Punctuation, Connector"

    goto :goto_0

    .line 86
    :pswitch_16
    const-string/jumbo v0, "Punctuation, Other"

    goto :goto_0

    .line 88
    :pswitch_17
    const-string/jumbo v0, "Symbol, Math"

    goto :goto_0

    .line 90
    :pswitch_18
    const-string/jumbo v0, "Symbol, Currency"

    goto :goto_0

    .line 92
    :pswitch_19
    const-string/jumbo v0, "Symbol, Modifier"

    goto :goto_0

    .line 94
    :pswitch_1a
    const-string/jumbo v0, "Symbol, Other"

    goto :goto_0

    .line 96
    :pswitch_1b
    const-string/jumbo v0, "Punctuation, Initial quote"

    goto :goto_0

    .line 98
    :pswitch_1c
    const-string/jumbo v0, "Punctuation, Final quote"

    goto :goto_0

    .line 40
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch
.end method
