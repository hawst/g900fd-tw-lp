.class public interface abstract Lcom/ibm/icu/lang/UCharacter$JoiningGroup;
.super Ljava/lang/Object;
.source "UCharacter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/lang/UCharacter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "JoiningGroup"
.end annotation


# static fields
.field public static final AIN:I = 0x1

.field public static final ALAPH:I = 0x2

.field public static final ALEF:I = 0x3

.field public static final BEH:I = 0x4

.field public static final BETH:I = 0x5

.field public static final BURUSHASKI_YEH_BARREE:I = 0x36

.field public static final COUNT:I = 0x37

.field public static final DAL:I = 0x6

.field public static final DALATH_RISH:I = 0x7

.field public static final E:I = 0x8

.field public static final FE:I = 0x33

.field public static final FEH:I = 0x9

.field public static final FINAL_SEMKATH:I = 0xa

.field public static final GAF:I = 0xb

.field public static final GAMAL:I = 0xc

.field public static final HAH:I = 0xd

.field public static final HAMZA_ON_HEH_GOAL:I = 0xe

.field public static final HE:I = 0xf

.field public static final HEH:I = 0x10

.field public static final HEH_GOAL:I = 0x11

.field public static final HETH:I = 0x12

.field public static final KAF:I = 0x13

.field public static final KAPH:I = 0x14

.field public static final KHAPH:I = 0x34

.field public static final KNOTTED_HEH:I = 0x15

.field public static final LAM:I = 0x16

.field public static final LAMADH:I = 0x17

.field public static final MEEM:I = 0x18

.field public static final MIM:I = 0x19

.field public static final NOON:I = 0x1a

.field public static final NO_JOINING_GROUP:I = 0x0

.field public static final NUN:I = 0x1b

.field public static final PE:I = 0x1c

.field public static final QAF:I = 0x1d

.field public static final QAPH:I = 0x1e

.field public static final REH:I = 0x1f

.field public static final REVERSED_PE:I = 0x20

.field public static final SAD:I = 0x21

.field public static final SADHE:I = 0x22

.field public static final SEEN:I = 0x23

.field public static final SEMKATH:I = 0x24

.field public static final SHIN:I = 0x25

.field public static final SWASH_KAF:I = 0x26

.field public static final SYRIAC_WAW:I = 0x27

.field public static final TAH:I = 0x28

.field public static final TAW:I = 0x29

.field public static final TEH_MARBUTA:I = 0x2a

.field public static final TETH:I = 0x2b

.field public static final WAW:I = 0x2c

.field public static final YEH:I = 0x2d

.field public static final YEH_BARREE:I = 0x2e

.field public static final YEH_WITH_TAIL:I = 0x2f

.field public static final YUDH:I = 0x30

.field public static final YUDH_HE:I = 0x31

.field public static final ZAIN:I = 0x32

.field public static final ZHAIN:I = 0x35
