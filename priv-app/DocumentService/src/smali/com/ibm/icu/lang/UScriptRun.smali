.class public final Lcom/ibm/icu/lang/UScriptRun;
.super Ljava/lang/Object;
.source "UScriptRun.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;
    }
.end annotation


# static fields
.field private static PAREN_STACK_DEPTH:I

.field private static pairedCharExtra:I

.field private static pairedCharPower:I

.field private static pairedChars:[I

.field private static parenStack:[Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;


# instance fields
.field private emptyCharArray:[C

.field private fixupCount:I

.field private parenSP:I

.field private pushCount:I

.field private scriptCode:I

.field private scriptLimit:I

.field private scriptStart:I

.field private text:[C

.field private textIndex:I

.field private textLimit:I

.field private textStart:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 526
    const/16 v0, 0x20

    sput v0, Lcom/ibm/icu/lang/UScriptRun;->PAREN_STACK_DEPTH:I

    .line 527
    sget v0, Lcom/ibm/icu/lang/UScriptRun;->PAREN_STACK_DEPTH:I

    new-array v0, v0, [Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;

    sput-object v0, Lcom/ibm/icu/lang/UScriptRun;->parenStack:[Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;

    .line 605
    const/16 v0, 0x22

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/lang/UScriptRun;->pairedChars:[I

    .line 625
    const/4 v0, 0x1

    sget-object v1, Lcom/ibm/icu/lang/UScriptRun;->pairedChars:[I

    array-length v1, v1

    invoke-static {v1}, Lcom/ibm/icu/lang/UScriptRun;->highBit(I)B

    move-result v1

    shl-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/lang/UScriptRun;->pairedCharPower:I

    .line 626
    sget-object v0, Lcom/ibm/icu/lang/UScriptRun;->pairedChars:[I

    array-length v0, v0

    sget v1, Lcom/ibm/icu/lang/UScriptRun;->pairedCharPower:I

    sub-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/lang/UScriptRun;->pairedCharExtra:I

    return-void

    .line 605
    nop

    :array_0
    .array-data 4
        0x28
        0x29
        0x3c
        0x3e
        0x5b
        0x5d
        0x7b
        0x7d
        0xab
        0xbb
        0x2018
        0x2019
        0x201c
        0x201d
        0x2039
        0x203a
        0x3008
        0x3009
        0x300a
        0x300b
        0x300c
        0x300d
        0x300e
        0x300f
        0x3010
        0x3011
        0x3014
        0x3015
        0x3016
        0x3017
        0x3018
        0x3019
        0x301a
        0x301b
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 514
    new-array v1, v2, [C

    iput-object v1, p0, Lcom/ibm/icu/lang/UScriptRun;->emptyCharArray:[C

    .line 528
    const/4 v1, -0x1

    iput v1, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    .line 529
    iput v2, p0, Lcom/ibm/icu/lang/UScriptRun;->pushCount:I

    .line 530
    iput v2, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    .line 63
    const/4 v0, 0x0

    .line 65
    .local v0, "nullChars":[C
    invoke-virtual {p0, v0, v2, v2}, Lcom/ibm/icu/lang/UScriptRun;->reset([CII)V

    .line 66
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 514
    new-array v0, v1, [C

    iput-object v0, p0, Lcom/ibm/icu/lang/UScriptRun;->emptyCharArray:[C

    .line 528
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    .line 529
    iput v1, p0, Lcom/ibm/icu/lang/UScriptRun;->pushCount:I

    .line 530
    iput v1, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    .line 79
    invoke-virtual {p0, p1}, Lcom/ibm/icu/lang/UScriptRun;->reset(Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "count"    # I

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 514
    new-array v0, v1, [C

    iput-object v0, p0, Lcom/ibm/icu/lang/UScriptRun;->emptyCharArray:[C

    .line 528
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    .line 529
    iput v1, p0, Lcom/ibm/icu/lang/UScriptRun;->pushCount:I

    .line 530
    iput v1, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    .line 95
    invoke-virtual {p0, p1, p2, p3}, Lcom/ibm/icu/lang/UScriptRun;->reset(Ljava/lang/String;II)V

    .line 96
    return-void
.end method

.method public constructor <init>([C)V
    .locals 2
    .param p1, "chars"    # [C

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 514
    new-array v0, v1, [C

    iput-object v0, p0, Lcom/ibm/icu/lang/UScriptRun;->emptyCharArray:[C

    .line 528
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    .line 529
    iput v1, p0, Lcom/ibm/icu/lang/UScriptRun;->pushCount:I

    .line 530
    iput v1, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    .line 109
    invoke-virtual {p0, p1}, Lcom/ibm/icu/lang/UScriptRun;->reset([C)V

    .line 110
    return-void
.end method

.method public constructor <init>([CII)V
    .locals 2
    .param p1, "chars"    # [C
    .param p2, "start"    # I
    .param p3, "count"    # I

    .prologue
    const/4 v1, 0x0

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 514
    new-array v0, v1, [C

    iput-object v0, p0, Lcom/ibm/icu/lang/UScriptRun;->emptyCharArray:[C

    .line 528
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    .line 529
    iput v1, p0, Lcom/ibm/icu/lang/UScriptRun;->pushCount:I

    .line 530
    iput v1, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    .line 125
    invoke-virtual {p0, p1, p2, p3}, Lcom/ibm/icu/lang/UScriptRun;->reset([CII)V

    .line 126
    return-void
.end method

.method private static final dec(I)I
    .locals 1
    .param p0, "sp"    # I

    .prologue
    .line 440
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/ibm/icu/lang/UScriptRun;->dec(II)I

    move-result v0

    return v0
.end method

.method private static final dec(II)I
    .locals 1
    .param p0, "sp"    # I
    .param p1, "count"    # I

    .prologue
    .line 435
    sget v0, Lcom/ibm/icu/lang/UScriptRun;->PAREN_STACK_DEPTH:I

    add-int/2addr v0, p0

    sub-int/2addr v0, p1

    invoke-static {v0}, Lcom/ibm/icu/lang/UScriptRun;->mod(I)I

    move-result v0

    return v0
.end method

.method private final fixup(I)V
    .locals 3
    .param p1, "scrptCode"    # I

    .prologue
    .line 506
    iget v1, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    iget v2, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    invoke-static {v1, v2}, Lcom/ibm/icu/lang/UScriptRun;->dec(II)I

    move-result v0

    .line 508
    .local v0, "fixupSP":I
    :goto_0
    iget v1, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    if-lez v1, :cond_0

    .line 509
    invoke-static {v0}, Lcom/ibm/icu/lang/UScriptRun;->inc(I)I

    move-result v0

    .line 510
    sget-object v1, Lcom/ibm/icu/lang/UScriptRun;->parenStack:[Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;

    aget-object v1, v1, v0

    iput p1, v1, Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;->scriptCode:I

    goto :goto_0

    .line 512
    :cond_0
    return-void
.end method

.method private static getPairIndex(I)I
    .locals 4
    .param p0, "ch"    # I

    .prologue
    .line 583
    sget v1, Lcom/ibm/icu/lang/UScriptRun;->pairedCharPower:I

    .line 584
    .local v1, "probe":I
    const/4 v0, 0x0

    .line 586
    .local v0, "index":I
    sget-object v2, Lcom/ibm/icu/lang/UScriptRun;->pairedChars:[I

    sget v3, Lcom/ibm/icu/lang/UScriptRun;->pairedCharExtra:I

    aget v2, v2, v3

    if-lt p0, v2, :cond_0

    .line 587
    sget v0, Lcom/ibm/icu/lang/UScriptRun;->pairedCharExtra:I

    .line 590
    :cond_0
    :goto_0
    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 591
    shr-int/lit8 v1, v1, 0x1

    .line 593
    sget-object v2, Lcom/ibm/icu/lang/UScriptRun;->pairedChars:[I

    add-int v3, v0, v1

    aget v2, v2, v3

    if-lt p0, v2, :cond_0

    .line 594
    add-int/2addr v0, v1

    .line 595
    goto :goto_0

    .line 598
    :cond_1
    sget-object v2, Lcom/ibm/icu/lang/UScriptRun;->pairedChars:[I

    aget v2, v2, v0

    if-eq v2, p0, :cond_2

    .line 599
    const/4 v0, -0x1

    .line 602
    :cond_2
    return v0
.end method

.method private static final highBit(I)B
    .locals 3
    .param p0, "n"    # I

    .prologue
    const/16 v2, 0x10

    .line 541
    if-gtz p0, :cond_1

    .line 542
    const/16 v0, -0x20

    .line 572
    :cond_0
    :goto_0
    return v0

    .line 545
    :cond_1
    const/4 v0, 0x0

    .line 547
    .local v0, "bit":B
    const/high16 v1, 0x10000

    if-lt p0, v1, :cond_2

    .line 548
    shr-int/lit8 p0, p0, 0x10

    .line 549
    int-to-byte v0, v2

    .line 552
    :cond_2
    const/16 v1, 0x100

    if-lt p0, v1, :cond_3

    .line 553
    shr-int/lit8 p0, p0, 0x8

    .line 554
    add-int/lit8 v1, v0, 0x8

    int-to-byte v0, v1

    .line 557
    :cond_3
    if-lt p0, v2, :cond_4

    .line 558
    shr-int/lit8 p0, p0, 0x4

    .line 559
    add-int/lit8 v1, v0, 0x4

    int-to-byte v0, v1

    .line 562
    :cond_4
    const/4 v1, 0x4

    if-lt p0, v1, :cond_5

    .line 563
    shr-int/lit8 p0, p0, 0x2

    .line 564
    add-int/lit8 v1, v0, 0x2

    int-to-byte v0, v1

    .line 567
    :cond_5
    const/4 v1, 0x2

    if-lt p0, v1, :cond_0

    .line 568
    shr-int/lit8 p0, p0, 0x1

    .line 569
    add-int/lit8 v1, v0, 0x1

    int-to-byte v0, v1

    goto :goto_0
.end method

.method private static final inc(I)I
    .locals 1
    .param p0, "sp"    # I

    .prologue
    .line 430
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/ibm/icu/lang/UScriptRun;->inc(II)I

    move-result v0

    return v0
.end method

.method private static final inc(II)I
    .locals 1
    .param p0, "sp"    # I
    .param p1, "count"    # I

    .prologue
    .line 425
    add-int v0, p0, p1

    invoke-static {v0}, Lcom/ibm/icu/lang/UScriptRun;->mod(I)I

    move-result v0

    return v0
.end method

.method private static final limitInc(I)I
    .locals 1
    .param p0, "count"    # I

    .prologue
    .line 445
    sget v0, Lcom/ibm/icu/lang/UScriptRun;->PAREN_STACK_DEPTH:I

    if-ge p0, v0, :cond_0

    .line 446
    add-int/lit8 p0, p0, 0x1

    .line 449
    :cond_0
    return p0
.end method

.method private static final mod(I)I
    .locals 1
    .param p0, "sp"    # I

    .prologue
    .line 420
    sget v0, Lcom/ibm/icu/lang/UScriptRun;->PAREN_STACK_DEPTH:I

    rem-int v0, p0, v0

    return v0
.end method

.method private final pop()V
    .locals 3

    .prologue
    .line 474
    invoke-direct {p0}, Lcom/ibm/icu/lang/UScriptRun;->stackIsEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 492
    :cond_0
    :goto_0
    return-void

    .line 478
    :cond_1
    sget-object v0, Lcom/ibm/icu/lang/UScriptRun;->parenStack:[Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;

    iget v1, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 480
    iget v0, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    if-lez v0, :cond_2

    .line 481
    iget v0, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    .line 484
    :cond_2
    iget v0, p0, Lcom/ibm/icu/lang/UScriptRun;->pushCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->pushCount:I

    .line 485
    iget v0, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    invoke-static {v0}, Lcom/ibm/icu/lang/UScriptRun;->dec(I)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    .line 489
    invoke-direct {p0}, Lcom/ibm/icu/lang/UScriptRun;->stackIsEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    goto :goto_0
.end method

.method private final push(II)V
    .locals 3
    .param p1, "pairIndex"    # I
    .param p2, "scrptCode"    # I

    .prologue
    .line 464
    iget v0, p0, Lcom/ibm/icu/lang/UScriptRun;->pushCount:I

    invoke-static {v0}, Lcom/ibm/icu/lang/UScriptRun;->limitInc(I)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->pushCount:I

    .line 465
    iget v0, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    invoke-static {v0}, Lcom/ibm/icu/lang/UScriptRun;->limitInc(I)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    .line 467
    iget v0, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    invoke-static {v0}, Lcom/ibm/icu/lang/UScriptRun;->inc(I)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    .line 468
    sget-object v0, Lcom/ibm/icu/lang/UScriptRun;->parenStack:[Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;

    iget v1, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    new-instance v2, Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;

    invoke-direct {v2, p1, p2}, Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;-><init>(II)V

    aput-object v2, v0, v1

    .line 469
    return-void
.end method

.method private static sameScript(II)Z
    .locals 1
    .param p0, "scriptOne"    # I
    .param p1, "scriptTwo"    # I

    .prologue
    const/4 v0, 0x1

    .line 400
    if-le p0, v0, :cond_0

    if-le p1, v0, :cond_0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final stackIsEmpty()Z
    .locals 1

    .prologue
    .line 454
    iget v0, p0, Lcom/ibm/icu/lang/UScriptRun;->pushCount:I

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final stackIsNotEmpty()Z
    .locals 1

    .prologue
    .line 459
    invoke-direct {p0}, Lcom/ibm/icu/lang/UScriptRun;->stackIsEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final syncFixup()V
    .locals 1

    .prologue
    .line 501
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    .line 502
    return-void
.end method

.method private final top()Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;
    .locals 2

    .prologue
    .line 496
    sget-object v0, Lcom/ibm/icu/lang/UScriptRun;->parenStack:[Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;

    iget v1, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    aget-object v0, v0, v1

    return-object v0
.end method


# virtual methods
.method public final getScriptCode()I
    .locals 1

    .prologue
    .line 310
    iget v0, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptCode:I

    return v0
.end method

.method public final getScriptLimit()I
    .locals 1

    .prologue
    .line 296
    iget v0, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptLimit:I

    return v0
.end method

.method public final getScriptStart()I
    .locals 1

    .prologue
    .line 283
    iget v0, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptStart:I

    return v0
.end method

.method public final next()Z
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 325
    iget v7, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptLimit:I

    iget v8, p0, Lcom/ibm/icu/lang/UScriptRun;->textLimit:I

    if-lt v7, v8, :cond_0

    .line 386
    :goto_0
    return v5

    .line 329
    :cond_0
    iput v5, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptCode:I

    .line 330
    iget v5, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptLimit:I

    iput v5, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptStart:I

    .line 332
    invoke-direct {p0}, Lcom/ibm/icu/lang/UScriptRun;->syncFixup()V

    .line 334
    :cond_1
    :goto_1
    iget v5, p0, Lcom/ibm/icu/lang/UScriptRun;->textIndex:I

    iget v7, p0, Lcom/ibm/icu/lang/UScriptRun;->textLimit:I

    if-ge v5, v7, :cond_7

    .line 335
    iget-object v5, p0, Lcom/ibm/icu/lang/UScriptRun;->text:[C

    iget v7, p0, Lcom/ibm/icu/lang/UScriptRun;->textStart:I

    iget v8, p0, Lcom/ibm/icu/lang/UScriptRun;->textLimit:I

    iget v9, p0, Lcom/ibm/icu/lang/UScriptRun;->textIndex:I

    iget v10, p0, Lcom/ibm/icu/lang/UScriptRun;->textStart:I

    sub-int/2addr v9, v10

    invoke-static {v5, v7, v8, v9}, Lcom/ibm/icu/text/UTF16;->charAt([CIII)I

    move-result v0

    .line 336
    .local v0, "ch":I
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v1

    .line 337
    .local v1, "codePointCount":I
    invoke-static {v0}, Lcom/ibm/icu/lang/UScript;->getScript(I)I

    move-result v4

    .line 338
    .local v4, "sc":I
    invoke-static {v0}, Lcom/ibm/icu/lang/UScriptRun;->getPairIndex(I)I

    move-result v2

    .line 340
    .local v2, "pairIndex":I
    iget v5, p0, Lcom/ibm/icu/lang/UScriptRun;->textIndex:I

    add-int/2addr v5, v1

    iput v5, p0, Lcom/ibm/icu/lang/UScriptRun;->textIndex:I

    .line 348
    if-ltz v2, :cond_2

    .line 349
    and-int/lit8 v5, v2, 0x1

    if-nez v5, :cond_4

    .line 350
    iget v5, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptCode:I

    invoke-direct {p0, v2, v5}, Lcom/ibm/icu/lang/UScriptRun;->push(II)V

    .line 364
    :cond_2
    :goto_2
    iget v5, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptCode:I

    invoke-static {v5, v4}, Lcom/ibm/icu/lang/UScriptRun;->sameScript(II)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 365
    iget v5, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptCode:I

    if-gt v5, v6, :cond_3

    if-le v4, v6, :cond_3

    .line 366
    iput v4, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptCode:I

    .line 368
    iget v5, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptCode:I

    invoke-direct {p0, v5}, Lcom/ibm/icu/lang/UScriptRun;->fixup(I)V

    .line 373
    :cond_3
    if-ltz v2, :cond_1

    and-int/lit8 v5, v2, 0x1

    if-eqz v5, :cond_1

    .line 374
    invoke-direct {p0}, Lcom/ibm/icu/lang/UScriptRun;->pop()V

    goto :goto_1

    .line 352
    :cond_4
    and-int/lit8 v3, v2, -0x2

    .line 354
    .local v3, "pi":I
    :goto_3
    invoke-direct {p0}, Lcom/ibm/icu/lang/UScriptRun;->stackIsNotEmpty()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-direct {p0}, Lcom/ibm/icu/lang/UScriptRun;->top()Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;

    move-result-object v5

    iget v5, v5, Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;->pairIndex:I

    if-eq v5, v3, :cond_5

    .line 355
    invoke-direct {p0}, Lcom/ibm/icu/lang/UScriptRun;->pop()V

    goto :goto_3

    .line 358
    :cond_5
    invoke-direct {p0}, Lcom/ibm/icu/lang/UScriptRun;->stackIsNotEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 359
    invoke-direct {p0}, Lcom/ibm/icu/lang/UScriptRun;->top()Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;

    move-result-object v5

    iget v4, v5, Lcom/ibm/icu/lang/UScriptRun$ParenStackEntry;->scriptCode:I

    goto :goto_2

    .line 380
    .end local v3    # "pi":I
    :cond_6
    iget v5, p0, Lcom/ibm/icu/lang/UScriptRun;->textIndex:I

    sub-int/2addr v5, v1

    iput v5, p0, Lcom/ibm/icu/lang/UScriptRun;->textIndex:I

    .line 385
    .end local v0    # "ch":I
    .end local v1    # "codePointCount":I
    .end local v2    # "pairIndex":I
    .end local v4    # "sc":I
    :cond_7
    iget v5, p0, Lcom/ibm/icu/lang/UScriptRun;->textIndex:I

    iput v5, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptLimit:I

    move v5, v6

    .line 386
    goto/16 :goto_0
.end method

.method public final reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 140
    :goto_0
    invoke-direct {p0}, Lcom/ibm/icu/lang/UScriptRun;->stackIsNotEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    invoke-direct {p0}, Lcom/ibm/icu/lang/UScriptRun;->pop()V

    goto :goto_0

    .line 144
    :cond_0
    iget v0, p0, Lcom/ibm/icu/lang/UScriptRun;->textStart:I

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptStart:I

    .line 145
    iget v0, p0, Lcom/ibm/icu/lang/UScriptRun;->textStart:I

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptLimit:I

    .line 146
    iput v1, p0, Lcom/ibm/icu/lang/UScriptRun;->scriptCode:I

    .line 147
    iput v1, p0, Lcom/ibm/icu/lang/UScriptRun;->parenSP:I

    .line 148
    iput v2, p0, Lcom/ibm/icu/lang/UScriptRun;->pushCount:I

    .line 149
    iput v2, p0, Lcom/ibm/icu/lang/UScriptRun;->fixupCount:I

    .line 151
    iget v0, p0, Lcom/ibm/icu/lang/UScriptRun;->textStart:I

    iput v0, p0, Lcom/ibm/icu/lang/UScriptRun;->textIndex:I

    .line 152
    return-void
.end method

.method public final reset(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 169
    const/4 v0, 0x0

    .line 171
    .local v0, "len":I
    iget-object v1, p0, Lcom/ibm/icu/lang/UScriptRun;->text:[C

    if-eqz v1, :cond_0

    .line 172
    iget-object v1, p0, Lcom/ibm/icu/lang/UScriptRun;->text:[C

    array-length v0, v1

    .line 175
    :cond_0
    if-ltz p1, :cond_1

    if-ltz p2, :cond_1

    sub-int v1, v0, p2

    if-le p1, v1, :cond_2

    .line 176
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 179
    :cond_2
    iput p1, p0, Lcom/ibm/icu/lang/UScriptRun;->textStart:I

    .line 180
    add-int v1, p1, p2

    iput v1, p0, Lcom/ibm/icu/lang/UScriptRun;->textLimit:I

    .line 182
    invoke-virtual {p0}, Lcom/ibm/icu/lang/UScriptRun;->reset()V

    .line 183
    return-void
.end method

.method public final reset(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 262
    const/4 v0, 0x0

    .line 264
    .local v0, "length":I
    if-eqz p1, :cond_0

    .line 265
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 268
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/ibm/icu/lang/UScriptRun;->reset(Ljava/lang/String;II)V

    .line 269
    return-void
.end method

.method public final reset(Ljava/lang/String;II)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "count"    # I

    .prologue
    .line 242
    const/4 v0, 0x0

    .line 244
    .local v0, "chars":[C
    if-eqz p1, :cond_0

    .line 245
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 248
    :cond_0
    invoke-virtual {p0, v0, p2, p3}, Lcom/ibm/icu/lang/UScriptRun;->reset([CII)V

    .line 249
    return-void
.end method

.method public final reset([C)V
    .locals 2
    .param p1, "chars"    # [C

    .prologue
    .line 219
    const/4 v0, 0x0

    .line 221
    .local v0, "length":I
    if-eqz p1, :cond_0

    .line 222
    array-length v0, p1

    .line 225
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/ibm/icu/lang/UScriptRun;->reset([CII)V

    .line 226
    return-void
.end method

.method public final reset([CII)V
    .locals 0
    .param p1, "chars"    # [C
    .param p2, "start"    # I
    .param p3, "count"    # I

    .prologue
    .line 199
    if-nez p1, :cond_0

    .line 200
    iget-object p1, p0, Lcom/ibm/icu/lang/UScriptRun;->emptyCharArray:[C

    .line 203
    :cond_0
    iput-object p1, p0, Lcom/ibm/icu/lang/UScriptRun;->text:[C

    .line 205
    invoke-virtual {p0, p2, p3}, Lcom/ibm/icu/lang/UScriptRun;->reset(II)V

    .line 206
    return-void
.end method
