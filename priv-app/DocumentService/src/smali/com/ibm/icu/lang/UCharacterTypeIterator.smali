.class Lcom/ibm/icu/lang/UCharacterTypeIterator;
.super Lcom/ibm/icu/impl/TrieIterator;
.source "UCharacterTypeIterator.java"


# direct methods
.method protected constructor <init>(Lcom/ibm/icu/impl/UCharacterProperty;)V
    .locals 1
    .param p1, "property"    # Lcom/ibm/icu/impl/UCharacterProperty;

    .prologue
    .line 44
    iget-object v0, p1, Lcom/ibm/icu/impl/UCharacterProperty;->m_trie_:Lcom/ibm/icu/impl/CharTrie;

    invoke-direct {p0, v0}, Lcom/ibm/icu/impl/TrieIterator;-><init>(Lcom/ibm/icu/impl/Trie;)V

    .line 45
    return-void
.end method


# virtual methods
.method protected extract(I)I
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 60
    and-int/lit8 v0, p1, 0x1f

    return v0
.end method
