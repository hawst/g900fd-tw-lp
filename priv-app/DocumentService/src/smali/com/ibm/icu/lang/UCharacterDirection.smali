.class public final Lcom/ibm/icu/lang/UCharacterDirection;
.super Ljava/lang/Object;
.source "UCharacterDirection.java"

# interfaces
.implements Lcom/ibm/icu/lang/UCharacterEnums$ECharacterDirection;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static toString(I)Ljava/lang/String;
    .locals 1
    .param p0, "dir"    # I

    .prologue
    .line 41
    packed-switch p0, :pswitch_data_0

    .line 82
    const-string/jumbo v0, "Unassigned"

    :goto_0
    return-object v0

    .line 44
    :pswitch_0
    const-string/jumbo v0, "Left-to-Right"

    goto :goto_0

    .line 46
    :pswitch_1
    const-string/jumbo v0, "Right-to-Left"

    goto :goto_0

    .line 48
    :pswitch_2
    const-string/jumbo v0, "European Number"

    goto :goto_0

    .line 50
    :pswitch_3
    const-string/jumbo v0, "European Number Separator"

    goto :goto_0

    .line 52
    :pswitch_4
    const-string/jumbo v0, "European Number Terminator"

    goto :goto_0

    .line 54
    :pswitch_5
    const-string/jumbo v0, "Arabic Number"

    goto :goto_0

    .line 56
    :pswitch_6
    const-string/jumbo v0, "Common Number Separator"

    goto :goto_0

    .line 58
    :pswitch_7
    const-string/jumbo v0, "Paragraph Separator"

    goto :goto_0

    .line 60
    :pswitch_8
    const-string/jumbo v0, "Segment Separator"

    goto :goto_0

    .line 62
    :pswitch_9
    const-string/jumbo v0, "Whitespace"

    goto :goto_0

    .line 64
    :pswitch_a
    const-string/jumbo v0, "Other Neutrals"

    goto :goto_0

    .line 66
    :pswitch_b
    const-string/jumbo v0, "Left-to-Right Embedding"

    goto :goto_0

    .line 68
    :pswitch_c
    const-string/jumbo v0, "Left-to-Right Override"

    goto :goto_0

    .line 70
    :pswitch_d
    const-string/jumbo v0, "Right-to-Left Arabic"

    goto :goto_0

    .line 72
    :pswitch_e
    const-string/jumbo v0, "Right-to-Left Embedding"

    goto :goto_0

    .line 74
    :pswitch_f
    const-string/jumbo v0, "Right-to-Left Override"

    goto :goto_0

    .line 76
    :pswitch_10
    const-string/jumbo v0, "Pop Directional Format"

    goto :goto_0

    .line 78
    :pswitch_11
    const-string/jumbo v0, "Non-Spacing Mark"

    goto :goto_0

    .line 80
    :pswitch_12
    const-string/jumbo v0, "Boundary Neutral"

    goto :goto_0

    .line 41
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method
