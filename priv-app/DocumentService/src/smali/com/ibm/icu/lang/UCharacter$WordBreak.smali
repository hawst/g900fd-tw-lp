.class public interface abstract Lcom/ibm/icu/lang/UCharacter$WordBreak;
.super Ljava/lang/Object;
.source "UCharacter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/lang/UCharacter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WordBreak"
.end annotation


# static fields
.field public static final ALETTER:I = 0x1

.field public static final COUNT:I = 0xd

.field public static final CR:I = 0x8

.field public static final EXTEND:I = 0x9

.field public static final EXTENDNUMLET:I = 0x7

.field public static final FORMAT:I = 0x2

.field public static final KATAKANA:I = 0x3

.field public static final LF:I = 0xa

.field public static final MIDLETTER:I = 0x4

.field public static final MIDNUM:I = 0x5

.field public static final MIDNUMLET:I = 0xb

.field public static final NEWLINE:I = 0xc

.field public static final NUMERIC:I = 0x6

.field public static final OTHER:I
