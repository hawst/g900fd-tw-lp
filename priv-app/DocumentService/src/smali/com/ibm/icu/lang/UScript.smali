.class public final Lcom/ibm/icu/lang/UScript;
.super Ljava/lang/Object;
.source "UScript.java"


# static fields
.field public static final ARABIC:I = 0x2

.field public static final ARMENIAN:I = 0x3

.field public static final AVESTAN:I = 0x75

.field public static final BALINESE:I = 0x3e

.field public static final BATAK:I = 0x3f

.field public static final BENGALI:I = 0x4

.field public static final BLISSYMBOLS:I = 0x40

.field public static final BOOK_PAHLAVI:I = 0x7c

.field public static final BOPOMOFO:I = 0x5

.field public static final BRAHMI:I = 0x41

.field public static final BRAILLE:I = 0x2e

.field public static final BUGINESE:I = 0x37

.field public static final BUHID:I = 0x2c

.field public static final CANADIAN_ABORIGINAL:I = 0x28

.field public static final CARIAN:I = 0x68

.field public static final CHAKMA:I = 0x76

.field public static final CHAM:I = 0x42

.field public static final CHEROKEE:I = 0x6

.field public static final CIRTH:I = 0x43

.field public static final CODE_LIMIT:I = 0x82

.field public static final COMMON:I = 0x0

.field public static final COPTIC:I = 0x7

.field public static final CUNEIFORM:I = 0x65

.field public static final CYPRIOT:I = 0x2f

.field public static final CYRILLIC:I = 0x8

.field public static final DEMOTIC_EGYPTIAN:I = 0x45

.field public static final DESERET:I = 0x9

.field public static final DEVANAGARI:I = 0xa

.field public static final EASTERN_SYRIAC:I = 0x61

.field public static final EGYPTIAN_HIEROGLYPHS:I = 0x47

.field public static final ESTRANGELO_SYRIAC:I = 0x5f

.field public static final ETHIOPIC:I = 0xb

.field public static final GEORGIAN:I = 0xc

.field public static final GLAGOLITIC:I = 0x38

.field public static final GOTHIC:I = 0xd

.field public static final GREEK:I = 0xe

.field public static final GUJARATI:I = 0xf

.field public static final GURMUKHI:I = 0x10

.field public static final HAN:I = 0x11

.field public static final HANGUL:I = 0x12

.field public static final HANUNOO:I = 0x2b

.field public static final HARAPPAN_INDUS:I = 0x4d

.field public static final HEBREW:I = 0x13

.field public static final HIERATIC_EGYPTIAN:I = 0x46

.field public static final HIRAGANA:I = 0x14

.field public static final IMPERIAL_ARAMAIC:I = 0x74

.field public static final INHERITED:I = 0x1

.field public static final INSCRIPTIONAL_PAHLAVI:I = 0x7a

.field public static final INSCRIPTIONAL_PARTHIAN:I = 0x7d

.field public static final INVALID_CODE:I = -0x1

.field public static final JAPANESE:I = 0x69

.field public static final JAVANESE:I = 0x4e

.field public static final KAITHI:I = 0x78

.field public static final KANNADA:I = 0x15

.field public static final KATAKANA:I = 0x16

.field public static final KATAKANA_OR_HIRAGANA:I = 0x36

.field public static final KAYAH_LI:I = 0x4f

.field public static final KHAROSHTHI:I = 0x39

.field public static final KHMER:I = 0x17

.field public static final KHUTSURI:I = 0x48

.field public static final KOREAN:I = 0x77

.field public static final LANNA:I = 0x6a

.field public static final LAO:I = 0x18

.field public static final LATIN:I = 0x19

.field public static final LATIN_FRAKTUR:I = 0x50

.field public static final LATIN_GAELIC:I = 0x51

.field public static final LEPCHA:I = 0x52

.field public static final LIMBU:I = 0x30

.field public static final LINEAR_A:I = 0x53

.field public static final LINEAR_B:I = 0x31

.field public static final LYCIAN:I = 0x6b

.field public static final LYDIAN:I = 0x6c

.field public static final MALAYALAM:I = 0x1a

.field public static final MANDAEAN:I = 0x54

.field public static final MANICHAEAN:I = 0x79

.field public static final MATHEMATICAL_NOTATION:I = 0x80

.field public static final MAYAN_HIEROGLYPHS:I = 0x55

.field public static final MEITEI_MAYEK:I = 0x73

.field public static final MEROITIC:I = 0x56

.field public static final MONGOLIAN:I = 0x1b

.field public static final MOON:I = 0x72

.field public static final MYANMAR:I = 0x1c

.field public static final NEW_TAI_LUE:I = 0x3b

.field public static final NKO:I = 0x57

.field public static final OGHAM:I = 0x1d

.field public static final OLD_CHURCH_SLAVONIC_CYRILLIC:I = 0x44

.field public static final OLD_HUNGARIAN:I = 0x4c

.field public static final OLD_ITALIC:I = 0x1e

.field public static final OLD_PERMIC:I = 0x59

.field public static final OLD_PERSIAN:I = 0x3d

.field public static final OL_CHIKI:I = 0x6d

.field public static final ORIYA:I = 0x1f

.field public static final ORKHON:I = 0x58

.field public static final OSMANYA:I = 0x32

.field public static final PAHAWH_HMONG:I = 0x4b

.field public static final PHAGS_PA:I = 0x5a

.field public static final PHOENICIAN:I = 0x5b

.field public static final PHONETIC_POLLARD:I = 0x5c

.field public static final PSALTER_PAHLAVI:I = 0x7b

.field public static final REJANG:I = 0x6e

.field public static final RONGORONGO:I = 0x5d

.field public static final RUNIC:I = 0x20

.field public static final SAMARITAN:I = 0x7e

.field public static final SARATI:I = 0x5e

.field public static final SAURASHTRA:I = 0x6f

.field private static final SCRIPT_MASK:I = 0x7f

.field public static final SHAVIAN:I = 0x33

.field public static final SIGN_WRITING:I = 0x70

.field public static final SIMPLIFIED_HAN:I = 0x49

.field public static final SINHALA:I = 0x21

.field public static final SUNDANESE:I = 0x71

.field public static final SYLOTI_NAGRI:I = 0x3a

.field public static final SYMBOLS:I = 0x81

.field public static final SYRIAC:I = 0x22

.field public static final TAGALOG:I = 0x2a

.field public static final TAGBANWA:I = 0x2d

.field public static final TAI_LE:I = 0x34

.field public static final TAI_VIET:I = 0x7f

.field public static final TAMIL:I = 0x23

.field public static final TELUGU:I = 0x24

.field public static final TENGWAR:I = 0x62

.field public static final THAANA:I = 0x25

.field public static final THAI:I = 0x26

.field public static final TIBETAN:I = 0x27

.field public static final TIFINAGH:I = 0x3c

.field public static final TRADITIONAL_HAN:I = 0x4a

.field public static final UCAS:I = 0x28

.field public static final UGARITIC:I = 0x35

.field public static final UNKNOWN:I = 0x67

.field public static final UNWRITTEN_LANGUAGES:I = 0x66

.field public static final VAI:I = 0x63

.field public static final VISIBLE_SPEECH:I = 0x64

.field public static final WESTERN_SYRIAC:I = 0x60

.field public static final YI:I = 0x29

.field private static final kLocaleScript:Ljava/lang/String; = "LocaleScript"

.field private static final prop:Lcom/ibm/icu/impl/UCharacterProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 742
    invoke-static {}, Lcom/ibm/icu/impl/UCharacterProperty;->getInstance()Lcom/ibm/icu/impl/UCharacterProperty;

    move-result-object v0

    sput-object v0, Lcom/ibm/icu/lang/UScript;->prop:Lcom/ibm/icu/impl/UCharacterProperty;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static findCodeFromLocale(Lcom/ibm/icu/util/ULocale;)[I
    .locals 11
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v4, 0x0

    .line 753
    :try_start_0
    const-string/jumbo v8, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v8, p0}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/impl/ICUResourceBundle;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 764
    .local v3, "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getLoadingStatus()I

    move-result v8

    const/4 v9, 0x3

    if-ne v8, v9, :cond_1

    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/ibm/icu/util/ULocale;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 783
    .end local v3    # "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_0
    :goto_0
    return-object v4

    .line 755
    :catch_0
    move-exception v1

    .line 756
    .local v1, "e":Ljava/util/MissingResourceException;
    goto :goto_0

    .line 767
    .end local v1    # "e":Ljava/util/MissingResourceException;
    .restart local v3    # "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_1
    const-string/jumbo v8, "LocaleScript"

    invoke-virtual {v3, v8}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v5

    .line 769
    .local v5, "sub":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v5}, Lcom/ibm/icu/util/UResourceBundle;->getSize()I

    move-result v8

    new-array v4, v8, [I

    .line 770
    .local v4, "result":[I
    const/4 v6, 0x0

    .line 771
    .local v6, "w":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v8, v4

    if-ge v2, v8, :cond_2

    .line 772
    const/16 v8, 0x100a

    invoke-virtual {v5, v2}, Lcom/ibm/icu/util/UResourceBundle;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/ibm/icu/lang/UCharacter;->getPropertyValueEnum(ILjava/lang/String;)I

    move-result v0

    .line 774
    .local v0, "code":I
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "w":I
    .local v7, "w":I
    aput v0, v4, v6

    .line 771
    add-int/lit8 v2, v2, 0x1

    move v6, v7

    .end local v7    # "w":I
    .restart local v6    # "w":I
    goto :goto_1

    .line 778
    .end local v0    # "code":I
    :cond_2
    array-length v8, v4

    if-ge v6, v8, :cond_0

    .line 779
    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v10, "bad locale data, listed "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    array-length v10, v4

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string/jumbo v10, " scripts but found only "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8
.end method

.method public static final getCode(Lcom/ibm/icu/util/ULocale;)[I
    .locals 1
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 806
    invoke-static {p0}, Lcom/ibm/icu/lang/UScript;->findCodeFromLocale(Lcom/ibm/icu/util/ULocale;)[I

    move-result-object v0

    return-object v0
.end method

.method public static final getCode(Ljava/lang/String;)[I
    .locals 4
    .param p0, "nameOrAbbrOrLocale"    # Ljava/lang/String;

    .prologue
    .line 823
    const/4 v1, 0x1

    :try_start_0
    new-array v1, v1, [I

    const/4 v2, 0x0

    const/16 v3, 0x100a

    invoke-static {v3, p0}, Lcom/ibm/icu/lang/UCharacter;->getPropertyValueEnum(ILjava/lang/String;)I

    move-result v3

    aput v3, v1, v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 828
    :goto_0
    return-object v1

    .line 827
    :catch_0
    move-exception v0

    .line 828
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v1, Lcom/ibm/icu/util/ULocale;

    invoke-direct {v1, p0}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/ibm/icu/lang/UScript;->findCodeFromLocale(Lcom/ibm/icu/util/ULocale;)[I

    move-result-object v1

    goto :goto_0
.end method

.method public static final getCode(Ljava/util/Locale;)[I
    .locals 1
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    .line 795
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0}, Lcom/ibm/icu/lang/UScript;->findCodeFromLocale(Lcom/ibm/icu/util/ULocale;)[I

    move-result-object v0

    return-object v0
.end method

.method public static final getCodeFromName(Ljava/lang/String;)I
    .locals 2
    .param p0, "nameOrAbbr"    # Ljava/lang/String;

    .prologue
    .line 843
    const/16 v1, 0x100a

    :try_start_0
    invoke-static {v1, p0}, Lcom/ibm/icu/lang/UCharacter;->getPropertyValueEnum(ILjava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 846
    :goto_0
    return v1

    .line 845
    :catch_0
    move-exception v0

    .line 846
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public static final getName(I)Ljava/lang/String;
    .locals 2
    .param p0, "scriptCode"    # I

    .prologue
    .line 873
    const/16 v0, 0x100a

    const/4 v1, 0x1

    invoke-static {v0, p0, v1}, Lcom/ibm/icu/lang/UCharacter;->getPropertyValueName(III)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final getScript(I)I
    .locals 4
    .param p0, "codepoint"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 858
    if-ltz p0, :cond_0

    move v2, v0

    :goto_0
    const v3, 0x10ffff

    if-gt p0, v3, :cond_1

    :goto_1
    and-int/2addr v0, v2

    if-eqz v0, :cond_2

    .line 859
    sget-object v0, Lcom/ibm/icu/lang/UScript;->prop:Lcom/ibm/icu/impl/UCharacterProperty;

    invoke-virtual {v0, p0, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->getAdditional(II)I

    move-result v0

    and-int/lit8 v0, v0, 0x7f

    return v0

    :cond_0
    move v2, v1

    .line 858
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 861
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static final getShortName(I)Ljava/lang/String;
    .locals 2
    .param p0, "scriptCode"    # I

    .prologue
    .line 886
    const/16 v0, 0x100a

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/ibm/icu/lang/UCharacter;->getPropertyValueName(III)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
