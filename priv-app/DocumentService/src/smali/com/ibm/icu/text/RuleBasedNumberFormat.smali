.class public Lcom/ibm/icu/text/RuleBasedNumberFormat;
.super Lcom/ibm/icu/text/NumberFormat;
.source "RuleBasedNumberFormat.java"


# static fields
.field private static final DEBUG:Z

.field public static final DURATION:I = 0x3

.field public static final ORDINAL:I = 0x2

.field public static final SPELLOUT:I = 0x1

.field private static final locnames:[Ljava/lang/String;

.field private static final rulenames:[Ljava/lang/String;

.field static final serialVersionUID:J = -0x6a5ce54888ed36fcL


# instance fields
.field private transient collator:Lcom/ibm/icu/text/Collator;

.field private transient decimalFormat:Lcom/ibm/icu/text/DecimalFormat;

.field private transient decimalFormatSymbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

.field private transient defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

.field private lenientParse:Z

.field private transient lenientParseRules:Ljava/lang/String;

.field private locale:Lcom/ibm/icu/util/ULocale;

.field private transient postProcessRules:Ljava/lang/String;

.field private transient postProcessor:Lcom/ibm/icu/text/RBNFPostProcessor;

.field private publicRuleSetNames:[Ljava/lang/String;

.field private ruleSetDisplayNames:Ljava/util/Map;

.field private transient ruleSets:[Lcom/ibm/icu/text/NFRuleSet;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 589
    const-string/jumbo v0, "rbnf"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->DEBUG:Z

    .line 757
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "SpelloutRules"

    aput-object v1, v0, v2

    const-string/jumbo v1, "OrdinalRules"

    aput-object v1, v0, v3

    const-string/jumbo v1, "DurationRules"

    aput-object v1, v0, v4

    sput-object v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->rulenames:[Ljava/lang/String;

    .line 760
    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "SpelloutLocalizations"

    aput-object v1, v0, v2

    const-string/jumbo v1, "OrdinalLocalizations"

    aput-object v1, v0, v3

    const-string/jumbo v1, "DurationLocalizations"

    aput-object v1, v0, v4

    sput-object v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locnames:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "format"    # I

    .prologue
    .line 775
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/RuleBasedNumberFormat;-><init>(Lcom/ibm/icu/util/ULocale;I)V

    .line 776
    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/util/ULocale;I)V
    .locals 8
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "format"    # I

    .prologue
    const/4 v3, 0x0

    .line 727
    invoke-direct {p0}, Lcom/ibm/icu/text/NumberFormat;-><init>()V

    .line 518
    iput-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    .line 524
    iput-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    .line 531
    iput-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 538
    iput-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->collator:Lcom/ibm/icu/text/Collator;

    .line 545
    iput-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormatSymbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 552
    iput-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormat:Lcom/ibm/icu/text/DecimalFormat;

    .line 558
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParse:Z

    .line 728
    iput-object p1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 730
    const-string/jumbo v6, "com/ibm/icu/impl/data/icudt40b/rbnf"

    invoke-static {v6, p1}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 736
    .local v0, "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v5

    .line 737
    .local v5, "uloc":Lcom/ibm/icu/util/ULocale;
    invoke-virtual {p0, v5, v5}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V

    .line 739
    const-string/jumbo v1, ""

    .line 740
    .local v1, "description":Ljava/lang/String;
    check-cast v3, [[Ljava/lang/String;

    .line 743
    .local v3, "localizations":[[Ljava/lang/String;
    :try_start_0
    sget-object v6, Lcom/ibm/icu/text/RuleBasedNumberFormat;->rulenames:[Ljava/lang/String;

    add-int/lit8 v7, p2, -0x1

    aget-object v6, v6, v7

    invoke-virtual {v0, v6}, Lcom/ibm/icu/impl/ICUResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 744
    sget-object v6, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locnames:[Ljava/lang/String;

    add-int/lit8 v7, p2, -0x1

    aget-object v6, v6, v7

    invoke-virtual {v0, v6}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v4

    .line 745
    .local v4, "locb":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v4}, Lcom/ibm/icu/util/UResourceBundle;->getSize()I

    move-result v6

    new-array v3, v6, [[Ljava/lang/String;

    .line 746
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, v3

    if-ge v2, v6, :cond_0

    .line 747
    invoke-virtual {v4, v2}, Lcom/ibm/icu/util/UResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v6

    invoke-virtual {v6}, Lcom/ibm/icu/util/UResourceBundle;->getStringArray()[Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v2
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 746
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 750
    .end local v2    # "i":I
    .end local v4    # "locb":Lcom/ibm/icu/util/UResourceBundle;
    :catch_0
    move-exception v6

    .line 754
    :cond_0
    invoke-direct {p0, v1, v3}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->init(Ljava/lang/String;[[Ljava/lang/String;)V

    .line 755
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 603
    invoke-direct {p0}, Lcom/ibm/icu/text/NumberFormat;-><init>()V

    .line 518
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    .line 524
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    .line 531
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 538
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->collator:Lcom/ibm/icu/text/Collator;

    .line 545
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormatSymbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 552
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormat:Lcom/ibm/icu/text/DecimalFormat;

    .line 558
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParse:Z

    .line 604
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 605
    check-cast v0, [[Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->init(Ljava/lang/String;[[Ljava/lang/String;)V

    .line 606
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V
    .locals 2
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v0, 0x0

    .line 664
    invoke-direct {p0}, Lcom/ibm/icu/text/NumberFormat;-><init>()V

    .line 518
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    .line 524
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    .line 531
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 538
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->collator:Lcom/ibm/icu/text/Collator;

    .line 545
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormatSymbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 552
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormat:Lcom/ibm/icu/text/DecimalFormat;

    .line 558
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParse:Z

    .line 665
    iput-object p2, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 666
    check-cast v0, [[Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->init(Ljava/lang/String;[[Ljava/lang/String;)V

    .line 667
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Locale;)V
    .locals 1
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/util/Locale;

    .prologue
    .line 648
    invoke-static {p2}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/RuleBasedNumberFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 649
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[[Ljava/lang/String;)V
    .locals 1
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "localizations"    # [[Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 629
    invoke-direct {p0}, Lcom/ibm/icu/text/NumberFormat;-><init>()V

    .line 518
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    .line 524
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    .line 531
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 538
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->collator:Lcom/ibm/icu/text/Collator;

    .line 545
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormatSymbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 552
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormat:Lcom/ibm/icu/text/DecimalFormat;

    .line 558
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParse:Z

    .line 630
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 631
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->init(Ljava/lang/String;[[Ljava/lang/String;)V

    .line 632
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[[Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V
    .locals 1
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "localizations"    # [[Ljava/lang/String;
    .param p3, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v0, 0x0

    .line 694
    invoke-direct {p0}, Lcom/ibm/icu/text/NumberFormat;-><init>()V

    .line 518
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    .line 524
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    .line 531
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 538
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->collator:Lcom/ibm/icu/text/Collator;

    .line 545
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormatSymbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 552
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormat:Lcom/ibm/icu/text/DecimalFormat;

    .line 558
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParse:Z

    .line 695
    iput-object p3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 696
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->init(Ljava/lang/String;[[Ljava/lang/String;)V

    .line 697
    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;I)V
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;
    .param p2, "format"    # I

    .prologue
    .line 712
    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/ibm/icu/text/RuleBasedNumberFormat;-><init>(Lcom/ibm/icu/util/ULocale;I)V

    .line 713
    return-void
.end method

.method private extractSpecial(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "description"    # Ljava/lang/StringBuffer;
    .param p2, "specialName"    # Ljava/lang/String;

    .prologue
    const/4 v6, -0x1

    .line 1376
    const/4 v3, 0x0

    .line 1377
    .local v3, "result":Ljava/lang/String;
    invoke-static {p1, p2}, Lcom/ibm/icu/impl/Utility;->indexOf(Ljava/lang/StringBuffer;Ljava/lang/String;)I

    move-result v0

    .line 1378
    .local v0, "lp":I
    if-eq v0, v6, :cond_3

    .line 1382
    if-eqz v0, :cond_0

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {p1, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    const/16 v5, 0x3b

    if-ne v4, v5, :cond_3

    .line 1386
    :cond_0
    const-string/jumbo v4, ";%"

    invoke-static {p1, v4, v0}, Lcom/ibm/icu/impl/Utility;->indexOf(Ljava/lang/StringBuffer;Ljava/lang/String;I)I

    move-result v1

    .line 1388
    .local v1, "lpEnd":I
    if-ne v1, v6, :cond_1

    .line 1389
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    .line 1391
    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    add-int v2, v0, v4

    .line 1392
    .local v2, "lpStart":I
    :goto_0
    if-ge v2, v1, :cond_2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1394
    add-int/lit8 v2, v2, 0x1

    .line 1395
    goto :goto_0

    .line 1398
    :cond_2
    invoke-virtual {p1, v2, v1}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 1401
    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p1, v0, v4}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 1404
    .end local v1    # "lpEnd":I
    .end local v2    # "lpStart":I
    :cond_3
    return-object v3
.end method

.method private format(DLcom/ibm/icu/text/NFRuleSet;)Ljava/lang/String;
    .locals 3
    .param p1, "number"    # D
    .param p3, "ruleSet"    # Lcom/ibm/icu/text/NFRuleSet;

    .prologue
    .line 1649
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1650
    .local v0, "result":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    invoke-virtual {p3, p1, p2, v0, v1}, Lcom/ibm/icu/text/NFRuleSet;->format(DLjava/lang/StringBuffer;I)V

    .line 1651
    invoke-direct {p0, v0, p3}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->postProcess(Ljava/lang/StringBuffer;Lcom/ibm/icu/text/NFRuleSet;)V

    .line 1652
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private format(JLcom/ibm/icu/text/NFRuleSet;)Ljava/lang/String;
    .locals 3
    .param p1, "number"    # J
    .param p3, "ruleSet"    # Lcom/ibm/icu/text/NFRuleSet;

    .prologue
    .line 1674
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1675
    .local v0, "result":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    invoke-virtual {p3, p1, p2, v0, v1}, Lcom/ibm/icu/text/NFRuleSet;->format(JLjava/lang/StringBuffer;I)V

    .line 1676
    invoke-direct {p0, v0, p3}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->postProcess(Ljava/lang/StringBuffer;Lcom/ibm/icu/text/NFRuleSet;)V

    .line 1677
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getNameListForLocale(Lcom/ibm/icu/util/ULocale;)[Ljava/lang/String;
    .locals 6
    .param p1, "loc"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 922
    if-eqz p1, :cond_2

    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSetDisplayNames:Ljava/util/Map;

    if-eqz v4, :cond_2

    .line 923
    const/4 v4, 0x2

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/ibm/icu/util/ULocale;->getBaseName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v5

    invoke-virtual {v5}, Lcom/ibm/icu/util/ULocale;->getBaseName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    .line 924
    .local v2, "localeNames":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_2

    .line 925
    aget-object v1, v2, v0

    .line 926
    .local v1, "lname":Ljava/lang/String;
    :goto_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 927
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSetDisplayNames:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v3, v4

    check-cast v3, [Ljava/lang/String;

    .line 928
    .local v3, "names":[Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 935
    .end local v0    # "i":I
    .end local v1    # "lname":Ljava/lang/String;
    .end local v2    # "localeNames":[Ljava/lang/String;
    .end local v3    # "names":[Ljava/lang/String;
    :goto_2
    return-object v3

    .line 931
    .restart local v0    # "i":I
    .restart local v1    # "lname":Ljava/lang/String;
    .restart local v2    # "localeNames":[Ljava/lang/String;
    .restart local v3    # "names":[Ljava/lang/String;
    :cond_0
    invoke-static {v1}, Lcom/ibm/icu/util/ULocale;->getFallback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 932
    goto :goto_1

    .line 924
    .end local v3    # "names":[Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 935
    .end local v0    # "i":I
    .end local v1    # "lname":Ljava/lang/String;
    .end local v2    # "localeNames":[Ljava/lang/String;
    :cond_2
    const/4 v3, 0x0

    goto :goto_2
.end method

.method private init(Ljava/lang/String;[[Ljava/lang/String;)V
    .locals 17
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "localizations"    # [[Ljava/lang/String;

    .prologue
    .line 1416
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->initLocalizations([[Ljava/lang/String;)V

    .line 1423
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->stripWhitespace(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 1430
    .local v3, "descBuf":Ljava/lang/StringBuffer;
    const-string/jumbo v14, "%%lenient-parse:"

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v14}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->extractSpecial(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParseRules:Ljava/lang/String;

    .line 1431
    const-string/jumbo v14, "%%post-process:"

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v14}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->extractSpecial(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->postProcessRules:Ljava/lang/String;

    .line 1436
    const/4 v7, 0x0

    .line 1437
    .local v7, "numRuleSets":I
    const-string/jumbo v14, ";%"

    invoke-static {v3, v14}, Lcom/ibm/icu/impl/Utility;->indexOf(Ljava/lang/StringBuffer;Ljava/lang/String;)I

    move-result v8

    .local v8, "p":I
    :goto_0
    const/4 v14, -0x1

    if-eq v8, v14, :cond_0

    .line 1438
    add-int/lit8 v7, v7, 0x1

    .line 1439
    add-int/lit8 v8, v8, 0x1

    .line 1437
    const-string/jumbo v14, ";%"

    invoke-static {v3, v14, v8}, Lcom/ibm/icu/impl/Utility;->indexOf(Ljava/lang/StringBuffer;Ljava/lang/String;I)I

    move-result v8

    goto :goto_0

    .line 1441
    :cond_0
    add-int/lit8 v7, v7, 0x1

    .line 1444
    new-array v14, v7, [Lcom/ibm/icu/text/NFRuleSet;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    .line 1453
    new-array v12, v7, [Ljava/lang/String;

    .line 1455
    .local v12, "ruleSetDescriptions":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 1456
    .local v2, "curRuleSet":I
    const/4 v13, 0x0

    .line 1457
    .local v13, "start":I
    const-string/jumbo v14, ";%"

    invoke-static {v3, v14}, Lcom/ibm/icu/impl/Utility;->indexOf(Ljava/lang/StringBuffer;Ljava/lang/String;)I

    move-result v8

    :goto_1
    const/4 v14, -0x1

    if-eq v8, v14, :cond_1

    .line 1458
    add-int/lit8 v14, v8, 0x1

    invoke-virtual {v3, v13, v14}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v2

    .line 1459
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    new-instance v15, Lcom/ibm/icu/text/NFRuleSet;

    invoke-direct {v15, v12, v2}, Lcom/ibm/icu/text/NFRuleSet;-><init>([Ljava/lang/String;I)V

    aput-object v15, v14, v2

    .line 1460
    add-int/lit8 v2, v2, 0x1

    .line 1461
    add-int/lit8 v13, v8, 0x1

    .line 1457
    const-string/jumbo v14, ";%"

    invoke-static {v3, v14, v13}, Lcom/ibm/icu/impl/Utility;->indexOf(Ljava/lang/StringBuffer;Ljava/lang/String;I)I

    move-result v8

    goto :goto_1

    .line 1463
    :cond_1
    invoke-virtual {v3, v13}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v2

    .line 1464
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    new-instance v15, Lcom/ibm/icu/text/NFRuleSet;

    invoke-direct {v15, v12, v2}, Lcom/ibm/icu/text/NFRuleSet;-><init>([Ljava/lang/String;I)V

    aput-object v15, v14, v2

    .line 1475
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    array-length v15, v15

    add-int/lit8 v15, v15, -0x1

    aget-object v14, v14, v15

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    .line 1476
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    array-length v14, v14

    add-int/lit8 v4, v14, -0x1

    .local v4, "i":I
    :goto_2
    if-ltz v4, :cond_2

    .line 1477
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v14, v14, v4

    invoke-virtual {v14}, Lcom/ibm/icu/text/NFRuleSet;->getName()Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v15, "%%"

    invoke-virtual {v14, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_3

    .line 1478
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v14, v14, v4

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    .line 1486
    :cond_2
    const/4 v4, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    array-length v14, v14

    if-ge v4, v14, :cond_4

    .line 1487
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v14, v14, v4

    aget-object v15, v12, v4

    move-object/from16 v0, p0

    invoke-virtual {v14, v15, v0}, Lcom/ibm/icu/text/NFRuleSet;->parseRules(Ljava/lang/String;Lcom/ibm/icu/text/RuleBasedNumberFormat;)V

    .line 1488
    const/4 v14, 0x0

    aput-object v14, v12, v4

    .line 1486
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 1476
    :cond_3
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 1496
    :cond_4
    const/4 v9, 0x0

    .line 1497
    .local v9, "publicRuleSetCount":I
    const/4 v4, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    array-length v14, v14

    if-ge v4, v14, :cond_6

    .line 1498
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v14, v14, v4

    invoke-virtual {v14}, Lcom/ibm/icu/text/NFRuleSet;->getName()Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v15, "%%"

    invoke-virtual {v14, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_5

    .line 1499
    add-int/lit8 v9, v9, 0x1

    .line 1497
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1504
    :cond_6
    new-array v11, v9, [Ljava/lang/String;

    .line 1505
    .local v11, "publicRuleSetTemp":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 1506
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    array-length v14, v14

    add-int/lit8 v4, v14, -0x1

    move v10, v9

    .end local v9    # "publicRuleSetCount":I
    .local v10, "publicRuleSetCount":I
    :goto_5
    if-ltz v4, :cond_7

    .line 1507
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v14, v14, v4

    invoke-virtual {v14}, Lcom/ibm/icu/text/NFRuleSet;->getName()Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v15, "%%"

    invoke-virtual {v14, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_c

    .line 1508
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "publicRuleSetCount":I
    .restart local v9    # "publicRuleSetCount":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v14, v14, v4

    invoke-virtual {v14}, Lcom/ibm/icu/text/NFRuleSet;->getName()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v11, v10

    .line 1506
    :goto_6
    add-int/lit8 v4, v4, -0x1

    move v10, v9

    .end local v9    # "publicRuleSetCount":I
    .restart local v10    # "publicRuleSetCount":I
    goto :goto_5

    .line 1512
    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->publicRuleSetNames:[Ljava/lang/String;

    if-eqz v14, :cond_b

    .line 1515
    const/4 v4, 0x0

    :goto_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->publicRuleSetNames:[Ljava/lang/String;

    array-length v14, v14

    if-ge v4, v14, :cond_a

    .line 1516
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->publicRuleSetNames:[Ljava/lang/String;

    aget-object v6, v14, v4

    .line 1517
    .local v6, "name":Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_8
    array-length v14, v11

    if-ge v5, v14, :cond_9

    .line 1518
    aget-object v14, v11, v5

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1515
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 1517
    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    .line 1522
    :cond_9
    new-instance v14, Ljava/lang/IllegalArgumentException;

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v16, "did not find public rule set: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 1525
    .end local v5    # "j":I
    .end local v6    # "name":Ljava/lang/String;
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->publicRuleSetNames:[Ljava/lang/String;

    const/4 v15, 0x0

    aget-object v14, v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->findRuleSet(Ljava/lang/String;)Lcom/ibm/icu/text/NFRuleSet;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    .line 1529
    :goto_9
    return-void

    .line 1527
    :cond_b
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->publicRuleSetNames:[Ljava/lang/String;

    goto :goto_9

    :cond_c
    move v9, v10

    .end local v10    # "publicRuleSetCount":I
    .restart local v9    # "publicRuleSetCount":I
    goto :goto_6
.end method

.method private initLocalizations([[Ljava/lang/String;)V
    .locals 8
    .param p1, "localizations"    # [[Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 1536
    if-eqz p1, :cond_2

    .line 1537
    aget-object v5, p1, v7

    invoke-virtual {v5}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    check-cast v5, [Ljava/lang/String;

    iput-object v5, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->publicRuleSetNames:[Ljava/lang/String;

    .line 1539
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1540
    .local v3, "m":Ljava/util/Map;
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v5, p1

    if-ge v1, v5, :cond_1

    .line 1541
    aget-object v0, p1, v1

    .line 1542
    .local v0, "data":[Ljava/lang/String;
    aget-object v2, v0, v7

    .line 1543
    .local v2, "loc":Ljava/lang/String;
    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    new-array v4, v5, [Ljava/lang/String;

    .line 1544
    .local v4, "names":[Ljava/lang/String;
    array-length v5, v4

    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->publicRuleSetNames:[Ljava/lang/String;

    array-length v6, v6

    if-eq v5, v6, :cond_0

    .line 1545
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "public name length: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->publicRuleSetNames:[Ljava/lang/String;

    array-length v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, " != localized names["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "] length: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    array-length v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1548
    :cond_0
    const/4 v5, 0x1

    array-length v6, v4

    invoke-static {v0, v5, v4, v7, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1549
    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1540
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1552
    .end local v0    # "data":[Ljava/lang/String;
    .end local v2    # "loc":Ljava/lang/String;
    .end local v4    # "names":[Ljava/lang/String;
    :cond_1
    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1553
    iput-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSetDisplayNames:Ljava/util/Map;

    .line 1556
    .end local v1    # "i":I
    .end local v3    # "m":Ljava/util/Map;
    :cond_2
    return-void
.end method

.method private postProcess(Ljava/lang/StringBuffer;Lcom/ibm/icu/text/NFRuleSet;)V
    .locals 8
    .param p1, "result"    # Ljava/lang/StringBuffer;
    .param p2, "ruleSet"    # Lcom/ibm/icu/text/NFRuleSet;

    .prologue
    const/4 v7, 0x0

    .line 1684
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->postProcessRules:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 1685
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->postProcessor:Lcom/ibm/icu/text/RBNFPostProcessor;

    if-nez v4, :cond_1

    .line 1686
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->postProcessRules:Ljava/lang/String;

    const-string/jumbo v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 1687
    .local v2, "ix":I
    const/4 v4, -0x1

    if-ne v2, v4, :cond_0

    .line 1688
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->postProcessRules:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    .line 1690
    :cond_0
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->postProcessRules:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 1692
    .local v3, "ppClassName":Ljava/lang/String;
    :try_start_0
    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 1693
    .local v0, "cls":Ljava/lang/Class;
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/text/RBNFPostProcessor;

    iput-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->postProcessor:Lcom/ibm/icu/text/RBNFPostProcessor;

    .line 1694
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->postProcessor:Lcom/ibm/icu/text/RBNFPostProcessor;

    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->postProcessRules:Ljava/lang/String;

    invoke-interface {v4, p0, v5}, Lcom/ibm/icu/text/RBNFPostProcessor;->init(Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1706
    .end local v0    # "cls":Ljava/lang/Class;
    .end local v2    # "ix":I
    .end local v3    # "ppClassName":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->postProcessor:Lcom/ibm/icu/text/RBNFPostProcessor;

    invoke-interface {v4, p1, p2}, Lcom/ibm/icu/text/RBNFPostProcessor;->process(Ljava/lang/StringBuffer;Lcom/ibm/icu/text/NFRuleSet;)V

    .line 1708
    :cond_2
    :goto_0
    return-void

    .line 1696
    .restart local v2    # "ix":I
    .restart local v3    # "ppClassName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1698
    .local v1, "e":Ljava/lang/Exception;
    sget-boolean v4, Lcom/ibm/icu/text/RuleBasedNumberFormat;->DEBUG:Z

    if-eqz v4, :cond_3

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "could not locate "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, ", error "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1700
    :cond_3
    iput-object v7, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->postProcessor:Lcom/ibm/icu/text/RBNFPostProcessor;

    .line 1701
    iput-object v7, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->postProcessRules:Ljava/lang/String;

    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 5
    .param p1, "in"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 865
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 869
    .local v0, "description":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/util/ULocale;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 878
    .local v2, "loc":Lcom/ibm/icu/util/ULocale;
    :goto_0
    new-instance v3, Lcom/ibm/icu/text/RuleBasedNumberFormat;

    invoke-direct {v3, v0, v2}, Lcom/ibm/icu/text/RuleBasedNumberFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 879
    .local v3, "temp":Lcom/ibm/icu/text/RuleBasedNumberFormat;
    iget-object v4, v3, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    iput-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    .line 880
    iget-object v4, v3, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    iput-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    .line 881
    iget-object v4, v3, Lcom/ibm/icu/text/RuleBasedNumberFormat;->publicRuleSetNames:[Ljava/lang/String;

    iput-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->publicRuleSetNames:[Ljava/lang/String;

    .line 882
    iget-object v4, v3, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormatSymbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    iput-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormatSymbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 883
    iget-object v4, v3, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormat:Lcom/ibm/icu/text/DecimalFormat;

    iput-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormat:Lcom/ibm/icu/text/DecimalFormat;

    .line 884
    iget-object v4, v3, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    iput-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 885
    return-void

    .line 870
    .end local v2    # "loc":Lcom/ibm/icu/util/ULocale;
    .end local v3    # "temp":Lcom/ibm/icu/text/RuleBasedNumberFormat;
    :catch_0
    move-exception v1

    .line 871
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v2

    .restart local v2    # "loc":Lcom/ibm/icu/util/ULocale;
    goto :goto_0
.end method

.method private stripWhitespace(Ljava/lang/String;)Ljava/lang/StringBuffer;
    .locals 6
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x3b

    const/4 v4, -0x1

    .line 1568
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1571
    .local v1, "result":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .line 1572
    .local v2, "start":I
    :goto_0
    if-eq v2, v4, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 1575
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1576
    add-int/lit8 v2, v2, 0x1

    .line 1577
    goto :goto_1

    .line 1580
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v5, :cond_1

    .line 1581
    add-int/lit8 v2, v2, 0x1

    .line 1582
    goto :goto_0

    .line 1588
    :cond_1
    invoke-virtual {p1, v5, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 1589
    .local v0, "p":I
    if-ne v0, v4, :cond_2

    .line 1592
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1593
    const/4 v2, -0x1

    .line 1594
    goto :goto_0

    .line 1595
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 1596
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1597
    add-int/lit8 v2, v0, 0x1

    .line 1598
    goto :goto_0

    .line 1605
    :cond_3
    const/4 v2, -0x1

    goto :goto_0

    .line 1608
    .end local v0    # "p":I
    :cond_4
    return-object v1
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/ObjectOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 853
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 854
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 855
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 788
    invoke-super {p0}, Lcom/ibm/icu/text/NumberFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "that"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 800
    instance-of v3, p1, Lcom/ibm/icu/text/RuleBasedNumberFormat;

    if-nez v3, :cond_1

    .line 822
    :cond_0
    :goto_0
    return v2

    :cond_1
    move-object v1, p1

    .line 805
    check-cast v1, Lcom/ibm/icu/text/RuleBasedNumberFormat;

    .line 808
    .local v1, "that2":Lcom/ibm/icu/text/RuleBasedNumberFormat;
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    iget-object v4, v1, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    invoke-virtual {v3, v4}, Lcom/ibm/icu/util/ULocale;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParse:Z

    iget-boolean v4, v1, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParse:Z

    if-ne v3, v4, :cond_0

    .line 813
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    array-length v3, v3

    iget-object v4, v1, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    array-length v4, v4

    if-ne v3, v4, :cond_0

    .line 816
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 817
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v3, v3, v0

    iget-object v4, v1, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/NFRuleSet;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 816
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 822
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method findRuleSet(Ljava/lang/String;)Lcom/ibm/icu/text/NFRuleSet;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 1717
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1718
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/ibm/icu/text/NFRuleSet;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1719
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v1, v1, v0

    return-object v1

    .line 1717
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1722
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "No rule set named "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public format(DLjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "number"    # D
    .param p3, "ruleSet"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 1012
    const-string/jumbo v0, "%%"

    invoke-virtual {p3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1013
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Can\'t use internal rule set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1015
    :cond_0
    invoke-virtual {p0, p3}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->findRuleSet(Ljava/lang/String;)Lcom/ibm/icu/text/NFRuleSet;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->format(DLcom/ibm/icu/text/NFRuleSet;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public format(JLjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "number"    # J
    .param p3, "ruleSet"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 1031
    const-string/jumbo v0, "%%"

    invoke-virtual {p3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1032
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Can\'t use internal rule set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1034
    :cond_0
    invoke-virtual {p0, p3}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->findRuleSet(Ljava/lang/String;)Lcom/ibm/icu/text/NFRuleSet;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->format(JLcom/ibm/icu/text/NFRuleSet;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public format(DLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 1
    .param p1, "number"    # D
    .param p3, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p4, "ignore"    # Ljava/text/FieldPosition;

    .prologue
    .line 1052
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->format(DLcom/ibm/icu/text/NFRuleSet;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1053
    return-object p3
.end method

.method public format(JLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 1
    .param p1, "number"    # J
    .param p3, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p4, "ignore"    # Ljava/text/FieldPosition;

    .prologue
    .line 1075
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->format(JLcom/ibm/icu/text/NFRuleSet;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1076
    return-object p3
.end method

.method public format(Lcom/ibm/icu/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 2
    .param p1, "number"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 1116
    invoke-virtual {p1}, Lcom/ibm/icu/math/BigDecimal;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, p2, p3}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->format(DLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public format(Ljava/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 1
    .param p1, "number"    # Ljava/math/BigDecimal;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 1102
    new-instance v0, Lcom/ibm/icu/math/BigDecimal;

    invoke-direct {v0, p1}, Lcom/ibm/icu/math/BigDecimal;-><init>(Ljava/math/BigDecimal;)V

    invoke-virtual {p0, v0, p2, p3}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->format(Lcom/ibm/icu/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public format(Ljava/math/BigInteger;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 1
    .param p1, "number"    # Ljava/math/BigInteger;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 1088
    new-instance v0, Lcom/ibm/icu/math/BigDecimal;

    invoke-direct {v0, p1}, Lcom/ibm/icu/math/BigDecimal;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {p0, v0, p2, p3}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->format(Lcom/ibm/icu/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method getCollator()Lcom/ibm/icu/text/Collator;
    .locals 5

    .prologue
    .line 1309
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->collator:Lcom/ibm/icu/text/Collator;

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParse:Z

    if-eqz v3, :cond_0

    .line 1315
    :try_start_0
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v3}, Lcom/ibm/icu/text/Collator;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/Collator;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/RuleBasedCollator;

    .line 1316
    .local v2, "temp":Lcom/ibm/icu/text/RuleBasedCollator;
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2}, Lcom/ibm/icu/text/RuleBasedCollator;->getRules()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParseRules:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1318
    .local v1, "rules":Ljava/lang/String;
    new-instance v3, Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-direct {v3, v1}, Lcom/ibm/icu/text/RuleBasedCollator;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->collator:Lcom/ibm/icu/text/Collator;

    .line 1319
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->collator:Lcom/ibm/icu/text/Collator;

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/Collator;->setDecomposition(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1333
    .end local v1    # "rules":Ljava/lang/String;
    .end local v2    # "temp":Lcom/ibm/icu/text/RuleBasedCollator;
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->collator:Lcom/ibm/icu/text/Collator;

    return-object v3

    .line 1321
    :catch_0
    move-exception v0

    .line 1324
    .local v0, "e":Ljava/lang/Exception;
    sget-boolean v3, Lcom/ibm/icu/text/RuleBasedNumberFormat;->DEBUG:Z

    if-eqz v3, :cond_1

    .line 1325
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1327
    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->collator:Lcom/ibm/icu/text/Collator;

    goto :goto_0
.end method

.method getDecimalFormat()Lcom/ibm/icu/text/DecimalFormat;
    .locals 1

    .prologue
    .line 1354
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormat:Lcom/ibm/icu/text/DecimalFormat;

    if-nez v0, :cond_0

    .line 1355
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v0}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/DecimalFormat;

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormat:Lcom/ibm/icu/text/DecimalFormat;

    .line 1357
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormat:Lcom/ibm/icu/text/DecimalFormat;

    return-object v0
.end method

.method getDecimalFormatSymbols()Lcom/ibm/icu/text/DecimalFormatSymbols;
    .locals 2

    .prologue
    .line 1347
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormatSymbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    if-nez v0, :cond_0

    .line 1348
    new-instance v0, Lcom/ibm/icu/text/DecimalFormatSymbols;

    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->locale:Lcom/ibm/icu/util/ULocale;

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/DecimalFormatSymbols;-><init>(Lcom/ibm/icu/util/ULocale;)V

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormatSymbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 1350
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->decimalFormatSymbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    return-object v0
.end method

.method getDefaultRuleSet()Lcom/ibm/icu/text/NFRuleSet;
    .locals 1

    .prologue
    .line 1298
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    return-object v0
.end method

.method public getDefaultRuleSetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1281
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/NFRuleSet;->isPublic()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1282
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/NFRuleSet;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1284
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getRuleSetDisplayName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "ruleSetName"    # Ljava/lang/String;

    .prologue
    .line 1000
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->getRuleSetDisplayName(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRuleSetDisplayName(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;
    .locals 6
    .param p1, "ruleSetName"    # Ljava/lang/String;
    .param p2, "loc"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 980
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->publicRuleSetNames:[Ljava/lang/String;

    .line 981
    .local v2, "rsnames":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "ix":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 982
    aget-object v3, v2, v0

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 983
    invoke-direct {p0, p2}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->getNameListForLocale(Lcom/ibm/icu/util/ULocale;)[Ljava/lang/String;

    move-result-object v1

    .line 984
    .local v1, "names":[Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 985
    aget-object v3, v1, v0

    .line 987
    :goto_1
    return-object v3

    :cond_0
    aget-object v3, v2, v0

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 981
    .end local v1    # "names":[Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 990
    :cond_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "unrecognized rule set name: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public getRuleSetDisplayNameLocales()[Lcom/ibm/icu/util/ULocale;
    .locals 6

    .prologue
    .line 908
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSetDisplayNames:Ljava/util/Map;

    if-eqz v4, :cond_0

    .line 909
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSetDisplayNames:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 910
    .local v3, "s":Ljava/util/Set;
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v1, v4

    check-cast v1, [Ljava/lang/String;

    .line 911
    .local v1, "locales":[Ljava/lang/String;
    sget-object v4, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-static {v1, v4}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 912
    array-length v4, v1

    new-array v2, v4, [Lcom/ibm/icu/util/ULocale;

    .line 913
    .local v2, "result":[Lcom/ibm/icu/util/ULocale;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_1

    .line 914
    new-instance v4, Lcom/ibm/icu/util/ULocale;

    aget-object v5, v1, v0

    invoke-direct {v4, v5}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    aput-object v4, v2, v0

    .line 913
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 918
    .end local v0    # "i":I
    .end local v1    # "locales":[Ljava/lang/String;
    .end local v2    # "result":[Lcom/ibm/icu/util/ULocale;
    .end local v3    # "s":Ljava/util/Set;
    :cond_0
    const/4 v2, 0x0

    :cond_1
    return-object v2
.end method

.method public getRuleSetDisplayNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 967
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->getRuleSetDisplayNames(Lcom/ibm/icu/util/ULocale;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRuleSetDisplayNames(Lcom/ibm/icu/util/ULocale;)[Ljava/lang/String;
    .locals 4
    .param p1, "loc"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 949
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->getNameListForLocale(Lcom/ibm/icu/util/ULocale;)[Ljava/lang/String;

    move-result-object v1

    .line 950
    .local v1, "names":[Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 951
    invoke-virtual {v1}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    check-cast v2, [Ljava/lang/String;

    .line 957
    :goto_0
    return-object v2

    .line 953
    :cond_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->getRuleSetNames()[Ljava/lang/String;

    move-result-object v1

    .line 954
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 955
    aget-object v2, v1, v0

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 954
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v2, v1

    .line 957
    goto :goto_0
.end method

.method public getRuleSetNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 898
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->publicRuleSetNames:[Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public lenientParseEnabled()Z
    .locals 1

    .prologue
    .line 1244
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParse:Z

    return v0
.end method

.method public parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;
    .locals 11
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "parsePosition"    # Ljava/text/ParsePosition;

    .prologue
    const/4 v10, 0x0

    .line 1141
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1142
    .local v5, "workingText":Ljava/lang/String;
    new-instance v4, Ljava/text/ParsePosition;

    invoke-direct {v4, v10}, Ljava/text/ParsePosition;-><init>(I)V

    .line 1143
    .local v4, "workingPos":Ljava/text/ParsePosition;
    const/4 v3, 0x0

    .line 1147
    .local v3, "tempResult":Ljava/lang/Number;
    new-instance v2, Ljava/lang/Long;

    const-wide/16 v6, 0x0

    invoke-direct {v2, v6, v7}, Ljava/lang/Long;-><init>(J)V

    .line 1148
    .local v2, "result":Ljava/lang/Number;
    new-instance v0, Ljava/text/ParsePosition;

    invoke-virtual {v4}, Ljava/text/ParsePosition;->getIndex()I

    move-result v6

    invoke-direct {v0, v6}, Ljava/text/ParsePosition;-><init>(I)V

    .line 1154
    .local v0, "highWaterMark":Ljava/text/ParsePosition;
    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    array-length v6, v6

    add-int/lit8 v1, v6, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_2

    .line 1156
    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Lcom/ibm/icu/text/NFRuleSet;->getName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "%%"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1154
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 1162
    :cond_0
    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v6, v6, v1

    const-wide v8, 0x7fefffffffffffffL    # Double.MAX_VALUE

    invoke-virtual {v6, v5, v4, v8, v9}, Lcom/ibm/icu/text/NFRuleSet;->parse(Ljava/lang/String;Ljava/text/ParsePosition;D)Ljava/lang/Number;

    move-result-object v3

    .line 1163
    invoke-virtual {v4}, Ljava/text/ParsePosition;->getIndex()I

    move-result v6

    invoke-virtual {v0}, Ljava/text/ParsePosition;->getIndex()I

    move-result v7

    if-le v6, v7, :cond_1

    .line 1164
    move-object v2, v3

    .line 1165
    invoke-virtual {v4}, Ljava/text/ParsePosition;->getIndex()I

    move-result v6

    invoke-virtual {v0, v6}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1174
    :cond_1
    invoke-virtual {v0}, Ljava/text/ParsePosition;->getIndex()I

    move-result v6

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v6, v7, :cond_3

    .line 1185
    :cond_2
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v6

    invoke-virtual {v0}, Ljava/text/ParsePosition;->getIndex()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {p2, v6}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1190
    return-object v2

    .line 1180
    :cond_3
    invoke-virtual {v4, v10}, Ljava/text/ParsePosition;->setIndex(I)V

    goto :goto_1
.end method

.method public setDefaultRuleSet(Ljava/lang/String;)V
    .locals 4
    .param p1, "ruleSetName"    # Ljava/lang/String;

    .prologue
    .line 1255
    if-nez p1, :cond_3

    .line 1256
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->publicRuleSetNames:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 1257
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->publicRuleSetNames:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->findRuleSet(Ljava/lang/String;)Lcom/ibm/icu/text/NFRuleSet;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    .line 1273
    :cond_0
    :goto_0
    return-void

    .line 1259
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    .line 1260
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    array-length v0, v1

    .line 1261
    .local v0, "n":I
    :cond_2
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    .line 1262
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/ibm/icu/text/NFRuleSet;->isPublic()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1263
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v1, v1, v0

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    goto :goto_0

    .line 1268
    .end local v0    # "n":I
    :cond_3
    const-string/jumbo v1, "%%"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1269
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "cannot use private rule set: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1271
    :cond_4
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->findRuleSet(Ljava/lang/String;)Lcom/ibm/icu/text/NFRuleSet;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->defaultRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    goto :goto_0
.end method

.method public setLenientParseMode(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 1227
    iput-boolean p1, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParse:Z

    .line 1231
    if-nez p1, :cond_0

    .line 1232
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->collator:Lcom/ibm/icu/text/Collator;

    .line 1234
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 838
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 839
    .local v1, "result":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 840
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedNumberFormat;->ruleSets:[Lcom/ibm/icu/text/NFRuleSet;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/ibm/icu/text/NFRuleSet;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 839
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 842
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
