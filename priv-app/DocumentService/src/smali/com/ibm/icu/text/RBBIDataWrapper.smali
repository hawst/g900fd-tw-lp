.class final Lcom/ibm/icu/text/RBBIDataWrapper;
.super Ljava/lang/Object;
.source "RBBIDataWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/RBBIDataWrapper$TrieFoldingFunc;,
        Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;
    }
.end annotation


# static fields
.field static final ACCEPTING:I = 0x0

.field static final DH_CATCOUNT:I = 0x3

.field static final DH_FORMATVERSION:I = 0x1

.field static final DH_FTABLE:I = 0x4

.field static final DH_FTABLELEN:I = 0x5

.field static final DH_LENGTH:I = 0x2

.field static final DH_MAGIC:I = 0x0

.field static final DH_RTABLE:I = 0x6

.field static final DH_RTABLELEN:I = 0x7

.field static final DH_RULESOURCE:I = 0xe

.field static final DH_RULESOURCELEN:I = 0xf

.field static final DH_SFTABLE:I = 0x8

.field static final DH_SFTABLELEN:I = 0x9

.field static final DH_SIZE:I = 0x18

.field static final DH_SRTABLE:I = 0xa

.field static final DH_SRTABLELEN:I = 0xb

.field static final DH_STATUSTABLE:I = 0x10

.field static final DH_STATUSTABLELEN:I = 0x11

.field static final DH_TRIE:I = 0xc

.field static final DH_TRIELEN:I = 0xd

.field static final FLAGS:I = 0x4

.field static final LOOKAHEAD:I = 0x1

.field static final NEXTSTATES:I = 0x4

.field static final NUMSTATES:I = 0x0

.field static final RBBI_BOF_REQUIRED:I = 0x2

.field static final RBBI_LOOKAHEAD_HARD_BREAK:I = 0x1

.field static final RESERVED:I = 0x3

.field static final RESERVED_2:I = 0x6

.field static final ROWLEN:I = 0x2

.field static final ROW_DATA:I = 0x8

.field static final TAGIDX:I = 0x2

.field static fTrieFoldingFunc:Lcom/ibm/icu/text/RBBIDataWrapper$TrieFoldingFunc;


# instance fields
.field fFTable:[S

.field fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

.field fRTable:[S

.field fRuleSource:Ljava/lang/String;

.field fSFTable:[S

.field fSRTable:[S

.field fStatusTable:[I

.field fTrie:Lcom/ibm/icu/impl/CharTrie;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 144
    new-instance v0, Lcom/ibm/icu/text/RBBIDataWrapper$TrieFoldingFunc;

    invoke-direct {v0}, Lcom/ibm/icu/text/RBBIDataWrapper$TrieFoldingFunc;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fTrieFoldingFunc:Lcom/ibm/icu/text/RBBIDataWrapper$TrieFoldingFunc;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    return-void
.end method

.method private dumpCharCategories()V
    .locals 11

    .prologue
    .line 447
    iget-object v8, p0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v8, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fCatCount:I

    .line 448
    .local v5, "n":I
    add-int/lit8 v8, v5, 0x1

    new-array v0, v8, [Ljava/lang/String;

    .line 449
    .local v0, "catStrings":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 450
    .local v7, "rangeStart":I
    const/4 v6, 0x0

    .line 451
    .local v6, "rangeEnd":I
    const/4 v3, -0x1

    .line 454
    .local v3, "lastCat":I
    add-int/lit8 v8, v5, 0x1

    new-array v4, v8, [I

    .line 456
    .local v4, "lastNewline":[I
    const/4 v1, 0x0

    .local v1, "category":I
    :goto_0
    iget-object v8, p0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v8, v8, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fCatCount:I

    if-gt v1, v8, :cond_0

    .line 457
    const-string/jumbo v8, ""

    aput-object v8, v0, v1

    .line 456
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 459
    :cond_0
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v9, "\nCharacter Categories"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 460
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v9, "--------------------"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 461
    const/4 v2, 0x0

    .local v2, "char32":I
    :goto_1
    const v8, 0x10ffff

    if-gt v2, v8, :cond_2

    .line 462
    iget-object v8, p0, Lcom/ibm/icu/text/RBBIDataWrapper;->fTrie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v8, v2}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v1

    .line 463
    and-int/lit16 v1, v1, -0x4001

    .line 464
    if-ltz v1, :cond_1

    iget-object v8, p0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v8, v8, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fCatCount:I

    if-le v1, v8, :cond_4

    .line 465
    :cond_1
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v10, "Error, bad category "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string/jumbo v10, " for char "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 487
    :cond_2
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    aget-object v9, v0, v3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v0, v3

    .line 488
    if-eq v6, v7, :cond_3

    .line 489
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    aget-object v9, v0, v3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string/jumbo v9, "-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v0, v3

    .line 492
    :cond_3
    const/4 v1, 0x0

    :goto_2
    iget-object v8, p0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v8, v8, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fCatCount:I

    if-gt v1, v8, :cond_8

    .line 493
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v10, 0x5

    invoke-static {v1, v10}, Lcom/ibm/icu/text/RBBIDataWrapper;->intToString(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string/jumbo v10, "  "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    aget-object v10, v0, v1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 492
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 469
    :cond_4
    if-ne v1, v3, :cond_5

    .line 470
    move v6, v2

    .line 461
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 472
    :cond_5
    if-ltz v3, :cond_7

    .line 473
    aget-object v8, v0, v3

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    aget v9, v4, v3

    add-int/lit8 v9, v9, 0x46

    if-le v8, v9, :cond_6

    .line 474
    aget-object v8, v0, v3

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0xa

    aput v8, v4, v3

    .line 475
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    aget-object v9, v0, v3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string/jumbo v9, "\n       "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v0, v3

    .line 478
    :cond_6
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    aget-object v9, v0, v3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v0, v3

    .line 479
    if-eq v6, v7, :cond_7

    .line 480
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    aget-object v9, v0, v3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string/jumbo v9, "-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v0, v3

    .line 483
    :cond_7
    move v3, v1

    .line 484
    move v6, v2

    move v7, v2

    goto :goto_3

    .line 495
    :cond_8
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v8}, Ljava/io/PrintStream;->println()V

    .line 496
    return-void
.end method

.method private dumpRow([SI)V
    .locals 5
    .param p1, "table"    # [S
    .param p2, "state"    # I

    .prologue
    const/4 v4, 0x5

    .line 422
    new-instance v1, Ljava/lang/StringBuffer;

    iget-object v3, p0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v3, v3, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fCatCount:I

    mul-int/lit8 v3, v3, 0x5

    add-int/lit8 v3, v3, 0x14

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 423
    .local v1, "dest":Ljava/lang/StringBuffer;
    const/4 v3, 0x4

    invoke-static {p2, v3}, Lcom/ibm/icu/text/RBBIDataWrapper;->intToString(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 424
    invoke-virtual {p0, p2}, Lcom/ibm/icu/text/RBBIDataWrapper;->getRowIndex(I)I

    move-result v2

    .line 425
    .local v2, "row":I
    add-int/lit8 v3, v2, 0x0

    aget-short v3, p1, v3

    if-eqz v3, :cond_0

    .line 426
    add-int/lit8 v3, v2, 0x0

    aget-short v3, p1, v3

    invoke-static {v3, v4}, Lcom/ibm/icu/text/RBBIDataWrapper;->intToString(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 430
    :goto_0
    add-int/lit8 v3, v2, 0x1

    aget-short v3, p1, v3

    if-eqz v3, :cond_1

    .line 431
    add-int/lit8 v3, v2, 0x1

    aget-short v3, p1, v3

    invoke-static {v3, v4}, Lcom/ibm/icu/text/RBBIDataWrapper;->intToString(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 435
    :goto_1
    add-int/lit8 v3, v2, 0x2

    aget-short v3, p1, v3

    invoke-static {v3, v4}, Lcom/ibm/icu/text/RBBIDataWrapper;->intToString(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 437
    const/4 v0, 0x0

    .local v0, "col":I
    :goto_2
    iget-object v3, p0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v3, v3, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fCatCount:I

    if-ge v0, v3, :cond_2

    .line 438
    add-int/lit8 v3, v2, 0x4

    add-int/2addr v3, v0

    aget-short v3, p1, v3

    invoke-static {v3, v4}, Lcom/ibm/icu/text/RBBIDataWrapper;->intToString(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 437
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 428
    .end local v0    # "col":I
    :cond_0
    const-string/jumbo v3, "     "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 433
    :cond_1
    const-string/jumbo v3, "     "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 441
    .restart local v0    # "col":I
    :cond_2
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v3, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 442
    return-void
.end method

.method private dumpTable([S)V
    .locals 5
    .param p1, "table"    # [S

    .prologue
    .line 392
    if-nez p1, :cond_0

    .line 393
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "  -- null -- "

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 411
    :goto_0
    return-void

    .line 397
    :cond_0
    const-string/jumbo v0, " Row  Acc Look  Tag"

    .line 398
    .local v0, "header":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_1
    iget-object v3, p0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v3, v3, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fCatCount:I

    if-ge v1, v3, :cond_1

    .line 399
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const/4 v4, 0x5

    invoke-static {v1, v4}, Lcom/ibm/icu/text/RBBIDataWrapper;->intToString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 398
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 401
    :cond_1
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 402
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 403
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "-"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 402
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 405
    :cond_2
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v3}, Ljava/io/PrintStream;->println()V

    .line 406
    const/4 v2, 0x0

    .local v2, "state":I
    :goto_3
    invoke-static {p1}, Lcom/ibm/icu/text/RBBIDataWrapper;->getNumStates([S)I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 407
    invoke-direct {p0, p1, v2}, Lcom/ibm/icu/text/RBBIDataWrapper;->dumpRow([SI)V

    .line 406
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 409
    :cond_3
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v3}, Ljava/io/PrintStream;->println()V

    goto :goto_0
.end method

.method static get(Ljava/io/InputStream;)Lcom/ibm/icu/text/RBBIDataWrapper;
    .locals 11
    .param p0, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 157
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-direct {v5, p0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 158
    .local v1, "dis":Ljava/io/DataInputStream;
    new-instance v0, Lcom/ibm/icu/text/RBBIDataWrapper;

    invoke-direct {v0}, Lcom/ibm/icu/text/RBBIDataWrapper;-><init>()V

    .line 162
    .local v0, "This":Lcom/ibm/icu/text/RBBIDataWrapper;
    const-wide/16 v6, 0x80

    invoke-virtual {v1, v6, v7}, Ljava/io/DataInputStream;->skip(J)J

    .line 165
    new-instance v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-direct {v5}, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;-><init>()V

    iput-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    .line 166
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fMagic:I

    .line 167
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fVersion:I

    .line 168
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fFormatVersion:[B

    iget-object v6, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v6, v6, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fVersion:I

    shr-int/lit8 v6, v6, 0x18

    int-to-byte v6, v6

    aput-byte v6, v5, v8

    .line 169
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fFormatVersion:[B

    iget-object v6, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v6, v6, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fVersion:I

    shr-int/lit8 v6, v6, 0x10

    int-to-byte v6, v6

    aput-byte v6, v5, v9

    .line 170
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fFormatVersion:[B

    const/4 v6, 0x2

    iget-object v7, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v7, v7, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fVersion:I

    shr-int/lit8 v7, v7, 0x8

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 171
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fFormatVersion:[B

    iget-object v6, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v6, v6, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fVersion:I

    int-to-byte v6, v6

    aput-byte v6, v5, v10

    .line 172
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fLength:I

    .line 173
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fCatCount:I

    .line 174
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fFTable:I

    .line 175
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fFTableLen:I

    .line 176
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fRTable:I

    .line 177
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fRTableLen:I

    .line 178
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fSFTable:I

    .line 179
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fSFTableLen:I

    .line 180
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fSRTable:I

    .line 181
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fSRTableLen:I

    .line 182
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fTrie:I

    .line 183
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fTrieLen:I

    .line 184
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fRuleSource:I

    .line 185
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fRuleSourceLen:I

    .line 186
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fStatusTable:I

    .line 187
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fStatusTableLen:I

    .line 188
    const-wide/16 v6, 0x18

    invoke-virtual {v1, v6, v7}, Ljava/io/DataInputStream;->skip(J)J

    .line 191
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fMagic:I

    const v6, 0xb1a0

    if-ne v5, v6, :cond_0

    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fVersion:I

    if-eq v5, v9, :cond_1

    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fFormatVersion:[B

    aget-byte v5, v5, v8

    if-eq v5, v10, :cond_1

    .line 195
    :cond_0
    new-instance v5, Ljava/io/IOException;

    const-string/jumbo v6, "Break Iterator Rule Data Magic Number Incorrect, or unsupported data version."

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 199
    :cond_1
    const/16 v3, 0x60

    .line 206
    .local v3, "pos":I
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fFTable:I

    if-lt v5, v3, :cond_2

    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fFTable:I

    iget-object v6, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v6, v6, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fLength:I

    if-le v5, v6, :cond_3

    .line 207
    :cond_2
    new-instance v5, Ljava/io/IOException;

    const-string/jumbo v6, "Break iterator Rule data corrupt"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 211
    :cond_3
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fFTable:I

    sub-int/2addr v5, v3

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Ljava/io/DataInputStream;->skip(J)J

    .line 212
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v3, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fFTable:I

    .line 214
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fFTableLen:I

    div-int/lit8 v5, v5, 0x2

    new-array v5, v5, [S

    iput-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fFTable:[S

    .line 215
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fFTable:[S

    array-length v5, v5

    if-ge v2, v5, :cond_4

    .line 216
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fFTable:[S

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readShort()S

    move-result v6

    aput-short v6, v5, v2

    .line 217
    add-int/lit8 v3, v3, 0x2

    .line 215
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 225
    :cond_4
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fRTable:I

    sub-int/2addr v5, v3

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Ljava/io/DataInputStream;->skip(J)J

    .line 226
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v3, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fRTable:I

    .line 229
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fRTableLen:I

    div-int/lit8 v5, v5, 0x2

    new-array v5, v5, [S

    iput-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fRTable:[S

    .line 230
    const/4 v2, 0x0

    :goto_1
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fRTable:[S

    array-length v5, v5

    if-ge v2, v5, :cond_5

    .line 231
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fRTable:[S

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readShort()S

    move-result v6

    aput-short v6, v5, v2

    .line 232
    add-int/lit8 v3, v3, 0x2

    .line 230
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 238
    :cond_5
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fSFTableLen:I

    if-lez v5, :cond_6

    .line 240
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fSFTable:I

    sub-int/2addr v5, v3

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Ljava/io/DataInputStream;->skip(J)J

    .line 241
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v3, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fSFTable:I

    .line 244
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fSFTableLen:I

    div-int/lit8 v5, v5, 0x2

    new-array v5, v5, [S

    iput-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fSFTable:[S

    .line 245
    const/4 v2, 0x0

    :goto_2
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fSFTable:[S

    array-length v5, v5

    if-ge v2, v5, :cond_6

    .line 246
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fSFTable:[S

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readShort()S

    move-result v6

    aput-short v6, v5, v2

    .line 247
    add-int/lit8 v3, v3, 0x2

    .line 245
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 254
    :cond_6
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fSRTableLen:I

    if-lez v5, :cond_7

    .line 256
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fSRTable:I

    sub-int/2addr v5, v3

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Ljava/io/DataInputStream;->skip(J)J

    .line 257
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v3, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fSRTable:I

    .line 260
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fSRTableLen:I

    div-int/lit8 v5, v5, 0x2

    new-array v5, v5, [S

    iput-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fSRTable:[S

    .line 261
    const/4 v2, 0x0

    :goto_3
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fSRTable:[S

    array-length v5, v5

    if-ge v2, v5, :cond_7

    .line 262
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fSRTable:[S

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readShort()S

    move-result v6

    aput-short v6, v5, v2

    .line 263
    add-int/lit8 v3, v3, 0x2

    .line 261
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 274
    :cond_7
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fTrie:I

    sub-int/2addr v5, v3

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Ljava/io/DataInputStream;->skip(J)J

    .line 275
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v3, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fTrie:I

    .line 277
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fTrieLen:I

    add-int/lit8 v5, v5, 0x64

    invoke-virtual {v1, v5}, Ljava/io/DataInputStream;->mark(I)V

    .line 282
    new-instance v5, Lcom/ibm/icu/impl/CharTrie;

    sget-object v6, Lcom/ibm/icu/text/RBBIDataWrapper;->fTrieFoldingFunc:Lcom/ibm/icu/text/RBBIDataWrapper$TrieFoldingFunc;

    invoke-direct {v5, v1, v6}, Lcom/ibm/icu/impl/CharTrie;-><init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V

    iput-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fTrie:Lcom/ibm/icu/impl/CharTrie;

    .line 286
    invoke-virtual {v1}, Ljava/io/DataInputStream;->reset()V

    .line 294
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fStatusTable:I

    if-le v3, v5, :cond_8

    .line 295
    new-instance v5, Ljava/io/IOException;

    const-string/jumbo v6, "Break iterator Rule data corrupt"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 297
    :cond_8
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fStatusTable:I

    sub-int/2addr v5, v3

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Ljava/io/DataInputStream;->skip(J)J

    .line 298
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v3, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fStatusTable:I

    .line 299
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fStatusTableLen:I

    div-int/lit8 v5, v5, 0x4

    new-array v5, v5, [I

    iput-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fStatusTable:[I

    .line 300
    const/4 v2, 0x0

    :goto_4
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fStatusTable:[I

    array-length v5, v5

    if-ge v2, v5, :cond_9

    .line 301
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fStatusTable:[I

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v2

    .line 302
    add-int/lit8 v3, v3, 0x4

    .line 300
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 308
    :cond_9
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fRuleSource:I

    if-le v3, v5, :cond_a

    .line 309
    new-instance v5, Ljava/io/IOException;

    const-string/jumbo v6, "Break iterator Rule data corrupt"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 311
    :cond_a
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fRuleSource:I

    sub-int/2addr v5, v3

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7}, Ljava/io/DataInputStream;->skip(J)J

    .line 312
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v3, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fRuleSource:I

    .line 313
    new-instance v4, Ljava/lang/StringBuffer;

    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fRuleSourceLen:I

    div-int/lit8 v5, v5, 0x2

    invoke-direct {v4, v5}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 314
    .local v4, "sb":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    :goto_5
    iget-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fRuleSourceLen:I

    if-ge v2, v5, :cond_b

    .line 315
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readChar()C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 316
    add-int/lit8 v3, v3, 0x2

    .line 314
    add-int/lit8 v2, v2, 0x2

    goto :goto_5

    .line 318
    :cond_b
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fRuleSource:Ljava/lang/String;

    .line 320
    sget-object v5, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fDebugEnv:Ljava/lang/String;

    if-eqz v5, :cond_c

    sget-object v5, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v6, "data"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ltz v5, :cond_c

    .line 321
    invoke-virtual {v0}, Lcom/ibm/icu/text/RBBIDataWrapper;->dump()V

    .line 323
    :cond_c
    return-object v0
.end method

.method static final getNumStates([S)I
    .locals 5
    .param p0, "table"    # [S

    .prologue
    .line 330
    const/4 v3, 0x0

    aget-short v0, p0, v3

    .line 331
    .local v0, "hi":I
    const/4 v3, 0x1

    aget-short v1, p0, v3

    .line 332
    .local v1, "lo":I
    shl-int/lit8 v3, v0, 0x10

    const v4, 0xffff

    and-int/2addr v4, v1

    add-int v2, v3, v4

    .line 333
    .local v2, "val":I
    return v2
.end method

.method public static intToHexString(II)Ljava/lang/String;
    .locals 3
    .param p0, "n"    # I
    .param p1, "width"    # I

    .prologue
    .line 380
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 381
    .local v0, "dest":Ljava/lang/StringBuffer;
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 382
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-ge v1, p1, :cond_0

    .line 383
    const/4 v1, 0x0

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 385
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static intToString(II)Ljava/lang/String;
    .locals 3
    .param p0, "n"    # I
    .param p1, "width"    # I

    .prologue
    .line 365
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 366
    .local v0, "dest":Ljava/lang/StringBuffer;
    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 367
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-ge v1, p1, :cond_0

    .line 368
    const/4 v1, 0x0

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 370
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method dump()V
    .locals 3

    .prologue
    .line 342
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "RBBI Data Wrapper dump ..."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 343
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->println()V

    .line 344
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "Forward State Table"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 345
    iget-object v0, p0, Lcom/ibm/icu/text/RBBIDataWrapper;->fFTable:[S

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/RBBIDataWrapper;->dumpTable([S)V

    .line 346
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "Reverse State Table"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 347
    iget-object v0, p0, Lcom/ibm/icu/text/RBBIDataWrapper;->fRTable:[S

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/RBBIDataWrapper;->dumpTable([S)V

    .line 348
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "Forward Safe Points Table"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/ibm/icu/text/RBBIDataWrapper;->fSFTable:[S

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/RBBIDataWrapper;->dumpTable([S)V

    .line 350
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "Reverse Safe Points Table"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 351
    iget-object v0, p0, Lcom/ibm/icu/text/RBBIDataWrapper;->fSRTable:[S

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/RBBIDataWrapper;->dumpTable([S)V

    .line 353
    invoke-direct {p0}, Lcom/ibm/icu/text/RBBIDataWrapper;->dumpCharCategories()V

    .line 354
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Source Rules: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/ibm/icu/text/RBBIDataWrapper;->fRuleSource:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 356
    return-void
.end method

.method getRowIndex(I)I
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 132
    iget-object v0, p0, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v0, v0, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fCatCount:I

    add-int/lit8 v0, v0, 0x4

    mul-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x8

    return v0
.end method
