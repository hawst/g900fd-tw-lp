.class public final Lcom/ibm/icu/text/UnicodeDecompressor;
.super Ljava/lang/Object;
.source "UnicodeDecompressor.java"

# interfaces
.implements Lcom/ibm/icu/text/SCSU;


# static fields
.field private static final BUFSIZE:I = 0x3


# instance fields
.field private fBuffer:[B

.field private fBufferLength:I

.field private fCurrentWindow:I

.field private fMode:I

.field private fOffsets:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput v1, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    .line 82
    const/16 v0, 0x8

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    .line 85
    iput v1, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fMode:I

    .line 91
    const/4 v0, 0x3

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBuffer:[B

    .line 94
    iput v1, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    .line 105
    invoke-virtual {p0}, Lcom/ibm/icu/text/UnicodeDecompressor;->reset()V

    .line 106
    return-void
.end method

.method public static decompress([B)Ljava/lang/String;
    .locals 3
    .param p0, "buffer"    # [B

    .prologue
    .line 117
    const/4 v1, 0x0

    array-length v2, p0

    invoke-static {p0, v1, v2}, Lcom/ibm/icu/text/UnicodeDecompressor;->decompress([BII)[C

    move-result-object v0

    .line 118
    .local v0, "buf":[C
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    return-object v1
.end method

.method public static decompress([BII)[C
    .locals 10
    .param p0, "buffer"    # [B
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    const/4 v6, 0x0

    .line 134
    new-instance v0, Lcom/ibm/icu/text/UnicodeDecompressor;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeDecompressor;-><init>()V

    .line 139
    .local v0, "comp":Lcom/ibm/icu/text/UnicodeDecompressor;
    const/4 v1, 0x2

    sub-int v2, p2, p1

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 140
    .local v7, "len":I
    new-array v5, v7, [C

    .line 142
    .local v5, "temp":[C
    const/4 v4, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    invoke-virtual/range {v0 .. v7}, Lcom/ibm/icu/text/UnicodeDecompressor;->decompress([BII[I[CII)I

    move-result v8

    .line 145
    .local v8, "charCount":I
    new-array v9, v8, [C

    .line 146
    .local v9, "result":[C
    invoke-static {v5, v6, v9, v6, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 147
    return-object v9
.end method


# virtual methods
.method public decompress([BII[I[CII)I
    .locals 19
    .param p1, "byteBuffer"    # [B
    .param p2, "byteBufferStart"    # I
    .param p3, "byteBufferLimit"    # I
    .param p4, "bytesRead"    # [I
    .param p5, "charBuffer"    # [C
    .param p6, "charBufferStart"    # I
    .param p7, "charBufferLimit"    # I

    .prologue
    .line 179
    move/from16 v11, p2

    .line 182
    .local v11, "bytePos":I
    move/from16 v17, p6

    .line 185
    .local v17, "ucPos":I
    const/4 v10, 0x0

    .line 189
    .local v10, "aByte":I
    move-object/from16 v0, p5

    array-length v2, v0

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    sub-int v2, p7, p6

    const/4 v3, 0x2

    if-ge v2, v3, :cond_1

    .line 190
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "charBuffer.length < 2"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 194
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    if-lez v2, :cond_4

    .line 196
    const/4 v15, 0x0

    .line 199
    .local v15, "newBytes":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_3

    .line 200
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBuffer:[B

    array-length v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    sub-int v15, v2, v3

    .line 203
    sub-int v2, p3, p2

    if-ge v2, v15, :cond_2

    .line 204
    sub-int v15, p3, p2

    .line 206
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBuffer:[B

    move-object/from16 v0, p0

    iget v3, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v0, v1, v2, v3, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 211
    :cond_3
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    .line 214
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBuffer:[B

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBuffer:[B

    array-length v5, v2

    const/4 v6, 0x0

    move-object/from16 v2, p0

    move-object/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-virtual/range {v2 .. v9}, Lcom/ibm/icu/text/UnicodeDecompressor;->decompress([BII[I[CII)I

    move-result v13

    .line 219
    .local v13, "count":I
    add-int v17, v17, v13

    .line 220
    add-int/2addr v11, v15

    .line 225
    .end local v13    # "count":I
    .end local v15    # "newBytes":I
    :cond_4
    :goto_0
    move/from16 v0, p3

    if-ge v11, v0, :cond_6

    move/from16 v0, v17

    move/from16 v1, p7

    if-ge v0, v1, :cond_6

    .line 226
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fMode:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move/from16 v18, v17

    .end local v17    # "ucPos":I
    .local v18, "ucPos":I
    move v12, v11

    .line 230
    .end local v11    # "bytePos":I
    .local v12, "bytePos":I
    :goto_1
    move/from16 v0, p3

    if-ge v12, v0, :cond_12

    move/from16 v0, v18

    move/from16 v1, p7

    if-ge v0, v1, :cond_12

    .line 231
    add-int/lit8 v11, v12, 0x1

    .end local v12    # "bytePos":I
    .restart local v11    # "bytePos":I
    aget-byte v2, p1, v12

    and-int/lit16 v10, v2, 0xff

    .line 232
    packed-switch v10, :pswitch_data_1

    :pswitch_1
    move/from16 v17, v18

    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    :goto_2
    move/from16 v18, v17

    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    move v12, v11

    .line 428
    .end local v11    # "bytePos":I
    .restart local v12    # "bytePos":I
    goto :goto_1

    .line 265
    .end local v12    # "bytePos":I
    .restart local v11    # "bytePos":I
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    aget v2, v2, v3

    const v3, 0xffff

    if-gt v2, v3, :cond_5

    .line 266
    add-int/lit8 v17, v18, 0x1

    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    aget v2, v2, v3

    add-int/2addr v2, v10

    add-int/lit8 v2, v2, -0x80

    int-to-char v2, v2

    aput-char v2, p5, v18

    goto :goto_2

    .line 278
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :cond_5
    add-int/lit8 v2, v18, 0x1

    move/from16 v0, p7

    if-lt v2, v0, :cond_8

    .line 279
    add-int/lit8 v11, v11, -0x1

    .line 280
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBuffer:[B

    const/4 v3, 0x0

    sub-int v4, p3, v11

    move-object/from16 v0, p1

    invoke-static {v0, v11, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 283
    sub-int v2, p3, v11

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    .line 284
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    add-int/2addr v11, v2

    move/from16 v17, v18

    .line 534
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    :cond_6
    :goto_3
    if-eqz p4, :cond_7

    .line 535
    const/4 v2, 0x0

    sub-int v3, v11, p2

    aput v3, p4, v2

    .line 538
    :cond_7
    sub-int v2, v17, p6

    return v2

    .line 288
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    aget v2, v2, v3

    const/high16 v3, 0x10000

    sub-int v16, v2, v3

    .line 290
    .local v16, "normalizedBase":I
    add-int/lit8 v17, v18, 0x1

    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    const v2, 0xd800

    shr-int/lit8 v3, v16, 0xa

    add-int/2addr v2, v3

    int-to-char v2, v2

    aput-char v2, p5, v18

    .line 292
    add-int/lit8 v18, v17, 0x1

    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    const v2, 0xdc00

    move/from16 v0, v16

    and-int/lit16 v3, v0, 0x3ff

    add-int/2addr v2, v3

    and-int/lit8 v3, v10, 0x7f

    add-int/2addr v2, v3

    int-to-char v2, v2

    aput-char v2, p5, v17

    move/from16 v17, v18

    .line 295
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    goto :goto_2

    .line 323
    .end local v16    # "normalizedBase":I
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :pswitch_3
    add-int/lit8 v17, v18, 0x1

    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    int-to-char v2, v10

    aput-char v2, p5, v18

    goto/16 :goto_2

    .line 330
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :pswitch_4
    add-int/lit8 v2, v11, 0x1

    move/from16 v0, p3

    if-lt v2, v0, :cond_9

    .line 331
    add-int/lit8 v11, v11, -0x1

    .line 332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBuffer:[B

    const/4 v3, 0x0

    sub-int v4, p3, v11

    move-object/from16 v0, p1

    invoke-static {v0, v11, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 335
    sub-int v2, p3, v11

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    .line 336
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    add-int/2addr v11, v2

    move/from16 v17, v18

    .line 337
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    goto :goto_3

    .line 340
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :cond_9
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "bytePos":I
    .restart local v12    # "bytePos":I
    aget-byte v10, p1, v11

    .line 341
    add-int/lit8 v17, v18, 0x1

    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    shl-int/lit8 v2, v10, 0x8

    add-int/lit8 v11, v12, 0x1

    .end local v12    # "bytePos":I
    .restart local v11    # "bytePos":I
    aget-byte v3, p1, v12

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    int-to-char v2, v2

    aput-char v2, p5, v18

    goto/16 :goto_2

    .line 347
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :pswitch_5
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fMode:I

    move/from16 v17, v18

    .line 348
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    goto/16 :goto_0

    .line 356
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :pswitch_6
    move/from16 v0, p3

    if-lt v11, v0, :cond_a

    .line 357
    add-int/lit8 v11, v11, -0x1

    .line 358
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBuffer:[B

    const/4 v3, 0x0

    sub-int v4, p3, v11

    move-object/from16 v0, p1

    invoke-static {v0, v11, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 361
    sub-int v2, p3, v11

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    .line 362
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    add-int/2addr v11, v2

    move/from16 v17, v18

    .line 363
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    goto/16 :goto_3

    .line 368
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :cond_a
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "bytePos":I
    .restart local v12    # "bytePos":I
    aget-byte v2, p1, v11

    and-int/lit16 v14, v2, 0xff

    .line 369
    .local v14, "dByte":I
    add-int/lit8 v17, v18, 0x1

    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    if-ltz v14, :cond_b

    const/16 v2, 0x80

    if-ge v14, v2, :cond_b

    sget-object v2, Lcom/ibm/icu/text/UnicodeDecompressor;->sOffsets:[I

    add-int/lit8 v3, v10, -0x1

    aget v2, v2, v3

    :goto_4
    add-int/2addr v2, v14

    int-to-char v2, v2

    aput-char v2, p5, v18

    move v11, v12

    .line 374
    .end local v12    # "bytePos":I
    .restart local v11    # "bytePos":I
    goto/16 :goto_2

    .line 369
    .end local v11    # "bytePos":I
    .restart local v12    # "bytePos":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    add-int/lit8 v3, v10, -0x1

    aget v2, v2, v3

    add-int/lit8 v2, v2, -0x80

    goto :goto_4

    .line 379
    .end local v12    # "bytePos":I
    .end local v14    # "dByte":I
    .end local v17    # "ucPos":I
    .restart local v11    # "bytePos":I
    .restart local v18    # "ucPos":I
    :pswitch_7
    add-int/lit8 v2, v10, -0x10

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    move/from16 v17, v18

    .line 380
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    goto/16 :goto_2

    .line 387
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :pswitch_8
    move/from16 v0, p3

    if-lt v11, v0, :cond_c

    .line 388
    add-int/lit8 v11, v11, -0x1

    .line 389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBuffer:[B

    const/4 v3, 0x0

    sub-int v4, p3, v11

    move-object/from16 v0, p1

    invoke-static {v0, v11, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 392
    sub-int v2, p3, v11

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    .line 393
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    add-int/2addr v11, v2

    move/from16 v17, v18

    .line 394
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    goto/16 :goto_3

    .line 397
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :cond_c
    add-int/lit8 v2, v10, -0x18

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    .line 398
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    sget-object v4, Lcom/ibm/icu/text/UnicodeDecompressor;->sOffsetTable:[I

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "bytePos":I
    .restart local v12    # "bytePos":I
    aget-byte v5, p1, v11

    and-int/lit16 v5, v5, 0xff

    aget v4, v4, v5

    aput v4, v2, v3

    move/from16 v17, v18

    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    move v11, v12

    .line 400
    .end local v12    # "bytePos":I
    .restart local v11    # "bytePos":I
    goto/16 :goto_2

    .line 406
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :pswitch_9
    add-int/lit8 v2, v11, 0x1

    move/from16 v0, p3

    if-lt v2, v0, :cond_d

    .line 407
    add-int/lit8 v11, v11, -0x1

    .line 408
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBuffer:[B

    const/4 v3, 0x0

    sub-int v4, p3, v11

    move-object/from16 v0, p1

    invoke-static {v0, v11, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 411
    sub-int v2, p3, v11

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    .line 412
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    add-int/2addr v11, v2

    move/from16 v17, v18

    .line 413
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    goto/16 :goto_3

    .line 416
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :cond_d
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "bytePos":I
    .restart local v12    # "bytePos":I
    aget-byte v2, p1, v11

    and-int/lit16 v10, v2, 0xff

    .line 417
    and-int/lit16 v2, v10, 0xe0

    shr-int/lit8 v2, v2, 0x5

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    .line 418
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    const/high16 v4, 0x10000

    and-int/lit8 v5, v10, 0x1f

    shl-int/lit8 v5, v5, 0x8

    add-int/lit8 v11, v12, 0x1

    .end local v12    # "bytePos":I
    .restart local v11    # "bytePos":I
    aget-byte v6, p1, v12

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v5, v6

    mul-int/lit16 v5, v5, 0x80

    add-int/2addr v4, v5

    aput v4, v2, v3

    move/from16 v17, v18

    .line 421
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    goto/16 :goto_2

    .line 504
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :cond_e
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "bytePos":I
    .restart local v12    # "bytePos":I
    aget-byte v10, p1, v11

    .line 505
    add-int/lit8 v17, v18, 0x1

    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    shl-int/lit8 v2, v10, 0x8

    add-int/lit8 v11, v12, 0x1

    .end local v12    # "bytePos":I
    .restart local v11    # "bytePos":I
    aget-byte v3, p1, v12

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    int-to-char v2, v2

    aput-char v2, p5, v18

    move/from16 v18, v17

    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    move v12, v11

    .line 434
    .end local v11    # "bytePos":I
    .restart local v12    # "bytePos":I
    :goto_5
    move/from16 v0, p3

    if-ge v12, v0, :cond_12

    move/from16 v0, v18

    move/from16 v1, p7

    if-ge v0, v1, :cond_12

    .line 435
    add-int/lit8 v11, v12, 0x1

    .end local v12    # "bytePos":I
    .restart local v11    # "bytePos":I
    aget-byte v2, p1, v12

    and-int/lit16 v10, v2, 0xff

    .line 436
    packed-switch v10, :pswitch_data_2

    .line 512
    move/from16 v0, p3

    if-lt v11, v0, :cond_11

    .line 513
    add-int/lit8 v11, v11, -0x1

    .line 514
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBuffer:[B

    const/4 v3, 0x0

    sub-int v4, p3, v11

    move-object/from16 v0, p1

    invoke-static {v0, v11, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 517
    sub-int v2, p3, v11

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    .line 518
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    add-int/2addr v11, v2

    move/from16 v17, v18

    .line 519
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    goto/16 :goto_3

    .line 442
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :pswitch_a
    move/from16 v0, p3

    if-lt v11, v0, :cond_f

    .line 443
    add-int/lit8 v11, v11, -0x1

    .line 444
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBuffer:[B

    const/4 v3, 0x0

    sub-int v4, p3, v11

    move-object/from16 v0, p1

    invoke-static {v0, v11, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 447
    sub-int v2, p3, v11

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    .line 448
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    add-int/2addr v11, v2

    move/from16 v17, v18

    .line 449
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    goto/16 :goto_3

    .line 452
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :cond_f
    add-int/lit16 v2, v10, -0xe8

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    .line 453
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    sget-object v4, Lcom/ibm/icu/text/UnicodeDecompressor;->sOffsetTable:[I

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "bytePos":I
    .restart local v12    # "bytePos":I
    aget-byte v5, p1, v11

    and-int/lit16 v5, v5, 0xff

    aget v4, v4, v5

    aput v4, v2, v3

    .line 455
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fMode:I

    move/from16 v17, v18

    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    move v11, v12

    .line 456
    .end local v12    # "bytePos":I
    .restart local v11    # "bytePos":I
    goto/16 :goto_0

    .line 463
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :pswitch_b
    add-int/lit8 v2, v11, 0x1

    move/from16 v0, p3

    if-lt v2, v0, :cond_10

    .line 464
    add-int/lit8 v11, v11, -0x1

    .line 465
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBuffer:[B

    const/4 v3, 0x0

    sub-int v4, p3, v11

    move-object/from16 v0, p1

    invoke-static {v0, v11, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 468
    sub-int v2, p3, v11

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    .line 469
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    add-int/2addr v11, v2

    move/from16 v17, v18

    .line 470
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    goto/16 :goto_3

    .line 473
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :cond_10
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "bytePos":I
    .restart local v12    # "bytePos":I
    aget-byte v2, p1, v11

    and-int/lit16 v10, v2, 0xff

    .line 474
    and-int/lit16 v2, v10, 0xe0

    shr-int/lit8 v2, v2, 0x5

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    .line 475
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    const/high16 v4, 0x10000

    and-int/lit8 v5, v10, 0x1f

    shl-int/lit8 v5, v5, 0x8

    add-int/lit8 v11, v12, 0x1

    .end local v12    # "bytePos":I
    .restart local v11    # "bytePos":I
    aget-byte v6, p1, v12

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v5, v6

    mul-int/lit16 v5, v5, 0x80

    add-int/2addr v4, v5

    aput v4, v2, v3

    .line 478
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fMode:I

    move/from16 v17, v18

    .line 479
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    goto/16 :goto_0

    .line 485
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :pswitch_c
    add-int/lit16 v2, v10, -0xe0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    .line 486
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fMode:I

    move/from16 v17, v18

    .line 487
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    goto/16 :goto_0

    .line 494
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :pswitch_d
    add-int/lit8 v2, p3, -0x1

    if-lt v11, v2, :cond_e

    .line 495
    add-int/lit8 v11, v11, -0x1

    .line 496
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBuffer:[B

    const/4 v3, 0x0

    sub-int v4, p3, v11

    move-object/from16 v0, p1

    invoke-static {v0, v11, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 499
    sub-int v2, p3, v11

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    .line 500
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    add-int/2addr v11, v2

    move/from16 v17, v18

    .line 501
    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    goto/16 :goto_3

    .line 522
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    :cond_11
    add-int/lit8 v17, v18, 0x1

    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    shl-int/lit8 v2, v10, 0x8

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "bytePos":I
    .restart local v12    # "bytePos":I
    aget-byte v3, p1, v11

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    int-to-char v2, v2

    aput-char v2, p5, v18

    move/from16 v18, v17

    .line 524
    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    goto/16 :goto_5

    :cond_12
    move/from16 v17, v18

    .end local v18    # "ucPos":I
    .restart local v17    # "ucPos":I
    move v11, v12

    .end local v12    # "bytePos":I
    .restart local v11    # "bytePos":I
    goto/16 :goto_0

    :pswitch_e
    move/from16 v18, v17

    .end local v17    # "ucPos":I
    .restart local v18    # "ucPos":I
    move v12, v11

    .end local v11    # "bytePos":I
    .restart local v12    # "bytePos":I
    goto/16 :goto_5

    .line 226
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_e
    .end packed-switch

    .line 232
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_9
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 436
    :pswitch_data_2
    .packed-switch 0xe0
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_d
        :pswitch_b
    .end packed-switch
.end method

.method public reset()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 548
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    const/16 v1, 0x80

    aput v1, v0, v3

    .line 549
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    const/4 v1, 0x1

    const/16 v2, 0xc0

    aput v2, v0, v1

    .line 550
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    const/4 v1, 0x2

    const/16 v2, 0x400

    aput v2, v0, v1

    .line 551
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    const/4 v1, 0x3

    const/16 v2, 0x600

    aput v2, v0, v1

    .line 552
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    const/4 v1, 0x4

    const/16 v2, 0x900

    aput v2, v0, v1

    .line 553
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    const/4 v1, 0x5

    const/16 v2, 0x3040

    aput v2, v0, v1

    .line 554
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    const/4 v1, 0x6

    const/16 v2, 0x30a0

    aput v2, v0, v1

    .line 555
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fOffsets:[I

    const/4 v1, 0x7

    const v2, 0xff00

    aput v2, v0, v1

    .line 558
    iput v3, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fCurrentWindow:I

    .line 559
    iput v3, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fMode:I

    .line 560
    iput v3, p0, Lcom/ibm/icu/text/UnicodeDecompressor;->fBufferLength:I

    .line 561
    return-void
.end method
