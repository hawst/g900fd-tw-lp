.class Lcom/ibm/icu/text/TransliterationRuleSet;
.super Ljava/lang/Object;
.source "TransliterationRuleSet.java"


# instance fields
.field private index:[I

.field private maxContextLength:I

.field private ruleVector:Ljava/util/Vector;

.field private rules:[Lcom/ibm/icu/text/TransliterationRule;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->ruleVector:Ljava/util/Vector;

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->maxContextLength:I

    .line 58
    return-void
.end method


# virtual methods
.method public addRule(Lcom/ibm/icu/text/TransliterationRule;)V
    .locals 2
    .param p1, "rule"    # Lcom/ibm/icu/text/TransliterationRule;

    .prologue
    .line 74
    iget-object v1, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->ruleVector:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 76
    invoke-virtual {p1}, Lcom/ibm/icu/text/TransliterationRule;->getAnteContextLength()I

    move-result v0

    .local v0, "len":I
    iget v1, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->maxContextLength:I

    if-le v0, v1, :cond_0

    .line 77
    iput v0, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->maxContextLength:I

    .line 80
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->rules:[Lcom/ibm/icu/text/TransliterationRule;

    .line 81
    return-void
.end method

.method public freeze()V
    .locals 13

    .prologue
    const/16 v12, 0x100

    .line 105
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->ruleVector:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v4

    .line 106
    .local v4, "n":I
    const/16 v10, 0x101

    new-array v10, v10, [I

    iput-object v10, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->index:[I

    .line 107
    new-instance v8, Ljava/util/Vector;

    mul-int/lit8 v10, v4, 0x2

    invoke-direct {v8, v10}, Ljava/util/Vector;-><init>(I)V

    .line 111
    .local v8, "v":Ljava/util/Vector;
    new-array v1, v4, [I

    .line 112
    .local v1, "indexValue":[I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    if-ge v2, v4, :cond_0

    .line 113
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->ruleVector:Ljava/util/Vector;

    invoke-virtual {v10, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ibm/icu/text/TransliterationRule;

    .line 114
    .local v5, "r":Lcom/ibm/icu/text/TransliterationRule;
    invoke-virtual {v5}, Lcom/ibm/icu/text/TransliterationRule;->getIndexValue()I

    move-result v10

    aput v10, v1, v2

    .line 112
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 116
    .end local v5    # "r":Lcom/ibm/icu/text/TransliterationRule;
    :cond_0
    const/4 v9, 0x0

    .local v9, "x":I
    :goto_1
    if-ge v9, v12, :cond_4

    .line 117
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->index:[I

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v11

    aput v11, v10, v9

    .line 118
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_3

    .line 119
    aget v10, v1, v2

    if-ltz v10, :cond_2

    .line 120
    aget v10, v1, v2

    if-ne v10, v9, :cond_1

    .line 121
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->ruleVector:Ljava/util/Vector;

    invoke-virtual {v10, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 118
    :cond_1
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 128
    :cond_2
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->ruleVector:Ljava/util/Vector;

    invoke-virtual {v10, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ibm/icu/text/TransliterationRule;

    .line 129
    .restart local v5    # "r":Lcom/ibm/icu/text/TransliterationRule;
    invoke-virtual {v5, v9}, Lcom/ibm/icu/text/TransliterationRule;->matchesIndexValue(I)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 130
    invoke-virtual {v8, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_3

    .line 116
    .end local v5    # "r":Lcom/ibm/icu/text/TransliterationRule;
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 135
    :cond_4
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->index:[I

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v11

    aput v11, v10, v12

    .line 139
    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v10

    new-array v10, v10, [Lcom/ibm/icu/text/TransliterationRule;

    iput-object v10, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->rules:[Lcom/ibm/icu/text/TransliterationRule;

    .line 140
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->rules:[Lcom/ibm/icu/text/TransliterationRule;

    invoke-virtual {v8, v10}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    .line 142
    const/4 v0, 0x0

    .line 151
    .local v0, "errors":Ljava/lang/StringBuffer;
    const/4 v9, 0x0

    :goto_4
    if-ge v9, v12, :cond_9

    .line 152
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->index:[I

    aget v2, v10, v9

    :goto_5
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->index:[I

    add-int/lit8 v11, v9, 0x1

    aget v10, v10, v11

    add-int/lit8 v10, v10, -0x1

    if-ge v2, v10, :cond_8

    .line 153
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->rules:[Lcom/ibm/icu/text/TransliterationRule;

    aget-object v6, v10, v2

    .line 154
    .local v6, "r1":Lcom/ibm/icu/text/TransliterationRule;
    add-int/lit8 v3, v2, 0x1

    .local v3, "k":I
    :goto_6
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->index:[I

    add-int/lit8 v11, v9, 0x1

    aget v10, v10, v11

    if-ge v3, v10, :cond_7

    .line 155
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->rules:[Lcom/ibm/icu/text/TransliterationRule;

    aget-object v7, v10, v3

    .line 156
    .local v7, "r2":Lcom/ibm/icu/text/TransliterationRule;
    invoke-virtual {v6, v7}, Lcom/ibm/icu/text/TransliterationRule;->masks(Lcom/ibm/icu/text/TransliterationRule;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 157
    if-nez v0, :cond_6

    .line 158
    new-instance v0, Ljava/lang/StringBuffer;

    .end local v0    # "errors":Ljava/lang/StringBuffer;
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 162
    .restart local v0    # "errors":Ljava/lang/StringBuffer;
    :goto_7
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v11, "Rule "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string/jumbo v11, " masks "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 154
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 160
    :cond_6
    const-string/jumbo v10, "\n"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_7

    .line 152
    .end local v7    # "r2":Lcom/ibm/icu/text/TransliterationRule;
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 151
    .end local v3    # "k":I
    .end local v6    # "r1":Lcom/ibm/icu/text/TransliterationRule;
    :cond_8
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 168
    :cond_9
    if-eqz v0, :cond_a

    .line 169
    new-instance v10, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 171
    :cond_a
    return-void
.end method

.method public getMaximumContextLength()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->maxContextLength:I

    return v0
.end method

.method getSourceTargetSet(Z)Lcom/ibm/icu/text/UnicodeSet;
    .locals 5
    .param p1, "getTarget"    # Z

    .prologue
    .line 245
    new-instance v3, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v3}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 246
    .local v3, "set":Lcom/ibm/icu/text/UnicodeSet;
    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->ruleVector:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v0

    .line 247
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 248
    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->ruleVector:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/TransliterationRule;

    .line 250
    .local v2, "r":Lcom/ibm/icu/text/TransliterationRule;
    if-eqz p1, :cond_0

    .line 251
    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/TransliterationRule;->addTargetSetTo(Lcom/ibm/icu/text/UnicodeSet;)V

    .line 247
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 253
    :cond_0
    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/TransliterationRule;->addSourceSetTo(Lcom/ibm/icu/text/UnicodeSet;)V

    goto :goto_1

    .line 256
    .end local v2    # "r":Lcom/ibm/icu/text/TransliterationRule;
    :cond_1
    return-object v3
.end method

.method toRules(Z)Ljava/lang/String;
    .locals 5
    .param p1, "escapeUnprintable"    # Z

    .prologue
    .line 227
    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->ruleVector:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v0

    .line 228
    .local v0, "count":I
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 229
    .local v3, "ruleSource":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 230
    if-eqz v1, :cond_0

    .line 231
    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 233
    :cond_0
    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->ruleVector:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/TransliterationRule;

    .line 235
    .local v2, "r":Lcom/ibm/icu/text/TransliterationRule;
    invoke-virtual {v2, p1}, Lcom/ibm/icu/text/TransliterationRule;->toRule(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 229
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 237
    .end local v2    # "r":Lcom/ibm/icu/text/TransliterationRule;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public transliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)Z
    .locals 6
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "pos"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "incremental"    # Z

    .prologue
    const/4 v3, 0x1

    .line 189
    iget v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    invoke-interface {p1, v4}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v4

    and-int/lit16 v1, v4, 0xff

    .line 190
    .local v1, "indexByte":I
    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->index:[I

    aget v0, v4, v1

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->index:[I

    add-int/lit8 v5, v1, 0x1

    aget v4, v4, v5

    if-ge v0, v4, :cond_0

    .line 191
    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRuleSet;->rules:[Lcom/ibm/icu/text/TransliterationRule;

    aget-object v4, v4, v0

    invoke-virtual {v4, p1, p2, p3}, Lcom/ibm/icu/text/TransliterationRule;->matchAndReplace(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)I

    move-result v2

    .line 192
    .local v2, "m":I
    packed-switch v2, :pswitch_data_0

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :pswitch_0
    const/4 v3, 0x0

    .line 219
    .end local v2    # "m":I
    :goto_1
    :pswitch_1
    return v3

    .line 214
    :cond_0
    iget v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    iget v5, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    invoke-interface {p1, v5}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v5

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    goto :goto_1

    .line 192
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
