.class Lcom/ibm/icu/text/NumberFormatServiceShim;
.super Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;
.source "NumberFormatServiceShim.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/NumberFormatServiceShim$NFService;,
        Lcom/ibm/icu/text/NumberFormatServiceShim$NFFactory;
    }
.end annotation


# static fields
.field private static service:Lcom/ibm/icu/impl/ICULocaleService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lcom/ibm/icu/text/NumberFormatServiceShim$NFService;

    invoke-direct {v0}, Lcom/ibm/icu/text/NumberFormatServiceShim$NFService;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/NumberFormatServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;-><init>()V

    .line 102
    return-void
.end method


# virtual methods
.method createInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;
    .locals 7
    .param p1, "desiredLocale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "choice"    # I

    .prologue
    .line 86
    const/4 v3, 0x1

    new-array v0, v3, [Lcom/ibm/icu/util/ULocale;

    .line 87
    .local v0, "actualLoc":[Lcom/ibm/icu/util/ULocale;
    sget-object v3, Lcom/ibm/icu/util/ULocale;->ROOT:Lcom/ibm/icu/util/ULocale;

    invoke-virtual {p1, v3}, Lcom/ibm/icu/util/ULocale;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 88
    sget-object p1, Lcom/ibm/icu/util/ULocale;->ROOT:Lcom/ibm/icu/util/ULocale;

    .line 90
    :cond_0
    sget-object v3, Lcom/ibm/icu/text/NumberFormatServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v3, p1, p2, v0}, Lcom/ibm/icu/impl/ICULocaleService;->get(Lcom/ibm/icu/util/ULocale;I[Lcom/ibm/icu/util/ULocale;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/NumberFormat;

    .line 92
    .local v1, "fmt":Lcom/ibm/icu/text/NumberFormat;
    if-nez v1, :cond_1

    .line 93
    new-instance v3, Ljava/util/MissingResourceException;

    const-string/jumbo v4, "Unable to construct NumberFormat"

    const-string/jumbo v5, ""

    const-string/jumbo v6, ""

    invoke-direct {v3, v4, v5, v6}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 95
    :cond_1
    invoke-virtual {v1}, Lcom/ibm/icu/text/NumberFormat;->clone()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "fmt":Lcom/ibm/icu/text/NumberFormat;
    check-cast v1, Lcom/ibm/icu/text/NumberFormat;

    .line 97
    .restart local v1    # "fmt":Lcom/ibm/icu/text/NumberFormat;
    const/4 v3, 0x0

    aget-object v2, v0, v3

    .line 98
    .local v2, "uloc":Lcom/ibm/icu/util/ULocale;
    invoke-virtual {v1, v2, v2}, Lcom/ibm/icu/text/NumberFormat;->setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V

    .line 99
    return-object v1
.end method

.method getAvailableLocales()[Ljava/util/Locale;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/ibm/icu/text/NumberFormatServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICULocaleService;->isDefault()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableLocales(Ljava/lang/String;)[Ljava/util/Locale;

    move-result-object v0

    .line 32
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/text/NumberFormatServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICULocaleService;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v0

    goto :goto_0
.end method

.method getAvailableULocales()[Lcom/ibm/icu/util/ULocale;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/ibm/icu/text/NumberFormatServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICULocaleService;->isDefault()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableULocales(Ljava/lang/String;)[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    .line 39
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/text/NumberFormatServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICULocaleService;->getAvailableULocales()[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    goto :goto_0
.end method

.method registerFactory(Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;)Ljava/lang/Object;
    .locals 2
    .param p1, "factory"    # Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;

    .prologue
    .line 72
    sget-object v0, Lcom/ibm/icu/text/NumberFormatServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    new-instance v1, Lcom/ibm/icu/text/NumberFormatServiceShim$NFFactory;

    invoke-direct {v1, p1}, Lcom/ibm/icu/text/NumberFormatServiceShim$NFFactory;-><init>(Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;)V

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ICULocaleService;->registerFactory(Lcom/ibm/icu/impl/ICUService$Factory;)Lcom/ibm/icu/impl/ICUService$Factory;

    move-result-object v0

    return-object v0
.end method

.method unregister(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "registryKey"    # Ljava/lang/Object;

    .prologue
    .line 76
    sget-object v0, Lcom/ibm/icu/text/NumberFormatServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    check-cast p1, Lcom/ibm/icu/impl/ICUService$Factory;

    .end local p1    # "registryKey":Ljava/lang/Object;
    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/ICULocaleService;->unregisterFactory(Lcom/ibm/icu/impl/ICUService$Factory;)Z

    move-result v0

    return v0
.end method
