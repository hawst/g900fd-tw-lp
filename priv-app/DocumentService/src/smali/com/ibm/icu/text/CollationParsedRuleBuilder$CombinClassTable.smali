.class Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;
.super Ljava/lang/Object;
.source "CollationParsedRuleBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CollationParsedRuleBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CombinClassTable"
.end annotation


# instance fields
.field cPoints:[C

.field curClass:I

.field index:[I

.field pos:I

.field size:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 942
    const/16 v0, 0x100

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->index:[I

    .line 956
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->cPoints:[C

    .line 957
    iput v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->size:I

    .line 958
    iput v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->pos:I

    .line 959
    const/4 v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->curClass:I

    .line 960
    return-void
.end method


# virtual methods
.method GetFirstCM(I)C
    .locals 3
    .param p1, "cClass"    # I

    .prologue
    .line 989
    iput p1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->curClass:I

    .line 990
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->cPoints:[C

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->index:[I

    aget v0, v0, p1

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->index:[I

    add-int/lit8 v2, p1, -0x1

    aget v1, v1, v2

    if-ne v0, v1, :cond_1

    .line 991
    :cond_0
    const/4 v0, 0x0

    .line 994
    :goto_0
    return v0

    .line 993
    :cond_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->pos:I

    .line 994
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->cPoints:[C

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->index:[I

    add-int/lit8 v2, p1, -0x1

    aget v1, v1, v2

    aget-char v0, v0, v1

    goto :goto_0
.end method

.method GetNextCM()C
    .locals 4

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->cPoints:[C

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->index:[I

    iget v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->curClass:I

    aget v0, v0, v1

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->index:[I

    iget v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->curClass:I

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    iget v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->pos:I

    add-int/2addr v1, v2

    if-ne v0, v1, :cond_1

    .line 1003
    :cond_0
    const/4 v0, 0x0

    .line 1005
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->cPoints:[C

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->index:[I

    iget v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->curClass:I

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    iget v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->pos:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->pos:I

    add-int/2addr v1, v2

    aget-char v0, v0, v1

    goto :goto_0
.end method

.method generate([CI[I)V
    .locals 6
    .param p1, "cps"    # [C
    .param p2, "numOfCM"    # I
    .param p3, "ccIndex"    # [I

    .prologue
    .line 971
    const/4 v0, 0x0

    .line 973
    .local v0, "count":I
    new-array v4, p2, [C

    iput-object v4, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->cPoints:[C

    .line 974
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/16 v4, 0x100

    if-ge v2, v4, :cond_1

    .line 975
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    aget v4, p3, v2

    if-ge v3, v4, :cond_0

    .line 976
    iget-object v4, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->cPoints:[C

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    shl-int/lit8 v5, v2, 0x8

    add-int/2addr v5, v3

    aget-char v5, p1, v5

    aput-char v5, v4, v0

    .line 975
    add-int/lit8 v3, v3, 0x1

    move v0, v1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    goto :goto_1

    .line 978
    :cond_0
    iget-object v4, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->index:[I

    aput v0, v4, v2

    .line 974
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 980
    .end local v3    # "j":I
    :cond_1
    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->size:I

    .line 981
    return-void
.end method
