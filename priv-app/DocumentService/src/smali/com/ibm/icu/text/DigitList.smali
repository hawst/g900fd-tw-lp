.class final Lcom/ibm/icu/text/DigitList;
.super Ljava/lang/Object;
.source "DigitList.java"


# static fields
.field public static final DBL_DIG:I = 0x11

.field private static LONG_MIN_REP:[B = null

.field public static final MAX_LONG_DIGITS:I = 0x13


# instance fields
.field public count:I

.field public decimalAt:I

.field public digits:[B


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x13

    .line 782
    const-wide/high16 v2, -0x8000000000000000L

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 783
    .local v1, "s":Ljava/lang/String;
    new-array v2, v4, [B

    sput-object v2, Lcom/ibm/icu/text/DigitList;->LONG_MIN_REP:[B

    .line 784
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 786
    sget-object v2, Lcom/ibm/icu/text/DigitList;->LONG_MIN_REP:[B

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v2, v0

    .line 784
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 788
    :cond_0
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput v0, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 76
    iput v0, p0, Lcom/ibm/icu/text/DigitList;->count:I

    .line 77
    const/16 v0, 0x13

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    return-void
.end method

.method private final ensureCapacity(II)V
    .locals 3
    .param p1, "digitCapacity"    # I
    .param p2, "digitsToCopy"    # I

    .prologue
    const/4 v2, 0x0

    .line 80
    iget-object v1, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    array-length v1, v1

    if-le p1, v1, :cond_0

    .line 81
    mul-int/lit8 v1, p1, 0x2

    new-array v0, v1, [B

    .line 82
    .local v0, "newDigits":[B
    iget-object v1, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    invoke-static {v1, v2, v0, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 83
    iput-object v0, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    .line 85
    .end local v0    # "newDigits":[B
    :cond_0
    return-void
.end method

.method private getStringRep(Z)Ljava/lang/String;
    .locals 7
    .param p1, "isPositive"    # Z

    .prologue
    const/16 v6, 0x30

    const/16 v5, 0x2e

    .line 205
    invoke-virtual {p0}, Lcom/ibm/icu/text/DigitList;->isZero()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string/jumbo v4, "0"

    .line 228
    :goto_0
    return-object v4

    .line 206
    :cond_0
    new-instance v3, Ljava/lang/StringBuffer;

    iget v4, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v4, v4, 0x1

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 207
    .local v3, "stringRep":Ljava/lang/StringBuffer;
    if-nez p1, :cond_1

    .line 208
    const/16 v4, 0x2d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 210
    :cond_1
    iget v0, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 211
    .local v0, "d":I
    if-gez v0, :cond_3

    .line 212
    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 213
    :goto_1
    if-gez v0, :cond_2

    .line 214
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 215
    add-int/lit8 v0, v0, 0x1

    .line 216
    goto :goto_1

    .line 217
    :cond_2
    const/4 v0, -0x1

    .line 219
    :cond_3
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    iget v4, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge v2, v4, :cond_5

    .line 220
    if-ne v0, v2, :cond_4

    .line 221
    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 223
    :cond_4
    iget-object v4, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v4, v4, v2

    int-to-char v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 219
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 225
    :cond_5
    :goto_3
    add-int/lit8 v1, v0, -0x1

    .end local v0    # "d":I
    .local v1, "d":I
    iget v4, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-le v0, v4, :cond_6

    .line 226
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v0, v1

    .line 227
    .end local v1    # "d":I
    .restart local v0    # "d":I
    goto :goto_3

    .line 228
    .end local v0    # "d":I
    .restart local v1    # "d":I
    :cond_6
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private isLongMIN_VALUE()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 766
    iget v2, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    iget v3, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    const/16 v3, 0x13

    if-eq v2, v3, :cond_1

    .line 774
    :cond_0
    :goto_0
    return v1

    .line 769
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge v0, v2, :cond_2

    .line 771
    iget-object v2, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v2, v2, v0

    sget-object v3, Lcom/ibm/icu/text/DigitList;->LONG_MIN_REP:[B

    aget-byte v3, v3, v0

    if-ne v2, v3, :cond_0

    .line 769
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 774
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private set(Ljava/lang/String;I)V
    .locals 10
    .param p1, "rep"    # Ljava/lang/String;
    .param p2, "maxCount"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v9, -0x1

    .line 448
    iput v9, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 449
    iput v5, p0, Lcom/ibm/icu/text/DigitList;->count:I

    .line 450
    const/4 v1, 0x0

    .line 453
    .local v1, "exponent":I
    const/4 v3, 0x0

    .line 454
    .local v3, "leadingZerosAfterDecimal":I
    const/4 v4, 0x0

    .line 456
    .local v4, "nonZeroDigitSeen":Z
    const/4 v2, 0x0

    .line 457
    .local v2, "i":I
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x2d

    if-ne v6, v7, :cond_0

    .line 458
    add-int/lit8 v2, v2, 0x1

    .line 460
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v2, v6, :cond_5

    .line 461
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 462
    .local v0, "c":C
    const/16 v6, 0x2e

    if-ne v0, v6, :cond_2

    .line 463
    iget v6, p0, Lcom/ibm/icu/text/DigitList;->count:I

    iput v6, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 460
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 464
    :cond_2
    const/16 v6, 0x65

    if-eq v0, v6, :cond_3

    const/16 v6, 0x45

    if-ne v0, v6, :cond_7

    .line 465
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 467
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x2b

    if-ne v5, v6, :cond_4

    .line 468
    add-int/lit8 v2, v2, 0x1

    .line 470
    :cond_4
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 486
    .end local v0    # "c":C
    :cond_5
    iget v5, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    if-ne v5, v9, :cond_6

    .line 487
    iget v5, p0, Lcom/ibm/icu/text/DigitList;->count:I

    iput v5, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 489
    :cond_6
    iget v5, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    sub-int v6, v1, v3

    add-int/2addr v5, v6

    iput v5, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 490
    return-void

    .line 472
    .restart local v0    # "c":C
    :cond_7
    iget v6, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge v6, p2, :cond_1

    .line 473
    if-nez v4, :cond_8

    .line 474
    const/16 v6, 0x30

    if-eq v0, v6, :cond_9

    const/4 v4, 0x1

    .line 475
    :goto_2
    if-nez v4, :cond_8

    iget v6, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    if-eq v6, v9, :cond_8

    .line 476
    add-int/lit8 v3, v3, 0x1

    .line 480
    :cond_8
    if-eqz v4, :cond_1

    .line 481
    iget v6, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v6, v6, 0x1

    iget v7, p0, Lcom/ibm/icu/text/DigitList;->count:I

    invoke-direct {p0, v6, v7}, Lcom/ibm/icu/text/DigitList;->ensureCapacity(II)V

    .line 482
    iget-object v6, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    iget v7, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lcom/ibm/icu/text/DigitList;->count:I

    int-to-byte v8, v0

    aput-byte v8, v6, v7

    goto :goto_1

    :cond_9
    move v4, v5

    .line 474
    goto :goto_2
.end method

.method private setBigDecimalDigits(Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "stringDigits"    # Ljava/lang/String;
    .param p2, "maximumDigits"    # I
    .param p3, "fixedPoint"    # Z

    .prologue
    .line 714
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/DigitList;->set(Ljava/lang/String;I)V

    .line 724
    if-eqz p3, :cond_1

    iget v0, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    add-int/2addr p2, v0

    .end local p2    # "maximumDigits":I
    :cond_0
    :goto_0
    invoke-virtual {p0, p2}, Lcom/ibm/icu/text/DigitList;->round(I)V

    .line 725
    return-void

    .line 724
    .restart local p2    # "maximumDigits":I
    :cond_1
    if-nez p2, :cond_0

    const/4 p2, -0x1

    goto :goto_0
.end method

.method private shouldRoundUp(I)Z
    .locals 5
    .param p1, "maximumDigits"    # I

    .prologue
    const/16 v4, 0x35

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 510
    iget v3, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge p1, v3, :cond_4

    .line 511
    iget-object v3, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v3, v3, p1

    if-le v3, v4, :cond_1

    .line 522
    :cond_0
    :goto_0
    return v1

    .line 513
    :cond_1
    iget-object v3, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v3, v3, p1

    if-ne v3, v4, :cond_4

    .line 514
    add-int/lit8 v0, p1, 0x1

    .local v0, "i":I
    :goto_1
    iget v3, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge v0, v3, :cond_2

    .line 515
    iget-object v3, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v3, v3, v0

    const/16 v4, 0x30

    if-ne v3, v4, :cond_0

    .line 514
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 519
    :cond_2
    if-lez p1, :cond_3

    iget-object v3, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    add-int/lit8 v4, p1, -0x1

    aget-byte v3, v3, v4

    rem-int/lit8 v3, v3, 0x2

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .end local v0    # "i":I
    :cond_4
    move v1, v2

    .line 522
    goto :goto_0
.end method


# virtual methods
.method public append(I)V
    .locals 3
    .param p1, "digit"    # I

    .prologue
    .line 113
    iget v0, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/ibm/icu/text/DigitList;->count:I

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/DigitList;->ensureCapacity(II)V

    .line 114
    iget-object v0, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    iget v1, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 115
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 829
    if-ne p0, p1, :cond_1

    .line 840
    :cond_0
    :goto_0
    return v2

    .line 831
    :cond_1
    instance-of v4, p1, Lcom/ibm/icu/text/DigitList;

    if-nez v4, :cond_2

    move v2, v3

    .line 832
    goto :goto_0

    :cond_2
    move-object v1, p1

    .line 833
    check-cast v1, Lcom/ibm/icu/text/DigitList;

    .line 834
    .local v1, "other":Lcom/ibm/icu/text/DigitList;
    iget v4, p0, Lcom/ibm/icu/text/DigitList;->count:I

    iget v5, v1, Lcom/ibm/icu/text/DigitList;->count:I

    if-ne v4, v5, :cond_3

    iget v4, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    iget v5, v1, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    if-eq v4, v5, :cond_4

    :cond_3
    move v2, v3

    .line 836
    goto :goto_0

    .line 837
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v4, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge v0, v4, :cond_0

    .line 838
    iget-object v4, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v4, v4, v0

    iget-object v5, v1, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v5, v5, v0

    if-eq v4, v5, :cond_5

    move v2, v3

    .line 839
    goto :goto_0

    .line 837
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getBigDecimal(Z)Ljava/math/BigDecimal;
    .locals 12
    .param p1, "isPositive"    # Z

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/ibm/icu/text/DigitList;->isZero()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 242
    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v8

    .line 274
    :goto_0
    return-object v8

    .line 248
    :cond_0
    iget v8, p0, Lcom/ibm/icu/text/DigitList;->count:I

    int-to-long v8, v8

    iget v10, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    int-to-long v10, v10

    sub-long v4, v8, v10

    .line 249
    .local v4, "scale":J
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-lez v8, :cond_5

    .line 250
    iget v1, p0, Lcom/ibm/icu/text/DigitList;->count:I

    .line 251
    .local v1, "numDigits":I
    const-wide/32 v8, 0x7fffffff

    cmp-long v8, v4, v8

    if-lez v8, :cond_1

    .line 253
    const-wide/32 v8, 0x7fffffff

    sub-long v2, v4, v8

    .line 254
    .local v2, "numShift":J
    iget v8, p0, Lcom/ibm/icu/text/DigitList;->count:I

    int-to-long v8, v8

    cmp-long v8, v2, v8

    if-gez v8, :cond_3

    .line 255
    int-to-long v8, v1

    sub-long/2addr v8, v2

    long-to-int v1, v8

    .line 261
    .end local v2    # "numShift":J
    :cond_1
    new-instance v6, Ljava/lang/StringBuffer;

    add-int/lit8 v8, v1, 0x1

    invoke-direct {v6, v8}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 262
    .local v6, "significantDigits":Ljava/lang/StringBuffer;
    if-nez p1, :cond_2

    .line 263
    const/16 v8, 0x2d

    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 265
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_4

    .line 266
    iget-object v8, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v8, v8, v0

    int-to-char v8, v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 265
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 258
    .end local v0    # "i":I
    .end local v6    # "significantDigits":Ljava/lang/StringBuffer;
    .restart local v2    # "numShift":J
    :cond_3
    new-instance v8, Ljava/math/BigDecimal;

    const/4 v9, 0x0

    invoke-direct {v8, v9}, Ljava/math/BigDecimal;-><init>(I)V

    goto :goto_0

    .line 268
    .end local v2    # "numShift":J
    .restart local v0    # "i":I
    .restart local v6    # "significantDigits":Ljava/lang/StringBuffer;
    :cond_4
    new-instance v7, Ljava/math/BigInteger;

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    .line 269
    .local v7, "unscaledVal":Ljava/math/BigInteger;
    new-instance v8, Ljava/math/BigDecimal;

    long-to-int v9, v4

    invoke-direct {v8, v7, v9}, Ljava/math/BigDecimal;-><init>(Ljava/math/BigInteger;I)V

    goto :goto_0

    .line 274
    .end local v0    # "i":I
    .end local v1    # "numDigits":I
    .end local v6    # "significantDigits":Ljava/lang/StringBuffer;
    .end local v7    # "unscaledVal":Ljava/math/BigInteger;
    :cond_5
    new-instance v8, Ljava/math/BigDecimal;

    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DigitList;->getStringRep(Z)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getBigDecimalICU(Z)Lcom/ibm/icu/math/BigDecimal;
    .locals 12
    .param p1, "isPositive"    # Z

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/ibm/icu/text/DigitList;->isZero()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 288
    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Lcom/ibm/icu/math/BigDecimal;->valueOf(J)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v8

    .line 317
    :goto_0
    return-object v8

    .line 294
    :cond_0
    iget v8, p0, Lcom/ibm/icu/text/DigitList;->count:I

    int-to-long v8, v8

    iget v10, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    int-to-long v10, v10

    sub-long v4, v8, v10

    .line 295
    .local v4, "scale":J
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-lez v8, :cond_5

    .line 296
    iget v1, p0, Lcom/ibm/icu/text/DigitList;->count:I

    .line 297
    .local v1, "numDigits":I
    const-wide/32 v8, 0x7fffffff

    cmp-long v8, v4, v8

    if-lez v8, :cond_1

    .line 299
    const-wide/32 v8, 0x7fffffff

    sub-long v2, v4, v8

    .line 300
    .local v2, "numShift":J
    iget v8, p0, Lcom/ibm/icu/text/DigitList;->count:I

    int-to-long v8, v8

    cmp-long v8, v2, v8

    if-gez v8, :cond_3

    .line 301
    int-to-long v8, v1

    sub-long/2addr v8, v2

    long-to-int v1, v8

    .line 307
    .end local v2    # "numShift":J
    :cond_1
    new-instance v6, Ljava/lang/StringBuffer;

    add-int/lit8 v8, v1, 0x1

    invoke-direct {v6, v8}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 308
    .local v6, "significantDigits":Ljava/lang/StringBuffer;
    if-nez p1, :cond_2

    .line 309
    const/16 v8, 0x2d

    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 311
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_4

    .line 312
    iget-object v8, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v8, v8, v0

    int-to-char v8, v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 311
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 304
    .end local v0    # "i":I
    .end local v6    # "significantDigits":Ljava/lang/StringBuffer;
    .restart local v2    # "numShift":J
    :cond_3
    new-instance v8, Lcom/ibm/icu/math/BigDecimal;

    const/4 v9, 0x0

    invoke-direct {v8, v9}, Lcom/ibm/icu/math/BigDecimal;-><init>(I)V

    goto :goto_0

    .line 314
    .end local v2    # "numShift":J
    .restart local v0    # "i":I
    .restart local v6    # "significantDigits":Ljava/lang/StringBuffer;
    :cond_4
    new-instance v7, Ljava/math/BigInteger;

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    .line 315
    .local v7, "unscaledVal":Ljava/math/BigInteger;
    new-instance v8, Lcom/ibm/icu/math/BigDecimal;

    long-to-int v9, v4

    invoke-direct {v8, v7, v9}, Lcom/ibm/icu/math/BigDecimal;-><init>(Ljava/math/BigInteger;I)V

    goto :goto_0

    .line 317
    .end local v0    # "i":I
    .end local v1    # "numDigits":I
    .end local v6    # "significantDigits":Ljava/lang/StringBuffer;
    .end local v7    # "unscaledVal":Ljava/math/BigInteger;
    :cond_5
    new-instance v8, Lcom/ibm/icu/math/BigDecimal;

    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DigitList;->getStringRep(Z)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/ibm/icu/math/BigDecimal;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getBigInteger(Z)Ljava/math/BigInteger;
    .locals 6
    .param p1, "isPositive"    # Z

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/ibm/icu/text/DigitList;->isZero()Z

    move-result v4

    if-eqz v4, :cond_0

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v4

    .line 200
    :goto_0
    return-object v4

    .line 179
    :cond_0
    iget v4, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    iget v5, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-le v4, v5, :cond_2

    iget v1, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 180
    .local v1, "len":I
    :goto_1
    if-nez p1, :cond_1

    .line 181
    add-int/lit8 v1, v1, 0x1

    .line 183
    :cond_1
    new-array v3, v1, [C

    .line 184
    .local v3, "text":[C
    const/4 v2, 0x0

    .line 185
    .local v2, "n":I
    if-nez p1, :cond_4

    .line 186
    const/4 v4, 0x0

    const/16 v5, 0x2d

    aput-char v5, v3, v4

    .line 187
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget v4, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge v0, v4, :cond_3

    .line 188
    add-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v5, v5, v0

    int-to-char v5, v5

    aput-char v5, v3, v4

    .line 187
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 179
    .end local v0    # "i":I
    .end local v1    # "len":I
    .end local v2    # "n":I
    .end local v3    # "text":[C
    :cond_2
    iget v1, p0, Lcom/ibm/icu/text/DigitList;->count:I

    goto :goto_1

    .line 190
    .restart local v0    # "i":I
    .restart local v1    # "len":I
    .restart local v2    # "n":I
    .restart local v3    # "text":[C
    :cond_3
    iget v4, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v2, v4, 0x1

    .line 197
    :goto_3
    move v0, v2

    :goto_4
    array-length v4, v3

    if-ge v0, v4, :cond_6

    .line 198
    const/16 v4, 0x30

    aput-char v4, v3, v0

    .line 197
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 192
    .end local v0    # "i":I
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_5
    iget v4, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge v0, v4, :cond_5

    .line 193
    iget-object v4, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v4, v4, v0

    int-to-char v4, v4

    aput-char v4, v3, v0

    .line 192
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 195
    :cond_5
    iget v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    goto :goto_3

    .line 200
    :cond_6
    new-instance v4, Ljava/math/BigInteger;

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v3}, Ljava/lang/String;-><init>([C)V

    invoke-direct {v4, v5}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final getDouble()D
    .locals 4

    .prologue
    .line 122
    iget v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-nez v2, :cond_0

    const-wide/16 v2, 0x0

    .line 128
    :goto_0
    return-wide v2

    .line 123
    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    iget v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 124
    .local v1, "temp":Ljava/lang/StringBuffer;
    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 125
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v2, v2, v0

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 126
    :cond_1
    const/16 v2, 0x45

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 127
    iget v2, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 128
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    goto :goto_0
.end method

.method public final getLong()J
    .locals 4

    .prologue
    .line 140
    iget v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-nez v2, :cond_0

    const-wide/16 v2, 0x0

    .line 152
    :goto_0
    return-wide v2

    .line 145
    :cond_0
    invoke-direct {p0}, Lcom/ibm/icu/text/DigitList;->isLongMIN_VALUE()Z

    move-result v2

    if-eqz v2, :cond_1

    const-wide/high16 v2, -0x8000000000000000L

    goto :goto_0

    .line 147
    :cond_1
    new-instance v1, Ljava/lang/StringBuffer;

    iget v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 148
    .local v1, "temp":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    if-ge v0, v2, :cond_3

    .line 150
    iget v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v2, v2, v0

    int-to-char v2, v2

    :goto_2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 148
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 150
    :cond_2
    const/16 v2, 0x30

    goto :goto_2

    .line 152
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 847
    iget v0, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 849
    .local v0, "hashcode":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge v1, v2, :cond_0

    .line 850
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v3, v3, v1

    add-int v0, v2, v3

    .line 849
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 852
    :cond_0
    return v0
.end method

.method isIntegral()Z
    .locals 2

    .prologue
    .line 328
    :goto_0
    iget v0, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    iget v1, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v1, v1, -0x1

    aget-byte v0, v0, v1

    const/16 v1, 0x30

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/DigitList;->count:I

    goto :goto_0

    .line 329
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    iget v1, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-lt v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method isZero()Z
    .locals 3

    .prologue
    .line 92
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v1, v1, v0

    const/16 v2, 0x30

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    .line 93
    :goto_1
    return v1

    .line 92
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final round(I)V
    .locals 3
    .param p1, "maximumDigits"    # I

    .prologue
    .line 535
    if-ltz p1, :cond_3

    iget v0, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge p1, v0, :cond_3

    .line 536
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DigitList;->shouldRoundUp(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 542
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 543
    if-gez p1, :cond_2

    .line 547
    iget-object v0, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    const/4 v1, 0x0

    const/16 v2, 0x31

    aput-byte v2, v0, v1

    .line 548
    iget v0, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 549
    const/4 p1, 0x0

    .line 557
    :goto_0
    add-int/lit8 p1, p1, 0x1

    .line 559
    :cond_1
    iput p1, p0, Lcom/ibm/icu/text/DigitList;->count:I

    .line 563
    :goto_1
    iget v0, p0, Lcom/ibm/icu/text/DigitList;->count:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_3

    iget-object v0, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    iget v1, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v1, v1, -0x1

    aget-byte v0, v0, v1

    const/16 v1, 0x30

    if-ne v0, v1, :cond_3

    .line 564
    iget v0, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/DigitList;->count:I

    goto :goto_1

    .line 553
    :cond_2
    iget-object v0, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v1, v0, p1

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    aput-byte v1, v0, p1

    .line 554
    iget-object v0, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v0, v0, p1

    const/16 v1, 0x39

    if-gt v0, v1, :cond_0

    goto :goto_0

    .line 567
    :cond_3
    return-void
.end method

.method final set(DIZ)V
    .locals 7
    .param p1, "source"    # D
    .param p3, "maximumDigits"    # I
    .param p4, "fixedPoint"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 404
    const-wide/16 v2, 0x0

    cmpl-double v1, p1, v2

    if-nez v1, :cond_0

    const-wide/16 p1, 0x0

    .line 407
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    .line 409
    .local v0, "rep":Ljava/lang/String;
    const/16 v1, 0x13

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/DigitList;->set(Ljava/lang/String;I)V

    .line 411
    if-eqz p4, :cond_3

    .line 417
    iget v1, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    neg-int v1, v1

    if-le v1, p3, :cond_1

    .line 418
    iput v4, p0, Lcom/ibm/icu/text/DigitList;->count:I

    .line 440
    .end local p3    # "maximumDigits":I
    :goto_0
    return-void

    .line 420
    .restart local p3    # "maximumDigits":I
    :cond_1
    iget v1, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    neg-int v1, v1

    if-ne v1, p3, :cond_3

    .line 421
    invoke-direct {p0, v4}, Lcom/ibm/icu/text/DigitList;->shouldRoundUp(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 422
    iput v5, p0, Lcom/ibm/icu/text/DigitList;->count:I

    .line 423
    iget v1, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 424
    iget-object v1, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    const/16 v2, 0x31

    aput-byte v2, v1, v4

    goto :goto_0

    .line 426
    :cond_2
    iput v4, p0, Lcom/ibm/icu/text/DigitList;->count:I

    goto :goto_0

    .line 434
    :cond_3
    :goto_1
    iget v1, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-le v1, v5, :cond_4

    iget-object v1, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    iget v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v2, v2, -0x1

    aget-byte v1, v1, v2

    const/16 v2, 0x30

    if-ne v1, v2, :cond_4

    .line 435
    iget v1, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/ibm/icu/text/DigitList;->count:I

    goto :goto_1

    .line 439
    :cond_4
    if-eqz p4, :cond_6

    iget v1, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    add-int/2addr p3, v1

    .end local p3    # "maximumDigits":I
    :cond_5
    :goto_2
    invoke-virtual {p0, p3}, Lcom/ibm/icu/text/DigitList;->round(I)V

    goto :goto_0

    .restart local p3    # "maximumDigits":I
    :cond_6
    if-nez p3, :cond_5

    const/4 p3, -0x1

    goto :goto_2
.end method

.method public final set(J)V
    .locals 1
    .param p1, "source"    # J

    .prologue
    .line 574
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/ibm/icu/text/DigitList;->set(JI)V

    .line 575
    return-void
.end method

.method public final set(JI)V
    .locals 9
    .param p1, "source"    # J
    .param p3, "maximumDigits"    # I

    .prologue
    .line 594
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-gtz v2, :cond_2

    .line 595
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, p1, v2

    if-nez v2, :cond_1

    .line 596
    const/16 v2, 0x13

    iput v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    iput v2, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 597
    sget-object v2, Lcom/ibm/icu/text/DigitList;->LONG_MIN_REP:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    const/4 v5, 0x0

    iget v6, p0, Lcom/ibm/icu/text/DigitList;->count:I

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 617
    :goto_0
    if-lez p3, :cond_0

    invoke-virtual {p0, p3}, Lcom/ibm/icu/text/DigitList;->round(I)V

    .line 618
    :cond_0
    return-void

    .line 599
    :cond_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    .line 600
    const/4 v2, 0x0

    iput v2, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    goto :goto_0

    .line 603
    :cond_2
    const/16 v0, 0x13

    .line 605
    .local v0, "left":I
    :goto_1
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-lez v2, :cond_3

    .line 606
    iget-object v2, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    add-int/lit8 v0, v0, -0x1

    const-wide/16 v4, 0x30

    const-wide/16 v6, 0xa

    rem-long v6, p1, v6

    add-long/2addr v4, v6

    long-to-int v3, v4

    int-to-byte v3, v3

    aput-byte v3, v2, v0

    .line 607
    const-wide/16 v2, 0xa

    div-long/2addr p1, v2

    .line 608
    goto :goto_1

    .line 609
    :cond_3
    rsub-int/lit8 v2, v0, 0x13

    iput v2, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 613
    const/16 v1, 0x12

    .local v1, "right":I
    :goto_2
    iget-object v2, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v2, v2, v1

    const/16 v3, 0x30

    if-ne v2, v3, :cond_4

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 614
    :cond_4
    sub-int v2, v1, v0

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    .line 615
    iget-object v2, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    iget-object v3, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    const/4 v4, 0x0

    iget v5, p0, Lcom/ibm/icu/text/DigitList;->count:I

    invoke-static {v2, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public final set(Lcom/ibm/icu/math/BigDecimal;IZ)V
    .locals 1
    .param p1, "source"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "maximumDigits"    # I
    .param p3, "fixedPoint"    # Z

    .prologue
    .line 757
    invoke-virtual {p1}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/ibm/icu/text/DigitList;->setBigDecimalDigits(Ljava/lang/String;IZ)V

    .line 758
    return-void
.end method

.method public final set(Ljava/math/BigDecimal;IZ)V
    .locals 1
    .param p1, "source"    # Ljava/math/BigDecimal;
    .param p2, "maximumDigits"    # I
    .param p3, "fixedPoint"    # Z

    .prologue
    .line 741
    invoke-virtual {p1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/ibm/icu/text/DigitList;->setBigDecimalDigits(Ljava/lang/String;IZ)V

    .line 742
    return-void
.end method

.method public final set(Ljava/math/BigInteger;I)V
    .locals 6
    .param p1, "source"    # Ljava/math/BigInteger;
    .param p2, "maximumDigits"    # I

    .prologue
    const/4 v5, 0x0

    .line 629
    invoke-virtual {p1}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v2

    .line 631
    .local v2, "stringDigits":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    iput v3, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    iput v3, p0, Lcom/ibm/icu/text/DigitList;->count:I

    .line 634
    :goto_0
    iget v3, p0, Lcom/ibm/icu/text/DigitList;->count:I

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    iget v3, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x30

    if-ne v3, v4, :cond_0

    iget v3, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/ibm/icu/text/DigitList;->count:I

    goto :goto_0

    .line 636
    :cond_0
    const/4 v1, 0x0

    .line 637
    .local v1, "offset":I
    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x2d

    if-ne v3, v4, :cond_1

    .line 638
    add-int/lit8 v1, v1, 0x1

    .line 639
    iget v3, p0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/ibm/icu/text/DigitList;->count:I

    .line 640
    iget v3, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 643
    :cond_1
    iget v3, p0, Lcom/ibm/icu/text/DigitList;->count:I

    invoke-direct {p0, v3, v5}, Lcom/ibm/icu/text/DigitList;->ensureCapacity(II)V

    .line 644
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge v0, v3, :cond_2

    .line 645
    iget-object v3, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    add-int v4, v0, v1

    invoke-virtual {v2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v3, v0

    .line 644
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 648
    :cond_2
    if-lez p2, :cond_3

    invoke-virtual {p0, p2}, Lcom/ibm/icu/text/DigitList;->round(I)V

    .line 649
    :cond_3
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 857
    invoke-virtual {p0}, Lcom/ibm/icu/text/DigitList;->isZero()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "0"

    .line 862
    :goto_0
    return-object v2

    .line 858
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    const-string/jumbo v2, "0."

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 859
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v2, p0, Lcom/ibm/icu/text/DigitList;->count:I

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/ibm/icu/text/DigitList;->digits:[B

    aget-byte v2, v2, v1

    int-to-char v2, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 860
    :cond_1
    const-string/jumbo v2, "x10^"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 861
    iget v2, p0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 862
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
