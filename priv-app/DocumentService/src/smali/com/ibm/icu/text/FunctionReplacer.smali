.class Lcom/ibm/icu/text/FunctionReplacer;
.super Ljava/lang/Object;
.source "FunctionReplacer.java"

# interfaces
.implements Lcom/ibm/icu/text/UnicodeReplacer;


# instance fields
.field private replacer:Lcom/ibm/icu/text/UnicodeReplacer;

.field private translit:Lcom/ibm/icu/text/Transliterator;


# direct methods
.method public constructor <init>(Lcom/ibm/icu/text/Transliterator;Lcom/ibm/icu/text/UnicodeReplacer;)V
    .locals 0
    .param p1, "theTranslit"    # Lcom/ibm/icu/text/Transliterator;
    .param p2, "theReplacer"    # Lcom/ibm/icu/text/UnicodeReplacer;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/ibm/icu/text/FunctionReplacer;->translit:Lcom/ibm/icu/text/Transliterator;

    .line 41
    iput-object p2, p0, Lcom/ibm/icu/text/FunctionReplacer;->replacer:Lcom/ibm/icu/text/UnicodeReplacer;

    .line 42
    return-void
.end method


# virtual methods
.method public addReplacementSetTo(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 1
    .param p1, "toUnionTo"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/ibm/icu/text/FunctionReplacer;->translit:Lcom/ibm/icu/text/Transliterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/Transliterator;->getTargetSet()Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    .line 81
    return-void
.end method

.method public replace(Lcom/ibm/icu/text/Replaceable;II[I)I
    .locals 2
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "start"    # I
    .param p3, "limit"    # I
    .param p4, "cursor"    # [I

    .prologue
    .line 53
    iget-object v1, p0, Lcom/ibm/icu/text/FunctionReplacer;->replacer:Lcom/ibm/icu/text/UnicodeReplacer;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/ibm/icu/text/UnicodeReplacer;->replace(Lcom/ibm/icu/text/Replaceable;II[I)I

    move-result v0

    .line 54
    .local v0, "len":I
    add-int p3, p2, v0

    .line 57
    iget-object v1, p0, Lcom/ibm/icu/text/FunctionReplacer;->translit:Lcom/ibm/icu/text/Transliterator;

    invoke-virtual {v1, p1, p2, p3}, Lcom/ibm/icu/text/Transliterator;->transliterate(Lcom/ibm/icu/text/Replaceable;II)I

    move-result p3

    .line 59
    sub-int v1, p3, p2

    return v1
.end method

.method public toReplacerPattern(Z)Ljava/lang/String;
    .locals 2
    .param p1, "escapeUnprintable"    # Z

    .prologue
    .line 66
    new-instance v0, Ljava/lang/StringBuffer;

    const-string/jumbo v1, "&"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 67
    .local v0, "rule":Ljava/lang/StringBuffer;
    iget-object v1, p0, Lcom/ibm/icu/text/FunctionReplacer;->translit:Lcom/ibm/icu/text/Transliterator;

    invoke-virtual {v1}, Lcom/ibm/icu/text/Transliterator;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 68
    const-string/jumbo v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 69
    iget-object v1, p0, Lcom/ibm/icu/text/FunctionReplacer;->replacer:Lcom/ibm/icu/text/UnicodeReplacer;

    invoke-interface {v1, p1}, Lcom/ibm/icu/text/UnicodeReplacer;->toReplacerPattern(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 70
    const-string/jumbo v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 71
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
