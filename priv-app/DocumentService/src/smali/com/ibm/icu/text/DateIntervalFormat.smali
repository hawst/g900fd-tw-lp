.class public Lcom/ibm/icu/text/DateIntervalFormat;
.super Lcom/ibm/icu/text/UFormat;
.source "DateIntervalFormat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;,
        Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;
    }
.end annotation


# static fields
.field private static LOCAL_PATTERN_CACHE:Lcom/ibm/icu/impl/ICUCache; = null

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

.field private fFromCalendar:Lcom/ibm/icu/util/Calendar;

.field private fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

.field private transient fIntervalPatterns:Ljava/util/Map;

.field private fSkeleton:Ljava/lang/String;

.field private fToCalendar:Lcom/ibm/icu/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 287
    new-instance v0, Lcom/ibm/icu/impl/SimpleCache;

    invoke-direct {v0}, Lcom/ibm/icu/impl/SimpleCache;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/DateIntervalFormat;->LOCAL_PATTERN_CACHE:Lcom/ibm/icu/impl/ICUCache;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 319
    invoke-direct {p0}, Lcom/ibm/icu/text/UFormat;-><init>()V

    .line 311
    iput-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fSkeleton:Ljava/lang/String;

    .line 313
    iput-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fIntervalPatterns:Ljava/util/Map;

    .line 320
    return-void
.end method

.method private constructor <init>(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/text/DateIntervalInfo;Ljava/lang/String;)V
    .locals 3
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "dtItvInfo"    # Lcom/ibm/icu/text/DateIntervalInfo;
    .param p3, "skeleton"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 337
    invoke-direct {p0}, Lcom/ibm/icu/text/UFormat;-><init>()V

    .line 311
    iput-object v2, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fSkeleton:Ljava/lang/String;

    .line 313
    iput-object v2, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fIntervalPatterns:Ljava/util/Map;

    .line 339
    invoke-virtual {p2}, Lcom/ibm/icu/text/DateIntervalInfo;->freeze()Ljava/lang/Object;

    .line 340
    iput-object p3, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fSkeleton:Ljava/lang/String;

    .line 341
    iput-object p2, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    .line 343
    invoke-static {p1}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateTimePatternGenerator;

    move-result-object v1

    .line 344
    .local v1, "generator":Lcom/ibm/icu/text/DateTimePatternGenerator;
    invoke-virtual {v1, p3}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getBestPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 345
    .local v0, "bestPattern":Ljava/lang/String;
    new-instance v2, Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-direct {v2, v0, p1}, Lcom/ibm/icu/text/SimpleDateFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    iput-object v2, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    .line 346
    iget-object v2, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v2}, Lcom/ibm/icu/text/SimpleDateFormat;->getCalendar()Lcom/ibm/icu/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ibm/icu/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/util/Calendar;

    iput-object v2, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fFromCalendar:Lcom/ibm/icu/util/Calendar;

    .line 347
    iget-object v2, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v2}, Lcom/ibm/icu/text/SimpleDateFormat;->getCalendar()Lcom/ibm/icu/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ibm/icu/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/util/Calendar;

    iput-object v2, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fToCalendar:Lcom/ibm/icu/util/Calendar;

    .line 348
    invoke-direct {p0}, Lcom/ibm/icu/text/DateIntervalFormat;->initializePattern()V

    .line 349
    return-void
.end method

.method private static adjustFieldWidth(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 19
    .param p0, "inputSkeleton"    # Ljava/lang/String;
    .param p1, "bestMatchSkeleton"    # Ljava/lang/String;
    .param p2, "bestMatchIntervalPattern"    # Ljava/lang/String;
    .param p3, "differenceInfo"    # I

    .prologue
    .line 1475
    if-nez p2, :cond_0

    .line 1476
    const/16 v17, 0x0

    .line 1565
    :goto_0
    return-object v17

    .line 1478
    :cond_0
    const/16 v17, 0x3a

    move/from16 v0, v17

    new-array v13, v0, [I

    .line 1479
    .local v13, "inputSkeletonFieldWidth":[I
    const/16 v17, 0x3a

    move/from16 v0, v17

    new-array v6, v0, [I

    .line 1495
    .local v6, "bestMatchSkeletonFieldWidth":[I
    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/ibm/icu/text/DateIntervalInfo;->parseSkeleton(Ljava/lang/String;[I)V

    .line 1496
    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/ibm/icu/text/DateIntervalInfo;->parseSkeleton(Ljava/lang/String;[I)V

    .line 1497
    const/16 v17, 0x2

    move/from16 v0, p3

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    .line 1498
    const/16 v17, 0x76

    const/16 v18, 0x7a

    move-object/from16 v0, p2

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p2

    .line 1501
    :cond_1
    new-instance v4, Ljava/lang/StringBuffer;

    move-object/from16 v0, p2

    invoke-direct {v4, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1503
    .local v4, "adjustedPtn":Ljava/lang/StringBuffer;
    const/4 v11, 0x0

    .line 1504
    .local v11, "inQuote":Z
    const/4 v15, 0x0

    .line 1505
    .local v15, "prevCh":C
    const/4 v8, 0x0

    .line 1507
    .local v8, "count":I
    const/16 v3, 0x41

    .line 1510
    .local v3, "PATTERN_CHAR_BASE":I
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    .line 1511
    .local v5, "adjustedPtnLength":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    if-ge v10, v5, :cond_c

    .line 1512
    invoke-virtual {v4, v10}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v7

    .line 1513
    .local v7, "ch":C
    if-eq v7, v15, :cond_5

    if-lez v8, :cond_5

    .line 1515
    move/from16 v16, v15

    .line 1516
    .local v16, "skeletonChar":C
    const/16 v17, 0x4c

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_2

    .line 1518
    const/16 v16, 0x4d

    .line 1520
    :cond_2
    sub-int v17, v16, v3

    aget v9, v6, v17

    .line 1521
    .local v9, "fieldCount":I
    sub-int v17, v16, v3

    aget v12, v13, v17

    .line 1522
    .local v12, "inputFieldCount":I
    if-ne v9, v8, :cond_4

    if-le v12, v9, :cond_4

    .line 1523
    sub-int v8, v12, v9

    .line 1524
    const/4 v14, 0x0

    .local v14, "j":I
    :goto_2
    if-ge v14, v8, :cond_3

    .line 1525
    invoke-virtual {v4, v10, v15}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 1524
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 1527
    :cond_3
    add-int/2addr v10, v8

    .line 1528
    add-int/2addr v5, v8

    .line 1530
    .end local v14    # "j":I
    :cond_4
    const/4 v8, 0x0

    .line 1532
    .end local v9    # "fieldCount":I
    .end local v12    # "inputFieldCount":I
    .end local v16    # "skeletonChar":C
    :cond_5
    const/16 v17, 0x27

    move/from16 v0, v17

    if-ne v7, v0, :cond_9

    .line 1535
    add-int/lit8 v17, v10, 0x1

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_7

    add-int/lit8 v17, v10, 0x1

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v17

    const/16 v18, 0x27

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    .line 1536
    add-int/lit8 v10, v10, 0x1

    .line 1511
    :cond_6
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 1538
    :cond_7
    if-nez v11, :cond_8

    const/4 v11, 0x1

    .line 1540
    :goto_4
    goto :goto_3

    .line 1538
    :cond_8
    const/4 v11, 0x0

    goto :goto_4

    .line 1541
    :cond_9
    if-nez v11, :cond_6

    const/16 v17, 0x61

    move/from16 v0, v17

    if-lt v7, v0, :cond_a

    const/16 v17, 0x7a

    move/from16 v0, v17

    if-le v7, v0, :cond_b

    :cond_a
    const/16 v17, 0x41

    move/from16 v0, v17

    if-lt v7, v0, :cond_6

    const/16 v17, 0x5a

    move/from16 v0, v17

    if-gt v7, v0, :cond_6

    .line 1544
    :cond_b
    move v15, v7

    .line 1545
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 1548
    .end local v7    # "ch":C
    :cond_c
    if-lez v8, :cond_e

    .line 1551
    move/from16 v16, v15

    .line 1552
    .restart local v16    # "skeletonChar":C
    const/16 v17, 0x4c

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_d

    .line 1554
    const/16 v16, 0x4d

    .line 1556
    :cond_d
    sub-int v17, v16, v3

    aget v9, v6, v17

    .line 1557
    .restart local v9    # "fieldCount":I
    sub-int v17, v16, v3

    aget v12, v13, v17

    .line 1558
    .restart local v12    # "inputFieldCount":I
    if-ne v9, v8, :cond_e

    if-le v12, v9, :cond_e

    .line 1559
    sub-int v8, v12, v9

    .line 1560
    const/4 v14, 0x0

    .restart local v14    # "j":I
    :goto_5
    if-ge v14, v8, :cond_e

    .line 1561
    invoke-virtual {v4, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1560
    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    .line 1565
    .end local v9    # "fieldCount":I
    .end local v12    # "inputFieldCount":I
    .end local v14    # "j":I
    .end local v16    # "skeletonChar":C
    :cond_e
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_0
.end method

.method private concatSingleDate2TimeInterval(Ljava/lang/String;Ljava/lang/String;ILjava/util/HashMap;)V
    .locals 5
    .param p1, "dtfmt"    # Ljava/lang/String;
    .param p2, "datePattern"    # Ljava/lang/String;
    .param p3, "field"    # I
    .param p4, "intervalPatterns"    # Ljava/util/HashMap;

    .prologue
    .line 1586
    sget-object v3, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    aget-object v3, v3, p3

    invoke-virtual {p4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    .line 1589
    .local v2, "timeItvPtnInfo":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    if-eqz v2, :cond_0

    .line 1590
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->getFirstPart()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v2}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->getSecondPart()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1592
    .local v1, "timeIntervalPattern":Ljava/lang/String;
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {p1, v3}, Lcom/ibm/icu/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1594
    .local v0, "pattern":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->firstDateInPtnIsLaterDate()Z

    move-result v3

    invoke-static {v0, v3}, Lcom/ibm/icu/text/DateIntervalInfo;->genPatternInfo(Ljava/lang/String;Z)Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    move-result-object v2

    .line 1596
    sget-object v3, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    aget-object v3, v3, p3

    invoke-virtual {p4, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1601
    .end local v0    # "pattern":Ljava/lang/String;
    .end local v1    # "timeIntervalPattern":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private final fallbackFormat(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 7
    .param p1, "fromCalendar"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "toCalendar"    # Lcom/ibm/icu/util/Calendar;
    .param p3, "appendTo"    # Ljava/lang/StringBuffer;
    .param p4, "pos"    # Ljava/text/FieldPosition;

    .prologue
    const/16 v5, 0x40

    .line 720
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, v5}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 721
    .local v0, "earlierDate":Ljava/lang/StringBuffer;
    iget-object v4, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v4, p1, v0, p4}, Lcom/ibm/icu/text/SimpleDateFormat;->format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 722
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3, v5}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 723
    .local v3, "laterDate":Ljava/lang/StringBuffer;
    iget-object v4, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v4, p2, v3, p4}, Lcom/ibm/icu/text/SimpleDateFormat;->format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 724
    iget-object v4, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    invoke-virtual {v4}, Lcom/ibm/icu/text/DateIntervalInfo;->getFallbackIntervalPattern()Ljava/lang/String;

    move-result-object v2

    .line 725
    .local v2, "fallbackPattern":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v4}, Lcom/ibm/icu/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 727
    .local v1, "fallback":Ljava/lang/String;
    invoke-virtual {p3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 728
    return-object p3
.end method

.method private final fallbackFormat(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/lang/String;)Ljava/lang/StringBuffer;
    .locals 2
    .param p1, "fromCalendar"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "toCalendar"    # Lcom/ibm/icu/util/Calendar;
    .param p3, "appendTo"    # Ljava/lang/StringBuffer;
    .param p4, "pos"    # Ljava/text/FieldPosition;
    .param p5, "fullPattern"    # Ljava/lang/String;

    .prologue
    .line 754
    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v1}, Lcom/ibm/icu/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v0

    .line 755
    .local v0, "originalPattern":Ljava/lang/String;
    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v1, p5}, Lcom/ibm/icu/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 756
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/ibm/icu/text/DateIntervalFormat;->fallbackFormat(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 757
    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v1, v0}, Lcom/ibm/icu/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 758
    return-object p3
.end method

.method private static fieldExistsInSkeleton(ILjava/lang/String;)Z
    .locals 3
    .param p0, "field"    # I
    .param p1, "skeleton"    # Ljava/lang/String;

    .prologue
    .line 1612
    sget-object v1, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    aget-object v0, v1, p0

    .line 1613
    .local v0, "fieldChar":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private genFallbackPattern(ILjava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/text/DateTimePatternGenerator;)V
    .locals 4
    .param p1, "field"    # I
    .param p2, "skeleton"    # Ljava/lang/String;
    .param p3, "intervalPatterns"    # Ljava/util/HashMap;
    .param p4, "dtpng"    # Lcom/ibm/icu/text/DateTimePatternGenerator;

    .prologue
    .line 1074
    invoke-virtual {p4, p2}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getBestPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1079
    .local v0, "pattern":Ljava/lang/String;
    new-instance v1, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    invoke-virtual {v3}, Lcom/ibm/icu/text/DateIntervalInfo;->getDefaultOrder()Z

    move-result v3

    invoke-direct {v1, v2, v0, v3}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1081
    .local v1, "ptn":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    sget-object v2, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {p3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1083
    return-void
.end method

.method private genIntervalPattern(ILjava/lang/String;Ljava/lang/String;ILjava/util/HashMap;)Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;
    .locals 15
    .param p1, "field"    # I
    .param p2, "skeleton"    # Ljava/lang/String;
    .param p3, "bestSkeleton"    # Ljava/lang/String;
    .param p4, "differenceInfo"    # I
    .param p5, "intervalPatterns"    # Ljava/util/HashMap;

    .prologue
    .line 1366
    const/4 v9, 0x0

    .line 1367
    .local v9, "retValue":Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;
    iget-object v12, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    move-object/from16 v0, p3

    move/from16 v1, p1

    invoke-virtual {v12, v0, v1}, Lcom/ibm/icu/text/DateIntervalInfo;->getIntervalPattern(Ljava/lang/String;I)Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    move-result-object v6

    .line 1369
    .local v6, "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    if-nez v6, :cond_6

    .line 1371
    move-object/from16 v0, p3

    move/from16 v1, p1

    invoke-static {v0, v1}, Lcom/ibm/icu/text/SimpleDateFormat;->isFieldUnitIgnored(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1372
    new-instance v8, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    iget-object v12, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v12}, Lcom/ibm/icu/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    invoke-virtual {v14}, Lcom/ibm/icu/text/DateIntervalInfo;->getDefaultOrder()Z

    move-result v14

    invoke-direct {v8, v12, v13, v14}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1376
    .local v8, "ptnInfo":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    sget-object v12, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    aget-object v12, v12, p1

    move-object/from16 v0, p5

    invoke-virtual {v0, v12, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1378
    const/4 v12, 0x0

    .line 1439
    .end local v8    # "ptnInfo":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    :goto_0
    return-object v12

    .line 1385
    :cond_0
    const/16 v12, 0x9

    move/from16 v0, p1

    if-ne v0, v12, :cond_2

    .line 1386
    iget-object v12, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    const/16 v13, 0xa

    move-object/from16 v0, p3

    invoke-virtual {v12, v0, v13}, Lcom/ibm/icu/text/DateIntervalInfo;->getIntervalPattern(Ljava/lang/String;I)Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    move-result-object v6

    .line 1388
    if-eqz v6, :cond_1

    .line 1390
    sget-object v12, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    aget-object v12, v12, p1

    move-object/from16 v0, p5

    invoke-virtual {v0, v12, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1394
    :cond_1
    const/4 v12, 0x0

    goto :goto_0

    .line 1402
    :cond_2
    sget-object v12, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    aget-object v3, v12, p1

    .line 1404
    .local v3, "fieldLetter":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p3

    .line 1405
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p2

    .line 1408
    iget-object v12, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    move-object/from16 v0, p3

    move/from16 v1, p1

    invoke-virtual {v12, v0, v1}, Lcom/ibm/icu/text/DateIntervalInfo;->getIntervalPattern(Ljava/lang/String;I)Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    move-result-object v6

    .line 1409
    if-nez v6, :cond_3

    if-nez p4, :cond_3

    .line 1412
    iget-object v12, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/ibm/icu/text/DateIntervalInfo;->getBestSkeleton(Ljava/lang/String;)Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;

    move-result-object v11

    .line 1413
    .local v11, "tmpRetValue":Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;
    iget-object v10, v11, Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;->bestMatchSkeleton:Ljava/lang/String;

    .line 1414
    .local v10, "tmpBestSkeleton":Ljava/lang/String;
    iget v0, v11, Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;->bestMatchDistanceInfo:I

    move/from16 p4, v0

    .line 1415
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v12

    if-eqz v12, :cond_3

    const/4 v12, -0x1

    move/from16 v0, p4

    if-eq v0, v12, :cond_3

    .line 1416
    iget-object v12, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    move/from16 v0, p1

    invoke-virtual {v12, v10, v0}, Lcom/ibm/icu/text/DateIntervalInfo;->getIntervalPattern(Ljava/lang/String;I)Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    move-result-object v6

    .line 1417
    move-object/from16 p3, v10

    .line 1420
    .end local v10    # "tmpBestSkeleton":Ljava/lang/String;
    .end local v11    # "tmpRetValue":Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;
    :cond_3
    if-eqz v6, :cond_6

    .line 1421
    new-instance v9, Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;

    .end local v9    # "retValue":Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {v9, v0, v1}, Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v9    # "retValue":Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;
    move-object v7, v6

    .line 1424
    .end local v3    # "fieldLetter":Ljava/lang/String;
    .end local v6    # "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    .local v7, "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    :goto_1
    if-eqz v7, :cond_5

    .line 1425
    if-eqz p4, :cond_4

    .line 1426
    invoke-virtual {v7}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->getFirstPart()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move/from16 v2, p4

    invoke-static {v0, v1, v12, v2}, Lcom/ibm/icu/text/DateIntervalFormat;->adjustFieldWidth(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 1428
    .local v4, "part1":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->getSecondPart()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move/from16 v2, p4

    invoke-static {v0, v1, v12, v2}, Lcom/ibm/icu/text/DateIntervalFormat;->adjustFieldWidth(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 1430
    .local v5, "part2":Ljava/lang/String;
    new-instance v6, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    invoke-virtual {v7}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->firstDateInPtnIsLaterDate()Z

    move-result v12

    invoke-direct {v6, v4, v5, v12}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1436
    .end local v4    # "part1":Ljava/lang/String;
    .end local v5    # "part2":Ljava/lang/String;
    .end local v7    # "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    .restart local v6    # "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    :goto_2
    sget-object v12, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    aget-object v12, v12, p1

    move-object/from16 v0, p5

    invoke-virtual {v0, v12, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    move-object v12, v9

    .line 1439
    goto/16 :goto_0

    .end local v6    # "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    .restart local v7    # "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    :cond_4
    move-object v6, v7

    .end local v7    # "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    .restart local v6    # "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    goto :goto_2

    .end local v6    # "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    .restart local v7    # "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    :cond_5
    move-object v6, v7

    .end local v7    # "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    .restart local v6    # "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    goto :goto_3

    :cond_6
    move-object v7, v6

    .end local v6    # "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    .restart local v7    # "pattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    goto :goto_1
.end method

.method private genSeparateDateTimePtn(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Z
    .locals 9
    .param p1, "dateSkeleton"    # Ljava/lang/String;
    .param p2, "timeSkeleton"    # Ljava/lang/String;
    .param p3, "intervalPatterns"    # Ljava/util/HashMap;

    .prologue
    const/4 v8, 0x1

    .line 1290
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1291
    move-object v2, p2

    .line 1305
    .local v2, "skeleton":Ljava/lang/String;
    :goto_0
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/DateIntervalInfo;->getBestSkeleton(Ljava/lang/String;)Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;

    move-result-object v6

    .line 1306
    .local v6, "retValue":Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;
    iget-object v3, v6, Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;->bestMatchSkeleton:Ljava/lang/String;

    .line 1307
    .local v3, "bestSkeleton":Ljava/lang/String;
    iget v4, v6, Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;->bestMatchDistanceInfo:I

    .line 1314
    .local v4, "differenceInfo":I
    const/4 v0, -0x1

    if-ne v4, v0, :cond_1

    .line 1316
    const/4 v0, 0x0

    .line 1336
    :goto_1
    return v0

    .line 1293
    .end local v2    # "skeleton":Ljava/lang/String;
    .end local v3    # "bestSkeleton":Ljava/lang/String;
    .end local v4    # "differenceInfo":I
    .end local v6    # "retValue":Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;
    :cond_0
    move-object v2, p1

    .restart local v2    # "skeleton":Ljava/lang/String;
    goto :goto_0

    .line 1319
    .restart local v3    # "bestSkeleton":Ljava/lang/String;
    .restart local v4    # "differenceInfo":I
    .restart local v6    # "retValue":Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;
    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 1321
    const/4 v1, 0x5

    move-object v0, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/DateIntervalFormat;->genIntervalPattern(ILjava/lang/String;Ljava/lang/String;ILjava/util/HashMap;)Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;

    .line 1322
    const/4 v1, 0x2

    move-object v0, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/DateIntervalFormat;->genIntervalPattern(ILjava/lang/String;Ljava/lang/String;ILjava/util/HashMap;)Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;

    move-result-object v7

    .line 1326
    .local v7, "skeletons":Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;
    if-eqz v7, :cond_2

    .line 1327
    iget-object v3, v7, Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;->skeleton:Ljava/lang/String;

    .line 1328
    iget-object v2, v7, Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;->bestMatchSkeleton:Ljava/lang/String;

    :cond_2
    move-object v0, p0

    move v1, v8

    move-object v5, p3

    .line 1330
    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/DateIntervalFormat;->genIntervalPattern(ILjava/lang/String;Ljava/lang/String;ILjava/util/HashMap;)Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;

    .end local v7    # "skeletons":Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;
    :goto_2
    move v0, v8

    .line 1336
    goto :goto_1

    .line 1332
    :cond_3
    const/16 v1, 0xc

    move-object v0, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/DateIntervalFormat;->genIntervalPattern(ILjava/lang/String;Ljava/lang/String;ILjava/util/HashMap;)Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;

    .line 1333
    const/16 v1, 0xa

    move-object v0, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/DateIntervalFormat;->genIntervalPattern(ILjava/lang/String;Ljava/lang/String;ILjava/util/HashMap;)Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;

    .line 1334
    const/16 v1, 0x9

    move-object v0, p0

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/DateIntervalFormat;->genIntervalPattern(ILjava/lang/String;Ljava/lang/String;ILjava/util/HashMap;)Lcom/ibm/icu/text/DateIntervalFormat$SkeletonAndItsBestMatch;

    goto :goto_2
.end method

.method private static getDateTimeSkeleton(Ljava/lang/String;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V
    .locals 11
    .param p0, "skeleton"    # Ljava/lang/String;
    .param p1, "dateSkeleton"    # Ljava/lang/StringBuffer;
    .param p2, "normalizedDateSkeleton"    # Ljava/lang/StringBuffer;
    .param p3, "timeSkeleton"    # Ljava/lang/StringBuffer;
    .param p4, "normalizedTimeSkeleton"    # Ljava/lang/StringBuffer;

    .prologue
    .line 1135
    const/4 v0, 0x0

    .line 1136
    .local v0, "ECount":I
    const/4 v3, 0x0

    .line 1137
    .local v3, "dCount":I
    const/4 v1, 0x0

    .line 1138
    .local v1, "MCount":I
    const/4 v8, 0x0

    .line 1139
    .local v8, "yCount":I
    const/4 v4, 0x0

    .line 1140
    .local v4, "hCount":I
    const/4 v6, 0x0

    .line 1141
    .local v6, "mCount":I
    const/4 v7, 0x0

    .line 1142
    .local v7, "vCount":I
    const/4 v9, 0x0

    .line 1144
    .local v9, "zCount":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v5, v10, :cond_0

    .line 1145
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 1146
    .local v2, "ch":C
    packed-switch v2, :pswitch_data_0

    .line 1144
    :goto_1
    :pswitch_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1148
    :pswitch_1
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1149
    add-int/lit8 v0, v0, 0x1

    .line 1150
    goto :goto_1

    .line 1152
    :pswitch_2
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1153
    add-int/lit8 v3, v3, 0x1

    .line 1154
    goto :goto_1

    .line 1156
    :pswitch_3
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1157
    add-int/lit8 v1, v1, 0x1

    .line 1158
    goto :goto_1

    .line 1160
    :pswitch_4
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1161
    add-int/lit8 v8, v8, 0x1

    .line 1162
    goto :goto_1

    .line 1177
    :pswitch_5
    invoke-virtual {p2, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1178
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1182
    :pswitch_6
    invoke-virtual {p3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1186
    :pswitch_7
    invoke-virtual {p3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1187
    add-int/lit8 v4, v4, 0x1

    .line 1188
    goto :goto_1

    .line 1190
    :pswitch_8
    invoke-virtual {p3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1191
    add-int/lit8 v6, v6, 0x1

    .line 1192
    goto :goto_1

    .line 1194
    :pswitch_9
    add-int/lit8 v9, v9, 0x1

    .line 1195
    invoke-virtual {p3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1198
    :pswitch_a
    add-int/lit8 v7, v7, 0x1

    .line 1199
    invoke-virtual {p3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1209
    :pswitch_b
    invoke-virtual {p3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1210
    invoke-virtual {p4, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1216
    .end local v2    # "ch":C
    :cond_0
    if-eqz v8, :cond_1

    .line 1217
    const/16 v10, 0x79

    invoke-virtual {p2, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1219
    :cond_1
    if-eqz v1, :cond_2

    .line 1220
    const/4 v10, 0x3

    if-ge v1, v10, :cond_9

    .line 1221
    const/16 v10, 0x4d

    invoke-virtual {p2, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1228
    :cond_2
    if-eqz v0, :cond_3

    .line 1229
    const/4 v10, 0x3

    if-gt v0, v10, :cond_a

    .line 1230
    const/16 v10, 0x45

    invoke-virtual {p2, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1237
    :cond_3
    if-eqz v3, :cond_4

    .line 1238
    const/16 v10, 0x64

    invoke-virtual {p2, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1242
    :cond_4
    if-eqz v4, :cond_5

    .line 1243
    const/16 v10, 0x68

    invoke-virtual {p4, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1245
    :cond_5
    if-eqz v6, :cond_6

    .line 1246
    const/16 v10, 0x6d

    invoke-virtual {p4, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1248
    :cond_6
    if-eqz v9, :cond_7

    .line 1249
    const/16 v10, 0x7a

    invoke-virtual {p4, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1251
    :cond_7
    if-eqz v7, :cond_8

    .line 1252
    const/16 v10, 0x76

    invoke-virtual {p4, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1254
    :cond_8
    return-void

    .line 1223
    :cond_9
    const/4 v5, 0x0

    :goto_2
    if-ge v5, v1, :cond_2

    const/4 v10, 0x5

    if-ge v5, v10, :cond_2

    .line 1224
    const/16 v10, 0x4d

    invoke-virtual {p2, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1223
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1232
    :cond_a
    const/4 v5, 0x0

    :goto_3
    if-ge v5, v0, :cond_3

    const/4 v10, 0x5

    if-ge v5, v10, :cond_3

    .line 1233
    const/16 v10, 0x45

    invoke-virtual {p2, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1232
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 1146
    :pswitch_data_0
    .packed-switch 0x41
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_5
        :pswitch_0
        :pswitch_5
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_5
        :pswitch_7
        :pswitch_0
        :pswitch_b
        :pswitch_b
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_5
        :pswitch_a
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_9
    .end packed-switch
.end method

.method public static final getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/DateIntervalFormat;
    .locals 1
    .param p0, "skeleton"    # Ljava/lang/String;

    .prologue
    .line 368
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/ibm/icu/text/DateIntervalFormat;->getInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateIntervalFormat;

    move-result-object v0

    return-object v0
.end method

.method public static final getInstance(Ljava/lang/String;Lcom/ibm/icu/text/DateIntervalInfo;)Lcom/ibm/icu/text/DateIntervalFormat;
    .locals 1
    .param p0, "skeleton"    # Ljava/lang/String;
    .param p1, "dtitvinf"    # Lcom/ibm/icu/text/DateIntervalInfo;

    .prologue
    .line 447
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/ibm/icu/text/DateIntervalFormat;->getInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/text/DateIntervalInfo;)Lcom/ibm/icu/text/DateIntervalFormat;

    move-result-object v0

    return-object v0
.end method

.method public static final getInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateIntervalFormat;
    .locals 2
    .param p0, "skeleton"    # Ljava/lang/String;
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 424
    new-instance v0, Lcom/ibm/icu/text/DateIntervalInfo;

    invoke-direct {v0, p1}, Lcom/ibm/icu/text/DateIntervalInfo;-><init>(Lcom/ibm/icu/util/ULocale;)V

    .line 425
    .local v0, "dtitvinf":Lcom/ibm/icu/text/DateIntervalInfo;
    new-instance v1, Lcom/ibm/icu/text/DateIntervalFormat;

    invoke-direct {v1, p1, v0, p0}, Lcom/ibm/icu/text/DateIntervalFormat;-><init>(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/text/DateIntervalInfo;Ljava/lang/String;)V

    return-object v1
.end method

.method public static final getInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/text/DateIntervalInfo;)Lcom/ibm/icu/text/DateIntervalFormat;
    .locals 1
    .param p0, "skeleton"    # Ljava/lang/String;
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "dtitvinf"    # Lcom/ibm/icu/text/DateIntervalInfo;

    .prologue
    .line 516
    sget-object v0, Lcom/ibm/icu/text/DateIntervalFormat;->LOCAL_PATTERN_CACHE:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v0}, Lcom/ibm/icu/impl/ICUCache;->clear()V

    .line 519
    invoke-virtual {p2}, Lcom/ibm/icu/text/DateIntervalInfo;->clone()Ljava/lang/Object;

    move-result-object p2

    .end local p2    # "dtitvinf":Lcom/ibm/icu/text/DateIntervalInfo;
    check-cast p2, Lcom/ibm/icu/text/DateIntervalInfo;

    .line 520
    .restart local p2    # "dtitvinf":Lcom/ibm/icu/text/DateIntervalInfo;
    new-instance v0, Lcom/ibm/icu/text/DateIntervalFormat;

    invoke-direct {v0, p1, p2, p0}, Lcom/ibm/icu/text/DateIntervalFormat;-><init>(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/text/DateIntervalInfo;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final getInstance(Ljava/lang/String;Ljava/util/Locale;)Lcom/ibm/icu/text/DateIntervalFormat;
    .locals 1
    .param p0, "skeleton"    # Ljava/lang/String;
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 387
    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/ibm/icu/text/DateIntervalFormat;->getInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateIntervalFormat;

    move-result-object v0

    return-object v0
.end method

.method public static final getInstance(Ljava/lang/String;Ljava/util/Locale;Lcom/ibm/icu/text/DateIntervalInfo;)Lcom/ibm/icu/text/DateIntervalFormat;
    .locals 1
    .param p0, "skeleton"    # Ljava/lang/String;
    .param p1, "locale"    # Ljava/util/Locale;
    .param p2, "dtitvinf"    # Lcom/ibm/icu/text/DateIntervalInfo;

    .prologue
    .line 470
    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/ibm/icu/text/DateIntervalFormat;->getInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/text/DateIntervalInfo;)Lcom/ibm/icu/text/DateIntervalFormat;

    move-result-object v0

    return-object v0
.end method

.method private initializeIntervalPattern(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/util/HashMap;
    .locals 23
    .param p1, "fullPattern"    # Ljava/lang/String;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 902
    invoke-static/range {p2 .. p2}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateTimePatternGenerator;

    move-result-object v7

    .line 903
    .local v7, "dtpng":Lcom/ibm/icu/text/DateTimePatternGenerator;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DateIntervalFormat;->fSkeleton:Ljava/lang/String;

    move-object/from16 v20, v0

    if-nez v20, :cond_0

    .line 906
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getSkeleton(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DateIntervalFormat;->fSkeleton:Ljava/lang/String;

    .line 908
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DateIntervalFormat;->fSkeleton:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 910
    .local v17, "skeleton":Ljava/lang/String;
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 915
    .local v9, "intervalPatterns":Ljava/util/HashMap;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v20

    move/from16 v0, v20

    invoke-direct {v4, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 916
    .local v4, "date":Ljava/lang/StringBuffer;
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v20

    move/from16 v0, v20

    invoke-direct {v10, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 917
    .local v10, "normalizedDate":Ljava/lang/StringBuffer;
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v20

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 918
    .local v18, "time":Ljava/lang/StringBuffer;
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v20

    move/from16 v0, v20

    invoke-direct {v12, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 931
    .local v12, "normalizedTime":Ljava/lang/StringBuffer;
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v4, v10, v1, v12}, Lcom/ibm/icu/text/DateIntervalFormat;->getDateTimeSkeleton(Ljava/lang/String;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V

    .line 934
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    .line 935
    .local v6, "dateSkeleton":Ljava/lang/String;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    .line 936
    .local v19, "timeSkeleton":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    .line 937
    .local v11, "normalizedDateSkeleton":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    .line 939
    .local v13, "normalizedTimeSkeleton":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v13, v9}, Lcom/ibm/icu/text/DateIntervalFormat;->genSeparateDateTimePtn(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Z

    move-result v8

    .line 943
    .local v8, "found":Z
    if-nez v8, :cond_2

    .line 947
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->length()I

    move-result v20

    if-eqz v20, :cond_1

    .line 951
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v20

    if-nez v20, :cond_1

    .line 953
    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v21, "yMd"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    .line 954
    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getBestPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 959
    .local v14, "pattern":Ljava/lang/String;
    new-instance v16, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/ibm/icu/text/DateIntervalInfo;->getDefaultOrder()Z

    move-result v21

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v14, v2}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 962
    .local v16, "ptn":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    sget-object v20, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v21, 0x5

    aget-object v20, v20, v21

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 965
    sget-object v20, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v21, 0x2

    aget-object v20, v20, v21

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 968
    sget-object v20, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v21, 0x1

    aget-object v20, v20, v21

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1059
    .end local v14    # "pattern":Ljava/lang/String;
    .end local v16    # "ptn":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    :cond_1
    :goto_0
    return-object v9

    .line 983
    :cond_2
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->length()I

    move-result v20

    if-eqz v20, :cond_1

    .line 985
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v20

    if-nez v20, :cond_3

    .line 998
    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v21, "yMd"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    .line 999
    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getBestPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1004
    .restart local v14    # "pattern":Ljava/lang/String;
    new-instance v16, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/ibm/icu/text/DateIntervalInfo;->getDefaultOrder()Z

    move-result v21

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v14, v2}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1006
    .restart local v16    # "ptn":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    sget-object v20, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v21, 0x5

    aget-object v20, v20, v21

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1008
    sget-object v20, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v21, 0x2

    aget-object v20, v20, v21

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1010
    sget-object v20, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v21, 0x1

    aget-object v20, v20, v21

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1024
    .end local v14    # "pattern":Ljava/lang/String;
    .end local v16    # "ptn":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    :cond_3
    const/16 v20, 0x5

    move/from16 v0, v20

    invoke-static {v0, v6}, Lcom/ibm/icu/text/DateIntervalFormat;->fieldExistsInSkeleton(ILjava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_4

    .line 1026
    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    sget-object v21, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v22, 0x5

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1028
    const/16 v20, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2, v9, v7}, Lcom/ibm/icu/text/DateIntervalFormat;->genFallbackPattern(ILjava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/text/DateTimePatternGenerator;)V

    .line 1030
    :cond_4
    const/16 v20, 0x2

    move/from16 v0, v20

    invoke-static {v0, v6}, Lcom/ibm/icu/text/DateIntervalFormat;->fieldExistsInSkeleton(ILjava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_5

    .line 1032
    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    sget-object v21, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v22, 0x2

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1034
    const/16 v20, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2, v9, v7}, Lcom/ibm/icu/text/DateIntervalFormat;->genFallbackPattern(ILjava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/text/DateTimePatternGenerator;)V

    .line 1036
    :cond_5
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-static {v0, v6}, Lcom/ibm/icu/text/DateIntervalFormat;->fieldExistsInSkeleton(ILjava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_6

    .line 1038
    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    sget-object v21, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v22, 0x1

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1040
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2, v9, v7}, Lcom/ibm/icu/text/DateIntervalFormat;->genFallbackPattern(ILjava/lang/String;Ljava/util/HashMap;Lcom/ibm/icu/text/DateTimePatternGenerator;)V

    .line 1051
    :cond_6
    new-instance v3, Lcom/ibm/icu/impl/CalendarData;

    const/16 v20, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-direct {v3, v0, v1}, Lcom/ibm/icu/impl/CalendarData;-><init>(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)V

    .line 1052
    .local v3, "calData":Lcom/ibm/icu/impl/CalendarData;
    const-string/jumbo v20, "DateTimePatterns"

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/ibm/icu/impl/CalendarData;->get(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/ibm/icu/impl/ICUResourceBundle;->getStringArray()[Ljava/lang/String;

    move-result-object v15

    .line 1053
    .local v15, "patterns":[Ljava/lang/String;
    invoke-virtual {v7, v6}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getBestPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1054
    .local v5, "datePattern":Ljava/lang/String;
    const/16 v20, 0x8

    aget-object v20, v15, v20

    const/16 v21, 0x9

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v5, v2, v9}, Lcom/ibm/icu/text/DateIntervalFormat;->concatSingleDate2TimeInterval(Ljava/lang/String;Ljava/lang/String;ILjava/util/HashMap;)V

    .line 1055
    const/16 v20, 0x8

    aget-object v20, v15, v20

    const/16 v21, 0xa

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v5, v2, v9}, Lcom/ibm/icu/text/DateIntervalFormat;->concatSingleDate2TimeInterval(Ljava/lang/String;Ljava/lang/String;ILjava/util/HashMap;)V

    .line 1056
    const/16 v20, 0x8

    aget-object v20, v15, v20

    const/16 v21, 0xc

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v5, v2, v9}, Lcom/ibm/icu/text/DateIntervalFormat;->concatSingleDate2TimeInterval(Ljava/lang/String;Ljava/lang/String;ILjava/util/HashMap;)V

    goto/16 :goto_0
.end method

.method private initializePattern()V
    .locals 7

    .prologue
    .line 846
    iget-object v5, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v5}, Lcom/ibm/icu/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v0

    .line 847
    .local v0, "fullPattern":Ljava/lang/String;
    iget-object v5, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v5}, Lcom/ibm/icu/text/SimpleDateFormat;->getLocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v3

    .line 849
    .local v3, "locale":Lcom/ibm/icu/util/ULocale;
    iget-object v5, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fSkeleton:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 850
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3}, Lcom/ibm/icu/util/ULocale;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, "+"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, "+"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fSkeleton:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 854
    .local v2, "key":Ljava/lang/String;
    :goto_0
    sget-object v5, Lcom/ibm/icu/text/DateIntervalFormat;->LOCAL_PATTERN_CACHE:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v5, v2}, Lcom/ibm/icu/impl/ICUCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    .line 855
    .local v4, "patterns":Ljava/util/Map;
    if-nez v4, :cond_0

    .line 856
    invoke-direct {p0, v0, v3}, Lcom/ibm/icu/text/DateIntervalFormat;->initializeIntervalPattern(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/util/HashMap;

    move-result-object v1

    .line 857
    .local v1, "intervalPatterns":Ljava/util/HashMap;
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v4

    .line 858
    sget-object v5, Lcom/ibm/icu/text/DateIntervalFormat;->LOCAL_PATTERN_CACHE:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v5, v2, v4}, Lcom/ibm/icu/impl/ICUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 860
    .end local v1    # "intervalPatterns":Ljava/util/HashMap;
    :cond_0
    iput-object v4, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fIntervalPatterns:Ljava/util/Map;

    .line 861
    return-void

    .line 852
    .end local v2    # "key":Ljava/lang/String;
    .end local v4    # "patterns":Ljava/util/Map;
    :cond_1
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3}, Lcom/ibm/icu/util/ULocale;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, "+"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "key":Ljava/lang/String;
    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 0
    .param p1, "stream"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 1622
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 1623
    invoke-direct {p0}, Lcom/ibm/icu/text/DateIntervalFormat;->initializePattern()V

    .line 1624
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 532
    invoke-super {p0}, Lcom/ibm/icu/text/UFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/DateIntervalFormat;

    .line 533
    .local v0, "other":Lcom/ibm/icu/text/DateIntervalFormat;
    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v1}, Lcom/ibm/icu/text/SimpleDateFormat;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/SimpleDateFormat;

    iput-object v1, v0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    .line 534
    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    invoke-virtual {v1}, Lcom/ibm/icu/text/DateIntervalInfo;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/DateIntervalInfo;

    iput-object v1, v0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    .line 535
    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fFromCalendar:Lcom/ibm/icu/util/Calendar;

    invoke-virtual {v1}, Lcom/ibm/icu/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/util/Calendar;

    iput-object v1, v0, Lcom/ibm/icu/text/DateIntervalFormat;->fFromCalendar:Lcom/ibm/icu/util/Calendar;

    .line 536
    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fToCalendar:Lcom/ibm/icu/util/Calendar;

    invoke-virtual {v1}, Lcom/ibm/icu/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/util/Calendar;

    iput-object v1, v0, Lcom/ibm/icu/text/DateIntervalFormat;->fToCalendar:Lcom/ibm/icu/util/Calendar;

    .line 537
    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fSkeleton:Ljava/lang/String;

    iput-object v1, v0, Lcom/ibm/icu/text/DateIntervalFormat;->fSkeleton:Ljava/lang/String;

    .line 538
    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fIntervalPatterns:Ljava/util/Map;

    iput-object v1, v0, Lcom/ibm/icu/text/DateIntervalFormat;->fIntervalPatterns:Ljava/util/Map;

    .line 539
    return-object v0
.end method

.method public final format(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 11
    .param p1, "fromCalendar"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "toCalendar"    # Lcom/ibm/icu/util/Calendar;
    .param p3, "appendTo"    # Ljava/lang/StringBuffer;
    .param p4, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 616
    invoke-virtual {p1, p2}, Lcom/ibm/icu/util/Calendar;->isEquivalentTo(Lcom/ibm/icu/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/ibm/icu/util/Calendar;->getType()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "gregorian"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 618
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "can not format on two different calendars or non-Gregorian calendars"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 622
    :cond_1
    const/16 v6, 0x16

    .line 624
    .local v6, "field":I
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 625
    const/4 v6, 0x0

    .line 652
    :goto_0
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fIntervalPatterns:Ljava/util/Map;

    sget-object v1, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    aget-object v1, v1, v6

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    .line 656
    .local v8, "intervalPattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    if-nez v8, :cond_a

    .line 657
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v0, v6}, Lcom/ibm/icu/text/SimpleDateFormat;->isFieldUnitIgnored(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 662
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v0, p1, p3, p4}, Lcom/ibm/icu/text/SimpleDateFormat;->format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object p3

    .line 695
    .end local v8    # "intervalPattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    .end local p3    # "appendTo":Ljava/lang/StringBuffer;
    :goto_1
    return-object p3

    .line 626
    .restart local p3    # "appendTo":Ljava/lang/StringBuffer;
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 628
    const/4 v6, 0x1

    .line 629
    goto :goto_0

    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x2

    invoke-virtual {p2, v1}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v1

    if-eq v0, v1, :cond_4

    .line 631
    const/4 v6, 0x2

    .line 632
    goto :goto_0

    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x5

    invoke-virtual {p2, v1}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v1

    if-eq v0, v1, :cond_5

    .line 634
    const/4 v6, 0x5

    .line 635
    goto :goto_0

    :cond_5
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v0

    const/16 v1, 0x9

    invoke-virtual {p2, v1}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v1

    if-eq v0, v1, :cond_6

    .line 637
    const/16 v6, 0x9

    .line 638
    goto :goto_0

    :cond_6
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v0

    const/16 v1, 0xa

    invoke-virtual {p2, v1}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v1

    if-eq v0, v1, :cond_7

    .line 640
    const/16 v6, 0xa

    .line 641
    goto :goto_0

    :cond_7
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v0

    const/16 v1, 0xc

    invoke-virtual {p2, v1}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v1

    if-eq v0, v1, :cond_8

    .line 643
    const/16 v6, 0xc

    .line 644
    goto :goto_0

    .line 648
    :cond_8
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v0, p1, p3, p4}, Lcom/ibm/icu/text/SimpleDateFormat;->format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object p3

    goto :goto_1

    .line 665
    .restart local v8    # "intervalPattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    :cond_9
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/ibm/icu/text/DateIntervalFormat;->fallbackFormat(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object p3

    goto :goto_1

    .line 671
    :cond_a
    invoke-virtual {v8}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->getFirstPart()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_b

    .line 673
    invoke-virtual {v8}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->getSecondPart()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/DateIntervalFormat;->fallbackFormat(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object p3

    goto :goto_1

    .line 678
    :cond_b
    invoke-virtual {v8}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->firstDateInPtnIsLaterDate()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 679
    move-object v7, p2

    .line 680
    .local v7, "firstCal":Lcom/ibm/icu/util/Calendar;
    move-object v10, p1

    .line 687
    .local v10, "secondCal":Lcom/ibm/icu/util/Calendar;
    :goto_2
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v0}, Lcom/ibm/icu/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v9

    .line 688
    .local v9, "originalPattern":Ljava/lang/String;
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v8}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->getFirstPart()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 689
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v0, v7, p3, p4}, Lcom/ibm/icu/text/SimpleDateFormat;->format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 690
    invoke-virtual {v8}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->getSecondPart()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 691
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v8}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->getSecondPart()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 692
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v0, v10, p3, p4}, Lcom/ibm/icu/text/SimpleDateFormat;->format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 694
    :cond_c
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v0, v9}, Lcom/ibm/icu/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 682
    .end local v7    # "firstCal":Lcom/ibm/icu/util/Calendar;
    .end local v9    # "originalPattern":Ljava/lang/String;
    .end local v10    # "secondCal":Lcom/ibm/icu/util/Calendar;
    :cond_d
    move-object v7, p1

    .line 683
    .restart local v7    # "firstCal":Lcom/ibm/icu/util/Calendar;
    move-object v10, p2

    .restart local v10    # "secondCal":Lcom/ibm/icu/util/Calendar;
    goto :goto_2
.end method

.method public final format(Lcom/ibm/icu/util/DateInterval;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 4
    .param p1, "dtInterval"    # Lcom/ibm/icu/util/DateInterval;
    .param p2, "appendTo"    # Ljava/lang/StringBuffer;
    .param p3, "fieldPosition"    # Ljava/text/FieldPosition;

    .prologue
    .line 588
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fFromCalendar:Lcom/ibm/icu/util/Calendar;

    invoke-virtual {p1}, Lcom/ibm/icu/util/DateInterval;->getFromDate()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/ibm/icu/util/Calendar;->setTimeInMillis(J)V

    .line 589
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fToCalendar:Lcom/ibm/icu/util/Calendar;

    invoke-virtual {p1}, Lcom/ibm/icu/util/DateInterval;->getToDate()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/ibm/icu/util/Calendar;->setTimeInMillis(J)V

    .line 590
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fFromCalendar:Lcom/ibm/icu/util/Calendar;

    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fToCalendar:Lcom/ibm/icu/util/Calendar;

    invoke-virtual {p0, v0, v1, p2, p3}, Lcom/ibm/icu/text/DateIntervalFormat;->format(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public final format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "appendTo"    # Ljava/lang/StringBuffer;
    .param p3, "fieldPosition"    # Ljava/text/FieldPosition;

    .prologue
    .line 564
    instance-of v0, p1, Lcom/ibm/icu/util/DateInterval;

    if-eqz v0, :cond_0

    .line 565
    check-cast p1, Lcom/ibm/icu/util/DateInterval;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3}, Lcom/ibm/icu/text/DateIntervalFormat;->format(Lcom/ibm/icu/util/DateInterval;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0

    .line 568
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Cannot format given Object ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ") as a DateInterval"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getDateFormat()Lcom/ibm/icu/text/DateFormat;
    .locals 1

    .prologue
    .line 834
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v0}, Lcom/ibm/icu/text/SimpleDateFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/DateFormat;

    return-object v0
.end method

.method public getDateIntervalInfo()Lcom/ibm/icu/text/DateIntervalInfo;
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    invoke-virtual {v0}, Lcom/ibm/icu/text/DateIntervalInfo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/DateIntervalInfo;

    return-object v0
.end method

.method public parseObject(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Object;
    .locals 2
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "parse_pos"    # Ljava/text/ParsePosition;

    .prologue
    .line 789
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "parsing is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setDateIntervalInfo(Lcom/ibm/icu/text/DateIntervalInfo;)V
    .locals 1
    .param p1, "newItvPattern"    # Lcom/ibm/icu/text/DateIntervalInfo;

    .prologue
    .line 816
    invoke-virtual {p1}, Lcom/ibm/icu/text/DateIntervalInfo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/DateIntervalInfo;

    iput-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    .line 817
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fInfo:Lcom/ibm/icu/text/DateIntervalInfo;

    invoke-virtual {v0}, Lcom/ibm/icu/text/DateIntervalInfo;->freeze()Ljava/lang/Object;

    .line 818
    sget-object v0, Lcom/ibm/icu/text/DateIntervalFormat;->LOCAL_PATTERN_CACHE:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v0}, Lcom/ibm/icu/impl/ICUCache;->clear()V

    .line 819
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalFormat;->fDateFormat:Lcom/ibm/icu/text/SimpleDateFormat;

    if-eqz v0, :cond_0

    .line 820
    invoke-direct {p0}, Lcom/ibm/icu/text/DateIntervalFormat;->initializePattern()V

    .line 822
    :cond_0
    return-void
.end method
