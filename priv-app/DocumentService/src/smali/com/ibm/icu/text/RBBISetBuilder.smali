.class Lcom/ibm/icu/text/RBBISetBuilder;
.super Ljava/lang/Object;
.source "RBBISetBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/RBBISetBuilder$RBBIDataManipulate;,
        Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
    }
.end annotation


# instance fields
.field dm:Lcom/ibm/icu/text/RBBISetBuilder$RBBIDataManipulate;

.field fGroupCount:I

.field fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

.field fRangeList:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

.field fSawBOF:Z

.field fTrie:Lcom/ibm/icu/impl/IntTrieBuilder;

.field fTrieSize:I


# direct methods
.method constructor <init>(Lcom/ibm/icu/text/RBBIRuleBuilder;)V
    .locals 1
    .param p1, "rb"    # Lcom/ibm/icu/text/RBBIRuleBuilder;

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331
    new-instance v0, Lcom/ibm/icu/text/RBBISetBuilder$RBBIDataManipulate;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/RBBISetBuilder$RBBIDataManipulate;-><init>(Lcom/ibm/icu/text/RBBISetBuilder;)V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBISetBuilder;->dm:Lcom/ibm/icu/text/RBBISetBuilder$RBBIDataManipulate;

    .line 146
    iput-object p1, p0, Lcom/ibm/icu/text/RBBISetBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    .line 147
    return-void
.end method


# virtual methods
.method addValToSet(Lcom/ibm/icu/text/RBBINode;I)V
    .locals 3
    .param p1, "usetNode"    # Lcom/ibm/icu/text/RBBINode;
    .param p2, "val"    # I

    .prologue
    .line 384
    new-instance v0, Lcom/ibm/icu/text/RBBINode;

    const/4 v2, 0x3

    invoke-direct {v0, v2}, Lcom/ibm/icu/text/RBBINode;-><init>(I)V

    .line 385
    .local v0, "leafNode":Lcom/ibm/icu/text/RBBINode;
    iput p2, v0, Lcom/ibm/icu/text/RBBINode;->fVal:I

    .line 386
    iget-object v2, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    if-nez v2, :cond_0

    .line 387
    iput-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 388
    iput-object p1, v0, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 401
    :goto_0
    return-void

    .line 393
    :cond_0
    new-instance v1, Lcom/ibm/icu/text/RBBINode;

    const/16 v2, 0x9

    invoke-direct {v1, v2}, Lcom/ibm/icu/text/RBBINode;-><init>(I)V

    .line 394
    .local v1, "orNode":Lcom/ibm/icu/text/RBBINode;
    iget-object v2, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iput-object v2, v1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 395
    iput-object v0, v1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    .line 396
    iget-object v2, v1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iput-object v1, v2, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 397
    iget-object v2, v1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iput-object v1, v2, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 398
    iput-object v1, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 399
    iput-object p1, v1, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    goto :goto_0
.end method

.method addValToSets(Ljava/util/List;I)V
    .locals 3
    .param p1, "sets"    # Ljava/util/List;
    .param p2, "val"    # I

    .prologue
    .line 377
    const/4 v0, 0x0

    .local v0, "ix":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 378
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/RBBINode;

    .line 379
    .local v1, "usetNode":Lcom/ibm/icu/text/RBBINode;
    invoke-virtual {p0, v1, p2}, Lcom/ibm/icu/text/RBBISetBuilder;->addValToSet(Lcom/ibm/icu/text/RBBINode;I)V

    .line 377
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 381
    .end local v1    # "usetNode":Lcom/ibm/icu/text/RBBINode;
    :cond_0
    return-void
.end method

.method build()V
    .locals 19

    .prologue
    .line 160
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v3, "usets"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/RBBISetBuilder;->printSets()V

    .line 165
    :cond_0
    new-instance v2, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    invoke-direct {v2}, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRangeList:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    .line 166
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRangeList:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    const/4 v3, 0x0

    iput v3, v2, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fStartChar:I

    .line 167
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRangeList:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    const v3, 0x10ffff

    iput v3, v2, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fEndChar:I

    .line 172
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIRuleBuilder;->fUSetNodes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .line 173
    .local v15, "ni":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 174
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/ibm/icu/text/RBBINode;

    .line 176
    .local v18, "usetNode":Lcom/ibm/icu/text/RBBINode;
    move-object/from16 v0, v18

    iget-object v10, v0, Lcom/ibm/icu/text/RBBINode;->fInputSet:Lcom/ibm/icu/text/UnicodeSet;

    .line 177
    .local v10, "inputSet":Lcom/ibm/icu/text/UnicodeSet;
    invoke-virtual {v10}, Lcom/ibm/icu/text/UnicodeSet;->getRangeCount()I

    move-result v12

    .line 178
    .local v12, "inputSetRangeCount":I
    const/4 v14, 0x0

    .line 179
    .local v14, "inputSetRangeIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRangeList:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    move-object/from16 v16, v0

    .line 182
    .local v16, "rlRange":Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
    :goto_0
    if-ge v14, v12, :cond_1

    .line 185
    invoke-virtual {v10, v14}, Lcom/ibm/icu/text/UnicodeSet;->getRangeStart(I)I

    move-result v11

    .line 186
    .local v11, "inputSetRangeBegin":I
    invoke-virtual {v10, v14}, Lcom/ibm/icu/text/UnicodeSet;->getRangeEnd(I)I

    move-result v13

    .line 190
    .local v13, "inputSetRangeEnd":I
    :goto_1
    move-object/from16 v0, v16

    iget v2, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fEndChar:I

    if-ge v2, v11, :cond_2

    .line 191
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNext:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    move-object/from16 v16, v0

    .line 192
    goto :goto_1

    .line 200
    :cond_2
    move-object/from16 v0, v16

    iget v2, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fStartChar:I

    if-ge v2, v11, :cond_3

    .line 201
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->split(I)V

    goto :goto_0

    .line 210
    :cond_3
    move-object/from16 v0, v16

    iget v2, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fEndChar:I

    if-le v2, v13, :cond_4

    .line 211
    add-int/lit8 v2, v13, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->split(I)V

    .line 216
    :cond_4
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fIncludesSets:Ljava/util/List;

    move-object/from16 v0, v18

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_5

    .line 217
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fIncludesSets:Ljava/util/List;

    move-object/from16 v0, v18

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    :cond_5
    move-object/from16 v0, v16

    iget v2, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fEndChar:I

    if-ne v13, v2, :cond_6

    .line 222
    add-int/lit8 v14, v14, 0x1

    .line 224
    :cond_6
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNext:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    move-object/from16 v16, v0

    .line 225
    goto :goto_0

    .line 228
    .end local v10    # "inputSet":Lcom/ibm/icu/text/UnicodeSet;
    .end local v11    # "inputSetRangeBegin":I
    .end local v12    # "inputSetRangeCount":I
    .end local v13    # "inputSetRangeEnd":I
    .end local v14    # "inputSetRangeIndex":I
    .end local v16    # "rlRange":Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
    .end local v18    # "usetNode":Lcom/ibm/icu/text/RBBINode;
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v3, "range"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/RBBISetBuilder;->printRanges()V

    .line 242
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRangeList:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    move-object/from16 v16, v0

    .restart local v16    # "rlRange":Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
    :goto_2
    if-eqz v16, :cond_c

    .line 243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRangeList:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    move-object/from16 v17, v0

    .local v17, "rlSearchRange":Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
    :goto_3
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    if-eq v0, v1, :cond_9

    .line 244
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fIncludesSets:Ljava/util/List;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fIncludesSets:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 245
    move-object/from16 v0, v17

    iget v2, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    move-object/from16 v0, v16

    iput v2, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    .line 249
    :cond_9
    move-object/from16 v0, v16

    iget v2, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    if-nez v2, :cond_a

    .line 250
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fGroupCount:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fGroupCount:I

    .line 251
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fGroupCount:I

    add-int/lit8 v2, v2, 0x2

    move-object/from16 v0, v16

    iput v2, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    .line 252
    invoke-virtual/range {v16 .. v16}, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->setDictionaryFlag()V

    .line 253
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fIncludesSets:Ljava/util/List;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fGroupCount:I

    add-int/lit8 v3, v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/ibm/icu/text/RBBISetBuilder;->addValToSets(Ljava/util/List;I)V

    .line 242
    :cond_a
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNext:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    move-object/from16 v16, v0

    goto :goto_2

    .line 243
    :cond_b
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNext:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    move-object/from16 v17, v0

    goto :goto_3

    .line 267
    .end local v17    # "rlSearchRange":Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
    :cond_c
    const-string/jumbo v9, "eof"

    .line 268
    .local v9, "eofString":Ljava/lang/String;
    const-string/jumbo v8, "bof"

    .line 270
    .local v8, "bofString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIRuleBuilder;->fUSetNodes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .line 271
    :cond_d
    :goto_4
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 272
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/ibm/icu/text/RBBINode;

    .line 273
    .restart local v18    # "usetNode":Lcom/ibm/icu/text/RBBINode;
    move-object/from16 v0, v18

    iget-object v10, v0, Lcom/ibm/icu/text/RBBINode;->fInputSet:Lcom/ibm/icu/text/UnicodeSet;

    .line 274
    .restart local v10    # "inputSet":Lcom/ibm/icu/text/UnicodeSet;
    invoke-virtual {v10, v9}, Lcom/ibm/icu/text/UnicodeSet;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 275
    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/RBBISetBuilder;->addValToSet(Lcom/ibm/icu/text/RBBINode;I)V

    .line 277
    :cond_e
    invoke-virtual {v10, v8}, Lcom/ibm/icu/text/UnicodeSet;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 278
    const/4 v2, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/RBBISetBuilder;->addValToSet(Lcom/ibm/icu/text/RBBINode;I)V

    .line 279
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fSawBOF:Z

    goto :goto_4

    .line 284
    .end local v10    # "inputSet":Lcom/ibm/icu/text/UnicodeSet;
    .end local v18    # "usetNode":Lcom/ibm/icu/text/RBBINode;
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v3, "rgroup"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/RBBISetBuilder;->printRangeGroups()V

    .line 285
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v3, "esets"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/RBBISetBuilder;->printSets()V

    .line 292
    :cond_11
    new-instance v2, Lcom/ibm/icu/impl/IntTrieBuilder;

    const/4 v3, 0x0

    const v4, 0x186a0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v2 .. v7}, Lcom/ibm/icu/impl/IntTrieBuilder;-><init>([IIIIZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fTrie:Lcom/ibm/icu/impl/IntTrieBuilder;

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fRangeList:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    move-object/from16 v16, v0

    :goto_5
    if-eqz v16, :cond_12

    .line 299
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISetBuilder;->fTrie:Lcom/ibm/icu/impl/IntTrieBuilder;

    move-object/from16 v0, v16

    iget v3, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fStartChar:I

    move-object/from16 v0, v16

    iget v4, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fEndChar:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, v16

    iget v5, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/ibm/icu/impl/IntTrieBuilder;->setRange(IIIZ)Z

    .line 298
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNext:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    move-object/from16 v16, v0

    goto :goto_5

    .line 301
    :cond_12
    return-void
.end method

.method getFirstChar(I)I
    .locals 3
    .param p1, "category"    # I

    .prologue
    .line 432
    const/4 v0, -0x1

    .line 433
    .local v0, "retVal":I
    iget-object v1, p0, Lcom/ibm/icu/text/RBBISetBuilder;->fRangeList:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    .local v1, "rlRange":Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
    :goto_0
    if-eqz v1, :cond_0

    .line 434
    iget v2, v1, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    if-ne v2, p1, :cond_1

    .line 435
    iget v0, v1, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fStartChar:I

    .line 439
    :cond_0
    return v0

    .line 433
    :cond_1
    iget-object v1, v1, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNext:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    goto :goto_0
.end method

.method getNumCharCategories()I
    .locals 1

    .prologue
    .line 410
    iget v0, p0, Lcom/ibm/icu/text/RBBISetBuilder;->fGroupCount:I

    add-int/lit8 v0, v0, 0x3

    return v0
.end method

.method getTrieSize()I
    .locals 6

    .prologue
    .line 339
    const/4 v1, 0x0

    .line 343
    .local v1, "size":I
    :try_start_0
    iget-object v2, p0, Lcom/ibm/icu/text/RBBISetBuilder;->fTrie:Lcom/ibm/icu/impl/IntTrieBuilder;

    const/4 v3, 0x0

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/ibm/icu/text/RBBISetBuilder;->dm:Lcom/ibm/icu/text/RBBISetBuilder$RBBIDataManipulate;

    invoke-virtual {v2, v3, v4, v5}, Lcom/ibm/icu/impl/IntTrieBuilder;->serialize(Ljava/io/OutputStream;ZLcom/ibm/icu/impl/TrieBuilder$DataManipulate;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 347
    :goto_0
    return v1

    .line 344
    :catch_0
    move-exception v0

    .line 345
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    goto :goto_0
.end method

.method printRangeGroups()V
    .locals 13

    .prologue
    .line 488
    const/4 v3, 0x0

    .line 490
    .local v3, "lastPrintedGroupNum":I
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v11, "\nRanges grouped by Unicode Set Membership...\n"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 491
    iget-object v4, p0, Lcom/ibm/icu/text/RBBISetBuilder;->fRangeList:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    .local v4, "rlRange":Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
    :goto_0
    if-eqz v4, :cond_7

    .line 492
    iget v10, v4, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    const v11, 0xbfff

    and-int v0, v10, v11

    .line 493
    .local v0, "groupNum":I
    if-le v0, v3, :cond_6

    .line 494
    move v3, v0

    .line 495
    const/16 v10, 0xa

    if-ge v0, v10, :cond_0

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v11, " "

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 496
    :cond_0
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string/jumbo v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 498
    iget v10, v4, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    and-int/lit16 v10, v10, 0x4000

    if-eqz v10, :cond_1

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v11, " <DICT> "

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 500
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v10, v4, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fIncludesSets:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-ge v1, v10, :cond_3

    .line 501
    iget-object v10, v4, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fIncludesSets:Ljava/util/List;

    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/ibm/icu/text/RBBINode;

    .line 502
    .local v8, "usetNode":Lcom/ibm/icu/text/RBBINode;
    const-string/jumbo v5, "anon"

    .line 503
    .local v5, "setName":Ljava/lang/String;
    iget-object v6, v8, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 504
    .local v6, "setRef":Lcom/ibm/icu/text/RBBINode;
    if-eqz v6, :cond_2

    .line 505
    iget-object v9, v6, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 506
    .local v9, "varRef":Lcom/ibm/icu/text/RBBINode;
    if-eqz v9, :cond_2

    iget v10, v9, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_2

    .line 507
    iget-object v5, v9, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    .line 510
    .end local v9    # "varRef":Lcom/ibm/icu/text/RBBINode;
    :cond_2
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v10, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v11, " "

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 500
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 513
    .end local v5    # "setName":Ljava/lang/String;
    .end local v6    # "setRef":Lcom/ibm/icu/text/RBBINode;
    .end local v8    # "usetNode":Lcom/ibm/icu/text/RBBINode;
    :cond_3
    const/4 v1, 0x0

    .line 514
    move-object v7, v4

    .local v7, "tRange":Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
    move v2, v1

    .end local v1    # "i":I
    .local v2, "i":I
    :goto_2
    if-eqz v7, :cond_5

    .line 515
    iget v10, v7, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    iget v11, v4, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    if-ne v10, v11, :cond_8

    .line 516
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    rem-int/lit8 v10, v2, 0x5

    if-nez v10, :cond_4

    .line 517
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v11, "\n    "

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 519
    :cond_4
    iget v10, v7, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fStartChar:I

    const/4 v11, -0x1

    invoke-static {v10, v11}, Lcom/ibm/icu/text/RBBINode;->printHex(II)V

    .line 520
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v11, "-"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 521
    iget v10, v7, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fEndChar:I

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/ibm/icu/text/RBBINode;->printHex(II)V

    .line 514
    :goto_3
    iget-object v7, v7, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNext:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_2

    .line 524
    :cond_5
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v11, "\n"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 491
    .end local v2    # "i":I
    .end local v7    # "tRange":Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
    :cond_6
    iget-object v4, v4, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNext:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    goto/16 :goto_0

    .line 527
    .end local v0    # "groupNum":I
    :cond_7
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v11, "\n"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 528
    return-void

    .restart local v0    # "groupNum":I
    .restart local v2    # "i":I
    .restart local v7    # "tRange":Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
    :cond_8
    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_3
.end method

.method printRanges()V
    .locals 9

    .prologue
    .line 455
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v7, "\n\n Nonoverlapping Ranges ...\n"

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 456
    iget-object v1, p0, Lcom/ibm/icu/text/RBBISetBuilder;->fRangeList:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    .local v1, "rlRange":Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
    :goto_0
    if-eqz v1, :cond_2

    .line 457
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget v8, v1, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string/jumbo v8, "   "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget v8, v1, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fStartChar:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string/jumbo v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget v8, v1, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fEndChar:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 459
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v6, v1, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fIncludesSets:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_1

    .line 460
    iget-object v6, v1, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fIncludesSets:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/text/RBBINode;

    .line 461
    .local v4, "usetNode":Lcom/ibm/icu/text/RBBINode;
    const-string/jumbo v2, "anon"

    .line 462
    .local v2, "setName":Ljava/lang/String;
    iget-object v3, v4, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 463
    .local v3, "setRef":Lcom/ibm/icu/text/RBBINode;
    if-eqz v3, :cond_0

    .line 464
    iget-object v5, v3, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 465
    .local v5, "varRef":Lcom/ibm/icu/text/RBBINode;
    if-eqz v5, :cond_0

    iget v6, v5, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_0

    .line 466
    iget-object v2, v5, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    .line 469
    .end local v5    # "varRef":Lcom/ibm/icu/text/RBBINode;
    :cond_0
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v6, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v7, "  "

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 459
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 471
    .end local v2    # "setName":Ljava/lang/String;
    .end local v3    # "setRef":Lcom/ibm/icu/text/RBBINode;
    .end local v4    # "usetNode":Lcom/ibm/icu/text/RBBINode;
    :cond_1
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v7, ""

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 456
    iget-object v1, v1, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNext:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    goto :goto_0

    .line 473
    .end local v0    # "i":I
    :cond_2
    return-void
.end method

.method printSets()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    .line 541
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "\n\nUnicode Sets List\n------------------\n"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 542
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v5, p0, Lcom/ibm/icu/text/RBBISetBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fUSetNodes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 548
    iget-object v5, p0, Lcom/ibm/icu/text/RBBISetBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fUSetNodes:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/text/RBBINode;

    .line 551
    .local v3, "usetNode":Lcom/ibm/icu/text/RBBINode;
    invoke-static {v8, v0}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 552
    const-string/jumbo v1, "anonymous"

    .line 553
    .local v1, "setName":Ljava/lang/String;
    iget-object v2, v3, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 554
    .local v2, "setRef":Lcom/ibm/icu/text/RBBINode;
    if-eqz v2, :cond_0

    .line 555
    iget-object v4, v2, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 556
    .local v4, "varRef":Lcom/ibm/icu/text/RBBINode;
    if-eqz v4, :cond_0

    iget v5, v4, Lcom/ibm/icu/text/RBBINode;->fType:I

    if-ne v5, v8, :cond_0

    .line 557
    iget-object v1, v4, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    .line 560
    .end local v4    # "varRef":Lcom/ibm/icu/text/RBBINode;
    :cond_0
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 561
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "   "

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 562
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v6, v3, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 563
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "\n"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 564
    iget-object v5, v3, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    if-eqz v5, :cond_1

    .line 565
    iget-object v5, v3, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/ibm/icu/text/RBBINode;->printTree(Z)V

    .line 542
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 568
    .end local v1    # "setName":Ljava/lang/String;
    .end local v2    # "setRef":Lcom/ibm/icu/text/RBBINode;
    .end local v3    # "usetNode":Lcom/ibm/icu/text/RBBINode;
    :cond_2
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "\n"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 569
    return-void
.end method

.method sawBOF()Z
    .locals 1

    .prologue
    .line 420
    iget-boolean v0, p0, Lcom/ibm/icu/text/RBBISetBuilder;->fSawBOF:Z

    return v0
.end method

.method serializeTrie(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 357
    iget-object v0, p0, Lcom/ibm/icu/text/RBBISetBuilder;->fTrie:Lcom/ibm/icu/impl/IntTrieBuilder;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/ibm/icu/text/RBBISetBuilder;->dm:Lcom/ibm/icu/text/RBBISetBuilder$RBBIDataManipulate;

    invoke-virtual {v0, p1, v1, v2}, Lcom/ibm/icu/impl/IntTrieBuilder;->serialize(Ljava/io/OutputStream;ZLcom/ibm/icu/impl/TrieBuilder$DataManipulate;)I

    .line 358
    return-void
.end method
