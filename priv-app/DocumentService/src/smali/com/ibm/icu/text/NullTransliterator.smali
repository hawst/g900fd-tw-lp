.class Lcom/ibm/icu/text/NullTransliterator;
.super Lcom/ibm/icu/text/Transliterator;
.source "NullTransliterator.java"


# static fields
.field static SHORT_ID:Ljava/lang/String;

.field static _ID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string/jumbo v0, "Null"

    sput-object v0, Lcom/ibm/icu/text/NullTransliterator;->SHORT_ID:Ljava/lang/String;

    .line 18
    const-string/jumbo v0, "Any-Null"

    sput-object v0, Lcom/ibm/icu/text/NullTransliterator;->_ID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    sget-object v0, Lcom/ibm/icu/text/NullTransliterator;->_ID:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/Transliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 26
    return-void
.end method


# virtual methods
.method protected handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V
    .locals 1
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "offsets"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "incremental"    # Z

    .prologue
    .line 34
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    iput v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 35
    return-void
.end method
