.class final Lcom/ibm/icu/text/CollatorReader;
.super Ljava/lang/Object;
.source "CollatorReader.java"


# static fields
.field private static final DATA_FORMAT_ID_:[B

.field private static final DATA_FORMAT_VERSION_:[B

.field private static final INVERSE_UCA_AUTHENTICATE_:Lcom/ibm/icu/impl/ICUBinary$Authenticate;

.field private static final INVERSE_UCA_DATA_FORMAT_ID_:[B

.field private static final INVERSE_UCA_DATA_FORMAT_VERSION_:[B

.field private static final UCA_AUTHENTICATE_:Lcom/ibm/icu/impl/ICUBinary$Authenticate;

.field private static final WRONG_UNICODE_VERSION_ERROR_:Ljava/lang/String; = "Unicode version in binary image is not compatible with the current Unicode version"


# instance fields
.field private m_UCAValuesSize_:I

.field private m_contractionCESize_:I

.field private m_contractionEndSize_:I

.field private m_contractionIndexSize_:I

.field private m_dataInputStream_:Ljava/io/DataInputStream;

.field private m_expansionEndCEMaxSizeSize_:I

.field private m_expansionEndCESize_:I

.field private m_expansionSize_:I

.field private m_headerSize_:I

.field private m_optionSize_:I

.field private m_size_:I

.field private m_unsafeSize_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 541
    new-instance v0, Lcom/ibm/icu/text/CollatorReader$1;

    invoke-direct {v0}, Lcom/ibm/icu/text/CollatorReader$1;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/CollatorReader;->UCA_AUTHENTICATE_:Lcom/ibm/icu/impl/ICUBinary$Authenticate;

    .line 557
    new-instance v0, Lcom/ibm/icu/text/CollatorReader$2;

    invoke-direct {v0}, Lcom/ibm/icu/text/CollatorReader$2;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/CollatorReader;->INVERSE_UCA_AUTHENTICATE_:Lcom/ibm/icu/impl/ICUBinary$Authenticate;

    .line 577
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/text/CollatorReader;->DATA_FORMAT_VERSION_:[B

    .line 579
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/ibm/icu/text/CollatorReader;->DATA_FORMAT_ID_:[B

    .line 585
    new-array v0, v1, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/ibm/icu/text/CollatorReader;->INVERSE_UCA_DATA_FORMAT_VERSION_:[B

    .line 587
    new-array v0, v1, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom/ibm/icu/text/CollatorReader;->INVERSE_UCA_DATA_FORMAT_ID_:[B

    return-void

    .line 577
    :array_0
    .array-data 1
        0x2t
        0x2t
        0x0t
        0x0t
    .end array-data

    .line 579
    :array_1
    .array-data 1
        0x55t
        0x43t
        0x6ft
        0x6ct
    .end array-data

    .line 585
    :array_2
    .array-data 1
        0x2t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 587
    :array_3
    .array-data 1
        0x49t
        0x6et
        0x76t
        0x43t
    .end array-data
.end method

.method private constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/CollatorReader;-><init>(Ljava/io/InputStream;Z)V

    .line 101
    return-void
.end method

.method private constructor <init>(Ljava/io/InputStream;Z)V
    .locals 4
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "readICUHeader"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    if-eqz p2, :cond_1

    .line 113
    sget-object v2, Lcom/ibm/icu/text/CollatorReader;->DATA_FORMAT_ID_:[B

    sget-object v3, Lcom/ibm/icu/text/CollatorReader;->UCA_AUTHENTICATE_:Lcom/ibm/icu/impl/ICUBinary$Authenticate;

    invoke-static {p1, v2, v3}, Lcom/ibm/icu/impl/ICUBinary;->readHeader(Ljava/io/InputStream;[BLcom/ibm/icu/impl/ICUBinary$Authenticate;)[B

    move-result-object v1

    .line 117
    .local v1, "UnicodeVersion":[B
    invoke-static {}, Lcom/ibm/icu/lang/UCharacter;->getUnicodeVersion()Lcom/ibm/icu/util/VersionInfo;

    move-result-object v0

    .line 118
    .local v0, "UCDVersion":Lcom/ibm/icu/util/VersionInfo;
    const/4 v2, 0x0

    aget-byte v2, v1, v2

    invoke-virtual {v0}, Lcom/ibm/icu/util/VersionInfo;->getMajor()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    aget-byte v2, v1, v2

    invoke-virtual {v0}, Lcom/ibm/icu/util/VersionInfo;->getMinor()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 120
    :cond_0
    new-instance v2, Ljava/io/IOException;

    const-string/jumbo v3, "Unicode version in binary image is not compatible with the current Unicode version"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 123
    .end local v0    # "UCDVersion":Lcom/ibm/icu/util/VersionInfo;
    .end local v1    # "UnicodeVersion":[B
    :cond_1
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v2, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    .line 124
    return-void
.end method

.method static access$000()[B
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/ibm/icu/text/CollatorReader;->DATA_FORMAT_VERSION_:[B

    return-object v0
.end method

.method static access$100()[B
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/ibm/icu/text/CollatorReader;->INVERSE_UCA_DATA_FORMAT_VERSION_:[B

    return-object v0
.end method

.method static getInverseUCA()Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    const/4 v2, 0x0

    .line 66
    .local v2, "result":Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;
    const-string/jumbo v3, "data/icudt40b/invuca.icu"

    invoke-static {v3}, Lcom/ibm/icu/impl/ICUData;->getRequiredStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 70
    .local v1, "i":Ljava/io/InputStream;
    new-instance v0, Ljava/io/BufferedInputStream;

    const v3, 0x1adb0

    invoke-direct {v0, v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 71
    .local v0, "b":Ljava/io/BufferedInputStream;
    invoke-static {v0}, Lcom/ibm/icu/text/CollatorReader;->readInverseUCA(Ljava/io/InputStream;)Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;

    move-result-object v2

    .line 72
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 73
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 74
    return-object v2
.end method

.method static initRBC(Lcom/ibm/icu/text/RuleBasedCollator;[B)V
    .locals 6
    .param p0, "rbc"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    const/16 v0, 0x10c

    .line 51
    .local v0, "MIN_BINARY_DATA_SIZE_":I
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 52
    .local v2, "i":Ljava/io/InputStream;
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 53
    .local v1, "b":Ljava/io/BufferedInputStream;
    new-instance v3, Lcom/ibm/icu/text/CollatorReader;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4}, Lcom/ibm/icu/text/CollatorReader;-><init>(Ljava/io/InputStream;Z)V

    .line 54
    .local v3, "reader":Lcom/ibm/icu/text/CollatorReader;
    array-length v4, p1

    const/16 v5, 0x10c

    if-le v4, v5, :cond_0

    .line 55
    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/ibm/icu/text/CollatorReader;->readImp(Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;)[C

    .line 62
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-direct {v3, p0}, Lcom/ibm/icu/text/CollatorReader;->readHeader(Lcom/ibm/icu/text/RuleBasedCollator;)V

    .line 58
    invoke-direct {v3, p0}, Lcom/ibm/icu/text/CollatorReader;->readOptions(Lcom/ibm/icu/text/RuleBasedCollator;)V

    .line 60
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->setWithUCATables()V

    goto :goto_0
.end method

.method static read(Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;)[C
    .locals 5
    .param p0, "rbc"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p1, "ucac"    # Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    const-string/jumbo v4, "data/icudt40b/ucadata.icu"

    invoke-static {v4}, Lcom/ibm/icu/impl/ICUData;->getRequiredStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 41
    .local v1, "i":Ljava/io/InputStream;
    new-instance v0, Ljava/io/BufferedInputStream;

    const v4, 0x15f90

    invoke-direct {v0, v1, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 42
    .local v0, "b":Ljava/io/BufferedInputStream;
    new-instance v2, Lcom/ibm/icu/text/CollatorReader;

    invoke-direct {v2, v0}, Lcom/ibm/icu/text/CollatorReader;-><init>(Ljava/io/InputStream;)V

    .line 43
    .local v2, "reader":Lcom/ibm/icu/text/CollatorReader;
    invoke-direct {v2, p0, p1}, Lcom/ibm/icu/text/CollatorReader;->readImp(Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;)[C

    move-result-object v3

    .line 44
    .local v3, "result":[C
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 45
    return-object v3
.end method

.method private readHeader(Lcom/ibm/icu/text/RuleBasedCollator;)V
    .locals 12
    .param p1, "rbc"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x4

    .line 136
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v8

    iput v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_size_:I

    .line 140
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v8

    iput v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_headerSize_:I

    .line 141
    const/16 v6, 0x8

    .line 144
    .local v6, "readcount":I
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 145
    .local v0, "UCAConst":I
    add-int/lit8 v6, v6, 0x4

    .line 148
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    const-wide/16 v10, 0x4

    invoke-virtual {v8, v10, v11}, Ljava/io/DataInputStream;->skip(J)J

    .line 149
    add-int/lit8 v6, v6, 0x4

    .line 151
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8, v9}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 152
    add-int/lit8 v6, v6, 0x4

    .line 154
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    .line 155
    .local v5, "mapping":I
    add-int/lit8 v6, v6, 0x4

    .line 157
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v8

    iput v8, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionOffset_:I

    .line 158
    add-int/lit8 v6, v6, 0x4

    .line 160
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v8

    iput v8, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    .line 161
    add-int/lit8 v6, v6, 0x4

    .line 163
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 164
    .local v1, "contractionCE":I
    add-int/lit8 v6, v6, 0x4

    .line 166
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    .line 167
    add-int/lit8 v6, v6, 0x4

    .line 169
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 170
    .local v3, "expansionEndCE":I
    add-int/lit8 v6, v6, 0x4

    .line 173
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    .line 174
    .local v4, "expansionEndCEMaxSize":I
    add-int/lit8 v6, v6, 0x4

    .line 176
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8, v9}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 177
    add-int/lit8 v6, v6, 0x4

    .line 179
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    .line 180
    .local v7, "unsafe":I
    add-int/lit8 v6, v6, 0x4

    .line 182
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    .line 183
    .local v2, "contractionEnd":I
    add-int/lit8 v6, v6, 0x4

    .line 185
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8, v9}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 186
    add-int/lit8 v6, v6, 0x4

    .line 188
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readBoolean()Z

    move-result v8

    iput-boolean v8, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_isJamoSpecial_:Z

    .line 189
    add-int/lit8 v6, v6, 0x1

    .line 191
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    const/4 v9, 0x3

    invoke-virtual {v8, v9}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 192
    add-int/lit8 v6, v6, 0x3

    .line 193
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-static {v8}, Lcom/ibm/icu/text/CollatorReader;->readVersion(Ljava/io/DataInputStream;)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v8

    iput-object v8, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_version_:Lcom/ibm/icu/util/VersionInfo;

    .line 194
    add-int/lit8 v6, v6, 0x4

    .line 195
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-static {v8}, Lcom/ibm/icu/text/CollatorReader;->readVersion(Ljava/io/DataInputStream;)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v8

    iput-object v8, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_UCA_version_:Lcom/ibm/icu/util/VersionInfo;

    .line 196
    add-int/lit8 v6, v6, 0x4

    .line 197
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-static {v8}, Lcom/ibm/icu/text/CollatorReader;->readVersion(Ljava/io/DataInputStream;)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v8

    iput-object v8, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_UCD_version_:Lcom/ibm/icu/util/VersionInfo;

    .line 198
    add-int/lit8 v6, v6, 0x4

    .line 200
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    const/16 v9, 0x20

    invoke-virtual {v8, v9}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 201
    add-int/lit8 v6, v6, 0x20

    .line 202
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    const/16 v9, 0x38

    invoke-virtual {v8, v9}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 203
    add-int/lit8 v6, v6, 0x38

    .line 204
    iget v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_headerSize_:I

    if-ge v8, v6, :cond_0

    .line 205
    new-instance v8, Ljava/io/IOException;

    const-string/jumbo v9, "Internal Error: Header size error"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 207
    :cond_0
    iget-object v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    iget v9, p0, Lcom/ibm/icu/text/CollatorReader;->m_headerSize_:I

    add-int/lit16 v9, v9, -0xa8

    invoke-virtual {v8, v9}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 209
    iget v8, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    if-nez v8, :cond_1

    .line 210
    iput v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    .line 211
    move v1, v5

    .line 213
    :cond_1
    iget v8, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionOffset_:I

    iget v9, p0, Lcom/ibm/icu/text/CollatorReader;->m_headerSize_:I

    sub-int/2addr v8, v9

    iput v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_optionSize_:I

    .line 214
    iget v8, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    iget v9, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionOffset_:I

    sub-int/2addr v8, v9

    iput v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionSize_:I

    .line 215
    iget v8, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    sub-int v8, v1, v8

    iput v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionIndexSize_:I

    .line 216
    sub-int v8, v5, v1

    iput v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionCESize_:I

    .line 218
    sub-int v8, v4, v3

    iput v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionEndCESize_:I

    .line 219
    sub-int v8, v7, v4

    iput v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionEndCEMaxSizeSize_:I

    .line 220
    sub-int v8, v2, v7

    iput v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_unsafeSize_:I

    .line 221
    iget v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_size_:I

    sub-int/2addr v8, v0

    iput v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_UCAValuesSize_:I

    .line 225
    iget v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_size_:I

    sub-int/2addr v8, v2

    iput v8, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionEndSize_:I

    .line 227
    iget v8, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    shr-int/lit8 v8, v8, 0x1

    iput v8, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    .line 228
    iget v8, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionOffset_:I

    shr-int/lit8 v8, v8, 0x2

    iput v8, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionOffset_:I

    .line 229
    return-void
.end method

.method private readImp(Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;)[C
    .locals 10
    .param p1, "rbc"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "UCAConst"    # Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 296
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/CollatorReader;->readHeader(Lcom/ibm/icu/text/RuleBasedCollator;)V

    .line 298
    iget v2, p0, Lcom/ibm/icu/text/CollatorReader;->m_headerSize_:I

    .line 300
    .local v2, "readcount":I
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/CollatorReader;->readOptions(Lcom/ibm/icu/text/RuleBasedCollator;)V

    .line 301
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_optionSize_:I

    add-int/2addr v2, v5

    .line 302
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionSize_:I

    shr-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionSize_:I

    .line 303
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionSize_:I

    new-array v5, v5, [I

    iput-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    .line 304
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionSize_:I

    if-ge v0, v5, :cond_0

    .line 305
    iget-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v0

    .line 304
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 307
    :cond_0
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionSize_:I

    shl-int/lit8 v5, v5, 0x2

    add-int/2addr v2, v5

    .line 308
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionIndexSize_:I

    if-lez v5, :cond_3

    .line 309
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionIndexSize_:I

    shr-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionIndexSize_:I

    .line 310
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionIndexSize_:I

    new-array v5, v5, [C

    iput-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    .line 311
    const/4 v0, 0x0

    :goto_1
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionIndexSize_:I

    if-ge v0, v5, :cond_1

    .line 312
    iget-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readChar()C

    move-result v6

    aput-char v6, v5, v0

    .line 311
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 314
    :cond_1
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionIndexSize_:I

    shl-int/lit8 v5, v5, 0x1

    add-int/2addr v2, v5

    .line 315
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionCESize_:I

    shr-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionCESize_:I

    .line 316
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionCESize_:I

    new-array v5, v5, [I

    iput-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    .line 317
    const/4 v0, 0x0

    :goto_2
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionCESize_:I

    if-ge v0, v5, :cond_2

    .line 318
    iget-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v0

    .line 317
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 320
    :cond_2
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionCESize_:I

    shl-int/lit8 v5, v5, 0x2

    add-int/2addr v2, v5

    .line 322
    :cond_3
    new-instance v5, Lcom/ibm/icu/impl/IntTrie;

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-static {}, Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;->getInstance()Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/ibm/icu/impl/IntTrie;-><init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V

    iput-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    .line 324
    iget-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v5}, Lcom/ibm/icu/impl/IntTrie;->isLatin1Linear()Z

    move-result v5

    if-nez v5, :cond_4

    .line 325
    new-instance v5, Ljava/io/IOException;

    const-string/jumbo v6, "Data corrupted, Collator Tries expected to have linear latin one data arrays"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 329
    :cond_4
    iget-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v5}, Lcom/ibm/icu/impl/IntTrie;->getSerializedDataSize()I

    move-result v5

    add-int/2addr v2, v5

    .line 330
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionEndCESize_:I

    shr-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionEndCESize_:I

    .line 331
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionEndCESize_:I

    new-array v5, v5, [I

    iput-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    .line 332
    const/4 v0, 0x0

    :goto_3
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionEndCESize_:I

    if-ge v0, v5, :cond_5

    .line 333
    iget-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v0

    .line 332
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 335
    :cond_5
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionEndCESize_:I

    shl-int/lit8 v5, v5, 0x2

    add-int/2addr v2, v5

    .line 336
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionEndCEMaxSizeSize_:I

    new-array v5, v5, [B

    iput-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCEMaxSize_:[B

    .line 337
    const/4 v0, 0x0

    :goto_4
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionEndCEMaxSizeSize_:I

    if-ge v0, v5, :cond_6

    .line 338
    iget-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCEMaxSize_:[B

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readByte()B

    move-result v6

    aput-byte v6, v5, v0

    .line 337
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 340
    :cond_6
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_expansionEndCEMaxSizeSize_:I

    add-int/2addr v2, v5

    .line 341
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_unsafeSize_:I

    new-array v5, v5, [B

    iput-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_unsafe_:[B

    .line 342
    const/4 v0, 0x0

    :goto_5
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_unsafeSize_:I

    if-ge v0, v5, :cond_7

    .line 343
    iget-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_unsafe_:[B

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readByte()B

    move-result v6

    aput-byte v6, v5, v0

    .line 342
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 345
    :cond_7
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_unsafeSize_:I

    add-int/2addr v2, v5

    .line 346
    if-eqz p2, :cond_8

    .line 350
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionEndSize_:I

    iget v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_UCAValuesSize_:I

    sub-int/2addr v5, v6

    iput v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionEndSize_:I

    .line 352
    :cond_8
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionEndSize_:I

    new-array v5, v5, [B

    iput-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionEnd_:[B

    .line 353
    const/4 v0, 0x0

    :goto_6
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionEndSize_:I

    if-ge v0, v5, :cond_9

    .line 354
    iget-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionEnd_:[B

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readByte()B

    move-result v6

    aput-byte v6, v5, v0

    .line 353
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 356
    :cond_9
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_contractionEndSize_:I

    add-int/2addr v2, v5

    .line 357
    if-eqz p2, :cond_b

    .line 358
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_TERTIARY_IGNORABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 360
    const/4 v1, 0x4

    .line 361
    .local v1, "readUCAConstcount":I
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_TERTIARY_IGNORABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 363
    add-int/lit8 v1, v1, 0x4

    .line 364
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_TERTIARY_IGNORABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 366
    add-int/lit8 v1, v1, 0x4

    .line 367
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_TERTIARY_IGNORABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 369
    add-int/lit8 v1, v1, 0x4

    .line 370
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_PRIMARY_IGNORABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 372
    add-int/lit8 v1, v1, 0x4

    .line 373
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_PRIMARY_IGNORABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 375
    add-int/lit8 v1, v1, 0x4

    .line 376
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_SECONDARY_IGNORABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 378
    add-int/lit8 v1, v1, 0x4

    .line 379
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_SECONDARY_IGNORABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 381
    add-int/lit8 v1, v1, 0x4

    .line 382
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_SECONDARY_IGNORABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 384
    add-int/lit8 v1, v1, 0x4

    .line 385
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_SECONDARY_IGNORABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 387
    add-int/lit8 v1, v1, 0x4

    .line 388
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_PRIMARY_IGNORABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 390
    add-int/lit8 v1, v1, 0x4

    .line 391
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_PRIMARY_IGNORABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 393
    add-int/lit8 v1, v1, 0x4

    .line 394
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_VARIABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 395
    add-int/lit8 v1, v1, 0x4

    .line 396
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_VARIABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 397
    add-int/lit8 v1, v1, 0x4

    .line 398
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_VARIABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 399
    add-int/lit8 v1, v1, 0x4

    .line 400
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_VARIABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 401
    add-int/lit8 v1, v1, 0x4

    .line 402
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_NON_VARIABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 403
    add-int/lit8 v1, v1, 0x4

    .line 404
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_NON_VARIABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 405
    add-int/lit8 v1, v1, 0x4

    .line 406
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_NON_VARIABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 407
    add-int/lit8 v1, v1, 0x4

    .line 408
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_NON_VARIABLE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 409
    add-int/lit8 v1, v1, 0x4

    .line 410
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->RESET_TOP_VALUE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 411
    add-int/lit8 v1, v1, 0x4

    .line 412
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->RESET_TOP_VALUE_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 413
    add-int/lit8 v1, v1, 0x4

    .line 414
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_IMPLICIT_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 415
    add-int/lit8 v1, v1, 0x4

    .line 416
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_IMPLICIT_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 417
    add-int/lit8 v1, v1, 0x4

    .line 418
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_IMPLICIT_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 419
    add-int/lit8 v1, v1, 0x4

    .line 420
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_IMPLICIT_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 421
    add-int/lit8 v1, v1, 0x4

    .line 422
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_TRAILING_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 423
    add-int/lit8 v1, v1, 0x4

    .line 424
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_TRAILING_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 425
    add-int/lit8 v1, v1, 0x4

    .line 426
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_TRAILING_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v8

    .line 427
    add-int/lit8 v1, v1, 0x4

    .line 428
    iget-object v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_TRAILING_:[I

    iget-object v6, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    aput v6, v5, v9

    .line 429
    add-int/lit8 v1, v1, 0x4

    .line 430
    iget-object v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    iput v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->PRIMARY_TOP_MIN_:I

    .line 431
    add-int/lit8 v1, v1, 0x4

    .line 432
    iget-object v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    iput v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->PRIMARY_IMPLICIT_MIN_:I

    .line 433
    add-int/lit8 v1, v1, 0x4

    .line 434
    iget-object v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    iput v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->PRIMARY_IMPLICIT_MAX_:I

    .line 435
    add-int/lit8 v1, v1, 0x4

    .line 436
    iget-object v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    iput v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->PRIMARY_TRAILING_MIN_:I

    .line 437
    add-int/lit8 v1, v1, 0x4

    .line 438
    iget-object v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    iput v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->PRIMARY_TRAILING_MAX_:I

    .line 439
    add-int/lit8 v1, v1, 0x4

    .line 440
    iget-object v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    iput v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->PRIMARY_SPECIAL_MIN_:I

    .line 441
    add-int/lit8 v1, v1, 0x4

    .line 442
    iget-object v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    iput v5, p2, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->PRIMARY_SPECIAL_MAX_:I

    .line 443
    add-int/lit8 v1, v1, 0x4

    .line 444
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_UCAValuesSize_:I

    add-int/lit16 v5, v5, -0x94

    shr-int/lit8 v4, v5, 0x1

    .line 445
    .local v4, "resultsize":I
    new-array v3, v4, [C

    .line 446
    .local v3, "result":[C
    const/4 v0, 0x0

    :goto_7
    if-ge v0, v4, :cond_a

    .line 447
    iget-object v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readChar()C

    move-result v5

    aput-char v5, v3, v0

    .line 446
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 449
    :cond_a
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_UCAValuesSize_:I

    add-int/2addr v2, v5

    .line 450
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_size_:I

    if-eq v2, v5, :cond_d

    .line 451
    new-instance v5, Ljava/io/IOException;

    const-string/jumbo v6, "Internal Error: Data file size error"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 455
    .end local v1    # "readUCAConstcount":I
    .end local v3    # "result":[C
    .end local v4    # "resultsize":I
    :cond_b
    iget v5, p0, Lcom/ibm/icu/text/CollatorReader;->m_size_:I

    if-eq v2, v5, :cond_c

    .line 456
    new-instance v5, Ljava/io/IOException;

    const-string/jumbo v6, "Internal Error: Data file size error"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 458
    :cond_c
    const/4 v3, 0x0

    :cond_d
    return-object v3
.end method

.method private static readInverseUCA(Ljava/io/InputStream;)Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;
    .locals 10
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 472
    sget-object v8, Lcom/ibm/icu/text/CollatorReader;->INVERSE_UCA_DATA_FORMAT_ID_:[B

    sget-object v9, Lcom/ibm/icu/text/CollatorReader;->INVERSE_UCA_AUTHENTICATE_:Lcom/ibm/icu/impl/ICUBinary$Authenticate;

    invoke-static {p0, v8, v9}, Lcom/ibm/icu/impl/ICUBinary;->readHeader(Ljava/io/InputStream;[BLcom/ibm/icu/impl/ICUBinary$Authenticate;)[B

    move-result-object v1

    .line 477
    .local v1, "UnicodeVersion":[B
    invoke-static {}, Lcom/ibm/icu/lang/UCharacter;->getUnicodeVersion()Lcom/ibm/icu/util/VersionInfo;

    move-result-object v0

    .line 478
    .local v0, "UCDVersion":Lcom/ibm/icu/util/VersionInfo;
    const/4 v8, 0x0

    aget-byte v8, v1, v8

    invoke-virtual {v0}, Lcom/ibm/icu/util/VersionInfo;->getMajor()I

    move-result v9

    if-ne v8, v9, :cond_0

    const/4 v8, 0x1

    aget-byte v8, v1, v8

    invoke-virtual {v0}, Lcom/ibm/icu/util/VersionInfo;->getMinor()I

    move-result v9

    if-eq v8, v9, :cond_1

    .line 480
    :cond_0
    new-instance v8, Ljava/io/IOException;

    const-string/jumbo v9, "Unicode version in binary image is not compatible with the current Unicode version"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 483
    :cond_1
    new-instance v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;

    invoke-direct {v5}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;-><init>()V

    .line 485
    .local v5, "result":Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;
    new-instance v4, Ljava/io/DataInputStream;

    invoke-direct {v4, p0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 486
    .local v4, "input":Ljava/io/DataInputStream;
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    .line 487
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    .line 488
    .local v7, "tablesize":I
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    .line 489
    .local v2, "contsize":I
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    .line 490
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    .line 491
    invoke-static {v4}, Lcom/ibm/icu/text/CollatorReader;->readVersion(Ljava/io/DataInputStream;)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v8

    iput-object v8, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_UCA_version_:Lcom/ibm/icu/util/VersionInfo;

    .line 492
    const/16 v8, 0x8

    invoke-virtual {v4, v8}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 494
    mul-int/lit8 v6, v7, 0x3

    .line 495
    .local v6, "size":I
    new-array v8, v6, [I

    iput-object v8, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_table_:[I

    .line 496
    new-array v8, v2, [C

    iput-object v8, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_continuations_:[C

    .line 498
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v6, :cond_2

    .line 499
    iget-object v8, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_table_:[I

    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    aput v9, v8, v3

    .line 498
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 501
    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_3

    .line 502
    iget-object v8, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_continuations_:[C

    invoke-virtual {v4}, Ljava/io/DataInputStream;->readChar()C

    move-result v9

    aput-char v9, v8, v3

    .line 501
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 504
    :cond_3
    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V

    .line 505
    return-object v5
.end method

.method private readOptions(Lcom/ibm/icu/text/RuleBasedCollator;)V
    .locals 7
    .param p1, "rbc"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x11

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 242
    const/4 v0, 0x0

    .line 243
    .local v0, "readcount":I
    iget-object v2, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iput v2, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultVariableTopValue_:I

    .line 244
    add-int/lit8 v0, v0, 0x4

    .line 245
    iget-object v2, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    if-ne v2, v6, :cond_0

    move v2, v3

    :goto_0
    iput-boolean v2, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsFrenchCollation_:Z

    .line 247
    add-int/lit8 v0, v0, 0x4

    .line 248
    iget-object v2, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    const/16 v5, 0x14

    if-ne v2, v5, :cond_1

    move v2, v3

    :goto_1
    iput-boolean v2, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsAlternateHandlingShifted_:Z

    .line 251
    add-int/lit8 v0, v0, 0x4

    .line 252
    iget-object v2, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iput v2, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultCaseFirst_:I

    .line 253
    add-int/lit8 v0, v0, 0x4

    .line 254
    iget-object v2, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    if-ne v2, v6, :cond_2

    move v2, v3

    :goto_2
    iput-boolean v2, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsCaseLevel_:Z

    .line 256
    add-int/lit8 v0, v0, 0x4

    .line 257
    iget-object v2, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 258
    .local v1, "value":I
    add-int/lit8 v0, v0, 0x4

    .line 259
    if-ne v1, v6, :cond_3

    .line 260
    const/16 v1, 0x11

    .line 265
    :goto_3
    iput v1, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultDecomposition_:I

    .line 266
    iget-object v2, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iput v2, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultStrength_:I

    .line 267
    add-int/lit8 v0, v0, 0x4

    .line 268
    iget-object v2, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    if-ne v2, v6, :cond_4

    move v2, v3

    :goto_4
    iput-boolean v2, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsHiragana4_:Z

    .line 270
    add-int/lit8 v0, v0, 0x4

    .line 271
    iget-object v2, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    if-ne v2, v6, :cond_5

    :goto_5
    iput-boolean v3, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsNumericCollation_:Z

    .line 273
    add-int/lit8 v0, v0, 0x4

    .line 274
    iget-object v2, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    const-wide/16 v4, 0x3c

    invoke-virtual {v2, v4, v5}, Ljava/io/DataInputStream;->skip(J)J

    .line 275
    add-int/lit8 v0, v0, 0x3c

    .line 276
    iget-object v2, p0, Lcom/ibm/icu/text/CollatorReader;->m_dataInputStream_:Ljava/io/DataInputStream;

    iget v3, p0, Lcom/ibm/icu/text/CollatorReader;->m_optionSize_:I

    add-int/lit8 v3, v3, -0x60

    invoke-virtual {v2, v3}, Ljava/io/DataInputStream;->skipBytes(I)I

    .line 277
    iget v2, p0, Lcom/ibm/icu/text/CollatorReader;->m_optionSize_:I

    if-ge v2, v0, :cond_6

    .line 278
    new-instance v2, Ljava/io/IOException;

    const-string/jumbo v3, "Internal Error: Option size error"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .end local v1    # "value":I
    :cond_0
    move v2, v4

    .line 245
    goto :goto_0

    :cond_1
    move v2, v4

    .line 248
    goto :goto_1

    :cond_2
    move v2, v4

    .line 254
    goto :goto_2

    .line 263
    .restart local v1    # "value":I
    :cond_3
    const/16 v1, 0x10

    goto :goto_3

    :cond_4
    move v2, v4

    .line 268
    goto :goto_4

    :cond_5
    move v3, v4

    .line 271
    goto :goto_5

    .line 280
    :cond_6
    return-void
.end method

.method protected static readVersion(Ljava/io/DataInputStream;)Lcom/ibm/icu/util/VersionInfo;
    .locals 7
    .param p0, "input"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 520
    const/4 v2, 0x4

    new-array v1, v2, [B

    .line 521
    .local v1, "version":[B
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    move-result v2

    aput-byte v2, v1, v3

    .line 522
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    move-result v2

    aput-byte v2, v1, v4

    .line 523
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    move-result v2

    aput-byte v2, v1, v5

    .line 524
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readByte()B

    move-result v2

    aput-byte v2, v1, v6

    .line 526
    aget-byte v2, v1, v3

    aget-byte v3, v1, v4

    aget-byte v4, v1, v5

    aget-byte v5, v1, v6

    invoke-static {v2, v3, v4, v5}, Lcom/ibm/icu/util/VersionInfo;->getInstance(IIII)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v0

    .line 531
    .local v0, "result":Lcom/ibm/icu/util/VersionInfo;
    return-object v0
.end method
