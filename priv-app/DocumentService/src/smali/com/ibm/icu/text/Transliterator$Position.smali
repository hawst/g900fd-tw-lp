.class public Lcom/ibm/icu/text/Transliterator$Position;
.super Ljava/lang/Object;
.source "Transliterator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/Transliterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Position"
.end annotation


# instance fields
.field public contextLimit:I

.field public contextStart:I

.field public limit:I

.field public start:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 333
    invoke-direct {p0, v0, v0, v0, v0}, Lcom/ibm/icu/text/Transliterator$Position;-><init>(IIII)V

    .line 334
    return-void
.end method

.method public constructor <init>(III)V
    .locals 0
    .param p1, "contextStart"    # I
    .param p2, "contextLimit"    # I
    .param p3, "start"    # I

    .prologue
    .line 343
    invoke-direct {p0, p1, p2, p3, p2}, Lcom/ibm/icu/text/Transliterator$Position;-><init>(IIII)V

    .line 344
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "contextStart"    # I
    .param p2, "contextLimit"    # I
    .param p3, "start"    # I
    .param p4, "limit"    # I

    .prologue
    .line 352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 353
    iput p1, p0, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    .line 354
    iput p2, p0, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    .line 355
    iput p3, p0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 356
    iput p4, p0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 357
    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/text/Transliterator$Position;)V
    .locals 0
    .param p1, "pos"    # Lcom/ibm/icu/text/Transliterator$Position;

    .prologue
    .line 363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 364
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/Transliterator$Position;->set(Lcom/ibm/icu/text/Transliterator$Position;)V

    .line 365
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 383
    instance-of v2, p1, Lcom/ibm/icu/text/Transliterator$Position;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 384
    check-cast v0, Lcom/ibm/icu/text/Transliterator$Position;

    .line 385
    .local v0, "pos":Lcom/ibm/icu/text/Transliterator$Position;
    iget v2, p0, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    iget v3, v0, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    iget v3, v0, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    iget v3, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    iget v3, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 390
    .end local v0    # "pos":Lcom/ibm/icu/text/Transliterator$Position;
    :cond_0
    return v1
.end method

.method public set(Lcom/ibm/icu/text/Transliterator$Position;)V
    .locals 1
    .param p1, "pos"    # Lcom/ibm/icu/text/Transliterator$Position;

    .prologue
    .line 372
    iget v0, p1, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    iput v0, p0, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    .line 373
    iget v0, p1, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    iput v0, p0, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    .line 374
    iget v0, p1, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    iput v0, p0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 375
    iget v0, p1, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    iput v0, p0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 376
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 398
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v1, "[cs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v1, p0, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, ", s="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v1, p0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, ", l="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v1, p0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, ", cl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v1, p0, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final validate(I)V
    .locals 3
    .param p1, "length"    # I

    .prologue
    .line 413
    iget v0, p0, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    iget v1, p0, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    iget v1, p0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    iget v1, p0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    if-ge p1, v0, :cond_1

    .line 418
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Invalid Position {cs="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ", s="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ", l="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ", cl="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "}, len="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 425
    :cond_1
    return-void
.end method
