.class public Lcom/ibm/icu/text/CharsetMatch;
.super Ljava/lang/Object;
.source "CharsetMatch.java"

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field public static final BOM:I = 0x2

.field public static final DECLARED_ENCODING:I = 0x4

.field public static final ENCODING_SCHEME:I = 0x1

.field public static final LANG_STATISTICS:I = 0x8


# instance fields
.field private fConfidence:I

.field private fInputStream:Ljava/io/InputStream;

.field private fRawInput:[B

.field private fRawLength:I

.field private fRecognizer:Lcom/ibm/icu/text/CharsetRecognizer;


# direct methods
.method constructor <init>(Lcom/ibm/icu/text/CharsetDetector;Lcom/ibm/icu/text/CharsetRecognizer;I)V
    .locals 1
    .param p1, "det"    # Lcom/ibm/icu/text/CharsetDetector;
    .param p2, "rec"    # Lcom/ibm/icu/text/CharsetRecognizer;
    .param p3, "conf"    # I

    .prologue
    const/4 v0, 0x0

    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 257
    iput-object v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fRawInput:[B

    .line 261
    iput-object v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fInputStream:Ljava/io/InputStream;

    .line 236
    iput-object p2, p0, Lcom/ibm/icu/text/CharsetMatch;->fRecognizer:Lcom/ibm/icu/text/CharsetRecognizer;

    .line 237
    iput p3, p0, Lcom/ibm/icu/text/CharsetMatch;->fConfidence:I

    .line 242
    iget-object v0, p1, Lcom/ibm/icu/text/CharsetDetector;->fInputStream:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 245
    iget-object v0, p1, Lcom/ibm/icu/text/CharsetDetector;->fRawInput:[B

    iput-object v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fRawInput:[B

    .line 246
    iget v0, p1, Lcom/ibm/icu/text/CharsetDetector;->fRawLength:I

    iput v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fRawLength:I

    .line 248
    :cond_0
    iget-object v0, p1, Lcom/ibm/icu/text/CharsetDetector;->fInputStream:Ljava/io/InputStream;

    iput-object v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fInputStream:Ljava/io/InputStream;

    .line 249
    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 222
    move-object v1, p1

    check-cast v1, Lcom/ibm/icu/text/CharsetMatch;

    .line 223
    .local v1, "other":Lcom/ibm/icu/text/CharsetMatch;
    const/4 v0, 0x0

    .line 224
    .local v0, "compareResult":I
    iget v2, p0, Lcom/ibm/icu/text/CharsetMatch;->fConfidence:I

    iget v3, v1, Lcom/ibm/icu/text/CharsetMatch;->fConfidence:I

    if-le v2, v3, :cond_1

    .line 225
    const/4 v0, 0x1

    .line 229
    :cond_0
    :goto_0
    return v0

    .line 226
    :cond_1
    iget v2, p0, Lcom/ibm/icu/text/CharsetMatch;->fConfidence:I

    iget v3, v1, Lcom/ibm/icu/text/CharsetMatch;->fConfidence:I

    if-ge v2, v3, :cond_0

    .line 227
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getConfidence()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fConfidence:I

    return v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fRecognizer:Lcom/ibm/icu/text/CharsetRecognizer;

    invoke-virtual {v0}, Lcom/ibm/icu/text/CharsetRecognizer;->getLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMatchType()I
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/ibm/icu/text/CharsetMatch;->fRecognizer:Lcom/ibm/icu/text/CharsetRecognizer;

    invoke-virtual {v0}, Lcom/ibm/icu/text/CharsetRecognizer;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReader()Ljava/io/Reader;
    .locals 5

    .prologue
    .line 46
    iget-object v1, p0, Lcom/ibm/icu/text/CharsetMatch;->fInputStream:Ljava/io/InputStream;

    .line 48
    .local v1, "inputStream":Ljava/io/InputStream;
    if-nez v1, :cond_0

    .line 49
    new-instance v1, Ljava/io/ByteArrayInputStream;

    .end local v1    # "inputStream":Ljava/io/InputStream;
    iget-object v2, p0, Lcom/ibm/icu/text/CharsetMatch;->fRawInput:[B

    const/4 v3, 0x0

    iget v4, p0, Lcom/ibm/icu/text/CharsetMatch;->fRawLength:I

    invoke-direct {v1, v2, v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 53
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Ljava/io/InputStream;->reset()V

    .line 54
    new-instance v2, Ljava/io/InputStreamReader;

    invoke-virtual {p0}, Lcom/ibm/icu/text/CharsetMatch;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :goto_0
    return-object v2

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/CharsetMatch;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 9
    .param p1, "maxLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x400

    const/4 v7, 0x0

    .line 88
    const/4 v4, 0x0

    .line 89
    .local v4, "result":Ljava/lang/String;
    iget-object v6, p0, Lcom/ibm/icu/text/CharsetMatch;->fInputStream:Ljava/io/InputStream;

    if-eqz v6, :cond_2

    .line 90
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 91
    .local v5, "sb":Ljava/lang/StringBuffer;
    new-array v0, v8, [C

    .line 92
    .local v0, "buffer":[C
    invoke-virtual {p0}, Lcom/ibm/icu/text/CharsetMatch;->getReader()Ljava/io/Reader;

    move-result-object v3

    .line 93
    .local v3, "reader":Ljava/io/Reader;
    if-gez p1, :cond_0

    const v2, 0x7fffffff

    .line 94
    .local v2, "max":I
    :goto_0
    const/4 v1, 0x0

    .line 96
    .local v1, "bytesRead":I
    :goto_1
    invoke-static {v2, v8}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v3, v0, v7, v6}, Ljava/io/Reader;->read([CII)I

    move-result v1

    if-ltz v1, :cond_1

    .line 97
    invoke-virtual {v5, v0, v7, v1}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 98
    sub-int/2addr v2, v1

    .line 99
    goto :goto_1

    .end local v1    # "bytesRead":I
    .end local v2    # "max":I
    :cond_0
    move v2, p1

    .line 93
    goto :goto_0

    .line 101
    .restart local v1    # "bytesRead":I
    .restart local v2    # "max":I
    :cond_1
    invoke-virtual {v3}, Ljava/io/Reader;->close()V

    .line 103
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    .line 107
    .end local v0    # "buffer":[C
    .end local v1    # "bytesRead":I
    .end local v2    # "max":I
    .end local v3    # "reader":Ljava/io/Reader;
    .end local v5    # "sb":Ljava/lang/StringBuffer;
    :goto_2
    return-object v6

    .line 105
    :cond_2
    new-instance v4, Ljava/lang/String;

    .end local v4    # "result":Ljava/lang/String;
    iget-object v6, p0, Lcom/ibm/icu/text/CharsetMatch;->fRawInput:[B

    invoke-virtual {p0}, Lcom/ibm/icu/text/CharsetMatch;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .restart local v4    # "result":Ljava/lang/String;
    move-object v6, v4

    .line 107
    goto :goto_2
.end method
