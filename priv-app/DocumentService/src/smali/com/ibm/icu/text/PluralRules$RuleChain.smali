.class Lcom/ibm/icu/text/PluralRules$RuleChain;
.super Ljava/lang/Object;
.source "PluralRules.java"

# interfaces
.implements Lcom/ibm/icu/text/PluralRules$RuleList;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/PluralRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RuleChain"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final next:Lcom/ibm/icu/text/PluralRules$RuleChain;

.field private final rule:Lcom/ibm/icu/text/PluralRules$Rule;


# direct methods
.method public constructor <init>(Lcom/ibm/icu/text/PluralRules$Rule;)V
    .locals 1
    .param p1, "rule"    # Lcom/ibm/icu/text/PluralRules$Rule;

    .prologue
    .line 604
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/PluralRules$RuleChain;-><init>(Lcom/ibm/icu/text/PluralRules$Rule;Lcom/ibm/icu/text/PluralRules$RuleChain;)V

    .line 605
    return-void
.end method

.method private constructor <init>(Lcom/ibm/icu/text/PluralRules$Rule;Lcom/ibm/icu/text/PluralRules$RuleChain;)V
    .locals 0
    .param p1, "rule"    # Lcom/ibm/icu/text/PluralRules$Rule;
    .param p2, "next"    # Lcom/ibm/icu/text/PluralRules$RuleChain;

    .prologue
    .line 607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 608
    iput-object p1, p0, Lcom/ibm/icu/text/PluralRules$RuleChain;->rule:Lcom/ibm/icu/text/PluralRules$Rule;

    .line 609
    iput-object p2, p0, Lcom/ibm/icu/text/PluralRules$RuleChain;->next:Lcom/ibm/icu/text/PluralRules$RuleChain;

    .line 610
    return-void
.end method

.method private selectRule(D)Lcom/ibm/icu/text/PluralRules$Rule;
    .locals 3
    .param p1, "n"    # D

    .prologue
    .line 617
    const/4 v0, 0x0

    .line 618
    .local v0, "r":Lcom/ibm/icu/text/PluralRules$Rule;
    iget-object v1, p0, Lcom/ibm/icu/text/PluralRules$RuleChain;->next:Lcom/ibm/icu/text/PluralRules$RuleChain;

    if-eqz v1, :cond_0

    .line 619
    iget-object v1, p0, Lcom/ibm/icu/text/PluralRules$RuleChain;->next:Lcom/ibm/icu/text/PluralRules$RuleChain;

    invoke-direct {v1, p1, p2}, Lcom/ibm/icu/text/PluralRules$RuleChain;->selectRule(D)Lcom/ibm/icu/text/PluralRules$Rule;

    move-result-object v0

    .line 621
    :cond_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/ibm/icu/text/PluralRules$RuleChain;->rule:Lcom/ibm/icu/text/PluralRules$Rule;

    invoke-interface {v1, p1, p2}, Lcom/ibm/icu/text/PluralRules$Rule;->appliesTo(D)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 622
    iget-object v0, p0, Lcom/ibm/icu/text/PluralRules$RuleChain;->rule:Lcom/ibm/icu/text/PluralRules$Rule;

    .line 624
    :cond_1
    return-object v0
.end method


# virtual methods
.method public addRule(Lcom/ibm/icu/text/PluralRules$Rule;)Lcom/ibm/icu/text/PluralRules$RuleChain;
    .locals 1
    .param p1, "nextRule"    # Lcom/ibm/icu/text/PluralRules$Rule;

    .prologue
    .line 613
    new-instance v0, Lcom/ibm/icu/text/PluralRules$RuleChain;

    invoke-direct {v0, p1, p0}, Lcom/ibm/icu/text/PluralRules$RuleChain;-><init>(Lcom/ibm/icu/text/PluralRules$Rule;Lcom/ibm/icu/text/PluralRules$RuleChain;)V

    return-object v0
.end method

.method public getKeywords()Ljava/util/Set;
    .locals 3

    .prologue
    .line 636
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 637
    .local v1, "result":Ljava/util/Set;
    const-string/jumbo v2, "other"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 638
    move-object v0, p0

    .line 639
    .local v0, "rc":Lcom/ibm/icu/text/PluralRules$RuleChain;
    :goto_0
    if-eqz v0, :cond_0

    .line 640
    iget-object v2, v0, Lcom/ibm/icu/text/PluralRules$RuleChain;->rule:Lcom/ibm/icu/text/PluralRules$Rule;

    invoke-interface {v2}, Lcom/ibm/icu/text/PluralRules$Rule;->getKeyword()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 641
    iget-object v0, v0, Lcom/ibm/icu/text/PluralRules$RuleChain;->next:Lcom/ibm/icu/text/PluralRules$RuleChain;

    .line 642
    goto :goto_0

    .line 643
    :cond_0
    return-object v1
.end method

.method public getRepeatLimit()I
    .locals 3

    .prologue
    .line 647
    const/4 v1, 0x0

    .line 648
    .local v1, "result":I
    move-object v0, p0

    .line 649
    .local v0, "rc":Lcom/ibm/icu/text/PluralRules$RuleChain;
    :goto_0
    if-eqz v0, :cond_0

    .line 650
    iget-object v2, v0, Lcom/ibm/icu/text/PluralRules$RuleChain;->rule:Lcom/ibm/icu/text/PluralRules$Rule;

    invoke-interface {v2, v1}, Lcom/ibm/icu/text/PluralRules$Rule;->updateRepeatLimit(I)I

    move-result v1

    .line 651
    iget-object v0, v0, Lcom/ibm/icu/text/PluralRules$RuleChain;->next:Lcom/ibm/icu/text/PluralRules$RuleChain;

    .line 652
    goto :goto_0

    .line 653
    :cond_0
    return v1
.end method

.method public select(D)Ljava/lang/String;
    .locals 3
    .param p1, "n"    # D

    .prologue
    .line 628
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/PluralRules$RuleChain;->selectRule(D)Lcom/ibm/icu/text/PluralRules$Rule;

    move-result-object v0

    .line 629
    .local v0, "r":Lcom/ibm/icu/text/PluralRules$Rule;
    if-nez v0, :cond_0

    .line 630
    const-string/jumbo v1, "other"

    .line 632
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0}, Lcom/ibm/icu/text/PluralRules$Rule;->getKeyword()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 657
    iget-object v1, p0, Lcom/ibm/icu/text/PluralRules$RuleChain;->rule:Lcom/ibm/icu/text/PluralRules$Rule;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 658
    .local v0, "s":Ljava/lang/String;
    iget-object v1, p0, Lcom/ibm/icu/text/PluralRules$RuleChain;->next:Lcom/ibm/icu/text/PluralRules$RuleChain;

    if-eqz v1, :cond_0

    .line 659
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lcom/ibm/icu/text/PluralRules$RuleChain;->next:Lcom/ibm/icu/text/PluralRules$RuleChain;

    invoke-virtual {v2}, Lcom/ibm/icu/text/PluralRules$RuleChain;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 661
    :cond_0
    return-object v0
.end method
