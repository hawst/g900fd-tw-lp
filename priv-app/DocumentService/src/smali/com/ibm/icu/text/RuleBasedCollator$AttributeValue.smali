.class interface abstract Lcom/ibm/icu/text/RuleBasedCollator$AttributeValue;
.super Ljava/lang/Object;
.source "RuleBasedCollator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/RuleBasedCollator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "AttributeValue"
.end annotation


# static fields
.field public static final CE_STRENGTH_LIMIT_:I = 0x3

.field public static final DEFAULT_:I = -0x1

.field public static final DEFAULT_STRENGTH_:I = 0x2

.field public static final IDENTICAL_:I = 0xf

.field public static final LIMIT_:I = 0x1d

.field public static final LOWER_FIRST_:I = 0x18

.field public static final NON_IGNORABLE_:I = 0x15

.field public static final OFF_:I = 0x10

.field public static final ON_:I = 0x11

.field public static final PRIMARY_:I = 0x0

.field public static final QUATERNARY_:I = 0x3

.field public static final SECONDARY_:I = 0x1

.field public static final SHIFTED_:I = 0x14

.field public static final STRENGTH_LIMIT_:I = 0x10

.field public static final TERTIARY_:I = 0x2

.field public static final UPPER_FIRST_:I = 0x19
