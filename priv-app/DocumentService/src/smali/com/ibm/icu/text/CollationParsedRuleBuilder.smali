.class final Lcom/ibm/icu/text/CollationParsedRuleBuilder;
.super Ljava/lang/Object;
.source "CollationParsedRuleBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;,
        Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;,
        Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;,
        Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;,
        Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;,
        Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;,
        Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;,
        Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;,
        Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;,
        Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;
    }
.end annotation


# static fields
.field private static final CE_BASIC_STRENGTH_LIMIT_:I = 0x3

.field private static final CE_CONTRACTION_TAG_:I = 0x2

.field private static final CE_EXPANSION_TAG_:I = 0x1

.field private static final CE_IMPLICIT_TAG_:I = 0xa

.field private static final CE_LONG_PRIMARY_TAG_:I = 0xc

.field private static final CE_NOT_FOUND_:I = -0x10000000

.field private static final CE_NOT_FOUND_TAG_:I = 0x0

.field private static final CE_SPEC_PROC_TAG_:I = 0xb

.field private static final CE_STRENGTH_LIMIT_:I = 0x10

.field private static final CE_SURROGATE_TAG_:I = 0x5

.field private static final CONTRACTION_TABLE_NEW_ELEMENT_:I = 0xffffff

.field static final INVERSE_UCA_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;

.field private static final INV_UCA_VERSION_MISMATCH_:Ljava/lang/String; = "UCA versions of UCA and inverse UCA should match"

.field private static final LOWER_CASE_:I = 0x0

.field private static final MIXED_CASE_:I = 0x40

.field private static final STRENGTH_MASK_:[I

.field private static final UCA_NOT_INSTANTIATED_:Ljava/lang/String; = "UCA is not instantiated!"

.field private static final UNSAFECP_TABLE_MASK_:I = 0x1fff

.field private static final UNSAFECP_TABLE_SIZE_:I = 0x420

.field private static final UPPER_CASE_:I = 0x80

.field private static buildCMTabFlag:Z


# instance fields
.field private m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

.field private m_utilCEBuffer_:[I

.field private m_utilCanIter_:Lcom/ibm/icu/text/CanonicalIterator;

.field private m_utilCharBuffer_:[C

.field private m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

.field private m_utilCountBuffer_:[I

.field private m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

.field private m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

.field private m_utilGens_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

.field private m_utilIntBuffer_:[I

.field private m_utilLongBuffer_:[J

.field private m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

.field private m_utilStringBuffer_:Ljava/lang/StringBuffer;

.field private m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

.field private m_utilUpperWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

.field private m_utilWeightRange_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 416
    const/4 v0, 0x0

    .line 418
    .local v0, "temp":Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;
    :try_start_0
    invoke-static {}, Lcom/ibm/icu/text/CollatorReader;->getInverseUCA()Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 438
    :goto_0
    if-eqz v0, :cond_0

    sget-object v1, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    if-eqz v1, :cond_0

    .line 439
    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_UCA_version_:Lcom/ibm/icu/util/VersionInfo;

    sget-object v2, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v2, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_UCA_version_:Lcom/ibm/icu/util/VersionInfo;

    invoke-virtual {v1, v2}, Lcom/ibm/icu/util/VersionInfo;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 440
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "UCA versions of UCA and inverse UCA should match"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 443
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "UCA is not instantiated!"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 446
    :cond_1
    sput-object v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->INVERSE_UCA_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;

    .line 1264
    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    sput-object v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->STRENGTH_MASK_:[I

    .line 1381
    const/4 v1, 0x0

    sput-boolean v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->buildCMTabFlag:Z

    return-void

    .line 419
    :catch_0
    move-exception v1

    goto :goto_0

    .line 1264
    nop

    :array_0
    .array-data 4
        -0x10000
        -0x100
        -0x1
    .end array-data
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 8
    .param p1, "rules"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x3

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1358
    new-array v0, v3, [Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    new-instance v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    invoke-direct {v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;-><init>()V

    aput-object v1, v0, v4

    new-instance v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    invoke-direct {v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;-><init>()V

    aput-object v1, v0, v5

    new-instance v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    invoke-direct {v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;-><init>()V

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilGens_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    .line 1360
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    .line 1361
    const/16 v0, 0x10

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    .line 1362
    new-instance v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-direct {v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .line 1363
    new-instance v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-direct {v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .line 1364
    new-instance v0, Lcom/ibm/icu/text/CollationRuleParser$Token;

    invoke-direct {v0}, Lcom/ibm/icu/text/CollationRuleParser$Token;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1366
    const/4 v0, 0x6

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCountBuffer_:[I

    .line 1367
    new-array v0, v7, [J

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLongBuffer_:[J

    .line 1368
    new-array v0, v7, [Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    new-instance v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    invoke-direct {v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;-><init>()V

    aput-object v1, v0, v4

    new-instance v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    invoke-direct {v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;-><init>()V

    aput-object v1, v0, v5

    new-instance v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    invoke-direct {v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;-><init>()V

    aput-object v1, v0, v6

    new-instance v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    invoke-direct {v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;-><init>()V

    aput-object v1, v0, v3

    const/4 v1, 0x4

    new-instance v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    invoke-direct {v2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    .line 1372
    new-array v0, v7, [Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    new-instance v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    invoke-direct {v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;-><init>()V

    aput-object v1, v0, v4

    new-instance v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    invoke-direct {v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;-><init>()V

    aput-object v1, v0, v5

    new-instance v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    invoke-direct {v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;-><init>()V

    aput-object v1, v0, v6

    new-instance v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    invoke-direct {v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;-><init>()V

    aput-object v1, v0, v3

    const/4 v1, 0x4

    new-instance v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    invoke-direct {v2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilUpperWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    .line 1376
    new-instance v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    invoke-direct {v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilWeightRange_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    .line 1377
    const/16 v0, 0x100

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCharBuffer_:[C

    .line 1378
    new-instance v0, Lcom/ibm/icu/text/CanonicalIterator;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/CanonicalIterator;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCanIter_:Lcom/ibm/icu/text/CanonicalIterator;

    .line 1379
    new-instance v0, Ljava/lang/StringBuffer;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    .line 45
    new-instance v0, Lcom/ibm/icu/text/CollationRuleParser;

    invoke-direct {v0, p1}, Lcom/ibm/icu/text/CollationRuleParser;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    .line 46
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    invoke-virtual {v0}, Lcom/ibm/icu/text/CollationRuleParser;->assembleTokenList()I

    .line 47
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->getCollationElementIterator(Ljava/lang/String;)Lcom/ibm/icu/text/CollationElementIterator;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    .line 49
    return-void
.end method

.method private static final ContrEndCPSet([BC)V
    .locals 5
    .param p0, "table"    # [B
    .param p1, "c"    # C

    .prologue
    .line 2583
    move v0, p1

    .line 2584
    .local v0, "hash":I
    const/16 v1, 0x2100

    if-lt v0, v1, :cond_0

    .line 2585
    and-int/lit16 v1, v0, 0x1fff

    add-int/lit16 v0, v1, 0x100

    .line 2587
    :cond_0
    shr-int/lit8 v1, v0, 0x3

    aget-byte v2, p0, v1

    const/4 v3, 0x1

    and-int/lit8 v4, v0, 0x7

    shl-int/2addr v3, v4

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, p0, v1

    .line 2588
    return-void
.end method

.method static access$000()[I
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->STRENGTH_MASK_:[I

    return-object v0
.end method

.method static access$100(III)I
    .locals 1
    .param p0, "x0"    # I
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 33
    invoke-static {p0, p1, p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->mergeCE(III)I

    move-result v0

    return v0
.end method

.method static access$200(I)I
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 33
    invoke-static {p0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCETag(I)I

    move-result v0

    return v0
.end method

.method static access$300(I)Z
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 33
    invoke-static {p0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isSpecial(I)Z

    move-result v0

    return v0
.end method

.method private static addAContractionElement(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;)Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;
    .locals 2
    .param p0, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    .prologue
    .line 2621
    new-instance v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    invoke-direct {v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;-><init>()V

    .line 2622
    .local v0, "result":Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;
    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_elements_:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 2623
    return-object v0
.end method

.method private addAnElement(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I
    .locals 13
    .param p1, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
    .param p2, "element"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .prologue
    const/4 v8, 0x5

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2077
    iget-object v1, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_expansions_:Ljava/util/Vector;

    .line 2078
    .local v1, "expansions":Ljava/util/Vector;
    iput v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 2080
    iget v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    if-ne v6, v11, :cond_7

    .line 2081
    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v6, v6, v10

    iput v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 2147
    :cond_0
    :goto_0
    const/4 v5, 0x0

    .line 2148
    .local v5, "uniChar":I
    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v6, v12, :cond_b

    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    invoke-virtual {v6, v10}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2150
    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    invoke-virtual {v6, v10}, Ljava/lang/String;->charAt(I)C

    move-result v6

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    invoke-virtual {v7, v11}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v6, v7}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v5

    .line 2165
    :cond_1
    :goto_1
    if-eqz v5, :cond_2

    invoke-static {v5}, Lcom/ibm/icu/lang/UCharacter;->isDigit(I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2167
    const v0, -0x2ffffff

    .line 2170
    .local v0, "expansion":I
    iget v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    if-eqz v6, :cond_c

    .line 2172
    iget v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    invoke-static {v1, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addExpansion(Ljava/util/Vector;I)I

    move-result v6

    shl-int/lit8 v6, v6, 0x4

    or-int/2addr v0, v6

    .line 2177
    :goto_2
    iput v0, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 2184
    .end local v0    # "expansion":I
    :cond_2
    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    if-eqz v6, :cond_4

    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    iget v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    sub-int/2addr v6, v7

    if-lez v6, :cond_4

    .line 2189
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-boolean v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_caseBit_:Z

    iput-boolean v7, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_caseBit_:Z

    .line 2190
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    iput v7, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 2191
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    iput-object v7, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    .line 2192
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    iput v7, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 2194
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizePrim_:[I

    iput-object v7, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizePrim_:[I

    .line 2195
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizeSec_:[I

    iput-object v7, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizeSec_:[I

    .line 2196
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizeTer_:[I

    iput-object v7, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizeTer_:[I

    .line 2197
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-boolean v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_variableTop_:Z

    iput-boolean v7, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_variableTop_:Z

    .line 2198
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    iput v7, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 2199
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    invoke-static {v7, v10}, Lcom/ibm/icu/text/Normalizer;->compose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 2200
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    iput-object v7, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    .line 2201
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iput-object v7, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 2202
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iput v10, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    .line 2204
    iget-object v6, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_prefixLookup_:Ljava/util/Hashtable;

    if-eqz v6, :cond_4

    .line 2205
    iget-object v6, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_prefixLookup_:Ljava/util/Hashtable;

    invoke-virtual {v6, p2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .line 2206
    .local v4, "uCE":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    if-eqz v4, :cond_d

    .line 2208
    iget v6, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    invoke-direct {p0, p1, v6, p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addPrefix(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;ILcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    move-result v6

    iput v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 2216
    :goto_3
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v6, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    iget v8, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    sub-int/2addr v7, v8

    if-ne v6, v7, :cond_3

    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v6, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    iget v8, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    iget-object v9, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v9, v9, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v6, v10, v7, v8, v9}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v6

    if-nez v6, :cond_4

    .line 2222
    :cond_3
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement2_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-direct {p0, p1, v7, v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addPrefix(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;ILcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    move-result v7

    iput v7, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 2231
    .end local v4    # "uCE":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    :cond_4
    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    iget v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    sub-int/2addr v6, v7

    if-le v6, v11, :cond_f

    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    iget v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    sub-int/2addr v6, v7

    if-ne v6, v12, :cond_5

    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v6, v10}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v6, v11}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v6

    if-nez v6, :cond_f

    .line 2237
    :cond_5
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCanIter_:Lcom/ibm/icu/text/CanonicalIterator;

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/ibm/icu/text/CanonicalIterator;->setSource(Ljava/lang/String;)V

    .line 2238
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCanIter_:Lcom/ibm/icu/text/CanonicalIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/CanonicalIterator;->next()Ljava/lang/String;

    move-result-object v3

    .line 2239
    .local v3, "source":Ljava/lang/String;
    :goto_4
    if-eqz v3, :cond_e

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_e

    .line 2240
    sget-object v6, Lcom/ibm/icu/text/Normalizer;->FCD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {v3, v6, v10}, Lcom/ibm/icu/text/Normalizer;->quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v6

    sget-object v7, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-eq v6, v7, :cond_6

    .line 2242
    iput-object v3, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    .line 2243
    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    iput-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 2244
    invoke-static {p1, p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->finalizeAddition(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    .line 2246
    :cond_6
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCanIter_:Lcom/ibm/icu/text/CanonicalIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/CanonicalIterator;->next()Ljava/lang/String;

    move-result-object v3

    .line 2247
    goto :goto_4

    .line 2093
    .end local v3    # "source":Ljava/lang/String;
    .end local v5    # "uniChar":I
    :cond_7
    iget v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    if-ne v6, v12, :cond_8

    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v6, v6, v11

    invoke-static {v6}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v6, v6, v11

    const v7, 0xffff3f

    and-int/2addr v6, v7

    if-nez v6, :cond_8

    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v6, v6, v10

    shr-int/lit8 v6, v6, 0x8

    and-int/lit16 v6, v6, 0xff

    if-ne v6, v8, :cond_8

    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v6, v6, v10

    and-int/lit16 v6, v6, 0xff

    if-ne v6, v8, :cond_8

    .line 2104
    const/high16 v6, -0x4000000

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v7, v7, v10

    shr-int/lit8 v7, v7, 0x8

    const v8, 0xffff00

    and-int/2addr v7, v8

    or-int/2addr v6, v7

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v7, v7, v11

    shr-int/lit8 v7, v7, 0x18

    and-int/lit16 v7, v7, 0xff

    or-int/2addr v6, v7

    iput v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    goto/16 :goto_0

    .line 2115
    :cond_8
    const/high16 v6, -0xf000000

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v7, v7, v10

    invoke-static {v1, v7}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addExpansion(Ljava/util/Vector;I)I

    move-result v7

    shl-int/lit8 v7, v7, 0x4

    const v8, 0xfffff0

    and-int/2addr v7, v8

    or-int v0, v6, v7

    .line 2121
    .restart local v0    # "expansion":I
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_5
    iget v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    if-ge v2, v6, :cond_9

    .line 2122
    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v6, v6, v2

    invoke-static {v1, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addExpansion(Ljava/util/Vector;I)I

    .line 2121
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 2124
    :cond_9
    iget v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    const/16 v7, 0xf

    if-gt v6, v7, :cond_a

    .line 2125
    iget v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    or-int/2addr v0, v6

    .line 2130
    :goto_6
    iput v0, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 2131
    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    iget v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    add-int/lit8 v7, v7, -0x1

    aget v6, v6, v7

    iget v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    int-to-byte v7, v7

    iget-object v8, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_maxExpansions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;

    invoke-static {v6, v7, v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setMaxExpansion(IBLcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;)I

    .line 2134
    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v6, v10}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isJamo(C)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2135
    iget-object v6, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iput-boolean v11, v6, Lcom/ibm/icu/text/RuleBasedCollator;->m_isJamoSpecial_:Z

    .line 2136
    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v6, v10}, Ljava/lang/String;->charAt(I)C

    move-result v6

    iget-object v7, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    iget v8, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    add-int/lit8 v8, v8, -0x1

    aget v7, v7, v8

    iget v8, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    int-to-byte v8, v8

    iget-object v9, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_maxJamoExpansions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;

    invoke-static {v6, v7, v8, v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setMaxJamoExpansion(CIBLcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;)I

    goto/16 :goto_0

    .line 2128
    :cond_a
    invoke-static {v1, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addExpansion(Ljava/util/Vector;I)I

    goto :goto_6

    .line 2154
    .end local v0    # "expansion":I
    .end local v2    # "i":I
    .restart local v5    # "uniChar":I
    :cond_b
    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v6, v11, :cond_1

    .line 2155
    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    invoke-virtual {v6, v10}, Ljava/lang/String;->charAt(I)C

    move-result v5

    goto/16 :goto_1

    .line 2175
    .restart local v0    # "expansion":I
    :cond_c
    iget-object v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v6, v6, v10

    invoke-static {v1, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addExpansion(Ljava/util/Vector;I)I

    move-result v6

    shl-int/lit8 v6, v6, 0x4

    or-int/2addr v0, v6

    goto/16 :goto_2

    .line 2211
    .end local v0    # "expansion":I
    .restart local v4    # "uCE":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    :cond_d
    const/high16 v6, -0x10000000

    invoke-direct {p0, p1, v6, p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addPrefix(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;ILcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    move-result v6

    iput v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 2212
    new-instance v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .end local v4    # "uCE":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    invoke-direct {v4, p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;-><init>(Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)V

    .line 2213
    .restart local v4    # "uCE":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    iget-object v6, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    iput-object v6, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 2214
    iget-object v6, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_prefixLookup_:Ljava/util/Hashtable;

    invoke-virtual {v6, v4, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 2249
    .end local v4    # "uCE":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    .restart local v3    # "source":Ljava/lang/String;
    :cond_e
    iget v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 2252
    .end local v3    # "source":Ljava/lang/String;
    :goto_7
    return v6

    :cond_f
    invoke-static {p1, p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->finalizeAddition(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    move-result v6

    goto :goto_7
.end method

.method private static addContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;ILcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I
    .locals 12
    .param p0, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
    .param p1, "CE"    # I
    .param p2, "element"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .prologue
    .line 2950
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contractions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    .line 2951
    .local v0, "contractions":Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;
    const/4 v9, 0x2

    iput v9, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_currentTag_:I

    .line 2954
    iget-object v9, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v1

    .line 2955
    .local v1, "cp":I
    const/4 v2, 0x1

    .line 2956
    .local v2, "cpsize":I
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->isSupplementary(I)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2957
    const/4 v2, 0x2

    .line 2959
    :cond_0
    iget-object v9, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v2, v9, :cond_7

    .line 2962
    iget-object v9, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    iget v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    sub-int v8, v9, v10

    .line 2963
    .local v8, "size":I
    const/4 v5, 0x1

    .local v5, "j":I
    :goto_0
    if-ge v5, v8, :cond_2

    .line 2967
    iget-object v9, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    add-int/2addr v10, v5

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v9}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v9

    if-nez v9, :cond_1

    .line 2969
    iget-object v9, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    iget-object v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v11, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    add-int/2addr v11, v5

    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-static {v9, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->unsafeCPSet([BC)V

    .line 2963
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2977
    :cond_2
    iget-object v9, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget-object v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v9}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v9

    if-nez v9, :cond_3

    .line 2979
    iget-object v9, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contrEndCP_:[B

    iget-object v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget-object v11, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-static {v9, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->ContrEndCPSet([BC)V

    .line 2986
    :cond_3
    iget-object v9, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isJamo(C)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2987
    iget-object v9, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    const/4 v10, 0x1

    iput-boolean v10, v9, Lcom/ibm/icu/text/RuleBasedCollator;->m_isJamoSpecial_:Z

    .line 2991
    :cond_4
    iget v9, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    add-int/2addr v9, v2

    iput v9, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    .line 2992
    invoke-static {p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isContraction(I)Z

    move-result v9

    if-nez v9, :cond_5

    .line 2994
    const v9, 0xffffff

    const/4 v10, 0x0

    invoke-static {v0, v9, v10, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    move-result v4

    .line 2996
    .local v4, "firstContractionOffset":I
    const/high16 v9, -0x10000000

    invoke-static {v0, p2, v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->processContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;I)I

    move-result v6

    .line 2998
    .local v6, "newCE":I
    iget-object v9, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v0, v4, v9, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    .line 3001
    const v9, 0xffff

    invoke-static {v0, v4, v9, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    .line 3003
    const/4 v9, 0x2

    invoke-static {v9, v4}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->constructSpecialCE(II)I

    move-result p1

    .line 3031
    .end local v4    # "firstContractionOffset":I
    :goto_1
    iget v9, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    sub-int/2addr v9, v2

    iput v9, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    .line 3032
    iget-object v9, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    invoke-virtual {v9, v1, p1}, Lcom/ibm/icu/impl/IntTrieBuilder;->setValue(II)Z

    .line 3044
    .end local v5    # "j":I
    .end local v6    # "newCE":I
    .end local v8    # "size":I
    :goto_2
    return p1

    .line 3012
    .restart local v5    # "j":I
    .restart local v8    # "size":I
    :cond_5
    iget-object v9, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v0, p1, v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->findCP(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;IC)I

    move-result v7

    .line 3014
    .local v7, "position":I
    if-lez v7, :cond_6

    .line 3016
    invoke-static {v0, p1, v7}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCE(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;II)I

    move-result v3

    .line 3017
    .local v3, "eCE":I
    invoke-static {v0, p2, v3}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->processContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;I)I

    move-result v6

    .line 3018
    .restart local v6    # "newCE":I
    iget-object v9, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v0, p1, v7, v9, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;IICI)I

    goto :goto_1

    .line 3024
    .end local v3    # "eCE":I
    .end local v6    # "newCE":I
    :cond_6
    const/high16 v9, -0x10000000

    invoke-static {v0, p2, v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->processContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;I)I

    move-result v6

    .line 3026
    .restart local v6    # "newCE":I
    iget-object v9, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v0, p1, v9, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->insertContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    goto :goto_1

    .line 3034
    .end local v5    # "j":I
    .end local v6    # "newCE":I
    .end local v7    # "position":I
    .end local v8    # "size":I
    :cond_7
    invoke-static {p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isContraction(I)Z

    move-result v9

    if-nez v9, :cond_8

    .line 3036
    iget-object v9, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    iget v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    invoke-virtual {v9, v1, v10}, Lcom/ibm/icu/impl/IntTrieBuilder;->setValue(II)Z

    goto :goto_2

    .line 3041
    :cond_8
    const/4 v9, 0x0

    iget v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    invoke-static {v0, p1, v9, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->changeContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    .line 3042
    const v9, 0xffff

    iget v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    invoke-static {v0, p1, v9, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->changeContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    goto :goto_2
.end method

.method private static addContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I
    .locals 3
    .param p0, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;
    .param p1, "element"    # I
    .param p2, "codePoint"    # C
    .param p3, "value"    # I

    .prologue
    .line 2602
    invoke-static {p0, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getBasicContractionTable(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;I)Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    move-result-object v0

    .line 2603
    .local v0, "tbl":Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;
    if-nez v0, :cond_0

    .line 2604
    invoke-static {p0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addAContractionElement(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;)Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    move-result-object v0

    .line 2605
    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_elements_:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    add-int/lit8 p1, v1, -0x1

    .line 2608
    :cond_0
    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_CEs_:Ljava/util/Vector;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 2609
    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2610
    iget v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_currentTag_:I

    invoke-static {v1, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->constructSpecialCE(II)I

    move-result v1

    return v1
.end method

.method private static final addExpansion(Ljava/util/Vector;I)I
    .locals 1
    .param p0, "expansions"    # Ljava/util/Vector;
    .param p1, "value"    # I

    .prologue
    .line 2264
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {p0, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 2265
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private addFCD4AccentedContractions(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationElementIterator;Ljava/lang/String;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)V
    .locals 7
    .param p1, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
    .param p2, "colEl"    # Lcom/ibm/icu/text/CollationElementIterator;
    .param p3, "data"    # Ljava/lang/String;
    .param p4, "element"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .prologue
    const/4 v5, 0x0

    .line 4280
    invoke-static {p3, v5}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 4281
    .local v2, "decomp":Ljava/lang/String;
    invoke-static {p3, v5}, Lcom/ibm/icu/text/Normalizer;->compose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 4283
    .local v1, "comp":Ljava/lang/String;
    iput-object v2, p4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 4284
    iput v5, p4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 4285
    iput v5, p4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 4286
    iget-object v4, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_prefixLookup_:Ljava/util/Hashtable;

    invoke-virtual {v4, p4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .line 4287
    .local v3, "prefix":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    if-nez v3, :cond_1

    .line 4288
    iput-object v1, p4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 4289
    iput v5, p4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 4290
    const/4 v4, 0x0

    iput-object v4, p4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 4291
    iput v5, p4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 4292
    invoke-virtual {p2, v2}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 4293
    invoke-virtual {p2}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v0

    .line 4294
    .local v0, "ce":I
    iput v5, p4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 4295
    :goto_0
    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    .line 4296
    iget-object v4, p4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    iget v5, p4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    aput v0, v4, v5

    .line 4297
    invoke-virtual {p2}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v0

    .line 4298
    goto :goto_0

    .line 4299
    :cond_0
    invoke-direct {p0, p1, p4}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addAnElement(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    .line 4301
    .end local v0    # "ce":I
    :cond_1
    return-void
.end method

.method private addMultiCMontractions(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationElementIterator;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;[C[IICILjava/lang/String;)I
    .locals 18
    .param p1, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
    .param p2, "colEl"    # Lcom/ibm/icu/text/CollationElementIterator;
    .param p3, "element"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    .param p4, "precompCh"    # [C
    .param p5, "precompClass"    # [I
    .param p6, "maxComp"    # I
    .param p7, "cMark"    # C
    .param p8, "cmPos"    # I
    .param p9, "decomp"    # Ljava/lang/String;

    .prologue
    .line 4215
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->cmLookup:Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;

    .line 4216
    .local v5, "cmLookup":Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;
    const/4 v15, 0x1

    new-array v7, v15, [C

    const/4 v15, 0x0

    aput-char p7, v7, v15

    .line 4217
    .local v7, "combiningMarks":[C
    invoke-static/range {p7 .. p7}, Lcom/ibm/icu/lang/UCharacter;->getCombiningClass(I)I

    move-result v15

    and-int/lit16 v3, v15, 0xff

    .line 4218
    .local v3, "cMarkClass":I
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([C)V

    .line 4219
    .local v6, "comMark":Ljava/lang/String;
    move/from16 v12, p6

    .line 4221
    .local v12, "noOfPrecomposedChs":I
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_0
    move/from16 v0, p6

    if-ge v10, v0, :cond_6

    .line 4222
    const/4 v9, 0x0

    .line 4228
    .local v9, "count":I
    :cond_0
    if-nez v9, :cond_1

    .line 4229
    new-instance v15, Ljava/lang/String;

    const/16 v16, 0x1

    move-object/from16 v0, p4

    move/from16 v1, v16

    invoke-direct {v15, v0, v10, v1}, Ljava/lang/String;-><init>([CII)V

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v11

    .line 4230
    .local v11, "newDecomp":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14, v11}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 4231
    .local v14, "temp":Ljava/lang/StringBuffer;
    iget-object v15, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->cPoints:[C

    aget-char v15, v15, p8

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 4232
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    .line 4239
    :goto_1
    const/4 v15, 0x0

    invoke-static {v11, v15}, Lcom/ibm/icu/text/Normalizer;->compose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    .line 4240
    .local v8, "comp":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_4

    .line 4241
    move/from16 v0, p7

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 4242
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p3

    iput-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 4243
    const/4 v15, 0x0

    move-object/from16 v0, p3

    iput v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 4244
    const/4 v15, 0x0

    move-object/from16 v0, p3

    iput v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 4245
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_prefixLookup_:Ljava/util/Hashtable;

    move-object/from16 v0, p3

    invoke-virtual {v15, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .line 4246
    .local v13, "prefix":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v15, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p3

    iput-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 4247
    if-nez v13, :cond_2

    .line 4248
    const/4 v15, 0x0

    move-object/from16 v0, p3

    iput v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 4249
    const/4 v15, 0x0

    move-object/from16 v0, p3

    iput-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 4250
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 4251
    invoke-virtual/range {p2 .. p2}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v4

    .line 4252
    .local v4, "ce":I
    const/4 v15, 0x0

    move-object/from16 v0, p3

    iput v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 4253
    :goto_2
    const/4 v15, -0x1

    if-eq v4, v15, :cond_3

    .line 4254
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    move-object/from16 v0, p3

    iget v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p3

    iput v0, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    aput v4, v15, v16

    .line 4255
    invoke-virtual/range {p2 .. p2}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v4

    .line 4256
    goto :goto_2

    .line 4235
    .end local v4    # "ce":I
    .end local v8    # "comp":Ljava/lang/String;
    .end local v11    # "newDecomp":Ljava/lang/String;
    .end local v13    # "prefix":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    .end local v14    # "temp":Ljava/lang/StringBuffer;
    :cond_1
    new-instance v14, Ljava/lang/StringBuffer;

    move-object/from16 v0, p9

    invoke-direct {v14, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 4236
    .restart local v14    # "temp":Ljava/lang/StringBuffer;
    aget-char v15, p4, v10

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 4237
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    .restart local v11    # "newDecomp":Ljava/lang/String;
    goto/16 :goto_1

    .line 4259
    .restart local v8    # "comp":Ljava/lang/String;
    .restart local v13    # "prefix":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    :cond_2
    move-object/from16 v0, p3

    iput-object v8, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 4260
    const/4 v15, 0x0

    move-object/from16 v0, p3

    iput v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 4261
    const/4 v15, 0x0

    move-object/from16 v0, p3

    iput-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 4262
    const/4 v15, 0x1

    move-object/from16 v0, p3

    iput v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 4263
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    const/16 v16, 0x0

    iget v0, v13, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    move/from16 v17, v0

    aput v17, v15, v16

    .line 4265
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setMapCE(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)V

    .line 4266
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->finalizeAddition(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    .line 4267
    const/4 v15, 0x0

    invoke-virtual {v8, v15}, Ljava/lang/String;->charAt(I)C

    move-result v15

    aput-char v15, p4, v12

    .line 4268
    aput v3, p5, v12

    .line 4269
    add-int/lit8 v12, v12, 0x1

    .line 4271
    .end local v13    # "prefix":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    :cond_4
    add-int/lit8 v9, v9, 0x1

    const/4 v15, 0x2

    if-ge v9, v15, :cond_5

    aget v15, p5, v10

    if-eq v15, v3, :cond_0

    .line 4221
    :cond_5
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 4273
    .end local v8    # "comp":Ljava/lang/String;
    .end local v9    # "count":I
    .end local v11    # "newDecomp":Ljava/lang/String;
    .end local v14    # "temp":Ljava/lang/StringBuffer;
    :cond_6
    return v12
.end method

.method private addPrefix(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;ILcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I
    .locals 15
    .param p1, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
    .param p2, "CE"    # I
    .param p3, "element"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .prologue
    .line 2397
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contractions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    .line 2398
    .local v2, "contractions":Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;
    move-object/from16 v0, p3

    iget-object v8, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 2399
    .local v8, "oldCP":Ljava/lang/String;
    move-object/from16 v0, p3

    iget v9, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    .line 2401
    .local v9, "oldCPOffset":I
    const/16 v12, 0xb

    iput v12, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_currentTag_:I

    .line 2403
    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    move-object/from16 v0, p3

    iget v13, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    sub-int v11, v12, v13

    .line 2404
    .local v11, "size":I
    const/4 v5, 0x1

    .local v5, "j":I
    :goto_0
    if-ge v5, v11, :cond_1

    .line 2408
    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    move-object/from16 v0, p3

    iget v13, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    add-int/2addr v13, v5

    invoke-virtual {v12, v13}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 2409
    .local v1, "ch":C
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v12

    if-nez v12, :cond_0

    .line 2410
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    invoke-static {v12, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->unsafeCPSet([BC)V

    .line 2404
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2415
    .end local v1    # "ch":C
    :cond_1
    iget-object v12, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->length()I

    move-result v14

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 2416
    const/4 v5, 0x0

    :goto_1
    if-ge v5, v11, :cond_2

    .line 2419
    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    sub-int/2addr v12, v5

    add-int/lit8 v7, v12, -0x1

    .line 2420
    .local v7, "offset":I
    iget-object v12, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    invoke-virtual {v13, v7}, Ljava/lang/String;->charAt(I)C

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2416
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 2422
    .end local v7    # "offset":I
    :cond_2
    iget-object v12, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p3

    iput-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 2423
    const/4 v12, 0x0

    move-object/from16 v0, p3

    iput v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 2427
    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Ljava/lang/String;->charAt(I)C

    move-result v12

    invoke-static {v12}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v12

    if-nez v12, :cond_3

    .line 2428
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/String;->charAt(I)C

    move-result v13

    invoke-static {v12, v13}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->unsafeCPSet([BC)V

    .line 2431
    :cond_3
    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    move-object/from16 v0, p3

    iput-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 2432
    move-object/from16 v0, p3

    iget v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    move-object/from16 v0, p3

    iput v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    .line 2437
    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    invoke-virtual {v12, v13}, Ljava/lang/String;->charAt(I)C

    move-result v12

    invoke-static {v12}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v12

    if-nez v12, :cond_4

    .line 2439
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contrEndCP_:[B

    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    invoke-virtual {v13, v14}, Ljava/lang/String;->charAt(I)C

    move-result v13

    invoke-static {v12, v13}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->ContrEndCPSet([BC)V

    .line 2447
    :cond_4
    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    move-object/from16 v0, p3

    iget v13, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    invoke-virtual {v12, v13}, Ljava/lang/String;->charAt(I)C

    move-result v12

    invoke-static {v12}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isJamo(C)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2448
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    const/4 v13, 0x1

    iput-boolean v13, v12, Lcom/ibm/icu/text/RuleBasedCollator;->m_isJamoSpecial_:Z

    .line 2452
    :cond_5
    invoke-static/range {p2 .. p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isPrefix(I)Z

    move-result v12

    if-nez v12, :cond_6

    .line 2454
    const v12, 0xffffff

    const/4 v13, 0x0

    move/from16 v0, p2

    invoke-static {v2, v12, v13, v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    move-result v4

    .line 2457
    .local v4, "firstContractionOffset":I
    const/high16 v12, -0x10000000

    move-object/from16 v0, p3

    invoke-static {v2, v0, v12}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->processContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;I)I

    move-result v6

    .line 2459
    .local v6, "newCE":I
    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    move-object/from16 v0, p3

    iget v13, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    invoke-virtual {v12, v13}, Ljava/lang/String;->charAt(I)C

    move-result v12

    invoke-static {v2, v4, v12, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    .line 2462
    const v12, 0xffff

    move/from16 v0, p2

    invoke-static {v2, v4, v12, v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    .line 2464
    const/16 v12, 0xb

    invoke-static {v12, v4}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->constructSpecialCE(II)I

    move-result p2

    .line 2487
    .end local v4    # "firstContractionOffset":I
    .end local v6    # "newCE":I
    :goto_2
    move-object/from16 v0, p3

    iput-object v8, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 2488
    move-object/from16 v0, p3

    iput v9, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    .line 2490
    return p2

    .line 2472
    :cond_6
    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    move-object/from16 v0, p3

    iget v13, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    invoke-virtual {v12, v13}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 2473
    .restart local v1    # "ch":C
    move/from16 v0, p2

    invoke-static {v2, v0, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->findCP(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;IC)I

    move-result v10

    .line 2474
    .local v10, "position":I
    if-lez v10, :cond_7

    .line 2476
    move/from16 v0, p2

    invoke-static {v2, v0, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCE(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;II)I

    move-result v3

    .line 2477
    .local v3, "eCE":I
    move-object/from16 v0, p3

    invoke-static {v2, v0, v3}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->processContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;I)I

    move-result v6

    .line 2478
    .restart local v6    # "newCE":I
    move/from16 v0, p2

    invoke-static {v2, v0, v10, v1, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;IICI)I

    goto :goto_2

    .line 2482
    .end local v3    # "eCE":I
    .end local v6    # "newCE":I
    :cond_7
    const/high16 v12, -0x10000000

    move-object/from16 v0, p3

    invoke-static {v2, v0, v12}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->processContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;I)I

    .line 2483
    move-object/from16 v0, p3

    iget v12, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    move/from16 v0, p2

    invoke-static {v2, v0, v1, v12}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->insertContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    goto :goto_2
.end method

.method private addTailCanonicalClosures(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/CollationElementIterator;CC)V
    .locals 25
    .param p1, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
    .param p2, "m_collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p3, "colEl"    # Lcom/ibm/icu/text/CollationElementIterator;
    .param p4, "baseChar"    # C
    .param p5, "cMark"    # C

    .prologue
    .line 4082
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->cmLookup:Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;

    if-nez v4, :cond_1

    .line 4161
    :cond_0
    return-void

    .line 4085
    :cond_1
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->cmLookup:Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;

    move-object/from16 v17, v0

    .line 4086
    .local v17, "cmLookup":Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->index:[I

    move-object/from16 v21, v0

    .line 4087
    .local v21, "index":[I
    invoke-static/range {p5 .. p5}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v4

    and-int/lit16 v15, v4, 0xff

    .line 4088
    .local v15, "cClass":I
    const/16 v23, 0x0

    .line 4089
    .local v23, "maxIndex":I
    const/16 v4, 0x100

    new-array v8, v4, [C

    .line 4090
    .local v8, "precompCh":[C
    const/16 v4, 0x100

    new-array v9, v4, [I

    .line 4091
    .local v9, "precompClass":[I
    const/4 v10, 0x0

    .line 4092
    .local v10, "precompLen":I
    new-instance v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-direct {v7}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;-><init>()V

    .line 4094
    .local v7, "element":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    if-lez v15, :cond_2

    .line 4095
    add-int/lit8 v4, v15, -0x1

    aget v23, v21, v4

    .line 4097
    :cond_2
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    move/from16 v0, v23

    if-ge v12, v0, :cond_0

    .line 4098
    new-instance v20, Ljava/lang/StringBuffer;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuffer;-><init>()V

    .line 4099
    .local v20, "decompBuf":Ljava/lang/StringBuffer;
    move-object/from16 v0, v20

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->cPoints:[C

    aget-char v5, v5, v12

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 4100
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/ibm/icu/text/Normalizer;->compose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v18

    .line 4101
    .local v18, "comp":Ljava/lang/String;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_8

    .line 4102
    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aput-char v4, v8, v10

    .line 4103
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->cPoints:[C

    aget-char v4, v4, v12

    invoke-static {v4}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v4

    and-int/lit16 v4, v4, 0xff

    aput v4, v9, v10

    .line 4105
    add-int/lit8 v10, v10, 0x1

    .line 4106
    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    .line 4107
    .local v19, "decomp":Ljava/lang/StringBuffer;
    const/16 v22, 0x0

    .local v22, "j":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v4, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    move/from16 v0, v22

    if-ge v0, v4, :cond_4

    .line 4108
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v4, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move/from16 v0, p5

    if-ne v4, v0, :cond_3

    .line 4109
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->cPoints:[C

    aget-char v4, v4, v12

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 4107
    :goto_2
    add-int/lit8 v22, v22, 0x1

    goto :goto_1

    .line 4112
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v4, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 4115
    :cond_4
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/ibm/icu/text/Normalizer;->compose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v18

    .line 4116
    new-instance v14, Ljava/lang/StringBuffer;

    move-object/from16 v0, v18

    invoke-direct {v14, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 4117
    .local v14, "buf":Ljava/lang/StringBuffer;
    move/from16 v0, p5

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 4118
    move-object/from16 v0, v19

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 4119
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    .line 4121
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 4122
    const/4 v4, 0x0

    iput v4, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 4123
    const/4 v4, 0x0

    iput v4, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 4124
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_prefixLookup_:Ljava/util/Hashtable;

    invoke-virtual {v4, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .line 4125
    .local v24, "prefix":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    move-object/from16 v0, v18

    iput-object v0, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 4126
    move-object/from16 v0, v18

    iput-object v0, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    .line 4128
    if-nez v24, :cond_5

    .line 4129
    const/4 v4, 0x0

    iput v4, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 4130
    const/4 v4, 0x0

    iput-object v4, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 4131
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 4132
    invoke-virtual/range {p3 .. p3}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v16

    .line 4133
    .local v16, "ce":I
    const/4 v4, 0x0

    iput v4, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 4134
    :goto_3
    const/4 v4, -0x1

    move/from16 v0, v16

    if-eq v0, v4, :cond_6

    .line 4135
    iget-object v4, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    iget v5, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    add-int/lit8 v6, v5, 0x1

    iput v6, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    aput v16, v4, v5

    .line 4136
    invoke-virtual/range {p3 .. p3}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v16

    .line 4137
    goto :goto_3

    .line 4140
    .end local v16    # "ce":I
    :cond_5
    move-object/from16 v0, v18

    iput-object v0, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 4141
    const/4 v4, 0x0

    iput v4, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 4142
    const/4 v4, 0x0

    iput-object v4, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 4143
    const/4 v4, 0x1

    iput v4, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 4144
    iget-object v4, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    const/4 v5, 0x0

    move-object/from16 v0, v24

    iget v6, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    aput v6, v4, v5

    .line 4146
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v7}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setMapCE(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)V

    .line 4147
    move-object/from16 v0, p1

    invoke-static {v0, v7}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->finalizeAddition(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    .line 4149
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x2

    if-le v4, v5, :cond_7

    .line 4152
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, v18

    invoke-direct {v0, v1, v2, v3, v7}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addFCD4AccentedContractions(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationElementIterator;Ljava/lang/String;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)V

    .line 4154
    :cond_7
    const/4 v4, 0x1

    if-le v10, v4, :cond_8

    .line 4155
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move/from16 v11, p5

    invoke-direct/range {v4 .. v13}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addMultiCMontractions(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationElementIterator;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;[C[IICILjava/lang/String;)I

    move-result v10

    .line 4097
    .end local v14    # "buf":Ljava/lang/StringBuffer;
    .end local v19    # "decomp":Ljava/lang/StringBuffer;
    .end local v22    # "j":I
    .end local v24    # "prefix":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    :cond_8
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0
.end method

.method private allocateWeights(IIII[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;)I
    .locals 22
    .param p1, "lowerLimit"    # I
    .param p2, "upperLimit"    # I
    .param p3, "n"    # I
    .param p4, "maxByte"    # I
    .param p5, "ranges"    # [Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    .prologue
    .line 3168
    add-int/lit8 v2, p4, -0x4

    add-int/lit8 v6, v2, 0x1

    .line 3172
    .local v6, "countBytes":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLongBuffer_:[J

    const/4 v3, 0x0

    const-wide/16 v4, 0x1

    aput-wide v4, v2, v3

    .line 3173
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLongBuffer_:[J

    const/4 v3, 0x1

    int-to-long v4, v6

    aput-wide v4, v2, v3

    .line 3174
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLongBuffer_:[J

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLongBuffer_:[J

    const/4 v5, 0x1

    aget-wide v4, v4, v5

    int-to-long v0, v6

    move-wide/from16 v20, v0

    mul-long v4, v4, v20

    aput-wide v4, v2, v3

    .line 3175
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLongBuffer_:[J

    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLongBuffer_:[J

    const/4 v5, 0x2

    aget-wide v4, v4, v5

    int-to-long v0, v6

    move-wide/from16 v20, v0

    mul-long v4, v4, v20

    aput-wide v4, v2, v3

    .line 3176
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLongBuffer_:[J

    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLongBuffer_:[J

    const/4 v5, 0x3

    aget-wide v4, v4, v5

    int-to-long v0, v6

    move-wide/from16 v20, v0

    mul-long v4, v4, v20

    aput-wide v4, v2, v3

    move-object/from16 v2, p0

    move/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p4

    move-object/from16 v7, p5

    .line 3177
    invoke-direct/range {v2 .. v7}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getWeightRanges(IIII[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;)I

    move-result v15

    .line 3179
    .local v15, "rangeCount":I
    if-gtz v15, :cond_0

    .line 3180
    const/4 v2, 0x0

    .line 3295
    :goto_0
    return v2

    .line 3183
    :cond_0
    const-wide/16 v12, 0x0

    .line 3184
    .local v12, "maxCount":J
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    if-ge v11, v15, :cond_1

    .line 3185
    aget-object v2, p5, v11

    iget v2, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    int-to-long v2, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLongBuffer_:[J

    aget-object v5, p5, v11

    iget v5, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length_:I

    rsub-int/lit8 v5, v5, 0x4

    aget-wide v4, v4, v5

    mul-long/2addr v2, v4

    add-long/2addr v12, v2

    .line 3184
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 3188
    :cond_1
    move/from16 v0, p3

    int-to-long v2, v0

    cmp-long v2, v12, v2

    if-gez v2, :cond_2

    .line 3189
    const/4 v2, 0x0

    goto :goto_0

    .line 3192
    :cond_2
    const/4 v11, 0x0

    :goto_2
    if-ge v11, v15, :cond_3

    .line 3193
    aget-object v2, p5, v11

    aget-object v3, p5, v11

    iget v3, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length_:I

    iput v3, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length2_:I

    .line 3194
    aget-object v2, p5, v11

    aget-object v3, p5, v11

    iget v3, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    iput v3, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count2_:I

    .line 3192
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 3199
    :cond_3
    const/4 v2, 0x0

    aget-object v2, p5, v2

    iget v14, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length2_:I

    .line 3202
    .local v14, "minLength":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCountBuffer_:[I

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([II)V

    .line 3203
    const/4 v11, 0x0

    :goto_3
    if-ge v11, v15, :cond_4

    .line 3204
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCountBuffer_:[I

    aget-object v3, p5, v11

    iget v3, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length2_:I

    aget v4, v2, v3

    aget-object v5, p5, v11

    iget v5, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count2_:I

    add-int/2addr v4, v5

    aput v4, v2, v3

    .line 3203
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 3207
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCountBuffer_:[I

    aget v2, v2, v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCountBuffer_:[I

    add-int/lit8 v4, v14, 0x1

    aget v3, v3, v4

    add-int/2addr v2, v3

    move/from16 v0, p3

    if-gt v0, v2, :cond_7

    .line 3210
    const-wide/16 v12, 0x0

    .line 3211
    const/4 v15, 0x0

    .line 3213
    :cond_5
    aget-object v2, p5, v15

    iget v2, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count2_:I

    int-to-long v2, v2

    add-long/2addr v12, v2

    .line 3214
    add-int/lit8 v15, v15, 0x1

    .line 3215
    move/from16 v0, p3

    int-to-long v2, v0

    cmp-long v2, v2, v12

    if-gtz v2, :cond_5

    .line 3287
    :goto_4
    const/4 v2, 0x1

    if-le v15, v2, :cond_6

    .line 3289
    const/4 v2, 0x0

    move-object/from16 v0, p5

    invoke-static {v0, v2, v15}, Ljava/util/Arrays;->sort([Ljava/lang/Object;II)V

    .line 3293
    :cond_6
    const/4 v2, 0x0

    aget-object v2, p5, v2

    move/from16 v0, p4

    iput v0, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    move v2, v15

    .line 3295
    goto/16 :goto_0

    .line 3218
    :cond_7
    const/4 v2, 0x0

    aget-object v2, p5, v2

    iget v2, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count2_:I

    mul-int/2addr v2, v6

    move/from16 v0, p3

    if-gt v0, v2, :cond_a

    .line 3221
    const/4 v15, 0x1

    .line 3224
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLongBuffer_:[J

    const/4 v3, 0x0

    aget-object v3, p5, v3

    iget v3, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length_:I

    sub-int v3, v14, v3

    aget-wide v18, v2, v3

    .line 3226
    .local v18, "power_1":J
    int-to-long v2, v6

    mul-long v16, v18, v2

    .line 3227
    .local v16, "power":J
    move/from16 v0, p3

    int-to-long v2, v0

    add-long v2, v2, v16

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    div-long v2, v2, v16

    long-to-int v10, v2

    .line 3228
    .local v10, "count2":I
    const/4 v2, 0x0

    aget-object v2, p5, v2

    iget v2, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    sub-int v9, v2, v10

    .line 3230
    .local v9, "count1":I
    const/4 v2, 0x1

    if-ge v9, v2, :cond_8

    .line 3232
    const/4 v2, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-static {v0, v2, v1, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->lengthenRange([Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;III)I

    goto :goto_4

    .line 3238
    :cond_8
    const/4 v15, 0x2

    .line 3239
    const/4 v2, 0x1

    aget-object v2, p5, v2

    const/4 v3, 0x0

    aget-object v3, p5, v3

    iget v3, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    iput v3, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    .line 3240
    const/4 v2, 0x1

    aget-object v2, p5, v2

    const/4 v3, 0x0

    aget-object v3, p5, v3

    iget v3, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length_:I

    iput v3, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length_:I

    .line 3241
    const/4 v2, 0x1

    aget-object v2, p5, v2

    iput v14, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length2_:I

    .line 3243
    const/4 v2, 0x0

    aget-object v2, p5, v2

    iget v11, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length_:I

    .line 3244
    const/4 v2, 0x0

    aget-object v2, p5, v2

    iget v2, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    invoke-static {v2, v11}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getWeightByte(II)I

    move-result v2

    add-int/2addr v2, v9

    add-int/lit8 v8, v2, -0x1

    .line 3247
    .local v8, "b":I
    move/from16 v0, p4

    if-gt v8, v0, :cond_9

    .line 3248
    const/4 v2, 0x0

    aget-object v2, p5, v2

    const/4 v3, 0x0

    aget-object v3, p5, v3

    iget v3, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    invoke-static {v3, v11, v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setWeightByte(III)I

    move-result v3

    iput v3, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    .line 3259
    :goto_5
    shl-int/lit8 v2, p4, 0x18

    shl-int/lit8 v3, p4, 0x10

    or-int/2addr v2, v3

    shl-int/lit8 v3, p4, 0x8

    or-int/2addr v2, v3

    or-int v8, v2, p4

    .line 3261
    const/4 v2, 0x0

    aget-object v2, p5, v2

    const/4 v3, 0x0

    aget-object v3, p5, v3

    iget v3, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    invoke-static {v3, v11}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->truncateWeight(II)I

    move-result v3

    shl-int/lit8 v4, v11, 0x3

    ushr-int v4, v8, v4

    rsub-int/lit8 v5, v14, 0x4

    shl-int/lit8 v5, v5, 0x3

    shl-int v5, v8, v5

    and-int/2addr v4, v5

    or-int/2addr v3, v4

    iput v3, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    .line 3266
    const/4 v2, 0x1

    aget-object v2, p5, v2

    const/4 v3, 0x0

    aget-object v3, p5, v3

    iget v3, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    move/from16 v0, p4

    invoke-static {v3, v14, v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->incWeight(III)I

    move-result v3

    iput v3, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    .line 3269
    const/4 v2, 0x0

    aget-object v2, p5, v2

    iput v9, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    .line 3270
    const/4 v2, 0x1

    aget-object v2, p5, v2

    iput v10, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    .line 3272
    const/4 v2, 0x0

    aget-object v2, p5, v2

    int-to-long v4, v9

    mul-long v4, v4, v18

    long-to-int v3, v4

    iput v3, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count2_:I

    .line 3274
    const/4 v2, 0x1

    aget-object v2, p5, v2

    int-to-long v4, v10

    mul-long v4, v4, v18

    long-to-int v3, v4

    iput v3, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count2_:I

    .line 3277
    const/4 v2, 0x1

    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-static {v0, v2, v1, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->lengthenRange([Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;III)I

    goto/16 :goto_4

    .line 3252
    :cond_9
    const/4 v2, 0x0

    aget-object v2, p5, v2

    const/4 v3, 0x0

    aget-object v3, p5, v3

    iget v3, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    add-int/lit8 v4, v11, -0x1

    move/from16 v0, p4

    invoke-static {v3, v4, v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->incWeight(III)I

    move-result v3

    sub-int v4, v8, v6

    invoke-static {v3, v11, v4}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setWeightByte(III)I

    move-result v3

    iput v3, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    goto :goto_5

    .line 3282
    .end local v8    # "b":I
    .end local v9    # "count1":I
    .end local v10    # "count2":I
    .end local v16    # "power":J
    .end local v18    # "power_1":J
    :cond_a
    const/4 v11, 0x0

    :goto_6
    aget-object v2, p5, v11

    iget v2, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length2_:I

    if-ne v2, v14, :cond_3

    .line 3283
    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-static {v0, v11, v1, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->lengthenRange([Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;III)I

    .line 3282
    add-int/lit8 v11, v11, 0x1

    goto :goto_6
.end method

.method private assembleTable(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/RuleBasedCollator;)V
    .locals 11
    .param p1, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
    .param p2, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;

    .prologue
    const/16 v10, 0x420

    const/4 v9, 0x0

    .line 3623
    iget-object v4, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    .line 3624
    .local v4, "mapping":Lcom/ibm/icu/impl/IntTrieBuilder;
    iget-object v2, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_expansions_:Ljava/util/Vector;

    .line 3625
    .local v2, "expansions":Ljava/util/Vector;
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contractions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    .line 3626
    .local v0, "contractions":Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;
    iget-object v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_maxExpansions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;

    .line 3632
    .local v5, "maxexpansion":Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;
    iput v9, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    .line 3633
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->constructTable(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;)I

    move-result v1

    .line 3638
    .local v1, "contractionsSize":I
    iget-object v7, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_maxJamoExpansions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;

    iget-boolean v8, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_isJamoSpecial_:Z

    invoke-static {v4, v5, v7, v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getMaxExpansionJamo(Lcom/ibm/icu/impl/IntTrieBuilder;Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;Z)V

    .line 3643
    iget-object v7, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    invoke-static {p2, v7}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setAttributes(Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/CollationRuleParser$OptionSet;)V

    .line 3645
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v6

    .line 3646
    .local v6, "size":I
    new-array v7, v6, [I

    iput-object v7, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    .line 3647
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v6, :cond_0

    .line 3648
    iget-object v8, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    invoke-virtual {v2, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aput v7, v8, v3

    .line 3647
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3651
    :cond_0
    if-eqz v1, :cond_1

    .line 3653
    new-array v7, v1, [C

    iput-object v7, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    .line 3654
    iget-object v7, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    iget-object v8, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    invoke-virtual {v7, v9, v1, v8, v9}, Ljava/lang/StringBuffer;->getChars(II[CI)V

    .line 3658
    new-array v7, v1, [I

    iput-object v7, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    .line 3659
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_1

    .line 3660
    iget-object v8, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    iget-object v7, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_CEs_:Ljava/util/Vector;

    invoke-virtual {v7, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aput v7, v8, v3

    .line 3659
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 3665
    :cond_1
    invoke-static {}, Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;->getInstance()Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;

    move-result-object v7

    invoke-virtual {v4, p1, v7}, Lcom/ibm/icu/impl/IntTrieBuilder;->serialize(Lcom/ibm/icu/impl/TrieBuilder$DataManipulate;Lcom/ibm/icu/impl/Trie$DataManipulate;)Lcom/ibm/icu/impl/IntTrie;

    move-result-object v7

    iput-object v7, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    .line 3672
    iput v9, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionOffset_:I

    .line 3673
    iget-object v7, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v6

    .line 3674
    add-int/lit8 v7, v6, -0x1

    new-array v7, v7, [I

    iput-object v7, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    .line 3675
    const/4 v3, 0x1

    :goto_2
    if-ge v3, v6, :cond_2

    .line 3676
    iget-object v8, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    add-int/lit8 v9, v3, -0x1

    iget-object v7, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v7, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aput v7, v8, v9

    .line 3675
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 3679
    :cond_2
    add-int/lit8 v7, v6, -0x1

    new-array v7, v7, [B

    iput-object v7, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCEMaxSize_:[B

    .line 3680
    const/4 v3, 0x1

    :goto_3
    if-ge v3, v6, :cond_3

    .line 3681
    iget-object v8, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCEMaxSize_:[B

    add-int/lit8 v9, v3, -0x1

    iget-object v7, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_expansionCESize_:Ljava/util/Vector;

    invoke-virtual {v7, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Byte;

    invoke-virtual {v7}, Ljava/lang/Byte;->byteValue()B

    move-result v7

    aput-byte v7, v8, v9

    .line 3680
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 3685
    :cond_3
    invoke-static {p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->unsafeCPAddCCNZ(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;)V

    .line 3687
    const/4 v3, 0x0

    :goto_4
    if-ge v3, v10, :cond_4

    .line 3688
    iget-object v7, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    aget-byte v8, v7, v3

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v9, v9, Lcom/ibm/icu/text/RuleBasedCollator;->m_unsafe_:[B

    aget-byte v9, v9, v3

    or-int/2addr v8, v9

    int-to-byte v8, v8

    aput-byte v8, v7, v3

    .line 3687
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 3690
    :cond_4
    iget-object v7, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    iput-object v7, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_unsafe_:[B

    .line 3695
    const/4 v3, 0x0

    :goto_5
    if-ge v3, v10, :cond_5

    .line 3696
    iget-object v7, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contrEndCP_:[B

    aget-byte v8, v7, v3

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v9, v9, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionEnd_:[B

    aget-byte v9, v9, v3

    or-int/2addr v8, v9

    int-to-byte v8, v8

    aput-byte v8, v7, v3

    .line 3695
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 3698
    :cond_5
    iget-object v7, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contrEndCP_:[B

    iput-object v7, p2, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionEnd_:[B

    .line 3699
    return-void
.end method

.method private canonicalClosure(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;)V
    .locals 17
    .param p1, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;

    .prologue
    .line 4004
    new-instance v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;-><init>(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;)V

    .line 4005
    .local v14, "temp":Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
    iget-object v1, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->assembleTable(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/RuleBasedCollator;)V

    .line 4007
    iget-object v1, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->getCollationElementIterator(Ljava/lang/String;)Lcom/ibm/icu/text/CollationElementIterator;

    move-result-object v4

    .line 4009
    .local v4, "coleiter":Lcom/ibm/icu/text/CollationElementIterator;
    invoke-static {}, Lcom/ibm/icu/lang/UCharacter;->getTypeIterator()Lcom/ibm/icu/util/RangeValueIterator;

    move-result-object v16

    .line 4010
    .local v16, "typeiter":Lcom/ibm/icu/util/RangeValueIterator;
    new-instance v8, Lcom/ibm/icu/util/RangeValueIterator$Element;

    invoke-direct {v8}, Lcom/ibm/icu/util/RangeValueIterator$Element;-><init>()V

    .line 4011
    .local v8, "element":Lcom/ibm/icu/util/RangeValueIterator$Element;
    :goto_0
    move-object/from16 v0, v16

    invoke-interface {v0, v8}, Lcom/ibm/icu/util/RangeValueIterator;->next(Lcom/ibm/icu/util/RangeValueIterator$Element;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4012
    iget-object v3, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v5, v8, Lcom/ibm/icu/util/RangeValueIterator$Element;->start:I

    iget v6, v8, Lcom/ibm/icu/util/RangeValueIterator$Element;->limit:I

    iget v7, v8, Lcom/ibm/icu/util/RangeValueIterator$Element;->value:I

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->enumCategoryRangeClosureCategory(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/CollationElementIterator;III)Z

    goto :goto_0

    .line 4017
    :cond_0
    iget-object v1, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->cmLookup:Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;

    move-object/from16 v0, p1

    iput-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->cmLookup:Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;

    .line 4018
    const/4 v1, 0x0

    iput-object v1, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->cmLookup:Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;

    .line 4020
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget v1, v1, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    if-ge v10, v1, :cond_7

    .line 4026
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v1, v1, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    aget-object v1, v1, v10

    iget-object v15, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 4027
    .local v15, "tok":Lcom/ibm/icu/text/CollationRuleParser$Token;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->clear()V

    .line 4028
    :goto_2
    if-eqz v15, :cond_6

    .line 4029
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v2, 0x0

    iput v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 4030
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v2, 0x0

    iput v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    .line 4031
    iget v1, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_prefix_:I

    if-eqz v1, :cond_2

    .line 4036
    iget v1, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_prefix_:I

    shr-int/lit8 v13, v1, 0x18

    .line 4037
    .local v13, "size":I
    iget v1, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_prefix_:I

    const v2, 0xffffff

    and-int v12, v1, v2

    .line 4038
    .local v12, "offset":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v2, v2, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    add-int v3, v12, v13

    invoke-virtual {v2, v12, v3}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 4040
    iget v1, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    shr-int/lit8 v1, v1, 0x18

    iget v2, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_prefix_:I

    shr-int/lit8 v2, v2, 0x18

    sub-int v13, v1, v2

    .line 4041
    iget v1, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    iget v2, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_prefix_:I

    shr-int/lit8 v2, v2, 0x18

    add-int v12, v1, v2

    .line 4042
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v2, v2, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    add-int v3, v12, v13

    invoke-virtual {v2, v12, v3}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    .line 4052
    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v2, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    iput-object v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 4054
    const/4 v6, 0x0

    .local v6, "firstCM":C
    move v5, v6

    .line 4055
    .local v5, "baseChar":I
    const/4 v11, 0x0

    .line 4056
    .end local v5    # "baseChar":I
    .local v11, "j":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v1, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget v2, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    sub-int/2addr v1, v2

    if-ge v11, v1, :cond_4

    .line 4058
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v1, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v1, v11}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v9

    .line 4059
    .local v9, "fcd":C
    and-int/lit16 v1, v9, 0xff

    if-nez v1, :cond_3

    .line 4060
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v1, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v1, v11}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 4056
    :cond_1
    :goto_5
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 4046
    .end local v6    # "firstCM":C
    .end local v9    # "fcd":C
    .end local v11    # "j":I
    .end local v12    # "offset":I
    .end local v13    # "size":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 4047
    iget v1, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    const v2, 0xffffff

    and-int v12, v1, v2

    .line 4048
    .restart local v12    # "offset":I
    iget v1, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    ushr-int/lit8 v13, v1, 0x18

    .line 4049
    .restart local v13    # "size":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v2, v2, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    add-int v3, v12, v13

    invoke-virtual {v2, v12, v3}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    goto :goto_3

    .line 4063
    .restart local v6    # "firstCM":C
    .restart local v9    # "fcd":C
    .restart local v11    # "j":I
    :cond_3
    if-eqz v5, :cond_1

    if-nez v6, :cond_1

    .line 4064
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v1, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v1, v11}, Ljava/lang/String;->charAt(I)C

    move-result v6

    goto :goto_5

    .line 4069
    .end local v9    # "fcd":C
    :cond_4
    if-eqz v5, :cond_5

    if-eqz v6, :cond_5

    .line 4070
    iget-object v3, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addTailCanonicalClosures(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/CollationElementIterator;CC)V

    .line 4072
    :cond_5
    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 4073
    goto/16 :goto_2

    .line 4020
    .end local v6    # "firstCM":C
    .end local v11    # "j":I
    .end local v12    # "offset":I
    .end local v13    # "size":I
    :cond_6
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 4075
    .end local v15    # "tok":Lcom/ibm/icu/text/CollationRuleParser$Token;
    :cond_7
    return-void
.end method

.method private static final changeContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I
    .locals 4
    .param p0, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;
    .param p1, "element"    # I
    .param p2, "codePoint"    # C
    .param p3, "newCE"    # I

    .prologue
    const/high16 v2, -0x10000000

    .line 2788
    invoke-static {p0, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getBasicContractionTable(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;I)Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    move-result-object v1

    .line 2789
    .local v1, "tbl":Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;
    if-nez v1, :cond_1

    .line 2790
    const/4 v2, 0x0

    .line 2804
    :cond_0
    :goto_0
    return v2

    .line 2792
    :cond_1
    const/4 v0, 0x0

    .line 2793
    .local v0, "position":I
    :cond_2
    iget-object v3, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    if-le p2, v3, :cond_3

    .line 2794
    add-int/lit8 v0, v0, 0x1

    .line 2795
    iget-object v3, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-le v0, v3, :cond_2

    goto :goto_0

    .line 2799
    :cond_3
    iget-object v3, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    if-ne p2, v3, :cond_0

    .line 2800
    iget-object v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_CEs_:Ljava/util/Vector;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, p3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v0, v3}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2801
    const v2, 0xffffff

    and-int/2addr v2, p1

    goto :goto_0
.end method

.method private static final changeLastCE(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;II)I
    .locals 4
    .param p0, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;
    .param p1, "element"    # I
    .param p2, "value"    # I

    .prologue
    .line 3057
    invoke-static {p0, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getBasicContractionTable(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;I)Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    move-result-object v0

    .line 3058
    .local v0, "tbl":Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;
    if-nez v0, :cond_0

    .line 3059
    const/4 v1, 0x0

    .line 3063
    :goto_0
    return v1

    .line 3062
    :cond_0
    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_CEs_:Ljava/util/Vector;

    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_CEs_:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, p2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2, v3}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3063
    iget v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_currentTag_:I

    const v2, 0xffffff

    and-int/2addr v2, p1

    invoke-static {v1, v2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->constructSpecialCE(II)I

    move-result v1

    goto :goto_0
.end method

.method private static final constructSpecialCE(II)I
    .locals 2
    .param p0, "tag"    # I
    .param p1, "CE"    # I

    .prologue
    .line 2634
    const/high16 v0, -0x10000000

    shl-int/lit8 v1, p0, 0x18

    or-int/2addr v0, v1

    const v1, 0xffffff

    and-int/2addr v1, p1

    or-int/2addr v0, v1

    return v0
.end method

.method private constructTable(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;)I
    .locals 22
    .param p1, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    .prologue
    .line 3730
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_elements_:Ljava/util/Vector;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/Vector;->size()I

    move-result v16

    .line 3731
    .local v16, "tsize":I
    if-nez v16, :cond_1

    .line 3732
    const/4 v14, 0x0

    .line 3789
    :cond_0
    return v14

    .line 3734
    :cond_1
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_offsets_:Ljava/util/Vector;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/Vector;->clear()V

    .line 3735
    const/4 v14, 0x0

    .line 3736
    .local v14, "position":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    move/from16 v0, v16

    if-ge v11, v0, :cond_2

    .line 3737
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_offsets_:Ljava/util/Vector;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/Integer;

    move-object/from16 v0, v18

    invoke-direct {v0, v14}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual/range {v17 .. v18}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3738
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_elements_:Ljava/util/Vector;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_CEs_:Ljava/util/Vector;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/Vector;->size()I

    move-result v17

    add-int v14, v14, v17

    .line 3736
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 3741
    :cond_2
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_CEs_:Ljava/util/Vector;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/Vector;->clear()V

    .line 3742
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->length()I

    move-result v19

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 3744
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    .line 3745
    .local v10, "cpPointer":Ljava/lang/StringBuffer;
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_CEs_:Ljava/util/Vector;

    .line 3746
    .local v3, "CEPointer":Ljava/util/Vector;
    const/4 v11, 0x0

    :goto_1
    move/from16 v0, v16

    if-ge v11, v0, :cond_9

    .line 3747
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_elements_:Ljava/util/Vector;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    .line 3749
    .local v4, "bct":Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;
    iget-object v0, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_CEs_:Ljava/util/Vector;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/Vector;->size()I

    move-result v15

    .line 3750
    .local v15, "size":I
    const/4 v6, 0x0

    .line 3751
    .local v6, "ccMax":C
    const/16 v7, 0xff

    .line 3752
    .local v7, "ccMin":C
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v13

    .line 3753
    .local v13, "offset":I
    iget-object v0, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_CEs_:Ljava/util/Vector;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3754
    const/4 v12, 0x1

    .local v12, "j":I
    :goto_2
    if-ge v12, v15, :cond_5

    .line 3755
    iget-object v0, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v9

    .line 3756
    .local v9, "ch":C
    invoke-static {v9}, Lcom/ibm/icu/lang/UCharacter;->getCombiningClass(I)I

    move-result v17

    move/from16 v0, v17

    and-int/lit16 v0, v0, 0xff

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-char v5, v0

    .line 3757
    .local v5, "cc":C
    if-le v5, v6, :cond_3

    .line 3758
    move v6, v5

    .line 3760
    :cond_3
    if-ge v5, v7, :cond_4

    .line 3761
    move v7, v5

    .line 3763
    :cond_4
    invoke-virtual {v10, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3764
    iget-object v0, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_CEs_:Ljava/util/Vector;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3754
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 3766
    .end local v5    # "cc":C
    .end local v9    # "ch":C
    :cond_5
    if-ne v7, v6, :cond_7

    const/16 v17, 0x1

    :goto_3
    or-int v17, v17, v6

    move/from16 v0, v17

    int-to-char v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v10, v13, v0}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 3768
    const/4 v12, 0x0

    :goto_4
    if-ge v12, v15, :cond_8

    .line 3769
    add-int v17, v13, v12

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isContractionTableElement(I)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 3771
    add-int v17, v13, v12

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 3772
    .local v8, "ce":I
    add-int v18, v13, v12

    new-instance v19, Ljava/lang/Integer;

    invoke-static {v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCETag(I)I

    move-result v20

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_offsets_:Ljava/util/Vector;

    move-object/from16 v17, v0

    invoke-static {v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getContractionOffset(I)I

    move-result v21

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move/from16 v0, v20

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->constructSpecialCE(II)I

    move-result v17

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 3768
    .end local v8    # "ce":I
    :cond_6
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    .line 3766
    :cond_7
    const/16 v17, 0x0

    goto :goto_3

    .line 3746
    :cond_8
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 3780
    .end local v4    # "bct":Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;
    .end local v6    # "ccMax":C
    .end local v7    # "ccMin":C
    .end local v12    # "j":I
    .end local v13    # "offset":I
    .end local v15    # "size":I
    :cond_9
    const/4 v11, 0x0

    :goto_5
    const v17, 0x10ffff

    move/from16 v0, v17

    if-gt v11, v0, :cond_0

    .line 3781
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/ibm/icu/impl/IntTrieBuilder;->getValue(I)I

    move-result v2

    .line 3782
    .local v2, "CE":I
    invoke-static {v2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isContractionTableElement(I)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 3783
    invoke-static {v2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCETag(I)I

    move-result v18

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_offsets_:Ljava/util/Vector;

    move-object/from16 v17, v0

    invoke-static {v2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getContractionOffset(I)I

    move-result v19

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move/from16 v0, v18

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->constructSpecialCE(II)I

    move-result v2

    .line 3786
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11, v2}, Lcom/ibm/icu/impl/IntTrieBuilder;->setValue(II)Z

    .line 3780
    :cond_a
    add-int/lit8 v11, v11, 0x1

    goto :goto_5
.end method

.method private copyRangeFromUCA(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;II)V
    .locals 9
    .param p1, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v8, -0x1

    const/high16 v7, -0x10000000

    const/4 v6, 0x0

    .line 471
    const/4 v1, 0x0

    .line 472
    .local v1, "u":I
    move v1, p2

    :goto_0
    if-gt v1, p3, :cond_4

    .line 474
    iget-object v2, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    invoke-virtual {v2, v1}, Lcom/ibm/icu/impl/IntTrieBuilder;->getValue(I)I

    move-result v0

    .line 475
    .local v0, "CE":I
    if-eq v0, v7, :cond_0

    invoke-static {v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isContractionTableElement(I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contractions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    invoke-static {v2, v0, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCE(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;II)I

    move-result v2

    if-ne v2, v7, :cond_3

    .line 483
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->toString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    .line 484
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v3, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v3, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    iput-object v3, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 485
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iput v6, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 486
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iput v6, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 487
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 488
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v3, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v3, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 489
    :cond_1
    :goto_1
    if-eq v0, v8, :cond_2

    .line 490
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v2}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v0

    .line 491
    if-eq v0, v8, :cond_1

    .line 492
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v2, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    iget-object v3, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget v4, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    add-int/lit8 v5, v4, 0x1

    iput v5, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    aput v0, v2, v4

    goto :goto_1

    .line 496
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-direct {p0, p1, v2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addAnElement(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    .line 472
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 499
    .end local v0    # "CE":I
    :cond_4
    return-void
.end method

.method private static final countBytes(I)I
    .locals 3
    .param p0, "ce"    # I

    .prologue
    .line 1764
    const/4 v0, -0x1

    .line 1765
    .local v0, "mask":I
    const/4 v1, 0x0

    .line 1766
    .local v1, "result":I
    :goto_0
    if-eqz v0, :cond_1

    .line 1767
    and-int v2, p0, v0

    if-eqz v2, :cond_0

    .line 1768
    add-int/lit8 v1, v1, 0x1

    .line 1770
    :cond_0
    ushr-int/lit8 v0, v0, 0x8

    .line 1771
    goto :goto_0

    .line 1772
    :cond_1
    return v1
.end method

.method private createElements(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;)V
    .locals 20
    .param p1, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
    .param p2, "lh"    # Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    .prologue
    .line 1783
    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1784
    .local v14, "tok":Lcom/ibm/icu/text/CollationRuleParser$Token;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-virtual {v15}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->clear()V

    .line 1785
    :goto_0
    if-eqz v14, :cond_d

    .line 1790
    iget v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expansion_:I

    if-eqz v15, :cond_5

    .line 1791
    iget v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expansion_:I

    ushr-int/lit8 v9, v15, 0x18

    .line 1792
    .local v9, "len":I
    move v3, v9

    .line 1793
    .local v3, "currentSequenceLen":I
    iget v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expansion_:I

    const v16, 0xffffff

    and-int v4, v15, v16

    .line 1794
    .local v4, "expOffset":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    or-int v16, v3, v4

    move/from16 v0, v16

    iput v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    .line 1795
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_rules_:Ljava/lang/StringBuffer;

    .line 1797
    :cond_0
    :goto_1
    if-lez v9, :cond_6

    .line 1798
    move v3, v9

    .line 1799
    :goto_2
    if-lez v3, :cond_2

    .line 1800
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    shl-int/lit8 v16, v3, 0x18

    or-int v16, v16, v4

    move/from16 v0, v16

    iput v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    .line 1802
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser;->m_hashTable_:Ljava/util/Hashtable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1805
    .local v5, "expt":Lcom/ibm/icu/text/CollationRuleParser$Token;
    if-eqz v5, :cond_3

    iget v15, v5, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    const v16, -0x21524111

    move/from16 v0, v16

    if-eq v15, v0, :cond_3

    .line 1809
    iget v10, v5, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CELength_:I

    .line 1810
    .local v10, "noOfCEsToCopy":I
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_3
    if-ge v8, v10, :cond_1

    .line 1811
    iget-object v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCE_:[I

    iget v0, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCELength_:I

    move/from16 v16, v0

    add-int v16, v16, v8

    iget-object v0, v5, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CE_:[I

    move-object/from16 v17, v0

    aget v17, v17, v8

    aput v17, v15, v16

    .line 1810
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 1814
    :cond_1
    iget v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCELength_:I

    add-int/2addr v15, v10

    iput v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCELength_:I

    .line 1817
    add-int/2addr v4, v3

    .line 1818
    sub-int/2addr v9, v3

    .line 1825
    .end local v5    # "expt":Lcom/ibm/icu/text/CollationRuleParser$Token;
    .end local v8    # "j":I
    .end local v10    # "noOfCEsToCopy":I
    :cond_2
    if-nez v3, :cond_0

    .line 1830
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v16, v0

    add-int/lit8 v17, v4, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 1833
    :goto_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v15}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v12

    .line 1834
    .local v12, "order":I
    const/4 v15, -0x1

    if-ne v12, v15, :cond_4

    .line 1839
    add-int/lit8 v4, v4, 0x1

    .line 1840
    add-int/lit8 v9, v9, -0x1

    .line 1841
    goto :goto_1

    .line 1822
    .end local v12    # "order":I
    .restart local v5    # "expt":Lcom/ibm/icu/text/CollationRuleParser$Token;
    :cond_3
    add-int/lit8 v3, v3, -0x1

    .line 1824
    goto :goto_2

    .line 1837
    .end local v5    # "expt":Lcom/ibm/icu/text/CollationRuleParser$Token;
    .restart local v12    # "order":I
    :cond_4
    iget-object v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCE_:[I

    iget v0, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCELength_:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    iput v0, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCELength_:I

    aput v12, v15, v16

    goto :goto_4

    .line 1845
    .end local v3    # "currentSequenceLen":I
    .end local v4    # "expOffset":I
    .end local v9    # "len":I
    .end local v12    # "order":I
    :cond_5
    const/4 v15, 0x0

    iput v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCELength_:I

    .line 1849
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget v0, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CELength_:I

    move/from16 v16, v0

    iget v0, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCELength_:I

    move/from16 v17, v0

    add-int v16, v16, v17

    move/from16 v0, v16

    iput v0, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 1852
    iget-object v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CE_:[I

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    move-object/from16 v17, v0

    const/16 v18, 0x0

    iget v0, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CELength_:I

    move/from16 v19, v0

    invoke-static/range {v15 .. v19}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1854
    iget-object v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCE_:[I

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    move-object/from16 v17, v0

    iget v0, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CELength_:I

    move/from16 v18, v0

    iget v0, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCELength_:I

    move/from16 v19, v0

    invoke-static/range {v15 .. v19}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1861
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/16 v16, 0x0

    move/from16 v0, v16

    iput v0, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 1862
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/16 v16, 0x0

    move/from16 v0, v16

    iput v0, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    .line 1863
    iget v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_prefix_:I

    if-eqz v15, :cond_9

    .line 1868
    iget v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_prefix_:I

    shr-int/lit8 v13, v15, 0x18

    .line 1869
    .local v13, "size":I
    iget v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_prefix_:I

    const v16, 0xffffff

    and-int v11, v15, v16

    .line 1870
    .local v11, "offset":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v16, v0

    add-int v17, v11, v13

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v11, v1}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 1872
    iget v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    shr-int/lit8 v15, v15, 0x18

    iget v0, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_prefix_:I

    move/from16 v16, v0

    shr-int/lit8 v16, v16, 0x18

    sub-int v13, v15, v16

    .line 1873
    iget v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    const v16, 0xffffff

    and-int v15, v15, v16

    iget v0, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_prefix_:I

    move/from16 v16, v0

    shr-int/lit8 v16, v16, 0x18

    add-int v11, v15, v16

    .line 1874
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v16, v0

    add-int v17, v11, v13

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v11, v1}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    .line 1884
    :goto_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 1886
    const/4 v2, 0x0

    .line 1887
    .local v2, "containCombinMarks":Z
    const/4 v7, 0x0

    .line 1888
    .local v7, "i":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    move/from16 v16, v0

    sub-int v15, v15, v16

    if-ge v7, v15, :cond_7

    .line 1889
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v15, v7}, Ljava/lang/String;->charAt(I)C

    move-result v15

    invoke-static {v15}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isJamo(C)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 1890
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    const/16 v16, 0x1

    move/from16 v0, v16

    iput-boolean v0, v15, Lcom/ibm/icu/text/RuleBasedCollator;->m_isJamoSpecial_:Z

    .line 1906
    :cond_7
    sget-boolean v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->buildCMTabFlag:Z

    if-nez v15, :cond_8

    if-eqz v2, :cond_8

    .line 1907
    const/4 v15, 0x1

    sput-boolean v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->buildCMTabFlag:Z

    .line 1929
    :cond_8
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addAnElement(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    .line 1930
    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1931
    goto/16 :goto_0

    .line 1878
    .end local v2    # "containCombinMarks":Z
    .end local v7    # "i":I
    .end local v11    # "offset":I
    .end local v13    # "size":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/16 v16, 0x0

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 1879
    iget v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    const v16, 0xffffff

    and-int v11, v15, v16

    .line 1880
    .restart local v11    # "offset":I
    iget v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    ushr-int/lit8 v13, v15, 0x18

    .line 1881
    .restart local v13    # "size":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v16, v0

    add-int v17, v11, v13

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v11, v1}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    goto/16 :goto_5

    .line 1893
    .restart local v2    # "containCombinMarks":Z
    .restart local v7    # "i":I
    :cond_a
    sget-boolean v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->buildCMTabFlag:Z

    if-nez v15, :cond_b

    .line 1895
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v15, v7}, Ljava/lang/String;->charAt(I)C

    move-result v15

    invoke-static {v15}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v6

    .line 1896
    .local v6, "fcd":C
    and-int/lit16 v15, v6, 0xff

    if-nez v15, :cond_c

    .line 1898
    const/4 v2, 0x0

    .line 1888
    .end local v6    # "fcd":C
    :cond_b
    :goto_7
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_6

    .line 1901
    .restart local v6    # "fcd":C
    :cond_c
    const/4 v2, 0x1

    goto :goto_7

    .line 1932
    .end local v2    # "containCombinMarks":Z
    .end local v6    # "fcd":C
    .end local v7    # "i":I
    .end local v11    # "offset":I
    .end local v13    # "size":I
    :cond_d
    return-void
.end method

.method private static decWeightTrail(II)I
    .locals 2
    .param p0, "weight"    # I
    .param p1, "length"    # I

    .prologue
    .line 3538
    const/4 v0, 0x1

    rsub-int/lit8 v1, p1, 0x4

    shl-int/lit8 v1, v1, 0x3

    shl-int/2addr v0, v1

    sub-int v0, p0, v0

    return v0
.end method

.method private doCE([ILcom/ibm/icu/text/CollationRuleParser$Token;)V
    .locals 12
    .param p1, "ceparts"    # [I
    .param p2, "token"    # Lcom/ibm/icu/text/CollationRuleParser$Token;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v11, 0x2

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 1699
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v7, 0x3

    if-ge v2, v7, :cond_0

    .line 1701
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    aget v8, p1, v2

    invoke-static {v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->countBytes(I)I

    move-result v8

    aput v8, v7, v2

    .line 1699
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1705
    :cond_0
    const/4 v1, 0x0

    .line 1706
    .local v1, "cei":I
    const/4 v6, 0x0

    .line 1709
    .local v6, "value":I
    :goto_1
    shl-int/lit8 v7, v1, 0x1

    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    aget v8, v8, v10

    if-lt v7, v8, :cond_1

    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    aget v7, v7, v9

    if-lt v1, v7, :cond_1

    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    aget v7, v7, v11

    if-ge v1, v7, :cond_6

    .line 1710
    :cond_1
    if-lez v1, :cond_5

    .line 1711
    const/16 v6, 0xc0

    .line 1716
    :goto_2
    shl-int/lit8 v7, v1, 0x1

    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    aget v8, v8, v10

    if-ge v7, v8, :cond_2

    .line 1717
    aget v7, p1, v10

    add-int/lit8 v8, v1, 0x1

    shl-int/lit8 v8, v8, 0x4

    rsub-int/lit8 v8, v8, 0x20

    shr-int/2addr v7, v8

    const v8, 0xffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x10

    or-int/2addr v6, v7

    .line 1720
    :cond_2
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    aget v7, v7, v9

    if-ge v1, v7, :cond_3

    .line 1721
    aget v7, p1, v9

    add-int/lit8 v8, v1, 0x1

    shl-int/lit8 v8, v8, 0x3

    rsub-int/lit8 v8, v8, 0x20

    shr-int/2addr v7, v8

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x8

    or-int/2addr v6, v7

    .line 1724
    :cond_3
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    aget v7, v7, v11

    if-ge v1, v7, :cond_4

    .line 1725
    aget v7, p1, v11

    add-int/lit8 v8, v1, 0x1

    shl-int/lit8 v8, v8, 0x3

    rsub-int/lit8 v8, v8, 0x20

    shr-int/2addr v7, v8

    and-int/lit8 v7, v7, 0x3f

    or-int/2addr v6, v7

    .line 1727
    :cond_4
    iget-object v7, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CE_:[I

    aput v6, v7, v1

    .line 1728
    add-int/lit8 v1, v1, 0x1

    .line 1729
    goto :goto_1

    .line 1713
    :cond_5
    const/4 v6, 0x0

    goto :goto_2

    .line 1730
    :cond_6
    if-nez v1, :cond_8

    .line 1731
    iput v9, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CELength_:I

    .line 1732
    iget-object v7, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CE_:[I

    aput v10, v7, v10

    .line 1739
    :goto_3
    iget-object v7, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CE_:[I

    aget v7, v7, v10

    if-eqz v7, :cond_7

    .line 1740
    iget v7, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    and-int/lit16 v4, v7, 0xff

    .line 1741
    .local v4, "startoftokenrule":I
    iget v7, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    ushr-int/lit8 v7, v7, 0x18

    if-le v7, v9, :cond_9

    .line 1743
    iget v7, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    ushr-int/lit8 v3, v7, 0x18

    .line 1744
    .local v3, "length":I
    iget-object v7, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_rules_:Ljava/lang/StringBuffer;

    add-int v8, v4, v3

    invoke-virtual {v7, v4, v8}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1746
    .local v5, "tokenstr":Ljava/lang/String;
    iget-object v7, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CE_:[I

    aget v8, v7, v10

    invoke-direct {p0, v5}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCaseBits(Ljava/lang/String;)I

    move-result v9

    or-int/2addr v8, v9

    aput v8, v7, v10

    .line 1755
    .end local v3    # "length":I
    .end local v4    # "startoftokenrule":I
    .end local v5    # "tokenstr":Ljava/lang/String;
    :cond_7
    :goto_4
    return-void

    .line 1735
    :cond_8
    iput v1, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CELength_:I

    goto :goto_3

    .line 1750
    .restart local v4    # "startoftokenrule":I
    :cond_9
    iget-object v7, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_rules_:Ljava/lang/StringBuffer;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v7

    invoke-direct {p0, v7}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getFirstCE(C)I

    move-result v0

    .line 1752
    .local v0, "caseCE":I
    iget-object v7, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CE_:[I

    aget v8, v7, v10

    and-int/lit16 v9, v0, 0xc0

    or-int/2addr v8, v9

    aput v8, v7, v10

    goto :goto_4
.end method

.method private enumCategoryRangeClosureCategory(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/CollationElementIterator;III)Z
    .locals 11
    .param p1, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
    .param p2, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p3, "colEl"    # Lcom/ibm/icu/text/CollationElementIterator;
    .param p4, "start"    # I
    .param p5, "limit"    # I
    .param p6, "type"    # I

    .prologue
    .line 3934
    if-eqz p6, :cond_3

    const/16 v7, 0x11

    move/from16 v0, p6

    if-eq v0, v7, :cond_3

    .line 3938
    move v6, p4

    .local v6, "u32":I
    :goto_0
    move/from16 v0, p5

    if-ge v6, v0, :cond_3

    .line 3939
    const/4 v7, 0x0

    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCharBuffer_:[C

    const/4 v9, 0x0

    const/16 v10, 0x100

    invoke-static {v6, v7, v8, v9, v10}, Lcom/ibm/icu/impl/NormalizerImpl;->getDecomposition(IZ[CII)I

    move-result v4

    .line 3942
    .local v4, "noOfDec":I
    if-lez v4, :cond_2

    .line 3944
    invoke-static {v6}, Lcom/ibm/icu/lang/UCharacter;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 3945
    .local v2, "comp":Ljava/lang/String;
    new-instance v3, Ljava/lang/String;

    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCharBuffer_:[C

    const/4 v8, 0x0

    invoke-direct {v3, v7, v8, v4}, Ljava/lang/String;-><init>([CII)V

    .line 3946
    .local v3, "decomp":Ljava/lang/String;
    invoke-virtual {p2, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 3947
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iput-object v3, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 3948
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v8, 0x0

    iput v8, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 3949
    iget-object v7, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_prefixLookup_:Ljava/util/Hashtable;

    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-virtual {v7, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .line 3951
    .local v5, "prefix":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    if-nez v5, :cond_0

    .line 3952
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iput-object v2, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 3953
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v8, 0x0

    iput v8, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 3954
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v8, 0x0

    iput-object v8, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 3955
    invoke-virtual {p3, v3}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 3956
    invoke-virtual {p3}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v1

    .line 3957
    .local v1, "ce":I
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v8, 0x0

    iput v8, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 3958
    :goto_1
    const/4 v7, -0x1

    if-eq v1, v7, :cond_1

    .line 3959
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v7, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget v9, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    add-int/lit8 v10, v9, 0x1

    iput v10, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    aput v1, v7, v9

    .line 3962
    invoke-virtual {p3}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v1

    .line 3963
    goto :goto_1

    .line 3966
    .end local v1    # "ce":I
    :cond_0
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iput-object v2, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 3967
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v8, 0x0

    iput v8, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 3968
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v8, 0x0

    iput-object v8, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 3969
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v8, 0x1

    iput v8, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 3970
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v7, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    const/4 v8, 0x0

    iget v9, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    aput v9, v7, v8

    .line 3979
    :cond_1
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-direct {p0, p1, v7}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addAnElement(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    .line 3938
    .end local v2    # "comp":Ljava/lang/String;
    .end local v3    # "decomp":Ljava/lang/String;
    .end local v5    # "prefix":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 3984
    .end local v4    # "noOfDec":I
    .end local v6    # "u32":I
    :cond_3
    const/4 v7, 0x1

    return v7
.end method

.method private static final finalizeAddition(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I
    .locals 10
    .param p0, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
    .param p1, "element"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2873
    const/high16 v0, -0x10000000

    .line 2877
    .local v0, "CE":I
    iget v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    if-nez v5, :cond_1

    .line 2878
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_1

    .line 2879
    iget-object v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 2880
    .local v1, "ch":C
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2881
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    invoke-static {v5, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->unsafeCPSet([BC)V

    .line 2878
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2886
    .end local v1    # "ch":C
    .end local v3    # "i":I
    :cond_1
    iget-object v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    iget v6, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    sub-int/2addr v5, v6

    if-le v5, v9, :cond_3

    .line 2888
    iget-object v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v6, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-static {v5, v6}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v2

    .line 2889
    .local v2, "cp":I
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    invoke-virtual {v5, v2}, Lcom/ibm/icu/impl/IntTrieBuilder;->getValue(I)I

    move-result v0

    .line 2890
    invoke-static {p0, v0, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;ILcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    move-result v0

    .line 2938
    .end local v2    # "cp":I
    :cond_2
    :goto_1
    return v0

    .line 2894
    :cond_3
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    iget-object v6, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v7, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-virtual {v5, v6}, Lcom/ibm/icu/impl/IntTrieBuilder;->getValue(I)I

    move-result v0

    .line 2897
    const/high16 v5, -0x10000000

    if-eq v0, v5, :cond_5

    .line 2898
    invoke-static {v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isContractionTableElement(I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2901
    iget v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    invoke-static {v5}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isPrefix(I)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2906
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contractions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    iget v6, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    invoke-static {v5, v0, v8, v8, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;IICI)I

    .line 2910
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contractions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    iget v6, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    invoke-static {v5, v0, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->changeLastCE(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;II)I

    goto :goto_1

    .line 2914
    :cond_4
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    iget-object v6, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v7, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    iget v7, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    invoke-virtual {v5, v6, v7}, Lcom/ibm/icu/impl/IntTrieBuilder;->setValue(II)Z

    .line 2917
    iget-object v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    invoke-static {v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCETag(I)I

    move-result v5

    const/16 v6, 0xa

    if-eq v5, v6, :cond_2

    .line 2921
    new-instance v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-direct {v4}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;-><init>()V

    .line 2922
    .local v4, "origElem":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    const/4 v5, 0x0

    iput-object v5, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 2923
    iget-object v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iput-object v5, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    .line 2924
    iget-object v5, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    iput-object v5, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 2925
    iget-object v5, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aput v0, v5, v8

    .line 2926
    iput v0, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 2927
    iput v9, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 2928
    invoke-static {p0, v4}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->finalizeAddition(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    goto :goto_1

    .line 2933
    .end local v4    # "origElem":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    :cond_5
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    iget-object v6, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v7, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    iget v7, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    invoke-virtual {v5, v6, v7}, Lcom/ibm/icu/impl/IntTrieBuilder;->setValue(II)Z

    goto :goto_1
.end method

.method private static findCE(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;IC)I
    .locals 4
    .param p0, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;
    .param p1, "element"    # I
    .param p2, "ch"    # C

    .prologue
    const/high16 v2, -0x10000000

    .line 3573
    if-nez p0, :cond_1

    .line 3584
    :cond_0
    :goto_0
    return v2

    .line 3576
    :cond_1
    invoke-static {p0, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getBasicContractionTable(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;I)Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    move-result-object v1

    .line 3577
    .local v1, "tbl":Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;
    if-eqz v1, :cond_0

    .line 3580
    invoke-static {v1, p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->findCP(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;C)I

    move-result v0

    .line 3581
    .local v0, "position":I
    iget-object v3, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_CEs_:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-gt v0, v3, :cond_0

    if-ltz v0, :cond_0

    .line 3584
    iget-object v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_CEs_:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0
.end method

.method private static findCP(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;C)I
    .locals 3
    .param p0, "tbl"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;
    .param p1, "codePoint"    # C

    .prologue
    const/4 v1, -0x1

    .line 3549
    const/4 v0, 0x0

    .line 3550
    .local v0, "position":I
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    if-le p1, v2, :cond_2

    .line 3551
    add-int/lit8 v0, v0, 0x1

    .line 3552
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-le v0, v2, :cond_0

    .line 3560
    :cond_1
    :goto_0
    return v1

    .line 3556
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    if-ne p1, v2, :cond_1

    move v1, v0

    .line 3557
    goto :goto_0
.end method

.method private static findCP(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;IC)I
    .locals 4
    .param p0, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;
    .param p1, "element"    # I
    .param p2, "codePoint"    # C

    .prologue
    const/4 v2, -0x1

    .line 2739
    invoke-static {p0, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getBasicContractionTable(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;I)Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    move-result-object v1

    .line 2740
    .local v1, "tbl":Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;
    if-nez v1, :cond_1

    move v0, v2

    .line 2755
    :cond_0
    :goto_0
    return v0

    .line 2744
    :cond_1
    const/4 v0, 0x0

    .line 2745
    .local v0, "position":I
    :cond_2
    iget-object v3, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    if-le p2, v3, :cond_3

    .line 2746
    add-int/lit8 v0, v0, 0x1

    .line 2747
    iget-object v3, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-le v0, v3, :cond_2

    move v0, v2

    .line 2748
    goto :goto_0

    .line 2751
    :cond_3
    iget-object v3, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    if-eq p2, v3, :cond_0

    move v0, v2

    .line 2755
    goto :goto_0
.end method

.method private static final getBasicContractionTable(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;I)Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;
    .locals 1
    .param p0, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;
    .param p1, "offset"    # I

    .prologue
    const v0, 0xffffff

    .line 2769
    and-int/2addr p1, v0

    .line 2770
    if-ne p1, v0, :cond_0

    .line 2771
    const/4 v0, 0x0

    .line 2773
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_elements_:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    goto :goto_0
.end method

.method private static final getCE(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;II)I
    .locals 3
    .param p0, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;
    .param p1, "element"    # I
    .param p2, "position"    # I

    .prologue
    const/high16 v1, -0x10000000

    .line 2543
    const v2, 0xffffff

    and-int/2addr p1, v2

    .line 2544
    invoke-static {p0, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getBasicContractionTable(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;I)Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    move-result-object v0

    .line 2546
    .local v0, "tbl":Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;
    if-nez v0, :cond_1

    .line 2553
    :cond_0
    :goto_0
    return v1

    .line 2549
    :cond_1
    iget-object v2, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_CEs_:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-gt p2, v2, :cond_0

    const/4 v2, -0x1

    if-eq p2, v2, :cond_0

    .line 2553
    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_CEs_:Ljava/util/Vector;

    invoke-virtual {v1, p2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method private getCEGenerator(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;[I[ILcom/ibm/icu/text/CollationRuleParser$Token;I)I
    .locals 9
    .param p1, "g"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;
    .param p2, "lows"    # [I
    .param p3, "highs"    # [I
    .param p4, "token"    # Lcom/ibm/icu/text/CollationRuleParser$Token;
    .param p5, "fstrength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1612
    iget v8, p4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    .line 1613
    .local v8, "strength":I
    mul-int/lit8 v0, p5, 0x3

    add-int/2addr v0, v8

    aget v1, p2, v0

    .line 1614
    .local v1, "low":I
    mul-int/lit8 v0, p5, 0x3

    add-int/2addr v0, v8

    aget v2, p3, v0

    .line 1615
    .local v2, "high":I
    const/4 v4, 0x0

    .line 1616
    .local v4, "maxbyte":I
    const/4 v0, 0x2

    if-ne v8, v0, :cond_5

    .line 1617
    const/16 v4, 0x3f

    .line 1624
    :goto_0
    iget v3, p4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_toInsert_:I

    .line 1626
    .local v3, "count":I
    invoke-static {v1, v2}, Lcom/ibm/icu/impl/Utility;->compareUnsigned(II)I

    move-result v0

    if-ltz v0, :cond_2

    if-lez v8, :cond_2

    .line 1628
    move v7, v8

    .line 1630
    .local v7, "s":I
    :cond_0
    add-int/lit8 v7, v7, -0x1

    .line 1631
    mul-int/lit8 v0, p5, 0x3

    add-int/2addr v0, v7

    aget v0, p2, v0

    mul-int/lit8 v5, p5, 0x3

    add-int/2addr v5, v7

    aget v5, p3, v5

    if-eq v0, v5, :cond_9

    .line 1632
    const/4 v0, 0x1

    if-ne v8, v0, :cond_7

    .line 1633
    const/high16 v0, -0x7a000000

    if-ge v1, v0, :cond_1

    .line 1635
    const/high16 v1, -0x7a000000

    .line 1637
    :cond_1
    const/4 v2, -0x1

    .line 1653
    .end local v7    # "s":I
    :cond_2
    :goto_1
    if-nez v1, :cond_3

    .line 1654
    const/high16 v1, 0x1000000

    .line 1656
    :cond_3
    const/4 v0, 0x1

    if-ne v8, v0, :cond_b

    .line 1657
    const/high16 v0, 0x5000000

    invoke-static {v1, v0}, Lcom/ibm/icu/impl/Utility;->compareUnsigned(II)I

    move-result v0

    if-ltz v0, :cond_d

    const/high16 v0, -0x7a000000

    invoke-static {v1, v0}, Lcom/ibm/icu/impl/Utility;->compareUnsigned(II)I

    move-result v0

    if-gez v0, :cond_d

    .line 1661
    const/high16 v1, -0x7a000000

    move v6, v1

    .line 1663
    .end local v1    # "low":I
    .local v6, "low":I
    :goto_2
    const/high16 v0, 0x5000000

    invoke-static {v2, v0}, Lcom/ibm/icu/impl/Utility;->compareUnsigned(II)I

    move-result v0

    if-lez v0, :cond_4

    const/high16 v0, -0x7a000000

    invoke-static {v2, v0}, Lcom/ibm/icu/impl/Utility;->compareUnsigned(II)I

    move-result v0

    if-gez v0, :cond_4

    .line 1667
    const/high16 v2, -0x7a000000

    .line 1669
    :cond_4
    const/high16 v0, 0x5000000

    invoke-static {v6, v0}, Lcom/ibm/icu/impl/Utility;->compareUnsigned(II)I

    move-result v0

    if-gez v0, :cond_a

    .line 1671
    const/high16 v1, 0x3000000

    iget-object v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_ranges_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->allocateWeights(IIII[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;)I

    move-result v0

    iput v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_rangesLength_:I

    .line 1674
    invoke-static {p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->nextWeight(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;)I

    move-result v0

    iput v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_current_:I

    .line 1676
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_current_:I

    move v1, v6

    .line 1686
    .end local v6    # "low":I
    .restart local v1    # "low":I
    :goto_3
    return v0

    .line 1618
    .end local v3    # "count":I
    :cond_5
    if-nez v8, :cond_6

    .line 1619
    const/16 v4, 0xfe

    .line 1620
    goto :goto_0

    .line 1621
    :cond_6
    const/16 v4, 0xff

    goto :goto_0

    .line 1640
    .restart local v3    # "count":I
    .restart local v7    # "s":I
    :cond_7
    const/high16 v0, 0x5000000

    if-ge v1, v0, :cond_8

    .line 1642
    const/high16 v1, 0x5000000

    .line 1644
    :cond_8
    const/high16 v2, 0x40000000    # 2.0f

    .line 1646
    goto :goto_1

    .line 1648
    :cond_9
    if-gez v7, :cond_0

    .line 1649
    new-instance v0, Ljava/lang/Exception;

    const-string/jumbo v5, "Internal program error"

    invoke-direct {v0, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .end local v1    # "low":I
    .end local v7    # "s":I
    .restart local v6    # "low":I
    :cond_a
    move v1, v6

    .line 1680
    .end local v6    # "low":I
    .restart local v1    # "low":I
    :cond_b
    iget-object v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_ranges_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->allocateWeights(IIII[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;)I

    move-result v0

    iput v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_rangesLength_:I

    .line 1682
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_rangesLength_:I

    if-nez v0, :cond_c

    .line 1683
    new-instance v0, Ljava/lang/Exception;

    const-string/jumbo v5, "Internal program error"

    invoke-direct {v0, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1685
    :cond_c
    invoke-static {p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->nextWeight(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;)I

    move-result v0

    iput v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_current_:I

    .line 1686
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_current_:I

    goto :goto_3

    :cond_d
    move v6, v1

    .end local v1    # "low":I
    .restart local v6    # "low":I
    goto :goto_2
.end method

.method private static final getCETag(I)I
    .locals 1
    .param p0, "CE"    # I

    .prologue
    .line 2530
    const/high16 v0, 0xf000000

    and-int/2addr v0, p0

    ushr-int/lit8 v0, v0, 0x18

    return v0
.end method

.method private final getCaseBits(Ljava/lang/String;)I
    .locals 8
    .param p1, "src"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/16 v5, 0x80

    .line 1942
    const/4 v4, 0x0

    .line 1943
    .local v4, "uCount":I
    const/4 v2, 0x0

    .line 1944
    .local v2, "lCount":I
    const/4 v6, 0x1

    invoke-static {p1, v6}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    .line 1945
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6, p1}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 1946
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v1, v6, :cond_4

    .line 1947
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    add-int/lit8 v7, v1, 0x1

    invoke-virtual {p1, v1, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 1948
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v3

    .line 1949
    .local v3, "order":I
    invoke-static {v3}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1950
    new-instance v5, Ljava/lang/Exception;

    const-string/jumbo v6, "Internal program error"

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1952
    :cond_0
    and-int/lit16 v6, v3, 0xc0

    if-ne v6, v5, :cond_2

    .line 1954
    add-int/lit8 v4, v4, 0x1

    .line 1946
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1957
    :cond_2
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1958
    .local v0, "ch":C
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->isLowerCase(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1959
    add-int/lit8 v2, v2, 0x1

    .line 1960
    goto :goto_1

    .line 1962
    :cond_3
    invoke-static {v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->toSmallKana(C)C

    move-result v6

    if-ne v6, v0, :cond_1

    invoke-static {v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->toLargeKana(C)C

    move-result v6

    if-eq v6, v0, :cond_1

    .line 1963
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1969
    .end local v0    # "ch":C
    .end local v3    # "order":I
    :cond_4
    if-eqz v4, :cond_6

    if-eqz v2, :cond_6

    .line 1970
    const/16 v5, 0x40

    .line 1976
    :cond_5
    :goto_2
    return v5

    .line 1972
    :cond_6
    if-nez v4, :cond_5

    .line 1976
    const/4 v5, 0x0

    goto :goto_2
.end method

.method private static final getContractionOffset(I)I
    .locals 1
    .param p0, "ce"    # I

    .prologue
    .line 3799
    const v0, 0xffffff

    and-int/2addr v0, p0

    return v0
.end method

.method private getFirstCE(C)I
    .locals 2
    .param p1, "ch"    # C

    .prologue
    .line 2065
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 2066
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v0

    return v0
.end method

.method private static getMaxExpansionJamo(Lcom/ibm/icu/impl/IntTrieBuilder;Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;Z)V
    .locals 12
    .param p0, "mapping"    # Lcom/ibm/icu/impl/IntTrieBuilder;
    .param p1, "maxexpansion"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;
    .param p2, "maxjamoexpansion"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;
    .param p3, "jamospecial"    # Z

    .prologue
    const/high16 v11, -0x10000000

    .line 3815
    const/16 v2, 0x1161

    .line 3816
    .local v2, "VBASE":I
    const/16 v0, 0x11a8

    .line 3817
    .local v0, "TBASE":I
    const/16 v3, 0x15

    .line 3818
    .local v3, "VCOUNT":I
    const/16 v1, 0x1c

    .line 3819
    .local v1, "TCOUNT":I
    const/16 v10, 0x1176

    add-int/lit8 v9, v10, -0x1

    .line 3820
    .local v9, "v":I
    const/16 v10, 0x11c4

    add-int/lit8 v8, v10, -0x1

    .line 3822
    .local v8, "t":I
    :goto_0
    if-lt v9, v2, :cond_1

    .line 3823
    invoke-virtual {p0, v9}, Lcom/ibm/icu/impl/IntTrieBuilder;->getValue(I)I

    move-result v4

    .line 3824
    .local v4, "ce":I
    and-int v10, v4, v11

    if-eq v10, v11, :cond_0

    .line 3826
    const/4 v10, 0x2

    invoke-static {v4, v10, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setMaxExpansion(IBLcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;)I

    .line 3828
    :cond_0
    add-int/lit8 v9, v9, -0x1

    .line 3829
    goto :goto_0

    .line 3831
    .end local v4    # "ce":I
    :cond_1
    :goto_1
    if-lt v8, v0, :cond_3

    .line 3833
    invoke-virtual {p0, v8}, Lcom/ibm/icu/impl/IntTrieBuilder;->getValue(I)I

    move-result v4

    .line 3834
    .restart local v4    # "ce":I
    and-int v10, v4, v11

    if-eq v10, v11, :cond_2

    .line 3836
    const/4 v10, 0x3

    invoke-static {v4, v10, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setMaxExpansion(IBLcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;)I

    .line 3838
    :cond_2
    add-int/lit8 v8, v8, -0x1

    .line 3839
    goto :goto_1

    .line 3841
    .end local v4    # "ce":I
    :cond_3
    if-eqz p3, :cond_5

    .line 3843
    iget-object v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v5

    .line 3844
    .local v5, "count":I
    iget-byte v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxLSize_:B

    iget-byte v11, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxVSize_:B

    add-int/2addr v10, v11

    iget-byte v11, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxTSize_:B

    add-int/2addr v10, v11

    int-to-byte v6, v10

    .line 3847
    .local v6, "maxTSize":B
    iget-byte v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxLSize_:B

    iget-byte v11, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxVSize_:B

    add-int/2addr v10, v11

    int-to-byte v7, v10

    .line 3850
    .local v7, "maxVSize":B
    :goto_2
    if-lez v5, :cond_5

    .line 3851
    add-int/lit8 v5, v5, -0x1

    .line 3852
    iget-object v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_isV_:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_4

    .line 3854
    iget-object v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-static {v10, v7, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setMaxExpansion(IBLcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;)I

    goto :goto_2

    .line 3859
    :cond_4
    iget-object v10, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-static {v10, v6, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setMaxExpansion(IBLcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;)I

    goto :goto_2

    .line 3865
    .end local v5    # "count":I
    .end local v6    # "maxTSize":B
    .end local v7    # "maxVSize":B
    :cond_5
    return-void
.end method

.method private getNextGenerated(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;)I
    .locals 1
    .param p1, "g"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    .prologue
    .line 1528
    invoke-static {p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->nextWeight(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;)I

    move-result v0

    iput v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_current_:I

    .line 1529
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_current_:I

    return v0
.end method

.method private getSimpleCEGenerator(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;Lcom/ibm/icu/text/CollationRuleParser$Token;I)I
    .locals 6
    .param p1, "g"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;
    .param p2, "token"    # Lcom/ibm/icu/text/CollationRuleParser$Token;
    .param p3, "strength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1543
    const/4 v3, 0x1

    .line 1544
    .local v3, "count":I
    const/4 v0, 0x2

    if-ne p3, v0, :cond_1

    const/16 v4, 0x3f

    .line 1546
    .local v4, "maxbyte":I
    :goto_0
    const/4 v0, 0x1

    if-ne p3, v0, :cond_2

    .line 1547
    const/high16 v1, -0x7a000000

    .line 1548
    .local v1, "low":I
    const/4 v2, -0x1

    .line 1549
    .local v2, "high":I
    const/16 v3, 0x79

    .line 1557
    :goto_1
    iget-object v0, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    if-ne v0, p3, :cond_0

    .line 1558
    iget-object v0, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget v3, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_toInsert_:I

    .line 1561
    :cond_0
    iget-object v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_ranges_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->allocateWeights(IIII[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;)I

    move-result v0

    iput v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_rangesLength_:I

    .line 1563
    const/high16 v0, 0x5000000

    iput v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_current_:I

    .line 1565
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_rangesLength_:I

    if-nez v0, :cond_3

    .line 1566
    new-instance v0, Ljava/lang/Exception;

    const-string/jumbo v5, "Internal program error"

    invoke-direct {v0, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1544
    .end local v1    # "low":I
    .end local v2    # "high":I
    .end local v4    # "maxbyte":I
    :cond_1
    const/16 v4, 0xff

    goto :goto_0

    .line 1552
    .restart local v4    # "maxbyte":I
    :cond_2
    const/high16 v1, 0x5000000

    .line 1553
    .restart local v1    # "low":I
    const/high16 v2, 0x40000000    # 2.0f

    .line 1554
    .restart local v2    # "high":I
    const/16 v3, 0x3b

    goto :goto_1

    .line 1568
    :cond_3
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_current_:I

    return v0
.end method

.method private static final getWeightByte(II)I
    .locals 1
    .param p0, "weight"    # I
    .param p1, "index"    # I

    .prologue
    .line 3135
    rsub-int/lit8 v0, p1, 0x4

    shl-int/lit8 v0, v0, 0x3

    shr-int v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private getWeightRanges(IIII[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;)I
    .locals 12
    .param p1, "lowerLimit"    # I
    .param p2, "upperLimit"    # I
    .param p3, "maxByte"    # I
    .param p4, "countBytes"    # I
    .param p5, "ranges"    # [Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    .prologue
    .line 3348
    invoke-static {p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->lengthOfWeight(I)I

    move-result v2

    .line 3349
    .local v2, "lowerLength":I
    invoke-static {p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->lengthOfWeight(I)I

    move-result v6

    .line 3350
    .local v6, "upperLength":I
    invoke-static {p1, p2}, Lcom/ibm/icu/impl/Utility;->compareUnsigned(II)I

    move-result v8

    if-ltz v8, :cond_1

    .line 3351
    const/4 v3, 0x0

    .line 3486
    :cond_0
    :goto_0
    return v3

    .line 3354
    :cond_1
    if-ge v2, v6, :cond_2

    .line 3355
    invoke-static {p2, v2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->truncateWeight(II)I

    move-result v8

    if-ne p1, v8, :cond_2

    .line 3356
    const/4 v3, 0x0

    goto :goto_0

    .line 3378
    :cond_2
    const/4 v1, 0x0

    .local v1, "length":I
    :goto_1
    const/4 v8, 0x5

    if-ge v1, v8, :cond_3

    .line 3379
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    invoke-virtual {v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->clear()V

    .line 3380
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilUpperWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    invoke-virtual {v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->clear()V

    .line 3378
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3382
    :cond_3
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilWeightRange_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    invoke-virtual {v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->clear()V

    .line 3384
    move v7, p1

    .line 3385
    .local v7, "weight":I
    move v1, v2

    :goto_2
    const/4 v8, 0x2

    if-lt v1, v8, :cond_5

    .line 3386
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    invoke-virtual {v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->clear()V

    .line 3387
    invoke-static {v7, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getWeightByte(II)I

    move-result v5

    .line 3388
    .local v5, "trail":I
    if-ge v5, p3, :cond_4

    .line 3389
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    invoke-static {v7, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->incWeightTrail(II)I

    move-result v9

    iput v9, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    .line 3391
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    invoke-static {v7, v1, p3}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setWeightTrail(III)I

    move-result v9

    iput v9, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    .line 3393
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    iput v1, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length_:I

    .line 3394
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    sub-int v9, p3, v5

    iput v9, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    .line 3396
    :cond_4
    add-int/lit8 v8, v1, -0x1

    invoke-static {v7, v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->truncateWeight(II)I

    move-result v7

    .line 3385
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 3398
    .end local v5    # "trail":I
    :cond_5
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilWeightRange_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    const/4 v9, 0x1

    invoke-static {v7, v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->incWeightTrail(II)I

    move-result v9

    iput v9, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    .line 3400
    move v7, p2

    .line 3404
    move v1, v6

    :goto_3
    const/4 v8, 0x2

    if-lt v1, v8, :cond_7

    .line 3405
    invoke-static {v7, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getWeightByte(II)I

    move-result v5

    .line 3406
    .restart local v5    # "trail":I
    const/4 v8, 0x4

    if-le v5, v8, :cond_6

    .line 3407
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilUpperWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    const/4 v9, 0x4

    invoke-static {v7, v1, v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setWeightTrail(III)I

    move-result v9

    iput v9, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    .line 3410
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilUpperWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    invoke-static {v7, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->decWeightTrail(II)I

    move-result v9

    iput v9, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    .line 3412
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilUpperWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    iput v1, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length_:I

    .line 3413
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilUpperWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    add-int/lit8 v9, v5, -0x4

    iput v9, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    .line 3416
    :cond_6
    add-int/lit8 v8, v1, -0x1

    invoke-static {v7, v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->truncateWeight(II)I

    move-result v7

    .line 3404
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 3418
    .end local v5    # "trail":I
    :cond_7
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilWeightRange_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    const/4 v9, 0x1

    invoke-static {v7, v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->decWeightTrail(II)I

    move-result v9

    iput v9, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    .line 3421
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilWeightRange_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    const/4 v9, 0x1

    iput v9, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length_:I

    .line 3422
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilWeightRange_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    iget v8, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    iget-object v9, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilWeightRange_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    iget v9, v9, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    invoke-static {v8, v9}, Lcom/ibm/icu/impl/Utility;->compareUnsigned(II)I

    move-result v8

    if-ltz v8, :cond_c

    .line 3424
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilWeightRange_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    iget-object v9, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilWeightRange_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    iget v9, v9, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    iget-object v10, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilWeightRange_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    iget v10, v10, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    sub-int/2addr v9, v10

    ushr-int/lit8 v9, v9, 0x18

    add-int/lit8 v9, v9, 0x1

    iput v9, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    .line 3467
    :cond_8
    const/4 v3, 0x0

    .line 3468
    .local v3, "rangeCount":I
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilWeightRange_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    iget v8, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    if-lez v8, :cond_9

    .line 3469
    const/4 v8, 0x0

    new-instance v9, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    iget-object v10, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilWeightRange_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    invoke-direct {v9, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;-><init>(Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;)V

    aput-object v9, p5, v8

    .line 3470
    const/4 v3, 0x1

    .line 3472
    :cond_9
    const/4 v1, 0x2

    :goto_4
    const/4 v8, 0x4

    if-gt v1, v8, :cond_0

    .line 3475
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilUpperWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    iget v8, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    if-lez v8, :cond_a

    .line 3476
    new-instance v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    iget-object v9, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilUpperWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v9, v9, v1

    invoke-direct {v8, v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;-><init>(Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;)V

    aput-object v8, p5, v3

    .line 3478
    add-int/lit8 v3, v3, 0x1

    .line 3480
    :cond_a
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    iget v8, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    if-lez v8, :cond_b

    .line 3481
    new-instance v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    iget-object v9, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v9, v9, v1

    invoke-direct {v8, v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;-><init>(Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;)V

    aput-object v8, p5, v3

    .line 3483
    add-int/lit8 v3, v3, 0x1

    .line 3472
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 3431
    .end local v3    # "rangeCount":I
    :cond_c
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilWeightRange_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    const/4 v9, 0x0

    iput v9, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    .line 3433
    const/4 v1, 0x4

    :goto_5
    const/4 v8, 0x2

    if-lt v1, v8, :cond_8

    .line 3434
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    iget v8, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    if-lez v8, :cond_e

    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilUpperWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    iget v8, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    if-lez v8, :cond_e

    .line 3436
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilUpperWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    iget v4, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    .line 3437
    .local v4, "start":I
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    iget v0, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    .line 3438
    .local v0, "end":I
    if-ge v0, v4, :cond_d

    invoke-static {v0, v1, p3}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->incWeight(III)I

    move-result v8

    if-ne v8, v4, :cond_e

    .line 3443
    :cond_d
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    iget v4, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    .line 3444
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    iget-object v9, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilUpperWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v9, v9, v1

    iget v0, v9, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    .end local v0    # "end":I
    iput v0, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    .line 3449
    .restart local v0    # "end":I
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    invoke-static {v0, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getWeightByte(II)I

    move-result v9

    invoke-static {v4, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getWeightByte(II)I

    move-result v10

    sub-int/2addr v9, v10

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v10, v1, -0x1

    invoke-static {v0, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getWeightByte(II)I

    move-result v10

    add-int/lit8 v11, v1, -0x1

    invoke-static {v4, v11}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getWeightByte(II)I

    move-result v11

    sub-int/2addr v10, v11

    mul-int v10, v10, p4

    add-int/2addr v9, v10

    iput v9, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    .line 3455
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilUpperWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    const/4 v9, 0x0

    iput v9, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    .line 3456
    :goto_6
    add-int/lit8 v1, v1, -0x1

    const/4 v8, 0x2

    if-lt v1, v8, :cond_8

    .line 3457
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilLowerWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v8, v8, v1

    iget-object v9, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilUpperWeightRange_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v9, v9, v1

    const/4 v10, 0x0

    iput v10, v9, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    iput v10, v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    goto :goto_6

    .line 3433
    .end local v0    # "end":I
    .end local v4    # "start":I
    :cond_e
    add-int/lit8 v1, v1, -0x1

    goto :goto_5
.end method

.method private static final incWeight(III)I
    .locals 2
    .param p0, "weight"    # I
    .param p1, "length"    # I
    .param p2, "maxByte"    # I

    .prologue
    .line 3113
    :goto_0
    invoke-static {p0, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getWeightByte(II)I

    move-result v0

    .line 3114
    .local v0, "b":I
    if-ge v0, p2, :cond_0

    .line 3115
    add-int/lit8 v1, v0, 0x1

    invoke-static {p0, p1, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setWeightByte(III)I

    move-result v1

    return v1

    .line 3120
    :cond_0
    const/4 v1, 0x4

    invoke-static {p0, p1, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setWeightByte(III)I

    move-result p0

    .line 3122
    add-int/lit8 p1, p1, -0x1

    .line 3124
    goto :goto_0
.end method

.method private static final incWeightTrail(II)I
    .locals 2
    .param p0, "weight"    # I
    .param p1, "length"    # I

    .prologue
    .line 3527
    const/4 v0, 0x1

    rsub-int/lit8 v1, p1, 0x4

    shl-int/lit8 v1, v1, 0x3

    shl-int/2addr v0, v1

    add-int/2addr v0, p0

    return v0
.end method

.method private initBuffers(Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;)V
    .locals 12
    .param p1, "listheader"    # Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v11, -0x1

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x2

    .line 1393
    iget-object v4, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_last_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1394
    .local v4, "token":Lcom/ibm/icu/text/CollationRuleParser$Token;
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    const/16 v1, 0x10

    invoke-static {v0, v10, v1, v10}, Ljava/util/Arrays;->fill([IIII)V

    .line 1396
    iput v9, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_toInsert_:I

    .line 1397
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    iget v1, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    aput v9, v0, v1

    .line 1398
    :goto_0
    iget-object v0, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    if-eqz v0, :cond_2

    .line 1399
    iget-object v0, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    iget v1, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    if-ge v0, v1, :cond_0

    .line 1401
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    iget v1, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    aput v10, v0, v1

    .line 1402
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    iget-object v1, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget v1, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 1411
    :goto_1
    iget-object v4, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1412
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    iget v1, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    aget v0, v0, v1

    iput v0, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_toInsert_:I

    goto :goto_0

    .line 1404
    :cond_0
    iget-object v0, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    iget v1, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    if-le v0, v1, :cond_1

    .line 1406
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    iget-object v1, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget v1, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    aput v9, v0, v1

    goto :goto_1

    .line 1409
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    iget v1, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    goto :goto_1

    .line 1415
    :cond_2
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilIntBuffer_:[I

    iget v1, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    aget v0, v0, v1

    iput v0, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_toInsert_:I

    .line 1416
    sget-object v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->INVERSE_UCA_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->getInverseGapPositions(Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;)V

    .line 1418
    iget-object v4, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1419
    const/16 v5, 0xf

    .line 1420
    .local v5, "fstrength":I
    const/16 v6, 0xf

    .line 1422
    .local v6, "initstrength":I
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget v1, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseCE_:I

    iget v2, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    invoke-static {v1, v2, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->mergeCE(III)I

    move-result v1

    aput v1, v0, v10

    .line 1425
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget v1, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseCE_:I

    iget v2, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    invoke-static {v1, v2, v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->mergeCE(III)I

    move-result v1

    aput v1, v0, v9

    .line 1428
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget v1, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseCE_:I

    iget v2, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    invoke-static {v1, v2, v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->mergeCE(III)I

    move-result v1

    aput v1, v0, v8

    .line 1431
    :goto_2
    if-eqz v4, :cond_b

    .line 1432
    iget v5, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    .line 1433
    if-ge v5, v6, :cond_8

    .line 1434
    move v6, v5

    .line 1435
    iget-object v0, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_pos_:[I

    aget v0, v0, v5

    if-ne v0, v11, :cond_4

    .line 1436
    :goto_3
    iget-object v0, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_pos_:[I

    aget v0, v0, v5

    if-ne v0, v11, :cond_3

    if-lez v5, :cond_3

    .line 1438
    add-int/lit8 v5, v5, -0x1

    .line 1439
    goto :goto_3

    .line 1440
    :cond_3
    iget-object v0, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_pos_:[I

    aget v0, v0, v5

    if-ne v0, v11, :cond_4

    .line 1441
    new-instance v0, Ljava/lang/Exception;

    const-string/jumbo v1, "Internal program error"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1444
    :cond_4
    if-ne v6, v8, :cond_6

    .line 1446
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v1, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    mul-int/lit8 v2, v5, 0x3

    aget v1, v1, v2

    aput v1, v0, v10

    .line 1448
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v1, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    mul-int/lit8 v2, v5, 0x3

    add-int/lit8 v2, v2, 0x1

    aget v1, v1, v2

    aput v1, v0, v9

    .line 1450
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilGens_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    aget-object v1, v0, v8

    iget-object v2, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    iget-object v3, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCEGenerator(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;[I[ILcom/ibm/icu/text/CollationRuleParser$Token;I)I

    move-result v0

    aput v0, v7, v8

    .line 1516
    :cond_5
    :goto_4
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    invoke-direct {p0, v0, v4}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->doCE([ILcom/ibm/icu/text/CollationRuleParser$Token;)V

    .line 1517
    iget-object v4, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1518
    goto :goto_2

    .line 1456
    :cond_6
    if-ne v6, v9, :cond_7

    .line 1458
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v1, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    mul-int/lit8 v2, v5, 0x3

    aget v1, v1, v2

    aput v1, v0, v10

    .line 1460
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilGens_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    aget-object v1, v0, v9

    iget-object v2, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    iget-object v3, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCEGenerator(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;[I[ILcom/ibm/icu/text/CollationRuleParser$Token;I)I

    move-result v0

    aput v0, v7, v9

    .line 1466
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilGens_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    aget-object v1, v1, v8

    invoke-direct {p0, v1, v4, v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getSimpleCEGenerator(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;Lcom/ibm/icu/text/CollationRuleParser$Token;I)I

    move-result v1

    aput v1, v0, v8

    goto :goto_4

    .line 1473
    :cond_7
    iget-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilGens_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    aget-object v1, v0, v10

    iget-object v2, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    iget-object v3, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCEGenerator(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;[I[ILcom/ibm/icu/text/CollationRuleParser$Token;I)I

    move-result v0

    aput v0, v7, v10

    .line 1479
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilGens_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    aget-object v1, v1, v9

    invoke-direct {p0, v1, v4, v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getSimpleCEGenerator(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;Lcom/ibm/icu/text/CollationRuleParser$Token;I)I

    move-result v1

    aput v1, v0, v9

    .line 1483
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilGens_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    aget-object v1, v1, v8

    invoke-direct {p0, v1, v4, v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getSimpleCEGenerator(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;Lcom/ibm/icu/text/CollationRuleParser$Token;I)I

    move-result v1

    aput v1, v0, v8

    goto :goto_4

    .line 1490
    :cond_8
    iget v0, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    if-ne v0, v8, :cond_9

    .line 1491
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilGens_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    aget-object v1, v1, v8

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getNextGenerated(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;)I

    move-result v1

    aput v1, v0, v8

    goto :goto_4

    .line 1494
    :cond_9
    iget v0, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    if-ne v0, v9, :cond_a

    .line 1495
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilGens_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    aget-object v1, v1, v9

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getNextGenerated(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;)I

    move-result v1

    aput v1, v0, v9

    .line 1497
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilGens_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    aget-object v1, v1, v8

    invoke-direct {p0, v1, v4, v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getSimpleCEGenerator(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;Lcom/ibm/icu/text/CollationRuleParser$Token;I)I

    move-result v1

    aput v1, v0, v8

    goto/16 :goto_4

    .line 1502
    :cond_a
    iget v0, v4, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    if-nez v0, :cond_5

    .line 1503
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilGens_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    aget-object v1, v1, v10

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getNextGenerated(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;)I

    move-result v1

    aput v1, v0, v10

    .line 1506
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilGens_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    aget-object v1, v1, v9

    invoke-direct {p0, v1, v4, v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getSimpleCEGenerator(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;Lcom/ibm/icu/text/CollationRuleParser$Token;I)I

    move-result v1

    aput v1, v0, v9

    .line 1510
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilCEBuffer_:[I

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilGens_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    aget-object v1, v1, v8

    invoke-direct {p0, v1, v4, v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getSimpleCEGenerator(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;Lcom/ibm/icu/text/CollationRuleParser$Token;I)I

    move-result v1

    aput v1, v0, v8

    goto/16 :goto_4

    .line 1519
    :cond_b
    return-void
.end method

.method private static final insertContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I
    .locals 4
    .param p0, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;
    .param p1, "element"    # I
    .param p2, "codePoint"    # C
    .param p3, "value"    # I

    .prologue
    .line 2847
    const v2, 0xffffff

    and-int/2addr p1, v2

    .line 2848
    invoke-static {p0, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getBasicContractionTable(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;I)Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    move-result-object v1

    .line 2849
    .local v1, "tbl":Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;
    if-nez v1, :cond_0

    .line 2850
    invoke-static {p0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addAContractionElement(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;)Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    move-result-object v1

    .line 2851
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_elements_:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    add-int/lit8 p1, v2, -0x1

    .line 2854
    :cond_0
    const/4 v0, 0x0

    .line 2856
    .local v0, "offset":I
    :goto_0
    iget-object v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    if-ge v2, p2, :cond_1

    iget-object v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2857
    add-int/lit8 v0, v0, 0x1

    .line 2858
    goto :goto_0

    .line 2860
    :cond_1
    iget-object v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_CEs_:Ljava/util/Vector;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, p3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v3, v0}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 2861
    iget-object v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    invoke-virtual {v2, v0, p2}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 2863
    iget v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_currentTag_:I

    invoke-static {v2, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->constructSpecialCE(II)I

    move-result v2

    return v2
.end method

.method private static final isContraction(I)Z
    .locals 2
    .param p0, "CE"    # I

    .prologue
    .line 2500
    invoke-static {p0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isSpecial(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCETag(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final isContractionTableElement(I)Z
    .locals 2
    .param p0, "CE"    # I

    .prologue
    .line 2724
    invoke-static {p0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isSpecial(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCETag(I)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-static {p0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCETag(I)I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final isJamo(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 3994
    const/16 v0, 0x1100

    if-lt p0, v0, :cond_0

    const/16 v0, 0x1112

    if-le p0, v0, :cond_2

    :cond_0
    const/16 v0, 0x1175

    if-lt p0, v0, :cond_1

    const/16 v0, 0x1161

    if-le p0, v0, :cond_2

    :cond_1
    const/16 v0, 0x11a8

    if-lt p0, v0, :cond_3

    const/16 v0, 0x11c2

    if-gt p0, v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final isPrefix(I)Z
    .locals 2
    .param p0, "CE"    # I

    .prologue
    .line 2510
    invoke-static {p0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isSpecial(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCETag(I)I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final isSpecial(I)Z
    .locals 2
    .param p0, "CE"    # I

    .prologue
    const/high16 v1, -0x10000000

    .line 2520
    and-int v0, p0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isTailored(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;I[CI)Z
    .locals 4
    .param p0, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;
    .param p1, "element"    # I
    .param p2, "array"    # [C
    .param p3, "offset"    # I

    .prologue
    const/4 v1, 0x1

    const/high16 v3, -0x10000000

    const/4 v0, 0x0

    .line 3598
    :goto_0
    aget-char v2, p2, p3

    if-eqz v2, :cond_3

    .line 3599
    aget-char v2, p2, p3

    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->findCE(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;IC)I

    move-result p1

    .line 3600
    if-ne p1, v3, :cond_1

    .line 3612
    :cond_0
    :goto_1
    return v0

    .line 3603
    :cond_1
    invoke-static {p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isContractionTableElement(I)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 3604
    goto :goto_1

    .line 3606
    :cond_2
    add-int/lit8 p3, p3, 0x1

    .line 3607
    goto :goto_0

    .line 3608
    :cond_3
    invoke-static {p0, p1, v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCE(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;II)I

    move-result v2

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 3609
    goto :goto_1
.end method

.method private static final lengthOfWeight(I)I
    .locals 1
    .param p0, "weight"    # I

    .prologue
    .line 3507
    const v0, 0xffffff

    and-int/2addr v0, p0

    if-nez v0, :cond_0

    .line 3508
    const/4 v0, 0x1

    .line 3516
    :goto_0
    return v0

    .line 3510
    :cond_0
    const v0, 0xffff

    and-int/2addr v0, p0

    if-nez v0, :cond_1

    .line 3511
    const/4 v0, 0x2

    goto :goto_0

    .line 3513
    :cond_1
    and-int/lit16 v0, p0, 0xff

    if-nez v0, :cond_2

    .line 3514
    const/4 v0, 0x3

    goto :goto_0

    .line 3516
    :cond_2
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private static final lengthenRange([Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;III)I
    .locals 4
    .param p0, "range"    # [Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;
    .param p1, "offset"    # I
    .param p2, "maxByte"    # I
    .param p3, "countBytes"    # I

    .prologue
    .line 3309
    aget-object v1, p0, p1

    iget v1, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length2_:I

    add-int/lit8 v0, v1, 0x1

    .line 3310
    .local v0, "length":I
    aget-object v1, p0, p1

    aget-object v2, p0, p1

    iget v2, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    const/4 v3, 0x4

    invoke-static {v2, v0, v3}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setWeightTrail(III)I

    move-result v2

    iput v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    .line 3312
    aget-object v1, p0, p1

    aget-object v2, p0, p1

    iget v2, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    invoke-static {v2, v0, p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setWeightTrail(III)I

    move-result v2

    iput v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    .line 3314
    aget-object v1, p0, p1

    iget v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count2_:I

    mul-int/2addr v2, p3

    iput v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count2_:I

    .line 3315
    aget-object v1, p0, p1

    iput v0, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length2_:I

    .line 3316
    return v0
.end method

.method private static mergeCE(III)I
    .locals 3
    .param p0, "ce1"    # I
    .param p1, "ce2"    # I
    .param p2, "strength"    # I

    .prologue
    .line 1580
    const/16 v0, 0xff

    .line 1581
    .local v0, "mask":I
    const/4 v1, 0x1

    if-ne p2, v1, :cond_1

    .line 1582
    const v0, 0xff00

    .line 1587
    :cond_0
    :goto_0
    and-int/2addr p0, v0

    .line 1588
    and-int/2addr p1, v0

    .line 1589
    packed-switch p2, :pswitch_data_0

    .line 1596
    shl-int/lit8 v1, p0, 0x18

    shl-int/lit8 v2, p1, 0x10

    or-int/2addr v1, v2

    :goto_1
    return v1

    .line 1584
    :cond_1
    if-nez p2, :cond_0

    .line 1585
    const/high16 v0, -0x10000

    goto :goto_0

    .line 1592
    :pswitch_0
    ushr-int/lit8 v1, p1, 0x10

    or-int/2addr v1, p0

    goto :goto_1

    .line 1594
    :pswitch_1
    shl-int/lit8 v1, p0, 0x10

    shl-int/lit8 v2, p1, 0x8

    or-int/2addr v1, v2

    goto :goto_1

    .line 1589
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static nextWeight(Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;)I
    .locals 7
    .param p0, "cegenerator"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;

    .prologue
    const/4 v6, 0x0

    .line 3075
    iget v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_rangesLength_:I

    if-lez v2, :cond_2

    .line 3077
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_ranges_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v2, v2, v6

    iget v0, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    .line 3079
    .local v0, "maxByte":I
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_ranges_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v2, v2, v6

    iget v1, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    .line 3080
    .local v1, "weight":I
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_ranges_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v2, v2, v6

    iget v2, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    if-ne v1, v2, :cond_1

    .line 3083
    iget v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_rangesLength_:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_rangesLength_:I

    .line 3084
    iget v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_rangesLength_:I

    if-lez v2, :cond_0

    .line 3085
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_ranges_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_ranges_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    iget v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_rangesLength_:I

    invoke-static {v2, v3, v4, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3088
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_ranges_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v2, v2, v6

    iput v0, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    .line 3100
    .end local v0    # "maxByte":I
    .end local v1    # "weight":I
    :cond_0
    :goto_0
    return v1

    .line 3094
    .restart local v0    # "maxByte":I
    .restart local v1    # "weight":I
    :cond_1
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_ranges_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v2, v2, v6

    iget-object v3, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CEGenerator;->m_ranges_:[Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    aget-object v3, v3, v6

    iget v3, v3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length2_:I

    invoke-static {v1, v3, v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->incWeight(III)I

    move-result v3

    iput v3, v2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    goto :goto_0

    .line 3100
    .end local v0    # "maxByte":I
    .end local v1    # "weight":I
    :cond_2
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private static processContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;I)I
    .locals 9
    .param p0, "contractions"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;
    .param p1, "element"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    .param p2, "existingCE"    # I

    .prologue
    const v8, 0xffff

    const/4 v7, 0x0

    const/high16 v6, -0x10000000

    .line 2649
    const/4 v1, 0x0

    .line 2651
    .local v1, "firstContractionOffset":I
    iget-object v4, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iget v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    sub-int/2addr v4, v5

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 2652
    invoke-static {p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isContractionTableElement(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCETag(I)I

    move-result v4

    iget v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_currentTag_:I

    if-ne v4, v5, :cond_0

    .line 2654
    iget v4, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    invoke-static {p0, p2, v7, v4}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->changeContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    .line 2656
    iget v4, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    invoke-static {p0, p2, v8, v4}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->changeContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    move v4, p2

    .line 2714
    :goto_0
    return v4

    .line 2663
    :cond_0
    iget v4, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    goto :goto_0

    .line 2672
    :cond_1
    iget v4, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    .line 2673
    invoke-static {p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isContractionTableElement(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2675
    const v4, 0xffffff

    invoke-static {p0, v4, v7, p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    move-result v1

    .line 2678
    invoke-static {p0, p1, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->processContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;I)I

    move-result v2

    .line 2680
    .local v2, "newCE":I
    iget-object v4, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {p0, v1, v4, v2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    .line 2683
    invoke-static {p0, v1, v8, p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    .line 2685
    iget v4, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_currentTag_:I

    invoke-static {v4, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->constructSpecialCE(II)I

    move-result p2

    .line 2713
    :goto_1
    iget v4, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    move v4, p2

    .line 2714
    goto :goto_0

    .line 2694
    .end local v2    # "newCE":I
    :cond_2
    iget-object v4, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {p0, p2, v4}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->findCP(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;IC)I

    move-result v3

    .line 2696
    .local v3, "position":I
    if-lez v3, :cond_3

    .line 2698
    invoke-static {p0, p2, v3}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getCE(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;II)I

    move-result v0

    .line 2699
    .local v0, "eCE":I
    invoke-static {p0, p1, v0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->processContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;I)I

    move-result v2

    .line 2700
    .restart local v2    # "newCE":I
    iget-object v4, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {p0, p2, v3, v4, v2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;IICI)I

    goto :goto_1

    .line 2706
    .end local v0    # "eCE":I
    .end local v2    # "newCE":I
    :cond_3
    invoke-static {p0, p1, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->processContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;I)I

    move-result v2

    .line 2708
    .restart local v2    # "newCE":I
    iget-object v4, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {p0, p2, v4, v2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->insertContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;ICI)I

    goto :goto_1
.end method

.method private processUCACompleteIgnorables(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;)V
    .locals 8
    .param p1, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;

    .prologue
    const/4 v7, 0x0

    .line 4305
    new-instance v4, Lcom/ibm/icu/impl/TrieIterator;

    sget-object v5, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v5, v5, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-direct {v4, v5}, Lcom/ibm/icu/impl/TrieIterator;-><init>(Lcom/ibm/icu/impl/Trie;)V

    .line 4307
    .local v4, "trieiterator":Lcom/ibm/icu/impl/TrieIterator;
    new-instance v1, Lcom/ibm/icu/util/RangeValueIterator$Element;

    invoke-direct {v1}, Lcom/ibm/icu/util/RangeValueIterator$Element;-><init>()V

    .line 4308
    .local v1, "element":Lcom/ibm/icu/util/RangeValueIterator$Element;
    :cond_0
    invoke-virtual {v4, v1}, Lcom/ibm/icu/impl/TrieIterator;->next(Lcom/ibm/icu/util/RangeValueIterator$Element;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 4309
    iget v3, v1, Lcom/ibm/icu/util/RangeValueIterator$Element;->start:I

    .line 4310
    .local v3, "start":I
    iget v2, v1, Lcom/ibm/icu/util/RangeValueIterator$Element;->limit:I

    .line 4311
    .local v2, "limit":I
    iget v5, v1, Lcom/ibm/icu/util/RangeValueIterator$Element;->value:I

    if-nez v5, :cond_0

    .line 4312
    :goto_0
    if-ge v3, v2, :cond_0

    .line 4313
    iget-object v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    invoke-virtual {v5, v3}, Lcom/ibm/icu/impl/IntTrieBuilder;->getValue(I)I

    move-result v0

    .line 4314
    .local v0, "CE":I
    const/high16 v5, -0x10000000

    if-ne v0, v5, :cond_1

    .line 4315
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iput v7, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 4316
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-static {v3}, Lcom/ibm/icu/lang/UCharacter;->toString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    .line 4317
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v6, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    iput-object v6, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 4318
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iput v7, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    .line 4319
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v6, 0x1

    iput v6, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 4320
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v5, v5, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aput v7, v5, v7

    .line 4321
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-direct {p0, p1, v5}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addAnElement(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    .line 4323
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 4324
    goto :goto_0

    .line 4327
    .end local v0    # "CE":I
    .end local v2    # "limit":I
    .end local v3    # "start":I
    :cond_2
    return-void
.end method

.method private static final setAttributes(Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/CollationRuleParser$OptionSet;)V
    .locals 1
    .param p0, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p1, "option"    # Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    .prologue
    .line 3709
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneFailed_:Z

    .line 3710
    iget v0, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_caseFirst_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    .line 3711
    iget v0, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_decomposition_:I

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->setDecomposition(I)V

    .line 3712
    iget-boolean v0, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isAlternateHandlingShifted_:Z

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->setAlternateHandlingShifted(Z)V

    .line 3714
    iget-boolean v0, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isCaseLevel_:Z

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->setCaseLevel(Z)V

    .line 3715
    iget-boolean v0, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isFrenchCollation_:Z

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->setFrenchCollation(Z)V

    .line 3716
    iget-boolean v0, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isHiragana4_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isHiragana4_:Z

    .line 3717
    iget v0, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_strength_:I

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->setStrength(I)V

    .line 3718
    iget v0, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_variableTopValue_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_variableTopValue_:I

    .line 3719
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneFailed_:Z

    .line 3720
    return-void
.end method

.method private static final setContraction(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;IICI)I
    .locals 3
    .param p0, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;
    .param p1, "element"    # I
    .param p2, "offset"    # I
    .param p3, "codePoint"    # C
    .param p4, "value"    # I

    .prologue
    .line 2822
    const v1, 0xffffff

    and-int/2addr p1, v1

    .line 2823
    invoke-static {p0, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->getBasicContractionTable(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;I)Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    move-result-object v0

    .line 2824
    .local v0, "tbl":Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;
    if-nez v0, :cond_0

    .line 2825
    invoke-static {p0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addAContractionElement(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;)Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;

    move-result-object v0

    .line 2826
    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_elements_:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    add-int/lit8 p1, v1, -0x1

    .line 2829
    :cond_0
    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_CEs_:Ljava/util/Vector;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p4}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, p2, v2}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2830
    iget-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BasicContractionTable;->m_codePoints_:Ljava/lang/StringBuffer;

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 2831
    iget v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_currentTag_:I

    invoke-static {v1, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->constructSpecialCE(II)I

    move-result v1

    return v1
.end method

.method private setMapCE(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)V
    .locals 8
    .param p1, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
    .param p2, "element"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .prologue
    const/4 v5, 0x5

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 4164
    iget-object v1, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_expansions_:Ljava/util/Vector;

    .line 4165
    .local v1, "expansions":Ljava/util/Vector;
    iput v6, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 4167
    iget v3, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    iget-object v3, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v3, v3, v7

    invoke-static {v3}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v3, v3, v7

    const v4, 0xffff3f

    and-int/2addr v3, v4

    if-nez v3, :cond_0

    iget-object v3, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v3, v3, v6

    shr-int/lit8 v3, v3, 0x8

    and-int/lit16 v3, v3, 0xff

    if-ne v3, v5, :cond_0

    iget-object v3, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v3, v3, v6

    and-int/lit16 v3, v3, 0xff

    if-ne v3, v5, :cond_0

    .line 4175
    const/high16 v3, -0x4000000

    iget-object v4, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v4, v4, v6

    shr-int/lit8 v4, v4, 0x8

    const v5, 0xffff00

    and-int/2addr v4, v5

    or-int/2addr v3, v4

    iget-object v4, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v4, v4, v7

    shr-int/lit8 v4, v4, 0x18

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    iput v3, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 4203
    :goto_0
    return-void

    .line 4186
    :cond_0
    const/high16 v3, -0xf000000

    iget-object v4, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v4, v4, v6

    invoke-static {v1, v4}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addExpansion(Ljava/util/Vector;I)I

    move-result v4

    shl-int/lit8 v4, v4, 0x4

    const v5, 0xfffff0

    and-int/2addr v4, v5

    or-int v0, v3, v4

    .line 4190
    .local v0, "expansion":I
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_1
    iget v3, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    if-ge v2, v3, :cond_1

    .line 4191
    iget-object v3, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    aget v3, v3, v2

    invoke-static {v1, v3}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addExpansion(Ljava/util/Vector;I)I

    .line 4190
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4193
    :cond_1
    iget v3, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    const/16 v4, 0xf

    if-gt v3, v4, :cond_2

    .line 4194
    iget v3, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    or-int/2addr v0, v3

    .line 4199
    :goto_2
    iput v0, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 4200
    iget-object v3, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    iget v4, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    add-int/lit8 v4, v4, -0x1

    aget v3, v3, v4

    iget v4, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    int-to-byte v4, v4

    iget-object v5, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_maxExpansions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;

    invoke-static {v3, v4, v5}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setMaxExpansion(IBLcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;)I

    goto :goto_0

    .line 4197
    :cond_2
    invoke-static {v1, v6}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addExpansion(Ljava/util/Vector;I)I

    goto :goto_2
.end method

.method private static setMaxExpansion(IBLcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;)I
    .locals 12
    .param p0, "endexpansion"    # I
    .param p1, "expansionsize"    # B
    .param p2, "maxexpansion"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;

    .prologue
    const-wide v10, 0xffffffffL

    .line 2280
    const/4 v4, 0x0

    .line 2281
    .local v4, "start":I
    iget-object v5, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v1

    .line 2282
    .local v1, "limit":I
    int-to-long v6, p0

    .line 2283
    .local v6, "unsigned":J
    and-long/2addr v6, v10

    .line 2287
    const/4 v3, -0x1

    .line 2288
    .local v3, "result":I
    :goto_0
    add-int/lit8 v5, v1, -0x1

    if-ge v4, v5, :cond_1

    .line 2289
    sub-int v5, v1, v4

    shr-int/lit8 v5, v5, 0x1

    add-int v2, v4, v5

    .line 2290
    .local v2, "mid":I
    iget-object v5, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v5, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-long v8, v5

    .line 2292
    .local v8, "unsignedce":J
    and-long/2addr v8, v10

    .line 2293
    cmp-long v5, v6, v8

    if-gtz v5, :cond_0

    .line 2294
    move v1, v2

    .line 2295
    goto :goto_0

    .line 2297
    :cond_0
    move v4, v2

    goto :goto_0

    .line 2301
    .end local v2    # "mid":I
    .end local v8    # "unsignedce":J
    :cond_1
    iget-object v5, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v5, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, p0, :cond_4

    .line 2303
    move v3, v4

    .line 2309
    :cond_2
    :goto_1
    const/4 v5, -0x1

    if-le v3, v5, :cond_5

    .line 2312
    iget-object v5, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_expansionCESize_:Ljava/util/Vector;

    invoke-virtual {v5, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2313
    .local v0, "currentsize":Ljava/lang/Object;
    check-cast v0, Ljava/lang/Byte;

    .end local v0    # "currentsize":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v5

    if-ge v5, p1, :cond_3

    .line 2314
    iget-object v5, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_expansionCESize_:Ljava/util/Vector;

    new-instance v10, Ljava/lang/Byte;

    invoke-direct {v10, p1}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v5, v3, v10}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2328
    :cond_3
    :goto_2
    iget-object v5, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    return v5

    .line 2305
    :cond_4
    iget-object v5, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v5, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, p0, :cond_2

    .line 2307
    move v3, v1

    goto :goto_1

    .line 2321
    :cond_5
    iget-object v5, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    new-instance v10, Ljava/lang/Integer;

    invoke-direct {v10, p0}, Ljava/lang/Integer;-><init>(I)V

    add-int/lit8 v11, v4, 0x1

    invoke-virtual {v5, v10, v11}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 2324
    iget-object v5, p2, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_expansionCESize_:Ljava/util/Vector;

    new-instance v10, Ljava/lang/Byte;

    invoke-direct {v10, p1}, Ljava/lang/Byte;-><init>(B)V

    add-int/lit8 v11, v4, 0x1

    invoke-virtual {v5, v10, v11}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_2
.end method

.method private static setMaxJamoExpansion(CIBLcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;)I
    .locals 4
    .param p0, "ch"    # C
    .param p1, "endexpansion"    # I
    .param p2, "expansionsize"    # B
    .param p3, "maxexpansion"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;

    .prologue
    .line 2345
    const/4 v0, 0x1

    .line 2346
    .local v0, "isV":Z
    const/16 v2, 0x1100

    if-lt p0, v2, :cond_1

    const/16 v2, 0x1112

    if-gt p0, v2, :cond_1

    .line 2349
    iget-byte v2, p3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxLSize_:B

    if-ge v2, p2, :cond_0

    .line 2350
    iput-byte p2, p3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxLSize_:B

    .line 2352
    :cond_0
    iget-object v2, p3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    .line 2381
    :goto_0
    return v2

    .line 2355
    :cond_1
    const/16 v2, 0x1161

    if-lt p0, v2, :cond_2

    const/16 v2, 0x1175

    if-gt p0, v2, :cond_2

    .line 2357
    iget-byte v2, p3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxVSize_:B

    if-ge v2, p2, :cond_2

    .line 2358
    iput-byte p2, p3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxVSize_:B

    .line 2362
    :cond_2
    const/16 v2, 0x11a8

    if-lt p0, v2, :cond_3

    const/16 v2, 0x11c2

    if-gt p0, v2, :cond_3

    .line 2363
    const/4 v0, 0x0

    .line 2365
    iget-byte v2, p3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxTSize_:B

    if-ge v2, p2, :cond_3

    .line 2366
    iput-byte p2, p3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxTSize_:B

    .line 2370
    :cond_3
    iget-object v2, p3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v1

    .line 2371
    .local v1, "pos":I
    :cond_4
    if-lez v1, :cond_5

    .line 2372
    add-int/lit8 v1, v1, -0x1

    .line 2373
    iget-object v2, p3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, p1, :cond_4

    .line 2375
    iget-object v2, p3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    goto :goto_0

    .line 2378
    :cond_5
    iget-object v2, p3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 2379
    iget-object v3, p3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_isV_:Ljava/util/Vector;

    if-eqz v0, :cond_6

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_1
    invoke-virtual {v3, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 2381
    iget-object v2, p3, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    goto :goto_0

    .line 2379
    :cond_6
    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_1
.end method

.method private static final setWeightByte(III)I
    .locals 3
    .param p0, "weight"    # I
    .param p1, "index"    # I
    .param p2, "b"    # I

    .prologue
    .line 3146
    shl-int/lit8 p1, p1, 0x3

    .line 3148
    const/4 v1, -0x1

    ushr-int v0, v1, p1

    .line 3149
    .local v0, "mask":I
    rsub-int/lit8 p1, p1, 0x20

    .line 3150
    const/16 v1, -0x100

    shl-int/2addr v1, p1

    or-int/2addr v0, v1

    .line 3151
    and-int v1, p0, v0

    shl-int v2, p2, p1

    or-int/2addr v1, v2

    return v1
.end method

.method private static final setWeightTrail(III)I
    .locals 2
    .param p0, "weight"    # I
    .param p1, "length"    # I
    .param p2, "trail"    # I

    .prologue
    .line 3328
    rsub-int/lit8 v0, p1, 0x4

    shl-int/lit8 p1, v0, 0x3

    .line 3329
    const/16 v0, -0x100

    shl-int/2addr v0, p1

    and-int/2addr v0, p0

    shl-int v1, p2, p1

    or-int/2addr v0, v1

    return v0
.end method

.method private static final toLargeKana(C)C
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 1987
    const/16 v0, 0x3042

    if-ge v0, p0, :cond_0

    const/16 v0, 0x30ef

    if-ge p0, v0, :cond_0

    .line 1988
    add-int/lit16 v0, p0, -0x3000

    sparse-switch v0, :sswitch_data_0

    .line 2017
    :cond_0
    :goto_0
    return p0

    .line 2007
    :sswitch_0
    add-int/lit8 v0, p0, 0x1

    int-to-char p0, v0

    .line 2008
    goto :goto_0

    .line 2010
    :sswitch_1
    const/16 p0, 0x30ab

    .line 2011
    goto :goto_0

    .line 2013
    :sswitch_2
    const/16 p0, 0x30b1

    goto :goto_0

    .line 1988
    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_0
        0x43 -> :sswitch_0
        0x45 -> :sswitch_0
        0x47 -> :sswitch_0
        0x49 -> :sswitch_0
        0x63 -> :sswitch_0
        0x83 -> :sswitch_0
        0x85 -> :sswitch_0
        0x8e -> :sswitch_0
        0xa1 -> :sswitch_0
        0xa3 -> :sswitch_0
        0xa5 -> :sswitch_0
        0xa7 -> :sswitch_0
        0xa9 -> :sswitch_0
        0xc3 -> :sswitch_0
        0xe3 -> :sswitch_0
        0xe5 -> :sswitch_0
        0xee -> :sswitch_0
        0xf5 -> :sswitch_1
        0xf6 -> :sswitch_2
    .end sparse-switch
.end method

.method private static final toSmallKana(C)C
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 2027
    const/16 v0, 0x3042

    if-ge v0, p0, :cond_0

    const/16 v0, 0x30ef

    if-ge p0, v0, :cond_0

    .line 2028
    add-int/lit16 v0, p0, -0x3000

    sparse-switch v0, :sswitch_data_0

    .line 2057
    :cond_0
    :goto_0
    return p0

    .line 2047
    :sswitch_0
    add-int/lit8 v0, p0, -0x1

    int-to-char p0, v0

    .line 2048
    goto :goto_0

    .line 2050
    :sswitch_1
    const/16 p0, 0x30f5

    .line 2051
    goto :goto_0

    .line 2053
    :sswitch_2
    const/16 p0, 0x30f6

    goto :goto_0

    .line 2028
    :sswitch_data_0
    .sparse-switch
        0x42 -> :sswitch_0
        0x44 -> :sswitch_0
        0x46 -> :sswitch_0
        0x48 -> :sswitch_0
        0x4a -> :sswitch_0
        0x64 -> :sswitch_0
        0x84 -> :sswitch_0
        0x86 -> :sswitch_0
        0x8f -> :sswitch_0
        0xa2 -> :sswitch_0
        0xa4 -> :sswitch_0
        0xa6 -> :sswitch_0
        0xa8 -> :sswitch_0
        0xaa -> :sswitch_0
        0xab -> :sswitch_1
        0xb1 -> :sswitch_2
        0xc4 -> :sswitch_0
        0xe4 -> :sswitch_0
        0xe6 -> :sswitch_0
        0xef -> :sswitch_0
    .end sparse-switch
.end method

.method private static final truncateWeight(II)I
    .locals 2
    .param p0, "weight"    # I
    .param p1, "length"    # I

    .prologue
    .line 3497
    const/4 v0, -0x1

    rsub-int/lit8 v1, p1, 0x4

    shl-int/lit8 v1, v1, 0x3

    shl-int/2addr v0, v1

    and-int/2addr v0, p0

    return v0
.end method

.method private static final unsafeCPAddCCNZ(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;)V
    .locals 15
    .param p0, "t"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;

    .prologue
    const/16 v14, 0x100

    const/4 v12, 0x0

    .line 3873
    sget-boolean v13, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->buildCMTabFlag:Z

    iget-object v11, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->cmLookup:Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;

    if-nez v11, :cond_3

    const/4 v11, 0x1

    :goto_0
    and-int v0, v13, v11

    .line 3874
    .local v0, "buildCMTable":Z
    const/4 v3, 0x0

    .line 3875
    .local v3, "cm":[C
    new-array v9, v14, [I

    .line 3876
    .local v9, "index":[I
    const/4 v5, 0x0

    .line 3878
    .local v5, "count":I
    if-eqz v0, :cond_0

    .line 3879
    const/high16 v11, 0x10000

    new-array v3, v11, [C

    .line 3881
    :cond_0
    const/4 v1, 0x0

    .local v1, "c":C
    :goto_1
    const v11, 0xffff

    if-ge v1, v11, :cond_4

    .line 3882
    invoke-static {v1}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v8

    .line 3883
    .local v8, "fcd":C
    if-ge v8, v14, :cond_1

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v11

    if-eqz v11, :cond_2

    if-eqz v8, :cond_2

    .line 3886
    :cond_1
    iget-object v11, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    invoke-static {v11, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->unsafeCPSet([BC)V

    .line 3887
    if-eqz v0, :cond_2

    if-eqz v8, :cond_2

    .line 3888
    and-int/lit16 v2, v8, 0xff

    .line 3889
    .local v2, "cc":I
    shl-int/lit8 v11, v2, 0x8

    aget v13, v9, v2

    add-int v10, v11, v13

    .line 3890
    .local v10, "pos":I
    aput-char v1, v3, v10

    .line 3891
    aget v11, v9, v2

    add-int/lit8 v11, v11, 0x1

    aput v11, v9, v2

    .line 3892
    add-int/lit8 v5, v5, 0x1

    .line 3881
    .end local v2    # "cc":I
    .end local v10    # "pos":I
    :cond_2
    add-int/lit8 v11, v1, 0x1

    int-to-char v1, v11

    goto :goto_1

    .end local v0    # "buildCMTable":Z
    .end local v1    # "c":C
    .end local v3    # "cm":[C
    .end local v5    # "count":I
    .end local v8    # "fcd":C
    .end local v9    # "index":[I
    :cond_3
    move v11, v12

    .line 3873
    goto :goto_0

    .line 3897
    .restart local v0    # "buildCMTable":Z
    .restart local v1    # "c":C
    .restart local v3    # "cm":[C
    .restart local v5    # "count":I
    .restart local v9    # "index":[I
    :cond_4
    iget-object v11, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_prefixLookup_:Ljava/util/Hashtable;

    if-eqz v11, :cond_5

    .line 3898
    iget-object v11, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_prefixLookup_:Ljava/util/Hashtable;

    invoke-virtual {v11}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v7

    .line 3899
    .local v7, "els":Ljava/util/Enumeration;
    :goto_2
    invoke-interface {v7}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 3900
    invoke-interface {v7}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .line 3908
    .local v6, "e":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    iget-object v11, v6, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-static {v11, v12}, Lcom/ibm/icu/text/Normalizer;->compose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 3909
    .local v4, "comp":Ljava/lang/String;
    iget-object v11, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    invoke-virtual {v4, v12}, Ljava/lang/String;->charAt(I)C

    move-result v13

    invoke-static {v11, v13}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->unsafeCPSet([BC)V

    goto :goto_2

    .line 3913
    .end local v4    # "comp":Ljava/lang/String;
    .end local v6    # "e":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    .end local v7    # "els":Ljava/util/Enumeration;
    :cond_5
    if-eqz v0, :cond_6

    .line 3914
    new-instance v11, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;

    invoke-direct {v11}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;-><init>()V

    iput-object v11, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->cmLookup:Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;

    .line 3915
    iget-object v11, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->cmLookup:Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;

    invoke-virtual {v11, v3, v5, v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;->generate([CI[I)V

    .line 3917
    :cond_6
    return-void
.end method

.method private static final unsafeCPSet([BC)V
    .locals 5
    .param p0, "table"    # [B
    .param p1, "c"    # C

    .prologue
    .line 2564
    move v0, p1

    .line 2565
    .local v0, "hash":I
    const/16 v1, 0x2100

    if-lt v0, v1, :cond_1

    .line 2566
    const v1, 0xd800

    if-lt v0, v1, :cond_0

    const v1, 0xf8ff

    if-gt v0, v1, :cond_0

    .line 2574
    :goto_0
    return-void

    .line 2571
    :cond_0
    and-int/lit16 v1, v0, 0x1fff

    add-int/lit16 v0, v1, 0x100

    .line 2573
    :cond_1
    shr-int/lit8 v1, v0, 0x3

    aget-byte v2, p0, v1

    const/4 v3, 0x1

    and-int/lit8 v4, v0, 0x7

    shl-int/2addr v3, v4

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, p0, v1

    goto :goto_0
.end method


# virtual methods
.method assembleTailoringTable(Lcom/ibm/icu/text/RuleBasedCollator;)V
    .locals 18
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 542
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    if-ge v5, v14, :cond_1

    .line 546
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    aget-object v14, v14, v5

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    if-eqz v14, :cond_0

    .line 551
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    aget-object v14, v14, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->initBuffers(Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;)V

    .line 542
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 555
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    if-eqz v14, :cond_5

    .line 557
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CE_:[I

    const/16 v16, 0x0

    aget v15, v15, v16

    ushr-int/lit8 v15, v15, 0x10

    iput v15, v14, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_variableTopValue_:I

    .line 560
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    if-ne v14, v15, :cond_2

    .line 562
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iput-object v15, v14, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 565
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_last_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    if-ne v14, v15, :cond_3

    .line 568
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iput-object v15, v14, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_last_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 571
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    if-eqz v14, :cond_4

    .line 572
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iput-object v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 575
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    if-eqz v14, :cond_5

    .line 576
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iput-object v15, v14, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 581
    :cond_5
    new-instance v12, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    invoke-direct {v12, v14}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;-><init>(Lcom/ibm/icu/text/CollationRuleParser;)V

    .line 586
    .local v12, "t":Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
    const/4 v5, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    if-ge v5, v14, :cond_6

    .line 590
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    aget-object v14, v14, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v14}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->createElements(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;)V

    .line 586
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 593
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-virtual {v14}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->clear()V

    .line 594
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    .line 597
    .local v11, "str":Ljava/lang/StringBuffer;
    const/4 v14, 0x0

    const/16 v15, 0xff

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v14, v15}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->copyRangeFromUCA(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;II)V

    .line 600
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_copySet_:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v14, :cond_7

    .line 601
    const/4 v5, 0x0

    .line 602
    const/4 v5, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_copySet_:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v14}, Lcom/ibm/icu/text/UnicodeSet;->getRangeCount()I

    move-result v14

    if-ge v5, v14, :cond_7

    .line 603
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_copySet_:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v14, v5}, Lcom/ibm/icu/text/UnicodeSet;->getRangeStart(I)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser;->m_copySet_:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v15, v5}, Lcom/ibm/icu/text/UnicodeSet;->getRangeEnd(I)I

    move-result v15

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v14, v15}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->copyRangeFromUCA(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;II)V

    .line 602
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 609
    :cond_7
    sget-object v3, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONTRACTIONS_:[C

    .line 610
    .local v3, "conts":[C
    const/4 v7, 0x0

    .line 611
    .local v7, "offset":I
    :goto_3
    aget-char v14, v3, v7

    if-eqz v14, :cond_14

    .line 613
    iget-object v14, v12, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    aget-char v15, v3, v7

    invoke-virtual {v14, v15}, Lcom/ibm/icu/impl/IntTrieBuilder;->getValue(I)I

    move-result v13

    .line 614
    .local v13, "tailoredCE":I
    const/4 v10, 0x0

    .line 615
    .local v10, "prefixElm":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    const/high16 v14, -0x10000000

    if-eq v13, v14, :cond_13

    .line 616
    const/4 v6, 0x1

    .line 617
    .local v6, "needToAdd":Z
    invoke-static {v13}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isContractionTableElement(I)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 618
    iget-object v14, v12, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contractions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    add-int/lit8 v15, v7, 0x1

    invoke-static {v14, v13, v3, v15}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isTailored(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;I[CI)Z

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_8

    .line 620
    const/4 v6, 0x0

    .line 623
    :cond_8
    if-nez v6, :cond_a

    invoke-static {v13}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->isPrefix(I)Z

    move-result v14

    if-eqz v14, :cond_a

    add-int/lit8 v14, v7, 0x1

    aget-char v14, v3, v14

    if-nez v14, :cond_a

    .line 627
    new-instance v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-direct {v4}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;-><init>()V

    .line 628
    .local v4, "elm":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    iput-object v14, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 629
    const/4 v14, 0x0

    iput v14, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 630
    aget-char v14, v3, v7

    invoke-static {v14}, Lcom/ibm/icu/lang/UCharacter;->toString(I)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    .line 631
    add-int/lit8 v14, v7, 0x2

    aget-char v14, v3, v14

    invoke-static {v14}, Lcom/ibm/icu/lang/UCharacter;->toString(I)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 632
    const/4 v14, 0x0

    iput v14, v4, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 633
    iget-object v14, v12, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_prefixLookup_:Ljava/util/Hashtable;

    invoke-virtual {v14, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "prefixElm":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    check-cast v10, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .line 634
    .restart local v10    # "prefixElm":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    if-eqz v10, :cond_9

    iget-object v14, v10, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Ljava/lang/String;->charAt(I)C

    move-result v14

    add-int/lit8 v15, v7, 0x2

    aget-char v15, v3, v15

    if-eq v14, v15, :cond_a

    .line 636
    :cond_9
    const/4 v6, 0x1

    .line 639
    .end local v4    # "elm":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_removeSet_:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v14, :cond_b

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_removeSet_:Lcom/ibm/icu/text/UnicodeSet;

    aget-char v15, v3, v7

    invoke-virtual {v14, v15}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 640
    const/4 v6, 0x0

    .line 644
    :cond_b
    const/4 v14, 0x1

    if-ne v6, v14, :cond_12

    .line 646
    add-int/lit8 v14, v7, 0x1

    aget-char v14, v3, v14

    if-eqz v14, :cond_e

    .line 647
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v15, 0x0

    iput v15, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 648
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v15, 0x0

    iput-object v15, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 649
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    iput-object v15, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 650
    const/4 v14, 0x0

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->length()I

    move-result v15

    invoke-virtual {v11, v14, v15}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 651
    aget-char v14, v3, v7

    invoke-virtual {v11, v14}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 652
    add-int/lit8 v14, v7, 0x1

    aget-char v14, v3, v14

    invoke-virtual {v11, v14}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 653
    add-int/lit8 v14, v7, 0x2

    aget-char v14, v3, v14

    if-eqz v14, :cond_c

    .line 654
    add-int/lit8 v14, v7, 0x2

    aget-char v14, v3, v14

    invoke-virtual {v11, v14}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 656
    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    .line 657
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v15, 0x0

    iput v15, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 658
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    invoke-virtual {v14, v15}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 690
    :cond_d
    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v14}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v2

    .line 691
    .local v2, "CE":I
    const/4 v14, -0x1

    if-eq v2, v14, :cond_11

    .line 692
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget v0, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    iput v0, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    aput v2, v14, v16

    goto :goto_4

    .line 661
    .end local v2    # "CE":I
    :cond_e
    const/4 v8, 0x0

    .line 662
    .local v8, "preKeyLen":I
    const/4 v14, 0x0

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->length()I

    move-result v15

    invoke-virtual {v11, v14, v15}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 663
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    aget-char v15, v3, v7

    invoke-static {v15}, Lcom/ibm/icu/lang/UCharacter;->toString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 664
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v15, 0x0

    iput v15, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 665
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    aget-char v15, v3, v7

    invoke-static {v15}, Lcom/ibm/icu/lang/UCharacter;->toString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    .line 666
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    add-int/lit8 v15, v7, 0x2

    aget-char v15, v3, v15

    invoke-static {v15}, Lcom/ibm/icu/lang/UCharacter;->toString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 667
    if-nez v10, :cond_f

    .line 668
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    const/4 v15, 0x0

    iput v15, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 674
    :goto_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget-object v15, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    invoke-virtual {v14, v15}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 675
    :goto_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v14}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v14

    const/4 v15, -0x1

    if-eq v14, v15, :cond_10

    .line 677
    add-int/lit8 v8, v8, 0x1

    .line 678
    goto :goto_6

    .line 671
    :cond_f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    iget v15, v15, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    iput v15, v14, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    goto :goto_5

    .line 679
    :cond_10
    add-int/lit8 v14, v7, 0x2

    aget-char v14, v3, v14

    invoke-virtual {v11, v14}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 680
    aget-char v14, v3, v7

    invoke-virtual {v11, v14}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 681
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    move v9, v8

    .line 684
    .end local v8    # "preKeyLen":I
    .local v9, "preKeyLen":I
    :goto_7
    add-int/lit8 v8, v9, -0x1

    .end local v9    # "preKeyLen":I
    .restart local v8    # "preKeyLen":I
    if-lez v9, :cond_d

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v14}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v14

    const/4 v15, -0x1

    if-eq v14, v15, :cond_d

    move v9, v8

    .line 685
    .end local v8    # "preKeyLen":I
    .restart local v9    # "preKeyLen":I
    goto :goto_7

    .line 698
    .end local v9    # "preKeyLen":I
    .restart local v2    # "CE":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_utilElement_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v14}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->addAnElement(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)I

    .line 704
    .end local v2    # "CE":I
    .end local v6    # "needToAdd":Z
    :cond_12
    :goto_8
    add-int/lit8 v7, v7, 0x3

    .line 705
    goto/16 :goto_3

    .line 700
    :cond_13
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_removeSet_:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v14, :cond_12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v14, v14, Lcom/ibm/icu/text/CollationRuleParser;->m_removeSet_:Lcom/ibm/icu/text/UnicodeSet;

    aget-char v15, v3, v7

    invoke-virtual {v14, v15}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v14

    if-eqz v14, :cond_12

    .line 701
    aget-char v14, v3, v7

    aget-char v15, v3, v7

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v14, v15}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->copyRangeFromUCA(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;II)V

    goto :goto_8

    .line 708
    .end local v10    # "prefixElm":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    .end local v13    # "tailoredCE":I
    :cond_14
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->processUCACompleteIgnorables(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;)V

    .line 711
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->canonicalClosure(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;)V

    .line 714
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v12, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->assembleTable(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;Lcom/ibm/icu/text/RuleBasedCollator;)V

    .line 715
    return-void
.end method

.method setRules(Lcom/ibm/icu/text/RuleBasedCollator;)V
    .locals 1
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 458
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_removeSet_:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v0, :cond_1

    .line 460
    :cond_0
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->assembleTailoringTable(Lcom/ibm/icu/text/RuleBasedCollator;)V

    .line 467
    :goto_0
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->m_parser_:Lcom/ibm/icu/text/CollationRuleParser;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/CollationRuleParser;->setDefaultOptionsInCollator(Lcom/ibm/icu/text/RuleBasedCollator;)V

    .line 468
    return-void

    .line 464
    :cond_1
    invoke-virtual {p1}, Lcom/ibm/icu/text/RuleBasedCollator;->setWithUCATables()V

    goto :goto_0
.end method
