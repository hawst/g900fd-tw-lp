.class Lcom/ibm/icu/text/UnescapeTransliterator;
.super Lcom/ibm/icu/text/Transliterator;
.source "UnescapeTransliterator.java"


# static fields
.field private static final END:C = '\uffff'


# instance fields
.field private spec:[C


# direct methods
.method constructor <init>(Ljava/lang/String;[C)V
    .locals 1
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "spec"    # [C

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/Transliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 131
    iput-object p2, p0, Lcom/ibm/icu/text/UnescapeTransliterator;->spec:[C

    .line 132
    return-void
.end method

.method static register()V
    .locals 2

    .prologue
    .line 50
    const-string/jumbo v0, "Hex-Any/Unicode"

    new-instance v1, Lcom/ibm/icu/text/UnescapeTransliterator$1;

    invoke-direct {v1}, Lcom/ibm/icu/text/UnescapeTransliterator$1;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 60
    const-string/jumbo v0, "Hex-Any/Java"

    new-instance v1, Lcom/ibm/icu/text/UnescapeTransliterator$2;

    invoke-direct {v1}, Lcom/ibm/icu/text/UnescapeTransliterator$2;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 70
    const-string/jumbo v0, "Hex-Any/C"

    new-instance v1, Lcom/ibm/icu/text/UnescapeTransliterator$3;

    invoke-direct {v1}, Lcom/ibm/icu/text/UnescapeTransliterator$3;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 81
    const-string/jumbo v0, "Hex-Any/XML"

    new-instance v1, Lcom/ibm/icu/text/UnescapeTransliterator$4;

    invoke-direct {v1}, Lcom/ibm/icu/text/UnescapeTransliterator$4;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 91
    const-string/jumbo v0, "Hex-Any/XML10"

    new-instance v1, Lcom/ibm/icu/text/UnescapeTransliterator$5;

    invoke-direct {v1}, Lcom/ibm/icu/text/UnescapeTransliterator$5;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 101
    const-string/jumbo v0, "Hex-Any/Perl"

    new-instance v1, Lcom/ibm/icu/text/UnescapeTransliterator$6;

    invoke-direct {v1}, Lcom/ibm/icu/text/UnescapeTransliterator$6;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 111
    const-string/jumbo v0, "Hex-Any"

    new-instance v1, Lcom/ibm/icu/text/UnescapeTransliterator$7;

    invoke-direct {v1}, Lcom/ibm/icu/text/UnescapeTransliterator$7;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 124
    return-void
.end method


# virtual methods
.method protected handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V
    .locals 26
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "pos"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "isIncremental"    # Z

    .prologue
    .line 139
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    move/from16 v20, v0

    .line 140
    .local v20, "start":I
    move-object/from16 v0, p2

    iget v12, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 144
    .local v12, "limit":I
    :cond_0
    :goto_0
    move/from16 v0, v20

    if-ge v0, v12, :cond_1

    .line 148
    const/4 v11, 0x0

    .local v11, "j":I
    const/4 v9, 0x0

    .local v9, "ipat":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/UnescapeTransliterator;->spec:[C

    move-object/from16 v24, v0

    aget-char v24, v24, v9

    const v25, 0xffff

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_6

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/UnescapeTransliterator;->spec:[C

    move-object/from16 v24, v0

    add-int/lit8 v10, v9, 0x1

    .end local v9    # "ipat":I
    .local v10, "ipat":I
    aget-char v16, v24, v9

    .line 152
    .local v16, "prefixLen":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/UnescapeTransliterator;->spec:[C

    move-object/from16 v24, v0

    add-int/lit8 v9, v10, 0x1

    .end local v10    # "ipat":I
    .restart local v9    # "ipat":I
    aget-char v22, v24, v10

    .line 153
    .local v22, "suffixLen":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/UnescapeTransliterator;->spec:[C

    move-object/from16 v24, v0

    add-int/lit8 v10, v9, 0x1

    .end local v9    # "ipat":I
    .restart local v10    # "ipat":I
    aget-char v17, v24, v9

    .line 154
    .local v17, "radix":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/UnescapeTransliterator;->spec:[C

    move-object/from16 v24, v0

    add-int/lit8 v9, v10, 0x1

    .end local v10    # "ipat":I
    .restart local v9    # "ipat":I
    aget-char v15, v24, v10

    .line 155
    .local v15, "minDigits":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/UnescapeTransliterator;->spec:[C

    move-object/from16 v24, v0

    add-int/lit8 v10, v9, 0x1

    .end local v9    # "ipat":I
    .restart local v10    # "ipat":I
    aget-char v14, v24, v9

    .line 159
    .local v14, "maxDigits":I
    move/from16 v18, v20

    .line 160
    .local v18, "s":I
    const/4 v13, 0x1

    .line 162
    .local v13, "match":Z
    const/4 v8, 0x0

    .local v8, "i":I
    move/from16 v19, v18

    .end local v18    # "s":I
    .local v19, "s":I
    :goto_2
    move/from16 v0, v16

    if-ge v8, v0, :cond_f

    .line 163
    move/from16 v0, v19

    if-lt v0, v12, :cond_7

    .line 164
    if-lez v8, :cond_7

    .line 169
    if-eqz p3, :cond_2

    .line 247
    .end local v8    # "i":I
    .end local v10    # "ipat":I
    .end local v11    # "j":I
    .end local v13    # "match":Z
    .end local v14    # "maxDigits":I
    .end local v15    # "minDigits":I
    .end local v16    # "prefixLen":I
    .end local v17    # "radix":I
    .end local v19    # "s":I
    .end local v22    # "suffixLen":I
    :cond_1
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    move/from16 v24, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    move/from16 v25, v0

    sub-int v25, v12, v25

    add-int v24, v24, v25

    move/from16 v0, v24

    move-object/from16 v1, p2

    iput v0, v1, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    .line 248
    move-object/from16 v0, p2

    iput v12, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 249
    move/from16 v0, v20

    move-object/from16 v1, p2

    iput v0, v1, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 250
    return-void

    .line 172
    .restart local v8    # "i":I
    .restart local v10    # "ipat":I
    .restart local v11    # "j":I
    .restart local v13    # "match":Z
    .restart local v14    # "maxDigits":I
    .restart local v15    # "minDigits":I
    .restart local v16    # "prefixLen":I
    .restart local v17    # "radix":I
    .restart local v19    # "s":I
    .restart local v22    # "suffixLen":I
    :cond_2
    const/4 v13, 0x0

    move/from16 v18, v19

    .line 183
    .end local v19    # "s":I
    .restart local v18    # "s":I
    :goto_3
    if-eqz v13, :cond_d

    .line 184
    const/16 v23, 0x0

    .line 185
    .local v23, "u":I
    const/4 v7, 0x0

    .line 187
    .local v7, "digitCount":I
    :cond_3
    move/from16 v0, v18

    if-lt v0, v12, :cond_9

    .line 189
    move/from16 v0, v18

    move/from16 v1, v20

    if-le v0, v1, :cond_4

    if-nez p3, :cond_1

    .line 206
    :cond_4
    :goto_4
    if-lt v7, v15, :cond_a

    const/4 v13, 0x1

    .line 208
    :goto_5
    if-eqz v13, :cond_d

    .line 209
    const/4 v8, 0x0

    move/from16 v19, v18

    .end local v18    # "s":I
    .restart local v19    # "s":I
    :goto_6
    move/from16 v0, v22

    if-ge v8, v0, :cond_e

    .line 210
    move/from16 v0, v19

    if-lt v0, v12, :cond_b

    .line 212
    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_5

    if-nez p3, :cond_1

    .line 215
    :cond_5
    const/4 v13, 0x0

    move/from16 v18, v19

    .line 225
    .end local v19    # "s":I
    .restart local v18    # "s":I
    :goto_7
    if-eqz v13, :cond_d

    .line 227
    invoke-static/range {v23 .. v23}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v21

    .line 228
    .local v21, "str":Ljava/lang/String;
    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v18

    move-object/from16 v3, v21

    invoke-interface {v0, v1, v2, v3}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 229
    sub-int v24, v18, v20

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v25

    sub-int v24, v24, v25

    sub-int v12, v12, v24

    move v9, v10

    .line 242
    .end local v7    # "digitCount":I
    .end local v8    # "i":I
    .end local v10    # "ipat":I
    .end local v13    # "match":Z
    .end local v14    # "maxDigits":I
    .end local v15    # "minDigits":I
    .end local v16    # "prefixLen":I
    .end local v17    # "radix":I
    .end local v18    # "s":I
    .end local v21    # "str":Ljava/lang/String;
    .end local v22    # "suffixLen":I
    .end local v23    # "u":I
    .restart local v9    # "ipat":I
    :cond_6
    move/from16 v0, v20

    if-ge v0, v12, :cond_0

    .line 243
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v24

    invoke-static/range {v24 .. v24}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v24

    add-int v20, v20, v24

    .line 244
    goto/16 :goto_0

    .line 176
    .end local v9    # "ipat":I
    .restart local v8    # "i":I
    .restart local v10    # "ipat":I
    .restart local v13    # "match":Z
    .restart local v14    # "maxDigits":I
    .restart local v15    # "minDigits":I
    .restart local v16    # "prefixLen":I
    .restart local v17    # "radix":I
    .restart local v19    # "s":I
    .restart local v22    # "suffixLen":I
    :cond_7
    add-int/lit8 v18, v19, 0x1

    .end local v19    # "s":I
    .restart local v18    # "s":I
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v4

    .line 177
    .local v4, "c":C
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/UnescapeTransliterator;->spec:[C

    move-object/from16 v24, v0

    add-int v25, v10, v8

    aget-char v24, v24, v25

    move/from16 v0, v24

    if-eq v4, v0, :cond_8

    .line 178
    const/4 v13, 0x0

    .line 179
    goto :goto_3

    .line 162
    :cond_8
    add-int/lit8 v8, v8, 0x1

    move/from16 v19, v18

    .end local v18    # "s":I
    .restart local v19    # "s":I
    goto/16 :goto_2

    .line 194
    .end local v4    # "c":C
    .end local v19    # "s":I
    .restart local v7    # "digitCount":I
    .restart local v18    # "s":I
    .restart local v23    # "u":I
    :cond_9
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v5

    .line 195
    .local v5, "ch":I
    move/from16 v0, v17

    invoke-static {v5, v0}, Lcom/ibm/icu/lang/UCharacter;->digit(II)I

    move-result v6

    .line 196
    .local v6, "digit":I
    if-ltz v6, :cond_4

    .line 199
    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v24

    add-int v18, v18, v24

    .line 200
    mul-int v24, v23, v17

    add-int v23, v24, v6

    .line 201
    add-int/lit8 v7, v7, 0x1

    if-ne v7, v14, :cond_3

    goto/16 :goto_4

    .line 206
    .end local v5    # "ch":I
    .end local v6    # "digit":I
    :cond_a
    const/4 v13, 0x0

    goto/16 :goto_5

    .line 218
    .end local v18    # "s":I
    .restart local v19    # "s":I
    :cond_b
    add-int/lit8 v18, v19, 0x1

    .end local v19    # "s":I
    .restart local v18    # "s":I
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v4

    .line 219
    .restart local v4    # "c":C
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/UnescapeTransliterator;->spec:[C

    move-object/from16 v24, v0

    add-int v25, v10, v16

    add-int v25, v25, v8

    aget-char v24, v24, v25

    move/from16 v0, v24

    if-eq v4, v0, :cond_c

    .line 220
    const/4 v13, 0x0

    .line 221
    goto/16 :goto_7

    .line 209
    :cond_c
    add-int/lit8 v8, v8, 0x1

    move/from16 v19, v18

    .end local v18    # "s":I
    .restart local v19    # "s":I
    goto/16 :goto_6

    .line 239
    .end local v4    # "c":C
    .end local v7    # "digitCount":I
    .end local v19    # "s":I
    .end local v23    # "u":I
    .restart local v18    # "s":I
    :cond_d
    add-int v24, v16, v22

    add-int v9, v10, v24

    .line 148
    .end local v10    # "ipat":I
    .restart local v9    # "ipat":I
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .end local v9    # "ipat":I
    .end local v18    # "s":I
    .restart local v7    # "digitCount":I
    .restart local v10    # "ipat":I
    .restart local v19    # "s":I
    .restart local v23    # "u":I
    :cond_e
    move/from16 v18, v19

    .end local v19    # "s":I
    .restart local v18    # "s":I
    goto/16 :goto_7

    .end local v7    # "digitCount":I
    .end local v18    # "s":I
    .end local v23    # "u":I
    .restart local v19    # "s":I
    :cond_f
    move/from16 v18, v19

    .end local v19    # "s":I
    .restart local v18    # "s":I
    goto/16 :goto_3
.end method
