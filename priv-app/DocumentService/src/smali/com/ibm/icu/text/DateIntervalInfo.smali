.class public Lcom/ibm/icu/text/DateIntervalInfo;
.super Ljava/lang/Object;
.source "DateIntervalInfo.java"

# interfaces
.implements Lcom/ibm/icu/util/Freezable;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    }
.end annotation


# static fields
.field static final CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

.field private static final DIICACHE:Lcom/ibm/icu/impl/ICUCache;

.field private static EARLIEST_FIRST_PREFIX:Ljava/lang/String; = null

.field private static FALLBACK_STRING:Ljava/lang/String; = null

.field private static LATEST_FIRST_PREFIX:Ljava/lang/String; = null

.field private static final MINIMUM_SUPPORTED_CALENDAR_FIELD:I = 0xc

.field static final currentSerialVersion:I = 0x1

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private fFallbackIntervalPattern:Ljava/lang/String;

.field private fFirstDateInPtnIsLaterDate:Z

.field private fIntervalPatterns:Ljava/util/HashMap;

.field private transient frozen:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 257
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "G"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "y"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "M"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "w"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "W"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "d"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "D"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "E"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "F"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "a"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "h"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "H"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "m"

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    .line 272
    const-string/jumbo v0, "fallback"

    sput-object v0, Lcom/ibm/icu/text/DateIntervalInfo;->FALLBACK_STRING:Ljava/lang/String;

    .line 273
    const-string/jumbo v0, "latestFirst:"

    sput-object v0, Lcom/ibm/icu/text/DateIntervalInfo;->LATEST_FIRST_PREFIX:Ljava/lang/String;

    .line 274
    const-string/jumbo v0, "earliestFirst:"

    sput-object v0, Lcom/ibm/icu/text/DateIntervalInfo;->EARLIEST_FIRST_PREFIX:Ljava/lang/String;

    .line 277
    new-instance v0, Lcom/ibm/icu/impl/SimpleCache;

    invoke-direct {v0}, Lcom/ibm/icu/impl/SimpleCache;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/DateIntervalInfo;->DIICACHE:Lcom/ibm/icu/impl/ICUCache;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282
    iput-boolean v1, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fFirstDateInPtnIsLaterDate:Z

    .line 286
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    .line 288
    iput-boolean v1, p0, Lcom/ibm/icu/text/DateIntervalInfo;->frozen:Z

    .line 307
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    .line 308
    const-string/jumbo v0, "{0} \u2013 {1}"

    iput-object v0, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fFallbackIntervalPattern:Ljava/lang/String;

    .line 309
    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/util/ULocale;)V
    .locals 2
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v1, 0x0

    .line 320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282
    iput-boolean v1, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fFirstDateInPtnIsLaterDate:Z

    .line 286
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    .line 288
    iput-boolean v1, p0, Lcom/ibm/icu/text/DateIntervalInfo;->frozen:Z

    .line 321
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateIntervalInfo;->initializeData(Lcom/ibm/icu/util/ULocale;)V

    .line 322
    return-void
.end method

.method private cloneUnfrozenDII()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 764
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/text/DateIntervalInfo;

    .line 765
    .local v4, "other":Lcom/ibm/icu/text/DateIntervalInfo;
    iget-object v9, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fFallbackIntervalPattern:Ljava/lang/String;

    iput-object v9, v4, Lcom/ibm/icu/text/DateIntervalInfo;->fFallbackIntervalPattern:Ljava/lang/String;

    .line 766
    iget-boolean v9, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fFirstDateInPtnIsLaterDate:Z

    iput-boolean v9, v4, Lcom/ibm/icu/text/DateIntervalInfo;->fFirstDateInPtnIsLaterDate:Z

    .line 767
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    iput-object v9, v4, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    .line 768
    iget-object v9, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 769
    .local v2, "iter":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 770
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 771
    .local v7, "skeleton":Ljava/lang/String;
    iget-object v9, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    invoke-virtual {v9, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/HashMap;

    .line 772
    .local v6, "patternsOfOneSkeleton":Ljava/util/HashMap;
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 773
    .local v3, "oneSetPtn":Ljava/util/HashMap;
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 774
    .local v5, "patternIter":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 775
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 776
    .local v0, "calField":Ljava/lang/String;
    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    .line 777
    .local v8, "value":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    invoke-virtual {v3, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 783
    .end local v0    # "calField":Ljava/lang/String;
    .end local v2    # "iter":Ljava/util/Iterator;
    .end local v3    # "oneSetPtn":Ljava/util/HashMap;
    .end local v4    # "other":Lcom/ibm/icu/text/DateIntervalInfo;
    .end local v5    # "patternIter":Ljava/util/Iterator;
    .end local v6    # "patternsOfOneSkeleton":Ljava/util/HashMap;
    .end local v7    # "skeleton":Ljava/lang/String;
    .end local v8    # "value":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    :catch_0
    move-exception v1

    .line 784
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v9, Ljava/lang/IllegalStateException;

    const-string/jumbo v10, "clone is not supported"

    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 779
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v2    # "iter":Ljava/util/Iterator;
    .restart local v3    # "oneSetPtn":Ljava/util/HashMap;
    .restart local v4    # "other":Lcom/ibm/icu/text/DateIntervalInfo;
    .restart local v5    # "patternIter":Ljava/util/Iterator;
    .restart local v6    # "patternsOfOneSkeleton":Ljava/util/HashMap;
    .restart local v7    # "skeleton":Ljava/lang/String;
    :cond_0
    :try_start_1
    iget-object v9, v4, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    invoke-virtual {v9, v7, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 781
    .end local v3    # "oneSetPtn":Ljava/util/HashMap;
    .end local v5    # "patternIter":Ljava/util/Iterator;
    .end local v6    # "patternsOfOneSkeleton":Ljava/util/HashMap;
    .end local v7    # "skeleton":Ljava/lang/String;
    :cond_1
    const/4 v9, 0x0

    iput-boolean v9, v4, Lcom/ibm/icu/text/DateIntervalInfo;->frozen:Z
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 782
    return-object v4
.end method

.method static genPatternInfo(Ljava/lang/String;Z)Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    .locals 4
    .param p0, "intervalPattern"    # Ljava/lang/String;
    .param p1, "laterDateFirst"    # Z

    .prologue
    .line 640
    invoke-static {p0}, Lcom/ibm/icu/text/DateIntervalInfo;->splitPatternInto2Part(Ljava/lang/String;)I

    move-result v2

    .line 642
    .local v2, "splitPoint":I
    const/4 v3, 0x0

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 643
    .local v0, "firstPart":Ljava/lang/String;
    const/4 v1, 0x0

    .line 644
    .local v1, "secondPart":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 645
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 648
    :cond_0
    new-instance v3, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    invoke-direct {v3, v0, v1, p1}, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v3
.end method

.method private initializeData(Lcom/ibm/icu/text/DateIntervalInfo;)V
    .locals 1
    .param p1, "dii"    # Lcom/ibm/icu/text/DateIntervalInfo;

    .prologue
    .line 353
    iget-object v0, p1, Lcom/ibm/icu/text/DateIntervalInfo;->fFallbackIntervalPattern:Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fFallbackIntervalPattern:Ljava/lang/String;

    .line 354
    iget-boolean v0, p1, Lcom/ibm/icu/text/DateIntervalInfo;->fFirstDateInPtnIsLaterDate:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fFirstDateInPtnIsLaterDate:Z

    .line 355
    iget-object v0, p1, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    .line 356
    return-void
.end method

.method private initializeData(Lcom/ibm/icu/util/ULocale;)V
    .locals 3
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 331
    invoke-virtual {p1}, Lcom/ibm/icu/util/ULocale;->toString()Ljava/lang/String;

    move-result-object v1

    .line 332
    .local v1, "key":Ljava/lang/String;
    sget-object v2, Lcom/ibm/icu/text/DateIntervalInfo;->DIICACHE:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v2, v1}, Lcom/ibm/icu/impl/ICUCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/DateIntervalInfo;

    .line 333
    .local v0, "dii":Lcom/ibm/icu/text/DateIntervalInfo;
    if-nez v0, :cond_0

    .line 335
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateIntervalInfo;->setup(Lcom/ibm/icu/util/ULocale;)V

    .line 339
    invoke-virtual {p0}, Lcom/ibm/icu/text/DateIntervalInfo;->clone()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "dii":Lcom/ibm/icu/text/DateIntervalInfo;
    check-cast v0, Lcom/ibm/icu/text/DateIntervalInfo;

    .line 340
    .restart local v0    # "dii":Lcom/ibm/icu/text/DateIntervalInfo;
    sget-object v2, Lcom/ibm/icu/text/DateIntervalInfo;->DIICACHE:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v2, v1, v0}, Lcom/ibm/icu/impl/ICUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 344
    :goto_0
    return-void

    .line 342
    :cond_0
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateIntervalInfo;->initializeData(Lcom/ibm/icu/text/DateIntervalInfo;)V

    goto :goto_0
.end method

.method static parseSkeleton(Ljava/lang/String;[I)V
    .locals 4
    .param p0, "skeleton"    # Ljava/lang/String;
    .param p1, "skeletonFieldWidth"    # [I

    .prologue
    .line 829
    const/16 v0, 0x41

    .line 830
    .local v0, "PATTERN_CHAR_BASE":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 831
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    sub-int/2addr v2, v0

    aget v3, p1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, p1, v2

    .line 830
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 833
    :cond_0
    return-void
.end method

.method private setIntervalPattern(Ljava/lang/String;Ljava/lang/String;Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;)V
    .locals 2
    .param p1, "skeleton"    # Ljava/lang/String;
    .param p2, "lrgDiffCalUnit"    # Ljava/lang/String;
    .param p3, "ptnInfo"    # Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    .prologue
    .line 624
    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 625
    .local v0, "patternsOfOneSkeleton":Ljava/util/HashMap;
    invoke-virtual {v0, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 626
    return-void
.end method

.method private setIntervalPatternInternally(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    .locals 7
    .param p1, "skeleton"    # Ljava/lang/String;
    .param p2, "lrgDiffCalUnit"    # Ljava/lang/String;
    .param p3, "intervalPattern"    # Ljava/lang/String;

    .prologue
    .line 587
    iget-object v6, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    invoke-virtual {v6, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    .line 588
    .local v4, "patternsOfOneSkeleton":Ljava/util/HashMap;
    const/4 v1, 0x0

    .line 589
    .local v1, "emptyHash":Z
    if-nez v4, :cond_0

    .line 590
    new-instance v4, Ljava/util/HashMap;

    .end local v4    # "patternsOfOneSkeleton":Ljava/util/HashMap;
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 591
    .restart local v4    # "patternsOfOneSkeleton":Ljava/util/HashMap;
    const/4 v1, 0x1

    .line 593
    :cond_0
    iget-boolean v3, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fFirstDateInPtnIsLaterDate:Z

    .line 595
    .local v3, "order":Z
    sget-object v6, Lcom/ibm/icu/text/DateIntervalInfo;->LATEST_FIRST_PREFIX:Ljava/lang/String;

    invoke-virtual {p3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 596
    const/4 v3, 0x1

    .line 597
    sget-object v6, Lcom/ibm/icu/text/DateIntervalInfo;->LATEST_FIRST_PREFIX:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v5

    .line 598
    .local v5, "prefixLength":I
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p3, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p3

    .line 604
    .end local v5    # "prefixLength":I
    :cond_1
    :goto_0
    invoke-static {p3, v3}, Lcom/ibm/icu/text/DateIntervalInfo;->genPatternInfo(Ljava/lang/String;Z)Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    move-result-object v2

    .line 606
    .local v2, "itvPtnInfo":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    invoke-virtual {v4, p2, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 607
    const/4 v6, 0x1

    if-ne v1, v6, :cond_2

    .line 608
    iget-object v6, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    invoke-virtual {v6, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 611
    :cond_2
    return-object v2

    .line 599
    .end local v2    # "itvPtnInfo":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    :cond_3
    sget-object v6, Lcom/ibm/icu/text/DateIntervalInfo;->EARLIEST_FIRST_PREFIX:Ljava/lang/String;

    invoke-virtual {p3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 600
    const/4 v3, 0x0

    .line 601
    sget-object v6, Lcom/ibm/icu/text/DateIntervalInfo;->EARLIEST_FIRST_PREFIX:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    .line 602
    .local v0, "earliestFirstLength":I
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p3, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p3

    goto :goto_0
.end method

.method private setup(Lcom/ibm/icu/util/ULocale;)V
    .locals 21
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 364
    const/16 v2, 0x13

    .line 365
    .local v2, "DEFAULT_HASH_SIZE":I
    new-instance v19, Ljava/util/HashMap;

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    .line 368
    const-string/jumbo v19, "{0} \u2013 {1}"

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DateIntervalInfo;->fFallbackIntervalPattern:Ljava/lang/String;

    .line 369
    new-instance v18, Ljava/util/HashSet;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashSet;-><init>()V

    .line 373
    .local v18, "skeletonSet":Ljava/util/HashSet;
    move-object/from16 v11, p1

    .line 376
    .local v11, "parentLocale":Lcom/ibm/icu/util/ULocale;
    :goto_0
    if-eqz v11, :cond_0

    :try_start_0
    sget-object v19, Lcom/ibm/icu/util/ULocale;->ROOT:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Lcom/ibm/icu/util/ULocale;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_0

    .line 377
    invoke-virtual {v11}, Lcom/ibm/icu/util/ULocale;->getName()Ljava/lang/String;

    move-result-object v10

    .line 378
    .local v10, "name":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v19

    if-nez v19, :cond_1

    .line 435
    .end local v10    # "name":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 382
    .restart local v10    # "name":Ljava/lang/String;
    :cond_1
    const-string/jumbo v19, "com/ibm/icu/impl/data/icudt40b"

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v15

    check-cast v15, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 384
    .local v15, "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v19, "calendar"

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v15

    .line 385
    const-string/jumbo v19, "gregorian"

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v5

    .line 387
    .local v5, "gregorianBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v19, "intervalFormats"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v8

    .line 390
    .local v8, "itvDtPtnResource":Lcom/ibm/icu/impl/ICUResourceBundle;
    sget-object v19, Lcom/ibm/icu/text/DateIntervalInfo;->FALLBACK_STRING:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getStringWithFallback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 392
    .local v4, "fallback":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/ibm/icu/text/DateIntervalInfo;->setFallbackIntervalPattern(Ljava/lang/String;)V

    .line 393
    invoke-virtual {v8}, Lcom/ibm/icu/impl/ICUResourceBundle;->getSize()I

    move-result v16

    .line 394
    .local v16, "size":I
    const/4 v6, 0x0

    .local v6, "index":I
    :goto_2
    move/from16 v0, v16

    if-ge v6, v0, :cond_b

    .line 395
    invoke-virtual {v8, v6}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v17

    .line 396
    .local v17, "skeleton":Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 394
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 399
    :cond_3
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 400
    sget-object v19, Lcom/ibm/icu/text/DateIntervalInfo;->FALLBACK_STRING:Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v19

    if-eqz v19, :cond_2

    .line 403
    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v7

    .line 405
    .local v7, "intervalPatterns":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {v7}, Lcom/ibm/icu/impl/ICUResourceBundle;->getSize()I

    move-result v14

    .line 406
    .local v14, "ptnNum":I
    const/4 v13, 0x0

    .local v13, "ptnIndex":I
    :goto_3
    if-ge v13, v14, :cond_2

    .line 407
    invoke-virtual {v7, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v9

    .line 408
    .local v9, "key":Ljava/lang/String;
    invoke-virtual {v7, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/util/UResourceBundle;->getString()Ljava/lang/String;

    move-result-object v12

    .line 410
    .local v12, "pattern":Ljava/lang/String;
    const/16 v3, 0x16

    .line 411
    .local v3, "calendarField":I
    sget-object v19, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v20, 0x1

    aget-object v19, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v19

    if-nez v19, :cond_6

    .line 412
    const/4 v3, 0x1

    .line 425
    :cond_4
    :goto_4
    const/16 v19, 0x16

    move/from16 v0, v19

    if-eq v3, v0, :cond_5

    .line 426
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v9, v12}, Lcom/ibm/icu/text/DateIntervalInfo;->setIntervalPatternInternally(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    .line 406
    :cond_5
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 413
    :cond_6
    sget-object v19, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v20, 0x2

    aget-object v19, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v19

    if-nez v19, :cond_7

    .line 414
    const/4 v3, 0x2

    .line 415
    goto :goto_4

    :cond_7
    sget-object v19, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v20, 0x5

    aget-object v19, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v19

    if-nez v19, :cond_8

    .line 416
    const/4 v3, 0x5

    .line 417
    goto :goto_4

    :cond_8
    sget-object v19, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v20, 0x9

    aget-object v19, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v19

    if-nez v19, :cond_9

    .line 418
    const/16 v3, 0x9

    .line 419
    goto :goto_4

    :cond_9
    sget-object v19, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v20, 0xa

    aget-object v19, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v19

    if-nez v19, :cond_a

    .line 420
    const/16 v3, 0xa

    .line 421
    goto :goto_4

    :cond_a
    sget-object v19, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v20, 0xc

    aget-object v19, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v19

    if-nez v19, :cond_4

    .line 422
    const/16 v3, 0xc

    goto :goto_4

    .line 430
    .end local v3    # "calendarField":I
    .end local v7    # "intervalPatterns":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v9    # "key":Ljava/lang/String;
    .end local v12    # "pattern":Ljava/lang/String;
    .end local v13    # "ptnIndex":I
    .end local v14    # "ptnNum":I
    .end local v17    # "skeleton":Ljava/lang/String;
    :cond_b
    invoke-virtual {v11}, Lcom/ibm/icu/util/ULocale;->getFallback()Lcom/ibm/icu/util/ULocale;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 431
    goto/16 :goto_0

    .line 432
    .end local v4    # "fallback":Ljava/lang/String;
    .end local v5    # "gregorianBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v6    # "index":I
    .end local v8    # "itvDtPtnResource":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v10    # "name":Ljava/lang/String;
    .end local v15    # "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v16    # "size":I
    :catch_0
    move-exception v19

    goto/16 :goto_1
.end method

.method private static splitPatternInto2Part(Ljava/lang/String;)I
    .locals 13
    .param p0, "intervalPattern"    # Ljava/lang/String;

    .prologue
    const/16 v12, 0x27

    const/4 v9, 0x1

    .line 444
    const/4 v5, 0x0

    .line 445
    .local v5, "inQuote":Z
    const/4 v7, 0x0

    .line 446
    .local v7, "prevCh":C
    const/4 v2, 0x0

    .line 452
    .local v2, "count":I
    const/16 v10, 0x3a

    new-array v6, v10, [I

    .line 454
    .local v6, "patternRepeated":[I
    const/16 v0, 0x41

    .line 461
    .local v0, "PATTERN_CHAR_BASE":I
    const/4 v3, 0x0

    .line 462
    .local v3, "foundRepetition":Z
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v4, v10, :cond_3

    .line 463
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 465
    .local v1, "ch":C
    if-eq v1, v7, :cond_0

    if-lez v2, :cond_0

    .line 467
    sub-int v10, v7, v0

    aget v8, v6, v10

    .line 468
    .local v8, "repeated":I
    if-nez v8, :cond_2

    .line 469
    sub-int v10, v7, v0

    aput v9, v6, v10

    .line 474
    const/4 v2, 0x0

    .line 476
    .end local v8    # "repeated":I
    :cond_0
    if-ne v1, v12, :cond_7

    .line 479
    add-int/lit8 v10, v4, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v11

    if-ge v10, v11, :cond_5

    add-int/lit8 v10, v4, 0x1

    invoke-virtual {p0, v10}, Ljava/lang/String;->charAt(I)C

    move-result v10

    if-ne v10, v12, :cond_5

    .line 481
    add-int/lit8 v4, v4, 0x1

    .line 462
    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 471
    .restart local v8    # "repeated":I
    :cond_2
    const/4 v3, 0x1

    .line 497
    .end local v1    # "ch":C
    .end local v8    # "repeated":I
    :cond_3
    if-lez v2, :cond_4

    if-nez v3, :cond_4

    .line 498
    sub-int v9, v7, v0

    aget v9, v6, v9

    if-nez v9, :cond_4

    .line 499
    const/4 v2, 0x0

    .line 502
    :cond_4
    sub-int v9, v4, v2

    return v9

    .line 483
    .restart local v1    # "ch":C
    :cond_5
    if-nez v5, :cond_6

    move v5, v9

    .line 485
    :goto_2
    goto :goto_1

    .line 483
    :cond_6
    const/4 v5, 0x0

    goto :goto_2

    .line 486
    :cond_7
    if-nez v5, :cond_1

    const/16 v10, 0x61

    if-lt v1, v10, :cond_8

    const/16 v10, 0x7a

    if-le v1, v10, :cond_9

    :cond_8
    const/16 v10, 0x41

    if-lt v1, v10, :cond_1

    const/16 v10, 0x5a

    if-gt v1, v10, :cond_1

    .line 489
    :cond_9
    move v7, v1

    .line 490
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private static stringNumeric(IIC)Z
    .locals 2
    .param p0, "fieldWidth"    # I
    .param p1, "anotherFieldWidth"    # I
    .param p2, "patternLetter"    # C

    .prologue
    const/4 v1, 0x2

    .line 851
    const/16 v0, 0x4d

    if-ne p2, v0, :cond_2

    .line 852
    if-gt p0, v1, :cond_0

    if-gt p1, v1, :cond_1

    :cond_0
    if-le p0, v1, :cond_2

    if-gt p1, v1, :cond_2

    .line 854
    :cond_1
    const/4 v0, 0x1

    .line 857
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 750
    iget-boolean v0, p0, Lcom/ibm/icu/text/DateIntervalInfo;->frozen:Z

    if-eqz v0, :cond_0

    .line 753
    .end local p0    # "this":Lcom/ibm/icu/text/DateIntervalInfo;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lcom/ibm/icu/text/DateIntervalInfo;
    :cond_0
    invoke-direct {p0}, Lcom/ibm/icu/text/DateIntervalInfo;->cloneUnfrozenDII()Ljava/lang/Object;

    move-result-object p0

    goto :goto_0
.end method

.method public cloneAsThawed()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 814
    invoke-direct {p0}, Lcom/ibm/icu/text/DateIntervalInfo;->cloneUnfrozenDII()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/DateIntervalInfo;

    move-object v0, v1

    check-cast v0, Lcom/ibm/icu/text/DateIntervalInfo;

    .line 815
    .local v0, "result":Lcom/ibm/icu/text/DateIntervalInfo;
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "a"    # Ljava/lang/Object;

    .prologue
    .line 952
    instance-of v1, p1, Lcom/ibm/icu/text/DateIntervalInfo;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 953
    check-cast v0, Lcom/ibm/icu/text/DateIntervalInfo;

    .line 954
    .local v0, "dtInfo":Lcom/ibm/icu/text/DateIntervalInfo;
    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    iget-object v2, v0, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 956
    .end local v0    # "dtInfo":Lcom/ibm/icu/text/DateIntervalInfo;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public freeze()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 804
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/DateIntervalInfo;->frozen:Z

    .line 805
    return-object p0
.end method

.method getBestSkeleton(Ljava/lang/String;)Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;
    .locals 21
    .param p1, "inputSkeleton"    # Ljava/lang/String;

    .prologue
    .line 876
    move-object/from16 v8, p1

    .line 877
    .local v8, "bestSkeleton":Ljava/lang/String;
    const/16 v19, 0x3a

    move/from16 v0, v19

    new-array v14, v0, [I

    .line 878
    .local v14, "inputSkeletonFieldWidth":[I
    const/16 v19, 0x3a

    move/from16 v0, v19

    new-array v0, v0, [I

    move-object/from16 v18, v0

    .line 880
    .local v18, "skeletonFieldWidth":[I
    const/16 v4, 0x1000

    .line 881
    .local v4, "DIFFERENT_FIELD":I
    const/16 v5, 0x100

    .line 882
    .local v5, "STRING_NUMERIC_DIFFERENCE":I
    const/16 v3, 0x41

    .line 887
    .local v3, "BASE":I
    const/16 v16, 0x0

    .line 888
    .local v16, "replaceZWithV":Z
    const/16 v19, 0x7a

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v19

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_0

    .line 889
    const/16 v19, 0x7a

    const/16 v20, 0x76

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    .line 890
    const/16 v16, 0x1

    .line 893
    :cond_0
    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/ibm/icu/text/DateIntervalInfo;->parseSkeleton(Ljava/lang/String;[I)V

    .line 894
    const v6, 0x7fffffff

    .line 899
    .local v6, "bestDistance":I
    const/4 v7, 0x0

    .line 900
    .local v7, "bestFieldDifference":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .line 901
    .local v15, "iter":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_9

    .line 902
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 904
    .local v17, "skeleton":Ljava/lang/String;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v12, v0, :cond_2

    .line 905
    const/16 v19, 0x0

    aput v19, v18, v12

    .line 904
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 907
    :cond_2
    invoke-static/range {v17 .. v18}, Lcom/ibm/icu/text/DateIntervalInfo;->parseSkeleton(Ljava/lang/String;[I)V

    .line 909
    const/4 v9, 0x0

    .line 910
    .local v9, "distance":I
    const/4 v10, 0x1

    .line 911
    .local v10, "fieldDifference":I
    const/4 v12, 0x0

    :goto_1
    array-length v0, v14

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v12, v0, :cond_7

    .line 912
    aget v13, v14, v12

    .line 913
    .local v13, "inputFieldWidth":I
    aget v11, v18, v12

    .line 914
    .local v11, "fieldWidth":I
    if-ne v13, v11, :cond_3

    .line 911
    :goto_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 917
    :cond_3
    if-nez v13, :cond_4

    .line 918
    const/4 v10, -0x1

    .line 919
    add-int/lit16 v9, v9, 0x1000

    .line 920
    goto :goto_2

    :cond_4
    if-nez v11, :cond_5

    .line 921
    const/4 v10, -0x1

    .line 922
    add-int/lit16 v9, v9, 0x1000

    .line 923
    goto :goto_2

    :cond_5
    add-int/lit8 v19, v12, 0x41

    move/from16 v0, v19

    int-to-char v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-static {v13, v11, v0}, Lcom/ibm/icu/text/DateIntervalInfo;->stringNumeric(IIC)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 925
    add-int/lit16 v9, v9, 0x100

    .line 926
    goto :goto_2

    .line 927
    :cond_6
    sub-int v19, v13, v11

    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(I)I

    move-result v19

    add-int v9, v9, v19

    goto :goto_2

    .line 930
    .end local v11    # "fieldWidth":I
    .end local v13    # "inputFieldWidth":I
    :cond_7
    if-ge v9, v6, :cond_8

    .line 931
    move-object/from16 v8, v17

    .line 932
    move v6, v9

    .line 933
    move v7, v10

    .line 935
    :cond_8
    if-nez v9, :cond_1

    .line 936
    const/4 v7, 0x0

    .line 940
    .end local v9    # "distance":I
    .end local v10    # "fieldDifference":I
    .end local v12    # "i":I
    .end local v17    # "skeleton":Ljava/lang/String;
    :cond_9
    if-eqz v16, :cond_a

    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v7, v0, :cond_a

    .line 941
    const/4 v7, 0x2

    .line 943
    :cond_a
    new-instance v19, Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;

    move-object/from16 v0, v19

    invoke-direct {v0, v8, v7}, Lcom/ibm/icu/text/DateIntervalFormat$BestMatchInfo;-><init>(Ljava/lang/String;I)V

    return-object v19
.end method

.method public getDefaultOrder()Z
    .locals 1

    .prologue
    .line 738
    iget-boolean v0, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fFirstDateInPtnIsLaterDate:Z

    return v0
.end method

.method public getFallbackIntervalPattern()Ljava/lang/String;
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fFallbackIntervalPattern:Ljava/lang/String;

    return-object v0
.end method

.method public getIntervalPattern(Ljava/lang/String;I)Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    .locals 4
    .param p1, "skeleton"    # Ljava/lang/String;
    .param p2, "field"    # I

    .prologue
    .line 665
    const/16 v2, 0xc

    if-le p2, v2, :cond_0

    .line 666
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "no support for field less than MINUTE"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 668
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    .line 669
    .local v1, "patternsOfOneSkeleton":Ljava/util/HashMap;
    if-eqz v1, :cond_1

    .line 670
    sget-object v2, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    .line 672
    .local v0, "intervalPattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    if-eqz v0, :cond_1

    .line 676
    .end local v0    # "intervalPattern":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fIntervalPatterns:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->hashCode()I

    move-result v0

    return v0
.end method

.method public isFrozen()Z
    .locals 1

    .prologue
    .line 795
    iget-boolean v0, p0, Lcom/ibm/icu/text/DateIntervalInfo;->frozen:Z

    return v0
.end method

.method public setFallbackIntervalPattern(Ljava/lang/String;)V
    .locals 4
    .param p1, "fallbackPattern"    # Ljava/lang/String;

    .prologue
    const/4 v3, -0x1

    .line 712
    iget-boolean v2, p0, Lcom/ibm/icu/text/DateIntervalInfo;->frozen:Z

    if-eqz v2, :cond_0

    .line 713
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v3, "no modification is allowed after DII is frozen"

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 715
    :cond_0
    const-string/jumbo v2, "{0}"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 716
    .local v0, "firstPatternIndex":I
    const-string/jumbo v2, "{1}"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 717
    .local v1, "secondPatternIndex":I
    if-eq v0, v3, :cond_1

    if-ne v1, v3, :cond_2

    .line 718
    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "no pattern {0} or pattern {1} in fallbackPattern"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 720
    :cond_2
    if-le v0, v1, :cond_3

    .line 721
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fFirstDateInPtnIsLaterDate:Z

    .line 723
    :cond_3
    iput-object p1, p0, Lcom/ibm/icu/text/DateIntervalInfo;->fFallbackIntervalPattern:Ljava/lang/String;

    .line 724
    return-void
.end method

.method public setIntervalPattern(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3
    .param p1, "skeleton"    # Ljava/lang/String;
    .param p2, "lrgDiffCalUnit"    # I
    .param p3, "intervalPattern"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x5

    .line 545
    iget-boolean v1, p0, Lcom/ibm/icu/text/DateIntervalInfo;->frozen:Z

    if-eqz v1, :cond_0

    .line 546
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v2, "no modification is allowed after DII is frozen"

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 548
    :cond_0
    const/16 v1, 0xc

    if-le p2, v1, :cond_1

    .line 549
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "calendar field is larger than MINIMUM_SUPPORTED_CALENDAR_FIELD"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 552
    :cond_1
    sget-object v1, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    aget-object v1, v1, p2

    invoke-direct {p0, p1, v1, p3}, Lcom/ibm/icu/text/DateIntervalInfo;->setIntervalPatternInternally(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    move-result-object v0

    .line 555
    .local v0, "ptnInfo":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    const/16 v1, 0xb

    if-ne p2, v1, :cond_3

    .line 556
    sget-object v1, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-direct {p0, p1, v1, v0}, Lcom/ibm/icu/text/DateIntervalInfo;->setIntervalPattern(Ljava/lang/String;Ljava/lang/String;Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;)V

    .line 559
    sget-object v1, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-direct {p0, p1, v1, v0}, Lcom/ibm/icu/text/DateIntervalInfo;->setIntervalPattern(Ljava/lang/String;Ljava/lang/String;Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;)V

    .line 568
    :cond_2
    :goto_0
    return-void

    .line 562
    :cond_3
    if-eq p2, v2, :cond_4

    const/4 v1, 0x7

    if-ne p2, v1, :cond_2

    .line 564
    :cond_4
    sget-object v1, Lcom/ibm/icu/text/DateIntervalInfo;->CALENDAR_FIELD_TO_PATTERN_LETTER:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-direct {p0, p1, v1, v0}, Lcom/ibm/icu/text/DateIntervalInfo;->setIntervalPattern(Ljava/lang/String;Ljava/lang/String;Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;)V

    goto :goto_0
.end method
