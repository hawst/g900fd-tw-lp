.class public final Lcom/ibm/icu/text/Normalizer;
.super Ljava/lang/Object;
.source "Normalizer.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/Normalizer$1;,
        Lcom/ibm/icu/text/Normalizer$IsNextTrueStarter;,
        Lcom/ibm/icu/text/Normalizer$IsNextNFDSafe;,
        Lcom/ibm/icu/text/Normalizer$IsNextBoundary;,
        Lcom/ibm/icu/text/Normalizer$IsPrevTrueStarter;,
        Lcom/ibm/icu/text/Normalizer$IsPrevNFDSafe;,
        Lcom/ibm/icu/text/Normalizer$IsPrevBoundary;,
        Lcom/ibm/icu/text/Normalizer$QuickCheckResult;,
        Lcom/ibm/icu/text/Normalizer$FCDMode;,
        Lcom/ibm/icu/text/Normalizer$NFKCMode;,
        Lcom/ibm/icu/text/Normalizer$NFCMode;,
        Lcom/ibm/icu/text/Normalizer$NFKDMode;,
        Lcom/ibm/icu/text/Normalizer$NFDMode;,
        Lcom/ibm/icu/text/Normalizer$Mode;
    }
.end annotation


# static fields
.field public static final COMPARE_CODE_POINT_ORDER:I = 0x8000

.field public static final COMPARE_IGNORE_CASE:I = 0x10000

.field public static final COMPARE_NORM_OPTIONS_SHIFT:I = 0x14

.field private static final COMPAT_BIT:I = 0x1

.field public static final COMPOSE:Lcom/ibm/icu/text/Normalizer$Mode;

.field private static final COMPOSE_BIT:I = 0x4

.field public static final COMPOSE_COMPAT:Lcom/ibm/icu/text/Normalizer$Mode;

.field public static final DECOMP:Lcom/ibm/icu/text/Normalizer$Mode;

.field private static final DECOMP_BIT:I = 0x2

.field public static final DECOMP_COMPAT:Lcom/ibm/icu/text/Normalizer$Mode;

.field public static final DEFAULT:Lcom/ibm/icu/text/Normalizer$Mode;

.field public static final DONE:I = -0x1

.field public static final FCD:Lcom/ibm/icu/text/Normalizer$Mode;

.field public static final FOLD_CASE_DEFAULT:I = 0x0

.field public static final FOLD_CASE_EXCLUDE_SPECIAL_I:I = 0x1

.field public static final IGNORE_HANGUL:I = 0x1

.field public static final INPUT_IS_FCD:I = 0x20000

.field private static final MAX_BUF_SIZE_COMPOSE:I = 0x2

.field private static final MAX_BUF_SIZE_DECOMPOSE:I = 0x3

.field public static final MAYBE:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

.field public static final NFC:Lcom/ibm/icu/text/Normalizer$Mode;

.field public static final NFD:Lcom/ibm/icu/text/Normalizer$Mode;

.field public static final NFKC:Lcom/ibm/icu/text/Normalizer$Mode;

.field public static final NFKD:Lcom/ibm/icu/text/Normalizer$Mode;

.field public static final NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

.field public static final NONE:Lcom/ibm/icu/text/Normalizer$Mode;

.field public static final NO_OP:Lcom/ibm/icu/text/Normalizer$Mode;

.field public static final UNICODE_3_2:I = 0x20

.field public static final YES:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;


# instance fields
.field private buffer:[C

.field private bufferLimit:I

.field private bufferPos:I

.field private bufferStart:I

.field private currentIndex:I

.field private mode:Lcom/ibm/icu/text/Normalizer$Mode;

.field private nextIndex:I

.field private options:I

.field private text:Lcom/ibm/icu/text/UCharacterIterator;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 259
    new-instance v0, Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-direct {v0, v3, v2}, Lcom/ibm/icu/text/Normalizer$Mode;-><init>(ILcom/ibm/icu/text/Normalizer$1;)V

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->NONE:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 265
    new-instance v0, Lcom/ibm/icu/text/Normalizer$NFDMode;

    invoke-direct {v0, v4, v2}, Lcom/ibm/icu/text/Normalizer$NFDMode;-><init>(ILcom/ibm/icu/text/Normalizer$1;)V

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 327
    new-instance v0, Lcom/ibm/icu/text/Normalizer$NFKDMode;

    const/4 v1, 0x3

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/Normalizer$NFKDMode;-><init>(ILcom/ibm/icu/text/Normalizer$1;)V

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->NFKD:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 389
    new-instance v0, Lcom/ibm/icu/text/Normalizer$NFCMode;

    const/4 v1, 0x4

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/Normalizer$NFCMode;-><init>(ILcom/ibm/icu/text/Normalizer$1;)V

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->NFC:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 448
    sget-object v0, Lcom/ibm/icu/text/Normalizer;->NFC:Lcom/ibm/icu/text/Normalizer$Mode;

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->DEFAULT:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 454
    new-instance v0, Lcom/ibm/icu/text/Normalizer$NFKCMode;

    const/4 v1, 0x5

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/Normalizer$NFKCMode;-><init>(ILcom/ibm/icu/text/Normalizer$1;)V

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->NFKC:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 512
    new-instance v0, Lcom/ibm/icu/text/Normalizer$FCDMode;

    const/4 v1, 0x6

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/Normalizer$FCDMode;-><init>(ILcom/ibm/icu/text/Normalizer$1;)V

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->FCD:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 565
    sget-object v0, Lcom/ibm/icu/text/Normalizer;->NONE:Lcom/ibm/icu/text/Normalizer$Mode;

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->NO_OP:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 583
    sget-object v0, Lcom/ibm/icu/text/Normalizer;->NFC:Lcom/ibm/icu/text/Normalizer$Mode;

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->COMPOSE:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 601
    sget-object v0, Lcom/ibm/icu/text/Normalizer;->NFKC:Lcom/ibm/icu/text/Normalizer$Mode;

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->COMPOSE_COMPAT:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 619
    sget-object v0, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->DECOMP:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 637
    sget-object v0, Lcom/ibm/icu/text/Normalizer;->NFKD:Lcom/ibm/icu/text/Normalizer$Mode;

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->DECOMP_COMPAT:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 674
    new-instance v0, Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/Normalizer$QuickCheckResult;-><init>(ILcom/ibm/icu/text/Normalizer$1;)V

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    .line 680
    new-instance v0, Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    invoke-direct {v0, v3, v2}, Lcom/ibm/icu/text/Normalizer$QuickCheckResult;-><init>(ILcom/ibm/icu/text/Normalizer$1;)V

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->YES:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    .line 687
    new-instance v0, Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    invoke-direct {v0, v4, v2}, Lcom/ibm/icu/text/Normalizer$QuickCheckResult;-><init>(ILcom/ibm/icu/text/Normalizer$1;)V

    sput-object v0, Lcom/ibm/icu/text/Normalizer;->MAYBE:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/text/UCharacterIterator;Lcom/ibm/icu/text/Normalizer$Mode;I)V
    .locals 3
    .param p1, "iter"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p2, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p3, "options"    # I

    .prologue
    const/4 v2, 0x0

    .line 803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    const/16 v1, 0x64

    new-array v1, v1, [C

    iput-object v1, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    .line 124
    iput v2, p0, Lcom/ibm/icu/text/Normalizer;->bufferStart:I

    .line 125
    iput v2, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    .line 126
    iput v2, p0, Lcom/ibm/icu/text/Normalizer;->bufferLimit:I

    .line 135
    sget-object v1, Lcom/ibm/icu/text/Normalizer;->NFC:Lcom/ibm/icu/text/Normalizer$Mode;

    iput-object v1, p0, Lcom/ibm/icu/text/Normalizer;->mode:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 136
    iput v2, p0, Lcom/ibm/icu/text/Normalizer;->options:I

    .line 805
    :try_start_0
    invoke-virtual {p1}, Lcom/ibm/icu/text/UCharacterIterator;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/UCharacterIterator;

    iput-object v1, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    .line 806
    iput-object p2, p0, Lcom/ibm/icu/text/Normalizer;->mode:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 807
    iput p3, p0, Lcom/ibm/icu/text/Normalizer;->options:I
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 811
    return-void

    .line 808
    :catch_0
    move-exception v0

    .line 809
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/CloneNotSupportedException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p3, "opt"    # I

    .prologue
    const/4 v1, 0x0

    .line 763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    const/16 v0, 0x64

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    .line 124
    iput v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferStart:I

    .line 125
    iput v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    .line 126
    iput v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferLimit:I

    .line 135
    sget-object v0, Lcom/ibm/icu/text/Normalizer;->NFC:Lcom/ibm/icu/text/Normalizer$Mode;

    iput-object v0, p0, Lcom/ibm/icu/text/Normalizer;->mode:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 136
    iput v1, p0, Lcom/ibm/icu/text/Normalizer;->options:I

    .line 764
    invoke-static {p1}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    .line 765
    iput-object p2, p0, Lcom/ibm/icu/text/Normalizer;->mode:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 766
    iput p3, p0, Lcom/ibm/icu/text/Normalizer;->options:I

    .line 767
    return-void
.end method

.method public constructor <init>(Ljava/text/CharacterIterator;Lcom/ibm/icu/text/Normalizer$Mode;I)V
    .locals 2
    .param p1, "iter"    # Ljava/text/CharacterIterator;
    .param p2, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p3, "opt"    # I

    .prologue
    const/4 v1, 0x0

    .line 784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    const/16 v0, 0x64

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    .line 124
    iput v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferStart:I

    .line 125
    iput v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    .line 126
    iput v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferLimit:I

    .line 135
    sget-object v0, Lcom/ibm/icu/text/Normalizer;->NFC:Lcom/ibm/icu/text/Normalizer$Mode;

    iput-object v0, p0, Lcom/ibm/icu/text/Normalizer;->mode:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 136
    iput v1, p0, Lcom/ibm/icu/text/Normalizer;->options:I

    .line 785
    invoke-interface {p1}, Ljava/text/CharacterIterator;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/CharacterIterator;

    invoke-static {v0}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance(Ljava/text/CharacterIterator;)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    .line 788
    iput-object p2, p0, Lcom/ibm/icu/text/Normalizer;->mode:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 789
    iput p3, p0, Lcom/ibm/icu/text/Normalizer;->options:I

    .line 790
    return-void
.end method

.method static access$1000(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # I

    .prologue
    .line 118
    invoke-static {p0, p1}, Lcom/ibm/icu/text/Normalizer;->makeFCD(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static access$1200(Lcom/ibm/icu/text/UCharacterIterator;II[C)J
    .locals 2
    .param p0, "x0"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # [C

    .prologue
    .line 118
    invoke-static {p0, p1, p2, p3}, Lcom/ibm/icu/text/Normalizer;->getPrevNorm32(Lcom/ibm/icu/text/UCharacterIterator;II[C)J

    move-result-wide v0

    return-wide v0
.end method

.method static access$1300(Lcom/ibm/icu/text/UCharacterIterator;II[I)J
    .locals 2
    .param p0, "x0"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # [I

    .prologue
    .line 118
    invoke-static {p0, p1, p2, p3}, Lcom/ibm/icu/text/Normalizer;->getNextNorm32(Lcom/ibm/icu/text/UCharacterIterator;II[I)J

    move-result-wide v0

    return-wide v0
.end method

.method private clearBuffer()V
    .locals 1

    .prologue
    .line 2637
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    iput v0, p0, Lcom/ibm/icu/text/Normalizer;->bufferStart:I

    iput v0, p0, Lcom/ibm/icu/text/Normalizer;->bufferLimit:I

    .line 2638
    return-void
.end method

.method public static compare(III)I
    .locals 2
    .param p0, "char32a"    # I
    .param p1, "char32b"    # I
    .param p2, "options"    # I

    .prologue
    .line 1502
    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/ibm/icu/text/Normalizer;->compare(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static compare(ILjava/lang/String;I)I
    .locals 1
    .param p0, "char32a"    # I
    .param p1, "str2"    # Ljava/lang/String;
    .param p2, "options"    # I

    .prologue
    .line 1516
    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/ibm/icu/text/Normalizer;->compare(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static compare(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 7
    .param p0, "s1"    # Ljava/lang/String;
    .param p1, "s2"    # Ljava/lang/String;
    .param p2, "options"    # I

    .prologue
    const/4 v1, 0x0

    .line 1449
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    move v4, v1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/ibm/icu/text/Normalizer;->compare([CII[CIII)I

    move-result v0

    return v0
.end method

.method public static compare([CII[CIII)I
    .locals 1
    .param p0, "s1"    # [C
    .param p1, "s1Start"    # I
    .param p2, "s1Limit"    # I
    .param p3, "s2"    # [C
    .param p4, "s2Start"    # I
    .param p5, "s2Limit"    # I
    .param p6, "options"    # I

    .prologue
    .line 1409
    invoke-static/range {p0 .. p6}, Lcom/ibm/icu/text/Normalizer;->internalCompare([CII[CIII)I

    move-result v0

    return v0
.end method

.method public static compare([C[CI)I
    .locals 7
    .param p0, "s1"    # [C
    .param p1, "s2"    # [C
    .param p2, "options"    # I

    .prologue
    const/4 v1, 0x0

    .line 1488
    array-length v2, p0

    array-length v5, p1

    move-object v0, p0

    move-object v3, p1

    move v4, v1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/ibm/icu/text/Normalizer;->compare([CII[CIII)I

    move-result v0

    return v0
.end method

.method public static compose([CII[CIIZI)I
    .locals 10
    .param p0, "src"    # [C
    .param p1, "srcStart"    # I
    .param p2, "srcLimit"    # I
    .param p3, "dest"    # [C
    .param p4, "destStart"    # I
    .param p5, "destLimit"    # I
    .param p6, "compat"    # Z
    .param p7, "options"    # I

    .prologue
    .line 951
    invoke-static/range {p7 .. p7}, Lcom/ibm/icu/impl/NormalizerImpl;->getNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v8

    .line 954
    .local v8, "nx":Lcom/ibm/icu/text/UnicodeSet;
    move/from16 v0, p7

    and-int/lit16 v0, v0, -0x3100

    move/from16 p7, v0

    .line 956
    if-eqz p6, :cond_0

    .line 957
    move/from16 v0, p7

    or-int/lit16 v0, v0, 0x1000

    move/from16 p7, v0

    :cond_0
    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p7

    .line 960
    invoke-static/range {v1 .. v8}, Lcom/ibm/icu/impl/NormalizerImpl;->compose([CII[CIIILcom/ibm/icu/text/UnicodeSet;)I

    move-result v9

    .line 963
    .local v9, "length":I
    sub-int v1, p5, p4

    if-gt v9, v1, :cond_1

    .line 964
    return v9

    .line 966
    :cond_1
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static compose([C[CZI)I
    .locals 9
    .param p0, "source"    # [C
    .param p1, "target"    # [C
    .param p2, "compat"    # Z
    .param p3, "options"    # I

    .prologue
    const/4 v1, 0x0

    .line 910
    invoke-static {p3}, Lcom/ibm/icu/impl/NormalizerImpl;->getNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v7

    .line 913
    .local v7, "nx":Lcom/ibm/icu/text/UnicodeSet;
    and-int/lit16 p3, p3, -0x3100

    .line 915
    if-eqz p2, :cond_0

    .line 916
    or-int/lit16 p3, p3, 0x1000

    .line 919
    :cond_0
    array-length v2, p0

    array-length v5, p1

    move-object v0, p0

    move-object v3, p1

    move v4, v1

    move v6, p3

    invoke-static/range {v0 .. v7}, Lcom/ibm/icu/impl/NormalizerImpl;->compose([CII[CIIILcom/ibm/icu/text/UnicodeSet;)I

    move-result v8

    .line 922
    .local v8, "length":I
    array-length v0, p1

    if-gt v8, v0, :cond_1

    .line 923
    return v8

    .line 925
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static compose(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "compat"    # Z

    .prologue
    .line 854
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/ibm/icu/text/Normalizer;->compose(Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static compose(Ljava/lang/String;ZI)Ljava/lang/String;
    .locals 9
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "compat"    # Z
    .param p2, "options"    # I

    .prologue
    const/4 v1, 0x0

    .line 870
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    new-array v3, v2, [C

    .line 871
    .local v3, "dest":[C
    const/4 v8, 0x0

    .line 872
    .local v8, "destSize":I
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 873
    .local v0, "src":[C
    invoke-static {p2}, Lcom/ibm/icu/impl/NormalizerImpl;->getNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v7

    .line 876
    .local v7, "nx":Lcom/ibm/icu/text/UnicodeSet;
    and-int/lit16 p2, p2, -0x3100

    .line 878
    if-eqz p1, :cond_0

    .line 879
    or-int/lit16 p2, p2, 0x1000

    .line 883
    :cond_0
    :goto_0
    array-length v2, v0

    array-length v5, v3

    move v4, v1

    move v6, p2

    invoke-static/range {v0 .. v7}, Lcom/ibm/icu/impl/NormalizerImpl;->compose([CII[CIIILcom/ibm/icu/text/UnicodeSet;)I

    move-result v8

    .line 886
    array-length v2, v3

    if-gt v8, v2, :cond_1

    .line 887
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3, v1, v8}, Ljava/lang/String;-><init>([CII)V

    return-object v2

    .line 889
    :cond_1
    new-array v3, v8, [C

    .line 891
    goto :goto_0
.end method

.method public static concatenate([CII[CII[CIILcom/ibm/icu/text/Normalizer$Mode;I)I
    .locals 23
    .param p0, "left"    # [C
    .param p1, "leftStart"    # I
    .param p2, "leftLimit"    # I
    .param p3, "right"    # [C
    .param p4, "rightStart"    # I
    .param p5, "rightLimit"    # I
    .param p6, "dest"    # [C
    .param p7, "destStart"    # I
    .param p8, "destLimit"    # I
    .param p9, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p10, "options"    # I

    .prologue
    .line 1574
    if-nez p6, :cond_0

    .line 1575
    new-instance v6, Ljava/lang/IllegalArgumentException;

    invoke-direct {v6}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v6

    .line 1579
    :cond_0
    move-object/from16 v0, p3

    move-object/from16 v1, p6

    if-ne v0, v1, :cond_1

    move/from16 v0, p4

    move/from16 v1, p8

    if-ge v0, v1, :cond_1

    move/from16 v0, p7

    move/from16 v1, p5

    if-ge v0, v1, :cond_1

    .line 1580
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v7, "overlapping right and dst ranges"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 1602
    :cond_1
    invoke-static/range {p0 .. p2}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance([CII)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v3

    .line 1604
    .local v3, "iter":Lcom/ibm/icu/text/UCharacterIterator;
    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->getLength()I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 1605
    const/16 v6, 0x64

    new-array v4, v6, [C

    .line 1607
    .local v4, "buffer":[C
    const/4 v5, 0x0

    array-length v6, v4

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v7, p9

    move/from16 v10, p10

    invoke-static/range {v3 .. v10}, Lcom/ibm/icu/text/Normalizer;->previous(Lcom/ibm/icu/text/UCharacterIterator;[CIILcom/ibm/icu/text/Normalizer$Mode;Z[ZI)I

    move-result v5

    .line 1609
    .local v5, "bufferLength":I
    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v19

    .line 1611
    .local v19, "leftBoundary":I
    array-length v6, v4

    if-le v5, v6, :cond_2

    .line 1612
    array-length v6, v4

    mul-int/lit8 v6, v6, 0x2

    new-array v0, v6, [C

    move-object/from16 v20, v0

    .line 1613
    .local v20, "newBuf":[C
    move-object/from16 v4, v20

    .line 1614
    const/16 v20, 0x0

    .line 1616
    const/4 v6, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1, v4, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1624
    .end local v20    # "newBuf":[C
    :cond_2
    invoke-static/range {p3 .. p5}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance([CII)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v3

    .line 1626
    array-length v6, v4

    sub-int/2addr v6, v5

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v7, p9

    move/from16 v10, p10

    invoke-static/range {v3 .. v10}, Lcom/ibm/icu/text/Normalizer;->next(Lcom/ibm/icu/text/UCharacterIterator;[CIILcom/ibm/icu/text/Normalizer$Mode;Z[ZI)I

    move-result v21

    .line 1629
    .local v21, "rightBoundary":I
    array-length v6, v4

    if-le v5, v6, :cond_3

    .line 1630
    array-length v6, v4

    mul-int/lit8 v6, v6, 0x2

    new-array v0, v6, [C

    move-object/from16 v20, v0

    .line 1631
    .restart local v20    # "newBuf":[C
    move-object/from16 v4, v20

    .line 1632
    const/16 v20, 0x0

    .line 1634
    move-object/from16 v0, p3

    move/from16 v1, v21

    move/from16 v2, v21

    invoke-static {v0, v1, v4, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1638
    .end local v20    # "newBuf":[C
    :cond_3
    add-int v5, v5, v21

    .line 1641
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    if-eq v0, v1, :cond_4

    if-lez v19, :cond_4

    if-lez p8, :cond_4

    .line 1642
    const/4 v6, 0x0

    const/4 v7, 0x0

    move/from16 v0, v19

    move/from16 v1, p8

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v8

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    invoke-static {v0, v6, v1, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1644
    :cond_4
    move/from16 v10, v19

    .line 1647
    .local v10, "destLength":I
    move/from16 v0, p8

    if-le v0, v10, :cond_6

    .line 1648
    const/4 v7, 0x0

    move-object v6, v4

    move v8, v5

    move-object/from16 v9, p6

    move/from16 v11, p8

    move-object/from16 v12, p9

    move/from16 v13, p10

    invoke-static/range {v6 .. v13}, Lcom/ibm/icu/text/Normalizer;->normalize([CII[CIILcom/ibm/icu/text/Normalizer$Mode;I)I

    move-result v6

    add-int/2addr v10, v6

    .line 1656
    :goto_0
    add-int p4, p4, v21

    .line 1657
    sub-int v22, p5, p4

    .line 1658
    .local v22, "rightLength":I
    if-lez v22, :cond_5

    move/from16 v0, p8

    if-le v0, v10, :cond_5

    .line 1659
    move/from16 v0, v22

    invoke-static {v0, v10}, Ljava/lang/Math;->min(II)I

    move-result v6

    move-object/from16 v0, p3

    move/from16 v1, p4

    move-object/from16 v2, p6

    invoke-static {v0, v1, v2, v10, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1663
    :cond_5
    add-int v10, v10, v22

    .line 1665
    sub-int v6, p8, p7

    if-gt v10, v6, :cond_7

    .line 1666
    return v10

    .line 1652
    .end local v22    # "rightLength":I
    :cond_6
    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v11, v4

    move v13, v5

    move-object/from16 v17, p9

    move/from16 v18, p10

    invoke-static/range {v11 .. v18}, Lcom/ibm/icu/text/Normalizer;->normalize([CII[CIILcom/ibm/icu/text/Normalizer$Mode;I)I

    move-result v6

    add-int/2addr v10, v6

    goto :goto_0

    .line 1668
    .restart local v22    # "rightLength":I
    :cond_7
    new-instance v6, Ljava/lang/IndexOutOfBoundsException;

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method public static concatenate(Ljava/lang/String;Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Ljava/lang/String;
    .locals 12
    .param p0, "left"    # Ljava/lang/String;
    .param p1, "right"    # Ljava/lang/String;
    .param p2, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p3, "options"    # I

    .prologue
    const/4 v1, 0x0

    .line 1743
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x3

    new-array v6, v0, [C

    .line 1746
    .local v6, "result":[C
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    array-length v8, v6

    move v4, v1

    move v7, v1

    move-object v9, p2

    move v10, p3

    invoke-static/range {v0 .. v10}, Lcom/ibm/icu/text/Normalizer;->concatenate([CII[CII[CIILcom/ibm/icu/text/Normalizer$Mode;I)I

    move-result v11

    .line 1750
    .local v11, "length":I
    array-length v0, v6

    if-gt v11, v0, :cond_0

    .line 1751
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6, v1, v11}, Ljava/lang/String;-><init>([CII)V

    return-object v0

    .line 1753
    :cond_0
    new-array v6, v11, [C

    .line 1755
    goto :goto_0
.end method

.method public static concatenate([C[CLcom/ibm/icu/text/Normalizer$Mode;I)Ljava/lang/String;
    .locals 12
    .param p0, "left"    # [C
    .param p1, "right"    # [C
    .param p2, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p3, "options"    # I

    .prologue
    const/4 v1, 0x0

    .line 1700
    array-length v0, p0

    array-length v2, p1

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x3

    new-array v6, v0, [C

    .line 1703
    .local v6, "result":[C
    :goto_0
    array-length v2, p0

    array-length v5, p1

    array-length v8, v6

    move-object v0, p0

    move-object v3, p1

    move v4, v1

    move v7, v1

    move-object v9, p2

    move v10, p3

    invoke-static/range {v0 .. v10}, Lcom/ibm/icu/text/Normalizer;->concatenate([CII[CII[CIILcom/ibm/icu/text/Normalizer$Mode;I)I

    move-result v11

    .line 1707
    .local v11, "length":I
    array-length v0, v6

    if-gt v11, v0, :cond_0

    .line 1708
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6, v1, v11}, Ljava/lang/String;-><init>([CII)V

    return-object v0

    .line 1710
    :cond_0
    new-array v6, v11, [C

    .line 1712
    goto :goto_0
.end method

.method public static decompose([CII[CIIZI)I
    .locals 10
    .param p0, "src"    # [C
    .param p1, "srcStart"    # I
    .param p2, "srcLimit"    # I
    .param p3, "dest"    # [C
    .param p4, "destStart"    # I
    .param p5, "destLimit"    # I
    .param p6, "compat"    # Z
    .param p7, "options"    # I

    .prologue
    .line 1067
    const/4 v0, 0x1

    new-array v7, v0, [I

    .line 1068
    .local v7, "trailCC":[I
    invoke-static/range {p7 .. p7}, Lcom/ibm/icu/impl/NormalizerImpl;->getNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v8

    .local v8, "nx":Lcom/ibm/icu/text/UnicodeSet;
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    .line 1069
    invoke-static/range {v0 .. v8}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose([CII[CIIZ[ILcom/ibm/icu/text/UnicodeSet;)I

    move-result v9

    .line 1072
    .local v9, "length":I
    sub-int v0, p5, p4

    if-gt v9, v0, :cond_0

    .line 1073
    return v9

    .line 1075
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static decompose([C[CZI)I
    .locals 10
    .param p0, "source"    # [C
    .param p1, "target"    # [C
    .param p2, "compat"    # Z
    .param p3, "options"    # I

    .prologue
    const/4 v1, 0x0

    .line 1033
    const/4 v0, 0x1

    new-array v7, v0, [I

    .line 1034
    .local v7, "trailCC":[I
    invoke-static {p3}, Lcom/ibm/icu/impl/NormalizerImpl;->getNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v8

    .line 1035
    .local v8, "nx":Lcom/ibm/icu/text/UnicodeSet;
    array-length v2, p0

    array-length v5, p1

    move-object v0, p0

    move-object v3, p1

    move v4, v1

    move v6, p2

    invoke-static/range {v0 .. v8}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose([CII[CIIZ[ILcom/ibm/icu/text/UnicodeSet;)I

    move-result v9

    .line 1038
    .local v9, "length":I
    array-length v0, p1

    if-gt v9, v0, :cond_0

    .line 1039
    return v9

    .line 1041
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static decompose(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "compat"    # Z

    .prologue
    .line 984
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static decompose(Ljava/lang/String;ZI)Ljava/lang/String;
    .locals 10
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "compat"    # Z
    .param p2, "options"    # I

    .prologue
    const/4 v1, 0x0

    .line 1000
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    new-array v3, v0, [C

    .line 1001
    .local v3, "dest":[C
    const/4 v0, 0x1

    new-array v7, v0, [I

    .line 1002
    .local v7, "trailCC":[I
    const/4 v9, 0x0

    .line 1003
    .local v9, "destSize":I
    invoke-static {p2}, Lcom/ibm/icu/impl/NormalizerImpl;->getNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v8

    .line 1005
    .local v8, "nx":Lcom/ibm/icu/text/UnicodeSet;
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    array-length v5, v3

    move v4, v1

    move v6, p1

    invoke-static/range {v0 .. v8}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose([CII[CIIZ[ILcom/ibm/icu/text/UnicodeSet;)I

    move-result v9

    .line 1008
    array-length v0, v3

    if-gt v9, v0, :cond_0

    .line 1009
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3, v1, v9}, Ljava/lang/String;-><init>([CII)V

    return-object v0

    .line 1011
    :cond_0
    new-array v3, v9, [C

    .line 1013
    goto :goto_0
.end method

.method private static findNextIterationBoundary(Lcom/ibm/icu/text/UCharacterIterator;Lcom/ibm/icu/text/Normalizer$IsNextBoundary;II[C)I
    .locals 10
    .param p0, "src"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p1, "obj"    # Lcom/ibm/icu/text/Normalizer$IsNextBoundary;
    .param p2, "minC"    # I
    .param p3, "mask"    # I
    .param p4, "buffer"    # [C

    .prologue
    const/4 v7, 0x2

    const/4 v4, -0x1

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 2506
    new-array v2, v7, [I

    .line 2507
    .local v2, "chars":[I
    const/4 v0, 0x0

    .line 2509
    .local v0, "bufferIndex":I
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v5

    if-ne v5, v4, :cond_0

    move v4, v8

    .line 2553
    :goto_0
    return v4

    .line 2513
    :cond_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    move-result v5

    aput v5, v2, v8

    .line 2514
    aget v5, v2, v8

    int-to-char v5, v5

    aput-char v5, p4, v8

    .line 2515
    const/4 v0, 0x1

    .line 2517
    aget v5, v2, v8

    int-to-char v5, v5

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v5

    if-eq v5, v4, :cond_1

    .line 2519
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    move-result v5

    aput v5, v2, v6

    int-to-char v5, v5

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2520
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "bufferIndex":I
    .local v1, "bufferIndex":I
    aget v5, v2, v6

    int-to-char v5, v5

    aput-char v5, p4, v0

    move v0, v1

    .line 2529
    .end local v1    # "bufferIndex":I
    .restart local v0    # "bufferIndex":I
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v5

    if-eq v5, v4, :cond_2

    .line 2530
    invoke-interface {p1, p0, p2, p3, v2}, Lcom/ibm/icu/text/Normalizer$IsNextBoundary;->isNextBoundary(Lcom/ibm/icu/text/UCharacterIterator;II[I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2532
    aget v5, v2, v6

    if-nez v5, :cond_4

    :goto_2
    invoke-virtual {p0, v4}, Lcom/ibm/icu/text/UCharacterIterator;->moveIndex(I)I

    :cond_2
    move v4, v0

    .line 2553
    goto :goto_0

    .line 2522
    :cond_3
    invoke-virtual {p0, v4}, Lcom/ibm/icu/text/UCharacterIterator;->moveIndex(I)I

    goto :goto_1

    .line 2532
    :cond_4
    const/4 v4, -0x2

    goto :goto_2

    .line 2535
    :cond_5
    aget v5, v2, v6

    if-nez v5, :cond_6

    move v5, v6

    :goto_3
    add-int/2addr v5, v0

    array-length v9, p4

    if-gt v5, v9, :cond_7

    .line 2536
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "bufferIndex":I
    .restart local v1    # "bufferIndex":I
    aget v5, v2, v8

    int-to-char v5, v5

    aput-char v5, p4, v0

    .line 2537
    aget v5, v2, v6

    if-eqz v5, :cond_8

    .line 2538
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "bufferIndex":I
    .restart local v0    # "bufferIndex":I
    aget v5, v2, v6

    int-to-char v5, v5

    aput-char v5, p4, v1

    goto :goto_1

    :cond_6
    move v5, v7

    .line 2535
    goto :goto_3

    .line 2541
    :cond_7
    array-length v5, p4

    mul-int/lit8 v5, v5, 0x2

    new-array v3, v5, [C

    .line 2542
    .local v3, "newBuf":[C
    invoke-static {p4, v8, v3, v8, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2543
    move-object p4, v3

    .line 2544
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "bufferIndex":I
    .restart local v1    # "bufferIndex":I
    aget v5, v2, v8

    int-to-char v5, v5

    aput-char v5, p4, v0

    .line 2545
    aget v5, v2, v6

    if-eqz v5, :cond_8

    .line 2546
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "bufferIndex":I
    .restart local v0    # "bufferIndex":I
    aget v5, v2, v6

    int-to-char v5, v5

    aput-char v5, p4, v1

    goto :goto_1

    .end local v0    # "bufferIndex":I
    .end local v3    # "newBuf":[C
    .restart local v1    # "bufferIndex":I
    :cond_8
    move v0, v1

    .end local v1    # "bufferIndex":I
    .restart local v0    # "bufferIndex":I
    goto :goto_1
.end method

.method private static findPreviousIterationBoundary(Lcom/ibm/icu/text/UCharacterIterator;Lcom/ibm/icu/text/Normalizer$IsPrevBoundary;II[C[I)I
    .locals 10
    .param p0, "src"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p1, "obj"    # Lcom/ibm/icu/text/Normalizer$IsPrevBoundary;
    .param p2, "minC"    # I
    .param p3, "mask"    # I
    .param p4, "buffer"    # [C
    .param p5, "startIndex"    # [I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v9, 0x0

    .line 2283
    new-array v0, v5, [C

    .line 2287
    .local v0, "chars":[C
    array-length v3, p4

    aput v3, p5, v9

    .line 2288
    aput-char v9, v0, v9

    .line 2289
    :cond_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v3

    if-lez v3, :cond_3

    aget-char v3, v0, v9

    const/4 v6, -0x1

    if-eq v3, v6, :cond_3

    .line 2290
    invoke-interface {p1, p0, p2, p3, v0}, Lcom/ibm/icu/text/Normalizer$IsPrevBoundary;->isPrevBoundary(Lcom/ibm/icu/text/UCharacterIterator;II[C)Z

    move-result v1

    .line 2294
    .local v1, "isBoundary":Z
    aget v6, p5, v9

    aget-char v3, v0, v4

    if-nez v3, :cond_4

    move v3, v4

    :goto_0
    if-ge v6, v3, :cond_1

    .line 2297
    array-length v3, p4

    mul-int/lit8 v3, v3, 0x2

    new-array v2, v3, [C

    .line 2299
    .local v2, "newBuf":[C
    aget v3, p5, v9

    array-length v6, v2

    array-length v7, p4

    aget v8, p5, v9

    sub-int/2addr v7, v8

    sub-int/2addr v6, v7

    array-length v7, p4

    aget v8, p5, v9

    sub-int/2addr v7, v8

    invoke-static {p4, v3, v2, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2303
    aget v3, p5, v9

    array-length v6, v2

    array-length v7, p4

    sub-int/2addr v6, v7

    add-int/2addr v3, v6

    aput v3, p5, v9

    .line 2305
    move-object p4, v2

    .line 2310
    .end local v2    # "newBuf":[C
    :cond_1
    aget v3, p5, v9

    add-int/lit8 v3, v3, -0x1

    aput v3, p5, v9

    aget-char v6, v0, v9

    aput-char v6, p4, v3

    .line 2311
    aget-char v3, v0, v4

    if-eqz v3, :cond_2

    .line 2312
    aget v3, p5, v9

    add-int/lit8 v3, v3, -0x1

    aput v3, p5, v9

    aget-char v6, v0, v4

    aput-char v6, p4, v3

    .line 2316
    :cond_2
    if-eqz v1, :cond_0

    .line 2322
    .end local v1    # "isBoundary":Z
    :cond_3
    array-length v3, p4

    aget v4, p5, v9

    sub-int/2addr v3, v4

    return v3

    .restart local v1    # "isBoundary":Z
    :cond_4
    move v3, v5

    .line 2294
    goto :goto_0
.end method

.method private getCodePointAt(I)I
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 2665
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    aget-char v0, v0, p1

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2666
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    aget-char v0, v0, p1

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2667
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferLimit:I

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    add-int/lit8 v1, p1, 0x1

    aget-char v0, v0, v1

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2669
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    aget-char v0, v0, p1

    iget-object v1, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    add-int/lit8 v2, p1, 0x1

    aget-char v1, v1, v2

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v0

    .line 2683
    :goto_0
    return v0

    .line 2674
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    aget-char v0, v0, p1

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2675
    if-lez p1, :cond_1

    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    add-int/lit8 v1, p1, -0x1

    aget-char v0, v0, v1

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2676
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    add-int/lit8 v1, p1, -0x1

    aget-char v0, v0, v1

    iget-object v1, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    aget-char v1, v1, p1

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v0

    goto :goto_0

    .line 2683
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    aget-char v0, v0, p1

    goto :goto_0
.end method

.method public static getFC_NFKC_Closure(I[C)I
    .locals 1
    .param p0, "c"    # I
    .param p1, "dest"    # [C

    .prologue
    .line 1765
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getFC_NFKC_Closure(I[C)I

    move-result v0

    return v0
.end method

.method public static getFC_NFKC_Closure(I)Ljava/lang/String;
    .locals 4
    .param p0, "c"    # I

    .prologue
    .line 1774
    const/16 v2, 0xa

    new-array v0, v2, [C

    .line 1776
    .local v0, "dest":[C
    :goto_0
    invoke-static {p0, v0}, Lcom/ibm/icu/text/Normalizer;->getFC_NFKC_Closure(I[C)I

    move-result v1

    .line 1777
    .local v1, "length":I
    array-length v2, v0

    if-gt v1, v2, :cond_0

    .line 1778
    new-instance v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3, v1}, Ljava/lang/String;-><init>([CII)V

    return-object v2

    .line 1780
    :cond_0
    new-array v0, v1, [C

    .line 1782
    goto :goto_0
.end method

.method private static getNextNorm32(Lcom/ibm/icu/text/UCharacterIterator;II[I)J
    .locals 7
    .param p0, "src"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p1, "minC"    # I
    .param p2, "mask"    # I
    .param p3, "chars"    # [I

    .prologue
    const-wide/16 v2, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2438
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    move-result v4

    aput v4, p3, v5

    .line 2439
    aput v5, p3, v6

    .line 2441
    aget v4, p3, v5

    if-ge v4, p1, :cond_1

    move-wide v0, v2

    .line 2462
    :cond_0
    :goto_0
    return-wide v0

    .line 2445
    :cond_1
    aget v4, p3, v5

    int-to-char v4, v4

    invoke-static {v4}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide v0

    .line 2446
    .local v0, "norm32":J
    aget v4, p3, v5

    int-to-char v4, v4

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2447
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v4

    aput v4, p3, v6

    int-to-char v4, v4

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2449
    invoke-virtual {p0, v6}, Lcom/ibm/icu/text/UCharacterIterator;->moveIndex(I)I

    .line 2450
    int-to-long v4, p2

    and-long/2addr v4, v0

    cmp-long v4, v4, v2

    if-nez v4, :cond_2

    move-wide v0, v2

    .line 2452
    goto :goto_0

    .line 2455
    :cond_2
    aget v2, p3, v6

    int-to-char v2, v2

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32FromSurrogatePair(JC)J

    move-result-wide v0

    goto :goto_0

    :cond_3
    move-wide v0, v2

    .line 2459
    goto :goto_0
.end method

.method private static getPrevNorm32(Lcom/ibm/icu/text/UCharacterIterator;II[C)J
    .locals 9
    .param p0, "src"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p1, "minC"    # I
    .param p2, "mask"    # I
    .param p3, "chars"    # [C

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v8, 0x0

    .line 2197
    const/4 v0, 0x0

    .line 2199
    .local v0, "ch":I
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 2228
    :cond_0
    :goto_0
    return-wide v4

    .line 2202
    :cond_1
    int-to-char v1, v0

    aput-char v1, p3, v8

    .line 2203
    aput-char v8, p3, v6

    .line 2207
    aget-char v1, p3, v8

    if-lt v1, p1, :cond_0

    .line 2209
    aget-char v1, p3, v8

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2210
    aget-char v1, p3, v8

    invoke-static {v1}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide v4

    goto :goto_0

    .line 2211
    :cond_2
    aget-char v1, p3, v8

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v1

    if-nez v1, :cond_4

    .line 2213
    :cond_3
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v1

    int-to-char v1, v1

    aput-char v1, p3, v6

    goto :goto_0

    .line 2215
    :cond_4
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v1

    int-to-char v1, v1

    aput-char v1, p3, v6

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2216
    aget-char v1, p3, v6

    invoke-static {v1}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32(C)J

    move-result-wide v2

    .line 2217
    .local v2, "norm32":J
    int-to-long v6, p2

    and-long/2addr v6, v2

    cmp-long v1, v6, v4

    if-eqz v1, :cond_0

    .line 2223
    aget-char v1, p3, v8

    invoke-static {v2, v3, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->getNorm32FromSurrogatePair(JC)J

    move-result-wide v4

    goto :goto_0

    .line 2227
    .end local v2    # "norm32":J
    :cond_5
    invoke-virtual {p0, v6}, Lcom/ibm/icu/text/UCharacterIterator;->moveIndex(I)I

    goto :goto_0
.end method

.method static getUnicodeVersion()Lcom/ibm/icu/util/VersionInfo;
    .locals 1

    .prologue
    .line 2813
    invoke-static {}, Lcom/ibm/icu/impl/NormalizerImpl;->getUnicodeVersion()Lcom/ibm/icu/util/VersionInfo;

    move-result-object v0

    return-object v0
.end method

.method private static internalCompare([CII[CIII)I
    .locals 31
    .param p0, "s1"    # [C
    .param p1, "s1Start"    # I
    .param p2, "s1Limit"    # I
    .param p3, "s2"    # [C
    .param p4, "s2Start"    # I
    .param p5, "s2Limit"    # I
    .param p6, "options"    # I

    .prologue
    .line 2701
    const/16 v3, 0x12c

    new-array v12, v3, [C

    .line 2702
    .local v12, "fcd1":[C
    const/16 v3, 0x12c

    new-array v0, v3, [C

    move-object/from16 v22, v0

    .line 2707
    .local v22, "fcd2":[C
    if-eqz p0, :cond_0

    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    if-eqz p3, :cond_0

    if-ltz p4, :cond_0

    if-ltz p5, :cond_0

    move/from16 v0, p2

    move/from16 v1, p1

    if-lt v0, v1, :cond_0

    move/from16 v0, p5

    move/from16 v1, p4

    if-ge v0, v1, :cond_1

    .line 2712
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3

    .line 2715
    :cond_1
    shr-int/lit8 v3, p6, 0x14

    invoke-static {v3}, Lcom/ibm/icu/impl/NormalizerImpl;->getNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v7

    .line 2716
    .local v7, "nx":Lcom/ibm/icu/text/UnicodeSet;
    const/high16 v3, 0x80000

    or-int p6, p6, v3

    .line 2717
    const/16 v30, 0x0

    .line 2740
    .local v30, "result":I
    and-int/lit8 v3, p6, 0x1

    if-lez v3, :cond_4

    .line 2741
    sget-object v2, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 2742
    .local v2, "mode":Lcom/ibm/icu/text/Normalizer$Mode;
    const v3, -0x20001

    and-int p6, p6, v3

    .line 2746
    :goto_0
    const/high16 v3, 0x20000

    and-int v3, v3, p6

    if-nez v3, :cond_3

    .line 2752
    sget-object v8, Lcom/ibm/icu/text/Normalizer;->YES:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    const/4 v6, 0x1

    move-object/from16 v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    invoke-virtual/range {v2 .. v7}, Lcom/ibm/icu/text/Normalizer$Mode;->quickCheck([CIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v3

    if-ne v8, v3, :cond_5

    const/16 v28, 0x1

    .line 2753
    .local v28, "isFCD1":Z
    :goto_1
    sget-object v8, Lcom/ibm/icu/text/Normalizer;->YES:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    const/4 v6, 0x1

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    invoke-virtual/range {v2 .. v7}, Lcom/ibm/icu/text/Normalizer$Mode;->quickCheck([CIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v3

    if-ne v8, v3, :cond_6

    const/16 v29, 0x1

    .line 2763
    .local v29, "isFCD2":Z
    :goto_2
    if-nez v28, :cond_2

    .line 2764
    const/4 v10, 0x0

    move-object/from16 v0, p0

    array-length v11, v0

    const/4 v13, 0x0

    array-length v14, v12

    move-object v8, v2

    move-object/from16 v9, p0

    move-object v15, v7

    invoke-virtual/range {v8 .. v15}, Lcom/ibm/icu/text/Normalizer$Mode;->normalize([CII[CIILcom/ibm/icu/text/UnicodeSet;)I

    move-result v26

    .line 2768
    .local v26, "fcdLen1":I
    array-length v3, v12

    move/from16 v0, v26

    if-le v0, v3, :cond_7

    .line 2769
    move/from16 v0, v26

    new-array v0, v0, [C

    move-object/from16 v17, v0

    .line 2770
    .local v17, "dest":[C
    const/4 v15, 0x0

    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v16, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v19, v0

    move-object v13, v2

    move-object/from16 v14, p0

    move-object/from16 v20, v7

    invoke-virtual/range {v13 .. v20}, Lcom/ibm/icu/text/Normalizer$Mode;->normalize([CII[CIILcom/ibm/icu/text/UnicodeSet;)I

    move-result v26

    .line 2773
    move-object/from16 p0, v17

    .line 2777
    .end local v17    # "dest":[C
    :goto_3
    move/from16 p2, v26

    .line 2778
    const/16 p1, 0x0

    .line 2781
    .end local v26    # "fcdLen1":I
    :cond_2
    if-nez v29, :cond_3

    .line 2782
    const/16 v23, 0x0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v24, v0

    move-object/from16 v18, v2

    move-object/from16 v19, p3

    move/from16 v20, p4

    move/from16 v21, p5

    move-object/from16 v25, v7

    invoke-virtual/range {v18 .. v25}, Lcom/ibm/icu/text/Normalizer$Mode;->normalize([CII[CIILcom/ibm/icu/text/UnicodeSet;)I

    move-result v27

    .line 2786
    .local v27, "fcdLen2":I
    move-object/from16 v0, v22

    array-length v3, v0

    move/from16 v0, v27

    if-le v0, v3, :cond_8

    .line 2787
    move/from16 v0, v27

    new-array v0, v0, [C

    move-object/from16 v17, v0

    .line 2788
    .restart local v17    # "dest":[C
    const/16 v18, 0x0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v19, v0

    move-object v13, v2

    move-object/from16 v14, p3

    move/from16 v15, p4

    move/from16 v16, p5

    move-object/from16 v20, v7

    invoke-virtual/range {v13 .. v20}, Lcom/ibm/icu/text/Normalizer$Mode;->normalize([CII[CIILcom/ibm/icu/text/UnicodeSet;)I

    move-result v27

    .line 2791
    move-object/from16 p3, v17

    .line 2795
    .end local v17    # "dest":[C
    :goto_4
    move/from16 p5, v27

    .line 2796
    const/16 p4, 0x0

    .line 2801
    .end local v27    # "fcdLen2":I
    .end local v28    # "isFCD1":Z
    .end local v29    # "isFCD2":Z
    :cond_3
    invoke-static/range {p0 .. p6}, Lcom/ibm/icu/impl/NormalizerImpl;->cmpEquivFold([CII[CIII)I

    move-result v30

    .line 2803
    return v30

    .line 2744
    .end local v2    # "mode":Lcom/ibm/icu/text/Normalizer$Mode;
    :cond_4
    sget-object v2, Lcom/ibm/icu/text/Normalizer;->FCD:Lcom/ibm/icu/text/Normalizer$Mode;

    .restart local v2    # "mode":Lcom/ibm/icu/text/Normalizer$Mode;
    goto/16 :goto_0

    .line 2752
    :cond_5
    const/16 v28, 0x0

    goto/16 :goto_1

    .line 2753
    .restart local v28    # "isFCD1":Z
    :cond_6
    const/16 v29, 0x0

    goto/16 :goto_2

    .line 2775
    .restart local v26    # "fcdLen1":I
    .restart local v29    # "isFCD2":Z
    :cond_7
    move-object/from16 p0, v12

    goto :goto_3

    .line 2793
    .end local v26    # "fcdLen1":I
    .restart local v27    # "fcdLen2":I
    :cond_8
    move-object/from16 p3, v22

    goto :goto_4
.end method

.method public static isNFSkippable(ILcom/ibm/icu/text/Normalizer$Mode;)Z
    .locals 1
    .param p0, "c"    # I
    .param p1, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;

    .prologue
    .line 2693
    invoke-virtual {p1, p0}, Lcom/ibm/icu/text/Normalizer$Mode;->isNFSkippable(I)Z

    move-result v0

    return v0
.end method

.method public static isNormalized(ILcom/ibm/icu/text/Normalizer$Mode;I)Z
    .locals 1
    .param p0, "char32"    # I
    .param p1, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p2, "options"    # I

    .prologue
    .line 1350
    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/ibm/icu/text/Normalizer;->isNormalized(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Z

    move-result v0

    return v0
.end method

.method public static isNormalized(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Z
    .locals 6
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p2, "options"    # I

    .prologue
    const/4 v2, 0x0

    .line 1333
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {p2}, Lcom/ibm/icu/impl/NormalizerImpl;->getNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v5

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/text/Normalizer$Mode;->quickCheck([CIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v0

    sget-object v1, Lcom/ibm/icu/text/Normalizer;->YES:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-ne v0, v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method public static isNormalized([CIILcom/ibm/icu/text/Normalizer$Mode;I)Z
    .locals 6
    .param p0, "src"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p4, "options"    # I

    .prologue
    const/4 v4, 0x0

    .line 1319
    invoke-static {p4}, Lcom/ibm/icu/impl/NormalizerImpl;->getNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v5

    move-object v0, p3

    move-object v1, p0

    move v2, p1

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/text/Normalizer$Mode;->quickCheck([CIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v0

    sget-object v1, Lcom/ibm/icu/text/Normalizer;->YES:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-ne v0, v1, :cond_0

    const/4 v4, 0x1

    :cond_0
    return v4
.end method

.method private static makeFCD(Ljava/lang/String;I)Ljava/lang/String;
    .locals 8
    .param p0, "src"    # Ljava/lang/String;
    .param p1, "options"    # I

    .prologue
    const/4 v1, 0x0

    .line 1080
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 1081
    .local v2, "srcLen":I
    mul-int/lit8 v0, v2, 0x3

    new-array v3, v0, [C

    .line 1082
    .local v3, "dest":[C
    const/4 v7, 0x0

    .line 1083
    .local v7, "length":I
    invoke-static {p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v6

    .line 1085
    .local v6, "nx":Lcom/ibm/icu/text/UnicodeSet;
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v5, v3

    move v4, v1

    invoke-static/range {v0 .. v6}, Lcom/ibm/icu/impl/NormalizerImpl;->makeFCD([CII[CIILcom/ibm/icu/text/UnicodeSet;)I

    move-result v7

    .line 1087
    array-length v0, v3

    if-gt v7, v0, :cond_0

    .line 1088
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3, v1, v7}, Ljava/lang/String;-><init>([CII)V

    return-object v0

    .line 1090
    :cond_0
    new-array v3, v7, [C

    .line 1092
    goto :goto_0
.end method

.method private static next(Lcom/ibm/icu/text/UCharacterIterator;[CIILcom/ibm/icu/text/Normalizer$Mode;Z[ZI)I
    .locals 21
    .param p0, "src"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p1, "dest"    # [C
    .param p2, "destStart"    # I
    .param p3, "destLimit"    # I
    .param p4, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p5, "doNormalize"    # Z
    .param p6, "pNeededToNormalize"    # [Z
    .param p7, "options"    # I

    .prologue
    .line 2568
    sub-int v14, p3, p2

    .line 2569
    .local v14, "destCapacity":I
    const/4 v15, 0x0

    .line 2571
    .local v15, "destLength":I
    if-eqz p6, :cond_0

    .line 2572
    const/4 v4, 0x0

    const/4 v6, 0x0

    aput-boolean v6, p6, v4

    .line 2575
    :cond_0
    invoke-virtual/range {p4 .. p4}, Lcom/ibm/icu/text/Normalizer$Mode;->getMinC()I

    move-result v4

    int-to-char v0, v4

    move/from16 v19, v0

    .line 2576
    .local v19, "minC":C
    invoke-virtual/range {p4 .. p4}, Lcom/ibm/icu/text/Normalizer$Mode;->getMask()I

    move-result v18

    .line 2577
    .local v18, "mask":I
    invoke-virtual/range {p4 .. p4}, Lcom/ibm/icu/text/Normalizer$Mode;->getNextBoundary()Lcom/ibm/icu/text/Normalizer$IsNextBoundary;

    move-result-object v17

    .line 2579
    .local v17, "isNextBoundary":Lcom/ibm/icu/text/Normalizer$IsNextBoundary;
    if-nez v17, :cond_4

    .line 2580
    const/4 v15, 0x0

    .line 2581
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    move-result v12

    .line 2582
    .local v12, "c":I
    const/4 v4, -0x1

    if-eq v12, v4, :cond_2

    .line 2583
    const/4 v15, 0x1

    .line 2584
    int-to-char v4, v12

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2585
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    move-result v13

    .line 2586
    .local v13, "c2":I
    const/4 v4, -0x1

    if-eq v13, v4, :cond_1

    .line 2587
    int-to-char v4, v13

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2588
    const/4 v4, 0x2

    if-lt v14, v4, :cond_1

    .line 2589
    const/4 v4, 0x1

    int-to-char v6, v13

    aput-char v6, p1, v4

    .line 2590
    const/4 v15, 0x2

    .line 2599
    .end local v13    # "c2":I
    :cond_1
    :goto_0
    if-lez v14, :cond_2

    .line 2600
    const/4 v4, 0x0

    int-to-char v6, v12

    aput-char v6, p1, v4

    :cond_2
    move/from16 v16, v15

    .line 2633
    .end local v12    # "c":I
    .end local v15    # "destLength":I
    .local v16, "destLength":I
    :goto_1
    return v16

    .line 2594
    .end local v16    # "destLength":I
    .restart local v12    # "c":I
    .restart local v13    # "c2":I
    .restart local v15    # "destLength":I
    :cond_3
    const/4 v4, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/ibm/icu/text/UCharacterIterator;->moveIndex(I)I

    goto :goto_0

    .line 2606
    .end local v12    # "c":I
    .end local v13    # "c2":I
    :cond_4
    const/16 v4, 0x64

    new-array v5, v4, [C

    .line 2607
    .local v5, "buffer":[C
    const/4 v4, 0x1

    new-array v0, v4, [I

    move-object/from16 v20, v0

    .line 2609
    .local v20, "startIndex":[I
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v19

    move/from16 v3, v18

    invoke-static {v0, v1, v2, v3, v5}, Lcom/ibm/icu/text/Normalizer;->findNextIterationBoundary(Lcom/ibm/icu/text/UCharacterIterator;Lcom/ibm/icu/text/Normalizer$IsNextBoundary;II[C)I

    move-result v7

    .line 2611
    .local v7, "bufferLength":I
    if-lez v7, :cond_6

    .line 2612
    if-eqz p5, :cond_8

    .line 2613
    const/4 v4, 0x0

    aget v6, v20, v4

    move-object/from16 v4, p4

    move-object/from16 v8, p1

    move/from16 v9, p2

    move/from16 v10, p3

    move/from16 v11, p7

    invoke-virtual/range {v4 .. v11}, Lcom/ibm/icu/text/Normalizer$Mode;->normalize([CII[CIII)I

    move-result v15

    .line 2616
    if-eqz p6, :cond_6

    .line 2617
    const/4 v6, 0x0

    if-ne v15, v7, :cond_5

    const/4 v4, 0x0

    aget v4, v20, v4

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v5, v4, v0, v1, v15}, Lcom/ibm/icu/impl/Utility;->arrayRegionMatches([CI[CII)Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_5
    const/4 v4, 0x1

    :goto_2
    aput-boolean v4, p6, v6

    :cond_6
    :goto_3
    move/from16 v16, v15

    .line 2633
    .end local v15    # "destLength":I
    .restart local v16    # "destLength":I
    goto :goto_1

    .line 2617
    .end local v16    # "destLength":I
    .restart local v15    # "destLength":I
    :cond_7
    const/4 v4, 0x0

    goto :goto_2

    .line 2624
    :cond_8
    if-lez v14, :cond_6

    .line 2625
    const/4 v4, 0x0

    invoke-static {v7, v14}, Ljava/lang/Math;->min(II)I

    move-result v6

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v5, v4, v0, v1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_3
.end method

.method private nextNormalize()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 2642
    invoke-direct {p0}, Lcom/ibm/icu/text/Normalizer;->clearBuffer()V

    .line 2643
    iget v0, p0, Lcom/ibm/icu/text/Normalizer;->nextIndex:I

    iput v0, p0, Lcom/ibm/icu/text/Normalizer;->currentIndex:I

    .line 2644
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    iget v1, p0, Lcom/ibm/icu/text/Normalizer;->nextIndex:I

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 2646
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    iget v2, p0, Lcom/ibm/icu/text/Normalizer;->bufferStart:I

    iget-object v3, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    array-length v3, v3

    iget-object v4, p0, Lcom/ibm/icu/text/Normalizer;->mode:Lcom/ibm/icu/text/Normalizer$Mode;

    const/4 v6, 0x0

    iget v7, p0, Lcom/ibm/icu/text/Normalizer;->options:I

    invoke-static/range {v0 .. v7}, Lcom/ibm/icu/text/Normalizer;->next(Lcom/ibm/icu/text/UCharacterIterator;[CIILcom/ibm/icu/text/Normalizer$Mode;Z[ZI)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/Normalizer;->bufferLimit:I

    .line 2648
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/Normalizer;->nextIndex:I

    .line 2649
    iget v0, p0, Lcom/ibm/icu/text/Normalizer;->bufferLimit:I

    if-lez v0, :cond_0

    :goto_0
    return v5

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static normalize([CII[CIILcom/ibm/icu/text/Normalizer$Mode;I)I
    .locals 9
    .param p0, "src"    # [C
    .param p1, "srcStart"    # I
    .param p2, "srcLimit"    # I
    .param p3, "dest"    # [C
    .param p4, "destStart"    # I
    .param p5, "destLimit"    # I
    .param p6, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p7, "options"    # I

    .prologue
    .line 1177
    move-object v0, p6

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/ibm/icu/text/Normalizer$Mode;->normalize([CII[CIII)I

    move-result v8

    .line 1179
    .local v8, "length":I
    sub-int v0, p5, p4

    if-gt v8, v0, :cond_0

    .line 1180
    return v8

    .line 1182
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static normalize([C[CLcom/ibm/icu/text/Normalizer$Mode;I)I
    .locals 9
    .param p0, "source"    # [C
    .param p1, "target"    # [C
    .param p2, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p3, "options"    # I

    .prologue
    const/4 v1, 0x0

    .line 1146
    array-length v2, p0

    array-length v5, p1

    move-object v0, p0

    move-object v3, p1

    move v4, v1

    move-object v6, p2

    move v7, p3

    invoke-static/range {v0 .. v7}, Lcom/ibm/icu/text/Normalizer;->normalize([CII[CIILcom/ibm/icu/text/Normalizer$Mode;I)I

    move-result v8

    .line 1147
    .local v8, "length":I
    array-length v0, p1

    if-gt v8, v0, :cond_0

    .line 1148
    return v8

    .line 1150
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static normalize(ILcom/ibm/icu/text/Normalizer$Mode;)Ljava/lang/String;
    .locals 2
    .param p0, "char32"    # I
    .param p1, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;

    .prologue
    .line 1213
    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/ibm/icu/text/Normalizer;->normalize(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static normalize(ILcom/ibm/icu/text/Normalizer$Mode;I)Ljava/lang/String;
    .locals 1
    .param p0, "char32"    # I
    .param p1, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p2, "options"    # I

    .prologue
    .line 1199
    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/ibm/icu/text/Normalizer;->normalize(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static normalize(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;)Ljava/lang/String;
    .locals 1
    .param p0, "src"    # Ljava/lang/String;
    .param p1, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;

    .prologue
    .line 1127
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/ibm/icu/text/Normalizer;->normalize(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static normalize(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Ljava/lang/String;
    .locals 1
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p2, "options"    # I

    .prologue
    .line 1111
    invoke-virtual {p1, p0, p2}, Lcom/ibm/icu/text/Normalizer$Mode;->normalize(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static previous(Lcom/ibm/icu/text/UCharacterIterator;[CIILcom/ibm/icu/text/Normalizer$Mode;Z[ZI)I
    .locals 23
    .param p0, "src"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p1, "dest"    # [C
    .param p2, "destStart"    # I
    .param p3, "destLimit"    # I
    .param p4, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p5, "doNormalize"    # Z
    .param p6, "pNeededToNormalize"    # [Z
    .param p7, "options"    # I

    .prologue
    .line 2339
    sub-int v20, p3, p2

    .line 2340
    .local v20, "destCapacity":I
    const/16 v21, 0x0

    .line 2343
    .local v21, "destLength":I
    if-eqz p6, :cond_0

    .line 2344
    const/4 v3, 0x0

    const/4 v9, 0x0

    aput-boolean v9, p6, v3

    .line 2346
    :cond_0
    invoke-virtual/range {p4 .. p4}, Lcom/ibm/icu/text/Normalizer$Mode;->getMinC()I

    move-result v3

    int-to-char v5, v3

    .line 2347
    .local v5, "minC":C
    invoke-virtual/range {p4 .. p4}, Lcom/ibm/icu/text/Normalizer$Mode;->getMask()I

    move-result v6

    .line 2348
    .local v6, "mask":I
    invoke-virtual/range {p4 .. p4}, Lcom/ibm/icu/text/Normalizer$Mode;->getPrevBoundary()Lcom/ibm/icu/text/Normalizer$IsPrevBoundary;

    move-result-object v4

    .line 2350
    .local v4, "isPreviousBoundary":Lcom/ibm/icu/text/Normalizer$IsPrevBoundary;
    if-nez v4, :cond_5

    .line 2351
    const/16 v21, 0x0

    .line 2352
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v18

    .local v18, "c":I
    if-ltz v18, :cond_3

    .line 2353
    const/16 v21, 0x1

    .line 2354
    move/from16 v0, v18

    int-to-char v3, v0

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2355
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v19

    .line 2356
    .local v19, "c2":I
    const/4 v3, -0x1

    move/from16 v0, v19

    if-eq v0, v3, :cond_2

    .line 2357
    move/from16 v0, v19

    int-to-char v3, v0

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2358
    const/4 v3, 0x2

    move/from16 v0, v20

    if-lt v0, v3, :cond_1

    .line 2359
    const/4 v3, 0x1

    move/from16 v0, v18

    int-to-char v9, v0

    aput-char v9, p1, v3

    .line 2360
    const/16 v21, 0x2

    .line 2363
    :cond_1
    move/from16 v18, v19

    .line 2370
    .end local v19    # "c2":I
    :cond_2
    :goto_0
    if-lez v20, :cond_3

    .line 2371
    const/4 v3, 0x0

    move/from16 v0, v18

    int-to-char v9, v0

    aput-char v9, p1, v3

    :cond_3
    move/from16 v22, v21

    .line 2409
    .end local v18    # "c":I
    .end local v21    # "destLength":I
    .local v22, "destLength":I
    :goto_1
    return v22

    .line 2365
    .end local v22    # "destLength":I
    .restart local v18    # "c":I
    .restart local v19    # "c2":I
    .restart local v21    # "destLength":I
    :cond_4
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/ibm/icu/text/UCharacterIterator;->moveIndex(I)I

    goto :goto_0

    .line 2377
    .end local v18    # "c":I
    .end local v19    # "c2":I
    :cond_5
    const/16 v3, 0x64

    new-array v7, v3, [C

    .line 2378
    .local v7, "buffer":[C
    const/4 v3, 0x1

    new-array v8, v3, [I

    .local v8, "startIndex":[I
    move-object/from16 v3, p0

    .line 2379
    invoke-static/range {v3 .. v8}, Lcom/ibm/icu/text/Normalizer;->findPreviousIterationBoundary(Lcom/ibm/icu/text/UCharacterIterator;Lcom/ibm/icu/text/Normalizer$IsPrevBoundary;II[C[I)I

    move-result v17

    .line 2383
    .local v17, "bufferLength":I
    if-lez v17, :cond_7

    .line 2384
    if-eqz p5, :cond_9

    .line 2385
    const/4 v3, 0x0

    aget v10, v8, v3

    const/4 v3, 0x0

    aget v3, v8, v3

    add-int v11, v3, v17

    move-object v9, v7

    move-object/from16 v12, p1

    move/from16 v13, p2

    move/from16 v14, p3

    move-object/from16 v15, p4

    move/from16 v16, p7

    invoke-static/range {v9 .. v16}, Lcom/ibm/icu/text/Normalizer;->normalize([CII[CIILcom/ibm/icu/text/Normalizer$Mode;I)I

    move-result v21

    .line 2390
    if-eqz p6, :cond_7

    .line 2391
    const/4 v9, 0x0

    move/from16 v0, v21

    move/from16 v1, v17

    if-ne v0, v1, :cond_6

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v7, v3, v0, v1, v2}, Lcom/ibm/icu/impl/Utility;->arrayRegionMatches([CI[CII)Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_6
    const/4 v3, 0x1

    :goto_2
    aput-boolean v3, p6, v9

    .end local v17    # "bufferLength":I
    :cond_7
    :goto_3
    move/from16 v22, v21

    .line 2409
    .end local v21    # "destLength":I
    .restart local v22    # "destLength":I
    goto :goto_1

    .line 2391
    .end local v22    # "destLength":I
    .restart local v17    # "bufferLength":I
    .restart local v21    # "destLength":I
    :cond_8
    const/4 v3, 0x0

    goto :goto_2

    .line 2399
    :cond_9
    if-lez v20, :cond_7

    .line 2400
    const/4 v3, 0x0

    aget v3, v8, v3

    const/4 v9, 0x0

    move/from16 v0, v17

    move/from16 v1, v20

    if-ge v0, v1, :cond_a

    .end local v17    # "bufferLength":I
    :goto_4
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v7, v3, v0, v9, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_3

    .restart local v17    # "bufferLength":I
    :cond_a
    move/from16 v17, v20

    goto :goto_4
.end method

.method private previousNormalize()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 2654
    invoke-direct {p0}, Lcom/ibm/icu/text/Normalizer;->clearBuffer()V

    .line 2655
    iget v0, p0, Lcom/ibm/icu/text/Normalizer;->currentIndex:I

    iput v0, p0, Lcom/ibm/icu/text/Normalizer;->nextIndex:I

    .line 2656
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    iget v1, p0, Lcom/ibm/icu/text/Normalizer;->currentIndex:I

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 2657
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    iget v2, p0, Lcom/ibm/icu/text/Normalizer;->bufferStart:I

    iget-object v3, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    array-length v3, v3

    iget-object v4, p0, Lcom/ibm/icu/text/Normalizer;->mode:Lcom/ibm/icu/text/Normalizer$Mode;

    const/4 v6, 0x0

    iget v7, p0, Lcom/ibm/icu/text/Normalizer;->options:I

    invoke-static/range {v0 .. v7}, Lcom/ibm/icu/text/Normalizer;->previous(Lcom/ibm/icu/text/UCharacterIterator;[CIILcom/ibm/icu/text/Normalizer$Mode;Z[ZI)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/Normalizer;->bufferLimit:I

    .line 2659
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/Normalizer;->currentIndex:I

    .line 2660
    iget v0, p0, Lcom/ibm/icu/text/Normalizer;->bufferLimit:I

    iput v0, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    .line 2661
    iget v0, p0, Lcom/ibm/icu/text/Normalizer;->bufferLimit:I

    if-lez v0, :cond_0

    :goto_0
    return v5

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;
    .locals 6
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;

    .prologue
    .line 1227
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/text/Normalizer$Mode;->quickCheck([CIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v0

    return-object v0
.end method

.method public static quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;
    .locals 6
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p2, "options"    # I

    .prologue
    .line 1243
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    invoke-static {p2}, Lcom/ibm/icu/impl/NormalizerImpl;->getNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/text/Normalizer$Mode;->quickCheck([CIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v0

    return-object v0
.end method

.method public static quickCheck([CIILcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;
    .locals 6
    .param p0, "source"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p4, "options"    # I

    .prologue
    .line 1289
    const/4 v4, 0x1

    invoke-static {p4}, Lcom/ibm/icu/impl/NormalizerImpl;->getNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v5

    move-object v0, p3

    move-object v1, p0

    move v2, p1

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/text/Normalizer$Mode;->quickCheck([CIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v0

    return-object v0
.end method

.method public static quickCheck([CLcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;
    .locals 6
    .param p0, "source"    # [C
    .param p1, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;
    .param p2, "options"    # I

    .prologue
    .line 1260
    const/4 v2, 0x0

    array-length v3, p0

    const/4 v4, 0x1

    invoke-static {p2}, Lcom/ibm/icu/impl/NormalizerImpl;->getNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v5

    move-object v0, p1

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/text/Normalizer$Mode;->quickCheck([CIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 825
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/Normalizer;

    .line 826
    .local v0, "copy":Lcom/ibm/icu/text/Normalizer;
    iget-object v2, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v2}, Lcom/ibm/icu/text/UCharacterIterator;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/UCharacterIterator;

    iput-object v2, v0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    .line 828
    iget-object v2, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    if-eqz v2, :cond_0

    .line 829
    iget-object v2, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    array-length v2, v2

    new-array v2, v2, [C

    iput-object v2, v0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    .line 830
    iget-object v2, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    const/4 v3, 0x0

    iget-object v4, v0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/ibm/icu/text/Normalizer;->buffer:[C

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 832
    :cond_0
    return-object v0

    .line 834
    .end local v0    # "copy":Lcom/ibm/icu/text/Normalizer;
    :catch_0
    move-exception v1

    .line 835
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/CloneNotSupportedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public current()I
    .locals 2

    .prologue
    .line 1794
    iget v0, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    iget v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferLimit:I

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Lcom/ibm/icu/text/Normalizer;->nextNormalize()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1795
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/Normalizer;->getCodePointAt(I)I

    move-result v0

    .line 1797
    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public endIndex()I
    .locals 1

    .prologue
    .line 1980
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->getLength()I

    move-result v0

    return v0
.end method

.method public first()I
    .locals 1

    .prologue
    .line 1920
    invoke-virtual {p0}, Lcom/ibm/icu/text/Normalizer;->reset()V

    .line 1921
    invoke-virtual {p0}, Lcom/ibm/icu/text/Normalizer;->next()I

    move-result v0

    return v0
.end method

.method public getBeginIndex()I
    .locals 1

    .prologue
    .line 1899
    const/4 v0, 0x0

    return v0
.end method

.method public getEndIndex()I
    .locals 1

    .prologue
    .line 1911
    invoke-virtual {p0}, Lcom/ibm/icu/text/Normalizer;->endIndex()I

    move-result v0

    return v0
.end method

.method public getIndex()I
    .locals 2

    .prologue
    .line 1954
    iget v0, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    iget v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferLimit:I

    if-ge v0, v1, :cond_0

    .line 1955
    iget v0, p0, Lcom/ibm/icu/text/Normalizer;->currentIndex:I

    .line 1957
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/Normalizer;->nextIndex:I

    goto :goto_0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 2083
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->getLength()I

    move-result v0

    return v0
.end method

.method public getMode()Lcom/ibm/icu/text/Normalizer$Mode;
    .locals 1

    .prologue
    .line 2022
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->mode:Lcom/ibm/icu/text/Normalizer$Mode;

    return-object v0
.end method

.method public getOption(I)I
    .locals 1
    .param p1, "option"    # I

    .prologue
    .line 2057
    iget v0, p0, Lcom/ibm/icu/text/Normalizer;->options:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    .line 2058
    const/4 v0, 0x1

    .line 2060
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getText([C)I
    .locals 1
    .param p1, "fillIn"    # [C

    .prologue
    .line 2074
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/UCharacterIterator;->getText([C)I

    move-result v0

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2092
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public last()I
    .locals 1

    .prologue
    .line 1932
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->setToLimit()V

    .line 1933
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/Normalizer;->nextIndex:I

    iput v0, p0, Lcom/ibm/icu/text/Normalizer;->currentIndex:I

    .line 1934
    invoke-direct {p0}, Lcom/ibm/icu/text/Normalizer;->clearBuffer()V

    .line 1935
    invoke-virtual {p0}, Lcom/ibm/icu/text/Normalizer;->previous()I

    move-result v0

    return v0
.end method

.method public next()I
    .locals 3

    .prologue
    .line 1809
    iget v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    iget v2, p0, Lcom/ibm/icu/text/Normalizer;->bufferLimit:I

    if-lt v1, v2, :cond_0

    invoke-direct {p0}, Lcom/ibm/icu/text/Normalizer;->nextNormalize()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1810
    :cond_0
    iget v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/Normalizer;->getCodePointAt(I)I

    move-result v0

    .line 1811
    .local v0, "c":I
    iget v2, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    const v1, 0xffff

    if-le v0, v1, :cond_1

    const/4 v1, 0x2

    :goto_0
    add-int/2addr v1, v2

    iput v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    .line 1814
    .end local v0    # "c":I
    :goto_1
    return v0

    .line 1811
    .restart local v0    # "c":I
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 1814
    .end local v0    # "c":I
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public previous()I
    .locals 3

    .prologue
    .line 1827
    iget v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    if-gtz v1, :cond_0

    invoke-direct {p0}, Lcom/ibm/icu/text/Normalizer;->previousNormalize()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1828
    :cond_0
    iget v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/Normalizer;->getCodePointAt(I)I

    move-result v0

    .line 1829
    .local v0, "c":I
    iget v2, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    const v1, 0xffff

    if-le v0, v1, :cond_1

    const/4 v1, 0x2

    :goto_0
    sub-int v1, v2, v1

    iput v1, p0, Lcom/ibm/icu/text/Normalizer;->bufferPos:I

    .line 1832
    .end local v0    # "c":I
    :goto_1
    return v0

    .line 1829
    .restart local v0    # "c":I
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 1832
    .end local v0    # "c":I
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1842
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 1843
    iput v1, p0, Lcom/ibm/icu/text/Normalizer;->nextIndex:I

    iput v1, p0, Lcom/ibm/icu/text/Normalizer;->currentIndex:I

    .line 1844
    invoke-direct {p0}, Lcom/ibm/icu/text/Normalizer;->clearBuffer()V

    .line 1845
    return-void
.end method

.method public setIndex(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1886
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/Normalizer;->setIndexOnly(I)V

    .line 1887
    invoke-virtual {p0}, Lcom/ibm/icu/text/Normalizer;->current()I

    move-result v0

    return v0
.end method

.method public setIndexOnly(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1857
    iget-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 1858
    iput p1, p0, Lcom/ibm/icu/text/Normalizer;->nextIndex:I

    iput p1, p0, Lcom/ibm/icu/text/Normalizer;->currentIndex:I

    .line 1859
    invoke-direct {p0}, Lcom/ibm/icu/text/Normalizer;->clearBuffer()V

    .line 1860
    return-void
.end method

.method public setMode(Lcom/ibm/icu/text/Normalizer$Mode;)V
    .locals 0
    .param p1, "newMode"    # Lcom/ibm/icu/text/Normalizer$Mode;

    .prologue
    .line 2013
    iput-object p1, p0, Lcom/ibm/icu/text/Normalizer;->mode:Lcom/ibm/icu/text/Normalizer$Mode;

    .line 2014
    return-void
.end method

.method public setOption(IZ)V
    .locals 2
    .param p1, "option"    # I
    .param p2, "value"    # Z

    .prologue
    .line 2043
    if-eqz p2, :cond_0

    .line 2044
    iget v0, p0, Lcom/ibm/icu/text/Normalizer;->options:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/ibm/icu/text/Normalizer;->options:I

    .line 2048
    :goto_0
    return-void

    .line 2046
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/Normalizer;->options:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/ibm/icu/text/Normalizer;->options:I

    goto :goto_0
.end method

.method public setText(Lcom/ibm/icu/text/UCharacterIterator;)V
    .locals 4
    .param p1, "newText"    # Lcom/ibm/icu/text/UCharacterIterator;

    .prologue
    .line 2167
    :try_start_0
    invoke-virtual {p1}, Lcom/ibm/icu/text/UCharacterIterator;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/UCharacterIterator;

    .line 2168
    .local v1, "newIter":Lcom/ibm/icu/text/UCharacterIterator;
    if-nez v1, :cond_0

    .line 2169
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "Could not create a new UCharacterIterator"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2173
    .end local v1    # "newIter":Lcom/ibm/icu/text/UCharacterIterator;
    :catch_0
    move-exception v0

    .line 2174
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "Could not clone the UCharacterIterator"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2171
    .end local v0    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v1    # "newIter":Lcom/ibm/icu/text/UCharacterIterator;
    :cond_0
    :try_start_1
    iput-object v1, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    .line 2172
    invoke-virtual {p0}, Lcom/ibm/icu/text/Normalizer;->reset()V
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2176
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 3
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    .line 2135
    invoke-static {p1}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v0

    .line 2136
    .local v0, "newIter":Lcom/ibm/icu/text/UCharacterIterator;
    if-nez v0, :cond_0

    .line 2137
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Could not create a new UCharacterIterator"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2139
    :cond_0
    iput-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    .line 2140
    invoke-virtual {p0}, Lcom/ibm/icu/text/Normalizer;->reset()V

    .line 2141
    return-void
.end method

.method public setText(Ljava/lang/StringBuffer;)V
    .locals 3
    .param p1, "newText"    # Ljava/lang/StringBuffer;

    .prologue
    .line 2103
    invoke-static {p1}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance(Ljava/lang/StringBuffer;)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v0

    .line 2104
    .local v0, "newIter":Lcom/ibm/icu/text/UCharacterIterator;
    if-nez v0, :cond_0

    .line 2105
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Could not create a new UCharacterIterator"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2107
    :cond_0
    iput-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    .line 2108
    invoke-virtual {p0}, Lcom/ibm/icu/text/Normalizer;->reset()V

    .line 2109
    return-void
.end method

.method public setText(Ljava/text/CharacterIterator;)V
    .locals 3
    .param p1, "newText"    # Ljava/text/CharacterIterator;

    .prologue
    .line 2151
    invoke-static {p1}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance(Ljava/text/CharacterIterator;)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v0

    .line 2152
    .local v0, "newIter":Lcom/ibm/icu/text/UCharacterIterator;
    if-nez v0, :cond_0

    .line 2153
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Could not create a new UCharacterIterator"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2155
    :cond_0
    iput-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    .line 2156
    invoke-virtual {p0}, Lcom/ibm/icu/text/Normalizer;->reset()V

    .line 2157
    return-void
.end method

.method public setText([C)V
    .locals 3
    .param p1, "newText"    # [C

    .prologue
    .line 2119
    invoke-static {p1}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance([C)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v0

    .line 2120
    .local v0, "newIter":Lcom/ibm/icu/text/UCharacterIterator;
    if-nez v0, :cond_0

    .line 2121
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "Could not create a new UCharacterIterator"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2123
    :cond_0
    iput-object v0, p0, Lcom/ibm/icu/text/Normalizer;->text:Lcom/ibm/icu/text/UCharacterIterator;

    .line 2124
    invoke-virtual {p0}, Lcom/ibm/icu/text/Normalizer;->reset()V

    .line 2125
    return-void
.end method

.method public startIndex()I
    .locals 1

    .prologue
    .line 1969
    const/4 v0, 0x0

    return v0
.end method
