.class public Lcom/ibm/icu/text/DateTimePatternGenerator;
.super Ljava/lang/Object;
.source "DateTimePatternGenerator.java"

# interfaces
.implements Lcom/ibm/icu/util/Freezable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/DateTimePatternGenerator$1;,
        Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;,
        Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;,
        Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;,
        Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;,
        Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;
    }
.end annotation


# static fields
.field private static CANONICAL_ITEMS:[Ljava/lang/String; = null

.field private static CANONICAL_SET:Ljava/util/Set; = null

.field private static CLDR_FIELD_APPEND:[Ljava/lang/String; = null

.field private static CLDR_FIELD_NAME:[Ljava/lang/String; = null

.field private static final DATE_MASK:I = 0x3ff

.field public static final DAY:I = 0x7

.field public static final DAYPERIOD:I = 0xa

.field public static final DAY_OF_WEEK_IN_MONTH:I = 0x9

.field public static final DAY_OF_YEAR:I = 0x8

.field private static final DELTA:I = 0x10

.field private static DTPNG_CACHE:Lcom/ibm/icu/impl/ICUCache; = null

.field public static final ERA:I = 0x0

.field private static final EXTRA_FIELD:I = 0x10000

.field private static FIELD_NAME:[Ljava/lang/String; = null

.field private static final FRACTIONAL_MASK:I = 0x4000

.field public static final FRACTIONAL_SECOND:I = 0xe

.field public static final HOUR:I = 0xb

.field private static final LONG:I = -0x103

.field public static final MINUTE:I = 0xc

.field private static final MISSING_FIELD:I = 0x1000

.field public static final MONTH:I = 0x3

.field private static final NARROW:I = -0x101

.field private static final NONE:I = 0x0

.field private static final NUMERIC:I = 0x100

.field public static final QUARTER:I = 0x2

.field public static final SECOND:I = 0xd

.field private static final SECOND_AND_FRACTIONAL_MASK:I = 0x6000

.field private static final SHORT:I = -0x102

.field private static final TIME_MASK:I = 0xfc00

.field public static final TYPE_LIMIT:I = 0x10

.field public static final WEEKDAY:I = 0x6

.field public static final WEEK_OF_MONTH:I = 0x5

.field public static final WEEK_OF_YEAR:I = 0x4

.field public static final YEAR:I = 0x1

.field public static final ZONE:I = 0xf

.field private static types:[[I


# instance fields
.field private transient _distanceInfo:Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

.field private appendItemFormats:[Ljava/lang/String;

.field private appendItemNames:[Ljava/lang/String;

.field private basePattern_pattern:Ljava/util/TreeMap;

.field private transient chineseMonthHack:Z

.field private cldrAvailableFormatKeys:Ljava/util/Set;

.field private transient current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

.field private dateTimeFormat:Ljava/lang/String;

.field private decimal:Ljava/lang/String;

.field private transient fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

.field private transient frozen:Z

.field private skeleton2pattern:Ljava/util/TreeMap;

.field private transient skipMatcher:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x5

    const/4 v3, 0x4

    .line 1303
    new-instance v0, Lcom/ibm/icu/impl/SimpleCache;

    invoke-direct {v0}, Lcom/ibm/icu/impl/SimpleCache;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/DateTimePatternGenerator;->DTPNG_CACHE:Lcom/ibm/icu/impl/ICUCache;

    .line 1494
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Era"

    aput-object v1, v0, v5

    const-string/jumbo v1, "Year"

    aput-object v1, v0, v6

    const-string/jumbo v1, "Quarter"

    aput-object v1, v0, v7

    const/4 v1, 0x3

    const-string/jumbo v2, "Month"

    aput-object v2, v0, v1

    const-string/jumbo v1, "Week"

    aput-object v1, v0, v3

    const-string/jumbo v1, "*"

    aput-object v1, v0, v4

    const/4 v1, 0x6

    const-string/jumbo v2, "Day-Of-Week"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "Day"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "*"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "*"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "*"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "Hour"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "Minute"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "Second"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "*"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "Timezone"

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/DateTimePatternGenerator;->CLDR_FIELD_APPEND:[Ljava/lang/String;

    .line 1500
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "era"

    aput-object v1, v0, v5

    const-string/jumbo v1, "year"

    aput-object v1, v0, v6

    const-string/jumbo v1, "*"

    aput-object v1, v0, v7

    const/4 v1, 0x3

    const-string/jumbo v2, "month"

    aput-object v2, v0, v1

    const-string/jumbo v1, "week"

    aput-object v1, v0, v3

    const-string/jumbo v1, "*"

    aput-object v1, v0, v4

    const/4 v1, 0x6

    const-string/jumbo v2, "weekday"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "day"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "*"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "*"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "dayperiod"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "hour"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "minute"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "second"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "*"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "zone"

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/DateTimePatternGenerator;->CLDR_FIELD_NAME:[Ljava/lang/String;

    .line 1506
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Era"

    aput-object v1, v0, v5

    const-string/jumbo v1, "Year"

    aput-object v1, v0, v6

    const-string/jumbo v1, "Quarter"

    aput-object v1, v0, v7

    const/4 v1, 0x3

    const-string/jumbo v2, "Month"

    aput-object v2, v0, v1

    const-string/jumbo v1, "Week_in_Year"

    aput-object v1, v0, v3

    const-string/jumbo v1, "Week_in_Month"

    aput-object v1, v0, v4

    const/4 v1, 0x6

    const-string/jumbo v2, "Weekday"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "Day"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "Day_Of_Year"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "Day_of_Week_in_Month"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "Dayperiod"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "Hour"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "Minute"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "Second"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "Fractional_Second"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "Zone"

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/DateTimePatternGenerator;->FIELD_NAME:[Ljava/lang/String;

    .line 1513
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "G"

    aput-object v1, v0, v5

    const-string/jumbo v1, "y"

    aput-object v1, v0, v6

    const-string/jumbo v1, "Q"

    aput-object v1, v0, v7

    const/4 v1, 0x3

    const-string/jumbo v2, "M"

    aput-object v2, v0, v1

    const-string/jumbo v1, "w"

    aput-object v1, v0, v3

    const-string/jumbo v1, "W"

    aput-object v1, v0, v4

    const/4 v1, 0x6

    const-string/jumbo v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "d"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "D"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "F"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "H"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "m"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "s"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "S"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "v"

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/DateTimePatternGenerator;->CANONICAL_ITEMS:[Ljava/lang/String;

    .line 1519
    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lcom/ibm/icu/text/DateTimePatternGenerator;->CANONICAL_ITEMS:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/ibm/icu/text/DateTimePatternGenerator;->CANONICAL_SET:Ljava/util/Set;

    .line 1578
    const/16 v0, 0x36

    new-array v0, v0, [[I

    new-array v1, v4, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v5

    new-array v1, v3, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v6

    new-array v1, v4, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v7

    const/4 v1, 0x3

    new-array v2, v4, [I

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    new-array v1, v4, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v3

    new-array v1, v4, [I

    fill-array-data v1, :array_5

    aput-object v1, v0, v4

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v4, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v3, [I

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v3, [I

    fill-array-data v2, :array_a

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v4, [I

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v3, [I

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v3, [I

    fill-array-data v2, :array_d

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v3, [I

    fill-array-data v2, :array_e

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v4, [I

    fill-array-data v2, :array_f

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v3, [I

    fill-array-data v2, :array_10

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v3, [I

    fill-array-data v2, :array_11

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v3, [I

    fill-array-data v2, :array_12

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v4, [I

    fill-array-data v2, :array_13

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v3, [I

    fill-array-data v2, :array_14

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v4, [I

    fill-array-data v2, :array_15

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v3, [I

    fill-array-data v2, :array_16

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v3, [I

    fill-array-data v2, :array_17

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v3, [I

    fill-array-data v2, :array_18

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v2, v4, [I

    fill-array-data v2, :array_19

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v3, [I

    fill-array-data v2, :array_1a

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-array v2, v3, [I

    fill-array-data v2, :array_1b

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v4, [I

    fill-array-data v2, :array_1c

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-array v2, v3, [I

    fill-array-data v2, :array_1d

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-array v2, v3, [I

    fill-array-data v2, :array_1e

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    new-array v2, v3, [I

    fill-array-data v2, :array_1f

    aput-object v2, v0, v1

    const/16 v1, 0x20

    new-array v2, v4, [I

    fill-array-data v2, :array_20

    aput-object v2, v0, v1

    const/16 v1, 0x21

    new-array v2, v4, [I

    fill-array-data v2, :array_21

    aput-object v2, v0, v1

    const/16 v1, 0x22

    new-array v2, v3, [I

    fill-array-data v2, :array_22

    aput-object v2, v0, v1

    const/16 v1, 0x23

    new-array v2, v4, [I

    fill-array-data v2, :array_23

    aput-object v2, v0, v1

    const/16 v1, 0x24

    new-array v2, v3, [I

    fill-array-data v2, :array_24

    aput-object v2, v0, v1

    const/16 v1, 0x25

    new-array v2, v4, [I

    fill-array-data v2, :array_25

    aput-object v2, v0, v1

    const/16 v1, 0x26

    new-array v2, v4, [I

    fill-array-data v2, :array_26

    aput-object v2, v0, v1

    const/16 v1, 0x27

    new-array v2, v4, [I

    fill-array-data v2, :array_27

    aput-object v2, v0, v1

    const/16 v1, 0x28

    new-array v2, v4, [I

    fill-array-data v2, :array_28

    aput-object v2, v0, v1

    const/16 v1, 0x29

    new-array v2, v4, [I

    fill-array-data v2, :array_29

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    new-array v2, v4, [I

    fill-array-data v2, :array_2a

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    new-array v2, v4, [I

    fill-array-data v2, :array_2b

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    new-array v2, v4, [I

    fill-array-data v2, :array_2c

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    new-array v2, v4, [I

    fill-array-data v2, :array_2d

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    new-array v2, v3, [I

    fill-array-data v2, :array_2e

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    new-array v2, v3, [I

    fill-array-data v2, :array_2f

    aput-object v2, v0, v1

    const/16 v1, 0x30

    new-array v2, v4, [I

    fill-array-data v2, :array_30

    aput-object v2, v0, v1

    const/16 v1, 0x31

    new-array v2, v3, [I

    fill-array-data v2, :array_31

    aput-object v2, v0, v1

    const/16 v1, 0x32

    new-array v2, v4, [I

    fill-array-data v2, :array_32

    aput-object v2, v0, v1

    const/16 v1, 0x33

    new-array v2, v3, [I

    fill-array-data v2, :array_33

    aput-object v2, v0, v1

    const/16 v1, 0x34

    new-array v2, v4, [I

    fill-array-data v2, :array_34

    aput-object v2, v0, v1

    const/16 v1, 0x35

    new-array v2, v3, [I

    fill-array-data v2, :array_35

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/DateTimePatternGenerator;->types:[[I

    return-void

    :array_0
    .array-data 4
        0x47
        0x0
        -0x102
        0x1
        0x3
    .end array-data

    :array_1
    .array-data 4
        0x47
        0x0
        -0x103
        0x4
    .end array-data

    :array_2
    .array-data 4
        0x79
        0x1
        0x100
        0x1
        0x14
    .end array-data

    :array_3
    .array-data 4
        0x59
        0x1
        0x110
        0x1
        0x14
    .end array-data

    :array_4
    .array-data 4
        0x75
        0x1
        0x120
        0x1
        0x14
    .end array-data

    :array_5
    .array-data 4
        0x51
        0x2
        0x100
        0x1
        0x2
    .end array-data

    :array_6
    .array-data 4
        0x51
        0x2
        -0x102
        0x3
    .end array-data

    :array_7
    .array-data 4
        0x51
        0x2
        -0x103
        0x4
    .end array-data

    :array_8
    .array-data 4
        0x71
        0x2
        0x110
        0x1
        0x2
    .end array-data

    :array_9
    .array-data 4
        0x71
        0x2
        -0xf2
        0x3
    .end array-data

    :array_a
    .array-data 4
        0x71
        0x2
        -0xf3
        0x4
    .end array-data

    :array_b
    .array-data 4
        0x4d
        0x3
        0x100
        0x1
        0x2
    .end array-data

    :array_c
    .array-data 4
        0x4d
        0x3
        -0x102
        0x3
    .end array-data

    :array_d
    .array-data 4
        0x4d
        0x3
        -0x103
        0x4
    .end array-data

    :array_e
    .array-data 4
        0x4d
        0x3
        -0x101
        0x5
    .end array-data

    :array_f
    .array-data 4
        0x4c
        0x3
        0x110
        0x1
        0x2
    .end array-data

    :array_10
    .array-data 4
        0x4c
        0x3
        -0x112
        0x3
    .end array-data

    :array_11
    .array-data 4
        0x4c
        0x3
        -0x113
        0x4
    .end array-data

    :array_12
    .array-data 4
        0x4c
        0x3
        -0x111
        0x5
    .end array-data

    :array_13
    .array-data 4
        0x77
        0x4
        0x100
        0x1
        0x2
    .end array-data

    :array_14
    .array-data 4
        0x57
        0x5
        0x110
        0x1
    .end array-data

    :array_15
    .array-data 4
        0x65
        0x6
        0x110
        0x1
        0x2
    .end array-data

    :array_16
    .array-data 4
        0x65
        0x6
        -0x112
        0x3
    .end array-data

    :array_17
    .array-data 4
        0x65
        0x6
        -0x113
        0x4
    .end array-data

    :array_18
    .array-data 4
        0x65
        0x6
        -0x111
        0x5
    .end array-data

    :array_19
    .array-data 4
        0x45
        0x6
        -0x102
        0x1
        0x3
    .end array-data

    :array_1a
    .array-data 4
        0x45
        0x6
        -0x103
        0x4
    .end array-data

    :array_1b
    .array-data 4
        0x45
        0x6
        -0x101
        0x5
    .end array-data

    :array_1c
    .array-data 4
        0x63
        0x6
        0x120
        0x1
        0x2
    .end array-data

    :array_1d
    .array-data 4
        0x63
        0x6
        -0x122
        0x3
    .end array-data

    :array_1e
    .array-data 4
        0x63
        0x6
        -0x123
        0x4
    .end array-data

    :array_1f
    .array-data 4
        0x63
        0x6
        -0x121
        0x5
    .end array-data

    :array_20
    .array-data 4
        0x64
        0x7
        0x100
        0x1
        0x2
    .end array-data

    :array_21
    .array-data 4
        0x44
        0x8
        0x110
        0x1
        0x3
    .end array-data

    :array_22
    .array-data 4
        0x46
        0x9
        0x120
        0x1
    .end array-data

    :array_23
    .array-data 4
        0x67
        0x7
        0x130
        0x1
        0x14
    .end array-data

    :array_24
    .array-data 4
        0x61
        0xa
        -0x102
        0x1
    .end array-data

    :array_25
    .array-data 4
        0x48
        0xb
        0x1a0
        0x1
        0x2
    .end array-data

    :array_26
    .array-data 4
        0x6b
        0xb
        0x1b0
        0x1
        0x2
    .end array-data

    :array_27
    .array-data 4
        0x68
        0xb
        0x100
        0x1
        0x2
    .end array-data

    :array_28
    .array-data 4
        0x4b
        0xb
        0x110
        0x1
        0x2
    .end array-data

    :array_29
    .array-data 4
        0x6a
        0xb
        0x240
        0x1
        0x2
    .end array-data

    :array_2a
    .array-data 4
        0x6d
        0xc
        0x100
        0x1
        0x2
    .end array-data

    :array_2b
    .array-data 4
        0x73
        0xd
        0x100
        0x1
        0x2
    .end array-data

    :array_2c
    .array-data 4
        0x53
        0xe
        0x110
        0x1
        0x3e8
    .end array-data

    :array_2d
    .array-data 4
        0x41
        0xd
        0x120
        0x1
        0x3e8
    .end array-data

    :array_2e
    .array-data 4
        0x76
        0xf
        -0x122
        0x1
    .end array-data

    :array_2f
    .array-data 4
        0x76
        0xf
        -0x123
        0x4
    .end array-data

    :array_30
    .array-data 4
        0x7a
        0xf
        -0x102
        0x1
        0x3
    .end array-data

    :array_31
    .array-data 4
        0x7a
        0xf
        -0x103
        0x4
    .end array-data

    :array_32
    .array-data 4
        0x5a
        0xf
        -0x112
        0x1
        0x3
    .end array-data

    :array_33
    .array-data 4
        0x5a
        0xf
        -0x113
        0x4
    .end array-data

    :array_34
    .array-data 4
        0x56
        0xf
        -0x112
        0x1
        0x3
    .end array-data

    :array_35
    .array-data 4
        0x56
        0xf
        -0x113
        0x4
    .end array-data
.end method

.method protected constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/16 v4, 0x10

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1277
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    iput-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->skeleton2pattern:Ljava/util/TreeMap;

    .line 1278
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    iput-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->basePattern_pattern:Ljava/util/TreeMap;

    .line 1279
    const-string/jumbo v1, "?"

    iput-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->decimal:Ljava/lang/String;

    .line 1280
    const-string/jumbo v1, "{0} {1}"

    iput-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->dateTimeFormat:Ljava/lang/String;

    .line 1281
    new-array v1, v4, [Ljava/lang/String;

    iput-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->appendItemFormats:[Ljava/lang/String;

    .line 1282
    new-array v1, v4, [Ljava/lang/String;

    iput-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->appendItemNames:[Ljava/lang/String;

    .line 1284
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 1285
    iget-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->appendItemFormats:[Ljava/lang/String;

    const-string/jumbo v2, "{0} \u251c{2}: {1}\u2524"

    aput-object v2, v1, v0

    .line 1286
    iget-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->appendItemNames:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "F"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1284
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1290
    :cond_0
    new-instance v1, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    invoke-direct {v1, v5}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;-><init>(Lcom/ibm/icu/text/DateTimePatternGenerator$1;)V

    iput-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    .line 1291
    new-instance v1, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-direct {v1}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;-><init>()V

    iput-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    .line 1292
    new-instance v1, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    invoke-direct {v1, v5}, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;-><init>(Lcom/ibm/icu/text/DateTimePatternGenerator$1;)V

    iput-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->_distanceInfo:Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    .line 1294
    iput-object v5, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->skipMatcher:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    .line 1295
    iput-boolean v6, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->frozen:Z

    .line 1297
    iput-boolean v6, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->chineseMonthHack:Z

    .line 1385
    invoke-direct {p0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->complete()V

    .line 1520
    new-instance v1, Ljava/util/HashSet;

    const/16 v2, 0x14

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->cldrAvailableFormatKeys:Ljava/util/Set;

    .line 112
    return-void
.end method

.method static access$300(Ljava/lang/String;Z)I
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    invoke-static {p0, p1}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getCanonicalIndex(Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method

.method static access$400()[[I
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/ibm/icu/text/DateTimePatternGenerator;->types:[[I

    return-object v0
.end method

.method static access$800(I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 94
    invoke-static {p0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->showMask(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private adjustFieldTypes(Ljava/lang/String;Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;Z)Ljava/lang/String;
    .locals 11
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "inputRequest"    # Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
    .param p3, "fixFractionalSeconds"    # Z

    .prologue
    .line 1418
    iget-object v9, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-virtual {v9, p1}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->set(Ljava/lang/String;)Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    .line 1419
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 1420
    .local v6, "newPattern":Ljava/lang/StringBuffer;
    iget-object v9, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-virtual {v9}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->getItems()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1421
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 1422
    .local v4, "item":Ljava/lang/Object;
    instance-of v9, v4, Ljava/lang/String;

    if-eqz v9, :cond_0

    .line 1423
    iget-object v9, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    check-cast v4, Ljava/lang/String;

    .end local v4    # "item":Ljava/lang/Object;
    invoke-virtual {v9, v4}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->quoteLiteral(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    goto :goto_0

    .restart local v4    # "item":Ljava/lang/Object;
    :cond_0
    move-object v8, v4

    .line 1425
    check-cast v8, Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;

    .line 1426
    .local v8, "variableField":Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;
    invoke-virtual {v8}, Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1432
    .local v1, "field":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;->getType()I

    move-result v7

    .line 1434
    .local v7, "type":I
    if-eqz p3, :cond_2

    const/16 v9, 0xd

    if-ne v7, v9, :cond_2

    .line 1435
    invoke-static {p2}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->access$500(Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;)[Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0xe

    aget-object v5, v9, v10

    .line 1436
    .local v5, "newField":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    iget-object v10, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->decimal:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1448
    .end local v5    # "newField":Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1437
    :cond_2
    invoke-static {p2}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->access$600(Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;)[I

    move-result-object v9

    aget v9, v9, v7

    if-eqz v9, :cond_1

    .line 1438
    invoke-static {p2}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->access$500(Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;)[Ljava/lang/String;

    move-result-object v9

    aget-object v5, v9, v7

    .line 1440
    .restart local v5    # "newField":Ljava/lang/String;
    const/16 v9, 0xb

    if-eq v7, v9, :cond_3

    .line 1441
    move-object v1, v5

    .line 1442
    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v10

    if-eq v9, v10, :cond_1

    .line 1443
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1444
    .local v0, "c":C
    const-string/jumbo v1, ""

    .line 1445
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v2

    .local v2, "i":I
    :goto_2
    if-lez v2, :cond_1

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 1452
    .end local v0    # "c":C
    .end local v1    # "field":Ljava/lang/String;
    .end local v2    # "i":I
    .end local v4    # "item":Ljava/lang/Object;
    .end local v5    # "newField":Ljava/lang/String;
    .end local v7    # "type":I
    .end local v8    # "variableField":Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;
    :cond_4
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9
.end method

.method private checkFrozen()V
    .locals 2

    .prologue
    .line 1306
    invoke-virtual {p0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->isFrozen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1307
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Attempt to modify frozen object"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1309
    :cond_0
    return-void
.end method

.method private complete()V
    .locals 4

    .prologue
    .line 1376
    new-instance v1, Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;

    invoke-direct {v1}, Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;-><init>()V

    .line 1378
    .local v1, "patternInfo":Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/ibm/icu/text/DateTimePatternGenerator;->CANONICAL_ITEMS:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1380
    sget-object v2, Lcom/ibm/icu/text/DateTimePatternGenerator;->CANONICAL_ITEMS:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v1}, Lcom/ibm/icu/text/DateTimePatternGenerator;->addPattern(Ljava/lang/String;ZLcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;)Lcom/ibm/icu/text/DateTimePatternGenerator;

    .line 1378
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1383
    :cond_0
    return-void
.end method

.method private getAppendFormat(I)Ljava/lang/String;
    .locals 1
    .param p1, "foundMask"    # I

    .prologue
    .line 1347
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->appendItemFormats:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method private static getAppendFormatNumber(Ljava/lang/String;)I
    .locals 2
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 307
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/ibm/icu/text/DateTimePatternGenerator;->CLDR_FIELD_APPEND:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 308
    sget-object v1, Lcom/ibm/icu/text/DateTimePatternGenerator;->CLDR_FIELD_APPEND:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 310
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 307
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 310
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private getAppendName(I)Ljava/lang/String;
    .locals 2
    .param p1, "foundMask"    # I

    .prologue
    .line 1344
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->appendItemNames:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getBestAppending(I)Ljava/lang/String;
    .locals 11
    .param p1, "missingFields"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1316
    const/4 v1, 0x0

    .line 1317
    .local v1, "resultPattern":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 1318
    iget-object v5, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    iget-object v6, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->_distanceInfo:Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    invoke-direct {p0, v5, p1, v6}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getBestRaw(Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;ILcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;)Ljava/lang/String;

    move-result-object v1

    .line 1319
    iget-object v5, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    invoke-direct {p0, v1, v5, v9}, Lcom/ibm/icu/text/DateTimePatternGenerator;->adjustFieldTypes(Ljava/lang/String;Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;Z)Ljava/lang/String;

    move-result-object v1

    .line 1321
    :goto_0
    iget-object v5, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->_distanceInfo:Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    iget v5, v5, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;->missingFieldMask:I

    if-eqz v5, :cond_1

    .line 1325
    iget-object v5, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->_distanceInfo:Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    iget v5, v5, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;->missingFieldMask:I

    and-int/lit16 v5, v5, 0x6000

    const/16 v6, 0x4000

    if-ne v5, v6, :cond_0

    and-int/lit16 v5, p1, 0x6000

    const/16 v6, 0x6000

    if-ne v5, v6, :cond_0

    .line 1327
    iget-object v5, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    invoke-direct {p0, v1, v5, v10}, Lcom/ibm/icu/text/DateTimePatternGenerator;->adjustFieldTypes(Ljava/lang/String;Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;Z)Ljava/lang/String;

    move-result-object v1

    .line 1328
    iget-object v5, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->_distanceInfo:Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    iget v6, v5, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;->missingFieldMask:I

    and-int/lit16 v6, v6, -0x4001

    iput v6, v5, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;->missingFieldMask:I

    goto :goto_0

    .line 1332
    :cond_0
    iget-object v5, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->_distanceInfo:Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    iget v2, v5, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;->missingFieldMask:I

    .line 1333
    .local v2, "startingMask":I
    iget-object v5, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    iget-object v6, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->_distanceInfo:Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    iget v6, v6, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;->missingFieldMask:I

    iget-object v7, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->_distanceInfo:Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    invoke-direct {p0, v5, v6, v7}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getBestRaw(Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;ILcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;)Ljava/lang/String;

    move-result-object v3

    .line 1334
    .local v3, "temp":Ljava/lang/String;
    iget-object v5, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    invoke-direct {p0, v3, v5, v9}, Lcom/ibm/icu/text/DateTimePatternGenerator;->adjustFieldTypes(Ljava/lang/String;Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;Z)Ljava/lang/String;

    move-result-object v3

    .line 1335
    iget-object v5, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->_distanceInfo:Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    iget v5, v5, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;->missingFieldMask:I

    xor-int/lit8 v5, v5, -0x1

    and-int v0, v2, v5

    .line 1336
    .local v0, "foundMask":I
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getTopBitNumber(I)I

    move-result v4

    .line 1337
    .local v4, "topField":I
    invoke-direct {p0, v4}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getAppendFormat(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v9

    aput-object v3, v6, v10

    const/4 v7, 0x2

    invoke-direct {p0, v4}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getAppendName(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/ibm/icu/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1338
    goto :goto_0

    .line 1340
    .end local v0    # "foundMask":I
    .end local v2    # "startingMask":I
    .end local v3    # "temp":Ljava/lang/String;
    .end local v4    # "topField":I
    :cond_1
    return-object v1
.end method

.method private getBestRaw(Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;ILcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;)Ljava/lang/String;
    .locals 7
    .param p1, "source"    # Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
    .param p2, "includeMask"    # I
    .param p3, "missingFields"    # Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    .prologue
    .line 1394
    const v0, 0x7fffffff

    .line 1395
    .local v0, "bestDistance":I
    const-string/jumbo v1, ""

    .line 1396
    .local v1, "bestPattern":Ljava/lang/String;
    new-instance v4, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    const/4 v6, 0x0

    invoke-direct {v4, v6}, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;-><init>(Lcom/ibm/icu/text/DateTimePatternGenerator$1;)V

    .line 1397
    .local v4, "tempInfo":Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;
    iget-object v6, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->skeleton2pattern:Ljava/util/TreeMap;

    invoke-virtual {v6}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1398
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    .line 1399
    .local v5, "trial":Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
    iget-object v6, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->skipMatcher:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    invoke-virtual {v5, v6}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1400
    invoke-virtual {p1, v5, p2, v4}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->getDistance(Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;ILcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;)I

    move-result v2

    .line 1403
    .local v2, "distance":I
    if-ge v2, v0, :cond_0

    .line 1404
    move v0, v2

    .line 1405
    iget-object v6, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->skeleton2pattern:Ljava/util/TreeMap;

    invoke-virtual {v6, v5}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "bestPattern":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 1406
    .restart local v1    # "bestPattern":Ljava/lang/String;
    invoke-virtual {p3, v4}, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;->setTo(Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;)V

    .line 1407
    if-nez v2, :cond_0

    .line 1410
    .end local v2    # "distance":I
    .end local v5    # "trial":Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
    :cond_1
    return-object v1
.end method

.method private static getCanonicalIndex(Ljava/lang/String;Z)I
    .locals 8
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "strict"    # Z

    .prologue
    const/4 v7, 0x0

    const/4 v5, -0x1

    .line 1555
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 1556
    .local v3, "len":I
    if-nez v3, :cond_1

    .line 1575
    :cond_0
    :goto_0
    return v5

    .line 1559
    :cond_1
    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 1561
    .local v1, "ch":I
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_2

    .line 1562
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v1, :cond_0

    .line 1561
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1566
    :cond_2
    const/4 v0, -0x1

    .line 1567
    .local v0, "bestRow":I
    const/4 v2, 0x0

    :goto_2
    sget-object v6, Lcom/ibm/icu/text/DateTimePatternGenerator;->types:[[I

    array-length v6, v6

    if-ge v2, v6, :cond_5

    .line 1568
    sget-object v6, Lcom/ibm/icu/text/DateTimePatternGenerator;->types:[[I

    aget-object v4, v6, v2

    .line 1569
    .local v4, "row":[I
    aget v6, v4, v7

    if-eq v6, v1, :cond_4

    .line 1567
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1570
    :cond_4
    move v0, v2

    .line 1571
    const/4 v6, 0x3

    aget v6, v4, v6

    if-gt v6, v3, :cond_3

    .line 1572
    array-length v6, v4

    add-int/lit8 v6, v6, -0x1

    aget v6, v4, v6

    if-lt v6, v3, :cond_3

    move v5, v2

    .line 1573
    goto :goto_0

    .line 1575
    .end local v4    # "row":[I
    :cond_5
    if-eqz p1, :cond_6

    move v0, v5

    .end local v0    # "bestRow":I
    :cond_6
    move v5, v0

    goto :goto_0
.end method

.method public static getEmptyInstance()Lcom/ibm/icu/text/DateTimePatternGenerator;
    .locals 1

    .prologue
    .line 104
    new-instance v0, Lcom/ibm/icu/text/DateTimePatternGenerator;

    invoke-direct {v0}, Lcom/ibm/icu/text/DateTimePatternGenerator;-><init>()V

    return-object v0
.end method

.method private static getFilteredPattern(Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;Ljava/util/BitSet;)Ljava/lang/String;
    .locals 5
    .param p0, "fp"    # Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;
    .param p1, "nuke"    # Ljava/util/BitSet;

    .prologue
    .line 286
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 287
    .local v2, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-static {p0}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->access$000(Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 288
    invoke-virtual {p1, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 287
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 289
    :cond_0
    invoke-static {p0}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->access$000(Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 290
    .local v1, "item":Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 291
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->quoteLiteral(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 292
    goto :goto_1

    .line 293
    :cond_1
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 296
    .end local v1    # "item":Ljava/lang/Object;
    :cond_2
    return-object v2
.end method

.method public static getInstance()Lcom/ibm/icu/text/DateTimePatternGenerator;
    .locals 1

    .prologue
    .line 119
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateTimePatternGenerator;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateTimePatternGenerator;
    .locals 31
    .param p0, "uLocale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 128
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/util/ULocale;->toString()Ljava/lang/String;

    move-result-object v19

    .line 129
    .local v19, "localeKey":Ljava/lang/String;
    sget-object v29, Lcom/ibm/icu/text/DateTimePatternGenerator;->DTPNG_CACHE:Lcom/ibm/icu/impl/ICUCache;

    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/ibm/icu/impl/ICUCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/ibm/icu/text/DateTimePatternGenerator;

    .line 130
    .local v25, "result":Lcom/ibm/icu/text/DateTimePatternGenerator;
    if-eqz v25, :cond_0

    move-object/from16 v26, v25

    .line 232
    .end local v25    # "result":Lcom/ibm/icu/text/DateTimePatternGenerator;
    .local v26, "result":Ljava/lang/Object;
    :goto_0
    return-object v26

    .line 133
    .end local v26    # "result":Ljava/lang/Object;
    .restart local v25    # "result":Lcom/ibm/icu/text/DateTimePatternGenerator;
    :cond_0
    new-instance v25, Lcom/ibm/icu/text/DateTimePatternGenerator;

    .end local v25    # "result":Lcom/ibm/icu/text/DateTimePatternGenerator;
    invoke-direct/range {v25 .. v25}, Lcom/ibm/icu/text/DateTimePatternGenerator;-><init>()V

    .line 134
    .restart local v25    # "result":Lcom/ibm/icu/text/DateTimePatternGenerator;
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/util/ULocale;->getLanguage()Ljava/lang/String;

    move-result-object v18

    .line 135
    .local v18, "lang":Ljava/lang/String;
    const-string/jumbo v29, "zh"

    move-object/from16 v0, v18

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-nez v29, :cond_1

    const-string/jumbo v29, "ko"

    move-object/from16 v0, v18

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-nez v29, :cond_1

    const-string/jumbo v29, "ja"

    move-object/from16 v0, v18

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_2

    .line 136
    :cond_1
    const/16 v29, 0x1

    move/from16 v0, v29

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/ibm/icu/text/DateTimePatternGenerator;->chineseMonthHack:Z

    .line 138
    :cond_2
    new-instance v27, Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;

    invoke-direct/range {v27 .. v27}, Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;-><init>()V

    .line 139
    .local v27, "returnInfo":Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;
    const/4 v15, 0x0

    .line 141
    .local v15, "hackPattern":Ljava/lang/String;
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_1
    const/16 v29, 0x3

    move/from16 v0, v16

    move/from16 v1, v29

    if-gt v0, v1, :cond_4

    .line 142
    move/from16 v0, v16

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/ibm/icu/text/DateFormat;->getDateInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v6

    check-cast v6, Lcom/ibm/icu/text/SimpleDateFormat;

    .line 143
    .local v6, "df":Lcom/ibm/icu/text/SimpleDateFormat;
    invoke-virtual {v6}, Lcom/ibm/icu/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    move/from16 v2, v30

    move-object/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Lcom/ibm/icu/text/DateTimePatternGenerator;->addPattern(Ljava/lang/String;ZLcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;)Lcom/ibm/icu/text/DateTimePatternGenerator;

    .line 144
    move/from16 v0, v16

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/ibm/icu/text/DateFormat;->getTimeInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v6

    .end local v6    # "df":Lcom/ibm/icu/text/SimpleDateFormat;
    check-cast v6, Lcom/ibm/icu/text/SimpleDateFormat;

    .line 145
    .restart local v6    # "df":Lcom/ibm/icu/text/SimpleDateFormat;
    invoke-virtual {v6}, Lcom/ibm/icu/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    move/from16 v2, v30

    move-object/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Lcom/ibm/icu/text/DateTimePatternGenerator;->addPattern(Ljava/lang/String;ZLcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;)Lcom/ibm/icu/text/DateTimePatternGenerator;

    .line 147
    const/16 v29, 0x2

    move/from16 v0, v16

    move/from16 v1, v29

    if-ne v0, v1, :cond_3

    .line 148
    invoke-virtual {v6}, Lcom/ibm/icu/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v15

    .line 141
    :cond_3
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 152
    .end local v6    # "df":Lcom/ibm/icu/text/SimpleDateFormat;
    :cond_4
    const-string/jumbo v29, "com/ibm/icu/impl/data/icudt40b"

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v24

    check-cast v24, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 153
    .local v24, "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v29, "calendar"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v24

    .line 154
    const-string/jumbo v29, "gregorian"

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v14

    .line 157
    .local v14, "gregorianBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v29, "appendItems"

    move-object/from16 v0, v29

    invoke-virtual {v14, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v17

    .line 158
    .local v17, "itemBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    const/16 v16, 0x0

    :goto_2
    invoke-virtual/range {v17 .. v17}, Lcom/ibm/icu/impl/ICUResourceBundle;->getSize()I

    move-result v29

    move/from16 v0, v16

    move/from16 v1, v29

    if-ge v0, v1, :cond_5

    .line 159
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v10

    check-cast v10, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 160
    .local v10, "formatBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v12

    .line 161
    .local v12, "formatName":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/ibm/icu/impl/ICUResourceBundle;->getString()Ljava/lang/String;

    move-result-object v28

    .line 162
    .local v28, "value":Ljava/lang/String;
    invoke-static {v12}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getAppendFormatNumber(Ljava/lang/String;)I

    move-result v29

    move-object/from16 v0, v25

    move/from16 v1, v29

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/DateTimePatternGenerator;->setAppendItemFormat(ILjava/lang/String;)V

    .line 158
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 166
    .end local v10    # "formatBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v12    # "formatName":Ljava/lang/String;
    .end local v28    # "value":Ljava/lang/String;
    :cond_5
    const-string/jumbo v29, "fields"

    move-object/from16 v0, v29

    invoke-virtual {v14, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v17

    .line 168
    const/16 v16, 0x0

    :goto_3
    const/16 v29, 0x10

    move/from16 v0, v16

    move/from16 v1, v29

    if-ge v0, v1, :cond_7

    .line 169
    invoke-static/range {v16 .. v16}, Lcom/ibm/icu/text/DateTimePatternGenerator;->isCLDRFieldName(I)Z

    move-result v29

    if-eqz v29, :cond_6

    .line 170
    sget-object v29, Lcom/ibm/icu/text/DateTimePatternGenerator;->CLDR_FIELD_NAME:[Ljava/lang/String;

    aget-object v29, v29, v16

    move-object/from16 v0, v17

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v9

    .line 171
    .local v9, "fieldBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v29, "dn"

    move-object/from16 v0, v29

    invoke-virtual {v9, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v8

    .line 172
    .local v8, "dnBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {v8}, Lcom/ibm/icu/impl/ICUResourceBundle;->getString()Ljava/lang/String;

    move-result-object v28

    .line 174
    .restart local v28    # "value":Ljava/lang/String;
    move-object/from16 v0, v25

    move/from16 v1, v16

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/DateTimePatternGenerator;->setAppendItemName(ILjava/lang/String;)V

    .line 168
    .end local v8    # "dnBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v9    # "fieldBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v28    # "value":Ljava/lang/String;
    :cond_6
    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    .line 180
    :cond_7
    :try_start_0
    const-string/jumbo v29, "availableFormats"

    move-object/from16 v0, v29

    invoke-virtual {v14, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v10

    .line 182
    .restart local v10    # "formatBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    const/16 v16, 0x0

    :goto_4
    invoke-virtual {v10}, Lcom/ibm/icu/impl/ICUResourceBundle;->getSize()I

    move-result v29

    move/from16 v0, v16

    move/from16 v1, v29

    if-ge v0, v1, :cond_8

    .line 183
    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v11

    .line 184
    .local v11, "formatKey":Ljava/lang/String;
    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/ibm/icu/util/UResourceBundle;->getString()Ljava/lang/String;

    move-result-object v13

    .line 186
    .local v13, "formatValue":Ljava/lang/String;
    move-object/from16 v0, v25

    invoke-direct {v0, v11}, Lcom/ibm/icu/text/DateTimePatternGenerator;->setAvailableFormat(Ljava/lang/String;)V

    .line 187
    const/16 v29, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v29

    move-object/from16 v2, v27

    invoke-virtual {v0, v13, v1, v2}, Lcom/ibm/icu/text/DateTimePatternGenerator;->addPattern(Ljava/lang/String;ZLcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;)Lcom/ibm/icu/text/DateTimePatternGenerator;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    add-int/lit8 v16, v16, 0x1

    goto :goto_4

    .line 189
    .end local v10    # "formatBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v11    # "formatKey":Ljava/lang/String;
    .end local v13    # "formatValue":Ljava/lang/String;
    :catch_0
    move-exception v29

    .line 192
    :cond_8
    move-object/from16 v21, p0

    .line 193
    .local v21, "parentLocale":Lcom/ibm/icu/util/ULocale;
    :cond_9
    :goto_5
    invoke-virtual/range {v21 .. v21}, Lcom/ibm/icu/util/ULocale;->getFallback()Lcom/ibm/icu/util/ULocale;

    move-result-object v21

    if-eqz v21, :cond_b

    .line 194
    const-string/jumbo v29, "com/ibm/icu/impl/data/icudt40b"

    move-object/from16 v0, v29

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v23

    check-cast v23, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 195
    .local v23, "prb":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v29, "calendar"

    move-object/from16 v0, v23

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v23

    .line 196
    const-string/jumbo v29, "gregorian"

    move-object/from16 v0, v23

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v20

    .line 198
    .local v20, "pGregorianBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    :try_start_1
    const-string/jumbo v29, "availableFormats"

    move-object/from16 v0, v20

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v10

    .line 200
    .restart local v10    # "formatBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    const/16 v16, 0x0

    :goto_6
    invoke-virtual {v10}, Lcom/ibm/icu/impl/ICUResourceBundle;->getSize()I

    move-result v29

    move/from16 v0, v16

    move/from16 v1, v29

    if-ge v0, v1, :cond_9

    .line 201
    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v11

    .line 202
    .restart local v11    # "formatKey":Ljava/lang/String;
    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/ibm/icu/util/UResourceBundle;->getString()Ljava/lang/String;

    move-result-object v13

    .line 204
    .restart local v13    # "formatValue":Ljava/lang/String;
    move-object/from16 v0, v25

    invoke-direct {v0, v11}, Lcom/ibm/icu/text/DateTimePatternGenerator;->isAvailableFormatSet(Ljava/lang/String;)Z

    move-result v29

    if-nez v29, :cond_a

    .line 205
    move-object/from16 v0, v25

    invoke-direct {v0, v11}, Lcom/ibm/icu/text/DateTimePatternGenerator;->setAvailableFormat(Ljava/lang/String;)V

    .line 206
    const/16 v29, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v29

    move-object/from16 v2, v27

    invoke-virtual {v0, v13, v1, v2}, Lcom/ibm/icu/text/DateTimePatternGenerator;->addPattern(Ljava/lang/String;ZLcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;)Lcom/ibm/icu/text/DateTimePatternGenerator;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 200
    :cond_a
    add-int/lit8 v16, v16, 0x1

    goto :goto_6

    .line 218
    .end local v10    # "formatBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v11    # "formatKey":Ljava/lang/String;
    .end local v13    # "formatValue":Ljava/lang/String;
    .end local v20    # "pGregorianBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v23    # "prb":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_b
    if-eqz v15, :cond_c

    .line 219
    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-static {v0, v1, v15}, Lcom/ibm/icu/text/DateTimePatternGenerator;->hackTimes(Lcom/ibm/icu/text/DateTimePatternGenerator;Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;Ljava/lang/String;)V

    .line 223
    :cond_c
    invoke-static/range {p0 .. p0}, Lcom/ibm/icu/util/Calendar;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/Calendar;

    move-result-object v4

    .line 224
    .local v4, "cal":Lcom/ibm/icu/util/Calendar;
    new-instance v5, Lcom/ibm/icu/impl/CalendarData;

    invoke-virtual {v4}, Lcom/ibm/icu/util/Calendar;->getType()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v5, v0, v1}, Lcom/ibm/icu/impl/CalendarData;-><init>(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)V

    .line 225
    .local v5, "calData":Lcom/ibm/icu/impl/CalendarData;
    const-string/jumbo v29, "DateTimePatterns"

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Lcom/ibm/icu/impl/CalendarData;->get(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/ibm/icu/impl/ICUResourceBundle;->getStringArray()[Ljava/lang/String;

    move-result-object v22

    .line 226
    .local v22, "patterns":[Ljava/lang/String;
    const/16 v29, 0x8

    aget-object v29, v22, v29

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DateTimePatternGenerator;->setDateTimeFormat(Ljava/lang/String;)V

    .line 229
    new-instance v7, Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/ibm/icu/text/DecimalFormatSymbols;-><init>(Lcom/ibm/icu/util/ULocale;)V

    .line 230
    .local v7, "dfs":Lcom/ibm/icu/text/DecimalFormatSymbols;
    invoke-virtual {v7}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v29

    invoke-static/range {v29 .. v29}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DateTimePatternGenerator;->setDecimal(Ljava/lang/String;)V

    .line 231
    sget-object v29, Lcom/ibm/icu/text/DateTimePatternGenerator;->DTPNG_CACHE:Lcom/ibm/icu/impl/ICUCache;

    move-object/from16 v0, v29

    move-object/from16 v1, v19

    move-object/from16 v2, v25

    invoke-interface {v0, v1, v2}, Lcom/ibm/icu/impl/ICUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v26, v25

    .line 232
    .restart local v26    # "result":Ljava/lang/Object;
    goto/16 :goto_0

    .line 211
    .end local v4    # "cal":Lcom/ibm/icu/util/Calendar;
    .end local v5    # "calData":Lcom/ibm/icu/impl/CalendarData;
    .end local v7    # "dfs":Lcom/ibm/icu/text/DecimalFormatSymbols;
    .end local v22    # "patterns":[Ljava/lang/String;
    .end local v26    # "result":Ljava/lang/Object;
    .restart local v20    # "pGregorianBundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v23    # "prb":Lcom/ibm/icu/impl/ICUResourceBundle;
    :catch_1
    move-exception v29

    goto/16 :goto_5
.end method

.method private static getName(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 1538
    invoke-static {p0, v2}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getCanonicalIndex(Ljava/lang/String;Z)I

    move-result v0

    .line 1539
    .local v0, "i":I
    sget-object v4, Lcom/ibm/icu/text/DateTimePatternGenerator;->FIELD_NAME:[Ljava/lang/String;

    sget-object v5, Lcom/ibm/icu/text/DateTimePatternGenerator;->types:[[I

    aget-object v5, v5, v0

    aget v5, v5, v2

    aget-object v1, v4, v5

    .line 1540
    .local v1, "name":Ljava/lang/String;
    sget-object v4, Lcom/ibm/icu/text/DateTimePatternGenerator;->types:[[I

    aget-object v4, v4, v0

    const/4 v5, 0x2

    aget v3, v4, v5

    .line 1541
    .local v3, "subtype":I
    if-gez v3, :cond_1

    .line 1542
    .local v2, "string":Z
    :goto_0
    if-eqz v2, :cond_0

    neg-int v3, v3

    .line 1543
    :cond_0
    if-gez v3, :cond_2

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ":S"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1545
    :goto_1
    return-object v1

    .line 1541
    .end local v2    # "string":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1544
    .restart local v2    # "string":Z
    :cond_2
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ":N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private getTopBitNumber(I)I
    .locals 2
    .param p1, "foundMask"    # I

    .prologue
    .line 1364
    const/4 v0, 0x0

    .line 1365
    .local v0, "i":I
    :goto_0
    if-eqz p1, :cond_0

    .line 1366
    ushr-int/lit8 p1, p1, 0x1

    .line 1367
    add-int/lit8 v0, v0, 0x1

    .line 1368
    goto :goto_0

    .line 1369
    :cond_0
    add-int/lit8 v1, v0, -0x1

    return v1
.end method

.method private static hackTimes(Lcom/ibm/icu/text/DateTimePatternGenerator;Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;Ljava/lang/String;)V
    .locals 12
    .param p0, "result"    # Lcom/ibm/icu/text/DateTimePatternGenerator;
    .param p1, "returnInfo"    # Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;
    .param p2, "hackPattern"    # Ljava/lang/String;

    .prologue
    .line 236
    iget-object v9, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-virtual {v9, p2}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->set(Ljava/lang/String;)Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    .line 237
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6}, Ljava/lang/String;-><init>()V

    .line 239
    .local v6, "mmss":Ljava/lang/String;
    const/4 v1, 0x0

    .line 240
    .local v1, "gotMm":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v9, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-static {v9}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->access$000(Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-ge v3, v9, :cond_3

    .line 241
    iget-object v9, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-static {v9}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->access$000(Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 242
    .local v4, "item":Ljava/lang/Object;
    instance-of v9, v4, Ljava/lang/String;

    if-eqz v9, :cond_1

    .line 243
    if-eqz v1, :cond_0

    .line 244
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    iget-object v10, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->quoteLiteral(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    .line 240
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 247
    :cond_1
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 248
    .local v0, "ch":C
    const/16 v9, 0x6d

    if-ne v0, v9, :cond_2

    .line 249
    const/4 v1, 0x1

    .line 250
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    .line 251
    goto :goto_1

    :cond_2
    const/16 v9, 0x73

    if-ne v0, v9, :cond_7

    .line 252
    if-nez v1, :cond_6

    .line 265
    .end local v0    # "ch":C
    .end local v4    # "item":Ljava/lang/Object;
    :cond_3
    :goto_2
    new-instance v8, Ljava/util/BitSet;

    invoke-direct {v8}, Ljava/util/BitSet;-><init>()V

    .line 266
    .local v8, "variables":Ljava/util/BitSet;
    new-instance v7, Ljava/util/BitSet;

    invoke-direct {v7}, Ljava/util/BitSet;-><init>()V

    .line 267
    .local v7, "nuke":Ljava/util/BitSet;
    const/4 v3, 0x0

    :goto_3
    iget-object v9, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-static {v9}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->access$000(Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-ge v3, v9, :cond_9

    .line 268
    iget-object v9, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-static {v9}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->access$000(Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 269
    .restart local v4    # "item":Ljava/lang/Object;
    instance-of v9, v4, Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;

    if-eqz v9, :cond_5

    .line 270
    invoke-virtual {v8, v3}, Ljava/util/BitSet;->set(I)V

    .line 271
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 272
    .restart local v0    # "ch":C
    const/16 v9, 0x73

    if-eq v0, v9, :cond_4

    const/16 v9, 0x53

    if-ne v0, v9, :cond_5

    .line 273
    :cond_4
    invoke-virtual {v7, v3}, Ljava/util/BitSet;->set(I)V

    .line 274
    add-int/lit8 v5, v3, -0x1

    .local v5, "j":I
    :goto_4
    if-ltz v5, :cond_5

    .line 275
    invoke-virtual {v8, v5}, Ljava/util/BitSet;->get(I)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 267
    .end local v0    # "ch":C
    .end local v5    # "j":I
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 255
    .end local v7    # "nuke":Ljava/util/BitSet;
    .end local v8    # "variables":Ljava/util/BitSet;
    .restart local v0    # "ch":C
    :cond_6
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    .line 256
    const/4 v9, 0x0

    invoke-virtual {p0, v6, v9, p1}, Lcom/ibm/icu/text/DateTimePatternGenerator;->addPattern(Ljava/lang/String;ZLcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;)Lcom/ibm/icu/text/DateTimePatternGenerator;

    goto :goto_2

    .line 258
    :cond_7
    if-nez v1, :cond_3

    const/16 v9, 0x7a

    if-eq v0, v9, :cond_3

    const/16 v9, 0x5a

    if-eq v0, v9, :cond_3

    const/16 v9, 0x76

    if-eq v0, v9, :cond_3

    const/16 v9, 0x56

    if-ne v0, v9, :cond_0

    goto :goto_2

    .line 276
    .restart local v5    # "j":I
    .restart local v7    # "nuke":Ljava/util/BitSet;
    .restart local v8    # "variables":Ljava/util/BitSet;
    :cond_8
    invoke-virtual {v7, v3}, Ljava/util/BitSet;->set(I)V

    .line 274
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 281
    .end local v0    # "ch":C
    .end local v4    # "item":Ljava/lang/Object;
    .end local v5    # "j":I
    :cond_9
    iget-object v9, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-static {v9, v7}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getFilteredPattern(Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;Ljava/util/BitSet;)Ljava/lang/String;

    move-result-object v2

    .line 282
    .local v2, "hhmm":Ljava/lang/String;
    const/4 v9, 0x0

    invoke-virtual {p0, v2, v9, p1}, Lcom/ibm/icu/text/DateTimePatternGenerator;->addPattern(Ljava/lang/String;ZLcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;)Lcom/ibm/icu/text/DateTimePatternGenerator;

    .line 283
    return-void
.end method

.method private isAvailableFormatSet(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 852
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->cldrAvailableFormatKeys:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static isCLDRFieldName(I)Z
    .locals 3
    .param p0, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 315
    if-gez p0, :cond_1

    const/16 v1, 0x10

    if-lt p0, v1, :cond_1

    .line 322
    :cond_0
    :goto_0
    return v0

    .line 318
    :cond_1
    sget-object v1, Lcom/ibm/icu/text/DateTimePatternGenerator;->CLDR_FIELD_NAME:[Ljava/lang/String;

    aget-object v1, v1, p0

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x2a

    if-eq v1, v2, :cond_0

    .line 322
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isSingleField(Ljava/lang/String;)Z
    .locals 4
    .param p0, "skeleton"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 822
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 823
    .local v0, "first":C
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 824
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v3, v0, :cond_0

    .line 826
    :goto_1
    return v2

    .line 823
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 826
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private setAvailableFormat(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 836
    invoke-direct {p0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->checkFrozen()V

    .line 837
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->cldrAvailableFormatKeys:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 838
    return-void
.end method

.method private static showMask(I)Ljava/lang/String;
    .locals 4
    .param p0, "mask"    # I

    .prologue
    .line 1485
    const-string/jumbo v1, ""

    .line 1486
    .local v1, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x10

    if-ge v0, v2, :cond_2

    .line 1487
    const/4 v2, 0x1

    shl-int/2addr v2, v0

    and-int/2addr v2, p0

    if-nez v2, :cond_0

    .line 1486
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1488
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, " | "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1489
    :cond_1
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    sget-object v3, Lcom/ibm/icu/text/DateTimePatternGenerator;->FIELD_NAME:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1491
    :cond_2
    return-object v1
.end method


# virtual methods
.method public addPattern(Ljava/lang/String;ZLcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;)Lcom/ibm/icu/text/DateTimePatternGenerator;
    .locals 6
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "override"    # Z
    .param p3, "returnInfo"    # Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;

    .prologue
    .line 430
    invoke-direct {p0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->checkFrozen()V

    .line 431
    new-instance v4, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;-><init>(Lcom/ibm/icu/text/DateTimePatternGenerator$1;)V

    iget-object v5, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-virtual {v4, p1, v5}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->set(Ljava/lang/String;Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;)Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    move-result-object v1

    .line 432
    .local v1, "matcher":Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
    invoke-virtual {v1}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->getBasePattern()Ljava/lang/String;

    move-result-object v0

    .line 433
    .local v0, "basePattern":Ljava/lang/String;
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->basePattern_pattern:Ljava/util/TreeMap;

    invoke-virtual {v4, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 434
    .local v2, "previousPatternWithSameBase":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 435
    const/4 v4, 0x1

    iput v4, p3, Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;->status:I

    .line 436
    iput-object v2, p3, Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;->conflictingPattern:Ljava/lang/String;

    .line 437
    if-nez p2, :cond_1

    .line 449
    :cond_0
    :goto_0
    return-object p0

    .line 439
    :cond_1
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->skeleton2pattern:Ljava/util/TreeMap;

    invoke-virtual {v4, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 440
    .local v3, "previousValue":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 441
    const/4 v4, 0x2

    iput v4, p3, Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;->status:I

    .line 442
    iput-object v3, p3, Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;->conflictingPattern:Ljava/lang/String;

    .line 443
    if-eqz p2, :cond_0

    .line 445
    :cond_2
    const/4 v4, 0x0

    iput v4, p3, Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;->status:I

    .line 446
    const-string/jumbo v4, ""

    iput-object v4, p3, Lcom/ibm/icu/text/DateTimePatternGenerator$PatternInfo;->conflictingPattern:Ljava/lang/String;

    .line 447
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->skeleton2pattern:Ljava/util/TreeMap;

    invoke-virtual {v4, v1, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->basePattern_pattern:Ljava/util/TreeMap;

    invoke-virtual {v4, v0, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public clone()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 888
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/text/DateTimePatternGenerator;

    move-object v0, v3

    check-cast v0, Lcom/ibm/icu/text/DateTimePatternGenerator;

    move-object v2, v0

    .line 889
    .local v2, "result":Lcom/ibm/icu/text/DateTimePatternGenerator;
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->skeleton2pattern:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/TreeMap;

    iput-object v3, v2, Lcom/ibm/icu/text/DateTimePatternGenerator;->skeleton2pattern:Ljava/util/TreeMap;

    .line 890
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->basePattern_pattern:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/TreeMap;

    iput-object v3, v2, Lcom/ibm/icu/text/DateTimePatternGenerator;->basePattern_pattern:Ljava/util/TreeMap;

    .line 891
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->appendItemFormats:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    check-cast v3, [Ljava/lang/String;

    iput-object v3, v2, Lcom/ibm/icu/text/DateTimePatternGenerator;->appendItemFormats:[Ljava/lang/String;

    .line 892
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->appendItemNames:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    check-cast v3, [Ljava/lang/String;

    iput-object v3, v2, Lcom/ibm/icu/text/DateTimePatternGenerator;->appendItemNames:[Ljava/lang/String;

    .line 893
    new-instance v3, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;-><init>(Lcom/ibm/icu/text/DateTimePatternGenerator$1;)V

    iput-object v3, v2, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    .line 894
    new-instance v3, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-direct {v3}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;-><init>()V

    iput-object v3, v2, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    .line 895
    new-instance v3, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;-><init>(Lcom/ibm/icu/text/DateTimePatternGenerator$1;)V

    iput-object v3, v2, Lcom/ibm/icu/text/DateTimePatternGenerator;->_distanceInfo:Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    .line 897
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/ibm/icu/text/DateTimePatternGenerator;->frozen:Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 898
    return-object v2

    .line 899
    .end local v2    # "result":Lcom/ibm/icu/text/DateTimePatternGenerator;
    :catch_0
    move-exception v1

    .line 900
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Internal Error"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public cloneAsThawed()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 877
    invoke-virtual {p0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/DateTimePatternGenerator;

    move-object v0, v1

    check-cast v0, Lcom/ibm/icu/text/DateTimePatternGenerator;

    .line 878
    .local v0, "result":Lcom/ibm/icu/text/DateTimePatternGenerator;
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->frozen:Z

    .line 879
    return-object v0
.end method

.method public freeze()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 868
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->frozen:Z

    .line 869
    return-object p0
.end method

.method public getAppendItemFormat(I)Ljava/lang/String;
    .locals 1
    .param p1, "field"    # I

    .prologue
    .line 782
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->appendItemFormats:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getAppendItemName(I)Ljava/lang/String;
    .locals 1
    .param p1, "field"    # I

    .prologue
    .line 810
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->appendItemNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getBaseSkeleton(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 481
    monitor-enter p0

    .line 482
    :try_start_0
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    iget-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-virtual {v0, p1, v1}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->set(Ljava/lang/String;Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;)Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    .line 483
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    invoke-virtual {v0}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->getBasePattern()Ljava/lang/String;

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 484
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getBaseSkeletons(Ljava/util/Set;)Ljava/util/Set;
    .locals 1
    .param p1, "result"    # Ljava/util/Set;

    .prologue
    .line 525
    if-nez p1, :cond_0

    new-instance p1, Ljava/util/HashSet;

    .end local p1    # "result":Ljava/util/Set;
    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 526
    .restart local p1    # "result":Ljava/util/Set;
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->basePattern_pattern:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 527
    return-object p1
.end method

.method public getBestPattern(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "skeleton"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 337
    iget-boolean v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->chineseMonthHack:Z

    if-eqz v4, :cond_0

    .line 354
    const-string/jumbo v4, "MMM+"

    const-string/jumbo v5, "MM"

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 358
    :cond_0
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    iget-object v5, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-virtual {v4, p1, v5}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->set(Ljava/lang/String;Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;)Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    .line 359
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    const/4 v5, -0x1

    iget-object v6, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->_distanceInfo:Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    invoke-direct {p0, v4, v5, v6}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getBestRaw(Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;ILcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;)Ljava/lang/String;

    move-result-object v0

    .line 360
    .local v0, "best":Ljava/lang/String;
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->_distanceInfo:Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    iget v4, v4, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;->missingFieldMask:I

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->_distanceInfo:Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    iget v4, v4, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;->extraFieldMask:I

    if-nez v4, :cond_2

    .line 362
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    invoke-direct {p0, v0, v4, v7}, Lcom/ibm/icu/text/DateTimePatternGenerator;->adjustFieldTypes(Ljava/lang/String;Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;Z)Ljava/lang/String;

    move-result-object v3

    .line 371
    :cond_1
    :goto_0
    return-object v3

    .line 364
    :cond_2
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    invoke-virtual {v4}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->getFieldMask()I

    move-result v2

    .line 366
    .local v2, "neededFields":I
    and-int/lit16 v4, v2, 0x3ff

    invoke-direct {p0, v4}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getBestAppending(I)Ljava/lang/String;

    move-result-object v1

    .line 367
    .local v1, "datePattern":Ljava/lang/String;
    const v4, 0xfc00

    and-int/2addr v4, v2

    invoke-direct {p0, v4}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getBestAppending(I)Ljava/lang/String;

    move-result-object v3

    .line 369
    .local v3, "timePattern":Ljava/lang/String;
    if-nez v1, :cond_3

    if-nez v3, :cond_1

    const-string/jumbo v3, ""

    goto :goto_0

    .line 370
    :cond_3
    if-nez v3, :cond_4

    move-object v3, v1

    goto :goto_0

    .line 371
    :cond_4
    invoke-virtual {p0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getDateTimeFormat()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v7

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Lcom/ibm/icu/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public getDateTimeFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->dateTimeFormat:Ljava/lang/String;

    return-object v0
.end method

.method public getDecimal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 606
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->decimal:Ljava/lang/String;

    return-object v0
.end method

.method public getFields(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 1471
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-virtual {v3, p1}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->set(Ljava/lang/String;)Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    .line 1472
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 1473
    .local v2, "newPattern":Ljava/lang/StringBuffer;
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-virtual {v3}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->getItems()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1474
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1475
    .local v1, "item":Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 1476
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    check-cast v1, Ljava/lang/String;

    .end local v1    # "item":Ljava/lang/Object;
    invoke-virtual {v3, v1}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->quoteLiteral(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1478
    .restart local v1    # "item":Ljava/lang/Object;
    :cond_0
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "{"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1481
    .end local v1    # "item":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getRedundants(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 5
    .param p1, "output"    # Ljava/util/Collection;

    .prologue
    .line 623
    monitor-enter p0

    .line 624
    if-nez p1, :cond_0

    .line 631
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->skeleton2pattern:Ljava/util/TreeMap;

    invoke-virtual {v4}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 632
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    .line 633
    .local v0, "cur":Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->skeleton2pattern:Ljava/util/TreeMap;

    invoke-virtual {v4, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 634
    .local v2, "pattern":Ljava/lang/String;
    sget-object v4, Lcom/ibm/icu/text/DateTimePatternGenerator;->CANONICAL_SET:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 635
    iput-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->skipMatcher:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    .line 636
    invoke-virtual {v0}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/ibm/icu/text/DateTimePatternGenerator;->getBestPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 637
    .local v3, "trial":Ljava/lang/String;
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 638
    invoke-interface {p1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 658
    .end local v0    # "cur":Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
    .end local v1    # "it":Ljava/util/Iterator;
    .end local v2    # "pattern":Ljava/lang/String;
    .end local v3    # "trial":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 657
    .restart local v1    # "it":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object p1
.end method

.method public getSkeleton(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 462
    monitor-enter p0

    .line 463
    :try_start_0
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    iget-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-virtual {v0, p1, v1}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->set(Ljava/lang/String;Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;)Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    .line 464
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    invoke-virtual {v0}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 465
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getSkeletons(Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .param p1, "result"    # Ljava/util/Map;

    .prologue
    .line 504
    if-nez p1, :cond_0

    .line 511
    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->skeleton2pattern:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 512
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    .line 513
    .local v1, "item":Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->skeleton2pattern:Ljava/util/TreeMap;

    invoke-virtual {v3, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 514
    .local v2, "pattern":Ljava/lang/String;
    sget-object v3, Lcom/ibm/icu/text/DateTimePatternGenerator;->CANONICAL_SET:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 515
    invoke-virtual {v1}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 517
    .end local v1    # "item":Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
    .end local v2    # "pattern":Ljava/lang/String;
    :cond_2
    return-object p1
.end method

.method public isFrozen()Z
    .locals 1

    .prologue
    .line 860
    iget-boolean v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->frozen:Z

    return v0
.end method

.method public replaceFieldTypes(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "skeleton"    # Ljava/lang/String;

    .prologue
    .line 545
    monitor-enter p0

    .line 546
    :try_start_0
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->current:Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    iget-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->fp:Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    invoke-virtual {v0, p2, v1}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->set(Ljava/lang/String;Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;)Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/ibm/icu/text/DateTimePatternGenerator;->adjustFieldTypes(Ljava/lang/String;Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;Z)Ljava/lang/String;

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 547
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setAppendItemFormat(ILjava/lang/String;)V
    .locals 1
    .param p1, "field"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 769
    invoke-direct {p0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->checkFrozen()V

    .line 770
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->appendItemFormats:[Ljava/lang/String;

    aput-object p2, v0, p1

    .line 771
    return-void
.end method

.method public setAppendItemName(ILjava/lang/String;)V
    .locals 1
    .param p1, "field"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 797
    invoke-direct {p0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->checkFrozen()V

    .line 798
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->appendItemNames:[Ljava/lang/String;

    aput-object p2, v0, p1

    .line 799
    return-void
.end method

.method public setDateTimeFormat(Ljava/lang/String;)V
    .locals 0
    .param p1, "dateTimeFormat"    # Ljava/lang/String;

    .prologue
    .line 570
    invoke-direct {p0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->checkFrozen()V

    .line 571
    iput-object p1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->dateTimeFormat:Ljava/lang/String;

    .line 572
    return-void
.end method

.method public setDecimal(Ljava/lang/String;)V
    .locals 0
    .param p1, "decimal"    # Ljava/lang/String;

    .prologue
    .line 596
    invoke-direct {p0}, Lcom/ibm/icu/text/DateTimePatternGenerator;->checkFrozen()V

    .line 597
    iput-object p1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator;->decimal:Ljava/lang/String;

    .line 598
    return-void
.end method
