.class public Lcom/ibm/icu/text/TimeUnitFormat;
.super Lcom/ibm/icu/text/MeasureFormat;
.source "TimeUnitFormat.java"


# static fields
.field private static final DEFAULT_PATTERN_FOR_DAY:Ljava/lang/String; = "{0} d"

.field private static final DEFAULT_PATTERN_FOR_HOUR:Ljava/lang/String; = "{0} h"

.field private static final DEFAULT_PATTERN_FOR_MINUTE:Ljava/lang/String; = "{0} min"

.field private static final DEFAULT_PATTERN_FOR_MONTH:Ljava/lang/String; = "{0} m"

.field private static final DEFAULT_PATTERN_FOR_SECOND:Ljava/lang/String; = "{0} s"

.field private static final DEFAULT_PATTERN_FOR_WEEK:Ljava/lang/String; = "{0} w"

.field private static final DEFAULT_PATTERN_FOR_YEAR:Ljava/lang/String; = "{0} y"

.field private static final serialVersionUID:J = -0x3374a809c751fb09L


# instance fields
.field private format:Lcom/ibm/icu/text/NumberFormat;

.field private transient isReady:Z

.field private locale:Lcom/ibm/icu/util/ULocale;

.field private transient pluralRules:Lcom/ibm/icu/text/PluralRules;

.field private transient timeUnitToCountToPatterns:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/ibm/icu/text/MeasureFormat;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/util/ULocale;)V
    .locals 1
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/ibm/icu/text/MeasureFormat;-><init>()V

    .line 90
    iput-object p1, p0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/TimeUnitFormat;->isReady:Z

    .line 92
    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/ibm/icu/text/MeasureFormat;-><init>()V

    .line 100
    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/TimeUnitFormat;->isReady:Z

    .line 102
    return-void
.end method

.method private searchInTree(Lcom/ibm/icu/util/TimeUnit;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 20
    .param p1, "timeUnit"    # Lcom/ibm/icu/util/TimeUnit;
    .param p2, "srcPluralCount"    # Ljava/lang/String;
    .param p3, "searchPluralCount"    # Ljava/lang/String;
    .param p4, "countToPatterns"    # Ljava/util/Map;

    .prologue
    .line 375
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 376
    .local v10, "parentLocale":Lcom/ibm/icu/util/ULocale;
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/util/TimeUnit;->toString()Ljava/lang/String;

    move-result-object v15

    .line 377
    .local v15, "srcTimeUnitName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 380
    .local v6, "found":Z
    :goto_0
    if-eqz v10, :cond_4

    .line 381
    :try_start_0
    const-string/jumbo v18, "com/ibm/icu/impl/data/icudt40b"

    move-object/from16 v0, v18

    invoke-static {v0, v10}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v17

    check-cast v17, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 382
    .local v17, "unitsRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v18, "units"

    invoke-virtual/range {v17 .. v18}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v17

    .line 383
    invoke-virtual/range {v17 .. v17}, Lcom/ibm/icu/impl/ICUResourceBundle;->getSize()I

    move-result v14

    .line 384
    .local v14, "size":I
    const/4 v7, 0x0

    .local v7, "index":I
    :goto_1
    if-ge v7, v14, :cond_3

    .line 385
    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v16

    .line 386
    .local v16, "timeUnitName":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 384
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 389
    :cond_0
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v9

    .line 390
    .local v9, "oneUnitRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {v9}, Lcom/ibm/icu/impl/ICUResourceBundle;->getSize()I

    move-result v5

    .line 391
    .local v5, "count":I
    const/4 v13, 0x0

    .local v13, "pluralIndex":I
    :goto_2
    if-ge v13, v5, :cond_3

    .line 393
    invoke-virtual {v9, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v12

    .line 394
    .local v12, "pluralCount":Ljava/lang/String;
    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 392
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 397
    :cond_1
    invoke-virtual {v9, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/ibm/icu/util/UResourceBundle;->getString()Ljava/lang/String;

    move-result-object v11

    .line 398
    .local v11, "pattern":Ljava/lang/String;
    new-instance v8, Lcom/ibm/icu/text/MessageFormat;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v8, v11, v0}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 399
    .local v8, "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->format:Lcom/ibm/icu/text/NumberFormat;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2

    .line 400
    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->format:Lcom/ibm/icu/text/NumberFormat;

    move-object/from16 v19, v0

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Lcom/ibm/icu/text/MessageFormat;->setFormatByArgumentIndex(ILjava/text/Format;)V

    .line 402
    :cond_2
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    const/4 v6, 0x1

    .line 409
    .end local v5    # "count":I
    .end local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    .end local v9    # "oneUnitRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v11    # "pattern":Ljava/lang/String;
    .end local v12    # "pluralCount":Ljava/lang/String;
    .end local v13    # "pluralIndex":I
    .end local v16    # "timeUnitName":Ljava/lang/String;
    :cond_3
    if-eqz v6, :cond_5

    .line 416
    .end local v7    # "index":I
    .end local v14    # "size":I
    .end local v17    # "unitsRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_4
    :goto_3
    if-eqz v6, :cond_6

    .line 447
    :goto_4
    return-void

    .line 412
    .restart local v7    # "index":I
    .restart local v14    # "size":I
    .restart local v17    # "unitsRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_5
    invoke-virtual {v10}, Lcom/ibm/icu/util/ULocale;->getFallback()Lcom/ibm/icu/util/ULocale;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 413
    goto/16 :goto_0

    .line 421
    .end local v7    # "index":I
    .end local v14    # "size":I
    .end local v17    # "unitsRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_6
    const-string/jumbo v18, "other"

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_f

    .line 423
    const/4 v8, 0x0

    .line 424
    .restart local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    sget-object v18, Lcom/ibm/icu/util/TimeUnit;->SECOND:Lcom/ibm/icu/util/TimeUnit;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_9

    .line 425
    new-instance v8, Lcom/ibm/icu/text/MessageFormat;

    .end local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    const-string/jumbo v18, "{0} s"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v8, v0, v1}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 439
    .restart local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    :cond_7
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->format:Lcom/ibm/icu/text/NumberFormat;

    move-object/from16 v18, v0

    if-eqz v18, :cond_8

    if-eqz v8, :cond_8

    .line 440
    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->format:Lcom/ibm/icu/text/NumberFormat;

    move-object/from16 v19, v0

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Lcom/ibm/icu/text/MessageFormat;->setFormatByArgumentIndex(ILjava/text/Format;)V

    .line 442
    :cond_8
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 426
    :cond_9
    sget-object v18, Lcom/ibm/icu/util/TimeUnit;->MINUTE:Lcom/ibm/icu/util/TimeUnit;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_a

    .line 427
    new-instance v8, Lcom/ibm/icu/text/MessageFormat;

    .end local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    const-string/jumbo v18, "{0} min"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v8, v0, v1}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 428
    .restart local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    goto :goto_5

    :cond_a
    sget-object v18, Lcom/ibm/icu/util/TimeUnit;->HOUR:Lcom/ibm/icu/util/TimeUnit;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_b

    .line 429
    new-instance v8, Lcom/ibm/icu/text/MessageFormat;

    .end local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    const-string/jumbo v18, "{0} h"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v8, v0, v1}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 430
    .restart local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    goto :goto_5

    :cond_b
    sget-object v18, Lcom/ibm/icu/util/TimeUnit;->WEEK:Lcom/ibm/icu/util/TimeUnit;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_c

    .line 431
    new-instance v8, Lcom/ibm/icu/text/MessageFormat;

    .end local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    const-string/jumbo v18, "{0} w"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v8, v0, v1}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 432
    .restart local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    goto :goto_5

    :cond_c
    sget-object v18, Lcom/ibm/icu/util/TimeUnit;->DAY:Lcom/ibm/icu/util/TimeUnit;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_d

    .line 433
    new-instance v8, Lcom/ibm/icu/text/MessageFormat;

    .end local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    const-string/jumbo v18, "{0} d"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v8, v0, v1}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 434
    .restart local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    goto/16 :goto_5

    :cond_d
    sget-object v18, Lcom/ibm/icu/util/TimeUnit;->MONTH:Lcom/ibm/icu/util/TimeUnit;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_e

    .line 435
    new-instance v8, Lcom/ibm/icu/text/MessageFormat;

    .end local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    const-string/jumbo v18, "{0} m"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v8, v0, v1}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 436
    .restart local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    goto/16 :goto_5

    :cond_e
    sget-object v18, Lcom/ibm/icu/util/TimeUnit;->YEAR:Lcom/ibm/icu/util/TimeUnit;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_7

    .line 437
    new-instance v8, Lcom/ibm/icu/text/MessageFormat;

    .end local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    const-string/jumbo v18, "{0} y"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v8, v0, v1}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .restart local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    goto/16 :goto_5

    .line 445
    .end local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    :cond_f
    const-string/jumbo v18, "other"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v18

    move-object/from16 v4, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/TimeUnitFormat;->searchInTree(Lcom/ibm/icu/util/TimeUnit;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_4

    .line 414
    :catch_0
    move-exception v18

    goto/16 :goto_3
.end method

.method private setup()V
    .locals 22

    .prologue
    .line 268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v20, v0

    if-nez v20, :cond_0

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->format:Lcom/ibm/icu/text/NumberFormat;

    move-object/from16 v20, v0

    if-eqz v20, :cond_3

    .line 270
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->format:Lcom/ibm/icu/text/NumberFormat;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/ibm/icu/text/NumberFormat;->getLocale(Lcom/ibm/icu/util/ULocale$Type;)Lcom/ibm/icu/util/ULocale;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 275
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->format:Lcom/ibm/icu/text/NumberFormat;

    move-object/from16 v20, v0

    if-nez v20, :cond_1

    .line 276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/ibm/icu/text/NumberFormat;->getNumberInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TimeUnitFormat;->format:Lcom/ibm/icu/text/NumberFormat;

    .line 278
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/ibm/icu/text/PluralRules;->forLocale(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/PluralRules;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TimeUnitFormat;->pluralRules:Lcom/ibm/icu/text/PluralRules;

    .line 279
    new-instance v20, Ljava/util/HashMap;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    .line 283
    :try_start_0
    const-string/jumbo v20, "com/ibm/icu/impl/data/icudt40b"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v14

    check-cast v14, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 284
    .local v14, "resource":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v20, "units"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v19

    .line 285
    .local v19, "unitsRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/impl/ICUResourceBundle;->getSize()I

    move-result v15

    .line 286
    .local v15, "size":I
    const/4 v5, 0x0

    .local v5, "index":I
    :goto_1
    if-ge v5, v15, :cond_7

    .line 287
    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v17

    .line 288
    .local v17, "timeUnitName":Ljava/lang/String;
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v9

    .line 289
    .local v9, "oneUnitRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {v9}, Lcom/ibm/icu/impl/ICUResourceBundle;->getSize()I

    move-result v2

    .line 290
    .local v2, "count":I
    new-instance v3, Ljava/util/TreeMap;

    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    .line 291
    .local v3, "countToPatterns":Ljava/util/Map;
    const/4 v12, 0x0

    .local v12, "pluralIndex":I
    :goto_2
    if-ge v12, v2, :cond_4

    .line 292
    invoke-virtual {v9, v12}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v11

    .line 293
    .local v11, "pluralCount":Ljava/lang/String;
    invoke-virtual {v9, v12}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/ibm/icu/util/UResourceBundle;->getString()Ljava/lang/String;

    move-result-object v10

    .line 294
    .local v10, "pattern":Ljava/lang/String;
    new-instance v8, Lcom/ibm/icu/text/MessageFormat;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v8, v10, v0}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 295
    .local v8, "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->format:Lcom/ibm/icu/text/NumberFormat;

    move-object/from16 v20, v0

    if-eqz v20, :cond_2

    .line 296
    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->format:Lcom/ibm/icu/text/NumberFormat;

    move-object/from16 v21, v0

    move/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/ibm/icu/text/MessageFormat;->setFormatByArgumentIndex(ILjava/text/Format;)V

    .line 298
    :cond_2
    invoke-interface {v3, v11, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 291
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 272
    .end local v2    # "count":I
    .end local v3    # "countToPatterns":Ljava/util/Map;
    .end local v5    # "index":I
    .end local v8    # "messageFormat":Lcom/ibm/icu/text/MessageFormat;
    .end local v9    # "oneUnitRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v10    # "pattern":Ljava/lang/String;
    .end local v11    # "pluralCount":Ljava/lang/String;
    .end local v12    # "pluralIndex":I
    .end local v14    # "resource":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v15    # "size":I
    .end local v17    # "timeUnitName":Ljava/lang/String;
    .end local v19    # "unitsRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_3
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    goto/16 :goto_0

    .line 300
    .restart local v2    # "count":I
    .restart local v3    # "countToPatterns":Ljava/util/Map;
    .restart local v5    # "index":I
    .restart local v9    # "oneUnitRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v12    # "pluralIndex":I
    .restart local v14    # "resource":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v15    # "size":I
    .restart local v17    # "timeUnitName":Ljava/lang/String;
    .restart local v19    # "unitsRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_4
    :try_start_1
    const-string/jumbo v20, "year"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    move-object/from16 v20, v0

    sget-object v21, Lcom/ibm/icu/util/TimeUnit;->YEAR:Lcom/ibm/icu/util/TimeUnit;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    :cond_5
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 302
    :cond_6
    const-string/jumbo v20, "month"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    move-object/from16 v20, v0

    sget-object v21, Lcom/ibm/icu/util/TimeUnit;->MONTH:Lcom/ibm/icu/util/TimeUnit;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 316
    .end local v2    # "count":I
    .end local v3    # "countToPatterns":Ljava/util/Map;
    .end local v5    # "index":I
    .end local v9    # "oneUnitRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v12    # "pluralIndex":I
    .end local v14    # "resource":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v15    # "size":I
    .end local v17    # "timeUnitName":Ljava/lang/String;
    .end local v19    # "unitsRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    :catch_0
    move-exception v20

    .line 337
    :cond_7
    invoke-static {}, Lcom/ibm/icu/util/TimeUnit;->values()[Lcom/ibm/icu/util/TimeUnit;

    move-result-object v18

    .line 338
    .local v18, "timeUnits":[Lcom/ibm/icu/util/TimeUnit;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->pluralRules:Lcom/ibm/icu/text/PluralRules;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/ibm/icu/text/PluralRules;->getKeywords()Ljava/util/Set;

    move-result-object v7

    .line 339
    .local v7, "keywords":Ljava/util/Set;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_4
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v4, v0, :cond_11

    .line 342
    aget-object v16, v18, v4

    .line 343
    .local v16, "timeUnit":Lcom/ibm/icu/util/TimeUnit;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 344
    .restart local v3    # "countToPatterns":Ljava/util/Map;
    const/4 v13, 0x0

    .line 345
    .local v13, "previousEmpty":Z
    if-nez v3, :cond_8

    .line 346
    new-instance v3, Ljava/util/TreeMap;

    .end local v3    # "countToPatterns":Ljava/util/Map;
    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    .line 347
    .restart local v3    # "countToPatterns":Ljava/util/Map;
    const/4 v13, 0x1

    .line 349
    :cond_8
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "it":Ljava/util/Iterator;
    :cond_9
    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_f

    .line 350
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 351
    .restart local v11    # "pluralCount":Ljava/lang/String;
    invoke-interface {v3, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_9

    .line 353
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v11, v11, v3}, Lcom/ibm/icu/text/TimeUnitFormat;->searchInTree(Lcom/ibm/icu/util/TimeUnit;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_5

    .line 304
    .end local v4    # "i":I
    .end local v6    # "it":Ljava/util/Iterator;
    .end local v7    # "keywords":Ljava/util/Set;
    .end local v11    # "pluralCount":Ljava/lang/String;
    .end local v13    # "previousEmpty":Z
    .end local v16    # "timeUnit":Lcom/ibm/icu/util/TimeUnit;
    .end local v18    # "timeUnits":[Lcom/ibm/icu/util/TimeUnit;
    .restart local v2    # "count":I
    .restart local v5    # "index":I
    .restart local v9    # "oneUnitRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v12    # "pluralIndex":I
    .restart local v14    # "resource":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v15    # "size":I
    .restart local v17    # "timeUnitName":Ljava/lang/String;
    .restart local v19    # "unitsRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_a
    :try_start_2
    const-string/jumbo v20, "day"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    move-object/from16 v20, v0

    sget-object v21, Lcom/ibm/icu/util/TimeUnit;->DAY:Lcom/ibm/icu/util/TimeUnit;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 306
    :cond_b
    const-string/jumbo v20, "hour"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    move-object/from16 v20, v0

    sget-object v21, Lcom/ibm/icu/util/TimeUnit;->HOUR:Lcom/ibm/icu/util/TimeUnit;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 308
    :cond_c
    const-string/jumbo v20, "minute"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    move-object/from16 v20, v0

    sget-object v21, Lcom/ibm/icu/util/TimeUnit;->MINUTE:Lcom/ibm/icu/util/TimeUnit;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 310
    :cond_d
    const-string/jumbo v20, "second"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    move-object/from16 v20, v0

    sget-object v21, Lcom/ibm/icu/util/TimeUnit;->SECOND:Lcom/ibm/icu/util/TimeUnit;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 312
    :cond_e
    const-string/jumbo v20, "week"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 313
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    move-object/from16 v20, v0

    sget-object v21, Lcom/ibm/icu/util/TimeUnit;->WEEK:Lcom/ibm/icu/util/TimeUnit;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/util/MissingResourceException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_3

    .line 356
    .end local v2    # "count":I
    .end local v5    # "index":I
    .end local v9    # "oneUnitRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v12    # "pluralIndex":I
    .end local v14    # "resource":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v15    # "size":I
    .end local v17    # "timeUnitName":Ljava/lang/String;
    .end local v19    # "unitsRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v4    # "i":I
    .restart local v6    # "it":Ljava/util/Iterator;
    .restart local v7    # "keywords":Ljava/util/Set;
    .restart local v13    # "previousEmpty":Z
    .restart local v16    # "timeUnit":Lcom/ibm/icu/util/TimeUnit;
    .restart local v18    # "timeUnits":[Lcom/ibm/icu/util/TimeUnit;
    :cond_f
    if-eqz v13, :cond_10

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    :cond_10
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_4

    .line 360
    .end local v3    # "countToPatterns":Ljava/util/Map;
    .end local v6    # "it":Ljava/util/Iterator;
    .end local v13    # "previousEmpty":Z
    .end local v16    # "timeUnit":Lcom/ibm/icu/util/TimeUnit;
    :cond_11
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/ibm/icu/text/TimeUnitFormat;->isReady:Z

    .line 361
    return-void
.end method


# virtual methods
.method public format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 9
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 168
    instance-of v6, p1, Lcom/ibm/icu/util/TimeUnitAmount;

    if-nez v6, :cond_0

    .line 169
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v7, "can not format non TimeUnitAmount object"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 171
    :cond_0
    iget-boolean v6, p0, Lcom/ibm/icu/text/TimeUnitFormat;->isReady:Z

    if-nez v6, :cond_1

    .line 172
    invoke-direct {p0}, Lcom/ibm/icu/text/TimeUnitFormat;->setup()V

    :cond_1
    move-object v0, p1

    .line 174
    check-cast v0, Lcom/ibm/icu/util/TimeUnitAmount;

    .line 175
    .local v0, "amount":Lcom/ibm/icu/util/TimeUnitAmount;
    iget-object v6, p0, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/ibm/icu/util/TimeUnitAmount;->getTimeUnit()Lcom/ibm/icu/util/TimeUnit;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 176
    .local v2, "countToPattern":Ljava/util/Map;
    invoke-virtual {v0}, Lcom/ibm/icu/util/TimeUnitAmount;->getNumber()Ljava/lang/Number;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v4

    .line 177
    .local v4, "number":D
    iget-object v6, p0, Lcom/ibm/icu/text/TimeUnitFormat;->pluralRules:Lcom/ibm/icu/text/PluralRules;

    invoke-virtual {v6, v4, v5}, Lcom/ibm/icu/text/PluralRules;->select(D)Ljava/lang/String;

    move-result-object v1

    .line 178
    .local v1, "count":Ljava/lang/String;
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/text/MessageFormat;

    .line 179
    .local v3, "pattern":Lcom/ibm/icu/text/MessageFormat;
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/ibm/icu/util/TimeUnitAmount;->getNumber()Ljava/lang/Number;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v6, p2, p3}, Lcom/ibm/icu/text/MessageFormat;->format([Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v6

    return-object v6
.end method

.method public parseObject(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Object;
    .locals 24
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;

    .prologue
    .line 189
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->isReady:Z

    move/from16 v20, v0

    if-nez v20, :cond_0

    .line 190
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/TimeUnitFormat;->setup()V

    .line 192
    :cond_0
    const/4 v15, 0x0

    .line 193
    .local v15, "resultNumber":Ljava/lang/Number;
    const/16 v16, 0x0

    .line 194
    .local v16, "resultTimeUnit":Lcom/ibm/icu/util/TimeUnit;
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v11

    .line 195
    .local v11, "oldPos":I
    const/4 v10, -0x1

    .line 196
    .local v10, "newPos":I
    const/4 v9, 0x0

    .line 197
    .local v9, "longestParseDistance":I
    const/4 v5, 0x0

    .line 201
    .local v5, "countOfLongestMatch":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "it":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_4

    .line 202
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/ibm/icu/util/TimeUnit;

    .line 203
    .local v19, "timeUnit":Lcom/ibm/icu/util/TimeUnit;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    .line 204
    .local v6, "countToPattern":Ljava/util/Map;
    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "it2":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_1

    .line 205
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 206
    .local v4, "count":Ljava/lang/String;
    invoke-interface {v6, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/ibm/icu/text/MessageFormat;

    .line 207
    .local v14, "pattern":Lcom/ibm/icu/text/MessageFormat;
    const/16 v20, -0x1

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 208
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 210
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v14, v0, v1}, Lcom/ibm/icu/text/MessageFormat;->parseObject(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Object;

    move-result-object v13

    .line 211
    .local v13, "parsed":Ljava/lang/Object;
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getErrorIndex()I

    move-result v20

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_2

    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v20

    move/from16 v0, v20

    if-eq v0, v11, :cond_2

    .line 215
    const/16 v18, 0x0

    .local v18, "temp":Ljava/lang/Number;
    move-object/from16 v20, v13

    .line 216
    check-cast v20, [Ljava/lang/Object;

    check-cast v20, [Ljava/lang/Object;

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    if-eqz v20, :cond_3

    .line 220
    check-cast v13, [Ljava/lang/Object;

    .end local v13    # "parsed":Ljava/lang/Object;
    check-cast v13, [Ljava/lang/Object;

    const/16 v20, 0x0

    aget-object v18, v13, v20

    .end local v18    # "temp":Ljava/lang/Number;
    check-cast v18, Ljava/lang/Number;

    .line 221
    .restart local v18    # "temp":Ljava/lang/Number;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TimeUnitFormat;->pluralRules:Lcom/ibm/icu/text/PluralRules;

    move-object/from16 v20, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v22

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/PluralRules;->select(D)Ljava/lang/String;

    move-result-object v17

    .line 222
    .local v17, "select":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 226
    .end local v17    # "select":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v20

    sub-int v12, v20, v11

    .line 227
    .local v12, "parseDistance":I
    if-le v12, v9, :cond_2

    .line 228
    move-object/from16 v15, v18

    .line 229
    move-object/from16 v16, v19

    .line 230
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v10

    .line 231
    move v9, v12

    .line 232
    move-object v5, v4

    goto/16 :goto_0

    .line 241
    .end local v4    # "count":Ljava/lang/String;
    .end local v6    # "countToPattern":Ljava/util/Map;
    .end local v8    # "it2":Ljava/util/Iterator;
    .end local v12    # "parseDistance":I
    .end local v14    # "pattern":Lcom/ibm/icu/text/MessageFormat;
    .end local v18    # "temp":Ljava/lang/Number;
    .end local v19    # "timeUnit":Lcom/ibm/icu/util/TimeUnit;
    :cond_4
    if-nez v15, :cond_5

    if-eqz v9, :cond_5

    .line 243
    const-string/jumbo v20, "zero"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 244
    new-instance v15, Ljava/lang/Integer;

    .end local v15    # "resultNumber":Ljava/lang/Number;
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-direct {v15, v0}, Ljava/lang/Integer;-><init>(I)V

    .line 255
    .restart local v15    # "resultNumber":Ljava/lang/Number;
    :cond_5
    :goto_1
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 256
    const/16 v20, -0x1

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 257
    new-instance v20, Lcom/ibm/icu/util/TimeUnitAmount;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lcom/ibm/icu/util/TimeUnitAmount;-><init>(Ljava/lang/Number;Lcom/ibm/icu/util/TimeUnit;)V

    return-object v20

    .line 245
    :cond_6
    const-string/jumbo v20, "one"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 246
    new-instance v15, Ljava/lang/Integer;

    .end local v15    # "resultNumber":Ljava/lang/Number;
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-direct {v15, v0}, Ljava/lang/Integer;-><init>(I)V

    .line 247
    .restart local v15    # "resultNumber":Ljava/lang/Number;
    goto :goto_1

    :cond_7
    const-string/jumbo v20, "two"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 248
    new-instance v15, Ljava/lang/Integer;

    .end local v15    # "resultNumber":Ljava/lang/Number;
    const/16 v20, 0x2

    move/from16 v0, v20

    invoke-direct {v15, v0}, Ljava/lang/Integer;-><init>(I)V

    .line 249
    .restart local v15    # "resultNumber":Ljava/lang/Number;
    goto :goto_1

    .line 252
    :cond_8
    new-instance v15, Ljava/lang/Integer;

    .end local v15    # "resultNumber":Ljava/lang/Number;
    const/16 v20, 0x3

    move/from16 v0, v20

    invoke-direct {v15, v0}, Ljava/lang/Integer;-><init>(I)V

    .restart local v15    # "resultNumber":Ljava/lang/Number;
    goto :goto_1
.end method

.method public setLocale(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/TimeUnitFormat;
    .locals 1
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/TimeUnitFormat;->isReady:Z

    .line 113
    return-object p0
.end method

.method public setLocale(Ljava/util/Locale;)Lcom/ibm/icu/text/TimeUnitFormat;
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 123
    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 124
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/TimeUnitFormat;->isReady:Z

    .line 125
    return-object p0
.end method

.method public setNumberFormat(Lcom/ibm/icu/text/NumberFormat;)Lcom/ibm/icu/text/TimeUnitFormat;
    .locals 8
    .param p1, "format"    # Lcom/ibm/icu/text/NumberFormat;

    .prologue
    const/4 v7, 0x0

    .line 135
    if-nez p1, :cond_3

    .line 136
    iget-object v6, p0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    if-nez v6, :cond_1

    .line 137
    iput-boolean v7, p0, Lcom/ibm/icu/text/TimeUnitFormat;->isReady:Z

    .line 156
    :cond_0
    return-object p0

    .line 140
    :cond_1
    iget-object v6, p0, Lcom/ibm/icu/text/TimeUnitFormat;->locale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v6}, Lcom/ibm/icu/text/NumberFormat;->getNumberInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v6

    iput-object v6, p0, Lcom/ibm/icu/text/TimeUnitFormat;->format:Lcom/ibm/icu/text/NumberFormat;

    .line 146
    :goto_0
    iget-object v6, p0, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 147
    .local v2, "it":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 148
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ibm/icu/util/TimeUnit;

    .line 149
    .local v5, "timeUnit":Lcom/ibm/icu/util/TimeUnit;
    iget-object v6, p0, Lcom/ibm/icu/text/TimeUnitFormat;->timeUnitToCountToPatterns:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 150
    .local v1, "countToPattern":Ljava/util/Map;
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it2":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 151
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 152
    .local v0, "count":Ljava/lang/String;
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/text/MessageFormat;

    .line 153
    .local v4, "pattern":Lcom/ibm/icu/text/MessageFormat;
    invoke-virtual {v4, v7, p1}, Lcom/ibm/icu/text/MessageFormat;->setFormatByArgumentIndex(ILjava/text/Format;)V

    goto :goto_1

    .line 143
    .end local v0    # "count":Ljava/lang/String;
    .end local v1    # "countToPattern":Ljava/util/Map;
    .end local v2    # "it":Ljava/util/Iterator;
    .end local v3    # "it2":Ljava/util/Iterator;
    .end local v4    # "pattern":Lcom/ibm/icu/text/MessageFormat;
    .end local v5    # "timeUnit":Lcom/ibm/icu/util/TimeUnit;
    :cond_3
    iput-object p1, p0, Lcom/ibm/icu/text/TimeUnitFormat;->format:Lcom/ibm/icu/text/NumberFormat;

    goto :goto_0
.end method
