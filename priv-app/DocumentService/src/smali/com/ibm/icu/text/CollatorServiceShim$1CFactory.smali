.class Lcom/ibm/icu/text/CollatorServiceShim$1CFactory;
.super Lcom/ibm/icu/impl/ICULocaleService$LocaleKeyFactory;
.source "CollatorServiceShim.java"


# instance fields
.field delegate:Lcom/ibm/icu/text/Collator$CollatorFactory;

.field private final this$0:Lcom/ibm/icu/text/CollatorServiceShim;


# direct methods
.method constructor <init>(Lcom/ibm/icu/text/CollatorServiceShim;Lcom/ibm/icu/text/Collator$CollatorFactory;)V
    .locals 1
    .param p2, "fctry"    # Lcom/ibm/icu/text/Collator$CollatorFactory;

    .prologue
    .line 56
    invoke-virtual {p2}, Lcom/ibm/icu/text/Collator$CollatorFactory;->visible()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKeyFactory;-><init>(Z)V

    .line 55
    iput-object p1, p0, Lcom/ibm/icu/text/CollatorServiceShim$1CFactory;->this$0:Lcom/ibm/icu/text/CollatorServiceShim;

    .line 57
    iput-object p2, p0, Lcom/ibm/icu/text/CollatorServiceShim$1CFactory;->delegate:Lcom/ibm/icu/text/Collator$CollatorFactory;

    .line 58
    return-void
.end method


# virtual methods
.method public getDisplayName(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "displayLocale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 66
    new-instance v0, Lcom/ibm/icu/util/ULocale;

    invoke-direct {v0, p1}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    .line 67
    .local v0, "objectLocale":Lcom/ibm/icu/util/ULocale;
    iget-object v1, p0, Lcom/ibm/icu/text/CollatorServiceShim$1CFactory;->delegate:Lcom/ibm/icu/text/Collator$CollatorFactory;

    invoke-virtual {v1, v0, p2}, Lcom/ibm/icu/text/Collator$CollatorFactory;->getDisplayName(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getSupportedIDs()Ljava/util/Set;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/ibm/icu/text/CollatorServiceShim$1CFactory;->delegate:Lcom/ibm/icu/text/Collator$CollatorFactory;

    invoke-virtual {v0}, Lcom/ibm/icu/text/Collator$CollatorFactory;->getSupportedLocaleIDs()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public handleCreate(Lcom/ibm/icu/util/ULocale;ILcom/ibm/icu/impl/ICUService;)Ljava/lang/Object;
    .locals 2
    .param p1, "loc"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "kind"    # I
    .param p3, "srvc"    # Lcom/ibm/icu/impl/ICUService;

    .prologue
    .line 61
    iget-object v1, p0, Lcom/ibm/icu/text/CollatorServiceShim$1CFactory;->delegate:Lcom/ibm/icu/text/Collator$CollatorFactory;

    invoke-virtual {v1, p1}, Lcom/ibm/icu/text/Collator$CollatorFactory;->createCollator(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/Collator;

    move-result-object v0

    .line 62
    .local v0, "coll":Lcom/ibm/icu/text/Collator;
    return-object v0
.end method
