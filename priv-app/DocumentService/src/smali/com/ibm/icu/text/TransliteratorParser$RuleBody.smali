.class abstract Lcom/ibm/icu/text/TransliteratorParser$RuleBody;
.super Ljava/lang/Object;
.source "TransliteratorParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/TransliteratorParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "RuleBody"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/text/TransliteratorParser$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/ibm/icu/text/TransliteratorParser$1;

    .prologue
    .line 279
    invoke-direct {p0}, Lcom/ibm/icu/text/TransliteratorParser$RuleBody;-><init>()V

    return-void
.end method


# virtual methods
.method abstract handleNextLine()Ljava/lang/String;
.end method

.method nextLine()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x5c

    .line 288
    invoke-virtual {p0}, Lcom/ibm/icu/text/TransliteratorParser$RuleBody;->handleNextLine()Ljava/lang/String;

    move-result-object v1

    .line 289
    .local v1, "s":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v2, v3, :cond_2

    .line 293
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 295
    .local v0, "b":Ljava/lang/StringBuffer;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 296
    invoke-virtual {p0}, Lcom/ibm/icu/text/TransliteratorParser$RuleBody;->handleNextLine()Ljava/lang/String;

    move-result-object v1

    .line 297
    if-nez v1, :cond_3

    .line 304
    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 306
    .end local v0    # "b":Ljava/lang/StringBuffer;
    :cond_2
    return-object v1

    .line 300
    .restart local v0    # "b":Ljava/lang/StringBuffer;
    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 302
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v2, v3, :cond_0

    goto :goto_0
.end method

.method abstract reset()V
.end method
