.class Lcom/ibm/icu/text/NullSubstitution;
.super Lcom/ibm/icu/text/NFSubstitution;
.source "NFSubstitution.java"


# direct methods
.method constructor <init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "ruleSet"    # Lcom/ibm/icu/text/NFRuleSet;
    .param p3, "formatter"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;
    .param p4, "description"    # Ljava/lang/String;

    .prologue
    .line 1738
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/ibm/icu/text/NFSubstitution;-><init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    .line 1739
    return-void
.end method


# virtual methods
.method public calcUpperBound(D)D
    .locals 2
    .param p1, "oldUpperBound"    # D

    .prologue
    .line 1824
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public composeRuleValue(DD)D
    .locals 2
    .param p1, "newRuleValue"    # D
    .param p3, "oldRuleValue"    # D

    .prologue
    .line 1815
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public doParse(Ljava/lang/String;Ljava/text/ParsePosition;DDZ)Ljava/lang/Number;
    .locals 5
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "parsePosition"    # Ljava/text/ParsePosition;
    .param p3, "baseValue"    # D
    .param p5, "upperBound"    # D
    .param p7, "lenientParse"    # Z

    .prologue
    .line 1803
    double-to-long v0, p3

    long-to-double v0, v0

    cmpl-double v0, p3, v0

    if-nez v0, :cond_0

    .line 1804
    new-instance v0, Ljava/lang/Long;

    double-to-long v2, p3

    invoke-direct {v0, v2, v3}, Ljava/lang/Long;-><init>(J)V

    .line 1806
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/Double;

    invoke-direct {v0, p3, p4}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0
.end method

.method public doSubstitution(DLjava/lang/StringBuffer;I)V
    .locals 0
    .param p1, "number"    # D
    .param p3, "toInsertInto"    # Ljava/lang/StringBuffer;
    .param p4, "position"    # I

    .prologue
    .line 1774
    return-void
.end method

.method public doSubstitution(JLjava/lang/StringBuffer;I)V
    .locals 0
    .param p1, "number"    # J
    .param p3, "toInsertInto"    # Ljava/lang/StringBuffer;
    .param p4, "position"    # I

    .prologue
    .line 1768
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "that"    # Ljava/lang/Object;

    .prologue
    .line 1749
    invoke-super {p0, p1}, Lcom/ibm/icu/text/NFSubstitution;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isNullSubstitution()Z
    .locals 1

    .prologue
    .line 1837
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1757
    const-string/jumbo v0, ""

    return-object v0
.end method

.method tokenChar()C
    .locals 1

    .prologue
    .line 1845
    const/16 v0, 0x20

    return v0
.end method

.method public transformNumber(D)D
    .locals 2
    .param p1, "number"    # D

    .prologue
    .line 1790
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public transformNumber(J)J
    .locals 2
    .param p1, "number"    # J

    .prologue
    .line 1781
    const-wide/16 v0, 0x0

    return-wide v0
.end method
