.class abstract Lcom/ibm/icu/text/NFSubstitution;
.super Ljava/lang/Object;
.source "NFSubstitution.java"


# instance fields
.field numberFormat:Lcom/ibm/icu/text/DecimalFormat;

.field pos:I

.field rbnf:Lcom/ibm/icu/text/RuleBasedNumberFormat;

.field ruleSet:Lcom/ibm/icu/text/NFRuleSet;


# direct methods
.method constructor <init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V
    .locals 4
    .param p1, "pos"    # I
    .param p2, "ruleSet"    # Lcom/ibm/icu/text/NFRuleSet;
    .param p3, "formatter"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;
    .param p4, "description"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v3, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    .line 41
    iput-object v3, p0, Lcom/ibm/icu/text/NFSubstitution;->numberFormat:Lcom/ibm/icu/text/DecimalFormat;

    .line 46
    iput-object v3, p0, Lcom/ibm/icu/text/NFSubstitution;->rbnf:Lcom/ibm/icu/text/RuleBasedNumberFormat;

    .line 165
    iput p1, p0, Lcom/ibm/icu/text/NFSubstitution;->pos:I

    .line 166
    iput-object p3, p0, Lcom/ibm/icu/text/NFSubstitution;->rbnf:Lcom/ibm/icu/text/RuleBasedNumberFormat;

    .line 172
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    invoke-virtual {p4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p4, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v0, v1, :cond_1

    .line 174
    const/4 v0, 0x1

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p4, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p4

    .line 183
    :cond_0
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 184
    iput-object p2, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    .line 217
    :goto_0
    return-void

    .line 176
    :cond_1
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Illegal substitution syntax"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_2
    invoke-virtual {p4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x25

    if-ne v0, v1, :cond_3

    .line 191
    invoke-virtual {p3, p4}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->findRuleSet(Ljava/lang/String;)Lcom/ibm/icu/text/NFRuleSet;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    goto :goto_0

    .line 198
    :cond_3
    invoke-virtual {p4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-eq v0, v1, :cond_4

    invoke-virtual {p4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x30

    if-ne v0, v1, :cond_5

    .line 199
    :cond_4
    new-instance v0, Lcom/ibm/icu/text/DecimalFormat;

    invoke-direct {v0, p4}, Lcom/ibm/icu/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/ibm/icu/text/NFSubstitution;->numberFormat:Lcom/ibm/icu/text/DecimalFormat;

    .line 200
    iget-object v0, p0, Lcom/ibm/icu/text/NFSubstitution;->numberFormat:Lcom/ibm/icu/text/DecimalFormat;

    invoke-virtual {p3}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->getDecimalFormatSymbols()Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setDecimalFormatSymbols(Lcom/ibm/icu/text/DecimalFormatSymbols;)V

    goto :goto_0

    .line 208
    :cond_5
    invoke-virtual {p4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x3e

    if-ne v0, v1, :cond_6

    .line 209
    iput-object p2, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    .line 210
    iput-object v3, p0, Lcom/ibm/icu/text/NFSubstitution;->numberFormat:Lcom/ibm/icu/text/DecimalFormat;

    goto :goto_0

    .line 215
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Illegal substitution syntax"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static makeSubstitution(ILcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)Lcom/ibm/icu/text/NFSubstitution;
    .locals 10
    .param p0, "pos"    # I
    .param p1, "rule"    # Lcom/ibm/icu/text/NFRule;
    .param p2, "rulePredecessor"    # Lcom/ibm/icu/text/NFRule;
    .param p3, "ruleSet"    # Lcom/ibm/icu/text/NFRuleSet;
    .param p4, "formatter"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;
    .param p5, "description"    # Ljava/lang/String;

    .prologue
    const-wide/16 v8, -0x1

    const-wide/16 v6, -0x2

    const-wide/16 v4, -0x3

    const-wide/16 v2, -0x4

    .line 77
    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lcom/ibm/icu/text/NullSubstitution;

    invoke-direct {v0, p0, p3, p4, p5}, Lcom/ibm/icu/text/NullSubstitution;-><init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    .line 142
    :goto_0
    return-object v0

    .line 81
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p5, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 146
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Illegal substitution character"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :pswitch_0
    invoke-virtual {p1}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-nez v0, :cond_1

    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "<< not allowed in negative-number rule"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_1
    invoke-virtual {p1}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 95
    :cond_2
    new-instance v0, Lcom/ibm/icu/text/IntegralPartSubstitution;

    invoke-direct {v0, p0, p3, p4, p5}, Lcom/ibm/icu/text/IntegralPartSubstitution;-><init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    goto :goto_0

    .line 100
    :cond_3
    invoke-virtual {p3}, Lcom/ibm/icu/text/NFRuleSet;->isFractionSet()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 101
    new-instance v0, Lcom/ibm/icu/text/NumeratorSubstitution;

    invoke-virtual {p1}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-virtual {p4}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->getDefaultRuleSet()Lcom/ibm/icu/text/NFRuleSet;

    move-result-object v4

    move v1, p0

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/NumeratorSubstitution;-><init>(IDLcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    goto :goto_0

    .line 107
    :cond_4
    new-instance v0, Lcom/ibm/icu/text/MultiplierSubstitution;

    invoke-virtual {p1}, Lcom/ibm/icu/text/NFRule;->getDivisor()D

    move-result-wide v2

    move v1, p0

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/MultiplierSubstitution;-><init>(IDLcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :pswitch_1
    invoke-virtual {p1}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-nez v0, :cond_5

    .line 116
    new-instance v0, Lcom/ibm/icu/text/AbsoluteValueSubstitution;

    invoke-direct {v0, p0, p3, p4, p5}, Lcom/ibm/icu/text/AbsoluteValueSubstitution;-><init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    goto :goto_0

    .line 121
    :cond_5
    invoke-virtual {p1}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_7

    .line 124
    :cond_6
    new-instance v0, Lcom/ibm/icu/text/FractionalPartSubstitution;

    invoke-direct {v0, p0, p3, p4, p5}, Lcom/ibm/icu/text/FractionalPartSubstitution;-><init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 129
    :cond_7
    invoke-virtual {p3}, Lcom/ibm/icu/text/NFRuleSet;->isFractionSet()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 130
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, ">> not allowed in fraction rule set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_8
    new-instance v0, Lcom/ibm/icu/text/ModulusSubstitution;

    invoke-virtual {p1}, Lcom/ibm/icu/text/NFRule;->getDivisor()D

    move-result-wide v2

    move v1, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/ibm/icu/text/ModulusSubstitution;-><init>(IDLcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 142
    :pswitch_2
    new-instance v0, Lcom/ibm/icu/text/SameValueSubstitution;

    invoke-direct {v0, p0, p3, p4, p5}, Lcom/ibm/icu/text/SameValueSubstitution;-><init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x3c
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public abstract calcUpperBound(D)D
.end method

.method public abstract composeRuleValue(DD)D
.end method

.method public doParse(Ljava/lang/String;Ljava/text/ParsePosition;DDZ)Ljava/lang/Number;
    .locals 7
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "parsePosition"    # Ljava/text/ParsePosition;
    .param p3, "baseValue"    # D
    .param p5, "upperBound"    # D
    .param p7, "lenientParse"    # Z

    .prologue
    .line 403
    invoke-virtual {p0, p5, p6}, Lcom/ibm/icu/text/NFSubstitution;->calcUpperBound(D)D

    move-result-wide p5

    .line 411
    iget-object v3, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    if-eqz v3, :cond_2

    .line 412
    iget-object v3, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    invoke-virtual {v3, p1, p2, p5, p6}, Lcom/ibm/icu/text/NFRuleSet;->parse(Ljava/lang/String;Ljava/text/ParsePosition;D)Ljava/lang/Number;

    move-result-object v2

    .line 413
    .local v2, "tempResult":Ljava/lang/Number;
    if-eqz p7, :cond_0

    iget-object v3, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    invoke-virtual {v3}, Lcom/ibm/icu/text/NFRuleSet;->isFractionSet()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v3

    if-nez v3, :cond_0

    .line 414
    iget-object v3, p0, Lcom/ibm/icu/text/NFSubstitution;->rbnf:Lcom/ibm/icu/text/RuleBasedNumberFormat;

    invoke-virtual {v3}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->getDecimalFormat()Lcom/ibm/icu/text/DecimalFormat;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Lcom/ibm/icu/text/DecimalFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v2

    .line 426
    :cond_0
    :goto_0
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v3

    if-eqz v3, :cond_1

    .line 427
    invoke-virtual {v2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    .line 451
    .local v0, "result":D
    invoke-virtual {p0, v0, v1, p3, p4}, Lcom/ibm/icu/text/NFSubstitution;->composeRuleValue(DD)D

    move-result-wide v0

    .line 452
    double-to-long v4, v0

    long-to-double v4, v4

    cmpl-double v3, v0, v4

    if-nez v3, :cond_3

    .line 453
    new-instance v2, Ljava/lang/Long;

    .end local v2    # "tempResult":Ljava/lang/Number;
    double-to-long v4, v0

    invoke-direct {v2, v4, v5}, Ljava/lang/Long;-><init>(J)V

    .line 460
    .end local v0    # "result":D
    :cond_1
    :goto_1
    return-object v2

    .line 419
    :cond_2
    iget-object v3, p0, Lcom/ibm/icu/text/NFSubstitution;->numberFormat:Lcom/ibm/icu/text/DecimalFormat;

    invoke-virtual {v3, p1, p2}, Lcom/ibm/icu/text/DecimalFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v2

    .restart local v2    # "tempResult":Ljava/lang/Number;
    goto :goto_0

    .line 455
    .restart local v0    # "result":D
    :cond_3
    new-instance v2, Ljava/lang/Double;

    .end local v2    # "tempResult":Ljava/lang/Number;
    invoke-direct {v2, v0, v1}, Ljava/lang/Double;-><init>(D)V

    goto :goto_1
.end method

.method public doSubstitution(DLjava/lang/StringBuffer;I)V
    .locals 7
    .param p1, "number"    # D
    .param p3, "toInsertInto"    # Ljava/lang/StringBuffer;
    .param p4, "position"    # I

    .prologue
    .line 319
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/NFSubstitution;->transformNumber(D)D

    move-result-wide v0

    .line 323
    .local v0, "numberToFormat":D
    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    cmpl-double v2, v0, v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    if-eqz v2, :cond_0

    .line 324
    iget-object v2, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    double-to-long v4, v0

    iget v3, p0, Lcom/ibm/icu/text/NFSubstitution;->pos:I

    add-int/2addr v3, p4

    invoke-virtual {v2, v4, v5, p3, v3}, Lcom/ibm/icu/text/NFRuleSet;->format(JLjava/lang/StringBuffer;I)V

    .line 336
    :goto_0
    return-void

    .line 330
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    if-eqz v2, :cond_1

    .line 331
    iget-object v2, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    iget v3, p0, Lcom/ibm/icu/text/NFSubstitution;->pos:I

    add-int/2addr v3, p4

    invoke-virtual {v2, v0, v1, p3, v3}, Lcom/ibm/icu/text/NFRuleSet;->format(DLjava/lang/StringBuffer;I)V

    goto :goto_0

    .line 333
    :cond_1
    iget v2, p0, Lcom/ibm/icu/text/NFSubstitution;->pos:I

    add-int/2addr v2, p4

    iget-object v3, p0, Lcom/ibm/icu/text/NFSubstitution;->numberFormat:Lcom/ibm/icu/text/DecimalFormat;

    invoke-virtual {v3, v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public doSubstitution(JLjava/lang/StringBuffer;I)V
    .locals 5
    .param p1, "number"    # J
    .param p3, "toInsertInto"    # Ljava/lang/StringBuffer;
    .param p4, "position"    # I

    .prologue
    .line 285
    iget-object v2, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    if-eqz v2, :cond_0

    .line 289
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/NFSubstitution;->transformNumber(J)J

    move-result-wide v0

    .line 291
    .local v0, "numberToFormat":J
    iget-object v2, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    iget v3, p0, Lcom/ibm/icu/text/NFSubstitution;->pos:I

    add-int/2addr v3, p4

    invoke-virtual {v2, v0, v1, p3, v3}, Lcom/ibm/icu/text/NFRuleSet;->format(JLjava/lang/StringBuffer;I)V

    .line 304
    .end local v0    # "numberToFormat":J
    :goto_0
    return-void

    .line 297
    :cond_0
    long-to-double v2, p1

    invoke-virtual {p0, v2, v3}, Lcom/ibm/icu/text/NFSubstitution;->transformNumber(D)D

    move-result-wide v0

    .line 298
    .local v0, "numberToFormat":D
    iget-object v2, p0, Lcom/ibm/icu/text/NFSubstitution;->numberFormat:Lcom/ibm/icu/text/DecimalFormat;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DecimalFormat;->getMaximumFractionDigits()I

    move-result v2

    if-nez v2, :cond_1

    .line 299
    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    .line 302
    :cond_1
    iget v2, p0, Lcom/ibm/icu/text/NFSubstitution;->pos:I

    add-int/2addr v2, p4

    iget-object v3, p0, Lcom/ibm/icu/text/NFSubstitution;->numberFormat:Lcom/ibm/icu/text/DecimalFormat;

    invoke-virtual {v3, v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v2, v3}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "that"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 242
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_1

    move-object v0, p1

    .line 243
    check-cast v0, Lcom/ibm/icu/text/NFSubstitution;

    .line 245
    .local v0, "that2":Lcom/ibm/icu/text/NFSubstitution;
    iget v2, p0, Lcom/ibm/icu/text/NFSubstitution;->pos:I

    iget v3, v0, Lcom/ibm/icu/text/NFSubstitution;->pos:I

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/NFSubstitution;->numberFormat:Lcom/ibm/icu/text/DecimalFormat;

    if-nez v2, :cond_2

    iget-object v2, v0, Lcom/ibm/icu/text/NFSubstitution;->numberFormat:Lcom/ibm/icu/text/DecimalFormat;

    if-nez v2, :cond_1

    :goto_0
    const/4 v1, 0x1

    .line 249
    .end local v0    # "that2":Lcom/ibm/icu/text/NFSubstitution;
    :cond_1
    return v1

    .line 245
    .restart local v0    # "that2":Lcom/ibm/icu/text/NFSubstitution;
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/NFSubstitution;->numberFormat:Lcom/ibm/icu/text/DecimalFormat;

    iget-object v3, v0, Lcom/ibm/icu/text/NFSubstitution;->numberFormat:Lcom/ibm/icu/text/DecimalFormat;

    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/DecimalFormat;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0
.end method

.method public final getPos()I
    .locals 1

    .prologue
    .line 496
    iget v0, p0, Lcom/ibm/icu/text/NFSubstitution;->pos:I

    return v0
.end method

.method public isModulusSubstitution()Z
    .locals 1

    .prologue
    .line 523
    const/4 v0, 0x0

    return v0
.end method

.method public isNullSubstitution()Z
    .locals 1

    .prologue
    .line 513
    const/4 v0, 0x0

    return v0
.end method

.method public setDivisor(II)V
    .locals 0
    .param p1, "radix"    # I
    .param p2, "exponent"    # I

    .prologue
    .line 228
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    if-eqz v0, :cond_0

    .line 264
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lcom/ibm/icu/text/NFSubstitution;->tokenChar()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/ibm/icu/text/NFSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    invoke-virtual {v1}, Lcom/ibm/icu/text/NFRuleSet;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/ibm/icu/text/NFSubstitution;->tokenChar()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 266
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lcom/ibm/icu/text/NFSubstitution;->tokenChar()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/ibm/icu/text/NFSubstitution;->numberFormat:Lcom/ibm/icu/text/DecimalFormat;

    invoke-virtual {v1}, Lcom/ibm/icu/text/DecimalFormat;->toPattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/ibm/icu/text/NFSubstitution;->tokenChar()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method abstract tokenChar()C
.end method

.method public abstract transformNumber(D)D
.end method

.method public abstract transformNumber(J)J
.end method
