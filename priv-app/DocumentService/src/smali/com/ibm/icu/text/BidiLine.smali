.class final Lcom/ibm/icu/text/BidiLine;
.super Ljava/lang/Object;
.source "BidiLine.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getLevelAt(Lcom/ibm/icu/text/Bidi;I)B
    .locals 2
    .param p0, "bidi"    # Lcom/ibm/icu/text/Bidi;
    .param p1, "charIndex"    # I

    .prologue
    .line 240
    iget-byte v0, p0, Lcom/ibm/icu/text/Bidi;->direction:B

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    if-lt p1, v0, :cond_1

    .line 241
    :cond_0
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v0

    .line 243
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aget-byte v0, v0, p1

    goto :goto_0
.end method

.method static getLevels(Lcom/ibm/icu/text/Bidi;)[B
    .locals 6
    .param p0, "bidi"    # Lcom/ibm/icu/text/Bidi;

    .prologue
    const/4 v5, 0x0

    .line 249
    iget v2, p0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    .line 250
    .local v2, "start":I
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->length:I

    .line 252
    .local v0, "length":I
    if-eq v2, v0, :cond_0

    .line 263
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    iget-byte v4, p0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    invoke-static {v3, v2, v0, v4}, Ljava/util/Arrays;->fill([BIIB)V

    .line 266
    iput v0, p0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    .line 268
    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 269
    new-array v1, v0, [B

    .line 270
    .local v1, "levels":[B
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    invoke-static {v3, v5, v1, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 273
    .end local v1    # "levels":[B
    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    goto :goto_0
.end method

.method static getLogicalIndex(Lcom/ibm/icu/text/Bidi;I)I
    .locals 21
    .param p0, "bidi"    # Lcom/ibm/icu/text/Bidi;
    .param p1, "visualIndex"    # I

    .prologue
    .line 908
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    .line 909
    .local v15, "runs":[Lcom/ibm/icu/text/BidiRun;
    move-object/from16 v0, p0

    iget v14, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    .line 910
    .local v14, "runCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    move/from16 v19, v0

    if-lez v19, :cond_6

    .line 912
    const/4 v13, 0x0

    .line 913
    .local v13, "markFound":I
    const/16 v18, 0x0

    .line 915
    .local v18, "visualStart":I
    const/4 v5, 0x0

    .line 916
    .local v5, "i":I
    :goto_0
    aget-object v19, v15, v5

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v19, v0

    sub-int v9, v19, v18

    .line 917
    .local v9, "length":I
    aget-object v19, v15, v5

    move-object/from16 v0, v19

    iget v6, v0, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 918
    .local v6, "insertRemove":I
    and-int/lit8 v19, v6, 0x5

    if-lez v19, :cond_1

    .line 919
    add-int v19, v18, v13

    move/from16 v0, p1

    move/from16 v1, v19

    if-gt v0, v1, :cond_0

    .line 920
    const/16 v19, -0x1

    .line 1006
    .end local v6    # "insertRemove":I
    .end local v9    # "length":I
    .end local v13    # "markFound":I
    .end local v18    # "visualStart":I
    :goto_1
    return v19

    .line 922
    .restart local v6    # "insertRemove":I
    .restart local v9    # "length":I
    .restart local v13    # "markFound":I
    .restart local v18    # "visualStart":I
    :cond_0
    add-int/lit8 v13, v13, 0x1

    .line 925
    :cond_1
    aget-object v19, v15, v5

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v19, v0

    add-int v19, v19, v13

    move/from16 v0, p1

    move/from16 v1, v19

    if-ge v0, v1, :cond_3

    .line 926
    sub-int p1, p1, v13

    .line 976
    .end local v5    # "i":I
    .end local v6    # "insertRemove":I
    .end local v9    # "length":I
    .end local v13    # "markFound":I
    .end local v18    # "visualStart":I
    :cond_2
    :goto_2
    const/16 v19, 0xa

    move/from16 v0, v19

    if-gt v14, v0, :cond_d

    .line 978
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_3
    aget-object v19, v15, v5

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v19, v0

    move/from16 v0, p1

    move/from16 v1, v19

    if-lt v0, v1, :cond_f

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 929
    .restart local v6    # "insertRemove":I
    .restart local v9    # "length":I
    .restart local v13    # "markFound":I
    .restart local v18    # "visualStart":I
    :cond_3
    and-int/lit8 v19, v6, 0xa

    if-lez v19, :cond_5

    .line 930
    add-int v19, v18, v9

    add-int v19, v19, v13

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 931
    const/16 v19, -0x1

    goto :goto_1

    .line 933
    :cond_4
    add-int/lit8 v13, v13, 0x1

    .line 915
    :cond_5
    add-int/lit8 v5, v5, 0x1

    add-int v18, v18, v9

    goto :goto_0

    .line 937
    .end local v5    # "i":I
    .end local v6    # "insertRemove":I
    .end local v9    # "length":I
    .end local v13    # "markFound":I
    .end local v18    # "visualStart":I
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->controlCount:I

    move/from16 v19, v0

    if-lez v19, :cond_2

    .line 939
    const/4 v3, 0x0

    .line 940
    .local v3, "controlFound":I
    const/16 v18, 0x0

    .line 944
    .restart local v18    # "visualStart":I
    const/4 v5, 0x0

    .line 945
    .restart local v5    # "i":I
    :goto_4
    aget-object v19, v15, v5

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v19, v0

    sub-int v9, v19, v18

    .line 946
    .restart local v9    # "length":I
    aget-object v19, v15, v5

    move-object/from16 v0, v19

    iget v6, v0, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 948
    .restart local v6    # "insertRemove":I
    aget-object v19, v15, v5

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v19, v0

    sub-int v19, v19, v3

    add-int v19, v19, v6

    move/from16 v0, p1

    move/from16 v1, v19

    if-lt v0, v1, :cond_7

    .line 949
    sub-int/2addr v3, v6

    .line 944
    add-int/lit8 v5, v5, 0x1

    add-int v18, v18, v9

    goto :goto_4

    .line 953
    :cond_7
    if-nez v6, :cond_8

    .line 954
    add-int p1, p1, v3

    .line 955
    goto :goto_2

    .line 958
    :cond_8
    aget-object v19, v15, v5

    move-object/from16 v0, v19

    iget v12, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 959
    .local v12, "logicalStart":I
    aget-object v19, v15, v5

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/text/BidiRun;->isEvenRun()Z

    move-result v4

    .line 960
    .local v4, "evenRun":Z
    add-int v19, v12, v9

    add-int/lit8 v11, v19, -0x1

    .line 961
    .local v11, "logicalEnd":I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_5
    if-ge v7, v9, :cond_a

    .line 962
    if-eqz v4, :cond_b

    add-int v8, v12, v7

    .line 963
    .local v8, "k":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->text:[C

    move-object/from16 v19, v0

    aget-char v17, v19, v8

    .line 964
    .local v17, "uchar":C
    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/text/Bidi;->IsBidiControlChar(I)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 965
    add-int/lit8 v3, v3, 0x1

    .line 967
    :cond_9
    add-int v19, p1, v3

    add-int v20, v18, v7

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_c

    .line 971
    .end local v8    # "k":I
    .end local v17    # "uchar":C
    :cond_a
    add-int p1, p1, v3

    .line 972
    goto/16 :goto_2

    .line 962
    :cond_b
    sub-int v8, v11, v7

    goto :goto_6

    .line 961
    .restart local v8    # "k":I
    .restart local v17    # "uchar":C
    :cond_c
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 981
    .end local v3    # "controlFound":I
    .end local v4    # "evenRun":Z
    .end local v5    # "i":I
    .end local v6    # "insertRemove":I
    .end local v7    # "j":I
    .end local v8    # "k":I
    .end local v9    # "length":I
    .end local v11    # "logicalEnd":I
    .end local v12    # "logicalStart":I
    .end local v17    # "uchar":C
    .end local v18    # "visualStart":I
    :cond_d
    const/4 v2, 0x0

    .local v2, "begin":I
    move v10, v14

    .line 985
    .local v10, "limit":I
    :goto_7
    add-int v19, v2, v10

    div-int/lit8 v5, v19, 0x2

    .line 986
    .restart local v5    # "i":I
    aget-object v19, v15, v5

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v19, v0

    move/from16 v0, p1

    move/from16 v1, v19

    if-lt v0, v1, :cond_e

    .line 987
    add-int/lit8 v2, v5, 0x1

    .line 988
    goto :goto_7

    :cond_e
    if-eqz v5, :cond_f

    add-int/lit8 v19, v5, -0x1

    aget-object v19, v15, v19

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v19, v0

    move/from16 v0, p1

    move/from16 v1, v19

    if-lt v0, v1, :cond_11

    .line 996
    .end local v2    # "begin":I
    .end local v10    # "limit":I
    :cond_f
    aget-object v19, v15, v5

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    move/from16 v16, v0

    .line 997
    .local v16, "start":I
    aget-object v19, v15, v5

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/text/BidiRun;->isEvenRun()Z

    move-result v19

    if-eqz v19, :cond_12

    .line 1000
    if-lez v5, :cond_10

    .line 1001
    add-int/lit8 v19, v5, -0x1

    aget-object v19, v15, v19

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v19, v0

    sub-int p1, p1, v19

    .line 1003
    :cond_10
    add-int v19, v16, p1

    goto/16 :goto_1

    .line 991
    .end local v16    # "start":I
    .restart local v2    # "begin":I
    .restart local v10    # "limit":I
    :cond_11
    move v10, v5

    .line 993
    goto :goto_7

    .line 1006
    .end local v2    # "begin":I
    .end local v10    # "limit":I
    .restart local v16    # "start":I
    :cond_12
    aget-object v19, v15, v5

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v19, v0

    add-int v19, v19, v16

    sub-int v19, v19, p1

    add-int/lit8 v19, v19, -0x1

    goto/16 :goto_1
.end method

.method static getLogicalMap(Lcom/ibm/icu/text/Bidi;)[I
    .locals 22
    .param p0, "bidi"    # Lcom/ibm/icu/text/Bidi;

    .prologue
    .line 1013
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    .line 1015
    .local v15, "runs":[Lcom/ibm/icu/text/BidiRun;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->length:I

    move/from16 v20, v0

    move/from16 v0, v20

    new-array v5, v0, [I

    .line 1016
    .local v5, "indexMap":[I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->length:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_0

    .line 1017
    const/16 v20, -0x1

    move/from16 v0, v20

    invoke-static {v5, v0}, Ljava/util/Arrays;->fill([II)V

    .line 1020
    :cond_0
    const/16 v18, 0x0

    .line 1021
    .local v18, "visualStart":I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v7, v0, :cond_2

    .line 1022
    aget-object v20, v15, v7

    move-object/from16 v0, v20

    iget v11, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 1023
    .local v11, "logicalStart":I
    aget-object v20, v15, v7

    move-object/from16 v0, v20

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v17, v0

    .line 1024
    .local v17, "visualLimit":I
    aget-object v20, v15, v7

    invoke-virtual/range {v20 .. v20}, Lcom/ibm/icu/text/BidiRun;->isEvenRun()Z

    move-result v20

    if-eqz v20, :cond_1

    .line 1026
    :goto_1
    add-int/lit8 v12, v11, 0x1

    .end local v11    # "logicalStart":I
    .local v12, "logicalStart":I
    add-int/lit8 v19, v18, 0x1

    .end local v18    # "visualStart":I
    .local v19, "visualStart":I
    aput v18, v5, v11

    .line 1027
    move/from16 v0, v19

    move/from16 v1, v17

    if-lt v0, v1, :cond_e

    move/from16 v18, v19

    .end local v19    # "visualStart":I
    .restart local v18    # "visualStart":I
    move v11, v12

    .line 1021
    .end local v12    # "logicalStart":I
    .restart local v11    # "logicalStart":I
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1029
    :cond_1
    sub-int v20, v17, v18

    add-int v11, v11, v20

    .line 1031
    :goto_3
    add-int/lit8 v11, v11, -0x1

    add-int/lit8 v19, v18, 0x1

    .end local v18    # "visualStart":I
    .restart local v19    # "visualStart":I
    aput v18, v5, v11

    .line 1032
    move/from16 v0, v19

    move/from16 v1, v17

    if-lt v0, v1, :cond_d

    move/from16 v18, v19

    .end local v19    # "visualStart":I
    .restart local v18    # "visualStart":I
    goto :goto_2

    .line 1037
    .end local v11    # "logicalStart":I
    .end local v17    # "visualLimit":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    move/from16 v20, v0

    if-lez v20, :cond_6

    .line 1038
    const/4 v13, 0x0

    .local v13, "markFound":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    .line 1040
    .local v14, "runCount":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    .line 1041
    const/16 v18, 0x0

    .line 1043
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_4
    if-ge v4, v14, :cond_c

    .line 1044
    aget-object v20, v15, v4

    move-object/from16 v0, v20

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v20, v0

    sub-int v9, v20, v18

    .line 1045
    .local v9, "length":I
    aget-object v20, v15, v4

    move-object/from16 v0, v20

    iget v6, v0, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 1046
    .local v6, "insertRemove":I
    and-int/lit8 v20, v6, 0x5

    if-lez v20, :cond_3

    .line 1047
    add-int/lit8 v13, v13, 0x1

    .line 1049
    :cond_3
    if-lez v13, :cond_4

    .line 1050
    aget-object v20, v15, v4

    move-object/from16 v0, v20

    iget v11, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 1051
    .restart local v11    # "logicalStart":I
    add-int v10, v11, v9

    .line 1052
    .local v10, "logicalLimit":I
    move v7, v11

    :goto_5
    if-ge v7, v10, :cond_4

    .line 1053
    aget v20, v5, v7

    add-int v20, v20, v13

    aput v20, v5, v7

    .line 1052
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 1056
    .end local v10    # "logicalLimit":I
    .end local v11    # "logicalStart":I
    :cond_4
    and-int/lit8 v20, v6, 0xa

    if-lez v20, :cond_5

    .line 1057
    add-int/lit8 v13, v13, 0x1

    .line 1043
    :cond_5
    add-int/lit8 v4, v4, 0x1

    add-int v18, v18, v9

    goto :goto_4

    .line 1061
    .end local v4    # "i":I
    .end local v6    # "insertRemove":I
    .end local v9    # "length":I
    .end local v13    # "markFound":I
    .end local v14    # "runCount":I
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->controlCount:I

    move/from16 v20, v0

    if-lez v20, :cond_c

    .line 1062
    const/4 v2, 0x0

    .local v2, "controlFound":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    .line 1066
    .restart local v14    # "runCount":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    .line 1067
    const/16 v18, 0x0

    .line 1069
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_6
    if-ge v4, v14, :cond_c

    .line 1070
    aget-object v20, v15, v4

    move-object/from16 v0, v20

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v20, v0

    sub-int v9, v20, v18

    .line 1071
    .restart local v9    # "length":I
    aget-object v20, v15, v4

    move-object/from16 v0, v20

    iget v6, v0, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 1073
    .restart local v6    # "insertRemove":I
    sub-int v20, v2, v6

    if-nez v20, :cond_8

    .line 1069
    :cond_7
    add-int/lit8 v4, v4, 0x1

    add-int v18, v18, v9

    goto :goto_6

    .line 1076
    :cond_8
    aget-object v20, v15, v4

    move-object/from16 v0, v20

    iget v11, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 1077
    .restart local v11    # "logicalStart":I
    aget-object v20, v15, v4

    invoke-virtual/range {v20 .. v20}, Lcom/ibm/icu/text/BidiRun;->isEvenRun()Z

    move-result v3

    .line 1078
    .local v3, "evenRun":Z
    add-int v10, v11, v9

    .line 1080
    .restart local v10    # "logicalLimit":I
    if-nez v6, :cond_9

    .line 1081
    move v7, v11

    :goto_7
    if-ge v7, v10, :cond_7

    .line 1082
    aget v20, v5, v7

    sub-int v20, v20, v2

    aput v20, v5, v7

    .line 1081
    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    .line 1086
    :cond_9
    const/4 v7, 0x0

    :goto_8
    if-ge v7, v9, :cond_7

    .line 1087
    if-eqz v3, :cond_a

    add-int v8, v11, v7

    .line 1088
    .local v8, "k":I
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->text:[C

    move-object/from16 v20, v0

    aget-char v16, v20, v8

    .line 1089
    .local v16, "uchar":C
    invoke-static/range {v16 .. v16}, Lcom/ibm/icu/text/Bidi;->IsBidiControlChar(I)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 1090
    add-int/lit8 v2, v2, 0x1

    .line 1091
    const/16 v20, -0x1

    aput v20, v5, v8

    .line 1086
    :goto_a
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 1087
    .end local v8    # "k":I
    .end local v16    # "uchar":C
    :cond_a
    sub-int v20, v10, v7

    add-int/lit8 v8, v20, -0x1

    goto :goto_9

    .line 1094
    .restart local v8    # "k":I
    .restart local v16    # "uchar":C
    :cond_b
    aget v20, v5, v8

    sub-int v20, v20, v2

    aput v20, v5, v8

    goto :goto_a

    .line 1098
    .end local v2    # "controlFound":I
    .end local v3    # "evenRun":Z
    .end local v4    # "i":I
    .end local v6    # "insertRemove":I
    .end local v8    # "k":I
    .end local v9    # "length":I
    .end local v10    # "logicalLimit":I
    .end local v11    # "logicalStart":I
    .end local v14    # "runCount":I
    .end local v16    # "uchar":C
    :cond_c
    return-object v5

    .end local v18    # "visualStart":I
    .restart local v11    # "logicalStart":I
    .restart local v17    # "visualLimit":I
    .restart local v19    # "visualStart":I
    :cond_d
    move/from16 v18, v19

    .end local v19    # "visualStart":I
    .restart local v18    # "visualStart":I
    goto/16 :goto_3

    .end local v11    # "logicalStart":I
    .end local v18    # "visualStart":I
    .restart local v12    # "logicalStart":I
    .restart local v19    # "visualStart":I
    :cond_e
    move/from16 v18, v19

    .end local v19    # "visualStart":I
    .restart local v18    # "visualStart":I
    move v11, v12

    .end local v12    # "logicalStart":I
    .restart local v11    # "logicalStart":I
    goto/16 :goto_1
.end method

.method static getLogicalRun(Lcom/ibm/icu/text/Bidi;I)Lcom/ibm/icu/text/BidiRun;
    .locals 8
    .param p0, "bidi"    # Lcom/ibm/icu/text/Bidi;
    .param p1, "logicalPosition"    # I

    .prologue
    .line 281
    new-instance v3, Lcom/ibm/icu/text/BidiRun;

    invoke-direct {v3}, Lcom/ibm/icu/text/BidiRun;-><init>()V

    .line 282
    .local v3, "newRun":Lcom/ibm/icu/text/BidiRun;
    invoke-static {p0}, Lcom/ibm/icu/text/BidiLine;->getRuns(Lcom/ibm/icu/text/Bidi;)V

    .line 283
    iget v4, p0, Lcom/ibm/icu/text/Bidi;->runCount:I

    .line 284
    .local v4, "runCount":I
    const/4 v5, 0x0

    .local v5, "visualStart":I
    const/4 v2, 0x0

    .line 285
    .local v2, "logicalLimit":I
    iget-object v6, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    const/4 v7, 0x0

    aget-object v1, v6, v7

    .line 287
    .local v1, "iRun":Lcom/ibm/icu/text/BidiRun;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 288
    iget-object v6, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    aget-object v1, v6, v0

    .line 289
    iget v6, v1, Lcom/ibm/icu/text/BidiRun;->start:I

    iget v7, v1, Lcom/ibm/icu/text/BidiRun;->limit:I

    add-int/2addr v6, v7

    sub-int v2, v6, v5

    .line 290
    iget v6, v1, Lcom/ibm/icu/text/BidiRun;->start:I

    if-lt p1, v6, :cond_1

    if-ge p1, v2, :cond_1

    .line 296
    :cond_0
    iget v6, v1, Lcom/ibm/icu/text/BidiRun;->start:I

    iput v6, v3, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 297
    iput v2, v3, Lcom/ibm/icu/text/BidiRun;->limit:I

    .line 298
    iget-byte v6, v1, Lcom/ibm/icu/text/BidiRun;->level:B

    iput-byte v6, v3, Lcom/ibm/icu/text/BidiRun;->level:B

    .line 299
    return-object v3

    .line 294
    :cond_1
    iget v5, v1, Lcom/ibm/icu/text/BidiRun;->limit:I

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static getRunFromLogicalIndex(Lcom/ibm/icu/text/Bidi;I)I
    .locals 8
    .param p0, "bidi"    # Lcom/ibm/icu/text/Bidi;
    .param p1, "logicalIndex"    # I

    .prologue
    .line 448
    iget-object v4, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    .line 449
    .local v4, "runs":[Lcom/ibm/icu/text/BidiRun;
    iget v3, p0, Lcom/ibm/icu/text/Bidi;->runCount:I

    .local v3, "runCount":I
    const/4 v5, 0x0

    .line 451
    .local v5, "visualStart":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 452
    aget-object v6, v4, v0

    iget v6, v6, Lcom/ibm/icu/text/BidiRun;->limit:I

    sub-int v1, v6, v5

    .line 453
    .local v1, "length":I
    aget-object v6, v4, v0

    iget v2, v6, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 454
    .local v2, "logicalStart":I
    if-lt p1, v2, :cond_0

    add-int v6, v2, v1

    if-ge p1, v6, :cond_0

    .line 455
    return v0

    .line 457
    :cond_0
    add-int/2addr v5, v1

    .line 451
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 460
    .end local v1    # "length":I
    .end local v2    # "logicalStart":I
    :cond_1
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string/jumbo v7, "Internal ICU error in getRunFromLogicalIndex"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method static getRuns(Lcom/ibm/icu/text/Bidi;)V
    .locals 22
    .param p0, "bidi"    # Lcom/ibm/icu/text/Bidi;

    .prologue
    .line 479
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    move/from16 v19, v0

    if-ltz v19, :cond_1

    .line 624
    :cond_0
    return-void

    .line 482
    :cond_1
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/Bidi;->direction:B

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3

    .line 485
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/ibm/icu/text/BidiLine;->getSingleRun(Lcom/ibm/icu/text/Bidi;B)V

    .line 602
    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    move/from16 v19, v0

    if-lez v19, :cond_f

    .line 605
    const/4 v6, 0x0

    .local v6, "ip":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v6, v0, :cond_f

    .line 606
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi$InsertPoints;->points:[Lcom/ibm/icu/text/Bidi$Point;

    move-object/from16 v19, v0

    aget-object v13, v19, v6

    .line 607
    .local v13, "point":Lcom/ibm/icu/text/Bidi$Point;
    iget v0, v13, Lcom/ibm/icu/text/Bidi$Point;->pos:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/ibm/icu/text/BidiLine;->getRunFromLogicalIndex(Lcom/ibm/icu/text/Bidi;I)I

    move-result v15

    .line 608
    .local v15, "runIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v19, v0

    aget-object v19, v19, v15

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    move/from16 v20, v0

    iget v0, v13, Lcom/ibm/icu/text/Bidi$Point;->flag:I

    move/from16 v21, v0

    or-int v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 605
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 488
    .end local v6    # "ip":I
    .end local v13    # "point":Lcom/ibm/icu/text/Bidi$Point;
    .end local v15    # "runIndex":I
    :cond_3
    move-object/from16 v0, p0

    iget v7, v0, Lcom/ibm/icu/text/Bidi;->length:I

    .line 489
    .local v7, "length":I
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/ibm/icu/text/Bidi;->levels:[B

    .line 491
    .local v9, "levels":[B
    const/16 v8, 0x7e

    .line 503
    .local v8, "level":B
    move-object/from16 v0, p0

    iget v10, v0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    .line 505
    .local v10, "limit":I
    const/4 v14, 0x0

    .line 506
    .local v14, "runCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v10, :cond_5

    .line 508
    aget-byte v19, v9, v4

    move/from16 v0, v19

    if-eq v0, v8, :cond_4

    .line 509
    add-int/lit8 v14, v14, 0x1

    .line 510
    aget-byte v8, v9, v4

    .line 506
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 518
    :cond_5
    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v14, v0, :cond_6

    if-ne v10, v7, :cond_6

    .line 520
    const/16 v19, 0x0

    aget-byte v19, v9, v19

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/ibm/icu/text/BidiLine;->getSingleRun(Lcom/ibm/icu/text/Bidi;B)V

    goto/16 :goto_0

    .line 525
    :cond_6
    const/16 v12, 0x3e

    .line 526
    .local v12, "minLevel":B
    const/4 v11, 0x0

    .line 529
    .local v11, "maxLevel":B
    if-ge v10, v7, :cond_7

    .line 530
    add-int/lit8 v14, v14, 0x1

    .line 534
    :cond_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/ibm/icu/text/Bidi;->getRunsMemory(I)V

    .line 535
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runsMemory:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v16, v0

    .line 543
    .local v16, "runs":[Lcom/ibm/icu/text/BidiRun;
    const/4 v15, 0x0

    .line 546
    .restart local v15    # "runIndex":I
    const/4 v4, 0x0

    .line 549
    :cond_8
    move/from16 v17, v4

    .line 550
    .local v17, "start":I
    aget-byte v8, v9, v4

    .line 551
    if-ge v8, v12, :cond_9

    .line 552
    move v12, v8

    .line 554
    :cond_9
    if-le v8, v11, :cond_a

    .line 555
    move v11, v8

    .line 559
    :cond_a
    add-int/lit8 v4, v4, 0x1

    if-ge v4, v10, :cond_b

    aget-byte v19, v9, v4

    move/from16 v0, v19

    if-eq v0, v8, :cond_a

    .line 562
    :cond_b
    new-instance v19, Lcom/ibm/icu/text/BidiRun;

    sub-int v20, v4, v17

    move-object/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v20

    invoke-direct {v0, v1, v2, v8}, Lcom/ibm/icu/text/BidiRun;-><init>(IIB)V

    aput-object v19, v16, v15

    .line 563
    add-int/lit8 v15, v15, 0x1

    .line 564
    if-lt v4, v10, :cond_8

    .line 566
    if-ge v10, v7, :cond_c

    .line 568
    new-instance v19, Lcom/ibm/icu/text/BidiRun;

    sub-int v20, v7, v10

    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    move/from16 v21, v0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v10, v1, v2}, Lcom/ibm/icu/text/BidiRun;-><init>(IIB)V

    aput-object v19, v16, v15

    .line 571
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v0, v12, :cond_c

    .line 572
    move-object/from16 v0, p0

    iget-byte v12, v0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    .line 577
    :cond_c
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    .line 578
    move-object/from16 v0, p0

    iput v14, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    .line 580
    move-object/from16 v0, p0

    invoke-static {v0, v12, v11}, Lcom/ibm/icu/text/BidiLine;->reorderLine(Lcom/ibm/icu/text/Bidi;BB)V

    .line 584
    const/4 v10, 0x0

    .line 585
    const/4 v4, 0x0

    :goto_3
    if-ge v4, v14, :cond_d

    .line 586
    aget-object v19, v16, v4

    aget-object v20, v16, v4

    move-object/from16 v0, v20

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    move/from16 v20, v0

    aget-byte v20, v9, v20

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-byte v0, v1, Lcom/ibm/icu/text/BidiRun;->level:B

    .line 587
    aget-object v19, v16, v4

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v20, v0

    add-int v10, v10, v20

    move-object/from16 v0, v19

    iput v10, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    .line 585
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 594
    :cond_d
    if-ge v15, v14, :cond_2

    .line 595
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    move/from16 v19, v0

    and-int/lit8 v19, v19, 0x1

    if-eqz v19, :cond_e

    const/16 v18, 0x0

    .line 596
    .local v18, "trailingRun":I
    :goto_4
    aget-object v19, v16, v18

    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-byte v0, v1, Lcom/ibm/icu/text/BidiRun;->level:B

    goto/16 :goto_0

    .end local v18    # "trailingRun":I
    :cond_e
    move/from16 v18, v15

    .line 595
    goto :goto_4

    .line 613
    .end local v4    # "i":I
    .end local v7    # "length":I
    .end local v8    # "level":B
    .end local v9    # "levels":[B
    .end local v10    # "limit":I
    .end local v11    # "maxLevel":B
    .end local v12    # "minLevel":B
    .end local v14    # "runCount":I
    .end local v15    # "runIndex":I
    .end local v16    # "runs":[Lcom/ibm/icu/text/BidiRun;
    .end local v17    # "start":I
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->controlCount:I

    move/from16 v19, v0

    if-lez v19, :cond_0

    .line 616
    const/4 v5, 0x0

    .local v5, "ic":I
    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->length:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v5, v0, :cond_0

    .line 617
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->text:[C

    move-object/from16 v19, v0

    aget-char v3, v19, v5

    .line 618
    .local v3, "c":C
    invoke-static {v3}, Lcom/ibm/icu/text/Bidi;->IsBidiControlChar(I)Z

    move-result v19

    if-eqz v19, :cond_10

    .line 619
    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/ibm/icu/text/BidiLine;->getRunFromLogicalIndex(Lcom/ibm/icu/text/Bidi;I)I

    move-result v15

    .line 620
    .restart local v15    # "runIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v19, v0

    aget-object v19, v19, v15

    move-object/from16 v0, v19

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 616
    .end local v15    # "runIndex":I
    :cond_10
    add-int/lit8 v5, v5, 0x1

    goto :goto_5
.end method

.method static getSingleRun(Lcom/ibm/icu/text/Bidi;B)V
    .locals 4
    .param p0, "bidi"    # Lcom/ibm/icu/text/Bidi;
    .param p1, "level"    # B

    .prologue
    const/4 v3, 0x0

    .line 321
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->simpleRuns:[Lcom/ibm/icu/text/BidiRun;

    iput-object v0, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    .line 322
    const/4 v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/Bidi;->runCount:I

    .line 325
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    new-instance v1, Lcom/ibm/icu/text/BidiRun;

    iget v2, p0, Lcom/ibm/icu/text/Bidi;->length:I

    invoke-direct {v1, v3, v2, p1}, Lcom/ibm/icu/text/BidiRun;-><init>(IIB)V

    aput-object v1, v0, v3

    .line 326
    return-void
.end method

.method static getVisualIndex(Lcom/ibm/icu/text/Bidi;I)I
    .locals 14
    .param p0, "bidi"    # Lcom/ibm/icu/text/Bidi;
    .param p1, "logicalIndex"    # I

    .prologue
    .line 802
    const/4 v11, -0x1

    .line 805
    .local v11, "visualIndex":I
    iget-byte v13, p0, Lcom/ibm/icu/text/Bidi;->direction:B

    packed-switch v13, :pswitch_data_0

    .line 813
    invoke-static {p0}, Lcom/ibm/icu/text/BidiLine;->getRuns(Lcom/ibm/icu/text/Bidi;)V

    .line 814
    iget-object v8, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    .line 815
    .local v8, "runs":[Lcom/ibm/icu/text/BidiRun;
    const/4 v12, 0x0

    .line 818
    .local v12, "visualStart":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v13, p0, Lcom/ibm/icu/text/Bidi;->runCount:I

    if-ge v1, v13, :cond_0

    .line 819
    aget-object v13, v8, v1

    iget v13, v13, Lcom/ibm/icu/text/BidiRun;->limit:I

    sub-int v4, v13, v12

    .line 820
    .local v4, "length":I
    aget-object v13, v8, v1

    iget v13, v13, Lcom/ibm/icu/text/BidiRun;->start:I

    sub-int v7, p1, v13

    .line 821
    .local v7, "offset":I
    if-ltz v7, :cond_4

    if-ge v7, v4, :cond_4

    .line 822
    aget-object v13, v8, v1

    invoke-virtual {v13}, Lcom/ibm/icu/text/BidiRun;->isEvenRun()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 824
    add-int v11, v12, v7

    .line 833
    .end local v4    # "length":I
    .end local v7    # "offset":I
    :cond_0
    :goto_1
    iget v13, p0, Lcom/ibm/icu/text/Bidi;->runCount:I

    if-lt v1, v13, :cond_1

    .line 834
    const/4 v13, -0x1

    .line 900
    .end local v1    # "i":I
    .end local v8    # "runs":[Lcom/ibm/icu/text/BidiRun;
    .end local v12    # "visualStart":I
    :goto_2
    return v13

    .line 807
    :pswitch_0
    move v11, p1

    .line 838
    :cond_1
    :goto_3
    iget-object v13, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v13, v13, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    if-lez v13, :cond_7

    .line 840
    iget-object v8, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    .line 842
    .restart local v8    # "runs":[Lcom/ibm/icu/text/BidiRun;
    const/4 v12, 0x0

    .restart local v12    # "visualStart":I
    const/4 v6, 0x0

    .line 843
    .local v6, "markFound":I
    const/4 v1, 0x0

    .line 844
    .restart local v1    # "i":I
    :goto_4
    aget-object v13, v8, v1

    iget v13, v13, Lcom/ibm/icu/text/BidiRun;->limit:I

    sub-int v4, v13, v12

    .line 845
    .restart local v4    # "length":I
    aget-object v13, v8, v1

    iget v2, v13, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 846
    .local v2, "insertRemove":I
    and-int/lit8 v13, v2, 0x5

    if-lez v13, :cond_2

    .line 847
    add-int/lit8 v6, v6, 0x1

    .line 850
    :cond_2
    aget-object v13, v8, v1

    iget v13, v13, Lcom/ibm/icu/text/BidiRun;->limit:I

    if-ge v11, v13, :cond_5

    .line 851
    add-int v13, v11, v6

    goto :goto_2

    .line 810
    .end local v1    # "i":I
    .end local v2    # "insertRemove":I
    .end local v4    # "length":I
    .end local v6    # "markFound":I
    .end local v8    # "runs":[Lcom/ibm/icu/text/BidiRun;
    .end local v12    # "visualStart":I
    :pswitch_1
    iget v13, p0, Lcom/ibm/icu/text/Bidi;->length:I

    sub-int/2addr v13, p1

    add-int/lit8 v11, v13, -0x1

    .line 811
    goto :goto_3

    .line 827
    .restart local v1    # "i":I
    .restart local v4    # "length":I
    .restart local v7    # "offset":I
    .restart local v8    # "runs":[Lcom/ibm/icu/text/BidiRun;
    .restart local v12    # "visualStart":I
    :cond_3
    add-int v13, v12, v4

    sub-int/2addr v13, v7

    add-int/lit8 v11, v13, -0x1

    .line 829
    goto :goto_1

    .line 831
    :cond_4
    add-int/2addr v12, v4

    .line 818
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 853
    .end local v7    # "offset":I
    .restart local v2    # "insertRemove":I
    .restart local v6    # "markFound":I
    :cond_5
    and-int/lit8 v13, v2, 0xa

    if-lez v13, :cond_6

    .line 854
    add-int/lit8 v6, v6, 0x1

    .line 843
    :cond_6
    add-int/lit8 v1, v1, 0x1

    add-int/2addr v12, v4

    goto :goto_4

    .line 858
    .end local v1    # "i":I
    .end local v2    # "insertRemove":I
    .end local v4    # "length":I
    .end local v6    # "markFound":I
    .end local v8    # "runs":[Lcom/ibm/icu/text/BidiRun;
    .end local v12    # "visualStart":I
    :cond_7
    iget v13, p0, Lcom/ibm/icu/text/Bidi;->controlCount:I

    if-lez v13, :cond_e

    .line 860
    iget-object v8, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    .line 862
    .restart local v8    # "runs":[Lcom/ibm/icu/text/BidiRun;
    const/4 v12, 0x0

    .restart local v12    # "visualStart":I
    const/4 v0, 0x0

    .line 863
    .local v0, "controlFound":I
    iget-object v13, p0, Lcom/ibm/icu/text/Bidi;->text:[C

    aget-char v10, v13, p1

    .line 865
    .local v10, "uchar":C
    invoke-static {v10}, Lcom/ibm/icu/text/Bidi;->IsBidiControlChar(I)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 866
    const/4 v13, -0x1

    goto :goto_2

    .line 869
    :cond_8
    const/4 v1, 0x0

    .line 870
    .restart local v1    # "i":I
    :goto_5
    aget-object v13, v8, v1

    iget v13, v13, Lcom/ibm/icu/text/BidiRun;->limit:I

    sub-int v4, v13, v12

    .line 871
    .restart local v4    # "length":I
    aget-object v13, v8, v1

    iget v2, v13, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 873
    .restart local v2    # "insertRemove":I
    aget-object v13, v8, v1

    iget v13, v13, Lcom/ibm/icu/text/BidiRun;->limit:I

    if-lt v11, v13, :cond_9

    .line 874
    sub-int/2addr v0, v2

    .line 869
    add-int/lit8 v1, v1, 0x1

    add-int/2addr v12, v4

    goto :goto_5

    .line 878
    :cond_9
    if-nez v2, :cond_a

    .line 879
    sub-int v13, v11, v0

    goto :goto_2

    .line 881
    :cond_a
    aget-object v13, v8, v1

    invoke-virtual {v13}, Lcom/ibm/icu/text/BidiRun;->isEvenRun()Z

    move-result v13

    if-eqz v13, :cond_c

    .line 883
    aget-object v13, v8, v1

    iget v9, v13, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 884
    .local v9, "start":I
    move v5, p1

    .line 890
    .local v5, "limit":I
    :goto_6
    move v3, v9

    .local v3, "j":I
    :goto_7
    if-ge v3, v5, :cond_d

    .line 891
    iget-object v13, p0, Lcom/ibm/icu/text/Bidi;->text:[C

    aget-char v10, v13, v3

    .line 892
    invoke-static {v10}, Lcom/ibm/icu/text/Bidi;->IsBidiControlChar(I)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 893
    add-int/lit8 v0, v0, 0x1

    .line 890
    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 887
    .end local v3    # "j":I
    .end local v5    # "limit":I
    .end local v9    # "start":I
    :cond_c
    add-int/lit8 v9, p1, 0x1

    .line 888
    .restart local v9    # "start":I
    aget-object v13, v8, v1

    iget v13, v13, Lcom/ibm/icu/text/BidiRun;->start:I

    add-int v5, v13, v4

    .restart local v5    # "limit":I
    goto :goto_6

    .line 896
    .restart local v3    # "j":I
    :cond_d
    sub-int v13, v11, v0

    goto/16 :goto_2

    .end local v0    # "controlFound":I
    .end local v1    # "i":I
    .end local v2    # "insertRemove":I
    .end local v3    # "j":I
    .end local v4    # "length":I
    .end local v5    # "limit":I
    .end local v8    # "runs":[Lcom/ibm/icu/text/BidiRun;
    .end local v9    # "start":I
    .end local v10    # "uchar":C
    .end local v12    # "visualStart":I
    :cond_e
    move v13, v11

    .line 900
    goto/16 :goto_2

    .line 805
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static getVisualMap(Lcom/ibm/icu/text/Bidi;)[I
    .locals 29
    .param p0, "bidi"    # Lcom/ibm/icu/text/Bidi;

    .prologue
    .line 1104
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v22, v0

    .line 1106
    .local v22, "runs":[Lcom/ibm/icu/text/BidiRun;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->length:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    move/from16 v27, v0

    move/from16 v0, v26

    move/from16 v1, v27

    if-le v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/Bidi;->length:I

    .line 1108
    .local v4, "allocLength":I
    :goto_0
    new-array v9, v4, [I

    .line 1110
    .local v9, "indexMap":[I
    const/16 v25, 0x0

    .line 1111
    .local v25, "visualStart":I
    const/4 v7, 0x0

    .line 1112
    .local v7, "idx":I
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    move/from16 v26, v0

    move/from16 v0, v26

    if-ge v11, v0, :cond_2

    .line 1113
    aget-object v26, v22, v11

    move-object/from16 v0, v26

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    move/from16 v16, v0

    .line 1114
    .local v16, "logicalStart":I
    aget-object v26, v22, v11

    move-object/from16 v0, v26

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v24, v0

    .line 1115
    .local v24, "visualLimit":I
    aget-object v26, v22, v11

    invoke-virtual/range {v26 .. v26}, Lcom/ibm/icu/text/BidiRun;->isEvenRun()Z

    move-result v26

    if-eqz v26, :cond_1

    .line 1117
    :goto_2
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "idx":I
    .local v8, "idx":I
    add-int/lit8 v17, v16, 0x1

    .end local v16    # "logicalStart":I
    .local v17, "logicalStart":I
    aput v16, v9, v7

    .line 1118
    add-int/lit8 v25, v25, 0x1

    move/from16 v0, v25

    move/from16 v1, v24

    if-lt v0, v1, :cond_13

    move v7, v8

    .end local v8    # "idx":I
    .restart local v7    # "idx":I
    move/from16 v16, v17

    .line 1112
    .end local v17    # "logicalStart":I
    .restart local v16    # "logicalStart":I
    :goto_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 1106
    .end local v4    # "allocLength":I
    .end local v7    # "idx":I
    .end local v9    # "indexMap":[I
    .end local v11    # "j":I
    .end local v16    # "logicalStart":I
    .end local v24    # "visualLimit":I
    .end local v25    # "visualStart":I
    :cond_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    goto :goto_0

    .line 1120
    .restart local v4    # "allocLength":I
    .restart local v7    # "idx":I
    .restart local v9    # "indexMap":[I
    .restart local v11    # "j":I
    .restart local v16    # "logicalStart":I
    .restart local v24    # "visualLimit":I
    .restart local v25    # "visualStart":I
    :cond_1
    sub-int v26, v24, v25

    add-int v16, v16, v26

    .line 1122
    :goto_4
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "idx":I
    .restart local v8    # "idx":I
    add-int/lit8 v16, v16, -0x1

    aput v16, v9, v7

    .line 1123
    add-int/lit8 v25, v25, 0x1

    move/from16 v0, v25

    move/from16 v1, v24

    if-lt v0, v1, :cond_12

    move v7, v8

    .end local v8    # "idx":I
    .restart local v7    # "idx":I
    goto :goto_3

    .line 1128
    .end local v16    # "logicalStart":I
    .end local v24    # "visualLimit":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    move/from16 v26, v0

    if-lez v26, :cond_a

    .line 1129
    const/16 v19, 0x0

    .local v19, "markFound":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    move/from16 v21, v0

    .line 1131
    .local v21, "runCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v22, v0

    .line 1133
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_5
    move/from16 v0, v21

    if-ge v6, v0, :cond_5

    .line 1134
    aget-object v26, v22, v6

    move-object/from16 v0, v26

    iget v10, v0, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 1135
    .local v10, "insertRemove":I
    and-int/lit8 v26, v10, 0x5

    if-lez v26, :cond_3

    .line 1136
    add-int/lit8 v19, v19, 0x1

    .line 1138
    :cond_3
    and-int/lit8 v26, v10, 0xa

    if-lez v26, :cond_4

    .line 1139
    add-int/lit8 v19, v19, 0x1

    .line 1133
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 1143
    .end local v10    # "insertRemove":I
    :cond_5
    move-object/from16 v0, p0

    iget v12, v0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    .line 1144
    .local v12, "k":I
    add-int/lit8 v6, v21, -0x1

    :goto_6
    if-ltz v6, :cond_e

    if-lez v19, :cond_e

    .line 1145
    aget-object v26, v22, v6

    move-object/from16 v0, v26

    iget v10, v0, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 1146
    .restart local v10    # "insertRemove":I
    and-int/lit8 v26, v10, 0xa

    if-lez v26, :cond_6

    .line 1147
    add-int/lit8 v12, v12, -0x1

    const/16 v26, -0x1

    aput v26, v9, v12

    .line 1148
    add-int/lit8 v19, v19, -0x1

    .line 1150
    :cond_6
    if-lez v6, :cond_7

    add-int/lit8 v26, v6, -0x1

    aget-object v26, v22, v26

    move-object/from16 v0, v26

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v25, v0

    .line 1151
    :goto_7
    aget-object v26, v22, v6

    move-object/from16 v0, v26

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v26, v0

    add-int/lit8 v11, v26, -0x1

    :goto_8
    move/from16 v0, v25

    if-lt v11, v0, :cond_8

    if-lez v19, :cond_8

    .line 1152
    add-int/lit8 v12, v12, -0x1

    aget v26, v9, v11

    aput v26, v9, v12

    .line 1151
    add-int/lit8 v11, v11, -0x1

    goto :goto_8

    .line 1150
    :cond_7
    const/16 v25, 0x0

    goto :goto_7

    .line 1154
    :cond_8
    and-int/lit8 v26, v10, 0x5

    if-lez v26, :cond_9

    .line 1155
    add-int/lit8 v12, v12, -0x1

    const/16 v26, -0x1

    aput v26, v9, v12

    .line 1156
    add-int/lit8 v19, v19, -0x1

    .line 1144
    :cond_9
    add-int/lit8 v6, v6, -0x1

    goto :goto_6

    .line 1160
    .end local v6    # "i":I
    .end local v10    # "insertRemove":I
    .end local v12    # "k":I
    .end local v19    # "markFound":I
    .end local v21    # "runCount":I
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->controlCount:I

    move/from16 v26, v0

    if-lez v26, :cond_e

    .line 1161
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    move/from16 v21, v0

    .line 1165
    .restart local v21    # "runCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v22, v0

    .line 1166
    const/16 v25, 0x0

    .line 1168
    const/4 v12, 0x0

    .line 1169
    .restart local v12    # "k":I
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_9
    move/from16 v0, v21

    if-ge v6, v0, :cond_e

    .line 1170
    aget-object v26, v22, v6

    move-object/from16 v0, v26

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v26, v0

    sub-int v14, v26, v25

    .line 1171
    .local v14, "length":I
    aget-object v26, v22, v6

    move-object/from16 v0, v26

    iget v10, v0, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 1173
    .restart local v10    # "insertRemove":I
    if-nez v10, :cond_b

    move/from16 v0, v25

    if-ne v12, v0, :cond_b

    .line 1174
    add-int/2addr v12, v14

    .line 1169
    :goto_a
    add-int/lit8 v6, v6, 0x1

    add-int v25, v25, v14

    goto :goto_9

    .line 1178
    :cond_b
    if-nez v10, :cond_c

    .line 1179
    aget-object v26, v22, v6

    move-object/from16 v0, v26

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v24, v0

    .line 1180
    .restart local v24    # "visualLimit":I
    move/from16 v11, v25

    move v13, v12

    .end local v12    # "k":I
    .local v13, "k":I
    :goto_b
    move/from16 v0, v24

    if-ge v11, v0, :cond_11

    .line 1181
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "k":I
    .restart local v12    # "k":I
    aget v26, v9, v11

    aput v26, v9, v13

    .line 1180
    add-int/lit8 v11, v11, 0x1

    move v13, v12

    .end local v12    # "k":I
    .restart local v13    # "k":I
    goto :goto_b

    .line 1185
    .end local v13    # "k":I
    .end local v24    # "visualLimit":I
    .restart local v12    # "k":I
    :cond_c
    aget-object v26, v22, v6

    move-object/from16 v0, v26

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    move/from16 v16, v0

    .line 1186
    .restart local v16    # "logicalStart":I
    aget-object v26, v22, v6

    invoke-virtual/range {v26 .. v26}, Lcom/ibm/icu/text/BidiRun;->isEvenRun()Z

    move-result v5

    .line 1187
    .local v5, "evenRun":Z
    add-int v26, v16, v14

    add-int/lit8 v15, v26, -0x1

    .line 1188
    .local v15, "logicalEnd":I
    const/4 v11, 0x0

    move v13, v12

    .end local v12    # "k":I
    .restart local v13    # "k":I
    :goto_c
    if-ge v11, v14, :cond_11

    .line 1189
    if-eqz v5, :cond_d

    add-int v18, v16, v11

    .line 1190
    .local v18, "m":I
    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->text:[C

    move-object/from16 v26, v0

    aget-char v23, v26, v18

    .line 1191
    .local v23, "uchar":C
    invoke-static/range {v23 .. v23}, Lcom/ibm/icu/text/Bidi;->IsBidiControlChar(I)Z

    move-result v26

    if-nez v26, :cond_10

    .line 1192
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "k":I
    .restart local v12    # "k":I
    aput v18, v9, v13

    .line 1188
    :goto_e
    add-int/lit8 v11, v11, 0x1

    move v13, v12

    .end local v12    # "k":I
    .restart local v13    # "k":I
    goto :goto_c

    .line 1189
    .end local v18    # "m":I
    .end local v23    # "uchar":C
    :cond_d
    sub-int v18, v15, v11

    goto :goto_d

    .line 1197
    .end local v5    # "evenRun":Z
    .end local v6    # "i":I
    .end local v10    # "insertRemove":I
    .end local v13    # "k":I
    .end local v14    # "length":I
    .end local v15    # "logicalEnd":I
    .end local v16    # "logicalStart":I
    .end local v21    # "runCount":I
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    move/from16 v26, v0

    move/from16 v0, v26

    if-ne v4, v0, :cond_f

    .line 1202
    .end local v9    # "indexMap":[I
    :goto_f
    return-object v9

    .line 1200
    .restart local v9    # "indexMap":[I
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    move/from16 v26, v0

    move/from16 v0, v26

    new-array v0, v0, [I

    move-object/from16 v20, v0

    .line 1201
    .local v20, "newMap":[I
    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    move/from16 v28, v0

    move/from16 v0, v26

    move-object/from16 v1, v20

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-static {v9, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v9, v20

    .line 1202
    goto :goto_f

    .end local v20    # "newMap":[I
    .restart local v5    # "evenRun":Z
    .restart local v6    # "i":I
    .restart local v10    # "insertRemove":I
    .restart local v13    # "k":I
    .restart local v14    # "length":I
    .restart local v15    # "logicalEnd":I
    .restart local v16    # "logicalStart":I
    .restart local v18    # "m":I
    .restart local v21    # "runCount":I
    .restart local v23    # "uchar":C
    :cond_10
    move v12, v13

    .end local v13    # "k":I
    .restart local v12    # "k":I
    goto :goto_e

    .end local v5    # "evenRun":Z
    .end local v12    # "k":I
    .end local v15    # "logicalEnd":I
    .end local v16    # "logicalStart":I
    .end local v18    # "m":I
    .end local v23    # "uchar":C
    .restart local v13    # "k":I
    :cond_11
    move v12, v13

    .end local v13    # "k":I
    .restart local v12    # "k":I
    goto/16 :goto_a

    .end local v6    # "i":I
    .end local v7    # "idx":I
    .end local v10    # "insertRemove":I
    .end local v12    # "k":I
    .end local v14    # "length":I
    .end local v21    # "runCount":I
    .restart local v8    # "idx":I
    .restart local v16    # "logicalStart":I
    .restart local v24    # "visualLimit":I
    :cond_12
    move v7, v8

    .end local v8    # "idx":I
    .restart local v7    # "idx":I
    goto/16 :goto_4

    .end local v7    # "idx":I
    .end local v16    # "logicalStart":I
    .restart local v8    # "idx":I
    .restart local v17    # "logicalStart":I
    :cond_13
    move v7, v8

    .end local v8    # "idx":I
    .restart local v7    # "idx":I
    move/from16 v16, v17

    .end local v17    # "logicalStart":I
    .restart local v16    # "logicalStart":I
    goto/16 :goto_2
.end method

.method static getVisualRun(Lcom/ibm/icu/text/Bidi;I)Lcom/ibm/icu/text/BidiRun;
    .locals 6
    .param p0, "bidi"    # Lcom/ibm/icu/text/Bidi;
    .param p1, "runIndex"    # I

    .prologue
    .line 304
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    aget-object v3, v3, p1

    iget v2, v3, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 306
    .local v2, "start":I
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    aget-object v3, v3, p1

    iget-byte v0, v3, Lcom/ibm/icu/text/BidiRun;->level:B

    .line 308
    .local v0, "level":B
    if-lez p1, :cond_0

    .line 309
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    aget-object v3, v3, p1

    iget v3, v3, Lcom/ibm/icu/text/BidiRun;->limit:I

    add-int/2addr v3, v2

    iget-object v4, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    add-int/lit8 v5, p1, -0x1

    aget-object v4, v4, v5

    iget v4, v4, Lcom/ibm/icu/text/BidiRun;->limit:I

    sub-int v1, v3, v4

    .line 315
    .local v1, "limit":I
    :goto_0
    new-instance v3, Lcom/ibm/icu/text/BidiRun;

    invoke-direct {v3, v2, v1, v0}, Lcom/ibm/icu/text/BidiRun;-><init>(IIB)V

    return-object v3

    .line 313
    .end local v1    # "limit":I
    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget v3, v3, Lcom/ibm/icu/text/BidiRun;->limit:I

    add-int v1, v2, v3

    .restart local v1    # "limit":I
    goto :goto_0
.end method

.method static invertMap([I)[I
    .locals 7
    .param p0, "srcMap"    # [I

    .prologue
    .line 1207
    array-length v5, p0

    .line 1208
    .local v5, "srcLength":I
    const/4 v1, -0x1

    .local v1, "destLength":I
    const/4 v0, 0x0

    .line 1211
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v5, :cond_2

    .line 1212
    aget v4, p0, v3

    .line 1213
    .local v4, "srcEntry":I
    if-le v4, v1, :cond_0

    .line 1214
    move v1, v4

    .line 1216
    :cond_0
    if-ltz v4, :cond_1

    .line 1217
    add-int/lit8 v0, v0, 0x1

    .line 1211
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1220
    .end local v4    # "srcEntry":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 1221
    new-array v2, v1, [I

    .line 1222
    .local v2, "destMap":[I
    if-ge v0, v1, :cond_3

    .line 1224
    const/4 v6, -0x1

    invoke-static {v2, v6}, Ljava/util/Arrays;->fill([II)V

    .line 1226
    :cond_3
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_5

    .line 1227
    aget v4, p0, v3

    .line 1228
    .restart local v4    # "srcEntry":I
    if-ltz v4, :cond_4

    .line 1229
    aput v3, v2, v4

    .line 1226
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1232
    .end local v4    # "srcEntry":I
    :cond_5
    return-object v2
.end method

.method static prepareReorder([B[B[B)[I
    .locals 7
    .param p0, "levels"    # [B
    .param p1, "pMinLevel"    # [B
    .param p2, "pMaxLevel"    # [B

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 631
    if-eqz p0, :cond_0

    array-length v5, p0

    if-gtz v5, :cond_1

    .line 660
    :cond_0
    return-object v0

    .line 636
    :cond_1
    const/16 v3, 0x3e

    .line 637
    .local v3, "minLevel":B
    const/4 v2, 0x0

    .line 638
    .local v2, "maxLevel":B
    array-length v4, p0

    .local v4, "start":I
    :cond_2
    :goto_0
    if-lez v4, :cond_4

    .line 639
    add-int/lit8 v4, v4, -0x1

    aget-byte v1, p0, v4

    .line 640
    .local v1, "level":B
    const/16 v5, 0x3e

    if-gt v1, v5, :cond_0

    .line 643
    if-ge v1, v3, :cond_3

    .line 644
    move v3, v1

    .line 646
    :cond_3
    if-le v1, v2, :cond_2

    .line 647
    move v2, v1

    .line 648
    goto :goto_0

    .line 650
    .end local v1    # "level":B
    :cond_4
    aput-byte v3, p1, v6

    .line 651
    aput-byte v2, p2, v6

    .line 654
    array-length v5, p0

    new-array v0, v5, [I

    .line 655
    .local v0, "indexMap":[I
    array-length v4, p0

    :goto_1
    if-lez v4, :cond_0

    .line 656
    add-int/lit8 v4, v4, -0x1

    .line 657
    aput v4, v0, v4

    goto :goto_1
.end method

.method private static reorderLine(Lcom/ibm/icu/text/Bidi;BB)V
    .locals 9
    .param p0, "bidi"    # Lcom/ibm/icu/text/Bidi;
    .param p1, "minLevel"    # B
    .param p2, "maxLevel"    # B

    .prologue
    .line 364
    or-int/lit8 v7, p1, 0x1

    if-gt p2, v7, :cond_1

    .line 443
    :cond_0
    return-void

    .line 378
    :cond_1
    add-int/lit8 v7, p1, 0x1

    int-to-byte p1, v7

    .line 380
    iget-object v5, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    .line 381
    .local v5, "runs":[Lcom/ibm/icu/text/BidiRun;
    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    .line 382
    .local v2, "levels":[B
    iget v4, p0, Lcom/ibm/icu/text/Bidi;->runCount:I

    .line 385
    .local v4, "runCount":I
    iget v7, p0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    iget v8, p0, Lcom/ibm/icu/text/Bidi;->length:I

    if-ge v7, v8, :cond_2

    .line 386
    add-int/lit8 v4, v4, -0x1

    .line 389
    :cond_2
    add-int/lit8 v7, p2, -0x1

    int-to-byte p2, v7

    if-lt p2, p1, :cond_7

    .line 390
    const/4 v1, 0x0

    .line 396
    .local v1, "firstRun":I
    :goto_0
    if-ge v1, v4, :cond_3

    aget-object v7, v5, v1

    iget v7, v7, Lcom/ibm/icu/text/BidiRun;->start:I

    aget-byte v7, v2, v7

    if-ge v7, p2, :cond_3

    .line 397
    add-int/lit8 v1, v1, 0x1

    .line 398
    goto :goto_0

    .line 399
    :cond_3
    if-ge v1, v4, :cond_2

    .line 404
    move v3, v1

    .line 405
    .local v3, "limitRun":I
    :cond_4
    add-int/lit8 v3, v3, 0x1

    if-ge v3, v4, :cond_5

    aget-object v7, v5, v3

    iget v7, v7, Lcom/ibm/icu/text/BidiRun;->start:I

    aget-byte v7, v2, v7

    if-ge v7, p2, :cond_4

    .line 408
    :cond_5
    add-int/lit8 v0, v3, -0x1

    .line 409
    .local v0, "endRun":I
    :goto_1
    if-ge v1, v0, :cond_6

    .line 410
    aget-object v6, v5, v1

    .line 411
    .local v6, "tempRun":Lcom/ibm/icu/text/BidiRun;
    aget-object v7, v5, v0

    aput-object v7, v5, v1

    .line 412
    aput-object v6, v5, v0

    .line 413
    add-int/lit8 v1, v1, 0x1

    .line 414
    add-int/lit8 v0, v0, -0x1

    .line 415
    goto :goto_1

    .line 417
    .end local v6    # "tempRun":Lcom/ibm/icu/text/BidiRun;
    :cond_6
    if-eq v3, v4, :cond_2

    .line 420
    add-int/lit8 v1, v3, 0x1

    .line 422
    goto :goto_0

    .line 426
    .end local v0    # "endRun":I
    .end local v1    # "firstRun":I
    .end local v3    # "limitRun":I
    :cond_7
    and-int/lit8 v7, p1, 0x1

    if-nez v7, :cond_0

    .line 427
    const/4 v1, 0x0

    .line 430
    .restart local v1    # "firstRun":I
    iget v7, p0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    iget v8, p0, Lcom/ibm/icu/text/Bidi;->length:I

    if-ne v7, v8, :cond_8

    .line 431
    add-int/lit8 v4, v4, -0x1

    .line 435
    :cond_8
    :goto_2
    if-ge v1, v4, :cond_0

    .line 436
    aget-object v6, v5, v1

    .line 437
    .restart local v6    # "tempRun":Lcom/ibm/icu/text/BidiRun;
    aget-object v7, v5, v4

    aput-object v7, v5, v1

    .line 438
    aput-object v6, v5, v4

    .line 439
    add-int/lit8 v1, v1, 0x1

    .line 440
    add-int/lit8 v4, v4, -0x1

    .line 441
    goto :goto_2
.end method

.method static reorderLogical([B)[I
    .locals 10
    .param p0, "levels"    # [B

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 665
    new-array v1, v9, [B

    .line 666
    .local v1, "aMinLevel":[B
    new-array v0, v9, [B

    .line 669
    .local v0, "aMaxLevel":[B
    invoke-static {p0, v1, v0}, Lcom/ibm/icu/text/BidiLine;->prepareReorder([B[B[B)[I

    move-result-object v2

    .line 670
    .local v2, "indexMap":[I
    if-nez v2, :cond_1

    .line 671
    const/4 v2, 0x0

    .line 729
    .end local v2    # "indexMap":[I
    :cond_0
    :goto_0
    return-object v2

    .line 674
    .restart local v2    # "indexMap":[I
    :cond_1
    aget-byte v5, v1, v8

    .line 675
    .local v5, "minLevel":B
    aget-byte v4, v0, v8

    .line 678
    .local v4, "maxLevel":B
    if-ne v5, v4, :cond_2

    and-int/lit8 v8, v5, 0x1

    if-eqz v8, :cond_0

    .line 683
    :cond_2
    or-int/lit8 v8, v5, 0x1

    int-to-byte v5, v8

    .line 687
    :cond_3
    const/4 v6, 0x0

    .line 693
    .local v6, "start":I
    :goto_1
    array-length v8, p0

    if-ge v6, v8, :cond_4

    aget-byte v8, p0, v6

    if-ge v8, v4, :cond_4

    .line 694
    add-int/lit8 v6, v6, 0x1

    .line 695
    goto :goto_1

    .line 696
    :cond_4
    array-length v8, p0

    if-lt v6, v8, :cond_6

    .line 728
    :cond_5
    add-int/lit8 v8, v4, -0x1

    int-to-byte v4, v8

    if-ge v4, v5, :cond_3

    goto :goto_0

    .line 701
    :cond_6
    move v3, v6

    .local v3, "limit":I
    :cond_7
    add-int/lit8 v3, v3, 0x1

    array-length v8, p0

    if-ge v3, v8, :cond_8

    aget-byte v8, p0, v3

    if-ge v8, v4, :cond_7

    .line 714
    :cond_8
    add-int v8, v6, v3

    add-int/lit8 v7, v8, -0x1

    .line 718
    .local v7, "sumOfSosEos":I
    :cond_9
    aget v8, v2, v6

    sub-int v8, v7, v8

    aput v8, v2, v6

    .line 719
    add-int/lit8 v6, v6, 0x1

    if-lt v6, v3, :cond_9

    .line 722
    array-length v8, p0

    if-eq v3, v8, :cond_5

    .line 725
    add-int/lit8 v6, v3, 0x1

    .line 727
    goto :goto_1
.end method

.method static reorderVisual([B)[I
    .locals 11
    .param p0, "levels"    # [B

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 734
    new-array v1, v10, [B

    .line 735
    .local v1, "aMinLevel":[B
    new-array v0, v10, [B

    .line 739
    .local v0, "aMaxLevel":[B
    invoke-static {p0, v1, v0}, Lcom/ibm/icu/text/BidiLine;->prepareReorder([B[B[B)[I

    move-result-object v3

    .line 740
    .local v3, "indexMap":[I
    if-nez v3, :cond_1

    .line 741
    const/4 v3, 0x0

    .line 797
    .end local v3    # "indexMap":[I
    :cond_0
    :goto_0
    return-object v3

    .line 744
    .restart local v3    # "indexMap":[I
    :cond_1
    aget-byte v6, v1, v9

    .line 745
    .local v6, "minLevel":B
    aget-byte v5, v0, v9

    .line 748
    .local v5, "maxLevel":B
    if-ne v6, v5, :cond_2

    and-int/lit8 v9, v6, 0x1

    if-eqz v9, :cond_0

    .line 753
    :cond_2
    or-int/lit8 v9, v6, 0x1

    int-to-byte v6, v9

    .line 757
    :cond_3
    const/4 v7, 0x0

    .line 763
    .local v7, "start":I
    :goto_1
    array-length v9, p0

    if-ge v7, v9, :cond_4

    aget-byte v9, p0, v7

    if-ge v9, v5, :cond_4

    .line 764
    add-int/lit8 v7, v7, 0x1

    .line 765
    goto :goto_1

    .line 766
    :cond_4
    array-length v9, p0

    if-lt v7, v9, :cond_6

    .line 795
    :cond_5
    add-int/lit8 v9, v5, -0x1

    int-to-byte v5, v9

    if-ge v5, v6, :cond_3

    goto :goto_0

    .line 771
    :cond_6
    move v4, v7

    .local v4, "limit":I
    :cond_7
    add-int/lit8 v4, v4, 0x1

    array-length v9, p0

    if-ge v4, v9, :cond_8

    aget-byte v9, p0, v4

    if-ge v9, v5, :cond_7

    .line 779
    :cond_8
    add-int/lit8 v2, v4, -0x1

    .line 780
    .local v2, "end":I
    :goto_2
    if-ge v7, v2, :cond_9

    .line 781
    aget v8, v3, v7

    .line 782
    .local v8, "temp":I
    aget v9, v3, v2

    aput v9, v3, v7

    .line 783
    aput v8, v3, v2

    .line 785
    add-int/lit8 v7, v7, 0x1

    .line 786
    add-int/lit8 v2, v2, -0x1

    .line 787
    goto :goto_2

    .line 789
    .end local v8    # "temp":I
    :cond_9
    array-length v9, p0

    if-eq v4, v9, :cond_5

    .line 792
    add-int/lit8 v7, v4, 0x1

    .line 794
    goto :goto_1
.end method

.method static setLine(Lcom/ibm/icu/text/Bidi;II)Lcom/ibm/icu/text/Bidi;
    .locals 11
    .param p0, "paraBidi"    # Lcom/ibm/icu/text/Bidi;
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    .line 117
    new-instance v5, Lcom/ibm/icu/text/Bidi;

    invoke-direct {v5}, Lcom/ibm/icu/text/Bidi;-><init>()V

    .line 125
    .local v5, "lineBidi":Lcom/ibm/icu/text/Bidi;
    sub-int v2, p2, p1

    iput v2, v5, Lcom/ibm/icu/text/Bidi;->resultLength:I

    iput v2, v5, Lcom/ibm/icu/text/Bidi;->originalLength:I

    iput v2, v5, Lcom/ibm/icu/text/Bidi;->length:I

    .line 128
    .local v2, "length":I
    new-array v7, v2, [C

    iput-object v7, v5, Lcom/ibm/icu/text/Bidi;->text:[C

    .line 129
    iget-object v7, p0, Lcom/ibm/icu/text/Bidi;->text:[C

    iget-object v8, v5, Lcom/ibm/icu/text/Bidi;->text:[C

    invoke-static {v7, p1, v8, v9, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v7

    iput-byte v7, v5, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    .line 131
    iget v7, p0, Lcom/ibm/icu/text/Bidi;->paraCount:I

    iput v7, v5, Lcom/ibm/icu/text/Bidi;->paraCount:I

    .line 132
    new-array v7, v9, [Lcom/ibm/icu/text/BidiRun;

    iput-object v7, v5, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    .line 133
    iget v7, p0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    iput v7, v5, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    .line 134
    iget v7, p0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    iput v7, v5, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    .line 135
    iget v7, p0, Lcom/ibm/icu/text/Bidi;->controlCount:I

    if-lez v7, :cond_2

    .line 137
    move v1, p1

    .local v1, "j":I
    :goto_0
    if-ge v1, p2, :cond_1

    .line 138
    iget-object v7, p0, Lcom/ibm/icu/text/Bidi;->text:[C

    aget-char v7, v7, v1

    invoke-static {v7}, Lcom/ibm/icu/text/Bidi;->IsBidiControlChar(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 139
    iget v7, v5, Lcom/ibm/icu/text/Bidi;->controlCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, v5, Lcom/ibm/icu/text/Bidi;->controlCount:I

    .line 137
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 142
    :cond_1
    iget v7, v5, Lcom/ibm/icu/text/Bidi;->resultLength:I

    iget v8, v5, Lcom/ibm/icu/text/Bidi;->controlCount:I

    sub-int/2addr v7, v8

    iput v7, v5, Lcom/ibm/icu/text/Bidi;->resultLength:I

    .line 145
    .end local v1    # "j":I
    :cond_2
    invoke-virtual {v5, v2}, Lcom/ibm/icu/text/Bidi;->getDirPropsMemory(I)V

    .line 146
    iget-object v7, v5, Lcom/ibm/icu/text/Bidi;->dirPropsMemory:[B

    iput-object v7, v5, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    .line 147
    iget-object v7, p0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    iget-object v8, v5, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    invoke-static {v7, p1, v8, v9, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 150
    invoke-virtual {v5, v2}, Lcom/ibm/icu/text/Bidi;->getLevelsMemory(I)V

    .line 151
    iget-object v7, v5, Lcom/ibm/icu/text/Bidi;->levelsMemory:[B

    iput-object v7, v5, Lcom/ibm/icu/text/Bidi;->levels:[B

    .line 152
    iget-object v7, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    iget-object v8, v5, Lcom/ibm/icu/text/Bidi;->levels:[B

    invoke-static {v7, p1, v8, v9, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 154
    const/4 v7, -0x1

    iput v7, v5, Lcom/ibm/icu/text/Bidi;->runCount:I

    .line 156
    iget-byte v7, p0, Lcom/ibm/icu/text/Bidi;->direction:B

    if-eq v7, v10, :cond_5

    .line 158
    iget-byte v7, p0, Lcom/ibm/icu/text/Bidi;->direction:B

    iput-byte v7, v5, Lcom/ibm/icu/text/Bidi;->direction:B

    .line 165
    iget v7, p0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    if-gt v7, p1, :cond_3

    .line 166
    iput v9, v5, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    .line 233
    :goto_1
    iput-object p0, v5, Lcom/ibm/icu/text/Bidi;->paraBidi:Lcom/ibm/icu/text/Bidi;

    .line 234
    return-object v5

    .line 167
    :cond_3
    iget v7, p0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    if-ge v7, p2, :cond_4

    .line 168
    iget v7, p0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    sub-int/2addr v7, p1

    iput v7, v5, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    goto :goto_1

    .line 170
    :cond_4
    iput v2, v5, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    goto :goto_1

    .line 173
    :cond_5
    iget-object v4, v5, Lcom/ibm/icu/text/Bidi;->levels:[B

    .line 177
    .local v4, "levels":[B
    invoke-static {v5}, Lcom/ibm/icu/text/BidiLine;->setTrailingWSStart(Lcom/ibm/icu/text/Bidi;)V

    .line 178
    iget v6, v5, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    .line 181
    .local v6, "trailingWSStart":I
    if-nez v6, :cond_6

    .line 183
    iget-byte v7, v5, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    and-int/lit8 v7, v7, 0x1

    int-to-byte v7, v7

    iput-byte v7, v5, Lcom/ibm/icu/text/Bidi;->direction:B

    .line 211
    :goto_2
    iget-byte v7, v5, Lcom/ibm/icu/text/Bidi;->direction:B

    packed-switch v7, :pswitch_data_0

    goto :goto_1

    .line 214
    :pswitch_0
    iget-byte v7, v5, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    add-int/lit8 v7, v7, 0x1

    and-int/lit8 v7, v7, -0x2

    int-to-byte v7, v7

    iput-byte v7, v5, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    .line 219
    iput v9, v5, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    goto :goto_1

    .line 186
    :cond_6
    aget-byte v7, v4, v9

    and-int/lit8 v7, v7, 0x1

    int-to-byte v3, v7

    .line 190
    .local v3, "level":B
    if-ge v6, v2, :cond_7

    iget-byte v7, v5, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    and-int/lit8 v7, v7, 0x1

    if-eq v7, v3, :cond_7

    .line 194
    iput-byte v10, v5, Lcom/ibm/icu/text/Bidi;->direction:B

    goto :goto_2

    .line 198
    :cond_7
    const/4 v0, 0x1

    .line 199
    .local v0, "i":I
    :goto_3
    if-ne v0, v6, :cond_8

    .line 201
    iput-byte v3, v5, Lcom/ibm/icu/text/Bidi;->direction:B

    goto :goto_2

    .line 203
    :cond_8
    aget-byte v7, v4, v0

    and-int/lit8 v7, v7, 0x1

    if-eq v7, v3, :cond_9

    .line 204
    iput-byte v10, v5, Lcom/ibm/icu/text/Bidi;->direction:B

    goto :goto_2

    .line 198
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 223
    .end local v0    # "i":I
    .end local v3    # "level":B
    :pswitch_1
    iget-byte v7, v5, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    or-int/lit8 v7, v7, 0x1

    int-to-byte v7, v7

    iput-byte v7, v5, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    .line 227
    iput v9, v5, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    goto :goto_1

    .line 211
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static setTrailingWSStart(Lcom/ibm/icu/text/Bidi;)V
    .locals 6
    .param p0, "bidi"    # Lcom/ibm/icu/text/Bidi;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    .line 86
    .local v0, "dirProps":[B
    iget-object v1, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    .line 87
    .local v1, "levels":[B
    iget v3, p0, Lcom/ibm/icu/text/Bidi;->length:I

    .line 88
    .local v3, "start":I
    iget-byte v2, p0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    .line 96
    .local v2, "paraLevel":B
    add-int/lit8 v4, v3, -0x1

    aget-byte v4, v0, v4

    invoke-static {v4}, Lcom/ibm/icu/text/Bidi;->NoContextRTL(B)B

    move-result v4

    const/4 v5, 0x7

    if-ne v4, v5, :cond_0

    .line 97
    iput v3, p0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    .line 112
    :goto_0
    return-void

    .line 102
    :cond_0
    :goto_1
    if-lez v3, :cond_1

    add-int/lit8 v4, v3, -0x1

    aget-byte v4, v0, v4

    invoke-static {v4}, Lcom/ibm/icu/text/Bidi;->DirPropFlagNC(B)I

    move-result v4

    sget v5, Lcom/ibm/icu/text/Bidi;->MASK_WS:I

    and-int/2addr v4, v5

    if-eqz v4, :cond_1

    .line 103
    add-int/lit8 v3, v3, -0x1

    .line 104
    goto :goto_1

    .line 107
    :cond_1
    :goto_2
    if-lez v3, :cond_2

    add-int/lit8 v4, v3, -0x1

    aget-byte v4, v1, v4

    if-ne v4, v2, :cond_2

    .line 108
    add-int/lit8 v3, v3, -0x1

    .line 109
    goto :goto_2

    .line 111
    :cond_2
    iput v3, p0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    goto :goto_0
.end method
