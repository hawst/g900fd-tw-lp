.class Lcom/ibm/icu/text/NameUnicodeTransliterator;
.super Lcom/ibm/icu/text/Transliterator;
.source "NameUnicodeTransliterator.java"


# static fields
.field static final CLOSE_DELIM:C = '}'

.field static final OPEN_DELIM:C = '\\'

.field static final OPEN_PAT:Ljava/lang/String; = "\\N~{~"

.field static final SPACE:C = ' '

.field static final _ID:Ljava/lang/String; = "Name-Any"


# instance fields
.field closeDelimiter:C

.field openDelimiter:C


# direct methods
.method public constructor <init>(Lcom/ibm/icu/text/UnicodeFilter;)V
    .locals 1
    .param p1, "filter"    # Lcom/ibm/icu/text/UnicodeFilter;

    .prologue
    .line 43
    const-string/jumbo v0, "Name-Any"

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/Transliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 44
    return-void
.end method

.method static register()V
    .locals 2

    .prologue
    .line 32
    const-string/jumbo v0, "Name-Any"

    new-instance v1, Lcom/ibm/icu/text/NameUnicodeTransliterator$1;

    invoke-direct {v1}, Lcom/ibm/icu/text/NameUnicodeTransliterator$1;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 37
    return-void
.end method


# virtual methods
.method protected handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V
    .locals 15
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "offsets"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "isIncremental"    # Z

    .prologue
    .line 52
    invoke-static {}, Lcom/ibm/icu/impl/UCharacterName;->getInstance()Lcom/ibm/icu/impl/UCharacterName;

    move-result-object v13

    invoke-virtual {v13}, Lcom/ibm/icu/impl/UCharacterName;->getMaxCharNameLength()I

    move-result v13

    add-int/lit8 v8, v13, 0x1

    .line 54
    .local v8, "maxLen":I
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10, v8}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 57
    .local v10, "name":Ljava/lang/StringBuffer;
    new-instance v5, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v5}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 58
    .local v5, "legal":Lcom/ibm/icu/text/UnicodeSet;
    invoke-static {}, Lcom/ibm/icu/impl/UCharacterName;->getInstance()Lcom/ibm/icu/impl/UCharacterName;

    move-result-object v13

    invoke-virtual {v13, v5}, Lcom/ibm/icu/impl/UCharacterName;->getCharNameCharacters(Lcom/ibm/icu/text/UnicodeSet;)V

    .line 60
    move-object/from16 v0, p2

    iget v2, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 61
    .local v2, "cursor":I
    move-object/from16 v0, p2

    iget v7, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 66
    .local v7, "limit":I
    const/4 v9, 0x0

    .line 67
    .local v9, "mode":I
    const/4 v11, -0x1

    .line 70
    .local v11, "openPos":I
    :goto_0
    if-ge v2, v7, :cond_6

    .line 71
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v1

    .line 73
    .local v1, "c":I
    packed-switch v9, :pswitch_data_0

    .line 162
    :cond_0
    :goto_1
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v13

    add-int/2addr v2, v13

    .line 163
    goto :goto_0

    .line 75
    :pswitch_0
    const/16 v13, 0x5c

    if-ne v1, v13, :cond_0

    .line 76
    move v11, v2

    .line 77
    const-string/jumbo v13, "\\N~{~"

    move-object/from16 v0, p1

    invoke-static {v13, v0, v2, v7}, Lcom/ibm/icu/impl/Utility;->parsePattern(Ljava/lang/String;Lcom/ibm/icu/text/Replaceable;II)I

    move-result v4

    .line 78
    .local v4, "i":I
    if-ltz v4, :cond_0

    if-ge v4, v7, :cond_0

    .line 79
    const/4 v9, 0x1

    .line 80
    const/4 v13, 0x0

    invoke-virtual {v10, v13}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 81
    move v2, v4

    .line 82
    goto :goto_0

    .line 95
    .end local v4    # "i":I
    :pswitch_1
    invoke-static {v1}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 97
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    if-lez v13, :cond_0

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    invoke-virtual {v10, v13}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    const/16 v14, 0x20

    if-eq v13, v14, :cond_0

    .line 99
    const/16 v13, 0x20

    invoke-virtual {v10, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 102
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    if-le v13, v8, :cond_0

    .line 103
    const/4 v9, 0x0

    .line 104
    goto :goto_1

    .line 109
    :cond_1
    const/16 v13, 0x7d

    if-ne v1, v13, :cond_4

    .line 111
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    .line 114
    .local v6, "len":I
    if-lez v6, :cond_2

    add-int/lit8 v13, v6, -0x1

    invoke-virtual {v10, v13}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    const/16 v14, 0x20

    if-ne v13, v14, :cond_2

    .line 116
    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v10, v6}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 119
    :cond_2
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/ibm/icu/lang/UCharacter;->getCharFromExtendedName(Ljava/lang/String;)I

    move-result v1

    .line 120
    const/4 v13, -0x1

    if-eq v1, v13, :cond_3

    .line 124
    add-int/lit8 v2, v2, 0x1

    .line 126
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    .line 127
    .local v12, "str":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-interface {v0, v11, v2, v12}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 132
    sub-int v13, v2, v11

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v14

    sub-int v3, v13, v14

    .line 133
    .local v3, "delta":I
    sub-int/2addr v2, v3

    .line 134
    sub-int/2addr v7, v3

    .line 139
    .end local v3    # "delta":I
    .end local v12    # "str":Ljava/lang/String;
    :cond_3
    const/4 v9, 0x0

    .line 140
    const/4 v11, -0x1

    .line 141
    goto/16 :goto_0

    .line 144
    .end local v6    # "len":I
    :cond_4
    invoke-virtual {v5, v1}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 145
    invoke-static {v10, v1}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 148
    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    if-lt v13, v8, :cond_0

    .line 149
    const/4 v9, 0x0

    .line 150
    goto/16 :goto_1

    .line 155
    :cond_5
    add-int/lit8 v2, v2, -0x1

    .line 156
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 165
    .end local v1    # "c":I
    :cond_6
    move-object/from16 v0, p2

    iget v13, v0, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    move-object/from16 v0, p2

    iget v14, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    sub-int v14, v7, v14

    add-int/2addr v13, v14

    move-object/from16 v0, p2

    iput v13, v0, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    .line 166
    move-object/from16 v0, p2

    iput v7, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 169
    if-eqz p3, :cond_7

    if-ltz v11, :cond_7

    .end local v11    # "openPos":I
    :goto_2
    move-object/from16 v0, p2

    iput v11, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 170
    return-void

    .restart local v11    # "openPos":I
    :cond_7
    move v11, v2

    .line 169
    goto :goto_2

    .line 73
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
