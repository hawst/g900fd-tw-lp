.class Lcom/ibm/icu/text/TransliteratorIDParser;
.super Ljava/lang/Object;
.source "TransliteratorIDParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;,
        Lcom/ibm/icu/text/TransliteratorIDParser$Specs;
    }
.end annotation


# static fields
.field private static final ANY:Ljava/lang/String; = "Any"

.field private static final CLOSE_REV:C = ')'

.field private static final FORWARD:I = 0x0

.field private static final ID_DELIM:C = ';'

.field private static final OPEN_REV:C = '('

.field private static final REVERSE:I = 0x1

.field private static final SPECIAL_INVERSES:Ljava/util/Hashtable;

.field private static final TARGET_SEP:C = '-'

.field private static final VARIANT_SEP:C = '/'


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/TransliteratorIDParser;->SPECIAL_INVERSES:Ljava/util/Hashtable;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    return-void
.end method

.method public static IDtoSTV(Ljava/lang/String;)[Ljava/lang/String;
    .locals 11
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 475
    const-string/jumbo v3, "Any"

    .line 476
    .local v3, "source":Ljava/lang/String;
    const/4 v4, 0x0

    .line 477
    .local v4, "target":Ljava/lang/String;
    const-string/jumbo v6, ""

    .line 479
    .local v6, "variant":Ljava/lang/String;
    const/16 v7, 0x2d

    invoke-virtual {p0, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 480
    .local v1, "sep":I
    const/16 v7, 0x2f

    invoke-virtual {p0, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 481
    .local v5, "var":I
    if-gez v5, :cond_0

    .line 482
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .line 484
    :cond_0
    const/4 v0, 0x0

    .line 486
    .local v0, "isSourcePresent":Z
    if-gez v1, :cond_2

    .line 488
    invoke-virtual {p0, v9, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 489
    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 508
    :goto_0
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_1

    .line 509
    invoke-virtual {v6, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 512
    :cond_1
    const/4 v7, 0x4

    new-array v8, v7, [Ljava/lang/String;

    aput-object v3, v8, v9

    aput-object v4, v8, v10

    const/4 v7, 0x2

    aput-object v6, v8, v7

    const/4 v9, 0x3

    if-eqz v0, :cond_6

    const-string/jumbo v7, ""

    :goto_1
    aput-object v7, v8, v9

    return-object v8

    .line 490
    :cond_2
    if-ge v1, v5, :cond_4

    .line 492
    if-lez v1, :cond_3

    .line 493
    invoke-virtual {p0, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 494
    const/4 v0, 0x1

    .line 496
    :cond_3
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 497
    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 498
    goto :goto_0

    .line 500
    :cond_4
    if-lez v5, :cond_5

    .line 501
    invoke-virtual {p0, v9, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 502
    const/4 v0, 0x1

    .line 504
    :cond_5
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "sep":I
    .local v2, "sep":I
    invoke-virtual {p0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 505
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    move v1, v2

    .end local v2    # "sep":I
    .restart local v1    # "sep":I
    goto :goto_0

    .line 512
    :cond_6
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public static STVtoID(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "variant"    # Ljava/lang/String;

    .prologue
    .line 524
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 525
    .local v0, "id":Ljava/lang/StringBuffer;
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 526
    const-string/jumbo v1, "Any"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 528
    :cond_0
    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 529
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    .line 530
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 532
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static instantiateList(Ljava/util/Vector;)V
    .locals 6
    .param p0, "list"    # Ljava/util/Vector;

    .prologue
    .line 431
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v3

    if-gt v0, v3, :cond_0

    .line 434
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v3

    if-ne v0, v3, :cond_1

    .line 453
    :cond_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v3

    if-nez v3, :cond_5

    .line 454
    const-string/jumbo v3, "Any-Null"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/ibm/icu/text/Transliterator;->getBasicInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;

    move-result-object v2

    .line 455
    .local v2, "t":Lcom/ibm/icu/text/Transliterator;
    if-nez v2, :cond_4

    .line 457
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "Internal error; cannot instantiate Any-Null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 438
    .end local v2    # "t":Lcom/ibm/icu/text/Transliterator;
    :cond_1
    invoke-virtual {p0, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    .line 439
    .local v1, "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    iget-object v3, v1, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->basicID:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    .line 440
    invoke-virtual {p0, v0}, Ljava/util/Vector;->removeElementAt(I)V

    goto :goto_0

    .line 442
    :cond_2
    invoke-virtual {v1}, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->getInstance()Lcom/ibm/icu/text/Transliterator;

    move-result-object v2

    .line 443
    .restart local v2    # "t":Lcom/ibm/icu/text/Transliterator;
    if-nez v2, :cond_3

    .line 444
    invoke-virtual {v1}, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->getInstance()Lcom/ibm/icu/text/Transliterator;

    move-result-object v2

    .line 445
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "Illegal ID "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, v1, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->canonID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 447
    :cond_3
    invoke-virtual {p0, v2, v0}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    .line 448
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 459
    .end local v1    # "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    :cond_4
    invoke-virtual {p0, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 461
    .end local v2    # "t":Lcom/ibm/icu/text/Transliterator;
    :cond_5
    return-void
.end method

.method public static parseCompoundID(Ljava/lang/String;ILjava/lang/StringBuffer;Ljava/util/Vector;[Lcom/ibm/icu/text/UnicodeSet;)Z
    .locals 10
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "dir"    # I
    .param p2, "canonID"    # Ljava/lang/StringBuffer;
    .param p3, "list"    # Ljava/util/Vector;
    .param p4, "globalFilter"    # [Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    const/16 v9, 0x3b

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 340
    new-array v2, v7, [I

    aput v6, v2, v6

    .line 341
    .local v2, "pos":[I
    new-array v5, v7, [I

    .line 342
    .local v5, "withParens":[I
    invoke-virtual {p3}, Ljava/util/Vector;->removeAllElements()V

    .line 344
    const/4 v8, 0x0

    aput-object v8, p4, v6

    .line 345
    invoke-virtual {p2, v6}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 348
    aput v6, v5, v6

    .line 349
    invoke-static {p0, v2, p1, v5, p2}, Lcom/ibm/icu/text/TransliteratorIDParser;->parseGlobalFilter(Ljava/lang/String;[II[ILjava/lang/StringBuffer;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    .line 350
    .local v0, "filter":Lcom/ibm/icu/text/UnicodeSet;
    if-eqz v0, :cond_1

    .line 351
    invoke-static {p0, v2, v9}, Lcom/ibm/icu/impl/Utility;->parseChar(Ljava/lang/String;[IC)Z

    move-result v8

    if-nez v8, :cond_0

    .line 353
    invoke-virtual {p2, v6}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 354
    aput v6, v2, v6

    .line 356
    :cond_0
    if-nez p1, :cond_1

    .line 357
    aput-object v0, p4, v6

    .line 361
    :cond_1
    const/4 v3, 0x1

    .line 363
    .local v3, "sawDelimiter":Z
    :cond_2
    invoke-static {p0, v2, p1}, Lcom/ibm/icu/text/TransliteratorIDParser;->parseSingleID(Ljava/lang/String;[II)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    move-result-object v4

    .line 364
    .local v4, "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    if-nez v4, :cond_4

    .line 378
    :goto_0
    invoke-virtual {p3}, Ljava/util/Vector;->size()I

    move-result v8

    if-nez v8, :cond_6

    .line 412
    :cond_3
    :goto_1
    return v6

    .line 367
    :cond_4
    if-nez p1, :cond_5

    .line 368
    invoke-virtual {p3, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 372
    :goto_2
    invoke-static {p0, v2, v9}, Lcom/ibm/icu/impl/Utility;->parseChar(Ljava/lang/String;[IC)Z

    move-result v8

    if-nez v8, :cond_2

    .line 373
    const/4 v3, 0x0

    .line 374
    goto :goto_0

    .line 370
    :cond_5
    invoke-virtual {p3, v4, v6}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_2

    .line 383
    :cond_6
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    invoke-virtual {p3}, Ljava/util/Vector;->size()I

    move-result v8

    if-ge v1, v8, :cond_8

    .line 384
    invoke-virtual {p3, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    check-cast v4, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    .line 385
    .restart local v4    # "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    iget-object v8, v4, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->canonID:Ljava/lang/String;

    invoke-virtual {p2, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 386
    invoke-virtual {p3}, Ljava/util/Vector;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-eq v1, v8, :cond_7

    .line 387
    invoke-virtual {p2, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 383
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 393
    :cond_8
    if-eqz v3, :cond_9

    .line 394
    aput v7, v5, v6

    .line 395
    invoke-static {p0, v2, p1, v5, p2}, Lcom/ibm/icu/text/TransliteratorIDParser;->parseGlobalFilter(Ljava/lang/String;[II[ILjava/lang/StringBuffer;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    .line 396
    if-eqz v0, :cond_9

    .line 398
    invoke-static {p0, v2, v9}, Lcom/ibm/icu/impl/Utility;->parseChar(Ljava/lang/String;[IC)Z

    .line 400
    if-ne p1, v7, :cond_9

    .line 401
    aput-object v0, p4, v6

    .line 407
    :cond_9
    aget v8, v2, v6

    invoke-static {p0, v8}, Lcom/ibm/icu/impl/Utility;->skipWhitespace(Ljava/lang/String;I)I

    .line 408
    aget v8, v2, v6

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v9

    if-ne v8, v9, :cond_3

    move v6, v7

    .line 412
    goto :goto_1
.end method

.method public static parseFilterID(Ljava/lang/String;[I)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    .locals 5
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "pos"    # [I

    .prologue
    const/4 v4, 0x0

    .line 144
    aget v2, p1, v4

    .line 145
    .local v2, "start":I
    const/4 v3, 0x1

    invoke-static {p0, p1, v3}, Lcom/ibm/icu/text/TransliteratorIDParser;->parseFilterID(Ljava/lang/String;[IZ)Lcom/ibm/icu/text/TransliteratorIDParser$Specs;

    move-result-object v1

    .line 146
    .local v1, "specs":Lcom/ibm/icu/text/TransliteratorIDParser$Specs;
    if-nez v1, :cond_0

    .line 147
    aput v2, p1, v4

    .line 148
    const/4 v0, 0x0

    .line 154
    :goto_0
    return-object v0

    .line 152
    :cond_0
    invoke-static {v1, v4}, Lcom/ibm/icu/text/TransliteratorIDParser;->specsToID(Lcom/ibm/icu/text/TransliteratorIDParser$Specs;I)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    move-result-object v0

    .line 153
    .local v0, "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    iget-object v3, v1, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->filter:Ljava/lang/String;

    iput-object v3, v0, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->filter:Ljava/lang/String;

    goto :goto_0
.end method

.method private static parseFilterID(Ljava/lang/String;[IZ)Lcom/ibm/icu/text/TransliteratorIDParser$Specs;
    .locals 14
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "pos"    # [I
    .param p2, "allowFilter"    # Z

    .prologue
    .line 601
    const/4 v8, 0x0

    .line 602
    .local v8, "first":Ljava/lang/String;
    const/4 v1, 0x0

    .line 603
    .local v1, "source":Ljava/lang/String;
    const/4 v2, 0x0

    .line 604
    .local v2, "target":Ljava/lang/String;
    const/4 v3, 0x0

    .line 605
    .local v3, "variant":Ljava/lang/String;
    const/4 v5, 0x0

    .line 606
    .local v5, "filter":Ljava/lang/String;
    const/4 v7, 0x0

    .line 607
    .local v7, "delimiter":C
    const/4 v11, 0x0

    .line 608
    .local v11, "specCount":I
    const/4 v0, 0x0

    aget v12, p1, v0

    .line 614
    .local v12, "start":I
    :goto_0
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->skipWhitespace(Ljava/lang/String;[I)V

    .line 615
    const/4 v0, 0x0

    aget v0, p1, v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v13

    if-ne v0, v13, :cond_2

    .line 673
    :cond_0
    if-eqz v8, :cond_1

    .line 674
    if-nez v2, :cond_8

    .line 675
    move-object v2, v8

    .line 682
    :cond_1
    :goto_1
    if-nez v1, :cond_9

    if-nez v2, :cond_9

    .line 683
    const/4 v0, 0x0

    aput v12, p1, v0

    .line 684
    const/4 v0, 0x0

    .line 697
    :goto_2
    return-object v0

    .line 620
    :cond_2
    if-eqz p2, :cond_3

    if-nez v5, :cond_3

    const/4 v0, 0x0

    aget v0, p1, v0

    invoke-static {p0, v0}, Lcom/ibm/icu/text/UnicodeSet;->resemblesPattern(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 623
    new-instance v9, Ljava/text/ParsePosition;

    const/4 v0, 0x0

    aget v0, p1, v0

    invoke-direct {v9, v0}, Ljava/text/ParsePosition;-><init>(I)V

    .line 625
    .local v9, "ppos":Ljava/text/ParsePosition;
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const/4 v13, 0x0

    invoke-direct {v0, p0, v9, v13}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;)V

    .line 626
    const/4 v0, 0x0

    aget v0, p1, v0

    invoke-virtual {v9}, Ljava/text/ParsePosition;->getIndex()I

    move-result v13

    invoke-virtual {p0, v0, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 627
    const/4 v0, 0x0

    invoke-virtual {v9}, Ljava/text/ParsePosition;->getIndex()I

    move-result v13

    aput v13, p1, v0

    goto :goto_0

    .line 631
    .end local v9    # "ppos":Ljava/text/ParsePosition;
    :cond_3
    if-nez v7, :cond_6

    .line 632
    const/4 v0, 0x0

    aget v0, p1, v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 633
    .local v6, "c":C
    const/16 v0, 0x2d

    if-ne v6, v0, :cond_4

    if-eqz v2, :cond_5

    :cond_4
    const/16 v0, 0x2f

    if-ne v6, v0, :cond_6

    if-nez v3, :cond_6

    .line 635
    :cond_5
    move v7, v6

    .line 636
    const/4 v0, 0x0

    aget v13, p1, v0

    add-int/lit8 v13, v13, 0x1

    aput v13, p1, v0

    goto :goto_0

    .line 644
    .end local v6    # "c":C
    :cond_6
    if-nez v7, :cond_7

    if-gtz v11, :cond_0

    .line 648
    :cond_7
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->parseUnicodeIdentifier(Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object v10

    .line 649
    .local v10, "spec":Ljava/lang/String;
    if-eqz v10, :cond_0

    .line 656
    sparse-switch v7, :sswitch_data_0

    .line 667
    :goto_3
    add-int/lit8 v11, v11, 0x1

    .line 668
    const/4 v7, 0x0

    .line 669
    goto :goto_0

    .line 658
    :sswitch_0
    move-object v8, v10

    .line 659
    goto :goto_3

    .line 661
    :sswitch_1
    move-object v2, v10

    .line 662
    goto :goto_3

    .line 664
    :sswitch_2
    move-object v3, v10

    goto :goto_3

    .line 677
    .end local v10    # "spec":Ljava/lang/String;
    :cond_8
    move-object v1, v8

    goto :goto_1

    .line 688
    :cond_9
    const/4 v4, 0x1

    .line 689
    .local v4, "sawSource":Z
    if-nez v1, :cond_a

    .line 690
    const-string/jumbo v1, "Any"

    .line 691
    const/4 v4, 0x0

    .line 693
    :cond_a
    if-nez v2, :cond_b

    .line 694
    const-string/jumbo v2, "Any"

    .line 697
    :cond_b
    new-instance v0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_2

    .line 656
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2d -> :sswitch_1
        0x2f -> :sswitch_2
    .end sparse-switch
.end method

.method public static parseGlobalFilter(Ljava/lang/String;[II[ILjava/lang/StringBuffer;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 9
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "pos"    # [I
    .param p2, "dir"    # I
    .param p3, "withParens"    # [I
    .param p4, "canonID"    # Ljava/lang/StringBuffer;

    .prologue
    .line 262
    const/4 v1, 0x0

    .line 263
    .local v1, "filter":Lcom/ibm/icu/text/UnicodeSet;
    const/4 v6, 0x0

    aget v5, p1, v6

    .line 265
    .local v5, "start":I
    const/4 v6, 0x0

    aget v6, p3, v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_2

    .line 266
    const/4 v7, 0x0

    const/16 v6, 0x28

    invoke-static {p0, p1, v6}, Lcom/ibm/icu/impl/Utility;->parseChar(Ljava/lang/String;[IC)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x1

    :goto_0
    aput v6, p3, v7

    .line 274
    :cond_0
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->skipWhitespace(Ljava/lang/String;[I)V

    .line 276
    const/4 v6, 0x0

    aget v6, p1, v6

    invoke-static {p0, v6}, Lcom/ibm/icu/text/UnicodeSet;->resemblesPattern(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 277
    new-instance v4, Ljava/text/ParsePosition;

    const/4 v6, 0x0

    aget v6, p1, v6

    invoke-direct {v4, v6}, Ljava/text/ParsePosition;-><init>(I)V

    .line 279
    .local v4, "ppos":Ljava/text/ParsePosition;
    :try_start_0
    new-instance v2, Lcom/ibm/icu/text/UnicodeSet;

    const/4 v6, 0x0

    invoke-direct {v2, p0, v4, v6}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    .end local v1    # "filter":Lcom/ibm/icu/text/UnicodeSet;
    .local v2, "filter":Lcom/ibm/icu/text/UnicodeSet;
    const/4 v6, 0x0

    aget v6, p1, v6

    invoke-virtual {v4}, Ljava/text/ParsePosition;->getIndex()I

    move-result v7

    invoke-virtual {p0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 286
    .local v3, "pattern":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-virtual {v4}, Ljava/text/ParsePosition;->getIndex()I

    move-result v7

    aput v7, p1, v6

    .line 288
    const/4 v6, 0x0

    aget v6, p3, v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    const/16 v6, 0x29

    invoke-static {p0, p1, v6}, Lcom/ibm/icu/impl/Utility;->parseChar(Ljava/lang/String;[IC)Z

    move-result v6

    if-nez v6, :cond_3

    .line 289
    const/4 v6, 0x0

    aput v5, p1, v6

    .line 290
    const/4 v1, 0x0

    move-object v6, v1

    move-object v1, v2

    .line 311
    .end local v2    # "filter":Lcom/ibm/icu/text/UnicodeSet;
    .end local v3    # "pattern":Ljava/lang/String;
    .end local v4    # "ppos":Ljava/text/ParsePosition;
    .restart local v1    # "filter":Lcom/ibm/icu/text/UnicodeSet;
    :goto_1
    return-object v6

    .line 266
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 267
    :cond_2
    const/4 v6, 0x0

    aget v6, p3, v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 268
    const/16 v6, 0x28

    invoke-static {p0, p1, v6}, Lcom/ibm/icu/impl/Utility;->parseChar(Ljava/lang/String;[IC)Z

    move-result v6

    if-nez v6, :cond_0

    .line 269
    const/4 v6, 0x0

    aput v5, p1, v6

    .line 270
    const/4 v6, 0x0

    goto :goto_1

    .line 280
    .restart local v4    # "ppos":Ljava/text/ParsePosition;
    :catch_0
    move-exception v0

    .line 281
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v6, 0x0

    aput v5, p1, v6

    .line 282
    const/4 v6, 0x0

    goto :goto_1

    .line 296
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v1    # "filter":Lcom/ibm/icu/text/UnicodeSet;
    .restart local v2    # "filter":Lcom/ibm/icu/text/UnicodeSet;
    .restart local v3    # "pattern":Ljava/lang/String;
    :cond_3
    if-eqz p4, :cond_8

    .line 297
    if-nez p2, :cond_6

    .line 298
    const/4 v6, 0x0

    aget v6, p3, v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_4

    .line 299
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const/16 v7, 0x28

    invoke-static {v7}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const/16 v7, 0x29

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 301
    :cond_4
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const/16 v7, 0x3b

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-object v1, v2

    .end local v2    # "filter":Lcom/ibm/icu/text/UnicodeSet;
    .end local v3    # "pattern":Ljava/lang/String;
    .end local v4    # "ppos":Ljava/text/ParsePosition;
    .restart local v1    # "filter":Lcom/ibm/icu/text/UnicodeSet;
    :cond_5
    :goto_2
    move-object v6, v1

    .line 311
    goto :goto_1

    .line 303
    .end local v1    # "filter":Lcom/ibm/icu/text/UnicodeSet;
    .restart local v2    # "filter":Lcom/ibm/icu/text/UnicodeSet;
    .restart local v3    # "pattern":Ljava/lang/String;
    .restart local v4    # "ppos":Ljava/text/ParsePosition;
    :cond_6
    const/4 v6, 0x0

    aget v6, p3, v6

    if-nez v6, :cond_7

    .line 304
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const/16 v7, 0x28

    invoke-static {v7}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const/16 v7, 0x29

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 306
    :cond_7
    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const/16 v8, 0x3b

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p4, v6, v7}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    :cond_8
    move-object v1, v2

    .end local v2    # "filter":Lcom/ibm/icu/text/UnicodeSet;
    .restart local v1    # "filter":Lcom/ibm/icu/text/UnicodeSet;
    goto :goto_2
.end method

.method public static parseSingleID(Ljava/lang/String;[II)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    .locals 12
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "pos"    # [I
    .param p2, "dir"    # I

    .prologue
    const/4 v11, 0x2

    const/16 v10, 0x28

    const/4 v7, 0x1

    const/16 v9, 0x29

    const/4 v8, 0x0

    .line 171
    aget v5, p1, v8

    .line 175
    .local v5, "start":I
    const/4 v3, 0x0

    .line 176
    .local v3, "specsA":Lcom/ibm/icu/text/TransliteratorIDParser$Specs;
    const/4 v4, 0x0

    .line 177
    .local v4, "specsB":Lcom/ibm/icu/text/TransliteratorIDParser$Specs;
    const/4 v1, 0x0

    .line 181
    .local v1, "sawParen":Z
    const/4 v0, 0x1

    .local v0, "pass":I
    :goto_0
    if-gt v0, v11, :cond_4

    .line 182
    if-ne v0, v11, :cond_1

    .line 183
    invoke-static {p0, p1, v7}, Lcom/ibm/icu/text/TransliteratorIDParser;->parseFilterID(Ljava/lang/String;[IZ)Lcom/ibm/icu/text/TransliteratorIDParser$Specs;

    move-result-object v3

    .line 184
    if-nez v3, :cond_1

    .line 185
    aput v5, p1, v8

    .line 186
    const/4 v2, 0x0

    .line 234
    :cond_0
    :goto_1
    return-object v2

    .line 189
    :cond_1
    invoke-static {p0, p1, v10}, Lcom/ibm/icu/impl/Utility;->parseChar(Ljava/lang/String;[IC)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 190
    const/4 v1, 0x1

    .line 191
    invoke-static {p0, p1, v9}, Lcom/ibm/icu/impl/Utility;->parseChar(Ljava/lang/String;[IC)Z

    move-result v6

    if-nez v6, :cond_4

    .line 192
    invoke-static {p0, p1, v7}, Lcom/ibm/icu/text/TransliteratorIDParser;->parseFilterID(Ljava/lang/String;[IZ)Lcom/ibm/icu/text/TransliteratorIDParser$Specs;

    move-result-object v4

    .line 194
    if-eqz v4, :cond_2

    invoke-static {p0, p1, v9}, Lcom/ibm/icu/impl/Utility;->parseChar(Ljava/lang/String;[IC)Z

    move-result v6

    if-nez v6, :cond_4

    .line 195
    :cond_2
    aput v5, p1, v8

    .line 196
    const/4 v2, 0x0

    goto :goto_1

    .line 181
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 205
    :cond_4
    if-eqz v1, :cond_6

    .line 206
    if-nez p2, :cond_5

    .line 207
    invoke-static {v3, v8}, Lcom/ibm/icu/text/TransliteratorIDParser;->specsToID(Lcom/ibm/icu/text/TransliteratorIDParser$Specs;I)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    move-result-object v2

    .line 208
    .local v2, "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v7, v2, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->canonID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-static {v4, v8}, Lcom/ibm/icu/text/TransliteratorIDParser;->specsToID(Lcom/ibm/icu/text/TransliteratorIDParser$Specs;I)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    move-result-object v7

    iget-object v7, v7, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->canonID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->canonID:Ljava/lang/String;

    .line 210
    if-eqz v3, :cond_0

    .line 211
    iget-object v6, v3, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->filter:Ljava/lang/String;

    iput-object v6, v2, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->filter:Ljava/lang/String;

    goto :goto_1

    .line 214
    .end local v2    # "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    :cond_5
    invoke-static {v4, v8}, Lcom/ibm/icu/text/TransliteratorIDParser;->specsToID(Lcom/ibm/icu/text/TransliteratorIDParser$Specs;I)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    move-result-object v2

    .line 215
    .restart local v2    # "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v7, v2, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->canonID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-static {v3, v8}, Lcom/ibm/icu/text/TransliteratorIDParser;->specsToID(Lcom/ibm/icu/text/TransliteratorIDParser$Specs;I)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    move-result-object v7

    iget-object v7, v7, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->canonID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->canonID:Ljava/lang/String;

    .line 217
    if-eqz v4, :cond_0

    .line 218
    iget-object v6, v4, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->filter:Ljava/lang/String;

    iput-object v6, v2, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->filter:Ljava/lang/String;

    goto :goto_1

    .line 223
    .end local v2    # "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    :cond_6
    if-nez p2, :cond_8

    .line 224
    invoke-static {v3, v8}, Lcom/ibm/icu/text/TransliteratorIDParser;->specsToID(Lcom/ibm/icu/text/TransliteratorIDParser$Specs;I)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    move-result-object v2

    .line 231
    .restart local v2    # "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    :cond_7
    :goto_2
    iget-object v6, v3, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->filter:Ljava/lang/String;

    iput-object v6, v2, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->filter:Ljava/lang/String;

    goto/16 :goto_1

    .line 226
    .end local v2    # "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    :cond_8
    invoke-static {v3}, Lcom/ibm/icu/text/TransliteratorIDParser;->specsToSpecialInverse(Lcom/ibm/icu/text/TransliteratorIDParser$Specs;)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    move-result-object v2

    .line 227
    .restart local v2    # "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    if-nez v2, :cond_7

    .line 228
    invoke-static {v3, v7}, Lcom/ibm/icu/text/TransliteratorIDParser;->specsToID(Lcom/ibm/icu/text/TransliteratorIDParser$Specs;I)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    move-result-object v2

    goto :goto_2
.end method

.method public static registerSpecialInverse(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p0, "target"    # Ljava/lang/String;
    .param p1, "inverseTarget"    # Ljava/lang/String;
    .param p2, "bidirectional"    # Z

    .prologue
    .line 570
    sget-object v0, Lcom/ibm/icu/text/TransliteratorIDParser;->SPECIAL_INVERSES:Ljava/util/Hashtable;

    new-instance v1, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v1, p0}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 571
    if-eqz p2, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 572
    sget-object v0, Lcom/ibm/icu/text/TransliteratorIDParser;->SPECIAL_INVERSES:Ljava/util/Hashtable;

    new-instance v1, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v1, p1}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 574
    :cond_0
    return-void
.end method

.method private static specsToID(Lcom/ibm/icu/text/TransliteratorIDParser$Specs;I)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    .locals 7
    .param p0, "specs"    # Lcom/ibm/icu/text/TransliteratorIDParser$Specs;
    .param p1, "dir"    # I

    .prologue
    const/16 v6, 0x2d

    .line 708
    const-string/jumbo v3, ""

    .line 709
    .local v3, "canonID":Ljava/lang/String;
    const-string/jumbo v0, ""

    .line 710
    .local v0, "basicID":Ljava/lang/String;
    const-string/jumbo v1, ""

    .line 711
    .local v1, "basicPrefix":Ljava/lang/String;
    if-eqz p0, :cond_2

    .line 712
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 713
    .local v2, "buf":Ljava/lang/StringBuffer;
    if-nez p1, :cond_4

    .line 714
    iget-boolean v4, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->sawSource:Z

    if-eqz v4, :cond_3

    .line 715
    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->source:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 719
    :goto_0
    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->target:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 723
    :goto_1
    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->variant:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 724
    const/16 v4, 0x2f

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->variant:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 726
    :cond_0
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 727
    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->filter:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 728
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->filter:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 730
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 732
    .end local v2    # "buf":Ljava/lang/StringBuffer;
    :cond_2
    new-instance v4, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    invoke-direct {v4, v3, v0}, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    .line 717
    .restart local v2    # "buf":Ljava/lang/StringBuffer;
    :cond_3
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v5, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->source:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 721
    :cond_4
    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->target:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->source:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method

.method private static specsToSpecialInverse(Lcom/ibm/icu/text/TransliteratorIDParser$Specs;)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    .locals 8
    .param p0, "specs"    # Lcom/ibm/icu/text/TransliteratorIDParser$Specs;

    .prologue
    const/4 v3, 0x0

    const/16 v7, 0x2f

    .line 743
    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->source:Ljava/lang/String;

    const-string/jumbo v5, "Any"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 769
    :cond_0
    :goto_0
    return-object v3

    .line 746
    :cond_1
    sget-object v4, Lcom/ibm/icu/text/TransliteratorIDParser;->SPECIAL_INVERSES:Ljava/util/Hashtable;

    new-instance v5, Lcom/ibm/icu/util/CaseInsensitiveString;

    iget-object v6, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->target:Ljava/lang/String;

    invoke-direct {v5, v6}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 748
    .local v2, "inverseTarget":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 752
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 753
    .local v1, "buf":Ljava/lang/StringBuffer;
    iget-object v3, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->filter:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 754
    iget-object v3, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->filter:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 756
    :cond_2
    iget-boolean v3, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->sawSource:Z

    if-eqz v3, :cond_3

    .line 757
    const-string/jumbo v3, "Any"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const/16 v4, 0x2d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 759
    :cond_3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 761
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "Any-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 763
    .local v0, "basicID":Ljava/lang/String;
    iget-object v3, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->variant:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 764
    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->variant:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 765
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorIDParser$Specs;->variant:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 767
    :cond_4
    new-instance v3, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
