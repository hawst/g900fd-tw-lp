.class Lcom/ibm/icu/text/FractionalPartSubstitution;
.super Lcom/ibm/icu/text/NFSubstitution;
.source "NFSubstitution.java"


# instance fields
.field private byDigits:Z

.field private useSpaces:Z


# direct methods
.method constructor <init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V
    .locals 3
    .param p1, "pos"    # I
    .param p2, "ruleSet"    # Lcom/ibm/icu/text/NFRuleSet;
    .param p3, "formatter"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;
    .param p4, "description"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1164
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/ibm/icu/text/NFSubstitution;-><init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    .line 1137
    iput-boolean v1, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->byDigits:Z

    .line 1143
    iput-boolean v2, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->useSpaces:Z

    .line 1168
    const-string/jumbo v0, ">>"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, ">>>"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    if-ne p2, v0, :cond_2

    .line 1169
    :cond_0
    iput-boolean v2, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->byDigits:Z

    .line 1170
    const-string/jumbo v0, ">>>"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1171
    iput-boolean v1, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->useSpaces:Z

    .line 1176
    :cond_1
    :goto_0
    return-void

    .line 1174
    :cond_2
    iget-object v0, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/NFRuleSet;->makeIntoFractionRuleSet()V

    goto :goto_0
.end method


# virtual methods
.method public calcUpperBound(D)D
    .locals 2
    .param p1, "oldUpperBound"    # D

    .prologue
    .line 1372
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public composeRuleValue(DD)D
    .locals 3
    .param p1, "newRuleValue"    # D
    .param p3, "oldRuleValue"    # D

    .prologue
    .line 1365
    add-double v0, p1, p3

    return-wide v0
.end method

.method public doParse(Ljava/lang/String;Ljava/text/ParsePosition;DDZ)Ljava/lang/Number;
    .locals 19
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "parsePosition"    # Ljava/text/ParsePosition;
    .param p3, "baseValue"    # D
    .param p5, "upperBound"    # D
    .param p7, "lenientParse"    # Z

    .prologue
    .line 1297
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/ibm/icu/text/FractionalPartSubstitution;->byDigits:Z

    if-nez v5, :cond_0

    .line 1298
    const-wide/16 v10, 0x0

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-wide/from16 v8, p3

    move/from16 v12, p7

    invoke-super/range {v5 .. v12}, Lcom/ibm/icu/text/NFSubstitution;->doParse(Ljava/lang/String;Ljava/text/ParsePosition;DDZ)Ljava/lang/Number;

    move-result-object v5

    .line 1353
    :goto_0
    return-object v5

    .line 1305
    :cond_0
    new-instance v17, Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 1306
    .local v17, "workText":Ljava/lang/String;
    new-instance v16, Ljava/text/ParsePosition;

    const/4 v5, 0x1

    move-object/from16 v0, v16

    invoke-direct {v0, v5}, Ljava/text/ParsePosition;-><init>(I)V

    .line 1307
    .local v16, "workPos":Ljava/text/ParsePosition;
    const-wide/16 v14, 0x0

    .line 1331
    .local v14, "result":D
    new-instance v13, Lcom/ibm/icu/text/DigitList;

    invoke-direct {v13}, Lcom/ibm/icu/text/DigitList;-><init>()V

    .line 1332
    .local v13, "dl":Lcom/ibm/icu/text/DigitList;
    :cond_1
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_3

    invoke-virtual/range {v16 .. v16}, Ljava/text/ParsePosition;->getIndex()I

    move-result v5

    if-eqz v5, :cond_3

    .line 1333
    const/4 v5, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1334
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/FractionalPartSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v5, v0, v1, v6, v7}, Lcom/ibm/icu/text/NFRuleSet;->parse(Ljava/lang/String;Ljava/text/ParsePosition;D)Ljava/lang/Number;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Number;->intValue()I

    move-result v4

    .line 1335
    .local v4, "digit":I
    if-eqz p7, :cond_2

    invoke-virtual/range {v16 .. v16}, Ljava/text/ParsePosition;->getIndex()I

    move-result v5

    if-nez v5, :cond_2

    .line 1336
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/FractionalPartSubstitution;->rbnf:Lcom/ibm/icu/text/RuleBasedNumberFormat;

    invoke-virtual {v5}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->getDecimalFormat()Lcom/ibm/icu/text/DecimalFormat;

    move-result-object v5

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v5, v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Number;->intValue()I

    move-result v4

    .line 1339
    :cond_2
    invoke-virtual/range {v16 .. v16}, Ljava/text/ParsePosition;->getIndex()I

    move-result v5

    if-eqz v5, :cond_1

    .line 1340
    add-int/lit8 v5, v4, 0x30

    invoke-virtual {v13, v5}, Lcom/ibm/icu/text/DigitList;->append(I)V

    .line 1342
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v5

    invoke-virtual/range {v16 .. v16}, Ljava/text/ParsePosition;->getIndex()I

    move-result v6

    add-int/2addr v5, v6

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1343
    invoke-virtual/range {v16 .. v16}, Ljava/text/ParsePosition;->getIndex()I

    move-result v5

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    .line 1344
    :goto_1
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    const/4 v5, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x20

    if-ne v5, v6, :cond_1

    .line 1345
    const/4 v5, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    .line 1346
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/text/ParsePosition;->setIndex(I)V

    goto :goto_1

    .line 1350
    .end local v4    # "digit":I
    :cond_3
    iget v5, v13, Lcom/ibm/icu/text/DigitList;->count:I

    if-nez v5, :cond_4

    const-wide/16 v14, 0x0

    .line 1352
    :goto_2
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-virtual {v0, v14, v15, v1, v2}, Lcom/ibm/icu/text/FractionalPartSubstitution;->composeRuleValue(DD)D

    move-result-wide v14

    .line 1353
    new-instance v5, Ljava/lang/Double;

    invoke-direct {v5, v14, v15}, Ljava/lang/Double;-><init>(D)V

    goto/16 :goto_0

    .line 1350
    :cond_4
    invoke-virtual {v13}, Lcom/ibm/icu/text/DigitList;->getDouble()D

    move-result-wide v14

    goto :goto_2
.end method

.method public doSubstitution(DLjava/lang/StringBuffer;I)V
    .locals 7
    .param p1, "number"    # D
    .param p3, "toInsertInto"    # Ljava/lang/StringBuffer;
    .param p4, "position"    # I

    .prologue
    const/16 v6, 0x20

    .line 1195
    iget-boolean v2, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->byDigits:Z

    if-nez v2, :cond_1

    .line 1196
    invoke-super {p0, p1, p2, p3, p4}, Lcom/ibm/icu/text/NFSubstitution;->doSubstitution(DLjava/lang/StringBuffer;I)V

    .line 1252
    :cond_0
    return-void

    .line 1210
    :cond_1
    new-instance v0, Lcom/ibm/icu/text/DigitList;

    invoke-direct {v0}, Lcom/ibm/icu/text/DigitList;-><init>()V

    .line 1211
    .local v0, "dl":Lcom/ibm/icu/text/DigitList;
    const/16 v2, 0x14

    const/4 v3, 0x1

    invoke-virtual {v0, p1, p2, v2, v3}, Lcom/ibm/icu/text/DigitList;->set(DIZ)V

    .line 1233
    const/4 v1, 0x0

    .line 1234
    .local v1, "pad":Z
    :goto_0
    iget v2, v0, Lcom/ibm/icu/text/DigitList;->count:I

    const/4 v3, 0x0

    iget v4, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    if-le v2, v3, :cond_3

    .line 1235
    if-eqz v1, :cond_2

    iget-boolean v2, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->useSpaces:Z

    if-eqz v2, :cond_2

    .line 1236
    iget v2, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->pos:I

    add-int/2addr v2, p4

    invoke-virtual {p3, v2, v6}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 1240
    :goto_1
    iget-object v2, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    iget-object v3, v0, Lcom/ibm/icu/text/DigitList;->digits:[B

    iget v4, v0, Lcom/ibm/icu/text/DigitList;->count:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v0, Lcom/ibm/icu/text/DigitList;->count:I

    aget-byte v3, v3, v4

    add-int/lit8 v3, v3, -0x30

    int-to-long v4, v3

    iget v3, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->pos:I

    add-int/2addr v3, p4

    invoke-virtual {v2, v4, v5, p3, v3}, Lcom/ibm/icu/text/NFRuleSet;->format(JLjava/lang/StringBuffer;I)V

    goto :goto_0

    .line 1238
    :cond_2
    const/4 v1, 0x1

    goto :goto_1

    .line 1242
    :cond_3
    :goto_2
    iget v2, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    if-gez v2, :cond_0

    .line 1243
    if-eqz v1, :cond_4

    iget-boolean v2, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->useSpaces:Z

    if-eqz v2, :cond_4

    .line 1244
    iget v2, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->pos:I

    add-int/2addr v2, p4

    invoke-virtual {p3, v2, v6}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 1248
    :goto_3
    iget-object v2, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    const-wide/16 v4, 0x0

    iget v3, p0, Lcom/ibm/icu/text/FractionalPartSubstitution;->pos:I

    add-int/2addr v3, p4

    invoke-virtual {v2, v4, v5, p3, v3}, Lcom/ibm/icu/text/NFRuleSet;->format(JLjava/lang/StringBuffer;I)V

    .line 1249
    iget v2, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    goto :goto_2

    .line 1246
    :cond_4
    const/4 v1, 0x1

    goto :goto_3
.end method

.method tokenChar()C
    .locals 1

    .prologue
    .line 1384
    const/16 v0, 0x3e

    return v0
.end method

.method public transformNumber(D)D
    .locals 3
    .param p1, "number"    # D

    .prologue
    .line 1270
    invoke-static {p1, p2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    sub-double v0, p1, v0

    return-wide v0
.end method

.method public transformNumber(J)J
    .locals 2
    .param p1, "number"    # J

    .prologue
    .line 1261
    const-wide/16 v0, 0x0

    return-wide v0
.end method
