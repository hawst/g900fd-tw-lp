.class Lcom/ibm/icu/text/PluralRules$OrConstraint;
.super Lcom/ibm/icu/text/PluralRules$BinaryConstraint;
.source "PluralRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/PluralRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OrConstraint"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x13814c681722a76eL


# direct methods
.method constructor <init>(Lcom/ibm/icu/text/PluralRules$Constraint;Lcom/ibm/icu/text/PluralRules$Constraint;)V
    .locals 1
    .param p1, "a"    # Lcom/ibm/icu/text/PluralRules$Constraint;
    .param p2, "b"    # Lcom/ibm/icu/text/PluralRules$Constraint;

    .prologue
    .line 546
    const-string/jumbo v0, " || "

    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/text/PluralRules$BinaryConstraint;-><init>(Lcom/ibm/icu/text/PluralRules$Constraint;Lcom/ibm/icu/text/PluralRules$Constraint;Ljava/lang/String;)V

    .line 547
    return-void
.end method


# virtual methods
.method public isFulfilled(D)Z
    .locals 1
    .param p1, "n"    # D

    .prologue
    .line 550
    iget-object v0, p0, Lcom/ibm/icu/text/PluralRules$OrConstraint;->a:Lcom/ibm/icu/text/PluralRules$Constraint;

    invoke-interface {v0, p1, p2}, Lcom/ibm/icu/text/PluralRules$Constraint;->isFulfilled(D)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/PluralRules$OrConstraint;->b:Lcom/ibm/icu/text/PluralRules$Constraint;

    invoke-interface {v0, p1, p2}, Lcom/ibm/icu/text/PluralRules$Constraint;->isFulfilled(D)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
