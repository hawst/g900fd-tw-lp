.class Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;
.super Ljava/lang/Object;
.source "TransliteratorRegistry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/TransliteratorRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CompoundRBTEntry"
.end annotation


# instance fields
.field private ID:Ljava/lang/String;

.field private compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

.field private dataVector:Ljava/util/Vector;

.field private idBlockVector:Ljava/util/Vector;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Vector;Ljava/util/Vector;Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 0
    .param p1, "theID"    # Ljava/lang/String;
    .param p2, "theIDBlockVector"    # Ljava/util/Vector;
    .param p3, "theDataVector"    # Ljava/util/Vector;
    .param p4, "theCompoundFilter"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    iput-object p1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;->ID:Ljava/lang/String;

    .line 249
    iput-object p2, p0, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;->idBlockVector:Ljava/util/Vector;

    .line 250
    iput-object p3, p0, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;->dataVector:Ljava/util/Vector;

    .line 251
    iput-object p4, p0, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    .line 252
    return-void
.end method


# virtual methods
.method public getInstance()Lcom/ibm/icu/text/Transliterator;
    .locals 11

    .prologue
    .line 255
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    .line 256
    .local v7, "transliterators":Ljava/util/Vector;
    const/4 v4, 0x1

    .line 258
    .local v4, "passNumber":I
    iget-object v8, p0, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;->idBlockVector:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v8

    iget-object v9, p0, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;->dataVector:Ljava/util/Vector;

    invoke-virtual {v9}, Ljava/util/Vector;->size()I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 259
    .local v3, "limit":I
    const/4 v1, 0x0

    .local v1, "i":I
    move v5, v4

    .end local v4    # "passNumber":I
    .local v5, "passNumber":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 260
    iget-object v8, p0, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;->idBlockVector:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v8

    if-ge v1, v8, :cond_0

    .line 261
    iget-object v8, p0, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;->idBlockVector:Ljava/util/Vector;

    invoke-virtual {v8, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 262
    .local v2, "idBlock":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_0

    .line 263
    invoke-static {v2}, Lcom/ibm/icu/text/Transliterator;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 265
    .end local v2    # "idBlock":Ljava/lang/String;
    :cond_0
    iget-object v8, p0, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;->dataVector:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v8

    if-ge v1, v8, :cond_3

    .line 266
    iget-object v8, p0, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;->dataVector:Ljava/util/Vector;

    invoke-virtual {v8, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .line 267
    .local v0, "data":Lcom/ibm/icu/text/RuleBasedTransliterator$Data;
    new-instance v8, Lcom/ibm/icu/text/RuleBasedTransliterator;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v10, "%Pass"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "passNumber":I
    .restart local v4    # "passNumber":I
    invoke-virtual {v9, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-direct {v8, v9, v0, v10}, Lcom/ibm/icu/text/RuleBasedTransliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/RuleBasedTransliterator$Data;Lcom/ibm/icu/text/UnicodeFilter;)V

    invoke-virtual {v7, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 259
    .end local v0    # "data":Lcom/ibm/icu/text/RuleBasedTransliterator$Data;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    move v5, v4

    .end local v4    # "passNumber":I
    .restart local v5    # "passNumber":I
    goto :goto_0

    .line 271
    :cond_1
    new-instance v6, Lcom/ibm/icu/text/CompoundTransliterator;

    add-int/lit8 v8, v5, -0x1

    invoke-direct {v6, v7, v8}, Lcom/ibm/icu/text/CompoundTransliterator;-><init>(Ljava/util/Vector;I)V

    .line 272
    .local v6, "t":Lcom/ibm/icu/text/Transliterator;
    iget-object v8, p0, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;->ID:Ljava/lang/String;

    invoke-virtual {v6, v8}, Lcom/ibm/icu/text/Transliterator;->setID(Ljava/lang/String;)V

    .line 273
    iget-object v8, p0, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v8, :cond_2

    .line 274
    iget-object v8, p0, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v6, v8}, Lcom/ibm/icu/text/Transliterator;->setFilter(Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 276
    :cond_2
    return-object v6

    .end local v6    # "t":Lcom/ibm/icu/text/Transliterator;
    :cond_3
    move v4, v5

    .end local v5    # "passNumber":I
    .restart local v4    # "passNumber":I
    goto :goto_1
.end method
