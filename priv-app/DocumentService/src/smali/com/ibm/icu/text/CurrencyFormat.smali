.class Lcom/ibm/icu/text/CurrencyFormat;
.super Lcom/ibm/icu/text/MeasureFormat;
.source "CurrencyFormat.java"


# static fields
.field static final serialVersionUID:J = -0xcedfd71ed74263aL


# instance fields
.field private fmt:Lcom/ibm/icu/text/NumberFormat;


# direct methods
.method public constructor <init>(Lcom/ibm/icu/util/ULocale;)V
    .locals 1
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/ibm/icu/text/MeasureFormat;-><init>()V

    .line 39
    invoke-virtual {p1}, Lcom/ibm/icu/util/ULocale;->toLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lcom/ibm/icu/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/CurrencyFormat;->fmt:Lcom/ibm/icu/text/NumberFormat;

    .line 40
    return-void
.end method


# virtual methods
.method public format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 48
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/ibm/icu/util/CurrencyAmount;

    move-object v1, v0

    .line 49
    .local v1, "currency":Lcom/ibm/icu/util/CurrencyAmount;
    iget-object v3, p0, Lcom/ibm/icu/text/CurrencyFormat;->fmt:Lcom/ibm/icu/text/NumberFormat;

    invoke-virtual {v1}, Lcom/ibm/icu/util/CurrencyAmount;->getCurrency()Lcom/ibm/icu/util/Currency;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/NumberFormat;->setCurrency(Lcom/ibm/icu/util/Currency;)V

    .line 50
    iget-object v3, p0, Lcom/ibm/icu/text/CurrencyFormat;->fmt:Lcom/ibm/icu/text/NumberFormat;

    invoke-virtual {v1}, Lcom/ibm/icu/util/CurrencyAmount;->getNumber()Ljava/lang/Number;

    move-result-object v4

    invoke-virtual {v3, v4, p2, p3}, Lcom/ibm/icu/text/NumberFormat;->format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    .line 51
    .end local v1    # "currency":Lcom/ibm/icu/util/CurrencyAmount;
    :catch_0
    move-exception v2

    .line 52
    .local v2, "e":Ljava/lang/ClassCastException;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "Invalid type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public parseObject(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Object;
    .locals 1
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/ibm/icu/text/CurrencyFormat;->fmt:Lcom/ibm/icu/text/NumberFormat;

    invoke-virtual {v0, p1, p2}, Lcom/ibm/icu/text/NumberFormat;->parseCurrency(Ljava/lang/String;Ljava/text/ParsePosition;)Lcom/ibm/icu/util/CurrencyAmount;

    move-result-object v0

    return-object v0
.end method
