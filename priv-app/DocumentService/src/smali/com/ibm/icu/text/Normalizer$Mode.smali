.class public Lcom/ibm/icu/text/Normalizer$Mode;
.super Ljava/lang/Object;
.source "Normalizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/Normalizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Mode"
.end annotation


# direct methods
.method private constructor <init>(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    return-void
.end method

.method constructor <init>(ILcom/ibm/icu/text/Normalizer$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Lcom/ibm/icu/text/Normalizer$1;

    .prologue
    .line 159
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/Normalizer$Mode;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected getMask()I
    .locals 1

    .prologue
    .line 215
    const/4 v0, -0x1

    return v0
.end method

.method protected getMinC()I
    .locals 1

    .prologue
    .line 207
    const/4 v0, -0x1

    return v0
.end method

.method protected getNextBoundary()Lcom/ibm/icu/text/Normalizer$IsNextBoundary;
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getPrevBoundary()Lcom/ibm/icu/text/Normalizer$IsPrevBoundary;
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isNFSkippable(I)Z
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 251
    const/4 v0, 0x1

    return v0
.end method

.method protected normalize([CII[CIII)I
    .locals 8
    .param p1, "src"    # [C
    .param p2, "srcStart"    # I
    .param p3, "srcLimit"    # I
    .param p4, "dest"    # [C
    .param p5, "destStart"    # I
    .param p6, "destLimit"    # I
    .param p7, "options"    # I

    .prologue
    .line 188
    invoke-static {p7}, Lcom/ibm/icu/impl/NormalizerImpl;->getNX(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v7

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/ibm/icu/text/Normalizer$Mode;->normalize([CII[CIILcom/ibm/icu/text/UnicodeSet;)I

    move-result v0

    return v0
.end method

.method protected normalize([CII[CIILcom/ibm/icu/text/UnicodeSet;)I
    .locals 2
    .param p1, "src"    # [C
    .param p2, "srcStart"    # I
    .param p3, "srcLimit"    # I
    .param p4, "dest"    # [C
    .param p5, "destStart"    # I
    .param p6, "destLimit"    # I
    .param p7, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 172
    sub-int v1, p3, p2

    .line 173
    .local v1, "srcLen":I
    sub-int v0, p6, p5

    .line 174
    .local v0, "destLen":I
    if-le v1, v0, :cond_0

    .line 178
    :goto_0
    return v1

    .line 177
    :cond_0
    invoke-static {p1, p2, p4, p5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method protected normalize(Ljava/lang/String;I)Ljava/lang/String;
    .locals 0
    .param p1, "src"    # Ljava/lang/String;
    .param p2, "options"    # I

    .prologue
    .line 199
    return-object p1
.end method

.method protected quickCheck([CIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;
    .locals 1
    .param p1, "src"    # [C
    .param p2, "start"    # I
    .param p3, "limit"    # I
    .param p4, "allowMaybe"    # Z
    .param p5, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 240
    if-eqz p4, :cond_0

    .line 241
    sget-object v0, Lcom/ibm/icu/text/Normalizer;->MAYBE:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    .line 243
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    goto :goto_0
.end method
