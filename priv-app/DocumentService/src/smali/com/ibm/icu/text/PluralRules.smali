.class public Lcom/ibm/icu/text/PluralRules;
.super Ljava/lang/Object;
.source "PluralRules.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/PluralRules$RuleChain;,
        Lcom/ibm/icu/text/PluralRules$ConstrainedRule;,
        Lcom/ibm/icu/text/PluralRules$OrConstraint;,
        Lcom/ibm/icu/text/PluralRules$AndConstraint;,
        Lcom/ibm/icu/text/PluralRules$BinaryConstraint;,
        Lcom/ibm/icu/text/PluralRules$RangeConstraint;,
        Lcom/ibm/icu/text/PluralRules$RuleList;,
        Lcom/ibm/icu/text/PluralRules$Rule;,
        Lcom/ibm/icu/text/PluralRules$Constraint;
    }
.end annotation


# static fields
.field private static final CONT_CHARS:Lcom/ibm/icu/text/UnicodeSet;

.field public static final DEFAULT:Lcom/ibm/icu/text/PluralRules;

.field private static final DEFAULT_RULE:Lcom/ibm/icu/text/PluralRules$Rule;

.field public static final KEYWORD_FEW:Ljava/lang/String; = "few"

.field public static final KEYWORD_MANY:Ljava/lang/String; = "many"

.field public static final KEYWORD_ONE:Ljava/lang/String; = "one"

.field public static final KEYWORD_OTHER:Ljava/lang/String; = "other"

.field public static final KEYWORD_TWO:Ljava/lang/String; = "two"

.field public static final KEYWORD_ZERO:Ljava/lang/String; = "zero"

.field private static final NO_CONSTRAINT:Lcom/ibm/icu/text/PluralRules$Constraint;

.field private static final START_CHARS:Lcom/ibm/icu/text/UnicodeSet;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final keywords:Ljava/util/Set;

.field private repeatLimit:I

.field private final rules:Lcom/ibm/icu/text/PluralRules$RuleList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 133
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v1, "[[:ID_Start:][_]]"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/ibm/icu/text/PluralRules;->START_CHARS:Lcom/ibm/icu/text/UnicodeSet;

    .line 140
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v1, "[:ID_Continue:]"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/ibm/icu/text/PluralRules;->CONT_CHARS:Lcom/ibm/icu/text/UnicodeSet;

    .line 146
    new-instance v0, Lcom/ibm/icu/text/PluralRules$1;

    invoke-direct {v0}, Lcom/ibm/icu/text/PluralRules$1;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/PluralRules;->NO_CONSTRAINT:Lcom/ibm/icu/text/PluralRules$Constraint;

    .line 164
    new-instance v0, Lcom/ibm/icu/text/PluralRules$2;

    invoke-direct {v0}, Lcom/ibm/icu/text/PluralRules$2;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/PluralRules;->DEFAULT_RULE:Lcom/ibm/icu/text/PluralRules$Rule;

    .line 191
    new-instance v0, Lcom/ibm/icu/text/PluralRules;

    new-instance v1, Lcom/ibm/icu/text/PluralRules$RuleChain;

    sget-object v2, Lcom/ibm/icu/text/PluralRules;->DEFAULT_RULE:Lcom/ibm/icu/text/PluralRules$Rule;

    invoke-direct {v1, v2}, Lcom/ibm/icu/text/PluralRules$RuleChain;-><init>(Lcom/ibm/icu/text/PluralRules$Rule;)V

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/PluralRules;-><init>(Lcom/ibm/icu/text/PluralRules$RuleList;)V

    sput-object v0, Lcom/ibm/icu/text/PluralRules;->DEFAULT:Lcom/ibm/icu/text/PluralRules;

    return-void
.end method

.method private constructor <init>(Lcom/ibm/icu/text/PluralRules$RuleList;)V
    .locals 1
    .param p1, "rules"    # Lcom/ibm/icu/text/PluralRules$RuleList;

    .prologue
    .line 708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 709
    iput-object p1, p0, Lcom/ibm/icu/text/PluralRules;->rules:Lcom/ibm/icu/text/PluralRules$RuleList;

    .line 710
    invoke-interface {p1}, Lcom/ibm/icu/text/PluralRules$RuleList;->getKeywords()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/PluralRules;->keywords:Ljava/util/Set;

    .line 711
    return-void
.end method

.method public static createRules(Ljava/lang/String;)Lcom/ibm/icu/text/PluralRules;
    .locals 2
    .param p0, "description"    # Ljava/lang/String;

    .prologue
    .line 223
    :try_start_0
    invoke-static {p0}, Lcom/ibm/icu/text/PluralRules;->parseDescription(Ljava/lang/String;)Lcom/ibm/icu/text/PluralRules;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 225
    :goto_0
    return-object v1

    .line 224
    :catch_0
    move-exception v0

    .line 225
    .local v0, "e":Ljava/text/ParseException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static forLocale(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/PluralRules;
    .locals 1
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 684
    sget-object v0, Lcom/ibm/icu/impl/PluralRulesLoader;->loader:Lcom/ibm/icu/impl/PluralRulesLoader;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/impl/PluralRulesLoader;->forLocale(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/PluralRules;

    move-result-object v0

    return-object v0
.end method

.method private getRepeatLimit()I
    .locals 1

    .prologue
    .line 794
    iget v0, p0, Lcom/ibm/icu/text/PluralRules;->repeatLimit:I

    if-nez v0, :cond_0

    .line 795
    iget-object v0, p0, Lcom/ibm/icu/text/PluralRules;->rules:Lcom/ibm/icu/text/PluralRules$RuleList;

    invoke-interface {v0}, Lcom/ibm/icu/text/PluralRules$RuleList;->getRepeatLimit()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/PluralRules;->repeatLimit:I

    .line 797
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/PluralRules;->repeatLimit:I

    return v0
.end method

.method private static isValidKeyword(Ljava/lang/String;)Z
    .locals 4
    .param p0, "token"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 694
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    sget-object v2, Lcom/ibm/icu/text/PluralRules;->START_CHARS:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 695
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 696
    sget-object v2, Lcom/ibm/icu/text/PluralRules;->CONT_CHARS:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 702
    .end local v0    # "i":I
    :cond_0
    :goto_1
    return v1

    .line 695
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 700
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private static nextToken([Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "tokens"    # [Ljava/lang/String;
    .param p1, "x"    # I
    .param p2, "context"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 405
    array-length v0, p0

    if-ge p1, v0, :cond_0

    .line 406
    aget-object v0, p0, p1

    return-object v0

    .line 408
    :cond_0
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "missing token at end of \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method private static parseConstraint(Ljava/lang/String;)Lcom/ibm/icu/text/PluralRules$Constraint;
    .locals 27
    .param p0, "description"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 302
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v25

    sget-object v26, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    .line 304
    const/16 v19, 0x0

    .line 305
    .local v19, "result":Lcom/ibm/icu/text/PluralRules$Constraint;
    const-string/jumbo v25, "or"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/Utility;->splitString(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 306
    .local v17, "or_together":[Ljava/lang/String;
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    if-ge v14, v0, :cond_c

    .line 307
    const/4 v10, 0x0

    .line 308
    .local v10, "andConstraint":Lcom/ibm/icu/text/PluralRules$Constraint;
    aget-object v25, v17, v14

    const-string/jumbo v26, "and"

    invoke-static/range {v25 .. v26}, Lcom/ibm/icu/impl/Utility;->splitString(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 309
    .local v12, "and_together":[Ljava/lang/String;
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_1
    array-length v0, v12

    move/from16 v25, v0

    move/from16 v0, v16

    move/from16 v1, v25

    if-ge v0, v1, :cond_a

    .line 310
    sget-object v2, Lcom/ibm/icu/text/PluralRules;->NO_CONSTRAINT:Lcom/ibm/icu/text/PluralRules$Constraint;

    .line 312
    .local v2, "newConstraint":Lcom/ibm/icu/text/PluralRules$Constraint;
    aget-object v25, v12, v16

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    .line 313
    .local v13, "condition":Ljava/lang/String;
    invoke-static {v13}, Lcom/ibm/icu/impl/Utility;->splitWhitespace(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v22

    .line 315
    .local v22, "tokens":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 316
    .local v3, "mod":I
    const/4 v4, 0x1

    .line 317
    .local v4, "inRange":Z
    const/4 v5, 0x1

    .line 318
    .local v5, "integersOnly":Z
    const-wide/16 v6, -0x1

    .line 319
    .local v6, "lowBound":J
    const-wide/16 v8, -0x1

    .line 321
    .local v8, "highBound":J
    const/4 v15, 0x0

    .line 323
    .local v15, "isRange":Z
    const/16 v23, 0x0

    .line 324
    .local v23, "x":I
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "x":I
    .local v24, "x":I
    aget-object v21, v22, v23

    .line 325
    .local v21, "t":Ljava/lang/String;
    const-string/jumbo v25, "n"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_0

    .line 326
    move-object/from16 v0, v21

    invoke-static {v0, v13}, Lcom/ibm/icu/text/PluralRules;->unexpected(Ljava/lang/String;Ljava/lang/String;)Ljava/text/ParseException;

    move-result-object v25

    throw v25

    .line 328
    :cond_0
    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_e

    .line 329
    add-int/lit8 v23, v24, 0x1

    .end local v24    # "x":I
    .restart local v23    # "x":I
    aget-object v21, v22, v24

    .line 330
    const-string/jumbo v25, "mod"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_1

    .line 331
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "x":I
    .restart local v24    # "x":I
    aget-object v25, v22, v23

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 332
    add-int/lit8 v23, v24, 0x1

    .end local v24    # "x":I
    .restart local v23    # "x":I
    move-object/from16 v0, v22

    move/from16 v1, v24

    invoke-static {v0, v1, v13}, Lcom/ibm/icu/text/PluralRules;->nextToken([Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 334
    :cond_1
    const-string/jumbo v25, "is"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_2

    .line 335
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "x":I
    .restart local v24    # "x":I
    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-static {v0, v1, v13}, Lcom/ibm/icu/text/PluralRules;->nextToken([Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 336
    const-string/jumbo v25, "not"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_d

    .line 337
    const/4 v4, 0x0

    .line 338
    add-int/lit8 v23, v24, 0x1

    .end local v24    # "x":I
    .restart local v23    # "x":I
    move-object/from16 v0, v22

    move/from16 v1, v24

    invoke-static {v0, v1, v13}, Lcom/ibm/icu/text/PluralRules;->nextToken([Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 356
    :goto_2
    if-eqz v15, :cond_7

    .line 357
    const-string/jumbo v25, ".."

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/Utility;->splitString(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 358
    .local v18, "pair":[Ljava/lang/String;
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_6

    .line 359
    const/16 v25, 0x0

    aget-object v25, v18, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 360
    const/16 v25, 0x1

    aget-object v25, v18, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 368
    .end local v18    # "pair":[Ljava/lang/String;
    :goto_3
    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v25, v0

    move/from16 v0, v23

    move/from16 v1, v25

    if-eq v0, v1, :cond_8

    .line 369
    aget-object v25, v22, v23

    move-object/from16 v0, v25

    invoke-static {v0, v13}, Lcom/ibm/icu/text/PluralRules;->unexpected(Ljava/lang/String;Ljava/lang/String;)Ljava/text/ParseException;

    move-result-object v25

    throw v25

    .line 341
    :cond_2
    const/4 v15, 0x1

    .line 342
    const-string/jumbo v25, "not"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 343
    const/4 v4, 0x0

    .line 344
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "x":I
    .restart local v24    # "x":I
    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-static {v0, v1, v13}, Lcom/ibm/icu/text/PluralRules;->nextToken([Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move/from16 v23, v24

    .line 346
    .end local v24    # "x":I
    .restart local v23    # "x":I
    :cond_3
    const-string/jumbo v25, "in"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_4

    .line 347
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "x":I
    .restart local v24    # "x":I
    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-static {v0, v1, v13}, Lcom/ibm/icu/text/PluralRules;->nextToken([Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move/from16 v23, v24

    .line 348
    .end local v24    # "x":I
    .restart local v23    # "x":I
    goto :goto_2

    :cond_4
    const-string/jumbo v25, "within"

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 349
    const/4 v5, 0x0

    .line 350
    add-int/lit8 v24, v23, 0x1

    .end local v23    # "x":I
    .restart local v24    # "x":I
    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-static {v0, v1, v13}, Lcom/ibm/icu/text/PluralRules;->nextToken([Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move/from16 v23, v24

    .line 351
    .end local v24    # "x":I
    .restart local v23    # "x":I
    goto/16 :goto_2

    .line 352
    :cond_5
    move-object/from16 v0, v21

    invoke-static {v0, v13}, Lcom/ibm/icu/text/PluralRules;->unexpected(Ljava/lang/String;Ljava/lang/String;)Ljava/text/ParseException;

    move-result-object v25

    throw v25

    .line 362
    .restart local v18    # "pair":[Ljava/lang/String;
    :cond_6
    move-object/from16 v0, v21

    invoke-static {v0, v13}, Lcom/ibm/icu/text/PluralRules;->unexpected(Ljava/lang/String;Ljava/lang/String;)Ljava/text/ParseException;

    move-result-object v25

    throw v25

    .line 365
    .end local v18    # "pair":[Ljava/lang/String;
    :cond_7
    invoke-static/range {v21 .. v21}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    move-wide v6, v8

    goto :goto_3

    .line 372
    :cond_8
    new-instance v2, Lcom/ibm/icu/text/PluralRules$RangeConstraint;

    .end local v2    # "newConstraint":Lcom/ibm/icu/text/PluralRules$Constraint;
    invoke-direct/range {v2 .. v9}, Lcom/ibm/icu/text/PluralRules$RangeConstraint;-><init>(IZZJJ)V

    .line 376
    .restart local v2    # "newConstraint":Lcom/ibm/icu/text/PluralRules$Constraint;
    :goto_4
    if-nez v10, :cond_9

    .line 377
    move-object v10, v2

    .line 309
    :goto_5
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .line 379
    :cond_9
    new-instance v11, Lcom/ibm/icu/text/PluralRules$AndConstraint;

    invoke-direct {v11, v10, v2}, Lcom/ibm/icu/text/PluralRules$AndConstraint;-><init>(Lcom/ibm/icu/text/PluralRules$Constraint;Lcom/ibm/icu/text/PluralRules$Constraint;)V

    .end local v10    # "andConstraint":Lcom/ibm/icu/text/PluralRules$Constraint;
    .local v11, "andConstraint":Lcom/ibm/icu/text/PluralRules$Constraint;
    move-object v10, v11

    .end local v11    # "andConstraint":Lcom/ibm/icu/text/PluralRules$Constraint;
    .restart local v10    # "andConstraint":Lcom/ibm/icu/text/PluralRules$Constraint;
    goto :goto_5

    .line 384
    .end local v2    # "newConstraint":Lcom/ibm/icu/text/PluralRules$Constraint;
    .end local v3    # "mod":I
    .end local v4    # "inRange":Z
    .end local v5    # "integersOnly":Z
    .end local v6    # "lowBound":J
    .end local v8    # "highBound":J
    .end local v13    # "condition":Ljava/lang/String;
    .end local v15    # "isRange":Z
    .end local v21    # "t":Ljava/lang/String;
    .end local v22    # "tokens":[Ljava/lang/String;
    .end local v23    # "x":I
    :cond_a
    if-nez v19, :cond_b

    .line 385
    move-object/from16 v19, v10

    .line 306
    :goto_6
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 387
    :cond_b
    new-instance v20, Lcom/ibm/icu/text/PluralRules$OrConstraint;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v10}, Lcom/ibm/icu/text/PluralRules$OrConstraint;-><init>(Lcom/ibm/icu/text/PluralRules$Constraint;Lcom/ibm/icu/text/PluralRules$Constraint;)V

    .end local v19    # "result":Lcom/ibm/icu/text/PluralRules$Constraint;
    .local v20, "result":Lcom/ibm/icu/text/PluralRules$Constraint;
    move-object/from16 v19, v20

    .end local v20    # "result":Lcom/ibm/icu/text/PluralRules$Constraint;
    .restart local v19    # "result":Lcom/ibm/icu/text/PluralRules$Constraint;
    goto :goto_6

    .line 391
    .end local v10    # "andConstraint":Lcom/ibm/icu/text/PluralRules$Constraint;
    .end local v12    # "and_together":[Ljava/lang/String;
    .end local v16    # "j":I
    :cond_c
    return-object v19

    .restart local v2    # "newConstraint":Lcom/ibm/icu/text/PluralRules$Constraint;
    .restart local v3    # "mod":I
    .restart local v4    # "inRange":Z
    .restart local v5    # "integersOnly":Z
    .restart local v6    # "lowBound":J
    .restart local v8    # "highBound":J
    .restart local v10    # "andConstraint":Lcom/ibm/icu/text/PluralRules$Constraint;
    .restart local v12    # "and_together":[Ljava/lang/String;
    .restart local v13    # "condition":Ljava/lang/String;
    .restart local v15    # "isRange":Z
    .restart local v16    # "j":I
    .restart local v21    # "t":Ljava/lang/String;
    .restart local v22    # "tokens":[Ljava/lang/String;
    .restart local v24    # "x":I
    :cond_d
    move/from16 v23, v24

    .end local v24    # "x":I
    .restart local v23    # "x":I
    goto/16 :goto_2

    .end local v23    # "x":I
    .restart local v24    # "x":I
    :cond_e
    move/from16 v23, v24

    .end local v24    # "x":I
    .restart local v23    # "x":I
    goto :goto_4
.end method

.method public static parseDescription(Ljava/lang/String;)Lcom/ibm/icu/text/PluralRules;
    .locals 2
    .param p0, "description"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 205
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 206
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 207
    sget-object v0, Lcom/ibm/icu/text/PluralRules;->DEFAULT:Lcom/ibm/icu/text/PluralRules;

    .line 210
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/ibm/icu/text/PluralRules;

    invoke-static {p0}, Lcom/ibm/icu/text/PluralRules;->parseRuleChain(Ljava/lang/String;)Lcom/ibm/icu/text/PluralRules$RuleChain;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/PluralRules;-><init>(Lcom/ibm/icu/text/PluralRules$RuleList;)V

    goto :goto_0
.end method

.method private static parseRule(Ljava/lang/String;)Lcom/ibm/icu/text/PluralRules$Rule;
    .locals 8
    .param p0, "description"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 417
    const/16 v4, 0x3a

    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 418
    .local v3, "x":I
    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 419
    new-instance v4, Ljava/text/ParseException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "missing \':\' in rule description \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v7}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v4

    .line 423
    :cond_0
    invoke-virtual {p0, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 424
    .local v1, "keyword":Ljava/lang/String;
    invoke-static {v1}, Lcom/ibm/icu/text/PluralRules;->isValidKeyword(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 425
    new-instance v4, Ljava/text/ParseException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "keyword \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, " is not valid"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v7}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v4

    .line 429
    :cond_1
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 430
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_2

    .line 431
    new-instance v4, Ljava/text/ParseException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "missing constraint in \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v6, v3, 0x1

    invoke-direct {v4, v5, v6}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v4

    .line 434
    :cond_2
    invoke-static {p0}, Lcom/ibm/icu/text/PluralRules;->parseConstraint(Ljava/lang/String;)Lcom/ibm/icu/text/PluralRules$Constraint;

    move-result-object v0

    .line 435
    .local v0, "constraint":Lcom/ibm/icu/text/PluralRules$Constraint;
    new-instance v2, Lcom/ibm/icu/text/PluralRules$ConstrainedRule;

    invoke-direct {v2, v1, v0}, Lcom/ibm/icu/text/PluralRules$ConstrainedRule;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/PluralRules$Constraint;)V

    .line 436
    .local v2, "rule":Lcom/ibm/icu/text/PluralRules$Rule;
    return-object v2
.end method

.method private static parseRuleChain(Ljava/lang/String;)Lcom/ibm/icu/text/PluralRules$RuleChain;
    .locals 5
    .param p0, "description"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 447
    const/4 v2, 0x0

    .line 448
    .local v2, "rc":Lcom/ibm/icu/text/PluralRules$RuleChain;
    const/16 v4, 0x3b

    invoke-static {p0, v4}, Lcom/ibm/icu/impl/Utility;->split(Ljava/lang/String;C)[Ljava/lang/String;

    move-result-object v3

    .line 449
    .local v3, "rules":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_1

    .line 450
    aget-object v4, v3, v0

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/ibm/icu/text/PluralRules;->parseRule(Ljava/lang/String;)Lcom/ibm/icu/text/PluralRules$Rule;

    move-result-object v1

    .line 451
    .local v1, "r":Lcom/ibm/icu/text/PluralRules$Rule;
    if-nez v2, :cond_0

    .line 452
    new-instance v2, Lcom/ibm/icu/text/PluralRules$RuleChain;

    .end local v2    # "rc":Lcom/ibm/icu/text/PluralRules$RuleChain;
    invoke-direct {v2, v1}, Lcom/ibm/icu/text/PluralRules$RuleChain;-><init>(Lcom/ibm/icu/text/PluralRules$Rule;)V

    .line 449
    .restart local v2    # "rc":Lcom/ibm/icu/text/PluralRules$RuleChain;
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 454
    :cond_0
    invoke-virtual {v2, v1}, Lcom/ibm/icu/text/PluralRules$RuleChain;->addRule(Lcom/ibm/icu/text/PluralRules$Rule;)Lcom/ibm/icu/text/PluralRules$RuleChain;

    move-result-object v2

    goto :goto_1

    .line 457
    .end local v1    # "r":Lcom/ibm/icu/text/PluralRules$Rule;
    :cond_1
    return-object v2
.end method

.method private static unexpected(Ljava/lang/String;Ljava/lang/String;)Ljava/text/ParseException;
    .locals 3
    .param p0, "token"    # Ljava/lang/String;
    .param p1, "context"    # Ljava/lang/String;

    .prologue
    .line 396
    new-instance v0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "unexpected token \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\' in \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method


# virtual methods
.method public equals(Lcom/ibm/icu/text/PluralRules;)Z
    .locals 8
    .param p1, "rhs"    # Lcom/ibm/icu/text/PluralRules;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 774
    if-nez p1, :cond_1

    .line 790
    :cond_0
    :goto_0
    return v2

    .line 777
    :cond_1
    if-ne p1, p0, :cond_2

    move v2, v3

    .line 778
    goto :goto_0

    .line 780
    :cond_2
    invoke-virtual {p1}, Lcom/ibm/icu/text/PluralRules;->getKeywords()Ljava/util/Set;

    move-result-object v4

    iget-object v5, p0, Lcom/ibm/icu/text/PluralRules;->keywords:Ljava/util/Set;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 784
    invoke-direct {p0}, Lcom/ibm/icu/text/PluralRules;->getRepeatLimit()I

    move-result v4

    invoke-direct {p1}, Lcom/ibm/icu/text/PluralRules;->getRepeatLimit()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 785
    .local v1, "limit":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 786
    int-to-double v4, v0

    invoke-virtual {p0, v4, v5}, Lcom/ibm/icu/text/PluralRules;->select(D)Ljava/lang/String;

    move-result-object v4

    int-to-double v6, v0

    invoke-virtual {p1, v6, v7}, Lcom/ibm/icu/text/PluralRules;->select(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 785
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v2, v3

    .line 790
    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "rhs"    # Ljava/lang/Object;

    .prologue
    .line 763
    instance-of v0, p1, Lcom/ibm/icu/text/PluralRules;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/ibm/icu/text/PluralRules;

    .end local p1    # "rhs":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/PluralRules;->equals(Lcom/ibm/icu/text/PluralRules;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getKeywords()Ljava/util/Set;
    .locals 1

    .prologue
    .line 735
    iget-object v0, p0, Lcom/ibm/icu/text/PluralRules;->keywords:Ljava/util/Set;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 754
    iget-object v0, p0, Lcom/ibm/icu/text/PluralRules;->keywords:Ljava/util/Set;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public select(D)Ljava/lang/String;
    .locals 1
    .param p1, "number"    # D

    .prologue
    .line 723
    iget-object v0, p0, Lcom/ibm/icu/text/PluralRules;->rules:Lcom/ibm/icu/text/PluralRules$RuleList;

    invoke-interface {v0, p1, p2}, Lcom/ibm/icu/text/PluralRules$RuleList;->select(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 744
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v1, "keywords: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/ibm/icu/text/PluralRules;->keywords:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, " rules: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/ibm/icu/text/PluralRules;->rules:Lcom/ibm/icu/text/PluralRules$RuleList;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, " limit: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-direct {p0}, Lcom/ibm/icu/text/PluralRules;->getRepeatLimit()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
