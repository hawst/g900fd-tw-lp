.class Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
.super Ljava/lang/Object;
.source "CollationParsedRuleBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CollationParsedRuleBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Elements"
.end annotation


# instance fields
.field m_CELength_:I

.field m_CEs_:[I

.field m_cPointsOffset_:I

.field m_cPoints_:Ljava/lang/String;

.field m_caseBit_:Z

.field m_mapCE_:I

.field m_prefixChars_:Ljava/lang/String;

.field m_prefix_:I

.field m_sizePrim_:[I

.field m_sizeSec_:[I

.field m_sizeTer_:[I

.field m_uchars_:Ljava/lang/String;

.field m_variableTop_:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x80

    .line 1168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizePrim_:[I

    .line 1170
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizeSec_:[I

    .line 1171
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizeTer_:[I

    .line 1172
    const/16 v0, 0x100

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    .line 1173
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 1174
    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;)V
    .locals 1
    .param p1, "element"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .prologue
    .line 1180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1181
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 1182
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 1183
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    .line 1184
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 1185
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    .line 1186
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CEs_:[I

    .line 1187
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 1188
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 1189
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizePrim_:[I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizePrim_:[I

    .line 1190
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizeSec_:[I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizeSec_:[I

    .line 1191
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizeTer_:[I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizeTer_:[I

    .line 1192
    iget-boolean v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_variableTop_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_variableTop_:Z

    .line 1193
    iget-boolean v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_caseBit_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_caseBit_:Z

    .line 1194
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1203
    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefixChars_:Ljava/lang/String;

    .line 1204
    iput v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_prefix_:I

    .line 1205
    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_uchars_:Ljava/lang/String;

    .line 1206
    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    .line 1207
    iput v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    .line 1208
    iput v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_CELength_:I

    .line 1209
    iput v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_mapCE_:I

    .line 1210
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizePrim_:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 1211
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizeSec_:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 1212
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_sizeTer_:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 1213
    iput-boolean v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_variableTop_:Z

    .line 1214
    iput-boolean v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_caseBit_:Z

    .line 1215
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    .line 1235
    if-ne p1, p0, :cond_0

    .line 1236
    const/4 v2, 0x1

    .line 1247
    :goto_0
    return v2

    .line 1238
    :cond_0
    instance-of v2, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    if-eqz v2, :cond_1

    move-object v1, p1

    .line 1239
    check-cast v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;

    .line 1240
    .local v1, "t":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget v3, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    sub-int v0, v2, v3

    .line 1241
    .local v0, "size":I
    iget-object v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget v3, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    sub-int/2addr v2, v3

    if-ne v0, v2, :cond_1

    .line 1242
    iget-object v2, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v3, v1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    iget-object v4, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-virtual {v2, v3, v4, v5, v0}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v2

    goto :goto_0

    .line 1247
    .end local v0    # "size":I
    .end local v1    # "t":Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 1224
    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPoints_:Ljava/lang/String;

    iget v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$Elements;->m_cPointsOffset_:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1225
    .local v0, "str":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    return v1
.end method
