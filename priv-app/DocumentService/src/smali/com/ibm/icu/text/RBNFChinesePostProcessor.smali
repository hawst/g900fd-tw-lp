.class final Lcom/ibm/icu/text/RBNFChinesePostProcessor;
.super Ljava/lang/Object;
.source "RBNFChinesePostProcessor.java"

# interfaces
.implements Lcom/ibm/icu/text/RBNFPostProcessor;


# static fields
.field private static final rulesetNames:[Ljava/lang/String;


# instance fields
.field private format:I

.field private lastRuleSet:Lcom/ibm/icu/text/NFRuleSet;

.field private longForm:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "%traditional"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "%simplified"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "%accounting"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "%time"

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/RBNFChinesePostProcessor;->rulesetNames:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public init(Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V
    .locals 0
    .param p1, "formatter"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;
    .param p2, "rules"    # Ljava/lang/String;

    .prologue
    .line 29
    return-void
.end method

.method public process(Ljava/lang/StringBuffer;Lcom/ibm/icu/text/NFRuleSet;)V
    .locals 17
    .param p1, "buf"    # Ljava/lang/StringBuffer;
    .param p2, "ruleSet"    # Lcom/ibm/icu/text/NFRuleSet;

    .prologue
    .line 38
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/RBNFChinesePostProcessor;->lastRuleSet:Lcom/ibm/icu/text/NFRuleSet;

    move-object/from16 v0, p2

    if-eq v0, v13, :cond_1

    .line 39
    invoke-virtual/range {p2 .. p2}, Lcom/ibm/icu/text/NFRuleSet;->getName()Ljava/lang/String;

    move-result-object v8

    .line 40
    .local v8, "name":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    sget-object v13, Lcom/ibm/icu/text/RBNFChinesePostProcessor;->rulesetNames:[Ljava/lang/String;

    array-length v13, v13

    if-ge v3, v13, :cond_1

    .line 41
    sget-object v13, Lcom/ibm/icu/text/RBNFChinesePostProcessor;->rulesetNames:[Ljava/lang/String;

    aget-object v13, v13, v3

    invoke-virtual {v13, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 42
    move-object/from16 v0, p0

    iput v3, v0, Lcom/ibm/icu/text/RBNFChinesePostProcessor;->format:I

    .line 43
    const/4 v13, 0x1

    if-eq v3, v13, :cond_0

    const/4 v13, 0x3

    if-ne v3, v13, :cond_2

    :cond_0
    const/4 v13, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/ibm/icu/text/RBNFChinesePostProcessor;->longForm:Z

    .line 49
    .end local v3    # "i":I
    .end local v8    # "name":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/ibm/icu/text/RBNFChinesePostProcessor;->longForm:Z

    if-eqz v13, :cond_4

    .line 50
    const-string/jumbo v13, "*"

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/ibm/icu/impl/Utility;->indexOf(Ljava/lang/StringBuffer;Ljava/lang/String;)I

    move-result v3

    .restart local v3    # "i":I
    :goto_2
    const/4 v13, -0x1

    if-eq v3, v13, :cond_d

    .line 51
    add-int/lit8 v13, v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 50
    const-string/jumbo v13, "*"

    move-object/from16 v0, p1

    invoke-static {v0, v13, v3}, Lcom/ibm/icu/impl/Utility;->indexOf(Ljava/lang/StringBuffer;Ljava/lang/String;I)I

    move-result v3

    goto :goto_2

    .line 43
    .restart local v8    # "name":Ljava/lang/String;
    :cond_2
    const/4 v13, 0x0

    goto :goto_1

    .line 40
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 56
    .end local v3    # "i":I
    .end local v8    # "name":Ljava/lang/String;
    :cond_4
    const-string/jumbo v1, "\u9ede"

    .line 58
    .local v1, "DIAN":Ljava/lang/String;
    const/4 v13, 0x3

    new-array v6, v13, [[Ljava/lang/String;

    const/4 v13, 0x0

    const/4 v14, 0x4

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string/jumbo v16, "\u842c"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string/jumbo v16, "\u5104"

    aput-object v16, v14, v15

    const/4 v15, 0x2

    const-string/jumbo v16, "\u5146"

    aput-object v16, v14, v15

    const/4 v15, 0x3

    const-string/jumbo v16, "\u3007"

    aput-object v16, v14, v15

    aput-object v14, v6, v13

    const/4 v13, 0x1

    const/4 v14, 0x4

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string/jumbo v16, "\u4e07"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string/jumbo v16, "\u4ebf"

    aput-object v16, v14, v15

    const/4 v15, 0x2

    const-string/jumbo v16, "\u5146"

    aput-object v16, v14, v15

    const/4 v15, 0x3

    const-string/jumbo v16, "\u3007"

    aput-object v16, v14, v15

    aput-object v14, v6, v13

    const/4 v13, 0x2

    const/4 v14, 0x4

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string/jumbo v16, "\u842c"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string/jumbo v16, "\u5104"

    aput-object v16, v14, v15

    const/4 v15, 0x2

    const-string/jumbo v16, "\u5146"

    aput-object v16, v14, v15

    const/4 v15, 0x3

    const-string/jumbo v16, "\u96f6"

    aput-object v16, v14, v15

    aput-object v14, v6, v13

    .line 82
    .local v6, "markers":[[Ljava/lang/String;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/RBNFChinesePostProcessor;->format:I

    aget-object v5, v6, v13

    .line 83
    .local v5, "m":[Ljava/lang/String;
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    array-length v13, v5

    add-int/lit8 v13, v13, -0x1

    if-ge v3, v13, :cond_6

    .line 84
    aget-object v13, v5, v3

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/ibm/icu/impl/Utility;->indexOf(Ljava/lang/StringBuffer;Ljava/lang/String;)I

    move-result v7

    .line 85
    .local v7, "n":I
    const/4 v13, -0x1

    if-eq v7, v13, :cond_5

    .line 86
    aget-object v13, v5, v3

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v13, v7

    const/16 v14, 0x7c

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 83
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 91
    .end local v7    # "n":I
    :cond_6
    const-string/jumbo v13, "\u9ede"

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/ibm/icu/impl/Utility;->indexOf(Ljava/lang/StringBuffer;Ljava/lang/String;)I

    move-result v12

    .line 92
    .local v12, "x":I
    const/4 v13, -0x1

    if-ne v12, v13, :cond_7

    .line 93
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v12

    .line 95
    :cond_7
    const/4 v11, 0x0

    .line 96
    .local v11, "s":I
    const/4 v7, -0x1

    .line 97
    .restart local v7    # "n":I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/RBNFChinesePostProcessor;->format:I

    aget-object v13, v6, v13

    const/4 v14, 0x3

    aget-object v4, v13, v14

    .line 98
    .end local v5    # "m":[Ljava/lang/String;
    .local v4, "ling":Ljava/lang/String;
    :goto_4
    if-ltz v12, :cond_a

    .line 99
    const-string/jumbo v13, "|"

    move-object/from16 v0, p1

    invoke-static {v0, v13, v12}, Lcom/ibm/icu/impl/Utility;->lastIndexOf(Ljava/lang/StringBuffer;Ljava/lang/String;I)I

    move-result v5

    .line 100
    .local v5, "m":I
    move-object/from16 v0, p1

    invoke-static {v0, v4, v12}, Lcom/ibm/icu/impl/Utility;->lastIndexOf(Ljava/lang/StringBuffer;Ljava/lang/String;I)I

    move-result v9

    .line 101
    .local v9, "nn":I
    const/4 v10, 0x0

    .line 102
    .local v10, "ns":I
    if-le v9, v5, :cond_8

    .line 103
    if-lez v9, :cond_9

    add-int/lit8 v13, v9, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    const/16 v14, 0x2a

    if-eq v13, v14, :cond_9

    const/4 v10, 0x2

    .line 105
    :cond_8
    :goto_5
    add-int/lit8 v12, v5, -0x1

    .line 108
    mul-int/lit8 v13, v11, 0x3

    add-int/2addr v13, v10

    packed-switch v13, :pswitch_data_0

    .line 149
    new-instance v13, Ljava/lang/IllegalStateException;

    invoke-direct {v13}, Ljava/lang/IllegalStateException;-><init>()V

    throw v13

    .line 103
    :cond_9
    const/4 v10, 0x1

    goto :goto_5

    .line 110
    :pswitch_0
    move v11, v10

    .line 111
    const/4 v7, -0x1

    .line 112
    goto :goto_4

    .line 114
    :pswitch_1
    move v11, v10

    .line 115
    move v7, v9

    .line 116
    goto :goto_4

    .line 118
    :pswitch_2
    move v11, v10

    .line 119
    const/4 v7, -0x1

    .line 120
    goto :goto_4

    .line 122
    :pswitch_3
    move v11, v10

    .line 123
    const/4 v7, -0x1

    .line 124
    goto :goto_4

    .line 126
    :pswitch_4
    add-int/lit8 v13, v9, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v14, v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 127
    const/4 v11, 0x0

    .line 128
    const/4 v7, -0x1

    .line 129
    goto :goto_4

    .line 131
    :pswitch_5
    add-int/lit8 v13, v7, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v14, v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 132
    move v11, v10

    .line 133
    const/4 v7, -0x1

    .line 134
    goto :goto_4

    .line 136
    :pswitch_6
    move v11, v10

    .line 137
    const/4 v7, -0x1

    .line 138
    goto :goto_4

    .line 140
    :pswitch_7
    add-int/lit8 v13, v9, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v14, v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 141
    const/4 v11, 0x0

    .line 142
    const/4 v7, -0x1

    .line 143
    goto :goto_4

    .line 145
    :pswitch_8
    move v11, v10

    .line 146
    const/4 v7, -0x1

    .line 147
    goto :goto_4

    .line 153
    .end local v5    # "m":I
    .end local v9    # "nn":I
    .end local v10    # "ns":I
    :cond_a
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    :cond_b
    :goto_6
    add-int/lit8 v3, v3, -0x1

    if-ltz v3, :cond_d

    .line 154
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    .line 155
    .local v2, "c":C
    const/16 v13, 0x2a

    if-eq v2, v13, :cond_c

    const/16 v13, 0x7c

    if-ne v2, v13, :cond_b

    .line 156
    :cond_c
    add-int/lit8 v13, v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    goto :goto_6

    .line 159
    .end local v1    # "DIAN":Ljava/lang/String;
    .end local v2    # "c":C
    .end local v4    # "ling":Ljava/lang/String;
    .end local v6    # "markers":[[Ljava/lang/String;
    .end local v7    # "n":I
    .end local v11    # "s":I
    .end local v12    # "x":I
    :cond_d
    return-void

    .line 108
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
