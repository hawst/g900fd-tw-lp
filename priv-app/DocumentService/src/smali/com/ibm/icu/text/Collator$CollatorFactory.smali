.class public abstract Lcom/ibm/icu/text/Collator$CollatorFactory;
.super Ljava/lang/Object;
.source "Collator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/Collator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CollatorFactory"
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 423
    return-void
.end method


# virtual methods
.method public createCollator(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/Collator;
    .locals 1
    .param p1, "loc"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 360
    invoke-virtual {p1}, Lcom/ibm/icu/util/ULocale;->toLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/Collator$CollatorFactory;->createCollator(Ljava/util/Locale;)Lcom/ibm/icu/text/Collator;

    move-result-object v0

    return-object v0
.end method

.method public createCollator(Ljava/util/Locale;)Lcom/ibm/icu/text/Collator;
    .locals 1
    .param p1, "loc"    # Ljava/util/Locale;

    .prologue
    .line 375
    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/Collator$CollatorFactory;->createCollator(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/Collator;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayName(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;
    .locals 3
    .param p1, "objectLocale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "displayLocale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 399
    invoke-virtual {p0}, Lcom/ibm/icu/text/Collator$CollatorFactory;->visible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 400
    invoke-virtual {p0}, Lcom/ibm/icu/text/Collator$CollatorFactory;->getSupportedLocaleIDs()Ljava/util/Set;

    move-result-object v1

    .line 401
    .local v1, "supported":Ljava/util/Set;
    invoke-virtual {p1}, Lcom/ibm/icu/util/ULocale;->getBaseName()Ljava/lang/String;

    move-result-object v0

    .line 402
    .local v0, "name":Ljava/lang/String;
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 403
    invoke-virtual {p1, p2}, Lcom/ibm/icu/util/ULocale;->getDisplayName(Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v2

    .line 406
    .end local v0    # "name":Ljava/lang/String;
    .end local v1    # "supported":Ljava/util/Set;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getDisplayName(Ljava/util/Locale;Ljava/util/Locale;)Ljava/lang/String;
    .locals 2
    .param p1, "objectLocale"    # Ljava/util/Locale;
    .param p2, "displayLocale"    # Ljava/util/Locale;

    .prologue
    .line 387
    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {p2}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/ibm/icu/text/Collator$CollatorFactory;->getDisplayName(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract getSupportedLocaleIDs()Ljava/util/Set;
.end method

.method public visible()Z
    .locals 1

    .prologue
    .line 347
    const/4 v0, 0x1

    return v0
.end method
