.class final Lcom/ibm/icu/text/Normalizer$NFKDMode;
.super Lcom/ibm/icu/text/Normalizer$Mode;
.source "Normalizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/Normalizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "NFKDMode"
.end annotation


# direct methods
.method private constructor <init>(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 331
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/Normalizer$Mode;-><init>(ILcom/ibm/icu/text/Normalizer$1;)V

    .line 332
    return-void
.end method

.method constructor <init>(ILcom/ibm/icu/text/Normalizer$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Lcom/ibm/icu/text/Normalizer$1;

    .prologue
    .line 329
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/Normalizer$NFKDMode;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected getMask()I
    .locals 1

    .prologue
    .line 360
    const v0, 0xff08

    return v0
.end method

.method protected getMinC()I
    .locals 1

    .prologue
    .line 348
    const/16 v0, 0x300

    return v0
.end method

.method protected getNextBoundary()Lcom/ibm/icu/text/Normalizer$IsNextBoundary;
    .locals 2

    .prologue
    .line 356
    new-instance v0, Lcom/ibm/icu/text/Normalizer$IsNextNFDSafe;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/Normalizer$IsNextNFDSafe;-><init>(Lcom/ibm/icu/text/Normalizer$1;)V

    return-object v0
.end method

.method protected getPrevBoundary()Lcom/ibm/icu/text/Normalizer$IsPrevBoundary;
    .locals 2

    .prologue
    .line 352
    new-instance v0, Lcom/ibm/icu/text/Normalizer$IsPrevNFDSafe;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/Normalizer$IsPrevNFDSafe;-><init>(Lcom/ibm/icu/text/Normalizer$1;)V

    return-object v0
.end method

.method protected isNFSkippable(I)Z
    .locals 2
    .param p1, "c"    # I

    .prologue
    .line 379
    const-wide/32 v0, 0xff08

    invoke-static {p1, p0, v0, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->isNFSkippable(ILcom/ibm/icu/text/Normalizer$Mode;J)Z

    move-result v0

    return v0
.end method

.method protected normalize([CII[CIILcom/ibm/icu/text/UnicodeSet;)I
    .locals 9
    .param p1, "src"    # [C
    .param p2, "srcStart"    # I
    .param p3, "srcLimit"    # I
    .param p4, "dest"    # [C
    .param p5, "destStart"    # I
    .param p6, "destLimit"    # I
    .param p7, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 337
    const/4 v0, 0x1

    new-array v7, v0, [I

    .line 338
    .local v7, "trailCC":[I
    const/4 v6, 0x1

    move-object v0, p1

    move v1, p2

    move v2, p3

    move-object v3, p4

    move v4, p5

    move v5, p6

    move-object/from16 v8, p7

    invoke-static/range {v0 .. v8}, Lcom/ibm/icu/impl/NormalizerImpl;->decompose([CII[CIIZ[ILcom/ibm/icu/text/UnicodeSet;)I

    move-result v0

    return v0
.end method

.method protected normalize(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1, "src"    # Ljava/lang/String;
    .param p2, "options"    # I

    .prologue
    .line 344
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected quickCheck([CIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;
    .locals 8
    .param p1, "src"    # [C
    .param p2, "start"    # I
    .param p3, "limit"    # I
    .param p4, "allowMaybe"    # Z
    .param p5, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 366
    const/16 v0, 0x9

    invoke-static {v0}, Lcom/ibm/icu/impl/NormalizerImpl;->getFromIndexesArr(I)I

    move-result v3

    const/16 v4, 0x8

    const/16 v5, 0x1000

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v6, p4

    move-object v7, p5

    invoke-static/range {v0 .. v7}, Lcom/ibm/icu/impl/NormalizerImpl;->quickCheck([CIIIIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v0

    return-object v0
.end method
