.class Lcom/ibm/icu/text/UnescapeTransliterator$5;
.super Ljava/lang/Object;
.source "UnescapeTransliterator.java"

# interfaces
.implements Lcom/ibm/icu/text/Transliterator$Factory;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;
    .locals 3
    .param p1, "ID"    # Ljava/lang/String;

    .prologue
    .line 93
    new-instance v0, Lcom/ibm/icu/text/UnescapeTransliterator;

    const-string/jumbo v1, "Hex-Any/XML10"

    const/16 v2, 0x9

    new-array v2, v2, [C

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/UnescapeTransliterator;-><init>(Ljava/lang/String;[C)V

    return-object v0

    :array_0
    .array-data 2
        0x2s
        0x1s
        0xas
        0x1s
        0x7s
        0x26s
        0x23s
        0x3bs
        -0x1s
    .end array-data
.end method
