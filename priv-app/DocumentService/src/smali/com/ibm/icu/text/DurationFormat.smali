.class public abstract Lcom/ibm/icu/text/DurationFormat;
.super Lcom/ibm/icu/text/UFormat;
.source "DurationFormat.java"


# static fields
.field private static final serialVersionUID:J = -0x1cd2d9e4c38cc84aL


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/ibm/icu/text/UFormat;-><init>()V

    .line 42
    return-void
.end method

.method protected constructor <init>(Lcom/ibm/icu/util/ULocale;)V
    .locals 0
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/ibm/icu/text/UFormat;-><init>()V

    .line 50
    invoke-virtual {p0, p1, p1}, Lcom/ibm/icu/text/DurationFormat;->setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V

    .line 51
    return-void
.end method

.method public static getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DurationFormat;
    .locals 1
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 32
    invoke-static {p0}, Lcom/ibm/icu/impl/duration/BasicDurationFormat;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DurationFormat;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
.end method

.method public abstract formatDurationFrom(JJ)Ljava/lang/String;
.end method

.method public abstract formatDurationFromNow(J)Ljava/lang/String;
.end method

.method public abstract formatDurationFromNowTo(Ljava/util/Date;)Ljava/lang/String;
.end method

.method public parseObject(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Object;
    .locals 1
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;

    .prologue
    .line 72
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
