.class public final Lcom/ibm/icu/text/ArabicShaping;
.super Ljava/lang/Object;
.source "ArabicShaping.java"


# static fields
.field private static final ALEFTYPE:I = 0x20

.field public static final DIGITS_AN2EN:I = 0x40

.field public static final DIGITS_EN2AN:I = 0x20

.field public static final DIGITS_EN2AN_INIT_AL:I = 0x80

.field public static final DIGITS_EN2AN_INIT_LR:I = 0x60

.field public static final DIGITS_MASK:I = 0xe0

.field public static final DIGITS_NOOP:I = 0x0

.field public static final DIGIT_TYPE_AN:I = 0x0

.field public static final DIGIT_TYPE_AN_EXTENDED:I = 0x100

.field public static final DIGIT_TYPE_MASK:I = 0x100

.field private static final IRRELEVANT:I = 0x4

.field private static final LAMTYPE:I = 0x10

.field public static final LENGTH_FIXED_SPACES_AT_BEGINNING:I = 0x3

.field public static final LENGTH_FIXED_SPACES_AT_END:I = 0x2

.field public static final LENGTH_FIXED_SPACES_NEAR:I = 0x1

.field public static final LENGTH_GROW_SHRINK:I = 0x0

.field public static final LENGTH_MASK:I = 0x3

.field public static final LETTERS_MASK:I = 0x18

.field public static final LETTERS_NOOP:I = 0x0

.field public static final LETTERS_SHAPE:I = 0x8

.field public static final LETTERS_SHAPE_TASHKEEL_ISOLATED:I = 0x18

.field public static final LETTERS_UNSHAPE:I = 0x10

.field private static final LINKL:I = 0x2

.field private static final LINKR:I = 0x1

.field private static final LINK_MASK:I = 0x3

.field public static final TEXT_DIRECTION_LOGICAL:I = 0x0

.field public static final TEXT_DIRECTION_MASK:I = 0x4

.field public static final TEXT_DIRECTION_VISUAL_LTR:I = 0x4

.field private static final araLink:[I

.field static class$com$ibm$icu$text$ArabicShaping:Ljava/lang/Class;

.field private static convertFEto06:[I

.field private static final convertNormalizedLamAlef:[C

.field private static final irrelevantPos:[I

.field private static final presLink:[I

.field private static final shapeTable:[[[I


# instance fields
.field private isLogical:Z

.field private final options:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 402
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/text/ArabicShaping;->irrelevantPos:[I

    .line 419
    new-array v0, v3, [C

    fill-array-data v0, :array_1

    sput-object v0, Lcom/ibm/icu/text/ArabicShaping;->convertNormalizedLamAlef:[C

    .line 426
    const/16 v0, 0xb2

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/ibm/icu/text/ArabicShaping;->araLink:[I

    .line 504
    const/16 v0, 0x8d

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/ibm/icu/text/ArabicShaping;->presLink:[I

    .line 545
    const/16 v0, 0x8d

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/ibm/icu/text/ArabicShaping;->convertFEto06:[I

    .line 558
    new-array v0, v3, [[[I

    new-array v1, v3, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v1, v5

    new-array v2, v3, [I

    fill-array-data v2, :array_7

    aput-object v2, v1, v6

    new-array v2, v3, [I

    fill-array-data v2, :array_8

    aput-object v2, v1, v7

    aput-object v1, v0, v4

    new-array v1, v3, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_9

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_a

    aput-object v2, v1, v5

    new-array v2, v3, [I

    fill-array-data v2, :array_b

    aput-object v2, v1, v6

    new-array v2, v3, [I

    fill-array-data v2, :array_c

    aput-object v2, v1, v7

    aput-object v1, v0, v5

    new-array v1, v3, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_d

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_e

    aput-object v2, v1, v5

    new-array v2, v3, [I

    fill-array-data v2, :array_f

    aput-object v2, v1, v6

    new-array v2, v3, [I

    fill-array-data v2, :array_10

    aput-object v2, v1, v7

    aput-object v1, v0, v6

    new-array v1, v3, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_11

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_12

    aput-object v2, v1, v5

    new-array v2, v3, [I

    fill-array-data v2, :array_13

    aput-object v2, v1, v6

    new-array v2, v3, [I

    fill-array-data v2, :array_14

    aput-object v2, v1, v7

    aput-object v1, v0, v7

    sput-object v0, Lcom/ibm/icu/text/ArabicShaping;->shapeTable:[[[I

    return-void

    .line 402
    nop

    :array_0
    .array-data 4
        0x0
        0x2
        0x4
        0x6
        0x8
        0xa
        0xc
        0xe
    .end array-data

    .line 419
    :array_1
    .array-data 2
        0x622s
        0x623s
        0x625s
        0x627s
    .end array-data

    .line 426
    :array_2
    .array-data 4
        0x1121
        0x1321
        0x1501
        0x1721
        0x1903
        0x1d21
        0x1f03
        0x2301
        0x2503
        0x2903
        0x2d03
        0x3103
        0x3503
        0x3901
        0x3b01
        0x3d01
        0x3f01
        0x4103
        0x4503
        0x4903
        0x4d03
        0x5103
        0x5503
        0x5903
        0x5d03
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3
        0x6103
        0x6503
        0x6903
        0x6d13
        0x7103
        0x7503
        0x7903
        0x7d01
        0x7f01
        0x8103
        0x4
        0x4
        0x4
        0x4
        0x4
        0x4
        0x4
        0x4
        0x4
        0x4
        0x4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x8501
        0x8701
        0x8901
        0x8b01
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x4
        0x0
        0x21
        0x21
        0x0
        0x21
        0x1
        0x1
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x1
        0x3
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x3
        0x1
        0x3
        0x3
        0x3
        0x3
        0x1
        0x1
    .end array-data

    .line 504
    :array_3
    .array-data 4
        0x3
        0x3
        0x3
        0x0
        0x3
        0x0
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x3
        0x0
        0x20
        0x21
        0x20
        0x21
        0x0
        0x1
        0x20
        0x21
        0x0
        0x2
        0x3
        0x1
        0x20
        0x21
        0x0
        0x2
        0x3
        0x1
        0x0
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x1
        0x0
        0x1
        0x0
        0x1
        0x0
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x10
        0x12
        0x13
        0x11
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x1
        0x0
        0x1
        0x0
        0x2
        0x3
        0x1
        0x0
        0x1
        0x0
        0x1
        0x0
        0x1
        0x0
        0x1
    .end array-data

    .line 545
    :array_4
    .array-data 4
        0x64b
        0x64b
        0x64c
        0x64c
        0x64d
        0x64d
        0x64e
        0x64e
        0x64f
        0x64f
        0x650
        0x650
        0x651
        0x651
        0x652
        0x652
        0x621
        0x622
        0x622
        0x623
        0x623
        0x624
        0x624
        0x625
        0x625
        0x626
        0x626
        0x626
        0x626
        0x627
        0x627
        0x628
        0x628
        0x628
        0x628
        0x629
        0x629
        0x62a
        0x62a
        0x62a
        0x62a
        0x62b
        0x62b
        0x62b
        0x62b
        0x62c
        0x62c
        0x62c
        0x62c
        0x62d
        0x62d
        0x62d
        0x62d
        0x62e
        0x62e
        0x62e
        0x62e
        0x62f
        0x62f
        0x630
        0x630
        0x631
        0x631
        0x632
        0x632
        0x633
        0x633
        0x633
        0x633
        0x634
        0x634
        0x634
        0x634
        0x635
        0x635
        0x635
        0x635
        0x636
        0x636
        0x636
        0x636
        0x637
        0x637
        0x637
        0x637
        0x638
        0x638
        0x638
        0x638
        0x639
        0x639
        0x639
        0x639
        0x63a
        0x63a
        0x63a
        0x63a
        0x641
        0x641
        0x641
        0x641
        0x642
        0x642
        0x642
        0x642
        0x643
        0x643
        0x643
        0x643
        0x644
        0x644
        0x644
        0x644
        0x645
        0x645
        0x645
        0x645
        0x646
        0x646
        0x646
        0x646
        0x647
        0x647
        0x647
        0x647
        0x648
        0x648
        0x649
        0x649
        0x64a
        0x64a
        0x64a
        0x64a
        0x65c
        0x65c
        0x65d
        0x65d
        0x65e
        0x65e
        0x65f
        0x65f
    .end array-data

    .line 558
    :array_5
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_6
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x1
        0x0
        0x3
    .end array-data

    :array_8
    .array-data 4
        0x0
        0x1
        0x0
        0x1
    .end array-data

    :array_9
    .array-data 4
        0x0
        0x0
        0x2
        0x2
    .end array-data

    :array_a
    .array-data 4
        0x0
        0x0
        0x1
        0x2
    .end array-data

    :array_b
    .array-data 4
        0x0
        0x1
        0x1
        0x2
    .end array-data

    :array_c
    .array-data 4
        0x0
        0x1
        0x1
        0x3
    .end array-data

    :array_d
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_e
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_f
    .array-data 4
        0x0
        0x1
        0x0
        0x3
    .end array-data

    :array_10
    .array-data 4
        0x0
        0x1
        0x0
        0x3
    .end array-data

    :array_11
    .array-data 4
        0x0
        0x0
        0x1
        0x2
    .end array-data

    :array_12
    .array-data 4
        0x0
        0x0
        0x1
        0x2
    .end array-data

    :array_13
    .array-data 4
        0x0
        0x1
        0x1
        0x2
    .end array-data

    :array_14
    .array-data 4
        0x0
        0x1
        0x1
        0x3
    .end array-data
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "options"    # I

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    iput p1, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    .line 167
    and-int/lit16 v0, p1, 0xe0

    const/16 v1, 0x80

    if-le v0, v1, :cond_0

    .line 168
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "bad DIGITS options"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_0
    and-int/lit8 v0, p1, 0x4

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/ibm/icu/text/ArabicShaping;->isLogical:Z

    .line 171
    return-void

    .line 170
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private calculateSize([CII)I
    .locals 5
    .param p1, "source"    # [C
    .param p2, "sourceStart"    # I
    .param p3, "sourceLength"    # I

    .prologue
    const/16 v4, 0x644

    .line 747
    move v0, p3

    .line 749
    .local v0, "destSize":I
    iget v3, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit8 v3, v3, 0x18

    sparse-switch v3, :sswitch_data_0

    .line 779
    :cond_0
    return v0

    .line 752
    :sswitch_0
    iget-boolean v3, p0, Lcom/ibm/icu/text/ArabicShaping;->isLogical:Z

    if-eqz v3, :cond_2

    .line 753
    move v2, p2

    .local v2, "i":I
    add-int v3, p2, p3

    add-int/lit8 v1, v3, -0x1

    .local v1, "e":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 754
    aget-char v3, p1, v2

    if-ne v3, v4, :cond_1

    add-int/lit8 v3, v2, 0x1

    aget-char v3, p1, v3

    invoke-static {v3}, Lcom/ibm/icu/text/ArabicShaping;->isAlefChar(C)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 755
    add-int/lit8 v0, v0, -0x1

    .line 753
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 759
    .end local v1    # "e":I
    .end local v2    # "i":I
    :cond_2
    add-int/lit8 v2, p2, 0x1

    .restart local v2    # "i":I
    add-int v1, p2, p3

    .restart local v1    # "e":I
    :goto_1
    if-ge v2, v1, :cond_0

    .line 760
    aget-char v3, p1, v2

    if-ne v3, v4, :cond_3

    add-int/lit8 v3, v2, -0x1

    aget-char v3, p1, v3

    invoke-static {v3}, Lcom/ibm/icu/text/ArabicShaping;->isAlefChar(C)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 761
    add-int/lit8 v0, v0, -0x1

    .line 759
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 768
    .end local v1    # "e":I
    .end local v2    # "i":I
    :sswitch_1
    move v2, p2

    .restart local v2    # "i":I
    add-int v1, p2, p3

    .restart local v1    # "e":I
    :goto_2
    if-ge v2, v1, :cond_0

    .line 769
    aget-char v3, p1, v2

    invoke-static {v3}, Lcom/ibm/icu/text/ArabicShaping;->isLamAlefChar(C)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 770
    add-int/lit8 v0, v0, 0x1

    .line 768
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 749
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_0
    .end sparse-switch
.end method

.method private static changeLamAlef(C)C
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 630
    packed-switch p0, :pswitch_data_0

    .line 635
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 631
    :pswitch_1
    const/16 v0, 0x65c

    goto :goto_0

    .line 632
    :pswitch_2
    const/16 v0, 0x65d

    goto :goto_0

    .line 633
    :pswitch_3
    const/16 v0, 0x65e

    goto :goto_0

    .line 634
    :pswitch_4
    const/16 v0, 0x65f

    goto :goto_0

    .line 630
    nop

    :pswitch_data_0
    .packed-switch 0x622
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 339
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static countSpacesLeft([CII)I
    .locals 4
    .param p0, "dest"    # [C
    .param p1, "start"    # I
    .param p2, "count"    # I

    .prologue
    .line 690
    move v1, p1

    .local v1, "i":I
    add-int v0, p1, p2

    .local v0, "e":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 691
    aget-char v2, p0, v1

    const/16 v3, 0x20

    if-eq v2, v3, :cond_1

    .line 692
    sub-int p2, v1, p1

    .line 695
    .end local p2    # "count":I
    :cond_0
    return p2

    .line 690
    .restart local p2    # "count":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static countSpacesRight([CII)I
    .locals 3
    .param p0, "dest"    # [C
    .param p1, "start"    # I
    .param p2, "count"    # I

    .prologue
    .line 702
    add-int v0, p1, p2

    .local v0, "i":I
    :cond_0
    add-int/lit8 v0, v0, -0x1

    if-lt v0, p1, :cond_1

    .line 703
    aget-char v1, p0, v0

    const/16 v2, 0x20

    if-eq v1, v2, :cond_0

    .line 704
    add-int v1, p1, p2

    add-int/lit8 v1, v1, -0x1

    sub-int p2, v1, v0

    .line 707
    .end local p2    # "count":I
    :cond_1
    return p2
.end method

.method private deShapeUnicode([CIII)I
    .locals 1
    .param p1, "dest"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .param p4, "destSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/ArabicShapingException;
        }
    .end annotation

    .prologue
    .line 1104
    invoke-direct {p0, p1, p2, p3}, Lcom/ibm/icu/text/ArabicShaping;->normalize([CII)I

    move-result v0

    .line 1107
    .local v0, "lamalef_count":I
    if-eqz v0, :cond_0

    .line 1109
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/ibm/icu/text/ArabicShaping;->expandLamAlef([CIII)I

    move-result p4

    .line 1114
    :goto_0
    return p4

    .line 1111
    :cond_0
    move p4, p3

    goto :goto_0
.end method

.method private expandLamAlef([CIII)I
    .locals 10
    .param p1, "dest"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .param p4, "lacount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/ArabicShapingException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x644

    .line 865
    iget v7, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit8 v3, v7, 0x3

    .line 866
    .local v3, "lenOptions":I
    iget-boolean v7, p0, Lcom/ibm/icu/text/ArabicShaping;->isLogical:Z

    if-nez v7, :cond_0

    .line 867
    packed-switch v3, :pswitch_data_0

    .line 874
    :cond_0
    :goto_0
    packed-switch v3, :pswitch_data_1

    .line 944
    :cond_1
    :goto_1
    return p3

    .line 868
    :pswitch_0
    const/4 v3, 0x2

    goto :goto_0

    .line 869
    :pswitch_1
    const/4 v3, 0x3

    goto :goto_0

    .line 877
    :pswitch_2
    add-int v4, p2, p3

    .local v4, "r":I
    add-int v5, v4, p4

    .local v5, "w":I
    :goto_2
    add-int/lit8 v4, v4, -0x1

    if-lt v4, p2, :cond_3

    .line 878
    aget-char v0, p1, v4

    .line 879
    .local v0, "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/ArabicShaping;->isNormalizedLamAlefChar(C)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 880
    add-int/lit8 v5, v5, -0x1

    aput-char v9, p1, v5

    .line 881
    add-int/lit8 v5, v5, -0x1

    sget-object v7, Lcom/ibm/icu/text/ArabicShaping;->convertNormalizedLamAlef:[C

    add-int/lit16 v8, v0, -0x65c

    aget-char v7, v7, v8

    aput-char v7, p1, v5

    goto :goto_2

    .line 883
    :cond_2
    add-int/lit8 v5, v5, -0x1

    aput-char v0, p1, v5

    goto :goto_2

    .line 887
    .end local v0    # "ch":C
    :cond_3
    add-int/2addr p3, p4

    .line 888
    goto :goto_1

    .line 892
    .end local v4    # "r":I
    .end local v5    # "w":I
    :pswitch_3
    aget-char v7, p1, p2

    invoke-static {v7}, Lcom/ibm/icu/text/ArabicShaping;->isNormalizedLamAlefChar(C)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 893
    new-instance v7, Lcom/ibm/icu/text/ArabicShapingException;

    const-string/jumbo v8, "no space for lamalef"

    invoke-direct {v7, v8}, Lcom/ibm/icu/text/ArabicShapingException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 895
    :cond_4
    add-int v2, p2, p3

    .local v2, "i":I
    :cond_5
    :goto_3
    add-int/lit8 v2, v2, -0x1

    if-le v2, p2, :cond_1

    .line 896
    aget-char v0, p1, v2

    .line 897
    .restart local v0    # "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/ArabicShaping;->isNormalizedLamAlefChar(C)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 898
    add-int/lit8 v7, v2, -0x1

    aget-char v7, p1, v7

    const/16 v8, 0x20

    if-ne v7, v8, :cond_6

    .line 899
    aput-char v9, p1, v2

    .line 900
    add-int/lit8 v2, v2, -0x1

    sget-object v7, Lcom/ibm/icu/text/ArabicShaping;->convertNormalizedLamAlef:[C

    add-int/lit16 v8, v0, -0x65c

    aget-char v7, v7, v8

    aput-char v7, p1, v2

    goto :goto_3

    .line 902
    :cond_6
    new-instance v7, Lcom/ibm/icu/text/ArabicShapingException;

    const-string/jumbo v8, "no space for lamalef"

    invoke-direct {v7, v8}, Lcom/ibm/icu/text/ArabicShapingException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 911
    .end local v0    # "ch":C
    .end local v2    # "i":I
    :pswitch_4
    invoke-static {p1, p2, p3}, Lcom/ibm/icu/text/ArabicShaping;->countSpacesLeft([CII)I

    move-result v7

    if-le p4, v7, :cond_7

    .line 912
    new-instance v7, Lcom/ibm/icu/text/ArabicShapingException;

    const-string/jumbo v8, "no space for lamalef"

    invoke-direct {v7, v8}, Lcom/ibm/icu/text/ArabicShapingException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 914
    :cond_7
    add-int v4, p2, p4

    .restart local v4    # "r":I
    move v5, p2

    .restart local v5    # "w":I
    add-int v1, p2, p3

    .local v1, "e":I
    move v6, v5

    .end local v5    # "w":I
    .local v6, "w":I
    :goto_4
    if-ge v4, v1, :cond_1

    .line 915
    aget-char v0, p1, v4

    .line 916
    .restart local v0    # "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/ArabicShaping;->isNormalizedLamAlefChar(C)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 917
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "w":I
    .restart local v5    # "w":I
    sget-object v7, Lcom/ibm/icu/text/ArabicShaping;->convertNormalizedLamAlef:[C

    add-int/lit16 v8, v0, -0x65c

    aget-char v7, v7, v8

    aput-char v7, p1, v6

    .line 918
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "w":I
    .restart local v6    # "w":I
    aput-char v9, p1, v5

    move v5, v6

    .line 914
    .end local v6    # "w":I
    .restart local v5    # "w":I
    :goto_5
    add-int/lit8 v4, v4, 0x1

    move v6, v5

    .end local v5    # "w":I
    .restart local v6    # "w":I
    goto :goto_4

    .line 920
    :cond_8
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "w":I
    .restart local v5    # "w":I
    aput-char v0, p1, v6

    goto :goto_5

    .line 928
    .end local v0    # "ch":C
    .end local v1    # "e":I
    .end local v4    # "r":I
    .end local v5    # "w":I
    :pswitch_5
    invoke-static {p1, p2, p3}, Lcom/ibm/icu/text/ArabicShaping;->countSpacesRight([CII)I

    move-result v7

    if-le p4, v7, :cond_9

    .line 929
    new-instance v7, Lcom/ibm/icu/text/ArabicShapingException;

    const-string/jumbo v8, "no space for lamalef"

    invoke-direct {v7, v8}, Lcom/ibm/icu/text/ArabicShapingException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 931
    :cond_9
    add-int v7, p2, p3

    sub-int v4, v7, p4

    .restart local v4    # "r":I
    add-int v5, p2, p3

    .restart local v5    # "w":I
    :goto_6
    add-int/lit8 v4, v4, -0x1

    if-lt v4, p2, :cond_1

    .line 932
    aget-char v0, p1, v4

    .line 933
    .restart local v0    # "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/ArabicShaping;->isNormalizedLamAlefChar(C)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 934
    add-int/lit8 v5, v5, -0x1

    aput-char v9, p1, v5

    .line 935
    add-int/lit8 v5, v5, -0x1

    sget-object v7, Lcom/ibm/icu/text/ArabicShaping;->convertNormalizedLamAlef:[C

    add-int/lit16 v8, v0, -0x65c

    aget-char v7, v7, v8

    aput-char v7, p1, v5

    goto :goto_6

    .line 937
    :cond_a
    add-int/lit8 v5, v5, -0x1

    aput-char v0, p1, v5

    goto :goto_6

    .line 867
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 874
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static getLink(C)I
    .locals 2
    .param p0, "ch"    # C

    .prologue
    const v1, 0xfe70

    .line 669
    const/16 v0, 0x622

    if-lt p0, v0, :cond_0

    const/16 v0, 0x6d3

    if-gt p0, v0, :cond_0

    .line 670
    sget-object v0, Lcom/ibm/icu/text/ArabicShaping;->araLink:[I

    add-int/lit16 v1, p0, -0x622

    aget v0, v0, v1

    .line 678
    :goto_0
    return v0

    .line 671
    :cond_0
    const/16 v0, 0x200d

    if-ne p0, v0, :cond_1

    .line 672
    const/4 v0, 0x3

    goto :goto_0

    .line 673
    :cond_1
    const/16 v0, 0x206d

    if-lt p0, v0, :cond_2

    const/16 v0, 0x206f

    if-gt p0, v0, :cond_2

    .line 674
    const/4 v0, 0x4

    goto :goto_0

    .line 675
    :cond_2
    if-lt p0, v1, :cond_3

    const v0, 0xfefc

    if-gt p0, v0, :cond_3

    .line 676
    sget-object v0, Lcom/ibm/icu/text/ArabicShaping;->presLink:[I

    sub-int v1, p0, v1

    aget v0, v0, v1

    goto :goto_0

    .line 678
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private internalShape([CII[CII)I
    .locals 11
    .param p1, "source"    # [C
    .param p2, "sourceStart"    # I
    .param p3, "sourceLength"    # I
    .param p4, "dest"    # [C
    .param p5, "destStart"    # I
    .param p6, "destSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/ArabicShapingException;
        }
    .end annotation

    .prologue
    .line 1124
    if-nez p3, :cond_1

    .line 1125
    const/4 p3, 0x0

    .line 1229
    .end local p3    # "sourceLength":I
    :cond_0
    :goto_0
    return p3

    .line 1128
    .restart local p3    # "sourceLength":I
    :cond_1
    if-nez p6, :cond_2

    .line 1129
    iget v1, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit8 v1, v1, 0x18

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit8 v1, v1, 0x3

    if-nez v1, :cond_0

    .line 1132
    invoke-direct {p0, p1, p2, p3}, Lcom/ibm/icu/text/ArabicShaping;->calculateSize([CII)I

    move-result p3

    goto :goto_0

    .line 1139
    :cond_2
    mul-int/lit8 v1, p3, 0x2

    new-array v2, v1, [C

    .line 1140
    .local v2, "temp":[C
    const/4 v1, 0x0

    invoke-static {p1, p2, v2, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1142
    iget-boolean v1, p0, Lcom/ibm/icu/text/ArabicShaping;->isLogical:Z

    if-eqz v1, :cond_3

    .line 1143
    const/4 v1, 0x0

    invoke-static {v2, v1, p3}, Lcom/ibm/icu/text/ArabicShaping;->invertBuffer([CII)V

    .line 1146
    :cond_3
    move v4, p3

    .line 1148
    .local v4, "outputSize":I
    iget v1, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit8 v1, v1, 0x18

    sparse-switch v1, :sswitch_data_0

    .line 1165
    :goto_1
    move/from16 v0, p6

    if-le v4, v0, :cond_4

    .line 1166
    new-instance v1, Lcom/ibm/icu/text/ArabicShapingException;

    const-string/jumbo v3, "not enough room for result data"

    invoke-direct {v1, v3}, Lcom/ibm/icu/text/ArabicShapingException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1150
    :sswitch_0
    const/4 v3, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    move v4, p3

    move/from16 v5, p6

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/text/ArabicShaping;->shapeUnicode([CIIII)I

    .end local v4    # "outputSize":I
    move-result v4

    .line 1151
    .restart local v4    # "outputSize":I
    goto :goto_1

    .line 1154
    :sswitch_1
    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move v4, p3

    move/from16 v5, p6

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/text/ArabicShaping;->shapeUnicode([CIIII)I

    .end local v4    # "outputSize":I
    move-result v4

    .line 1155
    .restart local v4    # "outputSize":I
    goto :goto_1

    .line 1158
    :sswitch_2
    const/4 v1, 0x0

    move/from16 v0, p6

    invoke-direct {p0, v2, v1, p3, v0}, Lcom/ibm/icu/text/ArabicShaping;->deShapeUnicode([CIII)I

    move-result v4

    .line 1159
    goto :goto_1

    .line 1169
    :cond_4
    iget v1, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit16 v1, v1, 0xe0

    if-eqz v1, :cond_5

    .line 1170
    const/16 v5, 0x30

    .line 1171
    .local v5, "digitBase":C
    iget v1, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit16 v1, v1, 0x100

    sparse-switch v1, :sswitch_data_1

    .line 1184
    :goto_2
    iget v1, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit16 v1, v1, 0xe0

    sparse-switch v1, :sswitch_data_2

    .line 1223
    .end local v5    # "digitBase":C
    :cond_5
    :goto_3
    iget-boolean v1, p0, Lcom/ibm/icu/text/ArabicShaping;->isLogical:Z

    if-eqz v1, :cond_6

    .line 1224
    const/4 v1, 0x0

    invoke-static {v2, v1, v4}, Lcom/ibm/icu/text/ArabicShaping;->invertBuffer([CII)V

    .line 1227
    :cond_6
    const/4 v1, 0x0

    move/from16 v0, p5

    invoke-static {v2, v1, p4, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move p3, v4

    .line 1229
    goto :goto_0

    .line 1173
    .restart local v5    # "digitBase":C
    :sswitch_3
    const/16 v5, 0x660

    .line 1174
    goto :goto_2

    .line 1177
    :sswitch_4
    const/16 v5, 0x6f0

    .line 1178
    goto :goto_2

    .line 1187
    :sswitch_5
    add-int/lit8 v8, v5, -0x30

    .line 1188
    .local v8, "digitDelta":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_4
    if-ge v10, v4, :cond_5

    .line 1189
    aget-char v7, v2, v10

    .line 1190
    .local v7, "ch":C
    const/16 v1, 0x39

    if-gt v7, v1, :cond_7

    const/16 v1, 0x30

    if-lt v7, v1, :cond_7

    .line 1191
    aget-char v1, v2, v10

    add-int/2addr v1, v8

    int-to-char v1, v1

    aput-char v1, v2, v10

    .line 1188
    :cond_7
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .line 1199
    .end local v7    # "ch":C
    .end local v8    # "digitDelta":I
    .end local v10    # "i":I
    :sswitch_6
    add-int/lit8 v1, v5, 0x9

    int-to-char v9, v1

    .line 1200
    .local v9, "digitTop":C
    rsub-int/lit8 v8, v5, 0x30

    .line 1201
    .restart local v8    # "digitDelta":I
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_5
    if-ge v10, v4, :cond_5

    .line 1202
    aget-char v7, v2, v10

    .line 1203
    .restart local v7    # "ch":C
    if-gt v7, v9, :cond_8

    if-lt v7, v5, :cond_8

    .line 1204
    aget-char v1, v2, v10

    add-int/2addr v1, v8

    int-to-char v1, v1

    aput-char v1, v2, v10

    .line 1201
    :cond_8
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 1211
    .end local v7    # "ch":C
    .end local v8    # "digitDelta":I
    .end local v9    # "digitTop":C
    .end local v10    # "i":I
    :sswitch_7
    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/text/ArabicShaping;->shapeToArabicDigitsWithContext([CIICZ)V

    goto :goto_3

    .line 1215
    :sswitch_8
    const/4 v3, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/text/ArabicShaping;->shapeToArabicDigitsWithContext([CIICZ)V

    goto :goto_3

    .line 1148
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_0
    .end sparse-switch

    .line 1171
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_3
        0x100 -> :sswitch_4
    .end sparse-switch

    .line 1184
    :sswitch_data_2
    .sparse-switch
        0x20 -> :sswitch_5
        0x40 -> :sswitch_6
        0x60 -> :sswitch_7
        0x80 -> :sswitch_8
    .end sparse-switch
.end method

.method private static invertBuffer([CII)V
    .locals 4
    .param p0, "buffer"    # [C
    .param p1, "start"    # I
    .param p2, "length"    # I

    .prologue
    .line 614
    move v0, p1

    .local v0, "i":I
    add-int v3, p1, p2

    add-int/lit8 v1, v3, -0x1

    .local v1, "j":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 615
    aget-char v2, p0, v0

    .line 616
    .local v2, "temp":C
    aget-char v3, p0, v1

    aput-char v3, p0, v0

    .line 617
    aput-char v2, p0, v1

    .line 614
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 619
    .end local v2    # "temp":C
    :cond_0
    return-void
.end method

.method private static isAlefChar(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 723
    const/16 v0, 0x622

    if-eq p0, v0, :cond_0

    const/16 v0, 0x623

    if-eq p0, v0, :cond_0

    const/16 v0, 0x625

    if-eq p0, v0, :cond_0

    const/16 v0, 0x627

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isLamAlefChar(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 731
    const v0, 0xfef5

    if-lt p0, v0, :cond_0

    const v0, 0xfefc

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isNormalizedLamAlefChar(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 735
    const/16 v0, 0x65c

    if-lt p0, v0, :cond_0

    const/16 v0, 0x65f

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isTashkeelChar(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 715
    const/16 v0, 0x64b

    if-lt p0, v0, :cond_0

    const/16 v0, 0x652

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private normalize([CII)I
    .locals 7
    .param p1, "dest"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    const v6, 0xfe70

    .line 953
    const/4 v3, 0x0

    .line 954
    .local v3, "lacount":I
    move v2, p2

    .local v2, "i":I
    add-int v1, v2, p3

    .local v1, "e":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 955
    aget-char v0, p1, v2

    .line 956
    .local v0, "ch":C
    if-lt v0, v6, :cond_1

    const v4, 0xfefc

    if-gt v0, v4, :cond_1

    .line 957
    invoke-static {v0}, Lcom/ibm/icu/text/ArabicShaping;->isLamAlefChar(C)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 958
    add-int/lit8 v3, v3, 0x1

    .line 960
    :cond_0
    sget-object v4, Lcom/ibm/icu/text/ArabicShaping;->convertFEto06:[I

    sub-int v5, v0, v6

    aget v4, v4, v5

    int-to-char v4, v4

    aput-char v4, p1, v2

    .line 954
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 963
    .end local v0    # "ch":C
    :cond_2
    return v3
.end method

.method private removeLamAlefSpaces([CII)I
    .locals 11
    .param p1, "dest"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    const v10, 0xffff

    const/16 v9, 0x20

    .line 794
    iget v8, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit8 v3, v8, 0x3

    .line 795
    .local v3, "lenOptions":I
    iget-boolean v8, p0, Lcom/ibm/icu/text/ArabicShaping;->isLogical:Z

    if-nez v8, :cond_0

    .line 796
    packed-switch v3, :pswitch_data_0

    .line 803
    :cond_0
    :goto_0
    const/4 v8, 0x1

    if-ne v3, v8, :cond_2

    .line 804
    move v2, p2

    .local v2, "i":I
    add-int v1, v2, p3

    .local v1, "e":I
    :goto_1
    if-ge v2, v1, :cond_7

    .line 805
    aget-char v8, p1, v2

    if-ne v8, v10, :cond_1

    .line 806
    aput-char v9, p1, v2

    .line 804
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 797
    .end local v1    # "e":I
    .end local v2    # "i":I
    :pswitch_0
    const/4 v3, 0x2

    goto :goto_0

    .line 798
    :pswitch_1
    const/4 v3, 0x3

    goto :goto_0

    .line 810
    :cond_2
    add-int v1, p2, p3

    .line 811
    .restart local v1    # "e":I
    move v6, v1

    .line 812
    .local v6, "w":I
    move v4, v1

    .line 813
    .local v4, "r":I
    :cond_3
    :goto_2
    add-int/lit8 v4, v4, -0x1

    if-lt v4, p2, :cond_4

    .line 814
    aget-char v0, p1, v4

    .line 815
    .local v0, "ch":C
    if-eq v0, v10, :cond_3

    .line 816
    add-int/lit8 v6, v6, -0x1

    .line 817
    if-eq v6, v4, :cond_3

    .line 818
    aput-char v0, p1, v6

    goto :goto_2

    .line 823
    .end local v0    # "ch":C
    :cond_4
    const/4 v8, 0x2

    if-ne v3, v8, :cond_5

    .line 824
    :goto_3
    if-le v6, p2, :cond_7

    .line 825
    add-int/lit8 v6, v6, -0x1

    aput-char v9, p1, v6

    goto :goto_3

    .line 828
    :cond_5
    if-le v6, p2, :cond_6

    .line 830
    move v4, v6

    .line 831
    move v6, p2

    move v5, v4

    .end local v4    # "r":I
    .local v5, "r":I
    move v7, v6

    .line 832
    .end local v6    # "w":I
    .local v7, "w":I
    :goto_4
    if-ge v5, v1, :cond_9

    .line 833
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "w":I
    .restart local v6    # "w":I
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "r":I
    .restart local v4    # "r":I
    aget-char v8, p1, v5

    aput-char v8, p1, v7

    move v5, v4

    .end local v4    # "r":I
    .restart local v5    # "r":I
    move v7, v6

    .line 834
    .end local v6    # "w":I
    .restart local v7    # "w":I
    goto :goto_4

    .line 836
    .end local v5    # "r":I
    .end local v7    # "w":I
    .restart local v4    # "r":I
    .restart local v6    # "w":I
    :cond_6
    move v6, v1

    .line 838
    :goto_5
    if-nez v3, :cond_8

    .line 839
    sub-int p3, v6, p2

    .line 847
    .end local v4    # "r":I
    .end local v6    # "w":I
    :cond_7
    return p3

    .line 841
    .restart local v4    # "r":I
    .restart local v7    # "w":I
    :goto_6
    if-ge v7, v1, :cond_7

    .line 842
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "w":I
    .restart local v6    # "w":I
    aput-char v9, p1, v7

    move v7, v6

    .line 843
    .end local v6    # "w":I
    .restart local v7    # "w":I
    goto :goto_6

    .end local v7    # "w":I
    .restart local v6    # "w":I
    :cond_8
    move v7, v6

    .end local v6    # "w":I
    .restart local v7    # "w":I
    goto :goto_6

    .end local v4    # "r":I
    .restart local v5    # "r":I
    :cond_9
    move v4, v5

    .end local v5    # "r":I
    .restart local v4    # "r":I
    move v6, v7

    .end local v7    # "w":I
    .restart local v6    # "w":I
    goto :goto_5

    .line 796
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private shapeToArabicDigitsWithContext([CIICZ)V
    .locals 8
    .param p1, "dest"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .param p4, "digitBase"    # C
    .param p5, "lastStrongWasAL"    # Z

    .prologue
    .line 577
    :try_start_0
    invoke-static {}, Lcom/ibm/icu/impl/UBiDiProps;->getSingleton()Lcom/ibm/icu/impl/UBiDiProps;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 581
    .local v0, "bdp":Lcom/ibm/icu/impl/UBiDiProps;
    add-int/lit8 v4, p4, -0x30

    int-to-char p4, v4

    .line 583
    add-int v3, p2, p3

    .local v3, "i":I
    :cond_0
    :goto_0
    add-int/lit8 v3, v3, -0x1

    if-lt v3, p2, :cond_1

    .line 584
    aget-char v1, p1, v3

    .line 585
    .local v1, "ch":C
    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/UBiDiProps;->getClass(I)I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    goto :goto_0

    .line 588
    :sswitch_0
    const/4 p5, 0x0

    .line 589
    goto :goto_0

    .line 578
    .end local v0    # "bdp":Lcom/ibm/icu/impl/UBiDiProps;
    .end local v1    # "ch":C
    .end local v3    # "i":I
    :catch_0
    move-exception v2

    .line 579
    .local v2, "e":Ljava/io/IOException;
    new-instance v4, Ljava/util/MissingResourceException;

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "(BidiProps)"

    const-string/jumbo v7, ""

    invoke-direct {v4, v5, v6, v7}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v4

    .line 591
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "bdp":Lcom/ibm/icu/impl/UBiDiProps;
    .restart local v1    # "ch":C
    .restart local v3    # "i":I
    :sswitch_1
    const/4 p5, 0x1

    .line 592
    goto :goto_0

    .line 594
    :sswitch_2
    if-eqz p5, :cond_0

    const/16 v4, 0x39

    if-gt v1, v4, :cond_0

    .line 595
    add-int v4, v1, p4

    int-to-char v4, v4

    aput-char v4, p1, v3

    goto :goto_0

    .line 602
    .end local v1    # "ch":C
    :cond_1
    return-void

    .line 585
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0xd -> :sswitch_1
    .end sparse-switch
.end method

.method private shapeUnicode([CIIII)I
    .locals 16
    .param p1, "dest"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .param p4, "destSize"    # I
    .param p5, "tashkeelFlag"    # I

    .prologue
    .line 978
    invoke-direct/range {p0 .. p3}, Lcom/ibm/icu/text/ArabicShaping;->normalize([CII)I

    .line 985
    const/4 v4, 0x0

    .line 986
    .local v4, "lamalef_found":Z
    add-int v13, p2, p3

    add-int/lit8 v3, v13, -0x1

    .line 987
    .local v3, "i":I
    aget-char v13, p1, v3

    invoke-static {v13}, Lcom/ibm/icu/text/ArabicShaping;->getLink(C)I

    move-result v1

    .line 988
    .local v1, "currLink":I
    const/4 v7, 0x0

    .line 989
    .local v7, "nextLink":I
    const/4 v10, 0x0

    .line 990
    .local v10, "prevLink":I
    const/4 v5, 0x0

    .line 992
    .local v5, "lastLink":I
    move v6, v3

    .line 993
    .local v6, "lastPos":I
    const/4 v9, -0x2

    .line 994
    .local v9, "nx":I
    const/4 v8, 0x0

    .line 996
    .local v8, "nw":I
    :cond_0
    :goto_0
    if-ltz v3, :cond_f

    .line 998
    const v13, 0xff00

    and-int/2addr v13, v1

    if-gtz v13, :cond_1

    aget-char v13, p1, v3

    invoke-static {v13}, Lcom/ibm/icu/text/ArabicShaping;->isTashkeelChar(C)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 999
    :cond_1
    add-int/lit8 v8, v3, -0x1

    .line 1000
    const/4 v9, -0x2

    .line 1001
    :goto_1
    if-gez v9, :cond_4

    .line 1002
    const/4 v13, -0x1

    if-ne v8, v13, :cond_2

    .line 1003
    const/4 v7, 0x0

    .line 1004
    const v9, 0x7fffffff

    .line 1005
    goto :goto_1

    .line 1006
    :cond_2
    aget-char v13, p1, v8

    invoke-static {v13}, Lcom/ibm/icu/text/ArabicShaping;->getLink(C)I

    move-result v7

    .line 1007
    and-int/lit8 v13, v7, 0x4

    if-nez v13, :cond_3

    .line 1008
    move v9, v8

    .line 1009
    goto :goto_1

    .line 1010
    :cond_3
    add-int/lit8 v8, v8, -0x1

    .line 1013
    goto :goto_1

    .line 1015
    :cond_4
    and-int/lit8 v13, v1, 0x20

    if-lez v13, :cond_6

    and-int/lit8 v13, v5, 0x10

    if-lez v13, :cond_6

    .line 1016
    const/4 v4, 0x1

    .line 1017
    aget-char v13, p1, v3

    invoke-static {v13}, Lcom/ibm/icu/text/ArabicShaping;->changeLamAlef(C)C

    move-result v12

    .line 1018
    .local v12, "wLamalef":C
    if-eqz v12, :cond_5

    .line 1020
    const v13, 0xffff

    aput-char v13, p1, v3

    .line 1021
    aput-char v12, p1, v6

    .line 1022
    move v3, v6

    .line 1025
    :cond_5
    move v5, v10

    .line 1026
    invoke-static {v12}, Lcom/ibm/icu/text/ArabicShaping;->getLink(C)I

    move-result v1

    .line 1033
    .end local v12    # "wLamalef":C
    :cond_6
    aget-char v13, p1, v3

    invoke-static {v13}, Lcom/ibm/icu/text/ArabicShaping;->specialChar(C)I

    move-result v2

    .line 1035
    .local v2, "flag":I
    sget-object v13, Lcom/ibm/icu/text/ArabicShaping;->shapeTable:[[[I

    and-int/lit8 v14, v7, 0x3

    aget-object v13, v13, v14

    and-int/lit8 v14, v5, 0x3

    aget-object v13, v13, v14

    and-int/lit8 v14, v1, 0x3

    aget v11, v13, v14

    .line 1039
    .local v11, "shape":I
    const/4 v13, 0x1

    if-ne v2, v13, :cond_a

    .line 1040
    and-int/lit8 v11, v11, 0x1

    .line 1056
    :cond_7
    :goto_2
    const/4 v13, 0x2

    if-ne v2, v13, :cond_d

    .line 1057
    const/4 v13, 0x2

    move/from16 v0, p5

    if-ge v0, v13, :cond_8

    .line 1058
    const v13, 0xfe70

    sget-object v14, Lcom/ibm/icu/text/ArabicShaping;->irrelevantPos:[I

    aget-char v15, p1, v3

    add-int/lit16 v15, v15, -0x64b

    aget v14, v14, v15

    add-int/2addr v13, v14

    add-int/2addr v13, v11

    int-to-char v13, v13

    aput-char v13, p1, v3

    .line 1066
    .end local v2    # "flag":I
    .end local v11    # "shape":I
    :cond_8
    :goto_3
    and-int/lit8 v13, v1, 0x4

    if-nez v13, :cond_9

    .line 1067
    move v10, v5

    .line 1068
    move v5, v1

    .line 1070
    move v6, v3

    .line 1073
    :cond_9
    add-int/lit8 v3, v3, -0x1

    .line 1074
    if-ne v3, v9, :cond_e

    .line 1075
    move v1, v7

    .line 1076
    const/4 v9, -0x2

    .line 1077
    goto/16 :goto_0

    .line 1041
    .restart local v2    # "flag":I
    .restart local v11    # "shape":I
    :cond_a
    const/4 v13, 0x2

    if-ne v2, v13, :cond_7

    .line 1042
    if-nez p5, :cond_c

    and-int/lit8 v13, v5, 0x2

    if-eqz v13, :cond_c

    and-int/lit8 v13, v7, 0x1

    if-eqz v13, :cond_c

    aget-char v13, p1, v3

    const/16 v14, 0x64c

    if-eq v13, v14, :cond_c

    aget-char v13, p1, v3

    const/16 v14, 0x64d

    if-eq v13, v14, :cond_c

    and-int/lit8 v13, v7, 0x20

    const/16 v14, 0x20

    if-ne v13, v14, :cond_b

    and-int/lit8 v13, v5, 0x10

    const/16 v14, 0x10

    if-eq v13, v14, :cond_c

    .line 1050
    :cond_b
    const/4 v11, 0x1

    .line 1051
    goto :goto_2

    .line 1052
    :cond_c
    const/4 v11, 0x0

    goto :goto_2

    .line 1061
    :cond_d
    const v13, 0xfe70

    shr-int/lit8 v14, v1, 0x8

    add-int/2addr v13, v14

    add-int/2addr v13, v11

    int-to-char v13, v13

    aput-char v13, p1, v3

    goto :goto_3

    .line 1077
    .end local v2    # "flag":I
    .end local v11    # "shape":I
    :cond_e
    const/4 v13, -0x1

    if-eq v3, v13, :cond_0

    .line 1078
    aget-char v13, p1, v3

    invoke-static {v13}, Lcom/ibm/icu/text/ArabicShaping;->getLink(C)I

    move-result v1

    .line 1079
    goto/16 :goto_0

    .line 1085
    :cond_f
    if-eqz v4, :cond_10

    .line 1086
    invoke-direct/range {p0 .. p3}, Lcom/ibm/icu/text/ArabicShaping;->removeLamAlefSpaces([CII)I

    move-result p4

    .line 1091
    :goto_4
    return p4

    .line 1088
    :cond_10
    move/from16 p4, p3

    goto :goto_4
.end method

.method private static specialChar(C)I
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 645
    const/16 v0, 0x621

    if-le p0, v0, :cond_0

    const/16 v0, 0x626

    if-lt p0, v0, :cond_3

    :cond_0
    const/16 v0, 0x627

    if-eq p0, v0, :cond_3

    const/16 v0, 0x62e

    if-le p0, v0, :cond_1

    const/16 v0, 0x633

    if-lt p0, v0, :cond_3

    :cond_1
    const/16 v0, 0x647

    if-le p0, v0, :cond_2

    const/16 v0, 0x64a

    if-lt p0, v0, :cond_3

    :cond_2
    const/16 v0, 0x629

    if-ne p0, v0, :cond_4

    .line 650
    :cond_3
    const/4 v0, 0x1

    .line 658
    :goto_0
    return v0

    .line 651
    :cond_4
    const/16 v0, 0x64b

    if-lt p0, v0, :cond_5

    const/16 v0, 0x652

    if-gt p0, v0, :cond_5

    .line 652
    const/4 v0, 0x2

    goto :goto_0

    .line 653
    :cond_5
    const/16 v0, 0x653

    if-lt p0, v0, :cond_6

    const/16 v0, 0x655

    if-le p0, v0, :cond_7

    :cond_6
    const/16 v0, 0x670

    if-eq p0, v0, :cond_7

    const v0, 0xfe70

    if-lt p0, v0, :cond_8

    const v0, 0xfe7f

    if-gt p0, v0, :cond_8

    .line 656
    :cond_7
    const/4 v0, 0x3

    goto :goto_0

    .line 658
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "rhs"    # Ljava/lang/Object;

    .prologue
    .line 338
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sget-object v0, Lcom/ibm/icu/text/ArabicShaping;->class$com$ibm$icu$text$ArabicShaping:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string/jumbo v0, "com.ibm.icu.text.ArabicShaping"

    invoke-static {v0}, Lcom/ibm/icu/text/ArabicShaping;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/ibm/icu/text/ArabicShaping;->class$com$ibm$icu$text$ArabicShaping:Ljava/lang/Class;

    :goto_0
    if-ne v1, v0, :cond_1

    iget v0, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    check-cast p1, Lcom/ibm/icu/text/ArabicShaping;

    .end local p1    # "rhs":Ljava/lang/Object;
    iget v1, p1, Lcom/ibm/icu/text/ArabicShaping;->options:I

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .restart local p1    # "rhs":Ljava/lang/Object;
    :cond_0
    sget-object v0, Lcom/ibm/icu/text/ArabicShaping;->class$com$ibm$icu$text$ArabicShaping:Ljava/lang/Class;

    goto :goto_0

    .end local p1    # "rhs":Ljava/lang/Object;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 348
    iget v0, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    return v0
.end method

.method public shape([CII[CII)I
    .locals 3
    .param p1, "source"    # [C
    .param p2, "sourceStart"    # I
    .param p3, "sourceLength"    # I
    .param p4, "dest"    # [C
    .param p5, "destStart"    # I
    .param p6, "destSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/ArabicShapingException;
        }
    .end annotation

    .prologue
    .line 90
    if-nez p1, :cond_0

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "source can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_0
    if-ltz p2, :cond_1

    if-ltz p3, :cond_1

    add-int v0, p2, p3

    array-length v1, p1

    if-le v0, v1, :cond_2

    .line 94
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "bad source start ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ") or length ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ") for buffer of length "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_2
    if-nez p4, :cond_3

    if-eqz p6, :cond_3

    .line 99
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "null dest requires destSize == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_3
    if-eqz p6, :cond_5

    if-ltz p5, :cond_4

    if-ltz p6, :cond_4

    add-int v0, p5, p6

    array-length v1, p4

    if-le v0, v1, :cond_5

    .line 103
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "bad dest start ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ") or size ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ") for buffer of length "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    array-length v2, p4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_5
    invoke-direct/range {p0 .. p6}, Lcom/ibm/icu/text/ArabicShaping;->internalShape([CII[CII)I

    move-result v0

    return v0
.end method

.method public shape(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/ArabicShapingException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 137
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 138
    .local v1, "src":[C
    move-object v4, v1

    .line 139
    .local v4, "dest":[C
    iget v0, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit8 v0, v0, 0x3

    if-nez v0, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit8 v0, v0, 0x18

    const/16 v3, 0x10

    if-ne v0, v3, :cond_0

    .line 142
    array-length v0, v1

    mul-int/lit8 v0, v0, 0x2

    new-array v4, v0, [C

    .line 144
    :cond_0
    array-length v3, v1

    array-length v6, v4

    move-object v0, p0

    move v5, v2

    invoke-virtual/range {v0 .. v6}, Lcom/ibm/icu/text/ArabicShaping;->shape([CII[CII)I

    move-result v7

    .line 146
    .local v7, "len":I
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4, v2, v7}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method public shape([CII)V
    .locals 7
    .param p1, "source"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/ArabicShapingException;
        }
    .end annotation

    .prologue
    .line 122
    iget v0, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit8 v0, v0, 0x3

    if-nez v0, :cond_0

    .line 123
    new-instance v0, Lcom/ibm/icu/text/ArabicShapingException;

    const-string/jumbo v1, "Cannot shape in place with length option grow/shrink."

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/ArabicShapingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p1

    move v5, p2

    move v6, p3

    .line 125
    invoke-virtual/range {v0 .. v6}, Lcom/ibm/icu/text/ArabicShaping;->shape([CII[CII)I

    .line 126
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 355
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 356
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 357
    iget v1, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit8 v1, v1, 0x3

    packed-switch v1, :pswitch_data_0

    .line 363
    :goto_0
    iget v1, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit8 v1, v1, 0x4

    sparse-switch v1, :sswitch_data_0

    .line 367
    :goto_1
    iget v1, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit8 v1, v1, 0x18

    sparse-switch v1, :sswitch_data_1

    .line 373
    :goto_2
    iget v1, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit16 v1, v1, 0xe0

    sparse-switch v1, :sswitch_data_2

    .line 380
    :goto_3
    iget v1, p0, Lcom/ibm/icu/text/ArabicShaping;->options:I

    and-int/lit16 v1, v1, 0x100

    sparse-switch v1, :sswitch_data_3

    .line 384
    :goto_4
    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 386
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 358
    :pswitch_0
    const-string/jumbo v1, "grow/shrink"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 359
    :pswitch_1
    const-string/jumbo v1, "spaces near"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 360
    :pswitch_2
    const-string/jumbo v1, "spaces at end"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 361
    :pswitch_3
    const-string/jumbo v1, "spaces at beginning"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 364
    :sswitch_0
    const-string/jumbo v1, ", logical"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 365
    :sswitch_1
    const-string/jumbo v1, ", visual"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 368
    :sswitch_2
    const-string/jumbo v1, ", no letter shaping"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 369
    :sswitch_3
    const-string/jumbo v1, ", shape letters"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 370
    :sswitch_4
    const-string/jumbo v1, ", shape letters tashkeel isolated"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 371
    :sswitch_5
    const-string/jumbo v1, ", unshape letters"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 374
    :sswitch_6
    const-string/jumbo v1, ", no digit shaping"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 375
    :sswitch_7
    const-string/jumbo v1, ", shape digits to AN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 376
    :sswitch_8
    const-string/jumbo v1, ", shape digits to EN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 377
    :sswitch_9
    const-string/jumbo v1, ", shape digits to AN contextually: default EN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 378
    :sswitch_a
    const-string/jumbo v1, ", shape digits to AN contextually: default AL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 381
    :sswitch_b
    const-string/jumbo v1, ", standard Arabic-Indic digits"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 382
    :sswitch_c
    const-string/jumbo v1, ", extended Arabic-Indic digits"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_4

    .line 357
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 363
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_1
    .end sparse-switch

    .line 367
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_5
        0x18 -> :sswitch_4
    .end sparse-switch

    .line 373
    :sswitch_data_2
    .sparse-switch
        0x0 -> :sswitch_6
        0x20 -> :sswitch_7
        0x40 -> :sswitch_8
        0x60 -> :sswitch_9
        0x80 -> :sswitch_a
    .end sparse-switch

    .line 380
    :sswitch_data_3
    .sparse-switch
        0x0 -> :sswitch_b
        0x100 -> :sswitch_c
    .end sparse-switch
.end method
