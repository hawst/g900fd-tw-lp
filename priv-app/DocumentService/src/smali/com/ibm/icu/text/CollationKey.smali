.class public final Lcom/ibm/icu/text/CollationKey;
.super Ljava/lang/Object;
.source "CollationKey.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/CollationKey$BoundMode;
    }
.end annotation


# static fields
.field private static final MERGE_SEPERATOR_:I = 0x2


# instance fields
.field private m_hashCode_:I

.field private m_key_:[B

.field private m_length_:I

.field private m_source_:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/ibm/icu/text/RawCollationKey;)V
    .locals 1
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "key"    # Lcom/ibm/icu/text/RawCollationKey;

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    iput-object p1, p0, Lcom/ibm/icu/text/CollationKey;->m_source_:Ljava/lang/String;

    .line 167
    invoke-virtual {p2}, Lcom/ibm/icu/text/RawCollationKey;->releaseBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    .line 168
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/CollationKey;->m_hashCode_:I

    .line 169
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/CollationKey;->m_length_:I

    .line 170
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 1
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "key"    # [B

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    iput-object p1, p0, Lcom/ibm/icu/text/CollationKey;->m_source_:Ljava/lang/String;

    .line 148
    iput-object p2, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    .line 149
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/CollationKey;->m_hashCode_:I

    .line 150
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/CollationKey;->m_length_:I

    .line 151
    return-void
.end method

.method private getLength()I
    .locals 3

    .prologue
    .line 634
    iget v2, p0, Lcom/ibm/icu/text/CollationKey;->m_length_:I

    if-ltz v2, :cond_0

    .line 635
    iget v2, p0, Lcom/ibm/icu/text/CollationKey;->m_length_:I

    .line 645
    :goto_0
    return v2

    .line 637
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    array-length v1, v2

    .line 638
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 639
    iget-object v2, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v2, v2, v0

    if-nez v2, :cond_2

    .line 640
    move v1, v0

    .line 644
    :cond_1
    iput v1, p0, Lcom/ibm/icu/text/CollationKey;->m_length_:I

    .line 645
    iget v2, p0, Lcom/ibm/icu/text/CollationKey;->m_length_:I

    goto :goto_0

    .line 638
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public compareTo(Lcom/ibm/icu/text/CollationKey;)I
    .locals 4
    .param p1, "target"    # Lcom/ibm/icu/text/CollationKey;

    .prologue
    .line 252
    const/4 v0, 0x0

    .line 253
    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v3, v3, v0

    and-int/lit16 v1, v3, 0xff

    .line 254
    .local v1, "l":I
    iget-object v3, p1, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v3, v3, v0

    and-int/lit16 v2, v3, 0xff

    .line 255
    .local v2, "r":I
    if-ge v1, v2, :cond_0

    .line 256
    const/4 v3, -0x1

    .line 260
    :goto_1
    return v3

    .line 257
    :cond_0
    if-le v1, v2, :cond_1

    .line 258
    const/4 v3, 0x1

    goto :goto_1

    .line 259
    :cond_1
    if-nez v1, :cond_2

    .line 260
    const/4 v3, 0x0

    goto :goto_1

    .line 252
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 285
    check-cast p1, Lcom/ibm/icu/text/CollationKey;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/CollationKey;->compareTo(Lcom/ibm/icu/text/CollationKey;)I

    move-result v0

    return v0
.end method

.method public equals(Lcom/ibm/icu/text/CollationKey;)Z
    .locals 6
    .param p1, "target"    # Lcom/ibm/icu/text/CollationKey;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 330
    if-ne p0, p1, :cond_1

    .line 347
    :cond_0
    :goto_0
    return v2

    .line 333
    :cond_1
    if-nez p1, :cond_2

    move v2, v3

    .line 334
    goto :goto_0

    .line 336
    :cond_2
    move-object v1, p1

    .line 337
    .local v1, "other":Lcom/ibm/icu/text/CollationKey;
    const/4 v0, 0x0

    .line 339
    .local v0, "i":I
    :goto_1
    iget-object v4, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v4, v4, v0

    iget-object v5, v1, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v5, v5, v0

    if-eq v4, v5, :cond_3

    move v2, v3

    .line 340
    goto :goto_0

    .line 342
    :cond_3
    iget-object v4, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v4, v4, v0

    if-eqz v4, :cond_0

    .line 345
    add-int/lit8 v0, v0, 0x1

    .line 346
    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    .line 306
    instance-of v0, p1, Lcom/ibm/icu/text/CollationKey;

    if-nez v0, :cond_0

    .line 307
    const/4 v0, 0x0

    .line 310
    .end local p1    # "target":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "target":Ljava/lang/Object;
    :cond_0
    check-cast p1, Lcom/ibm/icu/text/CollationKey;

    .end local p1    # "target":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/CollationKey;->equals(Lcom/ibm/icu/text/CollationKey;)Z

    move-result v0

    goto :goto_0
.end method

.method public getBound(II)Lcom/ibm/icu/text/CollationKey;
    .locals 8
    .param p1, "boundType"    # I
    .param p2, "noOfLevels"    # I

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 444
    const/4 v1, 0x0

    .line 445
    .local v1, "offset":I
    const/4 v0, 0x0

    .line 447
    .local v0, "keystrength":I
    if-lez p2, :cond_3

    .line 448
    :goto_0
    iget-object v4, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    array-length v4, v4

    if-ge v1, v4, :cond_3

    iget-object v4, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v4, v4, v1

    if-eqz v4, :cond_3

    .line 449
    iget-object v4, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "offset":I
    .local v2, "offset":I
    aget-byte v4, v4, v1

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    .line 451
    add-int/lit8 v0, v0, 0x1

    .line 452
    add-int/lit8 p2, p2, -0x1

    .line 453
    if-eqz p2, :cond_0

    iget-object v4, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    array-length v4, v4

    if-eq v2, v4, :cond_0

    iget-object v4, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v4, v4, v2

    if-nez v4, :cond_2

    .line 455
    :cond_0
    add-int/lit8 v1, v2, -0x1

    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    move v2, v1

    .line 462
    .end local v1    # "offset":I
    .restart local v2    # "offset":I
    :goto_1
    if-lez p2, :cond_1

    .line 463
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "Source collation key has only "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, " strength level. Call getBound() again "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, " with noOfLevels < "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 473
    :cond_1
    add-int v4, v2, p1

    add-int/lit8 v4, v4, 0x1

    new-array v3, v4, [B

    .line 474
    .local v3, "resultkey":[B
    iget-object v4, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    invoke-static {v4, v6, v3, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 475
    packed-switch p1, :pswitch_data_0

    .line 489
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "Illegal boundType argument"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :pswitch_0
    move v1, v2

    .line 492
    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    :goto_2
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "offset":I
    .restart local v2    # "offset":I
    aput-byte v6, v3, v1

    .line 493
    new-instance v4, Lcom/ibm/icu/text/CollationKey;

    const/4 v5, 0x0

    invoke-direct {v4, v5, v3}, Lcom/ibm/icu/text/CollationKey;-><init>(Ljava/lang/String;[B)V

    return-object v4

    .line 481
    :pswitch_1
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    const/4 v4, 0x2

    aput-byte v4, v3, v2

    goto :goto_2

    .line 485
    .end local v1    # "offset":I
    .restart local v2    # "offset":I
    :pswitch_2
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    aput-byte v7, v3, v2

    .line 486
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "offset":I
    .restart local v2    # "offset":I
    aput-byte v7, v3, v1

    move v1, v2

    .line 487
    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    goto :goto_2

    .end local v1    # "offset":I
    .end local v3    # "resultkey":[B
    .restart local v2    # "offset":I
    :cond_2
    move v1, v2

    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    goto/16 :goto_0

    :cond_3
    move v2, v1

    .end local v1    # "offset":I
    .restart local v2    # "offset":I
    goto :goto_1

    .line 475
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getSourceString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/ibm/icu/text/CollationKey;->m_source_:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 362
    iget v3, p0, Lcom/ibm/icu/text/CollationKey;->m_hashCode_:I

    if-nez v3, :cond_0

    .line 363
    iget-object v3, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    if-nez v3, :cond_1

    .line 364
    const/4 v3, 0x1

    iput v3, p0, Lcom/ibm/icu/text/CollationKey;->m_hashCode_:I

    .line 380
    :cond_0
    :goto_0
    iget v3, p0, Lcom/ibm/icu/text/CollationKey;->m_hashCode_:I

    return v3

    .line 367
    :cond_1
    iget-object v3, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    array-length v3, v3

    shr-int/lit8 v2, v3, 0x1

    .line 368
    .local v2, "size":I
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 369
    .local v1, "key":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .line 370
    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v3, v3, v0

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    add-int/lit8 v4, v0, 0x1

    aget-byte v3, v3, v4

    if-eqz v3, :cond_2

    .line 371
    iget-object v3, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v3, v3, v0

    shl-int/lit8 v3, v3, 0x8

    iget-object v4, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    add-int/lit8 v5, v0, 0x1

    aget-byte v4, v4, v5

    or-int/2addr v3, v4

    int-to-char v3, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 372
    add-int/lit8 v0, v0, 0x2

    .line 373
    goto :goto_1

    .line 374
    :cond_2
    iget-object v3, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v3, v3, v0

    if-eqz v3, :cond_3

    .line 375
    iget-object v3, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v3, v3, v0

    shl-int/lit8 v3, v3, 0x8

    int-to-char v3, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 377
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    iput v3, p0, Lcom/ibm/icu/text/CollationKey;->m_hashCode_:I

    goto :goto_0
.end method

.method public merge(Lcom/ibm/icu/text/CollationKey;)Lcom/ibm/icu/text/CollationKey;
    .locals 11
    .param p1, "source"    # Lcom/ibm/icu/text/CollationKey;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 539
    if-eqz p1, :cond_0

    invoke-direct {p1}, Lcom/ibm/icu/text/CollationKey;->getLength()I

    move-result v8

    if-nez v8, :cond_1

    .line 540
    :cond_0
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v9, "CollationKey argument can not be null or of 0 length"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 544
    :cond_1
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationKey;->getLength()I

    .line 545
    invoke-direct {p1}, Lcom/ibm/icu/text/CollationKey;->getLength()I

    move-result v7

    .line 547
    .local v7, "sourcelength":I
    iget v8, p0, Lcom/ibm/icu/text/CollationKey;->m_length_:I

    add-int/2addr v8, v7

    add-int/lit8 v8, v8, 0x2

    new-array v2, v8, [B

    .line 550
    .local v2, "result":[B
    const/4 v3, 0x0

    .line 551
    .local v3, "rindex":I
    const/4 v0, 0x0

    .line 552
    .local v0, "index":I
    const/4 v5, 0x0

    .line 557
    .local v5, "sourceindex":I
    :goto_0
    iget-object v8, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v8, v8, v0

    if-ltz v8, :cond_2

    iget-object v8, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v8, v8, v0

    if-lt v8, v10, :cond_3

    .line 558
    :cond_2
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "rindex":I
    .local v4, "rindex":I
    iget-object v8, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "index":I
    .local v1, "index":I
    aget-byte v8, v8, v0

    aput-byte v8, v2, v3

    move v0, v1

    .end local v1    # "index":I
    .restart local v0    # "index":I
    move v3, v4

    .line 559
    .end local v4    # "rindex":I
    .restart local v3    # "rindex":I
    goto :goto_0

    .line 562
    :cond_3
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "rindex":I
    .restart local v4    # "rindex":I
    aput-byte v10, v2, v3

    move v3, v4

    .line 566
    .end local v4    # "rindex":I
    .restart local v3    # "rindex":I
    :goto_1
    iget-object v8, p1, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v8, v8, v5

    if-ltz v8, :cond_4

    iget-object v8, p1, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v8, v8, v5

    if-lt v8, v10, :cond_5

    .line 567
    :cond_4
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "rindex":I
    .restart local v4    # "rindex":I
    iget-object v8, p1, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    add-int/lit8 v6, v5, 0x1

    .end local v5    # "sourceindex":I
    .local v6, "sourceindex":I
    aget-byte v8, v8, v5

    aput-byte v8, v2, v3

    move v5, v6

    .end local v6    # "sourceindex":I
    .restart local v5    # "sourceindex":I
    move v3, v4

    .line 568
    .end local v4    # "rindex":I
    .restart local v3    # "rindex":I
    goto :goto_1

    .line 572
    :cond_5
    iget-object v8, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v8, v8, v0

    if-ne v8, v9, :cond_6

    iget-object v8, p1, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v8, v8, v5

    if-ne v8, v9, :cond_6

    .line 575
    add-int/lit8 v0, v0, 0x1

    .line 576
    add-int/lit8 v5, v5, 0x1

    .line 577
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "rindex":I
    .restart local v4    # "rindex":I
    aput-byte v9, v2, v3

    move v3, v4

    .line 578
    .end local v4    # "rindex":I
    .restart local v3    # "rindex":I
    goto :goto_0

    .line 587
    :cond_6
    iget-object v8, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v8, v8, v0

    if-eqz v8, :cond_8

    .line 588
    iget-object v8, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    iget v9, p0, Lcom/ibm/icu/text/CollationKey;->m_length_:I

    sub-int/2addr v9, v0

    invoke-static {v8, v0, v2, v3, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 595
    :cond_7
    :goto_2
    array-length v8, v2

    add-int/lit8 v8, v8, -0x1

    const/4 v9, 0x0

    aput-byte v9, v2, v8

    .line 598
    new-instance v8, Lcom/ibm/icu/text/CollationKey;

    const/4 v9, 0x0

    invoke-direct {v8, v9, v2}, Lcom/ibm/icu/text/CollationKey;-><init>(Ljava/lang/String;[B)V

    return-object v8

    .line 591
    :cond_8
    iget-object v8, p1, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v8, v8, v5

    if-eqz v8, :cond_7

    .line 592
    iget-object v8, p1, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    iget v9, p1, Lcom/ibm/icu/text/CollationKey;->m_length_:I

    sub-int/2addr v9, v5

    invoke-static {v8, v5, v2, v3, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2
.end method

.method public toByteArray()[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 217
    const/4 v0, 0x0

    .line 219
    .local v0, "length":I
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    aget-byte v2, v2, v0

    if-nez v2, :cond_0

    .line 224
    add-int/lit8 v0, v0, 0x1

    .line 225
    new-array v1, v0, [B

    .line 226
    .local v1, "result":[B
    iget-object v2, p0, Lcom/ibm/icu/text/CollationKey;->m_key_:[B

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 227
    return-object v1

    .line 222
    .end local v1    # "result":[B
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 223
    goto :goto_0
.end method
