.class Lcom/ibm/icu/text/RBBISetBuilder$RBBIDataManipulate;
.super Ljava/lang/Object;
.source "RBBISetBuilder.java"

# interfaces
.implements Lcom/ibm/icu/impl/TrieBuilder$DataManipulate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/RBBISetBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RBBIDataManipulate"
.end annotation


# instance fields
.field private final this$0:Lcom/ibm/icu/text/RBBISetBuilder;


# direct methods
.method constructor <init>(Lcom/ibm/icu/text/RBBISetBuilder;)V
    .locals 0

    .prologue
    .line 311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ibm/icu/text/RBBISetBuilder$RBBIDataManipulate;->this$0:Lcom/ibm/icu/text/RBBISetBuilder;

    return-void
.end method


# virtual methods
.method public getFoldedValue(II)I
    .locals 5
    .param p1, "start"    # I
    .param p2, "offset"    # I

    .prologue
    const/4 v3, 0x0

    .line 315
    const/4 v4, 0x1

    new-array v0, v4, [Z

    .line 317
    .local v0, "inBlockZero":[Z
    add-int/lit16 v1, p1, 0x400

    .line 318
    .local v1, "limit":I
    :goto_0
    if-ge p1, v1, :cond_1

    .line 319
    iget-object v4, p0, Lcom/ibm/icu/text/RBBISetBuilder$RBBIDataManipulate;->this$0:Lcom/ibm/icu/text/RBBISetBuilder;

    iget-object v4, v4, Lcom/ibm/icu/text/RBBISetBuilder;->fTrie:Lcom/ibm/icu/impl/IntTrieBuilder;

    invoke-virtual {v4, p1, v0}, Lcom/ibm/icu/impl/IntTrieBuilder;->getValue(I[Z)I

    move-result v2

    .line 320
    .local v2, "value":I
    aget-boolean v4, v0, v3

    if-eqz v4, :cond_0

    .line 321
    add-int/lit8 p1, p1, 0x20

    .line 322
    goto :goto_0

    :cond_0
    if-eqz v2, :cond_2

    .line 323
    const v3, 0x8000

    or-int/2addr v3, p2

    .line 328
    .end local v2    # "value":I
    :cond_1
    return v3

    .line 325
    .restart local v2    # "value":I
    :cond_2
    add-int/lit8 p1, p1, 0x1

    .line 327
    goto :goto_0
.end method
