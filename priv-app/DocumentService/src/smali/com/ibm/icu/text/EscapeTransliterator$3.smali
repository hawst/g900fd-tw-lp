.class Lcom/ibm/icu/text/EscapeTransliterator$3;
.super Ljava/lang/Object;
.source "EscapeTransliterator.java"

# interfaces
.implements Lcom/ibm/icu/text/Transliterator$Factory;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;
    .locals 15
    .param p1, "ID"    # Ljava/lang/String;

    .prologue
    .line 100
    new-instance v9, Lcom/ibm/icu/text/EscapeTransliterator;

    const-string/jumbo v8, "Any-Hex/C"

    const-string/jumbo v10, "\\u"

    const-string/jumbo v11, ""

    const/16 v12, 0x10

    const/4 v13, 0x4

    const/4 v14, 0x1

    new-instance v0, Lcom/ibm/icu/text/EscapeTransliterator;

    const-string/jumbo v1, ""

    const-string/jumbo v2, "\\U"

    const-string/jumbo v3, ""

    const/16 v4, 0x10

    const/16 v5, 0x8

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/ibm/icu/text/EscapeTransliterator;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/ibm/icu/text/EscapeTransliterator;)V

    move-object v1, v9

    move-object v2, v8

    move-object v3, v10

    move-object v4, v11

    move v5, v12

    move v6, v13

    move v7, v14

    move-object v8, v0

    invoke-direct/range {v1 .. v8}, Lcom/ibm/icu/text/EscapeTransliterator;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/ibm/icu/text/EscapeTransliterator;)V

    return-object v9
.end method
