.class Lcom/ibm/icu/text/RBBITableBuilder;
.super Ljava/lang/Object;
.source "RBBITableBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    }
.end annotation


# instance fields
.field private fDStates:Ljava/util/List;

.field private fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

.field private fRootIx:I


# direct methods
.method constructor <init>(Lcom/ibm/icu/text/RBBIRuleBuilder;I)V
    .locals 1
    .param p1, "rb"    # Lcom/ibm/icu/text/RBBIRuleBuilder;
    .param p2, "rootNodeIx"    # I

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput p2, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    .line 82
    iput-object p1, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    .line 84
    return-void
.end method


# virtual methods
.method bofFixup()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 495
    iget-object v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v4, v4, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v7, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v4, v4, v7

    iget-object v4, v4, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v0, v4, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 496
    .local v0, "bofNode":Lcom/ibm/icu/text/RBBINode;
    iget v4, v0, Lcom/ibm/icu/text/RBBINode;->fType:I

    if-ne v4, v8, :cond_1

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 497
    iget v4, v0, Lcom/ibm/icu/text/RBBINode;->fVal:I

    const/4 v7, 0x2

    if-ne v4, v7, :cond_2

    :goto_1
    invoke-static {v5}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 504
    iget-object v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v4, v4, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v5, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v4, v4, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v1, v4, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    .line 505
    .local v1, "matchStartNodes":Ljava/util/Set;
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 506
    .local v3, "startNodeIt":Ljava/util/Iterator;
    :cond_0
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 507
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/RBBINode;

    .line 508
    .local v2, "startNode":Lcom/ibm/icu/text/RBBINode;
    iget v4, v2, Lcom/ibm/icu/text/RBBINode;->fType:I

    if-ne v4, v8, :cond_0

    .line 512
    iget v4, v2, Lcom/ibm/icu/text/RBBINode;->fVal:I

    iget v5, v0, Lcom/ibm/icu/text/RBBINode;->fVal:I

    if-ne v4, v5, :cond_0

    .line 518
    iget-object v4, v0, Lcom/ibm/icu/text/RBBINode;->fFollowPos:Ljava/util/Set;

    iget-object v5, v2, Lcom/ibm/icu/text/RBBINode;->fFollowPos:Ljava/util/Set;

    invoke-interface {v4, v5}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .end local v1    # "matchStartNodes":Ljava/util/Set;
    .end local v2    # "startNode":Lcom/ibm/icu/text/RBBINode;
    .end local v3    # "startNodeIt":Ljava/util/Iterator;
    :cond_1
    move v4, v6

    .line 496
    goto :goto_0

    :cond_2
    move v5, v6

    .line 497
    goto :goto_1

    .line 521
    .restart local v1    # "matchStartNodes":Ljava/util/Set;
    .restart local v3    # "startNodeIt":Ljava/util/Iterator;
    :cond_3
    return-void
.end method

.method build()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x1

    .line 98
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v3, v3, v4

    if-nez v3, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    iget-object v5, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v6, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v5, v5, v6

    invoke-virtual {v5}, Lcom/ibm/icu/text/RBBINode;->flattenVariables()Lcom/ibm/icu/text/RBBINode;

    move-result-object v5

    aput-object v5, v3, v4

    .line 107
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v4, "ftree"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_2

    .line 108
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "Parse tree after flattening variable references."

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 109
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v3, v3, v4

    invoke-virtual {v3, v7}, Lcom/ibm/icu/text/RBBINode;->printTree(Z)V

    .line 118
    :cond_2
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    invoke-virtual {v3}, Lcom/ibm/icu/text/RBBISetBuilder;->sawBOF()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 119
    new-instance v1, Lcom/ibm/icu/text/RBBINode;

    invoke-direct {v1, v8}, Lcom/ibm/icu/text/RBBINode;-><init>(I)V

    .line 120
    .local v1, "bofTop":Lcom/ibm/icu/text/RBBINode;
    new-instance v0, Lcom/ibm/icu/text/RBBINode;

    const/4 v3, 0x3

    invoke-direct {v0, v3}, Lcom/ibm/icu/text/RBBINode;-><init>(I)V

    .line 121
    .local v0, "bofLeaf":Lcom/ibm/icu/text/RBBINode;
    iput-object v0, v1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 122
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v3, v3, v4

    iput-object v3, v1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    .line 123
    iput-object v1, v0, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 124
    const/4 v3, 0x2

    iput v3, v0, Lcom/ibm/icu/text/RBBINode;->fVal:I

    .line 125
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aput-object v1, v3, v4

    .line 133
    .end local v0    # "bofLeaf":Lcom/ibm/icu/text/RBBINode;
    .end local v1    # "bofTop":Lcom/ibm/icu/text/RBBINode;
    :cond_3
    new-instance v2, Lcom/ibm/icu/text/RBBINode;

    invoke-direct {v2, v8}, Lcom/ibm/icu/text/RBBINode;-><init>(I)V

    .line 134
    .local v2, "cn":Lcom/ibm/icu/text/RBBINode;
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v3, v3, v4

    iput-object v3, v2, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 135
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v3, v3, v4

    iput-object v2, v3, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 136
    new-instance v3, Lcom/ibm/icu/text/RBBINode;

    const/4 v4, 0x6

    invoke-direct {v3, v4}, Lcom/ibm/icu/text/RBBINode;-><init>(I)V

    iput-object v3, v2, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    .line 137
    iget-object v3, v2, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iput-object v2, v3, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 138
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aput-object v2, v3, v4

    .line 144
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/ibm/icu/text/RBBINode;->flattenSets()V

    .line 145
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v4, "stree"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_4

    .line 146
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "Parse tree after flattening Unicode Set references."

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 147
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v3, v3, v4

    invoke-virtual {v3, v7}, Lcom/ibm/icu/text/RBBINode;->printTree(Z)V

    .line 158
    :cond_4
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/ibm/icu/text/RBBITableBuilder;->calcNullable(Lcom/ibm/icu/text/RBBINode;)V

    .line 159
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/ibm/icu/text/RBBITableBuilder;->calcFirstPos(Lcom/ibm/icu/text/RBBINode;)V

    .line 160
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/ibm/icu/text/RBBITableBuilder;->calcLastPos(Lcom/ibm/icu/text/RBBINode;)V

    .line 161
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/ibm/icu/text/RBBITableBuilder;->calcFollowPos(Lcom/ibm/icu/text/RBBINode;)V

    .line 162
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v4, "pos"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_5

    .line 163
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 164
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/ibm/icu/text/RBBITableBuilder;->printPosSets(Lcom/ibm/icu/text/RBBINode;)V

    .line 170
    :cond_5
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-boolean v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fChainRules:Z

    if-eqz v3, :cond_6

    .line 171
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v3, v3, v4

    invoke-virtual {p0, v3}, Lcom/ibm/icu/text/RBBITableBuilder;->calcChainedFollowPos(Lcom/ibm/icu/text/RBBINode;)V

    .line 177
    :cond_6
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    invoke-virtual {v3}, Lcom/ibm/icu/text/RBBISetBuilder;->sawBOF()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 178
    invoke-virtual {p0}, Lcom/ibm/icu/text/RBBITableBuilder;->bofFixup()V

    .line 184
    :cond_7
    invoke-virtual {p0}, Lcom/ibm/icu/text/RBBITableBuilder;->buildStateTable()V

    .line 185
    invoke-virtual {p0}, Lcom/ibm/icu/text/RBBITableBuilder;->flagAcceptingStates()V

    .line 186
    invoke-virtual {p0}, Lcom/ibm/icu/text/RBBITableBuilder;->flagLookAheadStates()V

    .line 187
    invoke-virtual {p0}, Lcom/ibm/icu/text/RBBITableBuilder;->flagTaggedStates()V

    .line 194
    invoke-virtual {p0}, Lcom/ibm/icu/text/RBBITableBuilder;->mergeRuleStatusVals()V

    .line 196
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v4, "states"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_0

    invoke-virtual {p0}, Lcom/ibm/icu/text/RBBITableBuilder;->printStates()V

    goto/16 :goto_0
.end method

.method buildStateTable()V
    .locals 20

    .prologue
    .line 535
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/ibm/icu/text/RBBISetBuilder;->getNumCharCategories()I

    move-result v17

    add-int/lit8 v9, v17, -0x1

    .line 536
    .local v9, "lastInputSymbol":I
    new-instance v6, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;

    invoke-direct {v6, v9}, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;-><init>(I)V

    .line 537
    .local v6, "failState":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 541
    new-instance v7, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;

    invoke-direct {v7, v9}, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;-><init>(I)V

    .line 542
    .local v7, "initialState":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    iget-object v0, v7, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fPositions:Ljava/util/Set;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    move/from16 v19, v0

    aget-object v18, v18, v19

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 543
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 547
    :cond_0
    const/4 v2, 0x0

    .line 549
    .local v2, "T":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    const/4 v15, 0x1

    .local v15, "tx":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v15, v0, :cond_1

    .line 550
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;

    .line 551
    .local v13, "temp":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    iget-boolean v0, v13, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fMarked:Z

    move/from16 v17, v0

    if-nez v17, :cond_2

    .line 552
    move-object v2, v13

    .line 556
    .end local v13    # "temp":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    :cond_1
    if-nez v2, :cond_3

    .line 613
    return-void

    .line 549
    .restart local v13    # "temp":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    :cond_2
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 561
    .end local v13    # "temp":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    :cond_3
    const/16 v17, 0x1

    move/from16 v0, v17

    iput-boolean v0, v2, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fMarked:Z

    .line 565
    const/4 v5, 0x1

    .local v5, "a":I
    :goto_1
    if-gt v5, v9, :cond_0

    .line 569
    const/4 v3, 0x0

    .line 571
    .local v3, "U":Ljava/util/Set;
    iget-object v0, v2, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fPositions:Ljava/util/Set;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 572
    .local v12, "pit":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_6

    .line 573
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/ibm/icu/text/RBBINode;

    .line 574
    .local v11, "p":Lcom/ibm/icu/text/RBBINode;
    iget v0, v11, Lcom/ibm/icu/text/RBBINode;->fType:I

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    iget v0, v11, Lcom/ibm/icu/text/RBBINode;->fVal:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v0, v5, :cond_4

    .line 575
    if-nez v3, :cond_5

    .line 576
    new-instance v3, Ljava/util/HashSet;

    .end local v3    # "U":Ljava/util/Set;
    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 578
    .restart local v3    # "U":Ljava/util/Set;
    :cond_5
    iget-object v0, v11, Lcom/ibm/icu/text/RBBINode;->fFollowPos:Ljava/util/Set;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 583
    .end local v11    # "p":Lcom/ibm/icu/text/RBBINode;
    :cond_6
    const/16 v16, 0x0

    .line 584
    .local v16, "ux":I
    const/4 v4, 0x0

    .line 585
    .local v4, "UinDstates":Z
    if-eqz v3, :cond_9

    .line 586
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v17

    if-lez v17, :cond_a

    const/16 v17, 0x1

    :goto_3
    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 588
    const/4 v8, 0x0

    .local v8, "ix":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v8, v0, :cond_7

    .line 590
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;

    .line 591
    .local v14, "temp2":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    iget-object v0, v14, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fPositions:Ljava/util/Set;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 592
    iget-object v3, v14, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fPositions:Ljava/util/Set;

    .line 593
    move/from16 v16, v8

    .line 594
    const/4 v4, 0x1

    .line 600
    .end local v14    # "temp2":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    :cond_7
    if-nez v4, :cond_8

    .line 602
    new-instance v10, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;

    invoke-direct {v10, v9}, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;-><init>(I)V

    .line 603
    .local v10, "newState":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    iput-object v3, v10, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fPositions:Ljava/util/Set;

    .line 604
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 605
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    add-int/lit8 v16, v17, -0x1

    .line 609
    .end local v10    # "newState":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    :cond_8
    iget-object v0, v2, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fDtran:[I

    move-object/from16 v17, v0

    aput v16, v17, v5

    .line 565
    .end local v8    # "ix":I
    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 586
    :cond_a
    const/16 v17, 0x0

    goto :goto_3

    .line 588
    .restart local v8    # "ix":I
    .restart local v14    # "temp2":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    :cond_b
    add-int/lit8 v8, v8, 0x1

    goto :goto_4
.end method

.method calcChainedFollowPos(Lcom/ibm/icu/text/RBBINode;)V
    .locals 16
    .param p1, "tree"    # Lcom/ibm/icu/text/RBBINode;

    .prologue
    .line 387
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 388
    .local v4, "endMarkerNodes":Ljava/util/List;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 391
    .local v8, "leafNodes":Ljava/util/List;
    const/4 v14, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, Lcom/ibm/icu/text/RBBINode;->findNodes(Ljava/util/List;I)V

    .line 394
    const/4 v14, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v14}, Lcom/ibm/icu/text/RBBINode;->findNodes(Ljava/util/List;I)V

    .line 399
    move-object/from16 v13, p1

    .line 400
    .local v13, "userRuleRoot":Lcom/ibm/icu/text/RBBINode;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v14, v14, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    invoke-virtual {v14}, Lcom/ibm/icu/text/RBBISetBuilder;->sawBOF()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 401
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v13, v14, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    .line 403
    :cond_0
    if-eqz v13, :cond_6

    const/4 v14, 0x1

    :goto_0
    invoke-static {v14}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 404
    iget-object v9, v13, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    .line 409
    .local v9, "matchStartNodes":Ljava/util/Set;
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 411
    .local v6, "endNodeIx":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 412
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/ibm/icu/text/RBBINode;

    .line 413
    .local v12, "tNode":Lcom/ibm/icu/text/RBBINode;
    const/4 v5, 0x0

    .line 417
    .local v5, "endNode":Lcom/ibm/icu/text/RBBINode;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 418
    .local v7, "i":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 419
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/text/RBBINode;

    .line 420
    .local v3, "endMarkerNode":Lcom/ibm/icu/text/RBBINode;
    iget-object v14, v12, Lcom/ibm/icu/text/RBBINode;->fFollowPos:Ljava/util/Set;

    invoke-interface {v14, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 421
    move-object v5, v12

    .line 425
    .end local v3    # "endMarkerNode":Lcom/ibm/icu/text/RBBINode;
    :cond_3
    if-eqz v5, :cond_1

    .line 436
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-boolean v14, v14, Lcom/ibm/icu/text/RBBIRuleBuilder;->fLBCMNoChain:Z

    if-eqz v14, :cond_4

    .line 437
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v14, v14, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    iget v15, v5, Lcom/ibm/icu/text/RBBINode;->fVal:I

    invoke-virtual {v14, v15}, Lcom/ibm/icu/text/RBBISetBuilder;->getFirstChar(I)I

    move-result v1

    .line 438
    .local v1, "c":I
    const/4 v14, -0x1

    if-eq v1, v14, :cond_4

    .line 440
    const/16 v14, 0x1008

    invoke-static {v1, v14}, Lcom/ibm/icu/lang/UCharacter;->getIntPropertyValue(II)I

    move-result v2

    .line 441
    .local v2, "cLBProp":I
    const/16 v14, 0x9

    if-eq v2, v14, :cond_1

    .line 451
    .end local v1    # "c":I
    .end local v2    # "cLBProp":I
    :cond_4
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 452
    .local v11, "startNodeIx":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 453
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/ibm/icu/text/RBBINode;

    .line 454
    .local v10, "startNode":Lcom/ibm/icu/text/RBBINode;
    iget v14, v10, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v15, 0x3

    if-ne v14, v15, :cond_5

    .line 458
    iget v14, v5, Lcom/ibm/icu/text/RBBINode;->fVal:I

    iget v15, v10, Lcom/ibm/icu/text/RBBINode;->fVal:I

    if-ne v14, v15, :cond_5

    .line 466
    iget-object v14, v5, Lcom/ibm/icu/text/RBBINode;->fFollowPos:Ljava/util/Set;

    iget-object v15, v10, Lcom/ibm/icu/text/RBBINode;->fFollowPos:Ljava/util/Set;

    invoke-interface {v14, v15}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 403
    .end local v5    # "endNode":Lcom/ibm/icu/text/RBBINode;
    .end local v6    # "endNodeIx":Ljava/util/Iterator;
    .end local v7    # "i":Ljava/util/Iterator;
    .end local v9    # "matchStartNodes":Ljava/util/Set;
    .end local v10    # "startNode":Lcom/ibm/icu/text/RBBINode;
    .end local v11    # "startNodeIx":Ljava/util/Iterator;
    .end local v12    # "tNode":Lcom/ibm/icu/text/RBBINode;
    :cond_6
    const/4 v14, 0x0

    goto :goto_0

    .line 470
    .restart local v6    # "endNodeIx":Ljava/util/Iterator;
    .restart local v9    # "matchStartNodes":Ljava/util/Set;
    :cond_7
    return-void
.end method

.method calcFirstPos(Lcom/ibm/icu/text/RBBINode;)V
    .locals 2
    .param p1, "n"    # Lcom/ibm/icu/text/RBBINode;

    .prologue
    .line 254
    if-nez p1, :cond_1

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 257
    :cond_1
    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 262
    :cond_2
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 268
    :cond_3
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RBBITableBuilder;->calcFirstPos(Lcom/ibm/icu/text/RBBINode;)V

    .line 269
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RBBITableBuilder;->calcFirstPos(Lcom/ibm/icu/text/RBBINode;)V

    .line 272
    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_4

    .line 273
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 274
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 276
    :cond_4
    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 277
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 278
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget-boolean v0, v0, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 282
    :cond_5
    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_6

    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v1, 0xc

    if-eq v0, v1, :cond_6

    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 285
    :cond_6
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method calcFollowPos(Lcom/ibm/icu/text/RBBINode;)V
    .locals 5
    .param p1, "n"    # Lcom/ibm/icu/text/RBBINode;

    .prologue
    .line 340
    if-eqz p1, :cond_0

    iget v3, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    iget v3, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v4, 0x6

    if-ne v3, v4, :cond_1

    .line 376
    :cond_0
    return-void

    .line 346
    :cond_1
    iget-object v3, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {p0, v3}, Lcom/ibm/icu/text/RBBITableBuilder;->calcFollowPos(Lcom/ibm/icu/text/RBBINode;)V

    .line 347
    iget-object v3, p1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {p0, v3}, Lcom/ibm/icu/text/RBBITableBuilder;->calcFollowPos(Lcom/ibm/icu/text/RBBINode;)V

    .line 350
    iget v3, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    .line 353
    iget-object v3, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v0, v3, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    .line 355
    .local v0, "LastPosOfLeftChild":Ljava/util/Set;
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 356
    .local v2, "ix":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 357
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/RBBINode;

    .line 358
    .local v1, "i":Lcom/ibm/icu/text/RBBINode;
    iget-object v3, v1, Lcom/ibm/icu/text/RBBINode;->fFollowPos:Ljava/util/Set;

    iget-object v4, p1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v4, v4, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    invoke-interface {v3, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 363
    .end local v0    # "LastPosOfLeftChild":Ljava/util/Set;
    .end local v1    # "i":Lcom/ibm/icu/text/RBBINode;
    .end local v2    # "ix":Ljava/util/Iterator;
    :cond_2
    iget v3, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v4, 0xa

    if-eq v3, v4, :cond_3

    iget v3, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v4, 0xb

    if-ne v3, v4, :cond_0

    .line 366
    :cond_3
    iget-object v3, p1, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 367
    .restart local v2    # "ix":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 368
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/RBBINode;

    .line 369
    .restart local v1    # "i":Lcom/ibm/icu/text/RBBINode;
    iget-object v3, v1, Lcom/ibm/icu/text/RBBINode;->fFollowPos:Ljava/util/Set;

    iget-object v4, p1, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    invoke-interface {v3, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method calcLastPos(Lcom/ibm/icu/text/RBBINode;)V
    .locals 2
    .param p1, "n"    # Lcom/ibm/icu/text/RBBINode;

    .prologue
    .line 297
    if-nez p1, :cond_1

    .line 330
    :cond_0
    :goto_0
    return-void

    .line 300
    :cond_1
    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 305
    :cond_2
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 311
    :cond_3
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RBBITableBuilder;->calcLastPos(Lcom/ibm/icu/text/RBBINode;)V

    .line 312
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RBBITableBuilder;->calcLastPos(Lcom/ibm/icu/text/RBBINode;)V

    .line 315
    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_4

    .line 316
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 317
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 319
    :cond_4
    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 320
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 321
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iget-boolean v0, v0, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 325
    :cond_5
    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_6

    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v1, 0xc

    if-eq v0, v1, :cond_6

    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 328
    :cond_6
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method calcNullable(Lcom/ibm/icu/text/RBBINode;)V
    .locals 4
    .param p1, "n"    # Lcom/ibm/icu/text/RBBINode;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 207
    if-nez p1, :cond_0

    .line 243
    :goto_0
    return-void

    .line 210
    :cond_0
    iget v2, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    if-eqz v2, :cond_1

    iget v2, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_2

    .line 213
    :cond_1
    iput-boolean v0, p1, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    goto :goto_0

    .line 217
    :cond_2
    iget v2, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_3

    iget v2, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_4

    .line 220
    :cond_3
    iput-boolean v1, p1, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    goto :goto_0

    .line 227
    :cond_4
    iget-object v2, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/RBBITableBuilder;->calcNullable(Lcom/ibm/icu/text/RBBINode;)V

    .line 228
    iget-object v2, p1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/RBBITableBuilder;->calcNullable(Lcom/ibm/icu/text/RBBINode;)V

    .line 231
    iget v2, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v3, 0x9

    if-ne v2, v3, :cond_7

    .line 232
    iget-object v2, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget-boolean v2, v2, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    if-nez v2, :cond_5

    iget-object v2, p1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iget-boolean v2, v2, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    if-eqz v2, :cond_6

    :cond_5
    move v0, v1

    :cond_6
    iput-boolean v0, p1, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    goto :goto_0

    .line 234
    :cond_7
    iget v2, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    .line 235
    iget-object v2, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget-boolean v2, v2, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    if-eqz v2, :cond_8

    iget-object v2, p1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iget-boolean v2, v2, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    if-eqz v2, :cond_8

    :goto_1
    iput-boolean v1, p1, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    goto :goto_0

    :cond_8
    move v1, v0

    goto :goto_1

    .line 237
    :cond_9
    iget v2, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v3, 0xa

    if-eq v2, v3, :cond_a

    iget v2, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/16 v3, 0xc

    if-ne v2, v3, :cond_b

    .line 238
    :cond_a
    iput-boolean v1, p1, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    goto :goto_0

    .line 241
    :cond_b
    iput-boolean v0, p1, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    goto :goto_0
.end method

.method exportTable()[S
    .locals 12

    .prologue
    .line 900
    iget-object v10, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v10, v10, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v11, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v10, v10, v11

    if-nez v10, :cond_1

    .line 901
    const/4 v10, 0x0

    new-array v8, v10, [S

    .line 953
    :cond_0
    return-object v8

    .line 904
    :cond_1
    iget-object v10, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v10, v10, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    invoke-virtual {v10}, Lcom/ibm/icu/text/RBBISetBuilder;->getNumCharCategories()I

    move-result v10

    const/16 v11, 0x7fff

    if-ge v10, v11, :cond_4

    iget-object v10, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    const/16 v11, 0x7fff

    if-ge v10, v11, :cond_4

    const/4 v10, 0x1

    :goto_0
    invoke-static {v10}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 907
    iget-object v10, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v3

    .line 911
    .local v3, "numStates":I
    iget-object v10, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v10, v10, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    invoke-virtual {v10}, Lcom/ibm/icu/text/RBBISetBuilder;->getNumCharCategories()I

    move-result v10

    add-int/lit8 v5, v10, 0x4

    .line 912
    .local v5, "rowLen":I
    invoke-virtual {p0}, Lcom/ibm/icu/text/RBBITableBuilder;->getTableSize()I

    move-result v10

    div-int/lit8 v9, v10, 0x2

    .line 915
    .local v9, "tableSize":I
    new-array v8, v9, [S

    .line 922
    .local v8, "table":[S
    const/4 v10, 0x0

    ushr-int/lit8 v11, v3, 0x10

    int-to-short v11, v11

    aput-short v11, v8, v10

    .line 923
    const/4 v10, 0x1

    const v11, 0xffff

    and-int/2addr v11, v3

    int-to-short v11, v11

    aput-short v11, v8, v10

    .line 926
    const/4 v10, 0x2

    ushr-int/lit8 v11, v5, 0x10

    int-to-short v11, v11

    aput-short v11, v8, v10

    .line 927
    const/4 v10, 0x3

    const v11, 0xffff

    and-int/2addr v11, v5

    int-to-short v11, v11

    aput-short v11, v8, v10

    .line 930
    const/4 v1, 0x0

    .line 931
    .local v1, "flags":I
    iget-object v10, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-boolean v10, v10, Lcom/ibm/icu/text/RBBIRuleBuilder;->fLookAheadHardBreak:Z

    if-eqz v10, :cond_2

    .line 932
    or-int/lit8 v1, v1, 0x1

    .line 934
    :cond_2
    iget-object v10, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v10, v10, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    invoke-virtual {v10}, Lcom/ibm/icu/text/RBBISetBuilder;->sawBOF()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 935
    or-int/lit8 v1, v1, 0x2

    .line 937
    :cond_3
    const/4 v10, 0x4

    ushr-int/lit8 v11, v1, 0x10

    int-to-short v11, v11

    aput-short v11, v8, v10

    .line 938
    const/4 v10, 0x5

    const v11, 0xffff

    and-int/2addr v11, v1

    int-to-short v11, v11

    aput-short v11, v8, v10

    .line 940
    iget-object v10, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v10, v10, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    invoke-virtual {v10}, Lcom/ibm/icu/text/RBBISetBuilder;->getNumCharCategories()I

    move-result v2

    .line 941
    .local v2, "numCharCategories":I
    const/4 v7, 0x0

    .local v7, "state":I
    :goto_1
    if-ge v7, v3, :cond_0

    .line 942
    iget-object v10, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    invoke-interface {v10, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;

    .line 943
    .local v6, "sd":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    mul-int v10, v7, v5

    add-int/lit8 v4, v10, 0x8

    .line 944
    .local v4, "row":I
    const/16 v10, -0x8000

    iget v11, v6, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fAccepting:I

    if-ge v10, v11, :cond_5

    iget v10, v6, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fAccepting:I

    const/16 v11, 0x7fff

    if-gt v10, v11, :cond_5

    const/4 v10, 0x1

    :goto_2
    invoke-static {v10}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 945
    const/16 v10, -0x8000

    iget v11, v6, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fLookAhead:I

    if-ge v10, v11, :cond_6

    iget v10, v6, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fLookAhead:I

    const/16 v11, 0x7fff

    if-gt v10, v11, :cond_6

    const/4 v10, 0x1

    :goto_3
    invoke-static {v10}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 946
    add-int/lit8 v10, v4, 0x0

    iget v11, v6, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fAccepting:I

    int-to-short v11, v11

    aput-short v11, v8, v10

    .line 947
    add-int/lit8 v10, v4, 0x1

    iget v11, v6, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fLookAhead:I

    int-to-short v11, v11

    aput-short v11, v8, v10

    .line 948
    add-int/lit8 v10, v4, 0x2

    iget v11, v6, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fTagsIdx:I

    int-to-short v11, v11

    aput-short v11, v8, v10

    .line 949
    const/4 v0, 0x0

    .local v0, "col":I
    :goto_4
    if-ge v0, v2, :cond_7

    .line 950
    add-int/lit8 v10, v4, 0x4

    add-int/2addr v10, v0

    iget-object v11, v6, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fDtran:[I

    aget v11, v11, v0

    int-to-short v11, v11

    aput-short v11, v8, v10

    .line 949
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 904
    .end local v0    # "col":I
    .end local v1    # "flags":I
    .end local v2    # "numCharCategories":I
    .end local v3    # "numStates":I
    .end local v4    # "row":I
    .end local v5    # "rowLen":I
    .end local v6    # "sd":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    .end local v7    # "state":I
    .end local v8    # "table":[S
    .end local v9    # "tableSize":I
    :cond_4
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 944
    .restart local v1    # "flags":I
    .restart local v2    # "numCharCategories":I
    .restart local v3    # "numStates":I
    .restart local v4    # "row":I
    .restart local v5    # "rowLen":I
    .restart local v6    # "sd":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    .restart local v7    # "state":I
    .restart local v8    # "table":[S
    .restart local v9    # "tableSize":I
    :cond_5
    const/4 v10, 0x0

    goto :goto_2

    .line 945
    :cond_6
    const/4 v10, 0x0

    goto :goto_3

    .line 941
    .restart local v0    # "col":I
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method

.method flagAcceptingStates()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 627
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 632
    .local v1, "endMarkerNodes":Ljava/util/List;
    iget-object v5, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v6, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v5, v5, v6

    const/4 v6, 0x6

    invoke-virtual {v5, v1, v6}, Lcom/ibm/icu/text/RBBINode;->findNodes(Ljava/util/List;I)V

    .line 634
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_4

    .line 635
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/RBBINode;

    .line 636
    .local v0, "endMarker":Lcom/ibm/icu/text/RBBINode;
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_1
    iget-object v5, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_3

    .line 637
    iget-object v5, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;

    .line 639
    .local v4, "sd":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    iget-object v5, v4, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fPositions:Ljava/util/Set;

    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 644
    iget v5, v4, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fAccepting:I

    if-nez v5, :cond_0

    .line 646
    iget v5, v0, Lcom/ibm/icu/text/RBBINode;->fVal:I

    iput v5, v4, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fAccepting:I

    .line 647
    iget v5, v4, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fAccepting:I

    if-nez v5, :cond_0

    .line 648
    iput v7, v4, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fAccepting:I

    .line 651
    :cond_0
    iget v5, v4, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fAccepting:I

    if-ne v5, v7, :cond_1

    iget v5, v0, Lcom/ibm/icu/text/RBBINode;->fVal:I

    if-eqz v5, :cond_1

    .line 655
    iget v5, v0, Lcom/ibm/icu/text/RBBINode;->fVal:I

    iput v5, v4, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fAccepting:I

    .line 662
    :cond_1
    iget-boolean v5, v0, Lcom/ibm/icu/text/RBBINode;->fLookAheadEnd:Z

    if-eqz v5, :cond_2

    .line 666
    iget v5, v4, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fAccepting:I

    iput v5, v4, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fLookAhead:I

    .line 636
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 634
    .end local v4    # "sd":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 671
    .end local v0    # "endMarker":Lcom/ibm/icu/text/RBBINode;
    .end local v3    # "n":I
    :cond_4
    return-void
.end method

.method flagLookAheadStates()V
    .locals 7

    .prologue
    .line 680
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 685
    .local v2, "lookAheadNodes":Ljava/util/List;
    iget-object v5, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v6, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v5, v5, v6

    const/4 v6, 0x4

    invoke-virtual {v5, v2, v6}, Lcom/ibm/icu/text/RBBINode;->findNodes(Ljava/util/List;I)V

    .line 686
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 687
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/RBBINode;

    .line 689
    .local v1, "lookAheadNode":Lcom/ibm/icu/text/RBBINode;
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_1
    iget-object v5, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_1

    .line 690
    iget-object v5, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;

    .line 691
    .local v4, "sd":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    iget-object v5, v4, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fPositions:Ljava/util/Set;

    invoke-interface {v5, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 692
    iget v5, v1, Lcom/ibm/icu/text/RBBINode;->fVal:I

    iput v5, v4, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fLookAhead:I

    .line 689
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 686
    .end local v4    # "sd":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 696
    .end local v1    # "lookAheadNode":Lcom/ibm/icu/text/RBBINode;
    .end local v3    # "n":I
    :cond_2
    return-void
.end method

.method flagTaggedStates()V
    .locals 8

    .prologue
    .line 707
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 712
    .local v4, "tagNodes":Ljava/util/List;
    iget-object v5, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v6, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v5, v5, v6

    const/4 v6, 0x5

    invoke-virtual {v5, v4, v6}, Lcom/ibm/icu/text/RBBINode;->findNodes(Ljava/util/List;I)V

    .line 713
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 714
    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/text/RBBINode;

    .line 716
    .local v3, "tagNode":Lcom/ibm/icu/text/RBBINode;
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_1
    iget-object v5, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 717
    iget-object v5, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;

    .line 718
    .local v2, "sd":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    iget-object v5, v2, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fPositions:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 719
    iget-object v5, v2, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fTagVals:Ljava/util/SortedSet;

    new-instance v6, Ljava/lang/Integer;

    iget v7, v3, Lcom/ibm/icu/text/RBBINode;->fVal:I

    invoke-direct {v6, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v5, v6}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    .line 716
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 713
    .end local v2    # "sd":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 723
    .end local v1    # "n":I
    .end local v3    # "tagNode":Lcom/ibm/icu/text/RBBINode;
    :cond_2
    return-void
.end method

.method getTableSize()I
    .locals 6

    .prologue
    .line 853
    const/4 v3, 0x0

    .line 858
    .local v3, "size":I
    iget-object v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v4, v4, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    iget v5, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRootIx:I

    aget-object v4, v4, v5

    if-nez v4, :cond_0

    .line 859
    const/4 v4, 0x0

    .line 877
    :goto_0
    return v4

    .line 862
    :cond_0
    const/16 v3, 0x10

    .line 864
    iget-object v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    .line 865
    .local v1, "numRows":I
    iget-object v4, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v4, v4, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    invoke-virtual {v4}, Lcom/ibm/icu/text/RBBISetBuilder;->getNumCharCategories()I

    move-result v0

    .line 871
    .local v0, "numCols":I
    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v2, v4, 0x8

    .line 872
    .local v2, "rowSize":I
    mul-int v4, v1, v2

    add-int/2addr v3, v4

    .line 873
    :goto_1
    rem-int/lit8 v4, v3, 0x8

    if-lez v4, :cond_1

    .line 874
    add-int/lit8 v3, v3, 0x1

    .line 875
    goto :goto_1

    :cond_1
    move v4, v3

    .line 877
    goto :goto_0
.end method

.method mergeRuleStatusVals()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 766
    iget-object v8, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v8, v8, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRuleStatusVals:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_0

    .line 767
    iget-object v8, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v8, v8, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRuleStatusVals:Ljava/util/List;

    new-instance v9, Ljava/lang/Integer;

    const/4 v10, 0x1

    invoke-direct {v9, v10}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 768
    iget-object v8, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v8, v8, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRuleStatusVals:Ljava/util/List;

    new-instance v9, Ljava/lang/Integer;

    invoke-direct {v9, v11}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 770
    new-instance v4, Ljava/util/TreeSet;

    invoke-direct {v4}, Ljava/util/TreeSet;-><init>()V

    .line 771
    .local v4, "s0":Ljava/util/SortedSet;
    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v11}, Ljava/lang/Integer;-><init>(I)V

    .line 772
    .local v2, "izero":Ljava/lang/Integer;
    iget-object v8, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v8, v8, Lcom/ibm/icu/text/RBBIRuleBuilder;->fStatusSets:Ljava/util/Map;

    invoke-interface {v8, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 773
    new-instance v5, Ljava/util/TreeSet;

    invoke-direct {v5}, Ljava/util/TreeSet;-><init>()V

    .line 774
    .local v5, "s1":Ljava/util/SortedSet;
    invoke-interface {v5, v2}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    .line 775
    iget-object v8, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v8, v8, Lcom/ibm/icu/text/RBBIRuleBuilder;->fStatusSets:Ljava/util/Map;

    invoke-interface {v8, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 780
    .end local v2    # "izero":Ljava/lang/Integer;
    .end local v4    # "s0":Ljava/util/SortedSet;
    .end local v5    # "s1":Ljava/util/SortedSet;
    :cond_0
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    iget-object v8, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v3, v8, :cond_2

    .line 781
    iget-object v8, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;

    .line 782
    .local v6, "sd":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    iget-object v7, v6, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fTagVals:Ljava/util/SortedSet;

    .line 783
    .local v7, "statusVals":Ljava/util/Set;
    iget-object v8, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v8, v8, Lcom/ibm/icu/text/RBBIRuleBuilder;->fStatusSets:Ljava/util/Map;

    invoke-interface {v8, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 784
    .local v0, "arrayIndexI":Ljava/lang/Integer;
    if-nez v0, :cond_1

    .line 789
    new-instance v0, Ljava/lang/Integer;

    .end local v0    # "arrayIndexI":Ljava/lang/Integer;
    iget-object v8, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v8, v8, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRuleStatusVals:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-direct {v0, v8}, Ljava/lang/Integer;-><init>(I)V

    .line 790
    .restart local v0    # "arrayIndexI":Ljava/lang/Integer;
    iget-object v8, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v8, v8, Lcom/ibm/icu/text/RBBIRuleBuilder;->fStatusSets:Ljava/util/Map;

    invoke-interface {v8, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 794
    iget-object v8, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v8, v8, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRuleStatusVals:Ljava/util/List;

    new-instance v9, Ljava/lang/Integer;

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v10

    invoke-direct {v9, v10}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 795
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 796
    .local v1, "it":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 797
    iget-object v8, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v8, v8, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRuleStatusVals:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 803
    .end local v1    # "it":Ljava/util/Iterator;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iput v8, v6, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fTagsIdx:I

    .line 780
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 805
    .end local v0    # "arrayIndexI":Ljava/lang/Integer;
    .end local v6    # "sd":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    .end local v7    # "statusVals":Ljava/util/Set;
    :cond_2
    return-void
.end method

.method printPosSets(Lcom/ibm/icu/text/RBBINode;)V
    .locals 3
    .param p1, "n"    # Lcom/ibm/icu/text/RBBINode;

    .prologue
    .line 821
    if-nez p1, :cond_0

    .line 838
    :goto_0
    return-void

    .line 824
    :cond_0
    invoke-static {p1}, Lcom/ibm/icu/text/RBBINode;->printNode(Lcom/ibm/icu/text/RBBINode;)V

    .line 825
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "         Nullable:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v2, p1, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 827
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "         firstpos:  "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 828
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RBBITableBuilder;->printSet(Ljava/util/Collection;)V

    .line 830
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "         lastpos:   "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 831
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RBBITableBuilder;->printSet(Ljava/util/Collection;)V

    .line 833
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "         followpos: "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 834
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fFollowPos:Ljava/util/Set;

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RBBITableBuilder;->printSet(Ljava/util/Collection;)V

    .line 836
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RBBITableBuilder;->printPosSets(Lcom/ibm/icu/text/RBBINode;)V

    .line 837
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RBBITableBuilder;->printPosSets(Lcom/ibm/icu/text/RBBINode;)V

    goto :goto_0
.end method

.method printRuleStatusTable()V
    .locals 8

    .prologue
    const/4 v7, 0x7

    .line 1024
    const/4 v3, 0x0

    .line 1025
    .local v3, "thisRecord":I
    const/4 v1, 0x0

    .line 1027
    .local v1, "nextRecord":I
    iget-object v5, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v2, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRuleStatusVals:Ljava/util/List;

    .line 1029
    .local v2, "tbl":Ljava/util/List;
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "index |  tags \n"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1030
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "-------------------\n"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1032
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 1033
    move v3, v1

    .line 1034
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/2addr v5, v3

    add-int/lit8 v1, v5, 0x1

    .line 1035
    invoke-static {v3, v7}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 1036
    add-int/lit8 v0, v3, 0x1

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 1037
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1038
    .local v4, "val":I
    invoke-static {v4, v7}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 1036
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1040
    .end local v4    # "val":I
    :cond_0
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "\n"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_0

    .line 1042
    .end local v0    # "i":I
    :cond_1
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1043
    return-void
.end method

.method printSet(Ljava/util/Collection;)V
    .locals 4
    .param p1, "s"    # Ljava/util/Collection;

    .prologue
    .line 965
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 966
    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 967
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/RBBINode;

    .line 968
    .local v1, "n":Lcom/ibm/icu/text/RBBINode;
    iget v2, v1, Lcom/ibm/icu/text/RBBINode;->fSerialNum:I

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    goto :goto_0

    .line 970
    .end local v1    # "n":Lcom/ibm/icu/text/RBBINode;
    :cond_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2}, Ljava/io/PrintStream;->println()V

    .line 971
    return-void
.end method

.method printStates()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 985
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "state |           i n p u t     s y m b o l s \n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 986
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "      | Acc  LA    Tag"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 987
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_0
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    invoke-virtual {v3}, Lcom/ibm/icu/text/RBBISetBuilder;->getNumCharCategories()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 988
    invoke-static {v0, v5}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 987
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 990
    :cond_0
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 991
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "      |---------------"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 992
    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    invoke-virtual {v3}, Lcom/ibm/icu/text/RBBISetBuilder;->getNumCharCategories()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 993
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "---"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 992
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 995
    :cond_1
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 997
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_2
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 998
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fDStates:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;

    .line 999
    .local v2, "sd":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    const/4 v3, 0x5

    invoke-static {v1, v3}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 1000
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, " | "

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1002
    iget v3, v2, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fAccepting:I

    invoke-static {v3, v5}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 1003
    iget v3, v2, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fLookAhead:I

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 1004
    iget v3, v2, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fTagsIdx:I

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 1005
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1006
    const/4 v0, 0x0

    :goto_3
    iget-object v3, p0, Lcom/ibm/icu/text/RBBITableBuilder;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    invoke-virtual {v3}, Lcom/ibm/icu/text/RBBISetBuilder;->getNumCharCategories()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 1007
    iget-object v3, v2, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fDtran:[I

    aget v3, v3, v0

    invoke-static {v3, v5}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 1006
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1009
    :cond_2
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 997
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1011
    .end local v2    # "sd":Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
    :cond_3
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v4, "\n\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1012
    return-void
.end method
