.class final Lcom/ibm/icu/text/CollationRuleParser;
.super Ljava/lang/Object;
.source "CollationRuleParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/CollationRuleParser$TokenOption;,
        Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;,
        Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;,
        Lcom/ibm/icu/text/CollationRuleParser$Token;,
        Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;,
        Lcom/ibm/icu/text/CollationRuleParser$OptionSet;
    }
.end annotation


# static fields
.field private static final INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

.field private static final RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

.field private static final TOKEN_BEFORE_:I = 0x3

.field private static final TOKEN_POLARITY_POSITIVE_:I = 0x1

.field static final TOKEN_RESET_:I = -0x21524111

.field private static final TOKEN_SUCCESS_MASK_:I = 0x10

.field private static final TOKEN_TOP_MASK_:I = 0x4

.field private static final TOKEN_UNSET_:I = -0x1

.field private static final TOKEN_VARIABLE_TOP_MASK_:I = 0x8


# instance fields
.field private m_UCAColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

.field m_copySet_:Lcom/ibm/icu/text/UnicodeSet;

.field private m_current_:I

.field private m_extraCurrent_:I

.field m_hashTable_:Ljava/util/Hashtable;

.field m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

.field private m_optionEnd_:I

.field private m_optionarg_:I

.field m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

.field private m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

.field m_removeSet_:Lcom/ibm/icu/text/UnicodeSet;

.field m_resultLength_:I

.field private m_rules_:Ljava/lang/String;

.field m_source_:Ljava/lang/StringBuffer;

.field private m_utilCEBuffer_:[I

.field private m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

.field m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x7

    const/4 v14, 0x2

    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 483
    const/16 v6, 0xf

    new-array v6, v6, [Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sput-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    .line 485
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    new-instance v7, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v8, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v8, v8, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_NON_VARIABLE_:[I

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v9, v9, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_IMPLICIT_:[I

    invoke-direct {v7, v8, v9}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v7, v6, v12

    .line 489
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    new-instance v7, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v8, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v8, v8, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_PRIMARY_IGNORABLE_:[I

    invoke-direct {v7, v8, v11}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v7, v6, v13

    .line 493
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    new-instance v7, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v8, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v8, v8, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_PRIMARY_IGNORABLE_:[I

    invoke-direct {v7, v8, v11}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v7, v6, v14

    .line 498
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    const/4 v7, 0x3

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v9, v9, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_SECONDARY_IGNORABLE_:[I

    invoke-direct {v8, v9, v11}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v8, v6, v7

    .line 502
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    const/4 v7, 0x4

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v9, v9, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_SECONDARY_IGNORABLE_:[I

    invoke-direct {v8, v9, v11}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v8, v6, v7

    .line 506
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    const/4 v7, 0x5

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v9, v9, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_TERTIARY_IGNORABLE_:[I

    invoke-direct {v8, v9, v11}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v8, v6, v7

    .line 510
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    const/4 v7, 0x6

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v9, v9, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_TERTIARY_IGNORABLE_:[I

    invoke-direct {v8, v9, v11}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v8, v6, v7

    .line 514
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    new-instance v7, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v8, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v8, v8, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_VARIABLE_:[I

    invoke-direct {v7, v8, v11}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v7, v6, v15

    .line 518
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    const/16 v7, 0x8

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v9, v9, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_VARIABLE_:[I

    invoke-direct {v8, v9, v11}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v8, v6, v7

    .line 522
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    const/16 v7, 0x9

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v9, v9, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_NON_VARIABLE_:[I

    invoke-direct {v8, v9, v11}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v8, v6, v7

    .line 526
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    const/16 v7, 0xa

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v9, v9, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_NON_VARIABLE_:[I

    sget-object v10, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v10, v10, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_IMPLICIT_:[I

    invoke-direct {v8, v9, v10}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v8, v6, v7

    .line 530
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    const/16 v7, 0xb

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v9, v9, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_IMPLICIT_:[I

    invoke-direct {v8, v9, v11}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v8, v6, v7

    .line 534
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    const/16 v7, 0xc

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v9, v9, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_IMPLICIT_:[I

    sget-object v10, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v10, v10, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_TRAILING_:[I

    invoke-direct {v8, v9, v10}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v8, v6, v7

    .line 538
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    const/16 v7, 0xd

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v9, v9, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_TRAILING_:[I

    invoke-direct {v8, v9, v11}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v8, v6, v7

    .line 542
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    const/16 v7, 0xe

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget-object v9, v9, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_TRAILING_:[I

    invoke-direct {v8, v9, v11}, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;-><init>([I[I)V

    aput-object v8, v6, v7

    .line 545
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    const/16 v7, 0xe

    aget-object v6, v6, v7

    sget-object v7, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget v7, v7, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->PRIMARY_SPECIAL_MIN_:I

    shl-int/lit8 v7, v7, 0x18

    iput v7, v6, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_limitCE_:I

    .line 548
    const/16 v6, 0x13

    new-array v6, v6, [Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    sput-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    .line 549
    new-array v4, v14, [Ljava/lang/String;

    const-string/jumbo v6, "non-ignorable"

    aput-object v6, v4, v12

    const-string/jumbo v6, "shifted"

    aput-object v6, v4, v13

    .line 550
    .local v4, "option":[Ljava/lang/String;
    new-array v5, v14, [I

    fill-array-data v5, :array_0

    .line 552
    .local v5, "value":[I
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    new-instance v7, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v8, "alternate"

    invoke-direct {v7, v8, v13, v4, v5}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v7, v6, v12

    .line 555
    new-array v4, v13, [Ljava/lang/String;

    .line 556
    const-string/jumbo v6, "2"

    aput-object v6, v4, v12

    .line 557
    new-array v5, v13, [I

    .line 558
    const/16 v6, 0x11

    aput v6, v5, v12

    .line 559
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    new-instance v7, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v8, "backwards"

    invoke-direct {v7, v8, v12, v4, v5}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v7, v6, v13

    .line 562
    new-array v2, v14, [Ljava/lang/String;

    .line 563
    .local v2, "offonoption":[Ljava/lang/String;
    const-string/jumbo v6, "off"

    aput-object v6, v2, v12

    .line 564
    const-string/jumbo v6, "on"

    aput-object v6, v2, v13

    .line 565
    new-array v3, v14, [I

    .line 566
    .local v3, "offonvalue":[I
    const/16 v6, 0x10

    aput v6, v3, v12

    .line 567
    const/16 v6, 0x11

    aput v6, v3, v13

    .line 568
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    new-instance v7, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v8, "caseLevel"

    const/4 v9, 0x3

    invoke-direct {v7, v8, v9, v2, v3}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v7, v6, v14

    .line 571
    const/4 v6, 0x3

    new-array v4, v6, [Ljava/lang/String;

    .line 572
    const-string/jumbo v6, "lower"

    aput-object v6, v4, v12

    .line 573
    const-string/jumbo v6, "upper"

    aput-object v6, v4, v13

    .line 574
    const-string/jumbo v6, "off"

    aput-object v6, v4, v14

    .line 575
    const/4 v6, 0x3

    new-array v5, v6, [I

    .line 576
    const/16 v6, 0x18

    aput v6, v5, v12

    .line 577
    const/16 v6, 0x19

    aput v6, v5, v13

    .line 578
    const/16 v6, 0x10

    aput v6, v5, v14

    .line 579
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/4 v7, 0x3

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "caseFirst"

    invoke-direct {v8, v9, v14, v4, v5}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 582
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/4 v7, 0x4

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "normalization"

    const/4 v10, 0x4

    invoke-direct {v8, v9, v10, v2, v3}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 585
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/4 v7, 0x5

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "hiraganaQ"

    const/4 v10, 0x6

    invoke-direct {v8, v9, v10, v2, v3}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 588
    const/4 v6, 0x5

    new-array v4, v6, [Ljava/lang/String;

    .line 589
    const-string/jumbo v6, "1"

    aput-object v6, v4, v12

    .line 590
    const-string/jumbo v6, "2"

    aput-object v6, v4, v13

    .line 591
    const-string/jumbo v6, "3"

    aput-object v6, v4, v14

    .line 592
    const/4 v6, 0x3

    const-string/jumbo v7, "4"

    aput-object v7, v4, v6

    .line 593
    const/4 v6, 0x4

    const-string/jumbo v7, "I"

    aput-object v7, v4, v6

    .line 594
    const/4 v6, 0x5

    new-array v5, v6, [I

    .line 595
    aput v12, v5, v12

    .line 596
    aput v13, v5, v13

    .line 597
    aput v14, v5, v14

    .line 598
    const/4 v6, 0x3

    const/4 v7, 0x3

    aput v7, v5, v6

    .line 599
    const/4 v6, 0x4

    const/16 v7, 0xf

    aput v7, v5, v6

    .line 600
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/4 v7, 0x6

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "strength"

    const/4 v10, 0x5

    invoke-direct {v8, v9, v10, v4, v5}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 603
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    new-instance v7, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v8, "variable top"

    invoke-direct {v7, v8, v15, v11, v11}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v7, v6, v15

    .line 606
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/16 v7, 0x8

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "rearrange"

    invoke-direct {v8, v9, v15, v11, v11}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 609
    const/4 v6, 0x3

    new-array v4, v6, [Ljava/lang/String;

    .line 610
    const-string/jumbo v6, "1"

    aput-object v6, v4, v12

    .line 611
    const-string/jumbo v6, "2"

    aput-object v6, v4, v13

    .line 612
    const-string/jumbo v6, "3"

    aput-object v6, v4, v14

    .line 613
    const/4 v6, 0x3

    new-array v5, v6, [I

    .line 614
    aput v12, v5, v12

    .line 615
    aput v13, v5, v13

    .line 616
    aput v14, v5, v14

    .line 617
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/16 v7, 0x9

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "before"

    invoke-direct {v8, v9, v15, v4, v5}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 620
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/16 v7, 0xa

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "top"

    invoke-direct {v8, v9, v15, v11, v11}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 623
    new-array v0, v15, [Ljava/lang/String;

    .line 624
    .local v0, "firstlastoption":[Ljava/lang/String;
    const-string/jumbo v6, "primary"

    aput-object v6, v0, v12

    .line 625
    const-string/jumbo v6, "secondary"

    aput-object v6, v0, v13

    .line 626
    const-string/jumbo v6, "tertiary"

    aput-object v6, v0, v14

    .line 627
    const/4 v6, 0x3

    const-string/jumbo v7, "variable"

    aput-object v7, v0, v6

    .line 628
    const/4 v6, 0x4

    const-string/jumbo v7, "regular"

    aput-object v7, v0, v6

    .line 629
    const/4 v6, 0x5

    const-string/jumbo v7, "implicit"

    aput-object v7, v0, v6

    .line 630
    const/4 v6, 0x6

    const-string/jumbo v7, "trailing"

    aput-object v7, v0, v6

    .line 632
    new-array v1, v15, [I

    .line 633
    .local v1, "firstlastvalue":[I
    invoke-static {v1, v12}, Ljava/util/Arrays;->fill([II)V

    .line 635
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/16 v7, 0xb

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "first"

    invoke-direct {v8, v9, v15, v0, v1}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 638
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/16 v7, 0xc

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "last"

    invoke-direct {v8, v9, v15, v0, v1}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 641
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/16 v7, 0xd

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "optimize"

    invoke-direct {v8, v9, v15, v11, v11}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 644
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/16 v7, 0xe

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "suppressContractions"

    invoke-direct {v8, v9, v15, v11, v11}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 647
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/16 v7, 0xf

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "undefined"

    invoke-direct {v8, v9, v15, v11, v11}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 650
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/16 v7, 0x10

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "scriptOrder"

    invoke-direct {v8, v9, v15, v11, v11}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 653
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/16 v7, 0x11

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "charsetname"

    invoke-direct {v8, v9, v15, v11, v11}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 656
    sget-object v6, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const/16 v7, 0x12

    new-instance v8, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    const-string/jumbo v9, "charset"

    invoke-direct {v8, v9, v15, v11, v11}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;-><init>(Ljava/lang/String;I[Ljava/lang/String;[I)V

    aput-object v8, v6, v7

    .line 659
    return-void

    .line 550
    nop

    :array_0
    .array-data 4
        0x15
        0x14
    .end array-data
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "rules"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 664
    new-instance v0, Lcom/ibm/icu/text/CollationRuleParser$Token;

    invoke-direct {v0}, Lcom/ibm/icu/text/CollationRuleParser$Token;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 665
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->getCollationElementIterator(Ljava/lang/String;)Lcom/ibm/icu/text/CollationElementIterator;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_UCAColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    .line 667
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    .line 1834
    iput v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_optionarg_:I

    .line 38
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/CollationRuleParser;->extractSetsFromRules(Ljava/lang/String;)V

    .line 39
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-static {p1, v2}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    .line 40
    iget-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    .line 41
    iput v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    .line 42
    iget-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 44
    new-instance v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    invoke-direct {v0}, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    .line 45
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_hashTable_:Ljava/util/Hashtable;

    .line 46
    new-instance v0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    sget-object v1, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;-><init>(Lcom/ibm/icu/text/RuleBasedCollator;)V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    .line 47
    const/16 v0, 0x200

    new-array v0, v0, [Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    .line 48
    iput v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    .line 52
    return-void
.end method

.method private doEndParseNextToken(IZIIZI)I
    .locals 3
    .param p1, "newstrength"    # I
    .param p2, "top"    # Z
    .param p3, "extensionoffset"    # I
    .param p4, "newextensionlen"    # I
    .param p5, "variabletop"    # Z
    .param p6, "before"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 1525
    if-ne p1, v1, :cond_0

    move v0, v1

    .line 1540
    :goto_0
    return v0

    .line 1528
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v1, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    if-nez v1, :cond_1

    if-nez p2, :cond_1

    .line 1529
    iget-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    iget v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-static {v1, v2}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    .line 1532
    :cond_1
    iget-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iput p1, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_strength_:I

    .line 1535
    iget-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iput p3, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_extensionOffset_:I

    .line 1536
    iget-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iput p4, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_extensionLen_:I

    .line 1537
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    if-eqz p5, :cond_3

    const/16 v1, 0x8

    :goto_1
    if-eqz p2, :cond_2

    const/4 v0, 0x4

    :cond_2
    or-int/2addr v0, v1

    or-int/2addr v0, p6

    int-to-char v0, v0

    iput-char v0, v2, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_flags_:C

    .line 1540
    iget v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    goto :goto_0

    :cond_3
    move v1, v0

    .line 1537
    goto :goto_1
.end method

.method private final doSetTop()Z
    .locals 5

    .prologue
    const v4, 0xffff

    .line 1103
    iget-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    iput v2, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    .line 1104
    iget-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    const v2, 0xfffe

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1105
    sget-object v1, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget-char v2, v2, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_indirectIndex_:C

    aget-object v0, v1, v2

    .line 1107
    .local v0, "ib":Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;
    iget-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    iget v2, v0, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_startCE_:I

    shr-int/lit8 v2, v2, 0x10

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1108
    iget-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    iget v2, v0, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_startCE_:I

    and-int/2addr v2, v4

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1109
    iget v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    add-int/lit8 v1, v1, 0x3

    iput v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    .line 1110
    sget-object v1, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget-char v2, v2, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_indirectIndex_:C

    aget-object v1, v1, v2

    iget v1, v1, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_startContCE_:I

    if-nez v1, :cond_0

    .line 1112
    iget-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    const/4 v2, 0x3

    iput v2, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    .line 1124
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 1115
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    sget-object v2, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    iget-object v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget-char v3, v3, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_indirectIndex_:C

    aget-object v2, v2, v3

    iget v2, v2, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_startContCE_:I

    shr-int/lit8 v2, v2, 0x10

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1118
    iget-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    sget-object v2, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    iget-object v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget-char v3, v3, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_indirectIndex_:C

    aget-object v2, v2, v3

    iget v2, v2, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_startContCE_:I

    and-int/2addr v2, v4

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1121
    iget v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    .line 1122
    iget-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    const/4 v2, 0x5

    iput v2, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    goto :goto_0
.end method

.method private final extractSetsFromRules(Ljava/lang/String;)V
    .locals 6
    .param p1, "rules"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 2040
    const/4 v2, -0x1

    .line 2041
    .local v2, "optionNumber":I
    const/4 v3, 0x0

    .line 2042
    .local v3, "setStart":I
    const/4 v0, 0x0

    .line 2043
    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 2044
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x5b

    if-ne v4, v5, :cond_0

    .line 2045
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-direct {p0, p1, v4, v5}, Lcom/ibm/icu/text/CollationRuleParser;->readOption(Ljava/lang/String;II)I

    move-result v2

    .line 2046
    iget v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_optionarg_:I

    .line 2047
    const/16 v4, 0xd

    if-ne v2, v4, :cond_2

    .line 2048
    invoke-direct {p0, p1, v3}, Lcom/ibm/icu/text/CollationRuleParser;->readAndSetUnicodeSet(Ljava/lang/String;I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v1

    .line 2049
    .local v1, "newSet":Lcom/ibm/icu/text/UnicodeSet;
    iget-object v4, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_copySet_:Lcom/ibm/icu/text/UnicodeSet;

    if-nez v4, :cond_1

    .line 2050
    iput-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_copySet_:Lcom/ibm/icu/text/UnicodeSet;

    .line 2063
    .end local v1    # "newSet":Lcom/ibm/icu/text/UnicodeSet;
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    .line 2064
    goto :goto_0

    .line 2052
    .restart local v1    # "newSet":Lcom/ibm/icu/text/UnicodeSet;
    :cond_1
    iget-object v4, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_copySet_:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v4, v1}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_1

    .line 2054
    .end local v1    # "newSet":Lcom/ibm/icu/text/UnicodeSet;
    :cond_2
    const/16 v4, 0xe

    if-ne v2, v4, :cond_0

    .line 2055
    invoke-direct {p0, p1, v3}, Lcom/ibm/icu/text/CollationRuleParser;->readAndSetUnicodeSet(Ljava/lang/String;I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v1

    .line 2056
    .restart local v1    # "newSet":Lcom/ibm/icu/text/UnicodeSet;
    iget-object v4, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_removeSet_:Lcom/ibm/icu/text/UnicodeSet;

    if-nez v4, :cond_3

    .line 2057
    iput-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_removeSet_:Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_1

    .line 2059
    :cond_3
    iget-object v4, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_removeSet_:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v4, v1}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_1

    .line 2065
    .end local v1    # "newSet":Lcom/ibm/icu/text/UnicodeSet;
    :cond_4
    return-void
.end method

.method private getVirginBefore(Lcom/ibm/icu/text/CollationRuleParser$Token;I)Lcom/ibm/icu/text/CollationRuleParser$Token;
    .locals 11
    .param p1, "sourcetoken"    # Lcom/ibm/icu/text/CollationRuleParser$Token;
    .param p2, "strength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 1554
    if-eqz p1, :cond_2

    .line 1555
    iget v7, p1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    const v8, 0xffffff

    and-int v3, v7, v8

    .line 1556
    .local v3, "offset":I
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_UCAColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    add-int/lit8 v9, v3, 0x1

    invoke-virtual {v8, v3, v9}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 1564
    .end local v3    # "offset":I
    :goto_0
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_UCAColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v7}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v7

    and-int/lit16 v0, v7, -0xc1

    .line 1565
    .local v0, "basece":I
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_UCAColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v7}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v1

    .line 1566
    .local v1, "basecontce":I
    const/4 v7, -0x1

    if-ne v1, v7, :cond_0

    .line 1567
    const/4 v1, 0x0

    .line 1570
    :cond_0
    const/4 v2, 0x0

    .line 1573
    .local v2, "ch":I
    ushr-int/lit8 v7, v0, 0x18

    sget-object v8, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget v8, v8, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->PRIMARY_IMPLICIT_MIN_:I

    if-lt v7, v8, :cond_4

    ushr-int/lit8 v7, v0, 0x18

    sget-object v8, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget v8, v8, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->PRIMARY_IMPLICIT_MAX_:I

    if-gt v7, v8, :cond_4

    .line 1576
    const/high16 v7, -0x10000

    and-int/2addr v7, v0

    const/high16 v8, -0x10000

    and-int/2addr v8, v1

    shr-int/lit8 v8, v8, 0x10

    or-int v4, v7, v8

    .line 1577
    .local v4, "primary":I
    sget-object v7, Lcom/ibm/icu/text/RuleBasedCollator;->impCEGen_:Lcom/ibm/icu/impl/ImplicitCEGenerator;

    invoke-virtual {v7, v4}, Lcom/ibm/icu/impl/ImplicitCEGenerator;->getRawFromImplicit(I)I

    move-result v6

    .line 1578
    .local v6, "raw":I
    sget-object v7, Lcom/ibm/icu/text/RuleBasedCollator;->impCEGen_:Lcom/ibm/icu/impl/ImplicitCEGenerator;

    add-int/lit8 v8, v6, -0x1

    invoke-virtual {v7, v8}, Lcom/ibm/icu/impl/ImplicitCEGenerator;->getCodePointFromRaw(I)I

    move-result v2

    .line 1579
    sget-object v7, Lcom/ibm/icu/text/RuleBasedCollator;->impCEGen_:Lcom/ibm/icu/impl/ImplicitCEGenerator;

    add-int/lit8 v8, v6, -0x1

    invoke-virtual {v7, v8}, Lcom/ibm/icu/impl/ImplicitCEGenerator;->getImplicitFromRaw(I)I

    move-result v5

    .line 1580
    .local v5, "primaryCE":I
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    const/4 v8, 0x0

    const/high16 v9, -0x10000

    and-int/2addr v9, v5

    or-int/lit16 v9, v9, 0x505

    aput v9, v7, v8

    .line 1581
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    const/4 v8, 0x1

    shl-int/lit8 v9, v5, 0x10

    const/high16 v10, -0x10000

    and-int/2addr v9, v10

    or-int/lit16 v9, v9, 0xc0

    aput v9, v7, v8

    .line 1583
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    .line 1584
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    const v8, 0xfffe

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1585
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    int-to-char v8, v2

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1586
    iget v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    add-int/lit8 v7, v7, 0x2

    iput v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    .line 1587
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v7, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    .line 1589
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v8, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    shl-int/lit8 v8, v8, 0x18

    iget-object v9, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v9, v9, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    or-int/2addr v8, v9

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    .line 1591
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    iput-object v8, v7, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_rules_:Ljava/lang/StringBuffer;

    .line 1592
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_hashTable_:Ljava/util/Hashtable;

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    invoke-virtual {v7, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .end local p1    # "sourcetoken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    check-cast p1, Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1594
    .restart local p1    # "sourcetoken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    if-nez p1, :cond_1

    .line 1595
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    new-instance v9, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    invoke-direct {v9}, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;-><init>()V

    aput-object v9, v7, v8

    .line 1596
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    const/4 v9, 0x0

    aget v8, v8, v9

    and-int/lit16 v8, v8, -0xc1

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseCE_:I

    .line 1598
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    invoke-static {v7}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1599
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    const/4 v9, 0x1

    aget v8, v8, v9

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    .line 1605
    :goto_1
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextCE_:I

    .line 1606
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextContCE_:I

    .line 1607
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_previousCE_:I

    .line 1608
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_previousContCE_:I

    .line 1609
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput-boolean v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_indirect_:Z

    .line 1611
    new-instance p1, Lcom/ibm/icu/text/CollationRuleParser$Token;

    .end local p1    # "sourcetoken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    invoke-direct {p1}, Lcom/ibm/icu/text/CollationRuleParser$Token;-><init>()V

    .line 1612
    .restart local p1    # "sourcetoken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    const/4 v7, -0x1

    invoke-direct {p0, v7, p1}, Lcom/ibm/icu/text/CollationRuleParser;->initAReset(ILcom/ibm/icu/text/CollationRuleParser$Token;)I

    .line 1712
    .end local v4    # "primary":I
    .end local v5    # "primaryCE":I
    .end local v6    # "raw":I
    :cond_1
    :goto_2
    return-object p1

    .line 1559
    .end local v0    # "basece":I
    .end local v1    # "basecontce":I
    .end local v2    # "ch":I
    :cond_2
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_UCAColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    iget-object v9, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v9, v9, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    iget-object v10, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v10, v10, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1603
    .restart local v0    # "basece":I
    .restart local v1    # "basecontce":I
    .restart local v2    # "ch":I
    .restart local v4    # "primary":I
    .restart local v5    # "primaryCE":I
    .restart local v6    # "raw":I
    :cond_3
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    goto :goto_1

    .line 1618
    .end local v4    # "primary":I
    .end local v5    # "primaryCE":I
    .end local v6    # "raw":I
    :cond_4
    sget-object v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->INVERSE_UCA_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    invoke-virtual {v7, v0, v1, p2, v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->getInversePrevCE(III[I)I

    .line 1625
    sget-object v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->INVERSE_UCA_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    const/4 v9, 0x0

    aget v8, v8, v9

    iget-object v9, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    const/4 v10, 0x1

    aget v9, v9, v10

    invoke-virtual {v7, v0, v1, v8, v9}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->getCEStrengthDifference(IIII)I

    move-result v7

    if-ge v7, p2, :cond_5

    .line 1629
    const/4 v7, 0x1

    if-ne p2, v7, :cond_6

    .line 1630
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    const/4 v8, 0x0

    add-int/lit16 v9, v0, -0x200

    aput v9, v7, v8

    .line 1634
    :goto_3
    invoke-static {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1635
    const/4 v7, 0x1

    if-ne p2, v7, :cond_7

    .line 1636
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    const/4 v8, 0x1

    add-int/lit16 v9, v1, -0x200

    aput v9, v7, v8

    .line 1691
    :cond_5
    :goto_4
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v7, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    add-int/lit8 v8, v8, -0xa

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    .line 1692
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v7, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    add-int/lit8 v8, v8, 0xa

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    .line 1693
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    new-instance v9, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    invoke-direct {v9}, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;-><init>()V

    aput-object v9, v7, v8

    .line 1694
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    const/4 v9, 0x0

    aget v8, v8, v9

    and-int/lit16 v8, v8, -0xc1

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseCE_:I

    .line 1696
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    invoke-static {v7}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1697
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    const/4 v9, 0x1

    aget v8, v8, v9

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    .line 1703
    :goto_5
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextCE_:I

    .line 1704
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextContCE_:I

    .line 1705
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_previousCE_:I

    .line 1706
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_previousContCE_:I

    .line 1707
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput-boolean v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_indirect_:Z

    .line 1708
    new-instance p1, Lcom/ibm/icu/text/CollationRuleParser$Token;

    .end local p1    # "sourcetoken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    invoke-direct {p1}, Lcom/ibm/icu/text/CollationRuleParser$Token;-><init>()V

    .line 1709
    .restart local p1    # "sourcetoken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    const/4 v7, -0x1

    invoke-direct {p0, v7, p1}, Lcom/ibm/icu/text/CollationRuleParser;->initAReset(ILcom/ibm/icu/text/CollationRuleParser$Token;)I

    goto/16 :goto_2

    .line 1632
    :cond_6
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    const/4 v8, 0x0

    add-int/lit8 v9, v0, -0x2

    aput v9, v7, v8

    goto/16 :goto_3

    .line 1638
    :cond_7
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilCEBuffer_:[I

    const/4 v8, 0x1

    add-int/lit8 v9, v1, -0x2

    aput v9, v7, v8

    goto/16 :goto_4

    .line 1701
    :cond_8
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v7, v7, v8

    const/4 v8, 0x0

    iput v8, v7, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    goto :goto_5
.end method

.method private initAReset(ILcom/ibm/icu/text/CollationRuleParser$Token;)I
    .locals 7
    .param p1, "expand"    # I
    .param p2, "targetToken"    # Lcom/ibm/icu/text/CollationRuleParser$Token;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1729
    iget v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    iget-object v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_0

    .line 1732
    iget v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    shl-int/lit8 v2, v2, 0x1

    new-array v1, v2, [Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    .line 1733
    .local v1, "temp":[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2, v5, v1, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1734
    iput-object v1, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    .line 1737
    .end local v1    # "temp":[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    iput-object v2, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_rules_:Ljava/lang/StringBuffer;

    .line 1738
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v2, v2, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    shl-int/lit8 v2, v2, 0x18

    iget-object v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v3, v3, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    or-int/2addr v2, v3

    iput v2, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    .line 1740
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v2, v2, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_extensionLen_:I

    shl-int/lit8 v2, v2, 0x18

    iget-object v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v3, v3, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_extensionOffset_:I

    or-int/2addr v2, v3

    iput v2, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expansion_:I

    .line 1743
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget-char v2, v2, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_flags_:C

    iput-char v2, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_flags_:C

    .line 1745
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v2, v2, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_prefixOffset_:I

    if-eqz v2, :cond_1

    .line 1746
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    iget-object v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v3, v3, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    .line 1749
    :cond_1
    iput v5, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_prefix_:I

    .line 1751
    iput v6, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_polarity_:I

    .line 1752
    const v2, -0x21524111

    iput v2, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    .line 1753
    iput-object v4, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1754
    iput-object v4, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1755
    iput v5, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CELength_:I

    .line 1756
    iput v5, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCELength_:I

    .line 1757
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v2, v2, v3

    iput-object v2, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    .line 1758
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v2, v2, v3

    iput-object v4, v2, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1759
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v2, v2, v3

    iput-object v4, v2, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_last_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1760
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v2, v2, v3

    iput-object v4, v2, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1761
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v2, v2, v3

    iput-object v4, v2, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_last_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1762
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    iget v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    aget-object v2, v2, v3

    iput-object p2, v2, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_reset_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 1773
    const/4 v0, 0x0

    .line 1774
    .local v0, "result":I
    if-lez p1, :cond_2

    .line 1776
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v2, v2, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    if-le v2, v6, :cond_2

    .line 1777
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v2, v2, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    sub-int v2, p1, v2

    shl-int/lit8 v2, v2, 0x18

    iget-object v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v3, v3, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    or-int/2addr v2, v3

    iput v2, p2, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    .line 1781
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v2, v2, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    iget-object v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v3, v3, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    add-int/2addr v2, v3

    sub-int/2addr v2, p1

    shl-int/lit8 v2, v2, 0x18

    or-int v0, v2, p1

    .line 1787
    :cond_2
    iget v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    .line 1788
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_hashTable_:Ljava/util/Hashtable;

    invoke-virtual {v2, p2, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1789
    return v0
.end method

.method private static isCharNewLine(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 1128
    sparse-switch p0, :sswitch_data_0

    .line 1137
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1135
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1128
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0xc -> :sswitch_0
        0xd -> :sswitch_0
        0x85 -> :sswitch_0
        0x2028 -> :sswitch_0
        0x2029 -> :sswitch_0
    .end sparse-switch
.end method

.method private static final isSpecialChar(C)Z
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 1799
    const/16 v0, 0x2f

    if-gt p0, v0, :cond_0

    const/16 v0, 0x20

    if-ge p0, v0, :cond_4

    :cond_0
    const/16 v0, 0x3f

    if-gt p0, v0, :cond_1

    const/16 v0, 0x3a

    if-ge p0, v0, :cond_4

    :cond_1
    const/16 v0, 0x60

    if-gt p0, v0, :cond_2

    const/16 v0, 0x5b

    if-ge p0, v0, :cond_4

    :cond_2
    const/16 v0, 0x7e

    if-gt p0, v0, :cond_3

    const/16 v0, 0x7d

    if-ge p0, v0, :cond_4

    :cond_3
    const/16 v0, 0x7b

    if-ne p0, v0, :cond_5

    :cond_4
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private parseNextToken(Z)I
    .locals 23
    .param p1, "startofrules"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 1153
    const/4 v6, 0x0

    .line 1154
    .local v6, "variabletop":Z
    const/4 v3, 0x0

    .line 1155
    .local v3, "top":Z
    const/16 v16, 0x1

    .line 1156
    .local v16, "inchars":Z
    const/16 v17, 0x0

    .line 1157
    .local v17, "inquote":Z
    const/16 v22, 0x0

    .line 1158
    .local v22, "wasinquote":Z
    const/4 v7, 0x0

    .line 1159
    .local v7, "before":B
    const/16 v18, 0x0

    .line 1160
    .local v18, "isescaped":Z
    const/4 v5, 0x0

    .line 1161
    .local v5, "newextensionlen":I
    const/4 v4, 0x0

    .line 1162
    .local v4, "extensionoffset":I
    const/4 v2, -0x1

    .line 1164
    .local v2, "newstrength":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    const/4 v8, 0x0

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    .line 1165
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    const/4 v8, 0x0

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    .line 1166
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    const/4 v8, 0x0

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_prefixOffset_:I

    .line 1167
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    const/4 v8, 0x0

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_prefixLen_:I

    .line 1168
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    const/4 v8, 0x0

    iput-char v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_indirectIndex_:C

    .line 1170
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v19

    .line 1171
    .local v19, "limit":I
    :goto_0
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    move/from16 v0, v19

    if-ge v1, v0, :cond_30

    .line 1172
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    .line 1173
    .local v15, "ch":C
    if-eqz v17, :cond_7

    .line 1174
    const/16 v1, 0x27

    if-ne v15, v1, :cond_2

    .line 1175
    const/16 v17, 0x0

    .line 1501
    :cond_0
    :goto_1
    :sswitch_0
    if-eqz v22, :cond_1

    .line 1502
    const/16 v1, 0x27

    if-eq v15, v1, :cond_1

    .line 1503
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    invoke-virtual {v1, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1504
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    .line 1507
    :cond_1
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    goto :goto_0

    .line 1178
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v1, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    if-eqz v1, :cond_3

    if-eqz v16, :cond_5

    .line 1179
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v1, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    if-nez v1, :cond_4

    .line 1180
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    .line 1182
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    goto :goto_1

    .line 1185
    :cond_5
    if-nez v5, :cond_6

    .line 1186
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    .line 1188
    :cond_6
    add-int/lit8 v5, v5, 0x1

    .line 1191
    goto :goto_1

    .line 1192
    :cond_7
    if-eqz v18, :cond_c

    .line 1193
    const/16 v18, 0x0

    .line 1194
    const/4 v1, -0x1

    if-ne v2, v1, :cond_8

    .line 1195
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-static {v1, v8}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    .line 1197
    :cond_8
    if-eqz v15, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    move/from16 v0, v19

    if-eq v1, v0, :cond_0

    .line 1198
    if-eqz v16, :cond_a

    .line 1199
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v1, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    if-nez v1, :cond_9

    .line 1200
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    .line 1202
    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    goto/16 :goto_1

    .line 1205
    :cond_a
    if-nez v5, :cond_b

    .line 1206
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    .line 1208
    :cond_b
    add-int/lit8 v5, v5, 0x1

    .line 1210
    goto/16 :goto_1

    .line 1213
    :cond_c
    invoke-static {v15}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1215
    sparse-switch v15, :sswitch_data_0

    .line 1476
    const/4 v1, -0x1

    if-ne v2, v1, :cond_d

    .line 1477
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-static {v1, v8}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    .line 1479
    :cond_d
    invoke-static {v15}, Lcom/ibm/icu/text/CollationRuleParser;->isSpecialChar(C)Z

    move-result v1

    if-eqz v1, :cond_e

    if-nez v17, :cond_e

    .line 1480
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-static {v1, v8}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    .line 1482
    :cond_e
    if-nez v15, :cond_f

    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v1, v1, 0x1

    move/from16 v0, v19

    if-eq v1, v0, :cond_0

    .line 1485
    :cond_f
    if-eqz v16, :cond_2e

    .line 1486
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v1, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    if-nez v1, :cond_10

    .line 1487
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    .line 1489
    :cond_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    goto/16 :goto_1

    .line 1217
    :sswitch_1
    const/4 v1, -0x1

    if-eq v2, v1, :cond_11

    move-object/from16 v1, p0

    .line 1218
    invoke-direct/range {v1 .. v7}, Lcom/ibm/icu/text/CollationRuleParser;->doEndParseNextToken(IZIIZI)I

    move-result v1

    .line 1509
    .end local v15    # "ch":C
    :goto_2
    return v1

    .line 1225
    .restart local v15    # "ch":C
    :cond_11
    const/4 v1, 0x1

    move/from16 v0, p1

    if-ne v0, v1, :cond_12

    .line 1226
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    const/4 v8, 0x5

    iput-char v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_indirectIndex_:C

    .line 1227
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationRuleParser;->doSetTop()Z

    move-result v3

    .line 1228
    const v9, -0x21524111

    move-object/from16 v8, p0

    move v10, v3

    move v11, v4

    move v12, v5

    move v13, v6

    move v14, v7

    invoke-direct/range {v8 .. v14}, Lcom/ibm/icu/text/CollationRuleParser;->doEndParseNextToken(IZIIZI)I

    move-result v1

    goto :goto_2

    .line 1234
    :cond_12
    const/16 v2, 0xf

    .line 1235
    goto/16 :goto_1

    .line 1237
    :sswitch_2
    const/4 v1, -0x1

    if-eq v2, v1, :cond_13

    move-object/from16 v1, p0

    .line 1238
    invoke-direct/range {v1 .. v7}, Lcom/ibm/icu/text/CollationRuleParser;->doEndParseNextToken(IZIIZI)I

    move-result v1

    goto :goto_2

    .line 1245
    :cond_13
    const/4 v1, 0x1

    move/from16 v0, p1

    if-ne v0, v1, :cond_14

    .line 1246
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    const/4 v8, 0x5

    iput-char v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_indirectIndex_:C

    .line 1247
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationRuleParser;->doSetTop()Z

    move-result v3

    .line 1248
    const v9, -0x21524111

    move-object/from16 v8, p0

    move v10, v3

    move v11, v4

    move v12, v5

    move v13, v6

    move v14, v7

    invoke-direct/range {v8 .. v14}, Lcom/ibm/icu/text/CollationRuleParser;->doEndParseNextToken(IZIIZI)I

    move-result v1

    goto :goto_2

    .line 1254
    :cond_14
    const/4 v2, 0x2

    .line 1255
    goto/16 :goto_1

    .line 1257
    :sswitch_3
    const/4 v1, -0x1

    if-eq v2, v1, :cond_15

    move-object/from16 v1, p0

    .line 1258
    invoke-direct/range {v1 .. v7}, Lcom/ibm/icu/text/CollationRuleParser;->doEndParseNextToken(IZIIZI)I

    move-result v1

    goto :goto_2

    .line 1265
    :cond_15
    const/4 v1, 0x1

    move/from16 v0, p1

    if-ne v0, v1, :cond_16

    .line 1266
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    const/4 v8, 0x5

    iput-char v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_indirectIndex_:C

    .line 1267
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationRuleParser;->doSetTop()Z

    move-result v3

    .line 1268
    const v9, -0x21524111

    move-object/from16 v8, p0

    move v10, v3

    move v11, v4

    move v12, v5

    move v13, v6

    move v14, v7

    invoke-direct/range {v8 .. v14}, Lcom/ibm/icu/text/CollationRuleParser;->doEndParseNextToken(IZIIZI)I

    move-result v1

    goto :goto_2

    .line 1274
    :cond_16
    const/4 v2, 0x1

    .line 1275
    goto/16 :goto_1

    .line 1277
    :sswitch_4
    const/4 v1, -0x1

    if-eq v2, v1, :cond_17

    move-object/from16 v1, p0

    .line 1278
    invoke-direct/range {v1 .. v7}, Lcom/ibm/icu/text/CollationRuleParser;->doEndParseNextToken(IZIIZI)I

    move-result v1

    goto/16 :goto_2

    .line 1285
    :cond_17
    const/4 v1, 0x1

    move/from16 v0, p1

    if-ne v0, v1, :cond_18

    .line 1286
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    const/4 v8, 0x5

    iput-char v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_indirectIndex_:C

    .line 1287
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationRuleParser;->doSetTop()Z

    move-result v3

    .line 1288
    const v9, -0x21524111

    move-object/from16 v8, p0

    move v10, v3

    move v11, v4

    move v12, v5

    move v13, v6

    move v14, v7

    invoke-direct/range {v8 .. v14}, Lcom/ibm/icu/text/CollationRuleParser;->doEndParseNextToken(IZIIZI)I

    move-result v1

    goto/16 :goto_2

    .line 1296
    :cond_18
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    const/16 v8, 0x3c

    if-ne v1, v8, :cond_1a

    .line 1297
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    .line 1298
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    const/16 v8, 0x3c

    if-ne v1, v8, :cond_19

    .line 1299
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    .line 1300
    const/4 v2, 0x2

    .line 1301
    goto/16 :goto_1

    .line 1303
    :cond_19
    const/4 v2, 0x1

    .line 1305
    goto/16 :goto_1

    .line 1307
    :cond_1a
    const/4 v2, 0x0

    .line 1309
    goto/16 :goto_1

    .line 1311
    :sswitch_5
    const/4 v1, -0x1

    if-eq v2, v1, :cond_1b

    move-object/from16 v1, p0

    .line 1312
    invoke-direct/range {v1 .. v7}, Lcom/ibm/icu/text/CollationRuleParser;->doEndParseNextToken(IZIIZI)I

    move-result v1

    goto/16 :goto_2

    .line 1318
    :cond_1b
    const v2, -0x21524111

    .line 1319
    goto/16 :goto_1

    .line 1322
    :sswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    const/16 v8, 0x5d

    move-object/from16 v0, p0

    iget v9, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_optionEnd_:I

    .line 1323
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_optionEnd_:I

    const/4 v8, -0x1

    if-eq v1, v8, :cond_0

    .line 1324
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationRuleParser;->readAndSetOption()B

    move-result v21

    .line 1325
    .local v21, "result":B
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_optionEnd_:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    .line 1326
    and-int/lit8 v1, v21, 0x4

    if-eqz v1, :cond_1e

    .line 1327
    const v1, -0x21524111

    if-ne v2, v1, :cond_1d

    .line 1328
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationRuleParser;->doSetTop()Z

    move-result v3

    .line 1329
    if-eqz v7, :cond_1c

    .line 1333
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    const/16 v8, 0x2d

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1334
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    int-to-char v8, v7

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1335
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    add-int/lit8 v1, v1, 0x2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    .line 1336
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    add-int/lit8 v8, v8, 0x2

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    .line 1338
    :cond_1c
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    .line 1339
    const/4 v10, 0x1

    move-object/from16 v8, p0

    move v9, v2

    move v11, v4

    move v12, v5

    move v13, v6

    move v14, v7

    invoke-direct/range {v8 .. v14}, Lcom/ibm/icu/text/CollationRuleParser;->doEndParseNextToken(IZIIZI)I

    move-result v1

    goto/16 :goto_2

    .line 1346
    :cond_1d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-static {v1, v8}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 1349
    :cond_1e
    and-int/lit8 v1, v21, 0x8

    if-eqz v1, :cond_20

    .line 1350
    const v1, -0x21524111

    if-eq v2, v1, :cond_1f

    const/4 v1, -0x1

    if-eq v2, v1, :cond_1f

    .line 1352
    const/4 v6, 0x1

    .line 1353
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    .line 1355
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    const v8, 0xffff

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1356
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    .line 1357
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    .line 1358
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    const/4 v8, 0x1

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    move-object/from16 v1, p0

    .line 1359
    invoke-direct/range {v1 .. v7}, Lcom/ibm/icu/text/CollationRuleParser;->doEndParseNextToken(IZIIZI)I

    move-result v1

    goto/16 :goto_2

    .line 1366
    :cond_1f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-static {v1, v8}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 1369
    :cond_20
    and-int/lit8 v1, v21, 0x3

    if-eqz v1, :cond_0

    .line 1370
    const v1, -0x21524111

    if-ne v2, v1, :cond_21

    .line 1371
    and-int/lit8 v1, v21, 0x3

    int-to-byte v7, v1

    .line 1372
    goto/16 :goto_1

    .line 1374
    :cond_21
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-static {v1, v8}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 1380
    .end local v21    # "result":B
    :sswitch_7
    const/16 v22, 0x0

    .line 1382
    const/16 v16, 0x0

    .line 1383
    goto/16 :goto_1

    .line 1385
    :sswitch_8
    const/16 v18, 0x1

    .line 1386
    goto/16 :goto_1

    .line 1389
    :sswitch_9
    const/4 v1, -0x1

    if-ne v2, v1, :cond_22

    .line 1391
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-static {v1, v8}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    .line 1393
    :cond_22
    const/16 v17, 0x1

    .line 1394
    if-eqz v16, :cond_25

    .line 1395
    if-nez v22, :cond_23

    .line 1396
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    .line 1398
    :cond_23
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v1, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    if-eqz v1, :cond_24

    .line 1399
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v10, v10, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    sub-int/2addr v9, v10

    move-object/from16 v0, p0

    iget v10, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1402
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v8, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    add-int/2addr v1, v8

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    .line 1404
    :cond_24
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    .line 1418
    :goto_3
    const/16 v22, 0x1

    .line 1419
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    .line 1420
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    .line 1421
    const/16 v1, 0x27

    if-ne v15, v1, :cond_0

    .line 1422
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    invoke-virtual {v1, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1423
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    .line 1424
    const/16 v17, 0x0

    .line 1425
    goto/16 :goto_1

    .line 1407
    :cond_25
    if-nez v22, :cond_26

    .line 1408
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    .line 1410
    :cond_26
    if-eqz v5, :cond_27

    .line 1411
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    sub-int/2addr v9, v5

    move-object/from16 v0, p0

    iget v10, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1414
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    add-int/2addr v1, v5

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    .line 1416
    :cond_27
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 1430
    :sswitch_a
    const/4 v1, -0x1

    if-ne v2, v1, :cond_28

    .line 1431
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    const/4 v8, 0x1

    iput-boolean v8, v1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isFrenchCollation_:Z

    goto/16 :goto_1

    .line 1443
    :cond_28
    :sswitch_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v8, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_prefixOffset_:I

    .line 1445
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v8, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_prefixLen_:I

    .line 1447
    if-eqz v16, :cond_2b

    .line 1448
    if-nez v22, :cond_29

    .line 1449
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    .line 1451
    :cond_29
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v1, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    if-eqz v1, :cond_2a

    .line 1452
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v9, v9, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    sub-int/2addr v8, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v20

    .line 1455
    .local v20, "prefix":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v0, v20

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1456
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v8, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    add-int/2addr v1, v8

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    .line 1458
    .end local v20    # "prefix":Ljava/lang/String;
    :cond_2a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    .line 1460
    :cond_2b
    const/16 v22, 0x1

    .line 1462
    :cond_2c
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    .line 1463
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    .line 1465
    invoke-static {v15}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v1

    if-nez v1, :cond_2c

    goto/16 :goto_1

    .line 1469
    :cond_2d
    :sswitch_c
    move-object/from16 v0, p0

    iget v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    .line 1470
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    .line 1471
    invoke-static {v15}, Lcom/ibm/icu/text/CollationRuleParser;->isCharNewLine(C)Z

    move-result v1

    if-eqz v1, :cond_2d

    goto/16 :goto_1

    .line 1492
    :cond_2e
    if-nez v5, :cond_2f

    .line 1493
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    .line 1495
    :cond_2f
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .end local v15    # "ch":C
    :cond_30
    move-object/from16 v1, p0

    .line 1509
    invoke-direct/range {v1 .. v7}, Lcom/ibm/icu/text/CollationRuleParser;->doEndParseNextToken(IZIIZI)I

    move-result v1

    goto/16 :goto_2

    .line 1215
    nop

    :sswitch_data_0
    .sparse-switch
        0x21 -> :sswitch_0
        0x23 -> :sswitch_c
        0x26 -> :sswitch_5
        0x27 -> :sswitch_9
        0x2c -> :sswitch_2
        0x2f -> :sswitch_7
        0x3b -> :sswitch_3
        0x3c -> :sswitch_4
        0x3d -> :sswitch_1
        0x40 -> :sswitch_a
        0x5b -> :sswitch_6
        0x5c -> :sswitch_8
        0x7c -> :sswitch_b
    .end sparse-switch
.end method

.method private readAndSetOption()B
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x14

    const/16 v12, 0xd

    const/4 v11, 0x7

    const/16 v7, 0x10

    .line 1870
    iget v9, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v5, v9, 0x1

    .line 1871
    .local v5, "start":I
    iget-object v9, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    iget v10, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_optionEnd_:I

    invoke-direct {p0, v9, v5, v10}, Lcom/ibm/icu/text/CollationRuleParser;->readOption(Ljava/lang/String;II)I

    move-result v0

    .line 1873
    .local v0, "i":I
    iget v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_optionarg_:I

    .line 1875
    .local v3, "optionarg":I
    if-gez v0, :cond_0

    .line 1876
    iget-object v9, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    invoke-static {v9, v5}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    .line 1879
    :cond_0
    if-ge v0, v11, :cond_4

    .line 1880
    if-eqz v3, :cond_3

    .line 1881
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    sget-object v8, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    aget-object v8, v8, v0

    invoke-static {v8}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->access$100(Lcom/ibm/icu/text/CollationRuleParser$TokenOption;)[Ljava/lang/String;

    move-result-object v8

    array-length v8, v8

    if-ge v1, v8, :cond_3

    .line 1883
    sget-object v8, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    aget-object v8, v8, v0

    invoke-static {v8}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->access$100(Lcom/ibm/icu/text/CollationRuleParser$TokenOption;)[Ljava/lang/String;

    move-result-object v8

    aget-object v6, v8, v1

    .line 1884
    .local v6, "subname":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    add-int v4, v3, v8

    .line 1885
    .local v4, "size":I
    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-le v8, v4, :cond_2

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    invoke-virtual {v8, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1888
    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    sget-object v9, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    aget-object v9, v9, v0

    invoke-static {v9}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->access$200(Lcom/ibm/icu/text/CollationRuleParser$TokenOption;)I

    move-result v9

    sget-object v10, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    aget-object v10, v10, v0

    invoke-static {v10}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->access$300(Lcom/ibm/icu/text/CollationRuleParser$TokenOption;)[I

    move-result-object v10

    aget v10, v10, v1

    invoke-direct {p0, v8, v9, v10}, Lcom/ibm/icu/text/CollationRuleParser;->setOptions(Lcom/ibm/icu/text/CollationRuleParser$OptionSet;II)V

    .line 1957
    .end local v1    # "j":I
    .end local v4    # "size":I
    .end local v6    # "subname":Ljava/lang/String;
    :cond_1
    :goto_1
    return v7

    .line 1882
    .restart local v1    # "j":I
    .restart local v4    # "size":I
    .restart local v6    # "subname":Ljava/lang/String;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1894
    .end local v1    # "j":I
    .end local v4    # "size":I
    .end local v6    # "subname":Ljava/lang/String;
    :cond_3
    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    invoke-static {v8, v3}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    goto :goto_1

    .line 1896
    :cond_4
    if-ne v0, v11, :cond_5

    .line 1897
    const/16 v7, 0x18

    goto :goto_1

    .line 1899
    :cond_5
    const/16 v9, 0x8

    if-eq v0, v9, :cond_1

    .line 1902
    const/16 v9, 0x9

    if-ne v0, v9, :cond_8

    .line 1903
    if-eqz v3, :cond_7

    .line 1904
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_2
    sget-object v8, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    aget-object v8, v8, v0

    invoke-static {v8}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->access$100(Lcom/ibm/icu/text/CollationRuleParser$TokenOption;)[Ljava/lang/String;

    move-result-object v8

    array-length v8, v8

    if-ge v1, v8, :cond_7

    .line 1906
    sget-object v8, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    aget-object v8, v8, v0

    invoke-static {v8}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->access$100(Lcom/ibm/icu/text/CollationRuleParser$TokenOption;)[Ljava/lang/String;

    move-result-object v8

    aget-object v6, v8, v1

    .line 1907
    .restart local v6    # "subname":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    add-int v4, v3, v8

    .line 1908
    .restart local v4    # "size":I
    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-le v8, v4, :cond_6

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v9, v3

    invoke-virtual {v8, v3, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1912
    sget-object v7, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    aget-object v7, v7, v0

    invoke-static {v7}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->access$300(Lcom/ibm/icu/text/CollationRuleParser$TokenOption;)[I

    move-result-object v7

    aget v7, v7, v1

    add-int/lit8 v7, v7, 0x1

    or-int/lit8 v7, v7, 0x10

    int-to-byte v7, v7

    goto :goto_1

    .line 1905
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1918
    .end local v1    # "j":I
    .end local v4    # "size":I
    .end local v6    # "subname":Ljava/lang/String;
    :cond_7
    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    invoke-static {v8, v3}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    goto :goto_1

    .line 1920
    :cond_8
    const/16 v9, 0xa

    if-ne v0, v9, :cond_9

    .line 1923
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    const/4 v9, 0x0

    iput-char v9, v7, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_indirectIndex_:C

    move v7, v8

    .line 1924
    goto :goto_1

    .line 1926
    :cond_9
    if-ge v0, v12, :cond_c

    .line 1927
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_3
    sget-object v9, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    aget-object v9, v9, v0

    invoke-static {v9}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->access$100(Lcom/ibm/icu/text/CollationRuleParser$TokenOption;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    if-ge v1, v9, :cond_b

    .line 1928
    sget-object v9, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    aget-object v9, v9, v0

    invoke-static {v9}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->access$100(Lcom/ibm/icu/text/CollationRuleParser$TokenOption;)[Ljava/lang/String;

    move-result-object v9

    aget-object v6, v9, v1

    .line 1929
    .restart local v6    # "subname":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    add-int v4, v3, v9

    .line 1930
    .restart local v4    # "size":I
    iget-object v9, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-le v9, v4, :cond_a

    iget-object v9, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 1933
    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    add-int/lit8 v9, v0, -0xa

    shl-int/lit8 v10, v1, 0x1

    add-int/2addr v9, v10

    int-to-char v9, v9

    iput-char v9, v7, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_indirectIndex_:C

    move v7, v8

    .line 1934
    goto/16 :goto_1

    .line 1927
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1937
    .end local v4    # "size":I
    .end local v6    # "subname":Ljava/lang/String;
    :cond_b
    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    invoke-static {v8, v3}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 1939
    .end local v1    # "j":I
    :cond_c
    if-eq v0, v12, :cond_d

    const/16 v8, 0xe

    if-ne v0, v8, :cond_11

    .line 1941
    :cond_d
    const/4 v2, 0x1

    .line 1942
    .local v2, "noOpenBraces":I
    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    .line 1943
    :goto_4
    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    iget-object v9, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v9

    if-ge v8, v9, :cond_10

    if-eqz v2, :cond_10

    .line 1944
    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    iget v9, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v8

    const/16 v9, 0x5b

    if-ne v8, v9, :cond_f

    .line 1945
    add-int/lit8 v2, v2, 0x1

    .line 1949
    :cond_e
    :goto_5
    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    goto :goto_4

    .line 1946
    :cond_f
    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    iget v9, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v8

    const/16 v9, 0x5d

    if-ne v8, v9, :cond_e

    .line 1947
    add-int/lit8 v2, v2, -0x1

    goto :goto_5

    .line 1951
    :cond_10
    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_optionEnd_:I

    goto/16 :goto_1

    .line 1955
    .end local v2    # "noOpenBraces":I
    :cond_11
    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    invoke-static {v8, v3}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    goto/16 :goto_1
.end method

.method private readAndSetUnicodeSet(Ljava/lang/String;I)Lcom/ibm/icu/text/UnicodeSet;
    .locals 5
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "start"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x5b

    .line 1807
    :goto_0
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v2, v4, :cond_0

    .line 1808
    add-int/lit8 p2, p2, 0x1

    .line 1809
    goto :goto_0

    .line 1812
    :cond_0
    const/4 v1, 0x1

    .line 1813
    .local v1, "noOpenBraces":I
    const/4 v0, 0x1

    .line 1814
    .local v0, "current":I
    :goto_1
    add-int v2, p2, v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_3

    if-eqz v1, :cond_3

    .line 1815
    add-int v2, p2, v0

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v2, v4, :cond_2

    .line 1816
    add-int/lit8 v1, v1, 0x1

    .line 1820
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    .line 1821
    goto :goto_1

    .line 1817
    :cond_2
    add-int v2, p2, v0

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x5d

    if-ne v2, v3, :cond_1

    .line 1818
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 1824
    :cond_3
    if-nez v1, :cond_4

    const-string/jumbo v2, "]"

    add-int v3, p2, v0

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_5

    .line 1825
    :cond_4
    iget-object v2, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_rules_:Ljava/lang/String;

    invoke-static {v2, p2}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    .line 1827
    :cond_5
    new-instance v2, Lcom/ibm/icu/text/UnicodeSet;

    add-int v3, p2, v0

    invoke-virtual {p1, p2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    return-object v2
.end method

.method private readOption(Ljava/lang/String;II)I
    .locals 5
    .param p1, "rules"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "optionend"    # I

    .prologue
    .line 1838
    const/4 v3, 0x0

    iput v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_optionarg_:I

    .line 1839
    const/4 v0, 0x0

    .line 1840
    .local v0, "i":I
    :goto_0
    sget-object v3, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1841
    sget-object v3, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    aget-object v3, v3, v0

    invoke-static {v3}, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->access$000(Lcom/ibm/icu/text/CollationRuleParser$TokenOption;)Ljava/lang/String;

    move-result-object v1

    .line 1842
    .local v1, "option":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    .line 1843
    .local v2, "optionlength":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int v4, p2, v2

    if-le v3, v4, :cond_0

    add-int v3, p2, v2

    invoke-virtual {p1, p2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1846
    sub-int v3, p3, p2

    if-le v3, v2, :cond_1

    .line 1847
    add-int v3, p2, v2

    iput v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_optionarg_:I

    .line 1849
    :goto_1
    iget v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_optionarg_:I

    if-ge v3, p3, :cond_1

    iget v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_optionarg_:I

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/lang/UCharacter;->isWhitespace(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1851
    iget v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_optionarg_:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_optionarg_:I

    goto :goto_1

    .line 1856
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 1857
    goto :goto_0

    .line 1858
    .end local v1    # "option":Ljava/lang/String;
    .end local v2    # "optionlength":I
    :cond_1
    sget-object v3, Lcom/ibm/icu/text/CollationRuleParser;->RULES_OPTIONS_:[Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    array-length v3, v3

    if-ne v0, v3, :cond_2

    .line 1859
    const/4 v0, -0x1

    .line 1861
    :cond_2
    return v0
.end method

.method private setOptions(Lcom/ibm/icu/text/CollationRuleParser$OptionSet;II)V
    .locals 3
    .param p1, "optionset"    # Lcom/ibm/icu/text/CollationRuleParser$OptionSet;
    .param p2, "attribute"    # I
    .param p3, "value"    # I

    .prologue
    const/16 v2, 0x11

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1968
    packed-switch p2, :pswitch_data_0

    .line 2001
    :goto_0
    return-void

    .line 1970
    :pswitch_0
    if-ne p3, v2, :cond_0

    :goto_1
    iput-boolean v0, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isHiragana4_:Z

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    .line 1974
    :pswitch_1
    if-ne p3, v2, :cond_1

    :goto_2
    iput-boolean v0, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isFrenchCollation_:Z

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_2

    .line 1978
    :pswitch_2
    const/16 v2, 0x14

    if-ne p3, v2, :cond_2

    :goto_3
    iput-boolean v0, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isAlternateHandlingShifted_:Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_3

    .line 1983
    :pswitch_3
    iput p3, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_caseFirst_:I

    goto :goto_0

    .line 1986
    :pswitch_4
    if-ne p3, v2, :cond_3

    :goto_4
    iput-boolean v0, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isCaseLevel_:Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_4

    .line 1990
    :pswitch_5
    if-ne p3, v2, :cond_4

    .line 1991
    const/16 p3, 0x11

    .line 1993
    :cond_4
    iput p3, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_decomposition_:I

    goto :goto_0

    .line 1996
    :pswitch_6
    iput p3, p1, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_strength_:I

    goto :goto_0

    .line 1968
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
    .end packed-switch
.end method

.method private static final throwParseException(Ljava/lang/String;I)V
    .locals 5
    .param p0, "rules"    # Ljava/lang/String;
    .param p1, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 1090
    const/4 v3, 0x0

    invoke-virtual {p0, v3, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1091
    .local v2, "precontext":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p0, p1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1092
    .local v1, "postcontext":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuffer;

    const-string/jumbo v3, "Parse error occurred in rule at offset "

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1094
    .local v0, "error":Ljava/lang/StringBuffer;
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1095
    const-string/jumbo v3, "\n after the prefix \""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1096
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1097
    const-string/jumbo v3, "\" before the suffix \""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1098
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1099
    new-instance v3, Ljava/text/ParseException;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, p1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v3
.end method


# virtual methods
.method assembleTokenList()I
    .locals 33
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 677
    const/4 v15, 0x0

    .line 678
    .local v15, "lastToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v28, v0

    const/16 v29, -0x1

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_strength_:I

    .line 679
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuffer;->length()I

    move-result v22

    .line 680
    .local v22, "sourcelimit":I
    const/4 v11, 0x0

    .line 682
    .local v11, "expandNext":I
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    move/from16 v28, v0

    move/from16 v0, v28

    move/from16 v1, v22

    if-ge v0, v1, :cond_2d

    .line 683
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_prefixOffset_:I

    .line 684
    if-nez v15, :cond_9

    const/16 v28, 0x1

    :goto_1
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/CollationRuleParser;->parseNextToken(Z)I

    move-result v28

    if-ltz v28, :cond_0

    .line 688
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-char v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_flags_:C

    move/from16 v23, v0

    .line 689
    .local v23, "specs":C
    and-int/lit8 v28, v23, 0x8

    if-eqz v28, :cond_a

    const/16 v27, 0x1

    .line 690
    .local v27, "variableTop":Z
    :goto_2
    and-int/lit8 v28, v23, 0x4

    if-eqz v28, :cond_b

    const/16 v26, 0x1

    .line 691
    .local v26, "top":Z
    :goto_3
    const/4 v14, -0x1

    .line 692
    .local v14, "lastStrength":I
    if-eqz v15, :cond_1

    .line 693
    iget v14, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    .line 695
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    move/from16 v29, v0

    shl-int/lit8 v29, v29, 0x18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    move/from16 v30, v0

    or-int v29, v29, v30

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    .line 697
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_rules_:Ljava/lang/StringBuffer;

    .line 700
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_hashTable_:Ljava/util/Hashtable;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 701
    .local v21, "sourceToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_strength_:I

    move/from16 v28, v0

    const v29, -0x21524111

    move/from16 v0, v28

    move/from16 v1, v29

    if-eq v0, v1, :cond_1e

    .line 702
    if-nez v15, :cond_2

    .line 704
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v28

    const/16 v29, 0x0

    invoke-static/range {v28 .. v29}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    .line 707
    :cond_2
    if-nez v21, :cond_c

    .line 709
    new-instance v21, Lcom/ibm/icu/text/CollationRuleParser$Token;

    .end local v21    # "sourceToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    invoke-direct/range {v21 .. v21}, Lcom/ibm/icu/text/CollationRuleParser$Token;-><init>()V

    .line 710
    .restart local v21    # "sourceToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_rules_:Ljava/lang/StringBuffer;

    .line 711
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    move/from16 v28, v0

    shl-int/lit8 v28, v28, 0x18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    move/from16 v29, v0

    or-int v28, v28, v29

    move/from16 v0, v28

    move-object/from16 v1, v21

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    .line 713
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_prefixLen_:I

    move/from16 v28, v0

    shl-int/lit8 v28, v28, 0x18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_prefixOffset_:I

    move/from16 v29, v0

    or-int v28, v28, v29

    move/from16 v0, v28

    move-object/from16 v1, v21

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_prefix_:I

    .line 716
    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, v21

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_polarity_:I

    .line 717
    const/16 v28, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 718
    const/16 v28, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 719
    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, v21

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CELength_:I

    .line 720
    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, v21

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCELength_:I

    .line 721
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_hashTable_:Ljava/util/Hashtable;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 753
    :cond_3
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_strength_:I

    move/from16 v28, v0

    move/from16 v0, v28

    move-object/from16 v1, v21

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    .line 754
    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    .line 758
    const v28, -0x21524111

    move/from16 v0, v28

    if-eq v14, v0, :cond_4

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-nez v28, :cond_14

    .line 761
    :cond_4
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-nez v28, :cond_10

    .line 762
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 763
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_last_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 845
    :cond_5
    :goto_5
    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-nez v28, :cond_6

    .line 846
    const/16 v27, 0x0

    .line 847
    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser;->m_variableTop_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 854
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_extensionLen_:I

    move/from16 v28, v0

    shl-int/lit8 v28, v28, 0x18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_extensionOffset_:I

    move/from16 v29, v0

    or-int v28, v28, v29

    move/from16 v0, v28

    move-object/from16 v1, v21

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expansion_:I

    .line 856
    if-eqz v11, :cond_7

    .line 857
    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v28, v0

    if-nez v28, :cond_1b

    .line 859
    const/4 v11, 0x0

    .line 889
    :cond_7
    :goto_6
    iget-char v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_flags_:C

    move/from16 v28, v0

    and-int/lit8 v28, v28, 0x3

    if-eqz v28, :cond_8

    .line 890
    iget-char v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_flags_:C

    move/from16 v28, v0

    and-int/lit8 v28, v28, 0x3

    add-int/lit8 v7, v28, -0x1

    .line 891
    .local v7, "beforeStrength":I
    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-eq v7, v0, :cond_8

    .line 892
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    move/from16 v29, v0

    invoke-static/range {v28 .. v29}, Lcom/ibm/icu/text/CollationRuleParser;->throwParseException(Ljava/lang/String;I)V

    .line 1070
    .end local v7    # "beforeStrength":I
    :cond_8
    :goto_7
    move-object/from16 v15, v21

    .line 1071
    goto/16 :goto_0

    .line 684
    .end local v14    # "lastStrength":I
    .end local v21    # "sourceToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    .end local v23    # "specs":C
    .end local v26    # "top":Z
    .end local v27    # "variableTop":Z
    :cond_9
    const/16 v28, 0x0

    goto/16 :goto_1

    .line 689
    .restart local v23    # "specs":C
    :cond_a
    const/16 v27, 0x0

    goto/16 :goto_2

    .line 690
    .restart local v27    # "variableTop":Z
    :cond_b
    const/16 v26, 0x0

    goto/16 :goto_3

    .line 725
    .restart local v14    # "lastStrength":I
    .restart local v21    # "sourceToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    .restart local v26    # "top":Z
    :cond_c
    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v28, v0

    const v29, -0x21524111

    move/from16 v0, v28

    move/from16 v1, v29

    if-eq v0, v1, :cond_3

    move-object/from16 v0, v21

    if-eq v15, v0, :cond_3

    .line 728
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-eqz v28, :cond_e

    .line 729
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_d

    .line 731
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    .line 734
    :cond_d
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 741
    :goto_8
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-eqz v28, :cond_f

    .line 742
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 749
    :goto_9
    const/16 v28, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 750
    const/16 v28, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    goto/16 :goto_4

    .line 738
    :cond_e
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_last_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    goto :goto_8

    .line 746
    :cond_f
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    goto :goto_9

    .line 767
    :cond_10
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-gt v0, v1, :cond_11

    .line 769
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 771
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 772
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 773
    const/16 v28, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    goto/16 :goto_5

    .line 776
    :cond_11
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v15, v0, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 779
    :goto_a
    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-eqz v28, :cond_12

    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_12

    .line 780
    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 781
    goto :goto_a

    .line 782
    :cond_12
    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-eqz v28, :cond_13

    .line 783
    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 789
    :goto_b
    move-object/from16 v0, v21

    iput-object v15, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 790
    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 791
    move-object/from16 v0, v21

    iput-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    goto/16 :goto_5

    .line 786
    :cond_13
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_last_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    goto :goto_b

    .line 802
    :cond_14
    move-object/from16 v0, v21

    if-eq v0, v15, :cond_1a

    .line 803
    iget v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_polarity_:I

    move/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_polarity_:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_17

    .line 806
    :goto_c
    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-eqz v28, :cond_15

    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_15

    .line 807
    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 808
    goto :goto_c

    .line 809
    :cond_15
    move-object/from16 v0, v21

    iput-object v15, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 810
    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-eqz v28, :cond_16

    .line 811
    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 816
    :goto_d
    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 817
    move-object/from16 v0, v21

    iput-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    goto/16 :goto_5

    .line 814
    :cond_16
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_last_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    goto :goto_d

    .line 822
    :cond_17
    :goto_e
    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-eqz v28, :cond_18

    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v28, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_18

    .line 823
    iget-object v15, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 824
    goto :goto_e

    .line 825
    :cond_18
    move-object/from16 v0, v21

    iput-object v15, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 826
    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-eqz v28, :cond_19

    .line 827
    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 833
    :goto_f
    iget-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 834
    move-object/from16 v0, v21

    iput-object v0, v15, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    goto/16 :goto_5

    .line 830
    :cond_19
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    goto :goto_f

    .line 839
    :cond_1a
    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v14, v0, :cond_5

    .line 840
    move-object/from16 v0, v21

    iput v14, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    goto/16 :goto_5

    .line 861
    :cond_1b
    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expansion_:I

    move/from16 v28, v0

    if-nez v28, :cond_1c

    .line 864
    move-object/from16 v0, v21

    iput v11, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expansion_:I

    goto/16 :goto_6

    .line 869
    :cond_1c
    const v28, 0xffffff

    and-int v24, v11, v28

    .line 870
    .local v24, "start":I
    ushr-int/lit8 v20, v11, 0x18

    .line 871
    .local v20, "size":I
    if-lez v20, :cond_1d

    .line 872
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v29, v0

    add-int v30, v24, v20

    move-object/from16 v0, v29

    move/from16 v1, v24

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 875
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_extensionOffset_:I

    move/from16 v24, v0

    .line 876
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_extensionLen_:I

    move/from16 v30, v0

    add-int v30, v30, v24

    move-object/from16 v0, v29

    move/from16 v1, v24

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 878
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_extensionLen_:I

    move/from16 v28, v0

    add-int v28, v28, v20

    shl-int/lit8 v28, v28, 0x18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    move/from16 v29, v0

    or-int v28, v28, v29

    move/from16 v0, v28

    move-object/from16 v1, v21

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expansion_:I

    .line 881
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_extensionLen_:I

    move/from16 v29, v0

    add-int v29, v29, v20

    add-int v28, v28, v29

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser;->m_extraCurrent_:I

    goto/16 :goto_6

    .line 898
    .end local v20    # "size":I
    .end local v24    # "start":I
    :cond_1e
    if-eqz v15, :cond_1f

    const v28, -0x21524111

    move/from16 v0, v28

    if-ne v14, v0, :cond_1f

    .line 902
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v28, v0

    if-lez v28, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    add-int/lit8 v29, v29, -0x1

    aget-object v28, v28, v29

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-nez v28, :cond_1f

    .line 903
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v28, v0

    add-int/lit8 v28, v28, -0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    .line 906
    :cond_1f
    if-nez v21, :cond_21

    .line 909
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    move/from16 v19, v0

    .line 910
    .local v19, "searchCharsLen":I
    :goto_10
    const/16 v28, 0x1

    move/from16 v0, v19

    move/from16 v1, v28

    if-le v0, v1, :cond_20

    if-nez v21, :cond_20

    .line 911
    add-int/lit8 v19, v19, -0x1

    .line 913
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    shl-int/lit8 v29, v19, 0x18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    move/from16 v30, v0

    or-int v29, v29, v30

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    .line 915
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_rules_:Ljava/lang/StringBuffer;

    .line 916
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_hashTable_:Ljava/util/Hashtable;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_utilToken_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    .end local v21    # "sourceToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    check-cast v21, Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 917
    .restart local v21    # "sourceToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    goto :goto_10

    .line 918
    :cond_20
    if-eqz v21, :cond_21

    .line 919
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    move/from16 v28, v0

    sub-int v28, v28, v19

    shl-int/lit8 v28, v28, 0x18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    move/from16 v29, v0

    add-int v29, v29, v19

    or-int v11, v28, v29

    .line 925
    .end local v19    # "searchCharsLen":I
    :cond_21
    and-int/lit8 v28, v23, 0x3

    if-eqz v28, :cond_23

    .line 926
    if-nez v26, :cond_28

    .line 928
    and-int/lit8 v28, v23, 0x3

    add-int/lit8 v25, v28, -0x1

    .line 929
    .local v25, "strength":I
    if-eqz v21, :cond_27

    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v28, v0

    const v29, -0x21524111

    move/from16 v0, v28

    move/from16 v1, v29

    if-eq v0, v1, :cond_27

    .line 934
    :goto_11
    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v28, v0

    move/from16 v0, v28

    move/from16 v1, v25

    if-le v0, v1, :cond_22

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-eqz v28, :cond_22

    .line 935
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v21, v0

    .line 936
    goto :goto_11

    .line 938
    :cond_22
    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    move/from16 v28, v0

    move/from16 v0, v28

    move/from16 v1, v25

    if-ne v0, v1, :cond_26

    .line 939
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-eqz v28, :cond_25

    .line 940
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v21, v0

    .line 1002
    .end local v25    # "strength":I
    :cond_23
    :goto_12
    if-nez v21, :cond_2c

    .line 1003
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    if-nez v28, :cond_24

    .line 1004
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    new-instance v30, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    invoke-direct/range {v30 .. v30}, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;-><init>()V

    aput-object v30, v28, v29

    .line 1015
    :cond_24
    if-nez v26, :cond_2b

    .line 1016
    sget-object v28, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    move/from16 v32, v0

    add-int v31, v31, v32

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/ibm/icu/text/RuleBasedCollator;->getCollationElementIterator(Ljava/lang/String;)Lcom/ibm/icu/text/CollationElementIterator;

    move-result-object v9

    .line 1022
    .local v9, "coleiter":Lcom/ibm/icu/text/CollationElementIterator;
    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v3

    .line 1024
    .local v3, "CE":I
    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    move/from16 v29, v0

    add-int v10, v28, v29

    .line 1026
    .local v10, "expand":I
    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v4

    .line 1028
    .local v4, "SecondCE":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    and-int/lit16 v0, v3, -0xc1

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseCE_:I

    .line 1030
    invoke-static {v4}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v28

    if-eqz v28, :cond_2a

    .line 1031
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    move-object/from16 v0, v28

    iput v4, v0, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    .line 1037
    :goto_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextCE_:I

    .line 1038
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextContCE_:I

    .line 1039
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_previousCE_:I

    .line 1040
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_previousContCE_:I

    .line 1041
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput-boolean v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_indirect_:Z

    .line 1042
    new-instance v21, Lcom/ibm/icu/text/CollationRuleParser$Token;

    .end local v21    # "sourceToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    invoke-direct/range {v21 .. v21}, Lcom/ibm/icu/text/CollationRuleParser$Token;-><init>()V

    .line 1043
    .restart local v21    # "sourceToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v10, v1}, Lcom/ibm/icu/text/CollationRuleParser;->initAReset(ILcom/ibm/icu/text/CollationRuleParser$Token;)I

    move-result v11

    .line 1044
    goto/16 :goto_7

    .line 943
    .end local v3    # "CE":I
    .end local v4    # "SecondCE":I
    .end local v9    # "coleiter":Lcom/ibm/icu/text/CollationElementIterator;
    .end local v10    # "expand":I
    .restart local v25    # "strength":I
    :cond_25
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_reset_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v21, v0

    .line 946
    goto/16 :goto_12

    .line 948
    :cond_26
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_reset_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v21, v0

    .line 950
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/CollationRuleParser;->getVirginBefore(Lcom/ibm/icu/text/CollationRuleParser$Token;I)Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-result-object v21

    .line 953
    goto/16 :goto_12

    .line 955
    :cond_27
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/CollationRuleParser;->getVirginBefore(Lcom/ibm/icu/text/CollationRuleParser$Token;I)Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-result-object v21

    goto/16 :goto_12

    .line 961
    .end local v25    # "strength":I
    :cond_28
    const/16 v26, 0x0

    .line 962
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    new-instance v30, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    invoke-direct/range {v30 .. v30}, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;-><init>()V

    aput-object v30, v28, v29

    .line 963
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_previousCE_:I

    .line 964
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_previousContCE_:I

    .line 965
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x1

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput-boolean v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_indirect_:Z

    .line 969
    and-int/lit8 v28, v23, 0x3

    add-int/lit8 v25, v28, -0x1

    .line 970
    .restart local v25    # "strength":I
    sget-object v28, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-char v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_indirectIndex_:C

    move/from16 v29, v0

    aget-object v28, v28, v29

    move-object/from16 v0, v28

    iget v5, v0, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_startCE_:I

    .line 972
    .local v5, "baseCE":I
    sget-object v28, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-char v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_indirectIndex_:C

    move/from16 v29, v0

    aget-object v28, v28, v29

    move-object/from16 v0, v28

    iget v6, v0, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_startContCE_:I

    .line 974
    .local v6, "baseContCE":I
    const/16 v28, 0x2

    move/from16 v0, v28

    new-array v8, v0, [I

    .line 975
    .local v8, "ce":[I
    ushr-int/lit8 v28, v5, 0x18

    sget-object v29, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->PRIMARY_IMPLICIT_MIN_:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-lt v0, v1, :cond_29

    ushr-int/lit8 v28, v5, 0x18

    sget-object v29, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->PRIMARY_IMPLICIT_MAX_:I

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-gt v0, v1, :cond_29

    .line 977
    const/high16 v28, -0x10000

    and-int v28, v28, v5

    const/high16 v29, -0x10000

    and-int v29, v29, v6

    shr-int/lit8 v29, v29, 0x10

    or-int v16, v28, v29

    .line 978
    .local v16, "primary":I
    sget-object v28, Lcom/ibm/icu/text/RuleBasedCollator;->impCEGen_:Lcom/ibm/icu/impl/ImplicitCEGenerator;

    move-object/from16 v0, v28

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ImplicitCEGenerator;->getRawFromImplicit(I)I

    move-result v18

    .line 979
    .local v18, "raw":I
    sget-object v28, Lcom/ibm/icu/text/RuleBasedCollator;->impCEGen_:Lcom/ibm/icu/impl/ImplicitCEGenerator;

    add-int/lit8 v29, v18, -0x1

    invoke-virtual/range {v28 .. v29}, Lcom/ibm/icu/impl/ImplicitCEGenerator;->getImplicitFromRaw(I)I

    move-result v17

    .line 980
    .local v17, "primaryCE":I
    const/16 v28, 0x0

    const/high16 v29, -0x10000

    and-int v29, v29, v17

    move/from16 v0, v29

    or-int/lit16 v0, v0, 0x505

    move/from16 v29, v0

    aput v29, v8, v28

    .line 981
    const/16 v28, 0x1

    shl-int/lit8 v29, v17, 0x10

    const/high16 v30, -0x10000

    and-int v29, v29, v30

    move/from16 v0, v29

    or-int/lit16 v0, v0, 0xc0

    move/from16 v29, v0

    aput v29, v8, v28

    .line 988
    .end local v16    # "primary":I
    .end local v17    # "primaryCE":I
    .end local v18    # "raw":I
    :goto_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x0

    aget v29, v8, v29

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseCE_:I

    .line 989
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x1

    aget v29, v8, v29

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    .line 990
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextCE_:I

    .line 991
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextContCE_:I

    .line 993
    new-instance v21, Lcom/ibm/icu/text/CollationRuleParser$Token;

    .end local v21    # "sourceToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    invoke-direct/range {v21 .. v21}, Lcom/ibm/icu/text/CollationRuleParser$Token;-><init>()V

    .line 994
    .restart local v21    # "sourceToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v28

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/CollationRuleParser;->initAReset(ILcom/ibm/icu/text/CollationRuleParser$Token;)I

    move-result v11

    goto/16 :goto_12

    .line 983
    :cond_29
    sget-object v13, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->INVERSE_UCA_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;

    .line 985
    .local v13, "invuca":Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;
    move/from16 v0, v25

    invoke-virtual {v13, v5, v6, v0, v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->getInversePrevCE(III[I)I

    goto :goto_14

    .line 1035
    .end local v5    # "baseCE":I
    .end local v6    # "baseContCE":I
    .end local v8    # "ce":[I
    .end local v13    # "invuca":Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;
    .end local v25    # "strength":I
    .restart local v3    # "CE":I
    .restart local v4    # "SecondCE":I
    .restart local v9    # "coleiter":Lcom/ibm/icu/text/CollationElementIterator;
    .restart local v10    # "expand":I
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    goto/16 :goto_13

    .line 1046
    .end local v3    # "CE":I
    .end local v4    # "SecondCE":I
    .end local v9    # "coleiter":Lcom/ibm/icu/text/CollationElementIterator;
    .end local v10    # "expand":I
    :cond_2b
    const/16 v26, 0x0

    .line 1047
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_previousCE_:I

    .line 1048
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_previousContCE_:I

    .line 1049
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    const/16 v29, 0x1

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput-boolean v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_indirect_:Z

    .line 1050
    sget-object v28, Lcom/ibm/icu/text/CollationRuleParser;->INDIRECT_BOUNDARIES_:[Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-char v0, v0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_indirectIndex_:C

    move/from16 v29, v0

    aget-object v12, v28, v29

    .line 1052
    .local v12, "ib":Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    iget v0, v12, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_startCE_:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseCE_:I

    .line 1054
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    iget v0, v12, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_startContCE_:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    .line 1056
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    iget v0, v12, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_limitCE_:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextCE_:I

    .line 1058
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    aget-object v28, v28, v29

    iget v0, v12, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_limitContCE_:I

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextContCE_:I

    .line 1060
    new-instance v21, Lcom/ibm/icu/text/CollationRuleParser$Token;

    .end local v21    # "sourceToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    invoke-direct/range {v21 .. v21}, Lcom/ibm/icu/text/CollationRuleParser$Token;-><init>()V

    .line 1061
    .restart local v21    # "sourceToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v28

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/CollationRuleParser;->initAReset(ILcom/ibm/icu/text/CollationRuleParser$Token;)I

    move-result v11

    .line 1063
    goto/16 :goto_7

    .line 1065
    .end local v12    # "ib":Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;
    :cond_2c
    const/16 v26, 0x0

    goto/16 :goto_7

    .line 1073
    .end local v14    # "lastStrength":I
    .end local v21    # "sourceToken":Lcom/ibm/icu/text/CollationRuleParser$Token;
    .end local v23    # "specs":C
    .end local v26    # "top":Z
    .end local v27    # "variableTop":Z
    :cond_2d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v28, v0

    if-lez v28, :cond_2e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_listHeader_:[Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v29, v0

    add-int/lit8 v29, v29, -0x1

    aget-object v28, v28, v29

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    move-object/from16 v28, v0

    if-nez v28, :cond_2e

    .line 1075
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v28, v0

    add-int/lit8 v28, v28, -0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    .line 1077
    :cond_2e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser;->m_resultLength_:I

    move/from16 v28, v0

    return v28
.end method

.method getTailoredSet()Lcom/ibm/icu/text/UnicodeSet;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 2005
    const/4 v3, 0x1

    .line 2006
    .local v3, "startOfRules":Z
    new-instance v4, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v4}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 2008
    .local v4, "tailored":Lcom/ibm/icu/text/UnicodeSet;
    new-instance v0, Lcom/ibm/icu/text/CanonicalIterator;

    const-string/jumbo v5, ""

    invoke-direct {v0, v5}, Lcom/ibm/icu/text/CanonicalIterator;-><init>(Ljava/lang/String;)V

    .line 2010
    .local v0, "it":Lcom/ibm/icu/text/CanonicalIterator;
    iget-object v5, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    const/4 v6, -0x1

    iput v6, v5, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_strength_:I

    .line 2011
    iget-object v5, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    .line 2014
    .local v2, "sourcelimit":I
    :cond_0
    iget v5, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_current_:I

    if-ge v5, v2, :cond_2

    .line 2015
    iget-object v5, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iput v9, v5, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_prefixOffset_:I

    .line 2016
    invoke-direct {p0, v3}, Lcom/ibm/icu/text/CollationRuleParser;->parseNextToken(Z)I

    move-result v5

    if-ltz v5, :cond_0

    .line 2020
    const/4 v3, 0x0

    .line 2023
    iget-object v5, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v5, v5, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_strength_:I

    const v6, -0x21524111

    if-eq v5, v6, :cond_0

    .line 2024
    iget-object v5, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_source_:Ljava/lang/StringBuffer;

    iget-object v6, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v6, v6, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    iget-object v7, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v7, v7, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_parsedToken_:Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;

    iget v8, v8, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    add-int/2addr v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/ibm/icu/text/CanonicalIterator;->setSource(Ljava/lang/String;)V

    .line 2027
    invoke-virtual {v0}, Lcom/ibm/icu/text/CanonicalIterator;->next()Ljava/lang/String;

    move-result-object v1

    .line 2028
    .local v1, "pattern":Ljava/lang/String;
    :goto_0
    if-eqz v1, :cond_0

    .line 2029
    sget-object v5, Lcom/ibm/icu/text/Normalizer;->FCD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {v1, v5, v9}, Lcom/ibm/icu/text/Normalizer;->quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v5

    sget-object v6, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-eq v5, v6, :cond_1

    .line 2030
    invoke-virtual {v4, v1}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    .line 2032
    :cond_1
    invoke-virtual {v0}, Lcom/ibm/icu/text/CanonicalIterator;->next()Ljava/lang/String;

    move-result-object v1

    .line 2033
    goto :goto_0

    .line 2036
    .end local v1    # "pattern":Ljava/lang/String;
    :cond_2
    return-object v4
.end method

.method setDefaultOptionsInCollator(Lcom/ibm/icu/text/RuleBasedCollator;)V
    .locals 1
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;

    .prologue
    .line 271
    iget-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_strength_:I

    iput v0, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultStrength_:I

    .line 272
    iget-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_decomposition_:I

    iput v0, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultDecomposition_:I

    .line 273
    iget-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    iget-boolean v0, v0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isFrenchCollation_:Z

    iput-boolean v0, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsFrenchCollation_:Z

    .line 274
    iget-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    iget-boolean v0, v0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isAlternateHandlingShifted_:Z

    iput-boolean v0, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsAlternateHandlingShifted_:Z

    .line 276
    iget-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    iget-boolean v0, v0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isCaseLevel_:Z

    iput-boolean v0, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsCaseLevel_:Z

    .line 277
    iget-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_caseFirst_:I

    iput v0, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultCaseFirst_:I

    .line 278
    iget-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    iget-boolean v0, v0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isHiragana4_:Z

    iput-boolean v0, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsHiragana4_:Z

    .line 279
    iget-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    iget v0, v0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_variableTopValue_:I

    iput v0, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultVariableTopValue_:I

    .line 280
    return-void
.end method
