.class public abstract Lcom/ibm/icu/text/UnicodeSet$XSymbolTable;
.super Ljava/lang/Object;
.source "UnicodeSet.java"

# interfaces
.implements Lcom/ibm/icu/text/SymbolTable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/UnicodeSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "XSymbolTable"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public applyPropertyAlias(Ljava/lang/String;Ljava/lang/String;Lcom/ibm/icu/text/UnicodeSet;)Z
    .locals 1
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "propertyValue"    # Ljava/lang/String;
    .param p3, "result"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 3697
    const/4 v0, 0x0

    return v0
.end method

.method public lookup(Ljava/lang/String;)[C
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 3705
    const/4 v0, 0x0

    return-object v0
.end method

.method public lookupMatcher(I)Lcom/ibm/icu/text/UnicodeMatcher;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 3685
    const/4 v0, 0x0

    return-object v0
.end method

.method public parseReference(Ljava/lang/String;Ljava/text/ParsePosition;I)Ljava/lang/String;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;
    .param p3, "limit"    # I

    .prologue
    .line 3713
    const/4 v0, 0x0

    return-object v0
.end method
