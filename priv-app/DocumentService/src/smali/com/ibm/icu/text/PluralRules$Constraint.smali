.class interface abstract Lcom/ibm/icu/text/PluralRules$Constraint;
.super Ljava/lang/Object;
.source "PluralRules.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/PluralRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "Constraint"
.end annotation


# virtual methods
.method public abstract isFulfilled(D)Z
.end method

.method public abstract updateRepeatLimit(I)I
.end method
