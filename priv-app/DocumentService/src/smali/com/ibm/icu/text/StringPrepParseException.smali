.class public Lcom/ibm/icu/text/StringPrepParseException;
.super Ljava/text/ParseException;
.source "StringPrepParseException.java"


# static fields
.field public static final ACE_PREFIX_ERROR:I = 0x6

.field public static final BUFFER_OVERFLOW_ERROR:I = 0x9

.field public static final CHECK_BIDI_ERROR:I = 0x4

.field public static final DOMAIN_NAME_TOO_LONG_ERROR:I = 0xb

.field public static final ILLEGAL_CHAR_FOUND:I = 0x1

.field public static final INVALID_CHAR_FOUND:I = 0x0

.field public static final LABEL_TOO_LONG_ERROR:I = 0x8

.field private static final PARSE_CONTEXT_LEN:I = 0x10

.field public static final PROHIBITED_ERROR:I = 0x2

.field public static final STD3_ASCII_RULES_ERROR:I = 0x5

.field public static final UNASSIGNED_ERROR:I = 0x3

.field public static final VERIFICATION_ERROR:I = 0x7

.field public static final ZERO_LENGTH_LABEL:I = 0xa

.field static final serialVersionUID:J = 0x635e5eec90e16737L


# instance fields
.field private error:I

.field private line:I

.field private postContext:Ljava/lang/StringBuffer;

.field private preContext:Ljava/lang/StringBuffer;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "error"    # I

    .prologue
    .line 82
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    .line 176
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/StringPrepParseException;->preContext:Ljava/lang/StringBuffer;

    .line 183
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/StringPrepParseException;->postContext:Ljava/lang/StringBuffer;

    .line 83
    iput p2, p0, Lcom/ibm/icu/text/StringPrepParseException;->error:I

    .line 84
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/StringPrepParseException;->line:I

    .line 85
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "error"    # I
    .param p3, "rules"    # Ljava/lang/String;
    .param p4, "pos"    # I

    .prologue
    .line 98
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    .line 176
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/StringPrepParseException;->preContext:Ljava/lang/StringBuffer;

    .line 183
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/StringPrepParseException;->postContext:Ljava/lang/StringBuffer;

    .line 99
    iput p2, p0, Lcom/ibm/icu/text/StringPrepParseException;->error:I

    .line 100
    invoke-direct {p0, p3, p4}, Lcom/ibm/icu/text/StringPrepParseException;->setContext(Ljava/lang/String;I)V

    .line 101
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/StringPrepParseException;->line:I

    .line 102
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;II)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "error"    # I
    .param p3, "rules"    # Ljava/lang/String;
    .param p4, "pos"    # I
    .param p5, "lineNumber"    # I

    .prologue
    .line 118
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    .line 176
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/StringPrepParseException;->preContext:Ljava/lang/StringBuffer;

    .line 183
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/StringPrepParseException;->postContext:Ljava/lang/StringBuffer;

    .line 119
    iput p2, p0, Lcom/ibm/icu/text/StringPrepParseException;->error:I

    .line 120
    invoke-direct {p0, p3, p4}, Lcom/ibm/icu/text/StringPrepParseException;->setContext(Ljava/lang/String;I)V

    .line 121
    iput p5, p0, Lcom/ibm/icu/text/StringPrepParseException;->line:I

    .line 122
    return-void
.end method

.method private setContext(Ljava/lang/String;I)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "pos"    # I

    .prologue
    .line 210
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/StringPrepParseException;->setPreContext(Ljava/lang/String;I)V

    .line 211
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/StringPrepParseException;->setPostContext(Ljava/lang/String;I)V

    .line 212
    return-void
.end method

.method private setPostContext(Ljava/lang/String;I)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "pos"    # I

    .prologue
    .line 199
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/ibm/icu/text/StringPrepParseException;->setPostContext([CI)V

    .line 200
    return-void
.end method

.method private setPostContext([CI)V
    .locals 3
    .param p1, "str"    # [C
    .param p2, "pos"    # I

    .prologue
    .line 203
    move v1, p2

    .line 204
    .local v1, "start":I
    array-length v2, p1

    sub-int v0, v2, v1

    .line 205
    .local v0, "len":I
    iget-object v2, p0, Lcom/ibm/icu/text/StringPrepParseException;->postContext:Ljava/lang/StringBuffer;

    invoke-virtual {v2, p1, v1, v0}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 207
    return-void
.end method

.method private setPreContext(Ljava/lang/String;I)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "pos"    # I

    .prologue
    .line 188
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/ibm/icu/text/StringPrepParseException;->setPreContext([CI)V

    .line 189
    return-void
.end method

.method private setPreContext([CI)V
    .locals 3
    .param p1, "str"    # [C
    .param p2, "pos"    # I

    .prologue
    const/16 v0, 0x10

    .line 192
    if-gt p2, v0, :cond_1

    const/4 v1, 0x0

    .line 193
    .local v1, "start":I
    :goto_0
    if-gt v1, v0, :cond_0

    move v0, v1

    .line 194
    .local v0, "len":I
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/StringPrepParseException;->preContext:Ljava/lang/StringBuffer;

    invoke-virtual {v2, p1, v1, v0}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 196
    return-void

    .line 192
    .end local v0    # "len":I
    .end local v1    # "start":I
    :cond_1
    add-int/lit8 v1, p2, -0xf

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 133
    instance-of v1, p1, Lcom/ibm/icu/text/StringPrepParseException;

    if-nez v1, :cond_1

    .line 136
    .end local p1    # "other":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .restart local p1    # "other":Ljava/lang/Object;
    :cond_1
    check-cast p1, Lcom/ibm/icu/text/StringPrepParseException;

    .end local p1    # "other":Ljava/lang/Object;
    iget v1, p1, Lcom/ibm/icu/text/StringPrepParseException;->error:I

    iget v2, p0, Lcom/ibm/icu/text/StringPrepParseException;->error:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getError()I
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/ibm/icu/text/StringPrepParseException;->error:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 146
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 147
    .local v0, "buf":Ljava/lang/StringBuffer;
    invoke-super {p0}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 148
    const-string/jumbo v1, ". line:  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 149
    iget v1, p0, Lcom/ibm/icu/text/StringPrepParseException;->line:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 150
    const-string/jumbo v1, ". preContext:  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 151
    iget-object v1, p0, Lcom/ibm/icu/text/StringPrepParseException;->preContext:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 152
    const-string/jumbo v1, ". postContext: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 153
    iget-object v1, p0, Lcom/ibm/icu/text/StringPrepParseException;->postContext:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 154
    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 155
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
