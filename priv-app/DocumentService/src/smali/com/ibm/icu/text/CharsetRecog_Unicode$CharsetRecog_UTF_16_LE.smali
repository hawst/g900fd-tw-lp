.class Lcom/ibm/icu/text/CharsetRecog_Unicode$CharsetRecog_UTF_16_LE;
.super Lcom/ibm/icu/text/CharsetRecog_Unicode;
.source "CharsetRecog_Unicode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CharsetRecog_Unicode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CharsetRecog_UTF_16_LE"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/ibm/icu/text/CharsetRecog_Unicode;-><init>()V

    return-void
.end method


# virtual methods
.method getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string/jumbo v0, "UTF-16LE"

    return-object v0
.end method

.method match(Lcom/ibm/icu/text/CharsetDetector;)I
    .locals 5
    .param p1, "det"    # Lcom/ibm/icu/text/CharsetDetector;

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 58
    iget-object v0, p1, Lcom/ibm/icu/text/CharsetDetector;->fRawInput:[B

    .line 60
    .local v0, "input":[B
    array-length v2, v0

    if-lt v2, v4, :cond_0

    aget-byte v2, v0, v1

    and-int/lit16 v2, v2, 0xff

    const/16 v3, 0xff

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    const/16 v3, 0xfe

    if-ne v2, v3, :cond_0

    .line 63
    array-length v2, v0

    const/4 v3, 0x4

    if-lt v2, v3, :cond_1

    aget-byte v2, v0, v4

    if-nez v2, :cond_1

    const/4 v2, 0x3

    aget-byte v2, v0, v2

    if-nez v2, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v1

    .line 67
    :cond_1
    const/16 v1, 0x64

    goto :goto_0
.end method
