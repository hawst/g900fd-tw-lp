.class public Lcom/ibm/icu/text/DecimalFormat;
.super Lcom/ibm/icu/text/NumberFormat;
.source "DecimalFormat.java"


# static fields
.field private static final CURRENCY_SIGN:C = '\u00a4'

.field static final DOUBLE_FRACTION_DIGITS:I = 0x154

.field static final DOUBLE_INTEGER_DIGITS:I = 0x135

.field static final MAX_SCIENTIFIC_INTEGER_DIGITS:I = 0x8

.field public static final PAD_AFTER_PREFIX:I = 0x1

.field public static final PAD_AFTER_SUFFIX:I = 0x3

.field public static final PAD_BEFORE_PREFIX:I = 0x0

.field public static final PAD_BEFORE_SUFFIX:I = 0x2

.field private static final PARSE_MAX_EXPONENT:I = 0x3e8

.field private static final PATTERN_DECIMAL_SEPARATOR:C = '.'

.field private static final PATTERN_DIGIT:C = '#'

.field static final PATTERN_EXPONENT:C = 'E'

.field private static final PATTERN_GROUPING_SEPARATOR:C = ','

.field private static final PATTERN_MINUS:C = '-'

.field static final PATTERN_PAD_ESCAPE:C = '*'

.field private static final PATTERN_PERCENT:C = '%'

.field private static final PATTERN_PER_MILLE:C = '\u2030'

.field static final PATTERN_PLUS_SIGN:C = '+'

.field private static final PATTERN_SEPARATOR:C = ';'

.field static final PATTERN_SIGNIFICANT_DIGIT:C = '@'

.field private static final PATTERN_ZERO_DIGIT:C = '0'

.field private static final QUOTE:C = '\''

.field private static final STATUS_INFINITE:I = 0x0

.field private static final STATUS_LENGTH:I = 0x3

.field private static final STATUS_POSITIVE:I = 0x1

.field private static final STATUS_UNDERFLOW:I = 0x2

.field private static final commaEquivalents:Lcom/ibm/icu/text/UnicodeSet;

.field static final currentSerialVersion:I = 0x3

.field private static final defaultGroupingSeparators:Lcom/ibm/icu/text/UnicodeSet;

.field private static final dotEquivalents:Lcom/ibm/icu/text/UnicodeSet;

.field private static epsilon:D = 0.0

.field private static final otherGroupingSeparators:Lcom/ibm/icu/text/UnicodeSet;

.field static final roundingIncrementEpsilon:D = 1.0E-9

.field private static final serialVersionUID:J = 0xbff0362d872303aL

.field private static final strictCommaEquivalents:Lcom/ibm/icu/text/UnicodeSet;

.field private static final strictDefaultGroupingSeparators:Lcom/ibm/icu/text/UnicodeSet;

.field private static final strictDotEquivalents:Lcom/ibm/icu/text/UnicodeSet;

.field private static final strictOtherGroupingSeparators:Lcom/ibm/icu/text/UnicodeSet;


# instance fields
.field private attributes:Ljava/util/ArrayList;

.field private currencyChoice:Ljava/text/ChoiceFormat;

.field private decimalSeparatorAlwaysShown:Z

.field private transient digitList:Lcom/ibm/icu/text/DigitList;

.field private exponentSignAlwaysShown:Z

.field private formatWidth:I

.field private groupingSize:B

.field private groupingSize2:B

.field private transient isCurrencyFormat:Z

.field private maxSignificantDigits:I

.field private minExponentDigits:B

.field private minSignificantDigits:I

.field private multiplier:I

.field private negPrefixPattern:Ljava/lang/String;

.field private negSuffixPattern:Ljava/lang/String;

.field private negativePrefix:Ljava/lang/String;

.field private negativeSuffix:Ljava/lang/String;

.field private pad:C

.field private padPosition:I

.field private parseBigDecimal:Z

.field private posPrefixPattern:Ljava/lang/String;

.field private posSuffixPattern:Ljava/lang/String;

.field private positivePrefix:Ljava/lang/String;

.field private positiveSuffix:Ljava/lang/String;

.field private transient roundingDouble:D

.field private transient roundingDoubleReciprocal:D

.field private roundingIncrement:Ljava/math/BigDecimal;

.field private transient roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

.field private roundingMode:I

.field private serialVersionOnStream:I

.field private symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

.field private useExponentialNotation:Z

.field private useSignificantDigits:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 882
    const-wide v0, 0x3da5fd7fe1796495L    # 1.0E-11

    sput-wide v0, Lcom/ibm/icu/text/DecimalFormat;->epsilon:D

    .line 1724
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v1, "[.\u2024\u3002\ufe12\ufe52\uff0e\uff61]"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->freeze()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    sput-object v0, Lcom/ibm/icu/text/DecimalFormat;->dotEquivalents:Lcom/ibm/icu/text/UnicodeSet;

    .line 1726
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v1, "[,\u060c\u066b\u3001\ufe10\ufe11\ufe50\ufe51\uff0c\uff64]"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->freeze()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    sput-object v0, Lcom/ibm/icu/text/DecimalFormat;->commaEquivalents:Lcom/ibm/icu/text/UnicodeSet;

    .line 1728
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v1, "[\\ \'\u00a0\u066c\u2000-\u200a\u2018\u2019\u202f\u205f\u3000\uff07]"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->freeze()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    sput-object v0, Lcom/ibm/icu/text/DecimalFormat;->otherGroupingSeparators:Lcom/ibm/icu/text/UnicodeSet;

    .line 1731
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v1, "[.\u2024\ufe52\uff0e\uff61]"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->freeze()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    sput-object v0, Lcom/ibm/icu/text/DecimalFormat;->strictDotEquivalents:Lcom/ibm/icu/text/UnicodeSet;

    .line 1733
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v1, "[,\u066b\ufe10\ufe50\uff0c]"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->freeze()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    sput-object v0, Lcom/ibm/icu/text/DecimalFormat;->strictCommaEquivalents:Lcom/ibm/icu/text/UnicodeSet;

    .line 1735
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v1, "[\\ \'\u00a0\u066c\u2000-\u200a\u2018\u2019\u202f\u205f\u3000\uff07]"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->freeze()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    sput-object v0, Lcom/ibm/icu/text/DecimalFormat;->strictOtherGroupingSeparators:Lcom/ibm/icu/text/UnicodeSet;

    .line 1738
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    sget-object v1, Lcom/ibm/icu/text/DecimalFormat;->dotEquivalents:Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Lcom/ibm/icu/text/UnicodeSet;)V

    sget-object v1, Lcom/ibm/icu/text/DecimalFormat;->commaEquivalents:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    sget-object v1, Lcom/ibm/icu/text/DecimalFormat;->otherGroupingSeparators:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->freeze()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    sput-object v0, Lcom/ibm/icu/text/DecimalFormat;->defaultGroupingSeparators:Lcom/ibm/icu/text/UnicodeSet;

    .line 1740
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    sget-object v1, Lcom/ibm/icu/text/DecimalFormat;->strictDotEquivalents:Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Lcom/ibm/icu/text/UnicodeSet;)V

    sget-object v1, Lcom/ibm/icu/text/DecimalFormat;->strictCommaEquivalents:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    sget-object v1, Lcom/ibm/icu/text/DecimalFormat;->strictOtherGroupingSeparators:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->freeze()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    sput-object v0, Lcom/ibm/icu/text/DecimalFormat;->strictDefaultGroupingSeparators:Lcom/ibm/icu/text/UnicodeSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v5, 0x1

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 625
    invoke-direct {p0}, Lcom/ibm/icu/text/NumberFormat;-><init>()V

    .line 4577
    new-instance v2, Lcom/ibm/icu/text/DigitList;

    invoke-direct {v2}, Lcom/ibm/icu/text/DigitList;-><init>()V

    iput-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    .line 4585
    const-string/jumbo v2, ""

    iput-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->positivePrefix:Ljava/lang/String;

    .line 4594
    const-string/jumbo v2, ""

    iput-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->positiveSuffix:Ljava/lang/String;

    .line 4602
    const-string/jumbo v2, "-"

    iput-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->negativePrefix:Ljava/lang/String;

    .line 4611
    const-string/jumbo v2, ""

    iput-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->negativeSuffix:Ljava/lang/String;

    .line 4676
    iput v5, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    .line 4687
    iput-byte v8, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    .line 4697
    iput-byte v3, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    .line 4706
    iput-boolean v3, p0, Lcom/ibm/icu/text/DecimalFormat;->decimalSeparatorAlwaysShown:Z

    .line 4712
    iput-boolean v3, p0, Lcom/ibm/icu/text/DecimalFormat;->isCurrencyFormat:Z

    .line 4723
    iput-object v4, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 4731
    iput-boolean v3, p0, Lcom/ibm/icu/text/DecimalFormat;->useSignificantDigits:Z

    .line 4740
    iput v5, p0, Lcom/ibm/icu/text/DecimalFormat;->minSignificantDigits:I

    .line 4749
    const/4 v2, 0x6

    iput v2, p0, Lcom/ibm/icu/text/DecimalFormat;->maxSignificantDigits:I

    .line 4785
    iput-boolean v3, p0, Lcom/ibm/icu/text/DecimalFormat;->exponentSignAlwaysShown:Z

    .line 4801
    iput-object v4, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrement:Ljava/math/BigDecimal;

    .line 4815
    iput-object v4, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    .line 4823
    iput-wide v6, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDouble:D

    .line 4830
    iput-wide v6, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDoubleReciprocal:D

    .line 4842
    const/4 v2, 0x6

    iput v2, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingMode:I

    .line 4851
    iput v3, p0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    .line 4860
    const/16 v2, 0x20

    iput-char v2, p0, Lcom/ibm/icu/text/DecimalFormat;->pad:C

    .line 4871
    iput v3, p0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    .line 4882
    iput-boolean v3, p0, Lcom/ibm/icu/text/DecimalFormat;->parseBigDecimal:Z

    .line 4900
    iput v8, p0, Lcom/ibm/icu/text/DecimalFormat;->serialVersionOnStream:I

    .line 5027
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->attributes:Ljava/util/ArrayList;

    .line 627
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    .line 628
    .local v0, "def":Lcom/ibm/icu/util/ULocale;
    invoke-static {v0, v3}, Lcom/ibm/icu/text/DecimalFormat;->getPattern(Lcom/ibm/icu/util/ULocale;I)Ljava/lang/String;

    move-result-object v1

    .line 630
    .local v1, "pattern":Ljava/lang/String;
    new-instance v2, Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-direct {v2, v0}, Lcom/ibm/icu/text/DecimalFormatSymbols;-><init>(Lcom/ibm/icu/util/ULocale;)V

    iput-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 631
    invoke-static {v0}, Lcom/ibm/icu/util/Currency;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/Currency;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/DecimalFormat;->setCurrency(Lcom/ibm/icu/util/Currency;)V

    .line 632
    invoke-direct {p0, v1, v3}, Lcom/ibm/icu/text/DecimalFormat;->applyPattern(Ljava/lang/String;Z)V

    .line 633
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 8
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 653
    invoke-direct {p0}, Lcom/ibm/icu/text/NumberFormat;-><init>()V

    .line 4577
    new-instance v1, Lcom/ibm/icu/text/DigitList;

    invoke-direct {v1}, Lcom/ibm/icu/text/DigitList;-><init>()V

    iput-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    .line 4585
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->positivePrefix:Ljava/lang/String;

    .line 4594
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->positiveSuffix:Ljava/lang/String;

    .line 4602
    const-string/jumbo v1, "-"

    iput-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->negativePrefix:Ljava/lang/String;

    .line 4611
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->negativeSuffix:Ljava/lang/String;

    .line 4676
    iput v6, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    .line 4687
    iput-byte v7, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    .line 4697
    iput-byte v2, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    .line 4706
    iput-boolean v2, p0, Lcom/ibm/icu/text/DecimalFormat;->decimalSeparatorAlwaysShown:Z

    .line 4712
    iput-boolean v2, p0, Lcom/ibm/icu/text/DecimalFormat;->isCurrencyFormat:Z

    .line 4723
    iput-object v3, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 4731
    iput-boolean v2, p0, Lcom/ibm/icu/text/DecimalFormat;->useSignificantDigits:Z

    .line 4740
    iput v6, p0, Lcom/ibm/icu/text/DecimalFormat;->minSignificantDigits:I

    .line 4749
    const/4 v1, 0x6

    iput v1, p0, Lcom/ibm/icu/text/DecimalFormat;->maxSignificantDigits:I

    .line 4785
    iput-boolean v2, p0, Lcom/ibm/icu/text/DecimalFormat;->exponentSignAlwaysShown:Z

    .line 4801
    iput-object v3, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrement:Ljava/math/BigDecimal;

    .line 4815
    iput-object v3, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    .line 4823
    iput-wide v4, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDouble:D

    .line 4830
    iput-wide v4, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDoubleReciprocal:D

    .line 4842
    const/4 v1, 0x6

    iput v1, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingMode:I

    .line 4851
    iput v2, p0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    .line 4860
    const/16 v1, 0x20

    iput-char v1, p0, Lcom/ibm/icu/text/DecimalFormat;->pad:C

    .line 4871
    iput v2, p0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    .line 4882
    iput-boolean v2, p0, Lcom/ibm/icu/text/DecimalFormat;->parseBigDecimal:Z

    .line 4900
    iput v7, p0, Lcom/ibm/icu/text/DecimalFormat;->serialVersionOnStream:I

    .line 5027
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->attributes:Ljava/util/ArrayList;

    .line 655
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    .line 656
    .local v0, "def":Lcom/ibm/icu/util/ULocale;
    new-instance v1, Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-direct {v1, v0}, Lcom/ibm/icu/text/DecimalFormatSymbols;-><init>(Lcom/ibm/icu/util/ULocale;)V

    iput-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 657
    invoke-static {v0}, Lcom/ibm/icu/util/Currency;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/Currency;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setCurrency(Lcom/ibm/icu/util/Currency;)V

    .line 658
    invoke-direct {p0, p1, v2}, Lcom/ibm/icu/text/DecimalFormat;->applyPattern(Ljava/lang/String;Z)V

    .line 659
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/ibm/icu/text/DecimalFormatSymbols;)V
    .locals 7
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "symbols"    # Lcom/ibm/icu/text/DecimalFormatSymbols;

    .prologue
    const/4 v6, 0x3

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 682
    invoke-direct {p0}, Lcom/ibm/icu/text/NumberFormat;-><init>()V

    .line 4577
    new-instance v0, Lcom/ibm/icu/text/DigitList;

    invoke-direct {v0}, Lcom/ibm/icu/text/DigitList;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    .line 4585
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->positivePrefix:Ljava/lang/String;

    .line 4594
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->positiveSuffix:Ljava/lang/String;

    .line 4602
    const-string/jumbo v0, "-"

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->negativePrefix:Ljava/lang/String;

    .line 4611
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->negativeSuffix:Ljava/lang/String;

    .line 4676
    iput v3, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    .line 4687
    iput-byte v6, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    .line 4697
    iput-byte v1, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    .line 4706
    iput-boolean v1, p0, Lcom/ibm/icu/text/DecimalFormat;->decimalSeparatorAlwaysShown:Z

    .line 4712
    iput-boolean v1, p0, Lcom/ibm/icu/text/DecimalFormat;->isCurrencyFormat:Z

    .line 4723
    iput-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 4731
    iput-boolean v1, p0, Lcom/ibm/icu/text/DecimalFormat;->useSignificantDigits:Z

    .line 4740
    iput v3, p0, Lcom/ibm/icu/text/DecimalFormat;->minSignificantDigits:I

    .line 4749
    const/4 v0, 0x6

    iput v0, p0, Lcom/ibm/icu/text/DecimalFormat;->maxSignificantDigits:I

    .line 4785
    iput-boolean v1, p0, Lcom/ibm/icu/text/DecimalFormat;->exponentSignAlwaysShown:Z

    .line 4801
    iput-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrement:Ljava/math/BigDecimal;

    .line 4815
    iput-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    .line 4823
    iput-wide v4, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDouble:D

    .line 4830
    iput-wide v4, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDoubleReciprocal:D

    .line 4842
    const/4 v0, 0x6

    iput v0, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingMode:I

    .line 4851
    iput v1, p0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    .line 4860
    const/16 v0, 0x20

    iput-char v0, p0, Lcom/ibm/icu/text/DecimalFormat;->pad:C

    .line 4871
    iput v1, p0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    .line 4882
    iput-boolean v1, p0, Lcom/ibm/icu/text/DecimalFormat;->parseBigDecimal:Z

    .line 4900
    iput v6, p0, Lcom/ibm/icu/text/DecimalFormat;->serialVersionOnStream:I

    .line 5027
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->attributes:Ljava/util/ArrayList;

    .line 684
    invoke-virtual {p2}, Lcom/ibm/icu/text/DecimalFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/DecimalFormatSymbols;

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 685
    invoke-direct {p0}, Lcom/ibm/icu/text/DecimalFormat;->setCurrencyForSymbols()V

    .line 686
    invoke-direct {p0, p1, v1}, Lcom/ibm/icu/text/DecimalFormat;->applyPattern(Ljava/lang/String;Z)V

    .line 687
    return-void
.end method

.method private addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V
    .locals 2
    .param p1, "field"    # Lcom/ibm/icu/text/NumberFormat$Field;
    .param p2, "begin"    # I
    .param p3, "end"    # I

    .prologue
    .line 3440
    new-instance v0, Ljava/text/FieldPosition;

    invoke-direct {v0, p1}, Ljava/text/FieldPosition;-><init>(Ljava/text/Format$Field;)V

    .line 3441
    .local v0, "pos":Ljava/text/FieldPosition;
    invoke-virtual {v0, p2}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 3442
    invoke-virtual {v0, p3}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 3443
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->attributes:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3444
    return-void
.end method

.method private final addPadding(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;II)V
    .locals 5
    .param p1, "result"    # Ljava/lang/StringBuffer;
    .param p2, "fieldPosition"    # Ljava/text/FieldPosition;
    .param p3, "prefixLen"    # I
    .param p4, "suffixLen"    # I

    .prologue
    .line 1530
    iget v3, p0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    if-lez v3, :cond_2

    .line 1531
    iget v3, p0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    sub-int v1, v3, v4

    .line 1532
    .local v1, "len":I
    if-lez v1, :cond_2

    .line 1533
    new-array v2, v1, [C

    .line 1534
    .local v2, "padding":[C
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 1535
    iget-char v3, p0, Lcom/ibm/icu/text/DecimalFormat;->pad:C

    aput-char v3, v2, v0

    .line 1534
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1537
    :cond_0
    iget v3, p0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    packed-switch v3, :pswitch_data_0

    .line 1551
    :goto_1
    iget v3, p0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 1553
    :cond_1
    invoke-virtual {p2}, Ljava/text/FieldPosition;->getBeginIndex()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p2, v3}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 1554
    invoke-virtual {p2}, Ljava/text/FieldPosition;->getEndIndex()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p2, v3}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 1558
    .end local v0    # "i":I
    .end local v1    # "len":I
    .end local v2    # "padding":[C
    :cond_2
    return-void

    .line 1539
    .restart local v0    # "i":I
    .restart local v1    # "len":I
    .restart local v2    # "padding":[C
    :pswitch_0
    invoke-virtual {p1, p3, v2}, Ljava/lang/StringBuffer;->insert(I[C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1542
    :pswitch_1
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v2}, Ljava/lang/StringBuffer;->insert(I[C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1545
    :pswitch_2
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    sub-int/2addr v3, p4

    invoke-virtual {p1, v3, v2}, Ljava/lang/StringBuffer;->insert(I[C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1548
    :pswitch_3
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1537
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private appendAffix(Ljava/lang/StringBuffer;ZZZ)I
    .locals 7
    .param p1, "buf"    # Ljava/lang/StringBuffer;
    .param p2, "isNegative"    # Z
    .param p3, "isPrefix"    # Z
    .param p4, "parseAttr"    # Z

    .prologue
    const/4 v5, -0x1

    .line 3373
    iget-object v4, p0, Lcom/ibm/icu/text/DecimalFormat;->currencyChoice:Ljava/text/ChoiceFormat;

    if-eqz v4, :cond_3

    .line 3374
    const/4 v2, 0x0

    .line 3375
    .local v2, "affixPat":Ljava/lang/String;
    if-eqz p3, :cond_1

    .line 3376
    if-eqz p2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->negPrefixPattern:Ljava/lang/String;

    .line 3380
    :goto_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 3381
    .local v1, "affixBuf":Ljava/lang/StringBuffer;
    const/4 v4, 0x1

    invoke-direct {p0, v2, v1, v4}, Lcom/ibm/icu/text/DecimalFormat;->expandAffix(Ljava/lang/String;Ljava/lang/StringBuffer;Z)V

    .line 3382
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3383
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    .line 3408
    .end local v1    # "affixBuf":Ljava/lang/StringBuffer;
    .end local v2    # "affixPat":Ljava/lang/String;
    :goto_1
    return v4

    .line 3376
    .restart local v2    # "affixPat":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->posPrefixPattern:Ljava/lang/String;

    goto :goto_0

    .line 3378
    :cond_1
    if-eqz p2, :cond_2

    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->negSuffixPattern:Ljava/lang/String;

    :goto_2
    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->posSuffixPattern:Ljava/lang/String;

    goto :goto_2

    .line 3386
    .end local v2    # "affixPat":Ljava/lang/String;
    :cond_3
    const/4 v0, 0x0

    .line 3387
    .local v0, "affix":Ljava/lang/String;
    if-eqz p3, :cond_7

    .line 3388
    if-eqz p2, :cond_6

    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->negativePrefix:Ljava/lang/String;

    .line 3395
    :goto_3
    if-eqz p4, :cond_5

    .line 3396
    iget-object v4, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v4}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getCurrencySymbol()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 3397
    .local v3, "offset":I
    if-ne v5, v3, :cond_4

    .line 3398
    iget-object v4, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v4}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPercent()C

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 3399
    if-ne v5, v3, :cond_4

    .line 3400
    const/4 v3, 0x0

    .line 3403
    :cond_4
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {p0, v0, v4, v5}, Lcom/ibm/icu/text/DecimalFormat;->formatAffix2Attribute(Ljava/lang/String;II)V

    .line 3407
    .end local v3    # "offset":I
    :cond_5
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3408
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    goto :goto_1

    .line 3388
    :cond_6
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->positivePrefix:Ljava/lang/String;

    goto :goto_3

    .line 3390
    :cond_7
    if-eqz p2, :cond_8

    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->negativeSuffix:Ljava/lang/String;

    :goto_4
    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->positiveSuffix:Ljava/lang/String;

    goto :goto_4
.end method

.method private appendAffixPattern(Ljava/lang/StringBuffer;ZZZ)V
    .locals 8
    .param p1, "buffer"    # Ljava/lang/StringBuffer;
    .param p2, "isNegative"    # Z
    .param p3, "isPrefix"    # Z
    .param p4, "localized"    # Z

    .prologue
    const/16 v6, 0x27

    .line 3496
    const/4 v1, 0x0

    .line 3497
    .local v1, "affixPat":Ljava/lang/String;
    if-eqz p3, :cond_2

    .line 3498
    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->negPrefixPattern:Ljava/lang/String;

    .line 3504
    :goto_0
    if-nez v1, :cond_9

    .line 3505
    const/4 v0, 0x0

    .line 3506
    .local v0, "affix":Ljava/lang/String;
    if-eqz p3, :cond_5

    .line 3507
    if-eqz p2, :cond_4

    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->negativePrefix:Ljava/lang/String;

    .line 3512
    :goto_1
    invoke-virtual {p1, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3513
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_7

    .line 3514
    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 3515
    .local v2, "ch":C
    if-ne v2, v6, :cond_0

    .line 3516
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3518
    :cond_0
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3513
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 3498
    .end local v0    # "affix":Ljava/lang/String;
    .end local v2    # "ch":C
    .end local v3    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->posPrefixPattern:Ljava/lang/String;

    goto :goto_0

    .line 3500
    :cond_2
    if-eqz p2, :cond_3

    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->negSuffixPattern:Ljava/lang/String;

    :goto_3
    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->posSuffixPattern:Ljava/lang/String;

    goto :goto_3

    .line 3507
    .restart local v0    # "affix":Ljava/lang/String;
    :cond_4
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->positivePrefix:Ljava/lang/String;

    goto :goto_1

    .line 3509
    :cond_5
    if-eqz p2, :cond_6

    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->negativeSuffix:Ljava/lang/String;

    :goto_4
    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->positiveSuffix:Ljava/lang/String;

    goto :goto_4

    .line 3520
    .restart local v3    # "i":I
    :cond_7
    invoke-virtual {p1, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3560
    .end local v0    # "affix":Ljava/lang/String;
    .end local v3    # "i":I
    :cond_8
    :goto_5
    return-void

    .line 3524
    :cond_9
    if-nez p4, :cond_a

    .line 3525
    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_5

    .line 3528
    :cond_a
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_6
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_8

    .line 3529
    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 3530
    .restart local v2    # "ch":C
    sparse-switch v2, :sswitch_data_0

    .line 3550
    :goto_7
    iget-object v5, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v5}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v5

    if-eq v2, v5, :cond_b

    iget-object v5, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v5}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getGroupingSeparator()C

    move-result v5

    if-ne v2, v5, :cond_d

    .line 3552
    :cond_b
    invoke-virtual {p1, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3553
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3554
    invoke-virtual {p1, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3528
    :goto_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 3532
    :sswitch_0
    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v1, v6, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 3533
    .local v4, "j":I
    if-gez v4, :cond_c

    .line 3534
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "Malformed affix pattern: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 3536
    :cond_c
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {v1, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3537
    move v3, v4

    .line 3538
    goto :goto_8

    .line 3540
    .end local v4    # "j":I
    :sswitch_1
    iget-object v5, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v5}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPerMill()C

    move-result v2

    .line 3541
    goto :goto_7

    .line 3543
    :sswitch_2
    iget-object v5, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v5}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPercent()C

    move-result v2

    .line 3544
    goto :goto_7

    .line 3546
    :sswitch_3
    iget-object v5, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v5}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getMinusSign()C

    move-result v2

    goto :goto_7

    .line 3556
    :cond_d
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_8

    .line 3530
    nop

    :sswitch_data_0
    .sparse-switch
        0x25 -> :sswitch_2
        0x27 -> :sswitch_0
        0x2d -> :sswitch_3
        0x2030 -> :sswitch_1
    .end sparse-switch
.end method

.method private applyPattern(Ljava/lang/String;Z)V
    .locals 60
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "localized"    # Z

    .prologue
    .line 3770
    const/16 v55, 0x30

    .line 3771
    .local v55, "zeroDigit":C
    const/16 v46, 0x40

    .line 3772
    .local v46, "sigDigit":C
    const/16 v23, 0x2c

    .line 3773
    .local v23, "groupingSeparator":C
    const/16 v10, 0x2e

    .line 3774
    .local v10, "decimalSeparator":C
    const/16 v39, 0x25

    .line 3775
    .local v39, "percent":C
    const/16 v38, 0x2030

    .line 3776
    .local v38, "perMill":C
    const/16 v11, 0x23

    .line 3777
    .local v11, "digit":C
    const/16 v45, 0x3b

    .line 3778
    .local v45, "separator":C
    const/16 v57, 0x45

    invoke-static/range {v57 .. v57}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v19

    .line 3779
    .local v19, "exponent":Ljava/lang/String;
    const/16 v40, 0x2b

    .line 3780
    .local v40, "plus":C
    const/16 v35, 0x2a

    .line 3781
    .local v35, "padEscape":C
    const/16 v29, 0x2d

    .line 3782
    .local v29, "minus":C
    if-eqz p2, :cond_0

    .line 3783
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v57, v0

    invoke-virtual/range {v57 .. v57}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getZeroDigit()C

    move-result v55

    .line 3784
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v57, v0

    invoke-virtual/range {v57 .. v57}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getSignificantDigit()C

    move-result v46

    .line 3785
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v57, v0

    invoke-virtual/range {v57 .. v57}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getGroupingSeparator()C

    move-result v23

    .line 3786
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v57, v0

    invoke-virtual/range {v57 .. v57}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v10

    .line 3787
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v57, v0

    invoke-virtual/range {v57 .. v57}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPercent()C

    move-result v39

    .line 3788
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v57, v0

    invoke-virtual/range {v57 .. v57}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPerMill()C

    move-result v38

    .line 3789
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v57, v0

    invoke-virtual/range {v57 .. v57}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getDigit()C

    move-result v11

    .line 3790
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v57, v0

    invoke-virtual/range {v57 .. v57}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPatternSeparator()C

    move-result v45

    .line 3791
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v57, v0

    invoke-virtual/range {v57 .. v57}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getExponentSeparator()Ljava/lang/String;

    move-result-object v19

    .line 3792
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v57, v0

    invoke-virtual/range {v57 .. v57}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPlusSign()C

    move-result v40

    .line 3793
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v57, v0

    invoke-virtual/range {v57 .. v57}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPadEscape()C

    move-result v35

    .line 3794
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v57, v0

    invoke-virtual/range {v57 .. v57}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getMinusSign()C

    move-result v29

    .line 3796
    :cond_0
    add-int/lit8 v57, v55, 0x9

    move/from16 v0, v57

    int-to-char v0, v0

    move/from16 v32, v0

    .line 3798
    .local v32, "nineDigit":C
    const/16 v20, 0x0

    .line 3800
    .local v20, "gotNegative":Z
    const/16 v41, 0x0

    .line 3803
    .local v41, "pos":I
    const/16 v37, 0x0

    .local v37, "part":I
    :goto_0
    const/16 v57, 0x2

    move/from16 v0, v37

    move/from16 v1, v57

    if-ge v0, v1, :cond_59

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v57

    move/from16 v0, v41

    move/from16 v1, v57

    if-ge v0, v1, :cond_59

    .line 3809
    const/16 v52, 0x1

    .local v52, "subpart":I
    const/16 v50, 0x0

    .local v50, "sub0Start":I
    const/16 v49, 0x0

    .local v49, "sub0Limit":I
    const/16 v51, 0x0

    .line 3817
    .local v51, "sub2Limit":I
    new-instance v43, Ljava/lang/StringBuffer;

    invoke-direct/range {v43 .. v43}, Ljava/lang/StringBuffer;-><init>()V

    .line 3818
    .local v43, "prefix":Ljava/lang/StringBuffer;
    new-instance v53, Ljava/lang/StringBuffer;

    invoke-direct/range {v53 .. v53}, Ljava/lang/StringBuffer;-><init>()V

    .line 3819
    .local v53, "suffix":Ljava/lang/StringBuffer;
    const/4 v9, -0x1

    .line 3820
    .local v9, "decimalPos":I
    const/16 v30, 0x1

    .line 3821
    .local v30, "multpl":I
    const/4 v12, 0x0

    .local v12, "digitLeftCount":I
    const/16 v56, 0x0

    .local v56, "zeroDigitCount":I
    const/4 v13, 0x0

    .local v13, "digitRightCount":I
    const/16 v47, 0x0

    .line 3822
    .local v47, "sigDigitCount":I
    const/16 v21, -0x1

    .line 3823
    .local v21, "groupingCount":B
    const/16 v22, -0x1

    .line 3824
    .local v22, "groupingCount2":B
    const/16 v36, -0x1

    .line 3825
    .local v36, "padPos":I
    const/16 v34, 0x0

    .line 3826
    .local v34, "padChar":C
    const/16 v24, -0x1

    .line 3827
    .local v24, "incrementPos":I
    const-wide/16 v26, 0x0

    .line 3828
    .local v26, "incrementVal":J
    const/16 v17, -0x1

    .line 3829
    .local v17, "expDigits":B
    const/16 v18, 0x0

    .line 3830
    .local v18, "expSignAlways":Z
    const/16 v25, 0x0

    .line 3833
    .local v25, "isCurrency":Z
    move-object/from16 v6, v43

    .line 3835
    .local v6, "affix":Ljava/lang/StringBuffer;
    move/from16 v48, v41

    .line 3838
    .local v48, "start":I
    :goto_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v57

    move/from16 v0, v41

    move/from16 v1, v57

    if-ge v0, v1, :cond_2b

    .line 3839
    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v8

    .line 3840
    .local v8, "ch":C
    packed-switch v52, :pswitch_data_0

    .line 3838
    :cond_1
    :goto_2
    add-int/lit8 v41, v41, 0x1

    goto :goto_1

    .line 3851
    :pswitch_0
    if-ne v8, v11, :cond_4

    .line 3852
    if-gtz v56, :cond_2

    if-lez v47, :cond_3

    .line 3853
    :cond_2
    add-int/lit8 v13, v13, 0x1

    .line 3857
    :goto_3
    if-ltz v21, :cond_1

    if-gez v9, :cond_1

    .line 3858
    add-int/lit8 v57, v21, 0x1

    move/from16 v0, v57

    int-to-byte v0, v0

    move/from16 v21, v0

    .line 3859
    goto :goto_2

    .line 3855
    :cond_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 3860
    :cond_4
    move/from16 v0, v55

    if-lt v8, v0, :cond_5

    move/from16 v0, v32

    if-le v8, v0, :cond_6

    :cond_5
    move/from16 v0, v46

    if-ne v8, v0, :cond_c

    .line 3862
    :cond_6
    if-lez v13, :cond_7

    .line 3863
    new-instance v57, Ljava/lang/StringBuffer;

    invoke-direct/range {v57 .. v57}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v58, "Unexpected \'"

    invoke-virtual/range {v57 .. v58}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v57

    move-object/from16 v0, v57

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v57

    const/16 v58, 0x27

    invoke-virtual/range {v57 .. v58}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v57

    invoke-virtual/range {v57 .. v57}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v57

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->patternError(Ljava/lang/String;Ljava/lang/String;)V

    .line 3865
    :cond_7
    move/from16 v0, v46

    if-ne v8, v0, :cond_9

    .line 3866
    add-int/lit8 v47, v47, 0x1

    .line 3883
    :cond_8
    :goto_4
    if-ltz v21, :cond_1

    if-gez v9, :cond_1

    .line 3884
    add-int/lit8 v57, v21, 0x1

    move/from16 v0, v57

    int-to-byte v0, v0

    move/from16 v21, v0

    .line 3885
    goto :goto_2

    .line 3868
    :cond_9
    add-int/lit8 v56, v56, 0x1

    .line 3869
    move/from16 v0, v55

    if-eq v8, v0, :cond_8

    .line 3870
    add-int v57, v12, v56

    add-int v33, v57, v13

    .line 3872
    .local v33, "p":I
    if-ltz v24, :cond_a

    .line 3873
    :goto_5
    move/from16 v0, v24

    move/from16 v1, v33

    if-ge v0, v1, :cond_b

    .line 3874
    const-wide/16 v58, 0xa

    mul-long v26, v26, v58

    .line 3875
    add-int/lit8 v24, v24, 0x1

    .line 3876
    goto :goto_5

    .line 3878
    :cond_a
    move/from16 v24, v33

    .line 3880
    :cond_b
    sub-int v57, v8, v55

    move/from16 v0, v57

    int-to-long v0, v0

    move-wide/from16 v58, v0

    add-long v26, v26, v58

    goto :goto_4

    .line 3886
    .end local v33    # "p":I
    :cond_c
    move/from16 v0, v23

    if-ne v8, v0, :cond_12

    .line 3892
    const/16 v57, 0x27

    move/from16 v0, v57

    if-ne v8, v0, :cond_e

    add-int/lit8 v57, v41, 0x1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v58

    move/from16 v0, v57

    move/from16 v1, v58

    if-ge v0, v1, :cond_e

    .line 3893
    add-int/lit8 v57, v41, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 3894
    .local v7, "after":C
    if-eq v7, v11, :cond_e

    move/from16 v0, v55

    if-lt v7, v0, :cond_d

    move/from16 v0, v32

    if-le v7, v0, :cond_e

    .line 3898
    :cond_d
    const/16 v57, 0x27

    move/from16 v0, v57

    if-ne v7, v0, :cond_10

    .line 3899
    add-int/lit8 v41, v41, 0x1

    .line 3915
    .end local v7    # "after":C
    :cond_e
    if-ltz v9, :cond_f

    .line 3916
    const-string/jumbo v57, "Grouping separator after decimal"

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->patternError(Ljava/lang/String;Ljava/lang/String;)V

    .line 3918
    :cond_f
    move/from16 v22, v21

    .line 3919
    const/16 v21, 0x0

    .line 3920
    goto/16 :goto_2

    .line 3902
    .restart local v7    # "after":C
    :cond_10
    if-gez v21, :cond_11

    .line 3903
    const/16 v52, 0x3

    .line 3904
    goto/16 :goto_2

    .line 3906
    :cond_11
    const/16 v52, 0x2

    .line 3907
    move-object/from16 v6, v53

    .line 3908
    add-int/lit8 v42, v41, -0x1

    .end local v41    # "pos":I
    .local v42, "pos":I
    move/from16 v49, v41

    move/from16 v41, v42

    .line 3910
    .end local v42    # "pos":I
    .restart local v41    # "pos":I
    goto/16 :goto_2

    .line 3920
    .end local v7    # "after":C
    :cond_12
    if-ne v8, v10, :cond_14

    .line 3921
    if-ltz v9, :cond_13

    .line 3922
    const-string/jumbo v57, "Multiple decimal separators"

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->patternError(Ljava/lang/String;Ljava/lang/String;)V

    .line 3927
    :cond_13
    add-int v57, v12, v56

    add-int v9, v57, v13

    .line 3928
    goto/16 :goto_2

    .line 3929
    :cond_14
    const/16 v57, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v58

    move-object/from16 v0, p1

    move/from16 v1, v41

    move-object/from16 v2, v19

    move/from16 v3, v57

    move/from16 v4, v58

    invoke-virtual {v0, v1, v2, v3, v4}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v57

    if-eqz v57, :cond_1c

    .line 3930
    if-ltz v17, :cond_15

    .line 3931
    const-string/jumbo v57, "Multiple exponential symbols"

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->patternError(Ljava/lang/String;Ljava/lang/String;)V

    .line 3933
    :cond_15
    if-ltz v21, :cond_16

    .line 3934
    const-string/jumbo v57, "Grouping separator in exponential"

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->patternError(Ljava/lang/String;Ljava/lang/String;)V

    .line 3936
    :cond_16
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v57

    add-int v41, v41, v57

    .line 3938
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v57

    move/from16 v0, v41

    move/from16 v1, v57

    if-ge v0, v1, :cond_17

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v57

    move/from16 v0, v57

    move/from16 v1, v40

    if-ne v0, v1, :cond_17

    .line 3940
    const/16 v18, 0x1

    .line 3941
    add-int/lit8 v41, v41, 0x1

    .line 3945
    :cond_17
    const/16 v17, 0x0

    .line 3947
    :goto_6
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v57

    move/from16 v0, v41

    move/from16 v1, v57

    if-ge v0, v1, :cond_18

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v57

    move/from16 v0, v57

    move/from16 v1, v55

    if-ne v0, v1, :cond_18

    .line 3948
    add-int/lit8 v57, v17, 0x1

    move/from16 v0, v57

    int-to-byte v0, v0

    move/from16 v17, v0

    .line 3949
    add-int/lit8 v41, v41, 0x1

    .line 3950
    goto :goto_6

    .line 3955
    :cond_18
    add-int v57, v12, v56

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-ge v0, v1, :cond_19

    add-int v57, v47, v13

    const/16 v58, 0x1

    move/from16 v0, v57

    move/from16 v1, v58

    if-lt v0, v1, :cond_1b

    :cond_19
    if-lez v47, :cond_1a

    if-gtz v12, :cond_1b

    :cond_1a
    const/16 v57, 0x1

    move/from16 v0, v17

    move/from16 v1, v57

    if-ge v0, v1, :cond_1c

    .line 3959
    :cond_1b
    const-string/jumbo v57, "Malformed exponential"

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->patternError(Ljava/lang/String;Ljava/lang/String;)V

    .line 3963
    :cond_1c
    const/16 v52, 0x2

    .line 3964
    move-object/from16 v6, v53

    .line 3965
    add-int/lit8 v42, v41, -0x1

    .end local v41    # "pos":I
    .restart local v42    # "pos":I
    move/from16 v49, v41

    move/from16 v41, v42

    .line 3966
    .end local v42    # "pos":I
    .restart local v41    # "pos":I
    goto/16 :goto_2

    .line 3978
    :pswitch_1
    if-eq v8, v11, :cond_1e

    move/from16 v0, v23

    if-eq v8, v0, :cond_1e

    if-eq v8, v10, :cond_1e

    move/from16 v0, v55

    if-lt v8, v0, :cond_1d

    move/from16 v0, v32

    if-le v8, v0, :cond_1e

    :cond_1d
    move/from16 v0, v46

    if-ne v8, v0, :cond_23

    .line 3985
    :cond_1e
    const/16 v57, 0x1

    move/from16 v0, v52

    move/from16 v1, v57

    if-ne v0, v1, :cond_1f

    .line 3986
    const/16 v52, 0x0

    .line 3987
    add-int/lit8 v42, v41, -0x1

    .end local v41    # "pos":I
    .restart local v42    # "pos":I
    move/from16 v50, v41

    move/from16 v41, v42

    .line 3988
    .end local v42    # "pos":I
    .restart local v41    # "pos":I
    goto/16 :goto_2

    .line 3989
    :cond_1f
    const/16 v57, 0x27

    move/from16 v0, v57

    if-ne v8, v0, :cond_21

    .line 3998
    add-int/lit8 v57, v41, 0x1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v58

    move/from16 v0, v57

    move/from16 v1, v58

    if-ge v0, v1, :cond_20

    add-int/lit8 v57, v41, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v57

    const/16 v58, 0x27

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_20

    .line 4000
    add-int/lit8 v41, v41, 0x1

    .line 4001
    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 4003
    :cond_20
    add-int/lit8 v52, v52, 0x2

    .line 4005
    goto/16 :goto_2

    .line 4007
    :cond_21
    new-instance v57, Ljava/lang/StringBuffer;

    invoke-direct/range {v57 .. v57}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v58, "Unquoted special character \'"

    invoke-virtual/range {v57 .. v58}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v57

    move-object/from16 v0, v57

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v57

    const/16 v58, 0x27

    invoke-virtual/range {v57 .. v58}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v57

    invoke-virtual/range {v57 .. v57}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v57

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->patternError(Ljava/lang/String;Ljava/lang/String;)V

    .line 4067
    :cond_22
    :goto_7
    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 4008
    :cond_23
    const/16 v57, 0xa4

    move/from16 v0, v57

    if-ne v8, v0, :cond_26

    .line 4011
    add-int/lit8 v57, v41, 0x1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v58

    move/from16 v0, v57

    move/from16 v1, v58

    if-ge v0, v1, :cond_25

    add-int/lit8 v57, v41, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v57

    const/16 v58, 0xa4

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_25

    const/4 v15, 0x1

    .line 4017
    .local v15, "doubled":Z
    :goto_8
    if-eqz v15, :cond_24

    .line 4018
    add-int/lit8 v41, v41, 0x1

    .line 4019
    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 4021
    :cond_24
    const/16 v25, 0x1

    .line 4023
    goto :goto_7

    .line 4011
    .end local v15    # "doubled":Z
    :cond_25
    const/4 v15, 0x0

    goto :goto_8

    .line 4023
    :cond_26
    const/16 v57, 0x27

    move/from16 v0, v57

    if-ne v8, v0, :cond_28

    .line 4027
    add-int/lit8 v57, v41, 0x1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v58

    move/from16 v0, v57

    move/from16 v1, v58

    if-ge v0, v1, :cond_27

    add-int/lit8 v57, v41, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v57

    const/16 v58, 0x27

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_27

    .line 4029
    add-int/lit8 v41, v41, 0x1

    .line 4030
    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_7

    .line 4032
    :cond_27
    add-int/lit8 v52, v52, 0x2

    .line 4035
    goto :goto_7

    :cond_28
    move/from16 v0, v45

    if-ne v8, v0, :cond_3b

    .line 4038
    const/16 v57, 0x1

    move/from16 v0, v52

    move/from16 v1, v57

    if-eq v0, v1, :cond_29

    const/16 v57, 0x1

    move/from16 v0, v37

    move/from16 v1, v57

    if-ne v0, v1, :cond_2a

    .line 4039
    :cond_29
    new-instance v57, Ljava/lang/StringBuffer;

    invoke-direct/range {v57 .. v57}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v58, "Unquoted special character \'"

    invoke-virtual/range {v57 .. v58}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v57

    move-object/from16 v0, v57

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v57

    const/16 v58, 0x27

    invoke-virtual/range {v57 .. v58}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v57

    invoke-virtual/range {v57 .. v57}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v57

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->patternError(Ljava/lang/String;Ljava/lang/String;)V

    .line 4041
    :cond_2a
    add-int/lit8 v42, v41, 0x1

    .end local v41    # "pos":I
    .restart local v42    # "pos":I
    move/from16 v51, v41

    move/from16 v41, v42

    .line 4093
    .end local v8    # "ch":C
    .end local v42    # "pos":I
    .restart local v41    # "pos":I
    :cond_2b
    const/16 v57, 0x3

    move/from16 v0, v52

    move/from16 v1, v57

    if-eq v0, v1, :cond_2c

    const/16 v57, 0x4

    move/from16 v0, v52

    move/from16 v1, v57

    if-ne v0, v1, :cond_2d

    .line 4094
    :cond_2c
    const-string/jumbo v57, "Unterminated quote"

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->patternError(Ljava/lang/String;Ljava/lang/String;)V

    .line 4097
    :cond_2d
    if-nez v49, :cond_2e

    .line 4098
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v49

    .line 4101
    :cond_2e
    if-nez v51, :cond_2f

    .line 4102
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v51

    .line 4118
    :cond_2f
    if-nez v56, :cond_31

    if-nez v47, :cond_31

    if-lez v12, :cond_31

    if-ltz v9, :cond_31

    .line 4121
    move/from16 v31, v9

    .line 4122
    .local v31, "n":I
    if-nez v31, :cond_30

    add-int/lit8 v31, v31, 0x1

    .line 4123
    :cond_30
    sub-int v13, v12, v31

    .line 4124
    add-int/lit8 v12, v31, -0x1

    .line 4125
    const/16 v56, 0x1

    .line 4129
    .end local v31    # "n":I
    :cond_31
    if-gez v9, :cond_32

    if-lez v13, :cond_32

    if-eqz v47, :cond_35

    :cond_32
    if-ltz v9, :cond_33

    if-gtz v47, :cond_35

    if-lt v9, v12, :cond_35

    add-int v57, v12, v56

    move/from16 v0, v57

    if-gt v9, v0, :cond_35

    :cond_33
    if-eqz v21, :cond_35

    if-eqz v22, :cond_35

    if-lez v47, :cond_34

    if-gtz v56, :cond_35

    :cond_34
    const/16 v57, 0x2

    move/from16 v0, v52

    move/from16 v1, v57

    if-le v0, v1, :cond_36

    .line 4137
    :cond_35
    const-string/jumbo v57, "Malformed pattern"

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->patternError(Ljava/lang/String;Ljava/lang/String;)V

    .line 4141
    :cond_36
    if-ltz v36, :cond_37

    .line 4142
    move/from16 v0, v36

    move/from16 v1, v48

    if-ne v0, v1, :cond_46

    .line 4143
    const/16 v36, 0x0

    .line 4155
    :cond_37
    :goto_9
    if-nez v37, :cond_58

    .line 4162
    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v57

    move-object/from16 v0, v57

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DecimalFormat;->negPrefixPattern:Ljava/lang/String;

    move-object/from16 v0, v57

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DecimalFormat;->posPrefixPattern:Ljava/lang/String;

    .line 4163
    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v57

    move-object/from16 v0, v57

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DecimalFormat;->negSuffixPattern:Ljava/lang/String;

    move-object/from16 v0, v57

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DecimalFormat;->posSuffixPattern:Ljava/lang/String;

    .line 4165
    if-ltz v17, :cond_4a

    const/16 v57, 0x1

    :goto_a
    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    .line 4166
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    move/from16 v57, v0

    if-eqz v57, :cond_38

    .line 4167
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-byte v0, v1, Lcom/ibm/icu/text/DecimalFormat;->minExponentDigits:B

    .line 4168
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/ibm/icu/text/DecimalFormat;->exponentSignAlwaysShown:Z

    .line 4170
    :cond_38
    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/ibm/icu/text/DecimalFormat;->isCurrencyFormat:Z

    .line 4171
    add-int v57, v12, v56

    add-int v14, v57, v13

    .line 4176
    .local v14, "digitTotalCount":I
    if-ltz v9, :cond_4b

    move/from16 v16, v9

    .line 4177
    .local v16, "effectiveDecimalPos":I
    :goto_b
    if-lez v47, :cond_4c

    const/16 v54, 0x1

    .line 4178
    .local v54, "useSigDig":Z
    :goto_c
    move-object/from16 v0, p0

    move/from16 v1, v54

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setSignificantDigitsUsed(Z)V

    .line 4179
    if-eqz v54, :cond_4d

    .line 4180
    move-object/from16 v0, p0

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setMinimumSignificantDigits(I)V

    .line 4181
    add-int v57, v47, v13

    move-object/from16 v0, p0

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setMaximumSignificantDigits(I)V

    .line 4195
    :goto_d
    if-lez v21, :cond_51

    const/16 v57, 0x1

    :goto_e
    move-object/from16 v0, p0

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setGroupingUsed(Z)V

    .line 4196
    if-lez v21, :cond_52

    move/from16 v57, v21

    :goto_f
    move/from16 v0, v57

    move-object/from16 v1, p0

    iput-byte v0, v1, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    .line 4197
    if-lez v22, :cond_53

    move/from16 v0, v22

    move/from16 v1, v21

    if-eq v0, v1, :cond_53

    .end local v22    # "groupingCount2":B
    :goto_10
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-byte v0, v1, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    .line 4199
    move/from16 v0, v30

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    .line 4200
    if-eqz v9, :cond_39

    if-ne v9, v14, :cond_54

    :cond_39
    const/16 v57, 0x1

    :goto_11
    move-object/from16 v0, p0

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setDecimalSeparatorAlwaysShown(Z)V

    .line 4202
    if-ltz v36, :cond_55

    .line 4203
    move/from16 v0, v36

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    .line 4204
    sub-int v57, v49, v50

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    .line 4205
    move/from16 v0, v34

    move-object/from16 v1, p0

    iput-char v0, v1, Lcom/ibm/icu/text/DecimalFormat;->pad:C

    .line 4209
    :goto_12
    const-wide/16 v58, 0x0

    cmp-long v57, v26, v58

    if-eqz v57, :cond_57

    .line 4212
    sub-int v44, v24, v16

    .line 4213
    .local v44, "scale":I
    if-lez v44, :cond_56

    move/from16 v57, v44

    :goto_13
    move-wide/from16 v0, v26

    move/from16 v2, v57

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/math/BigDecimal;->valueOf(JI)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v57

    move-object/from16 v0, v57

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    .line 4215
    if-gez v44, :cond_3a

    .line 4216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    move-object/from16 v57, v0

    move/from16 v0, v44

    neg-int v0, v0

    move/from16 v58, v0

    invoke-virtual/range {v57 .. v58}, Lcom/ibm/icu/math/BigDecimal;->movePointRight(I)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v57

    move-object/from16 v0, v57

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    .line 4219
    :cond_3a
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->setRoundingDouble()V

    .line 4220
    const/16 v57, 0x6

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/DecimalFormat;->roundingMode:I

    .line 3803
    .end local v14    # "digitTotalCount":I
    .end local v16    # "effectiveDecimalPos":I
    .end local v44    # "scale":I
    .end local v54    # "useSigDig":Z
    :goto_14
    add-int/lit8 v37, v37, 0x1

    goto/16 :goto_0

    .line 4043
    .restart local v8    # "ch":C
    .restart local v22    # "groupingCount2":B
    :cond_3b
    move/from16 v0, v39

    if-eq v8, v0, :cond_3c

    move/from16 v0, v38

    if-ne v8, v0, :cond_40

    .line 4045
    :cond_3c
    const/16 v57, 0x1

    move/from16 v0, v30

    move/from16 v1, v57

    if-eq v0, v1, :cond_3d

    .line 4046
    const-string/jumbo v57, "Too many percent/permille characters"

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->patternError(Ljava/lang/String;Ljava/lang/String;)V

    .line 4048
    :cond_3d
    move/from16 v0, v39

    if-ne v8, v0, :cond_3e

    const/16 v30, 0x64

    .line 4050
    :goto_15
    move/from16 v0, v39

    if-ne v8, v0, :cond_3f

    const/16 v8, 0x25

    .line 4052
    :goto_16
    goto/16 :goto_7

    .line 4048
    :cond_3e
    const/16 v30, 0x3e8

    goto :goto_15

    .line 4050
    :cond_3f
    const/16 v8, 0x2030

    goto :goto_16

    .line 4052
    :cond_40
    move/from16 v0, v29

    if-ne v8, v0, :cond_41

    .line 4054
    const/16 v8, 0x2d

    .line 4056
    goto/16 :goto_7

    :cond_41
    move/from16 v0, v35

    if-ne v8, v0, :cond_22

    .line 4057
    if-ltz v36, :cond_42

    .line 4058
    const-string/jumbo v57, "Multiple pad specifiers"

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->patternError(Ljava/lang/String;Ljava/lang/String;)V

    .line 4060
    :cond_42
    add-int/lit8 v57, v41, 0x1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v58

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_43

    .line 4061
    const-string/jumbo v57, "Invalid pad specifier"

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->patternError(Ljava/lang/String;Ljava/lang/String;)V

    .line 4063
    :cond_43
    add-int/lit8 v42, v41, 0x1

    .end local v41    # "pos":I
    .restart local v42    # "pos":I
    move/from16 v36, v41

    .line 4064
    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v34

    move/from16 v41, v42

    .line 4065
    .end local v42    # "pos":I
    .restart local v41    # "pos":I
    goto/16 :goto_2

    .line 4074
    :pswitch_2
    const/16 v57, 0x27

    move/from16 v0, v57

    if-ne v8, v0, :cond_44

    .line 4075
    add-int/lit8 v57, v41, 0x1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v58

    move/from16 v0, v57

    move/from16 v1, v58

    if-ge v0, v1, :cond_45

    add-int/lit8 v57, v41, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v57

    const/16 v58, 0x27

    move/from16 v0, v57

    move/from16 v1, v58

    if-ne v0, v1, :cond_45

    .line 4077
    add-int/lit8 v41, v41, 0x1

    .line 4078
    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 4088
    :cond_44
    :goto_17
    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 4080
    :cond_45
    add-int/lit8 v52, v52, -0x2

    goto :goto_17

    .line 4144
    .end local v8    # "ch":C
    :cond_46
    add-int/lit8 v57, v36, 0x2

    move/from16 v0, v57

    move/from16 v1, v50

    if-ne v0, v1, :cond_47

    .line 4145
    const/16 v36, 0x1

    .line 4146
    goto/16 :goto_9

    :cond_47
    move/from16 v0, v36

    move/from16 v1, v49

    if-ne v0, v1, :cond_48

    .line 4147
    const/16 v36, 0x2

    .line 4148
    goto/16 :goto_9

    :cond_48
    add-int/lit8 v57, v36, 0x2

    move/from16 v0, v57

    move/from16 v1, v51

    if-ne v0, v1, :cond_49

    .line 4149
    const/16 v36, 0x3

    .line 4150
    goto/16 :goto_9

    .line 4151
    :cond_49
    const-string/jumbo v57, "Illegal pad position"

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->patternError(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 4165
    :cond_4a
    const/16 v57, 0x0

    goto/16 :goto_a

    .restart local v14    # "digitTotalCount":I
    :cond_4b
    move/from16 v16, v14

    .line 4176
    goto/16 :goto_b

    .line 4177
    .restart local v16    # "effectiveDecimalPos":I
    :cond_4c
    const/16 v54, 0x0

    goto/16 :goto_c

    .line 4183
    .restart local v54    # "useSigDig":Z
    :cond_4d
    sub-int v28, v16, v12

    .line 4184
    .local v28, "minInt":I
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setMinimumIntegerDigits(I)V

    .line 4188
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    move/from16 v57, v0

    if-eqz v57, :cond_4e

    add-int v57, v12, v28

    :goto_18
    move-object/from16 v0, p0

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setMaximumIntegerDigits(I)V

    .line 4190
    if-ltz v9, :cond_4f

    sub-int v57, v14, v9

    :goto_19
    move-object/from16 v0, p0

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setMaximumFractionDigits(I)V

    .line 4192
    if-ltz v9, :cond_50

    add-int v57, v12, v56

    sub-int v57, v57, v9

    :goto_1a
    move-object/from16 v0, p0

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setMinimumFractionDigits(I)V

    goto/16 :goto_d

    .line 4188
    :cond_4e
    const/16 v57, 0x135

    goto :goto_18

    .line 4190
    :cond_4f
    const/16 v57, 0x0

    goto :goto_19

    .line 4192
    :cond_50
    const/16 v57, 0x0

    goto :goto_1a

    .line 4195
    .end local v28    # "minInt":I
    :cond_51
    const/16 v57, 0x0

    goto/16 :goto_e

    .line 4196
    :cond_52
    const/16 v57, 0x0

    goto/16 :goto_f

    .line 4197
    :cond_53
    const/16 v22, 0x0

    goto/16 :goto_10

    .line 4200
    .end local v22    # "groupingCount2":B
    :cond_54
    const/16 v57, 0x0

    goto/16 :goto_11

    .line 4207
    :cond_55
    const/16 v57, 0x0

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    goto/16 :goto_12

    .line 4213
    .restart local v44    # "scale":I
    :cond_56
    const/16 v57, 0x0

    goto/16 :goto_13

    .line 4222
    .end local v44    # "scale":I
    :cond_57
    const/16 v57, 0x0

    check-cast v57, Lcom/ibm/icu/math/BigDecimal;

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setRoundingIncrement(Lcom/ibm/icu/math/BigDecimal;)V

    goto/16 :goto_14

    .line 4229
    .end local v14    # "digitTotalCount":I
    .end local v16    # "effectiveDecimalPos":I
    .end local v54    # "useSigDig":Z
    .restart local v22    # "groupingCount2":B
    :cond_58
    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v57

    move-object/from16 v0, v57

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DecimalFormat;->negPrefixPattern:Ljava/lang/String;

    .line 4230
    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v57

    move-object/from16 v0, v57

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DecimalFormat;->negSuffixPattern:Ljava/lang/String;

    .line 4231
    const/16 v20, 0x1

    goto/16 :goto_14

    .line 4239
    .end local v6    # "affix":Ljava/lang/StringBuffer;
    .end local v9    # "decimalPos":I
    .end local v12    # "digitLeftCount":I
    .end local v13    # "digitRightCount":I
    .end local v17    # "expDigits":B
    .end local v18    # "expSignAlways":Z
    .end local v21    # "groupingCount":B
    .end local v22    # "groupingCount2":B
    .end local v24    # "incrementPos":I
    .end local v25    # "isCurrency":Z
    .end local v26    # "incrementVal":J
    .end local v30    # "multpl":I
    .end local v34    # "padChar":C
    .end local v36    # "padPos":I
    .end local v43    # "prefix":Ljava/lang/StringBuffer;
    .end local v47    # "sigDigitCount":I
    .end local v48    # "start":I
    .end local v49    # "sub0Limit":I
    .end local v50    # "sub0Start":I
    .end local v51    # "sub2Limit":I
    .end local v52    # "subpart":I
    .end local v53    # "suffix":Ljava/lang/StringBuffer;
    .end local v56    # "zeroDigitCount":I
    :cond_59
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v57

    if-nez v57, :cond_5a

    .line 4240
    const-string/jumbo v57, ""

    move-object/from16 v0, v57

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DecimalFormat;->posSuffixPattern:Ljava/lang/String;

    move-object/from16 v0, v57

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DecimalFormat;->posPrefixPattern:Ljava/lang/String;

    .line 4241
    const/16 v57, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setMinimumIntegerDigits(I)V

    .line 4242
    const/16 v57, 0x135

    move-object/from16 v0, p0

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setMaximumIntegerDigits(I)V

    .line 4243
    const/16 v57, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setMinimumFractionDigits(I)V

    .line 4244
    const/16 v57, 0x154

    move-object/from16 v0, p0

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setMaximumFractionDigits(I)V

    .line 4254
    :cond_5a
    if-eqz v20, :cond_5b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->negPrefixPattern:Ljava/lang/String;

    move-object/from16 v57, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->posPrefixPattern:Ljava/lang/String;

    move-object/from16 v58, v0

    invoke-virtual/range {v57 .. v58}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v57

    if-eqz v57, :cond_5c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->negSuffixPattern:Ljava/lang/String;

    move-object/from16 v57, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->posSuffixPattern:Ljava/lang/String;

    move-object/from16 v58, v0

    invoke-virtual/range {v57 .. v58}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v57

    if-eqz v57, :cond_5c

    .line 4257
    :cond_5b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->posSuffixPattern:Ljava/lang/String;

    move-object/from16 v57, v0

    move-object/from16 v0, v57

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DecimalFormat;->negSuffixPattern:Ljava/lang/String;

    .line 4258
    new-instance v57, Ljava/lang/StringBuffer;

    invoke-direct/range {v57 .. v57}, Ljava/lang/StringBuffer;-><init>()V

    const/16 v58, 0x2d

    invoke-virtual/range {v57 .. v58}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v57

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->posPrefixPattern:Ljava/lang/String;

    move-object/from16 v58, v0

    invoke-virtual/range {v57 .. v58}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v57

    invoke-virtual/range {v57 .. v57}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v57

    move-object/from16 v0, v57

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DecimalFormat;->negPrefixPattern:Ljava/lang/String;

    .line 4265
    :cond_5c
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->expandAffixes()V

    .line 4268
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    move/from16 v57, v0

    if-lez v57, :cond_5d

    .line 4269
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    move/from16 v57, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->positivePrefix:Ljava/lang/String;

    move-object/from16 v58, v0

    invoke-virtual/range {v58 .. v58}, Ljava/lang/String;->length()I

    move-result v58

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->positiveSuffix:Ljava/lang/String;

    move-object/from16 v59, v0

    invoke-virtual/range {v59 .. v59}, Ljava/lang/String;->length()I

    move-result v59

    add-int v58, v58, v59

    add-int v57, v57, v58

    move/from16 v0, v57

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    .line 4272
    :cond_5d
    const/16 v57, 0x0

    const/16 v58, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move-object/from16 v2, v58

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V

    .line 4273
    return-void

    .line 3840
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private compareAffix(Ljava/lang/String;IZZ[Lcom/ibm/icu/util/Currency;)I
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "pos"    # I
    .param p3, "isNegative"    # Z
    .param p4, "isPrefix"    # Z
    .param p5, "currency"    # [Lcom/ibm/icu/util/Currency;

    .prologue
    .line 2190
    if-nez p5, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->currencyChoice:Ljava/text/ChoiceFormat;

    if-eqz v0, :cond_4

    .line 2191
    :cond_0
    if-eqz p4, :cond_2

    .line 2192
    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->negPrefixPattern:Ljava/lang/String;

    :goto_0
    invoke-direct {p0, v0, p1, p2, p5}, Lcom/ibm/icu/text/DecimalFormat;->compareComplexAffix(Ljava/lang/String;Ljava/lang/String;I[Lcom/ibm/icu/util/Currency;)I

    move-result v0

    .line 2204
    :goto_1
    return v0

    .line 2192
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->posPrefixPattern:Ljava/lang/String;

    goto :goto_0

    .line 2195
    :cond_2
    if-eqz p3, :cond_3

    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->negSuffixPattern:Ljava/lang/String;

    :goto_2
    invoke-direct {p0, v0, p1, p2, p5}, Lcom/ibm/icu/text/DecimalFormat;->compareComplexAffix(Ljava/lang/String;Ljava/lang/String;I[Lcom/ibm/icu/util/Currency;)I

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->posSuffixPattern:Ljava/lang/String;

    goto :goto_2

    .line 2200
    :cond_4
    if-eqz p4, :cond_6

    .line 2201
    if-eqz p3, :cond_5

    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->negativePrefix:Ljava/lang/String;

    :goto_3
    invoke-static {v0, p1, p2}, Lcom/ibm/icu/text/DecimalFormat;->compareSimpleAffix(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->positivePrefix:Ljava/lang/String;

    goto :goto_3

    .line 2204
    :cond_6
    if-eqz p3, :cond_7

    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->negativeSuffix:Ljava/lang/String;

    :goto_4
    invoke-static {v0, p1, p2}, Lcom/ibm/icu/text/DecimalFormat;->compareSimpleAffix(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->positiveSuffix:Ljava/lang/String;

    goto :goto_4
.end method

.method private compareComplexAffix(Ljava/lang/String;Ljava/lang/String;I[Lcom/ibm/icu/util/Currency;)I
    .locals 10
    .param p1, "affixPat"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "pos"    # I
    .param p4, "currency"    # [Lcom/ibm/icu/util/Currency;

    .prologue
    .line 2319
    const/4 v1, 0x0

    .local v1, "i":I
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v1, v8, :cond_a

    if-ltz p3, :cond_a

    .line 2320
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2321
    .local v0, "c":C
    const/16 v8, 0x27

    if-ne v0, v8, :cond_3

    move v1, v2

    .line 2323
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :goto_1
    const/16 v8, 0x27

    invoke-virtual {p1, v8, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v5

    .line 2324
    .local v5, "j":I
    if-ne v5, v1, :cond_1

    .line 2325
    const/16 v8, 0x27

    invoke-static {p2, p3, v8}, Lcom/ibm/icu/text/DecimalFormat;->match(Ljava/lang/String;II)I

    move-result p3

    .line 2326
    add-int/lit8 v1, v5, 0x1

    .line 2327
    goto :goto_0

    .line 2328
    :cond_1
    if-le v5, v1, :cond_2

    .line 2329
    invoke-virtual {p1, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {p2, p3, v8}, Lcom/ibm/icu/text/DecimalFormat;->match(Ljava/lang/String;ILjava/lang/String;)I

    move-result p3

    .line 2330
    add-int/lit8 v1, v5, 0x1

    .line 2331
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v1, v8, :cond_0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x27

    if-ne v8, v9, :cond_0

    .line 2333
    const/16 v8, 0x27

    invoke-static {p2, p3, v8}, Lcom/ibm/icu/text/DecimalFormat;->match(Ljava/lang/String;II)I

    move-result p3

    .line 2334
    add-int/lit8 v1, v1, 0x1

    .line 2336
    goto :goto_1

    .line 2342
    :cond_2
    new-instance v8, Ljava/lang/RuntimeException;

    invoke-direct {v8}, Ljava/lang/RuntimeException;-><init>()V

    throw v8

    .line 2348
    .end local v1    # "i":I
    .end local v5    # "j":I
    .restart local v2    # "i":I
    :cond_3
    sparse-switch v0, :sswitch_data_0

    .line 2399
    :goto_2
    invoke-static {p2, p3, v0}, Lcom/ibm/icu/text/DecimalFormat;->match(Ljava/lang/String;II)I

    move-result p3

    .line 2400
    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 2401
    invoke-static {p1, v2}, Lcom/ibm/icu/text/DecimalFormat;->skipRuleWhiteSpace(Ljava/lang/String;I)I

    move-result v1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 2354
    .end local v1    # "i":I
    .restart local v2    # "i":I
    :sswitch_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v2, v8, :cond_5

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0xa4

    if-ne v8, v9, :cond_5

    const/4 v3, 0x1

    .line 2359
    .local v3, "intl":Z
    :goto_3
    if-eqz p4, :cond_7

    .line 2362
    sget-object v8, Lcom/ibm/icu/util/ULocale;->VALID_LOCALE:Lcom/ibm/icu/util/ULocale$Type;

    invoke-virtual {p0, v8}, Lcom/ibm/icu/text/DecimalFormat;->getLocale(Lcom/ibm/icu/util/ULocale$Type;)Lcom/ibm/icu/util/ULocale;

    move-result-object v7

    .line 2363
    .local v7, "uloc":Lcom/ibm/icu/util/ULocale;
    if-nez v7, :cond_4

    .line 2365
    iget-object v8, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    sget-object v9, Lcom/ibm/icu/util/ULocale;->VALID_LOCALE:Lcom/ibm/icu/util/ULocale$Type;

    invoke-virtual {v8, v9}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getLocale(Lcom/ibm/icu/util/ULocale$Type;)Lcom/ibm/icu/util/ULocale;

    move-result-object v7

    .line 2368
    :cond_4
    new-instance v6, Ljava/text/ParsePosition;

    invoke-direct {v6, p3}, Ljava/text/ParsePosition;-><init>(I)V

    .line 2369
    .local v6, "ppos":Ljava/text/ParsePosition;
    invoke-static {v7, p2, v6}, Lcom/ibm/icu/util/Currency;->parse(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/String;

    move-result-object v4

    .line 2372
    .local v4, "iso":Ljava/lang/String;
    if-eqz v4, :cond_6

    .line 2373
    const/4 v8, 0x0

    invoke-static {v4}, Lcom/ibm/icu/util/Currency;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/util/Currency;

    move-result-object v9

    aput-object v9, p4, v8

    .line 2374
    invoke-virtual {v6}, Ljava/text/ParsePosition;->getIndex()I

    move-result p3

    :goto_4
    move v1, v2

    .line 2378
    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto/16 :goto_0

    .line 2354
    .end local v1    # "i":I
    .end local v3    # "intl":Z
    .end local v4    # "iso":Ljava/lang/String;
    .end local v6    # "ppos":Ljava/text/ParsePosition;
    .end local v7    # "uloc":Lcom/ibm/icu/util/ULocale;
    .restart local v2    # "i":I
    :cond_5
    const/4 v3, 0x0

    goto :goto_3

    .line 2376
    .restart local v3    # "intl":Z
    .restart local v4    # "iso":Ljava/lang/String;
    .restart local v6    # "ppos":Ljava/text/ParsePosition;
    .restart local v7    # "uloc":Lcom/ibm/icu/util/ULocale;
    :cond_6
    const/4 p3, -0x1

    goto :goto_4

    .line 2379
    .end local v4    # "iso":Ljava/lang/String;
    .end local v6    # "ppos":Ljava/text/ParsePosition;
    .end local v7    # "uloc":Lcom/ibm/icu/util/ULocale;
    :cond_7
    if-eqz v3, :cond_8

    .line 2380
    add-int/lit8 v1, v2, 0x1

    .line 2381
    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {p0}, Lcom/ibm/icu/text/DecimalFormat;->getCurrency()Lcom/ibm/icu/util/Currency;

    move-result-object v8

    invoke-virtual {v8}, Lcom/ibm/icu/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v8

    invoke-static {p2, p3, v8}, Lcom/ibm/icu/text/DecimalFormat;->match(Ljava/lang/String;ILjava/lang/String;)I

    move-result p3

    .line 2382
    goto/16 :goto_0

    .line 2383
    .end local v1    # "i":I
    .restart local v2    # "i":I
    :cond_8
    new-instance v6, Ljava/text/ParsePosition;

    invoke-direct {v6, p3}, Ljava/text/ParsePosition;-><init>(I)V

    .line 2384
    .restart local v6    # "ppos":Ljava/text/ParsePosition;
    iget-object v8, p0, Lcom/ibm/icu/text/DecimalFormat;->currencyChoice:Ljava/text/ChoiceFormat;

    invoke-virtual {v8, p2, v6}, Ljava/text/ChoiceFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    .line 2385
    invoke-virtual {v6}, Ljava/text/ParsePosition;->getIndex()I

    move-result v8

    if-ne v8, p3, :cond_9

    const/4 p3, -0x1

    :goto_5
    move v1, v2

    .line 2388
    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto/16 :goto_0

    .line 2385
    .end local v1    # "i":I
    .restart local v2    # "i":I
    :cond_9
    invoke-virtual {v6}, Ljava/text/ParsePosition;->getIndex()I

    move-result p3

    goto :goto_5

    .line 2390
    .end local v3    # "intl":Z
    .end local v6    # "ppos":Ljava/text/ParsePosition;
    :sswitch_1
    iget-object v8, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v8}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPercent()C

    move-result v0

    .line 2391
    goto :goto_2

    .line 2393
    :sswitch_2
    iget-object v8, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v8}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPerMill()C

    move-result v0

    .line 2394
    goto/16 :goto_2

    .line 2396
    :sswitch_3
    iget-object v8, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v8}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getMinusSign()C

    move-result v0

    goto/16 :goto_2

    .line 2405
    .end local v0    # "c":C
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :cond_a
    return p3

    .end local v1    # "i":I
    .restart local v0    # "c":C
    .restart local v2    # "i":I
    :cond_b
    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto/16 :goto_0

    .line 2348
    :sswitch_data_0
    .sparse-switch
        0x25 -> :sswitch_1
        0x2d -> :sswitch_3
        0xa4 -> :sswitch_0
        0x2030 -> :sswitch_2
    .end sparse-switch
.end method

.method private static compareSimpleAffix(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 8
    .param p0, "affix"    # Ljava/lang/String;
    .param p1, "input"    # Ljava/lang/String;
    .param p2, "pos"    # I

    .prologue
    const/4 v6, -0x1

    .line 2220
    move v5, p2

    .line 2221
    .local v5, "start":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v1, v7, :cond_6

    .line 2222
    invoke-static {p0, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 2223
    .local v0, "c":I
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v2

    .line 2224
    .local v2, "len":I
    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2231
    const/4 v3, 0x0

    .line 2233
    .local v3, "literalMatch":Z
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-ge p2, v7, :cond_1

    invoke-static {p1, p2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v0, :cond_1

    .line 2234
    const/4 v3, 0x1

    .line 2235
    add-int/2addr v1, v2

    .line 2236
    add-int/2addr p2, v2

    .line 2237
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v1, v7, :cond_3

    .line 2248
    :cond_1
    :goto_1
    invoke-static {p0, v1}, Lcom/ibm/icu/text/DecimalFormat;->skipRuleWhiteSpace(Ljava/lang/String;I)I

    move-result v1

    .line 2253
    move v4, p2

    .line 2254
    .local v4, "s":I
    invoke-static {p1, p2}, Lcom/ibm/icu/text/DecimalFormat;->skipUWhiteSpace(Ljava/lang/String;I)I

    move-result p2

    .line 2255
    if-ne p2, v4, :cond_4

    if-nez v3, :cond_4

    .line 2272
    .end local v0    # "c":I
    .end local v2    # "len":I
    .end local v3    # "literalMatch":Z
    .end local v4    # "s":I
    :cond_2
    :goto_2
    return v6

    .line 2240
    .restart local v0    # "c":I
    .restart local v2    # "len":I
    .restart local v3    # "literalMatch":Z
    :cond_3
    invoke-static {p0, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 2241
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v2

    .line 2242
    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v7

    if-nez v7, :cond_0

    goto :goto_1

    .line 2261
    .restart local v4    # "s":I
    :cond_4
    invoke-static {p0, v1}, Lcom/ibm/icu/text/DecimalFormat;->skipUWhiteSpace(Ljava/lang/String;I)I

    move-result v1

    .line 2262
    goto :goto_0

    .line 2263
    .end local v3    # "literalMatch":Z
    .end local v4    # "s":I
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-ge p2, v7, :cond_2

    invoke-static {p1, p2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v0, :cond_2

    .line 2265
    add-int/2addr v1, v2

    .line 2266
    add-int/2addr p2, v2

    .line 2267
    goto :goto_0

    .line 2272
    .end local v0    # "c":I
    .end local v2    # "len":I
    :cond_6
    sub-int v6, p2, v5

    goto :goto_2
.end method

.method private equals(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "pat1"    # Ljava/lang/String;
    .param p2, "pat2"    # Ljava/lang/String;

    .prologue
    .line 3134
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3135
    const/4 v0, 0x1

    .line 3137
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DecimalFormat;->unquote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/ibm/icu/text/DecimalFormat;->unquote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private expandAffix(Ljava/lang/String;Ljava/lang/StringBuffer;Z)V
    .locals 12
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "buffer"    # Ljava/lang/StringBuffer;
    .param p3, "doFormat"    # Z

    .prologue
    .line 3265
    const/4 v9, 0x0

    invoke-virtual {p2, v9}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 3266
    const/4 v2, 0x0

    .local v2, "i":I
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v2, v9, :cond_b

    .line 3267
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 3268
    .local v0, "c":C
    const/16 v9, 0x27

    if-ne v0, v9, :cond_3

    move v2, v3

    .line 3270
    .end local v3    # "i":I
    .restart local v2    # "i":I
    :goto_1
    const/16 v9, 0x27

    invoke-virtual {p1, v9, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v6

    .line 3271
    .local v6, "j":I
    if-ne v6, v2, :cond_1

    .line 3272
    const/16 v9, 0x27

    invoke-virtual {p2, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3273
    add-int/lit8 v2, v6, 0x1

    .line 3274
    goto :goto_0

    .line 3275
    :cond_1
    if-le v6, v2, :cond_2

    .line 3276
    invoke-virtual {p1, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3277
    add-int/lit8 v2, v6, 0x1

    .line 3278
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v2, v9, :cond_0

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x27

    if-ne v9, v10, :cond_0

    .line 3280
    const/16 v9, 0x27

    invoke-virtual {p2, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3281
    add-int/lit8 v2, v2, 0x1

    .line 3283
    goto :goto_1

    .line 3289
    :cond_2
    new-instance v9, Ljava/lang/RuntimeException;

    invoke-direct {v9}, Ljava/lang/RuntimeException;-><init>()V

    throw v9

    .line 3295
    .end local v2    # "i":I
    .end local v6    # "j":I
    .restart local v3    # "i":I
    :cond_3
    sparse-switch v0, :sswitch_data_0

    .line 3361
    :goto_2
    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v2, v3

    .line 3362
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 3302
    .end local v2    # "i":I
    .restart local v3    # "i":I
    :sswitch_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v3, v9, :cond_6

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0xa4

    if-ne v9, v10, :cond_6

    const/4 v4, 0x1

    .line 3304
    .local v4, "intl":Z
    :goto_3
    if-eqz v4, :cond_c

    .line 3305
    add-int/lit8 v2, v3, 0x1

    .line 3307
    .end local v3    # "i":I
    .restart local v2    # "i":I
    :goto_4
    const/4 v8, 0x0

    .line 3308
    .local v8, "s":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/ibm/icu/text/DecimalFormat;->getCurrency()Lcom/ibm/icu/util/Currency;

    move-result-object v1

    .line 3309
    .local v1, "currency":Lcom/ibm/icu/util/Currency;
    if-eqz v1, :cond_9

    .line 3310
    if-nez v4, :cond_8

    .line 3311
    const/4 v9, 0x1

    new-array v5, v9, [Z

    .line 3312
    .local v5, "isChoiceFormat":[Z
    iget-object v9, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v9}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getULocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v1, v9, v10, v5}, Lcom/ibm/icu/util/Currency;->getName(Lcom/ibm/icu/util/ULocale;I[Z)Ljava/lang/String;

    move-result-object v8

    .line 3315
    const/4 v9, 0x0

    aget-boolean v9, v5, v9

    if-eqz v9, :cond_5

    .line 3320
    if-nez p3, :cond_7

    .line 3325
    iget-object v9, p0, Lcom/ibm/icu/text/DecimalFormat;->currencyChoice:Ljava/text/ChoiceFormat;

    if-nez v9, :cond_4

    .line 3326
    new-instance v9, Ljava/text/ChoiceFormat;

    invoke-direct {v9, v8}, Ljava/text/ChoiceFormat;-><init>(Ljava/lang/String;)V

    iput-object v9, p0, Lcom/ibm/icu/text/DecimalFormat;->currencyChoice:Ljava/text/ChoiceFormat;

    .line 3335
    :cond_4
    const/16 v9, 0xa4

    invoke-static {v9}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v8

    .line 3349
    .end local v5    # "isChoiceFormat":[Z
    :cond_5
    :goto_5
    invoke-virtual {p2, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 3302
    .end local v1    # "currency":Lcom/ibm/icu/util/Currency;
    .end local v2    # "i":I
    .end local v4    # "intl":Z
    .end local v8    # "s":Ljava/lang/String;
    .restart local v3    # "i":I
    :cond_6
    const/4 v4, 0x0

    goto :goto_3

    .line 3337
    .end local v3    # "i":I
    .restart local v1    # "currency":Lcom/ibm/icu/util/Currency;
    .restart local v2    # "i":I
    .restart local v4    # "intl":Z
    .restart local v5    # "isChoiceFormat":[Z
    .restart local v8    # "s":Ljava/lang/String;
    :cond_7
    new-instance v7, Ljava/text/FieldPosition;

    const/4 v9, 0x0

    invoke-direct {v7, v9}, Ljava/text/FieldPosition;-><init>(I)V

    .line 3338
    .local v7, "pos":Ljava/text/FieldPosition;
    iget-object v9, p0, Lcom/ibm/icu/text/DecimalFormat;->currencyChoice:Ljava/text/ChoiceFormat;

    iget-object v10, p0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    invoke-virtual {v10}, Lcom/ibm/icu/text/DigitList;->getDouble()D

    move-result-wide v10

    invoke-virtual {v9, v10, v11, p2, v7}, Ljava/text/ChoiceFormat;->format(DLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 3343
    .end local v5    # "isChoiceFormat":[Z
    .end local v7    # "pos":Ljava/text/FieldPosition;
    :cond_8
    invoke-virtual {v1}, Lcom/ibm/icu/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v8

    .line 3345
    goto :goto_5

    .line 3346
    :cond_9
    if-eqz v4, :cond_a

    iget-object v9, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v9}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getInternationalCurrencySymbol()Ljava/lang/String;

    move-result-object v8

    :goto_6
    goto :goto_5

    :cond_a
    iget-object v9, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v9}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getCurrencySymbol()Ljava/lang/String;

    move-result-object v8

    goto :goto_6

    .line 3352
    .end local v1    # "currency":Lcom/ibm/icu/util/Currency;
    .end local v2    # "i":I
    .end local v4    # "intl":Z
    .end local v8    # "s":Ljava/lang/String;
    .restart local v3    # "i":I
    :sswitch_1
    iget-object v9, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v9}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPercent()C

    move-result v0

    .line 3353
    goto :goto_2

    .line 3355
    :sswitch_2
    iget-object v9, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v9}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPerMill()C

    move-result v0

    .line 3356
    goto/16 :goto_2

    .line 3358
    :sswitch_3
    iget-object v9, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v9}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getMinusSign()C

    move-result v0

    goto/16 :goto_2

    .line 3363
    .end local v0    # "c":C
    .end local v3    # "i":I
    .restart local v2    # "i":I
    :cond_b
    return-void

    .end local v2    # "i":I
    .restart local v0    # "c":C
    .restart local v3    # "i":I
    .restart local v4    # "intl":Z
    :cond_c
    move v2, v3

    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_4

    .line 3295
    :sswitch_data_0
    .sparse-switch
        0x25 -> :sswitch_1
        0x2d -> :sswitch_3
        0xa4 -> :sswitch_0
        0x2030 -> :sswitch_2
    .end sparse-switch
.end method

.method private expandAffixes()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3210
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->currencyChoice:Ljava/text/ChoiceFormat;

    .line 3213
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 3214
    .local v0, "buffer":Ljava/lang/StringBuffer;
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->posPrefixPattern:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3215
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->posPrefixPattern:Ljava/lang/String;

    invoke-direct {p0, v1, v0, v2}, Lcom/ibm/icu/text/DecimalFormat;->expandAffix(Ljava/lang/String;Ljava/lang/StringBuffer;Z)V

    .line 3216
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->positivePrefix:Ljava/lang/String;

    .line 3218
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->posSuffixPattern:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3219
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->posSuffixPattern:Ljava/lang/String;

    invoke-direct {p0, v1, v0, v2}, Lcom/ibm/icu/text/DecimalFormat;->expandAffix(Ljava/lang/String;Ljava/lang/StringBuffer;Z)V

    .line 3220
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->positiveSuffix:Ljava/lang/String;

    .line 3222
    :cond_1
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->negPrefixPattern:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3223
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->negPrefixPattern:Ljava/lang/String;

    invoke-direct {p0, v1, v0, v2}, Lcom/ibm/icu/text/DecimalFormat;->expandAffix(Ljava/lang/String;Ljava/lang/StringBuffer;Z)V

    .line 3224
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->negativePrefix:Ljava/lang/String;

    .line 3226
    :cond_2
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->negSuffixPattern:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 3227
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->negSuffixPattern:Ljava/lang/String;

    invoke-direct {p0, v1, v0, v2}, Lcom/ibm/icu/text/DecimalFormat;->expandAffix(Ljava/lang/String;Ljava/lang/StringBuffer;Z)V

    .line 3228
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->negativeSuffix:Ljava/lang/String;

    .line 3230
    :cond_3
    return-void
.end method

.method private format(DLjava/lang/StringBuffer;Ljava/text/FieldPosition;Z)Ljava/lang/StringBuffer;
    .locals 21
    .param p1, "number"    # D
    .param p3, "result"    # Ljava/lang/StringBuffer;
    .param p4, "fieldPosition"    # Ljava/text/FieldPosition;
    .param p5, "parseAttr"    # Z

    .prologue
    .line 702
    const/4 v6, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 703
    const/4 v6, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 705
    invoke-static/range {p1 .. p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 707
    invoke-virtual/range {p4 .. p4}, Ljava/text/FieldPosition;->getField()I

    move-result v6

    if-nez v6, :cond_0

    .line 708
    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 711
    :cond_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v6}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getNaN()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 716
    if-eqz p5, :cond_1

    .line 717
    sget-object v6, Lcom/ibm/icu/text/NumberFormat$Field;->INTEGER:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v8}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getNaN()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v8}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    .line 721
    :cond_1
    invoke-virtual/range {p4 .. p4}, Ljava/text/FieldPosition;->getField()I

    move-result v6

    if-nez v6, :cond_2

    .line 722
    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 725
    :cond_2
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2, v6, v7}, Lcom/ibm/icu/text/DecimalFormat;->addPadding(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;II)V

    .line 786
    .end local p3    # "result":Ljava/lang/StringBuffer;
    :goto_0
    return-object p3

    .line 730
    .restart local p3    # "result":Ljava/lang/StringBuffer;
    :cond_3
    move-object/from16 v0, p0

    iget v6, v0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    int-to-double v6, v6

    mul-double p1, p1, v6

    .line 742
    :cond_4
    const-wide/16 v6, 0x0

    cmpg-double v6, p1, v6

    if-ltz v6, :cond_5

    const-wide/16 v6, 0x0

    cmpl-double v6, p1, v6

    if-nez v6, :cond_c

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    div-double v6, v6, p1

    const-wide/16 v8, 0x0

    cmpg-double v6, v6, v8

    if-gez v6, :cond_c

    :cond_5
    const/4 v13, 0x1

    .line 743
    .local v13, "isNegative":Z
    :goto_1
    if-eqz v13, :cond_6

    move-wide/from16 v0, p1

    neg-double v0, v0

    move-wide/from16 p1, v0

    .line 746
    :cond_6
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/ibm/icu/text/DecimalFormat;->roundingDouble:D

    const-wide/16 v8, 0x0

    cmpl-double v6, v6, v8

    if-lez v6, :cond_8

    .line 749
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/ibm/icu/text/DecimalFormat;->roundingDouble:D

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/ibm/icu/text/DecimalFormat;->roundingDoubleReciprocal:D

    move-object/from16 v0, p0

    iget v12, v0, Lcom/ibm/icu/text/DecimalFormat;->roundingMode:I

    move-wide/from16 v6, p1

    invoke-static/range {v6 .. v13}, Lcom/ibm/icu/text/DecimalFormat;->round(DDDIZ)D

    move-result-wide v16

    .line 750
    .local v16, "newNumber":D
    const-wide/16 v6, 0x0

    cmpl-double v6, v16, v6

    if-nez v6, :cond_7

    cmpl-double v6, p1, v16

    if-eqz v6, :cond_7

    const/4 v13, 0x0

    .line 751
    :cond_7
    move-wide/from16 p1, v16

    .line 754
    .end local v16    # "newNumber":D
    :cond_8
    invoke-static/range {p1 .. p2}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 756
    const/4 v6, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, p5

    invoke-direct {v0, v1, v13, v6, v2}, Lcom/ibm/icu/text/DecimalFormat;->appendAffix(Ljava/lang/StringBuffer;ZZZ)I

    move-result v18

    .line 758
    .local v18, "prefixLen":I
    invoke-virtual/range {p4 .. p4}, Ljava/text/FieldPosition;->getField()I

    move-result v6

    if-nez v6, :cond_9

    .line 759
    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 763
    :cond_9
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v6}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getInfinity()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 766
    if-eqz p5, :cond_a

    .line 767
    sget-object v6, Lcom/ibm/icu/text/NumberFormat$Field;->INTEGER:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v8}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getInfinity()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v8}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    .line 771
    :cond_a
    invoke-virtual/range {p4 .. p4}, Ljava/text/FieldPosition;->getField()I

    move-result v6

    if-nez v6, :cond_b

    .line 772
    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 775
    :cond_b
    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, p5

    invoke-direct {v0, v1, v13, v6, v2}, Lcom/ibm/icu/text/DecimalFormat;->appendAffix(Ljava/lang/StringBuffer;ZZZ)I

    move-result v19

    .line 777
    .local v19, "suffixLen":I
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move/from16 v3, v18

    move/from16 v4, v19

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/DecimalFormat;->addPadding(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;II)V

    goto/16 :goto_0

    .line 742
    .end local v13    # "isNegative":Z
    .end local v18    # "prefixLen":I
    .end local v19    # "suffixLen":I
    :cond_c
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 783
    .restart local v13    # "isNegative":Z
    :cond_d
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    monitor-enter v7

    .line 784
    :try_start_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/ibm/icu/text/DecimalFormat;->precision(Z)I

    move-result v9

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    if-nez v6, :cond_e

    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->areSignificantDigitsUsed()Z

    move-result v6

    if-nez v6, :cond_e

    const/4 v6, 0x1

    :goto_2
    move-wide/from16 v0, p1

    invoke-virtual {v8, v0, v1, v9, v6}, Lcom/ibm/icu/text/DigitList;->set(DIZ)V

    .line 786
    const/4 v14, 0x0

    move-object/from16 v10, p0

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move/from16 v15, p5

    invoke-direct/range {v10 .. v15}, Lcom/ibm/icu/text/DecimalFormat;->subformat(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;ZZZ)Ljava/lang/StringBuffer;

    move-result-object p3

    .end local p3    # "result":Ljava/lang/StringBuffer;
    monitor-exit v7

    goto/16 :goto_0

    .line 788
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 784
    .restart local p3    # "result":Ljava/lang/StringBuffer;
    :cond_e
    const/4 v6, 0x0

    goto :goto_2
.end method

.method private format(JLjava/lang/StringBuffer;Ljava/text/FieldPosition;Z)Ljava/lang/StringBuffer;
    .locals 15
    .param p1, "number"    # J
    .param p3, "result"    # Ljava/lang/StringBuffer;
    .param p4, "fieldPosition"    # Ljava/text/FieldPosition;
    .param p5, "parseAttr"    # Z

    .prologue
    .line 896
    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 897
    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 902
    iget-object v4, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    if-eqz v4, :cond_0

    .line 903
    invoke-static/range {p1 .. p2}, Lcom/ibm/icu/math/BigDecimal;->valueOf(J)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v4

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {p0, v4, v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->format(Lcom/ibm/icu/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v4

    .line 938
    :goto_0
    return-object v4

    .line 906
    :cond_0
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-gez v4, :cond_2

    const/4 v7, 0x1

    .line 907
    .local v7, "isNegative":Z
    :goto_1
    if-eqz v7, :cond_1

    move-wide/from16 v0, p1

    neg-long v0, v0

    move-wide/from16 p1, v0

    .line 916
    :cond_1
    iget v4, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_7

    .line 917
    const/4 v12, 0x0

    .line 918
    .local v12, "tooBig":Z
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-gez v4, :cond_4

    .line 919
    const-wide/high16 v4, -0x8000000000000000L

    iget v6, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    int-to-long v8, v6

    div-long v10, v4, v8

    .line 920
    .local v10, "cutoff":J
    cmp-long v4, p1, v10

    if-gtz v4, :cond_3

    const/4 v12, 0x1

    .line 925
    :goto_2
    if-eqz v12, :cond_7

    .line 929
    if-eqz v7, :cond_6

    move-wide/from16 v0, p1

    neg-long v4, v0

    :goto_3
    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v4

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    move/from16 v2, p5

    invoke-direct {p0, v4, v0, v1, v2}, Lcom/ibm/icu/text/DecimalFormat;->format(Ljava/math/BigInteger;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Z)Ljava/lang/StringBuffer;

    move-result-object v4

    goto :goto_0

    .line 906
    .end local v7    # "isNegative":Z
    .end local v10    # "cutoff":J
    .end local v12    # "tooBig":Z
    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    .line 920
    .restart local v7    # "isNegative":Z
    .restart local v10    # "cutoff":J
    .restart local v12    # "tooBig":Z
    :cond_3
    const/4 v12, 0x0

    goto :goto_2

    .line 922
    .end local v10    # "cutoff":J
    :cond_4
    const-wide v4, 0x7fffffffffffffffL

    iget v6, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    int-to-long v8, v6

    div-long v10, v4, v8

    .line 923
    .restart local v10    # "cutoff":J
    cmp-long v4, p1, v10

    if-lez v4, :cond_5

    const/4 v12, 0x1

    :goto_4
    goto :goto_2

    :cond_5
    const/4 v12, 0x0

    goto :goto_4

    :cond_6
    move-wide/from16 v4, p1

    .line 929
    goto :goto_3

    .line 935
    .end local v10    # "cutoff":J
    .end local v12    # "tooBig":Z
    :cond_7
    iget v4, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    int-to-long v4, v4

    mul-long p1, p1, v4

    .line 936
    iget-object v13, p0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    monitor-enter v13

    .line 937
    :try_start_0
    iget-object v4, p0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    const/4 v5, 0x1

    invoke-direct {p0, v5}, Lcom/ibm/icu/text/DecimalFormat;->precision(Z)I

    move-result v5

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1, v5}, Lcom/ibm/icu/text/DigitList;->set(JI)V

    .line 938
    const/4 v8, 0x1

    move-object v4, p0

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v9, p5

    invoke-direct/range {v4 .. v9}, Lcom/ibm/icu/text/DecimalFormat;->subformat(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;ZZZ)Ljava/lang/StringBuffer;

    move-result-object v4

    monitor-exit v13

    goto :goto_0

    .line 939
    :catchall_0
    move-exception v4

    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method private format(Ljava/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Z)Ljava/lang/StringBuffer;
    .locals 7
    .param p1, "number"    # Ljava/math/BigDecimal;
    .param p2, "result"    # Ljava/lang/StringBuffer;
    .param p3, "fieldPosition"    # Ljava/text/FieldPosition;
    .param p4, "parseAttr"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 988
    iget v1, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    if-eq v1, v3, :cond_0

    .line 989
    iget v1, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    int-to-long v4, v1

    invoke-static {v4, v5}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 992
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrement:Ljava/math/BigDecimal;

    if-eqz v1, :cond_1

    .line 993
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrement:Ljava/math/BigDecimal;

    iget v2, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingMode:I

    invoke-virtual {p1, v1, v0, v2}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;II)Ljava/math/BigDecimal;

    move-result-object v1

    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrement:Ljava/math/BigDecimal;

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 997
    :cond_1
    iget-object v6, p0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    monitor-enter v6

    .line 998
    :try_start_0
    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/DecimalFormat;->precision(Z)I

    move-result v4

    iget-boolean v1, p0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/ibm/icu/text/DecimalFormat;->areSignificantDigitsUsed()Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v3

    :goto_0
    invoke-virtual {v2, p1, v4, v1}, Lcom/ibm/icu/text/DigitList;->set(Ljava/math/BigDecimal;IZ)V

    .line 1000
    invoke-virtual {p1}, Ljava/math/BigDecimal;->signum()I

    move-result v1

    if-gez v1, :cond_3

    :goto_1
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/DecimalFormat;->subformat(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;ZZZ)Ljava/lang/StringBuffer;

    move-result-object v0

    monitor-exit v6

    return-object v0

    :cond_2
    move v1, v0

    .line 998
    goto :goto_0

    :cond_3
    move v3, v0

    .line 1000
    goto :goto_1

    .line 1001
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private format(Ljava/math/BigInteger;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Z)Ljava/lang/StringBuffer;
    .locals 7
    .param p1, "number"    # Ljava/math/BigInteger;
    .param p2, "result"    # Ljava/lang/StringBuffer;
    .param p3, "fieldPosition"    # Ljava/text/FieldPosition;
    .param p4, "parseAttr"    # Z

    .prologue
    const/4 v3, 0x1

    .line 958
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    if-eqz v0, :cond_0

    .line 959
    new-instance v0, Lcom/ibm/icu/math/BigDecimal;

    invoke-direct {v0, p1}, Lcom/ibm/icu/math/BigDecimal;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {p0, v0, p2, p3}, Lcom/ibm/icu/text/DecimalFormat;->format(Lcom/ibm/icu/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 970
    :goto_0
    return-object v0

    .line 962
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    if-eq v0, v3, :cond_1

    .line 963
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p1

    .line 968
    :cond_1
    iget-object v6, p0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    monitor-enter v6

    .line 969
    :try_start_0
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/DecimalFormat;->precision(Z)I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/ibm/icu/text/DigitList;->set(Ljava/math/BigInteger;I)V

    .line 970
    invoke-virtual {p1}, Ljava/math/BigInteger;->signum()I

    move-result v0

    if-gez v0, :cond_2

    :goto_1
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/DecimalFormat;->subformat(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;ZZZ)Ljava/lang/StringBuffer;

    move-result-object v0

    monitor-exit v6

    goto :goto_0

    .line 971
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 970
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private formatAffix2Attribute(Ljava/lang/String;II)V
    .locals 2
    .param p1, "affix"    # Ljava/lang/String;
    .param p2, "begin"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v1, -0x1

    .line 3422
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v0}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getCurrencySymbol()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-le v0, v1, :cond_1

    .line 3423
    sget-object v0, Lcom/ibm/icu/text/NumberFormat$Field;->CURRENCY:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-direct {p0, v0, p2, p3}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    .line 3431
    :cond_0
    :goto_0
    return-void

    .line 3424
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v0}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getMinusSign()C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-le v0, v1, :cond_2

    .line 3425
    sget-object v0, Lcom/ibm/icu/text/NumberFormat$Field;->SIGN:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-direct {p0, v0, p2, p3}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    goto :goto_0

    .line 3426
    :cond_2
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v0}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPercent()C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-le v0, v1, :cond_3

    .line 3427
    sget-object v0, Lcom/ibm/icu/text/NumberFormat$Field;->PERCENT:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-direct {p0, v0, p2, p3}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    goto :goto_0

    .line 3428
    :cond_3
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v0}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPerMill()C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-le v0, v1, :cond_0

    .line 3429
    sget-object v0, Lcom/ibm/icu/text/NumberFormat$Field;->PERMILLE:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-direct {p0, v0, p2, p3}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    goto :goto_0
.end method

.method private getSimilarDecimals(CZ)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p1, "decimal"    # C
    .param p2, "strictParse"    # Z

    .prologue
    .line 2151
    sget-object v0, Lcom/ibm/icu/text/DecimalFormat;->dotEquivalents:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2152
    if-eqz p2, :cond_0

    sget-object v0, Lcom/ibm/icu/text/DecimalFormat;->strictDotEquivalents:Lcom/ibm/icu/text/UnicodeSet;

    .line 2158
    :goto_0
    return-object v0

    .line 2152
    :cond_0
    sget-object v0, Lcom/ibm/icu/text/DecimalFormat;->dotEquivalents:Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0

    .line 2154
    :cond_1
    sget-object v0, Lcom/ibm/icu/text/DecimalFormat;->commaEquivalents:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2155
    if-eqz p2, :cond_2

    sget-object v0, Lcom/ibm/icu/text/DecimalFormat;->strictCommaEquivalents:Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/ibm/icu/text/DecimalFormat;->commaEquivalents:Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0

    .line 2158
    :cond_3
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    goto :goto_0
.end method

.method private isGroupingPosition(I)Z
    .locals 5
    .param p1, "pos"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1045
    const/4 v0, 0x0

    .line 1046
    .local v0, "result":Z
    invoke-virtual {p0}, Lcom/ibm/icu/text/DecimalFormat;->isGroupingUsed()Z

    move-result v3

    if-eqz v3, :cond_0

    if-lez p1, :cond_0

    iget-byte v3, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    if-lez v3, :cond_0

    .line 1047
    iget-byte v3, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    if-lez v3, :cond_2

    iget-byte v3, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    if-le p1, v3, :cond_2

    .line 1048
    iget-byte v3, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    sub-int v3, p1, v3

    iget-byte v4, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    rem-int/2addr v3, v4

    if-nez v3, :cond_1

    move v0, v1

    .line 1053
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v2

    .line 1048
    goto :goto_0

    .line 1050
    :cond_2
    iget-byte v3, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    rem-int v3, p1, v3

    if-nez v3, :cond_3

    move v0, v1

    :goto_1
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method static final match(Ljava/lang/String;II)I
    .locals 3
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "pos"    # I
    .param p2, "ch"    # I

    .prologue
    const/4 v1, -0x1

    .line 2414
    invoke-static {p2}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2417
    move v0, p1

    .line 2418
    .local v0, "s":I
    invoke-static {p0, p1}, Lcom/ibm/icu/text/DecimalFormat;->skipUWhiteSpace(Ljava/lang/String;I)I

    move-result p1

    .line 2419
    if-ne p1, v0, :cond_1

    .line 2424
    .end local v0    # "s":I
    :cond_0
    :goto_0
    return v1

    .restart local v0    # "s":I
    :cond_1
    move v1, p1

    .line 2422
    goto :goto_0

    .line 2424
    .end local v0    # "s":I
    :cond_2
    if-ltz p1, :cond_0

    invoke-static {p0, p1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, p2, :cond_0

    invoke-static {p2}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v1

    add-int/2addr v1, p1

    goto :goto_0
.end method

.method static final match(Ljava/lang/String;ILjava/lang/String;)I
    .locals 3
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "pos"    # I
    .param p2, "str"    # Ljava/lang/String;

    .prologue
    .line 2434
    const/4 v1, 0x0

    .local v1, "i":I
    :cond_0
    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    if-ltz p1, :cond_1

    .line 2435
    invoke-static {p2, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 2436
    .local v0, "ch":I
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 2437
    invoke-static {p0, p1, v0}, Lcom/ibm/icu/text/DecimalFormat;->match(Ljava/lang/String;II)I

    move-result p1

    .line 2438
    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2439
    invoke-static {p2, v1}, Lcom/ibm/icu/text/DecimalFormat;->skipRuleWhiteSpace(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0

    .line 2442
    .end local v0    # "ch":I
    :cond_1
    return p1
.end method

.method private parse(Ljava/lang/String;Ljava/text/ParsePosition;Z)Ljava/lang/Object;
    .locals 18
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "parsePosition"    # Ljava/text/ParsePosition;
    .param p3, "parseCurrency"    # Z

    .prologue
    .line 1621
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v9

    .local v9, "backup":I
    move v11, v9

    .line 1626
    .local v11, "i":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    if-lez v2, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 1628
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11}, Lcom/ibm/icu/text/DecimalFormat;->skipPadding(Ljava/lang/String;I)I

    move-result v11

    .line 1630
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getNaN()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v4}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getNaN()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v2, v3, v4}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1632
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getNaN()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v11, v2

    .line 1634
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    if-lez v2, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    .line 1636
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11}, Lcom/ibm/icu/text/DecimalFormat;->skipPadding(Ljava/lang/String;I)I

    move-result v11

    .line 1638
    :cond_3
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1639
    new-instance v15, Ljava/lang/Double;

    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    invoke-direct {v15, v2, v3}, Ljava/lang/Double;-><init>(D)V

    .line 1716
    :cond_4
    :goto_0
    return-object v15

    .line 1643
    :cond_5
    move v11, v9

    .line 1645
    const/4 v2, 0x3

    new-array v7, v2, [Z

    .line 1646
    .local v7, "status":[Z
    if-eqz p3, :cond_6

    const/4 v2, 0x1

    new-array v8, v2, [Lcom/ibm/icu/util/Currency;

    .line 1647
    .local v8, "currency":[Lcom/ibm/icu/util/Currency;
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    const/4 v6, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v8}, Lcom/ibm/icu/text/DecimalFormat;->subparse(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/DigitList;Z[Z[Lcom/ibm/icu/util/Currency;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1648
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1649
    const/4 v15, 0x0

    goto :goto_0

    .line 1646
    .end local v8    # "currency":[Lcom/ibm/icu/util/Currency;
    :cond_6
    const/4 v8, 0x0

    goto :goto_1

    .line 1652
    .restart local v8    # "currency":[Lcom/ibm/icu/util/Currency;
    :cond_7
    const/4 v15, 0x0

    .line 1655
    .local v15, "n":Ljava/lang/Number;
    const/4 v2, 0x0

    aget-boolean v2, v7, v2

    if-eqz v2, :cond_a

    .line 1656
    new-instance v15, Ljava/lang/Double;

    .end local v15    # "n":Ljava/lang/Number;
    const/4 v2, 0x1

    aget-boolean v2, v7, v2

    if-eqz v2, :cond_9

    const-wide/high16 v2, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    :goto_2
    invoke-direct {v15, v2, v3}, Ljava/lang/Double;-><init>(D)V

    .line 1716
    .restart local v15    # "n":Ljava/lang/Number;
    :cond_8
    :goto_3
    if-eqz p3, :cond_4

    new-instance v2, Lcom/ibm/icu/util/CurrencyAmount;

    const/4 v3, 0x0

    aget-object v3, v8, v3

    invoke-direct {v2, v15, v3}, Lcom/ibm/icu/util/CurrencyAmount;-><init>(Ljava/lang/Number;Lcom/ibm/icu/util/Currency;)V

    move-object v15, v2

    goto :goto_0

    .line 1656
    .end local v15    # "n":Ljava/lang/Number;
    :cond_9
    const-wide/high16 v2, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    goto :goto_2

    .line 1662
    .restart local v15    # "n":Ljava/lang/Number;
    :cond_a
    const/4 v2, 0x2

    aget-boolean v2, v7, v2

    if-eqz v2, :cond_c

    .line 1663
    const/4 v2, 0x1

    aget-boolean v2, v7, v2

    if-eqz v2, :cond_b

    new-instance v15, Ljava/lang/Double;

    .end local v15    # "n":Ljava/lang/Number;
    const-string/jumbo v2, "0.0"

    invoke-direct {v15, v2}, Ljava/lang/Double;-><init>(Ljava/lang/String;)V

    .line 1664
    .restart local v15    # "n":Ljava/lang/Number;
    :goto_4
    goto :goto_3

    .line 1663
    :cond_b
    new-instance v15, Ljava/lang/Double;

    .end local v15    # "n":Ljava/lang/Number;
    const-string/jumbo v2, "-0.0"

    invoke-direct {v15, v2}, Ljava/lang/Double;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 1667
    .restart local v15    # "n":Ljava/lang/Number;
    :cond_c
    const/4 v2, 0x1

    aget-boolean v2, v7, v2

    if-nez v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DigitList;->isZero()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1668
    new-instance v15, Ljava/lang/Double;

    .end local v15    # "n":Ljava/lang/Number;
    const-string/jumbo v2, "-0.0"

    invoke-direct {v15, v2}, Ljava/lang/Double;-><init>(Ljava/lang/String;)V

    .line 1669
    .restart local v15    # "n":Ljava/lang/Number;
    goto :goto_3

    .line 1674
    :cond_d
    move-object/from16 v0, p0

    iget v14, v0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    .line 1675
    .local v14, "mult":I
    :goto_5
    rem-int/lit8 v2, v14, 0xa

    if-nez v2, :cond_e

    .line 1676
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    iget v3, v2, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 1677
    div-int/lit8 v14, v14, 0xa

    .line 1678
    goto :goto_5

    .line 1681
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/ibm/icu/text/DecimalFormat;->parseBigDecimal:Z

    if-nez v2, :cond_14

    const/4 v2, 0x1

    if-ne v14, v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DigitList;->isIntegral()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1683
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    iget v2, v2, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    const/16 v3, 0xc

    if-ge v2, v3, :cond_12

    .line 1684
    const-wide/16 v12, 0x0

    .line 1685
    .local v12, "l":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    iget v2, v2, Lcom/ibm/icu/text/DigitList;->count:I

    if-lez v2, :cond_11

    .line 1686
    const/16 v16, 0x0

    .line 1687
    .local v16, "nx":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    iget v2, v2, Lcom/ibm/icu/text/DigitList;->count:I

    move/from16 v0, v16

    if-ge v0, v2, :cond_f

    .line 1688
    const-wide/16 v2, 0xa

    mul-long/2addr v2, v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    iget-object v4, v4, Lcom/ibm/icu/text/DigitList;->digits:[B

    add-int/lit8 v17, v16, 0x1

    .end local v16    # "nx":I
    .local v17, "nx":I
    aget-byte v4, v4, v16

    int-to-char v4, v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    const-wide/16 v4, 0x30

    sub-long v12, v2, v4

    move/from16 v16, v17

    .line 1689
    .end local v17    # "nx":I
    .restart local v16    # "nx":I
    goto :goto_6

    .line 1690
    :cond_f
    :goto_7
    add-int/lit8 v17, v16, 0x1

    .end local v16    # "nx":I
    .restart local v17    # "nx":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    iget v2, v2, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    move/from16 v0, v16

    if-ge v0, v2, :cond_10

    .line 1691
    const-wide/16 v2, 0xa

    mul-long/2addr v12, v2

    move/from16 v16, v17

    .line 1692
    .end local v17    # "nx":I
    .restart local v16    # "nx":I
    goto :goto_7

    .line 1693
    .end local v16    # "nx":I
    .restart local v17    # "nx":I
    :cond_10
    const/4 v2, 0x1

    aget-boolean v2, v7, v2

    if-nez v2, :cond_11

    .line 1694
    neg-long v12, v12

    .line 1697
    .end local v17    # "nx":I
    :cond_11
    new-instance v15, Ljava/lang/Long;

    .end local v15    # "n":Ljava/lang/Number;
    invoke-direct {v15, v12, v13}, Ljava/lang/Long;-><init>(J)V

    .line 1698
    .restart local v15    # "n":Ljava/lang/Number;
    goto/16 :goto_3

    .line 1699
    .end local v12    # "l":J
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    const/4 v3, 0x1

    aget-boolean v3, v7, v3

    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/DigitList;->getBigInteger(Z)Ljava/math/BigInteger;

    move-result-object v10

    .line 1700
    .local v10, "big":Ljava/math/BigInteger;
    invoke-virtual {v10}, Ljava/math/BigInteger;->bitLength()I

    move-result v2

    const/16 v3, 0x40

    if-ge v2, v3, :cond_13

    new-instance v15, Ljava/lang/Long;

    .end local v15    # "n":Ljava/lang/Number;
    invoke-virtual {v10}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v2

    invoke-direct {v15, v2, v3}, Ljava/lang/Long;-><init>(J)V

    .line 1703
    .restart local v15    # "n":Ljava/lang/Number;
    :goto_8
    goto/16 :goto_3

    :cond_13
    move-object v15, v10

    .line 1700
    goto :goto_8

    .line 1706
    .end local v10    # "big":Ljava/math/BigInteger;
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    const/4 v3, 0x1

    aget-boolean v3, v7, v3

    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/DigitList;->getBigDecimalICU(Z)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v10

    .line 1707
    .local v10, "big":Lcom/ibm/icu/math/BigDecimal;
    move-object v15, v10

    .line 1708
    const/4 v2, 0x1

    if-eq v14, v2, :cond_8

    .line 1709
    int-to-long v2, v14

    invoke-static {v2, v3}, Lcom/ibm/icu/math/BigDecimal;->valueOf(J)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v10, v2, v3}, Lcom/ibm/icu/math/BigDecimal;->divide(Lcom/ibm/icu/math/BigDecimal;I)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v15

    goto/16 :goto_3
.end method

.method private patternError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "pattern"    # Ljava/lang/String;

    .prologue
    .line 4289
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " in pattern \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const/16 v2, 0x22

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private precision(Z)I
    .locals 2
    .param p1, "isIntegral"    # Z

    .prologue
    .line 1062
    invoke-virtual {p0}, Lcom/ibm/icu/text/DecimalFormat;->areSignificantDigitsUsed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1063
    invoke-virtual {p0}, Lcom/ibm/icu/text/DecimalFormat;->getMaximumSignificantDigits()I

    move-result v0

    .line 1067
    :goto_0
    return v0

    .line 1064
    :cond_0
    iget-boolean v0, p0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    if-eqz v0, :cond_1

    .line 1065
    invoke-virtual {p0}, Lcom/ibm/icu/text/DecimalFormat;->getMinimumIntegerDigits()I

    move-result v0

    invoke-virtual {p0}, Lcom/ibm/icu/text/DecimalFormat;->getMaximumFractionDigits()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    .line 1067
    :cond_1
    if-eqz p1, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/ibm/icu/text/DecimalFormat;->getMaximumFractionDigits()I

    move-result v0

    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 5
    .param p1, "stream"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x154

    const/16 v1, 0x135

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 4513
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 4528
    invoke-virtual {p0}, Lcom/ibm/icu/text/DecimalFormat;->getMaximumIntegerDigits()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 4529
    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setMaximumIntegerDigits(I)V

    .line 4531
    :cond_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/DecimalFormat;->getMaximumFractionDigits()I

    move-result v0

    if-le v0, v4, :cond_1

    .line 4532
    invoke-virtual {p0, v4}, Lcom/ibm/icu/text/DecimalFormat;->setMaximumFractionDigits(I)V

    .line 4534
    :cond_1
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormat;->serialVersionOnStream:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_2

    .line 4535
    iput-boolean v2, p0, Lcom/ibm/icu/text/DecimalFormat;->exponentSignAlwaysShown:Z

    .line 4536
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DecimalFormat;->setInternalRoundingIncrement(Lcom/ibm/icu/math/BigDecimal;)V

    .line 4537
    invoke-direct {p0}, Lcom/ibm/icu/text/DecimalFormat;->setRoundingDouble()V

    .line 4538
    const/4 v0, 0x6

    iput v0, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingMode:I

    .line 4539
    iput v2, p0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    .line 4540
    const/16 v0, 0x20

    iput-char v0, p0, Lcom/ibm/icu/text/DecimalFormat;->pad:C

    .line 4541
    iput v2, p0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    .line 4542
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormat;->serialVersionOnStream:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_2

    .line 4544
    iput-boolean v2, p0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    .line 4547
    :cond_2
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormat;->serialVersionOnStream:I

    if-ge v0, v3, :cond_3

    .line 4550
    invoke-direct {p0}, Lcom/ibm/icu/text/DecimalFormat;->setCurrencyForSymbols()V

    .line 4552
    :cond_3
    iput v3, p0, Lcom/ibm/icu/text/DecimalFormat;->serialVersionOnStream:I

    .line 4553
    new-instance v0, Lcom/ibm/icu/text/DigitList;

    invoke-direct {v0}, Lcom/ibm/icu/text/DigitList;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    .line 4557
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrement:Ljava/math/BigDecimal;

    if-eqz v0, :cond_4

    .line 4558
    new-instance v0, Lcom/ibm/icu/math/BigDecimal;

    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrement:Ljava/math/BigDecimal;

    invoke-direct {v0, v1}, Lcom/ibm/icu/math/BigDecimal;-><init>(Ljava/math/BigDecimal;)V

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DecimalFormat;->setInternalRoundingIncrement(Lcom/ibm/icu/math/BigDecimal;)V

    .line 4559
    invoke-direct {p0}, Lcom/ibm/icu/text/DecimalFormat;->setRoundingDouble()V

    .line 4562
    :cond_4
    return-void
.end method

.method private static round(DDDIZ)D
    .locals 18
    .param p0, "number"    # D
    .param p2, "roundingInc"    # D
    .param p4, "roundingIncReciprocal"    # D
    .param p6, "mode"    # I
    .param p7, "isNegative"    # Z

    .prologue
    .line 810
    const-wide/16 v14, 0x0

    cmpl-double v14, p4, v14

    if-nez v14, :cond_0

    div-double v6, p0, p2

    .line 816
    .local v6, "div":D
    :goto_0
    packed-switch p6, :pswitch_data_0

    .line 842
    :pswitch_0
    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    .line 843
    .local v2, "ceil":D
    sub-double v4, v2, v6

    .line 844
    .local v4, "ceildiff":D
    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    .line 845
    .local v8, "floor":D
    sub-double v10, v6, v8

    .line 853
    .local v10, "floordiff":D
    packed-switch p6, :pswitch_data_1

    .line 874
    new-instance v14, Ljava/lang/IllegalArgumentException;

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v16, "Invalid rounding mode: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    move/from16 v0, p6

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 810
    .end local v2    # "ceil":D
    .end local v4    # "ceildiff":D
    .end local v6    # "div":D
    .end local v8    # "floor":D
    .end local v10    # "floordiff":D
    :cond_0
    mul-double v6, p0, p4

    goto :goto_0

    .line 818
    .restart local v6    # "div":D
    :pswitch_1
    if-eqz p7, :cond_2

    sget-wide v14, Lcom/ibm/icu/text/DecimalFormat;->epsilon:D

    add-double/2addr v14, v6

    invoke-static {v14, v15}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    .line 877
    :goto_1
    const-wide/16 v14, 0x0

    cmpl-double v14, p4, v14

    if-nez v14, :cond_9

    mul-double p0, v6, p2

    .line 880
    :cond_1
    :goto_2
    return-wide p0

    .line 818
    :cond_2
    sget-wide v14, Lcom/ibm/icu/text/DecimalFormat;->epsilon:D

    sub-double v14, v6, v14

    invoke-static {v14, v15}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    goto :goto_1

    .line 821
    :pswitch_2
    if-eqz p7, :cond_3

    sget-wide v14, Lcom/ibm/icu/text/DecimalFormat;->epsilon:D

    sub-double v14, v6, v14

    invoke-static {v14, v15}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    .line 822
    :goto_3
    goto :goto_1

    .line 821
    :cond_3
    sget-wide v14, Lcom/ibm/icu/text/DecimalFormat;->epsilon:D

    add-double/2addr v14, v6

    invoke-static {v14, v15}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    goto :goto_3

    .line 824
    :pswitch_3
    sget-wide v14, Lcom/ibm/icu/text/DecimalFormat;->epsilon:D

    add-double/2addr v14, v6

    invoke-static {v14, v15}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    .line 825
    goto :goto_1

    .line 827
    :pswitch_4
    sget-wide v14, Lcom/ibm/icu/text/DecimalFormat;->epsilon:D

    sub-double v14, v6, v14

    invoke-static {v14, v15}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    .line 828
    goto :goto_1

    .line 830
    :pswitch_5
    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v14

    cmpl-double v14, v6, v14

    if-eqz v14, :cond_1

    .line 831
    new-instance v14, Ljava/lang/ArithmeticException;

    const-string/jumbo v15, "Rounding necessary"

    invoke-direct {v14, v15}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 858
    .restart local v2    # "ceil":D
    .restart local v4    # "ceildiff":D
    .restart local v8    # "floor":D
    .restart local v10    # "floordiff":D
    :pswitch_6
    sget-wide v14, Lcom/ibm/icu/text/DecimalFormat;->epsilon:D

    add-double/2addr v14, v10

    cmpg-double v14, v14, v4

    if-gez v14, :cond_4

    .line 859
    move-wide v6, v8

    .line 860
    goto :goto_1

    :cond_4
    sget-wide v14, Lcom/ibm/icu/text/DecimalFormat;->epsilon:D

    add-double/2addr v14, v4

    cmpg-double v14, v14, v10

    if-gez v14, :cond_5

    .line 861
    move-wide v6, v2

    .line 862
    goto :goto_1

    .line 863
    :cond_5
    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double v12, v8, v14

    .line 864
    .local v12, "testFloor":D
    invoke-static {v12, v13}, Ljava/lang/Math;->floor(D)D

    move-result-wide v14

    cmpl-double v14, v12, v14

    if-nez v14, :cond_6

    move-wide v6, v8

    .line 866
    :goto_4
    goto :goto_1

    :cond_6
    move-wide v6, v2

    .line 864
    goto :goto_4

    .line 868
    .end local v12    # "testFloor":D
    :pswitch_7
    sget-wide v14, Lcom/ibm/icu/text/DecimalFormat;->epsilon:D

    add-double/2addr v14, v4

    cmpg-double v14, v10, v14

    if-gtz v14, :cond_7

    move-wide v6, v8

    .line 869
    :goto_5
    goto :goto_1

    :cond_7
    move-wide v6, v2

    .line 868
    goto :goto_5

    .line 871
    :pswitch_8
    sget-wide v14, Lcom/ibm/icu/text/DecimalFormat;->epsilon:D

    add-double/2addr v14, v10

    cmpg-double v14, v4, v14

    if-gtz v14, :cond_8

    move-wide v6, v2

    .line 872
    :goto_6
    goto :goto_1

    :cond_8
    move-wide v6, v8

    .line 871
    goto :goto_6

    .line 877
    .end local v2    # "ceil":D
    .end local v4    # "ceildiff":D
    .end local v8    # "floor":D
    .end local v10    # "floordiff":D
    :cond_9
    div-double p0, v6, p4

    goto :goto_2

    .line 816
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 853
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method private setCurrencyForSymbols()V
    .locals 3

    .prologue
    .line 2492
    new-instance v0, Lcom/ibm/icu/text/DecimalFormatSymbols;

    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v1}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 2495
    .local v0, "def":Lcom/ibm/icu/text/DecimalFormatSymbols;
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v1}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getCurrencySymbol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getCurrencySymbol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v1}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getInternationalCurrencySymbol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getInternationalCurrencySymbol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2499
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v1}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Lcom/ibm/icu/util/Currency;->getInstance(Ljava/util/Locale;)Lcom/ibm/icu/util/Currency;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setCurrency(Lcom/ibm/icu/util/Currency;)V

    .line 2503
    :goto_0
    return-void

    .line 2501
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setCurrency(Lcom/ibm/icu/util/Currency;)V

    goto :goto_0
.end method

.method private setInternalRoundingIncrement(Lcom/ibm/icu/math/BigDecimal;)V
    .locals 1
    .param p1, "value"    # Lcom/ibm/icu/math/BigDecimal;

    .prologue
    .line 4566
    iput-object p1, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    .line 4569
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrement:Ljava/math/BigDecimal;

    .line 4571
    return-void

    .line 4569
    :cond_0
    invoke-virtual {p1}, Lcom/ibm/icu/math/BigDecimal;->toBigDecimal()Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0
.end method

.method private setRoundingDouble()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 4279
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    if-nez v0, :cond_0

    .line 4280
    iput-wide v2, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDouble:D

    .line 4281
    iput-wide v2, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDoubleReciprocal:D

    .line 4286
    :goto_0
    return-void

    .line 4283
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    invoke-virtual {v0}, Lcom/ibm/icu/math/BigDecimal;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDouble:D

    .line 4284
    sget-object v0, Lcom/ibm/icu/math/BigDecimal;->ONE:Lcom/ibm/icu/math/BigDecimal;

    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/math/BigDecimal;->divide(Lcom/ibm/icu/math/BigDecimal;I)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/math/BigDecimal;->doubleValue()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setRoundingDoubleReciprocal(D)V

    goto :goto_0
.end method

.method private setRoundingDoubleReciprocal(D)V
    .locals 5
    .param p1, "rawRoundedReciprocal"    # D

    .prologue
    .line 2711
    invoke-static {p1, p2}, Ljava/lang/Math;->rint(D)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDoubleReciprocal:D

    .line 2712
    iget-wide v0, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDoubleReciprocal:D

    sub-double v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x3e112e0be826d695L    # 1.0E-9

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 2713
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDoubleReciprocal:D

    .line 2715
    :cond_0
    return-void
.end method

.method private final skipPadding(Ljava/lang/String;I)I
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "position"    # I

    .prologue
    .line 2167
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p2, v0, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iget-char v1, p0, Lcom/ibm/icu/text/DecimalFormat;->pad:C

    if-ne v0, v1, :cond_0

    .line 2168
    add-int/lit8 p2, p2, 0x1

    .line 2169
    goto :goto_0

    .line 2170
    :cond_0
    return p2
.end method

.method private static skipRuleWhiteSpace(Ljava/lang/String;I)I
    .locals 2
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "pos"    # I

    .prologue
    .line 2280
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 2281
    invoke-static {p0, p1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 2282
    .local v0, "c":I
    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2287
    .end local v0    # "c":I
    :cond_0
    return p1

    .line 2285
    .restart local v0    # "c":I
    :cond_1
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v1

    add-int/2addr p1, v1

    .line 2286
    goto :goto_0
.end method

.method private static skipUWhiteSpace(Ljava/lang/String;I)I
    .locals 2
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "pos"    # I

    .prologue
    .line 2295
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 2296
    invoke-static {p0, p1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 2297
    .local v0, "c":I
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->isUWhiteSpace(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2302
    .end local v0    # "c":I
    :cond_0
    return p1

    .line 2300
    .restart local v0    # "c":I
    :cond_1
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v1

    add-int/2addr p1, v1

    .line 2301
    goto :goto_0
.end method

.method private subformat(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;ZZ)Ljava/lang/StringBuffer;
    .locals 6
    .param p1, "result"    # Ljava/lang/StringBuffer;
    .param p2, "fieldPosition"    # Ljava/text/FieldPosition;
    .param p3, "isNegative"    # Z
    .param p4, "isInteger"    # Z

    .prologue
    .line 1077
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/DecimalFormat;->subformat(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;ZZZ)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method private subformat(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;ZZZ)Ljava/lang/StringBuffer;
    .locals 42
    .param p1, "result"    # Ljava/lang/StringBuffer;
    .param p2, "fieldPosition"    # Ljava/text/FieldPosition;
    .param p3, "isNegative"    # Z
    .param p4, "isInteger"    # Z
    .param p5, "parseAttr"    # Z

    .prologue
    .line 1099
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getZeroDigit()C

    move-result v36

    .line 1100
    .local v36, "zero":C
    add-int/lit8 v37, v36, -0x30

    .line 1101
    .local v37, "zeroDelta":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->isCurrencyFormat:Z

    move/from16 v38, v0

    if-eqz v38, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getMonetaryGroupingSeparator()C

    move-result v18

    .line 1104
    .local v18, "grouping":C
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->isCurrencyFormat:Z

    move/from16 v38, v0

    if-eqz v38, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getMonetaryDecimalSeparator()C

    move-result v8

    .line 1107
    .local v8, "decimal":C
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->areSignificantDigitsUsed()Z

    move-result v35

    .line 1108
    .local v35, "useSigDig":Z
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMaximumIntegerDigits()I

    move-result v23

    .line 1109
    .local v23, "maxIntDig":I
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMinimumIntegerDigits()I

    move-result v26

    .line 1117
    .local v26, "minIntDig":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/ibm/icu/text/DigitList;->isZero()Z

    move-result v38

    if-eqz v38, :cond_0

    .line 1119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    const/16 v39, 0x0

    move/from16 v0, v39

    move-object/from16 v1, v38

    iput v0, v1, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 1122
    :cond_0
    const/16 v38, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    move/from16 v3, v38

    move/from16 v4, p5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/DecimalFormat;->appendAffix(Ljava/lang/StringBuffer;ZZZ)I

    move-result v30

    .line 1124
    .local v30, "prefixLen":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    move/from16 v38, v0

    if-eqz v38, :cond_25

    .line 1127
    invoke-virtual/range {p2 .. p2}, Ljava/text/FieldPosition;->getField()I

    move-result v38

    if-nez v38, :cond_b

    .line 1128
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v38

    move-object/from16 v0, p2

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 1129
    const/16 v38, -0x1

    move-object/from16 v0, p2

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 1140
    :cond_1
    :goto_2
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v20

    .line 1141
    .local v20, "intBegin":I
    const/16 v21, -0x1

    .line 1142
    .local v21, "intEnd":I
    const/16 v16, -0x1

    .line 1145
    .local v16, "fracBegin":I
    const/16 v25, 0x0

    .line 1146
    .local v25, "minFracDig":I
    if-eqz v35, :cond_c

    .line 1147
    const/16 v26, 0x1

    move/from16 v23, v26

    .line 1148
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMinimumSignificantDigits()I

    move-result v38

    add-int/lit8 v25, v38, -0x1

    .line 1174
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v15, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 1175
    .local v15, "exponent":I
    const/16 v38, 0x1

    move/from16 v0, v23

    move/from16 v1, v38

    if-le v0, v1, :cond_f

    move/from16 v0, v23

    move/from16 v1, v26

    if-eq v0, v1, :cond_f

    .line 1177
    if-lez v15, :cond_e

    add-int/lit8 v38, v15, -0x1

    div-int v15, v38, v23

    .line 1179
    :goto_4
    mul-int v15, v15, v23

    .line 1191
    :goto_5
    add-int v28, v26, v25

    .line 1194
    .local v28, "minimumDigits":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/ibm/icu/text/DigitList;->isZero()Z

    move-result v38

    if-eqz v38, :cond_12

    move/from16 v22, v26

    .line 1196
    .local v22, "integerDigits":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->count:I

    move/from16 v34, v0

    .line 1197
    .local v34, "totalDigits":I
    move/from16 v0, v28

    move/from16 v1, v34

    if-le v0, v1, :cond_3

    move/from16 v34, v28

    .line 1198
    :cond_3
    move/from16 v0, v22

    move/from16 v1, v34

    if-le v0, v1, :cond_4

    move/from16 v34, v22

    .line 1200
    :cond_4
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_7
    move/from16 v0, v19

    move/from16 v1, v34

    if-ge v0, v1, :cond_14

    .line 1202
    move/from16 v0, v19

    move/from16 v1, v22

    if-ne v0, v1, :cond_8

    .line 1205
    invoke-virtual/range {p2 .. p2}, Ljava/text/FieldPosition;->getField()I

    move-result v38

    if-nez v38, :cond_5

    .line 1206
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v38

    move-object/from16 v0, p2

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 1211
    :cond_5
    if-eqz p5, :cond_6

    .line 1212
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v21

    .line 1213
    sget-object v38, Lcom/ibm/icu/text/NumberFormat$Field;->INTEGER:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v39

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v20

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    .line 1216
    :cond_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1220
    if-eqz p5, :cond_7

    .line 1222
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v38

    add-int/lit8 v9, v38, -0x1

    .line 1223
    .local v9, "decimalSeparatorBegin":I
    sget-object v38, Lcom/ibm/icu/text/NumberFormat$Field;->DECIMAL_SEPARATOR:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v39

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v39

    invoke-direct {v0, v1, v9, v2}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    .line 1225
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v16

    .line 1229
    .end local v9    # "decimalSeparatorBegin":I
    :cond_7
    invoke-virtual/range {p2 .. p2}, Ljava/text/FieldPosition;->getField()I

    move-result v38

    const/16 v39, 0x1

    move/from16 v0, v38

    move/from16 v1, v39

    if-ne v0, v1, :cond_8

    .line 1230
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v38

    move-object/from16 v0, p2

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 1233
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->count:I

    move/from16 v38, v0

    move/from16 v0, v19

    move/from16 v1, v38

    if-ge v0, v1, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/ibm/icu/text/DigitList;->digits:[B

    move-object/from16 v38, v0

    aget-byte v38, v38, v19

    add-int v38, v38, v37

    move/from16 v0, v38

    int-to-char v0, v0

    move/from16 v38, v0

    :goto_8
    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1200
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_7

    .line 1101
    .end local v8    # "decimal":C
    .end local v15    # "exponent":I
    .end local v16    # "fracBegin":I
    .end local v18    # "grouping":C
    .end local v19    # "i":I
    .end local v20    # "intBegin":I
    .end local v21    # "intEnd":I
    .end local v22    # "integerDigits":I
    .end local v23    # "maxIntDig":I
    .end local v25    # "minFracDig":I
    .end local v26    # "minIntDig":I
    .end local v28    # "minimumDigits":I
    .end local v30    # "prefixLen":I
    .end local v34    # "totalDigits":I
    .end local v35    # "useSigDig":Z
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getGroupingSeparator()C

    move-result v18

    goto/16 :goto_0

    .line 1104
    .restart local v18    # "grouping":C
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v8

    goto/16 :goto_1

    .line 1130
    .restart local v8    # "decimal":C
    .restart local v23    # "maxIntDig":I
    .restart local v26    # "minIntDig":I
    .restart local v30    # "prefixLen":I
    .restart local v35    # "useSigDig":Z
    :cond_b
    invoke-virtual/range {p2 .. p2}, Ljava/text/FieldPosition;->getField()I

    move-result v38

    const/16 v39, 0x1

    move/from16 v0, v38

    move/from16 v1, v39

    if-ne v0, v1, :cond_1

    .line 1131
    const/16 v38, -0x1

    move-object/from16 v0, p2

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    goto/16 :goto_2

    .line 1150
    .restart local v16    # "fracBegin":I
    .restart local v20    # "intBegin":I
    .restart local v21    # "intEnd":I
    .restart local v25    # "minFracDig":I
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMinimumFractionDigits()I

    move-result v25

    .line 1151
    const/16 v38, 0x8

    move/from16 v0, v23

    move/from16 v1, v38

    if-le v0, v1, :cond_d

    .line 1152
    const/16 v23, 0x1

    .line 1153
    move/from16 v0, v23

    move/from16 v1, v26

    if-ge v0, v1, :cond_d

    .line 1154
    move/from16 v23, v26

    .line 1157
    :cond_d
    move/from16 v0, v23

    move/from16 v1, v26

    if-le v0, v1, :cond_2

    .line 1158
    const/16 v26, 0x1

    goto/16 :goto_3

    .line 1177
    .restart local v15    # "exponent":I
    :cond_e
    div-int v38, v15, v23

    add-int/lit8 v15, v38, -0x1

    goto/16 :goto_4

    .line 1183
    :cond_f
    if-gtz v26, :cond_10

    if-lez v25, :cond_11

    :cond_10
    move/from16 v38, v26

    :goto_9
    sub-int v15, v15, v38

    goto/16 :goto_5

    :cond_11
    const/16 v38, 0x1

    goto :goto_9

    .line 1194
    .restart local v28    # "minimumDigits":I
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    move/from16 v38, v0

    sub-int v22, v38, v15

    goto/16 :goto_6

    .restart local v19    # "i":I
    .restart local v22    # "integerDigits":I
    .restart local v34    # "totalDigits":I
    :cond_13
    move/from16 v38, v36

    .line 1233
    goto :goto_8

    .line 1239
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/ibm/icu/text/DigitList;->isZero()Z

    move-result v38

    if-eqz v38, :cond_15

    if-nez v34, :cond_15

    .line 1240
    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1244
    :cond_15
    invoke-virtual/range {p2 .. p2}, Ljava/text/FieldPosition;->getField()I

    move-result v38

    if-nez v38, :cond_1d

    .line 1245
    invoke-virtual/range {p2 .. p2}, Ljava/text/FieldPosition;->getEndIndex()I

    move-result v38

    if-gez v38, :cond_16

    .line 1246
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v38

    move-object/from16 v0, p2

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 1258
    :cond_16
    :goto_a
    if-eqz p5, :cond_18

    .line 1259
    if-gez v21, :cond_17

    .line 1260
    sget-object v38, Lcom/ibm/icu/text/NumberFormat$Field;->INTEGER:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v39

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v20

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    .line 1262
    :cond_17
    if-lez v16, :cond_18

    .line 1263
    sget-object v38, Lcom/ibm/icu/text/NumberFormat$Field;->FRACTION:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v39

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v16

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    .line 1272
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getExponentSeparator()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1276
    if-eqz p5, :cond_19

    .line 1277
    sget-object v38, Lcom/ibm/icu/text/NumberFormat$Field;->EXPONENT_SYMBOL:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getExponentSeparator()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->length()I

    move-result v40

    sub-int v39, v39, v40

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v40

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v39

    move/from16 v3, v40

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    .line 1285
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/ibm/icu/text/DigitList;->isZero()Z

    move-result v38

    if-eqz v38, :cond_1a

    const/4 v15, 0x0

    .line 1287
    :cond_1a
    if-gez v15, :cond_1f

    const/16 v29, 0x1

    .line 1288
    .local v29, "negativeExponent":Z
    :goto_b
    if-eqz v29, :cond_20

    .line 1289
    neg-int v15, v15

    .line 1290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getMinusSign()C

    move-result v38

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1295
    if-eqz p5, :cond_1b

    .line 1297
    sget-object v38, Lcom/ibm/icu/text/NumberFormat$Field;->EXPONENT_SIGN:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v39

    add-int/lit8 v39, v39, -0x1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v40

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v39

    move/from16 v3, v40

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    .line 1316
    :cond_1b
    :goto_c
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v12

    .line 1318
    .local v12, "expBegin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    int-to-long v0, v15

    move-wide/from16 v40, v0

    move-object/from16 v0, v38

    move-wide/from16 v1, v40

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/DigitList;->set(J)V

    .line 1320
    move-object/from16 v0, p0

    iget-byte v13, v0, Lcom/ibm/icu/text/DecimalFormat;->minExponentDigits:B

    .line 1321
    .local v13, "expDig":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    move/from16 v38, v0

    if-eqz v38, :cond_1c

    const/16 v38, 0x1

    move/from16 v0, v38

    if-ge v13, v0, :cond_1c

    .line 1322
    const/4 v13, 0x1

    .line 1324
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    move/from16 v19, v0

    :goto_d
    move/from16 v0, v19

    if-ge v0, v13, :cond_21

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v19, v19, 0x1

    goto :goto_d

    .line 1248
    .end local v12    # "expBegin":I
    .end local v13    # "expDig":I
    .end local v29    # "negativeExponent":Z
    :cond_1d
    invoke-virtual/range {p2 .. p2}, Ljava/text/FieldPosition;->getField()I

    move-result v38

    const/16 v39, 0x1

    move/from16 v0, v38

    move/from16 v1, v39

    if-ne v0, v1, :cond_16

    .line 1249
    invoke-virtual/range {p2 .. p2}, Ljava/text/FieldPosition;->getBeginIndex()I

    move-result v38

    if-gez v38, :cond_1e

    .line 1250
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v38

    move-object/from16 v0, p2

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 1252
    :cond_1e
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v38

    move-object/from16 v0, p2

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setEndIndex(I)V

    goto/16 :goto_a

    .line 1287
    :cond_1f
    const/16 v29, 0x0

    goto/16 :goto_b

    .line 1301
    .restart local v29    # "negativeExponent":Z
    :cond_20
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->exponentSignAlwaysShown:Z

    move/from16 v38, v0

    if-eqz v38, :cond_1b

    .line 1302
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPlusSign()C

    move-result v38

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1306
    if-eqz p5, :cond_1b

    .line 1308
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v38

    add-int/lit8 v14, v38, -0x1

    .line 1309
    .local v14, "expSignBegin":I
    sget-object v38, Lcom/ibm/icu/text/NumberFormat$Field;->EXPONENT_SIGN:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v39

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v39

    invoke-direct {v0, v1, v14, v2}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    goto/16 :goto_c

    .line 1326
    .end local v14    # "expSignBegin":I
    .restart local v12    # "expBegin":I
    .restart local v13    # "expDig":I
    :cond_21
    const/16 v19, 0x0

    :goto_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    move/from16 v38, v0

    move/from16 v0, v19

    move/from16 v1, v38

    if-ge v0, v1, :cond_23

    .line 1328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->count:I

    move/from16 v38, v0

    move/from16 v0, v19

    move/from16 v1, v38

    if-ge v0, v1, :cond_22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/ibm/icu/text/DigitList;->digits:[B

    move-object/from16 v38, v0

    aget-byte v38, v38, v19

    add-int v38, v38, v37

    move/from16 v0, v38

    int-to-char v0, v0

    move/from16 v38, v0

    :goto_f
    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1326
    add-int/lit8 v19, v19, 0x1

    goto :goto_e

    :cond_22
    move/from16 v38, v36

    .line 1328
    goto :goto_f

    .line 1334
    :cond_23
    if-eqz p5, :cond_24

    .line 1335
    sget-object v38, Lcom/ibm/icu/text/NumberFormat$Field;->EXPONENT:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v39

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v39

    invoke-direct {v0, v1, v12, v2}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    .line 1520
    .end local v12    # "expBegin":I
    .end local v13    # "expDig":I
    .end local v15    # "exponent":I
    .end local v21    # "intEnd":I
    .end local v22    # "integerDigits":I
    .end local v25    # "minFracDig":I
    .end local v28    # "minimumDigits":I
    .end local v29    # "negativeExponent":Z
    .end local v34    # "totalDigits":I
    :cond_24
    :goto_10
    const/16 v38, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    move/from16 v3, v38

    move/from16 v4, p5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/DecimalFormat;->appendAffix(Ljava/lang/StringBuffer;ZZZ)I

    move-result v33

    .line 1523
    .local v33, "suffixLen":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, v30

    move/from16 v4, v33

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/DecimalFormat;->addPadding(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;II)V

    .line 1524
    return-object p1

    .line 1344
    .end local v16    # "fracBegin":I
    .end local v19    # "i":I
    .end local v20    # "intBegin":I
    .end local v33    # "suffixLen":I
    :cond_25
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v20

    .line 1347
    .restart local v20    # "intBegin":I
    invoke-virtual/range {p2 .. p2}, Ljava/text/FieldPosition;->getField()I

    move-result v38

    if-nez v38, :cond_26

    .line 1348
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v38

    move-object/from16 v0, p2

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 1351
    :cond_26
    const/16 v31, 0x0

    .line 1352
    .local v31, "sigCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMinimumSignificantDigits()I

    move-result v27

    .line 1353
    .local v27, "minSigDig":I
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMaximumSignificantDigits()I

    move-result v24

    .line 1354
    .local v24, "maxSigDig":I
    if-nez v35, :cond_27

    .line 1355
    const/16 v27, 0x0

    .line 1356
    const v24, 0x7fffffff

    .line 1363
    :cond_27
    if-eqz v35, :cond_2b

    const/16 v38, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    move/from16 v39, v0

    invoke-static/range {v38 .. v39}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 1365
    .local v6, "count":I
    :goto_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    move/from16 v38, v0

    if-lez v38, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    move/from16 v38, v0

    move/from16 v0, v38

    if-ge v6, v0, :cond_28

    .line 1366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v6, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 1374
    :cond_28
    const/4 v10, 0x0

    .line 1375
    .local v10, "digitIndex":I
    move/from16 v0, v23

    if-le v6, v0, :cond_29

    if-ltz v23, :cond_29

    .line 1376
    move/from16 v6, v23

    .line 1377
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    move/from16 v38, v0

    sub-int v10, v38, v6

    .line 1380
    :cond_29
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v32

    .line 1381
    .local v32, "sizeBeforeIntegerPart":I
    add-int/lit8 v19, v6, -0x1

    .restart local v19    # "i":I
    move v11, v10

    .end local v10    # "digitIndex":I
    .local v11, "digitIndex":I
    :goto_12
    if-ltz v19, :cond_2d

    .line 1383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    move/from16 v38, v0

    move/from16 v0, v19

    move/from16 v1, v38

    if-ge v0, v1, :cond_2c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->count:I

    move/from16 v38, v0

    move/from16 v0, v38

    if-ge v11, v0, :cond_2c

    move/from16 v0, v31

    move/from16 v1, v24

    if-ge v0, v1, :cond_2c

    .line 1386
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/ibm/icu/text/DigitList;->digits:[B

    move-object/from16 v38, v0

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "digitIndex":I
    .restart local v10    # "digitIndex":I
    aget-byte v7, v38, v11

    .line 1387
    .local v7, "d":B
    add-int v38, v7, v37

    move/from16 v0, v38

    int-to-char v0, v0

    move/from16 v38, v0

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1388
    add-int/lit8 v31, v31, 0x1

    .line 1400
    .end local v7    # "d":B
    :goto_13
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->isGroupingPosition(I)Z

    move-result v38

    if-eqz v38, :cond_2a

    .line 1401
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1405
    if-eqz p5, :cond_2a

    .line 1407
    sget-object v38, Lcom/ibm/icu/text/NumberFormat$Field;->GROUPING_SEPARATOR:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v39

    add-int/lit8 v39, v39, -0x1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v40

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v39

    move/from16 v3, v40

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    .line 1381
    :cond_2a
    add-int/lit8 v19, v19, -0x1

    move v11, v10

    .end local v10    # "digitIndex":I
    .restart local v11    # "digitIndex":I
    goto :goto_12

    .end local v6    # "count":I
    .end local v11    # "digitIndex":I
    .end local v19    # "i":I
    .end local v32    # "sizeBeforeIntegerPart":I
    :cond_2b
    move/from16 v6, v26

    .line 1363
    goto/16 :goto_11

    .line 1393
    .restart local v6    # "count":I
    .restart local v11    # "digitIndex":I
    .restart local v19    # "i":I
    .restart local v32    # "sizeBeforeIntegerPart":I
    :cond_2c
    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1394
    if-lez v31, :cond_43

    .line 1395
    add-int/lit8 v31, v31, 0x1

    move v10, v11

    .end local v11    # "digitIndex":I
    .restart local v10    # "digitIndex":I
    goto :goto_13

    .line 1415
    .end local v10    # "digitIndex":I
    .restart local v11    # "digitIndex":I
    :cond_2d
    invoke-virtual/range {p2 .. p2}, Ljava/text/FieldPosition;->getField()I

    move-result v38

    if-nez v38, :cond_2e

    .line 1416
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v38

    move-object/from16 v0, p2

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 1421
    :cond_2e
    if-nez p4, :cond_2f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->count:I

    move/from16 v38, v0

    move/from16 v0, v38

    if-lt v11, v0, :cond_30

    :cond_2f
    if-eqz v35, :cond_3b

    move/from16 v0, v31

    move/from16 v1, v27

    if-ge v0, v1, :cond_3c

    :cond_30
    const/16 v17, 0x1

    .line 1427
    .local v17, "fractionPresent":Z
    :goto_14
    if-nez v17, :cond_31

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v38

    move/from16 v0, v38

    move/from16 v1, v32

    if-ne v0, v1, :cond_31

    .line 1428
    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1432
    :cond_31
    if-eqz p5, :cond_32

    .line 1433
    sget-object v38, Lcom/ibm/icu/text/NumberFormat$Field;->INTEGER:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v39

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v20

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    .line 1437
    :cond_32
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->decimalSeparatorAlwaysShown:Z

    move/from16 v38, v0

    if-nez v38, :cond_33

    if-eqz v17, :cond_34

    .line 1439
    :cond_33
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1443
    if-eqz p5, :cond_34

    .line 1444
    sget-object v38, Lcom/ibm/icu/text/NumberFormat$Field;->DECIMAL_SEPARATOR:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v39

    add-int/lit8 v39, v39, -0x1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v40

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v39

    move/from16 v3, v40

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    .line 1451
    :cond_34
    invoke-virtual/range {p2 .. p2}, Ljava/text/FieldPosition;->getField()I

    move-result v38

    const/16 v39, 0x1

    move/from16 v0, v38

    move/from16 v1, v39

    if-ne v0, v1, :cond_35

    .line 1452
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v38

    move-object/from16 v0, p2

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 1458
    :cond_35
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v16

    .line 1461
    .restart local v16    # "fracBegin":I
    if-eqz v35, :cond_3d

    const v6, 0x7fffffff

    .line 1462
    :goto_15
    if-eqz v35, :cond_37

    move/from16 v0, v31

    move/from16 v1, v24

    if-eq v0, v1, :cond_36

    move/from16 v0, v31

    move/from16 v1, v27

    if-lt v0, v1, :cond_37

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->count:I

    move/from16 v38, v0

    move/from16 v0, v38

    if-ne v11, v0, :cond_37

    .line 1464
    :cond_36
    const/4 v6, 0x0

    .line 1466
    :cond_37
    const/16 v19, 0x0

    :goto_16
    move/from16 v0, v19

    if-ge v0, v6, :cond_42

    .line 1474
    if-nez v35, :cond_3e

    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMinimumFractionDigits()I

    move-result v38

    move/from16 v0, v19

    move/from16 v1, v38

    if-lt v0, v1, :cond_3e

    if-nez p4, :cond_42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->count:I

    move/from16 v38, v0

    move/from16 v0, v38

    if-lt v11, v0, :cond_3e

    move v10, v11

    .line 1508
    .end local v11    # "digitIndex":I
    .restart local v10    # "digitIndex":I
    :cond_38
    :goto_17
    invoke-virtual/range {p2 .. p2}, Ljava/text/FieldPosition;->getField()I

    move-result v38

    const/16 v39, 0x1

    move/from16 v0, v38

    move/from16 v1, v39

    if-ne v0, v1, :cond_39

    .line 1509
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v38

    move-object/from16 v0, p2

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 1514
    :cond_39
    if-eqz p5, :cond_24

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->decimalSeparatorAlwaysShown:Z

    move/from16 v38, v0

    if-nez v38, :cond_3a

    if-eqz v17, :cond_24

    .line 1515
    :cond_3a
    sget-object v38, Lcom/ibm/icu/text/NumberFormat$Field;->FRACTION:Lcom/ibm/icu/text/NumberFormat$Field;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v39

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move/from16 v2, v16

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/DecimalFormat;->addAttribute(Lcom/ibm/icu/text/NumberFormat$Field;II)V

    goto/16 :goto_10

    .line 1421
    .end local v10    # "digitIndex":I
    .end local v16    # "fracBegin":I
    .end local v17    # "fractionPresent":Z
    .restart local v11    # "digitIndex":I
    :cond_3b
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMinimumFractionDigits()I

    move-result v38

    if-gtz v38, :cond_30

    :cond_3c
    const/16 v17, 0x0

    goto/16 :goto_14

    .line 1461
    .restart local v16    # "fracBegin":I
    .restart local v17    # "fractionPresent":Z
    :cond_3d
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMaximumFractionDigits()I

    move-result v6

    goto/16 :goto_15

    .line 1483
    :cond_3e
    rsub-int/lit8 v38, v19, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    move/from16 v39, v0

    add-int/lit8 v39, v39, -0x1

    move/from16 v0, v38

    move/from16 v1, v39

    if-le v0, v1, :cond_40

    .line 1484
    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v10, v11

    .line 1466
    .end local v11    # "digitIndex":I
    .restart local v10    # "digitIndex":I
    :cond_3f
    add-int/lit8 v19, v19, 0x1

    move v11, v10

    .end local v10    # "digitIndex":I
    .restart local v11    # "digitIndex":I
    goto/16 :goto_16

    .line 1490
    :cond_40
    if-nez p4, :cond_41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->count:I

    move/from16 v38, v0

    move/from16 v0, v38

    if-ge v11, v0, :cond_41

    .line 1491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/ibm/icu/text/DigitList;->digits:[B

    move-object/from16 v38, v0

    add-int/lit8 v10, v11, 0x1

    .end local v11    # "digitIndex":I
    .restart local v10    # "digitIndex":I
    aget-byte v38, v38, v11

    add-int v38, v38, v37

    move/from16 v0, v38

    int-to-char v0, v0

    move/from16 v38, v0

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1499
    :goto_18
    add-int/lit8 v31, v31, 0x1

    .line 1500
    if-eqz v35, :cond_3f

    move/from16 v0, v31

    move/from16 v1, v24

    if-eq v0, v1, :cond_38

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget v0, v0, Lcom/ibm/icu/text/DigitList;->count:I

    move/from16 v38, v0

    move/from16 v0, v38

    if-ne v10, v0, :cond_3f

    move/from16 v0, v31

    move/from16 v1, v27

    if-lt v0, v1, :cond_3f

    goto/16 :goto_17

    .line 1493
    .end local v10    # "digitIndex":I
    .restart local v11    # "digitIndex":I
    :cond_41
    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v10, v11

    .end local v11    # "digitIndex":I
    .restart local v10    # "digitIndex":I
    goto :goto_18

    .end local v10    # "digitIndex":I
    .restart local v11    # "digitIndex":I
    :cond_42
    move v10, v11

    .end local v11    # "digitIndex":I
    .restart local v10    # "digitIndex":I
    goto/16 :goto_17

    .end local v10    # "digitIndex":I
    .end local v16    # "fracBegin":I
    .end local v17    # "fractionPresent":Z
    .restart local v11    # "digitIndex":I
    :cond_43
    move v10, v11

    .end local v11    # "digitIndex":I
    .restart local v10    # "digitIndex":I
    goto/16 :goto_13
.end method

.method private final subparse(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/DigitList;Z[Z[Lcom/ibm/icu/util/Currency;)Z
    .locals 34
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "parsePosition"    # Ljava/text/ParsePosition;
    .param p3, "digits"    # Lcom/ibm/icu/text/DigitList;
    .param p4, "isExponent"    # Z
    .param p5, "status"    # [Z
    .param p6, "currency"    # [Lcom/ibm/icu/util/Currency;

    .prologue
    .line 1772
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    .line 1773
    .local v4, "position":I
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v25

    .line 1776
    .local v25, "oldStart":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    if-lez v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    if-nez v2, :cond_0

    .line 1777
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/ibm/icu/text/DecimalFormat;->skipPadding(Ljava/lang/String;I)I

    move-result v4

    .line 1781
    :cond_0
    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v7, p6

    invoke-direct/range {v2 .. v7}, Lcom/ibm/icu/text/DecimalFormat;->compareAffix(Ljava/lang/String;IZZ[Lcom/ibm/icu/util/Currency;)I

    move-result v27

    .line 1782
    .local v27, "posMatch":I
    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v7, p6

    invoke-direct/range {v2 .. v7}, Lcom/ibm/icu/text/DecimalFormat;->compareAffix(Ljava/lang/String;IZZ[Lcom/ibm/icu/util/Currency;)I

    move-result v24

    .line 1783
    .local v24, "negMatch":I
    if-ltz v27, :cond_1

    if-ltz v24, :cond_1

    .line 1784
    move/from16 v0, v27

    move/from16 v1, v24

    if-le v0, v1, :cond_8

    .line 1785
    const/16 v24, -0x1

    .line 1790
    :cond_1
    :goto_0
    if-ltz v27, :cond_9

    .line 1791
    add-int v4, v4, v27

    .line 1800
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    if-lez v2, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 1801
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/ibm/icu/text/DecimalFormat;->skipPadding(Ljava/lang/String;I)I

    move-result v4

    .line 1805
    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-boolean v3, p5, v2

    .line 1806
    if-nez p4, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getInfinity()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v5}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getInfinity()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2, v3, v5}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1809
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getInfinity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v4, v2

    .line 1810
    const/4 v2, 0x0

    const/4 v3, 0x1

    aput-boolean v3, p5, v2

    .line 2101
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    if-lez v2, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 2102
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/ibm/icu/text/DecimalFormat;->skipPadding(Ljava/lang/String;I)I

    move-result v4

    .line 2106
    :cond_4
    if-ltz v27, :cond_5

    .line 2107
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v7, p6

    invoke-direct/range {v2 .. v7}, Lcom/ibm/icu/text/DecimalFormat;->compareAffix(Ljava/lang/String;IZZ[Lcom/ibm/icu/util/Currency;)I

    move-result v27

    .line 2109
    :cond_5
    if-ltz v24, :cond_6

    .line 2110
    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v7, p6

    invoke-direct/range {v2 .. v7}, Lcom/ibm/icu/text/DecimalFormat;->compareAffix(Ljava/lang/String;IZZ[Lcom/ibm/icu/util/Currency;)I

    move-result v24

    .line 2112
    :cond_6
    if-ltz v27, :cond_7

    if-ltz v24, :cond_7

    .line 2113
    move/from16 v0, v27

    move/from16 v1, v24

    if-le v0, v1, :cond_37

    .line 2114
    const/16 v24, -0x1

    .line 2121
    :cond_7
    :goto_2
    if-ltz v27, :cond_38

    const/4 v2, 0x1

    move v3, v2

    :goto_3
    if-ltz v24, :cond_39

    const/4 v2, 0x1

    :goto_4
    if-ne v3, v2, :cond_3a

    .line 2122
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 2123
    const/4 v2, 0x0

    .line 2141
    :goto_5
    return v2

    .line 1786
    :cond_8
    move/from16 v0, v24

    move/from16 v1, v27

    if-le v0, v1, :cond_1

    .line 1787
    const/16 v27, -0x1

    goto/16 :goto_0

    .line 1792
    :cond_9
    if-ltz v24, :cond_a

    .line 1793
    add-int v4, v4, v24

    .line 1794
    goto/16 :goto_1

    .line 1795
    :cond_a
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 1796
    const/4 v2, 0x0

    goto :goto_5

    .line 1819
    :cond_b
    const/4 v2, 0x0

    move-object/from16 v0, p3

    iput v2, v0, Lcom/ibm/icu/text/DigitList;->count:I

    move-object/from16 v0, p3

    iput v2, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 1820
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getZeroDigit()C

    move-result v33

    .line 1821
    .local v33, "zero":C
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/ibm/icu/text/DecimalFormat;->isCurrencyFormat:Z

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getMonetaryDecimalSeparator()C

    move-result v10

    .line 1823
    .local v10, "decimal":C
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getGroupingSeparator()C

    move-result v18

    .line 1825
    .local v18, "grouping":C
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getExponentSeparator()Ljava/lang/String;

    move-result-object v17

    .line 1826
    .local v17, "exponentSep":Ljava/lang/String;
    const/16 v28, 0x0

    .line 1827
    .local v28, "sawDecimal":Z
    const/16 v30, 0x0

    .line 1828
    .local v30, "sawExponent":Z
    const/16 v29, 0x0

    .line 1829
    .local v29, "sawDigit":Z
    const-wide/16 v14, 0x0

    .line 1830
    .local v14, "exponent":J
    const/4 v12, 0x0

    .line 1833
    .local v12, "digit":I
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->isParseStrict()Z

    move-result v32

    .line 1834
    .local v32, "strictParse":Z
    const/16 v31, 0x0

    .line 1835
    .local v31, "strictFail":Z
    const/16 v22, 0x0

    .line 1836
    .local v22, "leadingZero":Z
    const/16 v21, -0x1

    .line 1837
    .local v21, "lastGroup":I
    move-object/from16 v0, p0

    iget-byte v2, v0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    if-nez v2, :cond_14

    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    move/from16 v20, v0

    .line 1842
    .local v20, "gs2":I
    :goto_7
    new-instance v11, Lcom/ibm/icu/text/UnicodeSet;

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-direct {v0, v10, v1}, Lcom/ibm/icu/text/DecimalFormat;->getSimilarDecimals(CZ)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v2

    invoke-direct {v11, v2}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Lcom/ibm/icu/text/UnicodeSet;)V

    .line 1843
    .local v11, "decimalSet":Lcom/ibm/icu/text/UnicodeSet;
    new-instance v3, Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v32, :cond_15

    sget-object v2, Lcom/ibm/icu/text/DecimalFormat;->strictDefaultGroupingSeparators:Lcom/ibm/icu/text/UnicodeSet;

    :goto_8
    invoke-direct {v3, v2}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Lcom/ibm/icu/text/UnicodeSet;)V

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v2

    invoke-virtual {v2, v11}, Lcom/ibm/icu/text/UnicodeSet;->removeAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v19

    .line 1854
    .local v19, "groupingSet":Lcom/ibm/icu/text/UnicodeSet;
    const/4 v13, 0x0

    .line 1856
    .local v13, "digitCount":I
    const/4 v8, -0x1

    .line 1857
    .local v8, "backup":I
    :goto_9
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v4, v2, :cond_10

    .line 1859
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v9

    .line 1872
    .local v9, "ch":C
    sub-int v12, v9, v33

    .line 1873
    if-ltz v12, :cond_c

    const/16 v2, 0x9

    if-le v12, v2, :cond_d

    :cond_c
    const/16 v2, 0xa

    invoke-static {v9, v2}, Lcom/ibm/icu/lang/UCharacter;->digit(II)I

    move-result v12

    .line 1875
    :cond_d
    if-nez v12, :cond_1c

    .line 1878
    if-eqz v32, :cond_17

    const/4 v2, -0x1

    if-eq v8, v2, :cond_17

    .line 1884
    const/4 v2, -0x1

    move/from16 v0, v21

    if-eq v0, v2, :cond_e

    sub-int v2, v8, v21

    add-int/lit8 v2, v2, -0x1

    move/from16 v0, v20

    if-ne v2, v0, :cond_f

    :cond_e
    const/4 v2, -0x1

    move/from16 v0, v21

    if-ne v0, v2, :cond_16

    sub-int v2, v4, v25

    add-int/lit8 v2, v2, -0x1

    move/from16 v0, v20

    if-le v2, v0, :cond_16

    .line 1886
    :cond_f
    const/16 v31, 0x1

    .line 2058
    .end local v9    # "ch":C
    :cond_10
    :goto_a
    const/4 v2, -0x1

    if-eq v8, v2, :cond_11

    move v4, v8

    .line 2060
    :cond_11
    if-eqz v32, :cond_12

    if-nez v28, :cond_12

    .line 2061
    const/4 v2, -0x1

    move/from16 v0, v21

    if-eq v0, v2, :cond_12

    sub-int v2, v4, v21

    move-object/from16 v0, p0

    iget-byte v3, v0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    add-int/lit8 v3, v3, 0x1

    if-eq v2, v3, :cond_12

    .line 2062
    const/16 v31, 0x1

    .line 2065
    :cond_12
    if-eqz v31, :cond_33

    .line 2071
    move-object/from16 v0, p2

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 2072
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 2073
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 1821
    .end local v8    # "backup":I
    .end local v10    # "decimal":C
    .end local v11    # "decimalSet":Lcom/ibm/icu/text/UnicodeSet;
    .end local v12    # "digit":I
    .end local v13    # "digitCount":I
    .end local v14    # "exponent":J
    .end local v17    # "exponentSep":Ljava/lang/String;
    .end local v18    # "grouping":C
    .end local v19    # "groupingSet":Lcom/ibm/icu/text/UnicodeSet;
    .end local v20    # "gs2":I
    .end local v21    # "lastGroup":I
    .end local v22    # "leadingZero":Z
    .end local v28    # "sawDecimal":Z
    .end local v29    # "sawDigit":Z
    .end local v30    # "sawExponent":Z
    .end local v31    # "strictFail":Z
    .end local v32    # "strictParse":Z
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v10

    goto/16 :goto_6

    .line 1837
    .restart local v10    # "decimal":C
    .restart local v12    # "digit":I
    .restart local v14    # "exponent":J
    .restart local v17    # "exponentSep":Ljava/lang/String;
    .restart local v18    # "grouping":C
    .restart local v21    # "lastGroup":I
    .restart local v22    # "leadingZero":Z
    .restart local v28    # "sawDecimal":Z
    .restart local v29    # "sawDigit":Z
    .restart local v30    # "sawExponent":Z
    .restart local v31    # "strictFail":Z
    .restart local v32    # "strictParse":Z
    :cond_14
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    move/from16 v20, v0

    goto/16 :goto_7

    .line 1843
    .restart local v11    # "decimalSet":Lcom/ibm/icu/text/UnicodeSet;
    .restart local v20    # "gs2":I
    :cond_15
    sget-object v2, Lcom/ibm/icu/text/DecimalFormat;->defaultGroupingSeparators:Lcom/ibm/icu/text/UnicodeSet;

    goto/16 :goto_8

    .line 1889
    .restart local v8    # "backup":I
    .restart local v9    # "ch":C
    .restart local v13    # "digitCount":I
    .restart local v19    # "groupingSet":Lcom/ibm/icu/text/UnicodeSet;
    :cond_16
    move/from16 v21, v8

    .line 1891
    :cond_17
    const/4 v8, -0x1

    .line 1892
    const/16 v29, 0x1

    .line 1895
    move-object/from16 v0, p3

    iget v2, v0, Lcom/ibm/icu/text/DigitList;->count:I

    if-nez v2, :cond_1b

    .line 1897
    if-nez v28, :cond_1a

    .line 1898
    if-eqz v32, :cond_19

    if-nez p4, :cond_19

    .line 1900
    if-eqz v22, :cond_18

    .line 1901
    const/16 v31, 0x1

    .line 1902
    goto :goto_a

    .line 1904
    :cond_18
    const/16 v22, 0x1

    .line 1857
    :cond_19
    :goto_b
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_9

    .line 1913
    :cond_1a
    move-object/from16 v0, p3

    iget v2, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p3

    iput v2, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    goto :goto_b

    .line 1917
    :cond_1b
    add-int/lit8 v13, v13, 0x1

    .line 1918
    add-int/lit8 v2, v12, 0x30

    int-to-char v2, v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/DigitList;->append(I)V

    goto :goto_b

    .line 1921
    :cond_1c
    if-lez v12, :cond_22

    const/16 v2, 0x9

    if-gt v12, v2, :cond_22

    .line 1923
    if-eqz v32, :cond_21

    .line 1924
    if-eqz v22, :cond_1d

    .line 1926
    const/16 v31, 0x1

    .line 1927
    goto :goto_a

    .line 1929
    :cond_1d
    const/4 v2, -0x1

    if-eq v8, v2, :cond_21

    .line 1930
    const/4 v2, -0x1

    move/from16 v0, v21

    if-eq v0, v2, :cond_1e

    sub-int v2, v8, v21

    add-int/lit8 v2, v2, -0x1

    move/from16 v0, v20

    if-ne v2, v0, :cond_1f

    :cond_1e
    const/4 v2, -0x1

    move/from16 v0, v21

    if-ne v0, v2, :cond_20

    sub-int v2, v4, v25

    add-int/lit8 v2, v2, -0x1

    move/from16 v0, v20

    if-le v2, v0, :cond_20

    .line 1932
    :cond_1f
    const/16 v31, 0x1

    .line 1933
    goto/16 :goto_a

    .line 1935
    :cond_20
    move/from16 v21, v8

    .line 1939
    :cond_21
    const/16 v29, 0x1

    .line 1940
    add-int/lit8 v13, v13, 0x1

    .line 1941
    add-int/lit8 v2, v12, 0x30

    int-to-char v2, v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/DigitList;->append(I)V

    .line 1944
    const/4 v8, -0x1

    .line 1945
    goto :goto_b

    .line 1946
    :cond_22
    if-nez p4, :cond_25

    invoke-virtual {v11, v9}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 1948
    if-eqz v32, :cond_24

    .line 1949
    const/4 v2, -0x1

    if-ne v8, v2, :cond_23

    const/4 v2, -0x1

    move/from16 v0, v21

    if-eq v0, v2, :cond_24

    sub-int v2, v4, v21

    move-object/from16 v0, p0

    iget-byte v3, v0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    add-int/lit8 v3, v3, 0x1

    if-eq v2, v3, :cond_24

    .line 1951
    :cond_23
    const/16 v31, 0x1

    .line 1952
    goto/16 :goto_a

    .line 1957
    :cond_24
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->isParseIntegerOnly()Z

    move-result v2

    if-nez v2, :cond_10

    if-nez v28, :cond_10

    .line 1958
    move-object/from16 v0, p3

    iput v13, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 1959
    const/16 v28, 0x1

    .line 1960
    const/16 v22, 0x0

    .line 1963
    invoke-virtual {v11, v9, v9}, Lcom/ibm/icu/text/UnicodeSet;->set(II)Lcom/ibm/icu/text/UnicodeSet;

    goto/16 :goto_b

    .line 1965
    :cond_25
    if-nez p4, :cond_28

    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->isGroupingUsed()Z

    move-result v2

    if-eqz v2, :cond_28

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 1967
    if-nez v28, :cond_10

    .line 1970
    if-eqz v32, :cond_27

    .line 1971
    if-eqz v29, :cond_26

    const/4 v2, -0x1

    if-eq v8, v2, :cond_27

    .line 1973
    :cond_26
    const/16 v31, 0x1

    .line 1974
    goto/16 :goto_a

    .line 1978
    :cond_27
    move-object/from16 v0, v19

    invoke-virtual {v0, v9, v9}, Lcom/ibm/icu/text/UnicodeSet;->set(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 1983
    move v8, v4

    .line 1984
    goto/16 :goto_b

    .line 1985
    :cond_28
    if-nez p4, :cond_10

    if-nez v30, :cond_10

    const/4 v2, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v3

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v4, v1, v2, v3}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1990
    const/16 v23, 0x0

    .line 1991
    .local v23, "negExp":Z
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v2

    add-int v26, v4, v2

    .line 1992
    .local v26, "pos":I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v2

    move/from16 v0, v26

    if-ge v0, v2, :cond_29

    .line 1993
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v9

    .line 1994
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPlusSign()C

    move-result v2

    if-ne v9, v2, :cond_2c

    .line 1995
    add-int/lit8 v26, v26, 0x1

    .line 2002
    :cond_29
    :goto_c
    new-instance v16, Lcom/ibm/icu/text/DigitList;

    invoke-direct/range {v16 .. v16}, Lcom/ibm/icu/text/DigitList;-><init>()V

    .line 2003
    .local v16, "exponentDigits":Lcom/ibm/icu/text/DigitList;
    const/4 v2, 0x0

    move-object/from16 v0, v16

    iput v2, v0, Lcom/ibm/icu/text/DigitList;->count:I

    .line 2004
    :goto_d
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v2

    move/from16 v0, v26

    if-ge v0, v2, :cond_2d

    .line 2005
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    sub-int v12, v2, v33

    .line 2006
    if-ltz v12, :cond_2a

    const/16 v2, 0x9

    if-le v12, v2, :cond_2b

    .line 2013
    :cond_2a
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/ibm/icu/lang/UCharacter;->digit(II)I

    move-result v12

    .line 2015
    :cond_2b
    if-ltz v12, :cond_2d

    const/16 v2, 0x9

    if-gt v12, v2, :cond_2d

    .line 2016
    add-int/lit8 v2, v12, 0x30

    int-to-char v2, v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/DigitList;->append(I)V

    .line 2017
    add-int/lit8 v26, v26, 0x1

    .line 2018
    goto :goto_d

    .line 1996
    .end local v16    # "exponentDigits":Lcom/ibm/icu/text/DigitList;
    :cond_2c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getMinusSign()C

    move-result v2

    if-ne v9, v2, :cond_29

    .line 1997
    add-int/lit8 v26, v26, 0x1

    .line 1998
    const/16 v23, 0x1

    goto :goto_c

    .line 2023
    .restart local v16    # "exponentDigits":Lcom/ibm/icu/text/DigitList;
    :cond_2d
    move-object/from16 v0, v16

    iget v2, v0, Lcom/ibm/icu/text/DigitList;->count:I

    if-lez v2, :cond_10

    .line 2025
    if-eqz v32, :cond_2f

    .line 2026
    const/4 v2, -0x1

    if-ne v8, v2, :cond_2e

    const/4 v2, -0x1

    move/from16 v0, v21

    if-eq v0, v2, :cond_2f

    .line 2027
    :cond_2e
    const/16 v31, 0x1

    .line 2028
    goto/16 :goto_a

    .line 2034
    :cond_2f
    move-object/from16 v0, v16

    iget v2, v0, Lcom/ibm/icu/text/DigitList;->count:I

    const/16 v3, 0xa

    if-le v2, v3, :cond_32

    .line 2035
    if-eqz v23, :cond_31

    .line 2037
    const/4 v2, 0x2

    const/4 v3, 0x1

    aput-boolean v3, p5, v2

    .line 2049
    :cond_30
    :goto_e
    move/from16 v4, v26

    .line 2050
    const/16 v30, 0x1

    .line 2051
    goto/16 :goto_a

    .line 2040
    :cond_31
    const/4 v2, 0x0

    const/4 v3, 0x1

    aput-boolean v3, p5, v2

    goto :goto_e

    .line 2043
    :cond_32
    move-object/from16 v0, v16

    iget v2, v0, Lcom/ibm/icu/text/DigitList;->count:I

    move-object/from16 v0, v16

    iput v2, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 2044
    invoke-virtual/range {v16 .. v16}, Lcom/ibm/icu/text/DigitList;->getLong()J

    move-result-wide v14

    .line 2045
    if-eqz v23, :cond_30

    .line 2046
    neg-long v14, v14

    goto :goto_e

    .line 2077
    .end local v9    # "ch":C
    .end local v16    # "exponentDigits":Lcom/ibm/icu/text/DigitList;
    .end local v23    # "negExp":Z
    .end local v26    # "pos":I
    :cond_33
    if-nez v28, :cond_34

    move-object/from16 v0, p3

    iput v13, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    .line 2080
    :cond_34
    move-object/from16 v0, p3

    iget v2, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    int-to-long v2, v2

    add-long/2addr v14, v2

    .line 2081
    const-wide/16 v2, -0x3e8

    cmp-long v2, v14, v2

    if-gez v2, :cond_35

    .line 2082
    const/4 v2, 0x2

    const/4 v3, 0x1

    aput-boolean v3, p5, v2

    .line 2093
    :goto_f
    if-nez v29, :cond_3

    if-nez v13, :cond_3

    .line 2094
    move-object/from16 v0, p2

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 2095
    move-object/from16 v0, p2

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 2096
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 2083
    :cond_35
    const-wide/16 v2, 0x3e8

    cmp-long v2, v14, v2

    if-lez v2, :cond_36

    .line 2084
    const/4 v2, 0x0

    const/4 v3, 0x1

    aput-boolean v3, p5, v2

    goto :goto_f

    .line 2086
    :cond_36
    long-to-int v2, v14

    move-object/from16 v0, p3

    iput v2, v0, Lcom/ibm/icu/text/DigitList;->decimalAt:I

    goto :goto_f

    .line 2115
    .end local v8    # "backup":I
    .end local v10    # "decimal":C
    .end local v11    # "decimalSet":Lcom/ibm/icu/text/UnicodeSet;
    .end local v12    # "digit":I
    .end local v13    # "digitCount":I
    .end local v14    # "exponent":J
    .end local v17    # "exponentSep":Ljava/lang/String;
    .end local v18    # "grouping":C
    .end local v19    # "groupingSet":Lcom/ibm/icu/text/UnicodeSet;
    .end local v20    # "gs2":I
    .end local v21    # "lastGroup":I
    .end local v22    # "leadingZero":Z
    .end local v28    # "sawDecimal":Z
    .end local v29    # "sawDigit":Z
    .end local v30    # "sawExponent":Z
    .end local v31    # "strictFail":Z
    .end local v32    # "strictParse":Z
    .end local v33    # "zero":C
    :cond_37
    move/from16 v0, v24

    move/from16 v1, v27

    if-le v0, v1, :cond_7

    .line 2116
    const/16 v27, -0x1

    goto/16 :goto_2

    .line 2121
    :cond_38
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_3

    :cond_39
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 2126
    :cond_3a
    if-ltz v27, :cond_3c

    move/from16 v2, v27

    :goto_10
    add-int/2addr v4, v2

    .line 2129
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    if-lez v2, :cond_3b

    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3b

    .line 2130
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/ibm/icu/text/DecimalFormat;->skipPadding(Ljava/lang/String;I)I

    move-result v4

    .line 2133
    :cond_3b
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 2135
    const/4 v3, 0x1

    if-ltz v27, :cond_3d

    const/4 v2, 0x1

    :goto_11
    aput-boolean v2, p5, v3

    .line 2137
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v2

    move/from16 v0, v25

    if-ne v2, v0, :cond_3e

    .line 2138
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 2139
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_3c
    move/from16 v2, v24

    .line 2126
    goto :goto_10

    .line 2135
    :cond_3d
    const/4 v2, 0x0

    goto :goto_11

    .line 2141
    :cond_3e
    const/4 v2, 0x1

    goto/16 :goto_5
.end method

.method private toPattern(Z)Ljava/lang/String;
    .locals 27
    .param p1, "localized"    # Z

    .prologue
    .line 3567
    new-instance v17, Ljava/lang/StringBuffer;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuffer;-><init>()V

    .line 3568
    .local v17, "result":Ljava/lang/StringBuffer;
    if-eqz p1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getZeroDigit()C

    move-result v23

    .line 3569
    .local v23, "zero":C
    :goto_0
    if-eqz p1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getDigit()C

    move-result v6

    .line 3570
    .local v6, "digit":C
    :goto_1
    const/16 v20, 0x0

    .line 3571
    .local v20, "sigDigit":C
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->areSignificantDigitsUsed()Z

    move-result v22

    .line 3572
    .local v22, "useSigDig":Z
    if-eqz v22, :cond_0

    .line 3573
    if-eqz p1, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getSignificantDigit()C

    move-result v20

    .line 3575
    :cond_0
    :goto_2
    if-eqz p1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getGroupingSeparator()C

    move-result v8

    .line 3578
    .local v8, "group":C
    :goto_3
    const/16 v18, 0x0

    .line 3579
    .local v18, "roundingDecimalPos":I
    const/16 v19, 0x0

    .line 3580
    .local v19, "roundingDigits":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    move/from16 v24, v0

    if-lez v24, :cond_b

    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    .line 3581
    .local v13, "padPos":I
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    move/from16 v24, v0

    if-lez v24, :cond_d

    new-instance v25, Ljava/lang/StringBuffer;

    const/16 v24, 0x2

    move-object/from16 v0, v25

    move/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    if-eqz p1, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPadEscape()C

    move-result v24

    :goto_5
    move-object/from16 v0, v25

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v24

    move-object/from16 v0, p0

    iget-char v0, v0, Lcom/ibm/icu/text/DecimalFormat;->pad:C

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    .line 3586
    .local v14, "padSpec":Ljava/lang/String;
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    move-object/from16 v24, v0

    if-eqz v24, :cond_1

    .line 3587
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/ibm/icu/math/BigDecimal;->scale()I

    move-result v9

    .line 3588
    .local v9, "i":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Lcom/ibm/icu/math/BigDecimal;->movePointRight(I)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/ibm/icu/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v19

    .line 3589
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v24

    sub-int v18, v24, v9

    .line 3591
    .end local v9    # "i":I
    :cond_1
    const/4 v15, 0x0

    .local v15, "part":I
    :goto_7
    const/16 v24, 0x2

    move/from16 v0, v24

    if-ge v15, v0, :cond_28

    .line 3593
    if-nez v13, :cond_2

    .line 3594
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3599
    :cond_2
    if-eqz v15, :cond_e

    const/16 v24, 0x1

    :goto_8
    const/16 v25, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/DecimalFormat;->appendAffixPattern(Ljava/lang/StringBuffer;ZZZ)V

    .line 3600
    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v13, v0, :cond_3

    .line 3601
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3603
    :cond_3
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->length()I

    move-result v21

    .line 3604
    .local v21, "sub0Start":I
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->isGroupingUsed()Z

    move-result v24

    if-eqz v24, :cond_f

    const/16 v24, 0x0

    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    move/from16 v25, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 3605
    .local v7, "g":I
    :goto_9
    if-lez v7, :cond_4

    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    move/from16 v24, v0

    if-lez v24, :cond_4

    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_4

    .line 3606
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    move/from16 v24, v0

    add-int v7, v7, v24

    .line 3608
    :cond_4
    const/4 v10, 0x0

    .local v10, "maxDig":I
    const/4 v12, 0x0

    .local v12, "minDig":I
    const/4 v11, 0x0

    .line 3609
    .local v11, "maxSigDig":I
    if-eqz v22, :cond_10

    .line 3610
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMinimumSignificantDigits()I

    move-result v12

    .line 3611
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMaximumSignificantDigits()I

    move-result v11

    move v10, v11

    .line 3616
    :goto_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    move/from16 v24, v0

    if-eqz v24, :cond_11

    .line 3617
    const/16 v24, 0x8

    move/from16 v0, v24

    if-le v10, v0, :cond_5

    .line 3618
    const/4 v10, 0x1

    .line 3626
    :cond_5
    :goto_b
    move v9, v10

    .restart local v9    # "i":I
    :goto_c
    if-lez v9, :cond_17

    .line 3627
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    move/from16 v24, v0

    if-nez v24, :cond_6

    if-ge v9, v10, :cond_6

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/ibm/icu/text/DecimalFormat;->isGroupingPosition(I)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 3629
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3631
    :cond_6
    if-eqz v22, :cond_14

    .line 3636
    if-lt v11, v9, :cond_13

    sub-int v24, v11, v12

    move/from16 v0, v24

    if-le v9, v0, :cond_13

    move/from16 v24, v20

    :goto_d
    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3626
    :goto_e
    add-int/lit8 v9, v9, -0x1

    goto :goto_c

    .line 3568
    .end local v6    # "digit":C
    .end local v7    # "g":I
    .end local v8    # "group":C
    .end local v9    # "i":I
    .end local v10    # "maxDig":I
    .end local v11    # "maxSigDig":I
    .end local v12    # "minDig":I
    .end local v13    # "padPos":I
    .end local v14    # "padSpec":Ljava/lang/String;
    .end local v15    # "part":I
    .end local v18    # "roundingDecimalPos":I
    .end local v19    # "roundingDigits":Ljava/lang/String;
    .end local v20    # "sigDigit":C
    .end local v21    # "sub0Start":I
    .end local v22    # "useSigDig":Z
    .end local v23    # "zero":C
    :cond_7
    const/16 v23, 0x30

    goto/16 :goto_0

    .line 3569
    .restart local v23    # "zero":C
    :cond_8
    const/16 v6, 0x23

    goto/16 :goto_1

    .line 3573
    .restart local v6    # "digit":C
    .restart local v20    # "sigDigit":C
    .restart local v22    # "useSigDig":Z
    :cond_9
    const/16 v20, 0x40

    goto/16 :goto_2

    .line 3575
    :cond_a
    const/16 v8, 0x2c

    goto/16 :goto_3

    .line 3580
    .restart local v8    # "group":C
    .restart local v18    # "roundingDecimalPos":I
    .restart local v19    # "roundingDigits":Ljava/lang/String;
    :cond_b
    const/4 v13, -0x1

    goto/16 :goto_4

    .line 3581
    .restart local v13    # "padPos":I
    :cond_c
    const/16 v24, 0x2a

    goto/16 :goto_5

    :cond_d
    const/4 v14, 0x0

    goto/16 :goto_6

    .line 3599
    .restart local v14    # "padSpec":Ljava/lang/String;
    .restart local v15    # "part":I
    :cond_e
    const/16 v24, 0x0

    goto/16 :goto_8

    .line 3604
    .restart local v21    # "sub0Start":I
    :cond_f
    const/4 v7, 0x0

    goto/16 :goto_9

    .line 3613
    .restart local v7    # "g":I
    .restart local v10    # "maxDig":I
    .restart local v11    # "maxSigDig":I
    .restart local v12    # "minDig":I
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMinimumIntegerDigits()I

    move-result v12

    .line 3614
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMaximumIntegerDigits()I

    move-result v10

    goto :goto_a

    .line 3620
    :cond_11
    if-eqz v22, :cond_12

    .line 3621
    add-int/lit8 v24, v7, 0x1

    move/from16 v0, v24

    invoke-static {v10, v0}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 3622
    goto :goto_b

    .line 3623
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMinimumIntegerDigits()I

    move-result v24

    move/from16 v0, v24

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v24

    move/from16 v0, v24

    move/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v24

    add-int/lit8 v10, v24, 0x1

    goto :goto_b

    .restart local v9    # "i":I
    :cond_13
    move/from16 v24, v6

    .line 3636
    goto :goto_d

    .line 3638
    :cond_14
    if-eqz v19, :cond_15

    .line 3639
    sub-int v16, v18, v9

    .line 3640
    .local v16, "pos":I
    if-ltz v16, :cond_15

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v24

    move/from16 v0, v16

    move/from16 v1, v24

    if-ge v0, v1, :cond_15

    .line 3641
    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v24

    add-int/lit8 v24, v24, -0x30

    add-int v24, v24, v23

    move/from16 v0, v24

    int-to-char v0, v0

    move/from16 v24, v0

    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_e

    .line 3645
    .end local v16    # "pos":I
    :cond_15
    if-gt v9, v12, :cond_16

    move/from16 v24, v23

    :goto_f
    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_e

    :cond_16
    move/from16 v24, v6

    goto :goto_f

    .line 3648
    :cond_17
    if-nez v22, :cond_1e

    .line 3649
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMaximumFractionDigits()I

    move-result v24

    if-gtz v24, :cond_18

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->decimalSeparatorAlwaysShown:Z

    move/from16 v24, v0

    if-eqz v24, :cond_19

    .line 3650
    :cond_18
    if-eqz p1, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v24

    :goto_10
    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3653
    :cond_19
    move/from16 v16, v18

    .line 3654
    .restart local v16    # "pos":I
    const/4 v9, 0x0

    :goto_11
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMaximumFractionDigits()I

    move-result v24

    move/from16 v0, v24

    if-ge v9, v0, :cond_1e

    .line 3655
    if-eqz v19, :cond_1c

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v24

    move/from16 v0, v16

    move/from16 v1, v24

    if-ge v0, v1, :cond_1c

    .line 3657
    if-gez v16, :cond_1b

    move/from16 v24, v23

    :goto_12
    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3659
    add-int/lit8 v16, v16, 0x1

    .line 3654
    :goto_13
    add-int/lit8 v9, v9, 0x1

    goto :goto_11

    .line 3650
    .end local v16    # "pos":I
    :cond_1a
    const/16 v24, 0x2e

    goto :goto_10

    .line 3657
    .restart local v16    # "pos":I
    :cond_1b
    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v24

    add-int/lit8 v24, v24, -0x30

    add-int v24, v24, v23

    move/from16 v0, v24

    int-to-char v0, v0

    move/from16 v24, v0

    goto :goto_12

    .line 3662
    :cond_1c
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/DecimalFormat;->getMinimumFractionDigits()I

    move-result v24

    move/from16 v0, v24

    if-ge v9, v0, :cond_1d

    move/from16 v24, v23

    :goto_14
    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_13

    :cond_1d
    move/from16 v24, v6

    goto :goto_14

    .line 3665
    .end local v16    # "pos":I
    :cond_1e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    move/from16 v24, v0

    if-eqz v24, :cond_22

    .line 3666
    if-eqz p1, :cond_20

    .line 3667
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getExponentSeparator()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3671
    :goto_15
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->exponentSignAlwaysShown:Z

    move/from16 v24, v0

    if-eqz v24, :cond_1f

    .line 3672
    if-eqz p1, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPlusSign()C

    move-result v24

    :goto_16
    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3675
    :cond_1f
    const/4 v9, 0x0

    :goto_17
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/DecimalFormat;->minExponentDigits:B

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v9, v0, :cond_22

    .line 3676
    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3675
    add-int/lit8 v9, v9, 0x1

    goto :goto_17

    .line 3669
    :cond_20
    const/16 v24, 0x45

    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_15

    .line 3672
    :cond_21
    const/16 v24, 0x2b

    goto :goto_16

    .line 3679
    :cond_22
    if-eqz v14, :cond_25

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    move/from16 v24, v0

    if-nez v24, :cond_25

    .line 3680
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    move/from16 v24, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->length()I

    move-result v25

    sub-int v24, v24, v25

    add-int v25, v24, v21

    if-nez v15, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->positivePrefix:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->positiveSuffix:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v24, v24, v26

    :goto_18
    sub-int v5, v25, v24

    .line 3684
    .local v5, "add":I
    :cond_23
    :goto_19
    if-lez v5, :cond_25

    .line 3685
    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1, v6}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 3686
    add-int/lit8 v10, v10, 0x1

    .line 3687
    add-int/lit8 v5, v5, -0x1

    .line 3691
    const/16 v24, 0x1

    move/from16 v0, v24

    if-le v5, v0, :cond_23

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/ibm/icu/text/DecimalFormat;->isGroupingPosition(I)Z

    move-result v24

    if-eqz v24, :cond_23

    .line 3692
    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1, v8}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 3693
    add-int/lit8 v5, v5, -0x1

    .line 3694
    goto :goto_19

    .line 3680
    .end local v5    # "add":I
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->negativePrefix:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->negativeSuffix:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v24, v24, v26

    goto :goto_18

    .line 3697
    :cond_25
    const/16 v24, 0x2

    move/from16 v0, v24

    if-ne v13, v0, :cond_26

    .line 3698
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3703
    :cond_26
    if-eqz v15, :cond_29

    const/16 v24, 0x1

    :goto_1a
    const/16 v25, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/DecimalFormat;->appendAffixPattern(Ljava/lang/StringBuffer;ZZZ)V

    .line 3704
    const/16 v24, 0x3

    move/from16 v0, v24

    if-ne v13, v0, :cond_27

    .line 3705
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3707
    :cond_27
    if-nez v15, :cond_2b

    .line 3708
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->negativeSuffix:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->positiveSuffix:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_2a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->negativePrefix:Ljava/lang/String;

    move-object/from16 v24, v0

    new-instance v25, Ljava/lang/StringBuffer;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuffer;-><init>()V

    const/16 v26, 0x2d

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->positivePrefix:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_2a

    .line 3717
    .end local v7    # "g":I
    .end local v9    # "i":I
    .end local v10    # "maxDig":I
    .end local v11    # "maxSigDig":I
    .end local v12    # "minDig":I
    .end local v21    # "sub0Start":I
    :cond_28
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v24

    return-object v24

    .line 3703
    .restart local v7    # "g":I
    .restart local v9    # "i":I
    .restart local v10    # "maxDig":I
    .restart local v11    # "maxSigDig":I
    .restart local v12    # "minDig":I
    .restart local v21    # "sub0Start":I
    :cond_29
    const/16 v24, 0x0

    goto :goto_1a

    .line 3712
    :cond_2a
    if-eqz p1, :cond_2c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getPatternSeparator()C

    move-result v24

    :goto_1b
    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3591
    :cond_2b
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_7

    .line 3712
    :cond_2c
    const/16 v24, 0x3b

    goto :goto_1b
.end method

.method private unquote(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "pat"    # Ljava/lang/String;

    .prologue
    .line 3140
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v0, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 3141
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .line 3142
    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 3143
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 3144
    .local v1, "ch":C
    const/16 v4, 0x27

    if-eq v1, v4, :cond_0

    .line 3145
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_0
    move v2, v3

    .line 3147
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 3148
    .end local v1    # "ch":C
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 0
    .param p1, "stream"    # Ljava/io/ObjectOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4498
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 4499
    return-void
.end method


# virtual methods
.method public applyLocalizedPattern(Ljava/lang/String;)V
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 3762
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/DecimalFormat;->applyPattern(Ljava/lang/String;Z)V

    .line 3763
    return-void
.end method

.method public applyPattern(Ljava/lang/String;)V
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 3739
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/DecimalFormat;->applyPattern(Ljava/lang/String;Z)V

    .line 3740
    return-void
.end method

.method public areSignificantDigitsUsed()Z
    .locals 1

    .prologue
    .line 4383
    iget-boolean v0, p0, Lcom/ibm/icu/text/DecimalFormat;->useSignificantDigits:Z

    return v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 3081
    :try_start_0
    invoke-super {p0}, Lcom/ibm/icu/text/NumberFormat;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/DecimalFormat;

    .line 3082
    .local v1, "other":Lcom/ibm/icu/text/DecimalFormat;
    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v2}, Lcom/ibm/icu/text/DecimalFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/DecimalFormatSymbols;

    iput-object v2, v1, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 3083
    new-instance v2, Lcom/ibm/icu/text/DigitList;

    invoke-direct {v2}, Lcom/ibm/icu/text/DigitList;-><init>()V

    iput-object v2, v1, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3090
    return-object v1

    .line 3091
    .end local v1    # "other":Lcom/ibm/icu/text/DecimalFormat;
    :catch_0
    move-exception v0

    .line 3092
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 3102
    if-nez p1, :cond_1

    .line 3110
    :cond_0
    :goto_0
    return v1

    .line 3103
    :cond_1
    invoke-super {p0, p1}, Lcom/ibm/icu/text/NumberFormat;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 3105
    check-cast v0, Lcom/ibm/icu/text/DecimalFormat;

    .line 3110
    .local v0, "other":Lcom/ibm/icu/text/DecimalFormat;
    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->posPrefixPattern:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->posPrefixPattern:Ljava/lang/String;

    iget-object v3, v0, Lcom/ibm/icu/text/DecimalFormat;->posPrefixPattern:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/ibm/icu/text/DecimalFormat;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->posSuffixPattern:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->posSuffixPattern:Ljava/lang/String;

    iget-object v3, v0, Lcom/ibm/icu/text/DecimalFormat;->posSuffixPattern:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/ibm/icu/text/DecimalFormat;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->negPrefixPattern:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->negPrefixPattern:Ljava/lang/String;

    iget-object v3, v0, Lcom/ibm/icu/text/DecimalFormat;->negPrefixPattern:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/ibm/icu/text/DecimalFormat;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->negSuffixPattern:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->negSuffixPattern:Ljava/lang/String;

    iget-object v3, v0, Lcom/ibm/icu/text/DecimalFormat;->negSuffixPattern:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/ibm/icu/text/DecimalFormat;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    iget v3, v0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    if-ne v2, v3, :cond_0

    iget-byte v2, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    iget-byte v3, v0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    if-ne v2, v3, :cond_0

    iget-byte v2, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    iget-byte v3, v0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/ibm/icu/text/DecimalFormat;->decimalSeparatorAlwaysShown:Z

    iget-boolean v3, v0, Lcom/ibm/icu/text/DecimalFormat;->decimalSeparatorAlwaysShown:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    iget-boolean v3, v0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    if-eqz v2, :cond_2

    iget-byte v2, p0, Lcom/ibm/icu/text/DecimalFormat;->minExponentDigits:B

    iget-byte v3, v0, Lcom/ibm/icu/text/DecimalFormat;->minExponentDigits:B

    if-ne v2, v3, :cond_0

    :cond_2
    iget-boolean v2, p0, Lcom/ibm/icu/text/DecimalFormat;->useSignificantDigits:Z

    iget-boolean v3, v0, Lcom/ibm/icu/text/DecimalFormat;->useSignificantDigits:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/ibm/icu/text/DecimalFormat;->useSignificantDigits:Z

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/ibm/icu/text/DecimalFormat;->minSignificantDigits:I

    iget v3, v0, Lcom/ibm/icu/text/DecimalFormat;->minSignificantDigits:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/ibm/icu/text/DecimalFormat;->maxSignificantDigits:I

    iget v3, v0, Lcom/ibm/icu/text/DecimalFormat;->maxSignificantDigits:I

    if-ne v2, v3, :cond_0

    :cond_3
    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    iget-object v3, v0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/DecimalFormatSymbols;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public format(DLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 7
    .param p1, "number"    # D
    .param p3, "result"    # Ljava/lang/StringBuffer;
    .param p4, "fieldPosition"    # Ljava/text/FieldPosition;

    .prologue
    .line 694
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/text/DecimalFormat;->format(DLjava/lang/StringBuffer;Ljava/text/FieldPosition;Z)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public format(JLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 7
    .param p1, "number"    # J
    .param p3, "result"    # Ljava/lang/StringBuffer;
    .param p4, "fieldPosition"    # Ljava/text/FieldPosition;

    .prologue
    .line 890
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/text/DecimalFormat;->format(JLjava/lang/StringBuffer;Ljava/text/FieldPosition;Z)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public format(Lcom/ibm/icu/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 6
    .param p1, "number"    # Lcom/ibm/icu/math/BigDecimal;
    .param p2, "result"    # Ljava/lang/StringBuffer;
    .param p3, "fieldPosition"    # Ljava/text/FieldPosition;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1018
    iget v2, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    if-eq v2, v0, :cond_0

    .line 1019
    iget v2, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/ibm/icu/math/BigDecimal;->valueOf(J)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/ibm/icu/math/BigDecimal;->multiply(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object p1

    .line 1022
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    if-eqz v2, :cond_1

    .line 1023
    iget-object v2, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    iget v3, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingMode:I

    invoke-virtual {p1, v2, v1, v3}, Lcom/ibm/icu/math/BigDecimal;->divide(Lcom/ibm/icu/math/BigDecimal;II)Lcom/ibm/icu/math/BigDecimal;

    move-result-object v2

    iget-object v3, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    invoke-virtual {v2, v3}, Lcom/ibm/icu/math/BigDecimal;->multiply(Lcom/ibm/icu/math/BigDecimal;)Lcom/ibm/icu/math/BigDecimal;

    move-result-object p1

    .line 1027
    :cond_1
    iget-object v3, p0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    monitor-enter v3

    .line 1028
    :try_start_0
    iget-object v4, p0, Lcom/ibm/icu/text/DecimalFormat;->digitList:Lcom/ibm/icu/text/DigitList;

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/ibm/icu/text/DecimalFormat;->precision(Z)I

    move-result v5

    iget-boolean v2, p0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/ibm/icu/text/DecimalFormat;->areSignificantDigitsUsed()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_0
    invoke-virtual {v4, p1, v5, v2}, Lcom/ibm/icu/text/DigitList;->set(Lcom/ibm/icu/math/BigDecimal;IZ)V

    .line 1030
    invoke-virtual {p1}, Lcom/ibm/icu/math/BigDecimal;->signum()I

    move-result v2

    if-gez v2, :cond_3

    :goto_1
    const/4 v1, 0x0

    invoke-direct {p0, p2, p3, v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->subformat(Ljava/lang/StringBuffer;Ljava/text/FieldPosition;ZZ)Ljava/lang/StringBuffer;

    move-result-object v0

    monitor-exit v3

    return-object v0

    :cond_2
    move v2, v1

    .line 1028
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1030
    goto :goto_1

    .line 1031
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public format(Ljava/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 1
    .param p1, "number"    # Ljava/math/BigDecimal;
    .param p2, "result"    # Ljava/lang/StringBuffer;
    .param p3, "fieldPosition"    # Ljava/text/FieldPosition;

    .prologue
    .line 983
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/ibm/icu/text/DecimalFormat;->format(Ljava/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Z)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public format(Ljava/math/BigInteger;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 1
    .param p1, "number"    # Ljava/math/BigInteger;
    .param p2, "result"    # Ljava/lang/StringBuffer;
    .param p3, "fieldPosition"    # Ljava/text/FieldPosition;

    .prologue
    .line 950
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/ibm/icu/text/DecimalFormat;->format(Ljava/math/BigInteger;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Z)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public formatToCharacterIterator(Ljava/lang/Object;)Ljava/text/AttributedCharacterIterator;
    .locals 13
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x1

    const/4 v12, 0x0

    .line 3456
    instance-of v1, p1, Ljava/lang/Number;

    if-nez v1, :cond_0

    .line 3457
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_0
    move-object v9, p1

    .line 3458
    check-cast v9, Ljava/lang/Number;

    .line 3459
    .local v9, "number":Ljava/lang/Number;
    const/4 v11, 0x0

    .line 3460
    .local v11, "text":Ljava/lang/StringBuffer;
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->attributes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 3461
    instance-of v1, p1, Ljava/math/BigInteger;

    if-eqz v1, :cond_2

    .line 3462
    check-cast v9, Ljava/math/BigInteger;

    .end local v9    # "number":Ljava/lang/Number;
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v2, Ljava/text/FieldPosition;

    invoke-direct {v2, v12}, Ljava/text/FieldPosition;-><init>(I)V

    invoke-direct {p0, v9, v1, v2, v6}, Lcom/ibm/icu/text/DecimalFormat;->format(Ljava/math/BigInteger;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Z)Ljava/lang/StringBuffer;

    move-result-object v11

    .line 3475
    :cond_1
    :goto_0
    new-instance v0, Ljava/text/AttributedString;

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/text/AttributedString;-><init>(Ljava/lang/String;)V

    .line 3478
    .local v0, "as":Ljava/text/AttributedString;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->attributes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v8, v1, :cond_6

    .line 3479
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->attributes:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/text/FieldPosition;

    .line 3480
    .local v10, "pos":Ljava/text/FieldPosition;
    invoke-virtual {v10}, Ljava/text/FieldPosition;->getFieldAttribute()Ljava/text/Format$Field;

    move-result-object v7

    .line 3481
    .local v7, "attribute":Ljava/text/Format$Field;
    invoke-virtual {v10}, Ljava/text/FieldPosition;->getBeginIndex()I

    move-result v1

    invoke-virtual {v10}, Ljava/text/FieldPosition;->getEndIndex()I

    move-result v2

    invoke-virtual {v0, v7, v7, v1, v2}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;II)V

    .line 3478
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 3464
    .end local v0    # "as":Ljava/text/AttributedString;
    .end local v7    # "attribute":Ljava/text/Format$Field;
    .end local v8    # "i":I
    .end local v10    # "pos":Ljava/text/FieldPosition;
    .restart local v9    # "number":Ljava/lang/Number;
    :cond_2
    instance-of v1, p1, Ljava/math/BigDecimal;

    if-eqz v1, :cond_3

    .line 3465
    check-cast v9, Ljava/math/BigDecimal;

    .end local v9    # "number":Ljava/lang/Number;
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v2, Ljava/text/FieldPosition;

    invoke-direct {v2, v12}, Ljava/text/FieldPosition;-><init>(I)V

    invoke-direct {p0, v9, v1, v2, v6}, Lcom/ibm/icu/text/DecimalFormat;->format(Ljava/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Z)Ljava/lang/StringBuffer;

    move-result-object v11

    .line 3467
    goto :goto_0

    .restart local v9    # "number":Ljava/lang/Number;
    :cond_3
    instance-of v1, p1, Ljava/lang/Double;

    if-eqz v1, :cond_4

    .line 3468
    invoke-virtual {v9}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v5, Ljava/text/FieldPosition;

    invoke-direct {v5, v12}, Ljava/text/FieldPosition;-><init>(I)V

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/text/DecimalFormat;->format(DLjava/lang/StringBuffer;Ljava/text/FieldPosition;Z)Ljava/lang/StringBuffer;

    move-result-object v11

    .line 3470
    goto :goto_0

    :cond_4
    instance-of v1, p1, Ljava/lang/Integer;

    if-nez v1, :cond_5

    instance-of v1, p1, Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 3471
    :cond_5
    invoke-virtual {v9}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v5, Ljava/text/FieldPosition;

    invoke-direct {v5, v12}, Ljava/text/FieldPosition;-><init>(I)V

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/ibm/icu/text/DecimalFormat;->format(JLjava/lang/StringBuffer;Ljava/text/FieldPosition;Z)Ljava/lang/StringBuffer;

    move-result-object v11

    goto :goto_0

    .line 3486
    .end local v9    # "number":Ljava/lang/Number;
    .restart local v0    # "as":Ljava/text/AttributedString;
    .restart local v8    # "i":I
    :cond_6
    invoke-virtual {v0}, Ljava/text/AttributedString;->getIterator()Ljava/text/AttributedCharacterIterator;

    move-result-object v1

    return-object v1
.end method

.method public getDecimalFormatSymbols()Lcom/ibm/icu/text/DecimalFormatSymbols;
    .locals 2

    .prologue
    .line 2454
    :try_start_0
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v1}, Lcom/ibm/icu/text/DecimalFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/DecimalFormatSymbols;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2456
    :goto_0
    return-object v1

    .line 2455
    :catch_0
    move-exception v0

    .line 2456
    .local v0, "foo":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getEffectiveCurrency()Lcom/ibm/icu/util/Currency;
    .locals 2

    .prologue
    .line 4443
    invoke-virtual {p0}, Lcom/ibm/icu/text/DecimalFormat;->getCurrency()Lcom/ibm/icu/util/Currency;

    move-result-object v0

    .line 4444
    .local v0, "c":Lcom/ibm/icu/util/Currency;
    if-nez v0, :cond_0

    .line 4445
    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v1}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getInternationalCurrencySymbol()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/ibm/icu/util/Currency;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/util/Currency;

    move-result-object v0

    .line 4447
    :cond_0
    return-object v0
.end method

.method public getFormatWidth()I
    .locals 1

    .prologue
    .line 2769
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    return v0
.end method

.method public getGroupingSize()I
    .locals 1

    .prologue
    .line 2994
    iget-byte v0, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    return v0
.end method

.method public getMaximumSignificantDigits()I
    .locals 1

    .prologue
    .line 4335
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormat;->maxSignificantDigits:I

    return v0
.end method

.method public getMinimumExponentDigits()B
    .locals 1

    .prologue
    .line 2925
    iget-byte v0, p0, Lcom/ibm/icu/text/DecimalFormat;->minExponentDigits:B

    return v0
.end method

.method public getMinimumSignificantDigits()I
    .locals 1

    .prologue
    .line 4324
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormat;->minSignificantDigits:I

    return v0
.end method

.method public getMultiplier()I
    .locals 1

    .prologue
    .line 2590
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    return v0
.end method

.method public getNegativePrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2530
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->negativePrefix:Ljava/lang/String;

    return-object v0
.end method

.method public getNegativeSuffix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2568
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->negativeSuffix:Ljava/lang/String;

    return-object v0
.end method

.method public getPadCharacter()C
    .locals 1

    .prologue
    .line 2806
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormat;->pad:C

    return v0
.end method

.method public getPadPosition()I
    .locals 1

    .prologue
    .line 2846
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    return v0
.end method

.method public getPositivePrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2511
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->positivePrefix:Ljava/lang/String;

    return-object v0
.end method

.method public getPositiveSuffix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2549
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->positiveSuffix:Ljava/lang/String;

    return-object v0
.end method

.method public getRoundingIncrement()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 2625
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2626
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingIncrementICU:Lcom/ibm/icu/math/BigDecimal;

    invoke-virtual {v0}, Lcom/ibm/icu/math/BigDecimal;->toBigDecimal()Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0
.end method

.method public getRoundingMode()I
    .locals 1

    .prologue
    .line 2730
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingMode:I

    return v0
.end method

.method public getSecondaryGroupingSize()I
    .locals 1

    .prologue
    .line 3030
    iget-byte v0, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 3176
    invoke-super {p0}, Lcom/ibm/icu/text/NumberFormat;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, Lcom/ibm/icu/text/DecimalFormat;->positivePrefix:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public isDecimalSeparatorAlwaysShown()Z
    .locals 1

    .prologue
    .line 3054
    iget-boolean v0, p0, Lcom/ibm/icu/text/DecimalFormat;->decimalSeparatorAlwaysShown:Z

    return v0
.end method

.method public isExponentSignAlwaysShown()Z
    .locals 1

    .prologue
    .line 2963
    iget-boolean v0, p0, Lcom/ibm/icu/text/DecimalFormat;->exponentSignAlwaysShown:Z

    return v0
.end method

.method public isParseBigDecimal()Z
    .locals 1

    .prologue
    .line 4487
    iget-boolean v0, p0, Lcom/ibm/icu/text/DecimalFormat;->parseBigDecimal:Z

    return v0
.end method

.method public isScientificNotation()Z
    .locals 1

    .prologue
    .line 2890
    iget-boolean v0, p0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    return v0
.end method

.method public parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "parsePosition"    # Ljava/text/ParsePosition;

    .prologue
    .line 1581
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/text/DecimalFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    return-object v0
.end method

.method parseCurrency(Ljava/lang/String;Ljava/text/ParsePosition;)Lcom/ibm/icu/util/CurrencyAmount;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;

    .prologue
    .line 1604
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/text/DecimalFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/util/CurrencyAmount;

    return-object v0
.end method

.method public setCurrency(Lcom/ibm/icu/util/Currency;)V
    .locals 6
    .param p1, "theCurrency"    # Lcom/ibm/icu/util/Currency;

    .prologue
    .line 4413
    invoke-super {p0, p1}, Lcom/ibm/icu/text/NumberFormat;->setCurrency(Lcom/ibm/icu/util/Currency;)V

    .line 4414
    if-eqz p1, :cond_0

    .line 4415
    const/4 v3, 0x1

    new-array v1, v3, [Z

    .line 4416
    .local v1, "isChoiceFormat":[Z
    iget-object v3, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v3}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getULocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4, v1}, Lcom/ibm/icu/util/Currency;->getName(Lcom/ibm/icu/util/ULocale;I[Z)Ljava/lang/String;

    move-result-object v2

    .line 4419
    .local v2, "s":Ljava/lang/String;
    iget-object v3, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {v3, v2}, Lcom/ibm/icu/text/DecimalFormatSymbols;->setCurrencySymbol(Ljava/lang/String;)V

    .line 4420
    iget-object v3, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-virtual {p1}, Lcom/ibm/icu/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/DecimalFormatSymbols;->setInternationalCurrencySymbol(Ljava/lang/String;)V

    .line 4423
    .end local v1    # "isChoiceFormat":[Z
    .end local v2    # "s":Ljava/lang/String;
    :cond_0
    iget-boolean v3, p0, Lcom/ibm/icu/text/DecimalFormat;->isCurrencyFormat:Z

    if-eqz v3, :cond_2

    .line 4424
    if-eqz p1, :cond_1

    .line 4425
    invoke-virtual {p1}, Lcom/ibm/icu/util/Currency;->getRoundingIncrement()D

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/ibm/icu/text/DecimalFormat;->setRoundingIncrement(D)V

    .line 4427
    invoke-virtual {p1}, Lcom/ibm/icu/util/Currency;->getDefaultFractionDigits()I

    move-result v0

    .line 4428
    .local v0, "d":I
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/DecimalFormat;->setMinimumFractionDigits(I)V

    .line 4429
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/DecimalFormat;->setMaximumFractionDigits(I)V

    .line 4431
    .end local v0    # "d":I
    :cond_1
    invoke-direct {p0}, Lcom/ibm/icu/text/DecimalFormat;->expandAffixes()V

    .line 4433
    :cond_2
    return-void
.end method

.method public setDecimalFormatSymbols(Lcom/ibm/icu/text/DecimalFormatSymbols;)V
    .locals 1
    .param p1, "newSymbols"    # Lcom/ibm/icu/text/DecimalFormatSymbols;

    .prologue
    .line 2469
    invoke-virtual {p1}, Lcom/ibm/icu/text/DecimalFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/DecimalFormatSymbols;

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->symbols:Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 2470
    invoke-direct {p0}, Lcom/ibm/icu/text/DecimalFormat;->setCurrencyForSymbols()V

    .line 2471
    invoke-direct {p0}, Lcom/ibm/icu/text/DecimalFormat;->expandAffixes()V

    .line 2472
    return-void
.end method

.method public setDecimalSeparatorAlwaysShown(Z)V
    .locals 0
    .param p1, "newValue"    # Z

    .prologue
    .line 3072
    iput-boolean p1, p0, Lcom/ibm/icu/text/DecimalFormat;->decimalSeparatorAlwaysShown:Z

    .line 3073
    return-void
.end method

.method public setExponentSignAlwaysShown(Z)V
    .locals 0
    .param p1, "expSignAlways"    # Z

    .prologue
    .line 2981
    iput-boolean p1, p0, Lcom/ibm/icu/text/DecimalFormat;->exponentSignAlwaysShown:Z

    .line 2982
    return-void
.end method

.method public setFormatWidth(I)V
    .locals 2
    .param p1, "width"    # I

    .prologue
    .line 2788
    if-gez p1, :cond_0

    .line 2789
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Illegal format width"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2791
    :cond_0
    iput p1, p0, Lcom/ibm/icu/text/DecimalFormat;->formatWidth:I

    .line 2792
    return-void
.end method

.method public setGroupingSize(I)V
    .locals 1
    .param p1, "newValue"    # I

    .prologue
    .line 3007
    int-to-byte v0, p1

    iput-byte v0, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize:B

    .line 3008
    return-void
.end method

.method public setMaximumFractionDigits(I)V
    .locals 1
    .param p1, "newValue"    # I

    .prologue
    .line 4457
    const/16 v0, 0x154

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-super {p0, v0}, Lcom/ibm/icu/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 4458
    return-void
.end method

.method public setMaximumIntegerDigits(I)V
    .locals 1
    .param p1, "newValue"    # I

    .prologue
    .line 4303
    const/16 v0, 0x135

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-super {p0, v0}, Lcom/ibm/icu/text/NumberFormat;->setMaximumIntegerDigits(I)V

    .line 4304
    return-void
.end method

.method public setMaximumSignificantDigits(I)V
    .locals 2
    .param p1, "max"    # I

    .prologue
    .line 4367
    const/4 v1, 0x1

    if-ge p1, v1, :cond_0

    .line 4368
    const/4 p1, 0x1

    .line 4371
    :cond_0
    iget v1, p0, Lcom/ibm/icu/text/DecimalFormat;->minSignificantDigits:I

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4372
    .local v0, "min":I
    iput v0, p0, Lcom/ibm/icu/text/DecimalFormat;->minSignificantDigits:I

    .line 4373
    iput p1, p0, Lcom/ibm/icu/text/DecimalFormat;->maxSignificantDigits:I

    .line 4374
    return-void
.end method

.method public setMinimumExponentDigits(B)V
    .locals 2
    .param p1, "minExpDig"    # B

    .prologue
    .line 2943
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 2944
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Exponent digits must be >= 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2946
    :cond_0
    iput-byte p1, p0, Lcom/ibm/icu/text/DecimalFormat;->minExponentDigits:B

    .line 2947
    return-void
.end method

.method public setMinimumFractionDigits(I)V
    .locals 1
    .param p1, "newValue"    # I

    .prologue
    .line 4467
    const/16 v0, 0x154

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-super {p0, v0}, Lcom/ibm/icu/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 4468
    return-void
.end method

.method public setMinimumIntegerDigits(I)V
    .locals 1
    .param p1, "newValue"    # I

    .prologue
    .line 4313
    const/16 v0, 0x135

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-super {p0, v0}, Lcom/ibm/icu/text/NumberFormat;->setMinimumIntegerDigits(I)V

    .line 4314
    return-void
.end method

.method public setMinimumSignificantDigits(I)V
    .locals 2
    .param p1, "min"    # I

    .prologue
    .line 4348
    const/4 v1, 0x1

    if-ge p1, v1, :cond_0

    .line 4349
    const/4 p1, 0x1

    .line 4352
    :cond_0
    iget v1, p0, Lcom/ibm/icu/text/DecimalFormat;->maxSignificantDigits:I

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 4353
    .local v0, "max":I
    iput p1, p0, Lcom/ibm/icu/text/DecimalFormat;->minSignificantDigits:I

    .line 4354
    iput v0, p0, Lcom/ibm/icu/text/DecimalFormat;->maxSignificantDigits:I

    .line 4355
    return-void
.end method

.method public setMultiplier(I)V
    .locals 3
    .param p1, "newValue"    # I

    .prologue
    .line 2602
    if-nez p1, :cond_0

    .line 2603
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Bad multiplier: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2605
    :cond_0
    iput p1, p0, Lcom/ibm/icu/text/DecimalFormat;->multiplier:I

    .line 2606
    return-void
.end method

.method public setNegativePrefix(Ljava/lang/String;)V
    .locals 1
    .param p1, "newValue"    # Ljava/lang/String;

    .prologue
    .line 2539
    iput-object p1, p0, Lcom/ibm/icu/text/DecimalFormat;->negativePrefix:Ljava/lang/String;

    .line 2540
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->negPrefixPattern:Ljava/lang/String;

    .line 2541
    return-void
.end method

.method public setNegativeSuffix(Ljava/lang/String;)V
    .locals 1
    .param p1, "newValue"    # Ljava/lang/String;

    .prologue
    .line 2577
    iput-object p1, p0, Lcom/ibm/icu/text/DecimalFormat;->negativeSuffix:Ljava/lang/String;

    .line 2578
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->negSuffixPattern:Ljava/lang/String;

    .line 2579
    return-void
.end method

.method public setPadCharacter(C)V
    .locals 0
    .param p1, "padChar"    # C

    .prologue
    .line 2823
    iput-char p1, p0, Lcom/ibm/icu/text/DecimalFormat;->pad:C

    .line 2824
    return-void
.end method

.method public setPadPosition(I)V
    .locals 2
    .param p1, "padPos"    # I

    .prologue
    .line 2872
    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-le p1, v0, :cond_1

    .line 2873
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Illegal pad position"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2875
    :cond_1
    iput p1, p0, Lcom/ibm/icu/text/DecimalFormat;->padPosition:I

    .line 2876
    return-void
.end method

.method public setParseBigDecimal(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 4478
    iput-boolean p1, p0, Lcom/ibm/icu/text/DecimalFormat;->parseBigDecimal:Z

    .line 4479
    return-void
.end method

.method public setPositivePrefix(Ljava/lang/String;)V
    .locals 1
    .param p1, "newValue"    # Ljava/lang/String;

    .prologue
    .line 2520
    iput-object p1, p0, Lcom/ibm/icu/text/DecimalFormat;->positivePrefix:Ljava/lang/String;

    .line 2521
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->posPrefixPattern:Ljava/lang/String;

    .line 2522
    return-void
.end method

.method public setPositiveSuffix(Ljava/lang/String;)V
    .locals 1
    .param p1, "newValue"    # Ljava/lang/String;

    .prologue
    .line 2558
    iput-object p1, p0, Lcom/ibm/icu/text/DecimalFormat;->positiveSuffix:Ljava/lang/String;

    .line 2559
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormat;->posSuffixPattern:Ljava/lang/String;

    .line 2560
    return-void
.end method

.method public setRoundingIncrement(D)V
    .locals 9
    .param p1, "newValue"    # D

    .prologue
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/16 v4, 0x0

    .line 2692
    cmpg-double v2, p1, v4

    if-gez v2, :cond_0

    .line 2693
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Illegal rounding increment"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2695
    :cond_0
    iput-wide p1, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDouble:D

    .line 2696
    iput-wide v4, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDoubleReciprocal:D

    .line 2697
    cmpl-double v2, p1, v4

    if-nez v2, :cond_1

    .line 2698
    const/4 v2, 0x0

    check-cast v2, Lcom/ibm/icu/math/BigDecimal;

    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/DecimalFormat;->setRoundingIncrement(Lcom/ibm/icu/math/BigDecimal;)V

    .line 2707
    :goto_0
    return-void

    .line 2700
    :cond_1
    iput-wide p1, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDouble:D

    .line 2701
    iget-wide v2, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDouble:D

    cmpg-double v2, v2, v6

    if-gez v2, :cond_2

    .line 2702
    iget-wide v2, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingDouble:D

    div-double v0, v6, v2

    .line 2703
    .local v0, "rawRoundedReciprocal":D
    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setRoundingDoubleReciprocal(D)V

    .line 2705
    .end local v0    # "rawRoundedReciprocal":D
    :cond_2
    new-instance v2, Lcom/ibm/icu/math/BigDecimal;

    invoke-direct {v2, p1, p2}, Lcom/ibm/icu/math/BigDecimal;-><init>(D)V

    invoke-direct {p0, v2}, Lcom/ibm/icu/text/DecimalFormat;->setInternalRoundingIncrement(Lcom/ibm/icu/math/BigDecimal;)V

    goto :goto_0
.end method

.method public setRoundingIncrement(Lcom/ibm/icu/math/BigDecimal;)V
    .locals 3
    .param p1, "newValue"    # Lcom/ibm/icu/math/BigDecimal;

    .prologue
    .line 2666
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 2668
    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_1

    .line 2669
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Illegal rounding increment"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2666
    .end local v0    # "i":I
    :cond_0
    sget-object v1, Lcom/ibm/icu/math/BigDecimal;->ZERO:Lcom/ibm/icu/math/BigDecimal;

    invoke-virtual {p1, v1}, Lcom/ibm/icu/math/BigDecimal;->compareTo(Lcom/ibm/icu/math/BigDecimal;)I

    move-result v0

    goto :goto_0

    .line 2671
    .restart local v0    # "i":I
    :cond_1
    if-nez v0, :cond_2

    .line 2672
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/DecimalFormat;->setInternalRoundingIncrement(Lcom/ibm/icu/math/BigDecimal;)V

    .line 2676
    :goto_1
    invoke-direct {p0}, Lcom/ibm/icu/text/DecimalFormat;->setRoundingDouble()V

    .line 2677
    return-void

    .line 2674
    :cond_2
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DecimalFormat;->setInternalRoundingIncrement(Lcom/ibm/icu/math/BigDecimal;)V

    goto :goto_1
.end method

.method public setRoundingIncrement(Ljava/math/BigDecimal;)V
    .locals 1
    .param p1, "newValue"    # Ljava/math/BigDecimal;

    .prologue
    .line 2645
    if-nez p1, :cond_0

    .line 2646
    const/4 v0, 0x0

    check-cast v0, Lcom/ibm/icu/math/BigDecimal;

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/DecimalFormat;->setRoundingIncrement(Lcom/ibm/icu/math/BigDecimal;)V

    .line 2650
    :goto_0
    return-void

    .line 2648
    :cond_0
    new-instance v0, Lcom/ibm/icu/math/BigDecimal;

    invoke-direct {v0, p1}, Lcom/ibm/icu/math/BigDecimal;-><init>(Ljava/math/BigDecimal;)V

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/DecimalFormat;->setRoundingIncrement(Lcom/ibm/icu/math/BigDecimal;)V

    goto :goto_0
.end method

.method public setRoundingMode(I)V
    .locals 3
    .param p1, "roundingMode"    # I

    .prologue
    .line 2748
    if-ltz p1, :cond_0

    const/4 v0, 0x7

    if-le p1, v0, :cond_1

    .line 2750
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Invalid rounding mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2753
    :cond_1
    iput p1, p0, Lcom/ibm/icu/text/DecimalFormat;->roundingMode:I

    .line 2754
    return-void
.end method

.method public setScientificNotation(Z)V
    .locals 0
    .param p1, "useScientific"    # Z

    .prologue
    .line 2910
    iput-boolean p1, p0, Lcom/ibm/icu/text/DecimalFormat;->useExponentialNotation:Z

    .line 2911
    return-void
.end method

.method public setSecondaryGroupingSize(I)V
    .locals 1
    .param p1, "newValue"    # I

    .prologue
    .line 3044
    int-to-byte v0, p1

    iput-byte v0, p0, Lcom/ibm/icu/text/DecimalFormat;->groupingSize2:B

    .line 3045
    return-void
.end method

.method public setSignificantDigitsUsed(Z)V
    .locals 0
    .param p1, "useSignificantDigits"    # Z

    .prologue
    .line 4394
    iput-boolean p1, p0, Lcom/ibm/icu/text/DecimalFormat;->useSignificantDigits:Z

    .line 4395
    return-void
.end method

.method public toLocalizedPattern()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3197
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DecimalFormat;->toPattern(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toPattern()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3187
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DecimalFormat;->toPattern(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
