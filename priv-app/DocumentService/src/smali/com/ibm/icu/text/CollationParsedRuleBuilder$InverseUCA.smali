.class Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;
.super Ljava/lang/Object;
.source "CollationParsedRuleBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CollationParsedRuleBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InverseUCA"
.end annotation


# instance fields
.field m_UCA_version_:Lcom/ibm/icu/util/VersionInfo;

.field m_continuations_:[C

.field m_table_:[I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    return-void
.end method

.method private compareCEs(IIII)I
    .locals 10
    .param p1, "source0"    # I
    .param p2, "source1"    # I
    .param p3, "target0"    # I
    .param p4, "target1"    # I

    .prologue
    const v9, 0xff00

    const/high16 v8, -0x10000

    .line 132
    move v1, p1

    .local v1, "s1":I
    move v4, p3

    .line 133
    .local v4, "t1":I
    invoke-static {p2}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 134
    move v2, p2

    .line 138
    .local v2, "s2":I
    :goto_0
    invoke-static {p4}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 139
    move v5, p4

    .line 144
    .local v5, "t2":I
    :goto_1
    const/4 v0, 0x0

    .local v0, "s":I
    const/4 v3, 0x0

    .line 145
    .local v3, "t":I
    if-ne v1, v4, :cond_2

    if-ne v2, v5, :cond_2

    .line 146
    const/4 v6, 0x0

    .line 161
    :goto_2
    return v6

    .line 136
    .end local v0    # "s":I
    .end local v2    # "s2":I
    .end local v3    # "t":I
    .end local v5    # "t2":I
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "s2":I
    goto :goto_0

    .line 141
    :cond_1
    const/4 v5, 0x0

    .restart local v5    # "t2":I
    goto :goto_1

    .line 148
    .restart local v0    # "s":I
    .restart local v3    # "t":I
    :cond_2
    and-int v6, v1, v8

    and-int v7, v2, v8

    shr-int/lit8 v7, v7, 0x10

    or-int v0, v6, v7

    .line 149
    and-int v6, v4, v8

    and-int v7, v5, v8

    shr-int/lit8 v7, v7, 0x10

    or-int v3, v6, v7

    .line 150
    if-ne v0, v3, :cond_4

    .line 151
    and-int v6, v1, v9

    and-int v7, v2, v9

    shr-int/lit8 v7, v7, 0x8

    or-int v0, v6, v7

    .line 152
    and-int v6, v4, v9

    and-int v7, v5, v9

    shr-int/lit8 v7, v7, 0x8

    or-int v3, v6, v7

    .line 153
    if-ne v0, v3, :cond_3

    .line 154
    and-int/lit16 v6, v1, 0xff

    shl-int/lit8 v6, v6, 0x8

    and-int/lit16 v7, v2, 0xff

    or-int v0, v6, v7

    .line 155
    and-int/lit16 v6, v4, 0xff

    shl-int/lit8 v6, v6, 0x8

    and-int/lit16 v7, v5, 0xff

    or-int v3, v6, v7

    .line 156
    invoke-static {v0, v3}, Lcom/ibm/icu/impl/Utility;->compareUnsigned(II)I

    move-result v6

    goto :goto_2

    .line 158
    :cond_3
    invoke-static {v0, v3}, Lcom/ibm/icu/impl/Utility;->compareUnsigned(II)I

    move-result v6

    goto :goto_2

    .line 161
    :cond_4
    invoke-static {v0, v3}, Lcom/ibm/icu/impl/Utility;->compareUnsigned(II)I

    move-result v6

    goto :goto_2
.end method

.method private final getInverseNext(Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;I)I
    .locals 7
    .param p1, "listheader"    # Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;
    .param p2, "strength"    # I

    .prologue
    .line 366
    iget v0, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseCE_:I

    .line 367
    .local v0, "ce":I
    iget v4, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    .line 368
    .local v4, "secondce":I
    invoke-virtual {p0, v0, v4}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->findInverseCE(II)I

    move-result v3

    .line 370
    .local v3, "result":I
    if-gez v3, :cond_0

    .line 371
    const/4 v5, -0x1

    .line 389
    :goto_0
    return v5

    .line 374
    :cond_0
    invoke-static {}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$000()[I

    move-result-object v5

    aget v5, v5, p2

    and-int/2addr v0, v5

    .line 375
    invoke-static {}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$000()[I

    move-result-object v5

    aget v5, v5, p2

    and-int/2addr v4, v5

    .line 377
    move v1, v0

    .line 378
    .local v1, "nextce":I
    move v2, v4

    .line 381
    .local v2, "nextcontce":I
    :goto_1
    invoke-static {}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$000()[I

    move-result-object v5

    aget v5, v5, p2

    and-int/2addr v5, v1

    if-ne v5, v0, :cond_1

    invoke-static {}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$000()[I

    move-result-object v5

    aget v5, v5, p2

    and-int/2addr v5, v2

    if-ne v5, v4, :cond_1

    .line 382
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_table_:[I

    add-int/lit8 v3, v3, 0x1

    mul-int/lit8 v6, v3, 0x3

    aget v1, v5, v6

    .line 383
    iget-object v5, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_table_:[I

    mul-int/lit8 v6, v3, 0x3

    add-int/lit8 v6, v6, 0x1

    aget v2, v5, v6

    .line 384
    goto :goto_1

    .line 386
    :cond_1
    iput v1, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextCE_:I

    .line 387
    iput v2, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextContCE_:I

    move v5, v3

    .line 389
    goto :goto_0
.end method


# virtual methods
.method findInverseCE(II)I
    .locals 8
    .param p1, "ce"    # I
    .param p2, "contce"    # I

    .prologue
    .line 173
    const/4 v0, 0x0

    .line 174
    .local v0, "bottom":I
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_table_:[I

    array-length v6, v6

    div-int/lit8 v5, v6, 0x3

    .line 175
    .local v5, "top":I
    const/4 v3, 0x0

    .line 177
    .local v3, "result":I
    :goto_0
    add-int/lit8 v6, v5, -0x1

    if-ge v0, v6, :cond_1

    .line 178
    add-int v6, v5, v0

    shr-int/lit8 v3, v6, 0x1

    .line 179
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_table_:[I

    mul-int/lit8 v7, v3, 0x3

    aget v2, v6, v7

    .line 180
    .local v2, "first":I
    iget-object v6, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_table_:[I

    mul-int/lit8 v7, v3, 0x3

    add-int/lit8 v7, v7, 0x1

    aget v4, v6, v7

    .line 181
    .local v4, "second":I
    invoke-direct {p0, v2, v4, p1, p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->compareCEs(IIII)I

    move-result v1

    .line 182
    .local v1, "comparison":I
    if-lez v1, :cond_0

    .line 183
    move v5, v3

    .line 184
    goto :goto_0

    .line 185
    :cond_0
    if-gez v1, :cond_1

    .line 186
    move v0, v3

    goto :goto_0

    .line 193
    .end local v1    # "comparison":I
    .end local v2    # "first":I
    .end local v4    # "second":I
    :cond_1
    return v3
.end method

.method final getCEStrengthDifference(IIII)I
    .locals 3
    .param p1, "CE"    # I
    .param p2, "contCE"    # I
    .param p3, "prevCE"    # I
    .param p4, "prevContCE"    # I

    .prologue
    .line 121
    const/4 v0, 0x2

    .line 125
    .local v0, "strength":I
    :goto_0
    invoke-static {}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$000()[I

    move-result-object v1

    aget v1, v1, v0

    and-int/2addr v1, p3

    invoke-static {}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$000()[I

    move-result-object v2

    aget v2, v2, v0

    and-int/2addr v2, p1

    if-ne v1, v2, :cond_0

    invoke-static {}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$000()[I

    move-result-object v1

    aget v1, v1, v0

    and-int/2addr v1, p4

    invoke-static {}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$000()[I

    move-result-object v2

    aget v2, v2, v0

    and-int/2addr v2, p2

    if-eq v1, v2, :cond_1

    :cond_0
    if-eqz v0, :cond_1

    .line 126
    add-int/lit8 v0, v0, -0x1

    .line 127
    goto :goto_0

    .line 128
    :cond_1
    return v0
.end method

.method getInverseGapPositions(Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;)V
    .locals 12
    .param p1, "listheader"    # Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 207
    iget-object v6, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_first_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 208
    .local v6, "token":Lcom/ibm/icu/text/CollationRuleParser$Token;
    iget v7, v6, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    .line 210
    .local v7, "tokenstrength":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v8, 0x3

    if-ge v0, v8, :cond_0

    .line 211
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    mul-int/lit8 v9, v0, 0x3

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 212
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    mul-int/lit8 v9, v0, 0x3

    add-int/lit8 v9, v9, 0x1

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 213
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    mul-int/lit8 v9, v0, 0x3

    add-int/lit8 v9, v9, 0x2

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 214
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    mul-int/lit8 v9, v0, 0x3

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 215
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    mul-int/lit8 v9, v0, 0x3

    add-int/lit8 v9, v9, 0x1

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 216
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    mul-int/lit8 v9, v0, 0x3

    add-int/lit8 v9, v9, 0x2

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 217
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_numStr_:[I

    const/4 v9, 0x0

    aput v9, v8, v0

    .line 218
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_fStrToken_:[Lcom/ibm/icu/text/CollationRuleParser$Token;

    const/4 v9, 0x0

    aput-object v9, v8, v0

    .line 219
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_lStrToken_:[Lcom/ibm/icu/text/CollationRuleParser$Token;

    const/4 v9, 0x0

    aput-object v9, v8, v0

    .line 220
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_pos_:[I

    const/4 v9, -0x1

    aput v9, v8, v0

    .line 210
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 223
    :cond_0
    iget v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseCE_:I

    ushr-int/lit8 v8, v8, 0x18

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget v9, v9, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->PRIMARY_IMPLICIT_MIN_:I

    if-lt v8, v9, :cond_2

    iget v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseCE_:I

    ushr-int/lit8 v8, v8, 0x18

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    iget v9, v9, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->PRIMARY_IMPLICIT_MAX_:I

    if-gt v8, v9, :cond_2

    .line 229
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_pos_:[I

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 230
    iget v4, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseCE_:I

    .line 231
    .local v4, "t1":I
    iget v5, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    .line 232
    .local v5, "t2":I
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 234
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 236
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    const/4 v9, 0x2

    const/4 v10, 0x2

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 238
    const/high16 v8, -0x10000

    and-int/2addr v8, v4

    const/high16 v9, -0x10000

    and-int/2addr v9, v5

    ushr-int/lit8 v9, v9, 0x10

    or-int v2, v8, v9

    .line 239
    .local v2, "primaryCE":I
    sget-object v8, Lcom/ibm/icu/text/RuleBasedCollator;->impCEGen_:Lcom/ibm/icu/impl/ImplicitCEGenerator;

    sget-object v9, Lcom/ibm/icu/text/RuleBasedCollator;->impCEGen_:Lcom/ibm/icu/impl/ImplicitCEGenerator;

    invoke-virtual {v9, v2}, Lcom/ibm/icu/impl/ImplicitCEGenerator;->getRawFromImplicit(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v8, v9}, Lcom/ibm/icu/impl/ImplicitCEGenerator;->getImplicitFromRaw(I)I

    move-result v2

    .line 241
    const/high16 v8, -0x10000

    and-int/2addr v8, v2

    or-int/lit16 v4, v8, 0x505

    .line 242
    shl-int/lit8 v8, v2, 0x10

    const/high16 v9, -0x10000

    and-int/2addr v8, v9

    or-int/lit16 v5, v8, 0xc0

    .line 257
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 259
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 261
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    const/4 v9, 0x2

    const/4 v10, 0x2

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 354
    .end local v2    # "primaryCE":I
    .end local v4    # "t1":I
    .end local v5    # "t2":I
    :cond_1
    :goto_1
    return-void

    .line 264
    :cond_2
    iget-boolean v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_indirect_:Z

    const/4 v9, 0x1

    if-ne v8, v9, :cond_5

    iget v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextCE_:I

    if-eqz v8, :cond_5

    .line 266
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_pos_:[I

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v8, v9

    .line 267
    iget v4, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseCE_:I

    .line 268
    .restart local v4    # "t1":I
    iget v5, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    .line 269
    .restart local v5    # "t2":I
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 271
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 273
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    const/4 v9, 0x2

    const/4 v10, 0x2

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 275
    iget v4, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextCE_:I

    .line 276
    iget v5, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_nextContCE_:I

    .line 277
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 279
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 281
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    const/4 v9, 0x2

    const/4 v10, 0x2

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    goto :goto_1

    .line 308
    .end local v4    # "t1":I
    .end local v5    # "t2":I
    :cond_3
    const/4 v8, 0x2

    if-ge v7, v8, :cond_4

    .line 311
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_pos_:[I

    aget v8, v8, v7

    iget-object v9, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_pos_:[I

    add-int/lit8 v10, v7, 0x1

    aget v9, v9, v10

    if-ne v8, v9, :cond_4

    .line 313
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_fStrToken_:[Lcom/ibm/icu/text/CollationRuleParser$Token;

    iget-object v9, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_fStrToken_:[Lcom/ibm/icu/text/CollationRuleParser$Token;

    add-int/lit8 v10, v7, 0x1

    aget-object v9, v9, v10

    aput-object v9, v8, v7

    .line 316
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_fStrToken_:[Lcom/ibm/icu/text/CollationRuleParser$Token;

    add-int/lit8 v9, v7, 0x1

    const/4 v10, 0x0

    aput-object v10, v8, v9

    .line 317
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_lStrToken_:[Lcom/ibm/icu/text/CollationRuleParser$Token;

    add-int/lit8 v9, v7, 0x1

    const/4 v10, 0x0

    aput-object v10, v8, v9

    .line 318
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_pos_:[I

    add-int/lit8 v9, v7, 0x1

    const/4 v10, -0x1

    aput v10, v8, v9

    .line 321
    :cond_4
    if-eqz v6, :cond_9

    .line 322
    iget v7, v6, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    .line 286
    :cond_5
    const/4 v8, 0x3

    if-ge v7, v8, :cond_6

    .line 287
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_pos_:[I

    invoke-direct {p0, p1, v7}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->getInverseNext(Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;I)I

    move-result v9

    aput v9, v8, v7

    .line 290
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_pos_:[I

    aget v8, v8, v7

    if-ltz v8, :cond_8

    .line 291
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_fStrToken_:[Lcom/ibm/icu/text/CollationRuleParser$Token;

    aput-object v6, v8, v7

    .line 301
    :cond_6
    :goto_2
    if-eqz v6, :cond_3

    iget v8, v6, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_strength_:I

    if-lt v8, v7, :cond_3

    .line 303
    const/4 v8, 0x3

    if-ge v7, v8, :cond_7

    .line 304
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_lStrToken_:[Lcom/ibm/icu/text/CollationRuleParser$Token;

    aput-object v6, v8, v7

    .line 306
    :cond_7
    iget-object v6, v6, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 307
    goto :goto_2

    .line 297
    :cond_8
    new-instance v8, Ljava/lang/Exception;

    const-string/jumbo v9, "Internal program error"

    invoke-direct {v8, v9}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v8

    .line 328
    :cond_9
    const/4 v3, 0x0

    .local v3, "st":I
    :goto_3
    const/4 v8, 0x3

    if-ge v3, v8, :cond_1

    .line 329
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_pos_:[I

    aget v1, v8, v3

    .line 330
    .local v1, "pos":I
    if-ltz v1, :cond_a

    .line 331
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_table_:[I

    mul-int/lit8 v9, v1, 0x3

    aget v4, v8, v9

    .line 332
    .restart local v4    # "t1":I
    iget-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_table_:[I

    mul-int/lit8 v9, v1, 0x3

    add-int/lit8 v9, v9, 0x1

    aget v5, v8, v9

    .line 333
    .restart local v5    # "t2":I
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    mul-int/lit8 v9, v3, 0x3

    const/4 v10, 0x0

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 335
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    mul-int/lit8 v9, v3, 0x3

    add-int/lit8 v9, v9, 0x1

    const/4 v10, 0x1

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 337
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsHi_:[I

    mul-int/lit8 v9, v3, 0x3

    add-int/lit8 v9, v9, 0x2

    and-int/lit8 v10, v4, 0x3f

    shl-int/lit8 v10, v10, 0x18

    and-int/lit8 v11, v5, 0x3f

    shl-int/lit8 v11, v11, 0x10

    or-int/2addr v10, v11

    aput v10, v8, v9

    .line 342
    iget v4, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseCE_:I

    .line 343
    iget v5, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_baseContCE_:I

    .line 345
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    mul-int/lit8 v9, v3, 0x3

    const/4 v10, 0x0

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 347
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    mul-int/lit8 v9, v3, 0x3

    add-int/lit8 v9, v9, 0x1

    const/4 v10, 0x1

    invoke-static {v4, v5, v10}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$100(III)I

    move-result v10

    aput v10, v8, v9

    .line 349
    iget-object v8, p1, Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;->m_gapsLo_:[I

    mul-int/lit8 v9, v3, 0x3

    add-int/lit8 v9, v9, 0x2

    and-int/lit8 v10, v4, 0x3f

    shl-int/lit8 v10, v10, 0x18

    and-int/lit8 v11, v5, 0x3f

    shl-int/lit8 v11, v11, 0x10

    or-int/2addr v10, v11

    aput v10, v8, v9

    .line 328
    .end local v4    # "t1":I
    .end local v5    # "t2":I
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method final getInversePrevCE(III[I)I
    .locals 5
    .param p1, "ce"    # I
    .param p2, "contce"    # I
    .param p3, "strength"    # I
    .param p4, "prevresult"    # [I

    .prologue
    const/4 v1, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 94
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->findInverseCE(II)I

    move-result v0

    .line 96
    .local v0, "result":I
    if-gez v0, :cond_0

    .line 97
    aput v1, p4, v3

    .line 116
    :goto_0
    return v1

    .line 101
    :cond_0
    invoke-static {}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$000()[I

    move-result-object v1

    aget v1, v1, p3

    and-int/2addr p1, v1

    .line 102
    invoke-static {}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$000()[I

    move-result-object v1

    aget v1, v1, p3

    and-int/2addr p2, v1

    .line 104
    aput p1, p4, v3

    .line 105
    aput p2, p4, v4

    .line 109
    :goto_1
    aget v1, p4, v3

    invoke-static {}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$000()[I

    move-result-object v2

    aget v2, v2, p3

    and-int/2addr v1, v2

    if-ne v1, p1, :cond_1

    aget v1, p4, v4

    invoke-static {}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$000()[I

    move-result-object v2

    aget v2, v2, p3

    and-int/2addr v1, v2

    if-ne v1, p2, :cond_1

    if-lez v0, :cond_1

    .line 113
    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_table_:[I

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v2, v0, 0x3

    aget v1, v1, v2

    aput v1, p4, v3

    .line 114
    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$InverseUCA;->m_table_:[I

    mul-int/lit8 v2, v0, 0x3

    add-int/lit8 v2, v2, 0x1

    aget v1, v1, v2

    aput v1, p4, v4

    goto :goto_1

    :cond_1
    move v1, v0

    .line 116
    goto :goto_0
.end method
