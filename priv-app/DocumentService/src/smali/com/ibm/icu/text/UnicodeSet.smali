.class public Lcom/ibm/icu/text/UnicodeSet;
.super Lcom/ibm/icu/text/UnicodeFilter;
.source "UnicodeSet.java"

# interfaces
.implements Lcom/ibm/icu/util/Freezable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/UnicodeSet$XSymbolTable;,
        Lcom/ibm/icu/text/UnicodeSet$VersionFilter;,
        Lcom/ibm/icu/text/UnicodeSet$IntPropertyFilter;,
        Lcom/ibm/icu/text/UnicodeSet$GeneralCategoryMaskFilter;,
        Lcom/ibm/icu/text/UnicodeSet$NumericValueFilter;,
        Lcom/ibm/icu/text/UnicodeSet$Filter;
    }
.end annotation


# static fields
.field public static final ADD_CASE_MAPPINGS:I = 0x4

.field private static final ANY_ID:Ljava/lang/String; = "ANY"

.field private static final ASCII_ID:Ljava/lang/String; = "ASCII"

.field private static final ASSIGNED:Ljava/lang/String; = "Assigned"

.field public static final CASE:I = 0x2

.field public static final CASE_INSENSITIVE:I = 0x2

.field private static final GROW_EXTRA:I = 0x10

.field private static final HIGH:I = 0x110000

.field public static final IGNORE_SPACE:I = 0x1

.field private static INCLUSIONS:[Lcom/ibm/icu/text/UnicodeSet; = null

.field private static final LOW:I = 0x0

.field public static final MAX_VALUE:I = 0x10ffff

.field public static final MIN_VALUE:I = 0x0

.field static final NO_VERSION:Lcom/ibm/icu/util/VersionInfo;

.field private static final START_EXTRA:I = 0x10


# instance fields
.field private buffer:[I

.field private frozen:Z

.field private len:I

.field private list:[I

.field private pat:Ljava/lang/String;

.field private rangeList:[I

.field strings:Ljava/util/TreeSet;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 320
    const/4 v0, 0x0

    sput-object v0, Lcom/ibm/icu/text/UnicodeSet;->INCLUSIONS:[Lcom/ibm/icu/text/UnicodeSet;

    .line 2963
    invoke-static {v1, v1, v1, v1}, Lcom/ibm/icu/util/VersionInfo;->getInstance(IIII)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v0

    sput-object v0, Lcom/ibm/icu/text/UnicodeSet;->NO_VERSION:Lcom/ibm/icu/util/VersionInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 330
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeFilter;-><init>()V

    .line 293
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    .line 304
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    .line 331
    const/16 v0, 0x11

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    .line 332
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    iget v1, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    const/high16 v2, 0x110000

    aput v2, v0, v1

    .line 333
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 352
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 353
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/UnicodeSet;->complement(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 354
    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 1
    .param p1, "other"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 339
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeFilter;-><init>()V

    .line 293
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    .line 304
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    .line 340
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/UnicodeSet;->set(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    .line 341
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 365
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 366
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v1, v1, v0}, Lcom/ibm/icu/text/UnicodeSet;->applyPattern(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;I)Lcom/ibm/icu/text/UnicodeSet;

    .line 367
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "options"    # I

    .prologue
    const/4 v0, 0x0

    .line 395
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 396
    invoke-virtual {p0, p1, v0, v0, p2}, Lcom/ibm/icu/text/UnicodeSet;->applyPattern(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;I)Lcom/ibm/icu/text/UnicodeSet;

    .line 397
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;)V
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;
    .param p3, "symbols"    # Lcom/ibm/icu/text/SymbolTable;

    .prologue
    .line 412
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 413
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/ibm/icu/text/UnicodeSet;->applyPattern(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;I)Lcom/ibm/icu/text/UnicodeSet;

    .line 414
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;I)V
    .locals 0
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;
    .param p3, "symbols"    # Lcom/ibm/icu/text/SymbolTable;
    .param p4, "options"    # I

    .prologue
    .line 431
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 432
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/ibm/icu/text/UnicodeSet;->applyPattern(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;I)Lcom/ibm/icu/text/UnicodeSet;

    .line 433
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "ignoreWhitespace"    # Z

    .prologue
    const/4 v1, 0x0

    .line 380
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 381
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v1, v1, v0}, Lcom/ibm/icu/text/UnicodeSet;->applyPattern(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;I)Lcom/ibm/icu/text/UnicodeSet;

    .line 382
    return-void

    .line 381
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static _appendToPat(Ljava/lang/StringBuffer;IZ)V
    .locals 2
    .param p0, "buf"    # Ljava/lang/StringBuffer;
    .param p1, "c"    # I
    .param p2, "escapeUnprintable"    # Z

    .prologue
    const/16 v1, 0x5c

    .line 549
    if-eqz p2, :cond_0

    invoke-static {p1}, Lcom/ibm/icu/impl/Utility;->isUnprintable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 552
    invoke-static {p0, p1}, Lcom/ibm/icu/impl/Utility;->escapeUnprintable(Ljava/lang/StringBuffer;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578
    :goto_0
    return-void

    .line 557
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 572
    invoke-static {p1}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 573
    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 577
    :cond_1
    :goto_1
    invoke-static {p0, p1}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 568
    :sswitch_0
    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 557
    nop

    :sswitch_data_0
    .sparse-switch
        0x24 -> :sswitch_0
        0x26 -> :sswitch_0
        0x2d -> :sswitch_0
        0x3a -> :sswitch_0
        0x5b -> :sswitch_0
        0x5c -> :sswitch_0
        0x5d -> :sswitch_0
        0x5e -> :sswitch_0
        0x7b -> :sswitch_0
        0x7d -> :sswitch_0
    .end sparse-switch
.end method

.method private static _appendToPat(Ljava/lang/StringBuffer;Ljava/lang/String;Z)V
    .locals 2
    .param p0, "buf"    # Ljava/lang/StringBuffer;
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "escapeUnprintable"    # Z

    .prologue
    .line 539
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 540
    invoke-static {p1, v0}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {p0, v1, p2}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;IZ)V

    .line 539
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    .line 542
    :cond_0
    return-void
.end method

.method private _toPattern(Ljava/lang/StringBuffer;Z)Ljava/lang/StringBuffer;
    .locals 5
    .param p1, "result"    # Ljava/lang/StringBuffer;
    .param p2, "escapeUnprintable"    # Z

    .prologue
    const/4 v4, 0x1

    .line 598
    iget-object v3, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 600
    const/4 v0, 0x0

    .line 601
    .local v0, "backslashCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 602
    iget-object v3, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v1

    .line 603
    .local v1, "c":I
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 604
    if-eqz p2, :cond_1

    invoke-static {v1}, Lcom/ibm/icu/impl/Utility;->isUnprintable(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 609
    rem-int/lit8 v3, v0, 0x2

    if-ne v3, v4, :cond_0

    .line 610
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 612
    :cond_0
    invoke-static {p1, v1}, Lcom/ibm/icu/impl/Utility;->escapeUnprintable(Ljava/lang/StringBuffer;I)Z

    .line 613
    const/4 v0, 0x0

    .line 614
    goto :goto_0

    .line 615
    :cond_1
    invoke-static {p1, v1}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 616
    const/16 v3, 0x5c

    if-ne v1, v3, :cond_2

    .line 617
    add-int/lit8 v0, v0, 0x1

    .line 618
    goto :goto_0

    .line 619
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 626
    .end local v0    # "backslashCount":I
    .end local v1    # "c":I
    .end local v2    # "i":I
    :cond_3
    invoke-virtual {p0, p1, p2, v4}, Lcom/ibm/icu/text/UnicodeSet;->_generatePattern(Ljava/lang/StringBuffer;ZZ)Ljava/lang/StringBuffer;

    move-result-object p1

    .end local p1    # "result":Ljava/lang/StringBuffer;
    :cond_4
    return-object p1
.end method

.method private add([III)Lcom/ibm/icu/text/UnicodeSet;
    .locals 12
    .param p1, "other"    # [I
    .param p2, "otherLen"    # I
    .param p3, "polarity"    # I

    .prologue
    const/high16 v11, 0x110000

    .line 2759
    iget v9, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/2addr v9, p2

    invoke-direct {p0, v9}, Lcom/ibm/icu/text/UnicodeSet;->ensureBufferCapacity(I)V

    .line 2760
    const/4 v2, 0x0

    .local v2, "i":I
    const/4 v4, 0x0

    .local v4, "j":I
    const/4 v6, 0x0

    .line 2761
    .local v6, "k":I
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    aget v0, v9, v2

    .line 2762
    .local v0, "a":I
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "j":I
    .local v5, "j":I
    aget v1, p1, v4

    .local v1, "b":I
    move v7, v6

    .line 2767
    .end local v6    # "k":I
    .local v7, "k":I
    :goto_0
    packed-switch p3, :pswitch_data_0

    move v6, v7

    .end local v7    # "k":I
    .restart local v6    # "k":I
    move v4, v5

    .end local v5    # "j":I
    .restart local v4    # "j":I
    move v2, v3

    .end local v3    # "i":I
    .restart local v2    # "i":I
    :goto_1
    move v7, v6

    .end local v6    # "k":I
    .restart local v7    # "k":I
    move v5, v4

    .end local v4    # "j":I
    .restart local v5    # "j":I
    move v3, v2

    .line 2840
    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 2769
    :pswitch_0
    if-ge v0, v1, :cond_1

    .line 2771
    if-lez v7, :cond_0

    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v10, v7, -0x1

    aget v9, v9, v10

    if-gt v0, v9, :cond_0

    .line 2773
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aget v9, v9, v3

    iget-object v10, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, -0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aget v10, v10, v6

    invoke-static {v9, v10}, Lcom/ibm/icu/text/UnicodeSet;->max(II)I

    move-result v0

    .line 2779
    :goto_2
    add-int/lit8 v2, v3, 0x1

    .line 2780
    .end local v3    # "i":I
    .restart local v2    # "i":I
    xor-int/lit8 p3, p3, 0x1

    move v4, v5

    .line 2781
    .end local v5    # "j":I
    .restart local v4    # "j":I
    goto :goto_1

    .line 2776
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_0
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v0, v9, v7

    .line 2777
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aget v0, v9, v3

    goto :goto_2

    .line 2781
    .end local v6    # "k":I
    .restart local v7    # "k":I
    :cond_1
    if-ge v1, v0, :cond_3

    .line 2782
    if-lez v7, :cond_2

    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v10, v7, -0x1

    aget v9, v9, v10

    if-gt v1, v9, :cond_2

    .line 2783
    aget v9, p1, v5

    iget-object v10, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, -0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aget v10, v10, v6

    invoke-static {v9, v10}, Lcom/ibm/icu/text/UnicodeSet;->max(II)I

    move-result v1

    .line 2788
    :goto_3
    add-int/lit8 v4, v5, 0x1

    .line 2789
    .end local v5    # "j":I
    .restart local v4    # "j":I
    xor-int/lit8 p3, p3, 0x2

    move v2, v3

    .line 2790
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_1

    .line 2785
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_2
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v1, v9, v7

    .line 2786
    aget v1, p1, v5

    goto :goto_3

    .line 2791
    .end local v6    # "k":I
    .restart local v7    # "k":I
    :cond_3
    if-ne v0, v11, :cond_5

    .line 2841
    :cond_4
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v11, v9, v7

    .line 2842
    iput v6, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    .line 2844
    iget-object v8, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    .line 2845
    .local v8, "temp":[I
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    iput-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    .line 2846
    iput-object v8, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    .line 2847
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    .line 2848
    return-object p0

    .line 2794
    .end local v6    # "k":I
    .end local v8    # "temp":[I
    .restart local v7    # "k":I
    :cond_5
    if-lez v7, :cond_6

    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v10, v7, -0x1

    aget v9, v9, v10

    if-gt v0, v9, :cond_6

    .line 2795
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aget v9, v9, v3

    iget-object v10, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, -0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aget v10, v10, v6

    invoke-static {v9, v10}, Lcom/ibm/icu/text/UnicodeSet;->max(II)I

    move-result v0

    .line 2801
    :goto_4
    add-int/lit8 v2, v3, 0x1

    .line 2802
    .end local v3    # "i":I
    .restart local v2    # "i":I
    xor-int/lit8 p3, p3, 0x1

    .line 2803
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    xor-int/lit8 p3, p3, 0x2

    .line 2805
    goto/16 :goto_1

    .line 2798
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_6
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v0, v9, v7

    .line 2799
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aget v0, v9, v3

    goto :goto_4

    .line 2807
    .end local v6    # "k":I
    .restart local v7    # "k":I
    :pswitch_1
    if-gt v1, v0, :cond_7

    .line 2808
    if-eq v0, v11, :cond_4

    .line 2809
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v0, v9, v7

    .line 2814
    :goto_5
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    xor-int/lit8 p3, p3, 0x1

    .line 2815
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    xor-int/lit8 p3, p3, 0x2

    .line 2816
    goto/16 :goto_1

    .line 2811
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_7
    if-eq v1, v11, :cond_4

    .line 2812
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v1, v9, v7

    goto :goto_5

    .line 2818
    .end local v6    # "k":I
    .restart local v7    # "k":I
    :pswitch_2
    if-ge v0, v1, :cond_8

    .line 2819
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v0, v9, v7

    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    xor-int/lit8 p3, p3, 0x1

    move v4, v5

    .line 2820
    .end local v5    # "j":I
    .restart local v4    # "j":I
    goto/16 :goto_1

    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_8
    if-ge v1, v0, :cond_9

    .line 2821
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    xor-int/lit8 p3, p3, 0x2

    move v6, v7

    .end local v7    # "k":I
    .restart local v6    # "k":I
    move v2, v3

    .line 2822
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto/16 :goto_1

    .line 2823
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_9
    if-eq v0, v11, :cond_4

    .line 2824
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    xor-int/lit8 p3, p3, 0x1

    .line 2825
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    xor-int/lit8 p3, p3, 0x2

    move v6, v7

    .line 2827
    .end local v7    # "k":I
    .restart local v6    # "k":I
    goto/16 :goto_1

    .line 2829
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :pswitch_3
    if-ge v1, v0, :cond_a

    .line 2830
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v1, v9, v7

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    xor-int/lit8 p3, p3, 0x2

    move v2, v3

    .line 2831
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto/16 :goto_1

    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_a
    if-ge v0, v1, :cond_b

    .line 2832
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    xor-int/lit8 p3, p3, 0x1

    move v6, v7

    .end local v7    # "k":I
    .restart local v6    # "k":I
    move v4, v5

    .line 2833
    .end local v5    # "j":I
    .restart local v4    # "j":I
    goto/16 :goto_1

    .line 2834
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_b
    if-eq v0, v11, :cond_4

    .line 2835
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    xor-int/lit8 p3, p3, 0x1

    .line 2836
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    xor-int/lit8 p3, p3, 0x2

    move v6, v7

    .end local v7    # "k":I
    .restart local v6    # "k":I
    goto/16 :goto_1

    .line 2767
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private static final addCaseMapping(Lcom/ibm/icu/text/UnicodeSet;ILjava/lang/StringBuffer;)V
    .locals 1
    .param p0, "set"    # Lcom/ibm/icu/text/UnicodeSet;
    .param p1, "result"    # I
    .param p2, "full"    # Ljava/lang/StringBuffer;

    .prologue
    .line 3549
    if-ltz p1, :cond_0

    .line 3550
    const/16 v0, 0x1f

    if-le p1, v0, :cond_1

    .line 3552
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 3561
    :cond_0
    :goto_0
    return-void

    .line 3555
    :cond_1
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    .line 3556
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_0
.end method

.method private final add_unchecked(I)Lcom/ibm/icu/text/UnicodeSet;
    .locals 7
    .param p1, "c"    # I

    .prologue
    const v3, 0x10ffff

    const/4 v4, 0x0

    .line 1107
    if-ltz p1, :cond_0

    if-le p1, v3, :cond_1

    .line 1108
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "Invalid code point U+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const/4 v4, 0x6

    invoke-static {p1, v4}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1114
    :cond_1
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/UnicodeSet;->findCodePoint(I)I

    move-result v0

    .line 1117
    .local v0, "i":I
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_2

    .line 1187
    :goto_0
    return-object p0

    .line 1131
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aget v2, v2, v0

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_5

    .line 1133
    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aput p1, v2, v0

    .line 1135
    if-ne p1, v3, :cond_3

    .line 1136
    iget v2, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v2, v2, 0x1

    invoke-direct {p0, v2}, Lcom/ibm/icu/text/UnicodeSet;->ensureCapacity(I)V

    .line 1137
    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    iget v3, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    const/high16 v4, 0x110000

    aput v4, v2, v3

    .line 1139
    :cond_3
    if-lez v0, :cond_4

    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v3, v0, -0x1

    aget v2, v2, v3

    if-ne p1, v2, :cond_4

    .line 1145
    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v3, v0, 0x1

    iget-object v4, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v5, v0, -0x1

    iget v6, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    sub-int/2addr v6, v0

    add-int/lit8 v6, v6, -0x1

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1146
    iget v2, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    .line 1186
    :cond_4
    :goto_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    goto :goto_0

    .line 1150
    :cond_5
    if-lez v0, :cond_6

    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v3, v0, -0x1

    aget v2, v2, v3

    if-ne p1, v2, :cond_6

    .line 1152
    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v3, v0, -0x1

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    goto :goto_1

    .line 1172
    :cond_6
    iget v2, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    array-length v3, v3

    if-le v2, v3, :cond_8

    .line 1173
    iget v2, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0x10

    new-array v1, v2, [I

    .line 1174
    .local v1, "temp":[I
    if-eqz v0, :cond_7

    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1175
    :cond_7
    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v3, v0, 0x2

    iget v4, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    sub-int/2addr v4, v0

    invoke-static {v2, v0, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1176
    iput-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    .line 1181
    .end local v1    # "temp":[I
    :goto_2
    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aput p1, v2, v0

    .line 1182
    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v3, v0, 0x1

    add-int/lit8 v4, p1, 0x1

    aput v4, v2, v3

    .line 1183
    iget v2, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    goto :goto_1

    .line 1178
    :cond_8
    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    iget-object v3, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v4, v0, 0x2

    iget v5, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    sub-int/2addr v5, v0

    invoke-static {v2, v0, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2
.end method

.method private add_unchecked(II)Lcom/ibm/icu/text/UnicodeSet;
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const v0, 0x10ffff

    const/4 v3, 0x6

    .line 1061
    if-ltz p1, :cond_0

    if-le p1, v0, :cond_1

    .line 1062
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Invalid code point U+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p1, v3}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1064
    :cond_1
    if-ltz p2, :cond_2

    if-le p2, v0, :cond_3

    .line 1065
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Invalid code point U+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p2, v3}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1067
    :cond_3
    if-ge p1, p2, :cond_5

    .line 1068
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/UnicodeSet;->range(II)[I

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add([III)Lcom/ibm/icu/text/UnicodeSet;

    .line 1072
    :cond_4
    :goto_0
    return-object p0

    .line 1069
    :cond_5
    if-ne p1, p2, :cond_4

    .line 1070
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0
.end method

.method private applyFilter(Lcom/ibm/icu/text/UnicodeSet$Filter;I)Lcom/ibm/icu/text/UnicodeSet;
    .locals 8
    .param p1, "filter"    # Lcom/ibm/icu/text/UnicodeSet$Filter;
    .param p2, "src"    # I

    .prologue
    .line 3040
    invoke-virtual {p0}, Lcom/ibm/icu/text/UnicodeSet;->clear()Lcom/ibm/icu/text/UnicodeSet;

    .line 3042
    const/4 v6, -0x1

    .line 3043
    .local v6, "startHasProperty":I
    invoke-static {p2}, Lcom/ibm/icu/text/UnicodeSet;->getInclusions(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v2

    .line 3044
    .local v2, "inclusions":Lcom/ibm/icu/text/UnicodeSet;
    invoke-virtual {v2}, Lcom/ibm/icu/text/UnicodeSet;->getRangeCount()I

    move-result v4

    .line 3046
    .local v4, "limitRange":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_0
    if-ge v3, v4, :cond_3

    .line 3048
    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/UnicodeSet;->getRangeStart(I)I

    move-result v5

    .line 3049
    .local v5, "start":I
    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/UnicodeSet;->getRangeEnd(I)I

    move-result v1

    .line 3052
    .local v1, "end":I
    move v0, v5

    .local v0, "ch":I
    :goto_1
    if-gt v0, v1, :cond_2

    .line 3055
    invoke-interface {p1, v0}, Lcom/ibm/icu/text/UnicodeSet$Filter;->contains(I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 3056
    if-gez v6, :cond_0

    .line 3057
    move v6, v0

    .line 3052
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3059
    :cond_1
    if-ltz v6, :cond_0

    .line 3060
    add-int/lit8 v7, v0, -0x1

    invoke-direct {p0, v6, v7}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 3061
    const/4 v6, -0x1

    goto :goto_2

    .line 3046
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3065
    .end local v0    # "ch":I
    .end local v1    # "end":I
    .end local v5    # "start":I
    :cond_3
    if-ltz v6, :cond_4

    .line 3066
    const v7, 0x10ffff

    invoke-direct {p0, v6, v7}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 3069
    :cond_4
    return-object p0
.end method

.method private applyPropertyPattern(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 19
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "ppos"    # Ljava/text/ParsePosition;
    .param p3, "symbols"    # Lcom/ibm/icu/text/SymbolTable;

    .prologue
    .line 3374
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v6

    .line 3379
    .local v6, "pos":I
    add-int/lit8 v4, v6, 0x5

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v5

    if-le v4, v5, :cond_0

    .line 3380
    const/16 p0, 0x0

    .line 3454
    .end local p0    # "this":Lcom/ibm/icu/text/UnicodeSet;
    :goto_0
    return-object p0

    .line 3383
    .restart local p0    # "this":Lcom/ibm/icu/text/UnicodeSet;
    :cond_0
    const/16 v16, 0x0

    .line 3384
    .local v16, "posix":Z
    const/4 v14, 0x0

    .line 3385
    .local v14, "isName":Z
    const/4 v13, 0x0

    .line 3388
    .local v13, "invert":Z
    const-string/jumbo v4, "[:"

    const/4 v5, 0x0

    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v4, v5, v7}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3389
    const/16 v16, 0x1

    .line 3390
    add-int/lit8 v4, v6, 0x2

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/ibm/icu/impl/Utility;->skipWhitespace(Ljava/lang/String;I)I

    move-result v6

    .line 3391
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v6, v4, :cond_1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x5e

    if-ne v4, v5, :cond_1

    .line 3392
    add-int/lit8 v6, v6, 0x1

    .line 3393
    const/4 v13, 0x1

    .line 3411
    :cond_1
    :goto_1
    if-eqz v16, :cond_9

    const-string/jumbo v4, ":]"

    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v11

    .line 3412
    .local v11, "close":I
    if-gez v11, :cond_a

    .line 3414
    const/16 p0, 0x0

    goto :goto_0

    .line 3395
    .end local v11    # "close":I
    :cond_2
    const/4 v5, 0x1

    const-string/jumbo v7, "\\p"

    const/4 v8, 0x0

    const/4 v9, 0x2

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v4

    if-nez v4, :cond_3

    const-string/jumbo v4, "\\N"

    const/4 v5, 0x0

    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v4, v5, v7}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 3397
    :cond_3
    add-int/lit8 v4, v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v10

    .line 3398
    .local v10, "c":C
    const/16 v4, 0x50

    if-ne v10, v4, :cond_5

    const/4 v13, 0x1

    .line 3399
    :goto_3
    const/16 v4, 0x4e

    if-ne v10, v4, :cond_6

    const/4 v14, 0x1

    .line 3400
    :goto_4
    add-int/lit8 v4, v6, 0x2

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/ibm/icu/impl/Utility;->skipWhitespace(Ljava/lang/String;I)I

    move-result v6

    .line 3401
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v6, v4, :cond_4

    add-int/lit8 v15, v6, 0x1

    .end local v6    # "pos":I
    .local v15, "pos":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x7b

    if-eq v4, v5, :cond_7

    move v6, v15

    .line 3403
    .end local v15    # "pos":I
    .restart local v6    # "pos":I
    :cond_4
    const/16 p0, 0x0

    goto/16 :goto_0

    .line 3398
    :cond_5
    const/4 v13, 0x0

    goto :goto_3

    .line 3399
    :cond_6
    const/4 v14, 0x0

    goto :goto_4

    .end local v6    # "pos":I
    .restart local v15    # "pos":I
    :cond_7
    move v6, v15

    .line 3405
    .end local v15    # "pos":I
    .restart local v6    # "pos":I
    goto :goto_1

    .line 3407
    .end local v10    # "c":C
    :cond_8
    const/16 p0, 0x0

    goto/16 :goto_0

    .line 3411
    :cond_9
    const-string/jumbo v4, "}"

    goto :goto_2

    .line 3420
    .restart local v11    # "close":I
    :cond_a
    const/16 v4, 0x3d

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->indexOf(II)I

    move-result v12

    .line 3422
    .local v12, "equals":I
    if-ltz v12, :cond_d

    if-ge v12, v11, :cond_d

    if-nez v14, :cond_d

    .line 3424
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    .line 3425
    .local v17, "propName":Ljava/lang/String;
    add-int/lit8 v4, v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    .line 3445
    .local v18, "valueName":Ljava/lang/String;
    :cond_b
    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/ibm/icu/text/UnicodeSet;->applyPropertyAlias(Ljava/lang/String;Ljava/lang/String;Lcom/ibm/icu/text/SymbolTable;)Lcom/ibm/icu/text/UnicodeSet;

    .line 3447
    if-eqz v13, :cond_c

    .line 3448
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/UnicodeSet;->complement()Lcom/ibm/icu/text/UnicodeSet;

    .line 3452
    :cond_c
    if-eqz v16, :cond_e

    const/4 v4, 0x2

    :goto_6
    add-int/2addr v4, v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/text/ParsePosition;->setIndex(I)V

    goto/16 :goto_0

    .line 3430
    .end local v17    # "propName":Ljava/lang/String;
    .end local v18    # "valueName":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    .line 3431
    .restart local v17    # "propName":Ljava/lang/String;
    const-string/jumbo v18, ""

    .line 3434
    .restart local v18    # "valueName":Ljava/lang/String;
    if-eqz v14, :cond_b

    .line 3440
    move-object/from16 v18, v17

    .line 3441
    const-string/jumbo v17, "na"

    goto :goto_5

    .line 3452
    :cond_e
    const/4 v4, 0x1

    goto :goto_6
.end method

.method private applyPropertyPattern(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/text/SymbolTable;)V
    .locals 4
    .param p1, "chars"    # Lcom/ibm/icu/impl/RuleCharacterIterator;
    .param p2, "rebuiltPat"    # Ljava/lang/StringBuffer;
    .param p3, "symbols"    # Lcom/ibm/icu/text/SymbolTable;

    .prologue
    const/4 v3, 0x0

    .line 3469
    invoke-virtual {p1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->lookahead()Ljava/lang/String;

    move-result-object v0

    .line 3470
    .local v0, "patStr":Ljava/lang/String;
    new-instance v1, Ljava/text/ParsePosition;

    invoke-direct {v1, v3}, Ljava/text/ParsePosition;-><init>(I)V

    .line 3471
    .local v1, "pos":Ljava/text/ParsePosition;
    invoke-direct {p0, v0, v1, p3}, Lcom/ibm/icu/text/UnicodeSet;->applyPropertyPattern(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;)Lcom/ibm/icu/text/UnicodeSet;

    .line 3472
    invoke-virtual {v1}, Ljava/text/ParsePosition;->getIndex()I

    move-result v2

    if-nez v2, :cond_0

    .line 3473
    const-string/jumbo v2, "Invalid property pattern"

    invoke-static {p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    .line 3475
    :cond_0
    invoke-virtual {v1}, Ljava/text/ParsePosition;->getIndex()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/ibm/icu/impl/RuleCharacterIterator;->jumpahead(I)V

    .line 3476
    invoke-virtual {v1}, Ljava/text/ParsePosition;->getIndex()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3477
    return-void
.end method

.method private checkFrozen()V
    .locals 2

    .prologue
    .line 3751
    iget-boolean v0, p0, Lcom/ibm/icu/text/UnicodeSet;->frozen:Z

    if-eqz v0, :cond_0

    .line 3752
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "Attempt to modify frozen object"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3754
    :cond_0
    return-void
.end method

.method private containsAll(Ljava/lang/String;I)Z
    .locals 5
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "i"    # I

    .prologue
    const/4 v3, 0x1

    .line 1837
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lt p2, v4, :cond_1

    .line 1852
    :cond_0
    :goto_0
    return v3

    .line 1840
    :cond_1
    invoke-static {p1, p2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 1841
    .local v0, "cp":I
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v4

    add-int/2addr v4, p2

    invoke-direct {p0, p1, v4}, Lcom/ibm/icu/text/UnicodeSet;->containsAll(Ljava/lang/String;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1845
    :cond_2
    iget-object v4, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v4}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1846
    .local v1, "it":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1847
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1848
    .local v2, "setStr":Ljava/lang/String;
    invoke-virtual {p1, v2, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, p2

    invoke-direct {p0, p1, v4}, Lcom/ibm/icu/text/UnicodeSet;->containsAll(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_0

    .line 1852
    .end local v2    # "setStr":Ljava/lang/String;
    :cond_4
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private ensureBufferCapacity(I)V
    .locals 1
    .param p1, "newLen"    # I

    .prologue
    .line 2688
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    array-length v0, v0

    if-gt p1, v0, :cond_0

    .line 2690
    :goto_0
    return-void

    .line 2689
    :cond_0
    add-int/lit8 v0, p1, 0x10

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    goto :goto_0
.end method

.method private ensureCapacity(I)V
    .locals 4
    .param p1, "newLen"    # I

    .prologue
    const/4 v3, 0x0

    .line 2681
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    array-length v1, v1

    if-gt p1, v1, :cond_0

    .line 2685
    :goto_0
    return-void

    .line 2682
    :cond_0
    add-int/lit8 v1, p1, 0x10

    new-array v0, v1, [I

    .line 2683
    .local v0, "temp":[I
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    iget v2, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2684
    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    goto :goto_0
.end method

.method private final findCodePoint(I)I
    .locals 5
    .param p1, "c"    # I

    .prologue
    const/4 v0, 0x0

    .line 1551
    iget-object v3, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aget v3, v3, v0

    if-ge p1, v3, :cond_1

    .line 1561
    :cond_0
    :goto_0
    return v0

    .line 1554
    :cond_1
    iget v3, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    const/4 v4, 0x2

    if-lt v3, v4, :cond_2

    iget-object v3, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    iget v4, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v4, v4, -0x2

    aget v3, v3, v4

    if-lt p1, v3, :cond_2

    iget v3, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v0, v3, -0x1

    goto :goto_0

    .line 1555
    :cond_2
    const/4 v2, 0x0

    .line 1556
    .local v2, "lo":I
    iget v3, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v0, v3, -0x1

    .line 1560
    .local v0, "hi":I
    :goto_1
    add-int v3, v2, v0

    ushr-int/lit8 v1, v3, 0x1

    .line 1561
    .local v1, "i":I
    if-eq v1, v2, :cond_0

    .line 1562
    iget-object v3, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aget v3, v3, v1

    if-ge p1, v3, :cond_3

    .line 1563
    move v0, v1

    .line 1564
    goto :goto_1

    .line 1565
    :cond_3
    move v2, v1

    goto :goto_1
.end method

.method public static from(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 1290
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    invoke-virtual {v0, p0}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method public static fromAll(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 1301
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    invoke-virtual {v0, p0}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized getInclusions(I)Lcom/ibm/icu/text/UnicodeSet;
    .locals 7
    .param p0, "src"    # I

    .prologue
    .line 2978
    const-class v3, Lcom/ibm/icu/text/UnicodeSet;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/ibm/icu/text/UnicodeSet;->INCLUSIONS:[Lcom/ibm/icu/text/UnicodeSet;

    if-nez v2, :cond_0

    .line 2979
    const/16 v2, 0x9

    new-array v2, v2, [Lcom/ibm/icu/text/UnicodeSet;

    sput-object v2, Lcom/ibm/icu/text/UnicodeSet;->INCLUSIONS:[Lcom/ibm/icu/text/UnicodeSet;

    .line 2981
    :cond_0
    sget-object v2, Lcom/ibm/icu/text/UnicodeSet;->INCLUSIONS:[Lcom/ibm/icu/text/UnicodeSet;

    aget-object v2, v2, p0

    if-nez v2, :cond_1

    .line 2982
    new-instance v1, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 2983
    .local v1, "incl":Lcom/ibm/icu/text/UnicodeSet;
    packed-switch p0, :pswitch_data_0

    .line 3015
    :pswitch_0
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "UnicodeSet.getInclusions(unknown src "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2978
    .end local v1    # "incl":Lcom/ibm/icu/text/UnicodeSet;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 2985
    .restart local v1    # "incl":Lcom/ibm/icu/text/UnicodeSet;
    :pswitch_1
    :try_start_1
    invoke-static {}, Lcom/ibm/icu/impl/UCharacterProperty;->getInstance()Lcom/ibm/icu/impl/UCharacterProperty;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->addPropertyStarts(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    .line 3017
    :goto_0
    sget-object v2, Lcom/ibm/icu/text/UnicodeSet;->INCLUSIONS:[Lcom/ibm/icu/text/UnicodeSet;

    aput-object v1, v2, p0

    .line 3019
    .end local v1    # "incl":Lcom/ibm/icu/text/UnicodeSet;
    :cond_1
    sget-object v2, Lcom/ibm/icu/text/UnicodeSet;->INCLUSIONS:[Lcom/ibm/icu/text/UnicodeSet;

    aget-object v2, v2, p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v3

    return-object v2

    .line 2988
    .restart local v1    # "incl":Lcom/ibm/icu/text/UnicodeSet;
    :pswitch_2
    :try_start_2
    invoke-static {}, Lcom/ibm/icu/impl/UCharacterProperty;->getInstance()Lcom/ibm/icu/impl/UCharacterProperty;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->upropsvec_addPropertyStarts(Lcom/ibm/icu/text/UnicodeSet;)V

    goto :goto_0

    .line 2991
    :pswitch_3
    invoke-static {}, Lcom/ibm/icu/impl/UCharacterProperty;->getInstance()Lcom/ibm/icu/impl/UCharacterProperty;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->addPropertyStarts(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    .line 2992
    invoke-static {}, Lcom/ibm/icu/impl/UCharacterProperty;->getInstance()Lcom/ibm/icu/impl/UCharacterProperty;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->upropsvec_addPropertyStarts(Lcom/ibm/icu/text/UnicodeSet;)V

    goto :goto_0

    .line 2995
    :pswitch_4
    invoke-static {}, Lcom/ibm/icu/impl/UCharacterProperty;->getInstance()Lcom/ibm/icu/impl/UCharacterProperty;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->uhst_addPropertyStarts(Lcom/ibm/icu/text/UnicodeSet;)V

    goto :goto_0

    .line 2998
    :pswitch_5
    invoke-static {v1}, Lcom/ibm/icu/impl/NormalizerImpl;->addPropertyStarts(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3002
    :pswitch_6
    :try_start_3
    invoke-static {}, Lcom/ibm/icu/impl/UCaseProps;->getSingleton()Lcom/ibm/icu/impl/UCaseProps;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/ibm/icu/impl/UCaseProps;->addPropertyStarts(Lcom/ibm/icu/text/UnicodeSet;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 3003
    :catch_0
    move-exception v0

    .line 3004
    .local v0, "e":Ljava/io/IOException;
    :try_start_4
    new-instance v2, Ljava/util/MissingResourceException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, ""

    const-string/jumbo v6, ""

    invoke-direct {v2, v4, v5, v6}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3009
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_7
    :try_start_5
    invoke-static {}, Lcom/ibm/icu/impl/UBiDiProps;->getSingleton()Lcom/ibm/icu/impl/UBiDiProps;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/ibm/icu/impl/UBiDiProps;->addPropertyStarts(Lcom/ibm/icu/text/UnicodeSet;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 3010
    :catch_1
    move-exception v0

    .line 3011
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_6
    new-instance v2, Ljava/util/MissingResourceException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, ""

    const-string/jumbo v6, ""

    invoke-direct {v2, v4, v5, v6}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2983
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_3
    .end packed-switch
.end method

.method private static getSingleCP(Ljava/lang/String;)I
    .locals 6
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v1, -0x1

    .line 1218
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v5, :cond_0

    .line 1219
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Can\'t use zero-length strings in UnicodeSet"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1221
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-le v2, v3, :cond_2

    move v0, v1

    .line 1229
    :cond_1
    :goto_0
    return v0

    .line 1222
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v2, v5, :cond_3

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0

    .line 1225
    :cond_3
    invoke-static {p0, v4}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 1226
    .local v0, "cp":I
    const v2, 0xffff

    if-gt v0, v2, :cond_1

    move v0, v1

    .line 1229
    goto :goto_0
.end method

.method private static matchRest(Lcom/ibm/icu/text/Replaceable;IILjava/lang/String;)I
    .locals 6
    .param p0, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "s"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 899
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v2

    .line 900
    .local v2, "slen":I
    if-ge p1, p2, :cond_3

    .line 901
    sub-int v1, p2, p1

    .line 902
    .local v1, "maxLen":I
    if-le v1, v2, :cond_0

    move v1, v2

    .line 903
    :cond_0
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_5

    .line 904
    add-int v4, p1, v0

    invoke-interface {p0, v4}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v4

    invoke-virtual {p3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-eq v4, v5, :cond_2

    .line 914
    :cond_1
    :goto_1
    return v3

    .line 903
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 907
    .end local v0    # "i":I
    .end local v1    # "maxLen":I
    :cond_3
    sub-int v1, p1, p2

    .line 908
    .restart local v1    # "maxLen":I
    if-le v1, v2, :cond_4

    move v1, v2

    .line 909
    :cond_4
    add-int/lit8 v2, v2, -0x1

    .line 910
    const/4 v0, 0x1

    .restart local v0    # "i":I
    :goto_2
    if-ge v0, v1, :cond_5

    .line 911
    sub-int v4, p1, v0

    invoke-interface {p0, v4}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v4

    sub-int v5, v2, v0

    invoke-virtual {p3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_1

    .line 910
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move v3, v1

    .line 914
    goto :goto_1
.end method

.method private static matchesAt(Ljava/lang/CharSequence;ILjava/lang/CharSequence;)I
    .locals 5
    .param p0, "text"    # Ljava/lang/CharSequence;
    .param p1, "offset"    # I
    .param p2, "other"    # Ljava/lang/CharSequence;

    .prologue
    .line 965
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 966
    .local v2, "len":I
    const/4 v0, 0x0

    .line 967
    .local v0, "i":I
    move v1, p1

    .line 968
    .local v1, "j":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 969
    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 970
    .local v3, "pc":C
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    .line 971
    .local v4, "tc":C
    if-eq v3, v4, :cond_1

    const/4 v0, -0x1

    .line 973
    .end local v0    # "i":I
    .end local v3    # "pc":C
    .end local v4    # "tc":C
    :cond_0
    return v0

    .line 968
    .restart local v0    # "i":I
    .restart local v3    # "pc":C
    .restart local v4    # "tc":C
    :cond_1
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static final max(II)I
    .locals 0
    .param p0, "a"    # I
    .param p1, "b"    # I

    .prologue
    .line 2923
    if-le p0, p1, :cond_0

    .end local p0    # "a":I
    :goto_0
    return p0

    .restart local p0    # "a":I
    :cond_0
    move p0, p1

    goto :goto_0
.end method

.method private static mungeCharName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "source"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x20

    .line 3080
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 3081
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :cond_0
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 3082
    invoke-static {p0, v2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v1

    .line 3083
    .local v1, "ch":I
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 3084
    invoke-static {v1}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3085
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    if-eq v3, v4, :cond_0

    .line 3089
    const/16 v1, 0x20

    .line 3091
    :cond_1
    invoke-static {v0, v1}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 3093
    .end local v1    # "ch":I
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    if-ne v3, v4, :cond_3

    .line 3095
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 3097
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private range(II)[I
    .locals 3
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2696
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->rangeList:[I

    if-nez v0, :cond_0

    .line 2697
    const/4 v0, 0x3

    new-array v0, v0, [I

    aput p1, v0, v1

    add-int/lit8 v1, p2, 0x1

    aput v1, v0, v2

    const/4 v1, 0x2

    const/high16 v2, 0x110000

    aput v2, v0, v1

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->rangeList:[I

    .line 2702
    :goto_0
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->rangeList:[I

    return-object v0

    .line 2699
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->rangeList:[I

    aput p1, v0, v1

    .line 2700
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->rangeList:[I

    add-int/lit8 v1, p2, 0x1

    aput v1, v0, v2

    goto :goto_0
.end method

.method public static resemblesPattern(Ljava/lang/String;I)Z
    .locals 2
    .param p0, "pattern"    # Ljava/lang/String;
    .param p1, "pos"    # I

    .prologue
    .line 529
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x5b

    if-eq v0, v1, :cond_1

    :cond_0
    invoke-static {p0, p1}, Lcom/ibm/icu/text/UnicodeSet;->resemblesPropertyPattern(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static resemblesPropertyPattern(Lcom/ibm/icu/impl/RuleCharacterIterator;I)Z
    .locals 8
    .param p0, "chars"    # Lcom/ibm/icu/impl/RuleCharacterIterator;
    .param p1, "iterOpts"    # I

    .prologue
    const/16 v7, 0x5b

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 3356
    const/4 v3, 0x0

    .line 3357
    .local v3, "result":Z
    and-int/lit8 p1, p1, -0x3

    .line 3358
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/ibm/icu/impl/RuleCharacterIterator;->getPos(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 3359
    .local v2, "pos":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->next(I)I

    move-result v0

    .line 3360
    .local v0, "c":I
    if-eq v0, v7, :cond_0

    const/16 v6, 0x5c

    if-ne v0, v6, :cond_1

    .line 3361
    :cond_0
    and-int/lit8 v6, p1, -0x5

    invoke-virtual {p0, v6}, Lcom/ibm/icu/impl/RuleCharacterIterator;->next(I)I

    move-result v1

    .line 3362
    .local v1, "d":I
    if-ne v0, v7, :cond_3

    const/16 v6, 0x3a

    if-ne v1, v6, :cond_2

    move v3, v4

    .line 3365
    .end local v1    # "d":I
    :cond_1
    :goto_0
    invoke-virtual {p0, v2}, Lcom/ibm/icu/impl/RuleCharacterIterator;->setPos(Ljava/lang/Object;)V

    .line 3366
    return v3

    .restart local v1    # "d":I
    :cond_2
    move v3, v5

    .line 3362
    goto :goto_0

    :cond_3
    const/16 v6, 0x4e

    if-eq v1, v6, :cond_4

    const/16 v6, 0x70

    if-eq v1, v6, :cond_4

    const/16 v6, 0x50

    if-ne v1, v6, :cond_5

    :cond_4
    move v3, v4

    goto :goto_0

    :cond_5
    move v3, v5

    goto :goto_0
.end method

.method private static resemblesPropertyPattern(Ljava/lang/String;I)Z
    .locals 6
    .param p0, "pattern"    # Ljava/lang/String;
    .param p1, "pos"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 3336
    add-int/lit8 v0, p1, 0x5

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-le v0, v2, :cond_1

    .line 3341
    :cond_0
    :goto_0
    return v4

    :cond_1
    const-string/jumbo v0, "[:"

    invoke-virtual {p0, p1, v0, v4, v5}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v3, "\\p"

    move-object v0, p0

    move v2, p1

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "\\N"

    invoke-virtual {p0, p1, v0, v4, v5}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    move v4, v1

    goto :goto_0
.end method

.method private retain([III)Lcom/ibm/icu/text/UnicodeSet;
    .locals 11
    .param p1, "other"    # [I
    .param p2, "otherLen"    # I
    .param p3, "polarity"    # I

    .prologue
    const/high16 v10, 0x110000

    .line 2857
    iget v9, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/2addr v9, p2

    invoke-direct {p0, v9}, Lcom/ibm/icu/text/UnicodeSet;->ensureBufferCapacity(I)V

    .line 2858
    const/4 v2, 0x0

    .local v2, "i":I
    const/4 v4, 0x0

    .local v4, "j":I
    const/4 v6, 0x0

    .line 2859
    .local v6, "k":I
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    aget v0, v9, v2

    .line 2860
    .local v0, "a":I
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "j":I
    .local v5, "j":I
    aget v1, p1, v4

    .local v1, "b":I
    move v7, v6

    .line 2865
    .end local v6    # "k":I
    .local v7, "k":I
    :goto_0
    packed-switch p3, :pswitch_data_0

    move v6, v7

    .end local v7    # "k":I
    .restart local v6    # "k":I
    move v4, v5

    .end local v5    # "j":I
    .restart local v4    # "j":I
    move v2, v3

    .end local v3    # "i":I
    .restart local v2    # "i":I
    :goto_1
    move v7, v6

    .end local v6    # "k":I
    .restart local v7    # "k":I
    move v5, v4

    .end local v4    # "j":I
    .restart local v5    # "j":I
    move v3, v2

    .line 2911
    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 2867
    :pswitch_0
    if-ge v0, v1, :cond_0

    .line 2868
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    xor-int/lit8 p3, p3, 0x1

    move v6, v7

    .end local v7    # "k":I
    .restart local v6    # "k":I
    move v4, v5

    .line 2869
    .end local v5    # "j":I
    .restart local v4    # "j":I
    goto :goto_1

    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_0
    if-ge v1, v0, :cond_1

    .line 2870
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    xor-int/lit8 p3, p3, 0x2

    move v6, v7

    .end local v7    # "k":I
    .restart local v6    # "k":I
    move v2, v3

    .line 2871
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_1

    .line 2872
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_1
    if-ne v0, v10, :cond_3

    .line 2912
    :cond_2
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v10, v9, v7

    .line 2913
    iput v6, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    .line 2915
    iget-object v8, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    .line 2916
    .local v8, "temp":[I
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    iput-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    .line 2917
    iput-object v8, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    .line 2918
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    .line 2919
    return-object p0

    .line 2873
    .end local v6    # "k":I
    .end local v8    # "temp":[I
    .restart local v7    # "k":I
    :cond_3
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v0, v9, v7

    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    xor-int/lit8 p3, p3, 0x1

    .line 2874
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    xor-int/lit8 p3, p3, 0x2

    .line 2876
    goto :goto_1

    .line 2878
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :pswitch_1
    if-ge v0, v1, :cond_4

    .line 2879
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v0, v9, v7

    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    xor-int/lit8 p3, p3, 0x1

    move v4, v5

    .line 2880
    .end local v5    # "j":I
    .restart local v4    # "j":I
    goto :goto_1

    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_4
    if-ge v1, v0, :cond_5

    .line 2881
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v1, v9, v7

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    xor-int/lit8 p3, p3, 0x2

    move v2, v3

    .line 2882
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_1

    .line 2883
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_5
    if-eq v0, v10, :cond_2

    .line 2884
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v0, v9, v7

    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    xor-int/lit8 p3, p3, 0x1

    .line 2885
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    xor-int/lit8 p3, p3, 0x2

    .line 2887
    goto :goto_1

    .line 2889
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :pswitch_2
    if-ge v0, v1, :cond_6

    .line 2890
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    xor-int/lit8 p3, p3, 0x1

    move v6, v7

    .end local v7    # "k":I
    .restart local v6    # "k":I
    move v4, v5

    .line 2891
    .end local v5    # "j":I
    .restart local v4    # "j":I
    goto/16 :goto_1

    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_6
    if-ge v1, v0, :cond_7

    .line 2892
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v1, v9, v7

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    xor-int/lit8 p3, p3, 0x2

    move v2, v3

    .line 2893
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto/16 :goto_1

    .line 2894
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_7
    if-eq v0, v10, :cond_2

    .line 2895
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    xor-int/lit8 p3, p3, 0x1

    .line 2896
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    xor-int/lit8 p3, p3, 0x2

    move v6, v7

    .line 2898
    .end local v7    # "k":I
    .restart local v6    # "k":I
    goto/16 :goto_1

    .line 2900
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :pswitch_3
    if-ge v1, v0, :cond_8

    .line 2901
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    xor-int/lit8 p3, p3, 0x2

    move v6, v7

    .end local v7    # "k":I
    .restart local v6    # "k":I
    move v2, v3

    .line 2902
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto/16 :goto_1

    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_8
    if-ge v0, v1, :cond_9

    .line 2903
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v0, v9, v7

    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    xor-int/lit8 p3, p3, 0x1

    move v4, v5

    .line 2904
    .end local v5    # "j":I
    .restart local v4    # "j":I
    goto/16 :goto_1

    .line 2905
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v6    # "k":I
    .restart local v3    # "i":I
    .restart local v5    # "j":I
    .restart local v7    # "k":I
    :cond_9
    if-eq v0, v10, :cond_2

    .line 2906
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    xor-int/lit8 p3, p3, 0x1

    .line 2907
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    xor-int/lit8 p3, p3, 0x2

    move v6, v7

    .end local v7    # "k":I
    .restart local v6    # "k":I
    goto/16 :goto_1

    .line 2865
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private static syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V
    .locals 3
    .param p0, "chars"    # Lcom/ibm/icu/impl/RuleCharacterIterator;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 2646
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " at \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/ibm/icu/impl/RuleCharacterIterator;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/ibm/icu/impl/Utility;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const/16 v2, 0x22

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private xor([III)Lcom/ibm/icu/text/UnicodeSet;
    .locals 11
    .param p1, "other"    # [I
    .param p2, "otherLen"    # I
    .param p3, "polarity"    # I

    .prologue
    const/high16 v10, 0x110000

    .line 2713
    iget v9, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/2addr v9, p2

    invoke-direct {p0, v9}, Lcom/ibm/icu/text/UnicodeSet;->ensureBufferCapacity(I)V

    .line 2714
    const/4 v2, 0x0

    .local v2, "i":I
    const/4 v4, 0x0

    .local v4, "j":I
    const/4 v6, 0x0

    .line 2715
    .local v6, "k":I
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    aget v0, v9, v2

    .line 2717
    .local v0, "a":I
    const/4 v9, 0x1

    if-eq p3, v9, :cond_0

    const/4 v9, 0x2

    if-ne p3, v9, :cond_1

    .line 2718
    :cond_0
    const/4 v1, 0x0

    .line 2719
    .local v1, "b":I
    aget v9, p1, v4

    if-nez v9, :cond_5

    .line 2720
    add-int/lit8 v4, v4, 0x1

    .line 2721
    aget v1, p1, v4

    move v7, v6

    .end local v6    # "k":I
    .local v7, "k":I
    move v5, v4

    .line 2729
    .end local v4    # "j":I
    .local v5, "j":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 2730
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v0, v9, v7

    .line 2731
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    move v7, v6

    .end local v6    # "k":I
    .restart local v7    # "k":I
    move v3, v2

    .line 2732
    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 2724
    .end local v1    # "b":I
    .end local v5    # "j":I
    .end local v7    # "k":I
    .restart local v4    # "j":I
    .restart local v6    # "k":I
    :cond_1
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "j":I
    .restart local v5    # "j":I
    aget v1, p1, v4

    .restart local v1    # "b":I
    move v7, v6

    .end local v6    # "k":I
    .restart local v7    # "k":I
    goto :goto_0

    .line 2732
    :cond_2
    if-ge v1, v0, :cond_3

    .line 2733
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v1, v9, v7

    .line 2734
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    move v7, v6

    .end local v6    # "k":I
    .restart local v7    # "k":I
    move v5, v4

    .line 2735
    .end local v4    # "j":I
    .restart local v5    # "j":I
    goto :goto_0

    :cond_3
    if-eq v0, v10, :cond_4

    .line 2737
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aget v0, v9, v3

    .line 2738
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "j":I
    .restart local v4    # "j":I
    aget v1, p1, v5

    move v5, v4

    .end local v4    # "j":I
    .restart local v5    # "j":I
    move v3, v2

    .line 2739
    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 2740
    :cond_4
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "k":I
    .restart local v6    # "k":I
    aput v10, v9, v7

    .line 2741
    iput v6, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    .line 2746
    iget-object v8, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    .line 2747
    .local v8, "temp":[I
    iget-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    iput-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    .line 2748
    iput-object v8, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    .line 2749
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    .line 2750
    return-object p0

    .end local v5    # "j":I
    .end local v8    # "temp":[I
    .restart local v4    # "j":I
    :cond_5
    move v7, v6

    .end local v6    # "k":I
    .restart local v7    # "k":I
    move v5, v4

    .end local v4    # "j":I
    .restart local v5    # "j":I
    goto :goto_0
.end method


# virtual methods
.method public _generatePattern(Ljava/lang/StringBuffer;Z)Ljava/lang/StringBuffer;
    .locals 1
    .param p1, "result"    # Ljava/lang/StringBuffer;
    .param p2, "escapeUnprintable"    # Z

    .prologue
    .line 638
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/ibm/icu/text/UnicodeSet;->_generatePattern(Ljava/lang/StringBuffer;ZZ)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public _generatePattern(Ljava/lang/StringBuffer;ZZ)Ljava/lang/StringBuffer;
    .locals 8
    .param p1, "result"    # Ljava/lang/StringBuffer;
    .param p2, "escapeUnprintable"    # Z
    .param p3, "includeStrings"    # Z

    .prologue
    const/16 v7, 0x2d

    .line 650
    const/16 v5, 0x5b

    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 662
    invoke-virtual {p0}, Lcom/ibm/icu/text/UnicodeSet;->getRangeCount()I

    move-result v0

    .line 667
    .local v0, "count":I
    const/4 v5, 0x1

    if-le v0, v5, :cond_2

    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/ibm/icu/text/UnicodeSet;->getRangeStart(I)I

    move-result v5

    if-nez v5, :cond_2

    add-int/lit8 v5, v0, -0x1

    invoke-virtual {p0, v5}, Lcom/ibm/icu/text/UnicodeSet;->getRangeEnd(I)I

    move-result v5

    const v6, 0x10ffff

    if-ne v5, v6, :cond_2

    .line 672
    const/16 v5, 0x5e

    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 674
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_5

    .line 675
    add-int/lit8 v5, v2, -0x1

    invoke-virtual {p0, v5}, Lcom/ibm/icu/text/UnicodeSet;->getRangeEnd(I)I

    move-result v5

    add-int/lit8 v4, v5, 0x1

    .line 676
    .local v4, "start":I
    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/UnicodeSet;->getRangeStart(I)I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    .line 677
    .local v1, "end":I
    invoke-static {p1, v4, p2}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;IZ)V

    .line 678
    if-eq v4, v1, :cond_1

    .line 679
    add-int/lit8 v5, v4, 0x1

    if-eq v5, v1, :cond_0

    .line 680
    invoke-virtual {p1, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 682
    :cond_0
    invoke-static {p1, v1, p2}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;IZ)V

    .line 674
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 689
    .end local v1    # "end":I
    .end local v2    # "i":I
    .end local v4    # "start":I
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    if-ge v2, v0, :cond_5

    .line 690
    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/UnicodeSet;->getRangeStart(I)I

    move-result v4

    .line 691
    .restart local v4    # "start":I
    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/UnicodeSet;->getRangeEnd(I)I

    move-result v1

    .line 692
    .restart local v1    # "end":I
    invoke-static {p1, v4, p2}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;IZ)V

    .line 693
    if-eq v4, v1, :cond_4

    .line 694
    add-int/lit8 v5, v4, 0x1

    if-eq v5, v1, :cond_3

    .line 695
    invoke-virtual {p1, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 697
    :cond_3
    invoke-static {p1, v1, p2}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;IZ)V

    .line 689
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 702
    .end local v1    # "end":I
    .end local v4    # "start":I
    :cond_5
    if-eqz p3, :cond_6

    iget-object v5, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v5}, Ljava/util/TreeSet;->size()I

    move-result v5

    if-lez v5, :cond_6

    .line 703
    iget-object v5, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v5}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 704
    .local v3, "it":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 705
    const/16 v5, 0x7b

    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 706
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p1, v5, p2}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;Ljava/lang/String;Z)V

    .line 707
    const/16 v5, 0x7d

    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 710
    .end local v3    # "it":Ljava/util/Iterator;
    :cond_6
    const/16 v5, 0x5d

    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v5

    return-object v5
.end method

.method public final add(I)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 1101
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 1102
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method public add(II)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 1055
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 1056
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(II)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1201
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 1202
    invoke-static {p1}, Lcom/ibm/icu/text/UnicodeSet;->getSingleCP(Ljava/lang/String;)I

    move-result v0

    .line 1203
    .local v0, "cp":I
    if-gez v0, :cond_0

    .line 1204
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v1, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 1205
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    .line 1209
    :goto_0
    return-object p0

    .line 1207
    :cond_0
    invoke-direct {p0, v0, v0}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(II)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0
.end method

.method public addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 3
    .param p1, "c"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 2045
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 2046
    iget-object v0, p1, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    iget v1, p1, Lcom/ibm/icu/text/UnicodeSet;->len:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add([III)Lcom/ibm/icu/text/UnicodeSet;

    .line 2047
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    iget-object v1, p1, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    .line 2048
    return-object p0
.end method

.method public final addAll(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 3
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1240
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 1242
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1243
    invoke-static {p1, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 1244
    .local v0, "cp":I
    invoke-direct {p0, v0, v0}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 1242
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    .line 1246
    .end local v0    # "cp":I
    :cond_0
    return-object p0
.end method

.method public addAll(Ljava/util/Collection;)V
    .locals 2
    .param p1, "source"    # Ljava/util/Collection;

    .prologue
    .line 2669
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 2670
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 2671
    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2672
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0

    .line 2674
    :cond_0
    return-void
.end method

.method public addAllTo(Ljava/util/Collection;)V
    .locals 2
    .param p1, "target"    # Ljava/util/Collection;

    .prologue
    .line 2657
    new-instance v0, Lcom/ibm/icu/text/UnicodeSetIterator;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/UnicodeSetIterator;-><init>(Lcom/ibm/icu/text/UnicodeSet;)V

    .line 2658
    .local v0, "it":Lcom/ibm/icu/text/UnicodeSetIterator;
    :goto_0
    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSetIterator;->next()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2659
    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSetIterator;->getString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2661
    :cond_0
    return-void
.end method

.method public addMatchSetTo(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 0
    .param p1, "toUnionTo"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 985
    invoke-virtual {p1, p0}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    .line 986
    return-void
.end method

.method public applyIntPropertyValue(II)Lcom/ibm/icu/text/UnicodeSet;
    .locals 2
    .param p1, "prop"    # I
    .param p2, "value"    # I

    .prologue
    .line 3128
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 3129
    const/16 v0, 0x2000

    if-ne p1, v0, :cond_0

    .line 3130
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet$GeneralCategoryMaskFilter;

    invoke-direct {v0, p2}, Lcom/ibm/icu/text/UnicodeSet$GeneralCategoryMaskFilter;-><init>(I)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->applyFilter(Lcom/ibm/icu/text/UnicodeSet$Filter;I)Lcom/ibm/icu/text/UnicodeSet;

    .line 3134
    :goto_0
    return-object p0

    .line 3132
    :cond_0
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet$IntPropertyFilter;

    invoke-direct {v0, p1, p2}, Lcom/ibm/icu/text/UnicodeSet$IntPropertyFilter;-><init>(II)V

    invoke-static {}, Lcom/ibm/icu/impl/UCharacterProperty;->getInstance()Lcom/ibm/icu/impl/UCharacterProperty;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/ibm/icu/impl/UCharacterProperty;->getSource(I)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->applyFilter(Lcom/ibm/icu/text/UnicodeSet$Filter;I)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0
.end method

.method public final applyPattern(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 2
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 487
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 488
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v1, v1, v0}, Lcom/ibm/icu/text/UnicodeSet;->applyPattern(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method public applyPattern(Ljava/lang/String;I)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "options"    # I

    .prologue
    const/4 v0, 0x0

    .line 519
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 520
    invoke-virtual {p0, p1, v0, v0, p2}, Lcom/ibm/icu/text/UnicodeSet;->applyPattern(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method applyPattern(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;I)Lcom/ibm/icu/text/UnicodeSet;
    .locals 7
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;
    .param p3, "symbols"    # Lcom/ibm/icu/text/SymbolTable;
    .param p4, "options"    # I

    .prologue
    const/4 v4, 0x0

    .line 2251
    if-nez p2, :cond_3

    const/4 v2, 0x1

    .line 2252
    .local v2, "parsePositionWasNull":Z
    :goto_0
    if-eqz v2, :cond_0

    .line 2253
    new-instance p2, Ljava/text/ParsePosition;

    .end local p2    # "pos":Ljava/text/ParsePosition;
    invoke-direct {p2, v4}, Ljava/text/ParsePosition;-><init>(I)V

    .line 2256
    .restart local p2    # "pos":Ljava/text/ParsePosition;
    :cond_0
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 2257
    .local v3, "rebuiltPat":Ljava/lang/StringBuffer;
    new-instance v0, Lcom/ibm/icu/impl/RuleCharacterIterator;

    invoke-direct {v0, p1, p3, p2}, Lcom/ibm/icu/impl/RuleCharacterIterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/SymbolTable;Ljava/text/ParsePosition;)V

    .line 2259
    .local v0, "chars":Lcom/ibm/icu/impl/RuleCharacterIterator;
    invoke-virtual {p0, v0, p3, v3, p4}, Lcom/ibm/icu/text/UnicodeSet;->applyPattern(Lcom/ibm/icu/impl/RuleCharacterIterator;Lcom/ibm/icu/text/SymbolTable;Ljava/lang/StringBuffer;I)V

    .line 2260
    invoke-virtual {v0}, Lcom/ibm/icu/impl/RuleCharacterIterator;->inVariable()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2261
    const-string/jumbo v4, "Extra chars in variable value"

    invoke-static {v0, v4}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    .line 2263
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    .line 2264
    if-eqz v2, :cond_4

    .line 2265
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v1

    .line 2268
    .local v1, "i":I
    and-int/lit8 v4, p4, 0x1

    if-eqz v4, :cond_2

    .line 2269
    invoke-static {p1, v1}, Lcom/ibm/icu/impl/Utility;->skipWhitespace(Ljava/lang/String;I)I

    move-result v1

    .line 2272
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_4

    .line 2273
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "Parse of \""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, "\" failed at "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .end local v0    # "chars":Lcom/ibm/icu/impl/RuleCharacterIterator;
    .end local v1    # "i":I
    .end local v2    # "parsePositionWasNull":Z
    .end local v3    # "rebuiltPat":Ljava/lang/StringBuffer;
    :cond_3
    move v2, v4

    .line 2251
    goto :goto_0

    .line 2277
    .restart local v0    # "chars":Lcom/ibm/icu/impl/RuleCharacterIterator;
    .restart local v2    # "parsePositionWasNull":Z
    .restart local v3    # "rebuiltPat":Ljava/lang/StringBuffer;
    :cond_4
    return-object p0
.end method

.method public applyPattern(Ljava/lang/String;Z)Lcom/ibm/icu/text/UnicodeSet;
    .locals 2
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "ignoreWhitespace"    # Z

    .prologue
    const/4 v1, 0x0

    .line 503
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 504
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v1, v1, v0}, Lcom/ibm/icu/text/UnicodeSet;->applyPattern(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;I)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method applyPattern(Lcom/ibm/icu/impl/RuleCharacterIterator;Lcom/ibm/icu/text/SymbolTable;Ljava/lang/StringBuffer;I)V
    .locals 25
    .param p1, "chars"    # Lcom/ibm/icu/impl/RuleCharacterIterator;
    .param p2, "symbols"    # Lcom/ibm/icu/text/SymbolTable;
    .param p3, "rebuiltPat"    # Ljava/lang/StringBuffer;
    .param p4, "options"    # I

    .prologue
    .line 2301
    const/16 v18, 0x3

    .line 2303
    .local v18, "opts":I
    and-int/lit8 v23, p4, 0x1

    if-eqz v23, :cond_0

    .line 2304
    or-int/lit8 v18, v18, 0x4

    .line 2307
    :cond_0
    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    .local v19, "patBuf":Ljava/lang/StringBuffer;
    const/4 v6, 0x0

    .line 2308
    .local v6, "buf":Ljava/lang/StringBuffer;
    const/16 v22, 0x0

    .line 2309
    .local v22, "usePat":Z
    const/16 v20, 0x0

    .line 2310
    .local v20, "scratch":Lcom/ibm/icu/text/UnicodeSet;
    const/4 v5, 0x0

    .line 2314
    .local v5, "backup":Ljava/lang/Object;
    const/4 v11, 0x0

    .local v11, "lastItem":I
    const/4 v10, 0x0

    .local v10, "lastChar":I
    const/4 v14, 0x0

    .line 2315
    .local v14, "mode":I
    const/16 v17, 0x0

    .line 2317
    .local v17, "op":C
    const/4 v9, 0x0

    .line 2319
    .local v9, "invert":Z
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/UnicodeSet;->clear()Lcom/ibm/icu/text/UnicodeSet;

    .line 2321
    .end local v5    # "backup":Ljava/lang/Object;
    :goto_0
    const/16 v23, 0x2

    move/from16 v0, v23

    if-eq v14, v0, :cond_8

    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->atEnd()Z

    move-result v23

    if-nez v23, :cond_8

    .line 2331
    const/4 v7, 0x0

    .line 2332
    .local v7, "c":I
    const/4 v12, 0x0

    .line 2333
    .local v12, "literal":Z
    const/4 v15, 0x0

    .line 2338
    .local v15, "nested":Lcom/ibm/icu/text/UnicodeSet;
    const/16 v21, 0x0

    .line 2339
    .local v21, "setMode":I
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->resemblesPropertyPattern(Lcom/ibm/icu/impl/RuleCharacterIterator;I)Z

    move-result v23

    if-eqz v23, :cond_c

    .line 2340
    const/16 v21, 0x2

    .line 2403
    :cond_1
    :goto_1
    if-eqz v21, :cond_12

    .line 2404
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v11, v0, :cond_3

    .line 2405
    if-eqz v17, :cond_2

    .line 2406
    const-string/jumbo v23, "Char expected after operator"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    .line 2408
    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v10}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 2409
    const/16 v23, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-static {v0, v10, v1}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;IZ)V

    .line 2410
    const/16 v17, 0x0

    move/from16 v11, v17

    .line 2413
    :cond_3
    const/16 v23, 0x2d

    move/from16 v0, v17

    move/from16 v1, v23

    if-eq v0, v1, :cond_4

    const/16 v23, 0x26

    move/from16 v0, v17

    move/from16 v1, v23

    if-ne v0, v1, :cond_5

    .line 2414
    :cond_4
    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2417
    :cond_5
    if-nez v15, :cond_7

    .line 2418
    if-nez v20, :cond_6

    new-instance v20, Lcom/ibm/icu/text/UnicodeSet;

    .end local v20    # "scratch":Lcom/ibm/icu/text/UnicodeSet;
    invoke-direct/range {v20 .. v20}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 2419
    .restart local v20    # "scratch":Lcom/ibm/icu/text/UnicodeSet;
    :cond_6
    move-object/from16 v15, v20

    .line 2421
    :cond_7
    packed-switch v21, :pswitch_data_0

    .line 2434
    :goto_2
    const/16 v22, 0x1

    .line 2436
    if-nez v14, :cond_11

    .line 2438
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/ibm/icu/text/UnicodeSet;->set(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    .line 2439
    const/4 v14, 0x2

    .line 2617
    .end local v7    # "c":I
    .end local v12    # "literal":Z
    .end local v15    # "nested":Lcom/ibm/icu/text/UnicodeSet;
    .end local v21    # "setMode":I
    :cond_8
    const/16 v23, 0x2

    move/from16 v0, v23

    if-eq v14, v0, :cond_9

    .line 2618
    const-string/jumbo v23, "Missing \']\'"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    .line 2621
    :cond_9
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->skipIgnored(I)V

    .line 2629
    and-int/lit8 v23, p4, 0x2

    if-eqz v23, :cond_a

    .line 2630
    const/16 v23, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->closeOver(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 2632
    :cond_a
    if-eqz v9, :cond_b

    .line 2633
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/UnicodeSet;->complement()Lcom/ibm/icu/text/UnicodeSet;

    .line 2638
    :cond_b
    if-eqz v22, :cond_29

    .line 2639
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p3

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2643
    :goto_3
    return-void

    .line 2353
    .restart local v7    # "c":I
    .restart local v12    # "literal":Z
    .restart local v15    # "nested":Lcom/ibm/icu/text/UnicodeSet;
    .restart local v21    # "setMode":I
    :cond_c
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/ibm/icu/impl/RuleCharacterIterator;->getPos(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 2354
    .restart local v5    # "backup":Ljava/lang/Object;
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->next(I)I

    move-result v7

    .line 2355
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->isEscaped()Z

    move-result v12

    .line 2357
    const/16 v23, 0x5b

    move/from16 v0, v23

    if-ne v7, v0, :cond_10

    if-nez v12, :cond_10

    .line 2358
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v14, v0, :cond_d

    .line 2359
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/ibm/icu/impl/RuleCharacterIterator;->setPos(Ljava/lang/Object;)V

    .line 2360
    const/16 v21, 0x1

    .line 2361
    goto/16 :goto_1

    .line 2363
    :cond_d
    const/4 v14, 0x1

    .line 2364
    const/16 v23, 0x5b

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2365
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/ibm/icu/impl/RuleCharacterIterator;->getPos(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 2366
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->next(I)I

    move-result v7

    .line 2367
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->isEscaped()Z

    move-result v12

    .line 2368
    const/16 v23, 0x5e

    move/from16 v0, v23

    if-ne v7, v0, :cond_e

    if-nez v12, :cond_e

    .line 2369
    const/4 v9, 0x1

    .line 2370
    const/16 v23, 0x5e

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2371
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/ibm/icu/impl/RuleCharacterIterator;->getPos(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 2372
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->next(I)I

    move-result v7

    .line 2373
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->isEscaped()Z

    move-result v12

    .line 2377
    :cond_e
    const/16 v23, 0x2d

    move/from16 v0, v23

    if-ne v7, v0, :cond_f

    .line 2378
    const/4 v12, 0x1

    .line 2380
    goto/16 :goto_1

    .line 2381
    :cond_f
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/ibm/icu/impl/RuleCharacterIterator;->setPos(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 2385
    :cond_10
    if-eqz p2, :cond_1

    .line 2386
    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Lcom/ibm/icu/text/SymbolTable;->lookupMatcher(I)Lcom/ibm/icu/text/UnicodeMatcher;

    move-result-object v13

    .line 2387
    .local v13, "m":Lcom/ibm/icu/text/UnicodeMatcher;
    if-eqz v13, :cond_1

    .line 2389
    :try_start_0
    move-object v0, v13

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    move-object v15, v0
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2390
    const/16 v21, 0x3

    goto/16 :goto_1

    .line 2391
    :catch_0
    move-exception v8

    .line 2392
    .local v8, "e":Ljava/lang/ClassCastException;
    const-string/jumbo v23, "Syntax error"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2423
    .end local v5    # "backup":Ljava/lang/Object;
    .end local v8    # "e":Ljava/lang/ClassCastException;
    .end local v13    # "m":Lcom/ibm/icu/text/UnicodeMatcher;
    :pswitch_0
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v19

    move/from16 v3, p4

    invoke-virtual {v15, v0, v1, v2, v3}, Lcom/ibm/icu/text/UnicodeSet;->applyPattern(Lcom/ibm/icu/impl/RuleCharacterIterator;Lcom/ibm/icu/text/SymbolTable;Ljava/lang/StringBuffer;I)V

    goto/16 :goto_2

    .line 2426
    :pswitch_1
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->skipIgnored(I)V

    .line 2427
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-direct {v15, v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->applyPropertyPattern(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/text/SymbolTable;)V

    goto/16 :goto_2

    .line 2430
    :pswitch_2
    const/16 v23, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-direct {v15, v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->_toPattern(Ljava/lang/StringBuffer;Z)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 2443
    :cond_11
    sparse-switch v17, :sswitch_data_0

    .line 2455
    :goto_4
    const/16 v17, 0x0

    .line 2456
    const/4 v11, 0x2

    .line 2458
    goto/16 :goto_0

    .line 2445
    :sswitch_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/ibm/icu/text/UnicodeSet;->removeAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_4

    .line 2448
    :sswitch_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/ibm/icu/text/UnicodeSet;->retainAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_4

    .line 2451
    :sswitch_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_4

    .line 2461
    :cond_12
    if-nez v14, :cond_13

    .line 2462
    const-string/jumbo v23, "Missing \'[\'"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    .line 2469
    :cond_13
    if-nez v12, :cond_14

    .line 2470
    sparse-switch v7, :sswitch_data_1

    .line 2584
    :cond_14
    :goto_5
    packed-switch v11, :pswitch_data_1

    goto/16 :goto_0

    .line 2586
    :pswitch_3
    const/4 v11, 0x1

    .line 2587
    move v10, v7

    .line 2588
    goto/16 :goto_0

    .line 2472
    :sswitch_3
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v11, v0, :cond_15

    .line 2473
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v10}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 2474
    const/16 v23, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-static {v0, v10, v1}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;IZ)V

    .line 2477
    :cond_15
    const/16 v23, 0x2d

    move/from16 v0, v17

    move/from16 v1, v23

    if-ne v0, v1, :cond_17

    .line 2478
    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 2479
    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2483
    :cond_16
    :goto_6
    const/16 v23, 0x5d

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2484
    const/4 v14, 0x2

    .line 2485
    goto/16 :goto_0

    .line 2480
    :cond_17
    const/16 v23, 0x26

    move/from16 v0, v17

    move/from16 v1, v23

    if-ne v0, v1, :cond_16

    .line 2481
    const-string/jumbo v23, "Trailing \'&\'"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    goto :goto_6

    .line 2487
    :sswitch_4
    if-nez v17, :cond_19

    .line 2488
    if-eqz v11, :cond_18

    .line 2489
    int-to-char v0, v7

    move/from16 v17, v0

    .line 2490
    goto/16 :goto_0

    .line 2493
    :cond_18
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v7}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 2494
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->next(I)I

    move-result v7

    .line 2495
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->isEscaped()Z

    move-result v12

    .line 2496
    const/16 v23, 0x5d

    move/from16 v0, v23

    if-ne v7, v0, :cond_19

    if-nez v12, :cond_19

    .line 2497
    const-string/jumbo v23, "-]"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2498
    const/4 v14, 0x2

    .line 2499
    goto/16 :goto_0

    .line 2503
    :cond_19
    const-string/jumbo v23, "\'-\' not after char or set"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    .line 2505
    :sswitch_5
    const/16 v23, 0x2

    move/from16 v0, v23

    if-ne v11, v0, :cond_1a

    if-nez v17, :cond_1a

    .line 2506
    int-to-char v0, v7

    move/from16 v17, v0

    .line 2507
    goto/16 :goto_0

    .line 2509
    :cond_1a
    const-string/jumbo v23, "\'&\' not after set"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    .line 2511
    :sswitch_6
    const-string/jumbo v23, "\'^\' not after \'[\'"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    .line 2513
    :sswitch_7
    if-eqz v17, :cond_1b

    .line 2514
    const-string/jumbo v23, "Missing operand after operator"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    .line 2516
    :cond_1b
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v11, v0, :cond_1c

    .line 2517
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v10}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 2518
    const/16 v23, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-static {v0, v10, v1}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;IZ)V

    .line 2520
    :cond_1c
    const/4 v11, 0x0

    .line 2521
    if-nez v6, :cond_20

    .line 2522
    new-instance v6, Ljava/lang/StringBuffer;

    .end local v6    # "buf":Ljava/lang/StringBuffer;
    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 2526
    .restart local v6    # "buf":Ljava/lang/StringBuffer;
    :goto_7
    const/16 v16, 0x0

    .line 2527
    .local v16, "ok":Z
    :goto_8
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->atEnd()Z

    move-result v23

    if-nez v23, :cond_1d

    .line 2528
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->next(I)I

    move-result v7

    .line 2529
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->isEscaped()Z

    move-result v12

    .line 2530
    const/16 v23, 0x7d

    move/from16 v0, v23

    if-ne v7, v0, :cond_21

    if-nez v12, :cond_21

    .line 2531
    const/16 v16, 0x1

    .line 2536
    :cond_1d
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->length()I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_1e

    if-nez v16, :cond_1f

    .line 2537
    :cond_1e
    const-string/jumbo v23, "Invalid multicharacter string"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    .line 2542
    :cond_1f
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    .line 2543
    const/16 v23, 0x7b

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2544
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;Ljava/lang/String;Z)V

    .line 2545
    const/16 v23, 0x7d

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 2524
    .end local v16    # "ok":Z
    :cond_20
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_7

    .line 2534
    .restart local v16    # "ok":Z
    :cond_21
    invoke-static {v6, v7}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_8

    .line 2554
    .end local v16    # "ok":Z
    :sswitch_8
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/ibm/icu/impl/RuleCharacterIterator;->getPos(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 2555
    .restart local v5    # "backup":Ljava/lang/Object;
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->next(I)I

    move-result v7

    .line 2556
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/impl/RuleCharacterIterator;->isEscaped()Z

    move-result v12

    .line 2557
    const/16 v23, 0x5d

    move/from16 v0, v23

    if-ne v7, v0, :cond_22

    if-nez v12, :cond_22

    const/4 v4, 0x1

    .line 2558
    .local v4, "anchor":Z
    :goto_9
    if-nez p2, :cond_23

    if-nez v4, :cond_23

    .line 2559
    const/16 v7, 0x24

    .line 2560
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/ibm/icu/impl/RuleCharacterIterator;->setPos(Ljava/lang/Object;)V

    goto/16 :goto_5

    .line 2557
    .end local v4    # "anchor":Z
    :cond_22
    const/4 v4, 0x0

    goto :goto_9

    .line 2563
    .restart local v4    # "anchor":Z
    :cond_23
    if-eqz v4, :cond_25

    if-nez v17, :cond_25

    .line 2564
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v11, v0, :cond_24

    .line 2565
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v10}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 2566
    const/16 v23, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-static {v0, v10, v1}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;IZ)V

    .line 2568
    :cond_24
    const v23, 0xffff

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 2569
    const/16 v22, 0x1

    .line 2570
    const/16 v23, 0x24

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v23

    const/16 v24, 0x5d

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2571
    const/4 v14, 0x2

    .line 2572
    goto/16 :goto_0

    .line 2574
    :cond_25
    const-string/jumbo v23, "Unquoted \'$\'"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 2590
    .end local v4    # "anchor":Z
    .end local v5    # "backup":Ljava/lang/Object;
    :pswitch_4
    const/16 v23, 0x2d

    move/from16 v0, v17

    move/from16 v1, v23

    if-ne v0, v1, :cond_27

    .line 2591
    if-lt v10, v7, :cond_26

    .line 2594
    const-string/jumbo v23, "Invalid range"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    .line 2596
    :cond_26
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v7}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 2597
    const/16 v23, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-static {v0, v10, v1}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;IZ)V

    .line 2598
    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2599
    const/16 v23, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-static {v0, v7, v1}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;IZ)V

    .line 2600
    const/16 v17, 0x0

    move/from16 v11, v17

    .line 2601
    goto/16 :goto_0

    .line 2602
    :cond_27
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v10}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 2603
    const/16 v23, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-static {v0, v10, v1}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;IZ)V

    .line 2604
    move v10, v7

    .line 2606
    goto/16 :goto_0

    .line 2608
    :pswitch_5
    if-eqz v17, :cond_28

    .line 2609
    const-string/jumbo v23, "Set expected after operator"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->syntaxError(Lcom/ibm/icu/impl/RuleCharacterIterator;Ljava/lang/String;)V

    .line 2611
    :cond_28
    move v10, v7

    .line 2612
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 2641
    .end local v7    # "c":I
    .end local v12    # "literal":Z
    .end local v15    # "nested":Lcom/ibm/icu/text/UnicodeSet;
    .end local v21    # "setMode":I
    :cond_29
    const/16 v23, 0x0

    const/16 v24, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/ibm/icu/text/UnicodeSet;->_generatePattern(Ljava/lang/StringBuffer;ZZ)Ljava/lang/StringBuffer;

    goto/16 :goto_3

    .line 2421
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 2443
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x26 -> :sswitch_1
        0x2d -> :sswitch_0
    .end sparse-switch

    .line 2470
    :sswitch_data_1
    .sparse-switch
        0x24 -> :sswitch_8
        0x26 -> :sswitch_5
        0x2d -> :sswitch_4
        0x5d -> :sswitch_3
        0x5e -> :sswitch_6
        0x7b -> :sswitch_7
    .end sparse-switch

    .line 2584
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public applyPropertyAlias(Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p1, "propertyAlias"    # Ljava/lang/String;
    .param p2, "valueAlias"    # Ljava/lang/String;

    .prologue
    .line 3167
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/ibm/icu/text/UnicodeSet;->applyPropertyAlias(Ljava/lang/String;Ljava/lang/String;Lcom/ibm/icu/text/SymbolTable;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method public applyPropertyAlias(Ljava/lang/String;Ljava/lang/String;Lcom/ibm/icu/text/SymbolTable;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 19
    .param p1, "propertyAlias"    # Ljava/lang/String;
    .param p2, "valueAlias"    # Ljava/lang/String;
    .param p3, "symbols"    # Lcom/ibm/icu/text/SymbolTable;

    .prologue
    .line 3183
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 3186
    const/4 v10, 0x0

    .local v10, "mustNotBeEmpty":Z
    const/4 v9, 0x0

    .line 3188
    .local v9, "invert":Z
    if-eqz p3, :cond_1

    move-object/from16 v0, p3

    instance-of v0, v0, Lcom/ibm/icu/text/UnicodeSet$XSymbolTable;

    move/from16 v16, v0

    if-eqz v16, :cond_1

    check-cast p3, Lcom/ibm/icu/text/UnicodeSet$XSymbolTable;

    .end local p3    # "symbols":Lcom/ibm/icu/text/SymbolTable;
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p0

    invoke-virtual {v0, v1, v2, v3}, Lcom/ibm/icu/text/UnicodeSet$XSymbolTable;->applyPropertyAlias(Ljava/lang/String;Ljava/lang/String;Lcom/ibm/icu/text/UnicodeSet;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 3323
    :cond_0
    :goto_0
    return-object p0

    .line 3194
    :cond_1
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v16

    if-lez v16, :cond_e

    .line 3195
    invoke-static/range {p1 .. p1}, Lcom/ibm/icu/lang/UCharacter;->getPropertyEnum(Ljava/lang/String;)I

    move-result v11

    .line 3198
    .local v11, "p":I
    const/16 v16, 0x1005

    move/from16 v0, v16

    if-ne v11, v0, :cond_2

    .line 3199
    const/16 v11, 0x2000

    .line 3202
    :cond_2
    if-ltz v11, :cond_3

    const/16 v16, 0x31

    move/from16 v0, v16

    if-lt v11, v0, :cond_5

    :cond_3
    const/16 v16, 0x1000

    move/from16 v0, v16

    if-lt v11, v0, :cond_4

    const/16 v16, 0x1015

    move/from16 v0, v16

    if-lt v11, v0, :cond_5

    :cond_4
    const/16 v16, 0x2000

    move/from16 v0, v16

    if-lt v11, v0, :cond_b

    const/16 v16, 0x2001

    move/from16 v0, v16

    if-ge v11, v0, :cond_b

    .line 3206
    :cond_5
    :try_start_0
    move-object/from16 v0, p2

    invoke-static {v11, v0}, Lcom/ibm/icu/lang/UCharacter;->getPropertyValueEnum(ILjava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v12

    .line 3312
    .local v12, "v":I
    :cond_6
    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12}, Lcom/ibm/icu/text/UnicodeSet;->applyIntPropertyValue(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 3313
    if-eqz v9, :cond_7

    .line 3314
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/UnicodeSet;->complement()Lcom/ibm/icu/text/UnicodeSet;

    .line 3317
    :cond_7
    if-eqz v10, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/UnicodeSet;->isEmpty()Z

    move-result v16

    if-eqz v16, :cond_0

    .line 3320
    new-instance v16, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v17, "Invalid property value"

    invoke-direct/range {v16 .. v17}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 3207
    .end local v12    # "v":I
    :catch_0
    move-exception v6

    .line 3209
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    const/16 v16, 0x1002

    move/from16 v0, v16

    if-eq v11, v0, :cond_8

    const/16 v16, 0x1010

    move/from16 v0, v16

    if-eq v11, v0, :cond_8

    const/16 v16, 0x1011

    move/from16 v0, v16

    if-ne v11, v0, :cond_a

    .line 3212
    :cond_8
    invoke-static/range {p2 .. p2}, Lcom/ibm/icu/impl/Utility;->deleteRuleWhiteSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 3217
    .restart local v12    # "v":I
    if-ltz v12, :cond_9

    const/16 v16, 0xff

    move/from16 v0, v16

    if-le v12, v0, :cond_6

    :cond_9
    throw v6

    .line 3219
    .end local v12    # "v":I
    :cond_a
    throw v6

    .line 3226
    .end local v6    # "e":Ljava/lang/IllegalArgumentException;
    :cond_b
    sparse-switch v11, :sswitch_data_0

    .line 3264
    new-instance v16, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v17, "Unsupported property"

    invoke-direct/range {v16 .. v17}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 3229
    :sswitch_0
    invoke-static/range {p2 .. p2}, Lcom/ibm/icu/impl/Utility;->deleteRuleWhiteSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v14

    .line 3230
    .local v14, "value":D
    new-instance v16, Lcom/ibm/icu/text/UnicodeSet$NumericValueFilter;

    move-object/from16 v0, v16

    invoke-direct {v0, v14, v15}, Lcom/ibm/icu/text/UnicodeSet$NumericValueFilter;-><init>(D)V

    const/16 v17, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->applyFilter(Lcom/ibm/icu/text/UnicodeSet$Filter;I)Lcom/ibm/icu/text/UnicodeSet;

    goto/16 :goto_0

    .line 3239
    .end local v14    # "value":D
    :sswitch_1
    invoke-static/range {p2 .. p2}, Lcom/ibm/icu/text/UnicodeSet;->mungeCharName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3240
    .local v4, "buf":Ljava/lang/String;
    const/16 v16, 0x4005

    move/from16 v0, v16

    if-ne v11, v0, :cond_c

    invoke-static {v4}, Lcom/ibm/icu/lang/UCharacter;->getCharFromExtendedName(Ljava/lang/String;)I

    move-result v5

    .line 3244
    .local v5, "ch":I
    :goto_2
    const/16 v16, -0x1

    move/from16 v0, v16

    if-ne v5, v0, :cond_d

    .line 3245
    new-instance v16, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v17, "Invalid character name"

    invoke-direct/range {v16 .. v17}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 3240
    .end local v5    # "ch":I
    :cond_c
    invoke-static {v4}, Lcom/ibm/icu/lang/UCharacter;->getCharFromName1_0(Ljava/lang/String;)I

    move-result v5

    goto :goto_2

    .line 3247
    .restart local v5    # "ch":I
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/UnicodeSet;->clear()Lcom/ibm/icu/text/UnicodeSet;

    .line 3248
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/ibm/icu/text/UnicodeSet;->add_unchecked(I)Lcom/ibm/icu/text/UnicodeSet;

    goto/16 :goto_0

    .line 3256
    .end local v4    # "buf":Ljava/lang/String;
    .end local v5    # "ch":I
    :sswitch_2
    invoke-static/range {p2 .. p2}, Lcom/ibm/icu/text/UnicodeSet;->mungeCharName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/ibm/icu/util/VersionInfo;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v13

    .line 3257
    .local v13, "version":Lcom/ibm/icu/util/VersionInfo;
    new-instance v16, Lcom/ibm/icu/text/UnicodeSet$VersionFilter;

    move-object/from16 v0, v16

    invoke-direct {v0, v13}, Lcom/ibm/icu/text/UnicodeSet$VersionFilter;-><init>(Lcom/ibm/icu/util/VersionInfo;)V

    const/16 v17, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->applyFilter(Lcom/ibm/icu/text/UnicodeSet$Filter;I)Lcom/ibm/icu/text/UnicodeSet;

    goto/16 :goto_0

    .line 3273
    .end local v11    # "p":I
    .end local v13    # "version":Lcom/ibm/icu/util/VersionInfo;
    :cond_e
    const/16 v11, 0x2000

    .line 3274
    .restart local v11    # "p":I
    :try_start_1
    move-object/from16 v0, p1

    invoke-static {v11, v0}, Lcom/ibm/icu/lang/UCharacter;->getPropertyValueEnum(ILjava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v12

    .restart local v12    # "v":I
    goto/16 :goto_1

    .line 3275
    .end local v12    # "v":I
    :catch_1
    move-exception v6

    .line 3277
    .restart local v6    # "e":Ljava/lang/IllegalArgumentException;
    const/16 v11, 0x100a

    .line 3278
    :try_start_2
    move-object/from16 v0, p1

    invoke-static {v11, v0}, Lcom/ibm/icu/lang/UCharacter;->getPropertyValueEnum(ILjava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v12

    .restart local v12    # "v":I
    goto/16 :goto_1

    .line 3279
    .end local v12    # "v":I
    :catch_2
    move-exception v7

    .line 3281
    .local v7, "e2":Ljava/lang/IllegalArgumentException;
    :try_start_3
    invoke-static/range {p1 .. p1}, Lcom/ibm/icu/lang/UCharacter;->getPropertyEnum(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    move-result v11

    .line 3285
    :goto_3
    if-ltz v11, :cond_f

    const/16 v16, 0x31

    move/from16 v0, v16

    if-ge v11, v0, :cond_f

    .line 3286
    const/4 v12, 0x1

    .line 3287
    .restart local v12    # "v":I
    goto/16 :goto_1

    .line 3282
    .end local v12    # "v":I
    :catch_3
    move-exception v8

    .line 3283
    .local v8, "e3":Ljava/lang/IllegalArgumentException;
    const/4 v11, -0x1

    goto :goto_3

    .line 3287
    .end local v8    # "e3":Ljava/lang/IllegalArgumentException;
    :cond_f
    const/16 v16, -0x1

    move/from16 v0, v16

    if-ne v11, v0, :cond_13

    .line 3288
    const-string/jumbo v16, "ANY"

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/UPropertyAliases;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v16

    if-nez v16, :cond_10

    .line 3289
    const/16 v16, 0x0

    const v17, 0x10ffff

    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->set(II)Lcom/ibm/icu/text/UnicodeSet;

    goto/16 :goto_0

    .line 3291
    :cond_10
    const-string/jumbo v16, "ASCII"

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/UPropertyAliases;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v16

    if-nez v16, :cond_11

    .line 3292
    const/16 v16, 0x0

    const/16 v17, 0x7f

    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->set(II)Lcom/ibm/icu/text/UnicodeSet;

    goto/16 :goto_0

    .line 3294
    :cond_11
    const-string/jumbo v16, "Assigned"

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/UPropertyAliases;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v16

    if-nez v16, :cond_12

    .line 3296
    const/16 v11, 0x2000

    .line 3297
    const/4 v12, 0x1

    .line 3298
    .restart local v12    # "v":I
    const/4 v9, 0x1

    .line 3299
    goto/16 :goto_1

    .line 3301
    .end local v12    # "v":I
    :cond_12
    new-instance v16, Ljava/lang/IllegalArgumentException;

    new-instance v17, Ljava/lang/StringBuffer;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v18, "Invalid property alias: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    const-string/jumbo v18, "="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 3306
    :cond_13
    new-instance v16, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v17, "Missing property value"

    invoke-direct/range {v16 .. v17}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 3226
    :sswitch_data_0
    .sparse-switch
        0x3000 -> :sswitch_0
        0x4000 -> :sswitch_2
        0x4005 -> :sswitch_1
        0x400b -> :sswitch_1
    .end sparse-switch
.end method

.method public charAt(I)I
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 1025
    if-ltz p1, :cond_1

    .line 1029
    iget v5, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    and-int/lit8 v3, v5, -0x2

    .line 1030
    .local v3, "len2":I
    const/4 v1, 0x0

    .local v1, "i":I
    move v2, v1

    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 1031
    iget-object v5, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    aget v4, v5, v2

    .line 1032
    .local v4, "start":I
    iget-object v5, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    aget v5, v5, v1

    sub-int v0, v5, v4

    .line 1033
    .local v0, "count":I
    if-ge p1, v0, :cond_0

    .line 1034
    add-int v5, v4, p1

    .line 1039
    .end local v0    # "count":I
    .end local v2    # "i":I
    .end local v3    # "len2":I
    .end local v4    # "start":I
    :goto_1
    return v5

    .line 1036
    .restart local v0    # "count":I
    .restart local v2    # "i":I
    .restart local v3    # "len2":I
    .restart local v4    # "start":I
    :cond_0
    sub-int/2addr p1, v0

    .line 1037
    goto :goto_0

    .line 1039
    .end local v0    # "count":I
    .end local v2    # "i":I
    .end local v3    # "len2":I
    .end local v4    # "start":I
    :cond_1
    const/4 v5, -0x1

    goto :goto_1
.end method

.method public clear()Lcom/ibm/icu/text/UnicodeSet;
    .locals 3

    .prologue
    .line 2107
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 2108
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    const/4 v1, 0x0

    const/high16 v2, 0x110000

    aput v2, v0, v1

    .line 2109
    const/4 v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    .line 2110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    .line 2111
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->clear()V

    .line 2112
    return-object p0
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 441
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Lcom/ibm/icu/text/UnicodeSet;)V

    .line 442
    .local v0, "result":Lcom/ibm/icu/text/UnicodeSet;
    iget-boolean v1, p0, Lcom/ibm/icu/text/UnicodeSet;->frozen:Z

    iput-boolean v1, v0, Lcom/ibm/icu/text/UnicodeSet;->frozen:Z

    .line 443
    return-object v0
.end method

.method public cloneAsThawed()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 3744
    invoke-virtual {p0}, Lcom/ibm/icu/text/UnicodeSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    .line 3745
    .local v0, "result":Lcom/ibm/icu/text/UnicodeSet;
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/ibm/icu/text/UnicodeSet;->frozen:Z

    .line 3746
    return-object v0
.end method

.method public closeOver(I)Lcom/ibm/icu/text/UnicodeSet;
    .locals 18
    .param p1, "attribute"    # I

    .prologue
    .line 3588
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 3589
    and-int/lit8 v3, p1, 0x6

    if-eqz v3, :cond_1

    .line 3592
    :try_start_0
    invoke-static {}, Lcom/ibm/icu/impl/UCaseProps;->getSingleton()Lcom/ibm/icu/impl/UCaseProps;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3596
    .local v1, "csp":Lcom/ibm/icu/impl/UCaseProps;
    new-instance v10, Lcom/ibm/icu/text/UnicodeSet;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Lcom/ibm/icu/text/UnicodeSet;)V

    .line 3597
    .local v10, "foldSet":Lcom/ibm/icu/text/UnicodeSet;
    sget-object v5, Lcom/ibm/icu/util/ULocale;->ROOT:Lcom/ibm/icu/util/ULocale;

    .line 3602
    .local v5, "root":Lcom/ibm/icu/util/ULocale;
    and-int/lit8 v3, p1, 0x2

    if-eqz v3, :cond_0

    .line 3603
    iget-object v3, v10, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v3}, Ljava/util/TreeSet;->clear()V

    .line 3606
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/UnicodeSet;->getRangeCount()I

    move-result v13

    .line 3608
    .local v13, "n":I
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 3609
    .local v4, "full":Ljava/lang/StringBuffer;
    const/4 v3, 0x1

    new-array v6, v3, [I

    .line 3611
    .local v6, "locCache":[I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    if-ge v11, v13, :cond_4

    .line 3612
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/ibm/icu/text/UnicodeSet;->getRangeStart(I)I

    move-result v15

    .line 3613
    .local v15, "start":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/ibm/icu/text/UnicodeSet;->getRangeEnd(I)I

    move-result v9

    .line 3615
    .local v9, "end":I
    and-int/lit8 v3, p1, 0x2

    if-eqz v3, :cond_2

    .line 3617
    move v2, v15

    .local v2, "cp":I
    :goto_1
    if-gt v2, v9, :cond_3

    .line 3618
    invoke-virtual {v1, v2, v10}, Lcom/ibm/icu/impl/UCaseProps;->addCaseClosure(ILcom/ibm/icu/text/UnicodeSet;)V

    .line 3617
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3593
    .end local v1    # "csp":Lcom/ibm/icu/impl/UCaseProps;
    .end local v2    # "cp":I
    .end local v4    # "full":Ljava/lang/StringBuffer;
    .end local v5    # "root":Lcom/ibm/icu/util/ULocale;
    .end local v6    # "locCache":[I
    .end local v9    # "end":I
    .end local v10    # "foldSet":Lcom/ibm/icu/text/UnicodeSet;
    .end local v11    # "i":I
    .end local v13    # "n":I
    .end local v15    # "start":I
    :catch_0
    move-exception v8

    .line 3662
    :cond_1
    :goto_2
    return-object p0

    .line 3623
    .restart local v1    # "csp":Lcom/ibm/icu/impl/UCaseProps;
    .restart local v4    # "full":Ljava/lang/StringBuffer;
    .restart local v5    # "root":Lcom/ibm/icu/util/ULocale;
    .restart local v6    # "locCache":[I
    .restart local v9    # "end":I
    .restart local v10    # "foldSet":Lcom/ibm/icu/text/UnicodeSet;
    .restart local v11    # "i":I
    .restart local v13    # "n":I
    .restart local v15    # "start":I
    :cond_2
    move v2, v15

    .restart local v2    # "cp":I
    :goto_3
    if-gt v2, v9, :cond_3

    .line 3624
    const/4 v3, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/ibm/icu/impl/UCaseProps;->toFullLower(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[I)I

    move-result v14

    .line 3625
    .local v14, "result":I
    invoke-static {v10, v14, v4}, Lcom/ibm/icu/text/UnicodeSet;->addCaseMapping(Lcom/ibm/icu/text/UnicodeSet;ILjava/lang/StringBuffer;)V

    .line 3627
    const/4 v3, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/ibm/icu/impl/UCaseProps;->toFullTitle(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[I)I

    move-result v14

    .line 3628
    invoke-static {v10, v14, v4}, Lcom/ibm/icu/text/UnicodeSet;->addCaseMapping(Lcom/ibm/icu/text/UnicodeSet;ILjava/lang/StringBuffer;)V

    .line 3630
    const/4 v3, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/ibm/icu/impl/UCaseProps;->toFullUpper(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[I)I

    move-result v14

    .line 3631
    invoke-static {v10, v14, v4}, Lcom/ibm/icu/text/UnicodeSet;->addCaseMapping(Lcom/ibm/icu/text/UnicodeSet;ILjava/lang/StringBuffer;)V

    .line 3633
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v3}, Lcom/ibm/icu/impl/UCaseProps;->toFullFolding(ILjava/lang/StringBuffer;I)I

    move-result v14

    .line 3634
    invoke-static {v10, v14, v4}, Lcom/ibm/icu/text/UnicodeSet;->addCaseMapping(Lcom/ibm/icu/text/UnicodeSet;ILjava/lang/StringBuffer;)V

    .line 3623
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 3611
    .end local v14    # "result":I
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 3638
    .end local v2    # "cp":I
    .end local v9    # "end":I
    .end local v15    # "start":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v3}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    .line 3640
    and-int/lit8 v3, p1, 0x2

    if-eqz v3, :cond_6

    .line 3641
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v3}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 3642
    .local v12, "it":Ljava/util/Iterator;
    :cond_5
    :goto_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 3643
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v3, v0}, Lcom/ibm/icu/lang/UCharacter;->foldCase(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v16

    .line 3644
    .local v16, "str":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v1, v0, v10}, Lcom/ibm/icu/impl/UCaseProps;->addStringCaseClosure(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeSet;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 3645
    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_4

    .line 3649
    .end local v12    # "it":Ljava/util/Iterator;
    .end local v16    # "str":Ljava/lang/String;
    :cond_6
    invoke-static {v5}, Lcom/ibm/icu/text/BreakIterator;->getWordInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v7

    .line 3650
    .local v7, "bi":Lcom/ibm/icu/text/BreakIterator;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v3}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 3651
    .restart local v12    # "it":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 3652
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 3653
    .restart local v16    # "str":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-static {v5, v0}, Lcom/ibm/icu/lang/UCharacter;->toLowerCase(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    .line 3654
    move-object/from16 v0, v16

    invoke-static {v5, v0, v7}, Lcom/ibm/icu/lang/UCharacter;->toTitleCase(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;Lcom/ibm/icu/text/BreakIterator;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    .line 3655
    move-object/from16 v0, v16

    invoke-static {v5, v0}, Lcom/ibm/icu/lang/UCharacter;->toUpperCase(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    .line 3656
    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lcom/ibm/icu/lang/UCharacter;->foldCase(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_5

    .line 3660
    .end local v7    # "bi":Lcom/ibm/icu/text/BreakIterator;
    .end local v12    # "it":Ljava/util/Iterator;
    .end local v16    # "str":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/ibm/icu/text/UnicodeSet;->set(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    goto/16 :goto_2
.end method

.method public compact()Lcom/ibm/icu/text/UnicodeSet;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2158
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 2159
    iget v1, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    array-length v2, v2

    if-eq v1, v2, :cond_0

    .line 2160
    iget v1, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    new-array v0, v1, [I

    .line 2161
    .local v0, "temp":[I
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    iget v2, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2162
    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    .line 2164
    .end local v0    # "temp":[I
    :cond_0
    iput-object v4, p0, Lcom/ibm/icu/text/UnicodeSet;->rangeList:[I

    .line 2165
    iput-object v4, p0, Lcom/ibm/icu/text/UnicodeSet;->buffer:[I

    .line 2166
    return-object p0
.end method

.method public complement()Lcom/ibm/icu/text/UnicodeSet;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1468
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 1469
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aget v0, v0, v3

    if-nez v0, :cond_0

    .line 1470
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    iget v2, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v4, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1471
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    .line 1478
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    .line 1479
    return-object p0

    .line 1473
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/UnicodeSet;->ensureCapacity(I)V

    .line 1474
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    iget v2, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    invoke-static {v0, v3, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1475
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aput v3, v0, v3

    .line 1476
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    goto :goto_0
.end method

.method public final complement(I)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 1459
    invoke-virtual {p0, p1, p1}, Lcom/ibm/icu/text/UnicodeSet;->complement(II)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method public complement(II)Lcom/ibm/icu/text/UnicodeSet;
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const v0, 0x10ffff

    const/4 v3, 0x6

    .line 1438
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 1439
    if-ltz p1, :cond_0

    if-le p1, v0, :cond_1

    .line 1440
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Invalid code point U+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p1, v3}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1442
    :cond_1
    if-ltz p2, :cond_2

    if-le p2, v0, :cond_3

    .line 1443
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Invalid code point U+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p2, v3}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1445
    :cond_3
    if-gt p1, p2, :cond_4

    .line 1446
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/UnicodeSet;->range(II)[I

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->xor([III)Lcom/ibm/icu/text/UnicodeSet;

    .line 1448
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    .line 1449
    return-object p0
.end method

.method public final complement(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1492
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 1493
    invoke-static {p1}, Lcom/ibm/icu/text/UnicodeSet;->getSingleCP(Ljava/lang/String;)I

    move-result v0

    .line 1494
    .local v0, "cp":I
    if-gez v0, :cond_1

    .line 1495
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v1, p1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v1, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 1497
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    .line 1501
    :goto_1
    return-object p0

    .line 1496
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v1, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1499
    :cond_1
    invoke-virtual {p0, v0, v0}, Lcom/ibm/icu/text/UnicodeSet;->complement(II)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_1
.end method

.method public complementAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 3
    .param p1, "c"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 2095
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 2096
    iget-object v0, p1, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    iget v1, p1, Lcom/ibm/icu/text/UnicodeSet;->len:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->xor([III)Lcom/ibm/icu/text/UnicodeSet;

    .line 2097
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/SortedSetRelation;->doOperation(Ljava/util/SortedSet;ILjava/util/SortedSet;)Ljava/util/SortedSet;

    .line 2098
    return-object p0
.end method

.method public final complementAll(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1268
    invoke-static {p1}, Lcom/ibm/icu/text/UnicodeSet;->fromAll(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeSet;->complementAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method public contains(I)Z
    .locals 4
    .param p1, "c"    # I

    .prologue
    .line 1511
    if-ltz p1, :cond_0

    const v1, 0x10ffff

    if-le p1, v1, :cond_1

    .line 1512
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Invalid code point U+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const/4 v3, 0x6

    invoke-static {p1, v3}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1524
    :cond_1
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/UnicodeSet;->findCodePoint(I)I

    move-result v0

    .line 1526
    .local v0, "i":I
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public contains(II)Z
    .locals 5
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const v1, 0x10ffff

    const/4 v4, 0x6

    .line 1694
    if-ltz p1, :cond_0

    if-le p1, v1, :cond_1

    .line 1695
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Invalid code point U+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-static {p1, v4}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1697
    :cond_1
    if-ltz p2, :cond_2

    if-le p2, v1, :cond_3

    .line 1698
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Invalid code point U+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-static {p2, v4}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1704
    :cond_3
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/UnicodeSet;->findCodePoint(I)I

    move-result v0

    .line 1705
    .local v0, "i":I
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aget v1, v1, v0

    if-ge p2, v1, :cond_4

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final contains(Ljava/lang/String;)Z
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1717
    invoke-static {p1}, Lcom/ibm/icu/text/UnicodeSet;->getSingleCP(Ljava/lang/String;)I

    move-result v0

    .line 1718
    .local v0, "cp":I
    if-gez v0, :cond_0

    .line 1719
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v1, p1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 1721
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v1

    goto :goto_0
.end method

.method public containsAll(Lcom/ibm/icu/text/UnicodeSet;)Z
    .locals 16
    .param p1, "b"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 1737
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    .line 1738
    .local v9, "listB":[I
    const/4 v10, 0x1

    .line 1739
    .local v10, "needA":Z
    const/4 v11, 0x1

    .line 1740
    .local v11, "needB":Z
    const/4 v2, 0x0

    .line 1741
    .local v2, "aPtr":I
    const/4 v5, 0x0

    .line 1742
    .local v5, "bPtr":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v1, v14, -0x1

    .line 1743
    .local v1, "aLen":I
    move-object/from16 v0, p1

    iget v14, v0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v4, v14, -0x1

    .line 1744
    .local v4, "bLen":I
    const/4 v12, 0x0

    .local v12, "startA":I
    const/4 v13, 0x0

    .local v13, "startB":I
    const/4 v7, 0x0

    .local v7, "limitA":I
    const/4 v8, 0x0

    .local v8, "limitB":I
    move v6, v5

    .end local v5    # "bPtr":I
    .local v6, "bPtr":I
    move v3, v2

    .line 1747
    .end local v2    # "aPtr":I
    .local v3, "aPtr":I
    :goto_0
    if-eqz v10, :cond_7

    .line 1748
    if-lt v3, v1, :cond_2

    .line 1750
    if-eqz v11, :cond_1

    if-lt v6, v4, :cond_1

    move v2, v3

    .line 1782
    .end local v3    # "aPtr":I
    .restart local v2    # "aPtr":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v14, v15}, Ljava/util/TreeSet;->containsAll(Ljava/util/Collection;)Z

    move-result v14

    if-nez v14, :cond_5

    const/4 v14, 0x0

    move v5, v6

    .line 1783
    .end local v6    # "bPtr":I
    .restart local v5    # "bPtr":I
    :goto_1
    return v14

    .line 1753
    .end local v2    # "aPtr":I
    .end local v5    # "bPtr":I
    .restart local v3    # "aPtr":I
    .restart local v6    # "bPtr":I
    :cond_1
    const/4 v14, 0x0

    move v5, v6

    .end local v6    # "bPtr":I
    .restart local v5    # "bPtr":I
    move v2, v3

    .end local v3    # "aPtr":I
    .restart local v2    # "aPtr":I
    goto :goto_1

    .line 1755
    .end local v2    # "aPtr":I
    .end local v5    # "bPtr":I
    .restart local v3    # "aPtr":I
    .restart local v6    # "bPtr":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "aPtr":I
    .restart local v2    # "aPtr":I
    aget v12, v14, v3

    .line 1756
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "aPtr":I
    .restart local v3    # "aPtr":I
    aget v7, v14, v2

    move v2, v3

    .line 1758
    .end local v3    # "aPtr":I
    .restart local v2    # "aPtr":I
    :goto_2
    if-eqz v11, :cond_6

    .line 1759
    if-ge v6, v4, :cond_0

    .line 1763
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "bPtr":I
    .restart local v5    # "bPtr":I
    aget v13, v9, v6

    .line 1764
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "bPtr":I
    .restart local v6    # "bPtr":I
    aget v8, v9, v5

    move v5, v6

    .line 1767
    .end local v6    # "bPtr":I
    .restart local v5    # "bPtr":I
    :goto_3
    if-lt v13, v7, :cond_3

    .line 1768
    const/4 v10, 0x1

    .line 1769
    const/4 v11, 0x0

    move v6, v5

    .end local v5    # "bPtr":I
    .restart local v6    # "bPtr":I
    move v3, v2

    .line 1770
    .end local v2    # "aPtr":I
    .restart local v3    # "aPtr":I
    goto :goto_0

    .line 1773
    .end local v3    # "aPtr":I
    .end local v6    # "bPtr":I
    .restart local v2    # "aPtr":I
    .restart local v5    # "bPtr":I
    :cond_3
    if-lt v13, v12, :cond_4

    if-gt v8, v7, :cond_4

    .line 1774
    const/4 v10, 0x0

    .line 1775
    const/4 v11, 0x1

    move v6, v5

    .end local v5    # "bPtr":I
    .restart local v6    # "bPtr":I
    move v3, v2

    .line 1776
    .end local v2    # "aPtr":I
    .restart local v3    # "aPtr":I
    goto :goto_0

    .line 1779
    .end local v3    # "aPtr":I
    .end local v6    # "bPtr":I
    .restart local v2    # "aPtr":I
    .restart local v5    # "bPtr":I
    :cond_4
    const/4 v14, 0x0

    goto :goto_1

    .line 1783
    .end local v5    # "bPtr":I
    .restart local v6    # "bPtr":I
    :cond_5
    const/4 v14, 0x1

    move v5, v6

    .end local v6    # "bPtr":I
    .restart local v5    # "bPtr":I
    goto :goto_1

    .end local v5    # "bPtr":I
    .restart local v6    # "bPtr":I
    :cond_6
    move v5, v6

    .end local v6    # "bPtr":I
    .restart local v5    # "bPtr":I
    goto :goto_3

    .end local v2    # "aPtr":I
    .end local v5    # "bPtr":I
    .restart local v3    # "aPtr":I
    .restart local v6    # "bPtr":I
    :cond_7
    move v2, v3

    .end local v3    # "aPtr":I
    .restart local v2    # "aPtr":I
    goto :goto_2
.end method

.method public containsAll(Ljava/lang/String;)Z
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1818
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 1819
    invoke-static {p1, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 1820
    .local v0, "cp":I
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1821
    iget-object v3, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v3}, Ljava/util/TreeSet;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 1827
    .end local v0    # "cp":I
    :goto_1
    return v2

    .line 1824
    .restart local v0    # "cp":I
    :cond_0
    invoke-direct {p0, p1, v2}, Lcom/ibm/icu/text/UnicodeSet;->containsAll(Ljava/lang/String;I)Z

    move-result v2

    goto :goto_1

    .line 1818
    :cond_1
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_0

    .line 1827
    .end local v0    # "cp":I
    :cond_2
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public containsNone(II)Z
    .locals 5
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const v1, 0x10ffff

    const/4 v4, 0x6

    .line 1883
    if-ltz p1, :cond_0

    if-le p1, v1, :cond_1

    .line 1884
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Invalid code point U+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-static {p1, v4}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1886
    :cond_1
    if-ltz p2, :cond_2

    if-le p2, v1, :cond_3

    .line 1887
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Invalid code point U+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-static {p2, v4}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1889
    :cond_3
    const/4 v0, -0x1

    .line 1891
    .local v0, "i":I
    :cond_4
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v0, v0, 0x1

    aget v1, v1, v0

    if-ge p1, v1, :cond_4

    .line 1893
    and-int/lit8 v1, v0, 0x1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aget v1, v1, v0

    if-ge p2, v1, :cond_5

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public containsNone(Lcom/ibm/icu/text/UnicodeSet;)Z
    .locals 17
    .param p1, "b"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 1908
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    .line 1909
    .local v9, "listB":[I
    const/4 v10, 0x1

    .line 1910
    .local v10, "needA":Z
    const/4 v11, 0x1

    .line 1911
    .local v11, "needB":Z
    const/4 v2, 0x0

    .line 1912
    .local v2, "aPtr":I
    const/4 v5, 0x0

    .line 1913
    .local v5, "bPtr":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v1, v14, -0x1

    .line 1914
    .local v1, "aLen":I
    move-object/from16 v0, p1

    iget v14, v0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    add-int/lit8 v4, v14, -0x1

    .line 1915
    .local v4, "bLen":I
    const/4 v12, 0x0

    .local v12, "startA":I
    const/4 v13, 0x0

    .local v13, "startB":I
    const/4 v7, 0x0

    .local v7, "limitA":I
    const/4 v8, 0x0

    .local v8, "limitB":I
    move v6, v5

    .end local v5    # "bPtr":I
    .local v6, "bPtr":I
    move v3, v2

    .line 1918
    .end local v2    # "aPtr":I
    .local v3, "aPtr":I
    :goto_0
    if-eqz v10, :cond_6

    .line 1919
    if-lt v3, v1, :cond_1

    move v2, v3

    .line 1950
    .end local v3    # "aPtr":I
    .restart local v2    # "aPtr":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    const/4 v15, 0x5

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    move-object/from16 v16, v0

    invoke-static/range {v14 .. v16}, Lcom/ibm/icu/impl/SortedSetRelation;->hasRelation(Ljava/util/SortedSet;ILjava/util/SortedSet;)Z

    move-result v14

    if-nez v14, :cond_4

    const/4 v14, 0x0

    move v5, v6

    .line 1951
    .end local v6    # "bPtr":I
    .restart local v5    # "bPtr":I
    :goto_1
    return v14

    .line 1923
    .end local v2    # "aPtr":I
    .end local v5    # "bPtr":I
    .restart local v3    # "aPtr":I
    .restart local v6    # "bPtr":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "aPtr":I
    .restart local v2    # "aPtr":I
    aget v12, v14, v3

    .line 1924
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "aPtr":I
    .restart local v3    # "aPtr":I
    aget v7, v14, v2

    move v2, v3

    .line 1926
    .end local v3    # "aPtr":I
    .restart local v2    # "aPtr":I
    :goto_2
    if-eqz v11, :cond_5

    .line 1927
    if-ge v6, v4, :cond_0

    .line 1931
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "bPtr":I
    .restart local v5    # "bPtr":I
    aget v13, v9, v6

    .line 1932
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "bPtr":I
    .restart local v6    # "bPtr":I
    aget v8, v9, v5

    move v5, v6

    .line 1935
    .end local v6    # "bPtr":I
    .restart local v5    # "bPtr":I
    :goto_3
    if-lt v13, v7, :cond_2

    .line 1936
    const/4 v10, 0x1

    .line 1937
    const/4 v11, 0x0

    move v6, v5

    .end local v5    # "bPtr":I
    .restart local v6    # "bPtr":I
    move v3, v2

    .line 1938
    .end local v2    # "aPtr":I
    .restart local v3    # "aPtr":I
    goto :goto_0

    .line 1941
    .end local v3    # "aPtr":I
    .end local v6    # "bPtr":I
    .restart local v2    # "aPtr":I
    .restart local v5    # "bPtr":I
    :cond_2
    if-lt v12, v8, :cond_3

    .line 1942
    const/4 v10, 0x0

    .line 1943
    const/4 v11, 0x1

    move v6, v5

    .end local v5    # "bPtr":I
    .restart local v6    # "bPtr":I
    move v3, v2

    .line 1944
    .end local v2    # "aPtr":I
    .restart local v3    # "aPtr":I
    goto :goto_0

    .line 1947
    .end local v3    # "aPtr":I
    .end local v6    # "bPtr":I
    .restart local v2    # "aPtr":I
    .restart local v5    # "bPtr":I
    :cond_3
    const/4 v14, 0x0

    goto :goto_1

    .line 1951
    .end local v5    # "bPtr":I
    .restart local v6    # "bPtr":I
    :cond_4
    const/4 v14, 0x1

    move v5, v6

    .end local v6    # "bPtr":I
    .restart local v5    # "bPtr":I
    goto :goto_1

    .end local v5    # "bPtr":I
    .restart local v6    # "bPtr":I
    :cond_5
    move v5, v6

    .end local v6    # "bPtr":I
    .restart local v5    # "bPtr":I
    goto :goto_3

    .end local v2    # "aPtr":I
    .end local v5    # "bPtr":I
    .restart local v3    # "aPtr":I
    .restart local v6    # "bPtr":I
    :cond_6
    move v2, v3

    .end local v3    # "aPtr":I
    .restart local v2    # "aPtr":I
    goto :goto_2
.end method

.method public containsNone(Ljava/lang/String;)Z
    .locals 7
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1986
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v1, v6, :cond_1

    .line 1987
    invoke-static {p1, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 1988
    .local v0, "cp":I
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1996
    .end local v0    # "cp":I
    :goto_1
    return v4

    .line 1986
    .restart local v0    # "cp":I
    :cond_0
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v6

    add-int/2addr v1, v6

    goto :goto_0

    .line 1990
    .end local v0    # "cp":I
    :cond_1
    iget-object v6, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v6}, Ljava/util/TreeSet;->size()I

    move-result v6

    if-nez v6, :cond_2

    move v4, v5

    goto :goto_1

    .line 1992
    :cond_2
    iget-object v6, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v6}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "it":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1993
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1994
    .local v3, "item":Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_3

    goto :goto_1

    .end local v3    # "item":Ljava/lang/String;
    :cond_4
    move v4, v5

    .line 1996
    goto :goto_1
.end method

.method public final containsSome(II)Z
    .locals 1
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 2008
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/UnicodeSet;->containsNone(II)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final containsSome(Lcom/ibm/icu/text/UnicodeSet;)Z
    .locals 1
    .param p1, "s"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 2019
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/UnicodeSet;->containsNone(Lcom/ibm/icu/text/UnicodeSet;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final containsSome(Ljava/lang/String;)Z
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 2030
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/UnicodeSet;->containsNone(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 2182
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/ibm/icu/text/UnicodeSet;

    move-object v3, v0

    .line 2183
    .local v3, "that":Lcom/ibm/icu/text/UnicodeSet;
    iget v5, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    iget v6, v3, Lcom/ibm/icu/text/UnicodeSet;->len:I

    if-eq v5, v6, :cond_1

    .line 2191
    .end local v3    # "that":Lcom/ibm/icu/text/UnicodeSet;
    :cond_0
    :goto_0
    return v4

    .line 2184
    .restart local v3    # "that":Lcom/ibm/icu/text/UnicodeSet;
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget v5, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    if-ge v2, v5, :cond_2

    .line 2185
    iget-object v5, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aget v5, v5, v2

    iget-object v6, v3, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aget v6, v6, v2

    if-ne v5, v6, :cond_0

    .line 2184
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2187
    :cond_2
    iget-object v5, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    iget-object v6, v3, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v5, v6}, Ljava/util/TreeSet;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_0

    .line 2191
    const/4 v4, 0x1

    goto :goto_0

    .line 2188
    .end local v2    # "i":I
    .end local v3    # "that":Lcom/ibm/icu/text/UnicodeSet;
    :catch_0
    move-exception v1

    .line 2189
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public freeze()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3734
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/UnicodeSet;->frozen:Z

    .line 3735
    return-object p0
.end method

.method public getRangeCount()I
    .locals 1

    .prologue
    .line 2123
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public getRangeEnd(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 2149
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x1

    aget v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getRangeStart(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 2136
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    mul-int/lit8 v1, p1, 0x2

    aget v0, v0, v1

    return v0
.end method

.method public getRegexEquivalent()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1863
    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v2}, Ljava/util/TreeSet;->size()I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/ibm/icu/text/UnicodeSet;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1871
    :goto_0
    return-object v2

    .line 1864
    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    const-string/jumbo v2, "(?:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1865
    .local v1, "result":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v3, v2}, Lcom/ibm/icu/text/UnicodeSet;->_generatePattern(Ljava/lang/StringBuffer;ZZ)Ljava/lang/StringBuffer;

    .line 1866
    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v2}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 1867
    .local v0, "it":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1868
    const/16 v2, 0x7c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1869
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/UnicodeSet;->_appendToPat(Ljava/lang/StringBuffer;Ljava/lang/String;Z)V

    goto :goto_1

    .line 1871
    :cond_1
    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 2202
    iget v1, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    .line 2203
    .local v1, "result":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    if-ge v0, v2, :cond_0

    .line 2204
    const v2, 0xf4243

    mul-int/2addr v1, v2

    .line 2205
    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    aget v2, v2, v0

    add-int/2addr v1, v2

    .line 2203
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2207
    :cond_0
    return v1
.end method

.method public indexOf(I)I
    .locals 8
    .param p1, "c"    # I

    .prologue
    .line 997
    if-ltz p1, :cond_0

    const v5, 0x10ffff

    if-le p1, v5, :cond_1

    .line 998
    :cond_0
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "Invalid code point U+"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const/4 v7, 0x6

    invoke-static {p1, v7}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1000
    :cond_1
    const/4 v0, 0x0

    .line 1001
    .local v0, "i":I
    const/4 v3, 0x0

    .line 1003
    .local v3, "n":I
    :goto_0
    iget-object v5, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aget v4, v5, v0

    .line 1004
    .local v4, "start":I
    if-ge p1, v4, :cond_2

    .line 1005
    const/4 v5, -0x1

    move v0, v1

    .line 1009
    .end local v1    # "i":I
    .restart local v0    # "i":I
    :goto_1
    return v5

    .line 1007
    .end local v0    # "i":I
    .restart local v1    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aget v2, v5, v1

    .line 1008
    .local v2, "limit":I
    if-ge p1, v2, :cond_3

    .line 1009
    add-int v5, v3, p1

    sub-int/2addr v5, v4

    goto :goto_1

    .line 1011
    :cond_3
    sub-int v5, v2, v4

    add-int/2addr v3, v5

    .line 1012
    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 737
    iget v1, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->size()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFrozen()Z
    .locals 1

    .prologue
    .line 3725
    iget-boolean v0, p0, Lcom/ibm/icu/text/UnicodeSet;->frozen:Z

    return v0
.end method

.method public matches(Lcom/ibm/icu/text/Replaceable;[IIZ)I
    .locals 10
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "offset"    # [I
    .param p3, "limit"    # I
    .param p4, "incremental"    # Z

    .prologue
    .line 795
    const/4 v8, 0x0

    aget v8, p2, v8

    if-ne v8, p3, :cond_2

    .line 799
    const v8, 0xffff

    invoke-virtual {p0, v8}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 800
    if-eqz p4, :cond_0

    const/4 v8, 0x1

    .line 872
    :goto_0
    return v8

    .line 800
    :cond_0
    const/4 v8, 0x2

    goto :goto_0

    .line 802
    :cond_1
    const/4 v8, 0x0

    goto :goto_0

    .line 805
    :cond_2
    iget-object v8, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v8}, Ljava/util/TreeSet;->size()I

    move-result v8

    if-eqz v8, :cond_c

    .line 814
    iget-object v8, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v8}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 815
    .local v4, "it":Ljava/util/Iterator;
    const/4 v8, 0x0

    aget v8, p2, v8

    if-ge v8, p3, :cond_5

    const/4 v2, 0x1

    .line 820
    .local v2, "forward":Z
    :goto_1
    const/4 v8, 0x0

    aget v8, p2, v8

    invoke-interface {p1, v8}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v1

    .line 824
    .local v1, "firstChar":C
    const/4 v3, 0x0

    .line 826
    .local v3, "highWaterLength":I
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 827
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 834
    .local v7, "trial":Ljava/lang/String;
    if-eqz v2, :cond_6

    const/4 v8, 0x0

    :goto_2
    invoke-virtual {v7, v8}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 838
    .local v0, "c":C
    if-eqz v2, :cond_7

    if-le v0, v1, :cond_7

    .line 867
    .end local v0    # "c":C
    .end local v7    # "trial":Ljava/lang/String;
    :cond_4
    :goto_3
    if-eqz v3, :cond_c

    .line 868
    const/4 v8, 0x0

    aget v9, p2, v8

    if-eqz v2, :cond_b

    .end local v3    # "highWaterLength":I
    :goto_4
    add-int/2addr v9, v3

    aput v9, p2, v8

    .line 869
    const/4 v8, 0x2

    goto :goto_0

    .line 815
    .end local v1    # "firstChar":C
    .end local v2    # "forward":Z
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    .line 834
    .restart local v1    # "firstChar":C
    .restart local v2    # "forward":Z
    .restart local v3    # "highWaterLength":I
    .restart local v7    # "trial":Ljava/lang/String;
    :cond_6
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    goto :goto_2

    .line 839
    .restart local v0    # "c":C
    :cond_7
    if-ne v0, v1, :cond_3

    .line 841
    const/4 v8, 0x0

    aget v8, p2, v8

    invoke-static {p1, v8, p3, v7}, Lcom/ibm/icu/text/UnicodeSet;->matchRest(Lcom/ibm/icu/text/Replaceable;IILjava/lang/String;)I

    move-result v5

    .line 843
    .local v5, "length":I
    if-eqz p4, :cond_9

    .line 844
    if-eqz v2, :cond_8

    const/4 v8, 0x0

    aget v8, p2, v8

    sub-int v6, p3, v8

    .line 845
    .local v6, "maxLen":I
    :goto_5
    if-ne v5, v6, :cond_9

    .line 847
    const/4 v8, 0x1

    goto :goto_0

    .line 844
    .end local v6    # "maxLen":I
    :cond_8
    const/4 v8, 0x0

    aget v8, p2, v8

    sub-int v6, v8, p3

    goto :goto_5

    .line 851
    :cond_9
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-ne v5, v8, :cond_3

    .line 853
    if-le v5, v3, :cond_a

    .line 854
    move v3, v5

    .line 858
    :cond_a
    if-eqz v2, :cond_3

    if-ge v5, v3, :cond_3

    goto :goto_3

    .line 868
    .end local v0    # "c":C
    .end local v5    # "length":I
    .end local v7    # "trial":Ljava/lang/String;
    :cond_b
    neg-int v3, v3

    goto :goto_4

    .line 872
    .end local v1    # "firstChar":C
    .end local v2    # "forward":Z
    .end local v3    # "highWaterLength":I
    .end local v4    # "it":Ljava/util/Iterator;
    :cond_c
    invoke-super {p0, p1, p2, p3, p4}, Lcom/ibm/icu/text/UnicodeFilter;->matches(Lcom/ibm/icu/text/Replaceable;[IIZ)I

    move-result v8

    goto :goto_0
.end method

.method public matchesAt(Ljava/lang/CharSequence;I)I
    .locals 8
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "offset"    # I

    .prologue
    .line 925
    const/4 v4, -0x1

    .line 927
    .local v4, "lastLen":I
    iget-object v7, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v7}, Ljava/util/TreeSet;->size()I

    move-result v7

    if-eqz v7, :cond_1

    .line 928
    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 929
    .local v1, "firstChar":C
    const/4 v6, 0x0

    .line 931
    .local v6, "trial":Ljava/lang/String;
    iget-object v7, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v7}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 932
    .local v3, "it":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 933
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "trial":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 934
    .restart local v6    # "trial":Ljava/lang/String;
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 935
    .local v2, "firstStringChar":C
    if-lt v2, v1, :cond_0

    .line 936
    if-le v2, v1, :cond_0

    .line 947
    .end local v1    # "firstChar":C
    .end local v2    # "firstStringChar":C
    .end local v3    # "it":Ljava/util/Iterator;
    .end local v6    # "trial":Ljava/lang/String;
    :cond_1
    :goto_0
    const/4 v7, 0x2

    if-ge v4, v7, :cond_2

    .line 948
    invoke-static {p1, p2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 949
    .local v0, "cp":I
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 950
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v4

    .line 953
    .end local v0    # "cp":I
    :cond_2
    add-int v7, p2, v4

    return v7

    .line 944
    .restart local v1    # "firstChar":C
    .restart local v3    # "it":Ljava/util/Iterator;
    .local v5, "tempLen":I
    .restart local v6    # "trial":Ljava/lang/String;
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "trial":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 940
    .end local v5    # "tempLen":I
    .restart local v6    # "trial":Ljava/lang/String;
    :cond_4
    invoke-static {p1, p2, v6}, Lcom/ibm/icu/text/UnicodeSet;->matchesAt(Ljava/lang/CharSequence;ILjava/lang/CharSequence;)I

    move-result v5

    .line 941
    .restart local v5    # "tempLen":I
    if-gt v4, v5, :cond_1

    .line 942
    move v4, v5

    .line 943
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_3

    goto :goto_0
.end method

.method public matchesIndexValue(I)Z
    .locals 10
    .param p1, "v"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 756
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/UnicodeSet;->getRangeCount()I

    move-result v8

    if-ge v2, v8, :cond_3

    .line 757
    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/UnicodeSet;->getRangeStart(I)I

    move-result v4

    .line 758
    .local v4, "low":I
    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/UnicodeSet;->getRangeEnd(I)I

    move-result v1

    .line 759
    .local v1, "high":I
    and-int/lit16 v8, v4, -0x100

    and-int/lit16 v9, v1, -0x100

    if-ne v8, v9, :cond_1

    .line 760
    and-int/lit16 v8, v4, 0xff

    if-gt v8, p1, :cond_2

    and-int/lit16 v8, v1, 0xff

    if-gt p1, v8, :cond_2

    .line 782
    .end local v1    # "high":I
    .end local v4    # "low":I
    :cond_0
    :goto_1
    return v6

    .line 763
    .restart local v1    # "high":I
    .restart local v4    # "low":I
    :cond_1
    and-int/lit16 v8, v4, 0xff

    if-le v8, p1, :cond_0

    and-int/lit16 v8, v1, 0xff

    if-le p1, v8, :cond_0

    .line 756
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 767
    .end local v1    # "high":I
    .end local v4    # "low":I
    :cond_3
    iget-object v8, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v8}, Ljava/util/TreeSet;->size()I

    move-result v8

    if-eqz v8, :cond_5

    .line 768
    iget-object v8, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v8}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 769
    .local v3, "it":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 770
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 776
    .local v5, "s":Ljava/lang/String;
    invoke-static {v5, v7}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 777
    .local v0, "c":I
    and-int/lit16 v8, v0, 0xff

    if-ne v8, p1, :cond_4

    goto :goto_1

    .end local v0    # "c":I
    .end local v3    # "it":Ljava/util/Iterator;
    .end local v5    # "s":Ljava/lang/String;
    :cond_5
    move v6, v7

    .line 782
    goto :goto_1
.end method

.method public final remove(I)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 1403
    invoke-virtual {p0, p1, p1}, Lcom/ibm/icu/text/UnicodeSet;->remove(II)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method public remove(II)Lcom/ibm/icu/text/UnicodeSet;
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const v0, 0x10ffff

    const/4 v3, 0x6

    const/4 v1, 0x2

    .line 1381
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 1382
    if-ltz p1, :cond_0

    if-le p1, v0, :cond_1

    .line 1383
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Invalid code point U+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p1, v3}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1385
    :cond_1
    if-ltz p2, :cond_2

    if-le p2, v0, :cond_3

    .line 1386
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Invalid code point U+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p2, v3}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1388
    :cond_3
    if-gt p1, p2, :cond_4

    .line 1389
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/UnicodeSet;->range(II)[I

    move-result-object v0

    invoke-direct {p0, v0, v1, v1}, Lcom/ibm/icu/text/UnicodeSet;->retain([III)Lcom/ibm/icu/text/UnicodeSet;

    .line 1391
    :cond_4
    return-object p0
.end method

.method public final remove(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1415
    invoke-static {p1}, Lcom/ibm/icu/text/UnicodeSet;->getSingleCP(Ljava/lang/String;)I

    move-result v0

    .line 1416
    .local v0, "cp":I
    if-gez v0, :cond_0

    .line 1417
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v1, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 1418
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    .line 1422
    :goto_0
    return-object p0

    .line 1420
    :cond_0
    invoke-virtual {p0, v0, v0}, Lcom/ibm/icu/text/UnicodeSet;->remove(II)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0
.end method

.method public removeAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 3
    .param p1, "c"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 2079
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 2080
    iget-object v0, p1, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    iget v1, p1, Lcom/ibm/icu/text/UnicodeSet;->len:I

    const/4 v2, 0x2

    invoke-direct {p0, v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->retain([III)Lcom/ibm/icu/text/UnicodeSet;

    .line 2081
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    iget-object v1, p1, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->removeAll(Ljava/util/Collection;)Z

    .line 2082
    return-object p0
.end method

.method public final removeAll(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1279
    invoke-static {p1}, Lcom/ibm/icu/text/UnicodeSet;->fromAll(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeSet;->removeAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method public final retain(I)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 1341
    invoke-virtual {p0, p1, p1}, Lcom/ibm/icu/text/UnicodeSet;->retain(II)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method public retain(II)Lcom/ibm/icu/text/UnicodeSet;
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const v0, 0x10ffff

    const/4 v3, 0x6

    .line 1317
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 1318
    if-ltz p1, :cond_0

    if-le p1, v0, :cond_1

    .line 1319
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Invalid code point U+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p1, v3}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1321
    :cond_1
    if-ltz p2, :cond_2

    if-le p2, v0, :cond_3

    .line 1322
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Invalid code point U+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p2, v3}, Lcom/ibm/icu/impl/Utility;->hex(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1324
    :cond_3
    if-gt p1, p2, :cond_4

    .line 1325
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/UnicodeSet;->range(II)[I

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->retain([III)Lcom/ibm/icu/text/UnicodeSet;

    .line 1329
    :goto_0
    return-object p0

    .line 1327
    :cond_4
    invoke-virtual {p0}, Lcom/ibm/icu/text/UnicodeSet;->clear()Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0
.end method

.method public final retain(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1353
    invoke-static {p1}, Lcom/ibm/icu/text/UnicodeSet;->getSingleCP(Ljava/lang/String;)I

    move-result v0

    .line 1354
    .local v0, "cp":I
    if-gez v0, :cond_1

    .line 1355
    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v2, p1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 1356
    .local v1, "isIn":Z
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/ibm/icu/text/UnicodeSet;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1365
    .end local v1    # "isIn":Z
    :goto_0
    return-object p0

    .line 1359
    .restart local v1    # "isIn":Z
    :cond_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/UnicodeSet;->clear()Lcom/ibm/icu/text/UnicodeSet;

    .line 1360
    iget-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v2, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 1361
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    goto :goto_0

    .line 1363
    .end local v1    # "isIn":Z
    :cond_1
    invoke-virtual {p0, v0, v0}, Lcom/ibm/icu/text/UnicodeSet;->retain(II)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0
.end method

.method public retainAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 3
    .param p1, "c"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 2062
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 2063
    iget-object v0, p1, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    iget v1, p1, Lcom/ibm/icu/text/UnicodeSet;->len:I

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->retain([III)Lcom/ibm/icu/text/UnicodeSet;

    .line 2064
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    iget-object v1, p1, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->retainAll(Ljava/util/Collection;)Z

    .line 2065
    return-object p0
.end method

.method public final retainAll(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1257
    invoke-static {p1}, Lcom/ibm/icu/text/UnicodeSet;->fromAll(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeSet;->retainAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method public set(II)Lcom/ibm/icu/text/UnicodeSet;
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 456
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 457
    invoke-virtual {p0}, Lcom/ibm/icu/text/UnicodeSet;->clear()Lcom/ibm/icu/text/UnicodeSet;

    .line 458
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/UnicodeSet;->complement(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 459
    return-object p0
.end method

.method public set(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;
    .locals 1
    .param p1, "other"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 469
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeSet;->checkFrozen()V

    .line 470
    iget-object v0, p1, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    check-cast v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->list:[I

    .line 471
    iget v0, p1, Lcom/ibm/icu/text/UnicodeSet;->len:I

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSet;->len:I

    .line 472
    iget-object v0, p1, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->pat:Ljava/lang/String;

    .line 473
    iget-object v0, p1, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    .line 474
    return-object p0
.end method

.method public size()I
    .locals 5

    .prologue
    .line 722
    const/4 v2, 0x0

    .line 723
    .local v2, "n":I
    invoke-virtual {p0}, Lcom/ibm/icu/text/UnicodeSet;->getRangeCount()I

    move-result v0

    .line 724
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 725
    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/UnicodeSet;->getRangeEnd(I)I

    move-result v3

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/UnicodeSet;->getRangeStart(I)I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    .line 724
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 727
    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v3}, Ljava/util/TreeSet;->size()I

    move-result v3

    add-int/2addr v3, v2

    return v3
.end method

.method public toPattern(Z)Ljava/lang/String;
    .locals 2
    .param p1, "escapeUnprintable"    # Z

    .prologue
    .line 587
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 588
    .local v0, "result":Ljava/lang/StringBuffer;
    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/UnicodeSet;->_toPattern(Ljava/lang/StringBuffer;Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2215
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeSet;->toPattern(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
