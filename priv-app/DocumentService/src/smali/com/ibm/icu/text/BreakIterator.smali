.class public abstract Lcom/ibm/icu/text/BreakIterator;
.super Ljava/lang/Object;
.source "BreakIterator.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;,
        Lcom/ibm/icu/text/BreakIterator$BreakIteratorCache;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field public static final DONE:I = -0x1

.field public static final KIND_CHARACTER:I = 0x0

.field private static final KIND_COUNT:I = 0x5

.field public static final KIND_LINE:I = 0x2

.field public static final KIND_SENTENCE:I = 0x3

.field public static final KIND_TITLE:I = 0x4

.field public static final KIND_WORD:I = 0x1

.field private static final iterCache:[Ljava/lang/ref/SoftReference;

.field private static shim:Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;


# instance fields
.field private actualLocale:Lcom/ibm/icu/util/ULocale;

.field private validLocale:Lcom/ibm/icu/util/ULocale;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 207
    const-string/jumbo v0, "breakiterator"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/ibm/icu/text/BreakIterator;->DEBUG:Z

    .line 429
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/ref/SoftReference;

    sput-object v0, Lcom/ibm/icu/text/BreakIterator;->iterCache:[Ljava/lang/ref/SoftReference;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216
    return-void
.end method

.method public static declared-synchronized getAvailableLocales()[Ljava/util/Locale;
    .locals 2

    .prologue
    .line 732
    const-class v1, Lcom/ibm/icu/text/BreakIterator;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/ibm/icu/text/BreakIterator;->getShim()Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;->getAvailableLocales()[Ljava/util/Locale;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getAvailableULocales()[Lcom/ibm/icu/util/ULocale;
    .locals 2

    .prologue
    .line 745
    const-class v1, Lcom/ibm/icu/text/BreakIterator;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/ibm/icu/text/BreakIterator;->getShim()Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;->getAvailableULocales()[Lcom/ibm/icu/util/ULocale;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getBreakInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;
    .locals 4
    .param p0, "where"    # Lcom/ibm/icu/util/ULocale;
    .param p1, "kind"    # I

    .prologue
    .line 705
    sget-object v2, Lcom/ibm/icu/text/BreakIterator;->iterCache:[Ljava/lang/ref/SoftReference;

    aget-object v2, v2, p1

    if-eqz v2, :cond_0

    .line 706
    sget-object v2, Lcom/ibm/icu/text/BreakIterator;->iterCache:[Ljava/lang/ref/SoftReference;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/BreakIterator$BreakIteratorCache;

    .line 707
    .local v0, "cache":Lcom/ibm/icu/text/BreakIterator$BreakIteratorCache;
    if-eqz v0, :cond_0

    .line 708
    invoke-virtual {v0}, Lcom/ibm/icu/text/BreakIterator$BreakIteratorCache;->getLocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/ibm/icu/util/ULocale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 709
    invoke-virtual {v0}, Lcom/ibm/icu/text/BreakIterator$BreakIteratorCache;->createBreakInstance()Lcom/ibm/icu/text/BreakIterator;

    move-result-object v1

    .line 719
    :goto_0
    return-object v1

    .line 715
    .end local v0    # "cache":Lcom/ibm/icu/text/BreakIterator$BreakIteratorCache;
    :cond_0
    invoke-static {}, Lcom/ibm/icu/text/BreakIterator;->getShim()Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;->createBreakIterator(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v1

    .line 717
    .local v1, "result":Lcom/ibm/icu/text/BreakIterator;
    new-instance v0, Lcom/ibm/icu/text/BreakIterator$BreakIteratorCache;

    invoke-direct {v0, p0, v1}, Lcom/ibm/icu/text/BreakIterator$BreakIteratorCache;-><init>(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/text/BreakIterator;)V

    .line 718
    .restart local v0    # "cache":Lcom/ibm/icu/text/BreakIterator$BreakIteratorCache;
    sget-object v2, Lcom/ibm/icu/text/BreakIterator;->iterCache:[Ljava/lang/ref/SoftReference;

    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    aput-object v3, v2, p1

    goto :goto_0
.end method

.method public static getCharacterInstance()Lcom/ibm/icu/text/BreakIterator;
    .locals 1

    .prologue
    .line 516
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0}, Lcom/ibm/icu/text/BreakIterator;->getCharacterInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method public static getCharacterInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/BreakIterator;
    .locals 1
    .param p0, "where"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 542
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/ibm/icu/text/BreakIterator;->getBreakInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method public static getCharacterInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/BreakIterator;
    .locals 2
    .param p0, "where"    # Ljava/util/Locale;

    .prologue
    .line 529
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/ibm/icu/text/BreakIterator;->getBreakInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method public static getLineInstance()Lcom/ibm/icu/text/BreakIterator;
    .locals 1

    .prologue
    .line 477
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0}, Lcom/ibm/icu/text/BreakIterator;->getLineInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method public static getLineInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/BreakIterator;
    .locals 1
    .param p0, "where"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 503
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/ibm/icu/text/BreakIterator;->getBreakInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method public static getLineInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/BreakIterator;
    .locals 2
    .param p0, "where"    # Ljava/util/Locale;

    .prologue
    .line 490
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/ibm/icu/text/BreakIterator;->getBreakInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method public static getSentenceInstance()Lcom/ibm/icu/text/BreakIterator;
    .locals 1

    .prologue
    .line 554
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0}, Lcom/ibm/icu/text/BreakIterator;->getSentenceInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method public static getSentenceInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/BreakIterator;
    .locals 1
    .param p0, "where"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 576
    const/4 v0, 0x3

    invoke-static {p0, v0}, Lcom/ibm/icu/text/BreakIterator;->getBreakInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method public static getSentenceInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/BreakIterator;
    .locals 2
    .param p0, "where"    # Ljava/util/Locale;

    .prologue
    .line 565
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/ibm/icu/text/BreakIterator;->getBreakInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method private static getShim()Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;
    .locals 4

    .prologue
    .line 781
    sget-object v2, Lcom/ibm/icu/text/BreakIterator;->shim:Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;

    if-nez v2, :cond_0

    .line 783
    :try_start_0
    const-string/jumbo v2, "com.ibm.icu.text.BreakIteratorFactory"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 784
    .local v0, "cls":Ljava/lang/Class;
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;

    sput-object v2, Lcom/ibm/icu/text/BreakIterator;->shim:Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 799
    :cond_0
    sget-object v2, Lcom/ibm/icu/text/BreakIterator;->shim:Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;

    return-object v2

    .line 786
    :catch_0
    move-exception v1

    .line 788
    .local v1, "e":Ljava/util/MissingResourceException;
    throw v1

    .line 790
    .end local v1    # "e":Ljava/util/MissingResourceException;
    :catch_1
    move-exception v1

    .line 792
    .local v1, "e":Ljava/lang/Exception;
    sget-boolean v2, Lcom/ibm/icu/text/BreakIterator;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 793
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 795
    :cond_1
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static getTitleInstance()Lcom/ibm/icu/text/BreakIterator;
    .locals 1

    .prologue
    .line 590
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0}, Lcom/ibm/icu/text/BreakIterator;->getTitleInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method public static getTitleInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/BreakIterator;
    .locals 1
    .param p0, "where"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 618
    const/4 v0, 0x4

    invoke-static {p0, v0}, Lcom/ibm/icu/text/BreakIterator;->getBreakInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method public static getTitleInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/BreakIterator;
    .locals 2
    .param p0, "where"    # Ljava/util/Locale;

    .prologue
    .line 604
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/ibm/icu/text/BreakIterator;->getBreakInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method public static getWordInstance()Lcom/ibm/icu/text/BreakIterator;
    .locals 1

    .prologue
    .line 440
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0}, Lcom/ibm/icu/text/BreakIterator;->getWordInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method public static getWordInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/BreakIterator;
    .locals 1
    .param p0, "where"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 464
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/ibm/icu/text/BreakIterator;->getBreakInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method public static getWordInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/BreakIterator;
    .locals 2
    .param p0, "where"    # Ljava/util/Locale;

    .prologue
    .line 452
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/ibm/icu/text/BreakIterator;->getBreakInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method public static registerInstance(Lcom/ibm/icu/text/BreakIterator;Lcom/ibm/icu/util/ULocale;I)Ljava/lang/Object;
    .locals 3
    .param p0, "iter"    # Lcom/ibm/icu/text/BreakIterator;
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "kind"    # I

    .prologue
    .line 650
    sget-object v1, Lcom/ibm/icu/text/BreakIterator;->iterCache:[Ljava/lang/ref/SoftReference;

    aget-object v1, v1, p2

    if-eqz v1, :cond_0

    .line 651
    sget-object v1, Lcom/ibm/icu/text/BreakIterator;->iterCache:[Ljava/lang/ref/SoftReference;

    aget-object v1, v1, p2

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/BreakIterator$BreakIteratorCache;

    .line 652
    .local v0, "cache":Lcom/ibm/icu/text/BreakIterator$BreakIteratorCache;
    if-eqz v0, :cond_0

    .line 653
    invoke-virtual {v0}, Lcom/ibm/icu/text/BreakIterator$BreakIteratorCache;->getLocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/ibm/icu/util/ULocale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 654
    sget-object v1, Lcom/ibm/icu/text/BreakIterator;->iterCache:[Ljava/lang/ref/SoftReference;

    const/4 v2, 0x0

    aput-object v2, v1, p2

    .line 658
    .end local v0    # "cache":Lcom/ibm/icu/text/BreakIterator$BreakIteratorCache;
    :cond_0
    invoke-static {}, Lcom/ibm/icu/text/BreakIterator;->getShim()Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;

    move-result-object v1

    invoke-virtual {v1, p0, p1, p2}, Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;->registerInstance(Lcom/ibm/icu/text/BreakIterator;Lcom/ibm/icu/util/ULocale;I)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public static registerInstance(Lcom/ibm/icu/text/BreakIterator;Ljava/util/Locale;I)Ljava/lang/Object;
    .locals 1
    .param p0, "iter"    # Lcom/ibm/icu/text/BreakIterator;
    .param p1, "locale"    # Ljava/util/Locale;
    .param p2, "kind"    # I

    .prologue
    .line 633
    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/ibm/icu/text/BreakIterator;->registerInstance(Lcom/ibm/icu/text/BreakIterator;Lcom/ibm/icu/util/ULocale;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static unregister(Ljava/lang/Object;)Z
    .locals 3
    .param p0, "key"    # Ljava/lang/Object;

    .prologue
    .line 669
    if-nez p0, :cond_0

    .line 670
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "registry key must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 681
    :cond_0
    sget-object v1, Lcom/ibm/icu/text/BreakIterator;->shim:Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;

    if-eqz v1, :cond_2

    .line 686
    const/4 v0, 0x0

    .local v0, "kind":I
    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 687
    sget-object v1, Lcom/ibm/icu/text/BreakIterator;->iterCache:[Ljava/lang/ref/SoftReference;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 686
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 689
    :cond_1
    sget-object v1, Lcom/ibm/icu/text/BreakIterator;->shim:Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;

    invoke-virtual {v1, p0}, Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;->unregister(Ljava/lang/Object;)Z

    move-result v1

    .line 691
    .end local v0    # "kind":I
    :goto_1
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 227
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 229
    :catch_0
    move-exception v0

    .line 231
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1
.end method

.method public abstract current()I
.end method

.method public abstract first()I
.end method

.method public abstract following(I)I
.end method

.method public final getLocale(Lcom/ibm/icu/util/ULocale$Type;)Lcom/ibm/icu/util/ULocale;
    .locals 1
    .param p1, "type"    # Lcom/ibm/icu/util/ULocale$Type;

    .prologue
    .line 829
    sget-object v0, Lcom/ibm/icu/util/ULocale;->ACTUAL_LOCALE:Lcom/ibm/icu/util/ULocale$Type;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/BreakIterator;->actualLocale:Lcom/ibm/icu/util/ULocale;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/BreakIterator;->validLocale:Lcom/ibm/icu/util/ULocale;

    goto :goto_0
.end method

.method public abstract getText()Ljava/text/CharacterIterator;
.end method

.method public isBoundary(I)Z
    .locals 2
    .param p1, "offset"    # I

    .prologue
    const/4 v0, 0x1

    .line 360
    if-nez p1, :cond_1

    .line 364
    :cond_0
    :goto_0
    return v0

    :cond_1
    add-int/lit8 v1, p1, -0x1

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/BreakIterator;->following(I)I

    move-result v1

    if-eq v1, p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract last()I
.end method

.method public abstract next()I
.end method

.method public abstract next(I)I
.end method

.method public preceding(I)I
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 341
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/BreakIterator;->following(I)I

    move-result v0

    .line 342
    .local v0, "pos":I
    :goto_0
    if-lt v0, p1, :cond_0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 343
    invoke-virtual {p0}, Lcom/ibm/icu/text/BreakIterator;->previous()I

    move-result v0

    goto :goto_0

    .line 344
    :cond_0
    return v0
.end method

.method public abstract previous()I
.end method

.method final setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V
    .locals 3
    .param p1, "valid"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "actual"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 852
    if-nez p1, :cond_0

    move v2, v0

    :goto_0
    if-nez p2, :cond_1

    :goto_1
    if-eq v2, v0, :cond_2

    .line 854
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v2, v1

    .line 852
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 859
    :cond_2
    iput-object p1, p0, Lcom/ibm/icu/text/BreakIterator;->validLocale:Lcom/ibm/icu/util/ULocale;

    .line 860
    iput-object p2, p0, Lcom/ibm/icu/text/BreakIterator;->actualLocale:Lcom/ibm/icu/util/ULocale;

    .line 861
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    .line 398
    new-instance v0, Ljava/text/StringCharacterIterator;

    invoke-direct {v0, p1}, Ljava/text/StringCharacterIterator;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/BreakIterator;->setText(Ljava/text/CharacterIterator;)V

    .line 399
    return-void
.end method

.method public abstract setText(Ljava/text/CharacterIterator;)V
.end method
