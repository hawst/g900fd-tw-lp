.class public Lcom/ibm/icu/text/SimpleDateFormat;
.super Lcom/ibm/icu/text/DateFormat;
.source "SimpleDateFormat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;
    }
.end annotation


# static fields
.field private static final CALENDAR_FIELD_TO_LEVEL:[I

.field private static final COLON:C = ':'

.field private static final DEFAULT_GMT_PREFIX:Ljava/lang/String; = "GMT"

.field private static final DEFAULT_GMT_PREFIX_LEN:I = 0x3

.field private static final FALLBACKPATTERN:Ljava/lang/String; = "yy/MM/dd HH:mm"

.field private static final MINUS:C = '-'

.field private static final NUMERIC_FORMAT_CHARS:Ljava/lang/String; = "MYyudehHmsSDFwWkK"

.field private static PARSED_PATTERN_CACHE:Lcom/ibm/icu/impl/ICUCache; = null

.field private static final PATTERN_CHAR_BASE:I = 0x40

.field private static final PATTERN_CHAR_TO_INDEX:[I

.field private static final PATTERN_CHAR_TO_LEVEL:[I

.field private static final PATTERN_INDEX_TO_CALENDAR_FIELD:[I

.field private static final PATTERN_INDEX_TO_DATE_FORMAT_ATTRIBUTE:[Lcom/ibm/icu/text/DateFormat$Field;

.field private static final PATTERN_INDEX_TO_DATE_FORMAT_FIELD:[I

.field private static final PLUS:C = '+'

.field private static final SUPPRESS_NEGATIVE_PREFIX:Ljava/lang/String; = "\uab00"

.field private static final TZTYPE_DST:I = 0x2

.field private static final TZTYPE_STD:I = 0x1

.field private static final TZTYPE_UNK:I = 0x0

.field private static cachedDefaultLocale:Lcom/ibm/icu/util/ULocale; = null

.field private static cachedDefaultPattern:Ljava/lang/String; = null

.field static final currentSerialVersion:I = 0x1

.field private static final millisPerHour:I = 0x36ee80

.field private static final millisPerMinute:I = 0xea60

.field private static final millisPerSecond:I = 0x3e8

.field private static final serialVersionUID:J = 0x4243c9da93943590L


# instance fields
.field private transient decimalBuf:[C

.field private transient defaultCenturyBase:J

.field private defaultCenturyStart:Ljava/util/Date;

.field private transient defaultCenturyStartYear:I

.field private formatData:Lcom/ibm/icu/text/DateFormatSymbols;

.field private transient gmtfmtCache:[Ljava/lang/ref/WeakReference;

.field private transient locale:Lcom/ibm/icu/util/ULocale;

.field private pattern:Ljava/lang/String;

.field private transient patternItems:[Ljava/lang/Object;

.field private serialVersionOnStream:I

.field private transient tztype:I

.field private transient useFastFormat:Z

.field private transient useLocalZeroPaddingNumberFormat:Z

.field private transient zeroDigit:C


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v2, 0x40

    const/16 v3, 0x16

    const/16 v1, 0x1e

    .line 247
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/text/SimpleDateFormat;->CALENDAR_FIELD_TO_LEVEL:[I

    .line 268
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_CHAR_TO_LEVEL:[I

    .line 480
    sput-object v4, Lcom/ibm/icu/text/SimpleDateFormat;->cachedDefaultLocale:Lcom/ibm/icu/util/ULocale;

    .line 481
    sput-object v4, Lcom/ibm/icu/text/SimpleDateFormat;->cachedDefaultPattern:Ljava/lang/String;

    .line 641
    new-array v0, v2, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_CHAR_TO_INDEX:[I

    .line 654
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_INDEX_TO_CALENDAR_FIELD:[I

    .line 672
    new-array v0, v1, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_INDEX_TO_DATE_FORMAT_FIELD:[I

    .line 691
    new-array v0, v1, [Lcom/ibm/icu/text/DateFormat$Field;

    const/4 v1, 0x0

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->ERA:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->YEAR:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->MONTH:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->DAY_OF_MONTH:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->HOUR_OF_DAY1:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->HOUR_OF_DAY0:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->MINUTE:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->SECOND:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->MILLISECOND:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->DAY_OF_WEEK:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->DAY_OF_YEAR:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->DAY_OF_WEEK_IN_MONTH:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->WEEK_OF_YEAR:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->WEEK_OF_MONTH:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->AM_PM:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->HOUR1:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->HOUR0:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->TIME_ZONE:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->YEAR_WOY:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->DOW_LOCAL:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->EXTENDED_YEAR:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->JULIAN_DAY:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    sget-object v1, Lcom/ibm/icu/text/DateFormat$Field;->MILLISECONDS_IN_DAY:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v1, v0, v3

    const/16 v1, 0x17

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->TIME_ZONE:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->TIME_ZONE:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->DAY_OF_WEEK:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->MONTH:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->QUARTER:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->QUARTER:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/ibm/icu/text/DateFormat$Field;->TIME_ZONE:Lcom/ibm/icu/text/DateFormat$Field;

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_INDEX_TO_DATE_FORMAT_ATTRIBUTE:[Lcom/ibm/icu/text/DateFormat$Field;

    .line 1048
    new-instance v0, Lcom/ibm/icu/impl/SimpleCache;

    invoke-direct {v0}, Lcom/ibm/icu/impl/SimpleCache;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/SimpleDateFormat;->PARSED_PATTERN_CACHE:Lcom/ibm/icu/impl/ICUCache;

    return-void

    .line 247
    :array_0
    .array-data 4
        0x0
        0xa
        0x14
        0x14
        0x1e
        0x1e
        0x14
        0x1e
        0x1e
        0x28
        0x32
        0x32
        0x3c
        0x46
        0x50
        0x0
        0x0
        0xa
        0x1e
        0xa
        0x0
        0x28
    .end array-data

    .line 268
    :array_1
    .array-data 4
        -0x1
        0x28
        -0x1
        -0x1
        0x14
        0x1e
        0x1e
        0x0
        0x32
        -0x1
        -0x1
        0x32
        0x14
        0x14
        -0x1
        -0x1
        -0x1
        0x14
        -0x1
        0x50
        -0x1
        -0x1
        0x0
        0x1e
        -0x1
        0xa
        0x0
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x28
        -0x1
        0x1e
        0x1e
        0x1e
        -0x1
        0x0
        0x32
        -0x1
        -0x1
        0x32
        -0x1
        0x3c
        -0x1
        -0x1
        -0x1
        0x14
        -0x1
        0x46
        -0x1
        0xa
        0x0
        0x14
        -0x1
        0xa
        0x0
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
    .end array-data

    .line 641
    :array_2
    .array-data 4
        -0x1
        0x16
        -0x1
        -0x1
        0xa
        0x9
        0xb
        0x0
        0x5
        -0x1
        -0x1
        0x10
        0x1a
        0x2
        -0x1
        -0x1
        -0x1
        0x1b
        -0x1
        0x8
        -0x1
        -0x1
        0x1d
        0xd
        -0x1
        0x12
        0x17
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0xe
        -0x1
        0x19
        0x3
        0x13
        -0x1
        0x15
        0xf
        -0x1
        -0x1
        0x4
        -0x1
        0x6
        -0x1
        -0x1
        -0x1
        0x1c
        -0x1
        0x7
        -0x1
        0x14
        0x18
        0xc
        -0x1
        0x1
        0x11
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
    .end array-data

    .line 654
    :array_3
    .array-data 4
        0x0
        0x1
        0x2
        0x5
        0xb
        0xb
        0xc
        0xd
        0xe
        0x7
        0x6
        0x8
        0x3
        0x4
        0x9
        0xa
        0xa
        0xf
        0x11
        0x12
        0x13
        0x14
        0x15
        0xf
        0xf
        0x7
        0x2
        0x2
        0x2
        0xf
    .end array-data

    .line 672
    :array_4
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
        0x11
        0x12
        0x13
        0x14
        0x15
        0x16
        0x17
        0x18
        0x19
        0x1a
        0x1b
        0x1c
        0x1d
    .end array-data
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 357
    invoke-static {}, Lcom/ibm/icu/text/SimpleDateFormat;->getDefaultPattern()Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x1

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/SimpleDateFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/text/NumberFormat;Lcom/ibm/icu/util/ULocale;Z)V

    .line 358
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 7
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 368
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/SimpleDateFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/text/NumberFormat;Lcom/ibm/icu/util/ULocale;Z)V

    .line 369
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/ibm/icu/text/DateFormatSymbols;)V
    .locals 7
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "formatData"    # Lcom/ibm/icu/text/DateFormatSymbols;

    .prologue
    const/4 v3, 0x0

    .line 401
    invoke-virtual {p2}, Lcom/ibm/icu/text/DateFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/DateFormatSymbols;

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/SimpleDateFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/text/NumberFormat;Lcom/ibm/icu/util/ULocale;Z)V

    .line 402
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/text/NumberFormat;Lcom/ibm/icu/util/ULocale;Z)V
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "formatData"    # Lcom/ibm/icu/text/DateFormatSymbols;
    .param p3, "calendar"    # Lcom/ibm/icu/util/Calendar;
    .param p4, "numberFormat"    # Lcom/ibm/icu/text/NumberFormat;
    .param p5, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p6, "useFastFormat"    # Z

    .prologue
    .line 428
    invoke-direct {p0}, Lcom/ibm/icu/text/DateFormat;-><init>()V

    .line 294
    const/4 v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->serialVersionOnStream:I

    .line 331
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->tztype:I

    .line 429
    iput-object p1, p0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    .line 430
    iput-object p2, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    .line 431
    iput-object p3, p0, Lcom/ibm/icu/text/SimpleDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    .line 432
    iput-object p4, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    .line 433
    iput-object p5, p0, Lcom/ibm/icu/text/SimpleDateFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 434
    iput-boolean p6, p0, Lcom/ibm/icu/text/SimpleDateFormat;->useFastFormat:Z

    .line 435
    invoke-direct {p0}, Lcom/ibm/icu/text/SimpleDateFormat;->initialize()V

    .line 436
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/ULocale;Z)V
    .locals 7
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "formatData"    # Lcom/ibm/icu/text/DateFormatSymbols;
    .param p3, "calendar"    # Lcom/ibm/icu/util/Calendar;
    .param p4, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p5, "useFastFormat"    # Z

    .prologue
    .line 421
    invoke-virtual {p2}, Lcom/ibm/icu/text/DateFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-virtual {p3}, Lcom/ibm/icu/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/util/Calendar;

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/SimpleDateFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/text/NumberFormat;Lcom/ibm/icu/util/ULocale;Z)V

    .line 422
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/ULocale;)V
    .locals 7
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "formatData"    # Lcom/ibm/icu/text/DateFormatSymbols;
    .param p3, "loc"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v3, 0x0

    .line 410
    invoke-virtual {p2}, Lcom/ibm/icu/text/DateFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/DateFormatSymbols;

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/SimpleDateFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/text/NumberFormat;Lcom/ibm/icu/util/ULocale;Z)V

    .line 411
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V
    .locals 7
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "loc"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v2, 0x0

    .line 390
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/SimpleDateFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/text/NumberFormat;Lcom/ibm/icu/util/ULocale;Z)V

    .line 391
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Locale;)V
    .locals 7
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "loc"    # Ljava/util/Locale;

    .prologue
    const/4 v2, 0x0

    .line 379
    invoke-static {p2}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v5

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/SimpleDateFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/text/NumberFormat;Lcom/ibm/icu/util/ULocale;Z)V

    .line 380
    return-void
.end method

.method static access$000(CI)Z
    .locals 1
    .param p0, "x0"    # C
    .param p1, "x1"    # I

    .prologue
    .line 227
    invoke-static {p0, p1}, Lcom/ibm/icu/text/SimpleDateFormat;->isNumeric(CI)Z

    move-result v0

    return v0
.end method

.method private appendGMT(Ljava/lang/StringBuffer;Lcom/ibm/icu/util/Calendar;)V
    .locals 10
    .param p1, "buf"    # Ljava/lang/StringBuffer;
    .param p2, "cal"    # Lcom/ibm/icu/util/Calendar;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1147
    const/16 v6, 0xf

    invoke-virtual {p2, v6}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v6

    const/16 v7, 0x10

    invoke-virtual {p2, v7}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v7

    add-int v1, v6, v7

    .line 1149
    .local v1, "offset":I
    invoke-direct {p0}, Lcom/ibm/icu/text/SimpleDateFormat;->isDefaultGMTFormat()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1150
    invoke-direct {p0, p1, v1}, Lcom/ibm/icu/text/SimpleDateFormat;->formatGMTDefault(Ljava/lang/StringBuffer;I)V

    .line 1162
    :goto_0
    return-void

    .line 1152
    :cond_0
    const/4 v2, 0x1

    .line 1153
    .local v2, "sign":I
    if-gez v1, :cond_1

    .line 1154
    neg-int v1, v1

    .line 1155
    const/4 v2, 0x0

    .line 1157
    :cond_1
    const v6, 0xea60

    rem-int v6, v1, v6

    if-nez v6, :cond_2

    move v3, v4

    .line 1159
    .local v3, "width":I
    :goto_1
    invoke-direct {p0, v2, v3}, Lcom/ibm/icu/text/SimpleDateFormat;->getGMTFormatter(II)Lcom/ibm/icu/text/MessageFormat;

    move-result-object v0

    .line 1160
    .local v0, "fmt":Lcom/ibm/icu/text/MessageFormat;
    new-array v4, v4, [Ljava/lang/Object;

    new-instance v6, Ljava/lang/Long;

    int-to-long v8, v1

    invoke-direct {v6, v8, v9}, Ljava/lang/Long;-><init>(J)V

    aput-object v6, v4, v5

    const/4 v5, 0x0

    invoke-virtual {v0, v4, p1, v5}, Lcom/ibm/icu/text/MessageFormat;->format([Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    goto :goto_0

    .end local v0    # "fmt":Lcom/ibm/icu/text/MessageFormat;
    .end local v3    # "width":I
    :cond_2
    move v3, v5

    .line 1157
    goto :goto_1
.end method

.method private diffCalFieldValue(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/Calendar;[Ljava/lang/Object;I)Z
    .locals 10
    .param p1, "fromCalendar"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "toCalendar"    # Lcom/ibm/icu/util/Calendar;
    .param p3, "items"    # [Ljava/lang/Object;
    .param p4, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2843
    aget-object v7, p3, p4

    instance-of v7, v7, Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 2865
    :cond_0
    :goto_0
    return v6

    .line 2846
    :cond_1
    aget-object v2, p3, p4

    check-cast v2, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;

    .line 2847
    .local v2, "item":Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;
    iget-char v0, v2, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->type:C

    .line 2848
    .local v0, "ch":C
    const/4 v3, -0x1

    .line 2849
    .local v3, "patternCharIndex":I
    const/16 v7, 0x41

    if-gt v7, v0, :cond_2

    const/16 v7, 0x7a

    if-gt v0, v7, :cond_2

    .line 2850
    sget-object v7, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_CHAR_TO_INDEX:[I

    add-int/lit8 v8, v0, -0x40

    aget v3, v7, v8

    .line 2853
    :cond_2
    const/4 v7, -0x1

    if-ne v3, v7, :cond_3

    .line 2854
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v8, "Illegal pattern character \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string/jumbo v8, "\' in \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    new-instance v8, Ljava/lang/String;

    iget-object v9, p0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const/16 v8, 0x22

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 2859
    :cond_3
    sget-object v7, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_INDEX_TO_CALENDAR_FIELD:[I

    aget v1, v7, v3

    .line 2860
    .local v1, "field":I
    invoke-virtual {p1, v1}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v4

    .line 2861
    .local v4, "value":I
    invoke-virtual {p2, v1}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v5

    .line 2862
    .local v5, "value_2":I
    if-eq v4, v5, :cond_0

    .line 2863
    const/4 v6, 0x1

    goto :goto_0
.end method

.method private fastZeroPaddingNumber(Ljava/lang/StringBuffer;III)V
    .locals 7
    .param p1, "buf"    # Ljava/lang/StringBuffer;
    .param p2, "value"    # I
    .param p3, "minDigits"    # I
    .param p4, "maxDigits"    # I

    .prologue
    .line 1467
    iget-object v4, p0, Lcom/ibm/icu/text/SimpleDateFormat;->decimalBuf:[C

    array-length v4, v4

    if-ge v4, p4, :cond_1

    iget-object v4, p0, Lcom/ibm/icu/text/SimpleDateFormat;->decimalBuf:[C

    array-length v2, v4

    .line 1468
    .local v2, "limit":I
    :goto_0
    add-int/lit8 v0, v2, -0x1

    .line 1470
    .local v0, "index":I
    :goto_1
    iget-object v4, p0, Lcom/ibm/icu/text/SimpleDateFormat;->decimalBuf:[C

    rem-int/lit8 v5, p2, 0xa

    iget-char v6, p0, Lcom/ibm/icu/text/SimpleDateFormat;->zeroDigit:C

    add-int/2addr v5, v6

    int-to-char v5, v5

    aput-char v5, v4, v0

    .line 1471
    div-int/lit8 p2, p2, 0xa

    .line 1472
    if-eqz v0, :cond_0

    if-nez p2, :cond_2

    .line 1477
    :cond_0
    sub-int v4, v2, v0

    sub-int v3, p3, v4

    .line 1478
    .local v3, "padding":I
    :goto_2
    if-lez v3, :cond_3

    .line 1479
    iget-object v4, p0, Lcom/ibm/icu/text/SimpleDateFormat;->decimalBuf:[C

    add-int/lit8 v0, v0, -0x1

    iget-char v5, p0, Lcom/ibm/icu/text/SimpleDateFormat;->zeroDigit:C

    aput-char v5, v4, v0

    .line 1478
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .end local v0    # "index":I
    .end local v2    # "limit":I
    .end local v3    # "padding":I
    :cond_1
    move v2, p4

    .line 1467
    goto :goto_0

    .line 1475
    .restart local v0    # "index":I
    .restart local v2    # "limit":I
    :cond_2
    add-int/lit8 v0, v0, -0x1

    .line 1476
    goto :goto_1

    .line 1481
    .restart local v3    # "padding":I
    :cond_3
    sub-int v1, v2, v0

    .line 1482
    .local v1, "length":I
    iget-object v4, p0, Lcom/ibm/icu/text/SimpleDateFormat;->decimalBuf:[C

    invoke-virtual {p1, v4, v0, v1}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 1483
    return-void
.end method

.method private format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;
    .locals 15
    .param p1, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;
    .param p4, "attributes"    # Ljava/util/List;

    .prologue
    .line 592
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 593
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 599
    invoke-direct {p0}, Lcom/ibm/icu/text/SimpleDateFormat;->getPatternItems()[Ljava/lang/Object;

    move-result-object v13

    .line 600
    .local v13, "items":[Ljava/lang/Object;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    array-length v1, v13

    if-ge v11, v1, :cond_4

    .line 601
    aget-object v1, v13, v11

    instance-of v1, v1, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 602
    aget-object v1, v13, v11

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 600
    :cond_0
    :goto_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 604
    :cond_1
    aget-object v12, v13, v11

    check-cast v12, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;

    .line 607
    .local v12, "item":Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;
    const/4 v14, 0x0

    .line 608
    .local v14, "start":I
    if-eqz p4, :cond_2

    .line 610
    invoke-virtual/range {p2 .. p2}, Ljava/lang/StringBuffer;->length()I

    move-result v14

    .line 613
    :cond_2
    iget-boolean v1, p0, Lcom/ibm/icu/text/SimpleDateFormat;->useFastFormat:Z

    if-eqz v1, :cond_3

    .line 614
    iget-char v3, v12, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->type:C

    iget v4, v12, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->length:I

    invoke-virtual/range {p2 .. p2}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    move-object v1, p0

    move-object/from16 v2, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p1

    invoke-virtual/range {v1 .. v7}, Lcom/ibm/icu/text/SimpleDateFormat;->subFormat(Ljava/lang/StringBuffer;CIILjava/text/FieldPosition;Lcom/ibm/icu/util/Calendar;)V

    .line 620
    :goto_2
    if-eqz p4, :cond_0

    .line 622
    invoke-virtual/range {p2 .. p2}, Ljava/lang/StringBuffer;->length()I

    move-result v9

    .line 623
    .local v9, "end":I
    sub-int v1, v9, v14

    if-lez v1, :cond_0

    .line 625
    iget-char v1, v12, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->type:C

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/SimpleDateFormat;->patternCharToDateFormatField(C)Lcom/ibm/icu/text/DateFormat$Field;

    move-result-object v8

    .line 626
    .local v8, "attr":Lcom/ibm/icu/text/DateFormat$Field;
    new-instance v10, Ljava/text/FieldPosition;

    invoke-direct {v10, v8}, Ljava/text/FieldPosition;-><init>(Ljava/text/Format$Field;)V

    .line 627
    .local v10, "fp":Ljava/text/FieldPosition;
    invoke-virtual {v10, v14}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 628
    invoke-virtual {v10, v9}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 629
    move-object/from16 v0, p4

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 616
    .end local v8    # "attr":Lcom/ibm/icu/text/DateFormat$Field;
    .end local v9    # "end":I
    .end local v10    # "fp":Ljava/text/FieldPosition;
    :cond_3
    iget-char v2, v12, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->type:C

    iget v3, v12, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->length:I

    invoke-virtual/range {p2 .. p2}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    iget-object v6, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object v1, p0

    move-object/from16 v5, p3

    move-object/from16 v7, p1

    invoke-virtual/range {v1 .. v7}, Lcom/ibm/icu/text/SimpleDateFormat;->subFormat(CIILjava/text/FieldPosition;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 635
    .end local v12    # "item":Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;
    .end local v14    # "start":I
    :cond_4
    return-object p2
.end method

.method private formatGMTDefault(Ljava/lang/StringBuffer;I)V
    .locals 6
    .param p1, "buf"    # Ljava/lang/StringBuffer;
    .param p2, "offset"    # I

    .prologue
    const/16 v5, 0x3a

    const/4 v4, 0x2

    .line 1165
    const-string/jumbo v3, "GMT"

    invoke-virtual {p1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1166
    if-ltz p2, :cond_1

    .line 1167
    const/16 v3, 0x2b

    invoke-virtual {p1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1172
    :goto_0
    div-int/lit16 p2, p2, 0x3e8

    .line 1173
    rem-int/lit8 v2, p2, 0x3c

    .line 1174
    .local v2, "sec":I
    div-int/lit8 p2, p2, 0x3c

    .line 1175
    rem-int/lit8 v1, p2, 0x3c

    .line 1176
    .local v1, "min":I
    div-int/lit8 v0, p2, 0x3c

    .line 1178
    .local v0, "hour":I
    invoke-virtual {p0, p1, v0, v4, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    .line 1179
    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1180
    invoke-virtual {p0, p1, v1, v4, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    .line 1181
    if-eqz v2, :cond_0

    .line 1182
    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1183
    invoke-virtual {p0, p1, v2, v4, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    .line 1185
    :cond_0
    return-void

    .line 1169
    .end local v0    # "hour":I
    .end local v1    # "min":I
    .end local v2    # "sec":I
    :cond_1
    const/16 v3, 0x2d

    invoke-virtual {p1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1170
    neg-int p2, p2

    goto :goto_0
.end method

.method private getDefaultCenturyStart()Ljava/util/Date;
    .locals 2

    .prologue
    .line 530
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyStart:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 532
    iget-wide v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyBase:J

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/SimpleDateFormat;->initializeDefaultCenturyStart(J)V

    .line 534
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyStart:Ljava/util/Date;

    return-object v0
.end method

.method private getDefaultCenturyStartYear()I
    .locals 2

    .prologue
    .line 539
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyStart:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 541
    iget-wide v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyBase:J

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/SimpleDateFormat;->initializeDefaultCenturyStart(J)V

    .line 543
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyStartYear:I

    return v0
.end method

.method private static declared-synchronized getDefaultPattern()Ljava/lang/String;
    .locals 10

    .prologue
    .line 489
    const-class v6, Lcom/ibm/icu/text/SimpleDateFormat;

    monitor-enter v6

    :try_start_0
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v3

    .line 490
    .local v3, "defaultLocale":Lcom/ibm/icu/util/ULocale;
    sget-object v5, Lcom/ibm/icu/text/SimpleDateFormat;->cachedDefaultLocale:Lcom/ibm/icu/util/ULocale;

    invoke-virtual {v3, v5}, Lcom/ibm/icu/util/ULocale;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 491
    sput-object v3, Lcom/ibm/icu/text/SimpleDateFormat;->cachedDefaultLocale:Lcom/ibm/icu/util/ULocale;

    .line 492
    sget-object v5, Lcom/ibm/icu/text/SimpleDateFormat;->cachedDefaultLocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v5}, Lcom/ibm/icu/util/Calendar;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/Calendar;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 494
    .local v0, "cal":Lcom/ibm/icu/util/Calendar;
    :try_start_1
    new-instance v1, Lcom/ibm/icu/impl/CalendarData;

    sget-object v5, Lcom/ibm/icu/text/SimpleDateFormat;->cachedDefaultLocale:Lcom/ibm/icu/util/ULocale;

    invoke-virtual {v0}, Lcom/ibm/icu/util/Calendar;->getType()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v5, v7}, Lcom/ibm/icu/impl/CalendarData;-><init>(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)V

    .line 495
    .local v1, "calData":Lcom/ibm/icu/impl/CalendarData;
    const-string/jumbo v5, "DateTimePatterns"

    invoke-virtual {v1, v5}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 496
    .local v2, "dateTimePatterns":[Ljava/lang/String;
    const/16 v5, 0x8

    aget-object v5, v2, v5

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x3

    aget-object v9, v2, v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x7

    aget-object v9, v2, v9

    aput-object v9, v7, v8

    invoke-static {v5, v7}, Lcom/ibm/icu/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/ibm/icu/text/SimpleDateFormat;->cachedDefaultPattern:Ljava/lang/String;
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 502
    .end local v0    # "cal":Lcom/ibm/icu/util/Calendar;
    .end local v1    # "calData":Lcom/ibm/icu/impl/CalendarData;
    .end local v2    # "dateTimePatterns":[Ljava/lang/String;
    :cond_0
    :goto_0
    :try_start_2
    sget-object v5, Lcom/ibm/icu/text/SimpleDateFormat;->cachedDefaultPattern:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v6

    return-object v5

    .line 498
    .restart local v0    # "cal":Lcom/ibm/icu/util/Calendar;
    :catch_0
    move-exception v4

    .line 499
    .local v4, "e":Ljava/util/MissingResourceException;
    :try_start_3
    const-string/jumbo v5, "yy/MM/dd HH:mm"

    sput-object v5, Lcom/ibm/icu/text/SimpleDateFormat;->cachedDefaultPattern:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 489
    .end local v0    # "cal":Lcom/ibm/icu/util/Calendar;
    .end local v4    # "e":Ljava/util/MissingResourceException;
    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5
.end method

.method private getGMTFormatter(II)Lcom/ibm/icu/text/MessageFormat;
    .locals 5
    .param p1, "sign"    # I
    .param p2, "width"    # I

    .prologue
    .line 1356
    const/4 v1, 0x0

    .line 1357
    .local v1, "fmt":Lcom/ibm/icu/text/MessageFormat;
    iget-object v3, p0, Lcom/ibm/icu/text/SimpleDateFormat;->gmtfmtCache:[Ljava/lang/ref/WeakReference;

    if-nez v3, :cond_0

    .line 1358
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/ref/WeakReference;

    iput-object v3, p0, Lcom/ibm/icu/text/SimpleDateFormat;->gmtfmtCache:[Ljava/lang/ref/WeakReference;

    .line 1360
    :cond_0
    mul-int/lit8 v3, p1, 0x2

    add-int v0, v3, p2

    .line 1361
    .local v0, "cacheIdx":I
    iget-object v3, p0, Lcom/ibm/icu/text/SimpleDateFormat;->gmtfmtCache:[Ljava/lang/ref/WeakReference;

    aget-object v3, v3, v0

    if-eqz v3, :cond_1

    .line 1362
    iget-object v3, p0, Lcom/ibm/icu/text/SimpleDateFormat;->gmtfmtCache:[Ljava/lang/ref/WeakReference;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "fmt":Lcom/ibm/icu/text/MessageFormat;
    check-cast v1, Lcom/ibm/icu/text/MessageFormat;

    .line 1364
    .restart local v1    # "fmt":Lcom/ibm/icu/text/MessageFormat;
    :cond_1
    if-nez v1, :cond_2

    .line 1365
    new-instance v1, Lcom/ibm/icu/text/MessageFormat;

    .end local v1    # "fmt":Lcom/ibm/icu/text/MessageFormat;
    iget-object v3, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v3, v3, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    invoke-direct {v1, v3}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;)V

    .line 1366
    .restart local v1    # "fmt":Lcom/ibm/icu/text/MessageFormat;
    invoke-virtual {p0}, Lcom/ibm/icu/text/SimpleDateFormat;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/SimpleDateFormat;

    .line 1367
    .local v2, "sdf":Lcom/ibm/icu/text/SimpleDateFormat;
    const-string/jumbo v3, "Etc/UTC"

    invoke-static {v3}, Lcom/ibm/icu/util/TimeZone;->getTimeZone(Ljava/lang/String;)Lcom/ibm/icu/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/SimpleDateFormat;->setTimeZone(Lcom/ibm/icu/util/TimeZone;)V

    .line 1368
    iget-object v3, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-virtual {v3, p1, p2}, Lcom/ibm/icu/text/DateFormatSymbols;->getGmtHourFormat(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 1369
    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Lcom/ibm/icu/text/MessageFormat;->setFormat(ILjava/text/Format;)V

    .line 1370
    iget-object v3, p0, Lcom/ibm/icu/text/SimpleDateFormat;->gmtfmtCache:[Ljava/lang/ref/WeakReference;

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    aput-object v4, v3, v0

    .line 1372
    .end local v2    # "sdf":Lcom/ibm/icu/text/SimpleDateFormat;
    :cond_2
    return-object v1
.end method

.method public static getInstance(Lcom/ibm/icu/util/Calendar$FormatConfiguration;)Lcom/ibm/icu/text/SimpleDateFormat;
    .locals 7
    .param p0, "formatConfig"    # Lcom/ibm/icu/util/Calendar$FormatConfiguration;

    .prologue
    .line 446
    new-instance v0, Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/ibm/icu/util/Calendar$FormatConfiguration;->getPatternString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/ibm/icu/util/Calendar$FormatConfiguration;->getDateFormatSymbols()Lcom/ibm/icu/text/DateFormatSymbols;

    move-result-object v2

    invoke-virtual {p0}, Lcom/ibm/icu/util/Calendar$FormatConfiguration;->getCalendar()Lcom/ibm/icu/util/Calendar;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/ibm/icu/util/Calendar$FormatConfiguration;->getLocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/SimpleDateFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/text/NumberFormat;Lcom/ibm/icu/util/ULocale;Z)V

    return-object v0
.end method

.method private getPatternItems()[Ljava/lang/Object;
    .locals 12

    .prologue
    const/16 v11, 0x27

    const/4 v9, 0x0

    .line 1056
    iget-object v8, p0, Lcom/ibm/icu/text/SimpleDateFormat;->patternItems:[Ljava/lang/Object;

    if-eqz v8, :cond_0

    .line 1057
    iget-object v8, p0, Lcom/ibm/icu/text/SimpleDateFormat;->patternItems:[Ljava/lang/Object;

    .line 1134
    :goto_0
    return-object v8

    .line 1060
    :cond_0
    sget-object v8, Lcom/ibm/icu/text/SimpleDateFormat;->PARSED_PATTERN_CACHE:Lcom/ibm/icu/impl/ICUCache;

    iget-object v10, p0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-interface {v8, v10}, Lcom/ibm/icu/impl/ICUCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/Object;

    check-cast v8, [Ljava/lang/Object;

    iput-object v8, p0, Lcom/ibm/icu/text/SimpleDateFormat;->patternItems:[Ljava/lang/Object;

    .line 1061
    iget-object v8, p0, Lcom/ibm/icu/text/SimpleDateFormat;->patternItems:[Ljava/lang/Object;

    if-eqz v8, :cond_1

    .line 1062
    iget-object v8, p0, Lcom/ibm/icu/text/SimpleDateFormat;->patternItems:[Ljava/lang/Object;

    goto :goto_0

    .line 1065
    :cond_1
    const/4 v3, 0x0

    .line 1066
    .local v3, "isPrevQuote":Z
    const/4 v2, 0x0

    .line 1067
    .local v2, "inQuote":Z
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    .line 1068
    .local v7, "text":Ljava/lang/StringBuffer;
    const/4 v5, 0x0

    .line 1069
    .local v5, "itemType":C
    const/4 v4, 0x1

    .line 1071
    .local v4, "itemLength":I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1073
    .local v6, "items":Ljava/util/List;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v8, p0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v1, v8, :cond_e

    .line 1074
    iget-object v8, p0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v8, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1075
    .local v0, "ch":C
    if-ne v0, v11, :cond_5

    .line 1076
    if-eqz v3, :cond_3

    .line 1077
    invoke-virtual {v7, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1078
    const/4 v3, 0x0

    .line 1086
    :cond_2
    :goto_2
    if-nez v2, :cond_4

    const/4 v2, 0x1

    .line 1073
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1080
    :cond_3
    const/4 v3, 0x1

    .line 1081
    if-eqz v5, :cond_2

    .line 1082
    new-instance v8, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;

    invoke-direct {v8, v5, v4}, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;-><init>(CI)V

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1083
    const/4 v5, 0x0

    goto :goto_2

    :cond_4
    move v2, v9

    .line 1086
    goto :goto_3

    .line 1088
    :cond_5
    const/4 v3, 0x0

    .line 1089
    if-eqz v2, :cond_6

    .line 1090
    invoke-virtual {v7, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 1092
    :cond_6
    const/16 v8, 0x61

    if-lt v0, v8, :cond_7

    const/16 v8, 0x7a

    if-le v0, v8, :cond_8

    :cond_7
    const/16 v8, 0x41

    if-lt v0, v8, :cond_c

    const/16 v8, 0x5a

    if-gt v0, v8, :cond_c

    .line 1094
    :cond_8
    if-ne v0, v5, :cond_9

    .line 1095
    add-int/lit8 v4, v4, 0x1

    .line 1096
    goto :goto_3

    .line 1097
    :cond_9
    if-nez v5, :cond_b

    .line 1098
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    if-lez v8, :cond_a

    .line 1099
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1100
    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1105
    :cond_a
    :goto_4
    move v5, v0

    .line 1106
    const/4 v4, 0x1

    .line 1108
    goto :goto_3

    .line 1103
    :cond_b
    new-instance v8, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;

    invoke-direct {v8, v5, v4}, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;-><init>(CI)V

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1110
    :cond_c
    if-eqz v5, :cond_d

    .line 1111
    new-instance v8, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;

    invoke-direct {v8, v5, v4}, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;-><init>(CI)V

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1112
    const/4 v5, 0x0

    .line 1114
    :cond_d
    invoke-virtual {v7, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 1120
    .end local v0    # "ch":C
    :cond_e
    if-nez v5, :cond_10

    .line 1121
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    if-lez v8, :cond_f

    .line 1122
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1123
    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1129
    :cond_f
    :goto_5
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    new-array v8, v8, [Ljava/lang/Object;

    iput-object v8, p0, Lcom/ibm/icu/text/SimpleDateFormat;->patternItems:[Ljava/lang/Object;

    .line 1130
    iget-object v8, p0, Lcom/ibm/icu/text/SimpleDateFormat;->patternItems:[Ljava/lang/Object;

    invoke-interface {v6, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1132
    sget-object v8, Lcom/ibm/icu/text/SimpleDateFormat;->PARSED_PATTERN_CACHE:Lcom/ibm/icu/impl/ICUCache;

    iget-object v9, p0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    iget-object v10, p0, Lcom/ibm/icu/text/SimpleDateFormat;->patternItems:[Ljava/lang/Object;

    invoke-interface {v8, v9, v10}, Lcom/ibm/icu/impl/ICUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1134
    iget-object v8, p0, Lcom/ibm/icu/text/SimpleDateFormat;->patternItems:[Ljava/lang/Object;

    goto/16 :goto_0

    .line 1126
    :cond_10
    new-instance v8, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;

    invoke-direct {v8, v5, v4}, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;-><init>(CI)V

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5
.end method

.method private initLocalZeroPaddingNumberFormat()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1435
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    instance-of v0, v0, Lcom/ibm/icu/text/DecimalFormat;

    if-eqz v0, :cond_1

    .line 1436
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    check-cast v0, Lcom/ibm/icu/text/DecimalFormat;

    invoke-virtual {v0}, Lcom/ibm/icu/text/DecimalFormat;->getDecimalFormatSymbols()Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getZeroDigit()C

    move-result v0

    iput-char v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->zeroDigit:C

    .line 1437
    iput-boolean v1, p0, Lcom/ibm/icu/text/SimpleDateFormat;->useLocalZeroPaddingNumberFormat:Z

    .line 1445
    :goto_0
    iget-boolean v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->useLocalZeroPaddingNumberFormat:Z

    if-eqz v0, :cond_0

    .line 1446
    const/16 v0, 0xa

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->decimalBuf:[C

    .line 1448
    :cond_0
    return-void

    .line 1438
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    instance-of v0, v0, Lcom/ibm/icu/impl/DateNumberFormat;

    if-eqz v0, :cond_2

    .line 1439
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    check-cast v0, Lcom/ibm/icu/impl/DateNumberFormat;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/DateNumberFormat;->getZeroDigit()C

    move-result v0

    iput-char v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->zeroDigit:C

    .line 1440
    iput-boolean v1, p0, Lcom/ibm/icu/text/SimpleDateFormat;->useLocalZeroPaddingNumberFormat:Z

    goto :goto_0

    .line 1442
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->useLocalZeroPaddingNumberFormat:Z

    goto :goto_0
.end method

.method private initialize()V
    .locals 3

    .prologue
    .line 458
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->locale:Lcom/ibm/icu/util/ULocale;

    if-nez v0, :cond_0

    .line 459
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 461
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    if-nez v0, :cond_1

    .line 462
    new-instance v0, Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v1, p0, Lcom/ibm/icu/text/SimpleDateFormat;->locale:Lcom/ibm/icu/util/ULocale;

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/DateFormatSymbols;-><init>(Lcom/ibm/icu/util/ULocale;)V

    iput-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    .line 464
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    if-nez v0, :cond_2

    .line 465
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->locale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v0}, Lcom/ibm/icu/util/Calendar;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    .line 467
    :cond_2
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    if-nez v0, :cond_3

    .line 469
    new-instance v0, Lcom/ibm/icu/impl/DateNumberFormat;

    iget-object v1, p0, Lcom/ibm/icu/text/SimpleDateFormat;->locale:Lcom/ibm/icu/util/ULocale;

    invoke-direct {v0, v1}, Lcom/ibm/icu/impl/DateNumberFormat;-><init>(Lcom/ibm/icu/util/ULocale;)V

    iput-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    .line 473
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyBase:J

    .line 475
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    sget-object v1, Lcom/ibm/icu/util/ULocale;->VALID_LOCALE:Lcom/ibm/icu/util/ULocale$Type;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/util/Calendar;->getLocale(Lcom/ibm/icu/util/ULocale$Type;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    iget-object v1, p0, Lcom/ibm/icu/text/SimpleDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    sget-object v2, Lcom/ibm/icu/util/ULocale;->ACTUAL_LOCALE:Lcom/ibm/icu/util/ULocale$Type;

    invoke-virtual {v1, v2}, Lcom/ibm/icu/util/Calendar;->getLocale(Lcom/ibm/icu/util/ULocale$Type;)Lcom/ibm/icu/util/ULocale;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/ibm/icu/text/SimpleDateFormat;->setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V

    .line 476
    invoke-direct {p0}, Lcom/ibm/icu/text/SimpleDateFormat;->initLocalZeroPaddingNumberFormat()V

    .line 477
    return-void
.end method

.method private initializeDefaultCenturyStart(J)V
    .locals 3
    .param p1, "baseTime"    # J

    .prologue
    const/4 v2, 0x1

    .line 518
    iput-wide p1, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyBase:J

    .line 521
    iget-object v1, p0, Lcom/ibm/icu/text/SimpleDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    invoke-virtual {v1}, Lcom/ibm/icu/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/util/Calendar;

    .line 522
    .local v0, "tmpCal":Lcom/ibm/icu/util/Calendar;
    invoke-virtual {v0, p1, p2}, Lcom/ibm/icu/util/Calendar;->setTimeInMillis(J)V

    .line 523
    const/16 v1, -0x50

    invoke-virtual {v0, v2, v1}, Lcom/ibm/icu/util/Calendar;->add(II)V

    .line 524
    invoke-virtual {v0}, Lcom/ibm/icu/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyStart:Ljava/util/Date;

    .line 525
    invoke-virtual {v0, v2}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyStartYear:I

    .line 526
    return-void
.end method

.method private isDefaultGMTFormat()Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 1377
    const-string/jumbo v3, "GMT{0}"

    iget-object v4, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-virtual {v4}, Lcom/ibm/icu/text/DateFormatSymbols;->getGmtFormat()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1378
    const/4 v0, 0x0

    .line 1390
    :cond_0
    return v0

    .line 1381
    :cond_1
    const/4 v0, 0x1

    .line 1382
    .local v0, "res":Z
    const/4 v1, 0x0

    .local v1, "sign":I
    :goto_0
    if-ge v1, v5, :cond_0

    if-eqz v0, :cond_0

    .line 1383
    const/4 v2, 0x0

    .local v2, "width":I
    :goto_1
    if-ge v2, v5, :cond_2

    .line 1384
    sget-object v3, Lcom/ibm/icu/text/DateFormatSymbols;->DEFAULT_GMT_HOUR_PATTERNS:[[Ljava/lang/String;

    aget-object v3, v3, v1

    aget-object v3, v3, v2

    iget-object v4, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-virtual {v4, v1, v2}, Lcom/ibm/icu/text/DateFormatSymbols;->getGmtHourFormat(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1385
    const/4 v0, 0x0

    .line 1382
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1383
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method static isFieldUnitIgnored(Ljava/lang/String;I)Z
    .locals 12
    .param p0, "pattern"    # Ljava/lang/String;
    .param p1, "field"    # I

    .prologue
    const/16 v11, 0x27

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2623
    sget-object v9, Lcom/ibm/icu/text/SimpleDateFormat;->CALENDAR_FIELD_TO_LEVEL:[I

    aget v2, v9, p1

    .line 2626
    .local v2, "fieldLevel":I
    const/4 v4, 0x0

    .line 2627
    .local v4, "inQuote":Z
    const/4 v6, 0x0

    .line 2628
    .local v6, "prevCh":C
    const/4 v1, 0x0

    .line 2630
    .local v1, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v3, v9, :cond_9

    .line 2631
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2632
    .local v0, "ch":C
    if-eq v0, v6, :cond_2

    if-lez v1, :cond_2

    .line 2633
    sget-object v9, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_CHAR_TO_LEVEL:[I

    add-int/lit8 v10, v6, -0x40

    aget v5, v9, v10

    .line 2634
    .local v5, "level":I
    if-gt v2, v5, :cond_1

    .line 2659
    .end local v0    # "ch":C
    .end local v5    # "level":I
    :cond_0
    :goto_1
    return v8

    .line 2637
    .restart local v0    # "ch":C
    .restart local v5    # "level":I
    :cond_1
    const/4 v1, 0x0

    .line 2639
    .end local v5    # "level":I
    :cond_2
    if-ne v0, v11, :cond_6

    .line 2640
    add-int/lit8 v9, v3, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v10

    if-ge v9, v10, :cond_4

    add-int/lit8 v9, v3, 0x1

    invoke-virtual {p0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-ne v9, v11, :cond_4

    .line 2641
    add-int/lit8 v3, v3, 0x1

    .line 2630
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2643
    :cond_4
    if-nez v4, :cond_5

    move v4, v7

    .line 2645
    :goto_3
    goto :goto_2

    :cond_5
    move v4, v8

    .line 2643
    goto :goto_3

    .line 2646
    :cond_6
    if-nez v4, :cond_3

    const/16 v9, 0x61

    if-lt v0, v9, :cond_7

    const/16 v9, 0x7a

    if-le v0, v9, :cond_8

    :cond_7
    const/16 v9, 0x41

    if-lt v0, v9, :cond_3

    const/16 v9, 0x5a

    if-gt v0, v9, :cond_3

    .line 2648
    :cond_8
    move v6, v0

    .line 2649
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2652
    .end local v0    # "ch":C
    :cond_9
    if-lez v1, :cond_a

    .line 2654
    sget-object v9, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_CHAR_TO_LEVEL:[I

    add-int/lit8 v10, v6, -0x40

    aget v5, v9, v10

    .line 2655
    .restart local v5    # "level":I
    if-le v2, v5, :cond_0

    .end local v5    # "level":I
    :cond_a
    move v8, v7

    .line 2659
    goto :goto_1
.end method

.method private static final isNumeric(CI)Z
    .locals 2
    .param p0, "formatChar"    # C
    .param p1, "count"    # I

    .prologue
    .line 1507
    const-string/jumbo v1, "MYyudehHmsSDFwWkK"

    invoke-virtual {v1, p0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1508
    .local v0, "i":I
    if-gtz v0, :cond_0

    if-nez v0, :cond_1

    const/4 v1, 0x3

    if-ge p1, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private lowerLevel([Ljava/lang/Object;II)Z
    .locals 7
    .param p1, "items"    # [Ljava/lang/Object;
    .param p2, "i"    # I
    .param p3, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2885
    aget-object v4, p1, p2

    instance-of v4, v4, Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 2904
    :cond_0
    :goto_0
    return v3

    .line 2888
    :cond_1
    aget-object v1, p1, p2

    check-cast v1, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;

    .line 2889
    .local v1, "item":Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;
    iget-char v0, v1, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->type:C

    .line 2890
    .local v0, "ch":C
    const/4 v2, -0x1

    .line 2891
    .local v2, "patternCharIndex":I
    const/16 v4, 0x41

    if-gt v4, v0, :cond_2

    const/16 v4, 0x7a

    if-gt v0, v4, :cond_2

    .line 2892
    sget-object v4, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_CHAR_TO_LEVEL:[I

    add-int/lit8 v5, v0, -0x40

    aget v2, v4, v5

    .line 2895
    :cond_2
    const/4 v4, -0x1

    if-ne v2, v4, :cond_3

    .line 2896
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "Illegal pattern character \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, "\' in \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    new-instance v5, Ljava/lang/String;

    iget-object v6, p0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const/16 v5, 0x22

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 2901
    :cond_3
    if-lt v2, p3, :cond_0

    .line 2902
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private parseAmbiguousDatesAsAfter(Ljava/util/Date;)V
    .locals 2
    .param p1, "startDate"    # Ljava/util/Date;

    .prologue
    .line 509
    iput-object p1, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyStart:Ljava/util/Date;

    .line 510
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 511
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyStartYear:I

    .line 512
    return-void
.end method

.method private parseGMT(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Integer;
    .locals 11
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;

    .prologue
    const/4 v10, 0x1

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 1188
    invoke-direct {p0}, Lcom/ibm/icu/text/SimpleDateFormat;->isDefaultGMTFormat()Z

    move-result v7

    if-nez v7, :cond_5

    .line 1189
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v6

    .line 1190
    .local v6, "start":I
    iget-object v7, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v1, v7, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    .line 1193
    .local v1, "gmtPattern":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1194
    .local v5, "prefixMatch":Z
    const/16 v7, 0x7b

    invoke-virtual {v1, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 1195
    .local v4, "prefixLen":I
    if-lez v4, :cond_0

    invoke-virtual {p1, v6, v1, v8, v4}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1196
    const/4 v5, 0x1

    .line 1199
    :cond_0
    if-eqz v5, :cond_5

    .line 1206
    invoke-direct {p0, v8, v8}, Lcom/ibm/icu/text/SimpleDateFormat;->getGMTFormatter(II)Lcom/ibm/icu/text/MessageFormat;

    move-result-object v0

    .line 1207
    .local v0, "fmt":Lcom/ibm/icu/text/MessageFormat;
    invoke-virtual {v0, p1, p2}, Lcom/ibm/icu/text/MessageFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)[Ljava/lang/Object;

    move-result-object v3

    .line 1208
    .local v3, "parsedObjects":[Ljava/lang/Object;
    if-eqz v3, :cond_1

    aget-object v7, v3, v8

    instance-of v7, v7, Ljava/util/Date;

    if-eqz v7, :cond_1

    .line 1209
    aget-object v7, v3, v8

    check-cast v7, Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    long-to-int v2, v8

    .line 1210
    .local v2, "offset":I
    new-instance v7, Ljava/lang/Integer;

    neg-int v8, v2

    invoke-direct {v7, v8}, Ljava/lang/Integer;-><init>(I)V

    .line 1255
    .end local v0    # "fmt":Lcom/ibm/icu/text/MessageFormat;
    .end local v1    # "gmtPattern":Ljava/lang/String;
    .end local v2    # "offset":I
    .end local v3    # "parsedObjects":[Ljava/lang/Object;
    .end local v4    # "prefixLen":I
    .end local v5    # "prefixMatch":Z
    .end local v6    # "start":I
    :goto_0
    return-object v7

    .line 1214
    .restart local v0    # "fmt":Lcom/ibm/icu/text/MessageFormat;
    .restart local v1    # "gmtPattern":Ljava/lang/String;
    .restart local v3    # "parsedObjects":[Ljava/lang/Object;
    .restart local v4    # "prefixLen":I
    .restart local v5    # "prefixMatch":Z
    .restart local v6    # "start":I
    :cond_1
    invoke-virtual {p2, v6}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1215
    invoke-virtual {p2, v9}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 1218
    invoke-direct {p0, v10, v8}, Lcom/ibm/icu/text/SimpleDateFormat;->getGMTFormatter(II)Lcom/ibm/icu/text/MessageFormat;

    move-result-object v0

    .line 1219
    invoke-virtual {v0, p1, p2}, Lcom/ibm/icu/text/MessageFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)[Ljava/lang/Object;

    move-result-object v3

    .line 1220
    if-eqz v3, :cond_2

    aget-object v7, v3, v8

    instance-of v7, v7, Ljava/util/Date;

    if-eqz v7, :cond_2

    .line 1221
    aget-object v7, v3, v8

    check-cast v7, Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    long-to-int v2, v8

    .line 1222
    .restart local v2    # "offset":I
    new-instance v7, Ljava/lang/Integer;

    invoke-direct {v7, v2}, Ljava/lang/Integer;-><init>(I)V

    goto :goto_0

    .line 1226
    .end local v2    # "offset":I
    :cond_2
    invoke-virtual {p2, v6}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1227
    invoke-virtual {p2, v9}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 1230
    invoke-direct {p0, v8, v10}, Lcom/ibm/icu/text/SimpleDateFormat;->getGMTFormatter(II)Lcom/ibm/icu/text/MessageFormat;

    move-result-object v0

    .line 1231
    invoke-virtual {v0, p1, p2}, Lcom/ibm/icu/text/MessageFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)[Ljava/lang/Object;

    move-result-object v3

    .line 1232
    if-eqz v3, :cond_3

    aget-object v7, v3, v8

    instance-of v7, v7, Ljava/util/Date;

    if-eqz v7, :cond_3

    .line 1233
    aget-object v7, v3, v8

    check-cast v7, Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    long-to-int v2, v8

    .line 1234
    .restart local v2    # "offset":I
    new-instance v7, Ljava/lang/Integer;

    neg-int v8, v2

    invoke-direct {v7, v8}, Ljava/lang/Integer;-><init>(I)V

    goto :goto_0

    .line 1238
    .end local v2    # "offset":I
    :cond_3
    invoke-virtual {p2, v6}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1239
    invoke-virtual {p2, v9}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 1242
    invoke-direct {p0, v10, v10}, Lcom/ibm/icu/text/SimpleDateFormat;->getGMTFormatter(II)Lcom/ibm/icu/text/MessageFormat;

    move-result-object v0

    .line 1243
    invoke-virtual {v0, p1, p2}, Lcom/ibm/icu/text/MessageFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)[Ljava/lang/Object;

    move-result-object v3

    .line 1244
    if-eqz v3, :cond_4

    aget-object v7, v3, v8

    instance-of v7, v7, Ljava/util/Date;

    if-eqz v7, :cond_4

    .line 1245
    aget-object v7, v3, v8

    check-cast v7, Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    long-to-int v2, v8

    .line 1246
    .restart local v2    # "offset":I
    new-instance v7, Ljava/lang/Integer;

    invoke-direct {v7, v2}, Ljava/lang/Integer;-><init>(I)V

    goto :goto_0

    .line 1250
    .end local v2    # "offset":I
    :cond_4
    invoke-virtual {p2, v6}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1251
    invoke-virtual {p2, v9}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 1255
    .end local v0    # "fmt":Lcom/ibm/icu/text/MessageFormat;
    .end local v1    # "gmtPattern":Ljava/lang/String;
    .end local v3    # "parsedObjects":[Ljava/lang/Object;
    .end local v4    # "prefixLen":I
    .end local v5    # "prefixMatch":Z
    .end local v6    # "start":I
    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/SimpleDateFormat;->parseGMTDefault(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Integer;

    move-result-object v7

    goto :goto_0
.end method

.method private parseGMTDefault(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Integer;
    .locals 18
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;

    .prologue
    .line 1259
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v5

    .line 1261
    .local v5, "start":I
    add-int/lit8 v3, v5, 0x3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v3, v4, :cond_0

    .line 1262
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 1263
    const/4 v3, 0x0

    .line 1350
    :goto_0
    return-object v3

    .line 1266
    :cond_0
    move v9, v5

    .line 1268
    .local v9, "cur":I
    const/4 v4, 0x1

    const-string/jumbo v6, "GMT"

    const/4 v7, 0x0

    const/4 v8, 0x3

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1269
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 1270
    const/4 v3, 0x0

    goto :goto_0

    .line 1272
    :cond_1
    add-int/lit8 v9, v9, 0x3

    .line 1274
    const/4 v13, 0x0

    .line 1275
    .local v13, "negative":Z
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x2d

    if-ne v3, v4, :cond_4

    .line 1276
    const/4 v13, 0x1

    .line 1281
    :cond_2
    add-int/lit8 v9, v9, 0x1

    .line 1285
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1287
    const/4 v3, 0x6

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v3, v2, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->parseInt(Ljava/lang/String;ILjava/text/ParsePosition;Z)Ljava/lang/Number;

    move-result-object v12

    .line 1288
    .local v12, "n":Ljava/lang/Number;
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v3

    sub-int v14, v3, v9

    .line 1290
    .local v14, "numLen":I
    if-eqz v12, :cond_3

    if-lez v14, :cond_3

    const/4 v3, 0x6

    if-le v14, v3, :cond_5

    .line 1291
    :cond_3
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1292
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 1293
    const/4 v3, 0x0

    goto :goto_0

    .line 1277
    .end local v12    # "n":Ljava/lang/Number;
    .end local v14    # "numLen":I
    :cond_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x2b

    if-eq v3, v4, :cond_2

    .line 1278
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 1279
    const/4 v3, 0x0

    goto :goto_0

    .line 1296
    .restart local v12    # "n":Ljava/lang/Number;
    .restart local v14    # "numLen":I
    :cond_5
    invoke-virtual {v12}, Ljava/lang/Number;->intValue()I

    move-result v15

    .line 1298
    .local v15, "numVal":I
    const/4 v10, 0x0

    .line 1299
    .local v10, "hour":I
    const/4 v11, 0x0

    .line 1300
    .local v11, "min":I
    const/16 v17, 0x0

    .line 1302
    .local v17, "sec":I
    const/4 v3, 0x2

    if-gt v14, v3, :cond_a

    .line 1304
    move v10, v15

    .line 1305
    add-int/2addr v9, v14

    .line 1306
    add-int/lit8 v3, v9, 0x2

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_6

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x3a

    if-ne v3, v4, :cond_6

    .line 1307
    add-int/lit8 v9, v9, 0x1

    .line 1308
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1309
    const/4 v3, 0x2

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v3, v2, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->parseInt(Ljava/lang/String;ILjava/text/ParsePosition;Z)Ljava/lang/Number;

    move-result-object v12

    .line 1310
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v3

    sub-int v14, v3, v9

    .line 1311
    if-eqz v12, :cond_9

    const/4 v3, 0x2

    if-ne v14, v3, :cond_9

    .line 1313
    invoke-virtual {v12}, Ljava/lang/Number;->intValue()I

    move-result v11

    .line 1314
    add-int/2addr v9, v14

    .line 1315
    add-int/lit8 v3, v9, 0x2

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_6

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x3a

    if-ne v3, v4, :cond_6

    .line 1316
    add-int/lit8 v9, v9, 0x1

    .line 1317
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1318
    const/4 v3, 0x2

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v3, v2, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->parseInt(Ljava/lang/String;ILjava/text/ParsePosition;Z)Ljava/lang/Number;

    move-result-object v12

    .line 1319
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v3

    sub-int v14, v3, v9

    .line 1320
    if-eqz v12, :cond_8

    const/4 v3, 0x2

    if-ne v14, v3, :cond_8

    .line 1322
    invoke-virtual {v12}, Ljava/lang/Number;->intValue()I

    move-result v17

    .line 1346
    :cond_6
    :goto_1
    mul-int/lit8 v3, v10, 0x3c

    add-int/2addr v3, v11

    mul-int/lit8 v3, v3, 0x3c

    add-int v3, v3, v17

    mul-int/lit16 v0, v3, 0x3e8

    move/from16 v16, v0

    .line 1347
    .local v16, "offset":I
    if-eqz v13, :cond_7

    .line 1348
    move/from16 v0, v16

    neg-int v0, v0

    move/from16 v16, v0

    .line 1350
    :cond_7
    new-instance v3, Ljava/lang/Integer;

    move/from16 v0, v16

    invoke-direct {v3, v0}, Ljava/lang/Integer;-><init>(I)V

    goto/16 :goto_0

    .line 1325
    .end local v16    # "offset":I
    :cond_8
    add-int/lit8 v3, v9, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1326
    const/4 v3, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    goto :goto_1

    .line 1331
    :cond_9
    add-int/lit8 v3, v9, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1332
    const/4 v3, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    goto :goto_1

    .line 1335
    :cond_a
    const/4 v3, 0x3

    if-eq v14, v3, :cond_b

    const/4 v3, 0x4

    if-ne v14, v3, :cond_c

    .line 1337
    :cond_b
    div-int/lit8 v10, v15, 0x64

    .line 1338
    rem-int/lit8 v11, v15, 0x64

    .line 1339
    goto :goto_1

    .line 1341
    :cond_c
    div-int/lit16 v10, v15, 0x2710

    .line 1342
    rem-int/lit16 v3, v15, 0x2710

    div-int/lit8 v11, v3, 0x64

    .line 1343
    rem-int/lit8 v17, v15, 0x64

    goto :goto_1
.end method

.method private parseInt(Ljava/lang/String;ILjava/text/ParsePosition;Z)Ljava/lang/Number;
    .locals 10
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "maxDigits"    # I
    .param p3, "pos"    # Ljava/text/ParsePosition;
    .param p4, "allowNegative"    # Z

    .prologue
    .line 2331
    invoke-virtual {p3}, Ljava/text/ParsePosition;->getIndex()I

    move-result v3

    .line 2332
    .local v3, "oldPos":I
    if-eqz p4, :cond_1

    .line 2333
    iget-object v5, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    invoke-virtual {v5, p1, p3}, Lcom/ibm/icu/text/NumberFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v2

    .line 2352
    .local v2, "number":Ljava/lang/Number;
    :cond_0
    :goto_0
    if-lez p2, :cond_5

    .line 2355
    invoke-virtual {p3}, Ljava/text/ParsePosition;->getIndex()I

    move-result v5

    sub-int v1, v5, v3

    .line 2356
    .local v1, "nDigits":I
    if-le v1, p2, :cond_5

    .line 2357
    invoke-virtual {v2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v6

    .line 2358
    .local v6, "val":D
    sub-int/2addr v1, p2

    .line 2359
    :goto_1
    if-lez v1, :cond_4

    .line 2360
    const-wide/high16 v8, 0x4024000000000000L    # 10.0

    div-double/2addr v6, v8

    .line 2361
    add-int/lit8 v1, v1, -0x1

    .line 2362
    goto :goto_1

    .line 2336
    .end local v1    # "nDigits":I
    .end local v2    # "number":Ljava/lang/Number;
    .end local v6    # "val":D
    :cond_1
    iget-object v5, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    instance-of v5, v5, Lcom/ibm/icu/text/DecimalFormat;

    if-eqz v5, :cond_2

    .line 2337
    iget-object v5, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    check-cast v5, Lcom/ibm/icu/text/DecimalFormat;

    invoke-virtual {v5}, Lcom/ibm/icu/text/DecimalFormat;->getNegativePrefix()Ljava/lang/String;

    move-result-object v4

    .line 2338
    .local v4, "oldPrefix":Ljava/lang/String;
    iget-object v5, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    check-cast v5, Lcom/ibm/icu/text/DecimalFormat;

    const-string/jumbo v8, "\uab00"

    invoke-virtual {v5, v8}, Lcom/ibm/icu/text/DecimalFormat;->setNegativePrefix(Ljava/lang/String;)V

    .line 2339
    iget-object v5, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    invoke-virtual {v5, p1, p3}, Lcom/ibm/icu/text/NumberFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v2

    .line 2340
    .restart local v2    # "number":Ljava/lang/Number;
    iget-object v5, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    check-cast v5, Lcom/ibm/icu/text/DecimalFormat;

    invoke-virtual {v5, v4}, Lcom/ibm/icu/text/DecimalFormat;->setNegativePrefix(Ljava/lang/String;)V

    goto :goto_0

    .line 2342
    .end local v2    # "number":Ljava/lang/Number;
    .end local v4    # "oldPrefix":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    instance-of v0, v5, Lcom/ibm/icu/impl/DateNumberFormat;

    .line 2343
    .local v0, "dateNumberFormat":Z
    if-eqz v0, :cond_3

    .line 2344
    iget-object v5, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    check-cast v5, Lcom/ibm/icu/impl/DateNumberFormat;

    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Lcom/ibm/icu/impl/DateNumberFormat;->setParsePositiveOnly(Z)V

    .line 2346
    :cond_3
    iget-object v5, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    invoke-virtual {v5, p1, p3}, Lcom/ibm/icu/text/NumberFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v2

    .line 2347
    .restart local v2    # "number":Ljava/lang/Number;
    if-eqz v0, :cond_0

    .line 2348
    iget-object v5, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    check-cast v5, Lcom/ibm/icu/impl/DateNumberFormat;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/ibm/icu/impl/DateNumberFormat;->setParsePositiveOnly(Z)V

    goto :goto_0

    .line 2363
    .end local v0    # "dateNumberFormat":Z
    .restart local v1    # "nDigits":I
    .restart local v6    # "val":D
    :cond_4
    add-int v5, v3, p2

    invoke-virtual {p3, v5}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 2364
    new-instance v2, Ljava/lang/Integer;

    .end local v2    # "number":Ljava/lang/Number;
    double-to-int v5, v6

    invoke-direct {v2, v5}, Ljava/lang/Integer;-><init>(I)V

    .line 2367
    .end local v1    # "nDigits":I
    .end local v6    # "val":D
    .restart local v2    # "number":Ljava/lang/Number;
    :cond_5
    return-object v2
.end method

.method private parseInt(Ljava/lang/String;Ljava/text/ParsePosition;Z)Ljava/lang/Number;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;
    .param p3, "allowNegative"    # Z

    .prologue
    .line 2320
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/ibm/icu/text/SimpleDateFormat;->parseInt(Ljava/lang/String;ILjava/text/ParsePosition;Z)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 3
    .param p1, "stream"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2524
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 2527
    iget v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->serialVersionOnStream:I

    if-ge v0, v2, :cond_0

    .line 2529
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyBase:J

    .line 2536
    :goto_0
    iput v2, p0, Lcom/ibm/icu/text/SimpleDateFormat;->serialVersionOnStream:I

    .line 2537
    sget-object v0, Lcom/ibm/icu/util/ULocale;->VALID_LOCALE:Lcom/ibm/icu/util/ULocale$Type;

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/SimpleDateFormat;->getLocale(Lcom/ibm/icu/util/ULocale$Type;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->locale:Lcom/ibm/icu/util/ULocale;

    .line 2539
    invoke-direct {p0}, Lcom/ibm/icu/text/SimpleDateFormat;->initLocalZeroPaddingNumberFormat()V

    .line 2540
    return-void

    .line 2534
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyStart:Ljava/util/Date;

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/SimpleDateFormat;->parseAmbiguousDatesAsAfter(Ljava/util/Date;)V

    goto :goto_0
.end method

.method private static safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V
    .locals 1
    .param p0, "array"    # [Ljava/lang/String;
    .param p1, "value"    # I
    .param p2, "appendTo"    # Ljava/lang/StringBuffer;

    .prologue
    .line 1028
    if-eqz p0, :cond_0

    if-ltz p1, :cond_0

    array-length v0, p0

    if-ge p1, v0, :cond_0

    .line 1029
    aget-object v0, p0, p1

    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1031
    :cond_0
    return-void
.end method

.method private translatePattern(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "pat"    # Ljava/lang/String;
    .param p2, "from"    # Ljava/lang/String;
    .param p3, "to"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x27

    .line 2376
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 2377
    .local v4, "result":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .line 2378
    .local v3, "inQuote":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v2, v5, :cond_5

    .line 2379
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2380
    .local v0, "c":C
    if-eqz v3, :cond_1

    .line 2381
    if-ne v0, v6, :cond_0

    .line 2382
    const/4 v3, 0x0

    .line 2396
    :cond_0
    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2378
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2385
    :cond_1
    if-ne v0, v6, :cond_2

    .line 2386
    const/4 v3, 0x1

    goto :goto_1

    .line 2387
    :cond_2
    const/16 v5, 0x61

    if-lt v0, v5, :cond_3

    const/16 v5, 0x7a

    if-le v0, v5, :cond_4

    :cond_3
    const/16 v5, 0x41

    if-lt v0, v5, :cond_0

    const/16 v5, 0x5a

    if-gt v0, v5, :cond_0

    .line 2388
    :cond_4
    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 2389
    .local v1, "ci":I
    const/4 v5, -0x1

    if-eq v1, v5, :cond_0

    .line 2390
    invoke-virtual {p3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_1

    .line 2398
    .end local v0    # "c":C
    .end local v1    # "ci":I
    :cond_5
    if-eqz v3, :cond_6

    .line 2399
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v6, "Unfinished quote in pattern"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2400
    :cond_6
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2
    .param p1, "stream"    # Ljava/io/ObjectOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2511
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyStart:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 2514
    iget-wide v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->defaultCenturyBase:J

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/SimpleDateFormat;->initializeDefaultCenturyStart(J)V

    .line 2516
    :cond_0
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 2517
    return-void
.end method


# virtual methods
.method public applyLocalizedPattern(Ljava/lang/String;)V
    .locals 3
    .param p1, "pat"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 2438
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->localPatternChars:Ljava/lang/String;

    const-string/jumbo v1, "GyMdkHmsSEDFwWahKzYeugAZvcLQqV"

    invoke-direct {p0, p1, v0, v1}, Lcom/ibm/icu/text/SimpleDateFormat;->translatePattern(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    .line 2441
    invoke-virtual {p0, v2, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V

    .line 2442
    return-void
.end method

.method public applyPattern(Ljava/lang/String;)V
    .locals 1
    .param p1, "pat"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 2427
    iput-object p1, p0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    .line 2428
    invoke-virtual {p0, v0, v0}, Lcom/ibm/icu/text/SimpleDateFormat;->setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V

    .line 2430
    iput-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->patternItems:[Ljava/lang/Object;

    .line 2431
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2479
    invoke-super {p0}, Lcom/ibm/icu/text/DateFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/SimpleDateFormat;

    .line 2480
    .local v0, "other":Lcom/ibm/icu/text/SimpleDateFormat;
    iget-object v1, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-virtual {v1}, Lcom/ibm/icu/text/DateFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/DateFormatSymbols;

    iput-object v1, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    .line 2481
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 2501
    invoke-super {p0, p1}, Lcom/ibm/icu/text/DateFormat;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2503
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 2502
    check-cast v0, Lcom/ibm/icu/text/SimpleDateFormat;

    .line 2503
    .local v0, "that":Lcom/ibm/icu/text/SimpleDateFormat;
    iget-object v2, p0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    iget-object v3, v0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v3, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/DateFormatSymbols;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 1
    .param p1, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 584
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/ibm/icu/text/SimpleDateFormat;->format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public formatToCharacterIterator(Ljava/lang/Object;)Ljava/text/AttributedCharacterIterator;
    .locals 12
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 2554
    iget-object v3, p0, Lcom/ibm/icu/text/SimpleDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    .line 2555
    .local v3, "cal":Lcom/ibm/icu/util/Calendar;
    instance-of v8, p1, Lcom/ibm/icu/util/Calendar;

    if-eqz v8, :cond_0

    move-object v3, p1

    .line 2556
    check-cast v3, Lcom/ibm/icu/util/Calendar;

    .line 2564
    .end local p1    # "obj":Ljava/lang/Object;
    :goto_0
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    .line 2565
    .local v7, "toAppendTo":Ljava/lang/StringBuffer;
    new-instance v6, Ljava/text/FieldPosition;

    const/4 v8, 0x0

    invoke-direct {v6, v8}, Ljava/text/FieldPosition;-><init>(I)V

    .line 2566
    .local v6, "pos":Ljava/text/FieldPosition;
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 2567
    .local v2, "attributes":Ljava/util/List;
    invoke-direct {p0, v3, v7, v6, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;

    .line 2569
    new-instance v0, Ljava/text/AttributedString;

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v8}, Ljava/text/AttributedString;-><init>(Ljava/lang/String;)V

    .line 2572
    .local v0, "as":Ljava/text/AttributedString;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v8

    if-ge v5, v8, :cond_3

    .line 2573
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/text/FieldPosition;

    .line 2574
    .local v4, "fp":Ljava/text/FieldPosition;
    invoke-virtual {v4}, Ljava/text/FieldPosition;->getFieldAttribute()Ljava/text/Format$Field;

    move-result-object v1

    .line 2575
    .local v1, "attribute":Ljava/text/Format$Field;
    invoke-virtual {v4}, Ljava/text/FieldPosition;->getBeginIndex()I

    move-result v8

    invoke-virtual {v4}, Ljava/text/FieldPosition;->getEndIndex()I

    move-result v9

    invoke-virtual {v0, v1, v1, v8, v9}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;II)V

    .line 2572
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 2557
    .end local v0    # "as":Ljava/text/AttributedString;
    .end local v1    # "attribute":Ljava/text/Format$Field;
    .end local v2    # "attributes":Ljava/util/List;
    .end local v4    # "fp":Ljava/text/FieldPosition;
    .end local v5    # "i":I
    .end local v6    # "pos":Ljava/text/FieldPosition;
    .end local v7    # "toAppendTo":Ljava/lang/StringBuffer;
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_0
    instance-of v8, p1, Ljava/util/Date;

    if-eqz v8, :cond_1

    .line 2558
    iget-object v8, p0, Lcom/ibm/icu/text/SimpleDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    check-cast p1, Ljava/util/Date;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {v8, p1}, Lcom/ibm/icu/util/Calendar;->setTime(Ljava/util/Date;)V

    goto :goto_0

    .line 2559
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    instance-of v8, p1, Ljava/lang/Number;

    if-eqz v8, :cond_2

    .line 2560
    iget-object v8, p0, Lcom/ibm/icu/text/SimpleDateFormat;->calendar:Lcom/ibm/icu/util/Calendar;

    check-cast p1, Ljava/lang/Number;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lcom/ibm/icu/util/Calendar;->setTimeInMillis(J)V

    goto :goto_0

    .line 2562
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_2
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v9, "Cannot format given Object as a Date"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2578
    .end local p1    # "obj":Ljava/lang/Object;
    .restart local v0    # "as":Ljava/text/AttributedString;
    .restart local v2    # "attributes":Ljava/util/List;
    .restart local v5    # "i":I
    .restart local v6    # "pos":Ljava/text/FieldPosition;
    .restart local v7    # "toAppendTo":Ljava/lang/StringBuffer;
    :cond_3
    invoke-virtual {v0}, Ljava/text/AttributedString;->getIterator()Ljava/text/AttributedCharacterIterator;

    move-result-object v8

    return-object v8
.end method

.method public get2DigitYearStart()Ljava/util/Date;
    .locals 1

    .prologue
    .line 565
    invoke-direct {p0}, Lcom/ibm/icu/text/SimpleDateFormat;->getDefaultCenturyStart()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getDateFormatSymbols()Lcom/ibm/icu/text/DateFormatSymbols;
    .locals 1

    .prologue
    .line 2452
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-virtual {v0}, Lcom/ibm/icu/text/DateFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/DateFormatSymbols;

    return-object v0
.end method

.method getLocale()Lcom/ibm/icu/util/ULocale;
    .locals 1

    .prologue
    .line 2591
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->locale:Lcom/ibm/icu/util/ULocale;

    return-object v0
.end method

.method protected getSymbols()Lcom/ibm/icu/text/DateFormatSymbols;
    .locals 1

    .prologue
    .line 2471
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 2491
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final intervalFormatByAlgorithm(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 21
    .param p1, "fromCalendar"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "toCalendar"    # Lcom/ibm/icu/util/Calendar;
    .param p3, "appendTo"    # Ljava/lang/StringBuffer;
    .param p4, "pos"    # Ljava/text/FieldPosition;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 2687
    invoke-virtual/range {p1 .. p2}, Lcom/ibm/icu/util/Calendar;->isEquivalentTo(Lcom/ibm/icu/util/Calendar;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2688
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v6, "can not format on two different calendars"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2691
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/SimpleDateFormat;->getPatternItems()[Ljava/lang/Object;

    move-result-object v19

    .line 2692
    .local v19, "items":[Ljava/lang/Object;
    const/4 v13, -0x1

    .line 2693
    .local v13, "diffBegin":I
    const/4 v14, -0x1

    .line 2698
    .local v14, "diffEnd":I
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    :try_start_0
    move-object/from16 v0, v19

    array-length v5, v0

    move/from16 v0, v17

    if-ge v0, v5, :cond_1

    .line 2699
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v19

    move/from16 v4, v17

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->diffCalFieldValue(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/Calendar;[Ljava/lang/Object;I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2700
    move/from16 v13, v17

    .line 2705
    :cond_1
    const/4 v5, -0x1

    if-ne v13, v5, :cond_4

    .line 2707
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/ibm/icu/text/SimpleDateFormat;->format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object p3

    .line 2820
    .end local p3    # "appendTo":Ljava/lang/StringBuffer;
    :cond_2
    :goto_1
    return-object p3

    .line 2698
    .restart local p3    # "appendTo":Ljava/lang/StringBuffer;
    :cond_3
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 2711
    :cond_4
    move-object/from16 v0, v19

    array-length v5, v0

    add-int/lit8 v17, v5, -0x1

    :goto_2
    move/from16 v0, v17

    if-lt v0, v13, :cond_5

    .line 2712
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v19

    move/from16 v4, v17

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->diffCalFieldValue(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/Calendar;[Ljava/lang/Object;I)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_6

    .line 2713
    move/from16 v14, v17

    .line 2722
    :cond_5
    if-nez v13, :cond_7

    move-object/from16 v0, v19

    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    if-ne v14, v5, :cond_7

    .line 2723
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/ibm/icu/text/SimpleDateFormat;->format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 2724
    const-string/jumbo v5, " \u2013 "

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2725
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/ibm/icu/text/SimpleDateFormat;->format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 2711
    :cond_6
    add-int/lit8 v17, v17, -0x1

    goto :goto_2

    .line 2717
    :catch_0
    move-exception v15

    .line 2718
    .local v15, "e":Ljava/lang/IllegalArgumentException;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v15}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2731
    .end local v15    # "e":Ljava/lang/IllegalArgumentException;
    :cond_7
    const/16 v16, 0x3e8

    .line 2732
    .local v16, "highestLevel":I
    move/from16 v17, v13

    :goto_3
    move/from16 v0, v17

    if-gt v0, v14, :cond_c

    .line 2733
    aget-object v5, v19, v17

    instance-of v5, v5, Ljava/lang/String;

    if-eqz v5, :cond_9

    .line 2732
    :cond_8
    :goto_4
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 2736
    :cond_9
    aget-object v18, v19, v17

    check-cast v18, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;

    .line 2737
    .local v18, "item":Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;
    move-object/from16 v0, v18

    iget-char v12, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->type:C

    .line 2738
    .local v12, "ch":C
    const/16 v20, -0x1

    .line 2739
    .local v20, "patternCharIndex":I
    const/16 v5, 0x41

    if-gt v5, v12, :cond_a

    const/16 v5, 0x7a

    if-gt v12, v5, :cond_a

    .line 2740
    sget-object v5, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_CHAR_TO_LEVEL:[I

    add-int/lit8 v6, v12, -0x40

    aget v20, v5, v6

    .line 2743
    :cond_a
    const/4 v5, -0x1

    move/from16 v0, v20

    if-ne v0, v5, :cond_b

    .line 2744
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "Illegal pattern character \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "\' in \""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    new-instance v7, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const/16 v7, 0x22

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2749
    :cond_b
    move/from16 v0, v20

    move/from16 v1, v16

    if-ge v0, v1, :cond_8

    .line 2750
    move/from16 v16, v20

    goto :goto_4

    .line 2758
    .end local v12    # "ch":C
    .end local v18    # "item":Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;
    .end local v20    # "patternCharIndex":I
    :cond_c
    const/16 v17, 0x0

    :goto_5
    move/from16 v0, v17

    if-ge v0, v13, :cond_d

    .line 2759
    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v17

    move/from16 v3, v16

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/SimpleDateFormat;->lowerLevel([Ljava/lang/Object;II)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 2760
    move/from16 v13, v17

    .line 2766
    :cond_d
    move-object/from16 v0, v19

    array-length v5, v0

    add-int/lit8 v17, v5, -0x1

    :goto_6
    move/from16 v0, v17

    if-le v0, v14, :cond_e

    .line 2767
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v17

    move/from16 v3, v16

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/SimpleDateFormat;->lowerLevel([Ljava/lang/Object;II)Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    if-eqz v5, :cond_10

    .line 2768
    move/from16 v14, v17

    .line 2778
    :cond_e
    if-nez v13, :cond_11

    move-object/from16 v0, v19

    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    if-ne v14, v5, :cond_11

    .line 2779
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/ibm/icu/text/SimpleDateFormat;->format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 2780
    const-string/jumbo v5, " \u2013 "

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2781
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/ibm/icu/text/SimpleDateFormat;->format(Lcom/ibm/icu/util/Calendar;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 2758
    :cond_f
    add-int/lit8 v17, v17, 0x1

    goto :goto_5

    .line 2766
    :cond_10
    add-int/lit8 v17, v17, -0x1

    goto :goto_6

    .line 2772
    :catch_1
    move-exception v15

    .line 2773
    .restart local v15    # "e":Ljava/lang/IllegalArgumentException;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v15}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2788
    .end local v15    # "e":Ljava/lang/IllegalArgumentException;
    :cond_11
    const/4 v5, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 2789
    const/4 v5, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 2792
    const/16 v17, 0x0

    :goto_7
    move/from16 v0, v17

    if-gt v0, v14, :cond_14

    .line 2793
    aget-object v5, v19, v17

    instance-of v5, v5, Ljava/lang/String;

    if-eqz v5, :cond_12

    .line 2794
    aget-object v5, v19, v17

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2792
    :goto_8
    add-int/lit8 v17, v17, 0x1

    goto :goto_7

    .line 2796
    :cond_12
    aget-object v18, v19, v17

    check-cast v18, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;

    .line 2797
    .restart local v18    # "item":Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->useFastFormat:Z

    if-eqz v5, :cond_13

    .line 2798
    move-object/from16 v0, v18

    iget-char v7, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->type:C

    move-object/from16 v0, v18

    iget v8, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->length:I

    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuffer;->length()I

    move-result v9

    move-object/from16 v5, p0

    move-object/from16 v6, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p1

    invoke-virtual/range {v5 .. v11}, Lcom/ibm/icu/text/SimpleDateFormat;->subFormat(Ljava/lang/StringBuffer;CIILjava/text/FieldPosition;Lcom/ibm/icu/util/Calendar;)V

    goto :goto_8

    .line 2800
    :cond_13
    move-object/from16 v0, v18

    iget-char v6, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->type:C

    move-object/from16 v0, v18

    iget v7, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->length:I

    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v5, p0

    move-object/from16 v9, p4

    move-object/from16 v11, p1

    invoke-virtual/range {v5 .. v11}, Lcom/ibm/icu/text/SimpleDateFormat;->subFormat(CIILjava/text/FieldPosition;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_8

    .line 2805
    .end local v18    # "item":Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;
    :cond_14
    const-string/jumbo v5, " \u2013 "

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2808
    move/from16 v17, v13

    :goto_9
    move-object/from16 v0, v19

    array-length v5, v0

    move/from16 v0, v17

    if-ge v0, v5, :cond_2

    .line 2809
    aget-object v5, v19, v17

    instance-of v5, v5, Ljava/lang/String;

    if-eqz v5, :cond_15

    .line 2810
    aget-object v5, v19, v17

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2808
    :goto_a
    add-int/lit8 v17, v17, 0x1

    goto :goto_9

    .line 2812
    :cond_15
    aget-object v18, v19, v17

    check-cast v18, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;

    .line 2813
    .restart local v18    # "item":Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->useFastFormat:Z

    if-eqz v5, :cond_16

    .line 2814
    move-object/from16 v0, v18

    iget-char v7, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->type:C

    move-object/from16 v0, v18

    iget v8, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->length:I

    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuffer;->length()I

    move-result v9

    move-object/from16 v5, p0

    move-object/from16 v6, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p2

    invoke-virtual/range {v5 .. v11}, Lcom/ibm/icu/text/SimpleDateFormat;->subFormat(Ljava/lang/StringBuffer;CIILjava/text/FieldPosition;Lcom/ibm/icu/util/Calendar;)V

    goto :goto_a

    .line 2816
    :cond_16
    move-object/from16 v0, v18

    iget-char v6, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->type:C

    move-object/from16 v0, v18

    iget v7, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->length:I

    invoke-virtual/range {p3 .. p3}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v5, p0

    move-object/from16 v9, p4

    move-object/from16 v11, p2

    invoke-virtual/range {v5 .. v11}, Lcom/ibm/icu/text/SimpleDateFormat;->subFormat(CIILjava/text/FieldPosition;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_a
.end method

.method isFieldUnitIgnored(I)Z
    .locals 1
    .param p1, "field"    # I

    .prologue
    .line 2607
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/ibm/icu/text/SimpleDateFormat;->isFieldUnitIgnored(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method protected matchQuarterString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I
    .locals 10
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "field"    # I
    .param p4, "data"    # [Ljava/lang/String;
    .param p5, "cal"    # Lcom/ibm/icu/util/Calendar;

    .prologue
    .line 1861
    const/4 v9, 0x0

    .line 1862
    .local v9, "i":I
    array-length v8, p4

    .line 1868
    .local v8, "count":I
    const/4 v7, 0x0

    .local v7, "bestMatchLength":I
    const/4 v6, -0x1

    .line 1869
    .local v6, "bestMatch":I
    :goto_0
    if-ge v9, v8, :cond_1

    .line 1870
    aget-object v0, p4, v9

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    .line 1873
    .local v5, "length":I
    if-le v5, v7, :cond_0

    const/4 v1, 0x1

    aget-object v3, p4, v9

    const/4 v4, 0x0

    move-object v0, p1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1875
    move v6, v9

    .line 1876
    move v7, v5

    .line 1869
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 1880
    .end local v5    # "length":I
    :cond_1
    if-ltz v6, :cond_2

    .line 1881
    mul-int/lit8 v0, v6, 0x3

    invoke-virtual {p5, p3, v0}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 1882
    add-int v0, p2, v7

    .line 1885
    :goto_1
    return v0

    :cond_2
    neg-int v0, p2

    goto :goto_1
.end method

.method protected matchString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I
    .locals 10
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "field"    # I
    .param p4, "data"    # [Ljava/lang/String;
    .param p5, "cal"    # Lcom/ibm/icu/util/Calendar;

    .prologue
    .line 1812
    const/4 v9, 0x0

    .line 1813
    .local v9, "i":I
    array-length v8, p4

    .line 1815
    .local v8, "count":I
    const/4 v0, 0x7

    if-ne p3, v0, :cond_0

    const/4 v9, 0x1

    .line 1821
    :cond_0
    const/4 v7, 0x0

    .local v7, "bestMatchLength":I
    const/4 v6, -0x1

    .line 1822
    .local v6, "bestMatch":I
    :goto_0
    if-ge v9, v8, :cond_2

    .line 1824
    aget-object v0, p4, v9

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    .line 1827
    .local v5, "length":I
    if-le v5, v7, :cond_1

    const/4 v1, 0x1

    aget-object v3, p4, v9

    const/4 v4, 0x0

    move-object v0, p1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1830
    move v6, v9

    .line 1831
    move v7, v5

    .line 1822
    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 1834
    .end local v5    # "length":I
    :cond_2
    if-ltz v6, :cond_3

    .line 1836
    invoke-virtual {p5, p3, v6}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 1837
    add-int v0, p2, v7

    .line 1839
    :goto_1
    return v0

    :cond_3
    neg-int v0, p2

    goto :goto_1
.end method

.method public parse(Ljava/lang/String;Lcom/ibm/icu/util/Calendar;Ljava/text/ParsePosition;)V
    .locals 48
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p3, "parsePos"    # Ljava/text/ParsePosition;

    .prologue
    .line 1518
    invoke-virtual/range {p3 .. p3}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    .line 1519
    .local v4, "pos":I
    move/from16 v43, v4

    .line 1522
    .local v43, "start":I
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/ibm/icu/text/SimpleDateFormat;->tztype:I

    .line 1523
    const/4 v2, 0x1

    new-array v9, v2, [Z

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-boolean v3, v9, v2

    .line 1526
    .local v9, "ambiguousYear":[Z
    const/16 v35, -0x1

    .line 1528
    .local v35, "numericFieldStart":I
    const/16 v34, 0x0

    .line 1530
    .local v34, "numericFieldLength":I
    const/16 v36, 0x0

    .line 1532
    .local v36, "numericStartPos":I
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/SimpleDateFormat;->getPatternItems()[Ljava/lang/Object;

    move-result-object v33

    .line 1533
    .local v33, "items":[Ljava/lang/Object;
    const/16 v30, 0x0

    .line 1534
    .local v30, "i":I
    :goto_0
    move-object/from16 v0, v33

    array-length v2, v0

    move/from16 v0, v30

    if-ge v0, v2, :cond_b

    .line 1535
    aget-object v2, v33, v30

    instance-of v2, v2, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;

    if-eqz v2, :cond_5

    .line 1537
    aget-object v29, v33, v30

    check-cast v29, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;

    .line 1538
    .local v29, "field":Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;
    move-object/from16 v0, v29

    iget-boolean v2, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->isNumeric:Z

    if-eqz v2, :cond_0

    .line 1546
    const/4 v2, -0x1

    move/from16 v0, v35

    if-ne v0, v2, :cond_0

    .line 1548
    add-int/lit8 v2, v30, 0x1

    move-object/from16 v0, v33

    array-length v3, v0

    if-ge v2, v3, :cond_0

    add-int/lit8 v2, v30, 0x1

    aget-object v2, v33, v2

    instance-of v2, v2, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;

    if-eqz v2, :cond_0

    add-int/lit8 v2, v30, 0x1

    aget-object v2, v33, v2

    check-cast v2, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;

    iget-boolean v2, v2, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->isNumeric:Z

    if-eqz v2, :cond_0

    .line 1552
    move/from16 v35, v30

    .line 1553
    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->length:I

    move/from16 v34, v0

    .line 1554
    move/from16 v36, v4

    .line 1558
    :cond_0
    const/4 v2, -0x1

    move/from16 v0, v35

    if-eq v0, v2, :cond_4

    .line 1560
    move-object/from16 v0, v29

    iget v6, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->length:I

    .line 1561
    .local v6, "len":I
    move/from16 v0, v35

    move/from16 v1, v30

    if-ne v0, v1, :cond_1

    .line 1562
    move/from16 v6, v34

    .line 1566
    :cond_1
    move-object/from16 v0, v29

    iget-char v5, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->type:C

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v10, p2

    invoke-virtual/range {v2 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->subParse(Ljava/lang/String;ICIZZ[ZLcom/ibm/icu/util/Calendar;)I

    move-result v4

    .line 1569
    if-gez v4, :cond_a

    .line 1573
    add-int/lit8 v34, v34, -0x1

    .line 1574
    if-nez v34, :cond_3

    .line 1576
    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1577
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 1791
    .end local v6    # "len":I
    .end local v29    # "field":Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;
    :cond_2
    :goto_1
    return-void

    .line 1580
    .restart local v6    # "len":I
    .restart local v29    # "field":Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;
    :cond_3
    move/from16 v30, v35

    .line 1581
    move/from16 v4, v36

    .line 1582
    goto :goto_0

    .line 1587
    .end local v6    # "len":I
    :cond_4
    const/16 v35, -0x1

    .line 1589
    move/from16 v42, v4

    .line 1590
    .local v42, "s":I
    move-object/from16 v0, v29

    iget-char v13, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->type:C

    move-object/from16 v0, v29

    iget v14, v0, Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;->length:I

    const/4 v15, 0x0

    const/16 v16, 0x1

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move v12, v4

    move-object/from16 v17, v9

    move-object/from16 v18, p2

    invoke-virtual/range {v10 .. v18}, Lcom/ibm/icu/text/SimpleDateFormat;->subParse(Ljava/lang/String;ICIZZ[ZLcom/ibm/icu/util/Calendar;)I

    move-result v4

    .line 1592
    if-gez v4, :cond_a

    .line 1593
    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1594
    move-object/from16 v0, p3

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    goto :goto_1

    .line 1600
    .end local v29    # "field":Lcom/ibm/icu/text/SimpleDateFormat$PatternItem;
    .end local v42    # "s":I
    :cond_5
    const/16 v35, -0x1

    .line 1602
    aget-object v38, v33, v30

    check-cast v38, Ljava/lang/String;

    .line 1603
    .local v38, "patl":Ljava/lang/String;
    invoke-virtual/range {v38 .. v38}, Ljava/lang/String;->length()I

    move-result v40

    .line 1604
    .local v40, "plen":I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v46

    .line 1605
    .local v46, "tlen":I
    const/16 v32, 0x0

    .line 1606
    .local v32, "idx":I
    :goto_2
    move/from16 v0, v32

    move/from16 v1, v40

    if-ge v0, v1, :cond_8

    move/from16 v0, v46

    if-ge v4, v0, :cond_8

    .line 1607
    move-object/from16 v0, v38

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v39

    .line 1608
    .local v39, "pch":C
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v31

    .line 1609
    .local v31, "ich":C
    invoke-static/range {v39 .. v39}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static/range {v31 .. v31}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1612
    :goto_3
    add-int/lit8 v2, v32, 0x1

    move/from16 v0, v40

    if-ge v2, v0, :cond_6

    add-int/lit8 v2, v32, 0x1

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1614
    add-int/lit8 v32, v32, 0x1

    .line 1615
    goto :goto_3

    .line 1616
    :cond_6
    :goto_4
    add-int/lit8 v2, v4, 0x1

    move/from16 v0, v46

    if-ge v2, v0, :cond_9

    add-int/lit8 v2, v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1618
    add-int/lit8 v4, v4, 0x1

    .line 1619
    goto :goto_4

    .line 1620
    :cond_7
    move/from16 v0, v39

    move/from16 v1, v31

    if-eq v0, v1, :cond_9

    .line 1626
    .end local v31    # "ich":C
    .end local v39    # "pch":C
    :cond_8
    move/from16 v0, v32

    move/from16 v1, v40

    if-eq v0, v1, :cond_a

    .line 1628
    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1629
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    goto/16 :goto_1

    .line 1623
    .restart local v31    # "ich":C
    .restart local v39    # "pch":C
    :cond_9
    add-int/lit8 v32, v32, 0x1

    .line 1624
    add-int/lit8 v4, v4, 0x1

    .line 1625
    goto :goto_2

    .line 1633
    .end local v31    # "ich":C
    .end local v32    # "idx":I
    .end local v38    # "patl":Ljava/lang/String;
    .end local v39    # "pch":C
    .end local v40    # "plen":I
    .end local v46    # "tlen":I
    :cond_a
    add-int/lit8 v30, v30, 0x1

    .line 1634
    goto/16 :goto_0

    .line 1640
    :cond_b
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1665
    const/4 v2, 0x0

    :try_start_0
    aget-boolean v2, v9, v2

    if-nez v2, :cond_c

    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/SimpleDateFormat;->tztype:I

    if-eqz v2, :cond_2

    .line 1671
    :cond_c
    const/4 v2, 0x0

    aget-boolean v2, v9, v2

    if-eqz v2, :cond_d

    .line 1672
    invoke-virtual/range {p2 .. p2}, Lcom/ibm/icu/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/ibm/icu/util/Calendar;

    .line 1673
    .local v27, "copy":Lcom/ibm/icu/util/Calendar;
    invoke-virtual/range {v27 .. v27}, Lcom/ibm/icu/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v37

    .line 1674
    .local v37, "parsedDate":Ljava/util/Date;
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/SimpleDateFormat;->getDefaultCenturyStart()Ljava/util/Date;

    move-result-object v2

    move-object/from16 v0, v37

    invoke-virtual {v0, v2}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1676
    const/4 v2, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/SimpleDateFormat;->getDefaultCenturyStartYear()I

    move-result v3

    add-int/lit8 v3, v3, 0x64

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 1679
    .end local v27    # "copy":Lcom/ibm/icu/util/Calendar;
    .end local v37    # "parsedDate":Ljava/util/Date;
    :cond_d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/SimpleDateFormat;->tztype:I

    if-eqz v2, :cond_2

    .line 1680
    invoke-virtual/range {p2 .. p2}, Lcom/ibm/icu/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/ibm/icu/util/Calendar;

    .line 1681
    .restart local v27    # "copy":Lcom/ibm/icu/util/Calendar;
    invoke-virtual/range {v27 .. v27}, Lcom/ibm/icu/util/Calendar;->getTimeZone()Lcom/ibm/icu/util/TimeZone;

    move-result-object v47

    .line 1682
    .local v47, "tz":Lcom/ibm/icu/util/TimeZone;
    const/4 v11, 0x0

    .line 1683
    .local v11, "btz":Lcom/ibm/icu/util/BasicTimeZone;
    move-object/from16 v0, v47

    instance-of v2, v0, Lcom/ibm/icu/util/BasicTimeZone;

    if-eqz v2, :cond_e

    .line 1684
    move-object/from16 v0, v47

    check-cast v0, Lcom/ibm/icu/util/BasicTimeZone;

    move-object v11, v0

    .line 1688
    :cond_e
    const/16 v2, 0xf

    const/4 v3, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 1689
    const/16 v2, 0x10

    const/4 v3, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 1690
    invoke-virtual/range {v27 .. v27}, Lcom/ibm/icu/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    .line 1694
    .local v12, "localMillis":J
    const/4 v2, 0x2

    new-array v0, v2, [I

    move-object/from16 v16, v0

    .line 1695
    .local v16, "offsets":[I
    if-eqz v11, :cond_12

    .line 1696
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/SimpleDateFormat;->tztype:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_11

    .line 1697
    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-virtual/range {v11 .. v16}, Lcom/ibm/icu/util/BasicTimeZone;->getOffsetFromLocal(JII[I)V

    .line 1717
    :cond_f
    :goto_5
    const/4 v2, 0x1

    aget v41, v16, v2

    .line 1718
    .local v41, "resolvedSavings":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/SimpleDateFormat;->tztype:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_15

    .line 1719
    const/4 v2, 0x1

    aget v2, v16, v2

    if-eqz v2, :cond_10

    .line 1721
    const/16 v41, 0x0

    .line 1780
    :cond_10
    :goto_6
    const/16 v2, 0xf

    const/4 v3, 0x0

    aget v3, v16, v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 1781
    const/16 v2, 0x10

    move-object/from16 v0, p2

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, Lcom/ibm/icu/util/Calendar;->set(II)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 1787
    .end local v11    # "btz":Lcom/ibm/icu/util/BasicTimeZone;
    .end local v12    # "localMillis":J
    .end local v16    # "offsets":[I
    .end local v27    # "copy":Lcom/ibm/icu/util/Calendar;
    .end local v41    # "resolvedSavings":I
    .end local v47    # "tz":Lcom/ibm/icu/util/TimeZone;
    :catch_0
    move-exception v28

    .line 1788
    .local v28, "e":Ljava/lang/IllegalArgumentException;
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 1789
    move-object/from16 v0, p3

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    goto/16 :goto_1

    .line 1700
    .end local v28    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v11    # "btz":Lcom/ibm/icu/util/BasicTimeZone;
    .restart local v12    # "localMillis":J
    .restart local v16    # "offsets":[I
    .restart local v27    # "copy":Lcom/ibm/icu/util/Calendar;
    .restart local v47    # "tz":Lcom/ibm/icu/util/TimeZone;
    :cond_11
    const/4 v14, 0x3

    const/4 v15, 0x3

    :try_start_1
    invoke-virtual/range {v11 .. v16}, Lcom/ibm/icu/util/BasicTimeZone;->getOffsetFromLocal(JII[I)V

    goto :goto_5

    .line 1706
    :cond_12
    const/4 v2, 0x1

    move-object/from16 v0, v47

    move-object/from16 v1, v16

    invoke-virtual {v0, v12, v13, v2, v1}, Lcom/ibm/icu/util/TimeZone;->getOffset(JZ[I)V

    .line 1708
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/SimpleDateFormat;->tztype:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_13

    const/4 v2, 0x1

    aget v2, v16, v2

    if-nez v2, :cond_14

    :cond_13
    move-object/from16 v0, p0

    iget v2, v0, Lcom/ibm/icu/text/SimpleDateFormat;->tztype:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_f

    const/4 v2, 0x1

    aget v2, v16, v2

    if-nez v2, :cond_f

    .line 1712
    :cond_14
    const-wide/32 v2, 0x5265c00

    sub-long v2, v12, v2

    const/4 v5, 0x1

    move-object/from16 v0, v47

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v3, v5, v1}, Lcom/ibm/icu/util/TimeZone;->getOffset(JZ[I)V

    goto :goto_5

    .line 1724
    .restart local v41    # "resolvedSavings":I
    :cond_15
    const/4 v2, 0x1

    aget v2, v16, v2

    if-nez v2, :cond_10

    .line 1725
    if-eqz v11, :cond_1e

    .line 1726
    const/4 v2, 0x0

    aget v2, v16, v2

    int-to-long v2, v2

    add-long v44, v12, v2

    .line 1729
    .local v44, "time":J
    move-wide/from16 v24, v44

    .local v24, "beforeT":J
    move-wide/from16 v20, v44

    .line 1730
    .local v20, "afterT":J
    const/16 v23, 0x0

    .local v23, "beforeSav":I
    const/16 v19, 0x0

    .line 1734
    .local v19, "afterSav":I
    :cond_16
    const/4 v2, 0x1

    move-wide/from16 v0, v24

    invoke-virtual {v11, v0, v1, v2}, Lcom/ibm/icu/util/BasicTimeZone;->getPreviousTransition(JZ)Lcom/ibm/icu/util/TimeZoneTransition;

    move-result-object v26

    .line 1735
    .local v26, "beforeTrs":Lcom/ibm/icu/util/TimeZoneTransition;
    if-nez v26, :cond_18

    .line 1747
    :cond_17
    :goto_7
    const/4 v2, 0x0

    move-wide/from16 v0, v20

    invoke-virtual {v11, v0, v1, v2}, Lcom/ibm/icu/util/BasicTimeZone;->getNextTransition(JZ)Lcom/ibm/icu/util/TimeZoneTransition;

    move-result-object v22

    .line 1748
    .local v22, "afterTrs":Lcom/ibm/icu/util/TimeZoneTransition;
    if-nez v22, :cond_19

    .line 1758
    :goto_8
    if-eqz v26, :cond_1b

    if-eqz v22, :cond_1b

    .line 1759
    sub-long v2, v44, v24

    sub-long v14, v20, v44

    cmp-long v2, v2, v14

    if-lez v2, :cond_1a

    .line 1760
    move/from16 v41, v19

    .line 1774
    .end local v19    # "afterSav":I
    .end local v20    # "afterT":J
    .end local v22    # "afterTrs":Lcom/ibm/icu/util/TimeZoneTransition;
    .end local v23    # "beforeSav":I
    .end local v24    # "beforeT":J
    .end local v26    # "beforeTrs":Lcom/ibm/icu/util/TimeZoneTransition;
    .end local v44    # "time":J
    :goto_9
    if-nez v41, :cond_10

    .line 1776
    const v41, 0x36ee80

    goto/16 :goto_6

    .line 1738
    .restart local v19    # "afterSav":I
    .restart local v20    # "afterT":J
    .restart local v23    # "beforeSav":I
    .restart local v24    # "beforeT":J
    .restart local v26    # "beforeTrs":Lcom/ibm/icu/util/TimeZoneTransition;
    .restart local v44    # "time":J
    :cond_18
    invoke-virtual/range {v26 .. v26}, Lcom/ibm/icu/util/TimeZoneTransition;->getTime()J

    move-result-wide v2

    const-wide/16 v14, 0x1

    sub-long v24, v2, v14

    .line 1739
    invoke-virtual/range {v26 .. v26}, Lcom/ibm/icu/util/TimeZoneTransition;->getFrom()Lcom/ibm/icu/util/TimeZoneRule;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ibm/icu/util/TimeZoneRule;->getDSTSavings()I

    move-result v23

    .line 1740
    if-eqz v23, :cond_16

    goto :goto_7

    .line 1751
    .restart local v22    # "afterTrs":Lcom/ibm/icu/util/TimeZoneTransition;
    :cond_19
    invoke-virtual/range {v22 .. v22}, Lcom/ibm/icu/util/TimeZoneTransition;->getTime()J

    move-result-wide v20

    .line 1752
    invoke-virtual/range {v22 .. v22}, Lcom/ibm/icu/util/TimeZoneTransition;->getTo()Lcom/ibm/icu/util/TimeZoneRule;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ibm/icu/util/TimeZoneRule;->getDSTSavings()I

    move-result v19

    .line 1753
    if-eqz v19, :cond_17

    goto :goto_8

    .line 1762
    :cond_1a
    move/from16 v41, v23

    .line 1764
    goto :goto_9

    :cond_1b
    if-eqz v26, :cond_1c

    if-eqz v23, :cond_1c

    .line 1765
    move/from16 v41, v23

    .line 1766
    goto :goto_9

    :cond_1c
    if-eqz v22, :cond_1d

    if-eqz v19, :cond_1d

    .line 1767
    move/from16 v41, v19

    .line 1768
    goto :goto_9

    .line 1769
    :cond_1d
    invoke-virtual {v11}, Lcom/ibm/icu/util/BasicTimeZone;->getDSTSavings()I

    move-result v41

    goto :goto_9

    .line 1772
    .end local v19    # "afterSav":I
    .end local v20    # "afterT":J
    .end local v22    # "afterTrs":Lcom/ibm/icu/util/TimeZoneTransition;
    .end local v23    # "beforeSav":I
    .end local v24    # "beforeT":J
    .end local v26    # "beforeTrs":Lcom/ibm/icu/util/TimeZoneTransition;
    .end local v44    # "time":J
    :cond_1e
    invoke-virtual/range {v47 .. v47}, Lcom/ibm/icu/util/TimeZone;->getDSTSavings()I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v41

    goto :goto_9
.end method

.method protected patternCharToDateFormatField(C)Lcom/ibm/icu/text/DateFormat$Field;
    .locals 3
    .param p1, "ch"    # C

    .prologue
    .line 718
    const/4 v0, -0x1

    .line 719
    .local v0, "patternCharIndex":I
    const/16 v1, 0x41

    if-gt v1, p1, :cond_0

    const/16 v1, 0x7a

    if-gt p1, v1, :cond_0

    .line 720
    sget-object v1, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_CHAR_TO_INDEX:[I

    add-int/lit8 v2, p1, -0x40

    aget v0, v1, v2

    .line 722
    :cond_0
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 723
    sget-object v1, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_INDEX_TO_DATE_FORMAT_ATTRIBUTE:[Lcom/ibm/icu/text/DateFormat$Field;

    aget-object v1, v1, v0

    .line 725
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public set2DigitYearStart(Ljava/util/Date;)V
    .locals 0
    .param p1, "startDate"    # Ljava/util/Date;

    .prologue
    .line 554
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/SimpleDateFormat;->parseAmbiguousDatesAsAfter(Ljava/util/Date;)V

    .line 555
    return-void
.end method

.method public setDateFormatSymbols(Lcom/ibm/icu/text/DateFormatSymbols;)V
    .locals 1
    .param p1, "newFormatSymbols"    # Lcom/ibm/icu/text/DateFormatSymbols;

    .prologue
    .line 2462
    invoke-virtual {p1}, Lcom/ibm/icu/text/DateFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/DateFormatSymbols;

    iput-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    .line 2463
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->gmtfmtCache:[Ljava/lang/ref/WeakReference;

    .line 2464
    return-void
.end method

.method public setNumberFormat(Lcom/ibm/icu/text/NumberFormat;)V
    .locals 0
    .param p1, "newNumberFormat"    # Lcom/ibm/icu/text/NumberFormat;

    .prologue
    .line 1430
    invoke-super {p0, p1}, Lcom/ibm/icu/text/DateFormat;->setNumberFormat(Lcom/ibm/icu/text/NumberFormat;)V

    .line 1431
    invoke-direct {p0}, Lcom/ibm/icu/text/SimpleDateFormat;->initLocalZeroPaddingNumberFormat()V

    .line 1432
    return-void
.end method

.method protected subFormat(CIILjava/text/FieldPosition;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;)Ljava/lang/String;
    .locals 7
    .param p1, "ch"    # C
    .param p2, "count"    # I
    .param p3, "beginOffset"    # I
    .param p4, "pos"    # Ljava/text/FieldPosition;
    .param p5, "fmtData"    # Lcom/ibm/icu/text/DateFormatSymbols;
    .param p6, "cal"    # Lcom/ibm/icu/util/Calendar;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 747
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .local v1, "buf":Ljava/lang/StringBuffer;
    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p6

    .line 748
    invoke-virtual/range {v0 .. v6}, Lcom/ibm/icu/text/SimpleDateFormat;->subFormat(Ljava/lang/StringBuffer;CIILjava/text/FieldPosition;Lcom/ibm/icu/util/Calendar;)V

    .line 749
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected subFormat(Ljava/lang/StringBuffer;CIILjava/text/FieldPosition;Lcom/ibm/icu/util/Calendar;)V
    .locals 25
    .param p1, "buf"    # Ljava/lang/StringBuffer;
    .param p2, "ch"    # C
    .param p3, "count"    # I
    .param p4, "beginOffset"    # I
    .param p5, "pos"    # Ljava/text/FieldPosition;
    .param p6, "cal"    # Lcom/ibm/icu/util/Calendar;

    .prologue
    .line 770
    const v10, 0x7fffffff

    .line 771
    .local v10, "maxIntCount":I
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    .line 774
    .local v6, "bufstart":I
    const/16 v16, -0x1

    .line 775
    .local v16, "patternCharIndex":I
    const/16 v21, 0x41

    move/from16 v0, v21

    move/from16 v1, p2

    if-gt v0, v1, :cond_0

    const/16 v21, 0x7a

    move/from16 v0, p2

    move/from16 v1, v21

    if-gt v0, v1, :cond_0

    .line 776
    sget-object v21, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_CHAR_TO_INDEX:[I

    add-int/lit8 v22, p2, -0x40

    aget v16, v21, v22

    .line 779
    :cond_0
    const/16 v21, -0x1

    move/from16 v0, v16

    move/from16 v1, v21

    if-ne v0, v1, :cond_1

    .line 780
    new-instance v21, Ljava/lang/IllegalArgumentException;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v23, "Illegal pattern character \'"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v22

    const-string/jumbo v23, "\' in \""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    new-instance v23, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-direct/range {v23 .. v24}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    const/16 v23, 0x22

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v21

    .line 785
    :cond_1
    sget-object v21, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_INDEX_TO_CALENDAR_FIELD:[I

    aget v9, v21, v16

    .line 786
    .local v9, "field":I
    move-object/from16 v0, p6

    invoke-virtual {v0, v9}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v19

    .line 788
    .local v19, "value":I
    const/16 v20, 0x0

    .line 790
    .local v20, "zoneString":Ljava/lang/String;
    packed-switch v16, :pswitch_data_0

    .line 1007
    :pswitch_0
    const v21, 0x7fffffff

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, p3

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    .line 1012
    :cond_2
    :goto_0
    invoke-virtual/range {p5 .. p5}, Ljava/text/FieldPosition;->getBeginIndex()I

    move-result v21

    invoke-virtual/range {p5 .. p5}, Ljava/text/FieldPosition;->getEndIndex()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_3

    .line 1013
    invoke-virtual/range {p5 .. p5}, Ljava/text/FieldPosition;->getField()I

    move-result v21

    sget-object v22, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_INDEX_TO_DATE_FORMAT_FIELD:[I

    aget v22, v22, v16

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_25

    .line 1014
    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 1015
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v21

    add-int v21, v21, p4

    sub-int v21, v21, v6

    move-object/from16 v0, p5

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 1025
    :cond_3
    :goto_1
    return-void

    .line 792
    :pswitch_1
    const/16 v21, 0x5

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_4

    .line 793
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowEras:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto :goto_0

    .line 794
    :cond_4
    const/16 v21, 0x4

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    .line 795
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto :goto_0

    .line 797
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->eras:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 807
    :pswitch_2
    const/16 v21, 0x2

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_6

    .line 808
    const/16 v21, 0x2

    const/16 v22, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v21

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    goto/16 :goto_0

    .line 810
    :cond_6
    const v21, 0x7fffffff

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, p3

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    goto/16 :goto_0

    .line 813
    :pswitch_3
    const/16 v21, 0x5

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_7

    .line 814
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 815
    :cond_7
    const/16 v21, 0x4

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_8

    .line 816
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 817
    :cond_8
    const/16 v21, 0x3

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_9

    .line 818
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 820
    :cond_9
    add-int/lit8 v21, v19, 0x1

    const v22, 0x7fffffff

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, p3

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    goto/16 :goto_0

    .line 824
    :pswitch_4
    if-nez v19, :cond_a

    .line 825
    const/16 v21, 0xb

    move-object/from16 v0, p6

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/util/Calendar;->getMaximum(I)I

    move-result v21

    add-int/lit8 v21, v21, 0x1

    const v22, 0x7fffffff

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, p3

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    goto/16 :goto_0

    .line 829
    :cond_a
    const v21, 0x7fffffff

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, p3

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    goto/16 :goto_0

    .line 834
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    move-object/from16 v21, v0

    const/16 v22, 0x3

    move/from16 v0, v22

    move/from16 v1, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Lcom/ibm/icu/text/NumberFormat;->setMinimumIntegerDigits(I)V

    .line 835
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    move-object/from16 v21, v0

    const v22, 0x7fffffff

    invoke-virtual/range {v21 .. v22}, Lcom/ibm/icu/text/NumberFormat;->setMaximumIntegerDigits(I)V

    .line 836
    const/16 v21, 0x1

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_c

    .line 837
    add-int/lit8 v21, v19, 0x32

    div-int/lit8 v19, v21, 0x64

    .line 841
    :cond_b
    :goto_2
    new-instance v15, Ljava/text/FieldPosition;

    const/16 v21, -0x1

    move/from16 v0, v21

    invoke-direct {v15, v0}, Ljava/text/FieldPosition;-><init>(I)V

    .line 842
    .local v15, "p":Ljava/text/FieldPosition;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    move-object/from16 v21, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v22, v0

    move-object/from16 v0, v21

    move-wide/from16 v1, v22

    move-object/from16 v3, p1

    invoke-virtual {v0, v1, v2, v3, v15}, Lcom/ibm/icu/text/NumberFormat;->format(JLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 843
    const/16 v21, 0x3

    move/from16 v0, p3

    move/from16 v1, v21

    if-le v0, v1, :cond_2

    .line 844
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    move-object/from16 v21, v0

    add-int/lit8 v22, p3, -0x3

    invoke-virtual/range {v21 .. v22}, Lcom/ibm/icu/text/NumberFormat;->setMinimumIntegerDigits(I)V

    .line 845
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x0

    move-object/from16 v0, v21

    move-wide/from16 v1, v22

    move-object/from16 v3, p1

    invoke-virtual {v0, v1, v2, v3, v15}, Lcom/ibm/icu/text/NumberFormat;->format(JLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 838
    .end local v15    # "p":Ljava/text/FieldPosition;
    :cond_c
    const/16 v21, 0x2

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_b

    .line 839
    add-int/lit8 v21, v19, 0x5

    div-int/lit8 v19, v21, 0xa

    goto :goto_2

    .line 850
    :pswitch_6
    const/16 v21, 0x5

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_d

    .line 851
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 852
    :cond_d
    const/16 v21, 0x4

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_e

    .line 853
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 855
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 859
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->ampms:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 862
    :pswitch_8
    if-nez v19, :cond_f

    .line 863
    const/16 v21, 0xa

    move-object/from16 v0, p6

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/util/Calendar;->getLeastMaximum(I)I

    move-result v21

    add-int/lit8 v21, v21, 0x1

    const v22, 0x7fffffff

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, p3

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    goto/16 :goto_0

    .line 867
    :cond_f
    const v21, 0x7fffffff

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, p3

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    goto/16 :goto_0

    .line 870
    :pswitch_9
    const/16 v21, 0x4

    move/from16 v0, p3

    move/from16 v1, v21

    if-ge v0, v1, :cond_10

    .line 872
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/ibm/icu/text/DateFormatSymbols;->getZoneStringFormat()Lcom/ibm/icu/impl/ZoneStringFormat;

    move-result-object v21

    const/16 v22, 0x1

    move-object/from16 v0, v21

    move-object/from16 v1, p6

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getSpecificShortString(Lcom/ibm/icu/util/Calendar;Z)Ljava/lang/String;

    move-result-object v20

    .line 876
    :goto_3
    if-eqz v20, :cond_11

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v21

    if-eqz v21, :cond_11

    .line 877
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 874
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/ibm/icu/text/DateFormatSymbols;->getZoneStringFormat()Lcom/ibm/icu/impl/ZoneStringFormat;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat;->getSpecificLongString(Lcom/ibm/icu/util/Calendar;)Ljava/lang/String;

    move-result-object v20

    goto :goto_3

    .line 880
    :cond_11
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->appendGMT(Ljava/lang/StringBuffer;Lcom/ibm/icu/util/Calendar;)V

    goto/16 :goto_0

    .line 884
    :pswitch_a
    const/16 v21, 0x4

    move/from16 v0, p3

    move/from16 v1, v21

    if-ge v0, v1, :cond_14

    .line 886
    const/16 v21, 0xf

    move-object/from16 v0, p6

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v21

    const/16 v22, 0x10

    move-object/from16 v0, p6

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v22

    add-int v18, v21, v22

    .line 887
    .local v18, "val":I
    const/16 v17, 0x2b

    .line 888
    .local v17, "sign":C
    if-gez v18, :cond_12

    .line 889
    move/from16 v0, v18

    neg-int v0, v0

    move/from16 v18, v0

    .line 890
    const/16 v17, 0x2d

    .line 892
    :cond_12
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 894
    const v21, 0x36ee80

    div-int v12, v18, v21

    .line 895
    .local v12, "offsetH":I
    const v21, 0x36ee80

    rem-int v18, v18, v21

    .line 896
    const v21, 0xea60

    div-int v13, v18, v21

    .line 897
    .local v13, "offsetM":I
    const v21, 0xea60

    rem-int v18, v18, v21

    .line 898
    move/from16 v0, v18

    div-int/lit16 v14, v0, 0x3e8

    .line 900
    .local v14, "offsetS":I
    const/4 v11, 0x0

    .local v11, "num":I
    const/4 v7, 0x0

    .line 901
    .local v7, "denom":I
    if-nez v14, :cond_13

    .line 902
    mul-int/lit8 v21, v12, 0x64

    add-int v18, v21, v13

    .line 903
    move/from16 v0, v18

    rem-int/lit16 v11, v0, 0x2710

    .line 904
    const/16 v7, 0x3e8

    .line 910
    :goto_4
    const/16 v21, 0x1

    move/from16 v0, v21

    if-lt v7, v0, :cond_2

    .line 911
    div-int v21, v11, v7

    add-int/lit8 v21, v21, 0x30

    move/from16 v0, v21

    int-to-char v8, v0

    .line 912
    .local v8, "digit":C
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 913
    rem-int/2addr v11, v7

    .line 914
    div-int/lit8 v7, v7, 0xa

    .line 915
    goto :goto_4

    .line 906
    .end local v8    # "digit":C
    :cond_13
    mul-int/lit16 v0, v12, 0x2710

    move/from16 v21, v0

    mul-int/lit8 v22, v13, 0x64

    add-int v21, v21, v22

    add-int v18, v21, v14

    .line 907
    const v21, 0xf4240

    rem-int v11, v18, v21

    .line 908
    const v7, 0x186a0

    goto :goto_4

    .line 918
    .end local v7    # "denom":I
    .end local v11    # "num":I
    .end local v12    # "offsetH":I
    .end local v13    # "offsetM":I
    .end local v14    # "offsetS":I
    .end local v17    # "sign":C
    .end local v18    # "val":I
    :cond_14
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->appendGMT(Ljava/lang/StringBuffer;Lcom/ibm/icu/util/Calendar;)V

    goto/16 :goto_0

    .line 922
    :pswitch_b
    const/16 v21, 0x1

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_16

    .line 924
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/ibm/icu/text/DateFormatSymbols;->getZoneStringFormat()Lcom/ibm/icu/impl/ZoneStringFormat;

    move-result-object v21

    const/16 v22, 0x1

    move-object/from16 v0, v21

    move-object/from16 v1, p6

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getGenericShortString(Lcom/ibm/icu/util/Calendar;Z)Ljava/lang/String;

    move-result-object v20

    .line 929
    :cond_15
    :goto_5
    if-eqz v20, :cond_17

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v21

    if-eqz v21, :cond_17

    .line 930
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 925
    :cond_16
    const/16 v21, 0x4

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_15

    .line 927
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/ibm/icu/text/DateFormatSymbols;->getZoneStringFormat()Lcom/ibm/icu/impl/ZoneStringFormat;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat;->getGenericLongString(Lcom/ibm/icu/util/Calendar;)Ljava/lang/String;

    move-result-object v20

    goto :goto_5

    .line 933
    :cond_17
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->appendGMT(Ljava/lang/StringBuffer;Lcom/ibm/icu/util/Calendar;)V

    goto/16 :goto_0

    .line 937
    :pswitch_c
    const/16 v21, 0x5

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_18

    .line 938
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 939
    :cond_18
    const/16 v21, 0x4

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_19

    .line 940
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 941
    :cond_19
    const/16 v21, 0x3

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_1a

    .line 942
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 944
    :cond_1a
    const/16 v21, 0x1

    const v22, 0x7fffffff

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v21

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    goto/16 :goto_0

    .line 948
    :pswitch_d
    const/16 v21, 0x5

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_1b

    .line 949
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 950
    :cond_1b
    const/16 v21, 0x4

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_1c

    .line 951
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 952
    :cond_1c
    const/16 v21, 0x3

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_1d

    .line 953
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 955
    :cond_1d
    add-int/lit8 v21, v19, 0x1

    const v22, 0x7fffffff

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, p3

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    goto/16 :goto_0

    .line 959
    :pswitch_e
    const/16 v21, 0x4

    move/from16 v0, p3

    move/from16 v1, v21

    if-lt v0, v1, :cond_1e

    .line 960
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->quarters:[Ljava/lang/String;

    move-object/from16 v21, v0

    div-int/lit8 v22, v19, 0x3

    move-object/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 961
    :cond_1e
    const/16 v21, 0x3

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_1f

    .line 962
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->shortQuarters:[Ljava/lang/String;

    move-object/from16 v21, v0

    div-int/lit8 v22, v19, 0x3

    move-object/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 964
    :cond_1f
    div-int/lit8 v21, v19, 0x3

    add-int/lit8 v21, v21, 0x1

    const v22, 0x7fffffff

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, p3

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    goto/16 :goto_0

    .line 968
    :pswitch_f
    const/16 v21, 0x4

    move/from16 v0, p3

    move/from16 v1, v21

    if-lt v0, v1, :cond_20

    .line 969
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneQuarters:[Ljava/lang/String;

    move-object/from16 v21, v0

    div-int/lit8 v22, v19, 0x3

    move-object/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 970
    :cond_20
    const/16 v21, 0x3

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_21

    .line 971
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortQuarters:[Ljava/lang/String;

    move-object/from16 v21, v0

    div-int/lit8 v22, v19, 0x3

    move-object/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->safeAppend([Ljava/lang/String;ILjava/lang/StringBuffer;)V

    goto/16 :goto_0

    .line 973
    :cond_21
    div-int/lit8 v21, v19, 0x3

    add-int/lit8 v21, v21, 0x1

    const v22, 0x7fffffff

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, p3

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    goto/16 :goto_0

    .line 977
    :pswitch_10
    const/16 v21, 0x1

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_23

    .line 979
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/ibm/icu/text/DateFormatSymbols;->getZoneStringFormat()Lcom/ibm/icu/impl/ZoneStringFormat;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p6

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/impl/ZoneStringFormat;->getSpecificShortString(Lcom/ibm/icu/util/Calendar;Z)Ljava/lang/String;

    move-result-object v20

    .line 984
    :cond_22
    :goto_6
    if-eqz v20, :cond_24

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v21

    if-eqz v21, :cond_24

    .line 985
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    .line 980
    :cond_23
    const/16 v21, 0x4

    move/from16 v0, p3

    move/from16 v1, v21

    if-ne v0, v1, :cond_22

    .line 982
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/ibm/icu/text/DateFormatSymbols;->getZoneStringFormat()Lcom/ibm/icu/impl/ZoneStringFormat;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat;->getGenericLocationString(Lcom/ibm/icu/util/Calendar;)Ljava/lang/String;

    move-result-object v20

    goto :goto_6

    .line 988
    :cond_24
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->appendGMT(Ljava/lang/StringBuffer;Lcom/ibm/icu/util/Calendar;)V

    goto/16 :goto_0

    .line 1019
    :cond_25
    invoke-virtual/range {p5 .. p5}, Ljava/text/FieldPosition;->getFieldAttribute()Ljava/text/Format$Field;

    move-result-object v21

    sget-object v22, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_INDEX_TO_DATE_FORMAT_ATTRIBUTE:[Lcom/ibm/icu/text/DateFormat$Field;

    aget-object v22, v22, v16

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_3

    .line 1020
    move-object/from16 v0, p5

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 1021
    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v21

    add-int v21, v21, p4

    sub-int v21, v21, v6

    move-object/from16 v0, p5

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/text/FieldPosition;->setEndIndex(I)V

    goto/16 :goto_1

    .line 790
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method protected subParse(Ljava/lang/String;ICIZZ[ZLcom/ibm/icu/util/Calendar;)I
    .locals 34
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "ch"    # C
    .param p4, "count"    # I
    .param p5, "obeyCount"    # Z
    .param p6, "allowNegative"    # Z
    .param p7, "ambiguousYear"    # [Z
    .param p8, "cal"    # Lcom/ibm/icu/util/Calendar;

    .prologue
    .line 1911
    const/16 v21, 0x0

    .line 1912
    .local v21, "number":Ljava/lang/Number;
    const/16 v32, 0x0

    .line 1914
    .local v32, "value":I
    new-instance v26, Ljava/text/ParsePosition;

    const/4 v5, 0x0

    move-object/from16 v0, v26

    invoke-direct {v0, v5}, Ljava/text/ParsePosition;-><init>(I)V

    .line 1916
    .local v26, "pos":Ljava/text/ParsePosition;
    const/16 v25, -0x1

    .line 1917
    .local v25, "patternCharIndex":I
    const/16 v5, 0x41

    move/from16 v0, p3

    if-gt v5, v0, :cond_0

    const/16 v5, 0x7a

    move/from16 v0, p3

    if-gt v0, v5, :cond_0

    .line 1918
    sget-object v5, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_CHAR_TO_INDEX:[I

    add-int/lit8 v6, p3, -0x40

    aget v25, v5, v6

    .line 1921
    :cond_0
    const/4 v5, -0x1

    move/from16 v0, v25

    if-ne v0, v5, :cond_2

    .line 1922
    move/from16 v0, p2

    neg-int v0, v0

    move/from16 v19, v0

    .line 2309
    :cond_1
    :goto_0
    return v19

    .line 1925
    :cond_2
    sget-object v5, Lcom/ibm/icu/text/SimpleDateFormat;->PATTERN_INDEX_TO_CALENDAR_FIELD:[I

    aget v14, v5, v25

    .line 1930
    .local v14, "field":I
    :goto_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v5

    move/from16 v0, p2

    if-lt v0, v5, :cond_3

    .line 1931
    move/from16 v0, p2

    neg-int v0, v0

    move/from16 v19, v0

    goto :goto_0

    .line 1933
    :cond_3
    invoke-static/range {p1 .. p2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v13

    .line 1934
    .local v13, "c":I
    invoke-static {v13}, Lcom/ibm/icu/lang/UCharacter;->isUWhiteSpace(I)Z

    move-result v5

    if-nez v5, :cond_6

    .line 1939
    move-object/from16 v0, v26

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1945
    const/4 v5, 0x4

    move/from16 v0, v25

    if-eq v0, v5, :cond_5

    const/16 v5, 0xf

    move/from16 v0, v25

    if-eq v0, v5, :cond_5

    const/4 v5, 0x2

    move/from16 v0, v25

    if-ne v0, v5, :cond_4

    const/4 v5, 0x2

    move/from16 v0, p4

    if-le v0, v5, :cond_5

    :cond_4
    const/4 v5, 0x1

    move/from16 v0, v25

    if-eq v0, v5, :cond_5

    const/16 v5, 0x8

    move/from16 v0, v25

    if-ne v0, v5, :cond_a

    .line 1953
    :cond_5
    if-eqz p5, :cond_8

    .line 1955
    add-int v5, p2, p4

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v6

    if-le v5, v6, :cond_7

    move/from16 v0, p2

    neg-int v0, v0

    move/from16 v19, v0

    goto :goto_0

    .line 1937
    :cond_6
    invoke-static {v13}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v5

    add-int p2, p2, v5

    .line 1938
    goto :goto_1

    .line 1956
    :cond_7
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    move-object/from16 v3, v26

    move/from16 v4, p6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->parseInt(Ljava/lang/String;ILjava/text/ParsePosition;Z)Ljava/lang/Number;

    move-result-object v21

    .line 1959
    :goto_2
    if-nez v21, :cond_9

    .line 1960
    move/from16 v0, p2

    neg-int v0, v0

    move/from16 v19, v0

    goto :goto_0

    .line 1958
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    move/from16 v3, p6

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/SimpleDateFormat;->parseInt(Ljava/lang/String;Ljava/text/ParsePosition;Z)Ljava/lang/Number;

    move-result-object v21

    goto :goto_2

    .line 1961
    :cond_9
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Number;->intValue()I

    move-result v32

    .line 1964
    :cond_a
    packed-switch v25, :pswitch_data_0

    .line 2299
    :pswitch_0
    if-eqz p5, :cond_26

    .line 2301
    add-int v5, p2, p4

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v6

    if-le v5, v6, :cond_25

    move/from16 v0, p2

    neg-int v0, v0

    move/from16 v19, v0

    goto/16 :goto_0

    .line 1967
    :pswitch_1
    const/4 v5, 0x4

    move/from16 v0, p4

    if-ne v0, v5, :cond_b

    .line 1968
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    goto/16 :goto_0

    .line 1970
    :cond_b
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->eras:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    goto/16 :goto_0

    .line 1980
    :pswitch_2
    const/4 v5, 0x2

    move/from16 v0, p4

    if-ne v0, v5, :cond_c

    invoke-virtual/range {v26 .. v26}, Ljava/text/ParsePosition;->getIndex()I

    move-result v5

    sub-int v5, v5, p2

    const/4 v6, 0x2

    if-ne v5, v6, :cond_c

    invoke-virtual/range {p1 .. p2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_c

    add-int/lit8 v5, p2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 1992
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/SimpleDateFormat;->getDefaultCenturyStartYear()I

    move-result v5

    rem-int/lit8 v12, v5, 0x64

    .line 1993
    .local v12, "ambiguousTwoDigitYear":I
    const/4 v6, 0x0

    move/from16 v0, v32

    if-ne v0, v12, :cond_d

    const/4 v5, 0x1

    :goto_3
    aput-boolean v5, p7, v6

    .line 1994
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/SimpleDateFormat;->getDefaultCenturyStartYear()I

    move-result v5

    div-int/lit8 v5, v5, 0x64

    mul-int/lit8 v6, v5, 0x64

    move/from16 v0, v32

    if-ge v0, v12, :cond_e

    const/16 v5, 0x64

    :goto_4
    add-int/2addr v5, v6

    add-int v32, v32, v5

    .line 1997
    .end local v12    # "ambiguousTwoDigitYear":I
    :cond_c
    const/4 v5, 0x1

    move-object/from16 v0, p8

    move/from16 v1, v32

    invoke-virtual {v0, v5, v1}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 1998
    invoke-virtual/range {v26 .. v26}, Ljava/text/ParsePosition;->getIndex()I

    move-result v19

    goto/16 :goto_0

    .line 1993
    .restart local v12    # "ambiguousTwoDigitYear":I
    :cond_d
    const/4 v5, 0x0

    goto :goto_3

    .line 1994
    :cond_e
    const/4 v5, 0x0

    goto :goto_4

    .line 2000
    .end local v12    # "ambiguousTwoDigitYear":I
    :pswitch_3
    const/4 v5, 0x2

    move/from16 v0, p4

    if-gt v0, v5, :cond_f

    .line 2005
    const/4 v5, 0x2

    add-int/lit8 v6, v32, -0x1

    move-object/from16 v0, p8

    invoke-virtual {v0, v5, v6}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 2006
    invoke-virtual/range {v26 .. v26}, Ljava/text/ParsePosition;->getIndex()I

    move-result v19

    goto/16 :goto_0

    .line 2013
    :cond_f
    const/4 v8, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    .line 2015
    .local v19, "newStart":I
    if-gtz v19, :cond_1

    .line 2018
    const/4 v8, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    goto/16 :goto_0

    .line 2023
    .end local v19    # "newStart":I
    :pswitch_4
    const/4 v5, 0x2

    move/from16 v0, p4

    if-gt v0, v5, :cond_10

    .line 2028
    const/4 v5, 0x2

    add-int/lit8 v6, v32, -0x1

    move-object/from16 v0, p8

    invoke-virtual {v0, v5, v6}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 2029
    invoke-virtual/range {v26 .. v26}, Ljava/text/ParsePosition;->getIndex()I

    move-result v19

    goto/16 :goto_0

    .line 2036
    :cond_10
    const/4 v8, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    .line 2038
    .restart local v19    # "newStart":I
    if-gtz v19, :cond_1

    .line 2041
    const/4 v8, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    goto/16 :goto_0

    .line 2047
    .end local v19    # "newStart":I
    :pswitch_5
    const/16 v5, 0xb

    move-object/from16 v0, p8

    invoke-virtual {v0, v5}, Lcom/ibm/icu/util/Calendar;->getMaximum(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    move/from16 v0, v32

    if-ne v0, v5, :cond_11

    const/16 v32, 0x0

    .line 2048
    :cond_11
    const/16 v5, 0xb

    move-object/from16 v0, p8

    move/from16 v1, v32

    invoke-virtual {v0, v5, v1}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 2049
    invoke-virtual/range {v26 .. v26}, Ljava/text/ParsePosition;->getIndex()I

    move-result v19

    goto/16 :goto_0

    .line 2052
    :pswitch_6
    invoke-virtual/range {v26 .. v26}, Ljava/text/ParsePosition;->getIndex()I

    move-result v5

    sub-int v17, v5, p2

    .line 2053
    .local v17, "i":I
    const/4 v5, 0x3

    move/from16 v0, v17

    if-ge v0, v5, :cond_12

    .line 2054
    :goto_5
    const/4 v5, 0x3

    move/from16 v0, v17

    if-ge v0, v5, :cond_14

    .line 2055
    mul-int/lit8 v32, v32, 0xa

    .line 2056
    add-int/lit8 v17, v17, 0x1

    .line 2057
    goto :goto_5

    .line 2059
    :cond_12
    const/4 v11, 0x1

    .line 2060
    .local v11, "a":I
    :goto_6
    const/4 v5, 0x3

    move/from16 v0, v17

    if-le v0, v5, :cond_13

    .line 2061
    mul-int/lit8 v11, v11, 0xa

    .line 2062
    add-int/lit8 v17, v17, -0x1

    .line 2063
    goto :goto_6

    .line 2064
    :cond_13
    shr-int/lit8 v5, v11, 0x1

    add-int v5, v5, v32

    div-int v32, v5, v11

    .line 2066
    .end local v11    # "a":I
    :cond_14
    const/16 v5, 0xe

    move-object/from16 v0, p8

    move/from16 v1, v32

    invoke-virtual {v0, v5, v1}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 2067
    invoke-virtual/range {v26 .. v26}, Ljava/text/ParsePosition;->getIndex()I

    move-result v19

    goto/16 :goto_0

    .line 2071
    .end local v17    # "i":I
    :pswitch_7
    const/4 v8, 0x7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    .line 2073
    .restart local v19    # "newStart":I
    if-gtz v19, :cond_1

    .line 2076
    const/4 v8, 0x7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    goto/16 :goto_0

    .line 2083
    .end local v19    # "newStart":I
    :pswitch_8
    const/4 v8, 0x7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    .line 2085
    .restart local v19    # "newStart":I
    if-gtz v19, :cond_1

    .line 2088
    const/4 v8, 0x7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    goto/16 :goto_0

    .line 2093
    .end local v19    # "newStart":I
    :pswitch_9
    const/16 v8, 0x9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->ampms:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    goto/16 :goto_0

    .line 2096
    :pswitch_a
    const/16 v5, 0xa

    move-object/from16 v0, p8

    invoke-virtual {v0, v5}, Lcom/ibm/icu/util/Calendar;->getLeastMaximum(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    move/from16 v0, v32

    if-ne v0, v5, :cond_15

    const/16 v32, 0x0

    .line 2097
    :cond_15
    const/16 v5, 0xa

    move-object/from16 v0, p8

    move/from16 v1, v32

    invoke-virtual {v0, v5, v1}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 2098
    invoke-virtual/range {v26 .. v26}, Ljava/text/ParsePosition;->getIndex()I

    move-result v19

    goto/16 :goto_0

    .line 2104
    :pswitch_b
    const/16 v30, 0x0

    .line 2105
    .local v30, "tz":Lcom/ibm/icu/util/TimeZone;
    const/16 v22, 0x0

    .line 2106
    .local v22, "offset":I
    const/16 v24, 0x0

    .line 2110
    .local v24, "parsed":Z
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->parseGMT(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Integer;

    move-result-object v15

    .line 2111
    .local v15, "gmtoff":Ljava/lang/Integer;
    if-eqz v15, :cond_16

    .line 2112
    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 2113
    const/16 v24, 0x1

    .line 2116
    :cond_16
    if-nez v24, :cond_18

    .line 2123
    const/16 v28, 0x0

    .line 2124
    .local v28, "sign":I
    invoke-virtual/range {p1 .. p2}, Ljava/lang/String;->charAt(I)C

    move-result v29

    .line 2125
    .local v29, "signChar":C
    const/16 v5, 0x2b

    move/from16 v0, v29

    if-ne v0, v5, :cond_19

    .line 2126
    const/16 v28, 0x1

    .line 2135
    :goto_7
    add-int/lit8 v23, p2, 0x1

    .line 2136
    .local v23, "orgPos":I
    move-object/from16 v0, v26

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 2137
    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v5, v2, v6}, Lcom/ibm/icu/text/SimpleDateFormat;->parseInt(Ljava/lang/String;ILjava/text/ParsePosition;Z)Ljava/lang/Number;

    move-result-object v21

    .line 2138
    invoke-virtual/range {v26 .. v26}, Ljava/text/ParsePosition;->getIndex()I

    move-result v5

    sub-int v20, v5, v23

    .line 2139
    .local v20, "numLen":I
    if-gtz v20, :cond_1a

    .line 2177
    .end local v20    # "numLen":I
    .end local v23    # "orgPos":I
    :cond_17
    :goto_8
    if-nez v24, :cond_18

    .line 2179
    move-object/from16 v0, v26

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 2183
    .end local v28    # "sign":I
    .end local v29    # "signChar":C
    :cond_18
    if-eqz v24, :cond_1b

    .line 2186
    invoke-static/range {v22 .. v22}, Lcom/ibm/icu/impl/ZoneMeta;->getCustomTimeZone(I)Lcom/ibm/icu/util/TimeZone;

    move-result-object v30

    .line 2187
    move-object/from16 v0, p8

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/ibm/icu/util/Calendar;->setTimeZone(Lcom/ibm/icu/util/TimeZone;)V

    .line 2188
    invoke-virtual/range {v26 .. v26}, Ljava/text/ParsePosition;->getIndex()I

    move-result v19

    goto/16 :goto_0

    .line 2127
    .restart local v28    # "sign":I
    .restart local v29    # "signChar":C
    :cond_19
    const/16 v5, 0x2d

    move/from16 v0, v29

    if-ne v0, v5, :cond_17

    .line 2128
    const/16 v28, -0x1

    goto :goto_7

    .line 2150
    .restart local v20    # "numLen":I
    .restart local v23    # "orgPos":I
    :cond_1a
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Number;->intValue()I

    move-result v31

    .line 2151
    .local v31, "val":I
    const/16 v16, 0x0

    .local v16, "hour":I
    const/16 v18, 0x0

    .local v18, "min":I
    const/16 v27, 0x0

    .line 2152
    .local v27, "sec":I
    packed-switch v20, :pswitch_data_1

    .line 2169
    :goto_9
    const/16 v5, 0x17

    move/from16 v0, v16

    if-gt v0, v5, :cond_17

    const/16 v5, 0x3b

    move/from16 v0, v18

    if-gt v0, v5, :cond_17

    const/16 v5, 0x3b

    move/from16 v0, v27

    if-gt v0, v5, :cond_17

    .line 2173
    mul-int/lit8 v5, v16, 0x3c

    add-int v5, v5, v18

    mul-int/lit8 v5, v5, 0x3c

    add-int v5, v5, v27

    mul-int/lit16 v5, v5, 0x3e8

    mul-int v22, v5, v28

    .line 2174
    const/16 v24, 0x1

    goto :goto_8

    .line 2155
    :pswitch_c
    move/from16 v16, v31

    .line 2156
    goto :goto_9

    .line 2159
    :pswitch_d
    div-int/lit8 v16, v31, 0x64

    .line 2160
    rem-int/lit8 v18, v31, 0x64

    .line 2161
    goto :goto_9

    .line 2164
    :pswitch_e
    move/from16 v0, v31

    div-int/lit16 v0, v0, 0x2710

    move/from16 v16, v0

    .line 2165
    move/from16 v0, v31

    rem-int/lit16 v5, v0, 0x2710

    div-int/lit8 v18, v5, 0x64

    .line 2166
    rem-int/lit8 v27, v31, 0x64

    goto :goto_9

    .line 2196
    .end local v16    # "hour":I
    .end local v18    # "min":I
    .end local v20    # "numLen":I
    .end local v23    # "orgPos":I
    .end local v27    # "sec":I
    .end local v28    # "sign":I
    .end local v29    # "signChar":C
    .end local v31    # "val":I
    :cond_1b
    const/16 v33, 0x0

    .line 2197
    .local v33, "zsinfo":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    sparse-switch v25, :sswitch_data_0

    .line 2220
    :cond_1c
    :goto_a
    if-eqz v33, :cond_22

    .line 2221
    invoke-virtual/range {v33 .. v33}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->isStandard()Z

    move-result v5

    if-eqz v5, :cond_21

    .line 2222
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->tztype:I

    .line 2226
    :cond_1d
    :goto_b
    invoke-virtual/range {v33 .. v33}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/ibm/icu/util/TimeZone;->getTimeZone(Ljava/lang/String;)Lcom/ibm/icu/util/TimeZone;

    move-result-object v30

    .line 2227
    move-object/from16 v0, p8

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/ibm/icu/util/Calendar;->setTimeZone(Lcom/ibm/icu/util/TimeZone;)V

    .line 2228
    invoke-virtual/range {v33 .. v33}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->getString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int v19, p2, v5

    goto/16 :goto_0

    .line 2199
    :sswitch_0
    const/4 v5, 0x4

    move/from16 v0, p4

    if-ge v0, v5, :cond_1e

    .line 2200
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-virtual {v5}, Lcom/ibm/icu/text/DateFormatSymbols;->getZoneStringFormat()Lcom/ibm/icu/impl/ZoneStringFormat;

    move-result-object v5

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v5, v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat;->findSpecificShort(Ljava/lang/String;I)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    move-result-object v33

    .line 2201
    goto :goto_a

    .line 2202
    :cond_1e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-virtual {v5}, Lcom/ibm/icu/text/DateFormatSymbols;->getZoneStringFormat()Lcom/ibm/icu/impl/ZoneStringFormat;

    move-result-object v5

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v5, v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat;->findSpecificLong(Ljava/lang/String;I)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    move-result-object v33

    .line 2204
    goto :goto_a

    .line 2206
    :sswitch_1
    const/4 v5, 0x1

    move/from16 v0, p4

    if-ne v0, v5, :cond_1f

    .line 2207
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-virtual {v5}, Lcom/ibm/icu/text/DateFormatSymbols;->getZoneStringFormat()Lcom/ibm/icu/impl/ZoneStringFormat;

    move-result-object v5

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v5, v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat;->findGenericShort(Ljava/lang/String;I)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    move-result-object v33

    .line 2208
    goto :goto_a

    :cond_1f
    const/4 v5, 0x4

    move/from16 v0, p4

    if-ne v0, v5, :cond_1c

    .line 2209
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-virtual {v5}, Lcom/ibm/icu/text/DateFormatSymbols;->getZoneStringFormat()Lcom/ibm/icu/impl/ZoneStringFormat;

    move-result-object v5

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v5, v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat;->findGenericLong(Ljava/lang/String;I)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    move-result-object v33

    .line 2210
    goto :goto_a

    .line 2213
    :sswitch_2
    const/4 v5, 0x1

    move/from16 v0, p4

    if-ne v0, v5, :cond_20

    .line 2214
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-virtual {v5}, Lcom/ibm/icu/text/DateFormatSymbols;->getZoneStringFormat()Lcom/ibm/icu/impl/ZoneStringFormat;

    move-result-object v5

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v5, v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat;->findSpecificShort(Ljava/lang/String;I)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    move-result-object v33

    .line 2215
    goto/16 :goto_a

    :cond_20
    const/4 v5, 0x4

    move/from16 v0, p4

    if-ne v0, v5, :cond_1c

    .line 2216
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-virtual {v5}, Lcom/ibm/icu/text/DateFormatSymbols;->getZoneStringFormat()Lcom/ibm/icu/impl/ZoneStringFormat;

    move-result-object v5

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v5, v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat;->findGenericLocation(Ljava/lang/String;I)Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;

    move-result-object v33

    goto/16 :goto_a

    .line 2223
    :cond_21
    invoke-virtual/range {v33 .. v33}, Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;->isDaylight()Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 2224
    const/4 v5, 0x2

    move-object/from16 v0, p0

    iput v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->tztype:I

    goto/16 :goto_b

    .line 2231
    :cond_22
    move/from16 v0, p2

    neg-int v0, v0

    move/from16 v19, v0

    goto/16 :goto_0

    .line 2235
    .end local v15    # "gmtoff":Ljava/lang/Integer;
    .end local v22    # "offset":I
    .end local v24    # "parsed":Z
    .end local v30    # "tz":Lcom/ibm/icu/util/TimeZone;
    .end local v33    # "zsinfo":Lcom/ibm/icu/impl/ZoneStringFormat$ZoneStringInfo;
    :pswitch_f
    const/4 v5, 0x2

    move/from16 v0, p4

    if-gt v0, v5, :cond_23

    .line 2240
    const/4 v5, 0x2

    add-int/lit8 v6, v32, -0x1

    mul-int/lit8 v6, v6, 0x3

    move-object/from16 v0, p8

    invoke-virtual {v0, v5, v6}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 2241
    invoke-virtual/range {v26 .. v26}, Ljava/text/ParsePosition;->getIndex()I

    move-result v19

    goto/16 :goto_0

    .line 2248
    :cond_23
    const/4 v8, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->quarters:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchQuarterString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    .line 2250
    .restart local v19    # "newStart":I
    if-gtz v19, :cond_1

    .line 2253
    const/4 v8, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->shortQuarters:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchQuarterString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    goto/16 :goto_0

    .line 2259
    .end local v19    # "newStart":I
    :pswitch_10
    const/4 v5, 0x2

    move/from16 v0, p4

    if-gt v0, v5, :cond_24

    .line 2264
    const/4 v5, 0x2

    add-int/lit8 v6, v32, -0x1

    mul-int/lit8 v6, v6, 0x3

    move-object/from16 v0, p8

    invoke-virtual {v0, v5, v6}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 2265
    invoke-virtual/range {v26 .. v26}, Ljava/text/ParsePosition;->getIndex()I

    move-result v19

    goto/16 :goto_0

    .line 2272
    :cond_24
    const/4 v8, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneQuarters:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchQuarterString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    .line 2274
    .restart local v19    # "newStart":I
    if-gtz v19, :cond_1

    .line 2277
    const/4 v8, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v9, v5, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortQuarters:[Ljava/lang/String;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p8

    invoke-virtual/range {v5 .. v10}, Lcom/ibm/icu/text/SimpleDateFormat;->matchQuarterString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v19

    goto/16 :goto_0

    .line 2302
    .end local v19    # "newStart":I
    :cond_25
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    move-object/from16 v3, v26

    move/from16 v4, p6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/SimpleDateFormat;->parseInt(Ljava/lang/String;ILjava/text/ParsePosition;Z)Ljava/lang/Number;

    move-result-object v21

    .line 2305
    :goto_c
    if-eqz v21, :cond_27

    .line 2306
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Number;->intValue()I

    move-result v5

    move-object/from16 v0, p8

    invoke-virtual {v0, v14, v5}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 2307
    invoke-virtual/range {v26 .. v26}, Ljava/text/ParsePosition;->getIndex()I

    move-result v19

    goto/16 :goto_0

    .line 2304
    :cond_26
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    move/from16 v3, p6

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/SimpleDateFormat;->parseInt(Ljava/lang/String;Ljava/text/ParsePosition;Z)Ljava/lang/Number;

    move-result-object v21

    goto :goto_c

    .line 2309
    :cond_27
    move/from16 v0, p2

    neg-int v0, v0

    move/from16 v19, v0

    goto/16 :goto_0

    .line 1964
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_b
        :pswitch_8
        :pswitch_4
        :pswitch_f
        :pswitch_10
        :pswitch_b
    .end packed-switch

    .line 2152
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_c
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_e
        :pswitch_e
    .end packed-switch

    .line 2197
    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x18 -> :sswitch_1
        0x1d -> :sswitch_2
    .end sparse-switch
.end method

.method public toLocalizedPattern()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2416
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    const-string/jumbo v1, "GyMdkHmsSEDFwWahKzYeugAZvcLQqV"

    iget-object v2, p0, Lcom/ibm/icu/text/SimpleDateFormat;->formatData:Lcom/ibm/icu/text/DateFormatSymbols;

    iget-object v2, v2, Lcom/ibm/icu/text/DateFormatSymbols;->localPatternChars:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/ibm/icu/text/SimpleDateFormat;->translatePattern(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toPattern()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2408
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->pattern:Ljava/lang/String;

    return-object v0
.end method

.method protected zeroPaddingNumber(JII)Ljava/lang/String;
    .locals 1
    .param p1, "value"    # J
    .param p3, "minDigits"    # I
    .param p4, "maxDigits"    # I

    .prologue
    .line 1491
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    invoke-virtual {v0, p3}, Lcom/ibm/icu/text/NumberFormat;->setMinimumIntegerDigits(I)V

    .line 1492
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    invoke-virtual {v0, p4}, Lcom/ibm/icu/text/NumberFormat;->setMaximumIntegerDigits(I)V

    .line 1493
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    invoke-virtual {v0, p1, p2}, Lcom/ibm/icu/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected zeroPaddingNumber(Ljava/lang/StringBuffer;III)V
    .locals 5
    .param p1, "buf"    # Ljava/lang/StringBuffer;
    .param p2, "value"    # I
    .param p3, "minDigits"    # I
    .param p4, "maxDigits"    # I

    .prologue
    .line 1415
    iget-boolean v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->useLocalZeroPaddingNumberFormat:Z

    if-eqz v0, :cond_0

    .line 1416
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/ibm/icu/text/SimpleDateFormat;->fastZeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    .line 1422
    :goto_0
    return-void

    .line 1418
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    invoke-virtual {v0, p3}, Lcom/ibm/icu/text/NumberFormat;->setMinimumIntegerDigits(I)V

    .line 1419
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    invoke-virtual {v0, p4}, Lcom/ibm/icu/text/NumberFormat;->setMaximumIntegerDigits(I)V

    .line 1420
    iget-object v0, p0, Lcom/ibm/icu/text/SimpleDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    int-to-long v2, p2

    new-instance v1, Ljava/text/FieldPosition;

    const/4 v4, -0x1

    invoke-direct {v1, v4}, Ljava/text/FieldPosition;-><init>(I)V

    invoke-virtual {v0, v2, v3, p1, v1}, Lcom/ibm/icu/text/NumberFormat;->format(JLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
