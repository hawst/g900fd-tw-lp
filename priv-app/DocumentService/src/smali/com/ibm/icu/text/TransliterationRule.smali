.class Lcom/ibm/icu/text/TransliterationRule;
.super Ljava/lang/Object;
.source "TransliterationRule.java"


# static fields
.field static final ANCHOR_END:I = 0x2

.field static final ANCHOR_START:I = 0x1


# instance fields
.field private anteContext:Lcom/ibm/icu/text/StringMatcher;

.field private anteContextLength:I

.field private final data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

.field flags:B

.field private key:Lcom/ibm/icu/text/StringMatcher;

.field private keyLength:I

.field private output:Lcom/ibm/icu/text/UnicodeReplacer;

.field private pattern:Ljava/lang/String;

.field private postContext:Lcom/ibm/icu/text/StringMatcher;

.field segments:[Lcom/ibm/icu/text/UnicodeMatcher;


# direct methods
.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;II[Lcom/ibm/icu/text/UnicodeMatcher;ZZLcom/ibm/icu/text/RuleBasedTransliterator$Data;)V
    .locals 7
    .param p1, "input"    # Ljava/lang/String;
    .param p2, "anteContextPos"    # I
    .param p3, "postContextPos"    # I
    .param p4, "output"    # Ljava/lang/String;
    .param p5, "cursorPos"    # I
    .param p6, "cursorOffset"    # I
    .param p7, "segs"    # [Lcom/ibm/icu/text/UnicodeMatcher;
    .param p8, "anchorStart"    # Z
    .param p9, "anchorEnd"    # Z
    .param p10, "theData"    # Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/ibm/icu/text/TransliterationRule;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .line 162
    if-gez p2, :cond_6

    .line 163
    const/4 v2, 0x0

    iput v2, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    .line 170
    :goto_0
    if-gez p3, :cond_8

    .line 171
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    iget v3, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/ibm/icu/text/TransliterationRule;->keyLength:I

    .line 179
    :goto_1
    if-gez p5, :cond_b

    .line 180
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result p5

    .line 189
    :cond_0
    iput-object p7, p0, Lcom/ibm/icu/text/TransliterationRule;->segments:[Lcom/ibm/icu/text/UnicodeMatcher;

    .line 191
    iput-object p1, p0, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    .line 192
    const/4 v2, 0x0

    iput-byte v2, p0, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    .line 193
    if-eqz p8, :cond_1

    .line 194
    iget-byte v2, p0, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    or-int/lit8 v2, v2, 0x1

    int-to-byte v2, v2

    iput-byte v2, p0, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    .line 196
    :cond_1
    if-eqz p9, :cond_2

    .line 197
    iget-byte v2, p0, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    or-int/lit8 v2, v2, 0x2

    int-to-byte v2, v2

    iput-byte v2, p0, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    .line 200
    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContext:Lcom/ibm/icu/text/StringMatcher;

    .line 201
    iget v2, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    if-lez v2, :cond_3

    .line 202
    new-instance v2, Lcom/ibm/icu/text/StringMatcher;

    iget-object v3, p0, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    const/4 v4, 0x0

    iget v5, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/ibm/icu/text/TransliterationRule;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-direct {v2, v3, v4, v5}, Lcom/ibm/icu/text/StringMatcher;-><init>(Ljava/lang/String;ILcom/ibm/icu/text/RuleBasedTransliterator$Data;)V

    iput-object v2, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContext:Lcom/ibm/icu/text/StringMatcher;

    .line 206
    :cond_3
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/ibm/icu/text/TransliterationRule;->key:Lcom/ibm/icu/text/StringMatcher;

    .line 207
    iget v2, p0, Lcom/ibm/icu/text/TransliterationRule;->keyLength:I

    if-lez v2, :cond_4

    .line 208
    new-instance v2, Lcom/ibm/icu/text/StringMatcher;

    iget-object v3, p0, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    iget v4, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    iget v5, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    iget v6, p0, Lcom/ibm/icu/text/TransliterationRule;->keyLength:I

    add-int/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/ibm/icu/text/TransliterationRule;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-direct {v2, v3, v4, v5}, Lcom/ibm/icu/text/StringMatcher;-><init>(Ljava/lang/String;ILcom/ibm/icu/text/RuleBasedTransliterator$Data;)V

    iput-object v2, p0, Lcom/ibm/icu/text/TransliterationRule;->key:Lcom/ibm/icu/text/StringMatcher;

    .line 212
    :cond_4
    iget-object v2, p0, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget v3, p0, Lcom/ibm/icu/text/TransliterationRule;->keyLength:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    sub-int v1, v2, v3

    .line 213
    .local v1, "postContextLength":I
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/ibm/icu/text/TransliterationRule;->postContext:Lcom/ibm/icu/text/StringMatcher;

    .line 214
    if-lez v1, :cond_5

    .line 215
    new-instance v2, Lcom/ibm/icu/text/StringMatcher;

    iget-object v3, p0, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    iget v4, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    iget v5, p0, Lcom/ibm/icu/text/TransliterationRule;->keyLength:I

    add-int/2addr v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/ibm/icu/text/TransliterationRule;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-direct {v2, v3, v4, v5}, Lcom/ibm/icu/text/StringMatcher;-><init>(Ljava/lang/String;ILcom/ibm/icu/text/RuleBasedTransliterator$Data;)V

    iput-object v2, p0, Lcom/ibm/icu/text/TransliterationRule;->postContext:Lcom/ibm/icu/text/StringMatcher;

    .line 219
    :cond_5
    new-instance v2, Lcom/ibm/icu/text/StringReplacer;

    add-int v3, p5, p6

    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRule;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-direct {v2, p4, v3, v4}, Lcom/ibm/icu/text/StringReplacer;-><init>(Ljava/lang/String;ILcom/ibm/icu/text/RuleBasedTransliterator$Data;)V

    iput-object v2, p0, Lcom/ibm/icu/text/TransliterationRule;->output:Lcom/ibm/icu/text/UnicodeReplacer;

    .line 220
    return-void

    .line 165
    .end local v1    # "postContextLength":I
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-le p2, v2, :cond_7

    .line 166
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Invalid ante context"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 168
    :cond_7
    iput p2, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    goto/16 :goto_0

    .line 173
    :cond_8
    iget v2, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    if-lt p3, v2, :cond_9

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-le p3, v2, :cond_a

    .line 175
    :cond_9
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Invalid post context"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 177
    :cond_a
    iget v2, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    sub-int v2, p3, v2

    iput v2, p0, Lcom/ibm/icu/text/TransliterationRule;->keyLength:I

    goto/16 :goto_1

    .line 181
    :cond_b
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v2

    if-le p5, v2, :cond_0

    .line 182
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Invalid cursor position"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method static final posAfter(Lcom/ibm/icu/text/Replaceable;I)I
    .locals 1
    .param p0, "str"    # Lcom/ibm/icu/text/Replaceable;
    .param p1, "pos"    # I

    .prologue
    .line 341
    if-ltz p1, :cond_0

    invoke-interface {p0}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v0

    if-ge p1, v0, :cond_0

    invoke-interface {p0, p1}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v0

    add-int/2addr v0, p1

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, p1, 0x1

    goto :goto_0
.end method

.method static final posBefore(Lcom/ibm/icu/text/Replaceable;I)I
    .locals 1
    .param p0, "str"    # Lcom/ibm/icu/text/Replaceable;
    .param p1, "pos"    # I

    .prologue
    .line 335
    if-lez p1, :cond_0

    add-int/lit8 v0, p1, -0x1

    invoke-interface {p0, v0}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v0

    sub-int v0, p1, v0

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, p1, -0x1

    goto :goto_0
.end method


# virtual methods
.method addSourceSetTo(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 6
    .param p1, "toUnionTo"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 552
    iget v4, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    iget v5, p0, Lcom/ibm/icu/text/TransliterationRule;->keyLength:I

    add-int v2, v4, v5

    .line 553
    .local v2, "limit":I
    iget v1, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 554
    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    invoke-static {v4, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 555
    .local v0, "ch":I
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 556
    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRule;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-virtual {v4, v0}, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->lookupMatcher(I)Lcom/ibm/icu/text/UnicodeMatcher;

    move-result-object v3

    .line 557
    .local v3, "matcher":Lcom/ibm/icu/text/UnicodeMatcher;
    if-nez v3, :cond_0

    .line 558
    invoke-virtual {p1, v0}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_0

    .line 560
    :cond_0
    invoke-interface {v3, p1}, Lcom/ibm/icu/text/UnicodeMatcher;->addMatchSetTo(Lcom/ibm/icu/text/UnicodeSet;)V

    goto :goto_0

    .line 563
    .end local v0    # "ch":I
    .end local v3    # "matcher":Lcom/ibm/icu/text/UnicodeMatcher;
    :cond_1
    return-void
.end method

.method addTargetSetTo(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 1
    .param p1, "toUnionTo"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 570
    iget-object v0, p0, Lcom/ibm/icu/text/TransliterationRule;->output:Lcom/ibm/icu/text/UnicodeReplacer;

    invoke-interface {v0, p1}, Lcom/ibm/icu/text/UnicodeReplacer;->addReplacementSetTo(Lcom/ibm/icu/text/UnicodeSet;)V

    .line 571
    return-void
.end method

.method public getAnteContextLength()I
    .locals 2

    .prologue
    .line 228
    iget v1, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    iget-byte v0, p0, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final getIndexValue()I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 238
    iget v2, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    iget-object v3, p0, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 244
    :cond_0
    :goto_0
    return v1

    .line 243
    :cond_1
    iget-object v2, p0, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    iget v3, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    invoke-static {v2, v3}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 244
    .local v0, "c":I
    iget-object v2, p0, Lcom/ibm/icu/text/TransliterationRule;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-virtual {v2, v0}, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->lookupMatcher(I)Lcom/ibm/icu/text/UnicodeMatcher;

    move-result-object v2

    if-nez v2, :cond_0

    and-int/lit16 v1, v0, 0xff

    goto :goto_0
.end method

.method public masks(Lcom/ibm/icu/text/TransliterationRule;)Z
    .locals 10
    .param p1, "r2"    # Lcom/ibm/icu/text/TransliterationRule;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 309
    iget-object v7, p0, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v2

    .line 310
    .local v2, "len":I
    iget v0, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    .line 311
    .local v0, "left":I
    iget v1, p1, Lcom/ibm/icu/text/TransliterationRule;->anteContextLength:I

    .line 312
    .local v1, "left2":I
    iget-object v7, p0, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    sub-int v3, v7, v0

    .line 313
    .local v3, "right":I
    iget-object v7, p1, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    sub-int v4, v7, v1

    .line 319
    .local v4, "right2":I
    if-ne v0, v1, :cond_3

    if-ne v3, v4, :cond_3

    iget v7, p0, Lcom/ibm/icu/text/TransliterationRule;->keyLength:I

    iget v8, p1, Lcom/ibm/icu/text/TransliterationRule;->keyLength:I

    if-gt v7, v8, :cond_3

    iget-object v7, p1, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    iget-object v8, p0, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    invoke-virtual {v7, v5, v8, v5, v2}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 323
    iget-byte v7, p0, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    iget-byte v8, p1, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    if-eq v7, v8, :cond_1

    iget-byte v7, p0, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    and-int/lit8 v7, v7, 0x1

    if-nez v7, :cond_0

    iget-byte v7, p0, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    and-int/lit8 v7, v7, 0x2

    if-eqz v7, :cond_1

    :cond_0
    iget-byte v7, p1, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    and-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_2

    iget-byte v7, p1, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    and-int/lit8 v7, v7, 0x2

    if-eqz v7, :cond_2

    :cond_1
    move v5, v6

    .line 328
    :cond_2
    :goto_0
    return v5

    :cond_3
    if-gt v0, v1, :cond_5

    if-lt v3, v4, :cond_4

    if-ne v3, v4, :cond_5

    iget v7, p0, Lcom/ibm/icu/text/TransliterationRule;->keyLength:I

    iget v8, p1, Lcom/ibm/icu/text/TransliterationRule;->keyLength:I

    if-gt v7, v8, :cond_5

    :cond_4
    iget-object v7, p1, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    sub-int v8, v1, v0

    iget-object v9, p0, Lcom/ibm/icu/text/TransliterationRule;->pattern:Ljava/lang/String;

    invoke-virtual {v7, v8, v9, v5, v2}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v7

    if-eqz v7, :cond_5

    :goto_1
    move v5, v6

    goto :goto_0

    :cond_5
    move v6, v5

    goto :goto_1
.end method

.method public matchAndReplace(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)I
    .locals 12
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "pos"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "incremental"    # Z

    .prologue
    .line 379
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRule;->segments:[Lcom/ibm/icu/text/UnicodeMatcher;

    if-eqz v10, :cond_0

    .line 380
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRule;->segments:[Lcom/ibm/icu/text/UnicodeMatcher;

    array-length v10, v10

    if-ge v1, v10, :cond_0

    .line 381
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRule;->segments:[Lcom/ibm/icu/text/UnicodeMatcher;

    aget-object v10, v10, v1

    check-cast v10, Lcom/ibm/icu/text/StringMatcher;

    invoke-virtual {v10}, Lcom/ibm/icu/text/StringMatcher;->resetMatch()V

    .line 380
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 386
    .end local v1    # "i":I
    :cond_0
    const/4 v10, 0x1

    new-array v2, v10, [I

    .line 401
    .local v2, "intRef":[I
    iget v10, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    invoke-static {p1, v10}, Lcom/ibm/icu/text/TransliterationRule;->posBefore(Lcom/ibm/icu/text/Replaceable;I)I

    move-result v0

    .line 406
    .local v0, "anteLimit":I
    const/4 v10, 0x0

    iget v11, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    invoke-static {p1, v11}, Lcom/ibm/icu/text/TransliterationRule;->posBefore(Lcom/ibm/icu/text/Replaceable;I)I

    move-result v11

    aput v11, v2, v10

    .line 408
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContext:Lcom/ibm/icu/text/StringMatcher;

    if-eqz v10, :cond_2

    .line 409
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContext:Lcom/ibm/icu/text/StringMatcher;

    const/4 v11, 0x0

    invoke-virtual {v10, p1, v2, v0, v11}, Lcom/ibm/icu/text/StringMatcher;->matches(Lcom/ibm/icu/text/Replaceable;[IIZ)I

    move-result v5

    .line 410
    .local v5, "match":I
    const/4 v10, 0x2

    if-eq v5, v10, :cond_2

    .line 411
    const/4 v5, 0x0

    .line 480
    .end local v5    # "match":I
    :cond_1
    :goto_1
    return v5

    .line 415
    :cond_2
    const/4 v10, 0x0

    aget v9, v2, v10

    .line 417
    .local v9, "oText":I
    invoke-static {p1, v9}, Lcom/ibm/icu/text/TransliterationRule;->posAfter(Lcom/ibm/icu/text/Replaceable;I)I

    move-result v6

    .line 421
    .local v6, "minOText":I
    iget-byte v10, p0, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    and-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_3

    if-eq v9, v0, :cond_3

    .line 422
    const/4 v5, 0x0

    goto :goto_1

    .line 427
    :cond_3
    const/4 v10, 0x0

    iget v11, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    aput v11, v2, v10

    .line 429
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRule;->key:Lcom/ibm/icu/text/StringMatcher;

    if-eqz v10, :cond_4

    .line 430
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRule;->key:Lcom/ibm/icu/text/StringMatcher;

    iget v11, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    invoke-virtual {v10, p1, v2, v11, p3}, Lcom/ibm/icu/text/StringMatcher;->matches(Lcom/ibm/icu/text/Replaceable;[IIZ)I

    move-result v5

    .line 431
    .restart local v5    # "match":I
    const/4 v10, 0x2

    if-ne v5, v10, :cond_1

    .line 436
    .end local v5    # "match":I
    :cond_4
    const/4 v10, 0x0

    aget v3, v2, v10

    .line 438
    .local v3, "keyLimit":I
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRule;->postContext:Lcom/ibm/icu/text/StringMatcher;

    if-eqz v10, :cond_6

    .line 439
    if-eqz p3, :cond_5

    iget v10, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    if-ne v3, v10, :cond_5

    .line 444
    const/4 v5, 0x1

    goto :goto_1

    .line 447
    :cond_5
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRule;->postContext:Lcom/ibm/icu/text/StringMatcher;

    iget v11, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    invoke-virtual {v10, p1, v2, v11, p3}, Lcom/ibm/icu/text/StringMatcher;->matches(Lcom/ibm/icu/text/Replaceable;[IIZ)I

    move-result v5

    .line 448
    .restart local v5    # "match":I
    const/4 v10, 0x2

    if-ne v5, v10, :cond_1

    .line 453
    .end local v5    # "match":I
    :cond_6
    const/4 v10, 0x0

    aget v9, v2, v10

    .line 457
    iget-byte v10, p0, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    and-int/lit8 v10, v10, 0x2

    if-eqz v10, :cond_8

    .line 458
    iget v10, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    if-eq v9, v10, :cond_7

    .line 459
    const/4 v5, 0x0

    goto :goto_1

    .line 461
    :cond_7
    if-eqz p3, :cond_8

    .line 462
    const/4 v5, 0x1

    goto :goto_1

    .line 471
    :cond_8
    iget-object v10, p0, Lcom/ibm/icu/text/TransliterationRule;->output:Lcom/ibm/icu/text/UnicodeReplacer;

    iget v11, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    invoke-interface {v10, p1, v11, v3, v2}, Lcom/ibm/icu/text/UnicodeReplacer;->replace(Lcom/ibm/icu/text/Replaceable;II[I)I

    move-result v7

    .line 472
    .local v7, "newLength":I
    iget v10, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    sub-int v10, v3, v10

    sub-int v4, v7, v10

    .line 473
    .local v4, "lenDelta":I
    const/4 v10, 0x0

    aget v8, v2, v10

    .line 475
    .local v8, "newStart":I
    add-int/2addr v9, v4

    .line 476
    iget v10, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    add-int/2addr v10, v4

    iput v10, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 477
    iget v10, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    add-int/2addr v10, v4

    iput v10, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    .line 479
    iget v10, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v10

    invoke-static {v10, v8}, Ljava/lang/Math;->min(II)I

    move-result v10

    invoke-static {v6, v10}, Ljava/lang/Math;->max(II)I

    move-result v10

    iput v10, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 480
    const/4 v5, 0x2

    goto :goto_1
.end method

.method final matchesIndexValue(I)Z
    .locals 2
    .param p1, "v"    # I

    .prologue
    .line 260
    iget-object v1, p0, Lcom/ibm/icu/text/TransliterationRule;->key:Lcom/ibm/icu/text/StringMatcher;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/TransliterationRule;->key:Lcom/ibm/icu/text/StringMatcher;

    .line 261
    .local v0, "m":Lcom/ibm/icu/text/UnicodeMatcher;
    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lcom/ibm/icu/text/UnicodeMatcher;->matchesIndexValue(I)Z

    move-result v1

    :goto_1
    return v1

    .line 260
    .end local v0    # "m":Lcom/ibm/icu/text/UnicodeMatcher;
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/TransliterationRule;->postContext:Lcom/ibm/icu/text/StringMatcher;

    goto :goto_0

    .line 261
    .restart local v0    # "m":Lcom/ibm/icu/text/UnicodeMatcher;
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public toRule(Z)Ljava/lang/String;
    .locals 5
    .param p1, "escapeUnprintable"    # Z

    .prologue
    const/4 v3, 0x1

    .line 490
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 495
    .local v2, "rule":Ljava/lang/StringBuffer;
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 499
    .local v1, "quoteBuf":Ljava/lang/StringBuffer;
    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContext:Lcom/ibm/icu/text/StringMatcher;

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRule;->postContext:Lcom/ibm/icu/text/StringMatcher;

    if-eqz v4, :cond_5

    :cond_0
    move v0, v3

    .line 503
    .local v0, "emitBraces":Z
    :goto_0
    iget-byte v4, p0, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    .line 504
    const/16 v4, 0x5e

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 508
    :cond_1
    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRule;->anteContext:Lcom/ibm/icu/text/StringMatcher;

    invoke-static {v2, v4, p1, v1}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;Lcom/ibm/icu/text/UnicodeMatcher;ZLjava/lang/StringBuffer;)V

    .line 510
    if-eqz v0, :cond_2

    .line 511
    const/16 v4, 0x7b

    invoke-static {v2, v4, v3, p1, v1}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;IZZLjava/lang/StringBuffer;)V

    .line 514
    :cond_2
    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRule;->key:Lcom/ibm/icu/text/StringMatcher;

    invoke-static {v2, v4, p1, v1}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;Lcom/ibm/icu/text/UnicodeMatcher;ZLjava/lang/StringBuffer;)V

    .line 516
    if-eqz v0, :cond_3

    .line 517
    const/16 v4, 0x7d

    invoke-static {v2, v4, v3, p1, v1}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;IZZLjava/lang/StringBuffer;)V

    .line 520
    :cond_3
    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRule;->postContext:Lcom/ibm/icu/text/StringMatcher;

    invoke-static {v2, v4, p1, v1}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;Lcom/ibm/icu/text/UnicodeMatcher;ZLjava/lang/StringBuffer;)V

    .line 523
    iget-byte v4, p0, Lcom/ibm/icu/text/TransliterationRule;->flags:B

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_4

    .line 524
    const/16 v4, 0x24

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 527
    :cond_4
    const-string/jumbo v4, " > "

    invoke-static {v2, v4, v3, p1, v1}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;Ljava/lang/String;ZZLjava/lang/StringBuffer;)V

    .line 531
    iget-object v4, p0, Lcom/ibm/icu/text/TransliterationRule;->output:Lcom/ibm/icu/text/UnicodeReplacer;

    invoke-interface {v4, p1}, Lcom/ibm/icu/text/UnicodeReplacer;->toReplacerPattern(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v3, p1, v1}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;Ljava/lang/String;ZZLjava/lang/StringBuffer;)V

    .line 534
    const/16 v4, 0x3b

    invoke-static {v2, v4, v3, p1, v1}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;IZZLjava/lang/StringBuffer;)V

    .line 536
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 499
    .end local v0    # "emitBraces":Z
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 544
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/TransliterationRule;->toRule(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
