.class final Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;
.super Ljava/lang/Object;
.source "BreakTransliterator.java"

# interfaces
.implements Ljava/text/CharacterIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/BreakTransliterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ReplaceableCharacterIterator"
.end annotation


# instance fields
.field private begin:I

.field private end:I

.field private pos:I

.field private text:Lcom/ibm/icu/text/Replaceable;


# direct methods
.method public constructor <init>(Lcom/ibm/icu/text/Replaceable;III)V
    .locals 2
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "begin"    # I
    .param p3, "end"    # I
    .param p4, "pos"    # I

    .prologue
    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    if-nez p1, :cond_0

    .line 182
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 184
    :cond_0
    iput-object p1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->text:Lcom/ibm/icu/text/Replaceable;

    .line 186
    if-ltz p2, :cond_1

    if-gt p2, p3, :cond_1

    invoke-interface {p1}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v0

    if-le p3, v0, :cond_2

    .line 187
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid substring range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_2
    if-lt p4, p2, :cond_3

    if-le p4, p3, :cond_4

    .line 191
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid position"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_4
    iput p2, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->begin:I

    .line 195
    iput p3, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->end:I

    .line 196
    iput p4, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    .line 197
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 371
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 373
    .local v1, "other":Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;
    return-object v1

    .line 375
    .end local v1    # "other":Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;
    :catch_0
    move-exception v0

    .line 376
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2
.end method

.method public current()C
    .locals 2

    .prologue
    .line 260
    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    iget v1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->begin:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    iget v1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->end:I

    if-ge v0, v1, :cond_0

    .line 261
    iget-object v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->text:Lcom/ibm/icu/text/Replaceable;

    iget v1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    invoke-interface {v0, v1}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v0

    .line 264
    :goto_0
    return v0

    :cond_0
    const v0, 0xffff

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 334
    if-ne p0, p1, :cond_1

    .line 352
    :cond_0
    :goto_0
    return v1

    .line 337
    :cond_1
    instance-of v3, p1, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;

    if-nez v3, :cond_2

    move v1, v2

    .line 338
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 341
    check-cast v0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;

    .line 343
    .local v0, "that":Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;
    invoke-virtual {p0}, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->hashCode()I

    move-result v3

    invoke-virtual {v0}, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->hashCode()I

    move-result v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 344
    goto :goto_0

    .line 346
    :cond_3
    iget-object v3, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->text:Lcom/ibm/icu/text/Replaceable;

    iget-object v4, v0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->text:Lcom/ibm/icu/text/Replaceable;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 347
    goto :goto_0

    .line 349
    :cond_4
    iget v3, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    iget v4, v0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    if-ne v3, v4, :cond_5

    iget v3, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->begin:I

    iget v4, v0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->begin:I

    if-ne v3, v4, :cond_5

    iget v3, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->end:I

    iget v4, v0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->end:I

    if-eq v3, v4, :cond_0

    :cond_5
    move v1, v2

    .line 350
    goto :goto_0
.end method

.method public first()C
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->begin:I

    iput v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    .line 224
    invoke-virtual {p0}, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->current()C

    move-result v0

    return v0
.end method

.method public getBeginIndex()I
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->begin:I

    return v0
.end method

.method public getEndIndex()I
    .locals 1

    .prologue
    .line 314
    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->end:I

    return v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 323
    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->text:Lcom/ibm/icu/text/Replaceable;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    xor-int/2addr v0, v1

    iget v1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->begin:I

    xor-int/2addr v0, v1

    iget v1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->end:I

    xor-int/2addr v0, v1

    return v0
.end method

.method public last()C
    .locals 2

    .prologue
    .line 233
    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->end:I

    iget v1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->begin:I

    if-eq v0, v1, :cond_0

    .line 234
    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->end:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    .line 238
    :goto_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->current()C

    move-result v0

    return v0

    .line 236
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->end:I

    iput v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    goto :goto_0
.end method

.method public next()C
    .locals 2

    .prologue
    .line 274
    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    iget v1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->end:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 275
    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    .line 276
    iget-object v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->text:Lcom/ibm/icu/text/Replaceable;

    iget v1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    invoke-interface {v0, v1}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v0

    .line 280
    :goto_0
    return v0

    .line 279
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->end:I

    iput v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    .line 280
    const v0, 0xffff

    goto :goto_0
.end method

.method public previous()C
    .locals 2

    .prologue
    .line 290
    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    iget v1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->begin:I

    if-le v0, v1, :cond_0

    .line 291
    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    .line 292
    iget-object v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->text:Lcom/ibm/icu/text/Replaceable;

    iget v1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    invoke-interface {v0, v1}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v0

    .line 295
    :goto_0
    return v0

    :cond_0
    const v0, 0xffff

    goto :goto_0
.end method

.method public setIndex(I)C
    .locals 2
    .param p1, "p"    # I

    .prologue
    .line 247
    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->begin:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->end:I

    if-le p1, v0, :cond_1

    .line 248
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 250
    :cond_1
    iput p1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    .line 251
    invoke-virtual {p0}, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->current()C

    move-result v0

    return v0
.end method

.method public setText(Lcom/ibm/icu/text/Replaceable;)V
    .locals 2
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;

    .prologue
    const/4 v1, 0x0

    .line 208
    if-nez p1, :cond_0

    .line 209
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 211
    :cond_0
    iput-object p1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->text:Lcom/ibm/icu/text/Replaceable;

    .line 212
    iput v1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->begin:I

    .line 213
    invoke-interface {p1}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->end:I

    .line 214
    iput v1, p0, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;->pos:I

    .line 215
    return-void
.end method
