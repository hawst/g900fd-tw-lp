.class final Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
.super Ljava/lang/Object;
.source "RuleBasedCollator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/RuleBasedCollator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "UCAConstants"
.end annotation


# instance fields
.field FIRST_IMPLICIT_:[I

.field FIRST_NON_VARIABLE_:[I

.field FIRST_PRIMARY_IGNORABLE_:[I

.field FIRST_SECONDARY_IGNORABLE_:[I

.field FIRST_TERTIARY_IGNORABLE_:[I

.field FIRST_TRAILING_:[I

.field FIRST_VARIABLE_:[I

.field LAST_IMPLICIT_:[I

.field LAST_NON_VARIABLE_:[I

.field LAST_PRIMARY_IGNORABLE_:[I

.field LAST_SECONDARY_IGNORABLE_:[I

.field LAST_TERTIARY_IGNORABLE_:[I

.field LAST_TRAILING_:[I

.field LAST_VARIABLE_:[I

.field PRIMARY_IMPLICIT_MAX_:I

.field PRIMARY_IMPLICIT_MIN_:I

.field PRIMARY_SPECIAL_MAX_:I

.field PRIMARY_SPECIAL_MIN_:I

.field PRIMARY_TOP_MIN_:I

.field PRIMARY_TRAILING_MAX_:I

.field PRIMARY_TRAILING_MIN_:I

.field RESET_TOP_VALUE_:[I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1504
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_TERTIARY_IGNORABLE_:[I

    .line 1505
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_TERTIARY_IGNORABLE_:[I

    .line 1506
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_PRIMARY_IGNORABLE_:[I

    .line 1507
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_SECONDARY_IGNORABLE_:[I

    .line 1508
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_SECONDARY_IGNORABLE_:[I

    .line 1509
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_PRIMARY_IGNORABLE_:[I

    .line 1510
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_VARIABLE_:[I

    .line 1511
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_VARIABLE_:[I

    .line 1512
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_NON_VARIABLE_:[I

    .line 1513
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_NON_VARIABLE_:[I

    .line 1514
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->RESET_TOP_VALUE_:[I

    .line 1515
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_IMPLICIT_:[I

    .line 1516
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_IMPLICIT_:[I

    .line 1517
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->FIRST_TRAILING_:[I

    .line 1518
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;->LAST_TRAILING_:[I

    return-void
.end method
