.class Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;
.super Ljava/lang/Object;
.source "CollationParsedRuleBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CollationParsedRuleBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MaxJamoExpansionTable"
.end annotation


# instance fields
.field m_endExpansionCE_:Ljava/util/Vector;

.field m_isV_:Ljava/util/Vector;

.field m_maxLSize_:B

.field m_maxTSize_:B

.field m_maxVSize_:B


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    .line 830
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_isV_:Ljava/util/Vector;

    .line 831
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    new-instance v1, Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 832
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_isV_:Ljava/util/Vector;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 833
    iput-byte v3, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxLSize_:B

    .line 834
    iput-byte v3, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxVSize_:B

    .line 835
    iput-byte v3, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxTSize_:B

    .line 836
    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;)V
    .locals 1
    .param p1, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;

    .prologue
    .line 839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 840
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    .line 841
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_isV_:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_isV_:Ljava/util/Vector;

    .line 842
    iget-byte v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxLSize_:B

    iput-byte v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxLSize_:B

    .line 843
    iget-byte v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxVSize_:B

    iput-byte v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxVSize_:B

    .line 844
    iget-byte v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxTSize_:B

    iput-byte v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;->m_maxTSize_:B

    .line 845
    return-void
.end method
