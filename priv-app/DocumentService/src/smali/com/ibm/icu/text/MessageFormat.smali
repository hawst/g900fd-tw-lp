.class public Lcom/ibm/icu/text/MessageFormat;
.super Lcom/ibm/icu/text/UFormat;
.source "MessageFormat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/MessageFormat$Field;
    }
.end annotation


# static fields
.field private static final CURLY_BRACE_LEFT:C = '{'

.field private static final CURLY_BRACE_RIGHT:C = '}'

.field private static final DATE_MODIFIER_EMPTY:I = 0x0

.field private static final DATE_MODIFIER_FULL:I = 0x4

.field private static final DATE_MODIFIER_LONG:I = 0x3

.field private static final DATE_MODIFIER_MEDIUM:I = 0x2

.field private static final DATE_MODIFIER_SHORT:I = 0x1

.field private static IDContChars:Lcom/ibm/icu/text/UnicodeSet; = null

.field private static IDStartChars:Lcom/ibm/icu/text/UnicodeSet; = null

.field private static final INITIAL_FORMATS:I = 0xa

.field private static final MODIFIER_CURRENCY:I = 0x1

.field private static final MODIFIER_EMPTY:I = 0x0

.field private static final MODIFIER_INTEGER:I = 0x3

.field private static final MODIFIER_PERCENT:I = 0x2

.field private static final SINGLE_QUOTE:C = '\''

.field private static final STATE_INITIAL:I = 0x0

.field private static final STATE_IN_QUOTE:I = 0x2

.field private static final STATE_MSG_ELEMENT:I = 0x3

.field private static final STATE_SINGLE_QUOTE:I = 0x1

.field private static final TYPE_CHOICE:I = 0x4

.field private static final TYPE_DATE:I = 0x2

.field private static final TYPE_DURATION:I = 0x7

.field private static final TYPE_EMPTY:I = 0x0

.field private static final TYPE_NUMBER:I = 0x1

.field private static final TYPE_ORDINAL:I = 0x6

.field private static final TYPE_PLURAL:I = 0x8

.field private static final TYPE_SPELLOUT:I = 0x5

.field private static final TYPE_TIME:I = 0x3

.field static class$com$ibm$icu$text$MessageFormat$Field:Ljava/lang/Class; = null

.field private static final dateModifierList:[Ljava/lang/String;

.field private static final modifierList:[Ljava/lang/String;

.field static final serialVersionUID:J = 0x6308eb804ceb42dbL

.field private static final typeList:[Ljava/lang/String;


# instance fields
.field private argumentNames:[Ljava/lang/String;

.field private argumentNamesAreNumeric:Z

.field private argumentNumbers:[I

.field private formats:[Ljava/text/Format;

.field private locale:Ljava/util/Locale;

.field private maxOffset:I

.field private offsets:[I

.field private pattern:Ljava/lang/String;

.field private ulocale:Lcom/ibm/icu/util/ULocale;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1784
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, ""

    aput-object v1, v0, v3

    const-string/jumbo v1, "number"

    aput-object v1, v0, v4

    const-string/jumbo v1, "date"

    aput-object v1, v0, v5

    const-string/jumbo v1, "time"

    aput-object v1, v0, v6

    const-string/jumbo v1, "choice"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "spellout"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "ordinal"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "plural"

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/MessageFormat;->typeList:[Ljava/lang/String;

    .line 1798
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, ""

    aput-object v1, v0, v3

    const-string/jumbo v1, "currency"

    aput-object v1, v0, v4

    const-string/jumbo v1, "percent"

    aput-object v1, v0, v5

    const-string/jumbo v1, "integer"

    aput-object v1, v0, v6

    sput-object v0, Lcom/ibm/icu/text/MessageFormat;->modifierList:[Ljava/lang/String;

    .line 1807
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, ""

    aput-object v1, v0, v3

    const-string/jumbo v1, "short"

    aput-object v1, v0, v4

    const-string/jumbo v1, "medium"

    aput-object v1, v0, v5

    const-string/jumbo v1, "long"

    aput-object v1, v0, v6

    const-string/jumbo v1, "full"

    aput-object v1, v0, v7

    sput-object v0, Lcom/ibm/icu/text/MessageFormat;->dateModifierList:[Ljava/lang/String;

    .line 2144
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v1, "[:ID_Start:]"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/ibm/icu/text/MessageFormat;->IDStartChars:Lcom/ibm/icu/text/UnicodeSet;

    .line 2145
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v1, "[:ID_Continue:]"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/ibm/icu/text/MessageFormat;->IDContChars:Lcom/ibm/icu/text/UnicodeSet;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    const/16 v1, 0xa

    .line 416
    invoke-direct {p0}, Lcom/ibm/icu/text/UFormat;-><init>()V

    .line 1566
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    .line 1575
    new-array v0, v1, [Ljava/text/Format;

    iput-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    .line 1583
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    .line 1592
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNumbers:[I

    .line 1601
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    .line 1608
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    .line 1617
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    .line 417
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    .line 418
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/MessageFormat;->applyPattern(Ljava/lang/String;)V

    .line 419
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V
    .locals 2
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/16 v1, 0xa

    .line 451
    invoke-direct {p0}, Lcom/ibm/icu/text/UFormat;-><init>()V

    .line 1566
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    .line 1575
    new-array v0, v1, [Ljava/text/Format;

    iput-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    .line 1583
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    .line 1592
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNumbers:[I

    .line 1601
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    .line 1608
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    .line 1617
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    .line 452
    iput-object p2, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    .line 453
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/MessageFormat;->applyPattern(Ljava/lang/String;)V

    .line 454
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Locale;)V
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/util/Locale;

    .prologue
    .line 435
    invoke-static {p2}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 436
    return-void
.end method

.method private static _createAttributedCharacterIterator(Ljava/lang/String;)Ljava/text/AttributedCharacterIterator;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 2241
    new-instance v0, Ljava/text/AttributedString;

    invoke-direct {v0, p0}, Ljava/text/AttributedString;-><init>(Ljava/lang/String;)V

    .line 2242
    .local v0, "as":Ljava/text/AttributedString;
    invoke-virtual {v0}, Ljava/text/AttributedString;->getIterator()Ljava/text/AttributedCharacterIterator;

    move-result-object v1

    return-object v1
.end method

.method private static _createAttributedCharacterIterator(Ljava/lang/String;Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;)Ljava/text/AttributedCharacterIterator;
    .locals 2
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "key"    # Ljava/text/AttributedCharacterIterator$Attribute;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 2297
    new-instance v0, Ljava/text/AttributedString;

    invoke-direct {v0, p0}, Ljava/text/AttributedString;-><init>(Ljava/lang/String;)V

    .line 2298
    .local v0, "as":Ljava/text/AttributedString;
    invoke-virtual {v0, p1, p2}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;)V

    .line 2299
    invoke-virtual {v0}, Ljava/text/AttributedString;->getIterator()Ljava/text/AttributedCharacterIterator;

    move-result-object v1

    return-object v1
.end method

.method private static _createAttributedCharacterIterator(Ljava/text/AttributedCharacterIterator;Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;)Ljava/text/AttributedCharacterIterator;
    .locals 2
    .param p0, "iterator"    # Ljava/text/AttributedCharacterIterator;
    .param p1, "key"    # Ljava/text/AttributedCharacterIterator$Attribute;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 2290
    new-instance v0, Ljava/text/AttributedString;

    invoke-direct {v0, p0}, Ljava/text/AttributedString;-><init>(Ljava/text/AttributedCharacterIterator;)V

    .line 2291
    .local v0, "as":Ljava/text/AttributedString;
    invoke-virtual {v0, p1, p2}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;)V

    .line 2292
    invoke-virtual {v0}, Ljava/text/AttributedString;->getIterator()Ljava/text/AttributedCharacterIterator;

    move-result-object v1

    return-object v1
.end method

.method private static _createAttributedCharacterIterator([Ljava/text/AttributedCharacterIterator;)Ljava/text/AttributedCharacterIterator;
    .locals 15
    .param p0, "iterators"    # [Ljava/text/AttributedCharacterIterator;

    .prologue
    .line 2246
    if-eqz p0, :cond_0

    array-length v12, p0

    if-nez v12, :cond_1

    .line 2247
    :cond_0
    const-string/jumbo v12, ""

    invoke-static {v12}, Lcom/ibm/icu/text/MessageFormat;->_createAttributedCharacterIterator(Ljava/lang/String;)Ljava/text/AttributedCharacterIterator;

    move-result-object v12

    .line 2285
    :goto_0
    return-object v12

    .line 2250
    :cond_1
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    .line 2251
    .local v10, "sb":Ljava/lang/StringBuffer;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    array-length v12, p0

    if-ge v4, v12, :cond_3

    .line 2252
    aget-object v12, p0, v4

    invoke-interface {v12}, Ljava/text/AttributedCharacterIterator;->getBeginIndex()I

    move-result v5

    .line 2253
    .local v5, "index":I
    aget-object v12, p0, v4

    invoke-interface {v12}, Ljava/text/AttributedCharacterIterator;->getEndIndex()I

    move-result v2

    .local v2, "end":I
    move v6, v5

    .line 2254
    .end local v5    # "index":I
    .local v6, "index":I
    :goto_2
    if-ge v6, v2, :cond_2

    .line 2255
    aget-object v12, p0, v4

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "index":I
    .restart local v5    # "index":I
    invoke-interface {v12, v6}, Ljava/text/AttributedCharacterIterator;->setIndex(I)C

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v6, v5

    .line 2256
    .end local v5    # "index":I
    .restart local v6    # "index":I
    goto :goto_2

    .line 2251
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 2258
    .end local v2    # "end":I
    .end local v6    # "index":I
    :cond_3
    new-instance v0, Ljava/text/AttributedString;

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v0, v12}, Ljava/text/AttributedString;-><init>(Ljava/lang/String;)V

    .line 2261
    .local v0, "as":Ljava/text/AttributedString;
    const/4 v9, 0x0

    .line 2262
    .local v9, "offset":I
    const/4 v4, 0x0

    :goto_3
    array-length v12, p0

    if-ge v4, v12, :cond_6

    .line 2263
    aget-object v12, p0, v4

    invoke-interface {v12}, Ljava/text/AttributedCharacterIterator;->first()C

    .line 2264
    aget-object v12, p0, v4

    invoke-interface {v12}, Ljava/text/AttributedCharacterIterator;->getBeginIndex()I

    move-result v11

    .line 2266
    .local v11, "start":I
    :cond_4
    aget-object v12, p0, v4

    invoke-interface {v12}, Ljava/text/AttributedCharacterIterator;->getAttributes()Ljava/util/Map;

    move-result-object v8

    .line 2267
    .local v8, "map":Ljava/util/Map;
    aget-object v12, p0, v4

    invoke-interface {v12}, Ljava/text/AttributedCharacterIterator;->getRunLimit()I

    move-result v12

    sub-int v7, v12, v11

    .line 2268
    .local v7, "len":I
    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v12

    if-lez v12, :cond_5

    .line 2269
    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2270
    .local v1, "eit":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2271
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 2272
    .local v3, "entry":Ljava/util/Map$Entry;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/text/AttributedCharacterIterator$Attribute;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    add-int v14, v9, v7

    invoke-virtual {v0, v12, v13, v9, v14}, Ljava/text/AttributedString;->addAttribute(Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;II)V

    goto :goto_4

    .line 2276
    .end local v1    # "eit":Ljava/util/Iterator;
    .end local v3    # "entry":Ljava/util/Map$Entry;
    :cond_5
    add-int/2addr v9, v7

    .line 2277
    add-int/2addr v11, v7

    .line 2278
    aget-object v12, p0, v4

    invoke-interface {v12, v11}, Ljava/text/AttributedCharacterIterator;->setIndex(I)C

    .line 2279
    aget-object v12, p0, v4

    invoke-interface {v12}, Ljava/text/AttributedCharacterIterator;->current()C

    move-result v12

    const v13, 0xffff

    if-ne v12, v13, :cond_4

    .line 2262
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 2285
    .end local v7    # "len":I
    .end local v8    # "map":Ljava/util/Map;
    .end local v11    # "start":I
    :cond_6
    invoke-virtual {v0}, Ljava/text/AttributedString;->getIterator()Ljava/text/AttributedCharacterIterator;

    move-result-object v12

    goto/16 :goto_0
.end method

.method private append(Ljava/lang/StringBuffer;Ljava/text/CharacterIterator;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/StringBuffer;
    .param p2, "iterator"    # Ljava/text/CharacterIterator;

    .prologue
    const v2, 0xffff

    .line 1773
    invoke-interface {p2}, Ljava/text/CharacterIterator;->first()C

    move-result v1

    if-eq v1, v2, :cond_0

    .line 1776
    invoke-interface {p2}, Ljava/text/CharacterIterator;->first()C

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1777
    :goto_0
    invoke-interface {p2}, Ljava/text/CharacterIterator;->next()C

    move-result v0

    .local v0, "aChar":C
    if-eq v0, v2, :cond_0

    .line 1778
    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1781
    .end local v0    # "aChar":C
    :cond_0
    return-void
.end method

.method private arrayToMap([Ljava/lang/Object;)Ljava/util/Map;
    .locals 4
    .param p1, "array"    # [Ljava/lang/Object;

    .prologue
    .line 2113
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2114
    .local v1, "map":Ljava/util/Map;
    if-eqz p1, :cond_0

    .line 2115
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 2116
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aget-object v3, p1, v0

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2115
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2119
    .end local v0    # "i":I
    :cond_0
    return-object v1
.end method

.method public static autoQuoteApostrophe(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "pattern"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x27

    .line 2165
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    invoke-direct {v1, v6}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 2166
    .local v1, "buf":Ljava/lang/StringBuffer;
    const/4 v5, 0x0

    .line 2167
    .local v5, "state":I
    const/4 v0, 0x0

    .line 2168
    .local v0, "braceCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .local v4, "j":I
    :goto_0
    if-ge v3, v4, :cond_1

    .line 2169
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 2170
    .local v2, "c":C
    packed-switch v5, :pswitch_data_0

    .line 2219
    :cond_0
    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2168
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2172
    :pswitch_0
    sparse-switch v2, :sswitch_data_0

    goto :goto_1

    .line 2174
    :sswitch_0
    const/4 v5, 0x1

    .line 2175
    goto :goto_1

    .line 2177
    :sswitch_1
    const/4 v5, 0x3

    .line 2178
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2183
    :pswitch_1
    sparse-switch v2, :sswitch_data_1

    .line 2192
    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2193
    const/4 v5, 0x0

    .line 2194
    goto :goto_1

    .line 2185
    :sswitch_2
    const/4 v5, 0x0

    .line 2186
    goto :goto_1

    .line 2189
    :sswitch_3
    const/4 v5, 0x2

    .line 2190
    goto :goto_1

    .line 2198
    :pswitch_2
    packed-switch v2, :pswitch_data_1

    goto :goto_1

    .line 2200
    :pswitch_3
    const/4 v5, 0x0

    goto :goto_1

    .line 2205
    :pswitch_4
    packed-switch v2, :pswitch_data_2

    :pswitch_5
    goto :goto_1

    .line 2207
    :pswitch_6
    add-int/lit8 v0, v0, 0x1

    .line 2208
    goto :goto_1

    .line 2210
    :pswitch_7
    add-int/lit8 v0, v0, -0x1

    if-nez v0, :cond_0

    .line 2211
    const/4 v5, 0x0

    goto :goto_1

    .line 2222
    .end local v2    # "c":C
    :cond_1
    const/4 v6, 0x1

    if-eq v5, v6, :cond_2

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    .line 2223
    :cond_2
    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2225
    :cond_3
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v1}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V

    return-object v6

    .line 2170
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
    .end packed-switch

    .line 2172
    :sswitch_data_0
    .sparse-switch
        0x27 -> :sswitch_0
        0x7b -> :sswitch_1
    .end sparse-switch

    .line 2183
    :sswitch_data_1
    .sparse-switch
        0x27 -> :sswitch_2
        0x7b -> :sswitch_3
        0x7d -> :sswitch_3
    .end sparse-switch

    .line 2198
    :pswitch_data_1
    .packed-switch 0x27
        :pswitch_3
    .end packed-switch

    .line 2205
    :pswitch_data_2
    .packed-switch 0x7b
        :pswitch_6
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 1522
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static final copyAndFixQuotes(Ljava/lang/String;IILjava/lang/StringBuffer;)V
    .locals 4
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "target"    # Ljava/lang/StringBuffer;

    .prologue
    .line 2046
    const/4 v1, 0x0

    .line 2047
    .local v1, "gotLB":Z
    move v2, p1

    .local v2, "i":I
    :goto_0
    if-ge v2, p2, :cond_4

    .line 2048
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2049
    .local v0, "ch":C
    const/16 v3, 0x7b

    if-ne v0, v3, :cond_0

    .line 2050
    const-string/jumbo v3, "\'{\'"

    invoke-virtual {p3, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2051
    const/4 v1, 0x1

    .line 2047
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2052
    :cond_0
    const/16 v3, 0x7d

    if-ne v0, v3, :cond_2

    .line 2053
    if-eqz v1, :cond_1

    .line 2054
    invoke-virtual {p3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2055
    const/4 v1, 0x0

    .line 2056
    goto :goto_1

    .line 2057
    :cond_1
    const-string/jumbo v3, "\'}\'"

    invoke-virtual {p3, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 2059
    :cond_2
    const/16 v3, 0x27

    if-ne v0, v3, :cond_3

    .line 2060
    const-string/jumbo v3, "\'\'"

    invoke-virtual {p3, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 2062
    :cond_3
    invoke-virtual {p3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 2065
    .end local v0    # "ch":C
    :cond_4
    return-void
.end method

.method private static final findKeyword(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "list"    # [Ljava/lang/String;

    .prologue
    .line 2036
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    .line 2037
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 2038
    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2041
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 2037
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2041
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static format(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 2
    .param p0, "pattern"    # Ljava/lang/String;
    .param p1, "arguments"    # Ljava/util/Map;

    .prologue
    .line 1085
    new-instance v0, Lcom/ibm/icu/text/MessageFormat;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;)V

    .line 1086
    .local v0, "temp":Lcom/ibm/icu/text/MessageFormat;
    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .param p0, "pattern"    # Ljava/lang/String;
    .param p1, "arguments"    # [Ljava/lang/Object;

    .prologue
    .line 1067
    new-instance v0, Lcom/ibm/icu/text/MessageFormat;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;)V

    .line 1068
    .local v0, "temp":Lcom/ibm/icu/text/MessageFormat;
    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private isAlphaIdentifier(Ljava/lang/String;)Z
    .locals 4
    .param p1, "argument"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 2123
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 2132
    :cond_0
    :goto_0
    return v1

    .line 2126
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 2127
    if-nez v0, :cond_2

    sget-object v2, Lcom/ibm/icu/text/MessageFormat;->IDStartChars:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    if-lez v0, :cond_3

    sget-object v2, Lcom/ibm/icu/text/MessageFormat;->IDContChars:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2126
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2132
    :cond_4
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private makeFormat(II[Ljava/lang/StringBuffer;)V
    .locals 24
    .param p1, "position"    # I
    .param p2, "offsetNumber"    # I
    .param p3, "segments"    # [Ljava/lang/StringBuffer;

    .prologue
    .line 1834
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, p2

    move/from16 v1, v20

    if-lt v0, v1, :cond_0

    .line 1835
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    mul-int/lit8 v12, v20, 0x2

    .line 1836
    .local v12, "newLength":I
    new-array v11, v12, [Ljava/text/Format;

    .line 1837
    .local v11, "newFormats":[Ljava/text/Format;
    new-array v13, v12, [I

    .line 1838
    .local v13, "newOffsets":[I
    new-array v9, v12, [Ljava/lang/String;

    .line 1839
    .local v9, "newArgumentNames":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-static {v0, v1, v11, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1840
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-static {v0, v1, v13, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1841
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-static {v0, v1, v9, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1843
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    .line 1844
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    .line 1845
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    .line 1847
    .end local v9    # "newArgumentNames":[Ljava/lang/String;
    .end local v11    # "newFormats":[Ljava/text/Format;
    .end local v12    # "newLength":I
    .end local v13    # "newOffsets":[I
    :cond_0
    move-object/from16 v0, p0

    iget v14, v0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    .line 1848
    .local v14, "oldMaxOffset":I
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    .line 1849
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aget-object v21, p3, v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuffer;->length()I

    move-result v21

    aput v21, v20, p2

    .line 1850
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    aget-object v21, p3, v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v20, p2

    .line 1855
    const/16 v20, 0x1

    :try_start_0
    aget-object v20, p3, v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 1859
    .local v4, "argumentNumber":I
    :goto_0
    if-nez p2, :cond_1

    .line 1862
    if-ltz v4, :cond_4

    const/16 v20, 0x1

    :goto_1
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    .line 1865
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    move/from16 v20, v0

    if-eqz v20, :cond_2

    if-ltz v4, :cond_3

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    move/from16 v20, v0

    if-nez v20, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    move-object/from16 v20, v0

    aget-object v20, v20, p2

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/MessageFormat;->isAlphaIdentifier(Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_5

    .line 1868
    :cond_3
    new-instance v20, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v21, "All argument identifiers have to be either non-negative numbers or strings following the pattern ([:ID_Start:] [:ID_Continue:]*).\nFor more details on these unicode sets, visit http://demo.icu-project.org/icu-bin/ubrowse"

    invoke-direct/range {v20 .. v21}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 1856
    .end local v4    # "argumentNumber":I
    :catch_0
    move-exception v6

    .line 1857
    .local v6, "e":Ljava/lang/NumberFormatException;
    const/4 v4, -0x1

    .restart local v4    # "argumentNumber":I
    goto :goto_0

    .line 1862
    .end local v6    # "e":Ljava/lang/NumberFormatException;
    :cond_4
    const/16 v20, 0x0

    goto :goto_1

    .line 1877
    :cond_5
    const/4 v10, 0x0

    .line 1878
    .local v10, "newFormat":Ljava/text/Format;
    const/16 v20, 0x2

    aget-object v20, p3, v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    sget-object v21, Lcom/ibm/icu/text/MessageFormat;->typeList:[Ljava/lang/String;

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/text/MessageFormat;->findKeyword(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v20

    packed-switch v20, :pswitch_data_0

    .line 2026
    move-object/from16 v0, p0

    iput v14, v0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    .line 2027
    new-instance v20, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v21, "unknown format type at "

    invoke-direct/range {v20 .. v21}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 1882
    :pswitch_0
    const/16 v20, 0x3

    aget-object v20, p3, v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    sget-object v21, Lcom/ibm/icu/text/MessageFormat;->modifierList:[Ljava/lang/String;

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/text/MessageFormat;->findKeyword(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v20

    packed-switch v20, :pswitch_data_1

    .line 1896
    new-instance v10, Lcom/ibm/icu/text/DecimalFormat;

    .end local v10    # "newFormat":Ljava/text/Format;
    const/16 v20, 0x3

    aget-object v20, p3, v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    new-instance v21, Lcom/ibm/icu/text/DecimalFormatSymbols;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v22, v0

    invoke-direct/range {v21 .. v22}, Lcom/ibm/icu/text/DecimalFormatSymbols;-><init>(Lcom/ibm/icu/util/ULocale;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v10, v0, v1}, Lcom/ibm/icu/text/DecimalFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/DecimalFormatSymbols;)V

    .line 2029
    .restart local v10    # "newFormat":Ljava/text/Format;
    :goto_2
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    move-object/from16 v20, v0

    aput-object v10, v20, p2

    .line 2030
    const/16 v20, 0x1

    aget-object v20, p3, v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 2031
    const/16 v20, 0x2

    aget-object v20, p3, v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 2032
    const/16 v20, 0x3

    aget-object v20, p3, v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 2033
    return-void

    .line 1884
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v10

    .line 1885
    goto :goto_2

    .line 1887
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/ibm/icu/text/NumberFormat;->getCurrencyInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v10

    .line 1888
    goto :goto_2

    .line 1890
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/ibm/icu/text/NumberFormat;->getPercentInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v10

    .line 1891
    goto :goto_2

    .line 1893
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/ibm/icu/text/NumberFormat;->getIntegerInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v10

    .line 1894
    goto :goto_2

    .line 1901
    :pswitch_6
    const/16 v20, 0x3

    aget-object v20, p3, v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    sget-object v21, Lcom/ibm/icu/text/MessageFormat;->dateModifierList:[Ljava/lang/String;

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/text/MessageFormat;->findKeyword(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v20

    packed-switch v20, :pswitch_data_2

    .line 1918
    new-instance v10, Lcom/ibm/icu/text/SimpleDateFormat;

    .end local v10    # "newFormat":Ljava/text/Format;
    const/16 v20, 0x3

    aget-object v20, p3, v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v10, v0, v1}, Lcom/ibm/icu/text/SimpleDateFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 1919
    .restart local v10    # "newFormat":Ljava/text/Format;
    goto :goto_2

    .line 1903
    :pswitch_7
    const/16 v20, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/text/DateFormat;->getDateInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v10

    .line 1904
    goto/16 :goto_2

    .line 1906
    :pswitch_8
    const/16 v20, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/text/DateFormat;->getDateInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v10

    .line 1907
    goto/16 :goto_2

    .line 1909
    :pswitch_9
    const/16 v20, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/text/DateFormat;->getDateInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v10

    .line 1910
    goto/16 :goto_2

    .line 1912
    :pswitch_a
    const/16 v20, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/text/DateFormat;->getDateInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v10

    .line 1913
    goto/16 :goto_2

    .line 1915
    :pswitch_b
    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/text/DateFormat;->getDateInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v10

    .line 1916
    goto/16 :goto_2

    .line 1923
    :pswitch_c
    const/16 v20, 0x3

    aget-object v20, p3, v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    sget-object v21, Lcom/ibm/icu/text/MessageFormat;->dateModifierList:[Ljava/lang/String;

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/text/MessageFormat;->findKeyword(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v20

    packed-switch v20, :pswitch_data_3

    .line 1940
    new-instance v10, Lcom/ibm/icu/text/SimpleDateFormat;

    .end local v10    # "newFormat":Ljava/text/Format;
    const/16 v20, 0x3

    aget-object v20, p3, v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v10, v0, v1}, Lcom/ibm/icu/text/SimpleDateFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 1941
    .restart local v10    # "newFormat":Ljava/text/Format;
    goto/16 :goto_2

    .line 1925
    :pswitch_d
    const/16 v20, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/text/DateFormat;->getTimeInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v10

    .line 1926
    goto/16 :goto_2

    .line 1928
    :pswitch_e
    const/16 v20, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/text/DateFormat;->getTimeInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v10

    .line 1929
    goto/16 :goto_2

    .line 1931
    :pswitch_f
    const/16 v20, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/text/DateFormat;->getTimeInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v10

    .line 1932
    goto/16 :goto_2

    .line 1934
    :pswitch_10
    const/16 v20, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/text/DateFormat;->getTimeInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v10

    .line 1935
    goto/16 :goto_2

    .line 1937
    :pswitch_11
    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/ibm/icu/text/DateFormat;->getTimeInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v10

    .line 1938
    goto/16 :goto_2

    .line 1946
    :pswitch_12
    :try_start_1
    new-instance v10, Ljava/text/ChoiceFormat;

    .end local v10    # "newFormat":Ljava/text/Format;
    const/16 v20, 0x3

    aget-object v20, p3, v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v10, v0}, Ljava/text/ChoiceFormat;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .restart local v10    # "newFormat":Ljava/text/Format;
    goto/16 :goto_2

    .line 1947
    .end local v10    # "newFormat":Ljava/text/Format;
    :catch_1
    move-exception v6

    .line 1948
    .local v6, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iput v14, v0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    .line 1949
    new-instance v20, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v21, "Choice Pattern incorrect"

    invoke-direct/range {v20 .. v21}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 1954
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v10    # "newFormat":Ljava/text/Format;
    :pswitch_13
    new-instance v17, Lcom/ibm/icu/text/RuleBasedNumberFormat;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedNumberFormat;-><init>(Lcom/ibm/icu/util/ULocale;I)V

    .line 1955
    .local v17, "rbnf":Lcom/ibm/icu/text/RuleBasedNumberFormat;
    const/16 v20, 0x3

    aget-object v20, p3, v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    .line 1956
    .local v18, "ruleset":Ljava/lang/String;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v20

    if-eqz v20, :cond_6

    .line 1958
    :try_start_2
    invoke-virtual/range {v17 .. v18}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->setDefaultRuleSet(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1964
    :cond_6
    :goto_3
    move-object/from16 v10, v17

    .line 1966
    goto/16 :goto_2

    .line 1969
    .end local v17    # "rbnf":Lcom/ibm/icu/text/RuleBasedNumberFormat;
    .end local v18    # "ruleset":Ljava/lang/String;
    :pswitch_14
    new-instance v17, Lcom/ibm/icu/text/RuleBasedNumberFormat;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v20, v0

    const/16 v21, 0x2

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedNumberFormat;-><init>(Lcom/ibm/icu/util/ULocale;I)V

    .line 1970
    .restart local v17    # "rbnf":Lcom/ibm/icu/text/RuleBasedNumberFormat;
    const/16 v20, 0x3

    aget-object v20, p3, v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    .line 1971
    .restart local v18    # "ruleset":Ljava/lang/String;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v20

    if-eqz v20, :cond_7

    .line 1973
    :try_start_3
    invoke-virtual/range {v17 .. v18}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->setDefaultRuleSet(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 1979
    :cond_7
    :goto_4
    move-object/from16 v10, v17

    .line 1981
    goto/16 :goto_2

    .line 1984
    .end local v17    # "rbnf":Lcom/ibm/icu/text/RuleBasedNumberFormat;
    .end local v18    # "ruleset":Ljava/lang/String;
    :pswitch_15
    new-instance v17, Lcom/ibm/icu/text/RuleBasedNumberFormat;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v20, v0

    const/16 v21, 0x3

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedNumberFormat;-><init>(Lcom/ibm/icu/util/ULocale;I)V

    .line 1985
    .restart local v17    # "rbnf":Lcom/ibm/icu/text/RuleBasedNumberFormat;
    const/16 v20, 0x3

    aget-object v20, p3, v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    .line 1986
    .restart local v18    # "ruleset":Ljava/lang/String;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v20

    if-eqz v20, :cond_8

    .line 1988
    :try_start_4
    invoke-virtual/range {v17 .. v18}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->setDefaultRuleSet(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 1994
    :cond_8
    :goto_5
    move-object/from16 v10, v17

    .line 1996
    goto/16 :goto_2

    .line 2002
    .end local v17    # "rbnf":Lcom/ibm/icu/text/RuleBasedNumberFormat;
    .end local v18    # "ruleset":Ljava/lang/String;
    :pswitch_16
    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    .line 2003
    .local v19, "unquotedPattern":Ljava/lang/StringBuffer;
    const/16 v20, 0x3

    aget-object v20, p3, v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v16

    .line 2004
    .local v16, "quotedPattern":Ljava/lang/String;
    const/4 v8, 0x0

    .line 2005
    .local v8, "inQuote":Z
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_6
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v20

    move/from16 v0, v20

    if-ge v7, v0, :cond_c

    .line 2006
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 2007
    .local v5, "ch":C
    const/16 v20, 0x27

    move/from16 v0, v20

    if-ne v5, v0, :cond_b

    .line 2008
    add-int/lit8 v20, v7, 0x1

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_9

    add-int/lit8 v20, v7, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v20

    const/16 v21, 0x27

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_9

    .line 2010
    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2011
    add-int/lit8 v7, v7, 0x1

    .line 2005
    :goto_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 2013
    :cond_9
    if-nez v8, :cond_a

    const/4 v8, 0x1

    .line 2015
    :goto_8
    goto :goto_7

    .line 2013
    :cond_a
    const/4 v8, 0x0

    goto :goto_8

    .line 2016
    :cond_b
    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_7

    .line 2020
    .end local v5    # "ch":C
    :cond_c
    new-instance v15, Lcom/ibm/icu/text/PluralFormat;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v15, v0, v1}, Lcom/ibm/icu/text/PluralFormat;-><init>(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)V

    .line 2022
    .local v15, "pls":Lcom/ibm/icu/text/PluralFormat;
    move-object v10, v15

    .line 2024
    goto/16 :goto_2

    .line 1960
    .end local v7    # "i":I
    .end local v8    # "inQuote":Z
    .end local v15    # "pls":Lcom/ibm/icu/text/PluralFormat;
    .end local v16    # "quotedPattern":Ljava/lang/String;
    .end local v19    # "unquotedPattern":Ljava/lang/StringBuffer;
    .restart local v17    # "rbnf":Lcom/ibm/icu/text/RuleBasedNumberFormat;
    .restart local v18    # "ruleset":Ljava/lang/String;
    :catch_2
    move-exception v20

    goto/16 :goto_3

    .line 1975
    :catch_3
    move-exception v20

    goto/16 :goto_4

    .line 1990
    :catch_4
    move-exception v20

    goto :goto_5

    .line 1878
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_c
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch

    .line 1882
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 1901
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 1923
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 5
    .param p1, "in"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2073
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 2074
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    if-nez v3, :cond_0

    .line 2075
    iput-boolean v1, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    .line 2076
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNumbers:[I

    array-length v3, v3

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    .line 2077
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNumbers:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 2078
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNumbers:[I

    aget v4, v4, v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 2077
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2081
    .end local v0    # "i":I
    :cond_0
    iget v3, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    const/4 v4, -0x1

    if-lt v3, v4, :cond_3

    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    array-length v3, v3

    iget v4, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-le v3, v4, :cond_3

    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    array-length v3, v3

    iget v4, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-le v3, v4, :cond_3

    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    array-length v3, v3

    iget v4, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-le v3, v4, :cond_3

    .line 2085
    .local v1, "isValid":Z
    :goto_1
    if-eqz v1, :cond_2

    .line 2086
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v2, v3, 0x1

    .line 2087
    .local v2, "lastOffset":I
    iget v0, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    .restart local v0    # "i":I
    :goto_2
    if-ltz v0, :cond_2

    .line 2088
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    aget v3, v3, v0

    if-ltz v3, :cond_1

    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    aget v3, v3, v0

    if-le v3, v2, :cond_4

    .line 2089
    :cond_1
    const/4 v1, 0x0

    .line 2096
    .end local v0    # "i":I
    .end local v2    # "lastOffset":I
    :cond_2
    if-nez v1, :cond_5

    .line 2097
    new-instance v3, Ljava/io/InvalidObjectException;

    const-string/jumbo v4, "Could not reconstruct MessageFormat from corrupt stream."

    invoke-direct {v3, v4}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 2081
    .end local v1    # "isValid":Z
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 2092
    .restart local v0    # "i":I
    .restart local v1    # "isValid":Z
    .restart local v2    # "lastOffset":I
    :cond_4
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    aget v2, v3, v0

    .line 2087
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 2099
    .end local v0    # "i":I
    .end local v2    # "lastOffset":I
    :cond_5
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    if-nez v3, :cond_6

    .line 2100
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->locale:Ljava/util/Locale;

    invoke-static {v3}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v3

    iput-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    .line 2102
    :cond_6
    return-void
.end method

.method private subformat(Ljava/util/Map;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;
    .locals 11
    .param p1, "arguments"    # Ljava/util/Map;
    .param p2, "result"    # Ljava/lang/StringBuffer;
    .param p3, "fp"    # Ljava/text/FieldPosition;
    .param p4, "characterIterators"    # Ljava/util/List;

    .prologue
    const/4 v10, 0x3

    .line 1647
    const/4 v4, 0x0

    .line 1650
    .local v4, "lastOffset":I
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    .line 1652
    .local v3, "last":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v8, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-gt v2, v8, :cond_12

    .line 1653
    iget-object v8, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    iget-object v9, p0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    aget v9, v9, v2

    invoke-virtual {v8, v4, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1654
    iget-object v8, p0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    aget v4, v8, v2

    .line 1655
    iget-object v8, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    aget-object v1, v8, v2

    .line 1656
    .local v1, "argumentName":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1657
    :cond_0
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v9, "{"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string/jumbo v9, "}"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1652
    .end local v1    # "argumentName":Ljava/lang/String;
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1665
    .restart local v1    # "argumentName":Ljava/lang/String;
    :cond_2
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 1666
    .local v5, "obj":Ljava/lang/Object;
    const/4 v0, 0x0

    .line 1667
    .local v0, "arg":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1668
    .local v6, "subFormatter":Ljava/text/Format;
    if-nez v5, :cond_8

    .line 1669
    const-string/jumbo v0, "null"

    .line 1708
    .end local v5    # "obj":Ljava/lang/Object;
    :cond_3
    :goto_2
    if-eqz p4, :cond_f

    .line 1711
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    if-eq v3, v8, :cond_4

    .line 1712
    invoke-virtual {p2, v3}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/ibm/icu/text/MessageFormat;->_createAttributedCharacterIterator(Ljava/lang/String;)Ljava/text/AttributedCharacterIterator;

    move-result-object v8

    invoke-interface {p4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1715
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    .line 1717
    :cond_4
    if-eqz v6, :cond_6

    .line 1718
    invoke-virtual {v6, v5}, Ljava/text/Format;->formatToCharacterIterator(Ljava/lang/Object;)Ljava/text/AttributedCharacterIterator;

    move-result-object v7

    .line 1721
    .local v7, "subIterator":Ljava/text/AttributedCharacterIterator;
    invoke-direct {p0, p2, v7}, Lcom/ibm/icu/text/MessageFormat;->append(Ljava/lang/StringBuffer;Ljava/text/CharacterIterator;)V

    .line 1722
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    if-eq v3, v8, :cond_5

    .line 1723
    sget-object v9, Lcom/ibm/icu/text/MessageFormat$Field;->ARGUMENT:Lcom/ibm/icu/text/MessageFormat$Field;

    iget-boolean v8, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    if-eqz v8, :cond_e

    new-instance v8, Ljava/lang/Integer;

    invoke-direct {v8, v1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    :goto_3
    invoke-static {v7, v9, v8}, Lcom/ibm/icu/text/MessageFormat;->_createAttributedCharacterIterator(Ljava/text/AttributedCharacterIterator;Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;)Ljava/text/AttributedCharacterIterator;

    move-result-object v8

    invoke-interface {p4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1727
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    .line 1729
    :cond_5
    const/4 v0, 0x0

    .line 1731
    .end local v7    # "subIterator":Ljava/text/AttributedCharacterIterator;
    :cond_6
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_1

    .line 1732
    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1733
    sget-object v9, Lcom/ibm/icu/text/MessageFormat$Field;->ARGUMENT:Lcom/ibm/icu/text/MessageFormat$Field;

    iget-boolean v8, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    if-eqz v8, :cond_7

    new-instance v8, Ljava/lang/Integer;

    invoke-direct {v8, v1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    move-object v1, v8

    .end local v1    # "argumentName":Ljava/lang/String;
    :cond_7
    invoke-static {v0, v9, v1}, Lcom/ibm/icu/text/MessageFormat;->_createAttributedCharacterIterator(Ljava/lang/String;Ljava/text/AttributedCharacterIterator$Attribute;Ljava/lang/Object;)Ljava/text/AttributedCharacterIterator;

    move-result-object v8

    invoke-interface {p4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1737
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    .line 1738
    goto :goto_1

    .line 1670
    .restart local v1    # "argumentName":Ljava/lang/String;
    .restart local v5    # "obj":Ljava/lang/Object;
    :cond_8
    iget-object v8, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v8, v8, v2

    if-eqz v8, :cond_a

    .line 1671
    iget-object v8, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v6, v8, v2

    .line 1672
    instance-of v8, v6, Ljava/text/ChoiceFormat;

    if-nez v8, :cond_9

    instance-of v8, v6, Lcom/ibm/icu/text/PluralFormat;

    if-eqz v8, :cond_3

    .line 1674
    :cond_9
    iget-object v8, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v8, v8, v2

    invoke-virtual {v8, v5}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1677
    const/16 v8, 0x7b

    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    if-ltz v8, :cond_3

    .line 1678
    new-instance v6, Lcom/ibm/icu/text/MessageFormat;

    .end local v6    # "subFormatter":Ljava/text/Format;
    iget-object v8, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-direct {v6, v0, v8}, Lcom/ibm/icu/text/MessageFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 1679
    .restart local v6    # "subFormatter":Ljava/text/Format;
    move-object v5, p1

    .line 1680
    .local v5, "obj":Ljava/util/Map;
    const/4 v0, 0x0

    .line 1681
    goto/16 :goto_2

    .line 1683
    .local v5, "obj":Ljava/lang/Object;
    :cond_a
    instance-of v8, v5, Ljava/lang/Number;

    if-eqz v8, :cond_b

    .line 1685
    iget-object v8, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v8}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v6

    .line 1686
    goto/16 :goto_2

    :cond_b
    instance-of v8, v5, Ljava/util/Date;

    if-eqz v8, :cond_c

    .line 1688
    iget-object v8, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v10, v10, v8}, Lcom/ibm/icu/text/DateFormat;->getDateTimeInstance(IILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v6

    .line 1690
    goto/16 :goto_2

    :cond_c
    instance-of v8, v5, Ljava/lang/String;

    if-eqz v8, :cond_d

    move-object v0, v5

    .line 1691
    check-cast v0, Ljava/lang/String;

    .line 1693
    goto/16 :goto_2

    .line 1694
    :cond_d
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1695
    if-nez v0, :cond_3

    const-string/jumbo v0, "null"

    goto/16 :goto_2

    .end local v5    # "obj":Ljava/lang/Object;
    .restart local v7    # "subIterator":Ljava/text/AttributedCharacterIterator;
    :cond_e
    move-object v8, v1

    .line 1723
    goto/16 :goto_3

    .line 1740
    .end local v7    # "subIterator":Ljava/text/AttributedCharacterIterator;
    :cond_f
    if-eqz v6, :cond_10

    .line 1741
    invoke-virtual {v6, v5}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1743
    :cond_10
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    .line 1744
    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1745
    if-nez v2, :cond_11

    if-eqz p3, :cond_11

    sget-object v8, Lcom/ibm/icu/text/MessageFormat$Field;->ARGUMENT:Lcom/ibm/icu/text/MessageFormat$Field;

    invoke-virtual {p3}, Ljava/text/FieldPosition;->getFieldAttribute()Ljava/text/Format$Field;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/ibm/icu/text/MessageFormat$Field;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 1747
    invoke-virtual {p3, v3}, Ljava/text/FieldPosition;->setBeginIndex(I)V

    .line 1748
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    invoke-virtual {p3, v8}, Ljava/text/FieldPosition;->setEndIndex(I)V

    .line 1750
    :cond_11
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    goto/16 :goto_1

    .line 1755
    .end local v0    # "arg":Ljava/lang/String;
    .end local v1    # "argumentName":Ljava/lang/String;
    .end local v6    # "subFormatter":Ljava/text/Format;
    :cond_12
    iget-object v8, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    iget-object v9, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v8, v4, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1758
    if-eqz p4, :cond_13

    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    if-eq v3, v8, :cond_13

    .line 1759
    invoke-virtual {p2, v3}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/ibm/icu/text/MessageFormat;->_createAttributedCharacterIterator(Ljava/lang/String;)Ljava/text/AttributedCharacterIterator;

    move-result-object v8

    invoke-interface {p4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1763
    :cond_13
    return-object p2
.end method

.method private subformat([Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;
    .locals 1
    .param p1, "arguments"    # [Ljava/lang/Object;
    .param p2, "result"    # Ljava/lang/StringBuffer;
    .param p3, "fp"    # Ljava/text/FieldPosition;
    .param p4, "characterIterators"    # Ljava/util/List;

    .prologue
    .line 1633
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/MessageFormat;->arrayToMap([Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/ibm/icu/text/MessageFormat;->subformat(Ljava/util/Map;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public applyPattern(Ljava/lang/String;)V
    .locals 12
    .param p1, "pttrn"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v11, -0x1

    const/16 v10, 0x27

    .line 524
    const/4 v8, 0x4

    new-array v6, v8, [Ljava/lang/StringBuffer;

    .line 525
    .local v6, "segments":[Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v8, v6

    if-ge v3, v8, :cond_0

    .line 526
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    aput-object v8, v6, v3

    .line 525
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 528
    :cond_0
    const/4 v5, 0x0

    .line 529
    .local v5, "part":I
    const/4 v2, 0x0

    .line 530
    .local v2, "formatNumber":I
    const/4 v4, 0x0

    .line 531
    .local v4, "inQuote":Z
    const/4 v0, 0x0

    .line 532
    .local v0, "braceStack":I
    iput v11, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    .line 533
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v3, v8, :cond_a

    .line 534
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 535
    .local v1, "ch":C
    if-nez v5, :cond_6

    .line 536
    if-ne v1, v10, :cond_4

    .line 537
    add-int/lit8 v8, v3, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v8, v9, :cond_2

    add-int/lit8 v8, v3, 0x1

    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    if-ne v8, v10, :cond_2

    .line 539
    aget-object v8, v6, v5

    invoke-virtual {v8, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 540
    add-int/lit8 v3, v3, 0x1

    .line 533
    :cond_1
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 542
    :cond_2
    if-nez v4, :cond_3

    const/4 v4, 0x1

    .line 544
    :goto_3
    goto :goto_2

    :cond_3
    move v4, v7

    .line 542
    goto :goto_3

    .line 544
    :cond_4
    const/16 v8, 0x7b

    if-ne v1, v8, :cond_5

    if-nez v4, :cond_5

    .line 545
    const/4 v5, 0x1

    .line 546
    goto :goto_2

    .line 547
    :cond_5
    aget-object v8, v6, v5

    invoke-virtual {v8, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 549
    :cond_6
    if-eqz v4, :cond_7

    .line 550
    aget-object v8, v6, v5

    invoke-virtual {v8, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 551
    if-ne v1, v10, :cond_1

    .line 552
    const/4 v4, 0x0

    .line 553
    goto :goto_2

    .line 555
    :cond_7
    sparse-switch v1, :sswitch_data_0

    .line 580
    :goto_4
    aget-object v8, v6, v5

    invoke-virtual {v8, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 557
    :sswitch_0
    const/4 v8, 0x3

    if-ge v5, v8, :cond_8

    .line 558
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 560
    :cond_8
    aget-object v8, v6, v5

    invoke-virtual {v8, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 563
    :sswitch_1
    add-int/lit8 v0, v0, 0x1

    .line 564
    aget-object v8, v6, v5

    invoke-virtual {v8, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 567
    :sswitch_2
    if-nez v0, :cond_9

    .line 568
    const/4 v5, 0x0

    .line 569
    invoke-direct {p0, v3, v2, v6}, Lcom/ibm/icu/text/MessageFormat;->makeFormat(II[Ljava/lang/StringBuffer;)V

    .line 570
    add-int/lit8 v2, v2, 0x1

    .line 571
    goto :goto_2

    .line 572
    :cond_9
    add-int/lit8 v0, v0, -0x1

    .line 573
    aget-object v8, v6, v5

    invoke-virtual {v8, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 577
    :sswitch_3
    const/4 v4, 0x1

    goto :goto_4

    .line 585
    .end local v1    # "ch":C
    :cond_a
    if-nez v0, :cond_b

    if-eqz v5, :cond_b

    .line 586
    iput v11, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    .line 587
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v8, "Unmatched braces in the pattern."

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 589
    :cond_b
    aget-object v7, v6, v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    .line 590
    return-void

    .line 555
    nop

    :sswitch_data_0
    .sparse-switch
        0x27 -> :sswitch_3
        0x2c -> :sswitch_0
        0x7b -> :sswitch_1
        0x7d -> :sswitch_2
    .end sparse-switch
.end method

.method public clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1446
    invoke-super {p0}, Lcom/ibm/icu/text/UFormat;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/MessageFormat;

    .line 1449
    .local v1, "other":Lcom/ibm/icu/text/MessageFormat;
    iget-object v2, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    invoke-virtual {v2}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/text/Format;

    check-cast v2, [Ljava/text/Format;

    iput-object v2, v1, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    .line 1450
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1451
    iget-object v2, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 1452
    iget-object v3, v1, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    iget-object v2, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/text/Format;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/text/Format;

    aput-object v2, v3, v0

    .line 1450
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1455
    :cond_1
    iget-object v2, p0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    invoke-virtual {v2}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    check-cast v2, [I

    iput-object v2, v1, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    .line 1456
    iget-object v2, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v1, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    .line 1457
    iget-boolean v2, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    iput-boolean v2, v1, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    .line 1459
    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1467
    if-ne p0, p1, :cond_1

    .line 1472
    :cond_0
    :goto_0
    return v1

    .line 1469
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 1470
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 1471
    check-cast v0, Lcom/ibm/icu/text/MessageFormat;

    .line 1472
    .local v0, "other":Lcom/ibm/icu/text/MessageFormat;
    iget v3, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    iget v4, v0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    iget-object v4, v0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->objectEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    iget-object v4, v0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([ILjava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    iget-object v4, v0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    iget-boolean v4, v0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    if-eq v3, v4, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public final format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 2
    .param p1, "arguments"    # Ljava/lang/Object;
    .param p2, "result"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    const/4 v1, 0x0

    .line 1126
    if-eqz p1, :cond_0

    instance-of v0, p1, Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 1127
    :cond_0
    check-cast p1, Ljava/util/Map;

    .end local p1    # "arguments":Ljava/lang/Object;
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/ibm/icu/text/MessageFormat;->subformat(Ljava/util/Map;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 1134
    :goto_0
    return-object v0

    .line 1129
    .restart local p1    # "arguments":Ljava/lang/Object;
    :cond_1
    iget-boolean v0, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    if-nez v0, :cond_2

    .line 1130
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "This method is not available in MessageFormat objects that use alphanumeric argument names."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1134
    :cond_2
    check-cast p1, [Ljava/lang/Object;

    .end local p1    # "arguments":Ljava/lang/Object;
    check-cast p1, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2, p3, v1}, Lcom/ibm/icu/text/MessageFormat;->subformat([Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;

    move-result-object v0

    goto :goto_0
.end method

.method public final format(Ljava/util/Map;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 1
    .param p1, "arguments"    # Ljava/util/Map;
    .param p2, "result"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 1048
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/ibm/icu/text/MessageFormat;->subformat(Ljava/util/Map;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public final format([Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 2
    .param p1, "arguments"    # [Ljava/lang/Object;
    .param p2, "result"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 1009
    iget-boolean v0, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    if-nez v0, :cond_0

    .line 1010
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "This method is not available in MessageFormat objects that use alphanumeric argument names."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1014
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/ibm/icu/text/MessageFormat;->subformat([Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public formatToCharacterIterator(Ljava/lang/Object;)Ljava/text/AttributedCharacterIterator;
    .locals 4
    .param p1, "arguments"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 1176
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1177
    .local v1, "result":Ljava/lang/StringBuffer;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1179
    .local v0, "iterators":Ljava/util/ArrayList;
    if-nez p1, :cond_0

    .line 1180
    new-instance v2, Ljava/lang/NullPointerException;

    const-string/jumbo v3, "formatToCharacterIterator must be passed non-null object"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1183
    :cond_0
    instance-of v2, p1, Ljava/util/Map;

    if-eqz v2, :cond_1

    .line 1184
    check-cast p1, Ljava/util/Map;

    .end local p1    # "arguments":Ljava/lang/Object;
    invoke-direct {p0, p1, v1, v3, v0}, Lcom/ibm/icu/text/MessageFormat;->subformat(Ljava/util/Map;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;

    .line 1188
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 1189
    const-string/jumbo v2, ""

    invoke-static {v2}, Lcom/ibm/icu/text/MessageFormat;->_createAttributedCharacterIterator(Ljava/lang/String;)Ljava/text/AttributedCharacterIterator;

    move-result-object v2

    .line 1191
    :goto_1
    return-object v2

    .line 1186
    .restart local p1    # "arguments":Ljava/lang/Object;
    :cond_1
    check-cast p1, [Ljava/lang/Object;

    .end local p1    # "arguments":Ljava/lang/Object;
    check-cast p1, [Ljava/lang/Object;

    invoke-direct {p0, p1, v1, v3, v0}, Lcom/ibm/icu/text/MessageFormat;->subformat([Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;Ljava/util/List;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1191
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/text/AttributedCharacterIterator;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/text/AttributedCharacterIterator;

    check-cast v2, [Ljava/text/AttributedCharacterIterator;

    invoke-static {v2}, Lcom/ibm/icu/text/MessageFormat;->_createAttributedCharacterIterator([Ljava/text/AttributedCharacterIterator;)Ljava/text/AttributedCharacterIterator;

    move-result-object v2

    goto :goto_1
.end method

.method public getFormats()[Ljava/text/Format;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 929
    iget v1, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [Ljava/text/Format;

    .line 930
    .local v0, "resultArray":[Ljava/text/Format;
    iget-object v1, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    iget v2, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 931
    return-object v0
.end method

.method public getFormatsByArgumentIndex()[Ljava/text/Format;
    .locals 6

    .prologue
    .line 887
    iget-boolean v4, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    if-nez v4, :cond_0

    .line 888
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "This method is not available in MessageFormat objects that use alphanumeric argument names."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 892
    :cond_0
    const/4 v2, -0x1

    .line 893
    .local v2, "maximumArgumentNumber":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v4, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-gt v1, v4, :cond_2

    .line 894
    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 895
    .local v0, "argumentNumber":I
    if-le v0, v2, :cond_1

    .line 896
    move v2, v0

    .line 893
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 899
    .end local v0    # "argumentNumber":I
    :cond_2
    add-int/lit8 v4, v2, 0x1

    new-array v3, v4, [Ljava/text/Format;

    .line 900
    .local v3, "resultArray":[Ljava/text/Format;
    const/4 v1, 0x0

    :goto_1
    iget v4, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-gt v1, v4, :cond_3

    .line 901
    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iget-object v5, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v5, v5, v1

    aput-object v5, v3, v4

    .line 900
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 903
    :cond_3
    return-object v3
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-virtual {v0}, Lcom/ibm/icu/util/ULocale;->toLocale()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public getULocale()Lcom/ibm/icu/util/ULocale;
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1486
    iget-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public parse(Ljava/lang/String;)[Ljava/lang/Object;
    .locals 5
    .param p1, "source"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 1370
    new-instance v0, Ljava/text/ParsePosition;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/text/ParsePosition;-><init>(I)V

    .line 1371
    .local v0, "pos":Ljava/text/ParsePosition;
    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/text/MessageFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)[Ljava/lang/Object;

    move-result-object v1

    .line 1372
    .local v1, "result":[Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/text/ParsePosition;->getIndex()I

    move-result v2

    if-nez v2, :cond_0

    .line 1373
    new-instance v2, Ljava/text/ParseException;

    const-string/jumbo v3, "MessageFormat parse error!"

    invoke-virtual {v0}, Ljava/text/ParsePosition;->getErrorIndex()I

    move-result v4

    invoke-direct {v2, v3, v4}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 1376
    :cond_0
    return-object v1
.end method

.method public parse(Ljava/lang/String;Ljava/text/ParsePosition;)[Ljava/lang/Object;
    .locals 9
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;

    .prologue
    .line 1232
    iget-boolean v7, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    if-nez v7, :cond_0

    .line 1233
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v8, "This method is not available in MessageFormat objects that use named argument."

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1237
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/MessageFormat;->parseToMap(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Map;

    move-result-object v5

    .line 1238
    .local v5, "objectMap":Ljava/util/Map;
    const/4 v4, -0x1

    .line 1239
    .local v4, "maximumArgumentNumber":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v7, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-gt v1, v7, :cond_2

    .line 1240
    iget-object v7, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    aget-object v7, v7, v1

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1241
    .local v0, "argumentNumber":I
    if-le v0, v4, :cond_1

    .line 1242
    move v4, v0

    .line 1239
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1246
    .end local v0    # "argumentNumber":I
    :cond_2
    if-nez v5, :cond_4

    .line 1247
    const/4 v6, 0x0

    .line 1257
    :cond_3
    return-object v6

    .line 1250
    :cond_4
    add-int/lit8 v7, v4, 0x1

    new-array v6, v7, [Ljava/lang/Object;

    .line 1251
    .local v6, "resultArray":[Ljava/lang/Object;
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1252
    .local v3, "keyIter":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1253
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1254
    .local v2, "key":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v6, v7

    goto :goto_1
.end method

.method public parseObject(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Object;
    .locals 1
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;

    .prologue
    .line 1432
    iget-boolean v0, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    if-eqz v0, :cond_0

    .line 1433
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/MessageFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)[Ljava/lang/Object;

    move-result-object v0

    .line 1435
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/MessageFormat;->parseToMap(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public parseToMap(Ljava/lang/String;)Ljava/util/Map;
    .locals 5
    .param p1, "source"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 1395
    new-instance v0, Ljava/text/ParsePosition;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/text/ParsePosition;-><init>(I)V

    .line 1396
    .local v0, "pos":Ljava/text/ParsePosition;
    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/text/MessageFormat;->parseToMap(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Map;

    move-result-object v1

    .line 1397
    .local v1, "result":Ljava/util/Map;
    invoke-virtual {v0}, Ljava/text/ParsePosition;->getIndex()I

    move-result v2

    if-nez v2, :cond_0

    .line 1398
    new-instance v2, Ljava/text/ParseException;

    const-string/jumbo v3, "MessageFormat parse error!"

    invoke-virtual {v0}, Ljava/text/ParsePosition;->getErrorIndex()I

    move-result v4

    invoke-direct {v2, v3, v4}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 1401
    :cond_0
    return-object v1
.end method

.method public parseToMap(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Map;
    .locals 12
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;

    .prologue
    const/4 v0, 0x0

    .line 1274
    if-nez p1, :cond_0

    .line 1275
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1350
    :goto_0
    return-object v0

    .line 1288
    :cond_0
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1290
    .local v5, "resultMap":Ljava/util/Map;
    const/4 v4, 0x0

    .line 1291
    .local v4, "patternOffset":I
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v6

    .line 1292
    .local v6, "sourceOffset":I
    new-instance v9, Ljava/text/ParsePosition;

    const/4 v10, 0x0

    invoke-direct {v9, v10}, Ljava/text/ParsePosition;-><init>(I)V

    .line 1293
    .local v9, "tempStatus":Ljava/text/ParsePosition;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v10, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-gt v1, v10, :cond_9

    .line 1295
    iget-object v10, p0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    aget v10, v10, v1

    sub-int v2, v10, v4

    .line 1296
    .local v2, "len":I
    if-eqz v2, :cond_1

    iget-object v10, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v10, v4, p1, v6, v2}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1298
    :cond_1
    add-int/2addr v6, v2

    .line 1299
    add-int/2addr v4, v2

    .line 1306
    iget-object v10, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v10, v10, v1

    if-nez v10, :cond_7

    .line 1310
    iget v10, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-eq v1, v10, :cond_3

    iget-object v10, p0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    add-int/lit8 v11, v1, 0x1

    aget v8, v10, v11

    .line 1313
    .local v8, "tempLength":I
    :goto_2
    if-lt v4, v8, :cond_4

    .line 1314
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 1319
    .local v3, "next":I
    :goto_3
    if-gez v3, :cond_5

    .line 1320
    invoke-virtual {p2, v6}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    goto :goto_0

    .line 1301
    .end local v3    # "next":I
    .end local v8    # "tempLength":I
    :cond_2
    invoke-virtual {p2, v6}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    goto :goto_0

    .line 1310
    :cond_3
    iget-object v10, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v8

    goto :goto_2

    .line 1316
    .restart local v8    # "tempLength":I
    :cond_4
    iget-object v10, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v10, v4, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    .restart local v3    # "next":I
    goto :goto_3

    .line 1323
    :cond_5
    invoke-virtual {p1, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 1324
    .local v7, "strValue":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v11, "{"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    iget-object v11, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    aget-object v11, v11, v1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string/jumbo v11, "}"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_6

    .line 1325
    iget-object v10, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    aget-object v10, v10, v1

    invoke-virtual {p1, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v5, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1328
    :cond_6
    move v6, v3

    .line 1293
    .end local v3    # "next":I
    .end local v7    # "strValue":Ljava/lang/String;
    .end local v8    # "tempLength":I
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1331
    :cond_7
    invoke-virtual {v9, v6}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1332
    iget-object v10, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    aget-object v10, v10, v1

    iget-object v11, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v11, v11, v1

    invoke-virtual {v11, p1, v9}, Ljava/text/Format;->parseObject(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Object;

    move-result-object v11

    invoke-interface {v5, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1335
    invoke-virtual {v9}, Ljava/text/ParsePosition;->getIndex()I

    move-result v10

    if-ne v10, v6, :cond_8

    .line 1336
    invoke-virtual {p2, v6}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    goto/16 :goto_0

    .line 1339
    :cond_8
    invoke-virtual {v9}, Ljava/text/ParsePosition;->getIndex()I

    move-result v6

    goto :goto_4

    .line 1342
    .end local v2    # "len":I
    :cond_9
    iget-object v10, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    sub-int v2, v10, v4

    .line 1343
    .restart local v2    # "len":I
    if-eqz v2, :cond_a

    iget-object v10, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v10, v4, p1, v6, v2}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 1345
    :cond_a
    add-int v10, v6, v2

    invoke-virtual {p2, v10}, Ljava/text/ParsePosition;->setIndex(I)V

    move-object v0, v5

    .line 1350
    goto/16 :goto_0

    .line 1347
    :cond_b
    invoke-virtual {p2, v6}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    goto/16 :goto_0
.end method

.method public setFormat(ILjava/text/Format;)V
    .locals 1
    .param p1, "formatElementIndex"    # I
    .param p2, "newFormat"    # Ljava/text/Format;

    .prologue
    .line 858
    iget-object v0, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aput-object p2, v0, p1

    .line 859
    return-void
.end method

.method public setFormatByArgumentIndex(ILjava/text/Format;)V
    .locals 3
    .param p1, "argumentIndex"    # I
    .param p2, "newFormat"    # Ljava/text/Format;

    .prologue
    .line 800
    iget-boolean v1, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    if-nez v1, :cond_0

    .line 801
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "This method is not available in MessageFormat objects that use alphanumeric argument names."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 805
    :cond_0
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    iget v1, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-gt v0, v1, :cond_2

    .line 806
    iget-object v1, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, p1, :cond_1

    .line 807
    iget-object v1, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aput-object p2, v1, v0

    .line 805
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 810
    :cond_2
    return-void
.end method

.method public setFormatByArgumentName(Ljava/lang/String;Ljava/text/Format;)V
    .locals 2
    .param p1, "argumentName"    # Ljava/lang/String;
    .param p2, "newFormat"    # Ljava/text/Format;

    .prologue
    .line 832
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-ge v0, v1, :cond_1

    .line 833
    iget-object v1, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 834
    iget-object v1, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aput-object p2, v1, v0

    .line 832
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 837
    :cond_1
    return-void
.end method

.method public setFormats([Ljava/text/Format;)V
    .locals 4
    .param p1, "newFormats"    # [Ljava/text/Format;

    .prologue
    .line 765
    array-length v1, p1

    .line 766
    .local v1, "runsToCopy":I
    iget v2, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    add-int/lit8 v2, v2, 0x1

    if-le v1, v2, :cond_0

    .line 767
    iget v2, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    add-int/lit8 v1, v2, 0x1

    .line 769
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 770
    iget-object v2, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, p1, v0

    aput-object v3, v2, v0

    .line 769
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 772
    :cond_1
    return-void
.end method

.method public setFormatsByArgumentIndex([Ljava/text/Format;)V
    .locals 4
    .param p1, "newFormats"    # [Ljava/text/Format;

    .prologue
    .line 698
    iget-boolean v2, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    if-nez v2, :cond_0

    .line 699
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "This method is not available in MessageFormat objects that use alphanumeric argument names."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 703
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-gt v0, v2, :cond_2

    .line 704
    iget-object v2, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 705
    .local v1, "j":I
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 706
    iget-object v2, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, p1, v1

    aput-object v3, v2, v0

    .line 703
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 709
    .end local v1    # "j":I
    :cond_2
    return-void
.end method

.method public setFormatsByArgumentName(Ljava/util/Map;)V
    .locals 3
    .param p1, "newFormats"    # Ljava/util/Map;

    .prologue
    .line 733
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-gt v1, v2, :cond_1

    .line 734
    iget-object v2, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 735
    iget-object v2, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/Format;

    .line 736
    .local v0, "f":Ljava/text/Format;
    iget-object v2, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aput-object v0, v2, v1

    .line 733
    .end local v0    # "f":Ljava/text/Format;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 739
    :cond_1
    return-void
.end method

.method public setLocale(Lcom/ibm/icu/util/ULocale;)V
    .locals 1
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/ibm/icu/text/MessageFormat;->toPattern()Ljava/lang/String;

    move-result-object v0

    .line 485
    .local v0, "existingPattern":Ljava/lang/String;
    iput-object p1, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    .line 486
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/MessageFormat;->applyPattern(Ljava/lang/String;)V

    .line 487
    return-void
.end method

.method public setLocale(Ljava/util/Locale;)V
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 467
    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/MessageFormat;->setLocale(Lcom/ibm/icu/util/ULocale;)V

    .line 468
    return-void
.end method

.method public toPattern()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 603
    const/4 v1, 0x0

    .line 604
    .local v1, "lastOffset":I
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 605
    .local v2, "result":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lcom/ibm/icu/text/MessageFormat;->maxOffset:I

    if-gt v0, v3, :cond_11

    .line 606
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    aget v4, v4, v0

    invoke-static {v3, v1, v4, v2}, Lcom/ibm/icu/text/MessageFormat;->copyAndFixQuotes(Ljava/lang/String;IILjava/lang/StringBuffer;)V

    .line 607
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->offsets:[I

    aget v1, v3, v0

    .line 608
    const/16 v3, 0x7b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 609
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNames:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 610
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    if-nez v3, :cond_1

    .line 661
    :cond_0
    :goto_1
    const/16 v3, 0x7d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 605
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 612
    :cond_1
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    instance-of v3, v3, Lcom/ibm/icu/text/DecimalFormat;

    if-eqz v3, :cond_6

    .line 613
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v4}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 614
    const-string/jumbo v3, ",number"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 615
    :cond_2
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v4}, Lcom/ibm/icu/text/NumberFormat;->getCurrencyInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 616
    const-string/jumbo v3, ",number,currency"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 617
    :cond_3
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v4}, Lcom/ibm/icu/text/NumberFormat;->getPercentInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 618
    const-string/jumbo v3, ",number,percent"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 619
    :cond_4
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v4}, Lcom/ibm/icu/text/NumberFormat;->getIntegerInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 620
    const-string/jumbo v3, ",number,integer"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 622
    :cond_5
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, ",number,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    check-cast v3, Lcom/ibm/icu/text/DecimalFormat;

    invoke-virtual {v3}, Lcom/ibm/icu/text/DecimalFormat;->toPattern()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 625
    :cond_6
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    instance-of v3, v3, Lcom/ibm/icu/text/SimpleDateFormat;

    if-eqz v3, :cond_f

    .line 626
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v7, v4}, Lcom/ibm/icu/text/DateFormat;->getDateInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 627
    const-string/jumbo v3, ",date"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 628
    :cond_7
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v8, v4}, Lcom/ibm/icu/text/DateFormat;->getDateInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 629
    const-string/jumbo v3, ",date,short"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 633
    :cond_8
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v6, v4}, Lcom/ibm/icu/text/DateFormat;->getDateInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 634
    const-string/jumbo v3, ",date,long"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 635
    :cond_9
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v5, v4}, Lcom/ibm/icu/text/DateFormat;->getDateInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 636
    const-string/jumbo v3, ",date,full"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 637
    :cond_a
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v7, v4}, Lcom/ibm/icu/text/DateFormat;->getTimeInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 638
    const-string/jumbo v3, ",time"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 639
    :cond_b
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v8, v4}, Lcom/ibm/icu/text/DateFormat;->getTimeInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 640
    const-string/jumbo v3, ",time,short"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 644
    :cond_c
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v6, v4}, Lcom/ibm/icu/text/DateFormat;->getTimeInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 645
    const-string/jumbo v3, ",time,long"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 646
    :cond_d
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->ulocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v5, v4}, Lcom/ibm/icu/text/DateFormat;->getTimeInstance(ILcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormat;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 647
    const-string/jumbo v3, ",time,full"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 649
    :cond_e
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, ",date,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    check-cast v3, Lcom/ibm/icu/text/SimpleDateFormat;

    invoke-virtual {v3}, Lcom/ibm/icu/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 651
    :cond_f
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    instance-of v3, v3, Ljava/text/ChoiceFormat;

    if-eqz v3, :cond_10

    .line 652
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, ",choice,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    check-cast v3, Ljava/text/ChoiceFormat;

    invoke-virtual {v3}, Ljava/text/ChoiceFormat;->toPattern()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 654
    :cond_10
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->formats:[Ljava/text/Format;

    aget-object v3, v3, v0

    instance-of v3, v3, Lcom/ibm/icu/text/PluralFormat;

    if-eqz v3, :cond_0

    .line 656
    const-string/jumbo v3, ", plural, support for converting PluralFormat to pattern not yet available."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 663
    :cond_11
    iget-object v3, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    iget-object v4, p0, Lcom/ibm/icu/text/MessageFormat;->pattern:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-static {v3, v1, v4, v2}, Lcom/ibm/icu/text/MessageFormat;->copyAndFixQuotes(Ljava/lang/String;IILjava/lang/StringBuffer;)V

    .line 664
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public usesNamedArguments()Z
    .locals 1

    .prologue
    .line 1097
    iget-boolean v0, p0, Lcom/ibm/icu/text/MessageFormat;->argumentNamesAreNumeric:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
