.class Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;
.super Ljava/lang/Object;
.source "TransliteratorParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/TransliteratorParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RuleHalf"
.end annotation


# instance fields
.field public anchorEnd:Z

.field public anchorStart:Z

.field public ante:I

.field public cursor:I

.field public cursorOffset:I

.field private cursorOffsetPos:I

.field private nextSegmentNumber:I

.field public post:I

.field public text:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365
    iput v1, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursor:I

    .line 366
    iput v1, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->ante:I

    .line 367
    iput v1, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->post:I

    .line 378
    iput v0, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffset:I

    .line 382
    iput v0, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffsetPos:I

    .line 384
    iput-boolean v0, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorStart:Z

    .line 385
    iput-boolean v0, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorEnd:Z

    .line 391
    const/4 v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->nextSegmentNumber:I

    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/text/TransliteratorParser$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/ibm/icu/text/TransliteratorParser$1;

    .prologue
    .line 361
    invoke-direct {p0}, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;-><init>()V

    return-void
.end method

.method private parseSection(Ljava/lang/String;IILcom/ibm/icu/text/TransliteratorParser;Ljava/lang/StringBuffer;Lcom/ibm/icu/text/UnicodeSet;Z)I
    .locals 33
    .param p1, "rule"    # Ljava/lang/String;
    .param p2, "pos"    # I
    .param p3, "limit"    # I
    .param p4, "parser"    # Lcom/ibm/icu/text/TransliteratorParser;
    .param p5, "buf"    # Ljava/lang/StringBuffer;
    .param p6, "illegal"    # Lcom/ibm/icu/text/UnicodeSet;
    .param p7, "isSegment"    # Z

    .prologue
    .line 441
    move/from16 v29, p2

    .line 442
    .local v29, "start":I
    const/16 v23, 0x0

    .line 443
    .local v23, "pp":Ljava/text/ParsePosition;
    const/16 v25, -0x1

    .line 444
    .local v25, "quoteStart":I
    const/16 v24, -0x1

    .line 445
    .local v24, "quoteLimit":I
    const/16 v32, -0x1

    .line 446
    .local v32, "varStart":I
    const/16 v31, -0x1

    .line 447
    .local v31, "varLimit":I
    const/4 v4, 0x1

    new-array v0, v4, [I

    move-object/from16 v17, v0

    .line 448
    .local v17, "iref":[I
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v12

    .local v12, "bufStart":I
    move/from16 v22, p2

    .line 451
    .end local p2    # "pos":I
    .local v22, "pos":I
    :goto_0
    move/from16 v0, v22

    move/from16 v1, p3

    if-ge v0, v1, :cond_29

    .line 454
    add-int/lit8 p2, v22, 0x1

    .end local v22    # "pos":I
    .restart local p2    # "pos":I
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v13

    .line 455
    .local v13, "c":C
    invoke-static {v13}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v4

    if-eqz v4, :cond_0

    move/from16 v22, p2

    .line 456
    .end local p2    # "pos":I
    .restart local v22    # "pos":I
    goto :goto_0

    .line 459
    .end local v22    # "pos":I
    .restart local p2    # "pos":I
    :cond_0
    const-string/jumbo v4, "=><\u2190\u2192\u2194;"

    invoke-virtual {v4, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-ltz v4, :cond_2

    .line 460
    if-eqz p7, :cond_1

    .line 461
    const-string/jumbo v4, "Unclosed segment"

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 795
    .end local v13    # "c":C
    :cond_1
    :goto_1
    :sswitch_0
    return p2

    .line 465
    .restart local v13    # "c":C
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorEnd:Z

    if-eqz v4, :cond_3

    .line 467
    const-string/jumbo v4, "Malformed variable reference"

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 469
    :cond_3
    add-int/lit8 v4, p2, -0x1

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/ibm/icu/text/UnicodeSet;->resemblesPattern(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 470
    if-nez v23, :cond_4

    .line 471
    new-instance v23, Ljava/text/ParsePosition;

    .end local v23    # "pp":Ljava/text/ParsePosition;
    const/4 v4, 0x0

    move-object/from16 v0, v23

    invoke-direct {v0, v4}, Ljava/text/ParsePosition;-><init>(I)V

    .line 473
    .restart local v23    # "pp":Ljava/text/ParsePosition;
    :cond_4
    add-int/lit8 v4, p2, -0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 474
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/TransliteratorParser;->access$500(Lcom/ibm/icu/text/TransliteratorParser;Ljava/lang/String;Ljava/text/ParsePosition;)C

    move-result v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 475
    invoke-virtual/range {v23 .. v23}, Ljava/text/ParsePosition;->getIndex()I

    move-result p2

    move/from16 v22, p2

    .line 476
    .end local p2    # "pos":I
    .restart local v22    # "pos":I
    goto :goto_0

    .line 479
    .end local v22    # "pos":I
    .restart local p2    # "pos":I
    :cond_5
    const/16 v4, 0x5c

    if-ne v13, v4, :cond_8

    .line 480
    move/from16 v0, p2

    move/from16 v1, p3

    if-ne v0, v1, :cond_6

    .line 481
    const-string/jumbo v4, "Trailing backslash"

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 483
    :cond_6
    const/4 v4, 0x0

    aput p2, v17, v4

    .line 484
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/Utility;->unescapeAt(Ljava/lang/String;[I)I

    move-result v15

    .line 485
    .local v15, "escaped":I
    const/4 v4, 0x0

    aget p2, v17, v4

    .line 486
    const/4 v4, -0x1

    if-ne v15, v4, :cond_7

    .line 487
    const-string/jumbo v4, "Malformed escape"

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 489
    :cond_7
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move/from16 v2, v29

    invoke-static {v0, v15, v1, v2}, Lcom/ibm/icu/text/TransliteratorParser;->access$600(Lcom/ibm/icu/text/TransliteratorParser;ILjava/lang/String;I)V

    .line 490
    move-object/from16 v0, p5

    invoke-static {v0, v15}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    move/from16 v22, p2

    .line 491
    .end local p2    # "pos":I
    .restart local v22    # "pos":I
    goto/16 :goto_0

    .line 494
    .end local v15    # "escaped":I
    .end local v22    # "pos":I
    .restart local p2    # "pos":I
    :cond_8
    const/16 v4, 0x27

    if-ne v13, v4, :cond_c

    .line 495
    const/16 v4, 0x27

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v16

    .line 496
    .local v16, "iq":I
    move/from16 v0, v16

    move/from16 v1, p2

    if-ne v0, v1, :cond_9

    .line 497
    move-object/from16 v0, p5

    invoke-virtual {v0, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 498
    add-int/lit8 p2, p2, 0x1

    move/from16 v22, p2

    .line 499
    .end local p2    # "pos":I
    .restart local v22    # "pos":I
    goto/16 :goto_0

    .line 506
    .end local v22    # "pos":I
    .restart local p2    # "pos":I
    :cond_9
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v25

    .line 508
    :goto_2
    if-gez v16, :cond_a

    .line 509
    const-string/jumbo v4, "Unterminated quote"

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 511
    :cond_a
    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 512
    add-int/lit8 p2, v16, 0x1

    .line 513
    move/from16 v0, p2

    move/from16 v1, p3

    if-ge v0, v1, :cond_b

    invoke-virtual/range {p1 .. p2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v7, 0x27

    if-ne v4, v7, :cond_b

    .line 515
    const/16 v4, 0x27

    add-int/lit8 v7, p2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Ljava/lang/String;->indexOf(II)I

    move-result v16

    .line 517
    goto :goto_2

    .line 521
    :cond_b
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v24

    .line 523
    move/from16 v16, v25

    :goto_3
    move/from16 v0, v16

    move/from16 v1, v24

    if-ge v0, v1, :cond_28

    .line 524
    move-object/from16 v0, p5

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move/from16 v2, v29

    invoke-static {v0, v4, v1, v2}, Lcom/ibm/icu/text/TransliteratorParser;->access$600(Lcom/ibm/icu/text/TransliteratorParser;ILjava/lang/String;I)V

    .line 523
    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    .line 530
    .end local v16    # "iq":I
    :cond_c
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move/from16 v2, v29

    invoke-static {v0, v13, v1, v2}, Lcom/ibm/icu/text/TransliteratorParser;->access$600(Lcom/ibm/icu/text/TransliteratorParser;ILjava/lang/String;I)V

    .line 532
    move-object/from16 v0, p6

    invoke-virtual {v0, v13}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 533
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "Illegal character \'"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    const/16 v7, 0x27

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 536
    :cond_d
    sparse-switch v13, :sswitch_data_0

    .line 785
    const/16 v4, 0x21

    if-lt v13, v4, :cond_11

    const/16 v4, 0x7e

    if-gt v13, v4, :cond_11

    const/16 v4, 0x30

    if-lt v13, v4, :cond_e

    const/16 v4, 0x39

    if-le v13, v4, :cond_11

    :cond_e
    const/16 v4, 0x41

    if-lt v13, v4, :cond_f

    const/16 v4, 0x5a

    if-le v13, v4, :cond_11

    :cond_f
    const/16 v4, 0x61

    if-lt v13, v4, :cond_10

    const/16 v4, 0x7a

    if-le v13, v4, :cond_11

    .line 789
    :cond_10
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "Unquoted "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 791
    :cond_11
    move-object/from16 v0, p5

    invoke-virtual {v0, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_4
    move/from16 v22, p2

    .line 794
    .end local p2    # "pos":I
    .restart local v22    # "pos":I
    goto/16 :goto_0

    .line 542
    .end local v22    # "pos":I
    .restart local p2    # "pos":I
    :sswitch_1
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-nez v4, :cond_12

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorStart:Z

    if-nez v4, :cond_12

    .line 543
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorStart:Z

    goto :goto_4

    .line 545
    :cond_12
    const-string/jumbo v4, "Misplaced anchor start"

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_4

    .line 553
    :sswitch_2
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v11

    .line 558
    .local v11, "bufSegStart":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->nextSegmentNumber:I

    move/from16 v27, v0

    add-int/lit8 v4, v27, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->nextSegmentNumber:I

    .line 561
    .local v27, "segmentNumber":I
    invoke-static {}, Lcom/ibm/icu/text/TransliteratorParser;->access$700()Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v9

    const/4 v10, 0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    invoke-direct/range {v3 .. v10}, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->parseSection(Ljava/lang/String;IILcom/ibm/icu/text/TransliteratorParser;Ljava/lang/StringBuffer;Lcom/ibm/icu/text/UnicodeSet;Z)I

    move-result p2

    .line 567
    new-instance v3, Lcom/ibm/icu/text/StringMatcher;

    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p4 .. p4}, Lcom/ibm/icu/text/TransliteratorParser;->access$100(Lcom/ibm/icu/text/TransliteratorParser;)Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    move-result-object v7

    move/from16 v0, v27

    invoke-direct {v3, v4, v0, v7}, Lcom/ibm/icu/text/StringMatcher;-><init>(Ljava/lang/String;ILcom/ibm/icu/text/RuleBasedTransliterator$Data;)V

    .line 572
    .local v3, "m":Lcom/ibm/icu/text/StringMatcher;
    move-object/from16 v0, p4

    move/from16 v1, v27

    invoke-virtual {v0, v1, v3}, Lcom/ibm/icu/text/TransliteratorParser;->setSegmentObject(ILcom/ibm/icu/text/StringMatcher;)V

    .line 573
    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 574
    move-object/from16 v0, p4

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->getSegmentStandin(I)C

    move-result v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 580
    .end local v3    # "m":Lcom/ibm/icu/text/StringMatcher;
    .end local v11    # "bufSegStart":I
    .end local v27    # "segmentNumber":I
    :sswitch_3
    const/4 v4, 0x0

    aput p2, v17, v4

    .line 581
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/ibm/icu/text/TransliteratorIDParser;->parseFilterID(Ljava/lang/String;[I)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    move-result-object v28

    .line 583
    .local v28, "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    if-eqz v28, :cond_13

    const/16 v4, 0x28

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1, v4}, Lcom/ibm/icu/impl/Utility;->parseChar(Ljava/lang/String;[IC)Z

    move-result v4

    if-nez v4, :cond_14

    .line 585
    :cond_13
    const-string/jumbo v4, "Invalid function"

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 588
    :cond_14
    invoke-virtual/range {v28 .. v28}, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->getInstance()Lcom/ibm/icu/text/Transliterator;

    move-result-object v30

    .line 589
    .local v30, "t":Lcom/ibm/icu/text/Transliterator;
    if-nez v30, :cond_15

    .line 590
    const-string/jumbo v4, "Invalid function ID"

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 595
    :cond_15
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v11

    .line 598
    .restart local v11    # "bufSegStart":I
    const/4 v4, 0x0

    aget v5, v17, v4

    invoke-static {}, Lcom/ibm/icu/text/TransliteratorParser;->access$800()Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v9

    const/4 v10, 0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    invoke-direct/range {v3 .. v10}, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->parseSection(Ljava/lang/String;IILcom/ibm/icu/text/TransliteratorParser;Ljava/lang/StringBuffer;Lcom/ibm/icu/text/UnicodeSet;Z)I

    move-result p2

    .line 602
    new-instance v26, Lcom/ibm/icu/text/FunctionReplacer;

    new-instance v4, Lcom/ibm/icu/text/StringReplacer;

    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {p4 .. p4}, Lcom/ibm/icu/text/TransliteratorParser;->access$100(Lcom/ibm/icu/text/TransliteratorParser;)Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Lcom/ibm/icu/text/StringReplacer;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/RuleBasedTransliterator$Data;)V

    move-object/from16 v0, v26

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v4}, Lcom/ibm/icu/text/FunctionReplacer;-><init>(Lcom/ibm/icu/text/Transliterator;Lcom/ibm/icu/text/UnicodeReplacer;)V

    .line 607
    .local v26, "r":Lcom/ibm/icu/text/FunctionReplacer;
    move-object/from16 v0, p5

    invoke-virtual {v0, v11}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 608
    move-object/from16 v0, p4

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->generateStandInFor(Ljava/lang/Object;)C

    move-result v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_4

    .line 618
    .end local v11    # "bufSegStart":I
    .end local v26    # "r":Lcom/ibm/icu/text/FunctionReplacer;
    .end local v28    # "single":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    .end local v30    # "t":Lcom/ibm/icu/text/Transliterator;
    :sswitch_4
    move/from16 v0, p2

    move/from16 v1, p3

    if-ne v0, v1, :cond_16

    .line 621
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorEnd:Z

    goto/16 :goto_4

    .line 625
    :cond_16
    invoke-virtual/range {p1 .. p2}, Ljava/lang/String;->charAt(I)C

    move-result v13

    .line 626
    const/16 v4, 0xa

    invoke-static {v13, v4}, Lcom/ibm/icu/lang/UCharacter;->digit(II)I

    move-result v26

    .line 627
    .local v26, "r":I
    const/4 v4, 0x1

    move/from16 v0, v26

    if-lt v0, v4, :cond_18

    const/16 v4, 0x9

    move/from16 v0, v26

    if-gt v0, v4, :cond_18

    .line 628
    const/4 v4, 0x0

    aput p2, v17, v4

    .line 629
    const/16 v4, 0xa

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1, v4}, Lcom/ibm/icu/impl/Utility;->parseNumber(Ljava/lang/String;[II)I

    move-result v26

    .line 630
    if-gez v26, :cond_17

    .line 631
    const-string/jumbo v4, "Undefined segment reference"

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 634
    :cond_17
    const/4 v4, 0x0

    aget p2, v17, v4

    .line 635
    move-object/from16 v0, p4

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->getSegmentStandin(I)C

    move-result v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_4

    .line 637
    :cond_18
    if-nez v23, :cond_19

    .line 638
    new-instance v23, Ljava/text/ParsePosition;

    .end local v23    # "pp":Ljava/text/ParsePosition;
    const/4 v4, 0x0

    move-object/from16 v0, v23

    invoke-direct {v0, v4}, Ljava/text/ParsePosition;-><init>(I)V

    .line 640
    .restart local v23    # "pp":Ljava/text/ParsePosition;
    :cond_19
    move-object/from16 v0, v23

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 641
    invoke-static/range {p4 .. p4}, Lcom/ibm/icu/text/TransliteratorParser;->access$900(Lcom/ibm/icu/text/TransliteratorParser;)Lcom/ibm/icu/text/TransliteratorParser$ParseData;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move/from16 v2, p3

    invoke-virtual {v4, v0, v1, v2}, Lcom/ibm/icu/text/TransliteratorParser$ParseData;->parseReference(Ljava/lang/String;Ljava/text/ParsePosition;I)Ljava/lang/String;

    move-result-object v21

    .line 643
    .local v21, "name":Ljava/lang/String;
    if-nez v21, :cond_1a

    .line 649
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorEnd:Z

    goto/16 :goto_4

    .line 652
    :cond_1a
    invoke-virtual/range {v23 .. v23}, Ljava/text/ParsePosition;->getIndex()I

    move-result p2

    .line 657
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v32

    .line 658
    move-object/from16 v0, p4

    move-object/from16 v1, v21

    move-object/from16 v2, p5

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/TransliteratorParser;->access$1000(Lcom/ibm/icu/text/TransliteratorParser;Ljava/lang/String;Ljava/lang/StringBuffer;)V

    .line 659
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v31

    goto/16 :goto_4

    .line 664
    .end local v21    # "name":Ljava/lang/String;
    .end local v26    # "r":I
    :sswitch_5
    invoke-virtual/range {p4 .. p4}, Lcom/ibm/icu/text/TransliteratorParser;->getDotStandIn()C

    move-result v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_4

    .line 676
    :sswitch_6
    if-eqz p7, :cond_1b

    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-ne v4, v12, :cond_1b

    .line 678
    const-string/jumbo v4, "Misplaced quantifier"

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_4

    .line 685
    :cond_1b
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    move/from16 v0, v24

    if-ne v4, v0, :cond_1c

    .line 687
    move/from16 v5, v25

    .line 688
    .local v5, "qstart":I
    move/from16 v6, v24

    .line 702
    .local v6, "qlimit":I
    :goto_5
    :try_start_0
    new-instance v3, Lcom/ibm/icu/text/StringMatcher;

    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    invoke-static/range {p4 .. p4}, Lcom/ibm/icu/text/TransliteratorParser;->access$100(Lcom/ibm/icu/text/TransliteratorParser;)Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/ibm/icu/text/StringMatcher;-><init>(Ljava/lang/String;IIILcom/ibm/icu/text/RuleBasedTransliterator$Data;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 707
    .local v3, "m":Lcom/ibm/icu/text/UnicodeMatcher;
    const/16 v20, 0x0

    .line 708
    .local v20, "min":I
    const v19, 0x7fffffff

    .line 709
    .local v19, "max":I
    sparse-switch v13, :sswitch_data_1

    .line 720
    :goto_6
    new-instance v18, Lcom/ibm/icu/text/Quantifier;

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-direct {v0, v3, v1, v2}, Lcom/ibm/icu/text/Quantifier;-><init>(Lcom/ibm/icu/text/UnicodeMatcher;II)V

    .line 721
    .end local v3    # "m":Lcom/ibm/icu/text/UnicodeMatcher;
    .local v18, "m":Lcom/ibm/icu/text/UnicodeMatcher;
    move-object/from16 v0, p5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 722
    move-object/from16 v0, p4

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->generateStandInFor(Ljava/lang/Object;)C

    move-result v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_4

    .line 689
    .end local v5    # "qstart":I
    .end local v6    # "qlimit":I
    .end local v18    # "m":Lcom/ibm/icu/text/UnicodeMatcher;
    .end local v19    # "max":I
    .end local v20    # "min":I
    :cond_1c
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    move/from16 v0, v31

    if-ne v4, v0, :cond_1d

    .line 691
    move/from16 v5, v32

    .line 692
    .restart local v5    # "qstart":I
    move/from16 v6, v31

    .line 693
    .restart local v6    # "qlimit":I
    goto :goto_5

    .line 696
    .end local v5    # "qstart":I
    .end local v6    # "qlimit":I
    :cond_1d
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    add-int/lit8 v5, v4, -0x1

    .line 697
    .restart local v5    # "qstart":I
    add-int/lit8 v6, v5, 0x1

    .restart local v6    # "qlimit":I
    goto :goto_5

    .line 704
    :catch_0
    move-exception v14

    .line 705
    .local v14, "e":Ljava/lang/RuntimeException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v8, "Failure in rule: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual/range {p1 .. p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 711
    .end local v14    # "e":Ljava/lang/RuntimeException;
    .restart local v3    # "m":Lcom/ibm/icu/text/UnicodeMatcher;
    .restart local v19    # "max":I
    .restart local v20    # "min":I
    :sswitch_7
    const/16 v20, 0x1

    .line 712
    goto :goto_6

    .line 714
    :sswitch_8
    const/16 v20, 0x0

    .line 715
    const/16 v19, 0x1

    goto :goto_6

    .line 738
    .end local v3    # "m":Lcom/ibm/icu/text/UnicodeMatcher;
    .end local v5    # "qstart":I
    .end local v6    # "qlimit":I
    .end local v19    # "max":I
    .end local v20    # "min":I
    :sswitch_9
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->ante:I

    if-ltz v4, :cond_1e

    .line 739
    const-string/jumbo v4, "Multiple ante contexts"

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 741
    :cond_1e
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->ante:I

    goto/16 :goto_4

    .line 744
    :sswitch_a
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->post:I

    if-ltz v4, :cond_1f

    .line 745
    const-string/jumbo v4, "Multiple post contexts"

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 747
    :cond_1f
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->post:I

    goto/16 :goto_4

    .line 750
    :sswitch_b
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursor:I

    if-ltz v4, :cond_20

    .line 751
    const-string/jumbo v4, "Multiple cursors"

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 753
    :cond_20
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursor:I

    goto/16 :goto_4

    .line 756
    :sswitch_c
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffset:I

    if-gez v4, :cond_22

    .line 757
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-lez v4, :cond_21

    .line 758
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "Misplaced "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 760
    :cond_21
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffset:I

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffset:I

    goto/16 :goto_4

    .line 761
    :cond_22
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffset:I

    if-lez v4, :cond_25

    .line 762
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    move-object/from16 v0, p0

    iget v7, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffsetPos:I

    if-ne v4, v7, :cond_23

    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursor:I

    if-ltz v4, :cond_24

    .line 763
    :cond_23
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "Misplaced "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 765
    :cond_24
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffset:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffset:I

    goto/16 :goto_4

    .line 767
    :cond_25
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursor:I

    if-nez v4, :cond_26

    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-nez v4, :cond_26

    .line 768
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffset:I

    goto/16 :goto_4

    .line 769
    :cond_26
    move-object/from16 v0, p0

    iget v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursor:I

    if-gez v4, :cond_27

    .line 770
    invoke-virtual/range {p5 .. p5}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffsetPos:I

    .line 771
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffset:I

    goto/16 :goto_4

    .line 773
    :cond_27
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "Misplaced "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-static {v4, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_4

    .restart local v16    # "iq":I
    :cond_28
    move/from16 v22, p2

    .end local p2    # "pos":I
    .restart local v22    # "pos":I
    goto/16 :goto_0

    .end local v13    # "c":C
    .end local v16    # "iq":I
    :cond_29
    move/from16 p2, v22

    .end local v22    # "pos":I
    .restart local p2    # "pos":I
    goto/16 :goto_1

    .line 536
    :sswitch_data_0
    .sparse-switch
        0x24 -> :sswitch_4
        0x26 -> :sswitch_3
        0x28 -> :sswitch_2
        0x29 -> :sswitch_0
        0x2a -> :sswitch_6
        0x2b -> :sswitch_6
        0x2e -> :sswitch_5
        0x3f -> :sswitch_6
        0x40 -> :sswitch_c
        0x5e -> :sswitch_1
        0x7b -> :sswitch_9
        0x7c -> :sswitch_b
        0x7d -> :sswitch_a
        0x2206 -> :sswitch_3
    .end sparse-switch

    .line 709
    :sswitch_data_1
    .sparse-switch
        0x2b -> :sswitch_7
        0x3f -> :sswitch_8
    .end sparse-switch
.end method


# virtual methods
.method public isValidInput(Lcom/ibm/icu/text/TransliteratorParser;)Z
    .locals 3
    .param p1, "parser"    # Lcom/ibm/icu/text/TransliteratorParser;

    .prologue
    .line 828
    const/4 v1, 0x0

    .local v1, "i":I
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 829
    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 830
    .local v0, "c":I
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 831
    invoke-static {p1}, Lcom/ibm/icu/text/TransliteratorParser;->access$900(Lcom/ibm/icu/text/TransliteratorParser;)Lcom/ibm/icu/text/TransliteratorParser$ParseData;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/ibm/icu/text/TransliteratorParser$ParseData;->isMatcher(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 832
    const/4 v2, 0x0

    .line 835
    .end local v0    # "c":I
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public isValidOutput(Lcom/ibm/icu/text/TransliteratorParser;)Z
    .locals 3
    .param p1, "parser"    # Lcom/ibm/icu/text/TransliteratorParser;

    .prologue
    .line 813
    const/4 v1, 0x0

    .local v1, "i":I
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 814
    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 815
    .local v0, "c":I
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 816
    invoke-static {p1}, Lcom/ibm/icu/text/TransliteratorParser;->access$900(Lcom/ibm/icu/text/TransliteratorParser;)Lcom/ibm/icu/text/TransliteratorParser$ParseData;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/ibm/icu/text/TransliteratorParser$ParseData;->isReplacer(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 817
    const/4 v2, 0x0

    .line 820
    .end local v0    # "c":I
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public parse(Ljava/lang/String;IILcom/ibm/icu/text/TransliteratorParser;)I
    .locals 9
    .param p1, "rule"    # Ljava/lang/String;
    .param p2, "pos"    # I
    .param p3, "limit"    # I
    .param p4, "parser"    # Lcom/ibm/icu/text/TransliteratorParser;

    .prologue
    .line 401
    move v8, p2

    .line 402
    .local v8, "start":I
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 403
    .local v5, "buf":Ljava/lang/StringBuffer;
    invoke-static {}, Lcom/ibm/icu/text/TransliteratorParser;->access$400()Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v6

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v7}, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->parseSection(Ljava/lang/String;IILcom/ibm/icu/text/TransliteratorParser;Ljava/lang/StringBuffer;Lcom/ibm/icu/text/UnicodeSet;Z)I

    move-result p2

    .line 404
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    .line 406
    iget v0, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffset:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursor:I

    iget v1, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffsetPos:I

    if-eq v0, v1, :cond_0

    .line 407
    const-string/jumbo v0, "Misplaced |"

    invoke-static {v0, p1, v8}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 410
    :cond_0
    return p2
.end method

.method removeContext()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 802
    iget-object v3, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    iget v0, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->ante:I

    if-gez v0, :cond_0

    move v0, v1

    :goto_0
    iget v2, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->post:I

    if-gez v2, :cond_1

    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    :goto_1
    invoke-virtual {v3, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    .line 804
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->post:I

    iput v0, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->ante:I

    .line 805
    iput-boolean v1, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorEnd:Z

    iput-boolean v1, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorStart:Z

    .line 806
    return-void

    .line 802
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->ante:I

    goto :goto_0

    :cond_1
    iget v2, p0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->post:I

    goto :goto_1
.end method
