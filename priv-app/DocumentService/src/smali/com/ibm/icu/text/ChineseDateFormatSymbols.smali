.class public Lcom/ibm/icu/text/ChineseDateFormatSymbols;
.super Lcom/ibm/icu/text/DateFormatSymbols;
.source "ChineseDateFormatSymbols.java"


# static fields
.field static class$com$ibm$icu$util$ChineseCalendar:Ljava/lang/Class; = null

.field static final serialVersionUID:J = 0x5ec14695eb6891faL


# instance fields
.field isLeapMonth:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/ChineseDateFormatSymbols;-><init>(Lcom/ibm/icu/util/ULocale;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/ULocale;)V
    .locals 1
    .param p1, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 76
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0, p2}, Lcom/ibm/icu/text/DateFormatSymbols;-><init>(Ljava/lang/Class;Lcom/ibm/icu/util/ULocale;)V

    .line 77
    return-void

    .line 76
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/ibm/icu/util/Calendar;Ljava/util/Locale;)V
    .locals 1
    .param p1, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "locale"    # Ljava/util/Locale;

    .prologue
    .line 66
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0, p2}, Lcom/ibm/icu/text/DateFormatSymbols;-><init>(Ljava/lang/Class;Ljava/util/Locale;)V

    .line 67
    return-void

    .line 66
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/ibm/icu/util/ULocale;)V
    .locals 1
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 56
    sget-object v0, Lcom/ibm/icu/text/ChineseDateFormatSymbols;->class$com$ibm$icu$util$ChineseCalendar:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string/jumbo v0, "com.ibm.icu.util.ChineseCalendar"

    invoke-static {v0}, Lcom/ibm/icu/text/ChineseDateFormatSymbols;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/ibm/icu/text/ChineseDateFormatSymbols;->class$com$ibm$icu$util$ChineseCalendar:Ljava/lang/Class;

    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;-><init>(Ljava/lang/Class;Lcom/ibm/icu/util/ULocale;)V

    .line 57
    return-void

    .line 56
    :cond_0
    sget-object v0, Lcom/ibm/icu/text/ChineseDateFormatSymbols;->class$com$ibm$icu$util$ChineseCalendar:Ljava/lang/Class;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .locals 2
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 47
    sget-object v0, Lcom/ibm/icu/text/ChineseDateFormatSymbols;->class$com$ibm$icu$util$ChineseCalendar:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string/jumbo v0, "com.ibm.icu.util.ChineseCalendar"

    invoke-static {v0}, Lcom/ibm/icu/text/ChineseDateFormatSymbols;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/ibm/icu/text/ChineseDateFormatSymbols;->class$com$ibm$icu$util$ChineseCalendar:Ljava/lang/Class;

    :goto_0
    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/DateFormatSymbols;-><init>(Ljava/lang/Class;Lcom/ibm/icu/util/ULocale;)V

    .line 48
    return-void

    .line 47
    :cond_0
    sget-object v0, Lcom/ibm/icu/text/ChineseDateFormatSymbols;->class$com$ibm$icu$util$ChineseCalendar:Ljava/lang/Class;

    goto :goto_0
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 47
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public getLeapMonth(I)Ljava/lang/String;
    .locals 1
    .param p1, "leap"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/ibm/icu/text/ChineseDateFormatSymbols;->isLeapMonth:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method initializeData(Lcom/ibm/icu/text/DateFormatSymbols;)V
    .locals 1
    .param p1, "dfs"    # Lcom/ibm/icu/text/DateFormatSymbols;

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->initializeData(Lcom/ibm/icu/text/DateFormatSymbols;)V

    .line 98
    instance-of v0, p1, Lcom/ibm/icu/text/ChineseDateFormatSymbols;

    if-eqz v0, :cond_0

    .line 99
    check-cast p1, Lcom/ibm/icu/text/ChineseDateFormatSymbols;

    .end local p1    # "dfs":Lcom/ibm/icu/text/DateFormatSymbols;
    iget-object v0, p1, Lcom/ibm/icu/text/ChineseDateFormatSymbols;->isLeapMonth:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/ChineseDateFormatSymbols;->isLeapMonth:[Ljava/lang/String;

    .line 101
    :cond_0
    return-void
.end method

.method protected initializeData(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/impl/CalendarData;)V
    .locals 1
    .param p1, "loc"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "calData"    # Lcom/ibm/icu/impl/CalendarData;

    .prologue
    .line 92
    invoke-super {p0, p1, p2}, Lcom/ibm/icu/text/DateFormatSymbols;->initializeData(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/impl/CalendarData;)V

    .line 93
    const-string/jumbo v0, "isLeapMonth"

    invoke-virtual {p2, v0}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/ChineseDateFormatSymbols;->isLeapMonth:[Ljava/lang/String;

    .line 94
    return-void
.end method
