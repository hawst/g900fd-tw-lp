.class Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;
.super Ljava/lang/Object;
.source "RuleBasedCollator.java"

# interfaces
.implements Lcom/ibm/icu/impl/Trie$DataManipulate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/RuleBasedCollator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DataManipulate"
.end annotation


# static fields
.field private static m_instance_:Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1496
    return-void
.end method

.method public static final getInstance()Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;
    .locals 1

    .prologue
    .line 1476
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;->m_instance_:Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;

    if-nez v0, :cond_0

    .line 1477
    new-instance v0, Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;

    invoke-direct {v0}, Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;->m_instance_:Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;

    .line 1479
    :cond_0
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;->m_instance_:Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;

    return-object v0
.end method


# virtual methods
.method public final getFoldingOffset(I)I
    .locals 2
    .param p1, "ce"    # I

    .prologue
    .line 1465
    invoke-static {p1}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 1466
    const v0, 0xffffff

    and-int/2addr v0, p1

    .line 1468
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
