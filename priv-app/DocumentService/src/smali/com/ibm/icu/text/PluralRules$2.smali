.class Lcom/ibm/icu/text/PluralRules$2;
.super Ljava/lang/Object;
.source "PluralRules.java"

# interfaces
.implements Lcom/ibm/icu/text/PluralRules$Rule;


# static fields
.field private static final serialVersionUID:J = -0x4eca8753bada1885L


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public appliesTo(D)Z
    .locals 1
    .param p1, "n"    # D

    .prologue
    .line 172
    const/4 v0, 0x1

    return v0
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    const-string/jumbo v0, "other"

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    const-string/jumbo v0, "(other)"

    return-object v0
.end method

.method public updateRepeatLimit(I)I
    .locals 0
    .param p1, "limit"    # I

    .prologue
    .line 180
    return p1
.end method
