.class public final Lcom/ibm/icu/text/StringSearch;
.super Lcom/ibm/icu/text/SearchIterator;
.source "StringSearch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/StringSearch$Pattern;
    }
.end annotation


# static fields
.field private static final INITIAL_ARRAY_SIZE_:I = 0x100

.field private static final LAST_BYTE_MASK_:I = 0xff

.field private static final MAX_TABLE_SIZE_:I = 0x101

.field private static final SECOND_LAST_BYTE_SHIFT_:I = 0x8

.field private static final UNSIGNED_32BIT_MASK:J = 0xffffffffL


# instance fields
.field private m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

.field private m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

.field private m_ceMask_:I

.field private m_charBreakIter_:Lcom/ibm/icu/text/BreakIterator;

.field private m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

.field private m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

.field private m_isCanonicalMatch_:Z

.field private m_matchedIndex_:I

.field private m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

.field private m_textBeginOffset_:I

.field private m_textLimitOffset_:I

.field private m_utilBuffer_:[I

.field private m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    .line 273
    new-instance v1, Ljava/text/StringCharacterIterator;

    invoke-direct {v1, p2}, Ljava/text/StringCharacterIterator;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/ibm/icu/text/Collator;->getInstance()Lcom/ibm/icu/text/Collator;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/RuleBasedCollator;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v0, v2}, Lcom/ibm/icu/text/StringSearch;-><init>(Ljava/lang/String;Ljava/text/CharacterIterator;Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/BreakIterator;)V

    .line 276
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/text/CharacterIterator;Lcom/ibm/icu/text/RuleBasedCollator;)V
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/text/CharacterIterator;
    .param p3, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;

    .prologue
    .line 207
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/ibm/icu/text/StringSearch;-><init>(Ljava/lang/String;Ljava/text/CharacterIterator;Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/BreakIterator;)V

    .line 208
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/text/CharacterIterator;Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/BreakIterator;)V
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/text/CharacterIterator;
    .param p3, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p4, "breakiter"    # Lcom/ibm/icu/text/BreakIterator;

    .prologue
    .line 176
    invoke-direct {p0, p2, p4}, Lcom/ibm/icu/text/SearchIterator;-><init>(Ljava/text/CharacterIterator;Lcom/ibm/icu/text/BreakIterator;)V

    .line 782
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    .line 177
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v0}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    .line 178
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v0}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    .line 179
    iput-object p3, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    .line 180
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v0, p2}, Lcom/ibm/icu/text/RuleBasedCollator;->getCollationElementIterator(Ljava/text/CharacterIterator;)Lcom/ibm/icu/text/CollationElementIterator;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    .line 181
    const-string/jumbo v0, ""

    invoke-virtual {p3, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getCollationElementIterator(Ljava/lang/String;)Lcom/ibm/icu/text/CollationElementIterator;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    .line 182
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/StringSearch;->getMask(I)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/StringSearch;->m_ceMask_:I

    .line 183
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/StringSearch;->m_isCanonicalMatch_:Z

    .line 184
    new-instance v0, Lcom/ibm/icu/text/StringSearch$Pattern;

    invoke-direct {v0, p1}, Lcom/ibm/icu/text/StringSearch$Pattern;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    .line 185
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 186
    invoke-static {}, Lcom/ibm/icu/text/BreakIterator;->getCharacterInstance()Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_charBreakIter_:Lcom/ibm/icu/text/BreakIterator;

    .line 187
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_charBreakIter_:Lcom/ibm/icu/text/BreakIterator;

    invoke-virtual {v0, p2}, Lcom/ibm/icu/text/BreakIterator;->setText(Ljava/text/CharacterIterator;)V

    .line 188
    invoke-direct {p0}, Lcom/ibm/icu/text/StringSearch;->initialize()V

    .line 189
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/text/CharacterIterator;Lcom/ibm/icu/util/ULocale;)V
    .locals 2
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/text/CharacterIterator;
    .param p3, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 251
    invoke-static {p3}, Lcom/ibm/icu/text/Collator;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/Collator;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/RuleBasedCollator;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/ibm/icu/text/StringSearch;-><init>(Ljava/lang/String;Ljava/text/CharacterIterator;Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/BreakIterator;)V

    .line 253
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/text/CharacterIterator;Ljava/util/Locale;)V
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/text/CharacterIterator;
    .param p3, "locale"    # Ljava/util/Locale;

    .prologue
    .line 229
    invoke-static {p3}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/text/StringSearch;-><init>(Ljava/lang/String;Ljava/text/CharacterIterator;Lcom/ibm/icu/util/ULocale;)V

    .line 230
    return-void
.end method

.method private static final append(II[I)[I
    .locals 3
    .param p0, "offset"    # I
    .param p1, "value"    # I
    .param p2, "array"    # [I

    .prologue
    const/4 v2, 0x0

    .line 898
    array-length v1, p2

    if-lt p0, v1, :cond_0

    .line 899
    add-int/lit16 v1, p0, 0x100

    new-array v0, v1, [I

    .line 900
    .local v0, "temp":[I
    array-length v1, p2

    invoke-static {p2, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 901
    move-object p2, v0

    .line 903
    .end local v0    # "temp":[I
    :cond_0
    aput p1, p2, p0

    .line 904
    return-object p2
.end method

.method private checkBreakBoundary(I)I
    .locals 1
    .param p1, "end"    # I

    .prologue
    .line 3150
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_charBreakIter_:Lcom/ibm/icu/text/BreakIterator;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/BreakIterator;->isBoundary(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3151
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_charBreakIter_:Lcom/ibm/icu/text/BreakIterator;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/BreakIterator;->following(I)I

    move-result p1

    .line 3153
    :cond_0
    return p1
.end method

.method private final checkCollationMatch(Lcom/ibm/icu/text/CollationElementIterator;)Z
    .locals 4
    .param p1, "coleiter"    # Lcom/ibm/icu/text/CollationElementIterator;

    .prologue
    .line 1711
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v2, v3, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    .line 1712
    .local v2, "patternceindex":I
    const/4 v1, 0x0

    .line 1713
    .local v1, "offset":I
    :cond_0
    :goto_0
    if-lez v2, :cond_2

    .line 1714
    invoke-virtual {p1}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v0

    .line 1715
    .local v0, "ce":I
    if-eqz v0, :cond_0

    .line 1718
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v3, v3, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v3, v3, v1

    if-eq v0, v3, :cond_1

    .line 1719
    const/4 v3, 0x0

    .line 1724
    .end local v0    # "ce":I
    :goto_1
    return v3

    .line 1721
    .restart local v0    # "ce":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 1722
    add-int/lit8 v2, v2, -0x1

    .line 1723
    goto :goto_0

    .line 1724
    .end local v0    # "ce":I
    :cond_2
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private final checkExtraMatchAccents(II)Z
    .locals 11
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v8, 0x0

    .line 1236
    const/4 v5, 0x0

    .line 1237
    .local v5, "result":Z
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v9, v9, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasPrefixAccents_:Z

    if-eqz v9, :cond_4

    .line 1238
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v9, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1240
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v9}, Ljava/text/CharacterIterator;->next()C

    move-result v9

    invoke-static {v9}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1241
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v9}, Ljava/text/CharacterIterator;->next()C

    move-result v9

    invoke-static {v9}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v9

    if-nez v9, :cond_0

    .line 1242
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v9}, Ljava/text/CharacterIterator;->previous()C

    .line 1246
    :cond_0
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-static {v9, p1, p2}, Lcom/ibm/icu/text/StringSearch;->getString(Ljava/text/CharacterIterator;II)Ljava/lang/String;

    move-result-object v7

    .line 1247
    .local v7, "str":Ljava/lang/String;
    sget-object v9, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {v7, v9, v8}, Lcom/ibm/icu/text/Normalizer;->quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v9

    sget-object v10, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-ne v9, v10, :cond_4

    .line 1249
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/StringSearch;->getNextSafeOffset(II)I

    move-result v6

    .line 1250
    .local v6, "safeoffset":I
    if-eq v6, p2, :cond_1

    .line 1251
    add-int/lit8 v6, v6, 0x1

    .line 1253
    :cond_1
    sub-int v9, v6, p1

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v8}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 1255
    .local v1, "decomp":Ljava/lang/String;
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9, v1}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 1256
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v9, v9, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v2, v9, v8

    .line 1257
    .local v2, "firstce":I
    const/4 v3, 0x1

    .line 1258
    .local v3, "ignorable":Z
    const/4 v0, 0x0

    .line 1259
    .local v0, "ce":I
    const/4 v4, 0x0

    .line 1260
    .local v4, "offset":I
    :goto_0
    if-eq v0, v2, :cond_3

    .line 1261
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v4

    .line 1262
    if-eq v0, v2, :cond_2

    if-eqz v0, :cond_2

    .line 1264
    const/4 v3, 0x0

    .line 1266
    :cond_2
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v0

    .line 1267
    goto :goto_0

    .line 1268
    :cond_3
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9, v4}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 1269
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->previous()I

    .line 1270
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v4

    .line 1271
    if-nez v3, :cond_5

    invoke-static {v1, v4}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v9

    invoke-static {v9}, Lcom/ibm/icu/lang/UCharacter;->getCombiningClass(I)I

    move-result v9

    if-eqz v9, :cond_5

    const/4 v5, 0x1

    .line 1276
    .end local v0    # "ce":I
    .end local v1    # "decomp":Ljava/lang/String;
    .end local v2    # "firstce":I
    .end local v3    # "ignorable":Z
    .end local v4    # "offset":I
    .end local v6    # "safeoffset":I
    .end local v7    # "str":Ljava/lang/String;
    :cond_4
    :goto_1
    return v5

    .restart local v0    # "ce":I
    .restart local v1    # "decomp":Ljava/lang/String;
    .restart local v2    # "firstce":I
    .restart local v3    # "ignorable":Z
    .restart local v4    # "offset":I
    .restart local v6    # "safeoffset":I
    .restart local v7    # "str":Ljava/lang/String;
    :cond_5
    move v5, v8

    .line 1271
    goto :goto_1
.end method

.method private final checkIdentical(II)Z
    .locals 5
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v4, 0x0

    .line 1428
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v2}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v2

    const/16 v3, 0xf

    if-eq v2, v3, :cond_0

    .line 1429
    const/4 v2, 0x1

    .line 1442
    :goto_0
    return v2

    .line 1432
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    sub-int v3, p2, p1

    invoke-static {v2, p1, v3}, Lcom/ibm/icu/text/StringSearch;->getString(Ljava/text/CharacterIterator;II)Ljava/lang/String;

    move-result-object v1

    .line 1433
    .local v1, "textstr":Ljava/lang/String;
    sget-object v2, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {v1, v2, v4}, Lcom/ibm/icu/text/Normalizer;->quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v2

    sget-object v3, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-ne v2, v3, :cond_1

    .line 1435
    invoke-static {v1, v4}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 1437
    :cond_1
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v0, v2, Lcom/ibm/icu/text/StringSearch$Pattern;->targetText:Ljava/lang/String;

    .line 1438
    .local v0, "patternstr":Ljava/lang/String;
    sget-object v2, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {v0, v2, v4}, Lcom/ibm/icu/text/Normalizer;->quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v2

    sget-object v3, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-ne v2, v3, :cond_2

    .line 1440
    invoke-static {v0, v4}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1442
    :cond_2
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method

.method private checkNextCanonicalContractionMatch(II)Z
    .locals 12
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2050
    const/4 v6, 0x0

    .line 2051
    .local v6, "schar":C
    const/4 v2, 0x0

    .line 2052
    .local v2, "echar":C
    iget v10, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    if-ge p2, v10, :cond_0

    .line 2053
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v10, p2}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 2054
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v10}, Ljava/text/CharacterIterator;->current()C

    move-result v2

    .line 2056
    :cond_0
    iget v10, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    if-ge p1, v10, :cond_1

    .line 2057
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    add-int/lit8 v11, p1, 0x1

    invoke-interface {v10, v11}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 2058
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v10}, Ljava/text/CharacterIterator;->current()C

    move-result v6

    .line 2060
    :cond_1
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v10, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v10

    if-nez v10, :cond_2

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v10, v6}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 2061
    :cond_2
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v3, v10, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 2062
    .local v3, "expansion":I
    if-lez v3, :cond_4

    move v5, v8

    .line 2063
    .local v5, "hasExpansion":Z
    :goto_0
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10, p1}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 2064
    move v7, p1

    .line 2065
    .local v7, "temp":I
    :goto_1
    if-lez v3, :cond_5

    .line 2073
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    .line 2074
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v10

    if-eq v10, v7, :cond_3

    .line 2075
    move p1, v7

    .line 2076
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v7

    .line 2078
    :cond_3
    add-int/lit8 v3, v3, -0x1

    .line 2079
    goto :goto_1

    .end local v5    # "hasExpansion":Z
    .end local v7    # "temp":I
    :cond_4
    move v5, v9

    .line 2062
    goto :goto_0

    .line 2081
    .restart local v5    # "hasExpansion":Z
    .restart local v7    # "temp":I
    :cond_5
    const/4 v1, 0x0

    .line 2082
    .local v1, "count":I
    :cond_6
    :goto_2
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v10, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    if-ge v1, v10, :cond_a

    .line 2083
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v10

    invoke-direct {p0, v10}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v0

    .line 2086
    .local v0, "ce":I
    if-eqz v0, :cond_6

    .line 2089
    if-eqz v5, :cond_7

    if-nez v1, :cond_7

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v10

    if-eq v10, v7, :cond_7

    .line 2091
    move p1, v7

    .line 2092
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v7

    .line 2095
    :cond_7
    if-nez v1, :cond_8

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v10, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v10, v10, v9

    if-eq v0, v10, :cond_8

    .line 2099
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v10, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v4, v10, v9

    .line 2100
    .local v4, "expected":I
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-static {v10, p1}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/text/CharacterIterator;I)C

    move-result v10

    and-int/lit16 v10, v10, 0xff

    if-eqz v10, :cond_8

    .line 2101
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v10

    invoke-direct {p0, v10}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v0

    .line 2104
    :goto_3
    if-eq v0, v4, :cond_8

    const/4 v10, -0x1

    if-eq v0, v10, :cond_8

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v10

    if-gt v10, p2, :cond_8

    .line 2105
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v10

    invoke-direct {p0, v10}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v0

    .line 2106
    goto :goto_3

    .line 2109
    .end local v4    # "expected":I
    :cond_8
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v10, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v10, v10, v1

    if-eq v0, v10, :cond_9

    .line 2110
    add-int/lit8 p2, p2, 0x1

    .line 2111
    invoke-direct {p0, p2}, Lcom/ibm/icu/text/StringSearch;->getNextBaseOffset(I)I

    move-result p2

    .line 2112
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p1, v10, v9

    .line 2113
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p2, v10, v8

    .line 2121
    .end local v0    # "ce":I
    .end local v1    # "count":I
    .end local v3    # "expansion":I
    .end local v5    # "hasExpansion":Z
    .end local v7    # "temp":I
    :goto_4
    return v9

    .line 2116
    .restart local v0    # "ce":I
    .restart local v1    # "count":I
    .restart local v3    # "expansion":I
    .restart local v5    # "hasExpansion":Z
    .restart local v7    # "temp":I
    :cond_9
    add-int/lit8 v1, v1, 0x1

    .line 2117
    goto :goto_2

    .line 2119
    .end local v0    # "ce":I
    .end local v1    # "count":I
    .end local v3    # "expansion":I
    .end local v5    # "hasExpansion":Z
    .end local v7    # "temp":I
    :cond_a
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p1, v10, v9

    .line 2120
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p2, v9, v8

    move v9, v8

    .line 2121
    goto :goto_4
.end method

.method private checkNextCanonicalMatch(I)Z
    .locals 5
    .param p1, "textoffset"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2144
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v3, v3, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasSuffixAccents_:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v3, v3, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasPrefixAccents_:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-eqz v3, :cond_2

    .line 2148
    :cond_1
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/StringSearch;->getPreviousBaseOffset(I)I

    move-result v1

    iput v1, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 2149
    iget v1, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    sub-int v1, p1, v1

    iput v1, p0, Lcom/ibm/icu/text/StringSearch;->matchLength:I

    move v1, v2

    .line 2174
    :goto_0
    return v1

    .line 2153
    :cond_2
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v0

    .line 2154
    .local v0, "start":I
    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/StringSearch;->checkNextCanonicalContractionMatch(II)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2156
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    iget-object v4, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aget v2, v4, v2

    aput v2, v3, v1

    goto :goto_0

    .line 2159
    :cond_3
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aget v0, v3, v1

    .line 2160
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aget p1, v3, v2

    .line 2161
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/StringSearch;->getPreviousBaseOffset(I)I

    move-result v0

    .line 2163
    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/StringSearch;->checkRepeatedMatch(II)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/StringSearch;->isBreakUnit(II)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/StringSearch;->checkIdentical(II)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2166
    :cond_4
    add-int/lit8 p1, p1, 0x1

    .line 2167
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-direct {p0, v2, p1}, Lcom/ibm/icu/text/StringSearch;->getNextBaseOffset(Ljava/text/CharacterIterator;I)I

    move-result p1

    .line 2168
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p1, v2, v1

    goto :goto_0

    .line 2172
    :cond_5
    iput v0, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 2173
    sub-int v1, p1, v0

    iput v1, p0, Lcom/ibm/icu/text/StringSearch;->matchLength:I

    move v1, v2

    .line 2174
    goto :goto_0
.end method

.method private final checkNextExactContractionMatch(II)Z
    .locals 11
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1482
    const/4 v3, 0x0

    .line 1483
    .local v3, "endchar":C
    iget v9, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    if-ge p2, v9, :cond_0

    .line 1484
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v9, p2}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1485
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v9}, Ljava/text/CharacterIterator;->current()C

    move-result v3

    .line 1487
    :cond_0
    const/4 v5, 0x0

    .line 1488
    .local v5, "poststartchar":C
    add-int/lit8 v9, p1, 0x1

    iget v10, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    if-ge v9, v10, :cond_1

    .line 1489
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    add-int/lit8 v10, p1, 0x1

    invoke-interface {v9, v10}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1490
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v9}, Ljava/text/CharacterIterator;->current()C

    move-result v5

    .line 1492
    :cond_1
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v9, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v9

    if-nez v9, :cond_2

    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v9, v5}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1495
    :cond_2
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v0, v9, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 1496
    .local v0, "bufferedCEOffset":I
    if-lez v0, :cond_4

    move v4, v7

    .line 1497
    .local v4, "hasBufferedCE":Z
    :goto_0
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9, p1}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 1498
    move v6, p1

    .line 1499
    .local v6, "temp":I
    :goto_1
    if-lez v0, :cond_5

    .line 1507
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    .line 1508
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v9

    if-eq v9, v6, :cond_3

    .line 1509
    move p1, v6

    .line 1510
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v6

    .line 1512
    :cond_3
    add-int/lit8 v0, v0, -0x1

    .line 1513
    goto :goto_1

    .end local v4    # "hasBufferedCE":Z
    .end local v6    # "temp":I
    :cond_4
    move v4, v8

    .line 1496
    goto :goto_0

    .line 1515
    .restart local v4    # "hasBufferedCE":Z
    .restart local v6    # "temp":I
    :cond_5
    const/4 v2, 0x0

    .line 1516
    .local v2, "count":I
    :cond_6
    :goto_2
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v9, v9, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    if-ge v2, v9, :cond_9

    .line 1517
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v1

    .line 1518
    .local v1, "ce":I
    if-eqz v1, :cond_6

    .line 1521
    if-eqz v4, :cond_7

    if-nez v2, :cond_7

    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v9

    if-eq v9, v6, :cond_7

    .line 1523
    move p1, v6

    .line 1524
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v6

    .line 1526
    :cond_7
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v9, v9, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v9, v9, v2

    if-eq v1, v9, :cond_8

    .line 1527
    add-int/lit8 p2, p2, 0x1

    .line 1528
    invoke-direct {p0, p2}, Lcom/ibm/icu/text/StringSearch;->getNextBaseOffset(I)I

    move-result p2

    .line 1529
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p1, v9, v8

    .line 1530
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p2, v9, v7

    .line 1538
    .end local v0    # "bufferedCEOffset":I
    .end local v1    # "ce":I
    .end local v2    # "count":I
    .end local v4    # "hasBufferedCE":Z
    .end local v6    # "temp":I
    :goto_3
    return v8

    .line 1533
    .restart local v0    # "bufferedCEOffset":I
    .restart local v1    # "ce":I
    .restart local v2    # "count":I
    .restart local v4    # "hasBufferedCE":Z
    .restart local v6    # "temp":I
    :cond_8
    add-int/lit8 v2, v2, 0x1

    .line 1534
    goto :goto_2

    .line 1536
    .end local v0    # "bufferedCEOffset":I
    .end local v1    # "ce":I
    .end local v2    # "count":I
    .end local v4    # "hasBufferedCE":Z
    .end local v6    # "temp":I
    :cond_9
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p1, v9, v8

    .line 1537
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p2, v8, v7

    move v8, v7

    .line 1538
    goto :goto_3
.end method

.method private final checkNextExactMatch(I)Z
    .locals 5
    .param p1, "textoffset"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1561
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v0

    .line 1562
    .local v0, "start":I
    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/StringSearch;->checkNextExactContractionMatch(II)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1564
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    iget-object v4, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aget v2, v4, v2

    aput v2, v3, v1

    .line 1589
    :goto_0
    return v1

    .line 1568
    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aget v0, v3, v1

    .line 1569
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aget p1, v3, v2

    .line 1571
    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/StringSearch;->isBreakUnit(II)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/StringSearch;->checkRepeatedMatch(II)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/StringSearch;->hasAccentsBeforeMatch(II)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/StringSearch;->checkIdentical(II)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/StringSearch;->hasAccentsAfterMatch(II)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1576
    :cond_1
    add-int/lit8 p1, p1, 0x1

    .line 1577
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/StringSearch;->getNextBaseOffset(I)I

    move-result p1

    .line 1578
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p1, v2, v1

    goto :goto_0

    .line 1582
    :cond_2
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v1

    if-nez v1, :cond_3

    .line 1583
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/StringSearch;->checkBreakBoundary(I)I

    move-result p1

    .line 1587
    :cond_3
    iput v0, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 1588
    sub-int v1, p1, v0

    iput v1, p0, Lcom/ibm/icu/text/StringSearch;->matchLength:I

    move v1, v2

    .line 1589
    goto :goto_0
.end method

.method private checkPreviousCanonicalContractionMatch(II)Z
    .locals 12
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2615
    move v7, p2

    .line 2618
    .local v7, "temp":I
    const/4 v2, 0x0

    .line 2619
    .local v2, "echar":C
    const/4 v6, 0x0

    .line 2620
    .local v6, "schar":C
    iget v10, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    if-ge p2, v10, :cond_0

    .line 2621
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v10, p2}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 2622
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v10}, Ljava/text/CharacterIterator;->current()C

    move-result v2

    .line 2624
    :cond_0
    add-int/lit8 v10, p1, 0x1

    iget v11, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    if-ge v10, v11, :cond_1

    .line 2625
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    add-int/lit8 v11, p1, 0x1

    invoke-interface {v10, v11}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 2626
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v10}, Ljava/text/CharacterIterator;->current()C

    move-result v6

    .line 2628
    :cond_1
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v10, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v10

    if-nez v10, :cond_2

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v10, v6}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 2629
    :cond_2
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v10, v10, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v11, v11, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    sub-int v3, v10, v11

    .line 2631
    .local v3, "expansion":I
    if-lez v3, :cond_4

    move v5, v8

    .line 2632
    .local v5, "hasExpansion":Z
    :goto_0
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10, p2}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 2633
    :goto_1
    if-lez v3, :cond_5

    .line 2641
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->previous()I

    .line 2642
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v10

    if-eq v10, v7, :cond_3

    .line 2643
    move p2, v7

    .line 2644
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v7

    .line 2646
    :cond_3
    add-int/lit8 v3, v3, -0x1

    .line 2647
    goto :goto_1

    .end local v5    # "hasExpansion":Z
    :cond_4
    move v5, v9

    .line 2631
    goto :goto_0

    .line 2649
    .restart local v5    # "hasExpansion":Z
    :cond_5
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v1, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    .line 2650
    .local v1, "count":I
    :cond_6
    :goto_2
    if-lez v1, :cond_b

    .line 2651
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->previous()I

    move-result v10

    invoke-direct {p0, v10}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v0

    .line 2654
    .local v0, "ce":I
    if-eqz v0, :cond_6

    .line 2657
    if-eqz v5, :cond_7

    if-nez v1, :cond_7

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v10

    if-eq v10, v7, :cond_7

    .line 2659
    move p2, v7

    .line 2660
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v7

    .line 2662
    :cond_7
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v10, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    if-ne v1, v10, :cond_9

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v10, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v11, v11, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    add-int/lit8 v11, v11, -0x1

    aget v10, v10, v11

    if-eq v0, v10, :cond_9

    .line 2666
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v10, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v11, v11, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    add-int/lit8 v11, v11, -0x1

    aget v4, v10, v11

    .line 2667
    .local v4, "expected":I
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v10, p2}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 2668
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v10}, Ljava/text/CharacterIterator;->previous()C

    move-result v10

    invoke-static {v10}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 2669
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v10}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v10

    iget v11, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-le v10, v11, :cond_8

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v10}, Ljava/text/CharacterIterator;->previous()C

    move-result v10

    invoke-static {v10}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v10

    if-nez v10, :cond_8

    .line 2671
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v10}, Ljava/text/CharacterIterator;->next()C

    .line 2674
    :cond_8
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v10}, Ljava/text/CharacterIterator;->getIndex()I

    move-result p2

    .line 2675
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-static {v10, p2}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/text/CharacterIterator;I)C

    move-result v10

    and-int/lit16 v10, v10, 0xff

    if-eqz v10, :cond_9

    .line 2676
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->previous()I

    move-result v10

    invoke-direct {p0, v10}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v0

    .line 2679
    :goto_3
    if-eq v0, v4, :cond_9

    const/4 v10, -0x1

    if-eq v0, v10, :cond_9

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v10

    if-gt v10, p1, :cond_9

    .line 2680
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Lcom/ibm/icu/text/CollationElementIterator;->previous()I

    move-result v10

    invoke-direct {p0, v10}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v0

    .line 2681
    goto :goto_3

    .line 2684
    .end local v4    # "expected":I
    :cond_9
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v10, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    add-int/lit8 v11, v1, -0x1

    aget v10, v10, v11

    if-eq v0, v10, :cond_a

    .line 2685
    add-int/lit8 p1, p1, -0x1

    .line 2686
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/StringSearch;->getPreviousBaseOffset(I)I

    move-result p1

    .line 2687
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p1, v10, v9

    .line 2688
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p2, v10, v8

    .line 2696
    .end local v0    # "ce":I
    .end local v1    # "count":I
    .end local v3    # "expansion":I
    .end local v5    # "hasExpansion":Z
    :goto_4
    return v9

    .line 2691
    .restart local v0    # "ce":I
    .restart local v1    # "count":I
    .restart local v3    # "expansion":I
    .restart local v5    # "hasExpansion":Z
    :cond_a
    add-int/lit8 v1, v1, -0x1

    .line 2692
    goto/16 :goto_2

    .line 2694
    .end local v0    # "ce":I
    .end local v1    # "count":I
    .end local v3    # "expansion":I
    .end local v5    # "hasExpansion":Z
    :cond_b
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p1, v10, v9

    .line 2695
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p2, v9, v8

    move v9, v8

    .line 2696
    goto :goto_4
.end method

.method private checkPreviousCanonicalMatch(I)Z
    .locals 4
    .param p1, "textoffset"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2719
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v3, v3, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasSuffixAccents_:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v3, v3, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasPrefixAccents_:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-eqz v3, :cond_3

    .line 2723
    :cond_1
    iput p1, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 2724
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/StringSearch;->getNextBaseOffset(I)I

    move-result v1

    sub-int/2addr v1, p1

    iput v1, p0, Lcom/ibm/icu/text/StringSearch;->matchLength:I

    move v1, v2

    .line 2749
    :cond_2
    :goto_0
    return v1

    .line 2729
    :cond_3
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v0

    .line 2730
    .local v0, "end":I
    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/StringSearch;->checkPreviousCanonicalContractionMatch(II)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2734
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aget p1, v3, v1

    .line 2735
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aget v0, v3, v2

    .line 2736
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/StringSearch;->getNextBaseOffset(I)I

    move-result v0

    .line 2738
    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/StringSearch;->checkRepeatedMatch(II)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/StringSearch;->isBreakUnit(II)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/StringSearch;->checkIdentical(II)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2741
    :cond_4
    add-int/lit8 p1, p1, -0x1

    .line 2742
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/StringSearch;->getPreviousBaseOffset(I)I

    move-result p1

    .line 2743
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p1, v2, v1

    goto :goto_0

    .line 2747
    :cond_5
    iput p1, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 2748
    sub-int v1, v0, p1

    iput v1, p0, Lcom/ibm/icu/text/StringSearch;->matchLength:I

    move v1, v2

    .line 2749
    goto :goto_0
.end method

.method private checkPreviousExactContractionMatch(II)Z
    .locals 11
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2230
    const/4 v2, 0x0

    .line 2231
    .local v2, "echar":C
    iget v9, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    if-ge p2, v9, :cond_0

    .line 2232
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v9, p2}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 2233
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v9}, Ljava/text/CharacterIterator;->current()C

    move-result v2

    .line 2235
    :cond_0
    const/4 v5, 0x0

    .line 2236
    .local v5, "schar":C
    add-int/lit8 v9, p1, 0x1

    iget v10, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    if-ge v9, v10, :cond_1

    .line 2237
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    add-int/lit8 v10, p1, 0x1

    invoke-interface {v9, v10}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 2238
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v9}, Ljava/text/CharacterIterator;->current()C

    move-result v5

    .line 2240
    :cond_1
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v9, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v9

    if-nez v9, :cond_2

    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v9, v5}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 2242
    :cond_2
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v9, v9, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v10, v10, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    sub-int v3, v9, v10

    .line 2244
    .local v3, "expansion":I
    if-lez v3, :cond_4

    move v4, v7

    .line 2245
    .local v4, "hasExpansion":Z
    :goto_0
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9, p2}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 2246
    move v6, p2

    .line 2247
    .local v6, "temp":I
    :goto_1
    if-lez v3, :cond_5

    .line 2255
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->previous()I

    .line 2256
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v9

    if-eq v9, v6, :cond_3

    .line 2257
    move p2, v6

    .line 2258
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v6

    .line 2260
    :cond_3
    add-int/lit8 v3, v3, -0x1

    .line 2261
    goto :goto_1

    .end local v4    # "hasExpansion":Z
    .end local v6    # "temp":I
    :cond_4
    move v4, v8

    .line 2244
    goto :goto_0

    .line 2263
    .restart local v4    # "hasExpansion":Z
    .restart local v6    # "temp":I
    :cond_5
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v1, v9, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    .line 2264
    .local v1, "count":I
    :cond_6
    :goto_2
    if-lez v1, :cond_9

    .line 2265
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->previous()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v0

    .line 2268
    .local v0, "ce":I
    if-eqz v0, :cond_6

    .line 2271
    if-eqz v4, :cond_7

    if-nez v1, :cond_7

    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v9

    if-eq v9, v6, :cond_7

    .line 2273
    move p2, v6

    .line 2274
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v9}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v6

    .line 2276
    :cond_7
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v9, v9, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    add-int/lit8 v10, v1, -0x1

    aget v9, v9, v10

    if-eq v0, v9, :cond_8

    .line 2277
    add-int/lit8 p1, p1, -0x1

    .line 2278
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-direct {p0, v9, p1}, Lcom/ibm/icu/text/StringSearch;->getPreviousBaseOffset(Ljava/text/CharacterIterator;I)I

    move-result p1

    .line 2279
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p1, v9, v8

    .line 2280
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p2, v9, v7

    .line 2288
    .end local v0    # "ce":I
    .end local v1    # "count":I
    .end local v3    # "expansion":I
    .end local v4    # "hasExpansion":Z
    .end local v6    # "temp":I
    :goto_3
    return v8

    .line 2283
    .restart local v0    # "ce":I
    .restart local v1    # "count":I
    .restart local v3    # "expansion":I
    .restart local v4    # "hasExpansion":Z
    .restart local v6    # "temp":I
    :cond_8
    add-int/lit8 v1, v1, -0x1

    .line 2284
    goto :goto_2

    .line 2286
    .end local v0    # "ce":I
    .end local v1    # "count":I
    .end local v3    # "expansion":I
    .end local v4    # "hasExpansion":Z
    .end local v6    # "temp":I
    :cond_9
    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p1, v9, v8

    .line 2287
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p2, v8, v7

    move v8, v7

    .line 2288
    goto :goto_3
.end method

.method private final checkPreviousExactMatch(I)Z
    .locals 4
    .param p1, "textoffset"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2310
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v0

    .line 2311
    .local v0, "end":I
    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/StringSearch;->checkPreviousExactContractionMatch(II)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2336
    :goto_0
    return v1

    .line 2314
    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aget p1, v3, v1

    .line 2315
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aget v0, v3, v2

    .line 2319
    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/StringSearch;->checkRepeatedMatch(II)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/StringSearch;->isBreakUnit(II)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/StringSearch;->hasAccentsBeforeMatch(II)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/StringSearch;->checkIdentical(II)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/StringSearch;->hasAccentsAfterMatch(II)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2324
    :cond_1
    add-int/lit8 p1, p1, -0x1

    .line 2325
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-direct {p0, v2, p1}, Lcom/ibm/icu/text/StringSearch;->getPreviousBaseOffset(Ljava/text/CharacterIterator;I)I

    move-result p1

    .line 2326
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aput p1, v2, v1

    goto :goto_0

    .line 2330
    :cond_2
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v1

    if-nez v1, :cond_3

    .line 2331
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/StringSearch;->checkBreakBoundary(I)I

    move-result v0

    .line 2334
    :cond_3
    iput p1, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 2335
    sub-int v1, v0, p1

    iput v1, p0, Lcom/ibm/icu/text/StringSearch;->matchLength:I

    move v1, v2

    .line 2336
    goto :goto_0
.end method

.method private final checkRepeatedMatch(II)Z
    .locals 6
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1453
    iget v4, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    .line 1464
    :cond_0
    :goto_0
    return v2

    .line 1456
    :cond_1
    add-int/lit8 v0, p2, -0x1

    .line 1457
    .local v0, "end":I
    iget v4, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    iget v5, p0, Lcom/ibm/icu/text/StringSearch;->matchLength:I

    add-int/2addr v4, v5

    add-int/lit8 v1, v4, -0x1

    .line 1458
    .local v1, "lastmatchend":I
    invoke-virtual {p0}, Lcom/ibm/icu/text/StringSearch;->isOverlapping()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1459
    iget v4, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    if-lt p1, v4, :cond_2

    if-le p1, v1, :cond_4

    :cond_2
    iget v4, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    if-lt v0, v4, :cond_3

    if-le v0, v1, :cond_4

    :cond_3
    iget v4, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    if-gt p1, v4, :cond_0

    if-lt v0, v1, :cond_0

    :cond_4
    move v2, v3

    goto :goto_0

    .line 1464
    :cond_5
    iget v4, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    if-gt p1, v4, :cond_6

    if-lt v0, v1, :cond_6

    :goto_1
    move v2, v3

    goto :goto_0

    :cond_6
    move v3, v2

    goto :goto_1
.end method

.method private doNextCanonicalMatch(I)Z
    .locals 14
    .param p1, "textoffset"    # I

    .prologue
    .line 1948
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v11}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v9

    .line 1949
    .local v9, "offset":I
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v11, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1950
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v11}, Ljava/text/CharacterIterator;->previous()C

    move-result v11

    invoke-static {v11}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v11

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v11}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v11

    iget v12, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-le v11, v12, :cond_0

    .line 1952
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v11}, Ljava/text/CharacterIterator;->previous()C

    move-result v11

    invoke-static {v11}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v11

    if-nez v11, :cond_0

    .line 1953
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v11}, Ljava/text/CharacterIterator;->next()C

    .line 1956
    :cond_0
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v12}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v12

    invoke-static {v11, v12}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/text/CharacterIterator;I)C

    move-result v11

    and-int/lit16 v11, v11, 0xff

    if-nez v11, :cond_2

    .line 1957
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v11, v11, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasPrefixAccents_:Z

    if-eqz v11, :cond_1

    .line 1958
    invoke-direct {p0, v9, p1}, Lcom/ibm/icu/text/StringSearch;->doNextCanonicalPrefixMatch(II)I

    move-result v9

    .line 1959
    const/4 v11, -0x1

    if-eq v9, v11, :cond_1

    .line 1960
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v11, v9}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 1961
    const/4 v11, 0x1

    .line 2013
    :goto_0
    return v11

    .line 1964
    :cond_1
    const/4 v11, 0x0

    goto :goto_0

    .line 1967
    :cond_2
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v11, v11, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasSuffixAccents_:Z

    if-nez v11, :cond_3

    .line 1968
    const/4 v11, 0x0

    goto :goto_0

    .line 1971
    :cond_3
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1973
    .local v0, "accents":Ljava/lang/StringBuffer;
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-direct {p0, v11, p1}, Lcom/ibm/icu/text/StringSearch;->getPreviousBaseOffset(Ljava/text/CharacterIterator;I)I

    move-result v3

    .line 1975
    .local v3, "baseoffset":I
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    sub-int v12, p1, v3

    invoke-static {v11, v3, v12}, Lcom/ibm/icu/text/StringSearch;->getString(Ljava/text/CharacterIterator;II)Ljava/lang/String;

    move-result-object v2

    .line 1977
    .local v2, "accentstr":Ljava/lang/String;
    sget-object v11, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    const/4 v12, 0x0

    invoke-static {v2, v11, v12}, Lcom/ibm/icu/text/Normalizer;->quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v11

    sget-object v12, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-ne v11, v12, :cond_4

    .line 1979
    const/4 v11, 0x0

    invoke-static {v2, v11}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 1981
    :cond_4
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1984
    const/16 v11, 0x100

    new-array v1, v11, [I

    .line 1985
    .local v1, "accentsindex":[I
    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/StringSearch;->getUnblockedAccentIndex(Ljava/lang/StringBuffer;[I)I

    move-result v10

    .line 1988
    .local v10, "size":I
    const/4 v11, 0x2

    add-int/lit8 v12, v10, -0x1

    shl-int/2addr v11, v12

    add-int/lit8 v4, v11, -0x1

    .line 1989
    .local v4, "count":I
    :goto_1
    if-lez v4, :cond_9

    .line 1990
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 1993
    const/4 v7, 0x0

    .local v7, "k":I
    :goto_2
    const/4 v11, 0x0

    aget v11, v1, v11

    if-ge v7, v11, :cond_5

    .line 1994
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1993
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 1998
    :cond_5
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3
    add-int/lit8 v11, v10, -0x1

    if-gt v5, v11, :cond_7

    .line 1999
    const/4 v11, 0x1

    sub-int v12, v10, v5

    add-int/lit8 v12, v12, -0x1

    shl-int v8, v11, v12

    .line 2000
    .local v8, "mask":I
    and-int v11, v4, v8

    if-eqz v11, :cond_6

    .line 2001
    aget v6, v1, v5

    .local v6, "j":I
    :goto_4
    add-int/lit8 v11, v5, 0x1

    aget v11, v1, v11

    if-ge v6, v11, :cond_6

    .line 2003
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2002
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 1998
    .end local v6    # "j":I
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 2007
    .end local v8    # "mask":I
    :cond_7
    invoke-direct {p0, v3}, Lcom/ibm/icu/text/StringSearch;->doNextCanonicalSuffixMatch(I)I

    move-result v9

    .line 2008
    const/4 v11, -0x1

    if-eq v9, v11, :cond_8

    .line 2009
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 2011
    :cond_8
    add-int/lit8 v4, v4, -0x1

    .line 2012
    goto :goto_1

    .line 2013
    .end local v5    # "i":I
    .end local v7    # "k":I
    :cond_9
    const/4 v11, 0x0

    goto/16 :goto_0
.end method

.method private doNextCanonicalPrefixMatch(II)I
    .locals 15
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 1747
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    move/from16 v0, p1

    invoke-static {v12, v0}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/text/CharacterIterator;I)C

    move-result v12

    and-int/lit16 v12, v12, 0xff

    if-nez v12, :cond_0

    .line 1749
    const/4 v12, -0x1

    .line 1799
    :goto_0
    return v12

    .line 1752
    :cond_0
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v12}, Ljava/text/CharacterIterator;->getIndex()I

    move-result p1

    .line 1753
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    move/from16 v0, p1

    invoke-direct {p0, v12, v0}, Lcom/ibm/icu/text/StringSearch;->getNextBaseOffset(Ljava/text/CharacterIterator;I)I

    move-result v11

    .line 1754
    .local v11, "offset":I
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/text/StringSearch;->getPreviousBaseOffset(I)I

    move-result p1

    .line 1756
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1757
    .local v1, "accents":Ljava/lang/StringBuffer;
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    sub-int v13, v11, p1

    move/from16 v0, p1

    invoke-static {v12, v0, v13}, Lcom/ibm/icu/text/StringSearch;->getString(Ljava/text/CharacterIterator;II)Ljava/lang/String;

    move-result-object v4

    .line 1759
    .local v4, "accentstr":Ljava/lang/String;
    sget-object v12, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    const/4 v13, 0x0

    invoke-static {v4, v12, v13}, Lcom/ibm/icu/text/Normalizer;->quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v12

    sget-object v13, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-ne v12, v13, :cond_1

    .line 1761
    const/4 v12, 0x0

    invoke-static {v4, v12}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 1763
    :cond_1
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1765
    const/16 v12, 0x100

    new-array v2, v12, [I

    .line 1766
    .local v2, "accentsindex":[I
    invoke-direct {p0, v1, v2}, Lcom/ibm/icu/text/StringSearch;->getUnblockedAccentIndex(Ljava/lang/StringBuffer;[I)I

    move-result v3

    .line 1767
    .local v3, "accentsize":I
    const/4 v12, 0x2

    add-int/lit8 v13, v3, -0x1

    shl-int/2addr v12, v13

    add-int/lit8 v5, v12, -0x1

    .line 1768
    .local v5, "count":I
    :goto_1
    if-lez v5, :cond_6

    .line 1770
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->length()I

    move-result v14

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 1772
    const/4 v8, 0x0

    .line 1773
    .local v8, "k":I
    :goto_2
    const/4 v12, 0x0

    aget v12, v2, v12

    if-ge v8, v12, :cond_2

    .line 1774
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1773
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 1778
    :cond_2
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_3
    add-int/lit8 v12, v3, -0x1

    if-gt v6, v12, :cond_4

    .line 1779
    const/4 v12, 0x1

    sub-int v13, v3, v6

    add-int/lit8 v13, v13, -0x1

    shl-int v9, v12, v13

    .line 1780
    .local v9, "mask":I
    and-int v12, v5, v9

    if-eqz v12, :cond_3

    .line 1781
    aget v7, v2, v6

    .local v7, "j":I
    :goto_4
    add-int/lit8 v12, v6, 0x1

    aget v12, v2, v12

    if-ge v7, v12, :cond_3

    .line 1783
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1782
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 1778
    .end local v7    # "j":I
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1787
    .end local v9    # "mask":I
    :cond_4
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    iget-object v13, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    iget-object v14, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    move/from16 v0, p2

    invoke-static {v12, v13, v11, v0, v14}, Lcom/ibm/icu/text/StringSearch;->merge(Ljava/lang/StringBuffer;Ljava/text/CharacterIterator;IILjava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v10

    .line 1793
    .local v10, "match":Ljava/lang/StringBuffer;
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 1794
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-direct {p0, v12}, Lcom/ibm/icu/text/StringSearch;->checkCollationMatch(Lcom/ibm/icu/text/CollationElementIterator;)Z

    move-result v12

    if-eqz v12, :cond_5

    move/from16 v12, p1

    .line 1795
    goto/16 :goto_0

    .line 1797
    :cond_5
    add-int/lit8 v5, v5, -0x1

    .line 1798
    goto :goto_1

    .line 1799
    .end local v6    # "i":I
    .end local v8    # "k":I
    .end local v10    # "match":Ljava/lang/StringBuffer;
    :cond_6
    const/4 v12, -0x1

    goto/16 :goto_0
.end method

.method private doNextCanonicalSuffixMatch(I)I
    .locals 13
    .param p1, "textoffset"    # I

    .prologue
    .line 1840
    const/4 v6, 0x0

    .line 1842
    .local v6, "safelength":I
    iget v7, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    .line 1844
    .local v7, "safeoffset":I
    iget v10, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-eq p1, v10, :cond_2

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v10

    if-lez v10, :cond_2

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v11

    invoke-virtual {v10, v11}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1847
    iget v10, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    invoke-direct {p0, v10, p1}, Lcom/ibm/icu/text/StringSearch;->getPreviousSafeOffset(II)I

    move-result v7

    .line 1849
    sub-int v6, p1, v7

    .line 1850
    const/4 v10, 0x0

    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    invoke-static {v10, v11, v7, p1, v12}, Lcom/ibm/icu/text/StringSearch;->merge(Ljava/lang/StringBuffer;Ljava/text/CharacterIterator;IILjava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v8

    .line 1858
    .local v8, "safetext":Ljava/lang/StringBuffer;
    :goto_0
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    .line 1859
    .local v1, "coleiter":Lcom/ibm/icu/text/CollationElementIterator;
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 1862
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v10, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    add-int/lit8 v0, v10, -0x1

    .line 1863
    .local v0, "ceindex":I
    const/4 v3, 0x1

    .line 1865
    .local v3, "isSafe":Z
    :cond_0
    :goto_1
    if-ltz v0, :cond_9

    .line 1866
    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationElementIterator;->previous()I

    move-result v9

    .line 1867
    .local v9, "textce":I
    const/4 v10, -0x1

    if-ne v9, v10, :cond_5

    .line 1869
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    if-ne v1, v10, :cond_3

    .line 1870
    const/4 v5, -0x1

    .line 1925
    .end local v9    # "textce":I
    :cond_1
    :goto_2
    return v5

    .line 1854
    .end local v0    # "ceindex":I
    .end local v1    # "coleiter":Lcom/ibm/icu/text/CollationElementIterator;
    .end local v3    # "isSafe":Z
    .end local v8    # "safetext":Ljava/lang/StringBuffer;
    :cond_2
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    .restart local v8    # "safetext":Ljava/lang/StringBuffer;
    goto :goto_0

    .line 1872
    .restart local v0    # "ceindex":I
    .restart local v1    # "coleiter":Lcom/ibm/icu/text/CollationElementIterator;
    .restart local v3    # "isSafe":Z
    .restart local v9    # "textce":I
    :cond_3
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    .line 1873
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    if-eq v8, v10, :cond_4

    .line 1874
    const/4 v10, 0x0

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->length()I

    move-result v11

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 1876
    :cond_4
    invoke-virtual {v1, v7}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 1878
    const/4 v3, 0x0

    .line 1879
    goto :goto_1

    .line 1881
    :cond_5
    invoke-direct {p0, v9}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v9

    .line 1882
    if-eqz v9, :cond_8

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v10, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v10, v10, v0

    if-eq v9, v10, :cond_8

    .line 1885
    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v2

    .line 1886
    .local v2, "failedoffset":I
    if-eqz v3, :cond_6

    if-lt v2, v6, :cond_6

    .line 1888
    const/4 v5, -0x1

    goto :goto_2

    .line 1891
    :cond_6
    if-eqz v3, :cond_7

    .line 1892
    add-int/2addr v2, v7

    .line 1896
    :cond_7
    invoke-direct {p0, v2, p1}, Lcom/ibm/icu/text/StringSearch;->doNextCanonicalPrefixMatch(II)I

    move-result v5

    .line 1898
    .local v5, "result":I
    const/4 v10, -0x1

    if-eq v5, v10, :cond_1

    .line 1900
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10, v5}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    goto :goto_2

    .line 1905
    .end local v2    # "failedoffset":I
    .end local v5    # "result":I
    :cond_8
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v10, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v10, v10, v0

    if-ne v9, v10, :cond_0

    .line 1906
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 1910
    .end local v9    # "textce":I
    :cond_9
    if-eqz v3, :cond_b

    .line 1911
    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v5

    .line 1913
    .restart local v5    # "result":I
    iget v4, v1, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 1914
    .local v4, "leftoverces":I
    if-lt v5, v6, :cond_a

    .line 1915
    move v5, p1

    .line 1920
    :goto_3
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10, v5}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 1921
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iput v4, v10, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    goto :goto_2

    .line 1918
    :cond_a
    add-int/2addr v5, v7

    goto :goto_3

    .line 1925
    .end local v4    # "leftoverces":I
    .end local v5    # "result":I
    :cond_b
    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v5

    goto :goto_2
.end method

.method private doPreviousCanonicalMatch(I)Z
    .locals 14
    .param p1, "textoffset"    # I

    .prologue
    .line 2544
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v11}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v8

    .line 2545
    .local v8, "offset":I
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-static {v11, p1}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/text/CharacterIterator;I)C

    move-result v11

    shr-int/lit8 v11, v11, 0x8

    if-nez v11, :cond_1

    .line 2546
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v11, v11, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasSuffixAccents_:Z

    if-eqz v11, :cond_0

    .line 2547
    invoke-direct {p0, p1, v8}, Lcom/ibm/icu/text/StringSearch;->doPreviousCanonicalSuffixMatch(II)I

    move-result v8

    .line 2548
    const/4 v11, -0x1

    if-eq v8, v11, :cond_0

    .line 2549
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v11, v8}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 2550
    const/4 v11, 0x1

    .line 2602
    :goto_0
    return v11

    .line 2553
    :cond_0
    const/4 v11, 0x0

    goto :goto_0

    .line 2556
    :cond_1
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v11, v11, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasPrefixAccents_:Z

    if-nez v11, :cond_2

    .line 2557
    const/4 v11, 0x0

    goto :goto_0

    .line 2560
    :cond_2
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 2562
    .local v0, "accents":Ljava/lang/StringBuffer;
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-direct {p0, v11, p1}, Lcom/ibm/icu/text/StringSearch;->getNextBaseOffset(Ljava/text/CharacterIterator;I)I

    move-result v2

    .line 2564
    .local v2, "baseoffset":I
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    sub-int v12, v2, p1

    invoke-static {v11, p1, v12}, Lcom/ibm/icu/text/StringSearch;->getString(Ljava/text/CharacterIterator;II)Ljava/lang/String;

    move-result-object v10

    .line 2566
    .local v10, "textstr":Ljava/lang/String;
    sget-object v11, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Lcom/ibm/icu/text/Normalizer;->quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v11

    sget-object v12, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-ne v11, v12, :cond_3

    .line 2568
    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v10

    .line 2570
    :cond_3
    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2573
    const/16 v11, 0x100

    new-array v1, v11, [I

    .line 2574
    .local v1, "accentsindex":[I
    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/StringSearch;->getUnblockedAccentIndex(Ljava/lang/StringBuffer;[I)I

    move-result v9

    .line 2577
    .local v9, "size":I
    const/4 v11, 0x2

    add-int/lit8 v12, v9, -0x1

    shl-int/2addr v11, v12

    add-int/lit8 v3, v11, -0x1

    .line 2578
    .local v3, "count":I
    :goto_1
    if-lez v3, :cond_8

    .line 2579
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 2582
    const/4 v6, 0x0

    .local v6, "k":I
    :goto_2
    const/4 v11, 0x0

    aget v11, v1, v11

    if-ge v6, v11, :cond_4

    .line 2583
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2582
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 2587
    :cond_4
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    add-int/lit8 v11, v9, -0x1

    if-gt v4, v11, :cond_6

    .line 2588
    const/4 v11, 0x1

    sub-int v12, v9, v4

    add-int/lit8 v12, v12, -0x1

    shl-int v7, v11, v12

    .line 2589
    .local v7, "mask":I
    and-int v11, v3, v7

    if-eqz v11, :cond_5

    .line 2590
    aget v5, v1, v4

    .local v5, "j":I
    :goto_4
    add-int/lit8 v11, v4, 0x1

    aget v11, v1, v11

    if-ge v5, v11, :cond_5

    .line 2592
    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2591
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 2587
    .end local v5    # "j":I
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 2596
    .end local v7    # "mask":I
    :cond_6
    invoke-direct {p0, v2}, Lcom/ibm/icu/text/StringSearch;->doPreviousCanonicalPrefixMatch(I)I

    move-result v8

    .line 2597
    const/4 v11, -0x1

    if-eq v8, v11, :cond_7

    .line 2598
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 2600
    :cond_7
    add-int/lit8 v3, v3, -0x1

    .line 2601
    goto :goto_1

    .line 2602
    .end local v4    # "i":I
    .end local v6    # "k":I
    :cond_8
    const/4 v11, 0x0

    goto/16 :goto_0
.end method

.method private doPreviousCanonicalPrefixMatch(I)I
    .locals 13
    .param p1, "textoffset"    # I

    .prologue
    .line 2433
    move v7, p1

    .line 2435
    .local v7, "safeoffset":I
    iget v10, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-le p1, v10, :cond_2

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v11

    invoke-virtual {v10, v11}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 2438
    iget v10, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    invoke-direct {p0, p1, v10}, Lcom/ibm/icu/text/StringSearch;->getNextSafeOffset(II)I

    move-result v7

    .line 2440
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    const/4 v12, 0x0

    invoke-static {v10, v11, p1, v7, v12}, Lcom/ibm/icu/text/StringSearch;->merge(Ljava/lang/StringBuffer;Ljava/text/CharacterIterator;IILjava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v8

    .line 2448
    .local v8, "safetext":Ljava/lang/StringBuffer;
    :goto_0
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    .line 2449
    .local v1, "coleiter":Lcom/ibm/icu/text/CollationElementIterator;
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 2452
    const/4 v0, 0x0

    .line 2453
    .local v0, "ceindex":I
    const/4 v3, 0x1

    .line 2454
    .local v3, "isSafe":Z
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    .line 2456
    .local v5, "prefixlength":I
    :cond_0
    :goto_1
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v10, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    if-ge v0, v10, :cond_9

    .line 2457
    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v9

    .line 2458
    .local v9, "textce":I
    const/4 v10, -0x1

    if-ne v9, v10, :cond_5

    .line 2460
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    if-ne v1, v10, :cond_3

    .line 2461
    const/4 v6, -0x1

    .line 2521
    .end local v9    # "textce":I
    :cond_1
    :goto_2
    return v6

    .line 2444
    .end local v0    # "ceindex":I
    .end local v1    # "coleiter":Lcom/ibm/icu/text/CollationElementIterator;
    .end local v3    # "isSafe":Z
    .end local v5    # "prefixlength":I
    .end local v8    # "safetext":Ljava/lang/StringBuffer;
    :cond_2
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    .restart local v8    # "safetext":Ljava/lang/StringBuffer;
    goto :goto_0

    .line 2463
    .restart local v0    # "ceindex":I
    .restart local v1    # "coleiter":Lcom/ibm/icu/text/CollationElementIterator;
    .restart local v3    # "isSafe":Z
    .restart local v5    # "prefixlength":I
    .restart local v9    # "textce":I
    :cond_3
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    if-eq v8, v10, :cond_4

    .line 2464
    const/4 v10, 0x0

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->length()I

    move-result v11

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 2466
    :cond_4
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    .line 2467
    invoke-virtual {v1, v7}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 2469
    const/4 v3, 0x0

    .line 2470
    goto :goto_1

    .line 2472
    :cond_5
    invoke-direct {p0, v9}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v9

    .line 2473
    if-eqz v9, :cond_8

    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v10, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v10, v10, v0

    if-eq v9, v10, :cond_8

    .line 2476
    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v2

    .line 2477
    .local v2, "failedoffset":I
    if-eqz v3, :cond_6

    if-gt v2, v5, :cond_6

    .line 2479
    const/4 v6, -0x1

    goto :goto_2

    .line 2482
    :cond_6
    if-eqz v3, :cond_7

    .line 2483
    sub-int v2, v7, v2

    .line 2484
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    if-eq v8, v10, :cond_7

    .line 2485
    const/4 v10, 0x0

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->length()I

    move-result v11

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 2490
    :cond_7
    invoke-direct {p0, p1, v2}, Lcom/ibm/icu/text/StringSearch;->doPreviousCanonicalSuffixMatch(II)I

    move-result v6

    .line 2492
    .local v6, "result":I
    const/4 v10, -0x1

    if-eq v6, v10, :cond_1

    .line 2494
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10, v6}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    goto :goto_2

    .line 2499
    .end local v2    # "failedoffset":I
    .end local v6    # "result":I
    :cond_8
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v10, v10, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v10, v10, v0

    if-ne v9, v10, :cond_0

    .line 2500
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2504
    .end local v9    # "textce":I
    :cond_9
    if-eqz v3, :cond_b

    .line 2505
    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v6

    .line 2507
    .restart local v6    # "result":I
    iget v10, v1, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    iget v11, v1, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    sub-int v4, v10, v11

    .line 2509
    .local v4, "leftoverces":I
    if-gt v6, v5, :cond_a

    .line 2510
    move v6, p1

    .line 2515
    :goto_3
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10, v6}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 2516
    iget-object v10, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v11, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v11, v11, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    sub-int/2addr v11, v4

    iput v11, v10, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    goto :goto_2

    .line 2513
    :cond_a
    sub-int v10, v7, v6

    add-int v6, p1, v10

    goto :goto_3

    .line 2521
    .end local v4    # "leftoverces":I
    .end local v6    # "result":I
    :cond_b
    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v6

    goto :goto_2
.end method

.method private doPreviousCanonicalSuffixMatch(II)I
    .locals 15
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 2358
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    move/from16 v0, p2

    invoke-interface {v12, v0}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 2359
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v12}, Ljava/text/CharacterIterator;->previous()C

    move-result v12

    invoke-static {v12}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v12

    if-eqz v12, :cond_0

    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v12}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v12

    iget v13, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-le v12, v13, :cond_0

    .line 2361
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v12}, Ljava/text/CharacterIterator;->previous()C

    move-result v12

    invoke-static {v12}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v12

    if-nez v12, :cond_0

    .line 2362
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v12}, Ljava/text/CharacterIterator;->next()C

    .line 2365
    :cond_0
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    iget-object v13, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v13}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v13

    invoke-static {v12, v13}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/text/CharacterIterator;I)C

    move-result v12

    and-int/lit16 v12, v12, 0xff

    if-nez v12, :cond_1

    .line 2367
    const/4 v12, -0x1

    .line 2413
    :goto_0
    return v12

    .line 2369
    :cond_1
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    move/from16 v0, p2

    invoke-direct {p0, v12, v0}, Lcom/ibm/icu/text/StringSearch;->getNextBaseOffset(Ljava/text/CharacterIterator;I)I

    move-result p2

    .line 2371
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 2372
    .local v1, "accents":Ljava/lang/StringBuffer;
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    move/from16 v0, p2

    invoke-direct {p0, v12, v0}, Lcom/ibm/icu/text/StringSearch;->getPreviousBaseOffset(Ljava/text/CharacterIterator;I)I

    move-result v11

    .line 2374
    .local v11, "offset":I
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    sub-int v13, p2, v11

    invoke-static {v12, v11, v13}, Lcom/ibm/icu/text/StringSearch;->getString(Ljava/text/CharacterIterator;II)Ljava/lang/String;

    move-result-object v4

    .line 2375
    .local v4, "accentstr":Ljava/lang/String;
    sget-object v12, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    const/4 v13, 0x0

    invoke-static {v4, v12, v13}, Lcom/ibm/icu/text/Normalizer;->quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v12

    sget-object v13, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-ne v12, v13, :cond_2

    .line 2377
    const/4 v12, 0x0

    invoke-static {v4, v12}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 2379
    :cond_2
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2381
    const/16 v12, 0x100

    new-array v2, v12, [I

    .line 2382
    .local v2, "accentsindex":[I
    invoke-direct {p0, v1, v2}, Lcom/ibm/icu/text/StringSearch;->getUnblockedAccentIndex(Ljava/lang/StringBuffer;[I)I

    move-result v3

    .line 2383
    .local v3, "accentsize":I
    const/4 v12, 0x2

    add-int/lit8 v13, v3, -0x1

    shl-int/2addr v12, v13

    add-int/lit8 v5, v12, -0x1

    .line 2384
    .local v5, "count":I
    :goto_1
    if-lez v5, :cond_7

    .line 2385
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->length()I

    move-result v14

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 2388
    const/4 v8, 0x0

    .local v8, "k":I
    :goto_2
    const/4 v12, 0x0

    aget v12, v2, v12

    if-ge v8, v12, :cond_3

    .line 2389
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2388
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 2393
    :cond_3
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_3
    add-int/lit8 v12, v3, -0x1

    if-gt v6, v12, :cond_5

    .line 2394
    const/4 v12, 0x1

    sub-int v13, v3, v6

    add-int/lit8 v13, v13, -0x1

    shl-int v9, v12, v13

    .line 2395
    .local v9, "mask":I
    and-int v12, v5, v9

    if-eqz v12, :cond_4

    .line 2396
    aget v7, v2, v6

    .local v7, "j":I
    :goto_4
    add-int/lit8 v12, v6, 0x1

    aget v12, v2, v12

    if-ge v7, v12, :cond_4

    .line 2398
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2397
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 2393
    .end local v7    # "j":I
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 2402
    .end local v9    # "mask":I
    :cond_5
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    iget-object v13, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    iget-object v14, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    move/from16 v0, p1

    invoke-static {v12, v13, v0, v11, v14}, Lcom/ibm/icu/text/StringSearch;->merge(Ljava/lang/StringBuffer;Ljava/text/CharacterIterator;IILjava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v10

    .line 2407
    .local v10, "match":Ljava/lang/StringBuffer;
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 2408
    iget-object v12, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-direct {p0, v12}, Lcom/ibm/icu/text/StringSearch;->checkCollationMatch(Lcom/ibm/icu/text/CollationElementIterator;)Z

    move-result v12

    if-eqz v12, :cond_6

    move/from16 v12, p2

    .line 2409
    goto/16 :goto_0

    .line 2411
    :cond_6
    add-int/lit8 v5, v5, -0x1

    .line 2412
    goto :goto_1

    .line 2413
    .end local v6    # "i":I
    .end local v8    # "k":I
    .end local v10    # "match":Ljava/lang/StringBuffer;
    :cond_7
    const/4 v12, -0x1

    goto/16 :goto_0
.end method

.method private final getCE(I)I
    .locals 6
    .param p1, "ce"    # I

    .prologue
    const-wide v4, 0xffffffffL

    .line 867
    iget v0, p0, Lcom/ibm/icu/text/StringSearch;->m_ceMask_:I

    and-int/2addr p1, v0

    .line 869
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->isAlternateHandlingShifted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 875
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_variableTopValue_:I

    shl-int/lit8 v0, v0, 0x10

    int-to-long v0, v0

    and-long/2addr v0, v4

    int-to-long v2, p1

    and-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 876
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 877
    invoke-static {p1}, Lcom/ibm/icu/text/CollationElementIterator;->primaryOrder(I)I

    move-result p1

    .line 885
    :cond_0
    :goto_0
    return p1

    .line 880
    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private static final getFCD(Ljava/lang/String;I)C
    .locals 4
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "offset"    # I

    .prologue
    .line 841
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 842
    .local v0, "ch":C
    invoke-static {v0}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v1

    .line 844
    .local v1, "result":C
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v3, p1, 0x1

    if-eq v2, v3, :cond_0

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 846
    add-int/lit8 v2, p1, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 847
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 848
    invoke-static {v1, v0}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16FromSurrogatePair(CC)C

    move-result v1

    .line 853
    :cond_0
    :goto_0
    return v1

    .line 850
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static final getFCD(Ljava/text/CharacterIterator;I)C
    .locals 4
    .param p0, "str"    # Ljava/text/CharacterIterator;
    .param p1, "offset"    # I

    .prologue
    .line 816
    invoke-interface {p0, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 817
    invoke-interface {p0}, Ljava/text/CharacterIterator;->current()C

    move-result v0

    .line 818
    .local v0, "ch":C
    invoke-static {v0}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v1

    .line 820
    .local v1, "result":C
    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v2

    add-int/lit8 v3, p1, 0x1

    if-eq v2, v3, :cond_0

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 822
    invoke-interface {p0}, Ljava/text/CharacterIterator;->next()C

    move-result v0

    .line 823
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 824
    invoke-static {v1, v0}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16FromSurrogatePair(CC)C

    move-result v1

    .line 829
    :cond_0
    :goto_0
    return v1

    .line 826
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static final getMask(I)I
    .locals 1
    .param p0, "strength"    # I

    .prologue
    .line 3122
    packed-switch p0, :pswitch_data_0

    .line 3130
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 3125
    :pswitch_0
    const/high16 v0, -0x10000

    goto :goto_0

    .line 3127
    :pswitch_1
    const/16 v0, -0x100

    goto :goto_0

    .line 3122
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private final getNextBaseOffset(I)I
    .locals 2
    .param p1, "textoffset"    # I

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v0, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasSuffixAccents_:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    if-ge p1, v0, :cond_0

    .line 1144
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v0, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1145
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v0}, Ljava/text/CharacterIterator;->previous()C

    .line 1146
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v1}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v1

    invoke-static {v0, v1}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/text/CharacterIterator;I)C

    move-result v0

    and-int/lit16 v0, v0, 0xff

    if-eqz v0, :cond_0

    .line 1147
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/StringSearch;->getNextBaseOffset(Ljava/text/CharacterIterator;I)I

    move-result p1

    .line 1150
    .end local p1    # "textoffset":I
    :cond_0
    return p1
.end method

.method private final getNextBaseOffset(Ljava/text/CharacterIterator;I)I
    .locals 4
    .param p1, "text"    # Ljava/text/CharacterIterator;
    .param p2, "textoffset"    # I

    .prologue
    .line 1119
    invoke-interface {p1}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v2

    if-ge p2, v2, :cond_2

    .line 1120
    :goto_0
    invoke-interface {p1}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v2

    invoke-interface {p1}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 1121
    move v0, p2

    .line 1122
    .local v0, "result":I
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "textoffset":I
    .local v1, "textoffset":I
    invoke-static {p1, p2}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/text/CharacterIterator;I)C

    move-result v2

    shr-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_0

    move p2, v1

    .line 1129
    .end local v0    # "result":I
    .end local v1    # "textoffset":I
    .restart local p2    # "textoffset":I
    :goto_1
    return v0

    .end local p2    # "textoffset":I
    .restart local v0    # "result":I
    .restart local v1    # "textoffset":I
    :cond_0
    move p2, v1

    .line 1126
    .end local v1    # "textoffset":I
    .restart local p2    # "textoffset":I
    goto :goto_0

    .line 1127
    .end local v0    # "result":I
    :cond_1
    invoke-interface {p1}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v0

    goto :goto_1

    :cond_2
    move v0, p2

    .line 1129
    goto :goto_1
.end method

.method private final getNextSafeOffset(II)I
    .locals 3
    .param p1, "textoffset"    # I
    .param p2, "end"    # I

    .prologue
    .line 1201
    move v0, p1

    .line 1202
    .local v0, "result":I
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v1, v0}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1203
    :goto_0
    if-eq v0, p2, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->current()C

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1205
    add-int/lit8 v0, v0, 0x1

    .line 1206
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v1, v0}, Ljava/text/CharacterIterator;->setIndex(I)C

    goto :goto_0

    .line 1208
    :cond_0
    return v0
.end method

.method private final getPreviousBaseOffset(I)I
    .locals 2
    .param p1, "textoffset"    # I

    .prologue
    .line 2026
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v1, v1, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasPrefixAccents_:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-le p1, v1, :cond_0

    .line 2027
    move v0, p1

    .line 2028
    .local v0, "offset":I
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-static {v1, v0}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/text/CharacterIterator;I)C

    move-result v1

    shr-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_0

    .line 2029
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-direct {p0, v1, p1}, Lcom/ibm/icu/text/StringSearch;->getPreviousBaseOffset(Ljava/text/CharacterIterator;I)I

    move-result p1

    .line 2032
    .end local v0    # "offset":I
    .end local p1    # "textoffset":I
    :cond_0
    return p1
.end method

.method private final getPreviousBaseOffset(Ljava/text/CharacterIterator;I)I
    .locals 4
    .param p1, "text"    # Ljava/text/CharacterIterator;
    .param p2, "textoffset"    # I

    .prologue
    .line 1603
    iget v2, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-le p2, v2, :cond_4

    .line 1605
    :cond_0
    move v1, p2

    .line 1606
    .local v1, "result":I
    invoke-interface {p1, v1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1607
    invoke-interface {p1}, Ljava/text/CharacterIterator;->previous()C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1608
    invoke-interface {p1}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v2

    invoke-interface {p1}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v3

    if-eq v2, v3, :cond_1

    invoke-interface {p1}, Ljava/text/CharacterIterator;->previous()C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1610
    invoke-interface {p1}, Ljava/text/CharacterIterator;->next()C

    .line 1613
    :cond_1
    invoke-interface {p1}, Ljava/text/CharacterIterator;->getIndex()I

    move-result p2

    .line 1614
    invoke-static {p1, p2}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/text/CharacterIterator;I)C

    move-result v0

    .line 1615
    .local v0, "fcd":C
    shr-int/lit8 v2, v0, 0x8

    if-nez v2, :cond_3

    .line 1616
    and-int/lit16 v2, v0, 0xff

    if-eqz v2, :cond_2

    move v1, p2

    .line 1626
    .end local v0    # "fcd":C
    .end local v1    # "result":I
    :cond_2
    :goto_0
    return v1

    .line 1621
    .restart local v0    # "fcd":C
    .restart local v1    # "result":I
    :cond_3
    iget v2, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-ne p2, v2, :cond_0

    .line 1622
    iget v1, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    goto :goto_0

    .end local v0    # "fcd":C
    .end local v1    # "result":I
    :cond_4
    move v1, p2

    .line 1626
    goto :goto_0
.end method

.method private final getPreviousSafeOffset(II)I
    .locals 3
    .param p1, "start"    # I
    .param p2, "textoffset"    # I

    .prologue
    .line 1812
    move v0, p2

    .line 1813
    .local v0, "result":I
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v1, p2}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1814
    :goto_0
    if-lt v0, p1, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->previous()C

    move-result v2

    invoke-virtual {v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1815
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v1}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v0

    .line 1816
    goto :goto_0

    .line 1817
    :cond_0
    if-eq v0, p1, :cond_1

    .line 1819
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v1}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v0

    .line 1821
    :cond_1
    return v0
.end method

.method private static final getString(Ljava/text/CharacterIterator;II)Ljava/lang/String;
    .locals 4
    .param p0, "text"    # Ljava/text/CharacterIterator;
    .param p1, "start"    # I
    .param p2, "length"    # I

    .prologue
    .line 3104
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2, p2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 3105
    .local v2, "result":Ljava/lang/StringBuffer;
    invoke-interface {p0}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v1

    .line 3106
    .local v1, "offset":I
    invoke-interface {p0, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 3107
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 3108
    invoke-interface {p0}, Ljava/text/CharacterIterator;->current()C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3109
    invoke-interface {p0}, Ljava/text/CharacterIterator;->next()C

    .line 3107
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3111
    :cond_0
    invoke-interface {p0, v1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 3112
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private getUnblockedAccentIndex(Ljava/lang/StringBuffer;[I)I
    .locals 7
    .param p1, "accents"    # Ljava/lang/StringBuffer;
    .param p2, "accentsindex"    # [I

    .prologue
    .line 1640
    const/4 v2, 0x0

    .line 1641
    .local v2, "index":I
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    .line 1642
    .local v3, "length":I
    const/4 v0, 0x0

    .line 1643
    .local v0, "cclass":I
    const/4 v4, 0x0

    .line 1644
    .local v4, "result":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 1645
    invoke-static {p1, v2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/StringBuffer;I)I

    move-result v1

    .line 1646
    .local v1, "codepoint":I
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->getCombiningClass(I)I

    move-result v5

    .line 1647
    .local v5, "tempclass":I
    if-eq v5, v0, :cond_0

    .line 1648
    move v0, v5

    .line 1649
    aput v2, p2, v4

    .line 1650
    add-int/lit8 v4, v4, 0x1

    .line 1652
    :cond_0
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->isSupplementary(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1653
    add-int/lit8 v2, v2, 0x2

    .line 1654
    goto :goto_0

    .line 1656
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1659
    .end local v1    # "codepoint":I
    .end local v5    # "tempclass":I
    :cond_2
    aput v3, p2, v4

    .line 1660
    return v4
.end method

.method private handleNextCanonical(I)V
    .locals 11
    .param p1, "start"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v10, -0x1

    const/4 v6, 0x0

    .line 2841
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v8, v8, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasSuffixAccents_:Z

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v8, v8, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasPrefixAccents_:Z

    if-eqz v8, :cond_5

    :cond_0
    move v1, v7

    .line 2847
    .local v1, "hasPatternAccents":Z
    :goto_0
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v8, v8, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    invoke-direct {p0, p1, v10, v8}, Lcom/ibm/icu/text/StringSearch;->shiftForward(III)I

    move-result v5

    .line 2849
    .local v5, "textoffset":I
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v9

    invoke-virtual {v8, v6, v9}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 2850
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v9

    invoke-virtual {v8, v6, v9}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 2851
    const/4 v4, 0x0

    .line 2853
    .local v4, "targetce":I
    :goto_1
    iget v8, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    if-gt v5, v8, :cond_e

    .line 2855
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v8, v5}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 2856
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v8, v8, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    add-int/lit8 v3, v8, -0x1

    .line 2857
    .local v3, "patternceindex":I
    const/4 v0, 0x0

    .line 2858
    .local v0, "found":Z
    const/4 v2, -0x1

    .line 2864
    .local v2, "lastce":I
    :cond_1
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v8}, Lcom/ibm/icu/text/CollationElementIterator;->previous()I

    move-result v4

    .line 2865
    if-ne v4, v10, :cond_6

    .line 2866
    const/4 v0, 0x0

    .line 2885
    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    if-lez v3, :cond_3

    .line 2886
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v8}, Lcom/ibm/icu/text/CollationElementIterator;->previous()I

    move-result v4

    .line 2887
    if-ne v4, v10, :cond_a

    .line 2888
    const/4 v0, 0x0

    .line 2901
    :cond_3
    if-eqz v1, :cond_4

    if-nez v0, :cond_4

    .line 2902
    invoke-direct {p0, v5}, Lcom/ibm/icu/text/StringSearch;->doNextCanonicalMatch(I)Z

    move-result v0

    .line 2905
    :cond_4
    if-nez v0, :cond_c

    .line 2906
    invoke-direct {p0, v5, v2, v3}, Lcom/ibm/icu/text/StringSearch;->shiftForward(III)I

    move-result v5

    .line 2908
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v3, v8, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    .line 2909
    goto :goto_1

    .end local v0    # "found":Z
    .end local v1    # "hasPatternAccents":Z
    .end local v2    # "lastce":I
    .end local v3    # "patternceindex":I
    .end local v4    # "targetce":I
    .end local v5    # "textoffset":I
    :cond_5
    move v1, v6

    .line 2841
    goto :goto_0

    .line 2869
    .restart local v0    # "found":Z
    .restart local v1    # "hasPatternAccents":Z
    .restart local v2    # "lastce":I
    .restart local v3    # "patternceindex":I
    .restart local v4    # "targetce":I
    .restart local v5    # "textoffset":I
    :cond_6
    invoke-direct {p0, v4}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v4

    .line 2870
    if-eq v2, v10, :cond_7

    if-nez v2, :cond_8

    .line 2872
    :cond_7
    move v2, v4

    .line 2874
    :cond_8
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v8, v8, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v8, v8, v3

    if-ne v4, v8, :cond_9

    .line 2876
    const/4 v0, 0x1

    .line 2877
    goto :goto_2

    .line 2879
    :cond_9
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v8, v8, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    if-gtz v8, :cond_1

    .line 2880
    const/4 v0, 0x0

    .line 2881
    goto :goto_2

    .line 2891
    :cond_a
    invoke-direct {p0, v4}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v4

    .line 2892
    if-eqz v4, :cond_2

    .line 2896
    add-int/lit8 v3, v3, -0x1

    .line 2897
    if-eqz v0, :cond_b

    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v8, v8, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v8, v8, v3

    if-ne v4, v8, :cond_b

    move v0, v7

    .line 2898
    :goto_3
    goto :goto_2

    :cond_b
    move v0, v6

    .line 2897
    goto :goto_3

    .line 2912
    :cond_c
    invoke-direct {p0, v5}, Lcom/ibm/icu/text/StringSearch;->checkNextCanonicalMatch(I)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 2918
    .end local v0    # "found":Z
    .end local v2    # "lastce":I
    .end local v3    # "patternceindex":I
    :goto_4
    return-void

    .line 2915
    .restart local v0    # "found":Z
    .restart local v2    # "lastce":I
    .restart local v3    # "patternceindex":I
    :cond_d
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aget v5, v8, v6

    .line 2916
    goto :goto_1

    .line 2917
    .end local v0    # "found":Z
    .end local v2    # "lastce":I
    .end local v3    # "patternceindex":I
    :cond_e
    invoke-direct {p0}, Lcom/ibm/icu/text/StringSearch;->setMatchNotFound()V

    goto :goto_4
.end method

.method private handleNextExact(I)V
    .locals 8
    .param p1, "start"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v7, -0x1

    .line 2759
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v6, v6, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    invoke-direct {p0, p1, v7, v6}, Lcom/ibm/icu/text/StringSearch;->shiftForward(III)I

    move-result v4

    .line 2762
    .local v4, "textoffset":I
    const/4 v3, 0x0

    .line 2763
    .local v3, "targetce":I
    :goto_0
    iget v6, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    if-gt v4, v6, :cond_c

    .line 2764
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6, v4}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 2765
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v6, v6, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    add-int/lit8 v2, v6, -0x1

    .line 2766
    .local v2, "patternceindex":I
    const/4 v0, 0x0

    .line 2767
    .local v0, "found":Z
    const/4 v1, -0x1

    .line 2773
    .local v1, "lastce":I
    :cond_0
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/CollationElementIterator;->previous()I

    move-result v3

    .line 2774
    if-ne v3, v7, :cond_3

    .line 2775
    const/4 v0, 0x0

    .line 2800
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    if-lez v2, :cond_2

    .line 2801
    move v1, v3

    .line 2802
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/CollationElementIterator;->previous()I

    move-result v3

    .line 2803
    if-ne v3, v7, :cond_8

    .line 2804
    const/4 v0, 0x0

    .line 2816
    :cond_2
    move v3, v1

    .line 2818
    if-nez v0, :cond_a

    .line 2819
    invoke-direct {p0, v4, v1, v2}, Lcom/ibm/icu/text/StringSearch;->shiftForward(III)I

    move-result v4

    .line 2821
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v2, v6, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    .line 2822
    goto :goto_0

    .line 2778
    :cond_3
    invoke-direct {p0, v3}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v3

    .line 2779
    if-nez v3, :cond_4

    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/CollationElementIterator;->isInBuffer()Z

    move-result v6

    if-nez v6, :cond_0

    .line 2785
    :cond_4
    if-eq v1, v7, :cond_5

    if-nez v1, :cond_6

    .line 2787
    :cond_5
    move v1, v3

    .line 2789
    :cond_6
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v6, v6, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v6, v6, v2

    if-ne v3, v6, :cond_7

    .line 2791
    const/4 v0, 0x1

    .line 2792
    goto :goto_1

    .line 2794
    :cond_7
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v6, v6, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    if-gtz v6, :cond_0

    .line 2795
    const/4 v0, 0x0

    .line 2796
    goto :goto_1

    .line 2807
    :cond_8
    invoke-direct {p0, v3}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v3

    .line 2808
    if-eqz v3, :cond_1

    .line 2812
    add-int/lit8 v2, v2, -0x1

    .line 2813
    if-eqz v0, :cond_9

    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v6, v6, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v6, v6, v2

    if-ne v3, v6, :cond_9

    const/4 v0, 0x1

    .line 2814
    :goto_2
    goto :goto_1

    :cond_9
    move v0, v5

    .line 2813
    goto :goto_2

    .line 2825
    :cond_a
    invoke-direct {p0, v4}, Lcom/ibm/icu/text/StringSearch;->checkNextExactMatch(I)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2832
    .end local v0    # "found":Z
    .end local v1    # "lastce":I
    .end local v2    # "patternceindex":I
    :goto_3
    return-void

    .line 2829
    .restart local v0    # "found":Z
    .restart local v1    # "lastce":I
    .restart local v2    # "patternceindex":I
    :cond_b
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aget v4, v6, v5

    .line 2830
    goto :goto_0

    .line 2831
    .end local v0    # "found":Z
    .end local v1    # "lastce":I
    .end local v2    # "patternceindex":I
    :cond_c
    invoke-direct {p0}, Lcom/ibm/icu/text/StringSearch;->setMatchNotFound()V

    goto :goto_3
.end method

.method private handlePreviousCanonical(I)V
    .locals 11
    .param p1, "start"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v10, -0x1

    const/4 v6, 0x0

    .line 3009
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v8, v8, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasSuffixAccents_:Z

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v8, v8, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasPrefixAccents_:Z

    if-eqz v8, :cond_5

    :cond_0
    move v2, v7

    .line 3015
    .local v2, "hasPatternAccents":Z
    :goto_0
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v8, v8, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    invoke-direct {p0, p1, v10, v8}, Lcom/ibm/icu/text/StringSearch;->reverseShift(III)I

    move-result v5

    .line 3017
    .local v5, "textoffset":I
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v9

    invoke-virtual {v8, v6, v9}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 3018
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v9

    invoke-virtual {v8, v6, v9}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 3020
    :goto_1
    iget v8, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-lt v5, v8, :cond_f

    .line 3022
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v8, v5}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 3023
    const/4 v3, 0x1

    .line 3024
    .local v3, "patternceindex":I
    const/4 v4, 0x0

    .line 3025
    .local v4, "targetce":I
    const/4 v1, 0x0

    .line 3026
    .local v1, "found":Z
    const/4 v0, -0x1

    .line 3033
    .local v0, "firstce":I
    :cond_1
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v8}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v4

    .line 3034
    if-ne v4, v10, :cond_6

    .line 3035
    const/4 v1, 0x0

    .line 3058
    :goto_2
    move v4, v0

    .line 3060
    :cond_2
    :goto_3
    if-eqz v1, :cond_3

    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v8, v8, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    if-ge v3, v8, :cond_3

    .line 3061
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v8}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v4

    .line 3062
    if-ne v4, v10, :cond_b

    .line 3063
    const/4 v1, 0x0

    .line 3076
    :cond_3
    if-eqz v2, :cond_4

    if-nez v1, :cond_4

    .line 3077
    invoke-direct {p0, v5}, Lcom/ibm/icu/text/StringSearch;->doPreviousCanonicalMatch(I)Z

    move-result v1

    .line 3080
    :cond_4
    if-nez v1, :cond_d

    .line 3081
    invoke-direct {p0, v5, v4, v3}, Lcom/ibm/icu/text/StringSearch;->reverseShift(III)I

    move-result v5

    .line 3082
    const/4 v3, 0x0

    .line 3083
    goto :goto_1

    .end local v0    # "firstce":I
    .end local v1    # "found":Z
    .end local v2    # "hasPatternAccents":Z
    .end local v3    # "patternceindex":I
    .end local v4    # "targetce":I
    .end local v5    # "textoffset":I
    :cond_5
    move v2, v6

    .line 3009
    goto :goto_0

    .line 3038
    .restart local v0    # "firstce":I
    .restart local v1    # "found":Z
    .restart local v2    # "hasPatternAccents":Z
    .restart local v3    # "patternceindex":I
    .restart local v4    # "targetce":I
    .restart local v5    # "textoffset":I
    :cond_6
    invoke-direct {p0, v4}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v4

    .line 3039
    if-eq v0, v10, :cond_7

    if-nez v0, :cond_8

    .line 3041
    :cond_7
    move v0, v4

    .line 3044
    :cond_8
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v8, v8, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v8, v8, v6

    if-ne v4, v8, :cond_9

    .line 3046
    const/4 v1, 0x1

    .line 3047
    goto :goto_2

    .line 3049
    :cond_9
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v8, v8, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    if-eq v8, v10, :cond_a

    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v8, v8, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v9, v9, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    if-ne v8, v9, :cond_1

    .line 3053
    :cond_a
    const/4 v1, 0x0

    .line 3054
    goto :goto_2

    .line 3066
    :cond_b
    invoke-direct {p0, v4}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v4

    .line 3067
    if-eqz v4, :cond_2

    .line 3071
    if-eqz v1, :cond_c

    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v8, v8, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v8, v8, v3

    if-ne v4, v8, :cond_c

    move v1, v7

    .line 3072
    :goto_4
    add-int/lit8 v3, v3, 0x1

    .line 3073
    goto :goto_3

    :cond_c
    move v1, v6

    .line 3071
    goto :goto_4

    .line 3086
    :cond_d
    invoke-direct {p0, v5}, Lcom/ibm/icu/text/StringSearch;->checkPreviousCanonicalMatch(I)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 3092
    .end local v0    # "firstce":I
    .end local v1    # "found":Z
    .end local v3    # "patternceindex":I
    .end local v4    # "targetce":I
    :goto_5
    return-void

    .line 3089
    .restart local v0    # "firstce":I
    .restart local v1    # "found":Z
    .restart local v3    # "patternceindex":I
    .restart local v4    # "targetce":I
    :cond_e
    iget-object v8, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aget v5, v8, v6

    .line 3090
    goto :goto_1

    .line 3091
    .end local v0    # "firstce":I
    .end local v1    # "found":Z
    .end local v3    # "patternceindex":I
    .end local v4    # "targetce":I
    :cond_f
    invoke-direct {p0}, Lcom/ibm/icu/text/StringSearch;->setMatchNotFound()V

    goto :goto_5
.end method

.method private handlePreviousExact(I)V
    .locals 9
    .param p1, "start"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v8, -0x1

    .line 2927
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v6, v6, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    invoke-direct {p0, p1, v8, v6}, Lcom/ibm/icu/text/StringSearch;->reverseShift(III)I

    move-result v4

    .line 2929
    .local v4, "textoffset":I
    :goto_0
    iget v6, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-lt v4, v6, :cond_d

    .line 2931
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6, v4}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 2932
    const/4 v2, 0x1

    .line 2933
    .local v2, "patternceindex":I
    const/4 v3, 0x0

    .line 2934
    .local v3, "targetce":I
    const/4 v1, 0x0

    .line 2935
    .local v1, "found":Z
    const/4 v0, -0x1

    .line 2942
    .local v0, "firstce":I
    :cond_0
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v3

    .line 2943
    if-ne v3, v8, :cond_3

    .line 2944
    const/4 v1, 0x0

    .line 2970
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v6, v6, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    if-ge v2, v6, :cond_2

    .line 2971
    move v0, v3

    .line 2972
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v3

    .line 2973
    if-ne v3, v8, :cond_9

    .line 2974
    const/4 v1, 0x0

    .line 2986
    :cond_2
    move v3, v0

    .line 2988
    if-nez v1, :cond_b

    .line 2989
    invoke-direct {p0, v4, v3, v2}, Lcom/ibm/icu/text/StringSearch;->reverseShift(III)I

    move-result v4

    .line 2990
    const/4 v2, 0x0

    .line 2991
    goto :goto_0

    .line 2947
    :cond_3
    invoke-direct {p0, v3}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v3

    .line 2948
    if-eq v0, v8, :cond_4

    if-nez v0, :cond_5

    .line 2950
    :cond_4
    move v0, v3

    .line 2952
    :cond_5
    if-nez v3, :cond_6

    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v6

    if-nez v6, :cond_0

    .line 2955
    :cond_6
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v6, v6, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v6, v6, v5

    if-ne v3, v6, :cond_7

    .line 2956
    const/4 v1, 0x1

    .line 2957
    goto :goto_1

    .line 2959
    :cond_7
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v6, v6, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    if-eq v6, v8, :cond_8

    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v6, v6, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget v7, v7, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    if-ne v6, v7, :cond_0

    .line 2963
    :cond_8
    const/4 v1, 0x0

    .line 2964
    goto :goto_1

    .line 2977
    :cond_9
    invoke-direct {p0, v3}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v3

    .line 2978
    if-eqz v3, :cond_1

    .line 2982
    if-eqz v1, :cond_a

    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v6, v6, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v6, v6, v2

    if-ne v3, v6, :cond_a

    const/4 v1, 0x1

    .line 2983
    :goto_2
    add-int/lit8 v2, v2, 0x1

    .line 2984
    goto :goto_1

    :cond_a
    move v1, v5

    .line 2982
    goto :goto_2

    .line 2994
    :cond_b
    invoke-direct {p0, v4}, Lcom/ibm/icu/text/StringSearch;->checkPreviousExactMatch(I)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 3000
    .end local v0    # "firstce":I
    .end local v1    # "found":Z
    .end local v2    # "patternceindex":I
    .end local v3    # "targetce":I
    :goto_3
    return-void

    .line 2997
    .restart local v0    # "firstce":I
    .restart local v1    # "found":Z
    .restart local v2    # "patternceindex":I
    .restart local v3    # "targetce":I
    :cond_c
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_utilBuffer_:[I

    aget v4, v6, v5

    .line 2998
    goto/16 :goto_0

    .line 2999
    .end local v0    # "firstce":I
    .end local v1    # "found":Z
    .end local v2    # "patternceindex":I
    .end local v3    # "targetce":I
    :cond_d
    invoke-direct {p0}, Lcom/ibm/icu/text/StringSearch;->setMatchNotFound()V

    goto :goto_3
.end method

.method private final hasAccentsAfterMatch(II)Z
    .locals 8
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v7, -0x1

    .line 1363
    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v5, v5, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasSuffixAccents_:Z

    if-eqz v5, :cond_7

    .line 1364
    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v5, p2}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1365
    iget v5, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-le p2, v5, :cond_0

    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v5}, Ljava/text/CharacterIterator;->previous()C

    move-result v5

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1367
    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v5}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v5

    iget v6, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-le v5, v6, :cond_0

    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v5}, Ljava/text/CharacterIterator;->previous()C

    move-result v5

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1369
    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v5}, Ljava/text/CharacterIterator;->next()C

    .line 1372
    :cond_0
    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v6}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v6

    invoke-static {v5, v6}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/text/CharacterIterator;I)C

    move-result v5

    and-int/lit16 v5, v5, 0xff

    if-eqz v5, :cond_7

    .line 1373
    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v5, v5, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v2, v5, v4

    .line 1374
    .local v2, "firstce":I
    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v5, p1}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 1375
    :cond_1
    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v5

    if-ne v5, v2, :cond_1

    .line 1377
    const/4 v1, 0x1

    .line 1378
    .local v1, "count":I
    :goto_0
    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v5, v5, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    if-ge v1, v5, :cond_3

    .line 1379
    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v5

    if-nez v5, :cond_2

    .line 1381
    add-int/lit8 v1, v1, -0x1

    .line 1383
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 1384
    goto :goto_0

    .line 1386
    :cond_3
    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v0

    .line 1387
    .local v0, "ce":I
    if-eq v0, v7, :cond_4

    if-eqz v0, :cond_4

    .line 1389
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v0

    .line 1391
    :cond_4
    if-eq v0, v7, :cond_7

    if-eqz v0, :cond_7

    .line 1393
    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v5

    if-gt v5, p2, :cond_6

    .line 1403
    .end local v0    # "ce":I
    .end local v1    # "count":I
    .end local v2    # "firstce":I
    :cond_5
    :goto_1
    return v3

    .line 1396
    .restart local v0    # "ce":I
    .restart local v1    # "count":I
    .restart local v2    # "firstce":I
    :cond_6
    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-static {v5, p2}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/text/CharacterIterator;I)C

    move-result v5

    shr-int/lit8 v5, v5, 0x8

    if-nez v5, :cond_5

    .end local v0    # "ce":I
    .end local v1    # "count":I
    .end local v2    # "firstce":I
    :cond_7
    move v3, v4

    .line 1403
    goto :goto_1
.end method

.method private final hasAccentsBeforeMatch(II)Z
    .locals 8
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1302
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-boolean v6, v6, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasPrefixAccents_:Z

    if-eqz v6, :cond_6

    .line 1304
    const/4 v3, 0x1

    .line 1305
    .local v3, "ignorable":Z
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v6, v6, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v2, v6, v5

    .line 1306
    .local v2, "firstce":I
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6, p1}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 1307
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v1

    .line 1308
    .local v1, "ce":I
    :goto_0
    if-eq v1, v2, :cond_1

    .line 1309
    if-eqz v1, :cond_0

    .line 1310
    const/4 v3, 0x0

    .line 1312
    :cond_0
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v1

    .line 1313
    goto :goto_0

    .line 1314
    :cond_1
    if-nez v3, :cond_3

    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/CollationElementIterator;->isInBuffer()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1343
    .end local v1    # "ce":I
    .end local v2    # "firstce":I
    .end local v3    # "ignorable":Z
    :cond_2
    :goto_1
    return v4

    .line 1320
    .restart local v1    # "ce":I
    .restart local v2    # "firstce":I
    .restart local v3    # "ignorable":Z
    :cond_3
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-static {v6, p1}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/text/CharacterIterator;I)C

    move-result v6

    shr-int/lit8 v6, v6, 0x8

    if-eqz v6, :cond_4

    move v0, v4

    .line 1322
    .local v0, "accent":Z
    :goto_2
    if-nez v0, :cond_5

    .line 1323
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/StringSearch;->checkExtraMatchAccents(II)Z

    move-result v4

    goto :goto_1

    .end local v0    # "accent":Z
    :cond_4
    move v0, v5

    .line 1320
    goto :goto_2

    .line 1325
    .restart local v0    # "accent":Z
    :cond_5
    if-eqz v3, :cond_2

    .line 1328
    iget v6, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-le p1, v6, :cond_6

    .line 1329
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v6, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1330
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v6}, Ljava/text/CharacterIterator;->previous()C

    .line 1331
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v7}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v7

    invoke-static {v6, v7}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/text/CharacterIterator;I)C

    move-result v6

    and-int/lit16 v6, v6, 0xff

    if-eqz v6, :cond_6

    .line 1333
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6, p1}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 1334
    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/CollationElementIterator;->previous()I

    move-result v1

    .line 1335
    const/4 v6, -0x1

    if-eq v1, v6, :cond_6

    if-nez v1, :cond_2

    .end local v0    # "accent":Z
    .end local v1    # "ce":I
    .end local v2    # "firstce":I
    .end local v3    # "ignorable":Z
    :cond_6
    move v4, v5

    .line 1343
    goto :goto_1
.end method

.method private static final hash(I)I
    .locals 1
    .param p0, "ce"    # I

    .prologue
    .line 803
    invoke-static {p0}, Lcom/ibm/icu/text/CollationElementIterator;->primaryOrder(I)I

    move-result v0

    rem-int/lit16 v0, v0, 0x101

    return v0
.end method

.method private final initialize()V
    .locals 8

    .prologue
    .line 1036
    invoke-direct {p0}, Lcom/ibm/icu/text/StringSearch;->initializePattern()I

    move-result v5

    .line 1037
    .local v5, "expandlength":I
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v0, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    if-lez v0, :cond_1

    .line 1038
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v0, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    if-le v0, v5, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v0, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    sub-int/2addr v0, v5

    :goto_0
    int-to-char v6, v0

    .line 1040
    .local v6, "minlength":C
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iput v6, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_defaultShiftSize_:I

    .line 1041
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v1, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_shift_:[C

    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v2, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_backShift_:[C

    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v3, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v4, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    move-object v0, p0

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/ibm/icu/text/StringSearch;->setShiftTable([C[C[IIICC)V

    .line 1048
    .end local v6    # "minlength":C
    :goto_1
    return-void

    .line 1038
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1046
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    const/4 v1, 0x0

    iput v1, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_defaultShiftSize_:I

    goto :goto_1
.end method

.method private final initializePattern()I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 946
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v0

    if-nez v0, :cond_0

    .line 947
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iput-boolean v2, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasPrefixAccents_:Z

    .line 948
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iput-boolean v2, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasSuffixAccents_:Z

    .line 958
    :goto_0
    invoke-direct {p0}, Lcom/ibm/icu/text/StringSearch;->initializePatternCETable()I

    move-result v0

    return v0

    .line 950
    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v0, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->targetText:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/lang/String;I)C

    move-result v0

    shr-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasPrefixAccents_:Z

    .line 952
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v3, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v3, v3, Lcom/ibm/icu/text/StringSearch$Pattern;->targetText:Ljava/lang/String;

    iget-object v4, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v4, v4, Lcom/ibm/icu/text/StringSearch$Pattern;->targetText:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v3, v4}, Lcom/ibm/icu/text/StringSearch;->getFCD(Ljava/lang/String;I)C

    move-result v3

    and-int/lit16 v3, v3, 0xff

    if-eqz v3, :cond_2

    :goto_2
    iput-boolean v1, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasSuffixAccents_:Z

    goto :goto_0

    :cond_1
    move v0, v2

    .line 950
    goto :goto_1

    :cond_2
    move v1, v2

    .line 952
    goto :goto_2
.end method

.method private final initializePatternCETable()I
    .locals 7

    .prologue
    .line 917
    iget-object v4, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v5, v5, Lcom/ibm/icu/text/StringSearch$Pattern;->targetText:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 919
    const/4 v2, 0x0

    .line 920
    .local v2, "offset":I
    const/4 v3, 0x0

    .line 921
    .local v3, "result":I
    iget-object v4, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v0

    .line 923
    .local v0, "ce":I
    :goto_0
    const/4 v4, -0x1

    if-eq v0, v4, :cond_1

    .line 924
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v1

    .line 925
    .local v1, "newce":I
    if-eqz v1, :cond_0

    .line 926
    iget-object v4, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v5, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v5, v5, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    invoke-static {v2, v1, v5}, Lcom/ibm/icu/text/StringSearch;->append(II[I)[I

    move-result-object v5

    iput-object v5, v4, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    .line 927
    add-int/lit8 v2, v2, 0x1

    .line 929
    :cond_0
    iget-object v4, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v4, v0}, Lcom/ibm/icu/text/CollationElementIterator;->getMaxExpansion(I)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    add-int/2addr v3, v4

    .line 930
    iget-object v4, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v0

    .line 931
    goto :goto_0

    .line 933
    .end local v1    # "newce":I
    :cond_1
    iget-object v4, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v6, v6, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    invoke-static {v2, v5, v6}, Lcom/ibm/icu/text/StringSearch;->append(II[I)[I

    move-result-object v5

    iput-object v5, v4, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    .line 934
    iget-object v4, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iput v2, v4, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    .line 936
    return v3
.end method

.method private final isBreakUnit(II)Z
    .locals 10
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 1059
    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->breakIterator:Lcom/ibm/icu/text/BreakIterator;

    if-eqz v7, :cond_1

    .line 1060
    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->breakIterator:Lcom/ibm/icu/text/BreakIterator;

    invoke-virtual {v7}, Lcom/ibm/icu/text/BreakIterator;->first()I

    move-result v5

    .line 1061
    .local v5, "startindex":I
    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->breakIterator:Lcom/ibm/icu/text/BreakIterator;

    invoke-virtual {v7}, Lcom/ibm/icu/text/BreakIterator;->last()I

    move-result v2

    .line 1064
    .local v2, "endindex":I
    if-lt p1, v5, :cond_0

    if-gt p1, v2, :cond_0

    if-lt p2, v5, :cond_0

    if-le p2, v2, :cond_2

    :cond_0
    move v4, v6

    .line 1103
    .end local v2    # "endindex":I
    .end local v5    # "startindex":I
    :cond_1
    :goto_0
    return v4

    .line 1071
    .restart local v2    # "endindex":I
    .restart local v5    # "startindex":I
    :cond_2
    if-eq p1, v5, :cond_3

    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->breakIterator:Lcom/ibm/icu/text/BreakIterator;

    add-int/lit8 v8, p1, -0x1

    invoke-virtual {v7, v8}, Lcom/ibm/icu/text/BreakIterator;->following(I)I

    move-result v7

    if-ne v7, p1, :cond_6

    :cond_3
    if-eq p2, v2, :cond_4

    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->breakIterator:Lcom/ibm/icu/text/BreakIterator;

    add-int/lit8 v8, p2, -0x1

    invoke-virtual {v7, v8}, Lcom/ibm/icu/text/BreakIterator;->following(I)I

    move-result v7

    if-ne v7, p2, :cond_6

    .line 1075
    .local v4, "result":Z
    :cond_4
    :goto_1
    if-eqz v4, :cond_1

    .line 1077
    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    new-instance v8, Lcom/ibm/icu/impl/CharacterIteratorWrapper;

    iget-object v9, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-direct {v8, v9}, Lcom/ibm/icu/impl/CharacterIteratorWrapper;-><init>(Ljava/text/CharacterIterator;)V

    invoke-virtual {v7, v8, p1}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Lcom/ibm/icu/text/UCharacterIterator;I)V

    .line 1079
    const/4 v1, 0x0

    .local v1, "count":I
    :goto_2
    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v7, v7, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    if-ge v1, v7, :cond_8

    .line 1081
    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v7}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v0

    .line 1082
    .local v0, "ce":I
    if-nez v0, :cond_7

    .line 1083
    add-int/lit8 v1, v1, -0x1

    .line 1080
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v0    # "ce":I
    .end local v1    # "count":I
    .end local v4    # "result":Z
    :cond_6
    move v4, v6

    .line 1071
    goto :goto_1

    .line 1086
    .restart local v0    # "ce":I
    .restart local v1    # "count":I
    .restart local v4    # "result":Z
    :cond_7
    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v7, v7, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    aget v7, v7, v1

    if-eq v0, v7, :cond_5

    move v4, v6

    .line 1087
    goto :goto_0

    .line 1090
    .end local v0    # "ce":I
    :cond_8
    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v7}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v3

    .line 1092
    .local v3, "nextce":I
    :goto_3
    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v7}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v7

    if-ne v7, p2, :cond_9

    invoke-direct {p0, v3}, Lcom/ibm/icu/text/StringSearch;->getCE(I)I

    move-result v7

    if-nez v7, :cond_9

    .line 1093
    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v7}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v3

    .line 1094
    goto :goto_3

    .line 1095
    :cond_9
    const/4 v7, -0x1

    if-eq v3, v7, :cond_1

    iget-object v7, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v7}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v7

    if-ne v7, p2, :cond_1

    move v4, v6

    .line 1098
    goto :goto_0
.end method

.method private static final isOutOfBounds(III)Z
    .locals 1
    .param p0, "textstart"    # I
    .param p1, "textlimit"    # I
    .param p2, "offset"    # I

    .prologue
    .line 1416
    if-lt p2, p0, :cond_0

    if-le p2, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final merge(Ljava/lang/StringBuffer;Ljava/text/CharacterIterator;IILjava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 2
    .param p0, "source1"    # Ljava/lang/StringBuffer;
    .param p1, "source2"    # Ljava/text/CharacterIterator;
    .param p2, "start2"    # I
    .param p3, "end2"    # I
    .param p4, "source3"    # Ljava/lang/StringBuffer;

    .prologue
    .line 1678
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1679
    .local v0, "result":Ljava/lang/StringBuffer;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1681
    sget-boolean v1, Lcom/ibm/icu/impl/ICUDebug;->isJDK14OrHigher:Z

    if-eqz v1, :cond_1

    .line 1682
    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 1687
    :cond_0
    :goto_0
    invoke-interface {p1, p2}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1688
    :goto_1
    invoke-interface {p1}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v1

    if-ge v1, p3, :cond_2

    .line 1689
    invoke-interface {p1}, Ljava/text/CharacterIterator;->current()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1690
    invoke-interface {p1}, Ljava/text/CharacterIterator;->next()C

    goto :goto_1

    .line 1684
    :cond_1
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1692
    :cond_2
    if-eqz p4, :cond_3

    invoke-virtual {p4}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-eqz v1, :cond_3

    .line 1694
    sget-boolean v1, Lcom/ibm/icu/impl/ICUDebug;->isJDK14OrHigher:Z

    if-eqz v1, :cond_4

    .line 1695
    invoke-virtual {v0, p4}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 1700
    :cond_3
    :goto_2
    return-object v0

    .line 1697
    :cond_4
    invoke-virtual {p4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2
.end method

.method private reverseShift(III)I
    .locals 4
    .param p1, "textoffset"    # I
    .param p2, "ce"    # I
    .param p3, "patternceindex"    # I

    .prologue
    .line 2189
    invoke-virtual {p0}, Lcom/ibm/icu/text/StringSearch;->isOverlapping()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2190
    iget v2, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    if-eq p1, v2, :cond_0

    .line 2191
    add-int/lit8 p1, p1, -0x1

    .line 2214
    :goto_0
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/StringSearch;->getPreviousBaseOffset(I)I

    move-result p1

    .line 2215
    return p1

    .line 2194
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v2, v2, Lcom/ibm/icu/text/StringSearch$Pattern;->m_defaultShiftSize_:I

    sub-int/2addr p1, v2

    .line 2196
    goto :goto_0

    .line 2198
    :cond_1
    const/4 v2, -0x1

    if-eq p2, v2, :cond_3

    .line 2199
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v2, v2, Lcom/ibm/icu/text/StringSearch$Pattern;->m_backShift_:[C

    invoke-static {p2}, Lcom/ibm/icu/text/StringSearch;->hash(I)I

    move-result v3

    aget-char v1, v2, v3

    .line 2203
    .local v1, "shift":I
    move v0, p3

    .line 2204
    .local v0, "adjust":I
    const/4 v2, 0x1

    if-le v0, v2, :cond_2

    if-le v1, v0, :cond_2

    .line 2205
    add-int/lit8 v2, v0, -0x1

    sub-int/2addr v1, v2

    .line 2207
    :cond_2
    sub-int/2addr p1, v1

    .line 2208
    goto :goto_0

    .line 2210
    .end local v0    # "adjust":I
    .end local v1    # "shift":I
    :cond_3
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v2, v2, Lcom/ibm/icu/text/StringSearch$Pattern;->m_defaultShiftSize_:I

    sub-int/2addr p1, v2

    goto :goto_0
.end method

.method private setMatchNotFound()V
    .locals 1

    .prologue
    .line 3142
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 3143
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/StringSearch;->setMatchLength(I)V

    .line 3144
    return-void
.end method

.method private final setShiftTable([C[C[IIICC)V
    .locals 7
    .param p1, "shift"    # [C
    .param p2, "backshift"    # [C
    .param p3, "cetable"    # [I
    .param p4, "cesize"    # I
    .param p5, "expansionsize"    # I
    .param p6, "defaultforward"    # C
    .param p7, "defaultbackward"    # C

    .prologue
    const/16 v6, 0x101

    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 983
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    if-ge v0, v6, :cond_0

    .line 984
    aput-char p6, p1, v0

    .line 983
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 986
    :cond_0
    add-int/lit8 p4, p4, -0x1

    .line 987
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p4, :cond_2

    .line 989
    sub-int v2, p6, v0

    add-int/lit8 v1, v2, -0x1

    .line 990
    .local v1, "temp":I
    aget v2, p3, v0

    invoke-static {v2}, Lcom/ibm/icu/text/StringSearch;->hash(I)I

    move-result v4

    if-le v1, v3, :cond_1

    int-to-char v2, v1

    :goto_2
    aput-char v2, p1, v4

    .line 987
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v2, v3

    .line 990
    goto :goto_2

    .line 992
    .end local v1    # "temp":I
    :cond_2
    aget v2, p3, p4

    invoke-static {v2}, Lcom/ibm/icu/text/StringSearch;->hash(I)I

    move-result v2

    aput-char v3, p1, v2

    .line 994
    invoke-static {v5}, Lcom/ibm/icu/text/StringSearch;->hash(I)I

    move-result v2

    aput-char v3, p1, v2

    .line 996
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v6, :cond_3

    .line 997
    aput-char p7, p2, v0

    .line 996
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 999
    :cond_3
    move v0, p4

    :goto_4
    if-lez v0, :cond_5

    .line 1001
    aget v2, p3, v0

    invoke-static {v2}, Lcom/ibm/icu/text/StringSearch;->hash(I)I

    move-result v4

    if-le v0, p5, :cond_4

    sub-int v2, v0, p5

    :goto_5
    int-to-char v2, v2

    aput-char v2, p2, v4

    .line 999
    add-int/lit8 v0, v0, -0x1

    goto :goto_4

    :cond_4
    move v2, v3

    .line 1001
    goto :goto_5

    .line 1004
    :cond_5
    aget v2, p3, v5

    invoke-static {v2}, Lcom/ibm/icu/text/StringSearch;->hash(I)I

    move-result v2

    aput-char v3, p2, v2

    .line 1005
    invoke-static {v5}, Lcom/ibm/icu/text/StringSearch;->hash(I)I

    move-result v2

    aput-char v3, p2, v2

    .line 1006
    return-void
.end method

.method private shiftForward(III)I
    .locals 4
    .param p1, "textoffset"    # I
    .param p2, "ce"    # I
    .param p3, "patternceindex"    # I

    .prologue
    .line 1167
    const/4 v2, -0x1

    if-eq p2, v2, :cond_1

    .line 1168
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v2, v2, Lcom/ibm/icu/text/StringSearch$Pattern;->m_shift_:[C

    invoke-static {p2}, Lcom/ibm/icu/text/StringSearch;->hash(I)I

    move-result v3

    aget-char v1, v2, v3

    .line 1171
    .local v1, "shift":I
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v2, v2, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    sub-int v0, v2, p3

    .line 1172
    .local v0, "adjust":I
    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    if-lt v1, v0, :cond_0

    .line 1173
    add-int/lit8 v2, v0, -0x1

    sub-int/2addr v1, v2

    .line 1175
    :cond_0
    add-int/2addr p1, v1

    .line 1181
    .end local v0    # "adjust":I
    .end local v1    # "shift":I
    :goto_0
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/StringSearch;->getNextBaseOffset(I)I

    move-result p1

    .line 1188
    return p1

    .line 1178
    :cond_1
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v2, v2, Lcom/ibm/icu/text/StringSearch$Pattern;->m_defaultShiftSize_:I

    add-int/2addr p1, v2

    goto :goto_0
.end method


# virtual methods
.method public getCollator()Lcom/ibm/icu/text/RuleBasedCollator;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    return-object v0
.end method

.method public getIndex()I
    .locals 3

    .prologue
    .line 322
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v0

    .line 323
    .local v0, "result":I
    iget v1, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    iget v2, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    invoke-static {v1, v2, v0}, Lcom/ibm/icu/text/StringSearch;->isOutOfBounds(III)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 324
    const/4 v0, -0x1

    .line 326
    .end local v0    # "result":I
    :cond_0
    return v0
.end method

.method public getPattern()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget-object v0, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->targetText:Ljava/lang/String;

    return-object v0
.end method

.method protected handleNext(I)I
    .locals 4
    .param p1, "start"    # I

    .prologue
    const/4 v3, -0x1

    .line 523
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v2, v2, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    if-nez v2, :cond_3

    .line 524
    const/4 v2, 0x0

    iput v2, p0, Lcom/ibm/icu/text/StringSearch;->matchLength:I

    .line 525
    iget v2, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-ne p1, v2, :cond_0

    .line 526
    iput p1, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 527
    iget v2, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 579
    :goto_0
    return v2

    .line 530
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 531
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->current()C

    move-result v0

    .line 533
    .local v0, "ch":C
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->next()C

    move-result v1

    .line 534
    .local v1, "ch2":C
    const v2, 0xffff

    if-ne v1, v2, :cond_2

    .line 535
    iput v3, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 540
    :goto_1
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 541
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->next()C

    .line 542
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v2

    iput v2, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 573
    .end local v0    # "ch":C
    .end local v1    # "ch2":C
    :cond_1
    :goto_2
    iget v2, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    if-ne v2, v3, :cond_7

    .line 574
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    iget v3, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    invoke-interface {v2, v3}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 579
    :goto_3
    iget v2, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    goto :goto_0

    .line 538
    .restart local v0    # "ch":C
    .restart local v1    # "ch2":C
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v2

    iput v2, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    goto :goto_1

    .line 546
    .end local v0    # "ch":C
    .end local v1    # "ch2":C
    :cond_3
    iget v2, p0, Lcom/ibm/icu/text/StringSearch;->matchLength:I

    if-gtz v2, :cond_4

    .line 552
    iget v2, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    if-ne p1, v2, :cond_5

    .line 553
    iput v3, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 565
    :cond_4
    :goto_4
    iget-boolean v2, p0, Lcom/ibm/icu/text/StringSearch;->m_isCanonicalMatch_:Z

    if-eqz v2, :cond_6

    .line 567
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/StringSearch;->handleNextCanonical(I)V

    goto :goto_2

    .line 560
    :cond_5
    add-int/lit8 v2, p1, -0x1

    iput v2, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    goto :goto_4

    .line 570
    :cond_6
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/StringSearch;->handleNextExact(I)V

    goto :goto_2

    .line 577
    :cond_7
    iget-object v2, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    iget v3, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    invoke-interface {v2, v3}, Ljava/text/CharacterIterator;->setIndex(I)C

    goto :goto_3
.end method

.method protected handlePrevious(I)I
    .locals 3
    .param p1, "start"    # I

    .prologue
    const/4 v2, -0x1

    .line 598
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iget v1, v1, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    if-nez v1, :cond_2

    .line 599
    const/4 v1, 0x0

    iput v1, p0, Lcom/ibm/icu/text/StringSearch;->matchLength:I

    .line 601
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v1, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 602
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v1}, Ljava/text/CharacterIterator;->previous()C

    move-result v0

    .line 603
    .local v0, "ch":C
    const v1, 0xffff

    if-ne v0, v1, :cond_1

    .line 604
    iput v2, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 633
    .end local v0    # "ch":C
    :cond_0
    :goto_0
    iget v1, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    if-ne v1, v2, :cond_5

    .line 634
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    iget v2, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    invoke-interface {v1, v2}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 639
    :goto_1
    iget v1, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    return v1

    .line 607
    .restart local v0    # "ch":C
    :cond_1
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v1}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v1

    iput v1, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 608
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 609
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v1}, Ljava/text/CharacterIterator;->previous()C

    move-result v1

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 610
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v1}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v1

    iput v1, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    goto :goto_0

    .line 616
    .end local v0    # "ch":C
    :cond_2
    iget v1, p0, Lcom/ibm/icu/text/StringSearch;->matchLength:I

    if-nez v1, :cond_3

    .line 622
    iput v2, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 624
    :cond_3
    iget-boolean v1, p0, Lcom/ibm/icu/text/StringSearch;->m_isCanonicalMatch_:Z

    if-eqz v1, :cond_4

    .line 626
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/StringSearch;->handlePreviousCanonical(I)V

    goto :goto_0

    .line 629
    :cond_4
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/StringSearch;->handlePreviousExact(I)V

    goto :goto_0

    .line 637
    :cond_5
    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    iget v2, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    invoke-interface {v1, v2}, Ljava/text/CharacterIterator;->setIndex(I)C

    goto :goto_1
.end method

.method public isCanonical()Z
    .locals 1

    .prologue
    .line 339
    iget-boolean v0, p0, Lcom/ibm/icu/text/StringSearch;->m_isCanonicalMatch_:Z

    return v0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 495
    invoke-super {p0}, Lcom/ibm/icu/text/SearchIterator;->reset()V

    .line 496
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/StringSearch;->m_isCanonicalMatch_:Z

    .line 497
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/StringSearch;->getMask(I)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/StringSearch;->m_ceMask_:I

    .line 499
    invoke-direct {p0}, Lcom/ibm/icu/text/StringSearch;->initialize()V

    .line 500
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/CollationElementIterator;->setCollator(Lcom/ibm/icu/text/RuleBasedCollator;)V

    .line 501
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/CollationElementIterator;->reset()V

    .line 502
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/CollationElementIterator;->setCollator(Lcom/ibm/icu/text/RuleBasedCollator;)V

    .line 503
    return-void
.end method

.method public setCanonical(Z)V
    .locals 3
    .param p1, "allowCanonical"    # Z

    .prologue
    const/4 v2, 0x0

    .line 453
    iput-boolean p1, p0, Lcom/ibm/icu/text/StringSearch;->m_isCanonicalMatch_:Z

    .line 454
    iget-boolean v0, p0, Lcom/ibm/icu/text/StringSearch;->m_isCanonicalMatch_:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 455
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    if-nez v0, :cond_1

    .line 456
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    .line 462
    :goto_0
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    if-nez v0, :cond_2

    .line 463
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    .line 470
    :cond_0
    :goto_1
    return-void

    .line 459
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalPrefixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 466
    :cond_2
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_canonicalSuffixAccents_:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    goto :goto_1
.end method

.method public setCollator(Lcom/ibm/icu/text/RuleBasedCollator;)V
    .locals 2
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;

    .prologue
    .line 359
    if-nez p1, :cond_0

    .line 360
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Collator can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 362
    :cond_0
    iput-object p1, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    .line 363
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/StringSearch;->getMask(I)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/StringSearch;->m_ceMask_:I

    .line 365
    invoke-direct {p0}, Lcom/ibm/icu/text/StringSearch;->initialize()V

    .line 366
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/CollationElementIterator;->setCollator(Lcom/ibm/icu/text/RuleBasedCollator;)V

    .line 367
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/CollationElementIterator;->setCollator(Lcom/ibm/icu/text/RuleBasedCollator;)V

    .line 368
    invoke-static {}, Lcom/ibm/icu/text/BreakIterator;->getCharacterInstance()Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_charBreakIter_:Lcom/ibm/icu/text/BreakIterator;

    .line 369
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_charBreakIter_:Lcom/ibm/icu/text/BreakIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/BreakIterator;->setText(Ljava/text/CharacterIterator;)V

    .line 370
    return-void
.end method

.method public setIndex(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 437
    invoke-super {p0, p1}, Lcom/ibm/icu/text/SearchIterator;->setIndex(I)V

    .line 438
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/StringSearch;->m_matchedIndex_:I

    .line 439
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/CollationElementIterator;->setExactOffset(I)V

    .line 440
    return-void
.end method

.method public setPattern(Ljava/lang/String;)V
    .locals 2
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 388
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 389
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Pattern to search for can not be null or of length 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 392
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_pattern_:Lcom/ibm/icu/text/StringSearch$Pattern;

    iput-object p1, v0, Lcom/ibm/icu/text/StringSearch$Pattern;->targetText:Ljava/lang/String;

    .line 393
    invoke-direct {p0}, Lcom/ibm/icu/text/StringSearch;->initialize()V

    .line 394
    return-void
.end method

.method public setTarget(Ljava/text/CharacterIterator;)V
    .locals 2
    .param p1, "text"    # Ljava/text/CharacterIterator;

    .prologue
    .line 408
    invoke-super {p0, p1}, Lcom/ibm/icu/text/SearchIterator;->setTarget(Ljava/text/CharacterIterator;)V

    .line 409
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v0}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/StringSearch;->m_textBeginOffset_:I

    .line 410
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v0}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/StringSearch;->m_textLimitOffset_:I

    .line 411
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_colEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/text/CharacterIterator;)V

    .line 412
    iget-object v0, p0, Lcom/ibm/icu/text/StringSearch;->m_charBreakIter_:Lcom/ibm/icu/text/BreakIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/StringSearch;->targetText:Ljava/text/CharacterIterator;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/BreakIterator;->setText(Ljava/text/CharacterIterator;)V

    .line 413
    return-void
.end method
