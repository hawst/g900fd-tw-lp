.class public Lcom/ibm/icu/text/ReplaceableString;
.super Ljava/lang/Object;
.source "ReplaceableString.java"

# interfaces
.implements Lcom/ibm/icu/text/Replaceable;


# instance fields
.field private buf:Ljava/lang/StringBuffer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/ReplaceableString;->buf:Ljava/lang/StringBuffer;

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/ibm/icu/text/ReplaceableString;->buf:Ljava/lang/StringBuffer;

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/lang/StringBuffer;)V
    .locals 0
    .param p1, "buf"    # Ljava/lang/StringBuffer;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/ibm/icu/text/ReplaceableString;->buf:Ljava/lang/StringBuffer;

    .line 48
    return-void
.end method


# virtual methods
.method public char32At(I)I
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 107
    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableString;->buf:Ljava/lang/StringBuffer;

    invoke-static {v0, p1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/StringBuffer;I)I

    move-result v0

    return v0
.end method

.method public charAt(I)C
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 92
    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableString;->buf:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    return v0
.end method

.method public copy(III)V
    .locals 6
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "dest"    # I

    .prologue
    const/4 v4, 0x0

    .line 182
    if-ne p1, p2, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableString;->buf:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-gt p1, v0, :cond_0

    .line 188
    :goto_0
    return-void

    .line 185
    :cond_0
    sub-int v0, p2, p1

    new-array v3, v0, [C

    .line 186
    .local v3, "text":[C
    invoke-virtual {p0, p1, p2, v3, v4}, Lcom/ibm/icu/text/ReplaceableString;->getChars(II[CI)V

    .line 187
    sub-int v5, p2, p1

    move-object v0, p0

    move v1, p3

    move v2, p3

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/text/ReplaceableString;->replace(II[CII)V

    goto :goto_0
.end method

.method public getChars(II[CI)V
    .locals 1
    .param p1, "srcStart"    # I
    .param p2, "srcLimit"    # I
    .param p3, "dst"    # [C
    .param p4, "dstStart"    # I

    .prologue
    .line 129
    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableString;->buf:Ljava/lang/StringBuffer;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/ibm/icu/impl/Utility;->getChars(Ljava/lang/StringBuffer;II[CI)V

    .line 130
    return-void
.end method

.method public hasMetaData()Z
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableString;->buf:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    return v0
.end method

.method public replace(IILjava/lang/String;)V
    .locals 1
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableString;->buf:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 145
    return-void
.end method

.method public replace(II[CII)V
    .locals 1
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "chars"    # [C
    .param p4, "charsStart"    # I
    .param p5, "charsLen"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableString;->buf:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 163
    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableString;->buf:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1, p3, p4, p5}, Ljava/lang/StringBuffer;->insert(I[CII)Ljava/lang/StringBuffer;

    .line 164
    return-void
.end method

.method public substring(II)Ljava/lang/String;
    .locals 1
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableString;->buf:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableString;->buf:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
