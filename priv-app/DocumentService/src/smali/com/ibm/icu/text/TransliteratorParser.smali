.class Lcom/ibm/icu/text/TransliteratorParser;
.super Ljava/lang/Object;
.source "TransliteratorParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/TransliteratorParser$1;,
        Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;,
        Lcom/ibm/icu/text/TransliteratorParser$RuleArray;,
        Lcom/ibm/icu/text/TransliteratorParser$RuleBody;,
        Lcom/ibm/icu/text/TransliteratorParser$ParseData;
    }
.end annotation


# static fields
.field private static final ALT_FORWARD_RULE_OP:C = '\u2192'

.field private static final ALT_FUNCTION:C = '\u2206'

.field private static final ALT_FWDREV_RULE_OP:C = '\u2194'

.field private static final ALT_REVERSE_RULE_OP:C = '\u2190'

.field private static final ANCHOR_START:C = '^'

.field private static final CONTEXT_ANTE:C = '{'

.field private static final CONTEXT_POST:C = '}'

.field private static final CURSOR_OFFSET:C = '@'

.field private static final CURSOR_POS:C = '|'

.field private static final DOT:C = '.'

.field private static final DOT_SET:Ljava/lang/String; = "[^[:Zp:][:Zl:]\\r\\n$]"

.field private static final END_OF_RULE:C = ';'

.field private static final ESCAPE:C = '\\'

.field private static final FORWARD_RULE_OP:C = '>'

.field private static final FUNCTION:C = '&'

.field private static final FWDREV_RULE_OP:C = '~'

.field private static final HALF_ENDERS:Ljava/lang/String; = "=><\u2190\u2192\u2194;"

.field private static final ID_TOKEN:Ljava/lang/String; = "::"

.field private static final ID_TOKEN_LEN:I = 0x2

.field private static ILLEGAL_FUNC:Lcom/ibm/icu/text/UnicodeSet; = null

.field private static ILLEGAL_SEG:Lcom/ibm/icu/text/UnicodeSet; = null

.field private static ILLEGAL_TOP:Lcom/ibm/icu/text/UnicodeSet; = null

.field private static final KLEENE_STAR:C = '*'

.field private static final ONE_OR_MORE:C = '+'

.field private static final OPERATORS:Ljava/lang/String; = "=><\u2190\u2192\u2194"

.field private static final QUOTE:C = '\''

.field private static final REVERSE_RULE_OP:C = '<'

.field private static final RULE_COMMENT_CHAR:C = '#'

.field private static final SEGMENT_CLOSE:C = ')'

.field private static final SEGMENT_OPEN:C = '('

.field private static final VARIABLE_DEF_OP:C = '='

.field private static final ZERO_OR_ONE:C = '?'


# instance fields
.field public compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

.field private curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

.field public dataVector:Ljava/util/Vector;

.field private direction:I

.field private dotStandIn:I

.field public idBlockVector:Ljava/util/Vector;

.field private parseData:Lcom/ibm/icu/text/TransliteratorParser$ParseData;

.field private segmentObjects:Ljava/util/Vector;

.field private segmentStandins:Ljava/lang/StringBuffer;

.field private undefinedVariableName:Ljava/lang/String;

.field private variableLimit:C

.field private variableNames:Ljava/util/Hashtable;

.field private variableNext:C

.field private variablesVector:Ljava/util/Vector;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 177
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v1, "[\\)]"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/ibm/icu/text/TransliteratorParser;->ILLEGAL_TOP:Lcom/ibm/icu/text/UnicodeSet;

    .line 180
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v1, "[\\{\\}\\|\\@]"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/ibm/icu/text/TransliteratorParser;->ILLEGAL_SEG:Lcom/ibm/icu/text/UnicodeSet;

    .line 183
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v1, "[\\^\\(\\.\\*\\+\\?\\{\\}\\|\\@]"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/ibm/icu/text/TransliteratorParser;->ILLEGAL_FUNC:Lcom/ibm/icu/text/UnicodeSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->dotStandIn:I

    .line 847
    return-void
.end method

.method static access$000(Lcom/ibm/icu/text/TransliteratorParser;)Ljava/util/Hashtable;
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/text/TransliteratorParser;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableNames:Ljava/util/Hashtable;

    return-object v0
.end method

.method static access$100(Lcom/ibm/icu/text/TransliteratorParser;)Lcom/ibm/icu/text/RuleBasedTransliterator$Data;
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/text/TransliteratorParser;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    return-object v0
.end method

.method static access$1000(Lcom/ibm/icu/text/TransliteratorParser;Ljava/lang/String;Ljava/lang/StringBuffer;)V
    .locals 0
    .param p0, "x0"    # Lcom/ibm/icu/text/TransliteratorParser;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/StringBuffer;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/TransliteratorParser;->appendVariableDef(Ljava/lang/String;Ljava/lang/StringBuffer;)V

    return-void
.end method

.method static access$200(Lcom/ibm/icu/text/TransliteratorParser;)Ljava/util/Vector;
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/text/TransliteratorParser;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->variablesVector:Ljava/util/Vector;

    return-object v0
.end method

.method static access$400()Lcom/ibm/icu/text/UnicodeSet;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/ibm/icu/text/TransliteratorParser;->ILLEGAL_TOP:Lcom/ibm/icu/text/UnicodeSet;

    return-object v0
.end method

.method static access$500(Lcom/ibm/icu/text/TransliteratorParser;Ljava/lang/String;Ljava/text/ParsePosition;)C
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/text/TransliteratorParser;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/text/ParsePosition;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/TransliteratorParser;->parseSet(Ljava/lang/String;Ljava/text/ParsePosition;)C

    move-result v0

    return v0
.end method

.method static access$600(Lcom/ibm/icu/text/TransliteratorParser;ILjava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/ibm/icu/text/TransliteratorParser;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3}, Lcom/ibm/icu/text/TransliteratorParser;->checkVariableRange(ILjava/lang/String;I)V

    return-void
.end method

.method static access$700()Lcom/ibm/icu/text/UnicodeSet;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/ibm/icu/text/TransliteratorParser;->ILLEGAL_SEG:Lcom/ibm/icu/text/UnicodeSet;

    return-object v0
.end method

.method static access$800()Lcom/ibm/icu/text/UnicodeSet;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/ibm/icu/text/TransliteratorParser;->ILLEGAL_FUNC:Lcom/ibm/icu/text/UnicodeSet;

    return-object v0
.end method

.method static access$900(Lcom/ibm/icu/text/TransliteratorParser;)Lcom/ibm/icu/text/TransliteratorParser$ParseData;
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/text/TransliteratorParser;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->parseData:Lcom/ibm/icu/text/TransliteratorParser$ParseData;

    return-object v0
.end method

.method private appendVariableDef(Ljava/lang/String;Ljava/lang/StringBuffer;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "buf"    # Ljava/lang/StringBuffer;

    .prologue
    .line 1519
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableNames:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    move-object v0, v1

    check-cast v0, [C

    .line 1520
    .local v0, "ch":[C
    if-nez v0, :cond_2

    .line 1525
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->undefinedVariableName:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 1526
    iput-object p1, p0, Lcom/ibm/icu/text/TransliteratorParser;->undefinedVariableName:Ljava/lang/String;

    .line 1527
    iget-char v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableNext:C

    iget-char v2, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableLimit:C

    if-lt v1, v2, :cond_0

    .line 1528
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Private use variables exhausted"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1530
    :cond_0
    iget-char v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableLimit:C

    add-int/lit8 v1, v1, -0x1

    int-to-char v1, v1

    iput-char v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableLimit:C

    invoke-virtual {p2, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1538
    :goto_0
    return-void

    .line 1532
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Undefined variable $"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1536
    :cond_2
    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method private checkVariableRange(ILjava/lang/String;I)V
    .locals 1
    .param p1, "ch"    # I
    .param p2, "rule"    # Ljava/lang/String;
    .param p3, "start"    # I

    .prologue
    .line 1309
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    iget-char v0, v0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variablesBase:C

    if-lt p1, v0, :cond_0

    iget-char v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableLimit:C

    if-ge p1, v0, :cond_0

    .line 1310
    const-string/jumbo v0, "Variable range character in rule"

    invoke-static {v0, p2, p3}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1312
    :cond_0
    return-void
.end method

.method private parsePragma(Ljava/lang/String;II)I
    .locals 5
    .param p1, "rule"    # Ljava/lang/String;
    .param p2, "pos"    # I
    .param p3, "limit"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1363
    const/4 v2, 0x2

    new-array v0, v2, [I

    .line 1368
    .local v0, "array":[I
    add-int/lit8 p2, p2, 0x4

    .line 1374
    const-string/jumbo v2, "~variable range # #~;"

    invoke-static {p1, p2, p3, v2, v0}, Lcom/ibm/icu/impl/Utility;->parsePattern(Ljava/lang/String;IILjava/lang/String;[I)I

    move-result v1

    .line 1375
    .local v1, "p":I
    if-ltz v1, :cond_0

    .line 1376
    aget v2, v0, v3

    const/4 v3, 0x1

    aget v3, v0, v3

    invoke-direct {p0, v2, v3}, Lcom/ibm/icu/text/TransliteratorParser;->setVariableRange(II)V

    move v2, v1

    .line 1399
    :goto_0
    return v2

    .line 1380
    :cond_0
    const-string/jumbo v2, "~maximum backup #~;"

    invoke-static {p1, p2, p3, v2, v0}, Lcom/ibm/icu/impl/Utility;->parsePattern(Ljava/lang/String;IILjava/lang/String;[I)I

    move-result v1

    .line 1381
    if-ltz v1, :cond_1

    .line 1382
    aget v2, v0, v3

    invoke-direct {p0, v2}, Lcom/ibm/icu/text/TransliteratorParser;->pragmaMaximumBackup(I)V

    move v2, v1

    .line 1383
    goto :goto_0

    .line 1386
    :cond_1
    const-string/jumbo v2, "~nfd rules~;"

    invoke-static {p1, p2, p3, v2, v4}, Lcom/ibm/icu/impl/Utility;->parsePattern(Ljava/lang/String;IILjava/lang/String;[I)I

    move-result v1

    .line 1387
    if-ltz v1, :cond_2

    .line 1388
    sget-object v2, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-direct {p0, v2}, Lcom/ibm/icu/text/TransliteratorParser;->pragmaNormalizeRules(Lcom/ibm/icu/text/Normalizer$Mode;)V

    move v2, v1

    .line 1389
    goto :goto_0

    .line 1392
    :cond_2
    const-string/jumbo v2, "~nfc rules~;"

    invoke-static {p1, p2, p3, v2, v4}, Lcom/ibm/icu/impl/Utility;->parsePattern(Ljava/lang/String;IILjava/lang/String;[I)I

    move-result v1

    .line 1393
    if-ltz v1, :cond_3

    .line 1394
    sget-object v2, Lcom/ibm/icu/text/Normalizer;->NFC:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-direct {p0, v2}, Lcom/ibm/icu/text/TransliteratorParser;->pragmaNormalizeRules(Lcom/ibm/icu/text/Normalizer$Mode;)V

    move v2, v1

    .line 1395
    goto :goto_0

    .line 1399
    :cond_3
    const/4 v2, -0x1

    goto :goto_0
.end method

.method private parseRule(Ljava/lang/String;II)I
    .locals 25
    .param p1, "rule"    # Ljava/lang/String;
    .param p2, "pos"    # I
    .param p3, "limit"    # I

    .prologue
    .line 1118
    move/from16 v21, p2

    .line 1119
    .local v21, "start":I
    const/16 v19, 0x0

    .line 1122
    .local v19, "operator":C
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->segmentStandins:Ljava/lang/StringBuffer;

    .line 1123
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->segmentObjects:Ljava/util/Vector;

    .line 1125
    new-instance v17, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;

    const/4 v5, 0x0

    move-object/from16 v0, v17

    invoke-direct {v0, v5}, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;-><init>(Lcom/ibm/icu/text/TransliteratorParser$1;)V

    .line 1126
    .local v17, "left":Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;
    new-instance v20, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;

    const/4 v5, 0x0

    move-object/from16 v0, v20

    invoke-direct {v0, v5}, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;-><init>(Lcom/ibm/icu/text/TransliteratorParser$1;)V

    .line 1128
    .local v20, "right":Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->undefinedVariableName:Ljava/lang/String;

    .line 1129
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->parse(Ljava/lang/String;IILcom/ibm/icu/text/TransliteratorParser;)I

    move-result p2

    .line 1131
    move/from16 v0, p2

    move/from16 v1, p3

    if-eq v0, v1, :cond_0

    const-string/jumbo v5, "=><\u2190\u2192\u2194"

    add-int/lit8 p2, p2, -0x1

    invoke-virtual/range {p1 .. p2}, Ljava/lang/String;->charAt(I)C

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    if-gez v5, :cond_1

    .line 1133
    :cond_0
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "No operator pos="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-static {v5, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1135
    :cond_1
    add-int/lit8 p2, p2, 0x1

    .line 1138
    const/16 v5, 0x3c

    move/from16 v0, v19

    if-ne v0, v5, :cond_2

    move/from16 v0, p2

    move/from16 v1, p3

    if-ge v0, v1, :cond_2

    invoke-virtual/range {p1 .. p2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x3e

    if-ne v5, v6, :cond_2

    .line 1140
    add-int/lit8 p2, p2, 0x1

    .line 1141
    const/16 v19, 0x7e

    .line 1145
    :cond_2
    packed-switch v19, :pswitch_data_0

    .line 1157
    :goto_0
    :pswitch_0
    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->parse(Ljava/lang/String;IILcom/ibm/icu/text/TransliteratorParser;)I

    move-result p2

    .line 1159
    move/from16 v0, p2

    move/from16 v1, p3

    if-ge v0, v1, :cond_3

    .line 1160
    add-int/lit8 p2, p2, -0x1

    invoke-virtual/range {p1 .. p2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x3b

    if-ne v5, v6, :cond_a

    .line 1161
    add-int/lit8 p2, p2, 0x1

    .line 1168
    :cond_3
    :goto_1
    const/16 v5, 0x3d

    move/from16 v0, v19

    if-ne v0, v5, :cond_b

    .line 1176
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->undefinedVariableName:Ljava/lang/String;

    if-nez v5, :cond_4

    .line 1177
    const-string/jumbo v5, "Missing \'$\' or duplicate definition"

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-static {v5, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1179
    :cond_4
    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move-object/from16 v0, p0

    iget-char v6, v0, Lcom/ibm/icu/text/TransliteratorParser;->variableLimit:C

    if-eq v5, v6, :cond_6

    .line 1180
    :cond_5
    const-string/jumbo v5, "Malformed LHS"

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-static {v5, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1182
    :cond_6
    move-object/from16 v0, v17

    iget-boolean v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorStart:Z

    if-nez v5, :cond_7

    move-object/from16 v0, v17

    iget-boolean v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorEnd:Z

    if-nez v5, :cond_7

    move-object/from16 v0, v20

    iget-boolean v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorStart:Z

    if-nez v5, :cond_7

    move-object/from16 v0, v20

    iget-boolean v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorEnd:Z

    if-eqz v5, :cond_8

    .line 1184
    :cond_7
    const-string/jumbo v5, "Malformed variable def"

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-static {v5, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1187
    :cond_8
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v18

    .line 1188
    .local v18, "n":I
    move/from16 v0, v18

    new-array v0, v0, [C

    move-object/from16 v23, v0

    .line 1189
    .local v23, "value":[C
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v5, v6, v0, v1, v7}, Ljava/lang/String;->getChars(II[CI)V

    .line 1190
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->variableNames:Ljava/util/Hashtable;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/ibm/icu/text/TransliteratorParser;->undefinedVariableName:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v5, v6, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1192
    move-object/from16 v0, p0

    iget-char v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->variableLimit:C

    add-int/lit8 v5, v5, 0x1

    int-to-char v5, v5

    move-object/from16 v0, p0

    iput-char v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->variableLimit:C

    .line 1284
    .end local v18    # "n":I
    .end local v23    # "value":[C
    :cond_9
    :goto_2
    return p2

    .line 1147
    :pswitch_1
    const/16 v19, 0x3e

    .line 1148
    goto/16 :goto_0

    .line 1150
    :pswitch_2
    const/16 v19, 0x3c

    .line 1151
    goto/16 :goto_0

    .line 1153
    :pswitch_3
    const/16 v19, 0x7e

    goto/16 :goto_0

    .line 1164
    :cond_a
    const-string/jumbo v5, "Unquoted operator"

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-static {v5, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 1198
    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->undefinedVariableName:Ljava/lang/String;

    if-eqz v5, :cond_c

    .line 1199
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "Undefined variable $"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/ibm/icu/text/TransliteratorParser;->undefinedVariableName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-static {v5, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1204
    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->segmentStandins:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/ibm/icu/text/TransliteratorParser;->segmentObjects:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v6

    if-le v5, v6, :cond_d

    .line 1205
    const-string/jumbo v5, "Undefined segment reference"

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-static {v5, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1207
    :cond_d
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->segmentStandins:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    move/from16 v0, v16

    if-ge v0, v5, :cond_f

    .line 1208
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->segmentStandins:Ljava/lang/StringBuffer;

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v5

    if-nez v5, :cond_e

    .line 1209
    const-string/jumbo v5, "Internal error"

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-static {v5, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1207
    :cond_e
    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    .line 1212
    :cond_f
    const/16 v16, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->segmentObjects:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    move/from16 v0, v16

    if-ge v0, v5, :cond_11

    .line 1213
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->segmentObjects:Ljava/util/Vector;

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_10

    .line 1214
    const-string/jumbo v5, "Internal error"

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-static {v5, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1212
    :cond_10
    add-int/lit8 v16, v16, 0x1

    goto :goto_4

    .line 1220
    :cond_11
    const/16 v5, 0x7e

    move/from16 v0, v19

    if-eq v0, v5, :cond_12

    move-object/from16 v0, p0

    iget v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->direction:I

    if-nez v5, :cond_1b

    const/4 v5, 0x1

    move v6, v5

    :goto_5
    const/16 v5, 0x3e

    move/from16 v0, v19

    if-ne v0, v5, :cond_1c

    const/4 v5, 0x1

    :goto_6
    if-ne v6, v5, :cond_9

    .line 1227
    :cond_12
    move-object/from16 v0, p0

    iget v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->direction:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_13

    .line 1228
    move-object/from16 v22, v17

    .line 1229
    .local v22, "temp":Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;
    move-object/from16 v17, v20

    .line 1230
    move-object/from16 v20, v22

    .line 1236
    .end local v22    # "temp":Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;
    :cond_13
    const/16 v5, 0x7e

    move/from16 v0, v19

    if-ne v0, v5, :cond_14

    .line 1237
    invoke-virtual/range {v20 .. v20}, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->removeContext()V

    .line 1238
    const/4 v5, -0x1

    move-object/from16 v0, v17

    iput v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursor:I

    .line 1239
    const/4 v5, 0x0

    move-object/from16 v0, v17

    iput v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffset:I

    .line 1243
    :cond_14
    move-object/from16 v0, v17

    iget v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->ante:I

    if-gez v5, :cond_15

    .line 1244
    const/4 v5, 0x0

    move-object/from16 v0, v17

    iput v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->ante:I

    .line 1246
    :cond_15
    move-object/from16 v0, v17

    iget v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->post:I

    if-gez v5, :cond_16

    .line 1247
    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    move-object/from16 v0, v17

    iput v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->post:I

    .line 1256
    :cond_16
    move-object/from16 v0, v20

    iget v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->ante:I

    if-gez v5, :cond_18

    move-object/from16 v0, v20

    iget v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->post:I

    if-gez v5, :cond_18

    move-object/from16 v0, v17

    iget v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursor:I

    if-gez v5, :cond_18

    move-object/from16 v0, v20

    iget v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffset:I

    if-eqz v5, :cond_17

    move-object/from16 v0, v20

    iget v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursor:I

    if-ltz v5, :cond_18

    :cond_17
    move-object/from16 v0, v20

    iget-boolean v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorStart:Z

    if-nez v5, :cond_18

    move-object/from16 v0, v20

    iget-boolean v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorEnd:Z

    if-nez v5, :cond_18

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->isValidInput(Lcom/ibm/icu/text/TransliteratorParser;)Z

    move-result v5

    if-eqz v5, :cond_18

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->isValidOutput(Lcom/ibm/icu/text/TransliteratorParser;)Z

    move-result v5

    if-eqz v5, :cond_18

    move-object/from16 v0, v17

    iget v5, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->ante:I

    move-object/from16 v0, v17

    iget v6, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->post:I

    if-le v5, v6, :cond_19

    .line 1267
    :cond_18
    const-string/jumbo v5, "Malformed rule"

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-static {v5, v0, v1}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1271
    :cond_19
    const/4 v12, 0x0

    .line 1272
    .local v12, "segmentsArray":[Lcom/ibm/icu/text/UnicodeMatcher;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->segmentObjects:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-lez v5, :cond_1a

    .line 1273
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->segmentObjects:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    new-array v12, v5, [Lcom/ibm/icu/text/UnicodeMatcher;

    .line 1274
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->segmentObjects:Ljava/util/Vector;

    invoke-virtual {v5, v12}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1277
    :cond_1a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    iget-object v0, v5, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->ruleSet:Lcom/ibm/icu/text/TransliterationRuleSet;

    move-object/from16 v24, v0

    new-instance v5, Lcom/ibm/icu/text/TransliterationRule;

    move-object/from16 v0, v17

    iget-object v6, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    move-object/from16 v0, v17

    iget v7, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->ante:I

    move-object/from16 v0, v17

    iget v8, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->post:I

    move-object/from16 v0, v20

    iget-object v9, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->text:Ljava/lang/String;

    move-object/from16 v0, v20

    iget v10, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursor:I

    move-object/from16 v0, v20

    iget v11, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->cursorOffset:I

    move-object/from16 v0, v17

    iget-boolean v13, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorStart:Z

    move-object/from16 v0, v17

    iget-boolean v14, v0, Lcom/ibm/icu/text/TransliteratorParser$RuleHalf;->anchorEnd:Z

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-direct/range {v5 .. v15}, Lcom/ibm/icu/text/TransliterationRule;-><init>(Ljava/lang/String;IILjava/lang/String;II[Lcom/ibm/icu/text/UnicodeMatcher;ZZLcom/ibm/icu/text/RuleBasedTransliterator$Data;)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lcom/ibm/icu/text/TransliterationRuleSet;->addRule(Lcom/ibm/icu/text/TransliterationRule;)V

    goto/16 :goto_2

    .line 1220
    .end local v12    # "segmentsArray":[Lcom/ibm/icu/text/UnicodeMatcher;
    :cond_1b
    const/4 v5, 0x0

    move v6, v5

    goto/16 :goto_5

    :cond_1c
    const/4 v5, 0x0

    goto/16 :goto_6

    .line 1145
    :pswitch_data_0
    .packed-switch 0x2190
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private final parseSet(Ljava/lang/String;Ljava/text/ParsePosition;)C
    .locals 3
    .param p1, "rule"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;

    .prologue
    .line 1430
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->parseData:Lcom/ibm/icu/text/TransliteratorParser$ParseData;

    invoke-direct {v0, p1, p2, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;)V

    .line 1431
    .local v0, "set":Lcom/ibm/icu/text/UnicodeSet;
    iget-char v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableNext:C

    iget-char v2, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableLimit:C

    if-lt v1, v2, :cond_0

    .line 1432
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Private use variables exhausted"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1434
    :cond_0
    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->compact()Lcom/ibm/icu/text/UnicodeSet;

    .line 1435
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/TransliteratorParser;->generateStandInFor(Ljava/lang/Object;)C

    move-result v1

    return v1
.end method

.method private pragmaMaximumBackup(I)V
    .locals 2
    .param p1, "backup"    # I

    .prologue
    .line 1324
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "use maximum backup pragma not implemented yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private pragmaNormalizeRules(Lcom/ibm/icu/text/Normalizer$Mode;)V
    .locals 2
    .param p1, "mode"    # Lcom/ibm/icu/text/Normalizer$Mode;

    .prologue
    .line 1338
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "use normalize rules pragma not implemented yet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static resemblesPragma(Ljava/lang/String;II)Z
    .locals 2
    .param p0, "rule"    # Ljava/lang/String;
    .param p1, "pos"    # I
    .param p2, "limit"    # I

    .prologue
    .line 1350
    const-string/jumbo v0, "use "

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/ibm/icu/impl/Utility;->parsePattern(Ljava/lang/String;IILjava/lang/String;[I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final ruleEnd(Ljava/lang/String;II)I
    .locals 2
    .param p0, "rule"    # Ljava/lang/String;
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    .line 1418
    const-string/jumbo v1, ";"

    invoke-static {p0, p1, p2, v1}, Lcom/ibm/icu/impl/Utility;->quotedIndexOf(Ljava/lang/String;IILjava/lang/String;)I

    move-result v0

    .line 1419
    .local v0, "end":I
    if-gez v0, :cond_0

    .line 1420
    move v0, p2

    .line 1422
    :cond_0
    return v0
.end method

.method private setVariableRange(II)V
    .locals 3
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 1291
    if-gt p1, p2, :cond_0

    if-ltz p1, :cond_0

    const v0, 0xffff

    if-le p2, v0, :cond_1

    .line 1292
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Invalid variable range "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1295
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    int-to-char v1, p1

    iput-char v1, v0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variablesBase:C

    .line 1297
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 1298
    int-to-char v0, p1

    iput-char v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableNext:C

    .line 1299
    add-int/lit8 v0, p2, 0x1

    int-to-char v0, v0

    iput-char v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableLimit:C

    .line 1301
    :cond_2
    return-void
.end method

.method static final syntaxError(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p0, "msg"    # Ljava/lang/String;
    .param p1, "rule"    # Ljava/lang/String;
    .param p2, "start"    # I

    .prologue
    .line 1412
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p1, p2, v1}, Lcom/ibm/icu/text/TransliteratorParser;->ruleEnd(Ljava/lang/String;II)I

    move-result v0

    .line 1413
    .local v0, "end":I
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, " in \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {p1, p2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/ibm/icu/impl/Utility;->escape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const/16 v3, 0x22

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method generateStandInFor(Ljava/lang/Object;)C
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1447
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variablesVector:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1448
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variablesVector:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 1449
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    iget-char v1, v1, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variablesBase:C

    add-int/2addr v1, v0

    int-to-char v1, v1

    .line 1457
    :goto_1
    return v1

    .line 1447
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1453
    :cond_1
    iget-char v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableNext:C

    iget-char v2, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableLimit:C

    if-lt v1, v2, :cond_2

    .line 1454
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Variable range exhausted"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1456
    :cond_2
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variablesVector:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1457
    iget-char v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableNext:C

    add-int/lit8 v2, v1, 0x1

    int-to-char v2, v2

    iput-char v2, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableNext:C

    goto :goto_1
.end method

.method getDotStandIn()C
    .locals 2

    .prologue
    .line 1507
    iget v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->dotStandIn:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1508
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v1, "[^[:Zp:][:Zl:]\\r\\n$]"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/TransliteratorParser;->generateStandInFor(Ljava/lang/Object;)C

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->dotStandIn:I

    .line 1510
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->dotStandIn:I

    int-to-char v0, v0

    return v0
.end method

.method public getSegmentStandin(I)C
    .locals 3
    .param p1, "seg"    # I

    .prologue
    .line 1464
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->segmentStandins:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-ge v1, p1, :cond_0

    .line 1465
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->segmentStandins:Ljava/lang/StringBuffer;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1467
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->segmentStandins:Ljava/lang/StringBuffer;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 1468
    .local v0, "c":C
    if-nez v0, :cond_2

    .line 1469
    iget-char v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableNext:C

    iget-char v2, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableLimit:C

    if-lt v1, v2, :cond_1

    .line 1470
    new-instance v1, Ljava/lang/RuntimeException;

    const-string/jumbo v2, "Variable range exhausted"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1472
    :cond_1
    iget-char v0, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableNext:C

    .end local v0    # "c":C
    add-int/lit8 v1, v0, 0x1

    int-to-char v1, v1

    iput-char v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variableNext:C

    .line 1476
    .restart local v0    # "c":C
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variablesVector:Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1477
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->segmentStandins:Ljava/lang/StringBuffer;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v1, v2, v0}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 1479
    :cond_2
    return v0
.end method

.method public parse(Ljava/lang/String;I)V
    .locals 3
    .param p1, "rules"    # Ljava/lang/String;
    .param p2, "dir"    # I

    .prologue
    .line 854
    new-instance v0, Lcom/ibm/icu/text/TransliteratorParser$RuleArray;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/TransliteratorParser$RuleArray;-><init>([Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/ibm/icu/text/TransliteratorParser;->parseRules(Lcom/ibm/icu/text/TransliteratorParser$RuleBody;I)V

    .line 855
    return-void
.end method

.method parseRules(Lcom/ibm/icu/text/TransliteratorParser$RuleBody;I)V
    .locals 27
    .param p1, "ruleArray"    # Lcom/ibm/icu/text/TransliteratorParser$RuleBody;
    .param p2, "dir"    # I

    .prologue
    .line 883
    const/16 v17, 0x1

    .line 884
    .local v17, "parsingIDs":Z
    const/16 v22, 0x0

    .line 886
    .local v22, "ruleCount":I
    new-instance v24, Ljava/util/Vector;

    invoke-direct/range {v24 .. v24}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    .line 887
    new-instance v24, Ljava/util/Vector;

    invoke-direct/range {v24 .. v24}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    .line 888
    const/16 v24, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .line 889
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/TransliteratorParser;->direction:I

    .line 890
    const/16 v24, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TransliteratorParser;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    .line 891
    new-instance v24, Ljava/util/Vector;

    invoke-direct/range {v24 .. v24}, Ljava/util/Vector;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TransliteratorParser;->variablesVector:Ljava/util/Vector;

    .line 892
    new-instance v24, Ljava/util/Hashtable;

    invoke-direct/range {v24 .. v24}, Ljava/util/Hashtable;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TransliteratorParser;->variableNames:Ljava/util/Hashtable;

    .line 893
    new-instance v24, Lcom/ibm/icu/text/TransliteratorParser$ParseData;

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/TransliteratorParser$ParseData;-><init>(Lcom/ibm/icu/text/TransliteratorParser;Lcom/ibm/icu/text/TransliteratorParser$1;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TransliteratorParser;->parseData:Lcom/ibm/icu/text/TransliteratorParser$ParseData;

    .line 895
    const/4 v10, 0x0

    .line 896
    .local v10, "errors":Ljava/lang/StringBuffer;
    const/4 v9, 0x0

    .line 898
    .local v9, "errorCount":I
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/text/TransliteratorParser$RuleBody;->reset()V

    .line 900
    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    .line 907
    .local v14, "idBlockResult":Ljava/lang/StringBuffer;
    const/16 v24, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TransliteratorParser;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    .line 908
    const/4 v6, -0x1

    .line 912
    .local v6, "compoundFilterOffset":I
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/text/TransliteratorParser$RuleBody;->nextLine()Ljava/lang/String;

    move-result-object v21

    .line 913
    .local v21, "rule":Ljava/lang/String;
    if-nez v21, :cond_2

    .line 1046
    :goto_1
    if-eqz v17, :cond_19

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->length()I

    move-result v24

    if-lez v24, :cond_19

    .line 1047
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->direction:I

    move/from16 v24, v0

    if-nez v24, :cond_18

    .line 1048
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1060
    :cond_1
    :goto_2
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/Vector;->size()I

    move-result v24

    move/from16 v0, v24

    if-ge v12, v0, :cond_1b

    .line 1061
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .line 1062
    .local v7, "data":Lcom/ibm/icu/text/RuleBasedTransliterator$Data;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->variablesVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/Vector;->size()I

    move-result v24

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iput-object v0, v7, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variables:[Ljava/lang/Object;

    .line 1063
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->variablesVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    iget-object v0, v7, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variables:[Ljava/lang/Object;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    .line 1064
    new-instance v24, Ljava/util/Hashtable;

    invoke-direct/range {v24 .. v24}, Ljava/util/Hashtable;-><init>()V

    move-object/from16 v0, v24

    iput-object v0, v7, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variableNames:Ljava/util/Hashtable;

    .line 1065
    iget-object v0, v7, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variableNames:Ljava/util/Hashtable;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->variableNames:Ljava/util/Hashtable;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/util/Hashtable;->putAll(Ljava/util/Map;)V

    .line 1060
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 916
    .end local v7    # "data":Lcom/ibm/icu/text/RuleBasedTransliterator$Data;
    .end local v12    # "i":I
    :cond_2
    const/16 v18, 0x0

    .line 917
    .local v18, "pos":I
    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v15

    .local v15, "limit":I
    move/from16 v19, v18

    .line 918
    .end local v18    # "pos":I
    .local v19, "pos":I
    :goto_4
    move/from16 v0, v19

    if-ge v0, v15, :cond_23

    .line 919
    add-int/lit8 v18, v19, 0x1

    .end local v19    # "pos":I
    .restart local v18    # "pos":I
    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 920
    .local v5, "c":C
    invoke-static {v5}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v24

    if-eqz v24, :cond_3

    move/from16 v19, v18

    .line 921
    .end local v18    # "pos":I
    .restart local v19    # "pos":I
    goto :goto_4

    .line 924
    .end local v19    # "pos":I
    .restart local v18    # "pos":I
    :cond_3
    const/16 v24, 0x23

    move/from16 v0, v24

    if-ne v5, v0, :cond_4

    .line 925
    const-string/jumbo v24, "\n"

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v24

    add-int/lit8 v18, v24, 0x1

    .line 926
    if-eqz v18, :cond_0

    move/from16 v19, v18

    .end local v18    # "pos":I
    .restart local v19    # "pos":I
    goto :goto_4

    .line 933
    .end local v19    # "pos":I
    .restart local v18    # "pos":I
    :cond_4
    const/16 v24, 0x3b

    move/from16 v0, v24

    if-ne v5, v0, :cond_5

    move/from16 v19, v18

    .line 934
    .end local v18    # "pos":I
    .restart local v19    # "pos":I
    goto :goto_4

    .line 941
    .end local v19    # "pos":I
    .restart local v18    # "pos":I
    :cond_5
    add-int/lit8 v22, v22, 0x1

    .line 945
    add-int/lit8 v18, v18, -0x1

    .line 948
    add-int/lit8 v24, v18, 0x2

    add-int/lit8 v24, v24, 0x1

    move/from16 v0, v24

    if-gt v0, v15, :cond_11

    :try_start_0
    const-string/jumbo v24, "::"

    const/16 v25, 0x0

    const/16 v26, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v18

    move-object/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v24

    if-eqz v24, :cond_11

    .line 950
    add-int/lit8 v18, v18, 0x2

    .line 951
    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 952
    :goto_5
    invoke-static {v5}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v24

    if-eqz v24, :cond_6

    move/from16 v0, v18

    if-ge v0, v15, :cond_6

    .line 953
    add-int/lit8 v18, v18, 0x1

    .line 954
    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 955
    goto :goto_5

    .line 956
    :cond_6
    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [I

    move-object/from16 v16, v0

    const/16 v24, 0x0

    aput v18, v16, v24

    .line 958
    .local v16, "p":[I
    if-nez v17, :cond_8

    .line 959
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    move-object/from16 v24, v0

    if-eqz v24, :cond_7

    .line 960
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->direction:I

    move/from16 v24, v0

    if-nez v24, :cond_a

    .line 961
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 964
    :goto_6
    const/16 v24, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .line 966
    :cond_7
    const/16 v17, 0x1

    .line 969
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->direction:I

    move/from16 v24, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    move/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/TransliteratorIDParser;->parseSingleID(Ljava/lang/String;[II)Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;

    move-result-object v13

    .line 972
    .local v13, "id":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    const/16 v24, 0x0

    aget v24, v16, v24

    move/from16 v0, v24

    move/from16 v1, v18

    if-eq v0, v1, :cond_c

    const/16 v24, 0x3b

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    move/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/Utility;->parseChar(Ljava/lang/String;[IC)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 975
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->direction:I

    move/from16 v24, v0

    if-nez v24, :cond_b

    .line 976
    iget-object v0, v13, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->canonID:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    const/16 v25, 0x3b

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1002
    :cond_9
    :goto_7
    const/16 v24, 0x0

    aget v18, v16, v24

    .end local v13    # "id":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    .end local v16    # "p":[I
    :goto_8
    move/from16 v19, v18

    .line 1044
    .end local v18    # "pos":I
    .restart local v19    # "pos":I
    goto/16 :goto_4

    .line 963
    .end local v19    # "pos":I
    .restart local v16    # "p":[I
    .restart local v18    # "pos":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v24 .. v26}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_6

    .line 1031
    .end local v16    # "p":[I
    :catch_0
    move-exception v8

    .line 1032
    .local v8, "e":Ljava/lang/IllegalArgumentException;
    const/16 v24, 0x1e

    move/from16 v0, v24

    if-ne v9, v0, :cond_16

    .line 1033
    const-string/jumbo v24, "\nMore than 30 errors; further messages squelched"

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 978
    .end local v8    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v13    # "id":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    .restart local v16    # "p":[I
    :cond_b
    const/16 v24, 0x0

    :try_start_1
    new-instance v25, Ljava/lang/StringBuffer;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v0, v13, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->canonID:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v25

    const/16 v26, 0x3b

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    move/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_7

    .line 983
    :cond_c
    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [I

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, -0x1

    aput v25, v23, v24

    .line 984
    .local v23, "withParens":[I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->direction:I

    move/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    move/from16 v2, v24

    move-object/from16 v3, v23

    move-object/from16 v4, v25

    invoke-static {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/TransliteratorIDParser;->parseGlobalFilter(Ljava/lang/String;[II[ILjava/lang/StringBuffer;)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v11

    .line 985
    .local v11, "f":Lcom/ibm/icu/text/UnicodeSet;
    if-eqz v11, :cond_10

    const/16 v24, 0x3b

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    move/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/impl/Utility;->parseChar(Ljava/lang/String;[IC)Z

    move-result v24

    if-eqz v24, :cond_10

    .line 986
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->direction:I

    move/from16 v24, v0

    if-nez v24, :cond_e

    const/16 v24, 0x1

    move/from16 v25, v24

    :goto_9
    const/16 v24, 0x0

    aget v24, v23, v24

    if-nez v24, :cond_f

    const/16 v24, 0x1

    :goto_a
    move/from16 v0, v25

    move/from16 v1, v24

    if-ne v0, v1, :cond_9

    .line 988
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    move-object/from16 v24, v0

    if-eqz v24, :cond_d

    .line 990
    const-string/jumbo v24, "Multiple global filters"

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 992
    :cond_d
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/ibm/icu/text/TransliteratorParser;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    .line 993
    move/from16 v6, v22

    .line 994
    goto/16 :goto_7

    .line 986
    :cond_e
    const/16 v24, 0x0

    move/from16 v25, v24

    goto :goto_9

    :cond_f
    const/16 v24, 0x0

    goto :goto_a

    .line 998
    :cond_10
    const-string/jumbo v24, "Invalid ::ID"

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_7

    .line 1004
    .end local v11    # "f":Lcom/ibm/icu/text/UnicodeSet;
    .end local v13    # "id":Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
    .end local v16    # "p":[I
    .end local v23    # "withParens":[I
    :cond_11
    if-eqz v17, :cond_12

    .line 1005
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->direction:I

    move/from16 v24, v0

    if-nez v24, :cond_14

    .line 1006
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1009
    :goto_b
    const/16 v24, 0x0

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->length()I

    move-result v25

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 1010
    const/16 v17, 0x0

    .line 1011
    new-instance v24, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-direct/range {v24 .. v24}, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .line 1017
    const v24, 0xf000

    const v25, 0xf8ff

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/TransliteratorParser;->setVariableRange(II)V

    .line 1020
    :cond_12
    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-static {v0, v1, v15}, Lcom/ibm/icu/text/TransliteratorParser;->resemblesPragma(Ljava/lang/String;II)Z

    move-result v24

    if-eqz v24, :cond_15

    .line 1021
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v15}, Lcom/ibm/icu/text/TransliteratorParser;->parsePragma(Ljava/lang/String;II)I

    move-result v20

    .line 1022
    .local v20, "ppp":I
    if-gez v20, :cond_13

    .line 1023
    const-string/jumbo v24, "Unrecognized pragma"

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/TransliteratorParser;->syntaxError(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1025
    :cond_13
    move/from16 v18, v20

    .line 1027
    goto/16 :goto_8

    .line 1008
    .end local v20    # "ppp":I
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    const/16 v26, 0x0

    invoke-virtual/range {v24 .. v26}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_b

    .line 1028
    :cond_15
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v15}, Lcom/ibm/icu/text/TransliteratorParser;->parseRule(Ljava/lang/String;II)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v18

    goto/16 :goto_8

    .line 1036
    .restart local v8    # "e":Ljava/lang/IllegalArgumentException;
    :cond_16
    if-nez v10, :cond_17

    .line 1037
    new-instance v10, Ljava/lang/StringBuffer;

    .end local v10    # "errors":Ljava/lang/StringBuffer;
    invoke-virtual {v8}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v10, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1041
    .restart local v10    # "errors":Ljava/lang/StringBuffer;
    :goto_c
    add-int/lit8 v9, v9, 0x1

    .line 1042
    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-static {v0, v1, v15}, Lcom/ibm/icu/text/TransliteratorParser;->ruleEnd(Ljava/lang/String;II)I

    move-result v24

    add-int/lit8 v18, v24, 0x1

    goto/16 :goto_8

    .line 1039
    :cond_17
    new-instance v24, Ljava/lang/StringBuffer;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v25, "\n"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    invoke-virtual {v8}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_c

    .line 1050
    .end local v5    # "c":C
    .end local v8    # "e":Ljava/lang/IllegalArgumentException;
    .end local v15    # "limit":I
    .end local v18    # "pos":I
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    const/16 v26, 0x0

    invoke-virtual/range {v24 .. v26}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto/16 :goto_2

    .line 1052
    :cond_19
    if-nez v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    move-object/from16 v24, v0

    if-eqz v24, :cond_1

    .line 1053
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->direction:I

    move/from16 v24, v0

    if-nez v24, :cond_1a

    .line 1054
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1056
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v24 .. v26}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto/16 :goto_2

    .line 1067
    .restart local v12    # "i":I
    :cond_1b
    const/16 v24, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/TransliteratorParser;->variablesVector:Ljava/util/Vector;

    .line 1071
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    move-object/from16 v24, v0

    if-eqz v24, :cond_1f

    .line 1072
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->direction:I

    move/from16 v24, v0

    if-nez v24, :cond_1c

    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v6, v0, :cond_1d

    :cond_1c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->direction:I

    move/from16 v24, v0

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_1f

    move/from16 v0, v22

    if-eq v6, v0, :cond_1f

    .line 1076
    :cond_1d
    new-instance v24, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v25, "Compound filters misplaced"

    invoke-direct/range {v24 .. v25}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v24
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1088
    :catch_1
    move-exception v8

    .line 1089
    .restart local v8    # "e":Ljava/lang/IllegalArgumentException;
    if-nez v10, :cond_21

    .line 1090
    new-instance v10, Ljava/lang/StringBuffer;

    .end local v10    # "errors":Ljava/lang/StringBuffer;
    invoke-virtual {v8}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v10, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1096
    .end local v8    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v10    # "errors":Ljava/lang/StringBuffer;
    :cond_1e
    :goto_d
    if-eqz v10, :cond_22

    .line 1097
    new-instance v24, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 1080
    :cond_1f
    const/4 v12, 0x0

    :goto_e
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/Vector;->size()I

    move-result v24

    move/from16 v0, v24

    if-ge v12, v0, :cond_20

    .line 1081
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .line 1082
    .restart local v7    # "data":Lcom/ibm/icu/text/RuleBasedTransliterator$Data;
    iget-object v0, v7, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->ruleSet:Lcom/ibm/icu/text/TransliterationRuleSet;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/ibm/icu/text/TransliterationRuleSet;->freeze()V

    .line 1080
    add-int/lit8 v12, v12, 0x1

    goto :goto_e

    .line 1085
    .end local v7    # "data":Lcom/ibm/icu/text/RuleBasedTransliterator$Data;
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/Vector;->size()I

    move-result v24

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v24

    if-nez v24, :cond_1e

    .line 1086
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_d

    .line 1092
    .restart local v8    # "e":Ljava/lang/IllegalArgumentException;
    :cond_21
    const-string/jumbo v24, "\n"

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v24

    invoke-virtual {v8}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_d

    .line 1099
    .end local v8    # "e":Ljava/lang/IllegalArgumentException;
    :cond_22
    return-void

    .end local v12    # "i":I
    .restart local v15    # "limit":I
    .restart local v19    # "pos":I
    :cond_23
    move/from16 v18, v19

    .end local v19    # "pos":I
    .restart local v18    # "pos":I
    goto/16 :goto_0
.end method

.method public setSegmentObject(ILcom/ibm/icu/text/StringMatcher;)V
    .locals 3
    .param p1, "seg"    # I
    .param p2, "obj"    # Lcom/ibm/icu/text/StringMatcher;

    .prologue
    .line 1490
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->segmentObjects:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v1, p1, :cond_0

    .line 1491
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->segmentObjects:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->setSize(I)V

    .line 1493
    :cond_0
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/TransliteratorParser;->getSegmentStandin(I)C

    move-result v1

    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorParser;->curData:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    iget-char v2, v2, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variablesBase:C

    sub-int v0, v1, v2

    .line 1494
    .local v0, "index":I
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->segmentObjects:Ljava/util/Vector;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variablesVector:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1496
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    throw v1

    .line 1498
    :cond_2
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->segmentObjects:Ljava/util/Vector;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v1, p2, v2}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    .line 1499
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser;->variablesVector:Ljava/util/Vector;

    invoke-virtual {v1, p2, v0}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    .line 1500
    return-void
.end method
