.class public Lcom/ibm/icu/text/MessageFormat$Field;
.super Ljava/text/Format$Field;
.source "MessageFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/MessageFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Field"
.end annotation


# static fields
.field public static final ARGUMENT:Lcom/ibm/icu/text/MessageFormat$Field;

.field private static final serialVersionUID:J = 0x683a3b3b54a02d5dL


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1541
    new-instance v0, Lcom/ibm/icu/text/MessageFormat$Field;

    const-string/jumbo v1, "message argument field"

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/MessageFormat$Field;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/ibm/icu/text/MessageFormat$Field;->ARGUMENT:Lcom/ibm/icu/text/MessageFormat$Field;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1510
    invoke-direct {p0, p1}, Ljava/text/Format$Field;-><init>(Ljava/lang/String;)V

    .line 1511
    return-void
.end method


# virtual methods
.method protected readResolve()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InvalidObjectException;
        }
    .end annotation

    .prologue
    .line 1522
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sget-object v0, Lcom/ibm/icu/text/MessageFormat;->class$com$ibm$icu$text$MessageFormat$Field:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string/jumbo v0, "com.ibm.icu.text.MessageFormat$Field"

    invoke-static {v0}, Lcom/ibm/icu/text/MessageFormat;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/ibm/icu/text/MessageFormat;->class$com$ibm$icu$text$MessageFormat$Field:Ljava/lang/Class;

    :goto_0
    if-eq v1, v0, :cond_1

    .line 1523
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string/jumbo v1, "A subclass of MessageFormat.Field must implement readResolve."

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1522
    :cond_0
    sget-object v0, Lcom/ibm/icu/text/MessageFormat;->class$com$ibm$icu$text$MessageFormat$Field:Ljava/lang/Class;

    goto :goto_0

    .line 1525
    :cond_1
    invoke-virtual {p0}, Lcom/ibm/icu/text/MessageFormat$Field;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/ibm/icu/text/MessageFormat$Field;->ARGUMENT:Lcom/ibm/icu/text/MessageFormat$Field;

    invoke-virtual {v1}, Lcom/ibm/icu/text/MessageFormat$Field;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1526
    sget-object v0, Lcom/ibm/icu/text/MessageFormat$Field;->ARGUMENT:Lcom/ibm/icu/text/MessageFormat$Field;

    return-object v0

    .line 1528
    :cond_2
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string/jumbo v1, "Unknown attribute name."

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
