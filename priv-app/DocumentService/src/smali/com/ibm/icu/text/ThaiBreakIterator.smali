.class Lcom/ibm/icu/text/ThaiBreakIterator;
.super Lcom/ibm/icu/text/DictionaryBasedBreakIterator;
.source "ThaiBreakIterator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;
    }
.end annotation


# static fields
.field private static final THAI_LOOKAHEAD:B = 0x3t

.field private static final THAI_MAIYAMOK:C = '\u0e46'

.field private static final THAI_MIN_WORD:B = 0x2t

.field private static final THAI_PAIYANNOI:C = '\u0e2f'

.field private static final THAI_PREFIX_COMBINE_THRESHOLD:B = 0x3t

.field private static final THAI_ROOT_COMBINE_THRESHOLD:B = 0x3t

.field private static fBeginWordSet:Lcom/ibm/icu/text/UnicodeSet;

.field private static fEndWordSet:Lcom/ibm/icu/text/UnicodeSet;

.field private static fMarkSet:Lcom/ibm/icu/text/UnicodeSet;

.field private static fSuffixSet:Lcom/ibm/icu/text/UnicodeSet;

.field private static fThaiWordSet:Lcom/ibm/icu/text/UnicodeSet;


# instance fields
.field private fDictionary:Lcom/ibm/icu/text/BreakCTDictionary;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xe44

    const/16 v3, 0xe40

    .line 113
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fThaiWordSet:Lcom/ibm/icu/text/UnicodeSet;

    .line 114
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fMarkSet:Lcom/ibm/icu/text/UnicodeSet;

    .line 115
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fEndWordSet:Lcom/ibm/icu/text/UnicodeSet;

    .line 116
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fBeginWordSet:Lcom/ibm/icu/text/UnicodeSet;

    .line 117
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fSuffixSet:Lcom/ibm/icu/text/UnicodeSet;

    .line 119
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fThaiWordSet:Lcom/ibm/icu/text/UnicodeSet;

    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "[[:Thai:]&[:LineBreak=SA:]]"

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->applyPattern(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    .line 120
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fThaiWordSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->compact()Lcom/ibm/icu/text/UnicodeSet;

    .line 122
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fMarkSet:Lcom/ibm/icu/text/UnicodeSet;

    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "[[:Thai:]&[:LineBreak=SA:]&[:M:]]"

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->applyPattern(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    .line 123
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fMarkSet:Lcom/ibm/icu/text/UnicodeSet;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 124
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fThaiWordSet:Lcom/ibm/icu/text/UnicodeSet;

    sput-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fEndWordSet:Lcom/ibm/icu/text/UnicodeSet;

    .line 125
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fEndWordSet:Lcom/ibm/icu/text/UnicodeSet;

    const/16 v1, 0xe31

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->remove(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 126
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fEndWordSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0, v3, v4}, Lcom/ibm/icu/text/UnicodeSet;->remove(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 127
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fBeginWordSet:Lcom/ibm/icu/text/UnicodeSet;

    const/16 v1, 0xe01

    const/16 v2, 0xe2e

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->add(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 128
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fBeginWordSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0, v3, v4}, Lcom/ibm/icu/text/UnicodeSet;->add(II)Lcom/ibm/icu/text/UnicodeSet;

    .line 129
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fSuffixSet:Lcom/ibm/icu/text/UnicodeSet;

    const/16 v1, 0xe2f

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 130
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fSuffixSet:Lcom/ibm/icu/text/UnicodeSet;

    const/16 v1, 0xe46

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 133
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fMarkSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->compact()Lcom/ibm/icu/text/UnicodeSet;

    .line 134
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fEndWordSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->compact()Lcom/ibm/icu/text/UnicodeSet;

    .line 135
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fBeginWordSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->compact()Lcom/ibm/icu/text/UnicodeSet;

    .line 136
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fSuffixSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->compact()Lcom/ibm/icu/text/UnicodeSet;

    .line 139
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fThaiWordSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->freeze()Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fMarkSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->freeze()Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fEndWordSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->freeze()Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fBeginWordSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->freeze()Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fSuffixSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->freeze()Ljava/lang/Object;

    .line 144
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/io/InputStream;)V
    .locals 1
    .param p1, "ruleStream"    # Ljava/io/InputStream;
    .param p2, "dictionaryStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;-><init>(Ljava/io/InputStream;)V

    .line 149
    new-instance v0, Lcom/ibm/icu/text/BreakCTDictionary;

    invoke-direct {v0, p2}, Lcom/ibm/icu/text/BreakCTDictionary;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator;->fDictionary:Lcom/ibm/icu/text/BreakCTDictionary;

    .line 150
    return-void
.end method

.method private divideUpDictionaryRange(II)I
    .locals 22
    .param p1, "rangeStart"    # I
    .param p2, "rangeEnd"    # I

    .prologue
    .line 206
    sub-int v19, p2, p1

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_0

    .line 207
    const/16 v17, 0x0

    .line 383
    :goto_0
    return v17

    .line 209
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/ThaiBreakIterator;->getText()Ljava/text/CharacterIterator;

    move-result-object v8

    .line 210
    .local v8, "fIter":Ljava/text/CharacterIterator;
    const/16 v17, 0x0

    .line 213
    .local v17, "wordsFound":I
    new-instance v10, Ljava/util/Stack;

    invoke-direct {v10}, Ljava/util/Stack;-><init>()V

    .line 214
    .local v10, "foundBreaks":Ljava/util/Stack;
    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v0, v0, [Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;

    move-object/from16 v16, v0

    .line 215
    .local v16, "words":[Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    const/16 v19, 0x3

    move/from16 v0, v19

    if-ge v11, v0, :cond_1

    .line 216
    new-instance v19, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;

    invoke-direct/range {v19 .. v19}, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;-><init>()V

    aput-object v19, v16, v11

    .line 215
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 220
    :cond_1
    move/from16 v0, p1

    invoke-interface {v8, v0}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 222
    :cond_2
    :goto_2
    invoke-interface {v8}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v7

    .local v7, "current":I
    move/from16 v0, p2

    if-ge v7, v0, :cond_18

    .line 223
    const/4 v15, 0x0

    .line 226
    .local v15, "wordLength":I
    rem-int/lit8 v19, v17, 0x3

    aget-object v19, v16, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fDictionary:Lcom/ibm/icu/text/BreakCTDictionary;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, p2

    invoke-virtual {v0, v8, v1, v2}, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->candidates(Ljava/text/CharacterIterator;Lcom/ibm/icu/text/BreakCTDictionary;I)I

    move-result v4

    .line 229
    .local v4, "candidates":I
    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v4, v0, :cond_8

    .line 230
    rem-int/lit8 v19, v17, 0x3

    aget-object v19, v16, v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->acceptMarked(Ljava/text/CharacterIterator;)I

    move-result v15

    .line 231
    add-int/lit8 v17, v17, 0x1

    .line 273
    :cond_3
    :goto_3
    invoke-interface {v8}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v19

    move/from16 v0, v19

    move/from16 v1, p2

    if-ge v0, v1, :cond_7

    const/16 v19, 0x3

    move/from16 v0, v19

    if-ge v15, v0, :cond_7

    .line 277
    rem-int/lit8 v19, v17, 0x3

    aget-object v19, v16, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fDictionary:Lcom/ibm/icu/text/BreakCTDictionary;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, p2

    invoke-virtual {v0, v8, v1, v2}, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->candidates(Ljava/text/CharacterIterator;Lcom/ibm/icu/text/BreakCTDictionary;I)I

    move-result v19

    if-gtz v19, :cond_11

    if-eqz v15, :cond_4

    rem-int/lit8 v19, v17, 0x3

    aget-object v19, v16, v19

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->longestPrefix()I

    move-result v19

    const/16 v20, 0x3

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_11

    .line 281
    :cond_4
    add-int v19, v7, v15

    sub-int v13, p2, v19

    .line 282
    .local v13, "remaining":I
    invoke-interface {v8}, Ljava/text/CharacterIterator;->current()C

    move-result v12

    .line 283
    .local v12, "pc":I
    const/4 v5, 0x0

    .line 285
    .local v5, "chars":I
    :goto_4
    invoke-interface {v8}, Ljava/text/CharacterIterator;->next()C

    .line 286
    invoke-interface {v8}, Ljava/text/CharacterIterator;->current()C

    move-result v14

    .line 287
    .local v14, "uc":I
    add-int/lit8 v5, v5, 0x1

    .line 288
    add-int/lit8 v13, v13, -0x1

    if-gtz v13, :cond_f

    .line 307
    :cond_5
    if-gtz v15, :cond_6

    .line 308
    add-int/lit8 v17, v17, 0x1

    .line 312
    :cond_6
    add-int/2addr v15, v5

    .line 321
    .end local v5    # "chars":I
    .end local v12    # "pc":I
    .end local v13    # "remaining":I
    .end local v14    # "uc":I
    :cond_7
    :goto_5
    invoke-interface {v8}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v6

    .local v6, "currPos":I
    move/from16 v0, p2

    if-ge v6, v0, :cond_12

    sget-object v19, Lcom/ibm/icu/text/ThaiBreakIterator;->fMarkSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-interface {v8}, Ljava/text/CharacterIterator;->current()C

    move-result v20

    invoke-virtual/range {v19 .. v20}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v19

    if-eqz v19, :cond_12

    .line 322
    invoke-interface {v8}, Ljava/text/CharacterIterator;->next()C

    .line 323
    invoke-interface {v8}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v19

    sub-int v19, v19, v6

    add-int v15, v15, v19

    .line 324
    goto :goto_5

    .line 235
    .end local v6    # "currPos":I
    :cond_8
    const/16 v19, 0x1

    move/from16 v0, v19

    if-le v4, v0, :cond_3

    .line 236
    const/4 v9, 0x0

    .line 238
    .local v9, "foundBest":Z
    invoke-interface {v8}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v19

    move/from16 v0, v19

    move/from16 v1, p2

    if-ge v0, v1, :cond_b

    .line 240
    :cond_9
    const/16 v18, 0x1

    .line 241
    .local v18, "wordsMatched":I
    add-int/lit8 v19, v17, 0x1

    rem-int/lit8 v19, v19, 0x3

    aget-object v19, v16, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fDictionary:Lcom/ibm/icu/text/BreakCTDictionary;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, p2

    invoke-virtual {v0, v8, v1, v2}, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->candidates(Ljava/text/CharacterIterator;Lcom/ibm/icu/text/BreakCTDictionary;I)I

    move-result v19

    if-lez v19, :cond_d

    .line 242
    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_a

    .line 244
    rem-int/lit8 v19, v17, 0x3

    aget-object v19, v16, v19

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->markCurrent()V

    .line 245
    const/16 v18, 0x2

    .line 249
    :cond_a
    invoke-interface {v8}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v19

    move/from16 v0, v19

    move/from16 v1, p2

    if-lt v0, v1, :cond_c

    .line 265
    .end local v18    # "wordsMatched":I
    :cond_b
    :goto_6
    rem-int/lit8 v19, v17, 0x3

    aget-object v19, v16, v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->acceptMarked(Ljava/text/CharacterIterator;)I

    move-result v15

    .line 266
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_3

    .line 256
    .restart local v18    # "wordsMatched":I
    :cond_c
    add-int/lit8 v19, v17, 0x2

    rem-int/lit8 v19, v19, 0x3

    aget-object v19, v16, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fDictionary:Lcom/ibm/icu/text/BreakCTDictionary;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, p2

    invoke-virtual {v0, v8, v1, v2}, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->candidates(Ljava/text/CharacterIterator;Lcom/ibm/icu/text/BreakCTDictionary;I)I

    move-result v19

    if-lez v19, :cond_e

    .line 257
    rem-int/lit8 v19, v17, 0x3

    aget-object v19, v16, v19

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->markCurrent()V

    .line 258
    const/4 v9, 0x1

    .line 263
    :cond_d
    :goto_7
    rem-int/lit8 v19, v17, 0x3

    aget-object v19, v16, v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->backUp(Ljava/text/CharacterIterator;)Z

    move-result v19

    if-eqz v19, :cond_b

    if-eqz v9, :cond_9

    goto :goto_6

    .line 261
    :cond_e
    add-int/lit8 v19, v17, 0x1

    rem-int/lit8 v19, v19, 0x3

    aget-object v19, v16, v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->backUp(Ljava/text/CharacterIterator;)Z

    move-result v19

    if-nez v19, :cond_c

    goto :goto_7

    .line 291
    .end local v9    # "foundBest":Z
    .end local v18    # "wordsMatched":I
    .restart local v5    # "chars":I
    .restart local v12    # "pc":I
    .restart local v13    # "remaining":I
    .restart local v14    # "uc":I
    :cond_f
    sget-object v19, Lcom/ibm/icu/text/ThaiBreakIterator;->fEndWordSet:Lcom/ibm/icu/text/UnicodeSet;

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v19

    if-eqz v19, :cond_10

    sget-object v19, Lcom/ibm/icu/text/ThaiBreakIterator;->fBeginWordSet:Lcom/ibm/icu/text/UnicodeSet;

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v19

    if-eqz v19, :cond_10

    .line 297
    add-int/lit8 v19, v17, 0x1

    rem-int/lit8 v19, v19, 0x3

    aget-object v19, v16, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fDictionary:Lcom/ibm/icu/text/BreakCTDictionary;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, p2

    invoke-virtual {v0, v8, v1, v2}, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->candidates(Ljava/text/CharacterIterator;Lcom/ibm/icu/text/BreakCTDictionary;I)I

    move-result v3

    .line 298
    .local v3, "candidate":I
    add-int v19, v7, v15

    add-int v19, v19, v5

    move/from16 v0, v19

    invoke-interface {v8, v0}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 299
    if-gtz v3, :cond_5

    .line 303
    .end local v3    # "candidate":I
    :cond_10
    move v12, v14

    .line 304
    goto/16 :goto_4

    .line 315
    .end local v5    # "chars":I
    .end local v12    # "pc":I
    .end local v13    # "remaining":I
    .end local v14    # "uc":I
    :cond_11
    add-int v19, v7, v15

    move/from16 v0, v19

    invoke-interface {v8, v0}, Ljava/text/CharacterIterator;->setIndex(I)C

    goto/16 :goto_5

    .line 330
    .restart local v6    # "currPos":I
    :cond_12
    invoke-interface {v8}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v19

    move/from16 v0, v19

    move/from16 v1, p2

    if-ge v0, v1, :cond_14

    if-lez v15, :cond_14

    .line 331
    rem-int/lit8 v19, v17, 0x3

    aget-object v19, v16, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/ThaiBreakIterator;->fDictionary:Lcom/ibm/icu/text/BreakCTDictionary;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, p2

    invoke-virtual {v0, v8, v1, v2}, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->candidates(Ljava/text/CharacterIterator;Lcom/ibm/icu/text/BreakCTDictionary;I)I

    move-result v19

    if-gtz v19, :cond_17

    sget-object v19, Lcom/ibm/icu/text/ThaiBreakIterator;->fSuffixSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-interface {v8}, Ljava/text/CharacterIterator;->current()C

    move-result v14

    .restart local v14    # "uc":I
    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v19

    if-eqz v19, :cond_17

    .line 333
    const/16 v19, 0xe2f

    move/from16 v0, v19

    if-ne v14, v0, :cond_13

    .line 334
    sget-object v19, Lcom/ibm/icu/text/ThaiBreakIterator;->fSuffixSet:Lcom/ibm/icu/text/UnicodeSet;

    invoke-interface {v8}, Ljava/text/CharacterIterator;->previous()C

    move-result v20

    invoke-virtual/range {v19 .. v20}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v19

    if-nez v19, :cond_15

    .line 336
    invoke-interface {v8}, Ljava/text/CharacterIterator;->next()C

    .line 337
    invoke-interface {v8}, Ljava/text/CharacterIterator;->next()C

    .line 338
    add-int/lit8 v15, v15, 0x1

    .line 339
    invoke-interface {v8}, Ljava/text/CharacterIterator;->current()C

    move-result v14

    .line 345
    :cond_13
    :goto_8
    const/16 v19, 0xe46

    move/from16 v0, v19

    if-ne v14, v0, :cond_14

    .line 346
    invoke-interface {v8}, Ljava/text/CharacterIterator;->previous()C

    move-result v19

    const/16 v20, 0xe46

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_16

    .line 348
    invoke-interface {v8}, Ljava/text/CharacterIterator;->next()C

    .line 349
    invoke-interface {v8}, Ljava/text/CharacterIterator;->next()C

    .line 350
    add-int/lit8 v15, v15, 0x1

    .line 362
    .end local v14    # "uc":I
    :cond_14
    :goto_9
    if-lez v15, :cond_2

    .line 363
    new-instance v19, Ljava/lang/Integer;

    add-int v20, v7, v15

    invoke-direct/range {v19 .. v20}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 342
    .restart local v14    # "uc":I
    :cond_15
    invoke-interface {v8}, Ljava/text/CharacterIterator;->next()C

    goto :goto_8

    .line 353
    :cond_16
    invoke-interface {v8}, Ljava/text/CharacterIterator;->next()C

    goto :goto_9

    .line 357
    .end local v14    # "uc":I
    :cond_17
    add-int v19, v7, v15

    move/from16 v0, v19

    invoke-interface {v8, v0}, Ljava/text/CharacterIterator;->setIndex(I)C

    goto :goto_9

    .line 368
    .end local v4    # "candidates":I
    .end local v6    # "currPos":I
    .end local v15    # "wordLength":I
    :cond_18
    invoke-virtual {v10}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move/from16 v0, v19

    move/from16 v1, p2

    if-lt v0, v1, :cond_19

    .line 369
    invoke-virtual {v10}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 370
    add-int/lit8 v17, v17, -0x1

    .line 374
    :cond_19
    invoke-virtual {v10}, Ljava/util/Stack;->size()I

    move-result v19

    add-int/lit8 v19, v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [I

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/ThaiBreakIterator;->cachedBreakPositions:[I

    .line 375
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/ThaiBreakIterator;->cachedBreakPositions:[I

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput p1, v19, v20

    .line 377
    const/4 v11, 0x0

    :goto_a
    invoke-virtual {v10}, Ljava/util/Stack;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v11, v0, :cond_1a

    .line 378
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/ThaiBreakIterator;->cachedBreakPositions:[I

    move-object/from16 v20, v0

    add-int/lit8 v21, v11, 0x1

    invoke-virtual {v10, v11}, Ljava/util/Stack;->elementAt(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    aput v19, v20, v21

    .line 377
    add-int/lit8 v11, v11, 0x1

    goto :goto_a

    .line 380
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/ThaiBreakIterator;->cachedBreakPositions:[I

    move-object/from16 v19, v0

    add-int/lit8 v20, v11, 0x1

    aput p2, v19, v20

    .line 381
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/ThaiBreakIterator;->positionInCache:I

    goto/16 :goto_0
.end method


# virtual methods
.method protected handleNext()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 158
    invoke-virtual {p0}, Lcom/ibm/icu/text/ThaiBreakIterator;->getText()Ljava/text/CharacterIterator;

    move-result-object v2

    .line 163
    .local v2, "text":Ljava/text/CharacterIterator;
    iget-object v3, p0, Lcom/ibm/icu/text/ThaiBreakIterator;->cachedBreakPositions:[I

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/ibm/icu/text/ThaiBreakIterator;->positionInCache:I

    iget-object v4, p0, Lcom/ibm/icu/text/ThaiBreakIterator;->cachedBreakPositions:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_1

    .line 168
    :cond_0
    invoke-interface {v2}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v1

    .line 169
    .local v1, "startPos":I
    iput v5, p0, Lcom/ibm/icu/text/ThaiBreakIterator;->fDictionaryCharCount:I

    .line 170
    invoke-super {p0}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->handleNext()I

    move-result v0

    .line 175
    .local v0, "result":I
    iget v3, p0, Lcom/ibm/icu/text/ThaiBreakIterator;->fDictionaryCharCount:I

    if-le v3, v6, :cond_2

    sub-int v3, v0, v1

    if-le v3, v6, :cond_2

    .line 176
    invoke-direct {p0, v1, v0}, Lcom/ibm/icu/text/ThaiBreakIterator;->divideUpDictionaryRange(II)I

    .line 189
    .end local v0    # "result":I
    .end local v1    # "startPos":I
    :cond_1
    iget-object v3, p0, Lcom/ibm/icu/text/ThaiBreakIterator;->cachedBreakPositions:[I

    if-eqz v3, :cond_3

    .line 190
    iget v3, p0, Lcom/ibm/icu/text/ThaiBreakIterator;->positionInCache:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/ibm/icu/text/ThaiBreakIterator;->positionInCache:I

    .line 191
    iget-object v3, p0, Lcom/ibm/icu/text/ThaiBreakIterator;->cachedBreakPositions:[I

    iget v4, p0, Lcom/ibm/icu/text/ThaiBreakIterator;->positionInCache:I

    aget v3, v3, v4

    invoke-interface {v2, v3}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 192
    iget-object v3, p0, Lcom/ibm/icu/text/ThaiBreakIterator;->cachedBreakPositions:[I

    iget v4, p0, Lcom/ibm/icu/text/ThaiBreakIterator;->positionInCache:I

    aget v0, v3, v4

    .line 195
    :goto_0
    return v0

    .line 182
    .restart local v0    # "result":I
    .restart local v1    # "startPos":I
    :cond_2
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/ibm/icu/text/ThaiBreakIterator;->cachedBreakPositions:[I

    goto :goto_0

    .line 194
    .end local v0    # "result":I
    .end local v1    # "startPos":I
    :cond_3
    invoke-static {v5}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 195
    const/16 v0, -0x270f

    goto :goto_0
.end method
