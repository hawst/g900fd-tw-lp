.class Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;
.super Ljava/lang/Object;
.source "AnyTransliterator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/AnyTransliterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScriptRunIterator"
.end annotation


# instance fields
.field public limit:I

.field public scriptCode:I

.field public start:I

.field private text:Lcom/ibm/icu/text/Replaceable;

.field private textLimit:I

.field private textStart:I


# direct methods
.method public constructor <init>(Lcom/ibm/icu/text/Replaceable;II)V
    .locals 0
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "start"    # I
    .param p3, "limit"    # I

    .prologue
    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274
    iput-object p1, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->text:Lcom/ibm/icu/text/Replaceable;

    .line 275
    iput p2, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->textStart:I

    .line 276
    iput p3, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->textLimit:I

    .line 277
    iput p2, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->limit:I

    .line 278
    return-void
.end method


# virtual methods
.method public adjustLimit(I)V
    .locals 1
    .param p1, "delta"    # I

    .prologue
    .line 335
    iget v0, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->limit:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->limit:I

    .line 336
    iget v0, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->textLimit:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->textLimit:I

    .line 337
    return-void
.end method

.method public next()Z
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v2, 0x1

    .line 290
    iput v5, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->scriptCode:I

    .line 291
    iget v3, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->limit:I

    iput v3, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->start:I

    .line 294
    iget v3, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->start:I

    iget v4, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->textLimit:I

    if-ne v3, v4, :cond_1

    .line 295
    const/4 v2, 0x0

    .line 327
    :cond_0
    :goto_0
    return v2

    .line 300
    :cond_1
    :goto_1
    iget v3, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->start:I

    iget v4, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->textStart:I

    if-le v3, v4, :cond_3

    .line 301
    iget-object v3, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->text:Lcom/ibm/icu/text/Replaceable;

    iget v4, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->start:I

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v3, v4}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v0

    .line 302
    .local v0, "ch":I
    invoke-static {v0}, Lcom/ibm/icu/lang/UScript;->getScript(I)I

    move-result v1

    .line 303
    .local v1, "s":I
    if-eqz v1, :cond_2

    if-ne v1, v2, :cond_3

    .line 304
    :cond_2
    iget v3, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->start:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->start:I

    goto :goto_1

    .line 312
    .end local v0    # "ch":I
    .end local v1    # "s":I
    :cond_3
    :goto_2
    iget v3, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->limit:I

    iget v4, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->textLimit:I

    if-ge v3, v4, :cond_0

    .line 313
    iget-object v3, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->text:Lcom/ibm/icu/text/Replaceable;

    iget v4, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->limit:I

    invoke-interface {v3, v4}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v0

    .line 314
    .restart local v0    # "ch":I
    invoke-static {v0}, Lcom/ibm/icu/lang/UScript;->getScript(I)I

    move-result v1

    .line 315
    .restart local v1    # "s":I
    if-eqz v1, :cond_4

    if-eq v1, v2, :cond_4

    .line 316
    iget v3, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->scriptCode:I

    if-ne v3, v5, :cond_5

    .line 317
    iput v1, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->scriptCode:I

    .line 322
    :cond_4
    iget v3, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->limit:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->limit:I

    goto :goto_2

    .line 318
    :cond_5
    iget v3, p0, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->scriptCode:I

    if-eq v1, v3, :cond_4

    goto :goto_0
.end method
