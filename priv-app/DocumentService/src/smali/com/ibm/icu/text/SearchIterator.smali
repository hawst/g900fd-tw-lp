.class public abstract Lcom/ibm/icu/text/SearchIterator;
.super Ljava/lang/Object;
.source "SearchIterator.java"


# static fields
.field public static final DONE:I = -0x1


# instance fields
.field protected breakIterator:Lcom/ibm/icu/text/BreakIterator;

.field private m_isForwardSearching_:Z

.field private m_isOverlap_:Z

.field private m_lastMatchStart_:I

.field private m_reset_:Z

.field private m_setOffset_:I

.field protected matchLength:I

.field protected targetText:Ljava/text/CharacterIterator;


# direct methods
.method protected constructor <init>(Ljava/text/CharacterIterator;Lcom/ibm/icu/text/BreakIterator;)V
    .locals 5
    .param p1, "target"    # Ljava/text/CharacterIterator;
    .param p2, "breaker"    # Lcom/ibm/icu/text/BreakIterator;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v0

    invoke-interface {p1}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v1

    sub-int/2addr v0, v1

    if-nez v0, :cond_1

    .line 680
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Illegal argument target.  Argument can not be null or of length 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 684
    :cond_1
    iput-object p1, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    .line 685
    iput-object p2, p0, Lcom/ibm/icu/text/SearchIterator;->breakIterator:Lcom/ibm/icu/text/BreakIterator;

    .line 686
    iget-object v0, p0, Lcom/ibm/icu/text/SearchIterator;->breakIterator:Lcom/ibm/icu/text/BreakIterator;

    if-eqz v0, :cond_2

    .line 687
    iget-object v0, p0, Lcom/ibm/icu/text/SearchIterator;->breakIterator:Lcom/ibm/icu/text/BreakIterator;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/BreakIterator;->setText(Ljava/text/CharacterIterator;)V

    .line 689
    :cond_2
    iput v3, p0, Lcom/ibm/icu/text/SearchIterator;->matchLength:I

    .line 690
    iput v2, p0, Lcom/ibm/icu/text/SearchIterator;->m_lastMatchStart_:I

    .line 691
    iput-boolean v3, p0, Lcom/ibm/icu/text/SearchIterator;->m_isOverlap_:Z

    .line 692
    iput-boolean v4, p0, Lcom/ibm/icu/text/SearchIterator;->m_isForwardSearching_:Z

    .line 693
    iput-boolean v4, p0, Lcom/ibm/icu/text/SearchIterator;->m_reset_:Z

    .line 694
    iput v2, p0, Lcom/ibm/icu/text/SearchIterator;->m_setOffset_:I

    .line 695
    return-void
.end method


# virtual methods
.method public final first()I
    .locals 1

    .prologue
    .line 549
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/SearchIterator;->m_isForwardSearching_:Z

    .line 550
    iget-object v0, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v0}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/SearchIterator;->setIndex(I)V

    .line 551
    invoke-virtual {p0}, Lcom/ibm/icu/text/SearchIterator;->next()I

    move-result v0

    return v0
.end method

.method public final following(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 574
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/SearchIterator;->m_isForwardSearching_:Z

    .line 576
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/SearchIterator;->setIndex(I)V

    .line 577
    invoke-virtual {p0}, Lcom/ibm/icu/text/SearchIterator;->next()I

    move-result v0

    return v0
.end method

.method public getBreakIterator()Lcom/ibm/icu/text/BreakIterator;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/ibm/icu/text/SearchIterator;->breakIterator:Lcom/ibm/icu/text/BreakIterator;

    return-object v0
.end method

.method public abstract getIndex()I
.end method

.method public getMatchLength()I
    .locals 1

    .prologue
    .line 300
    iget v0, p0, Lcom/ibm/icu/text/SearchIterator;->matchLength:I

    return v0
.end method

.method public getMatchStart()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/ibm/icu/text/SearchIterator;->m_lastMatchStart_:I

    return v0
.end method

.method public getMatchedText()Ljava/lang/String;
    .locals 4

    .prologue
    .line 348
    iget v2, p0, Lcom/ibm/icu/text/SearchIterator;->matchLength:I

    if-lez v2, :cond_1

    .line 349
    iget v2, p0, Lcom/ibm/icu/text/SearchIterator;->m_lastMatchStart_:I

    iget v3, p0, Lcom/ibm/icu/text/SearchIterator;->matchLength:I

    add-int v0, v2, v3

    .line 350
    .local v0, "limit":I
    new-instance v1, Ljava/lang/StringBuffer;

    iget v2, p0, Lcom/ibm/icu/text/SearchIterator;->matchLength:I

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 351
    .local v1, "result":Ljava/lang/StringBuffer;
    iget-object v2, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->current()C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 352
    iget-object v2, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->next()C

    .line 353
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v2

    if-ge v2, v0, :cond_0

    .line 354
    iget-object v2, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->current()C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 355
    iget-object v2, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->next()C

    goto :goto_0

    .line 357
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    iget v3, p0, Lcom/ibm/icu/text/SearchIterator;->m_lastMatchStart_:I

    invoke-interface {v2, v3}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 358
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 360
    .end local v0    # "limit":I
    .end local v1    # "result":Ljava/lang/StringBuffer;
    :goto_1
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getTarget()Ljava/text/CharacterIterator;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    return-object v0
.end method

.method protected abstract handleNext(I)I
.end method

.method protected abstract handlePrevious(I)I
.end method

.method public isOverlapping()Z
    .locals 1

    .prologue
    .line 504
    iget-boolean v0, p0, Lcom/ibm/icu/text/SearchIterator;->m_isOverlap_:Z

    return v0
.end method

.method public final last()I
    .locals 1

    .prologue
    .line 599
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/SearchIterator;->m_isForwardSearching_:Z

    .line 600
    iget-object v0, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v0}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/SearchIterator;->setIndex(I)V

    .line 601
    invoke-virtual {p0}, Lcom/ibm/icu/text/SearchIterator;->previous()I

    move-result v0

    return v0
.end method

.method public next()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, -0x1

    .line 389
    iget-object v2, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v0

    .line 390
    .local v0, "start":I
    iget v2, p0, Lcom/ibm/icu/text/SearchIterator;->m_setOffset_:I

    if-eq v2, v1, :cond_0

    .line 391
    iget v0, p0, Lcom/ibm/icu/text/SearchIterator;->m_setOffset_:I

    .line 392
    iput v1, p0, Lcom/ibm/icu/text/SearchIterator;->m_setOffset_:I

    .line 394
    :cond_0
    iget-boolean v2, p0, Lcom/ibm/icu/text/SearchIterator;->m_isForwardSearching_:Z

    if-eqz v2, :cond_5

    .line 395
    iget-boolean v2, p0, Lcom/ibm/icu/text/SearchIterator;->m_reset_:Z

    if-nez v2, :cond_1

    iget v2, p0, Lcom/ibm/icu/text/SearchIterator;->matchLength:I

    add-int/2addr v2, v0

    iget-object v3, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v3}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 398
    iput v4, p0, Lcom/ibm/icu/text/SearchIterator;->matchLength:I

    .line 399
    iget-object v2, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    iget-object v3, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v3}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v3

    invoke-interface {v2, v3}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 400
    iput v1, p0, Lcom/ibm/icu/text/SearchIterator;->m_lastMatchStart_:I

    .line 432
    :goto_0
    return v1

    .line 403
    :cond_1
    iput-boolean v4, p0, Lcom/ibm/icu/text/SearchIterator;->m_reset_:Z

    .line 419
    :cond_2
    if-ne v0, v1, :cond_3

    .line 420
    iget-object v1, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v1}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v0

    .line 422
    :cond_3
    iget v1, p0, Lcom/ibm/icu/text/SearchIterator;->matchLength:I

    if-lez v1, :cond_4

    .line 424
    iget-boolean v1, p0, Lcom/ibm/icu/text/SearchIterator;->m_isOverlap_:Z

    if-eqz v1, :cond_6

    .line 425
    add-int/lit8 v0, v0, 0x1

    .line 431
    :cond_4
    :goto_1
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/SearchIterator;->handleNext(I)I

    move-result v1

    iput v1, p0, Lcom/ibm/icu/text/SearchIterator;->m_lastMatchStart_:I

    .line 432
    iget v1, p0, Lcom/ibm/icu/text/SearchIterator;->m_lastMatchStart_:I

    goto :goto_0

    .line 411
    :cond_5
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/ibm/icu/text/SearchIterator;->m_isForwardSearching_:Z

    .line 412
    if-eq v0, v1, :cond_2

    move v1, v0

    .line 415
    goto :goto_0

    .line 428
    :cond_6
    iget v1, p0, Lcom/ibm/icu/text/SearchIterator;->matchLength:I

    add-int/2addr v0, v1

    goto :goto_1
.end method

.method public final preceding(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 625
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/SearchIterator;->m_isForwardSearching_:Z

    .line 627
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/SearchIterator;->setIndex(I)V

    .line 628
    invoke-virtual {p0}, Lcom/ibm/icu/text/SearchIterator;->previous()I

    move-result v0

    return v0
.end method

.method public previous()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, -0x1

    .line 459
    iget-object v2, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v0

    .line 460
    .local v0, "start":I
    iget v2, p0, Lcom/ibm/icu/text/SearchIterator;->m_setOffset_:I

    if-eq v2, v1, :cond_0

    .line 461
    iget v0, p0, Lcom/ibm/icu/text/SearchIterator;->m_setOffset_:I

    .line 462
    iput v1, p0, Lcom/ibm/icu/text/SearchIterator;->m_setOffset_:I

    .line 464
    :cond_0
    iget-boolean v2, p0, Lcom/ibm/icu/text/SearchIterator;->m_reset_:Z

    if-eqz v2, :cond_1

    .line 465
    iput-boolean v4, p0, Lcom/ibm/icu/text/SearchIterator;->m_isForwardSearching_:Z

    .line 466
    iput-boolean v4, p0, Lcom/ibm/icu/text/SearchIterator;->m_reset_:Z

    .line 467
    iget-object v2, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v0

    .line 470
    :cond_1
    iget-boolean v2, p0, Lcom/ibm/icu/text/SearchIterator;->m_isForwardSearching_:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 476
    iput-boolean v4, p0, Lcom/ibm/icu/text/SearchIterator;->m_isForwardSearching_:Z

    .line 477
    iget-object v1, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v1}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 492
    .end local v0    # "start":I
    :goto_0
    return v0

    .line 482
    .restart local v0    # "start":I
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v2

    if-ne v0, v2, :cond_3

    .line 484
    iput v4, p0, Lcom/ibm/icu/text/SearchIterator;->matchLength:I

    .line 485
    iget-object v2, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    iget-object v3, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v3}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v3

    invoke-interface {v2, v3}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 486
    iput v1, p0, Lcom/ibm/icu/text/SearchIterator;->m_lastMatchStart_:I

    move v0, v1

    .line 487
    goto :goto_0

    .line 491
    :cond_3
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/SearchIterator;->handlePrevious(I)I

    move-result v1

    iput v1, p0, Lcom/ibm/icu/text/SearchIterator;->m_lastMatchStart_:I

    .line 492
    iget v0, p0, Lcom/ibm/icu/text/SearchIterator;->m_lastMatchStart_:I

    goto :goto_0
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 522
    iput v1, p0, Lcom/ibm/icu/text/SearchIterator;->matchLength:I

    .line 523
    iget-object v0, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v0}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/SearchIterator;->setIndex(I)V

    .line 524
    iput-boolean v1, p0, Lcom/ibm/icu/text/SearchIterator;->m_isOverlap_:Z

    .line 525
    iput-boolean v2, p0, Lcom/ibm/icu/text/SearchIterator;->m_isForwardSearching_:Z

    .line 526
    iput-boolean v2, p0, Lcom/ibm/icu/text/SearchIterator;->m_reset_:Z

    .line 527
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/SearchIterator;->m_setOffset_:I

    .line 528
    return-void
.end method

.method public setBreakIterator(Lcom/ibm/icu/text/BreakIterator;)V
    .locals 2
    .param p1, "breakiter"    # Lcom/ibm/icu/text/BreakIterator;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/ibm/icu/text/SearchIterator;->breakIterator:Lcom/ibm/icu/text/BreakIterator;

    .line 199
    iget-object v0, p0, Lcom/ibm/icu/text/SearchIterator;->breakIterator:Lcom/ibm/icu/text/BreakIterator;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/ibm/icu/text/SearchIterator;->breakIterator:Lcom/ibm/icu/text/BreakIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/BreakIterator;->setText(Ljava/text/CharacterIterator;)V

    .line 202
    :cond_0
    return-void
.end method

.method public setIndex(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 156
    iget-object v0, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v0}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v0

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v0}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v0

    if-le p1, v0, :cond_1

    .line 158
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "setIndex(int) expected position to be between "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_1
    iput p1, p0, Lcom/ibm/icu/text/SearchIterator;->m_setOffset_:I

    .line 163
    iput-boolean v1, p0, Lcom/ibm/icu/text/SearchIterator;->m_reset_:Z

    .line 164
    iput v1, p0, Lcom/ibm/icu/text/SearchIterator;->matchLength:I

    .line 165
    return-void
.end method

.method protected setMatchLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 711
    iput p1, p0, Lcom/ibm/icu/text/SearchIterator;->matchLength:I

    .line 712
    return-void
.end method

.method public setOverlapping(Z)V
    .locals 0
    .param p1, "allowOverlap"    # Z

    .prologue
    .line 181
    iput-boolean p1, p0, Lcom/ibm/icu/text/SearchIterator;->m_isOverlap_:Z

    .line 182
    return-void
.end method

.method public setTarget(Ljava/text/CharacterIterator;)V
    .locals 3
    .param p1, "text"    # Ljava/text/CharacterIterator;

    .prologue
    const/4 v2, 0x1

    .line 216
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v0

    invoke-interface {p1}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 217
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Illegal null or empty text"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    :cond_1
    iput-object p1, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    .line 221
    iget-object v0, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-interface {v1}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 222
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/SearchIterator;->matchLength:I

    .line 223
    iput-boolean v2, p0, Lcom/ibm/icu/text/SearchIterator;->m_reset_:Z

    .line 224
    iput-boolean v2, p0, Lcom/ibm/icu/text/SearchIterator;->m_isForwardSearching_:Z

    .line 225
    iget-object v0, p0, Lcom/ibm/icu/text/SearchIterator;->breakIterator:Lcom/ibm/icu/text/BreakIterator;

    if-eqz v0, :cond_2

    .line 226
    iget-object v0, p0, Lcom/ibm/icu/text/SearchIterator;->breakIterator:Lcom/ibm/icu/text/BreakIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/SearchIterator;->targetText:Ljava/text/CharacterIterator;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/BreakIterator;->setText(Ljava/text/CharacterIterator;)V

    .line 228
    :cond_2
    return-void
.end method
