.class Lcom/ibm/icu/text/UnescapeTransliterator$3;
.super Ljava/lang/Object;
.source "UnescapeTransliterator.java"

# interfaces
.implements Lcom/ibm/icu/text/Transliterator$Factory;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;
    .locals 3
    .param p1, "ID"    # Ljava/lang/String;

    .prologue
    .line 72
    new-instance v0, Lcom/ibm/icu/text/UnescapeTransliterator;

    const-string/jumbo v1, "Hex-Any/C"

    const/16 v2, 0xf

    new-array v2, v2, [C

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/UnescapeTransliterator;-><init>(Ljava/lang/String;[C)V

    return-object v0

    :array_0
    .array-data 2
        0x2s
        0x0s
        0x10s
        0x4s
        0x4s
        0x5cs
        0x75s
        0x2s
        0x0s
        0x10s
        0x8s
        0x8s
        0x5cs
        0x55s
        -0x1s
    .end array-data
.end method
