.class Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;
.super Ljava/lang/Object;
.source "CollationRuleParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CollationRuleParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ParsedToken"
.end annotation


# instance fields
.field m_charsLen_:I

.field m_charsOffset_:I

.field m_extensionLen_:I

.field m_extensionOffset_:I

.field m_flags_:C

.field m_indirectIndex_:C

.field m_prefixLen_:I

.field m_prefixOffset_:I

.field m_strength_:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297
    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsLen_:I

    .line 298
    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_charsOffset_:I

    .line 299
    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_extensionLen_:I

    .line 300
    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_extensionOffset_:I

    .line 301
    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_prefixLen_:I

    .line 302
    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_prefixOffset_:I

    .line 303
    iput-char v0, p0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_flags_:C

    .line 304
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$ParsedToken;->m_strength_:I

    .line 305
    return-void
.end method
