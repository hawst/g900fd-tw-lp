.class Lcom/ibm/icu/text/StringSearch$Pattern;
.super Ljava/lang/Object;
.source "StringSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/StringSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Pattern"
.end annotation


# instance fields
.field protected m_CELength_:I

.field protected m_CE_:[I

.field protected m_backShift_:[C

.field protected m_defaultShiftSize_:I

.field protected m_hasPrefixAccents_:Z

.field protected m_hasSuffixAccents_:Z

.field protected m_shift_:[C

.field protected targetText:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x101

    const/4 v1, 0x0

    .line 689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 690
    iput-object p1, p0, Lcom/ibm/icu/text/StringSearch$Pattern;->targetText:Ljava/lang/String;

    .line 691
    const/16 v0, 0x100

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CE_:[I

    .line 692
    iput v1, p0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_CELength_:I

    .line 693
    iput-boolean v1, p0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasPrefixAccents_:Z

    .line 694
    iput-boolean v1, p0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_hasSuffixAccents_:Z

    .line 695
    const/4 v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_defaultShiftSize_:I

    .line 696
    new-array v0, v2, [C

    iput-object v0, p0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_shift_:[C

    .line 697
    new-array v0, v2, [C

    iput-object v0, p0, Lcom/ibm/icu/text/StringSearch$Pattern;->m_backShift_:[C

    .line 698
    return-void
.end method
