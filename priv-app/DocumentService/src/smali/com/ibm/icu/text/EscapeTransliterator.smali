.class Lcom/ibm/icu/text/EscapeTransliterator;
.super Lcom/ibm/icu/text/Transliterator;
.source "EscapeTransliterator.java"


# instance fields
.field private grokSupplementals:Z

.field private minDigits:I

.field private prefix:Ljava/lang/String;

.field private radix:I

.field private suffix:Ljava/lang/String;

.field private supplementalHandler:Lcom/ibm/icu/text/EscapeTransliterator;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/ibm/icu/text/EscapeTransliterator;)V
    .locals 1
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "prefix"    # Ljava/lang/String;
    .param p3, "suffix"    # Ljava/lang/String;
    .param p4, "radix"    # I
    .param p5, "minDigits"    # I
    .param p6, "grokSupplementals"    # Z
    .param p7, "supplementalHandler"    # Lcom/ibm/icu/text/EscapeTransliterator;

    .prologue
    .line 147
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/Transliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 148
    iput-object p2, p0, Lcom/ibm/icu/text/EscapeTransliterator;->prefix:Ljava/lang/String;

    .line 149
    iput-object p3, p0, Lcom/ibm/icu/text/EscapeTransliterator;->suffix:Ljava/lang/String;

    .line 150
    iput p4, p0, Lcom/ibm/icu/text/EscapeTransliterator;->radix:I

    .line 151
    iput p5, p0, Lcom/ibm/icu/text/EscapeTransliterator;->minDigits:I

    .line 152
    iput-boolean p6, p0, Lcom/ibm/icu/text/EscapeTransliterator;->grokSupplementals:Z

    .line 153
    iput-object p7, p0, Lcom/ibm/icu/text/EscapeTransliterator;->supplementalHandler:Lcom/ibm/icu/text/EscapeTransliterator;

    .line 154
    return-void
.end method

.method static register()V
    .locals 2

    .prologue
    .line 82
    const-string/jumbo v0, "Any-Hex/Unicode"

    new-instance v1, Lcom/ibm/icu/text/EscapeTransliterator$1;

    invoke-direct {v1}, Lcom/ibm/icu/text/EscapeTransliterator$1;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 90
    const-string/jumbo v0, "Any-Hex/Java"

    new-instance v1, Lcom/ibm/icu/text/EscapeTransliterator$2;

    invoke-direct {v1}, Lcom/ibm/icu/text/EscapeTransliterator$2;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 98
    const-string/jumbo v0, "Any-Hex/C"

    new-instance v1, Lcom/ibm/icu/text/EscapeTransliterator$3;

    invoke-direct {v1}, Lcom/ibm/icu/text/EscapeTransliterator$3;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 107
    const-string/jumbo v0, "Any-Hex/XML"

    new-instance v1, Lcom/ibm/icu/text/EscapeTransliterator$4;

    invoke-direct {v1}, Lcom/ibm/icu/text/EscapeTransliterator$4;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 115
    const-string/jumbo v0, "Any-Hex/XML10"

    new-instance v1, Lcom/ibm/icu/text/EscapeTransliterator$5;

    invoke-direct {v1}, Lcom/ibm/icu/text/EscapeTransliterator$5;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 123
    const-string/jumbo v0, "Any-Hex/Perl"

    new-instance v1, Lcom/ibm/icu/text/EscapeTransliterator$6;

    invoke-direct {v1}, Lcom/ibm/icu/text/EscapeTransliterator$6;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 131
    const-string/jumbo v0, "Any-Hex"

    new-instance v1, Lcom/ibm/icu/text/EscapeTransliterator$7;

    invoke-direct {v1}, Lcom/ibm/icu/text/EscapeTransliterator$7;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 137
    return-void
.end method


# virtual methods
.method protected handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V
    .locals 10
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "pos"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "incremental"    # Z

    .prologue
    const/4 v9, 0x0

    .line 161
    iget v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 162
    .local v6, "start":I
    iget v3, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 164
    .local v3, "limit":I
    new-instance v0, Ljava/lang/StringBuffer;

    iget-object v7, p0, Lcom/ibm/icu/text/EscapeTransliterator;->prefix:Ljava/lang/String;

    invoke-direct {v0, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 165
    .local v0, "buf":Ljava/lang/StringBuffer;
    iget-object v7, p0, Lcom/ibm/icu/text/EscapeTransliterator;->prefix:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v4

    .line 166
    .local v4, "prefixLen":I
    const/4 v5, 0x0

    .line 168
    .local v5, "redoPrefix":Z
    :goto_0
    if-ge v6, v3, :cond_4

    .line 169
    iget-boolean v7, p0, Lcom/ibm/icu/text/EscapeTransliterator;->grokSupplementals:Z

    if-eqz v7, :cond_0

    invoke-interface {p1, v6}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v1

    .line 170
    .local v1, "c":I
    :goto_1
    iget-boolean v7, p0, Lcom/ibm/icu/text/EscapeTransliterator;->grokSupplementals:Z

    if-eqz v7, :cond_1

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v2

    .line 172
    .local v2, "charLen":I
    :goto_2
    const/high16 v7, -0x10000

    and-int/2addr v7, v1

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/ibm/icu/text/EscapeTransliterator;->supplementalHandler:Lcom/ibm/icu/text/EscapeTransliterator;

    if-eqz v7, :cond_2

    .line 173
    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 174
    iget-object v7, p0, Lcom/ibm/icu/text/EscapeTransliterator;->supplementalHandler:Lcom/ibm/icu/text/EscapeTransliterator;

    iget-object v7, v7, Lcom/ibm/icu/text/EscapeTransliterator;->prefix:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 175
    iget-object v7, p0, Lcom/ibm/icu/text/EscapeTransliterator;->supplementalHandler:Lcom/ibm/icu/text/EscapeTransliterator;

    iget v7, v7, Lcom/ibm/icu/text/EscapeTransliterator;->radix:I

    iget-object v8, p0, Lcom/ibm/icu/text/EscapeTransliterator;->supplementalHandler:Lcom/ibm/icu/text/EscapeTransliterator;

    iget v8, v8, Lcom/ibm/icu/text/EscapeTransliterator;->minDigits:I

    invoke-static {v0, v1, v7, v8}, Lcom/ibm/icu/impl/Utility;->appendNumber(Ljava/lang/StringBuffer;III)Ljava/lang/StringBuffer;

    .line 177
    iget-object v7, p0, Lcom/ibm/icu/text/EscapeTransliterator;->supplementalHandler:Lcom/ibm/icu/text/EscapeTransliterator;

    iget-object v7, v7, Lcom/ibm/icu/text/EscapeTransliterator;->suffix:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 178
    const/4 v5, 0x1

    .line 191
    :goto_3
    add-int v7, v6, v2

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p1, v6, v7, v8}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 192
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    add-int/2addr v6, v7

    .line 193
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    sub-int/2addr v7, v2

    add-int/2addr v3, v7

    .line 194
    goto :goto_0

    .line 169
    .end local v1    # "c":I
    .end local v2    # "charLen":I
    :cond_0
    invoke-interface {p1, v6}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v1

    goto :goto_1

    .line 170
    .restart local v1    # "c":I
    :cond_1
    const/4 v2, 0x1

    goto :goto_2

    .line 180
    .restart local v2    # "charLen":I
    :cond_2
    if-eqz v5, :cond_3

    .line 181
    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 182
    iget-object v7, p0, Lcom/ibm/icu/text/EscapeTransliterator;->prefix:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 183
    const/4 v5, 0x0

    .line 187
    :goto_4
    iget v7, p0, Lcom/ibm/icu/text/EscapeTransliterator;->radix:I

    iget v8, p0, Lcom/ibm/icu/text/EscapeTransliterator;->minDigits:I

    invoke-static {v0, v1, v7, v8}, Lcom/ibm/icu/impl/Utility;->appendNumber(Ljava/lang/StringBuffer;III)Ljava/lang/StringBuffer;

    .line 188
    iget-object v7, p0, Lcom/ibm/icu/text/EscapeTransliterator;->suffix:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 185
    :cond_3
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_4

    .line 196
    .end local v1    # "c":I
    .end local v2    # "charLen":I
    :cond_4
    iget v7, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    iget v8, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    sub-int v8, v3, v8

    add-int/2addr v7, v8

    iput v7, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    .line 197
    iput v3, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 198
    iput v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 199
    return-void
.end method
