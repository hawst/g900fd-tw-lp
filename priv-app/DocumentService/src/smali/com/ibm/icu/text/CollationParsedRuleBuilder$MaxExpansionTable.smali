.class Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;
.super Ljava/lang/Object;
.source "CollationParsedRuleBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CollationParsedRuleBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MaxExpansionTable"
.end annotation


# instance fields
.field m_endExpansionCE_:Ljava/util/Vector;

.field m_expansionCESize_:Ljava/util/Vector;


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 854
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    .line 855
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_expansionCESize_:Ljava/util/Vector;

    .line 856
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 857
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_expansionCESize_:Ljava/util/Vector;

    new-instance v1, Ljava/lang/Byte;

    invoke-direct {v1, v2}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 858
    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;)V
    .locals 1
    .param p1, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;

    .prologue
    .line 861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 862
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    .line 863
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_expansionCESize_:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_expansionCESize_:Ljava/util/Vector;

    .line 864
    return-void
.end method
