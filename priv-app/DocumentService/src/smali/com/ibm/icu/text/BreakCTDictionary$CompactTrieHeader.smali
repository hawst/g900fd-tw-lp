.class Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;
.super Ljava/lang/Object;
.source "BreakCTDictionary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/BreakCTDictionary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CompactTrieHeader"
.end annotation


# instance fields
.field magic:I

.field nodeCount:I

.field offset:[I

.field root:I

.field size:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput v0, p0, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;->size:I

    .line 38
    iput v0, p0, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;->magic:I

    .line 39
    iput v0, p0, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;->nodeCount:I

    .line 40
    iput v0, p0, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;->root:I

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;->offset:[I

    .line 42
    return-void
.end method
