.class Lcom/ibm/icu/text/SameValueSubstitution;
.super Lcom/ibm/icu/text/NFSubstitution;
.source "NFSubstitution.java"


# direct methods
.method constructor <init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V
    .locals 2
    .param p1, "pos"    # I
    .param p2, "ruleSet"    # Lcom/ibm/icu/text/NFRuleSet;
    .param p3, "formatter"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;
    .param p4, "description"    # Ljava/lang/String;

    .prologue
    .line 550
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/ibm/icu/text/NFSubstitution;-><init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    .line 551
    const-string/jumbo v0, "=="

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 552
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "== is not a legal token"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 554
    :cond_0
    return-void
.end method


# virtual methods
.method public calcUpperBound(D)D
    .locals 1
    .param p1, "oldUpperBound"    # D

    .prologue
    .line 599
    return-wide p1
.end method

.method public composeRuleValue(DD)D
    .locals 1
    .param p1, "newRuleValue"    # D
    .param p3, "oldRuleValue"    # D

    .prologue
    .line 590
    return-wide p1
.end method

.method tokenChar()C
    .locals 1

    .prologue
    .line 611
    const/16 v0, 0x3d

    return v0
.end method

.method public transformNumber(D)D
    .locals 1
    .param p1, "number"    # D

    .prologue
    .line 573
    return-wide p1
.end method

.method public transformNumber(J)J
    .locals 1
    .param p1, "number"    # J

    .prologue
    .line 565
    return-wide p1
.end method
