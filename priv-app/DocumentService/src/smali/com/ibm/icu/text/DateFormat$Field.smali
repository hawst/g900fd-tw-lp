.class public Lcom/ibm/icu/text/DateFormat$Field;
.super Ljava/text/Format$Field;
.source "DateFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/DateFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Field"
.end annotation


# static fields
.field public static final AM_PM:Lcom/ibm/icu/text/DateFormat$Field;

.field private static final CAL_FIELDS:[Lcom/ibm/icu/text/DateFormat$Field;

.field private static final CAL_FIELD_COUNT:I

.field public static final DAY_OF_MONTH:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final DAY_OF_WEEK:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final DAY_OF_WEEK_IN_MONTH:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final DAY_OF_YEAR:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final DOW_LOCAL:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final ERA:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final EXTENDED_YEAR:Lcom/ibm/icu/text/DateFormat$Field;

.field private static final FIELD_NAME_MAP:Ljava/util/Map;

.field public static final HOUR0:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final HOUR1:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final HOUR_OF_DAY0:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final HOUR_OF_DAY1:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final JULIAN_DAY:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final MILLISECOND:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final MILLISECONDS_IN_DAY:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final MINUTE:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final MONTH:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final QUARTER:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final SECOND:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final TIME_ZONE:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final WEEK_OF_MONTH:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final WEEK_OF_YEAR:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final YEAR:Lcom/ibm/icu/text/DateFormat$Field;

.field public static final YEAR_WOY:Lcom/ibm/icu/text/DateFormat$Field;

.field private static final serialVersionUID:J = -0x325750c275d0f8cdL


# instance fields
.field private final calendarField:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 1604
    new-instance v0, Lcom/ibm/icu/util/GregorianCalendar;

    invoke-direct {v0}, Lcom/ibm/icu/util/GregorianCalendar;-><init>()V

    .line 1605
    .local v0, "cal":Lcom/ibm/icu/util/GregorianCalendar;
    invoke-virtual {v0}, Lcom/ibm/icu/util/GregorianCalendar;->getFieldCount()I

    move-result v1

    sput v1, Lcom/ibm/icu/text/DateFormat$Field;->CAL_FIELD_COUNT:I

    .line 1606
    sget v1, Lcom/ibm/icu/text/DateFormat$Field;->CAL_FIELD_COUNT:I

    new-array v1, v1, [Lcom/ibm/icu/text/DateFormat$Field;

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->CAL_FIELDS:[Lcom/ibm/icu/text/DateFormat$Field;

    .line 1607
    new-instance v1, Ljava/util/HashMap;

    sget v2, Lcom/ibm/icu/text/DateFormat$Field;->CAL_FIELD_COUNT:I

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->FIELD_NAME_MAP:Ljava/util/Map;

    .line 1616
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "am pm"

    const/16 v3, 0x9

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->AM_PM:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1622
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "day of month"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->DAY_OF_MONTH:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1628
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "day of week"

    const/4 v3, 0x7

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->DAY_OF_WEEK:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1634
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "day of week in month"

    const/16 v3, 0x8

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->DAY_OF_WEEK_IN_MONTH:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1640
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "day of year"

    const/4 v3, 0x6

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->DAY_OF_YEAR:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1646
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "era"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->ERA:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1652
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "hour of day"

    const/16 v3, 0xb

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->HOUR_OF_DAY0:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1658
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "hour of day 1"

    invoke-direct {v1, v2, v4}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->HOUR_OF_DAY1:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1664
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "hour"

    const/16 v3, 0xa

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->HOUR0:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1670
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "hour 1"

    invoke-direct {v1, v2, v4}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->HOUR1:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1676
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "millisecond"

    const/16 v3, 0xe

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->MILLISECOND:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1682
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "minute"

    const/16 v3, 0xc

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->MINUTE:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1688
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "month"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->MONTH:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1694
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "second"

    const/16 v3, 0xd

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->SECOND:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1700
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "time zone"

    invoke-direct {v1, v2, v4}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->TIME_ZONE:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1706
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "week of month"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->WEEK_OF_MONTH:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1712
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "week of year"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->WEEK_OF_YEAR:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1718
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "year"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->YEAR:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1727
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "local day of week"

    const/16 v3, 0x12

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->DOW_LOCAL:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1733
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "extended year"

    const/16 v3, 0x13

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->EXTENDED_YEAR:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1739
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "Julian day"

    const/16 v3, 0x14

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->JULIAN_DAY:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1745
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "milliseconds in day"

    const/16 v3, 0x15

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->MILLISECONDS_IN_DAY:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1751
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "year for week of year"

    const/16 v3, 0x11

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->YEAR_WOY:Lcom/ibm/icu/text/DateFormat$Field;

    .line 1757
    new-instance v1, Lcom/ibm/icu/text/DateFormat$Field;

    const-string/jumbo v2, "quarter"

    invoke-direct {v1, v2, v4}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/ibm/icu/text/DateFormat$Field;->QUARTER:Lcom/ibm/icu/text/DateFormat$Field;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "calendarField"    # I

    .prologue
    .line 1782
    invoke-direct {p0, p1}, Ljava/text/Format$Field;-><init>(Ljava/lang/String;)V

    .line 1783
    iput p2, p0, Lcom/ibm/icu/text/DateFormat$Field;->calendarField:I

    .line 1784
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sget-object v0, Lcom/ibm/icu/text/DateFormat;->class$com$ibm$icu$text$DateFormat$Field:Ljava/lang/Class;

    if-nez v0, :cond_1

    const-string/jumbo v0, "com.ibm.icu.text.DateFormat$Field"

    invoke-static {v0}, Lcom/ibm/icu/text/DateFormat;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/ibm/icu/text/DateFormat;->class$com$ibm$icu$text$DateFormat$Field:Ljava/lang/Class;

    :goto_0
    if-ne v1, v0, :cond_0

    .line 1785
    sget-object v0, Lcom/ibm/icu/text/DateFormat$Field;->FIELD_NAME_MAP:Ljava/util/Map;

    invoke-interface {v0, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1786
    if-ltz p2, :cond_0

    sget v0, Lcom/ibm/icu/text/DateFormat$Field;->CAL_FIELD_COUNT:I

    if-ge p2, v0, :cond_0

    .line 1787
    sget-object v0, Lcom/ibm/icu/text/DateFormat$Field;->CAL_FIELDS:[Lcom/ibm/icu/text/DateFormat$Field;

    aput-object p0, v0, p2

    .line 1790
    :cond_0
    return-void

    .line 1784
    :cond_1
    sget-object v0, Lcom/ibm/icu/text/DateFormat;->class$com$ibm$icu$text$DateFormat$Field:Ljava/lang/Class;

    goto :goto_0
.end method

.method public static ofCalendarField(I)Lcom/ibm/icu/text/DateFormat$Field;
    .locals 2
    .param p0, "calendarField"    # I

    .prologue
    .line 1806
    if-ltz p0, :cond_0

    sget v0, Lcom/ibm/icu/text/DateFormat$Field;->CAL_FIELD_COUNT:I

    if-lt p0, v0, :cond_1

    .line 1807
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Calendar field number is out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1809
    :cond_1
    sget-object v0, Lcom/ibm/icu/text/DateFormat$Field;->CAL_FIELDS:[Lcom/ibm/icu/text/DateFormat$Field;

    aget-object v0, v0, p0

    return-object v0
.end method


# virtual methods
.method public getCalendarField()I
    .locals 1

    .prologue
    .line 1822
    iget v0, p0, Lcom/ibm/icu/text/DateFormat$Field;->calendarField:I

    return v0
.end method

.method protected readResolve()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InvalidObjectException;
        }
    .end annotation

    .prologue
    .line 1834
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    sget-object v1, Lcom/ibm/icu/text/DateFormat;->class$com$ibm$icu$text$DateFormat$Field:Ljava/lang/Class;

    if-nez v1, :cond_0

    const-string/jumbo v1, "com.ibm.icu.text.DateFormat$Field"

    invoke-static {v1}, Lcom/ibm/icu/text/DateFormat;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/ibm/icu/text/DateFormat;->class$com$ibm$icu$text$DateFormat$Field:Ljava/lang/Class;

    :goto_0
    if-eq v2, v1, :cond_1

    .line 1835
    new-instance v1, Ljava/io/InvalidObjectException;

    const-string/jumbo v2, "A subclass of DateFormat.Field must implement readResolve."

    invoke-direct {v1, v2}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1834
    :cond_0
    sget-object v1, Lcom/ibm/icu/text/DateFormat;->class$com$ibm$icu$text$DateFormat$Field:Ljava/lang/Class;

    goto :goto_0

    .line 1838
    :cond_1
    sget-object v1, Lcom/ibm/icu/text/DateFormat$Field;->FIELD_NAME_MAP:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/ibm/icu/text/DateFormat$Field;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1840
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_2

    .line 1841
    new-instance v1, Ljava/io/InvalidObjectException;

    const-string/jumbo v2, "Unknown attribute name."

    invoke-direct {v1, v2}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1844
    :cond_2
    return-object v0
.end method
