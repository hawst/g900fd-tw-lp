.class final Lcom/ibm/icu/text/NumberFormatServiceShim$NFFactory;
.super Lcom/ibm/icu/impl/ICULocaleService$LocaleKeyFactory;
.source "NumberFormatServiceShim.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/NumberFormatServiceShim;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "NFFactory"
.end annotation


# instance fields
.field private delegate:Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;


# direct methods
.method constructor <init>(Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;)V
    .locals 1
    .param p1, "delegate"    # Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;

    .prologue
    .line 46
    invoke-virtual {p1}, Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;->visible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKeyFactory;-><init>(Z)V

    .line 48
    iput-object p1, p0, Lcom/ibm/icu/text/NumberFormatServiceShim$NFFactory;->delegate:Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;

    .line 49
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public create(Lcom/ibm/icu/impl/ICUService$Key;Lcom/ibm/icu/impl/ICUService;)Ljava/lang/Object;
    .locals 6
    .param p1, "key"    # Lcom/ibm/icu/impl/ICUService$Key;
    .param p2, "srvc"    # Lcom/ibm/icu/impl/ICUService;

    .prologue
    const/4 v4, 0x0

    .line 52
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/NumberFormatServiceShim$NFFactory;->handlesKey(Lcom/ibm/icu/impl/ICUService$Key;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v1, p1

    .line 53
    check-cast v1, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;

    .line 54
    .local v1, "lkey":Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;
    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->canonicalLocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v2

    .line 55
    .local v2, "loc":Lcom/ibm/icu/util/ULocale;
    invoke-virtual {v1}, Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;->kind()I

    move-result v0

    .line 57
    .local v0, "kind":I
    iget-object v5, p0, Lcom/ibm/icu/text/NumberFormatServiceShim$NFFactory;->delegate:Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;

    invoke-virtual {v5, v2, v0}, Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;->createFormat(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v3

    .line 58
    .local v3, "result":Lcom/ibm/icu/text/NumberFormat;
    if-nez v3, :cond_0

    .line 59
    invoke-virtual {p2, p1, v4, p0}, Lcom/ibm/icu/impl/ICUService;->getKey(Lcom/ibm/icu/impl/ICUService$Key;[Ljava/lang/String;Lcom/ibm/icu/impl/ICUService$Factory;)Ljava/lang/Object;

    move-result-object v3

    .line 63
    .end local v0    # "kind":I
    .end local v1    # "lkey":Lcom/ibm/icu/impl/ICULocaleService$LocaleKey;
    .end local v2    # "loc":Lcom/ibm/icu/util/ULocale;
    .end local v3    # "result":Lcom/ibm/icu/text/NumberFormat;
    :cond_0
    :goto_0
    return-object v3

    :cond_1
    move-object v3, v4

    goto :goto_0
.end method

.method protected getSupportedIDs()Ljava/util/Set;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/ibm/icu/text/NumberFormatServiceShim$NFFactory;->delegate:Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;

    invoke-virtual {v0}, Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;->getSupportedLocaleNames()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
