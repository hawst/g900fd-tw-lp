.class Lcom/ibm/icu/text/Quantifier;
.super Ljava/lang/Object;
.source "Quantifier.java"

# interfaces
.implements Lcom/ibm/icu/text/UnicodeMatcher;


# static fields
.field public static final MAX:I = 0x7fffffff


# instance fields
.field private matcher:Lcom/ibm/icu/text/UnicodeMatcher;

.field private maxCount:I

.field private minCount:I


# direct methods
.method public constructor <init>(Lcom/ibm/icu/text/UnicodeMatcher;II)V
    .locals 2
    .param p1, "theMatcher"    # Lcom/ibm/icu/text/UnicodeMatcher;
    .param p2, "theMinCount"    # I
    .param p3, "theMaxCount"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/Quantifier;->minCount:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/Quantifier;->maxCount:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/Quantifier;->minCount:I

    iget v1, p0, Lcom/ibm/icu/text/Quantifier;->maxCount:I

    if-le v0, v1, :cond_1

    .line 26
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 28
    :cond_1
    iput-object p1, p0, Lcom/ibm/icu/text/Quantifier;->matcher:Lcom/ibm/icu/text/UnicodeMatcher;

    .line 29
    iput p2, p0, Lcom/ibm/icu/text/Quantifier;->minCount:I

    .line 30
    iput p3, p0, Lcom/ibm/icu/text/Quantifier;->maxCount:I

    .line 31
    return-void
.end method


# virtual methods
.method public addMatchSetTo(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 1
    .param p1, "toUnionTo"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 109
    iget v0, p0, Lcom/ibm/icu/text/Quantifier;->maxCount:I

    if-lez v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/ibm/icu/text/Quantifier;->matcher:Lcom/ibm/icu/text/UnicodeMatcher;

    invoke-interface {v0, p1}, Lcom/ibm/icu/text/UnicodeMatcher;->addMatchSetTo(Lcom/ibm/icu/text/UnicodeSet;)V

    .line 112
    :cond_0
    return-void
.end method

.method public matches(Lcom/ibm/icu/text/Replaceable;[IIZ)I
    .locals 8
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "offset"    # [I
    .param p3, "limit"    # I
    .param p4, "incremental"    # Z

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 40
    aget v3, p2, v6

    .line 41
    .local v3, "start":I
    const/4 v0, 0x0

    .line 42
    .local v0, "count":I
    :cond_0
    iget v7, p0, Lcom/ibm/icu/text/Quantifier;->maxCount:I

    if-ge v0, v7, :cond_1

    .line 43
    aget v2, p2, v6

    .line 44
    .local v2, "pos":I
    iget-object v7, p0, Lcom/ibm/icu/text/Quantifier;->matcher:Lcom/ibm/icu/text/UnicodeMatcher;

    invoke-interface {v7, p1, p2, p3, p4}, Lcom/ibm/icu/text/UnicodeMatcher;->matches(Lcom/ibm/icu/text/Replaceable;[IIZ)I

    move-result v1

    .line 45
    .local v1, "m":I
    if-ne v1, v5, :cond_2

    .line 46
    add-int/lit8 v0, v0, 0x1

    .line 47
    aget v7, p2, v6

    if-ne v2, v7, :cond_0

    .line 58
    .end local v1    # "m":I
    .end local v2    # "pos":I
    :cond_1
    if-eqz p4, :cond_3

    aget v7, p2, v6

    if-ne v7, p3, :cond_3

    .line 65
    :goto_0
    return v4

    .line 52
    .restart local v1    # "m":I
    .restart local v2    # "pos":I
    :cond_2
    if-eqz p4, :cond_1

    if-ne v1, v4, :cond_1

    goto :goto_0

    .line 61
    .end local v1    # "m":I
    .end local v2    # "pos":I
    :cond_3
    iget v4, p0, Lcom/ibm/icu/text/Quantifier;->minCount:I

    if-lt v0, v4, :cond_4

    move v4, v5

    .line 62
    goto :goto_0

    .line 64
    :cond_4
    aput v3, p2, v6

    move v4, v6

    .line 65
    goto :goto_0
.end method

.method public matchesIndexValue(I)Z
    .locals 1
    .param p1, "v"    # I

    .prologue
    .line 98
    iget v0, p0, Lcom/ibm/icu/text/Quantifier;->minCount:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/Quantifier;->matcher:Lcom/ibm/icu/text/UnicodeMatcher;

    invoke-interface {v0, p1}, Lcom/ibm/icu/text/UnicodeMatcher;->matchesIndexValue(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toPattern(Z)Ljava/lang/String;
    .locals 4
    .param p1, "escapeUnprintable"    # Z

    .prologue
    const/4 v3, 0x1

    const v2, 0x7fffffff

    .line 72
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 73
    .local v0, "result":Ljava/lang/StringBuffer;
    iget-object v1, p0, Lcom/ibm/icu/text/Quantifier;->matcher:Lcom/ibm/icu/text/UnicodeMatcher;

    invoke-interface {v1, p1}, Lcom/ibm/icu/text/UnicodeMatcher;->toPattern(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 74
    iget v1, p0, Lcom/ibm/icu/text/Quantifier;->minCount:I

    if-nez v1, :cond_1

    .line 75
    iget v1, p0, Lcom/ibm/icu/text/Quantifier;->maxCount:I

    if-ne v1, v3, :cond_0

    .line 76
    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 91
    :goto_0
    return-object v1

    .line 77
    :cond_0
    iget v1, p0, Lcom/ibm/icu/text/Quantifier;->maxCount:I

    if-ne v1, v2, :cond_2

    .line 78
    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 81
    :cond_1
    iget v1, p0, Lcom/ibm/icu/text/Quantifier;->minCount:I

    if-ne v1, v3, :cond_2

    iget v1, p0, Lcom/ibm/icu/text/Quantifier;->maxCount:I

    if-ne v1, v2, :cond_2

    .line 82
    const/16 v1, 0x2b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 84
    :cond_2
    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 85
    iget v1, p0, Lcom/ibm/icu/text/Quantifier;->minCount:I

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/Utility;->appendNumber(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 86
    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 87
    iget v1, p0, Lcom/ibm/icu/text/Quantifier;->maxCount:I

    if-eq v1, v2, :cond_3

    .line 88
    iget v1, p0, Lcom/ibm/icu/text/Quantifier;->maxCount:I

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/Utility;->appendNumber(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 90
    :cond_3
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 91
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
