.class abstract Lcom/ibm/icu/text/CharsetRecog_mbcs$CharsetRecog_euc;
.super Lcom/ibm/icu/text/CharsetRecog_mbcs;
.source "CharsetRecog_mbcs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CharsetRecog_mbcs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "CharsetRecog_euc"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/CharsetRecog_mbcs$CharsetRecog_euc$CharsetRecog_euc_kr;,
        Lcom/ibm/icu/text/CharsetRecog_mbcs$CharsetRecog_euc$CharsetRecog_euc_jp;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 312
    invoke-direct {p0}, Lcom/ibm/icu/text/CharsetRecog_mbcs;-><init>()V

    .line 414
    return-void
.end method


# virtual methods
.method nextChar(Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;Lcom/ibm/icu/text/CharsetDetector;)Z
    .locals 7
    .param p1, "it"    # Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;
    .param p2, "det"    # Lcom/ibm/icu/text/CharsetDetector;

    .prologue
    const/4 v4, 0x0

    const/16 v6, 0xa1

    const/4 v3, 0x1

    .line 321
    iget v5, p1, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->nextIndex:I

    iput v5, p1, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->index:I

    .line 322
    iput-boolean v4, p1, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->error:Z

    .line 323
    const/4 v0, 0x0

    .line 324
    .local v0, "firstByte":I
    const/4 v1, 0x0

    .line 325
    .local v1, "secondByte":I
    const/4 v2, 0x0

    .line 329
    .local v2, "thirdByte":I
    invoke-virtual {p1, p2}, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->nextByte(Lcom/ibm/icu/text/CharsetDetector;)I

    move-result v0

    .end local v0    # "firstByte":I
    iput v0, p1, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->charValue:I

    .line 330
    .restart local v0    # "firstByte":I
    if-gez v0, :cond_1

    .line 332
    iput-boolean v3, p1, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->done:Z

    .line 374
    :cond_0
    :goto_0
    iget-boolean v5, p1, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->done:Z

    if-nez v5, :cond_4

    :goto_1
    return v3

    .line 335
    :cond_1
    const/16 v5, 0x8d

    if-le v0, v5, :cond_0

    .line 340
    invoke-virtual {p1, p2}, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->nextByte(Lcom/ibm/icu/text/CharsetDetector;)I

    move-result v1

    .line 341
    iget v5, p1, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->charValue:I

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v5, v1

    iput v5, p1, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->charValue:I

    .line 343
    if-lt v0, v6, :cond_2

    const/16 v5, 0xfe

    if-gt v0, v5, :cond_2

    .line 345
    if-ge v1, v6, :cond_0

    .line 346
    iput-boolean v3, p1, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->error:Z

    goto :goto_0

    .line 350
    :cond_2
    const/16 v5, 0x8e

    if-ne v0, v5, :cond_3

    .line 357
    if-ge v1, v6, :cond_0

    .line 358
    iput-boolean v3, p1, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->error:Z

    goto :goto_0

    .line 363
    :cond_3
    const/16 v5, 0x8f

    if-ne v0, v5, :cond_0

    .line 366
    invoke-virtual {p1, p2}, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->nextByte(Lcom/ibm/icu/text/CharsetDetector;)I

    move-result v2

    .line 367
    iget v5, p1, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->charValue:I

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v5, v2

    iput v5, p1, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->charValue:I

    .line 368
    if-ge v2, v6, :cond_0

    .line 369
    iput-boolean v3, p1, Lcom/ibm/icu/text/CharsetRecog_mbcs$iteratedChar;->error:Z

    goto :goto_0

    :cond_4
    move v3, v4

    .line 374
    goto :goto_1
.end method
