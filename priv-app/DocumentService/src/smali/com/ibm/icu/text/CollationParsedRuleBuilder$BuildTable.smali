.class final Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;
.super Ljava/lang/Object;
.source "CollationParsedRuleBuilder.java"

# interfaces
.implements Lcom/ibm/icu/impl/TrieBuilder$DataManipulate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CollationParsedRuleBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BuildTable"
.end annotation


# instance fields
.field cmLookup:Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;

.field m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

.field m_contrEndCP_:[B

.field m_contractions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

.field m_expansions_:Ljava/util/Vector;

.field m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

.field m_maxExpansions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;

.field m_maxJamoExpansions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;

.field m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

.field m_prefixLookup_:Ljava/util/Hashtable;

.field m_unsafeCP_:[B


# direct methods
.method constructor <init>(Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;)V
    .locals 4
    .param p1, "table"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;

    .prologue
    const/4 v3, 0x0

    .line 1097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->cmLookup:Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;

    .line 1098
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    .line 1099
    new-instance v0, Lcom/ibm/icu/impl/IntTrieBuilder;

    iget-object v1, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    invoke-direct {v0, v1}, Lcom/ibm/icu/impl/IntTrieBuilder;-><init>(Lcom/ibm/icu/impl/IntTrieBuilder;)V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    .line 1100
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_expansions_:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_expansions_:Ljava/util/Vector;

    .line 1101
    new-instance v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    iget-object v1, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contractions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;-><init>(Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;)V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contractions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    .line 1102
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contractions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    iput-object v1, v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    .line 1103
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    .line 1104
    new-instance v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;

    iget-object v1, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_maxExpansions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;-><init>(Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;)V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_maxExpansions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;

    .line 1105
    new-instance v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;

    iget-object v1, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_maxJamoExpansions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;-><init>(Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;)V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_maxJamoExpansions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;

    .line 1107
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    array-length v0, v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    .line 1108
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1110
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contrEndCP_:[B

    array-length v0, v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contrEndCP_:[B

    .line 1111
    iget-object v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contrEndCP_:[B

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contrEndCP_:[B

    iget-object v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contrEndCP_:[B

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1113
    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/text/CollationRuleParser;)V
    .locals 11
    .param p1, "parser"    # Lcom/ibm/icu/text/CollationRuleParser;

    .prologue
    const/4 v1, 0x0

    const/16 v10, 0x420

    const/4 v9, 0x0

    .line 1056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128
    iput-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->cmLookup:Lcom/ibm/icu/text/CollationParsedRuleBuilder$CombinClassTable;

    .line 1057
    new-instance v0, Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-direct {v0}, Lcom/ibm/icu/text/RuleBasedCollator;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    .line 1058
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->setWithUCAData()V

    .line 1059
    new-instance v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;

    invoke-direct {v7}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;-><init>()V

    .line 1060
    .local v7, "maxet":Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;
    new-instance v8, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;

    invoke-direct {v8}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;-><init>()V

    .line 1061
    .local v8, "maxjet":Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;
    iget-object v0, p1, Lcom/ibm/icu/text/CollationRuleParser;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_options_:Lcom/ibm/icu/text/CollationRuleParser$OptionSet;

    .line 1062
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_expansions_:Ljava/util/Vector;

    .line 1065
    const/high16 v3, -0x10000000

    .line 1068
    .local v3, "trieinitialvalue":I
    new-instance v0, Lcom/ibm/icu/impl/IntTrieBuilder;

    const/high16 v2, 0x30000

    const/4 v5, 0x1

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/impl/IntTrieBuilder;-><init>([IIIIZ)V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    .line 1070
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_prefixLookup_:Ljava/util/Hashtable;

    .line 1072
    new-instance v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    iget-object v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;-><init>(Lcom/ibm/icu/impl/IntTrieBuilder;)V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contractions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$ContractionTable;

    .line 1074
    iput-object v7, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_maxExpansions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;

    .line 1076
    const/4 v6, 0x0

    .line 1077
    .local v6, "i":I
    :goto_0
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    array-length v0, v0

    if-ge v6, v0, :cond_0

    .line 1078
    iget-object v0, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_endExpansionCE_:Ljava/util/Vector;

    new-instance v1, Ljava/lang/Integer;

    sget-object v2, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v2, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    aget v2, v2, v6

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1080
    iget-object v0, v7, Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxExpansionTable;->m_expansionCESize_:Ljava/util/Vector;

    new-instance v1, Ljava/lang/Byte;

    sget-object v2, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v2, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCEMaxSize_:[B

    aget-byte v2, v2, v6

    invoke-direct {v1, v2}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1077
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1083
    :cond_0
    iput-object v8, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_maxJamoExpansions_:Lcom/ibm/icu/text/CollationParsedRuleBuilder$MaxJamoExpansionTable;

    .line 1085
    new-array v0, v10, [B

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    .line 1086
    new-array v0, v10, [B

    iput-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contrEndCP_:[B

    .line 1087
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_unsafeCP_:[B

    invoke-static {v0, v9}, Ljava/util/Arrays;->fill([BB)V

    .line 1088
    iget-object v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_contrEndCP_:[B

    invoke-static {v0, v9}, Ljava/util/Arrays;->fill([BB)V

    .line 1089
    return-void
.end method


# virtual methods
.method public getFoldedValue(II)I
    .locals 5
    .param p1, "cp"    # I
    .param p2, "offset"    # I

    .prologue
    .line 1026
    add-int/lit16 v1, p1, 0x400

    .line 1027
    .local v1, "limit":I
    :goto_0
    if-ge p1, v1, :cond_3

    .line 1028
    iget-object v4, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    invoke-virtual {v4, p1}, Lcom/ibm/icu/impl/IntTrieBuilder;->getValue(I)I

    move-result v3

    .line 1029
    .local v3, "value":I
    iget-object v4, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$BuildTable;->m_mapping_:Lcom/ibm/icu/impl/IntTrieBuilder;

    invoke-virtual {v4, p1}, Lcom/ibm/icu/impl/IntTrieBuilder;->isInZeroBlock(I)Z

    move-result v0

    .line 1030
    .local v0, "inBlockZero":Z
    invoke-static {v3}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$200(I)I

    move-result v2

    .line 1031
    .local v2, "tag":I
    const/4 v4, 0x1

    if-ne v0, v4, :cond_0

    .line 1032
    add-int/lit8 p1, p1, 0x20

    .line 1033
    goto :goto_0

    .line 1034
    :cond_0
    invoke-static {v3}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->access$300(I)Z

    move-result v4

    if-eqz v4, :cond_1

    const/16 v4, 0xa

    if-eq v2, v4, :cond_2

    if-eqz v2, :cond_2

    .line 1040
    :cond_1
    const/high16 v4, -0xb000000

    or-int/2addr v4, p2

    .line 1047
    .end local v0    # "inBlockZero":Z
    .end local v2    # "tag":I
    .end local v3    # "value":I
    :goto_1
    return v4

    .line 1044
    .restart local v0    # "inBlockZero":Z
    .restart local v2    # "tag":I
    .restart local v3    # "value":I
    :cond_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 1047
    .end local v0    # "inBlockZero":Z
    .end local v2    # "tag":I
    .end local v3    # "value":I
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method
