.class Lcom/ibm/icu/text/RuleBasedCollator$contContext;
.super Ljava/lang/Object;
.source "RuleBasedCollator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/RuleBasedCollator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "contContext"
.end annotation


# instance fields
.field addPrefixes:Z

.field coll:Lcom/ibm/icu/text/RuleBasedCollator;

.field contractions:Lcom/ibm/icu/text/UnicodeSet;

.field expansions:Lcom/ibm/icu/text/UnicodeSet;

.field removedContractions:Lcom/ibm/icu/text/UnicodeSet;

.field private final this$0:Lcom/ibm/icu/text/RuleBasedCollator;


# direct methods
.method constructor <init>(Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/UnicodeSet;Lcom/ibm/icu/text/UnicodeSet;Lcom/ibm/icu/text/UnicodeSet;Z)V
    .locals 0
    .param p2, "coll"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p3, "contractions"    # Lcom/ibm/icu/text/UnicodeSet;
    .param p4, "expansions"    # Lcom/ibm/icu/text/UnicodeSet;
    .param p5, "removedContractions"    # Lcom/ibm/icu/text/UnicodeSet;
    .param p6, "addPrefixes"    # Z

    .prologue
    .line 751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->this$0:Lcom/ibm/icu/text/RuleBasedCollator;

    .line 752
    iput-object p2, p0, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->coll:Lcom/ibm/icu/text/RuleBasedCollator;

    .line 753
    iput-object p3, p0, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->contractions:Lcom/ibm/icu/text/UnicodeSet;

    .line 754
    iput-object p4, p0, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->expansions:Lcom/ibm/icu/text/UnicodeSet;

    .line 755
    iput-object p5, p0, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->removedContractions:Lcom/ibm/icu/text/UnicodeSet;

    .line 756
    iput-boolean p6, p0, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->addPrefixes:Z

    .line 757
    return-void
.end method
