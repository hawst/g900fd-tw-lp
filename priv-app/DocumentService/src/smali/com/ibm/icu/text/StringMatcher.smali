.class Lcom/ibm/icu/text/StringMatcher;
.super Ljava/lang/Object;
.source "StringMatcher.java"

# interfaces
.implements Lcom/ibm/icu/text/UnicodeMatcher;
.implements Lcom/ibm/icu/text/UnicodeReplacer;


# instance fields
.field private final data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

.field private matchLimit:I

.field private matchStart:I

.field private pattern:Ljava/lang/String;

.field private segmentNumber:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IIILcom/ibm/icu/text/RuleBasedTransliterator$Data;)V
    .locals 1
    .param p1, "theString"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "limit"    # I
    .param p4, "segmentNum"    # I
    .param p5, "theData"    # Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .prologue
    .line 93
    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p4, p5}, Lcom/ibm/icu/text/StringMatcher;-><init>(Ljava/lang/String;ILcom/ibm/icu/text/RuleBasedTransliterator$Data;)V

    .line 94
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILcom/ibm/icu/text/RuleBasedTransliterator$Data;)V
    .locals 1
    .param p1, "theString"    # Ljava/lang/String;
    .param p2, "segmentNum"    # I
    .param p3, "theData"    # Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p3, p0, Lcom/ibm/icu/text/StringMatcher;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .line 70
    iput-object p1, p0, Lcom/ibm/icu/text/StringMatcher;->pattern:Ljava/lang/String;

    .line 71
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/StringMatcher;->matchLimit:I

    iput v0, p0, Lcom/ibm/icu/text/StringMatcher;->matchStart:I

    .line 72
    iput p2, p0, Lcom/ibm/icu/text/StringMatcher;->segmentNumber:I

    .line 73
    return-void
.end method


# virtual methods
.method public addMatchSetTo(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 4
    .param p1, "toUnionTo"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 220
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/ibm/icu/text/StringMatcher;->pattern:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 221
    iget-object v3, p0, Lcom/ibm/icu/text/StringMatcher;->pattern:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 222
    .local v0, "ch":I
    iget-object v3, p0, Lcom/ibm/icu/text/StringMatcher;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-virtual {v3, v0}, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->lookupMatcher(I)Lcom/ibm/icu/text/UnicodeMatcher;

    move-result-object v2

    .line 223
    .local v2, "matcher":Lcom/ibm/icu/text/UnicodeMatcher;
    if-nez v2, :cond_0

    .line 224
    invoke-virtual {p1, v0}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 220
    :goto_1
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_0

    .line 226
    :cond_0
    invoke-interface {v2, p1}, Lcom/ibm/icu/text/UnicodeMatcher;->addMatchSetTo(Lcom/ibm/icu/text/UnicodeSet;)V

    goto :goto_1

    .line 229
    .end local v0    # "ch":I
    .end local v2    # "matcher":Lcom/ibm/icu/text/UnicodeMatcher;
    :cond_1
    return-void
.end method

.method public addReplacementSetTo(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 0
    .param p1, "toUnionTo"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 286
    return-void
.end method

.method public matches(Lcom/ibm/icu/text/Replaceable;[IIZ)I
    .locals 9
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "offset"    # [I
    .param p3, "limit"    # I
    .param p4, "incremental"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v5, 0x0

    .line 108
    new-array v0, v6, [I

    aget v8, p2, v5

    aput v8, v0, v5

    .line 109
    .local v0, "cursor":[I
    aget v8, v0, v5

    if-ge p3, v8, :cond_5

    .line 111
    iget-object v6, p0, Lcom/ibm/icu/text/StringMatcher;->pattern:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v1, v6, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_3

    .line 112
    iget-object v6, p0, Lcom/ibm/icu/text/StringMatcher;->pattern:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 113
    .local v2, "keyChar":C
    iget-object v6, p0, Lcom/ibm/icu/text/StringMatcher;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-virtual {v6, v2}, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->lookupMatcher(I)Lcom/ibm/icu/text/UnicodeMatcher;

    move-result-object v4

    .line 114
    .local v4, "subm":Lcom/ibm/icu/text/UnicodeMatcher;
    if-nez v4, :cond_2

    .line 115
    aget v6, v0, v5

    if-le v6, p3, :cond_1

    aget v6, v0, v5

    invoke-interface {p1, v6}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v6

    if-ne v2, v6, :cond_1

    .line 117
    aget v6, v0, v5

    add-int/lit8 v6, v6, -0x1

    aput v6, v0, v5

    .line 111
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    move v3, v5

    .line 169
    .end local v2    # "keyChar":C
    .end local v4    # "subm":Lcom/ibm/icu/text/UnicodeMatcher;
    :goto_1
    return v3

    .line 122
    .restart local v2    # "keyChar":C
    .restart local v4    # "subm":Lcom/ibm/icu/text/UnicodeMatcher;
    :cond_2
    invoke-interface {v4, p1, v0, p3, p4}, Lcom/ibm/icu/text/UnicodeMatcher;->matches(Lcom/ibm/icu/text/Replaceable;[IIZ)I

    move-result v3

    .line 124
    .local v3, "m":I
    if-eq v3, v7, :cond_0

    goto :goto_1

    .line 132
    .end local v2    # "keyChar":C
    .end local v3    # "m":I
    .end local v4    # "subm":Lcom/ibm/icu/text/UnicodeMatcher;
    :cond_3
    iget v6, p0, Lcom/ibm/icu/text/StringMatcher;->matchStart:I

    if-gez v6, :cond_4

    .line 133
    aget v6, v0, v5

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/ibm/icu/text/StringMatcher;->matchStart:I

    .line 134
    aget v6, p2, v5

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/ibm/icu/text/StringMatcher;->matchLimit:I

    .line 168
    :cond_4
    :goto_2
    aget v6, v0, v5

    aput v6, p2, v5

    move v3, v7

    .line 169
    goto :goto_1

    .line 137
    .end local v1    # "i":I
    :cond_5
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v8, p0, Lcom/ibm/icu/text/StringMatcher;->pattern:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v1, v8, :cond_a

    .line 138
    if-eqz p4, :cond_6

    aget v8, v0, v5

    if-ne v8, p3, :cond_6

    move v3, v6

    .line 141
    goto :goto_1

    .line 143
    :cond_6
    iget-object v8, p0, Lcom/ibm/icu/text/StringMatcher;->pattern:Ljava/lang/String;

    invoke-virtual {v8, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 144
    .restart local v2    # "keyChar":C
    iget-object v8, p0, Lcom/ibm/icu/text/StringMatcher;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-virtual {v8, v2}, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->lookupMatcher(I)Lcom/ibm/icu/text/UnicodeMatcher;

    move-result-object v4

    .line 145
    .restart local v4    # "subm":Lcom/ibm/icu/text/UnicodeMatcher;
    if-nez v4, :cond_9

    .line 149
    aget v8, v0, v5

    if-ge v8, p3, :cond_8

    aget v8, v0, v5

    invoke-interface {p1, v8}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v8

    if-ne v2, v8, :cond_8

    .line 151
    aget v8, v0, v5

    add-int/lit8 v8, v8, 0x1

    aput v8, v0, v5

    .line 137
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_8
    move v3, v5

    .line 153
    goto :goto_1

    .line 156
    :cond_9
    invoke-interface {v4, p1, v0, p3, p4}, Lcom/ibm/icu/text/UnicodeMatcher;->matches(Lcom/ibm/icu/text/Replaceable;[IIZ)I

    move-result v3

    .line 158
    .restart local v3    # "m":I
    if-eq v3, v7, :cond_7

    goto :goto_1

    .line 164
    .end local v2    # "keyChar":C
    .end local v3    # "m":I
    .end local v4    # "subm":Lcom/ibm/icu/text/UnicodeMatcher;
    :cond_a
    aget v6, p2, v5

    iput v6, p0, Lcom/ibm/icu/text/StringMatcher;->matchStart:I

    .line 165
    aget v6, v0, v5

    iput v6, p0, Lcom/ibm/icu/text/StringMatcher;->matchLimit:I

    goto :goto_2
.end method

.method public matchesIndexValue(I)Z
    .locals 5
    .param p1, "v"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 204
    iget-object v4, p0, Lcom/ibm/icu/text/StringMatcher;->pattern:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 209
    :cond_0
    :goto_0
    return v2

    .line 207
    :cond_1
    iget-object v4, p0, Lcom/ibm/icu/text/StringMatcher;->pattern:Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 208
    .local v0, "c":I
    iget-object v4, p0, Lcom/ibm/icu/text/StringMatcher;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-virtual {v4, v0}, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->lookupMatcher(I)Lcom/ibm/icu/text/UnicodeMatcher;

    move-result-object v1

    .line 209
    .local v1, "m":Lcom/ibm/icu/text/UnicodeMatcher;
    if-nez v1, :cond_2

    and-int/lit16 v4, v0, 0xff

    if-eq v4, p1, :cond_0

    move v2, v3

    goto :goto_0

    :cond_2
    invoke-interface {v1, p1}, Lcom/ibm/icu/text/UnicodeMatcher;->matchesIndexValue(I)Z

    move-result v2

    goto :goto_0
.end method

.method public replace(Lcom/ibm/icu/text/Replaceable;II[I)I
    .locals 4
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "start"    # I
    .param p3, "limit"    # I
    .param p4, "cursor"    # [I

    .prologue
    .line 239
    const/4 v1, 0x0

    .line 242
    .local v1, "outLen":I
    move v0, p3

    .line 245
    .local v0, "dest":I
    iget v2, p0, Lcom/ibm/icu/text/StringMatcher;->matchStart:I

    if-ltz v2, :cond_0

    .line 246
    iget v2, p0, Lcom/ibm/icu/text/StringMatcher;->matchStart:I

    iget v3, p0, Lcom/ibm/icu/text/StringMatcher;->matchLimit:I

    if-eq v2, v3, :cond_0

    .line 247
    iget v2, p0, Lcom/ibm/icu/text/StringMatcher;->matchStart:I

    iget v3, p0, Lcom/ibm/icu/text/StringMatcher;->matchLimit:I

    invoke-interface {p1, v2, v3, v0}, Lcom/ibm/icu/text/Replaceable;->copy(III)V

    .line 248
    iget v2, p0, Lcom/ibm/icu/text/StringMatcher;->matchLimit:I

    iget v3, p0, Lcom/ibm/icu/text/StringMatcher;->matchStart:I

    sub-int v1, v2, v3

    .line 252
    :cond_0
    const-string/jumbo v2, ""

    invoke-interface {p1, p2, p3, v2}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 254
    return v1
.end method

.method public resetMatch()V
    .locals 1

    .prologue
    .line 272
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/StringMatcher;->matchLimit:I

    iput v0, p0, Lcom/ibm/icu/text/StringMatcher;->matchStart:I

    .line 273
    return-void
.end method

.method public toPattern(Z)Ljava/lang/String;
    .locals 7
    .param p1, "escapeUnprintable"    # Z

    .prologue
    const/4 v6, 0x1

    .line 176
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 177
    .local v4, "result":Ljava/lang/StringBuffer;
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 178
    .local v3, "quoteBuf":Ljava/lang/StringBuffer;
    iget v5, p0, Lcom/ibm/icu/text/StringMatcher;->segmentNumber:I

    if-lez v5, :cond_0

    .line 179
    const/16 v5, 0x28

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 181
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v5, p0, Lcom/ibm/icu/text/StringMatcher;->pattern:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 182
    iget-object v5, p0, Lcom/ibm/icu/text/StringMatcher;->pattern:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 183
    .local v1, "keyChar":C
    iget-object v5, p0, Lcom/ibm/icu/text/StringMatcher;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-virtual {v5, v1}, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->lookupMatcher(I)Lcom/ibm/icu/text/UnicodeMatcher;

    move-result-object v2

    .line 184
    .local v2, "m":Lcom/ibm/icu/text/UnicodeMatcher;
    if-nez v2, :cond_1

    .line 185
    const/4 v5, 0x0

    invoke-static {v4, v1, v5, p1, v3}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;IZZLjava/lang/StringBuffer;)V

    .line 181
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 187
    :cond_1
    invoke-interface {v2, p1}, Lcom/ibm/icu/text/UnicodeMatcher;->toPattern(Z)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v6, p1, v3}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;Ljava/lang/String;ZZLjava/lang/StringBuffer;)V

    goto :goto_1

    .line 191
    .end local v1    # "keyChar":C
    .end local v2    # "m":Lcom/ibm/icu/text/UnicodeMatcher;
    :cond_2
    iget v5, p0, Lcom/ibm/icu/text/StringMatcher;->segmentNumber:I

    if-lez v5, :cond_3

    .line 192
    const/16 v5, 0x29

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 195
    :cond_3
    const/4 v5, -0x1

    invoke-static {v4, v5, v6, p1, v3}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;IZZLjava/lang/StringBuffer;)V

    .line 197
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public toReplacerPattern(Z)Ljava/lang/String;
    .locals 4
    .param p1, "escapeUnprintable"    # Z

    .prologue
    .line 262
    new-instance v0, Ljava/lang/StringBuffer;

    const-string/jumbo v1, "$"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 263
    .local v0, "rule":Ljava/lang/StringBuffer;
    iget v1, p0, Lcom/ibm/icu/text/StringMatcher;->segmentNumber:I

    const/16 v2, 0xa

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/ibm/icu/impl/Utility;->appendNumber(Ljava/lang/StringBuffer;III)Ljava/lang/StringBuffer;

    .line 264
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
