.class public final Lcom/ibm/icu/text/UTF16$StringComparator;
.super Ljava/lang/Object;
.source "UTF16.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/UTF16;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StringComparator"
.end annotation


# static fields
.field private static final CODE_POINT_COMPARE_SURROGATE_OFFSET_:I = 0x2800

.field public static final FOLD_CASE_DEFAULT:I = 0x0

.field public static final FOLD_CASE_EXCLUDE_SPECIAL_I:I = 0x1


# instance fields
.field private m_codePointCompare_:I

.field private m_foldCase_:I

.field private m_ignoreCase_:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2520
    invoke-direct {p0, v0, v0, v0}, Lcom/ibm/icu/text/UTF16$StringComparator;-><init>(ZZI)V

    .line 2521
    return-void
.end method

.method public constructor <init>(ZZI)V
    .locals 2
    .param p1, "codepointcompare"    # Z
    .param p2, "ignorecase"    # Z
    .param p3, "foldcaseoption"    # I

    .prologue
    .line 2541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2542
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/UTF16$StringComparator;->setCodePointCompare(Z)V

    .line 2543
    iput-boolean p2, p0, Lcom/ibm/icu/text/UTF16$StringComparator;->m_ignoreCase_:Z

    .line 2544
    if-ltz p3, :cond_0

    const/4 v0, 0x1

    if-le p3, v0, :cond_1

    .line 2545
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid fold case option"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2547
    :cond_1
    iput p3, p0, Lcom/ibm/icu/text/UTF16$StringComparator;->m_foldCase_:I

    .line 2548
    return-void
.end method

.method private compareCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1, "s1"    # Ljava/lang/String;
    .param p2, "s2"    # Ljava/lang/String;

    .prologue
    .line 2737
    iget v0, p0, Lcom/ibm/icu/text/UTF16$StringComparator;->m_foldCase_:I

    iget v1, p0, Lcom/ibm/icu/text/UTF16$StringComparator;->m_codePointCompare_:I

    or-int/2addr v0, v1

    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    invoke-static {p1, p2, v0}, Lcom/ibm/icu/impl/NormalizerImpl;->cmpEquivFold(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private compareCaseSensitive(Ljava/lang/String;Ljava/lang/String;)I
    .locals 12
    .param p1, "s1"    # Ljava/lang/String;
    .param p2, "s2"    # Ljava/lang/String;

    .prologue
    const v11, 0xdbff

    const v10, 0xd800

    .line 2754
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 2755
    .local v4, "length1":I
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    .line 2756
    .local v5, "length2":I
    move v6, v4

    .line 2757
    .local v6, "minlength":I
    const/4 v7, 0x0

    .line 2758
    .local v7, "result":I
    if-ge v4, v5, :cond_2

    .line 2759
    const/4 v7, -0x1

    .line 2765
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 2766
    .local v0, "c1":C
    const/4 v1, 0x0

    .line 2767
    .local v1, "c2":C
    const/4 v3, 0x0

    .line 2768
    .local v3, "index":I
    :goto_1
    if-ge v3, v6, :cond_1

    .line 2769
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2770
    invoke-virtual {p2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 2772
    if-eq v0, v1, :cond_3

    .line 2777
    :cond_1
    if-ne v3, v6, :cond_4

    .line 2806
    .end local v7    # "result":I
    :goto_2
    return v7

    .line 2760
    .end local v0    # "c1":C
    .end local v1    # "c2":C
    .end local v3    # "index":I
    .restart local v7    # "result":I
    :cond_2
    if-le v4, v5, :cond_0

    .line 2761
    const/4 v7, 0x1

    .line 2762
    move v6, v5

    goto :goto_0

    .line 2768
    .restart local v0    # "c1":C
    .restart local v1    # "c2":C
    .restart local v3    # "index":I
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2781
    :cond_4
    iget v8, p0, Lcom/ibm/icu/text/UTF16$StringComparator;->m_codePointCompare_:I

    const v9, 0x8000

    if-ne v8, v9, :cond_9

    const/4 v2, 0x1

    .line 2783
    .local v2, "codepointcompare":Z
    :goto_3
    if-lt v0, v10, :cond_8

    if-lt v1, v10, :cond_8

    if-eqz v2, :cond_8

    .line 2787
    if-gt v0, v11, :cond_5

    add-int/lit8 v8, v3, 0x1

    if-eq v8, v4, :cond_5

    add-int/lit8 v8, v3, 0x1

    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v8

    if-nez v8, :cond_6

    :cond_5
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v8

    if-eqz v8, :cond_a

    if-eqz v3, :cond_a

    add-int/lit8 v8, v3, -0x1

    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 2796
    :cond_6
    :goto_4
    if-gt v1, v11, :cond_7

    add-int/lit8 v8, v3, 0x1

    if-eq v8, v5, :cond_7

    add-int/lit8 v8, v3, 0x1

    invoke-virtual {p2, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v8

    if-nez v8, :cond_8

    :cond_7
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v8

    if-eqz v8, :cond_b

    if-eqz v3, :cond_b

    add-int/lit8 v8, v3, -0x1

    invoke-virtual {p2, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 2806
    :cond_8
    :goto_5
    sub-int v7, v0, v1

    goto :goto_2

    .line 2781
    .end local v2    # "codepointcompare":Z
    :cond_9
    const/4 v2, 0x0

    goto :goto_3

    .line 2793
    .restart local v2    # "codepointcompare":Z
    :cond_a
    add-int/lit16 v8, v0, -0x2800

    int-to-char v0, v8

    goto :goto_4

    .line 2801
    :cond_b
    add-int/lit16 v8, v1, -0x2800

    int-to-char v1, v8

    goto :goto_5
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3
    .param p1, "a"    # Ljava/lang/Object;
    .param p2, "b"    # Ljava/lang/Object;

    .prologue
    .line 2682
    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    .local v0, "str1":Ljava/lang/String;
    move-object v1, p2

    .line 2683
    check-cast v1, Ljava/lang/String;

    .line 2685
    .local v1, "str2":Ljava/lang/String;
    if-ne v0, v1, :cond_0

    .line 2686
    const/4 v2, 0x0

    .line 2698
    :goto_0
    return v2

    .line 2688
    :cond_0
    if-nez v0, :cond_1

    .line 2689
    const/4 v2, -0x1

    goto :goto_0

    .line 2691
    :cond_1
    if-nez v1, :cond_2

    .line 2692
    const/4 v2, 0x1

    goto :goto_0

    .line 2695
    :cond_2
    iget-boolean v2, p0, Lcom/ibm/icu/text/UTF16$StringComparator;->m_ignoreCase_:Z

    if-eqz v2, :cond_3

    .line 2696
    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/UTF16$StringComparator;->compareCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 2698
    :cond_3
    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/UTF16$StringComparator;->compareCaseSensitive(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method public getCodePointCompare()Z
    .locals 2

    .prologue
    .line 2641
    iget v0, p0, Lcom/ibm/icu/text/UTF16$StringComparator;->m_codePointCompare_:I

    const v1, 0x8000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIgnoreCase()Z
    .locals 1

    .prologue
    .line 2651
    iget-boolean v0, p0, Lcom/ibm/icu/text/UTF16$StringComparator;->m_ignoreCase_:Z

    return v0
.end method

.method public getIgnoreCaseOption()I
    .locals 1

    .prologue
    .line 2663
    iget v0, p0, Lcom/ibm/icu/text/UTF16$StringComparator;->m_foldCase_:I

    return v0
.end method

.method public setCodePointCompare(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 2603
    if-eqz p1, :cond_0

    .line 2604
    const v0, 0x8000

    iput v0, p0, Lcom/ibm/icu/text/UTF16$StringComparator;->m_codePointCompare_:I

    .line 2608
    :goto_0
    return-void

    .line 2606
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/UTF16$StringComparator;->m_codePointCompare_:I

    goto :goto_0
.end method

.method public setIgnoreCase(ZI)V
    .locals 2
    .param p1, "ignorecase"    # Z
    .param p2, "foldcaseoption"    # I

    .prologue
    .line 2625
    iput-boolean p1, p0, Lcom/ibm/icu/text/UTF16$StringComparator;->m_ignoreCase_:Z

    .line 2626
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    if-le p2, v0, :cond_1

    .line 2627
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid fold case option"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2629
    :cond_1
    iput p2, p0, Lcom/ibm/icu/text/UTF16$StringComparator;->m_foldCase_:I

    .line 2630
    return-void
.end method
