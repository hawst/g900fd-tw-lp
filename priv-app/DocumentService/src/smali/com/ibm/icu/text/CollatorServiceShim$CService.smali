.class Lcom/ibm/icu/text/CollatorServiceShim$CService;
.super Lcom/ibm/icu/impl/ICULocaleService;
.source "CollatorServiceShim.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CollatorServiceShim;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CService"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 104
    const-string/jumbo v0, "Collator"

    invoke-direct {p0, v0}, Lcom/ibm/icu/impl/ICULocaleService;-><init>(Ljava/lang/String;)V

    .line 116
    new-instance v0, Lcom/ibm/icu/text/CollatorServiceShim$CService$1CollatorFactory;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/CollatorServiceShim$CService$1CollatorFactory;-><init>(Lcom/ibm/icu/text/CollatorServiceShim$CService;)V

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/CollatorServiceShim$CService;->registerFactory(Lcom/ibm/icu/impl/ICUService$Factory;)Lcom/ibm/icu/impl/ICUService$Factory;

    .line 117
    invoke-virtual {p0}, Lcom/ibm/icu/text/CollatorServiceShim$CService;->markDefault()V

    .line 118
    return-void
.end method


# virtual methods
.method protected handleDefault(Lcom/ibm/icu/impl/ICUService$Key;[Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p1, "key"    # Lcom/ibm/icu/impl/ICUService$Key;
    .param p2, "actualIDReturn"    # [Ljava/lang/String;

    .prologue
    .line 121
    if-eqz p2, :cond_0

    .line 122
    const/4 v1, 0x0

    const-string/jumbo v2, "root"

    aput-object v2, p2, v1

    .line 125
    :cond_0
    :try_start_0
    new-instance v1, Lcom/ibm/icu/text/RuleBasedCollator;

    sget-object v2, Lcom/ibm/icu/util/ULocale;->ROOT:Lcom/ibm/icu/util/ULocale;

    invoke-direct {v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;-><init>(Lcom/ibm/icu/util/ULocale;)V
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :goto_0
    return-object v1

    .line 127
    :catch_0
    move-exception v0

    .line 128
    .local v0, "e":Ljava/util/MissingResourceException;
    const/4 v1, 0x0

    goto :goto_0
.end method
