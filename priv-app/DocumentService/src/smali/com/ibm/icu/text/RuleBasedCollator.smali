.class public final Lcom/ibm/icu/text/RuleBasedCollator;
.super Lcom/ibm/icu/text/Collator;
.source "RuleBasedCollator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/RuleBasedCollator$1;,
        Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;,
        Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;,
        Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;,
        Lcom/ibm/icu/text/RuleBasedCollator$DataManipulate;,
        Lcom/ibm/icu/text/RuleBasedCollator$Attribute;,
        Lcom/ibm/icu/text/RuleBasedCollator$AttributeValue;,
        Lcom/ibm/icu/text/RuleBasedCollator$contContext;
    }
.end annotation


# static fields
.field private static final BAIL_OUT_CE_:I = -0x1000000

.field private static final BOTTOM_COUNT_2_:I = 0x40

.field static final BYTE_COMMON_:B = 0x5t

.field private static final BYTE_FIRST_NON_LATIN_PRIMARY_:B = 0x4dt

.field static final BYTE_FIRST_TAILORED_:B = 0x4t

.field private static final BYTE_SHIFT_PREFIX_:B = 0x3t

.field private static final BYTE_UNSHIFTED_MAX_:B = -0x1t

.field static final BYTE_UNSHIFTED_MIN_:B = 0x3t

.field private static final CASE_SWITCH_:I = 0xc0

.field private static final CE_BUFFER_SIZE_:I = 0x200

.field static final CE_CASE_BIT_MASK_:I = 0xc0

.field private static final CE_CASE_MASK_3_:I = 0xff

.field static final CE_CONTINUATION_MARKER_:I = 0xc0

.field private static final CE_CONTINUATION_TAG_:I = 0xc0

.field private static final CE_KEEP_CASE_:I = 0xff

.field static final CE_PRIMARY_MASK_:I = -0x10000

.field static final CE_PRIMARY_SHIFT_:I = 0x10

.field private static final CE_REMOVE_CASE_:I = 0x3f

.field private static final CE_REMOVE_CONTINUATION_MASK_:I = -0xc1

.field static final CE_SECONDARY_MASK_:I = 0xff00

.field static final CE_SECONDARY_SHIFT_:I = 0x8

.field static final CE_SPECIAL_FLAG_:I = -0x10000000

.field static final CE_SURROGATE_TAG_:I = 0x5

.field static final CE_TAG_MASK_:I = 0xf000000

.field static final CE_TAG_SHIFT_:I = 0x18

.field static final CE_TERTIARY_MASK_:I = 0xff

.field static final CODAN_PLACEHOLDER:B = 0x24t

.field private static final COMMON_2_:I = 0x5

.field static final COMMON_BOTTOM_2_:I = 0x5

.field static final COMMON_BOTTOM_3:I = 0x5

.field private static final COMMON_BOTTOM_3_:I = 0x5

.field private static final COMMON_BOTTOM_CASE_SWITCH_LOWER_3_:I = 0x5

.field private static final COMMON_BOTTOM_CASE_SWITCH_UPPER_3_:I = 0x86

.field private static final COMMON_NORMAL_3_:I = 0x5

.field static final COMMON_TOP_2_:I = 0x86

.field private static final COMMON_TOP_CASE_SWITCH_LOWER_3_:I = 0x45

.field private static final COMMON_TOP_CASE_SWITCH_OFF_3_:I = 0x85

.field private static final COMMON_TOP_CASE_SWITCH_UPPER_3_:I = 0xc5

.field private static final COMMON_UPPER_FIRST_3_:I = 0xc5

.field private static final DEFAULT_MIN_HEURISTIC_:I = 0x300

.field private static final ENDOFLATINONERANGE_:I = 0xff

.field private static final FLAG_BIT_MASK_CASE_SWITCH_OFF_:I = 0x80

.field private static final FLAG_BIT_MASK_CASE_SWITCH_ON_:I = 0x40

.field private static final HEURISTIC_MASK_:C = '\u0007'

.field private static final HEURISTIC_OVERFLOW_MASK_:C = '\u1fff'

.field private static final HEURISTIC_OVERFLOW_OFFSET_:C = '\u0100'

.field private static final HEURISTIC_SHIFT_:I = 0x3

.field private static final HEURISTIC_SIZE_:C = '\u0420'

.field private static final LAST_BYTE_MASK_:I = 0xff

.field private static final LATINONETABLELEN_:I = 0x131

.field private static final NO_CASE_SWITCH_:I = 0x0

.field private static final PROPORTION_2_:D = 0.5

.field private static final PROPORTION_3_:D = 0.667

.field private static final SORT_BUFFER_INIT_SIZE_:I = 0x80

.field private static final SORT_BUFFER_INIT_SIZE_1_:I = 0x400

.field private static final SORT_BUFFER_INIT_SIZE_2_:I = 0x80

.field private static final SORT_BUFFER_INIT_SIZE_3_:I = 0x80

.field private static final SORT_BUFFER_INIT_SIZE_4_:I = 0x80

.field private static final SORT_BUFFER_INIT_SIZE_CASE_:I = 0x20

.field private static final SORT_CASE_BYTE_START_:B = -0x80t

.field private static final SORT_CASE_SHIFT_START_:B = 0x7t

.field static final SORT_LEVEL_TERMINATOR_:B = 0x1t

.field private static final TOP_COUNT_2_:I = 0x40

.field private static final TOTAL_2_:I = 0x80

.field static final UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

.field static final UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

.field static final UCA_CONTRACTIONS_:[C

.field private static UCA_INIT_COMPLETE:Z = false

.field static final impCEGen_:Lcom/ibm/icu/impl/ImplicitCEGenerator;

.field static final maxImplicitPrimary:I = 0xe4

.field static final maxRegularPrimary:I = 0xa0

.field static final minImplicitPrimary:I = 0xe0


# instance fields
.field latinOneCEs_:[I

.field latinOneFailed_:Z

.field latinOneRegenTable_:Z

.field latinOneTableLen_:I

.field latinOneUse_:Z

.field m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

.field m_UCA_version_:Lcom/ibm/icu/util/VersionInfo;

.field m_UCD_version_:Lcom/ibm/icu/util/VersionInfo;

.field private m_addition3_:I

.field private m_bottom3_:I

.field private m_bottomCount3_:I

.field m_caseFirst_:I

.field private m_caseSwitch_:I

.field private m_common3_:I

.field m_contractionCE_:[I

.field m_contractionEnd_:[B

.field m_contractionIndex_:[C

.field m_contractionOffset_:I

.field m_defaultCaseFirst_:I

.field m_defaultDecomposition_:I

.field m_defaultIsAlternateHandlingShifted_:Z

.field m_defaultIsCaseLevel_:Z

.field m_defaultIsFrenchCollation_:Z

.field m_defaultIsHiragana4_:Z

.field m_defaultIsNumericCollation_:Z

.field m_defaultStrength_:I

.field m_defaultVariableTopValue_:I

.field m_expansionEndCEMaxSize_:[B

.field m_expansionEndCE_:[I

.field m_expansionOffset_:I

.field m_expansion_:[I

.field private m_isAlternateHandlingShifted_:Z

.field private m_isCaseLevel_:Z

.field private m_isFrenchCollation_:Z

.field m_isHiragana4_:Z

.field m_isJamoSpecial_:Z

.field m_isNumericCollation_:Z

.field private m_isSimple3_:Z

.field private m_mask3_:I

.field m_minContractionEnd_:C

.field m_minUnsafe_:C

.field private transient m_reallocLatinOneCEs_:Z

.field m_rules_:Ljava/lang/String;

.field private m_srcUtilCEBufferSize_:I

.field private m_srcUtilCEBuffer_:[I

.field private m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

.field private m_srcUtilContOffset_:I

.field private m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

.field private m_srcUtilOffset_:I

.field private m_tgtUtilCEBufferSize_:I

.field private m_tgtUtilCEBuffer_:[I

.field private m_tgtUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

.field private m_tgtUtilContOffset_:I

.field private m_tgtUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

.field private m_tgtUtilOffset_:I

.field private m_top3_:I

.field private m_topCount3_:I

.field m_trie_:Lcom/ibm/icu/impl/IntTrie;

.field m_unsafe_:[B

.field private m_utilBytes0_:[B

.field private m_utilBytes1_:[B

.field private m_utilBytes2_:[B

.field private m_utilBytes3_:[B

.field private m_utilBytes4_:[B

.field private m_utilBytesCount0_:I

.field private m_utilBytesCount1_:I

.field private m_utilBytesCount2_:I

.field private m_utilBytesCount3_:I

.field private m_utilBytesCount4_:I

.field private m_utilCompare0_:Z

.field private m_utilCompare2_:Z

.field private m_utilCompare3_:Z

.field private m_utilCompare4_:Z

.field private m_utilCompare5_:Z

.field private m_utilCount2_:I

.field private m_utilCount3_:I

.field private m_utilCount4_:I

.field private m_utilFrenchEnd_:I

.field private m_utilFrenchStart_:I

.field private m_utilRawCollationKey_:Lcom/ibm/icu/text/RawCollationKey;

.field m_variableTopValue_:I

.field m_version_:Lcom/ibm/icu/util/VersionInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    .line 1746
    const/4 v0, 0x0

    .line 1747
    .local v0, "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    const/4 v2, 0x0

    .line 1748
    .local v2, "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    const/4 v4, 0x0

    .line 1749
    .local v4, "iUCA_CONTRACTIONS_":[C
    const/4 v5, 0x0

    .line 1757
    .local v5, "iimpCEGen_":Lcom/ibm/icu/impl/ImplicitCEGenerator;
    :try_start_0
    new-instance v1, Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-direct {v1}, Lcom/ibm/icu/text/RuleBasedCollator;-><init>()V
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1758
    .end local v0    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .local v1, "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    :try_start_1
    new-instance v3, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    invoke-direct {v3}, Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;-><init>()V
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1759
    .end local v2    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    .local v3, "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    :try_start_2
    invoke-static {v1, v3}, Lcom/ibm/icu/text/CollatorReader;->read(Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;)[C

    move-result-object v4

    .line 1762
    new-instance v6, Lcom/ibm/icu/impl/ImplicitCEGenerator;

    const/16 v8, 0xe0

    const/16 v9, 0xe4

    invoke-direct {v6, v8, v9}, Lcom/ibm/icu/impl/ImplicitCEGenerator;-><init>(II)V
    :try_end_2
    .catch Ljava/util/MissingResourceException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1764
    .end local v5    # "iimpCEGen_":Lcom/ibm/icu/impl/ImplicitCEGenerator;
    .local v6, "iimpCEGen_":Lcom/ibm/icu/impl/ImplicitCEGenerator;
    :try_start_3
    invoke-direct {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->init()V

    .line 1765
    const-string/jumbo v8, "com/ibm/icu/impl/data/icudt40b/coll"

    sget-object v9, Lcom/ibm/icu/util/ULocale;->ENGLISH:Lcom/ibm/icu/util/ULocale;

    invoke-static {v8, v9}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v7

    check-cast v7, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 1766
    .local v7, "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v8, "UCARules"

    invoke-virtual {v7, v8}, Lcom/ibm/icu/impl/ICUResourceBundle;->getObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iput-object v8, v1, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;
    :try_end_3
    .catch Ljava/util/MissingResourceException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move-object v5, v6

    .end local v6    # "iimpCEGen_":Lcom/ibm/icu/impl/ImplicitCEGenerator;
    .restart local v5    # "iimpCEGen_":Lcom/ibm/icu/impl/ImplicitCEGenerator;
    move-object v2, v3

    .end local v3    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    .restart local v2    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    move-object v0, v1

    .line 1778
    .end local v1    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .end local v7    # "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v0    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    :goto_0
    sput-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    .line 1779
    sput-object v2, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONSTANTS_:Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;

    .line 1780
    sput-object v4, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_CONTRACTIONS_:[C

    .line 1781
    sput-object v5, Lcom/ibm/icu/text/RuleBasedCollator;->impCEGen_:Lcom/ibm/icu/impl/ImplicitCEGenerator;

    .line 1783
    const/4 v8, 0x1

    sput-boolean v8, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_INIT_COMPLETE:Z

    .line 1784
    return-void

    .line 1772
    :catch_0
    move-exception v8

    goto :goto_0

    .end local v0    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .restart local v1    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    :catch_1
    move-exception v8

    move-object v0, v1

    .end local v1    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .restart local v0    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    goto :goto_0

    .end local v0    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .end local v2    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    .restart local v1    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .restart local v3    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    :catch_2
    move-exception v8

    move-object v2, v3

    .end local v3    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    .restart local v2    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    move-object v0, v1

    .end local v1    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .restart local v0    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    goto :goto_0

    .end local v0    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .end local v2    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    .end local v5    # "iimpCEGen_":Lcom/ibm/icu/impl/ImplicitCEGenerator;
    .restart local v1    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .restart local v3    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    .restart local v6    # "iimpCEGen_":Lcom/ibm/icu/impl/ImplicitCEGenerator;
    :catch_3
    move-exception v8

    move-object v5, v6

    .end local v6    # "iimpCEGen_":Lcom/ibm/icu/impl/ImplicitCEGenerator;
    .restart local v5    # "iimpCEGen_":Lcom/ibm/icu/impl/ImplicitCEGenerator;
    move-object v2, v3

    .end local v3    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    .restart local v2    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    move-object v0, v1

    .end local v1    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .restart local v0    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    goto :goto_0

    .line 1768
    :catch_4
    move-exception v8

    goto :goto_0

    .end local v0    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .restart local v1    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    :catch_5
    move-exception v8

    move-object v0, v1

    .end local v1    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .restart local v0    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    goto :goto_0

    .end local v0    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .end local v2    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    .restart local v1    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .restart local v3    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    :catch_6
    move-exception v8

    move-object v2, v3

    .end local v3    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    .restart local v2    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    move-object v0, v1

    .end local v1    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .restart local v0    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    goto :goto_0

    .end local v0    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .end local v2    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    .end local v5    # "iimpCEGen_":Lcom/ibm/icu/impl/ImplicitCEGenerator;
    .restart local v1    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .restart local v3    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    .restart local v6    # "iimpCEGen_":Lcom/ibm/icu/impl/ImplicitCEGenerator;
    :catch_7
    move-exception v8

    move-object v5, v6

    .end local v6    # "iimpCEGen_":Lcom/ibm/icu/impl/ImplicitCEGenerator;
    .restart local v5    # "iimpCEGen_":Lcom/ibm/icu/impl/ImplicitCEGenerator;
    move-object v2, v3

    .end local v3    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    .restart local v2    # "iUCA_CONSTANTS_":Lcom/ibm/icu/text/RuleBasedCollator$UCAConstants;
    move-object v0, v1

    .end local v1    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    .restart local v0    # "iUCA_":Lcom/ibm/icu/text/RuleBasedCollator;
    goto :goto_0
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1805
    invoke-direct {p0}, Lcom/ibm/icu/text/Collator;-><init>()V

    .line 2196
    iput-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneUse_:Z

    .line 2197
    iput-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneRegenTable_:Z

    .line 2198
    iput-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneFailed_:Z

    .line 2200
    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    .line 2201
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    .line 1806
    invoke-static {}, Lcom/ibm/icu/text/RuleBasedCollator;->checkUCA()V

    .line 1807
    invoke-direct {p0, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->initUtility(Z)V

    .line 1808
    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/util/ULocale;)V
    .locals 8
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v7, 0x0

    .line 1817
    invoke-direct {p0}, Lcom/ibm/icu/text/Collator;-><init>()V

    .line 2196
    iput-boolean v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneUse_:Z

    .line 2197
    iput-boolean v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneRegenTable_:Z

    .line 2198
    iput-boolean v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneFailed_:Z

    .line 2200
    iput v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    .line 2201
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    .line 1818
    invoke-static {}, Lcom/ibm/icu/text/RuleBasedCollator;->checkUCA()V

    .line 1819
    const-string/jumbo v6, "com/ibm/icu/impl/data/icudt40b/coll"

    invoke-static {v6, p1}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 1820
    .local v4, "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-direct {p0, v7}, Lcom/ibm/icu/text/RuleBasedCollator;->initUtility(Z)V

    .line 1821
    if-eqz v4, :cond_3

    .line 1824
    :try_start_0
    const-string/jumbo v6, "collation"

    invoke-virtual {p1, v6}, Lcom/ibm/icu/util/ULocale;->getKeywordValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1825
    .local v1, "collkey":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 1826
    const-string/jumbo v6, "collations/default"

    invoke-virtual {v4, v6}, Lcom/ibm/icu/impl/ICUResourceBundle;->getStringWithFallback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1832
    :cond_0
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "collations/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v2

    .line 1833
    .local v2, "elements":Lcom/ibm/icu/impl/ICUResourceBundle;
    if-eqz v2, :cond_3

    .line 1835
    invoke-virtual {v4}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v5

    .line 1836
    .local v5, "uloc":Lcom/ibm/icu/util/ULocale;
    invoke-virtual {p0, v5, v5}, Lcom/ibm/icu/text/RuleBasedCollator;->setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V

    .line 1838
    const-string/jumbo v6, "Sequence"

    invoke-virtual {v2, v6}, Lcom/ibm/icu/impl/ICUResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    .line 1839
    const-string/jumbo v6, "%%CollationBin"

    invoke-virtual {v2, v6}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v6

    invoke-virtual {v6}, Lcom/ibm/icu/util/UResourceBundle;->getBinary()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1841
    .local v0, "buf":Ljava/nio/ByteBuffer;
    if-eqz v0, :cond_4

    .line 1843
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    .line 1844
    .local v3, "map":[B
    invoke-static {p0, v3}, Lcom/ibm/icu/text/CollatorReader;->initRBC(Lcom/ibm/icu/text/RuleBasedCollator;[B)V

    .line 1864
    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_UCA_version_:Lcom/ibm/icu/util/VersionInfo;

    sget-object v7, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v7, v7, Lcom/ibm/icu/text/RuleBasedCollator;->m_UCA_version_:Lcom/ibm/icu/util/VersionInfo;

    invoke-virtual {v6, v7}, Lcom/ibm/icu/util/VersionInfo;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_UCD_version_:Lcom/ibm/icu/util/VersionInfo;

    sget-object v7, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v7, v7, Lcom/ibm/icu/text/RuleBasedCollator;->m_UCD_version_:Lcom/ibm/icu/util/VersionInfo;

    invoke-virtual {v6, v7}, Lcom/ibm/icu/util/VersionInfo;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1866
    :cond_1
    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/ibm/icu/text/RuleBasedCollator;->init(Ljava/lang/String;)V

    .line 1884
    .end local v0    # "buf":Ljava/nio/ByteBuffer;
    .end local v1    # "collkey":Ljava/lang/String;
    .end local v2    # "elements":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v3    # "map":[B
    .end local v5    # "uloc":Lcom/ibm/icu/util/ULocale;
    :goto_0
    return-void

    .line 1869
    .restart local v0    # "buf":Ljava/nio/ByteBuffer;
    .restart local v1    # "collkey":Ljava/lang/String;
    .restart local v2    # "elements":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v3    # "map":[B
    .restart local v5    # "uloc":Lcom/ibm/icu/util/ULocale;
    :cond_2
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->init()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1878
    .end local v0    # "buf":Ljava/nio/ByteBuffer;
    .end local v1    # "collkey":Ljava/lang/String;
    .end local v2    # "elements":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v3    # "map":[B
    .end local v5    # "uloc":Lcom/ibm/icu/util/ULocale;
    :catch_0
    move-exception v6

    .line 1883
    :cond_3
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->setWithUCAData()V

    goto :goto_0

    .line 1873
    .restart local v0    # "buf":Ljava/nio/ByteBuffer;
    .restart local v1    # "collkey":Ljava/lang/String;
    .restart local v2    # "elements":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v5    # "uloc":Lcom/ibm/icu/util/ULocale;
    :cond_4
    :try_start_1
    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/ibm/icu/text/RuleBasedCollator;->init(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "rules"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 215
    invoke-direct {p0}, Lcom/ibm/icu/text/Collator;-><init>()V

    .line 2196
    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneUse_:Z

    .line 2197
    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneRegenTable_:Z

    .line 2198
    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneFailed_:Z

    .line 2200
    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    .line 2201
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    .line 216
    invoke-static {}, Lcom/ibm/icu/text/RuleBasedCollator;->checkUCA()V

    .line 217
    if-nez p1, :cond_0

    .line 218
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Collation rules can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_0
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/RuleBasedCollator;->init(Ljava/lang/String;)V

    .line 222
    return-void
.end method

.method private final addLatinOneEntry(CILcom/ibm/icu/text/RuleBasedCollator$shiftValues;)V
    .locals 9
    .param p1, "ch"    # C
    .param p2, "CE"    # I
    .param p3, "sh"    # Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;

    .prologue
    const/high16 v8, -0x1000000

    .line 4086
    const/4 v0, 0x0

    .local v0, "primary1":I
    const/4 v1, 0x0

    .local v1, "primary2":I
    const/4 v3, 0x0

    .local v3, "secondary":I
    const/4 v4, 0x0

    .line 4087
    .local v4, "tertiary":I
    const/4 v2, 0x0

    .line 4088
    .local v2, "reverseSecondary":Z
    invoke-static {p2}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v5

    if-nez v5, :cond_2

    .line 4089
    iget v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_mask3_:I

    and-int v4, p2, v5

    .line 4090
    iget v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseSwitch_:I

    xor-int/2addr v4, v5

    .line 4091
    const/4 v2, 0x1

    .line 4098
    :goto_0
    ushr-int/lit8 p2, p2, 0x8

    and-int/lit16 v3, p2, 0xff

    .line 4099
    ushr-int/lit8 p2, p2, 0x8

    and-int/lit16 v1, p2, 0xff

    .line 4100
    ushr-int/lit8 v0, p2, 0x8

    .line 4102
    if-eqz v0, :cond_0

    .line 4103
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    aget v6, v5, p1

    iget v7, p3, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->primShift:I

    shl-int v7, v0, v7

    or-int/2addr v6, v7

    aput v6, v5, p1

    .line 4104
    iget v5, p3, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->primShift:I

    add-int/lit8 v5, v5, -0x8

    iput v5, p3, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->primShift:I

    .line 4106
    :cond_0
    if-eqz v1, :cond_4

    .line 4107
    iget v5, p3, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->primShift:I

    if-gez v5, :cond_3

    .line 4108
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    aput v8, v5, p1

    .line 4109
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    add-int/2addr v6, p1

    aput v8, v5, v6

    .line 4110
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v6, p1

    aput v8, v5, v6

    .line 4129
    :cond_1
    :goto_1
    return-void

    .line 4093
    :cond_2
    and-int/lit16 v5, p2, -0xc1

    int-to-byte v4, v5

    .line 4094
    and-int/lit8 v4, v4, 0x3f

    .line 4095
    const/4 v2, 0x0

    goto :goto_0

    .line 4113
    :cond_3
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    aget v6, v5, p1

    iget v7, p3, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->primShift:I

    shl-int v7, v1, v7

    or-int/2addr v6, v7

    aput v6, v5, p1

    .line 4114
    iget v5, p3, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->primShift:I

    add-int/lit8 v5, v5, -0x8

    iput v5, p3, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->primShift:I

    .line 4116
    :cond_4
    if-eqz v3, :cond_5

    .line 4117
    if-eqz v2, :cond_6

    iget-boolean v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    if-eqz v5, :cond_6

    .line 4118
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    add-int/2addr v6, p1

    aget v7, v5, v6

    ushr-int/lit8 v7, v7, 0x8

    aput v7, v5, v6

    .line 4119
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    add-int/2addr v6, p1

    aget v7, v5, v6

    shl-int/lit8 v8, v3, 0x18

    or-int/2addr v7, v8

    aput v7, v5, v6

    .line 4123
    :goto_2
    iget v5, p3, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->secShift:I

    add-int/lit8 v5, v5, -0x8

    iput v5, p3, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->secShift:I

    .line 4125
    :cond_5
    if-eqz v4, :cond_1

    .line 4126
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v6, p1

    aget v7, v5, v6

    iget v8, p3, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->terShift:I

    shl-int v8, v4, v8

    or-int/2addr v7, v8

    aput v7, v5, v6

    .line 4127
    iget v5, p3, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->terShift:I

    add-int/lit8 v5, v5, -0x8

    iput v5, p3, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->terShift:I

    goto :goto_1

    .line 4121
    :cond_6
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    add-int/2addr v6, p1

    aget v7, v5, v6

    iget v8, p3, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->secShift:I

    shl-int v8, v3, v8

    or-int/2addr v7, v8

    aput v7, v5, v6

    goto :goto_2
.end method

.method private addSpecial(Lcom/ibm/icu/text/RuleBasedCollator$contContext;Ljava/lang/StringBuffer;I)V
    .locals 10
    .param p1, "c"    # Lcom/ibm/icu/text/RuleBasedCollator$contContext;
    .param p2, "buffer"    # Ljava/lang/StringBuffer;
    .param p3, "CE"    # I

    .prologue
    const v9, 0xffff

    const/4 v8, 0x0

    const/16 v7, 0xb

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 763
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 764
    .local v0, "b":Ljava/lang/StringBuffer;
    const v3, 0xffffff

    and-int/2addr v3, p3

    iget-object v4, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->coll:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v4, v4, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    sub-int v2, v3, v4

    .line 765
    .local v2, "offset":I
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->coll:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v3, v3, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget v1, v3, v2

    .line 767
    .local v1, "newCE":I
    const/high16 v3, -0x10000000

    if-eq v1, v3, :cond_2

    .line 768
    invoke-static {p3}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p3}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v3

    if-ne v3, v6, :cond_0

    invoke-static {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v3

    if-ne v3, v7, :cond_0

    iget-boolean v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->addPrefixes:Z

    if-eqz v3, :cond_0

    .line 771
    invoke-direct {p0, p1, p2, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->addSpecial(Lcom/ibm/icu/text/RuleBasedCollator$contContext;Ljava/lang/StringBuffer;I)V

    .line 773
    :cond_0
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-le v3, v5, :cond_2

    .line 774
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->contractions:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v3, :cond_1

    .line 775
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->contractions:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {p2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    .line 777
    :cond_1
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->expansions:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v3, :cond_2

    invoke-static {p3}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p3}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v3

    if-ne v3, v5, :cond_2

    .line 778
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->expansions:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {p2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    .line 783
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 785
    invoke-static {p3}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v3

    if-ne v3, v7, :cond_7

    iget-boolean v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->addPrefixes:Z

    if-eqz v3, :cond_7

    .line 786
    :goto_0
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->coll:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v3, v3, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v3, v3, v2

    if-eq v3, v9, :cond_c

    .line 787
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    invoke-virtual {v0, v8, v3}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 788
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 789
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->coll:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v3, v3, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget v1, v3, v2

    .line 790
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->coll:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v3, v3, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v3, v3, v2

    invoke-virtual {v0, v8, v3}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 791
    invoke-static {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v3

    if-eq v3, v6, :cond_3

    invoke-static {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v3

    if-ne v3, v7, :cond_5

    .line 792
    :cond_3
    invoke-direct {p0, p1, v0, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->addSpecial(Lcom/ibm/icu/text/RuleBasedCollator$contContext;Ljava/lang/StringBuffer;I)V

    .line 801
    :cond_4
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 802
    goto :goto_0

    .line 794
    :cond_5
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->contractions:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v3, :cond_6

    .line 795
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->contractions:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    .line 797
    :cond_6
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->expansions:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v3, :cond_4

    invoke-static {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v3

    if-ne v3, v5, :cond_4

    .line 798
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->expansions:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_1

    .line 803
    :cond_7
    invoke-static {p3}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v3

    if-ne v3, v6, :cond_c

    .line 804
    :goto_2
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->coll:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v3, v3, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v3, v3, v2

    if-eq v3, v9, :cond_c

    .line 805
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    invoke-virtual {v0, v8, v3}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 806
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 807
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->coll:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v3, v3, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget v1, v3, v2

    .line 808
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->coll:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v3, v3, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v3, v3, v2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 809
    invoke-static {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-static {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v3

    if-eq v3, v6, :cond_8

    invoke-static {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v3

    if-ne v3, v7, :cond_a

    .line 810
    :cond_8
    invoke-direct {p0, p1, v0, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->addSpecial(Lcom/ibm/icu/text/RuleBasedCollator$contContext;Ljava/lang/StringBuffer;I)V

    .line 819
    :cond_9
    :goto_3
    add-int/lit8 v2, v2, 0x1

    .line 820
    goto :goto_2

    .line 812
    :cond_a
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->contractions:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v3, :cond_b

    .line 813
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->contractions:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    .line 815
    :cond_b
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->expansions:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v3, :cond_9

    invoke-static {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-static {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v3

    if-ne v3, v5, :cond_9

    .line 816
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->expansions:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/UnicodeSet;->add(Ljava/lang/String;)Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_3

    .line 822
    :cond_c
    return-void
.end method

.method private static final append([BIB)[B
    .locals 2
    .param p0, "array"    # [B
    .param p1, "appendindex"    # I
    .param p2, "value"    # B

    .prologue
    .line 3205
    :try_start_0
    aput-byte p2, p0, p1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3211
    :goto_0
    return-object p0

    .line 3207
    :catch_0
    move-exception v0

    .line 3208
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/16 v1, 0x80

    invoke-static {p0, p1, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->increase([BII)[B

    move-result-object p0

    .line 3209
    aput-byte p2, p0, p1

    goto :goto_0
.end method

.method private static final append([III)[I
    .locals 2
    .param p0, "array"    # [I
    .param p1, "appendindex"    # I
    .param p2, "value"    # I

    .prologue
    .line 3489
    add-int/lit8 v0, p1, 0x1

    array-length v1, p0

    if-lt v0, v1, :cond_0

    .line 3490
    const/16 v0, 0x200

    invoke-static {p0, p1, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->increase([III)[I

    move-result-object p0

    .line 3492
    :cond_0
    aput p2, p0, p1

    .line 3493
    return-object p0
.end method

.method private static checkUCA()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/MissingResourceException;
        }
    .end annotation

    .prologue
    .line 1788
    sget-boolean v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_INIT_COMPLETE:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    if-nez v0, :cond_0

    .line 1789
    new-instance v0, Ljava/util/MissingResourceException;

    const-string/jumbo v1, "Collator UCA data unavailable"

    const-string/jumbo v2, ""

    const-string/jumbo v3, ""

    invoke-direct {v0, v1, v2, v3}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    .line 1791
    :cond_0
    return-void
.end method

.method private final compareBySortKeys(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    .line 3224
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilRawCollationKey_:Lcom/ibm/icu/text/RawCollationKey;

    invoke-virtual {p0, p1, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->getRawCollationKey(Ljava/lang/String;Lcom/ibm/icu/text/RawCollationKey;)Lcom/ibm/icu/text/RawCollationKey;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilRawCollationKey_:Lcom/ibm/icu/text/RawCollationKey;

    .line 3227
    const/4 v1, 0x0

    invoke-virtual {p0, p2, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->getRawCollationKey(Ljava/lang/String;Lcom/ibm/icu/text/RawCollationKey;)Lcom/ibm/icu/text/RawCollationKey;

    move-result-object v0

    .line 3228
    .local v0, "targetkey":Lcom/ibm/icu/text/RawCollationKey;
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilRawCollationKey_:Lcom/ibm/icu/text/RawCollationKey;

    invoke-virtual {v1, v0}, Lcom/ibm/icu/text/RawCollationKey;->compareTo(Ljava/lang/Object;)I

    move-result v1

    return v1
.end method

.method private final compareRegular(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 16
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "offset"    # I

    .prologue
    .line 2273
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    if-nez v3, :cond_0

    .line 2274
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->initUtility(Z)V

    .line 2276
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v14

    .line 2278
    .local v14, "strength":I
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isCaseLevel_:Z

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare0_:Z

    .line 2280
    const/4 v3, 0x1

    if-lt v14, v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare2_:Z

    .line 2281
    const/4 v3, 0x2

    if-lt v14, v3, :cond_3

    const/4 v3, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare3_:Z

    .line 2282
    const/4 v3, 0x3

    if-lt v14, v3, :cond_4

    const/4 v3, 0x1

    :goto_2
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare4_:Z

    .line 2283
    const/16 v3, 0xf

    if-ne v14, v3, :cond_5

    const/4 v3, 0x1

    :goto_3
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare5_:Z

    .line 2284
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare2_:Z

    if-eqz v3, :cond_6

    const/4 v9, 0x1

    .line 2285
    .local v9, "doFrench":Z
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isAlternateHandlingShifted_:Z

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare4_:Z

    if-eqz v3, :cond_7

    const/4 v10, 0x1

    .line 2286
    .local v10, "doShift4":Z
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isHiragana4_:Z

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare4_:Z

    if-eqz v3, :cond_8

    const/4 v4, 0x1

    .line 2288
    .local v4, "doHiragana4":Z
    :goto_6
    if-eqz v4, :cond_9

    if-eqz v10, :cond_9

    .line 2289
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 2290
    .local v13, "sourcesub":Ljava/lang/String;
    invoke-virtual/range {p2 .. p3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    .line 2291
    .local v15, "targetsub":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v15}, Lcom/ibm/icu/text/RuleBasedCollator;->compareBySortKeys(Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    .line 2351
    .end local v13    # "sourcesub":Ljava/lang/String;
    .end local v15    # "targetsub":Ljava/lang/String;
    :cond_1
    :goto_7
    return v12

    .line 2280
    .end local v4    # "doHiragana4":Z
    .end local v9    # "doFrench":Z
    .end local v10    # "doShift4":Z
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 2281
    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    .line 2282
    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    .line 2283
    :cond_5
    const/4 v3, 0x0

    goto :goto_3

    .line 2284
    :cond_6
    const/4 v9, 0x0

    goto :goto_4

    .line 2285
    .restart local v9    # "doFrench":Z
    :cond_7
    const/4 v10, 0x0

    goto :goto_5

    .line 2286
    .restart local v10    # "doShift4":Z
    :cond_8
    const/4 v4, 0x0

    goto :goto_6

    .line 2295
    .restart local v4    # "doHiragana4":Z
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isAlternateHandlingShifted_:Z

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_variableTopValue_:I

    shl-int/lit8 v5, v3, 0x10

    .line 2297
    .local v5, "lowestpvalue":I
    :goto_8
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBufferSize_:I

    .line 2298
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBufferSize_:I

    move-object/from16 v3, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move/from16 v8, p3

    .line 2299
    invoke-direct/range {v3 .. v8}, Lcom/ibm/icu/text/RuleBasedCollator;->doPrimaryCompare(ZILjava/lang/String;Ljava/lang/String;I)I

    move-result v12

    .line 2301
    .local v12, "result":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBufferSize_:I

    const/4 v6, -0x1

    if-ne v3, v6, :cond_a

    move-object/from16 v0, p0

    iget v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBufferSize_:I

    const/4 v6, -0x1

    if-eq v3, v6, :cond_1

    .line 2309
    :cond_a
    move v11, v12

    .line 2311
    .local v11, "hiraganaresult":I
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare2_:Z

    if-eqz v3, :cond_b

    .line 2312
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/ibm/icu/text/RuleBasedCollator;->doSecondaryCompare(Z)I

    move-result v12

    .line 2313
    if-nez v12, :cond_1

    .line 2318
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare0_:Z

    if-eqz v3, :cond_c

    .line 2319
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/RuleBasedCollator;->doCaseCompare()I

    move-result v12

    .line 2320
    if-nez v12, :cond_1

    .line 2325
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare3_:Z

    if-eqz v3, :cond_d

    .line 2326
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/RuleBasedCollator;->doTertiaryCompare()I

    move-result v12

    .line 2327
    if-nez v12, :cond_1

    .line 2332
    :cond_d
    if-eqz v10, :cond_10

    .line 2333
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/ibm/icu/text/RuleBasedCollator;->doQuaternaryCompare(I)I

    move-result v12

    .line 2334
    if-nez v12, :cond_1

    .line 2348
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare5_:Z

    if-eqz v3, :cond_11

    .line 2349
    const/4 v3, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->doIdenticalCompare(Ljava/lang/String;Ljava/lang/String;IZ)I

    move-result v12

    goto/16 :goto_7

    .line 2295
    .end local v5    # "lowestpvalue":I
    .end local v11    # "hiraganaresult":I
    .end local v12    # "result":I
    :cond_f
    const/4 v5, 0x0

    goto :goto_8

    .line 2338
    .restart local v5    # "lowestpvalue":I
    .restart local v11    # "hiraganaresult":I
    .restart local v12    # "result":I
    :cond_10
    if-eqz v4, :cond_e

    if-eqz v11, :cond_e

    move v12, v11

    .line 2341
    goto/16 :goto_7

    .line 2351
    :cond_11
    const/4 v12, 0x0

    goto/16 :goto_7
.end method

.method private final compareUseLatin1(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 20
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "startOffset"    # I

    .prologue
    .line 4362
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v10

    .line 4363
    .local v10, "sLen":I
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v16

    .line 4365
    .local v16, "tLen":I
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v12

    .line 4367
    .local v12, "strength":I
    move/from16 v8, p3

    .local v8, "sIndex":I
    move/from16 v14, p3

    .line 4368
    .local v14, "tIndex":I
    const/4 v7, 0x0

    .local v7, "sChar":C
    const/4 v13, 0x0

    .line 4369
    .local v13, "tChar":C
    const/4 v11, 0x0

    .local v11, "sOrder":I
    const/16 v17, 0x0

    .line 4371
    .local v17, "tOrder":I
    const/4 v4, 0x0

    .line 4375
    .local v4, "endOfSource":Z
    const/4 v5, 0x0

    .line 4378
    .local v5, "haveContractions":Z
    move-object/from16 v0, p0

    iget v6, v0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    .local v6, "offset":I
    move v9, v8

    .line 4383
    .end local v8    # "sIndex":I
    .local v9, "sIndex":I
    :goto_0
    if-nez v11, :cond_2d

    .line 4385
    if-ne v9, v10, :cond_1

    .line 4386
    const/4 v4, 0x1

    move v15, v14

    .line 4414
    .end local v14    # "tIndex":I
    .local v15, "tIndex":I
    :goto_1
    if-nez v17, :cond_8

    .line 4416
    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 4417
    if-eqz v4, :cond_4

    .line 4472
    const/16 v18, 0x1

    move/from16 v0, v18

    if-lt v12, v0, :cond_2c

    .line 4475
    const/4 v4, 0x0

    .line 4477
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    move/from16 v18, v0

    if-nez v18, :cond_15

    .line 4483
    move/from16 v8, p3

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    move/from16 v14, p3

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v9, v8

    .line 4486
    .end local v8    # "sIndex":I
    .restart local v9    # "sIndex":I
    :goto_2
    if-nez v11, :cond_2a

    .line 4487
    if-ne v9, v10, :cond_d

    .line 4488
    const/4 v4, 0x1

    move v15, v14

    .line 4500
    .end local v14    # "tIndex":I
    .restart local v15    # "tIndex":I
    :goto_3
    if-nez v17, :cond_10

    .line 4501
    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    .line 4502
    if-eqz v4, :cond_e

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .line 4590
    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    :cond_0
    :goto_4
    const/16 v18, 0x2

    move/from16 v0, v18

    if-lt v12, v0, :cond_27

    .line 4592
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    move/from16 v18, v0

    add-int v6, v6, v18

    .line 4594
    move/from16 v8, p3

    move/from16 v14, p3

    .line 4595
    const/4 v4, 0x0

    move v9, v8

    .line 4597
    .end local v8    # "sIndex":I
    .restart local v9    # "sIndex":I
    :goto_5
    if-nez v11, :cond_28

    .line 4598
    if-ne v9, v10, :cond_1f

    .line 4599
    const/4 v4, 0x1

    move v15, v14

    .line 4610
    .end local v14    # "tIndex":I
    .restart local v15    # "tIndex":I
    :goto_6
    if-nez v17, :cond_22

    .line 4611
    move/from16 v0, v16

    if-ne v15, v0, :cond_21

    .line 4612
    if-eqz v4, :cond_20

    .line 4613
    const/16 v18, 0x0

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .line 4645
    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    :goto_7
    return v18

    .line 4389
    .end local v8    # "sIndex":I
    .restart local v9    # "sIndex":I
    :cond_1
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 4391
    const/16 v18, 0xff

    move/from16 v0, v18

    if-le v7, v0, :cond_2

    .line 4393
    invoke-direct/range {p0 .. p3}, Lcom/ibm/icu/text/RuleBasedCollator;->compareRegular(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v18

    goto :goto_7

    .line 4395
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    move-object/from16 v18, v0

    aget v11, v18, v7

    .line 4396
    invoke-static {v11}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v18

    if-eqz v18, :cond_2e

    .line 4399
    invoke-static {v11}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    .line 4400
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput v8, v0, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    .line 4401
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v11, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->getLatinOneContraction(IILjava/lang/String;)I

    move-result v11

    .line 4402
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v8, v0, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    .line 4403
    const/4 v5, 0x1

    .line 4407
    :cond_3
    invoke-static {v11}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v18

    if-eqz v18, :cond_2e

    .line 4409
    invoke-direct/range {p0 .. p3}, Lcom/ibm/icu/text/RuleBasedCollator;->compareRegular(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v18

    goto :goto_7

    .line 4420
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_4
    const/16 v18, 0x1

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto :goto_7

    .line 4423
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_5
    add-int/lit8 v14, v15, 0x1

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v13

    .line 4424
    const/16 v18, 0xff

    move/from16 v0, v18

    if-le v13, v0, :cond_6

    .line 4426
    invoke-direct/range {p0 .. p3}, Lcom/ibm/icu/text/RuleBasedCollator;->compareRegular(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v18

    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto :goto_7

    .line 4428
    .end local v8    # "sIndex":I
    .restart local v9    # "sIndex":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    move-object/from16 v18, v0

    aget v17, v18, v13

    .line 4429
    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v18

    if-eqz v18, :cond_2d

    .line 4431
    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_7

    .line 4432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput v14, v0, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    .line 4433
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v17

    move-object/from16 v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->getLatinOneContraction(IILjava/lang/String;)I

    move-result v17

    .line 4434
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v14, v0, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    .line 4435
    const/4 v5, 0x1

    .line 4437
    :cond_7
    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v18

    if-eqz v18, :cond_2d

    .line 4439
    invoke-direct/range {p0 .. p3}, Lcom/ibm/icu/text/RuleBasedCollator;->compareRegular(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v18

    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto/16 :goto_7

    .line 4443
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_8
    if-eqz v4, :cond_9

    .line 4444
    const/16 v18, -0x1

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto/16 :goto_7

    .line 4447
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_9
    move/from16 v0, v17

    if-ne v11, v0, :cond_a

    .line 4448
    const/4 v11, 0x0

    const/16 v17, 0x0

    move v14, v15

    .line 4449
    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    goto/16 :goto_0

    .line 4452
    .end local v14    # "tIndex":I
    .restart local v15    # "tIndex":I
    :cond_a
    xor-int v18, v11, v17

    const/high16 v19, -0x1000000

    and-int v18, v18, v19

    if-eqz v18, :cond_c

    .line 4454
    ushr-int/lit8 v18, v11, 0x8

    ushr-int/lit8 v19, v17, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_b

    .line 4455
    const/16 v18, -0x1

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto/16 :goto_7

    .line 4457
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_b
    const/16 v18, 0x1

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto/16 :goto_7

    .line 4464
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_c
    shl-int/lit8 v11, v11, 0x8

    .line 4465
    shl-int/lit8 v17, v17, 0x8

    move v14, v15

    .line 4467
    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    goto/16 :goto_0

    .line 4491
    :cond_d
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 4492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    move-object/from16 v18, v0

    add-int v19, v6, v7

    aget v11, v18, v19

    .line 4493
    invoke-static {v11}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v18

    if-eqz v18, :cond_2b

    .line 4494
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput v8, v0, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    .line 4495
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v11, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->getLatinOneContraction(IILjava/lang/String;)I

    move-result v11

    .line 4496
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v8, v0, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    move v9, v8

    .line 4497
    .end local v8    # "sIndex":I
    .restart local v9    # "sIndex":I
    goto/16 :goto_2

    .line 4505
    .end local v14    # "tIndex":I
    .restart local v15    # "tIndex":I
    :cond_e
    const/16 v18, 0x1

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto/16 :goto_7

    .line 4508
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_f
    add-int/lit8 v14, v15, 0x1

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v13

    .line 4509
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    move-object/from16 v18, v0

    add-int v19, v6, v13

    aget v17, v18, v19

    .line 4510
    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v18

    if-eqz v18, :cond_2a

    .line 4511
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput v14, v0, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    .line 4512
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v17

    move-object/from16 v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->getLatinOneContraction(IILjava/lang/String;)I

    move-result v17

    .line 4513
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v14, v0, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    move v15, v14

    .line 4514
    .end local v14    # "tIndex":I
    .restart local v15    # "tIndex":I
    goto/16 :goto_3

    .line 4516
    :cond_10
    if-eqz v4, :cond_11

    .line 4517
    const/16 v18, -0x1

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto/16 :goto_7

    .line 4520
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_11
    move/from16 v0, v17

    if-ne v11, v0, :cond_12

    .line 4521
    const/4 v11, 0x0

    const/16 v17, 0x0

    move v14, v15

    .line 4522
    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    goto/16 :goto_2

    .line 4525
    .end local v14    # "tIndex":I
    .restart local v15    # "tIndex":I
    :cond_12
    xor-int v18, v11, v17

    const/high16 v19, -0x1000000

    and-int v18, v18, v19

    if-eqz v18, :cond_14

    .line 4526
    ushr-int/lit8 v18, v11, 0x8

    ushr-int/lit8 v19, v17, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_13

    .line 4527
    const/16 v18, -0x1

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto/16 :goto_7

    .line 4529
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_13
    const/16 v18, 0x1

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto/16 :goto_7

    .line 4532
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_14
    shl-int/lit8 v11, v11, 0x8

    .line 4533
    shl-int/lit8 v17, v17, 0x8

    move v14, v15

    .line 4535
    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    goto/16 :goto_2

    .line 4537
    .end local v14    # "tIndex":I
    .restart local v15    # "tIndex":I
    :cond_15
    if-eqz v5, :cond_16

    .line 4539
    invoke-direct/range {p0 .. p3}, Lcom/ibm/icu/text/RuleBasedCollator;->compareRegular(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v18

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto/16 :goto_7

    .line 4542
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_16
    move v8, v10

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    move/from16 v14, v16

    .line 4545
    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    :goto_8
    if-nez v11, :cond_17

    .line 4546
    move/from16 v0, p3

    if-ne v8, v0, :cond_18

    .line 4547
    const/4 v4, 0x1

    .line 4555
    :cond_17
    :goto_9
    if-nez v17, :cond_1a

    .line 4556
    move/from16 v0, p3

    if-ne v14, v0, :cond_19

    .line 4557
    if-nez v4, :cond_0

    .line 4560
    const/16 v18, 0x1

    goto/16 :goto_7

    .line 4550
    :cond_18
    add-int/lit8 v8, v8, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 4551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    move-object/from16 v18, v0

    add-int v19, v6, v7

    aget v11, v18, v19

    .line 4553
    goto :goto_8

    .line 4563
    :cond_19
    add-int/lit8 v14, v14, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/lang/String;->charAt(I)C

    move-result v13

    .line 4564
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    move-object/from16 v18, v0

    add-int v19, v6, v13

    aget v17, v18, v19

    .line 4566
    goto :goto_9

    .line 4567
    :cond_1a
    if-eqz v4, :cond_1b

    .line 4568
    const/16 v18, -0x1

    goto/16 :goto_7

    .line 4571
    :cond_1b
    move/from16 v0, v17

    if-ne v11, v0, :cond_1c

    .line 4572
    const/4 v11, 0x0

    const/16 v17, 0x0

    .line 4573
    goto :goto_8

    .line 4576
    :cond_1c
    xor-int v18, v11, v17

    const/high16 v19, -0x1000000

    and-int v18, v18, v19

    if-eqz v18, :cond_1e

    .line 4577
    ushr-int/lit8 v18, v11, 0x8

    ushr-int/lit8 v19, v17, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_1d

    .line 4578
    const/16 v18, -0x1

    goto/16 :goto_7

    .line 4580
    :cond_1d
    const/16 v18, 0x1

    goto/16 :goto_7

    .line 4583
    :cond_1e
    shl-int/lit8 v11, v11, 0x8

    .line 4584
    shl-int/lit8 v17, v17, 0x8

    .line 4586
    goto :goto_8

    .line 4602
    .end local v8    # "sIndex":I
    .restart local v9    # "sIndex":I
    :cond_1f
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 4603
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    move-object/from16 v18, v0

    add-int v19, v6, v7

    aget v11, v18, v19

    .line 4604
    invoke-static {v11}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v18

    if-eqz v18, :cond_29

    .line 4605
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput v8, v0, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    .line 4606
    const/16 v18, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v11, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->getLatinOneContraction(IILjava/lang/String;)I

    move-result v11

    .line 4607
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v8, v0, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    move v9, v8

    .line 4608
    .end local v8    # "sIndex":I
    .restart local v9    # "sIndex":I
    goto/16 :goto_5

    .line 4615
    .end local v14    # "tIndex":I
    .restart local v15    # "tIndex":I
    :cond_20
    const/16 v18, 0x1

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto/16 :goto_7

    .line 4618
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_21
    add-int/lit8 v14, v15, 0x1

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v13

    .line 4619
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    move-object/from16 v18, v0

    add-int v19, v6, v13

    aget v17, v18, v19

    .line 4620
    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v18

    if-eqz v18, :cond_28

    .line 4621
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iput v14, v0, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    .line 4622
    const/16 v18, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v17

    move-object/from16 v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->getLatinOneContraction(IILjava/lang/String;)I

    move-result v17

    .line 4623
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v14, v0, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    move v15, v14

    .line 4624
    .end local v14    # "tIndex":I
    .restart local v15    # "tIndex":I
    goto/16 :goto_6

    .line 4626
    :cond_22
    if-eqz v4, :cond_23

    .line 4627
    const/16 v18, -0x1

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto/16 :goto_7

    .line 4629
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_23
    move/from16 v0, v17

    if-ne v11, v0, :cond_24

    .line 4630
    const/4 v11, 0x0

    const/16 v17, 0x0

    move v14, v15

    .line 4631
    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    goto/16 :goto_5

    .line 4633
    .end local v14    # "tIndex":I
    .restart local v15    # "tIndex":I
    :cond_24
    xor-int v18, v11, v17

    const/high16 v19, -0x1000000

    and-int v18, v18, v19

    if-eqz v18, :cond_26

    .line 4634
    ushr-int/lit8 v18, v11, 0x8

    ushr-int/lit8 v19, v17, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_25

    .line 4635
    const/16 v18, -0x1

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto/16 :goto_7

    .line 4637
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_25
    const/16 v18, 0x1

    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto/16 :goto_7

    .line 4640
    .end local v8    # "sIndex":I
    .end local v14    # "tIndex":I
    .restart local v9    # "sIndex":I
    .restart local v15    # "tIndex":I
    :cond_26
    shl-int/lit8 v11, v11, 0x8

    .line 4641
    shl-int/lit8 v17, v17, 0x8

    move v14, v15

    .line 4643
    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    goto/16 :goto_5

    .line 4645
    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    :cond_27
    const/16 v18, 0x0

    goto/16 :goto_7

    .end local v8    # "sIndex":I
    .restart local v9    # "sIndex":I
    :cond_28
    move v15, v14

    .end local v14    # "tIndex":I
    .restart local v15    # "tIndex":I
    goto/16 :goto_6

    .end local v9    # "sIndex":I
    .end local v15    # "tIndex":I
    .restart local v8    # "sIndex":I
    .restart local v14    # "tIndex":I
    :cond_29
    move v9, v8

    .end local v8    # "sIndex":I
    .restart local v9    # "sIndex":I
    goto/16 :goto_5

    :cond_2a
    move v15, v14

    .end local v14    # "tIndex":I
    .restart local v15    # "tIndex":I
    goto/16 :goto_3

    .end local v9    # "sIndex":I
    .end local v15    # "tIndex":I
    .restart local v8    # "sIndex":I
    .restart local v14    # "tIndex":I
    :cond_2b
    move v9, v8

    .end local v8    # "sIndex":I
    .restart local v9    # "sIndex":I
    goto/16 :goto_2

    .end local v14    # "tIndex":I
    .restart local v15    # "tIndex":I
    :cond_2c
    move v14, v15

    .end local v15    # "tIndex":I
    .restart local v14    # "tIndex":I
    move v8, v9

    .end local v9    # "sIndex":I
    .restart local v8    # "sIndex":I
    goto/16 :goto_4

    .end local v8    # "sIndex":I
    .restart local v9    # "sIndex":I
    :cond_2d
    move v15, v14

    .end local v14    # "tIndex":I
    .restart local v15    # "tIndex":I
    goto/16 :goto_1

    .end local v9    # "sIndex":I
    .end local v15    # "tIndex":I
    .restart local v8    # "sIndex":I
    .restart local v14    # "tIndex":I
    :cond_2e
    move v9, v8

    .end local v8    # "sIndex":I
    .restart local v9    # "sIndex":I
    goto/16 :goto_0
.end method

.method private final doCase()V
    .locals 5

    .prologue
    .line 3023
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 3025
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 3026
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    array-length v0, v0

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount0_:I

    add-int/2addr v1, v2

    if-gt v0, v1, :cond_0

    .line 3027
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount0_:I

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->increase([BII)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 3030
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes0_:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount0_:I

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3032
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount0_:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 3033
    return-void
.end method

.method private final doCaseBytes(IZI)I
    .locals 5
    .param p1, "tertiary"    # I
    .param p2, "notIsContinuation"    # Z
    .param p3, "caseshift"    # I

    .prologue
    const/4 v4, 0x1

    .line 2601
    invoke-direct {p0, p3}, Lcom/ibm/icu/text/RuleBasedCollator;->doCaseShift(I)I

    move-result p3

    .line 2603
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 2604
    and-int/lit16 v1, p1, 0xc0

    int-to-byte v0, v1

    .line 2605
    .local v0, "casebits":B
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    const/16 v2, 0x19

    if-ne v1, v2, :cond_2

    .line 2606
    if-nez v0, :cond_1

    .line 2607
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes0_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount0_:I

    add-int/lit8 v2, v2, -0x1

    aget-byte v3, v1, v2

    add-int/lit8 p3, p3, -0x1

    shl-int/2addr v4, p3

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 2632
    .end local v0    # "casebits":B
    :cond_0
    :goto_0
    return p3

    .line 2612
    .restart local v0    # "casebits":B
    :cond_1
    add-int/lit8 v1, p3, -0x1

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->doCaseShift(I)I

    move-result p3

    .line 2613
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes0_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount0_:I

    add-int/lit8 v2, v2, -0x1

    aget-byte v3, v1, v2

    shr-int/lit8 v4, v0, 0x6

    and-int/lit8 v4, v4, 0x1

    add-int/lit8 p3, p3, -0x1

    shl-int/2addr v4, p3

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    goto :goto_0

    .line 2618
    :cond_2
    if-eqz v0, :cond_3

    .line 2619
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes0_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount0_:I

    add-int/lit8 v2, v2, -0x1

    aget-byte v3, v1, v2

    add-int/lit8 p3, p3, -0x1

    shl-int/2addr v4, p3

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 2622
    invoke-direct {p0, p3}, Lcom/ibm/icu/text/RuleBasedCollator;->doCaseShift(I)I

    move-result p3

    .line 2623
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes0_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount0_:I

    add-int/lit8 v2, v2, -0x1

    aget-byte v3, v1, v2

    shr-int/lit8 v4, v0, 0x7

    and-int/lit8 v4, v4, 0x1

    add-int/lit8 p3, p3, -0x1

    shl-int/2addr v4, p3

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    goto :goto_0

    .line 2627
    :cond_3
    add-int/lit8 p3, p3, -0x1

    goto :goto_0
.end method

.method private final doCaseCompare()I
    .locals 11

    .prologue
    const/high16 v10, -0x10000

    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 3630
    const/4 v0, 0x0

    .line 3631
    .local v0, "soffset":I
    const/4 v3, 0x0

    .line 3633
    .local v3, "toffset":I
    :goto_0
    const/4 v2, 0x0

    .line 3634
    .local v2, "sorder":I
    const/4 v5, 0x0

    .local v5, "torder":I
    move v1, v0

    .line 3636
    .end local v0    # "soffset":I
    .local v1, "soffset":I
    :goto_1
    and-int/lit8 v8, v2, 0x3f

    if-nez v8, :cond_b

    .line 3637
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "soffset":I
    .restart local v0    # "soffset":I
    aget v2, v8, v1

    .line 3638
    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v8

    if-nez v8, :cond_1

    and-int v8, v2, v10

    if-nez v8, :cond_0

    iget-boolean v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare2_:Z

    if-ne v8, v7, :cond_1

    .line 3641
    :cond_0
    and-int/lit16 v2, v2, 0xff

    .line 3642
    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseSwitch_:I

    xor-int/2addr v2, v8

    move v1, v0

    .line 3643
    .end local v0    # "soffset":I
    .restart local v1    # "soffset":I
    goto :goto_1

    .line 3645
    .end local v1    # "soffset":I
    .restart local v0    # "soffset":I
    :cond_1
    const/4 v2, 0x0

    move v1, v0

    .line 3647
    .end local v0    # "soffset":I
    .restart local v1    # "soffset":I
    goto :goto_1

    .line 3650
    .end local v3    # "toffset":I
    .local v4, "toffset":I
    :goto_2
    and-int/lit8 v8, v5, 0x3f

    if-nez v8, :cond_4

    .line 3651
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v3, v4, 0x1

    .end local v4    # "toffset":I
    .restart local v3    # "toffset":I
    aget v5, v8, v4

    .line 3652
    invoke-static {v5}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v8

    if-nez v8, :cond_3

    and-int v8, v5, v10

    if-nez v8, :cond_2

    iget-boolean v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare2_:Z

    if-ne v8, v7, :cond_3

    .line 3655
    :cond_2
    and-int/lit16 v5, v5, 0xff

    .line 3656
    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseSwitch_:I

    xor-int/2addr v5, v8

    move v4, v3

    .line 3657
    .end local v3    # "toffset":I
    .restart local v4    # "toffset":I
    goto :goto_2

    .line 3659
    .end local v4    # "toffset":I
    .restart local v3    # "toffset":I
    :cond_3
    const/4 v5, 0x0

    move v4, v3

    .line 3661
    .end local v3    # "toffset":I
    .restart local v4    # "toffset":I
    goto :goto_2

    .line 3663
    :cond_4
    and-int/lit16 v2, v2, 0xc0

    .line 3664
    and-int/lit16 v5, v5, 0xc0

    .line 3665
    if-ne v2, v5, :cond_7

    .line 3667
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    add-int/lit8 v9, v1, -0x1

    aget v8, v8, v9

    if-ne v8, v6, :cond_6

    .line 3669
    iget-object v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v8, v4, -0x1

    aget v7, v7, v8

    if-eq v7, v6, :cond_a

    .line 3692
    :cond_5
    :goto_3
    return v6

    .line 3675
    :cond_6
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v9, v4, -0x1

    aget v8, v8, v9

    if-ne v8, v6, :cond_9

    move v6, v7

    .line 3677
    goto :goto_3

    .line 3681
    :cond_7
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    add-int/lit8 v9, v1, -0x1

    aget v8, v8, v9

    if-eq v8, v6, :cond_5

    .line 3685
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v9, v1, -0x1

    aget v8, v8, v9

    if-ne v8, v6, :cond_8

    move v6, v7

    .line 3687
    goto :goto_3

    .line 3689
    :cond_8
    if-lt v2, v5, :cond_5

    move v6, v7

    goto :goto_3

    :cond_9
    move v3, v4

    .end local v4    # "toffset":I
    .restart local v3    # "toffset":I
    move v0, v1

    .line 3691
    .end local v1    # "soffset":I
    .restart local v0    # "soffset":I
    goto :goto_0

    .line 3692
    .end local v0    # "soffset":I
    .end local v3    # "toffset":I
    .restart local v1    # "soffset":I
    .restart local v4    # "toffset":I
    :cond_a
    const/4 v6, 0x0

    goto :goto_3

    .end local v4    # "toffset":I
    .restart local v3    # "toffset":I
    :cond_b
    move v4, v3

    .end local v3    # "toffset":I
    .restart local v4    # "toffset":I
    goto :goto_2
.end method

.method private final doCaseShift(I)I
    .locals 3
    .param p1, "caseshift"    # I

    .prologue
    .line 2581
    if-nez p1, :cond_0

    .line 2582
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes0_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount0_:I

    const/16 v2, -0x80

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes0_:[B

    .line 2584
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount0_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount0_:I

    .line 2585
    const/4 p1, 0x7

    .line 2587
    :cond_0
    return p1
.end method

.method private final doFrench()V
    .locals 8

    .prologue
    const/16 v7, 0x45

    const/4 v6, 0x5

    const/16 v5, 0x40

    .line 2890
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    if-ge v0, v2, :cond_5

    .line 2891
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    sub-int/2addr v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-byte v1, v2, v3

    .line 2893
    .local v1, "s":B
    if-ne v1, v6, :cond_0

    .line 2894
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    .line 2890
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2897
    :cond_0
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    if-lez v2, :cond_2

    .line 2899
    and-int/lit16 v2, v1, 0xff

    if-le v2, v6, :cond_3

    .line 2901
    :goto_2
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    if-le v2, v5, :cond_1

    .line 2902
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    const/16 v4, 0x46

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2905
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 2906
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    add-int/lit8 v2, v2, -0x40

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    goto :goto_2

    .line 2908
    :cond_1
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    add-int/lit8 v4, v4, -0x1

    rsub-int v4, v4, 0x86

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2912
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 2928
    :goto_3
    const/4 v2, 0x0

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    .line 2930
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    invoke-static {v2, v3, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2931
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    goto :goto_1

    .line 2915
    :cond_3
    :goto_4
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    if-le v2, v5, :cond_4

    .line 2916
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    invoke-static {v2, v3, v7}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2919
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 2920
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    add-int/lit8 v2, v2, -0x40

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    goto :goto_4

    .line 2922
    :cond_4
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v4, v4, 0x5

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2926
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    goto :goto_3

    .line 2934
    .end local v1    # "s":B
    :cond_5
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    if-lez v2, :cond_7

    .line 2935
    :goto_5
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    if-le v2, v5, :cond_6

    .line 2936
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    invoke-static {v2, v3, v7}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2939
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 2940
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    add-int/lit8 v2, v2, -0x40

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    goto :goto_5

    .line 2942
    :cond_6
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v4, v4, 0x5

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2945
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 2947
    :cond_7
    return-void
.end method

.method private final doIdentical(Ljava/lang/String;)V
    .locals 4
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 3115
    invoke-static {p1}, Lcom/ibm/icu/impl/BOCU;->getCompressionLength(Ljava/lang/String;)I

    move-result v0

    .line 3116
    .local v0, "isize":I
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 3118
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 3119
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    array-length v1, v1

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/2addr v2, v0

    if-gt v1, v2, :cond_0

    .line 3120
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v3, v0, 0x1

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->increase([BII)[B

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 3123
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    invoke-static {p1, v1, v2}, Lcom/ibm/icu/impl/BOCU;->compress(Ljava/lang/String;[BI)I

    move-result v1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 3125
    return-void
.end method

.method private static final doIdenticalCompare(Ljava/lang/String;Ljava/lang/String;IZ)I
    .locals 3
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "offset"    # I
    .param p3, "normalize"    # Z

    .prologue
    const/4 v2, 0x0

    .line 3858
    if-eqz p3, :cond_2

    .line 3859
    sget-object v0, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {p0, v0, v2}, Lcom/ibm/icu/text/Normalizer;->quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v0

    sget-object v1, Lcom/ibm/icu/text/Normalizer;->YES:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-eq v0, v1, :cond_0

    .line 3861
    invoke-static {p0, v2}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p0

    .line 3864
    :cond_0
    sget-object v0, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {p1, v0, v2}, Lcom/ibm/icu/text/Normalizer;->quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v0

    sget-object v1, Lcom/ibm/icu/text/Normalizer;->YES:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-eq v0, v1, :cond_1

    .line 3866
    invoke-static {p1, v2}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    .line 3868
    :cond_1
    const/4 p2, 0x0

    .line 3871
    :cond_2
    invoke-static {p0, p1, p2}, Lcom/ibm/icu/text/RuleBasedCollator;->doStringCompare(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private final doPrimaryBytes(IZZIII)I
    .locals 5
    .param p1, "ce"    # I
    .param p2, "notIsContinuation"    # Z
    .param p3, "doShift"    # Z
    .param p4, "leadPrimary"    # I
    .param p5, "commonBottom4"    # I
    .param p6, "bottomCount4"    # I

    .prologue
    .line 2370
    shr-int/lit8 p1, p1, 0x10

    and-int/lit16 v1, p1, 0xff

    .line 2371
    .local v1, "p2":I
    ushr-int/lit8 v0, p1, 0x8

    .line 2372
    .local v0, "p1":I
    if-eqz p3, :cond_4

    .line 2373
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    if-lez v2, :cond_1

    .line 2374
    :goto_0
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    if-le v2, p6, :cond_0

    .line 2375
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    add-int v4, p5, p6

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    .line 2377
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    .line 2378
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    sub-int/2addr v2, p6

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    goto :goto_0

    .line 2380
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    add-int/lit8 v4, v4, -0x1

    add-int/2addr v4, p5

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    .line 2383
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    .line 2384
    const/4 v2, 0x0

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    .line 2388
    :cond_1
    if-eqz v0, :cond_2

    .line 2390
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    int-to-byte v4, v0

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    .line 2392
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    .line 2394
    :cond_2
    if-eqz v1, :cond_3

    .line 2395
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    int-to-byte v4, v1

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    .line 2397
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    .line 2475
    :cond_3
    :goto_1
    return p4

    .line 2406
    :cond_4
    if-eqz v0, :cond_3

    .line 2407
    if-eqz p2, :cond_b

    .line 2408
    if-ne p4, v0, :cond_5

    .line 2409
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    int-to-byte v4, v1

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2411
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    goto :goto_1

    .line 2414
    :cond_5
    if-eqz p4, :cond_6

    .line 2415
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    if-le v0, p4, :cond_7

    const/4 v2, -0x1

    :goto_2
    invoke-static {v3, v4, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2420
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 2422
    :cond_6
    if-nez v1, :cond_8

    .line 2424
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    int-to-byte v4, v0

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2427
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 2428
    const/4 p4, 0x0

    .line 2429
    goto :goto_1

    .line 2415
    :cond_7
    const/4 v2, 0x3

    goto :goto_2

    .line 2430
    :cond_8
    const/16 v2, 0x4d

    if-lt v0, v2, :cond_9

    const/16 v2, 0xa0

    if-le v0, v2, :cond_a

    const/16 v2, 0xe0

    if-ge v0, v2, :cond_a

    .line 2439
    :cond_9
    const/4 p4, 0x0

    .line 2440
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    int-to-byte v4, v0

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2443
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 2444
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    int-to-byte v4, v1

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2447
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    goto :goto_1

    .line 2450
    :cond_a
    move p4, v0

    .line 2451
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    int-to-byte v4, v0

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2454
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 2455
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    int-to-byte v4, v1

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2457
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    goto/16 :goto_1

    .line 2463
    :cond_b
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    int-to-byte v4, v0

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2465
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 2466
    if-eqz v1, :cond_3

    .line 2467
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    int-to-byte v4, v1

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2470
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    goto/16 :goto_1
.end method

.method private final doPrimaryCompare(ZILjava/lang/String;Ljava/lang/String;I)I
    .locals 8
    .param p1, "doHiragana4"    # Z
    .param p2, "lowestpvalue"    # I
    .param p3, "source"    # Ljava/lang/String;
    .param p4, "target"    # Ljava/lang/String;
    .param p5, "textoffset"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    const/high16 v7, -0x10000

    const/4 v3, -0x1

    .line 3257
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-virtual {v5, p3}, Lcom/ibm/icu/impl/StringUCharacterIterator;->setText(Ljava/lang/String;)V

    .line 3258
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-virtual {v5, v6, p5}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Lcom/ibm/icu/text/UCharacterIterator;I)V

    .line 3259
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-virtual {v5, p4}, Lcom/ibm/icu/impl/StringUCharacterIterator;->setText(Ljava/lang/String;)V

    .line 3260
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-virtual {v5, v6, p5}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Lcom/ibm/icu/text/UCharacterIterator;I)V

    .line 3263
    iget-boolean v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isAlternateHandlingShifted_:Z

    if-nez v5, :cond_8

    .line 3264
    const/4 v0, 0x0

    .line 3266
    .local v0, "hiraganaresult":I
    :cond_0
    :goto_0
    const/4 v1, 0x0

    .line 3269
    .local v1, "sorder":I
    :cond_1
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v1

    .line 3270
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    iget v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBufferSize_:I

    invoke-static {v5, v6, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->append([III)[I

    move-result-object v5

    iput-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    .line 3272
    iget v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBufferSize_:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBufferSize_:I

    .line 3273
    and-int/2addr v1, v7

    .line 3274
    if-eqz v1, :cond_1

    .line 3276
    const/4 v2, 0x0

    .line 3278
    .local v2, "torder":I
    :cond_2
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v2

    .line 3279
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    iget v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBufferSize_:I

    invoke-static {v5, v6, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([III)[I

    move-result-object v5

    iput-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    .line 3281
    iget v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBufferSize_:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBufferSize_:I

    .line 3282
    and-int/2addr v2, v7

    .line 3283
    if-eqz v2, :cond_2

    .line 3286
    if-ne v1, v2, :cond_7

    .line 3289
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    iget v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBufferSize_:I

    add-int/lit8 v6, v6, -0x1

    aget v5, v5, v6

    if-ne v5, v3, :cond_4

    .line 3291
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    iget v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBufferSize_:I

    add-int/lit8 v5, v5, -0x1

    aget v4, v4, v5

    if-eq v4, v3, :cond_3

    move v0, v3

    .line 3340
    .end local v0    # "hiraganaresult":I
    :cond_3
    :goto_1
    return v0

    .line 3297
    .restart local v0    # "hiraganaresult":I
    :cond_4
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    iget v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBufferSize_:I

    add-int/lit8 v6, v6, -0x1

    aget v5, v5, v6

    if-ne v5, v3, :cond_5

    move v0, v4

    .line 3299
    goto :goto_1

    .line 3301
    :cond_5
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-boolean v5, v5, Lcom/ibm/icu/text/CollationElementIterator;->m_isCodePointHiragana_:Z

    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-boolean v6, v6, Lcom/ibm/icu/text/CollationElementIterator;->m_isCodePointHiragana_:Z

    if-eq v5, v6, :cond_0

    .line 3304
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-boolean v5, v5, Lcom/ibm/icu/text/CollationElementIterator;->m_isCodePointHiragana_:Z

    if-eqz v5, :cond_6

    .line 3305
    const/4 v0, -0x1

    .line 3306
    goto :goto_0

    .line 3308
    :cond_6
    const/4 v0, 0x1

    .line 3310
    goto :goto_0

    .line 3314
    :cond_7
    invoke-direct {p0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->endPrimaryCompare(II)I

    move-result v0

    goto :goto_1

    .line 3322
    .end local v0    # "hiraganaresult":I
    .end local v1    # "sorder":I
    .end local v2    # "torder":I
    :cond_8
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-direct {p0, v5, p2, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->getPrimaryShiftedCompareCE(Lcom/ibm/icu/text/CollationElementIterator;IZ)I

    move-result v1

    .line 3324
    .restart local v1    # "sorder":I
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-direct {p0, v5, p2, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getPrimaryShiftedCompareCE(Lcom/ibm/icu/text/CollationElementIterator;IZ)I

    move-result v2

    .line 3326
    .restart local v2    # "torder":I
    if-ne v1, v2, :cond_9

    .line 3327
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    iget v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBufferSize_:I

    add-int/lit8 v6, v6, -0x1

    aget v5, v5, v6

    if-ne v5, v3, :cond_8

    goto :goto_1

    .line 3336
    :cond_9
    invoke-direct {p0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->endPrimaryCompare(II)I

    move-result v0

    goto :goto_1
.end method

.method private final doQuaternary(II)V
    .locals 5
    .param p1, "commonbottom4"    # I
    .param p2, "bottomcount4"    # I

    .prologue
    .line 3083
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    if-lez v0, :cond_1

    .line 3084
    :goto_0
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    if-le v0, p2, :cond_0

    .line 3085
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    add-int v2, p1, p2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    .line 3087
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    .line 3088
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    sub-int/2addr v0, p2

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    goto :goto_0

    .line 3090
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    add-int/lit8 v2, v2, -0x1

    add-int/2addr v2, p1

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    .line 3093
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    .line 3095
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 3097
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 3098
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    array-length v0, v0

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    add-int/2addr v1, v2

    if-gt v0, v1, :cond_2

    .line 3099
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->increase([BII)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 3102
    :cond_2
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3104
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 3105
    return-void
.end method

.method private final doQuaternaryBytes(ZIIB)V
    .locals 3
    .param p1, "isCodePointHiragana"    # Z
    .param p2, "commonBottom4"    # I
    .param p3, "bottomCount4"    # I
    .param p4, "hiragana4"    # B

    .prologue
    .line 2708
    if-eqz p1, :cond_2

    .line 2709
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    if-lez v0, :cond_1

    .line 2710
    :goto_0
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    if-le v0, p3, :cond_0

    .line 2711
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    add-int v2, p2, p3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    .line 2714
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    .line 2715
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    sub-int/2addr v0, p3

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    goto :goto_0

    .line 2717
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    add-int/lit8 v2, v2, -0x1

    add-int/2addr v2, p2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    .line 2720
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    .line 2721
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    .line 2723
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    invoke-static {v0, v1, p4}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    .line 2725
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    .line 2730
    :goto_1
    return-void

    .line 2728
    :cond_2
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    goto :goto_1
.end method

.method private final doQuaternaryCompare(I)I
    .locals 13
    .param p1, "lowestpvalue"    # I

    .prologue
    const/high16 v12, -0x10000

    const/4 v9, 0x1

    const/4 v8, -0x1

    .line 3765
    const/4 v0, 0x1

    .line 3766
    .local v0, "sShifted":Z
    const/4 v4, 0x1

    .line 3767
    .local v4, "tShifted":Z
    const/4 v1, 0x0

    .line 3768
    .local v1, "soffset":I
    const/4 v5, 0x0

    .line 3770
    .local v5, "toffset":I
    :goto_0
    const/4 v3, 0x0

    .line 3771
    .local v3, "sorder":I
    const/4 v7, 0x0

    .local v7, "torder":I
    move v2, v1

    .line 3773
    .end local v1    # "soffset":I
    .local v2, "soffset":I
    :goto_1
    if-eqz v3, :cond_0

    invoke-static {v3}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v10

    if-eqz v10, :cond_4

    if-nez v0, :cond_4

    .line 3774
    :cond_0
    iget-object v10, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "soffset":I
    .restart local v1    # "soffset":I
    aget v3, v10, v2

    .line 3775
    invoke-static {v3}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 3776
    if-nez v0, :cond_11

    move v2, v1

    .line 3777
    .end local v1    # "soffset":I
    .restart local v2    # "soffset":I
    goto :goto_1

    .line 3780
    .end local v2    # "soffset":I
    .restart local v1    # "soffset":I
    :cond_1
    invoke-static {v3, p1}, Lcom/ibm/icu/impl/Utility;->compareUnsigned(II)I

    move-result v10

    if-gtz v10, :cond_2

    and-int v10, v3, v12

    if-nez v10, :cond_3

    .line 3784
    :cond_2
    const/high16 v3, -0x10000

    .line 3785
    const/4 v0, 0x0

    move v2, v1

    .line 3786
    .end local v1    # "soffset":I
    .restart local v2    # "soffset":I
    goto :goto_1

    .line 3788
    .end local v2    # "soffset":I
    .restart local v1    # "soffset":I
    :cond_3
    const/4 v0, 0x1

    move v2, v1

    .line 3790
    .end local v1    # "soffset":I
    .restart local v2    # "soffset":I
    goto :goto_1

    .line 3791
    :cond_4
    ushr-int/lit8 v3, v3, 0x10

    move v6, v5

    .line 3793
    .end local v5    # "toffset":I
    .local v6, "toffset":I
    :goto_2
    if-eqz v7, :cond_5

    invoke-static {v7}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v10

    if-eqz v10, :cond_9

    if-nez v4, :cond_9

    .line 3794
    :cond_5
    iget-object v10, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "toffset":I
    .restart local v5    # "toffset":I
    aget v7, v10, v6

    .line 3795
    invoke-static {v7}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 3796
    if-nez v4, :cond_10

    move v6, v5

    .line 3797
    .end local v5    # "toffset":I
    .restart local v6    # "toffset":I
    goto :goto_2

    .line 3800
    .end local v6    # "toffset":I
    .restart local v5    # "toffset":I
    :cond_6
    invoke-static {v7, p1}, Lcom/ibm/icu/impl/Utility;->compareUnsigned(II)I

    move-result v10

    if-gtz v10, :cond_7

    and-int v10, v7, v12

    if-nez v10, :cond_8

    .line 3804
    :cond_7
    const/high16 v7, -0x10000

    .line 3805
    const/4 v4, 0x0

    move v6, v5

    .line 3806
    .end local v5    # "toffset":I
    .restart local v6    # "toffset":I
    goto :goto_2

    .line 3808
    .end local v6    # "toffset":I
    .restart local v5    # "toffset":I
    :cond_8
    const/4 v4, 0x1

    move v6, v5

    .line 3810
    .end local v5    # "toffset":I
    .restart local v6    # "toffset":I
    goto :goto_2

    .line 3811
    :cond_9
    ushr-int/lit8 v7, v7, 0x10

    .line 3813
    if-ne v3, v7, :cond_c

    .line 3814
    iget-object v10, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    add-int/lit8 v11, v2, -0x1

    aget v10, v10, v11

    if-ne v10, v8, :cond_b

    .line 3816
    iget-object v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v10, v6, -0x1

    aget v9, v9, v10

    if-eq v9, v8, :cond_f

    .line 3839
    :cond_a
    :goto_3
    return v8

    .line 3822
    :cond_b
    iget-object v10, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v11, v6, -0x1

    aget v10, v10, v11

    if-ne v10, v8, :cond_e

    move v8, v9

    .line 3824
    goto :goto_3

    .line 3828
    :cond_c
    iget-object v10, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    add-int/lit8 v11, v2, -0x1

    aget v10, v10, v11

    if-eq v10, v8, :cond_a

    .line 3832
    iget-object v10, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v11, v6, -0x1

    aget v10, v10, v11

    if-ne v10, v8, :cond_d

    move v8, v9

    .line 3834
    goto :goto_3

    .line 3836
    :cond_d
    if-lt v3, v7, :cond_a

    move v8, v9

    goto :goto_3

    :cond_e
    move v5, v6

    .end local v6    # "toffset":I
    .restart local v5    # "toffset":I
    move v1, v2

    .line 3838
    .end local v2    # "soffset":I
    .restart local v1    # "soffset":I
    goto/16 :goto_0

    .line 3839
    .end local v1    # "soffset":I
    .end local v5    # "toffset":I
    .restart local v2    # "soffset":I
    .restart local v6    # "toffset":I
    :cond_f
    const/4 v8, 0x0

    goto :goto_3

    .end local v6    # "toffset":I
    .restart local v5    # "toffset":I
    :cond_10
    move v6, v5

    .end local v5    # "toffset":I
    .restart local v6    # "toffset":I
    goto :goto_2

    .end local v2    # "soffset":I
    .end local v6    # "toffset":I
    .restart local v1    # "soffset":I
    .restart local v5    # "toffset":I
    :cond_11
    move v2, v1

    .end local v1    # "soffset":I
    .restart local v2    # "soffset":I
    goto/16 :goto_1
.end method

.method private final doSecondary(Z)V
    .locals 5
    .param p1, "doFrench"    # Z

    .prologue
    .line 2955
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    if-lez v0, :cond_1

    .line 2956
    :goto_0
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    const/16 v1, 0x40

    if-le v0, v1, :cond_0

    .line 2957
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    const/16 v2, 0x45

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    .line 2960
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    .line 2961
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    add-int/lit8 v0, v0, -0x40

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    goto :goto_0

    .line 2963
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v2, v2, 0x5

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    .line 2966
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    .line 2969
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2971
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 2973
    if-eqz p1, :cond_2

    .line 2974
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->doFrench()V

    .line 2986
    :goto_1
    return-void

    .line 2977
    :cond_2
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    array-length v0, v0

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    add-int/2addr v1, v2

    if-gt v0, v1, :cond_3

    .line 2979
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->increase([BII)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2982
    :cond_3
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2984
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    goto :goto_1
.end method

.method private final doSecondaryBytes(IZZ)V
    .locals 6
    .param p1, "ce"    # I
    .param p2, "notIsContinuation"    # Z
    .param p3, "doFrench"    # Z

    .prologue
    const/16 v5, 0x40

    const/4 v2, 0x5

    const/4 v4, -0x1

    .line 2488
    shr-int/lit8 p1, p1, 0x8

    and-int/lit16 v0, p1, 0xff

    .line 2489
    .local v0, "s":I
    if-eqz v0, :cond_0

    .line 2490
    if-nez p3, :cond_6

    .line 2492
    if-ne v0, v2, :cond_1

    if-eqz p2, :cond_1

    .line 2493
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    .line 2557
    :cond_0
    :goto_0
    return-void

    .line 2496
    :cond_1
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    if-lez v1, :cond_3

    .line 2497
    if-le v0, v2, :cond_4

    .line 2498
    :goto_1
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    if-le v1, v5, :cond_2

    .line 2499
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    const/16 v3, 0x46

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    .line 2502
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    .line 2503
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    add-int/lit8 v1, v1, -0x40

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    goto :goto_1

    .line 2505
    :cond_2
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    add-int/lit8 v3, v3, -0x1

    rsub-int v3, v3, 0x86

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    .line 2509
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    .line 2525
    :goto_2
    const/4 v1, 0x0

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    .line 2527
    :cond_3
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    int-to-byte v3, v0

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    .line 2529
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    goto :goto_0

    .line 2512
    :cond_4
    :goto_3
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    if-le v1, v5, :cond_5

    .line 2513
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    const/16 v3, 0x45

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    .line 2516
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    .line 2517
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    add-int/lit8 v1, v1, -0x40

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    goto :goto_3

    .line 2519
    :cond_5
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    add-int/lit8 v3, v3, -0x1

    add-int/lit8 v3, v3, 0x5

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    .line 2523
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    goto :goto_2

    .line 2533
    :cond_6
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    int-to-byte v3, v0

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    .line 2535
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    .line 2541
    if-eqz p2, :cond_7

    .line 2542
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilFrenchStart_:I

    if-eq v1, v4, :cond_0

    .line 2545
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->reverseBuffer([B)V

    .line 2546
    iput v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilFrenchStart_:I

    goto/16 :goto_0

    .line 2550
    :cond_7
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilFrenchStart_:I

    if-ne v1, v4, :cond_8

    .line 2551
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    add-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilFrenchStart_:I

    .line 2553
    :cond_8
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilFrenchEnd_:I

    goto/16 :goto_0
.end method

.method private final doSecondaryCompare(Z)I
    .locals 12
    .param p1, "doFrench"    # Z

    .prologue
    const v11, 0xff00

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 3504
    if-nez p1, :cond_7

    .line 3505
    const/4 v0, 0x0

    .line 3506
    .local v0, "soffset":I
    const/4 v3, 0x0

    .line 3508
    .local v3, "toffset":I
    :goto_0
    const/4 v2, 0x0

    .local v2, "sorder":I
    move v1, v0

    .line 3509
    .end local v0    # "soffset":I
    .local v1, "soffset":I
    :goto_1
    if-nez v2, :cond_0

    .line 3510
    iget-object v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "soffset":I
    .restart local v0    # "soffset":I
    aget v9, v9, v1

    and-int v2, v9, v11

    move v1, v0

    .line 3512
    .end local v0    # "soffset":I
    .restart local v1    # "soffset":I
    goto :goto_1

    .line 3513
    :cond_0
    const/4 v5, 0x0

    .local v5, "torder":I
    move v4, v3

    .line 3514
    .end local v3    # "toffset":I
    .local v4, "toffset":I
    :goto_2
    if-nez v5, :cond_1

    .line 3515
    iget-object v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v3, v4, 0x1

    .end local v4    # "toffset":I
    .restart local v3    # "toffset":I
    aget v9, v9, v4

    and-int v5, v9, v11

    move v4, v3

    .line 3517
    .end local v3    # "toffset":I
    .restart local v4    # "toffset":I
    goto :goto_2

    .line 3519
    :cond_1
    if-ne v2, v5, :cond_4

    .line 3520
    iget-object v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    add-int/lit8 v10, v1, -0x1

    aget v9, v9, v10

    if-ne v9, v6, :cond_3

    .line 3522
    iget-object v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v9, v4, -0x1

    aget v7, v7, v9

    if-eq v7, v6, :cond_a

    .line 3567
    .end local v1    # "soffset":I
    .end local v4    # "toffset":I
    :cond_2
    :goto_3
    return v6

    .line 3528
    .restart local v1    # "soffset":I
    .restart local v4    # "toffset":I
    :cond_3
    iget-object v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v10, v4, -0x1

    aget v9, v9, v10

    if-ne v9, v6, :cond_6

    move v6, v7

    .line 3530
    goto :goto_3

    .line 3534
    :cond_4
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    add-int/lit8 v9, v1, -0x1

    aget v8, v8, v9

    if-eq v8, v6, :cond_2

    .line 3538
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v9, v4, -0x1

    aget v8, v8, v9

    if-ne v8, v6, :cond_5

    move v6, v7

    .line 3540
    goto :goto_3

    .line 3542
    :cond_5
    if-lt v2, v5, :cond_2

    move v6, v7

    goto :goto_3

    :cond_6
    move v3, v4

    .end local v4    # "toffset":I
    .restart local v3    # "toffset":I
    move v0, v1

    .line 3544
    .end local v1    # "soffset":I
    .restart local v0    # "soffset":I
    goto :goto_0

    .line 3547
    .end local v0    # "soffset":I
    .end local v2    # "sorder":I
    .end local v3    # "toffset":I
    .end local v5    # "torder":I
    :cond_7
    iput v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilContOffset_:I

    .line 3548
    iput v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilContOffset_:I

    .line 3549
    iget v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBufferSize_:I

    add-int/lit8 v9, v9, -0x2

    iput v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilOffset_:I

    .line 3550
    iget v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBufferSize_:I

    add-int/lit8 v9, v9, -0x2

    iput v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilOffset_:I

    .line 3552
    :cond_8
    invoke-direct {p0, v7}, Lcom/ibm/icu/text/RuleBasedCollator;->getSecondaryFrenchCE(Z)I

    move-result v2

    .line 3553
    .restart local v2    # "sorder":I
    invoke-direct {p0, v8}, Lcom/ibm/icu/text/RuleBasedCollator;->getSecondaryFrenchCE(Z)I

    move-result v5

    .line 3554
    .restart local v5    # "torder":I
    if-ne v2, v5, :cond_b

    .line 3555
    iget v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilOffset_:I

    if-gez v9, :cond_9

    iget v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilOffset_:I

    if-ltz v9, :cond_a

    :cond_9
    iget v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilOffset_:I

    if-ltz v9, :cond_8

    iget-object v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    iget v10, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilOffset_:I

    aget v9, v9, v10

    if-ne v9, v6, :cond_8

    :cond_a
    move v6, v8

    .line 3567
    goto :goto_3

    .line 3563
    :cond_b
    if-lt v2, v5, :cond_2

    move v6, v7

    goto :goto_3
.end method

.method private static final doStringCompare(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 9
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "target"    # Ljava/lang/String;
    .param p2, "offset"    # I

    .prologue
    const v8, 0xd800

    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 3888
    const/4 v2, 0x0

    .line 3889
    .local v2, "schar":C
    const/4 v4, 0x0

    .line 3890
    .local v4, "tchar":C
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 3891
    .local v3, "slength":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    .line 3892
    .local v5, "tlength":I
    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .local v0, "minlength":I
    move v1, p2

    .line 3893
    .end local p2    # "offset":I
    .local v1, "offset":I
    :goto_0
    if-ge v1, v0, :cond_5

    .line 3894
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 3895
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "offset":I
    .restart local p2    # "offset":I
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 3896
    if-eq v2, v4, :cond_4

    .line 3901
    :goto_1
    if-ne v2, v4, :cond_2

    if-ne p2, v0, :cond_2

    .line 3902
    if-le v3, v0, :cond_1

    move v6, v7

    .line 3919
    :cond_0
    :goto_2
    return v6

    .line 3905
    :cond_1
    if-gt v5, v0, :cond_0

    .line 3908
    const/4 v6, 0x0

    goto :goto_2

    .line 3912
    :cond_2
    if-lt v2, v8, :cond_3

    if-lt v4, v8, :cond_3

    .line 3914
    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedCollator;->fixupUTF16(C)C

    move-result v2

    .line 3915
    invoke-static {v4}, Lcom/ibm/icu/text/RuleBasedCollator;->fixupUTF16(C)C

    move-result v4

    .line 3919
    :cond_3
    if-lt v2, v4, :cond_0

    move v6, v7

    goto :goto_2

    :cond_4
    move v1, p2

    .end local p2    # "offset":I
    .restart local v1    # "offset":I
    goto :goto_0

    :cond_5
    move p2, v1

    .end local v1    # "offset":I
    .restart local p2    # "offset":I
    goto :goto_1
.end method

.method private final doTertiary()V
    .locals 5

    .prologue
    .line 3040
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    if-lez v0, :cond_1

    .line 3041
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_common3_:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    .line 3042
    :goto_0
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_topCount3_:I

    if-lt v0, v1, :cond_0

    .line 3043
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_top3_:I

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_topCount3_:I

    sub-int/2addr v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    .line 3045
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    .line 3046
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_topCount3_:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    goto :goto_0

    .line 3048
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_top3_:I

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    sub-int/2addr v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    .line 3050
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    .line 3066
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 3068
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 3069
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    array-length v0, v0

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    add-int/2addr v1, v2

    if-gt v0, v1, :cond_2

    .line 3070
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->increase([BII)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 3073
    :cond_2
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3075
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 3076
    return-void

    .line 3053
    :cond_3
    :goto_2
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottomCount3_:I

    if-le v0, v1, :cond_4

    .line 3054
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottom3_:I

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottomCount3_:I

    add-int/2addr v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    .line 3057
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    .line 3058
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottomCount3_:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    goto :goto_2

    .line 3060
    :cond_4
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottom3_:I

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    add-int/lit8 v3, v3, -0x1

    add-int/2addr v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    .line 3063
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    goto :goto_1
.end method

.method private final doTertiaryBytes(IZ)V
    .locals 5
    .param p1, "tertiary"    # I
    .param p2, "notIsContinuation"    # Z

    .prologue
    .line 2643
    if-eqz p1, :cond_0

    .line 2646
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_common3_:I

    if-ne p1, v1, :cond_1

    if-eqz p2, :cond_1

    .line 2647
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    .line 2694
    :cond_0
    :goto_0
    return-void

    .line 2650
    :cond_1
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_common3_:I

    and-int/lit16 v0, v1, 0xff

    .line 2651
    .local v0, "common3":I
    if-le p1, v0, :cond_3

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_common3_:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_3

    .line 2652
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_addition3_:I

    add-int/2addr p1, v1

    .line 2658
    :cond_2
    :goto_1
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    if-lez v1, :cond_5

    .line 2659
    if-le p1, v0, :cond_6

    .line 2660
    :goto_2
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_topCount3_:I

    if-le v1, v2, :cond_4

    .line 2661
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_top3_:I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_topCount3_:I

    sub-int/2addr v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    .line 2664
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    .line 2665
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_topCount3_:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    goto :goto_2

    .line 2654
    :cond_3
    if-gt p1, v0, :cond_2

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_common3_:I

    const/16 v2, 0xc5

    if-ne v1, v2, :cond_2

    .line 2656
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_addition3_:I

    sub-int/2addr p1, v1

    goto :goto_1

    .line 2667
    :cond_4
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_top3_:I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    .line 2671
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    .line 2687
    :goto_3
    const/4 v1, 0x0

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    .line 2689
    :cond_5
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    int-to-byte v3, p1

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    .line 2691
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    goto :goto_0

    .line 2674
    :cond_6
    :goto_4
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottomCount3_:I

    if-le v1, v2, :cond_7

    .line 2675
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottom3_:I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottomCount3_:I

    add-int/2addr v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    .line 2678
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    .line 2679
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottomCount3_:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    goto :goto_4

    .line 2681
    :cond_7
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottom3_:I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    add-int/lit8 v4, v4, -0x1

    add-int/2addr v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    .line 2685
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    goto :goto_3
.end method

.method private final doTertiaryCompare()I
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 3701
    const/4 v0, 0x0

    .line 3702
    .local v0, "soffset":I
    const/4 v3, 0x0

    .line 3704
    .local v3, "toffset":I
    :goto_0
    const/4 v2, 0x0

    .line 3705
    .local v2, "sorder":I
    const/4 v5, 0x0

    .local v5, "torder":I
    move v1, v0

    .line 3707
    .end local v0    # "soffset":I
    .local v1, "soffset":I
    :goto_1
    and-int/lit8 v8, v2, 0x3f

    if-nez v8, :cond_9

    .line 3708
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "soffset":I
    .restart local v0    # "soffset":I
    aget v8, v8, v1

    iget v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_mask3_:I

    and-int v2, v8, v9

    .line 3709
    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 3710
    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseSwitch_:I

    xor-int/2addr v2, v8

    move v1, v0

    .line 3711
    .end local v0    # "soffset":I
    .restart local v1    # "soffset":I
    goto :goto_1

    .line 3713
    .end local v1    # "soffset":I
    .restart local v0    # "soffset":I
    :cond_0
    and-int/lit8 v2, v2, 0x3f

    move v1, v0

    .line 3715
    .end local v0    # "soffset":I
    .restart local v1    # "soffset":I
    goto :goto_1

    .line 3718
    .end local v3    # "toffset":I
    .local v4, "toffset":I
    :goto_2
    and-int/lit8 v8, v5, 0x3f

    if-nez v8, :cond_2

    .line 3719
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v3, v4, 0x1

    .end local v4    # "toffset":I
    .restart local v3    # "toffset":I
    aget v8, v8, v4

    iget v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_mask3_:I

    and-int v5, v8, v9

    .line 3720
    invoke-static {v5}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v8

    if-nez v8, :cond_1

    .line 3721
    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseSwitch_:I

    xor-int/2addr v5, v8

    move v4, v3

    .line 3722
    .end local v3    # "toffset":I
    .restart local v4    # "toffset":I
    goto :goto_2

    .line 3724
    .end local v4    # "toffset":I
    .restart local v3    # "toffset":I
    :cond_1
    and-int/lit8 v5, v5, 0x3f

    move v4, v3

    .line 3726
    .end local v3    # "toffset":I
    .restart local v4    # "toffset":I
    goto :goto_2

    .line 3728
    :cond_2
    if-ne v2, v5, :cond_5

    .line 3729
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    add-int/lit8 v9, v1, -0x1

    aget v8, v8, v9

    if-ne v8, v6, :cond_4

    .line 3731
    iget-object v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v8, v4, -0x1

    aget v7, v7, v8

    if-eq v7, v6, :cond_8

    .line 3754
    :cond_3
    :goto_3
    return v6

    .line 3737
    :cond_4
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v9, v4, -0x1

    aget v8, v8, v9

    if-ne v8, v6, :cond_7

    move v6, v7

    .line 3739
    goto :goto_3

    .line 3743
    :cond_5
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    add-int/lit8 v9, v1, -0x1

    aget v8, v8, v9

    if-eq v8, v6, :cond_3

    .line 3747
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    add-int/lit8 v9, v4, -0x1

    aget v8, v8, v9

    if-ne v8, v6, :cond_6

    move v6, v7

    .line 3749
    goto :goto_3

    .line 3751
    :cond_6
    if-lt v2, v5, :cond_3

    move v6, v7

    goto :goto_3

    :cond_7
    move v3, v4

    .end local v4    # "toffset":I
    .restart local v3    # "toffset":I
    move v0, v1

    .line 3753
    .end local v1    # "soffset":I
    .restart local v0    # "soffset":I
    goto :goto_0

    .line 3754
    .end local v0    # "soffset":I
    .end local v3    # "toffset":I
    .restart local v1    # "soffset":I
    .restart local v4    # "toffset":I
    :cond_8
    const/4 v6, 0x0

    goto :goto_3

    .end local v4    # "toffset":I
    .restart local v3    # "toffset":I
    :cond_9
    move v4, v3

    .end local v3    # "toffset":I
    .restart local v4    # "toffset":I
    goto :goto_2
.end method

.method private final endPrimaryCompare(II)I
    .locals 7
    .param p1, "sorder"    # I
    .param p2, "torder"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v4, -0x1

    .line 3356
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    iget v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBufferSize_:I

    add-int/lit8 v6, v6, -0x1

    aget v5, v5, v6

    if-ne v5, v4, :cond_1

    move v0, v2

    .line 3359
    .local v0, "isSourceNullOrder":Z
    :goto_0
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    iget v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBufferSize_:I

    add-int/lit8 v6, v6, -0x1

    aget v5, v5, v6

    if-ne v5, v4, :cond_2

    move v1, v2

    .line 3362
    .local v1, "isTargetNullOrder":Z
    :goto_1
    iput v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBufferSize_:I

    .line 3363
    iput v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBufferSize_:I

    .line 3364
    if-eqz v0, :cond_3

    move v2, v4

    .line 3376
    :cond_0
    :goto_2
    return v2

    .end local v0    # "isSourceNullOrder":Z
    .end local v1    # "isTargetNullOrder":Z
    :cond_1
    move v0, v3

    .line 3356
    goto :goto_0

    .restart local v0    # "isSourceNullOrder":Z
    :cond_2
    move v1, v3

    .line 3359
    goto :goto_1

    .line 3367
    .restart local v1    # "isTargetNullOrder":Z
    :cond_3
    if-nez v1, :cond_0

    .line 3371
    ushr-int/lit8 p1, p1, 0x10

    .line 3372
    ushr-int/lit8 p2, p2, 0x10

    .line 3373
    if-ge p1, p2, :cond_0

    move v2, v4

    .line 3374
    goto :goto_2
.end method

.method private static final fixupUTF16(C)C
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 3927
    const v0, 0xe000

    if-lt p0, v0, :cond_0

    .line 3928
    add-int/lit16 v0, p0, -0x800

    int-to-char p0, v0

    .line 3933
    :goto_0
    return p0

    .line 3931
    :cond_0
    add-int/lit16 v0, p0, 0x2000

    int-to-char p0, v0

    goto :goto_0
.end method

.method private final getFirstUnmatchedOffset(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    .line 3138
    const/4 v1, 0x0

    .line 3139
    .local v1, "result":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 3140
    .local v4, "slength":I
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v6

    .line 3141
    .local v6, "tlength":I
    move v0, v4

    .line 3142
    .local v0, "minlength":I
    if-le v0, v6, :cond_0

    .line 3143
    move v0, v6

    .line 3146
    :cond_0
    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v8

    if-ne v7, v8, :cond_1

    .line 3147
    add-int/lit8 v1, v1, 0x1

    .line 3148
    goto :goto_0

    .line 3149
    :cond_1
    if-lez v1, :cond_3

    .line 3154
    const/4 v3, 0x0

    .line 3155
    .local v3, "schar":C
    const/4 v5, 0x0

    .line 3156
    .local v5, "tchar":C
    if-ge v1, v0, :cond_4

    .line 3157
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 3158
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 3175
    :goto_1
    invoke-virtual {p0, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p0, v5}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 3184
    :cond_2
    add-int/lit8 v1, v1, -0x1

    .line 3186
    if-lez v1, :cond_3

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {p0, v7}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v7

    if-nez v7, :cond_2

    .end local v3    # "schar":C
    .end local v5    # "tchar":C
    :cond_3
    move v2, v1

    .line 3189
    .end local v1    # "result":I
    .local v2, "result":I
    :goto_2
    return v2

    .line 3161
    .end local v2    # "result":I
    .restart local v1    # "result":I
    .restart local v3    # "schar":C
    .restart local v5    # "tchar":C
    :cond_4
    add-int/lit8 v7, v0, -0x1

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 3162
    invoke-virtual {p0, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 3163
    move v5, v3

    .line 3164
    goto :goto_1

    .line 3165
    :cond_5
    if-ne v4, v6, :cond_6

    move v2, v1

    .line 3166
    .end local v1    # "result":I
    .restart local v2    # "result":I
    goto :goto_2

    .line 3168
    .end local v2    # "result":I
    .restart local v1    # "result":I
    :cond_6
    if-ge v4, v6, :cond_7

    .line 3169
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 3170
    goto :goto_1

    .line 3172
    :cond_7
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    goto :goto_1
.end method

.method private getLatinOneContraction(IILjava/lang/String;)I
    .locals 9
    .param p1, "strength"    # I
    .param p2, "CE"    # I
    .param p3, "s"    # Ljava/lang/String;

    .prologue
    .line 4300
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    .line 4302
    .local v3, "len":I
    and-int/lit16 v7, p2, 0xfff

    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    sub-int v0, v7, v8

    .line 4303
    .local v0, "UCharOffset":I
    const/4 v4, 0x1

    .line 4304
    .local v4, "offset":I
    const v7, 0xfff000

    and-int/2addr v7, p2

    ushr-int/lit8 v2, v7, 0xc

    .line 4305
    .local v2, "latinOneOffset":I
    const/4 v5, 0x0

    .local v5, "schar":C
    const/4 v6, 0x0

    .line 4317
    .local v6, "tchar":C
    :goto_0
    iget-object v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    iget v7, v7, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    if-ne v7, v3, :cond_0

    .line 4318
    iget-object v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    mul-int/2addr v8, p1

    add-int/2addr v8, v2

    aget v7, v7, v8

    .line 4344
    :goto_1
    return v7

    .line 4320
    :cond_0
    iget-object v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    iget v7, v7, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    invoke-virtual {p3, v7}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 4324
    :goto_2
    iget-object v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    add-int v8, v0, v4

    aget-char v6, v7, v8

    if-le v5, v6, :cond_1

    .line 4325
    add-int/lit8 v4, v4, 0x1

    .line 4326
    goto :goto_2

    .line 4328
    :cond_1
    if-ne v5, v6, :cond_2

    .line 4329
    iget-object v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    iget v8, v7, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    .line 4330
    iget-object v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    mul-int/2addr v8, p1

    add-int/2addr v8, v2

    add-int/2addr v8, v4

    aget v7, v7, v8

    goto :goto_1

    .line 4334
    :cond_2
    const/16 v7, 0xff

    if-le v5, v7, :cond_3

    .line 4335
    const/high16 v7, -0x1000000

    goto :goto_1

    .line 4338
    :cond_3
    iget-object v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v7, v5}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v1

    .line 4339
    .local v1, "isZeroCE":I
    if-nez v1, :cond_4

    .line 4340
    iget-object v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    iget v8, v7, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;->index:I

    goto :goto_0

    .line 4344
    :cond_4
    iget-object v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    mul-int/2addr v8, p1

    add-int/2addr v8, v2

    aget v7, v7, v8

    goto :goto_1
.end method

.method private final getPrimaryShiftedCompareCE(Lcom/ibm/icu/text/CollationElementIterator;IZ)I
    .locals 6
    .param p1, "coleiter"    # Lcom/ibm/icu/text/CollationElementIterator;
    .param p2, "lowestpvalue"    # I
    .param p3, "isSrc"    # Z

    .prologue
    const/high16 v5, -0x10000

    .line 3394
    const/4 v3, 0x0

    .line 3395
    .local v3, "shifted":Z
    const/4 v2, 0x0

    .line 3396
    .local v2, "result":I
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    .line 3397
    .local v0, "cebuffer":[I
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBufferSize_:I

    .line 3398
    .local v1, "cebuffersize":I
    if-nez p3, :cond_0

    .line 3399
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    .line 3400
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBufferSize_:I

    .line 3403
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v2

    .line 3404
    const/4 v4, -0x1

    if-ne v2, v4, :cond_1

    .line 3405
    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([III)[I

    move-result-object v0

    .line 3406
    add-int/lit8 v1, v1, 0x1

    .line 3466
    :goto_1
    if-eqz p3, :cond_8

    .line 3467
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    .line 3468
    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBufferSize_:I

    .line 3474
    :goto_2
    and-int/2addr v2, v5

    .line 3475
    return v2

    .line 3409
    :cond_1
    if-eqz v2, :cond_0

    if-eqz v3, :cond_2

    and-int v4, v2, v5

    if-eqz v4, :cond_0

    .line 3417
    :cond_2
    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 3418
    and-int v4, v2, v5

    if-eqz v4, :cond_4

    .line 3421
    if-eqz v3, :cond_3

    .line 3422
    and-int v4, v2, v5

    or-int/lit16 v2, v4, 0xc0

    .line 3425
    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([III)[I

    move-result-object v0

    .line 3426
    add-int/lit8 v1, v1, 0x1

    .line 3427
    goto :goto_0

    .line 3430
    :cond_3
    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([III)[I

    move-result-object v0

    .line 3431
    add-int/lit8 v1, v1, 0x1

    .line 3432
    goto :goto_1

    .line 3436
    :cond_4
    if-nez v3, :cond_0

    .line 3437
    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([III)[I

    move-result-object v0

    .line 3438
    add-int/lit8 v1, v1, 0x1

    .line 3439
    goto :goto_0

    .line 3443
    :cond_5
    and-int v4, v2, v5

    invoke-static {v4, p2}, Lcom/ibm/icu/impl/Utility;->compareUnsigned(II)I

    move-result v4

    if-lez v4, :cond_6

    .line 3445
    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([III)[I

    move-result-object v0

    .line 3446
    add-int/lit8 v1, v1, 0x1

    .line 3447
    goto :goto_1

    .line 3450
    :cond_6
    and-int v4, v2, v5

    if-eqz v4, :cond_7

    .line 3451
    const/4 v3, 0x1

    .line 3452
    and-int/2addr v2, v5

    .line 3453
    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([III)[I

    move-result-object v0

    .line 3454
    add-int/lit8 v1, v1, 0x1

    .line 3455
    goto :goto_0

    .line 3458
    :cond_7
    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([III)[I

    move-result-object v0

    .line 3459
    add-int/lit8 v1, v1, 0x1

    .line 3460
    const/4 v3, 0x0

    .line 3461
    goto :goto_0

    .line 3471
    :cond_8
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    .line 3472
    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBufferSize_:I

    goto :goto_2
.end method

.method private final getSecondaryFrenchCE(Z)I
    .locals 6
    .param p1, "isSrc"    # Z

    .prologue
    .line 3577
    const/4 v4, 0x0

    .line 3578
    .local v4, "result":I
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilOffset_:I

    .line 3579
    .local v2, "offset":I
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilContOffset_:I

    .line 3580
    .local v1, "continuationoffset":I
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    .line 3581
    .local v0, "cebuffer":[I
    if-nez p1, :cond_6

    .line 3582
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilOffset_:I

    .line 3583
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilContOffset_:I

    .line 3584
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    move v3, v2

    .line 3588
    .end local v2    # "offset":I
    .local v3, "offset":I
    :goto_0
    if-nez v4, :cond_3

    if-ltz v3, :cond_3

    .line 3589
    if-nez v1, :cond_2

    .line 3590
    aget v4, v0, v3

    move v2, v3

    .line 3591
    .end local v3    # "offset":I
    .restart local v2    # "offset":I
    :goto_1
    add-int/lit8 v3, v2, -0x1

    .end local v2    # "offset":I
    .restart local v3    # "offset":I
    aget v5, v0, v2

    invoke-static {v5}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v5

    if-eqz v5, :cond_0

    move v2, v3

    .line 3592
    .end local v3    # "offset":I
    .restart local v2    # "offset":I
    goto :goto_1

    .line 3595
    .end local v2    # "offset":I
    .restart local v3    # "offset":I
    :cond_0
    add-int/lit8 v5, v3, 0x1

    aget v5, v0, v5

    invoke-static {v5}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 3597
    move v1, v3

    .line 3598
    add-int/lit8 v2, v3, 0x2

    .line 3611
    .end local v3    # "offset":I
    .restart local v2    # "offset":I
    :cond_1
    :goto_2
    const v5, 0xff00

    and-int/2addr v4, v5

    move v3, v2

    .line 3612
    .end local v2    # "offset":I
    .restart local v3    # "offset":I
    goto :goto_0

    .line 3602
    :cond_2
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "offset":I
    .restart local v2    # "offset":I
    aget v4, v0, v3

    .line 3603
    invoke-static {v4}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 3605
    move v2, v1

    .line 3607
    const/4 v1, 0x0

    move v3, v2

    .line 3608
    .end local v2    # "offset":I
    .restart local v3    # "offset":I
    goto :goto_0

    .line 3613
    :cond_3
    if-eqz p1, :cond_4

    .line 3614
    iput v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilOffset_:I

    .line 3615
    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilContOffset_:I

    .line 3621
    :goto_3
    return v4

    .line 3618
    :cond_4
    iput v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilOffset_:I

    .line 3619
    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilContOffset_:I

    goto :goto_3

    :cond_5
    move v2, v3

    .end local v3    # "offset":I
    .restart local v2    # "offset":I
    goto :goto_2

    :cond_6
    move v3, v2

    .end local v2    # "offset":I
    .restart local v3    # "offset":I
    goto :goto_0
.end method

.method private final getSortKey(Ljava/lang/String;ZIILcom/ibm/icu/text/RawCollationKey;)V
    .locals 3
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "doFrench"    # Z
    .param p3, "commonBottom4"    # I
    .param p4, "bottomCount4"    # I
    .param p5, "key"    # Lcom/ibm/icu/text/RawCollationKey;

    .prologue
    const/4 v2, 0x0

    .line 2862
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare2_:Z

    if-eqz v0, :cond_0

    .line 2863
    invoke-direct {p0, p2}, Lcom/ibm/icu/text/RuleBasedCollator;->doSecondary(Z)V

    .line 2866
    :cond_0
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare0_:Z

    if-eqz v0, :cond_1

    .line 2867
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->doCase()V

    .line 2869
    :cond_1
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare3_:Z

    if-eqz v0, :cond_2

    .line 2870
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->doTertiary()V

    .line 2871
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare4_:Z

    if-eqz v0, :cond_2

    .line 2872
    invoke-direct {p0, p3, p4}, Lcom/ibm/icu/text/RuleBasedCollator;->doQuaternary(II)V

    .line 2873
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare5_:Z

    if-eqz v0, :cond_2

    .line 2874
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/RuleBasedCollator;->doIdentical(Ljava/lang/String;)V

    .line 2879
    :cond_2
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->append([BIB)[B

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 2880
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 2882
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    invoke-virtual {p5, v0, v2, v1}, Lcom/ibm/icu/text/RawCollationKey;->set([BII)Lcom/ibm/icu/util/ByteArrayWrapper;

    .line 2883
    return-void
.end method

.method private final getSortKeyBytes(Ljava/lang/String;ZBII)V
    .locals 15
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "doFrench"    # Z
    .param p3, "hiragana4"    # B
    .param p4, "commonBottom4"    # I
    .param p5, "bottomCount4"    # I

    .prologue
    .line 2747
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    if-nez v3, :cond_0

    .line 2748
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->initUtility(Z)V

    .line 2750
    :cond_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->getDecomposition()I

    move-result v10

    .line 2751
    .local v10, "backupDecomposition":I
    const/16 v3, 0x10

    invoke-virtual {p0, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->setDecomposition(I)V

    .line 2752
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/ibm/icu/impl/StringUCharacterIterator;->setText(Ljava/lang/String;)V

    .line 2753
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-virtual {v3, v8}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Lcom/ibm/icu/text/UCharacterIterator;)V

    .line 2754
    const/4 v3, -0x1

    iput v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilFrenchStart_:I

    .line 2755
    const/4 v3, -0x1

    iput v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilFrenchEnd_:I

    .line 2760
    const/4 v6, 0x0

    .line 2761
    .local v6, "doShift":Z
    const/4 v5, 0x0

    .line 2763
    .local v5, "notIsContinuation":Z
    const/4 v7, 0x0

    .line 2764
    .local v7, "leadPrimary":I
    const/4 v11, 0x0

    .line 2767
    .local v11, "caseShift":I
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v4

    .line 2768
    .local v4, "ce":I
    const/4 v3, -0x1

    if-ne v4, v3, :cond_3

    .line 2838
    invoke-virtual {p0, v10}, Lcom/ibm/icu/text/RuleBasedCollator;->setDecomposition(I)V

    .line 2839
    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilFrenchStart_:I

    const/4 v8, -0x1

    if-eq v3, v8, :cond_2

    .line 2841
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    invoke-direct {p0, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->reverseBuffer([B)V

    .line 2843
    :cond_2
    return-void

    .line 2772
    :cond_3
    if-eqz v4, :cond_1

    .line 2776
    invoke-static {v4}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v3

    if-nez v3, :cond_d

    const/4 v5, 0x1

    .line 2784
    :goto_1
    const/high16 v3, -0x10000

    and-int/2addr v3, v4

    if-nez v3, :cond_e

    const/4 v12, 0x1

    .line 2787
    .local v12, "isPrimaryByteIgnorable":Z
    :goto_2
    ushr-int/lit8 v3, v4, 0x10

    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_variableTopValue_:I

    if-gt v3, v8, :cond_f

    const/4 v13, 0x1

    .line 2789
    .local v13, "isSmallerThanVariableTop":Z
    :goto_3
    iget-boolean v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isAlternateHandlingShifted_:Z

    if-eqz v3, :cond_5

    if-eqz v5, :cond_4

    if-eqz v13, :cond_4

    if-eqz v12, :cond_6

    :cond_4
    if-nez v5, :cond_5

    if-nez v6, :cond_6

    :cond_5
    if-eqz v6, :cond_10

    if-eqz v12, :cond_10

    :cond_6
    const/4 v6, 0x1

    .line 2794
    :goto_4
    if-eqz v6, :cond_7

    if-nez v12, :cond_1

    :cond_7
    move-object v3, p0

    move/from16 v8, p4

    move/from16 v9, p5

    .line 2802
    invoke-direct/range {v3 .. v9}, Lcom/ibm/icu/text/RuleBasedCollator;->doPrimaryBytes(IZZIII)I

    move-result v7

    .line 2805
    if-nez v6, :cond_1

    .line 2808
    iget-boolean v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare2_:Z

    if-eqz v3, :cond_8

    .line 2809
    move/from16 v0, p2

    invoke-direct {p0, v4, v5, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->doSecondaryBytes(IZZ)V

    .line 2812
    :cond_8
    and-int/lit16 v14, v4, 0xff

    .line 2813
    .local v14, "t":I
    if-nez v5, :cond_9

    .line 2814
    and-int/lit16 v14, v4, -0xc1

    .line 2817
    :cond_9
    iget-boolean v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare0_:Z

    if-eqz v3, :cond_11

    if-eqz v12, :cond_a

    iget-boolean v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare2_:Z

    if-eqz v3, :cond_11

    .line 2821
    :cond_a
    invoke-direct {p0, v14, v5, v11}, Lcom/ibm/icu/text/RuleBasedCollator;->doCaseBytes(IZI)I

    move-result v11

    .line 2827
    :cond_b
    :goto_5
    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_mask3_:I

    and-int/2addr v14, v3

    .line 2829
    iget-boolean v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare3_:Z

    if-eqz v3, :cond_c

    .line 2830
    invoke-direct {p0, v14, v5}, Lcom/ibm/icu/text/RuleBasedCollator;->doTertiaryBytes(IZ)V

    .line 2833
    :cond_c
    iget-boolean v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare4_:Z

    if-eqz v3, :cond_1

    if-eqz v5, :cond_1

    .line 2834
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-boolean v3, v3, Lcom/ibm/icu/text/CollationElementIterator;->m_isCodePointHiragana_:Z

    move/from16 v0, p4

    move/from16 v1, p5

    move/from16 v2, p3

    invoke-direct {p0, v3, v0, v1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->doQuaternaryBytes(ZIIB)V

    goto/16 :goto_0

    .line 2776
    .end local v12    # "isPrimaryByteIgnorable":Z
    .end local v13    # "isSmallerThanVariableTop":Z
    .end local v14    # "t":I
    :cond_d
    const/4 v5, 0x0

    goto :goto_1

    .line 2784
    :cond_e
    const/4 v12, 0x0

    goto :goto_2

    .line 2787
    .restart local v12    # "isPrimaryByteIgnorable":Z
    :cond_f
    const/4 v13, 0x0

    goto :goto_3

    .line 2789
    .restart local v13    # "isSmallerThanVariableTop":Z
    :cond_10
    const/4 v6, 0x0

    goto :goto_4

    .line 2823
    .restart local v14    # "t":I
    :cond_11
    if-eqz v5, :cond_b

    .line 2824
    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseSwitch_:I

    xor-int/2addr v14, v3

    goto :goto_5
.end method

.method static getTag(I)I
    .locals 1
    .param p0, "ce"    # I

    .prologue
    .line 2013
    const/high16 v0, 0xf000000

    and-int/2addr v0, p0

    shr-int/lit8 v0, v0, 0x18

    return v0
.end method

.method private static final increase([BII)[B
    .locals 3
    .param p0, "buffer"    # [B
    .param p1, "size"    # I
    .param p2, "incrementsize"    # I

    .prologue
    const/4 v2, 0x0

    .line 2998
    array-length v1, p0

    add-int/2addr v1, p2

    new-array v0, v1, [B

    .line 2999
    .local v0, "result":[B
    invoke-static {p0, v2, v0, v2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3000
    return-object v0
.end method

.method private static final increase([III)[I
    .locals 3
    .param p0, "buffer"    # [I
    .param p1, "size"    # I
    .param p2, "incrementsize"    # I

    .prologue
    const/4 v2, 0x0

    .line 3013
    array-length v1, p0

    add-int/2addr v1, p2

    new-array v0, v1, [I

    .line 3014
    .local v0, "result":[I
    invoke-static {p0, v2, v0, v2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3015
    return-object v0
.end method

.method private final init()V
    .locals 3

    .prologue
    const/16 v2, 0x300

    const/4 v1, 0x0

    .line 4006
    iput-char v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minUnsafe_:C

    :goto_0
    iget-char v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minUnsafe_:C

    if-ge v0, v2, :cond_0

    .line 4009
    iget-char v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minUnsafe_:C

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4014
    :cond_0
    iput-char v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minContractionEnd_:C

    .line 4015
    :goto_1
    iget-char v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minContractionEnd_:C

    if-ge v0, v2, :cond_1

    .line 4018
    iget-char v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minContractionEnd_:C

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->isContractionEnd(C)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4022
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneFailed_:Z

    .line 4023
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultStrength_:I

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->setStrength(I)V

    .line 4024
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultDecomposition_:I

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->setDecomposition(I)V

    .line 4025
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultVariableTopValue_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_variableTopValue_:I

    .line 4026
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsFrenchCollation_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    .line 4027
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsAlternateHandlingShifted_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isAlternateHandlingShifted_:Z

    .line 4028
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsCaseLevel_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isCaseLevel_:Z

    .line 4029
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultCaseFirst_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    .line 4030
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsHiragana4_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isHiragana4_:Z

    .line 4031
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsNumericCollation_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isNumericCollation_:Z

    .line 4032
    iput-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneFailed_:Z

    .line 4033
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 4034
    return-void

    .line 4007
    :cond_2
    iget-char v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minUnsafe_:C

    add-int/lit8 v0, v0, 0x1

    int-to-char v0, v0

    iput-char v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minUnsafe_:C

    goto :goto_0

    .line 4016
    :cond_3
    iget-char v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minContractionEnd_:C

    add-int/lit8 v0, v0, 0x1

    int-to-char v0, v0

    iput-char v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minContractionEnd_:C

    goto :goto_1
.end method

.method private init(Ljava/lang/String;)V
    .locals 2
    .param p1, "rules"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 2263
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->setWithUCAData()V

    .line 2264
    new-instance v0, Lcom/ibm/icu/text/CollationParsedRuleBuilder;

    invoke-direct {v0, p1}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;-><init>(Ljava/lang/String;)V

    .line 2266
    .local v0, "builder":Lcom/ibm/icu/text/CollationParsedRuleBuilder;
    invoke-virtual {v0, p0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder;->setRules(Lcom/ibm/icu/text/RuleBasedCollator;)V

    .line 2267
    iput-object p1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    .line 2268
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->init()V

    .line 2269
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->initUtility(Z)V

    .line 2270
    return-void
.end method

.method private final initUtility(Z)V
    .locals 4
    .param p1, "allocate"    # Z

    .prologue
    const/16 v3, 0x200

    const/16 v2, 0x80

    const/4 v0, 0x0

    .line 4040
    if-eqz p1, :cond_1

    .line 4041
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    if-nez v0, :cond_0

    .line 4042
    new-instance v0, Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-direct {v0}, Lcom/ibm/icu/impl/StringUCharacterIterator;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    .line 4043
    new-instance v0, Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-direct {v0, v1, p0}, Lcom/ibm/icu/text/CollationElementIterator;-><init>(Lcom/ibm/icu/text/UCharacterIterator;Lcom/ibm/icu/text/RuleBasedCollator;)V

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    .line 4044
    new-instance v0, Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-direct {v0}, Lcom/ibm/icu/impl/StringUCharacterIterator;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    .line 4045
    new-instance v0, Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-direct {v0, v1, p0}, Lcom/ibm/icu/text/CollationElementIterator;-><init>(Lcom/ibm/icu/text/UCharacterIterator;Lcom/ibm/icu/text/RuleBasedCollator;)V

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    .line 4046
    const/16 v0, 0x20

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes0_:[B

    .line 4047
    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 4048
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    .line 4049
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    .line 4050
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    .line 4051
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    .line 4052
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    .line 4067
    :cond_0
    :goto_0
    return-void

    .line 4055
    :cond_1
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    .line 4056
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    .line 4057
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    .line 4058
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    .line 4059
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes0_:[B

    .line 4060
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes1_:[B

    .line 4061
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes2_:[B

    .line 4062
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes3_:[B

    .line 4063
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytes4_:[B

    .line 4064
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilCEBuffer_:[I

    .line 4065
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_tgtUtilCEBuffer_:[I

    goto :goto_0
.end method

.method static final isContinuation(I)Z
    .locals 2
    .param p0, "ce"    # I

    .prologue
    .line 2033
    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    and-int/lit16 v0, p0, 0xc0

    const/16 v1, 0xc0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isSpecial(I)Z
    .locals 2
    .param p0, "ce"    # I

    .prologue
    const/high16 v1, -0x10000000

    .line 2023
    and-int v0, p0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processSpecials(Lcom/ibm/icu/text/RuleBasedCollator$contContext;)V
    .locals 10
    .param p1, "c"    # Lcom/ibm/icu/text/RuleBasedCollator$contContext;

    .prologue
    .line 827
    const/16 v3, 0x200

    .line 828
    .local v3, "internalBufferSize":I
    new-instance v7, Lcom/ibm/icu/impl/TrieIterator;

    iget-object v8, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->coll:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v8, v8, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-direct {v7, v8}, Lcom/ibm/icu/impl/TrieIterator;-><init>(Lcom/ibm/icu/impl/Trie;)V

    .line 830
    .local v7, "trieiterator":Lcom/ibm/icu/impl/TrieIterator;
    new-instance v2, Lcom/ibm/icu/util/RangeValueIterator$Element;

    invoke-direct {v2}, Lcom/ibm/icu/util/RangeValueIterator$Element;-><init>()V

    .line 831
    .local v2, "element":Lcom/ibm/icu/util/RangeValueIterator$Element;
    :cond_0
    :goto_0
    invoke-virtual {v7, v2}, Lcom/ibm/icu/impl/TrieIterator;->next(Lcom/ibm/icu/util/RangeValueIterator$Element;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 832
    iget v5, v2, Lcom/ibm/icu/util/RangeValueIterator$Element;->start:I

    .line 833
    .local v5, "start":I
    iget v4, v2, Lcom/ibm/icu/util/RangeValueIterator$Element;->limit:I

    .line 834
    .local v4, "limit":I
    iget v0, v2, Lcom/ibm/icu/util/RangeValueIterator$Element;->value:I

    .line 835
    .local v0, "CE":I
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 837
    .local v1, "contraction":Ljava/lang/StringBuffer;
    invoke-static {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 838
    invoke-static {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v8

    const/16 v9, 0xb

    if-ne v8, v9, :cond_1

    iget-boolean v8, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->addPrefixes:Z

    if-nez v8, :cond_2

    :cond_1
    invoke-static {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_4

    .line 839
    :cond_2
    :goto_1
    if-ge v5, v4, :cond_0

    .line 842
    iget-object v8, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->removedContractions:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v8, :cond_3

    iget-object v8, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->removedContractions:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v8, v5}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 843
    add-int/lit8 v5, v5, 0x1

    .line 844
    goto :goto_1

    .line 848
    :cond_3
    int-to-char v8, v5

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 849
    invoke-direct {p0, p1, v1, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->addSpecial(Lcom/ibm/icu/text/RuleBasedCollator$contContext;Ljava/lang/StringBuffer;I)V

    .line 850
    add-int/lit8 v5, v5, 0x1

    .line 851
    goto :goto_1

    .line 852
    :cond_4
    iget-object v8, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->expansions:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v8, :cond_0

    invoke-static {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    move v6, v5

    .line 853
    .end local v5    # "start":I
    .local v6, "start":I
    :goto_2
    if-ge v6, v4, :cond_6

    .line 854
    iget-object v8, p1, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->expansions:Lcom/ibm/icu/text/UnicodeSet;

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "start":I
    .restart local v5    # "start":I
    invoke-virtual {v8, v6}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    move v6, v5

    .line 855
    .end local v5    # "start":I
    .restart local v6    # "start":I
    goto :goto_2

    .line 859
    .end local v0    # "CE":I
    .end local v1    # "contraction":Ljava/lang/StringBuffer;
    .end local v4    # "limit":I
    .end local v6    # "start":I
    :cond_5
    return-void

    .restart local v0    # "CE":I
    .restart local v1    # "contraction":Ljava/lang/StringBuffer;
    .restart local v4    # "limit":I
    .restart local v6    # "start":I
    :cond_6
    move v5, v6

    .end local v6    # "start":I
    .restart local v5    # "start":I
    goto :goto_0
.end method

.method private final resizeLatinOneTable(I)V
    .locals 5
    .param p1, "newSize"    # I

    .prologue
    const/4 v3, 0x0

    .line 4133
    mul-int/lit8 v2, p1, 0x3

    new-array v0, v2, [I

    .line 4134
    .local v0, "newTable":[I
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    if-ge p1, v2, :cond_0

    move v1, p1

    .line 4136
    .local v1, "sizeToCopy":I
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    invoke-static {v2, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4137
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    invoke-static {v2, v3, v0, p1, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4138
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    mul-int/lit8 v3, v3, 0x2

    mul-int/lit8 v4, p1, 0x2

    invoke-static {v2, v3, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4139
    iput p1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    .line 4140
    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    .line 4141
    return-void

    .line 4134
    .end local v1    # "sizeToCopy":I
    :cond_0
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    goto :goto_0
.end method

.method private reverseBuffer([B)V
    .locals 6
    .param p1, "buffer"    # [B

    .prologue
    .line 2565
    iget v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilFrenchStart_:I

    .line 2566
    .local v3, "start":I
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilFrenchEnd_:I

    .local v1, "end":I
    move v2, v1

    .end local v1    # "end":I
    .local v2, "end":I
    move v4, v3

    .line 2567
    .end local v3    # "start":I
    .local v4, "start":I
    :goto_0
    if-ge v4, v2, :cond_0

    .line 2568
    aget-byte v0, p1, v4

    .line 2569
    .local v0, "b":B
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "start":I
    .restart local v3    # "start":I
    aget-byte v5, p1, v2

    aput-byte v5, p1, v4

    .line 2570
    add-int/lit8 v1, v2, -0x1

    .end local v2    # "end":I
    .restart local v1    # "end":I
    aput-byte v0, p1, v2

    move v2, v1

    .end local v1    # "end":I
    .restart local v2    # "end":I
    move v4, v3

    .line 2571
    .end local v3    # "start":I
    .restart local v4    # "start":I
    goto :goto_0

    .line 2572
    .end local v0    # "b":B
    :cond_0
    return-void
.end method

.method private final setUpLatinOne()Z
    .locals 14

    .prologue
    .line 4144
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    if-eqz v11, :cond_0

    iget-boolean v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_reallocLatinOneCEs_:Z

    if-eqz v11, :cond_4

    .line 4145
    :cond_0
    const/16 v11, 0x393

    new-array v11, v11, [I

    iput-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    .line 4146
    const/16 v11, 0x131

    iput v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    .line 4147
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_reallocLatinOneCEs_:Z

    .line 4151
    :goto_0
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    if-nez v11, :cond_1

    .line 4152
    new-instance v11, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    const/4 v12, 0x0

    invoke-direct {v11, p0, v12}, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;-><init>(Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/RuleBasedCollator$1;)V

    iput-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    .line 4154
    :cond_1
    const/4 v2, 0x0

    .line 4157
    .local v2, "ch":C
    const-string/jumbo v11, ""

    invoke-virtual {p0, v11}, Lcom/ibm/icu/text/RuleBasedCollator;->getCollationElementIterator(Ljava/lang/String;)Lcom/ibm/icu/text/CollationElementIterator;

    move-result-object v6

    .line 4159
    .local v6, "it":Lcom/ibm/icu/text/CollationElementIterator;
    new-instance v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;

    const/4 v11, 0x0

    invoke-direct {v9, p0, v11}, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;-><init>(Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/RuleBasedCollator$1;)V

    .line 4160
    .local v9, "s":Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;
    const/4 v0, 0x0

    .line 4161
    .local v0, "CE":I
    const/16 v3, 0x100

    .line 4163
    .local v3, "contractionOffset":C
    const/4 v2, 0x0

    :goto_1
    const/16 v11, 0xff

    if-gt v2, v11, :cond_15

    .line 4164
    const/16 v11, 0x18

    iput v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->primShift:I

    const/16 v11, 0x18

    iput v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->secShift:I

    const/16 v11, 0x18

    iput v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->terShift:I

    .line 4165
    const/16 v11, 0x100

    if-ge v2, v11, :cond_5

    .line 4166
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v11, v2}, Lcom/ibm/icu/impl/IntTrie;->getLatin1LinearValue(C)I

    move-result v0

    .line 4173
    :cond_2
    :goto_2
    invoke-static {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v11

    if-nez v11, :cond_6

    .line 4174
    invoke-direct {p0, v2, v0, v9}, Lcom/ibm/icu/text/RuleBasedCollator;->addLatinOneEntry(CILcom/ibm/icu/text/RuleBasedCollator$shiftValues;)V

    .line 4163
    :cond_3
    :goto_3
    add-int/lit8 v11, v2, 0x1

    int-to-char v2, v11

    goto :goto_1

    .line 4149
    .end local v0    # "CE":I
    .end local v2    # "ch":C
    .end local v3    # "contractionOffset":C
    .end local v6    # "it":Lcom/ibm/icu/text/CollationElementIterator;
    .end local v9    # "s":Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;
    :cond_4
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    const/4 v12, 0x0

    invoke-static {v11, v12}, Ljava/util/Arrays;->fill([II)V

    goto :goto_0

    .line 4168
    .restart local v0    # "CE":I
    .restart local v2    # "ch":C
    .restart local v3    # "contractionOffset":C
    .restart local v6    # "it":Lcom/ibm/icu/text/CollationElementIterator;
    .restart local v9    # "s":Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;
    :cond_5
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v11, v2}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v0

    .line 4169
    const/high16 v11, -0x10000000

    if-ne v0, v11, :cond_2

    .line 4170
    sget-object v11, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v11, v11, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v11, v2}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v0

    goto :goto_2

    .line 4176
    :cond_6
    invoke-static {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v11

    sparse-switch v11, :sswitch_data_0

    .line 4279
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneFailed_:Z

    .line 4280
    const/4 v11, 0x0

    .line 4288
    :goto_4
    return v11

    .line 4182
    :sswitch_0
    invoke-static {v2}, Lcom/ibm/icu/lang/UCharacter;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 4183
    :goto_5
    invoke-virtual {v6}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v0

    const/4 v11, -0x1

    if-eq v0, v11, :cond_3

    .line 4184
    iget v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->primShift:I

    if-ltz v11, :cond_7

    iget v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->secShift:I

    if-ltz v11, :cond_7

    iget v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->terShift:I

    if-gez v11, :cond_8

    .line 4185
    :cond_7
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    const/high16 v12, -0x1000000

    aput v12, v11, v2

    .line 4186
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v12, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    add-int/2addr v12, v2

    const/high16 v13, -0x1000000

    aput v13, v11, v12

    .line 4187
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v12, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    mul-int/lit8 v12, v12, 0x2

    add-int/2addr v12, v2

    const/high16 v13, -0x1000000

    aput v13, v11, v12

    goto :goto_3

    .line 4190
    :cond_8
    invoke-direct {p0, v2, v0, v9}, Lcom/ibm/icu/text/RuleBasedCollator;->addLatinOneEntry(CILcom/ibm/icu/text/RuleBasedCollator$shiftValues;)V

    goto :goto_5

    .line 4200
    :sswitch_1
    const v11, 0xfff000

    and-int/2addr v11, v0

    if-eqz v11, :cond_9

    .line 4201
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneFailed_:Z

    .line 4202
    const/4 v11, 0x0

    goto :goto_4

    .line 4205
    :cond_9
    const v11, 0xffffff

    and-int/2addr v11, v0

    iget v12, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    sub-int v1, v11, v12

    .line 4207
    .local v1, "UCharOffset":I
    and-int/lit16 v11, v3, 0xfff

    shl-int/lit8 v11, v11, 0xc

    or-int/2addr v0, v11

    .line 4209
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    aput v0, v11, v2

    .line 4210
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v12, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    add-int/2addr v12, v2

    aput v0, v11, v12

    .line 4211
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v12, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    mul-int/lit8 v12, v12, 0x2

    add-int/2addr v12, v2

    aput v0, v11, v12

    .line 4217
    :cond_a
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget v0, v11, v1

    .line 4218
    invoke-static {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v11

    if-eqz v11, :cond_12

    invoke-static {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_12

    .line 4223
    const v11, 0xfffff0

    and-int/2addr v11, v0

    shr-int/lit8 v11, v11, 0x4

    iget v12, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionOffset_:I

    sub-int v7, v11, v12

    .line 4224
    .local v7, "offset":I
    and-int/lit8 v10, v0, 0xf

    .line 4226
    .local v10, "size":I
    if-eqz v10, :cond_10

    .line 4227
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_6
    if-ge v5, v10, :cond_c

    .line 4228
    iget v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->primShift:I

    if-ltz v11, :cond_b

    iget v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->secShift:I

    if-ltz v11, :cond_b

    iget v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->terShift:I

    if-gez v11, :cond_e

    .line 4229
    :cond_b
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    const/high16 v12, -0x1000000

    aput v12, v11, v3

    .line 4230
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v12, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    add-int/2addr v12, v3

    const/high16 v13, -0x1000000

    aput v13, v11, v12

    .line 4231
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v12, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    mul-int/lit8 v12, v12, 0x2

    add-int/2addr v12, v3

    const/high16 v13, -0x1000000

    aput v13, v11, v12

    .line 4247
    .end local v5    # "i":I
    :cond_c
    :goto_7
    add-int/lit8 v11, v3, 0x1

    int-to-char v3, v11

    .line 4256
    .end local v7    # "offset":I
    .end local v10    # "size":I
    :goto_8
    add-int/lit8 v1, v1, 0x1

    .line 4257
    const/16 v11, 0x18

    iput v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->primShift:I

    const/16 v11, 0x18

    iput v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->secShift:I

    const/16 v11, 0x18

    iput v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->terShift:I

    .line 4258
    iget v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    if-ne v3, v11, :cond_d

    .line 4259
    iget v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    mul-int/lit8 v11, v11, 0x2

    invoke-direct {p0, v11}, Lcom/ibm/icu/text/RuleBasedCollator;->resizeLatinOneTable(I)V

    .line 4261
    :cond_d
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v11, v11, v1

    const v12, 0xffff

    if-ne v11, v12, :cond_a

    goto/16 :goto_3

    .line 4234
    .restart local v5    # "i":I
    .restart local v7    # "offset":I
    .restart local v10    # "size":I
    :cond_e
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    add-int v12, v7, v5

    aget v11, v11, v12

    invoke-direct {p0, v3, v11, v9}, Lcom/ibm/icu/text/RuleBasedCollator;->addLatinOneEntry(CILcom/ibm/icu/text/RuleBasedCollator$shiftValues;)V

    .line 4227
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 4244
    .end local v5    # "i":I
    :cond_f
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "offset":I
    .local v8, "offset":I
    aget v11, v11, v7

    invoke-direct {p0, v3, v11, v9}, Lcom/ibm/icu/text/RuleBasedCollator;->addLatinOneEntry(CILcom/ibm/icu/text/RuleBasedCollator$shiftValues;)V

    move v7, v8

    .line 4237
    .end local v8    # "offset":I
    .restart local v7    # "offset":I
    :cond_10
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    aget v11, v11, v7

    if-eqz v11, :cond_c

    .line 4238
    iget v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->primShift:I

    if-ltz v11, :cond_11

    iget v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->secShift:I

    if-ltz v11, :cond_11

    iget v11, v9, Lcom/ibm/icu/text/RuleBasedCollator$shiftValues;->terShift:I

    if-gez v11, :cond_f

    .line 4239
    :cond_11
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    const/high16 v12, -0x1000000

    aput v12, v11, v3

    .line 4240
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v12, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    add-int/2addr v12, v3

    const/high16 v13, -0x1000000

    aput v13, v11, v12

    .line 4241
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v12, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    mul-int/lit8 v12, v12, 0x2

    add-int/2addr v12, v3

    const/high16 v13, -0x1000000

    aput v13, v11, v12

    goto :goto_7

    .line 4248
    .end local v7    # "offset":I
    .end local v10    # "size":I
    :cond_12
    invoke-static {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v11

    if-nez v11, :cond_13

    .line 4249
    add-int/lit8 v11, v3, 0x1

    int-to-char v4, v11

    .end local v3    # "contractionOffset":C
    .local v4, "contractionOffset":C
    invoke-direct {p0, v3, v0, v9}, Lcom/ibm/icu/text/RuleBasedCollator;->addLatinOneEntry(CILcom/ibm/icu/text/RuleBasedCollator$shiftValues;)V

    move v3, v4

    .line 4250
    .end local v4    # "contractionOffset":C
    .restart local v3    # "contractionOffset":C
    goto :goto_8

    .line 4251
    :cond_13
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    const/high16 v12, -0x1000000

    aput v12, v11, v3

    .line 4252
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v12, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    add-int/2addr v12, v3

    const/high16 v13, -0x1000000

    aput v13, v11, v12

    .line 4253
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    iget v12, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    mul-int/lit8 v12, v12, 0x2

    add-int/2addr v12, v3

    const/high16 v13, -0x1000000

    aput v13, v11, v12

    .line 4254
    add-int/lit8 v11, v3, 0x1

    int-to-char v3, v11

    goto/16 :goto_8

    .line 4269
    .end local v1    # "UCharOffset":I
    :sswitch_2
    const/16 v11, 0xb7

    if-ne v2, v11, :cond_14

    .line 4270
    invoke-direct {p0, v2, v0, v9}, Lcom/ibm/icu/text/RuleBasedCollator;->addLatinOneEntry(CILcom/ibm/icu/text/RuleBasedCollator$shiftValues;)V

    goto/16 :goto_3

    .line 4273
    :cond_14
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneFailed_:Z

    .line 4274
    const/4 v11, 0x0

    goto/16 :goto_4

    .line 4285
    :cond_15
    iget v11, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneTableLen_:I

    if-ge v3, v11, :cond_16

    .line 4286
    invoke-direct {p0, v3}, Lcom/ibm/icu/text/RuleBasedCollator;->resizeLatinOneTable(I)V

    .line 4288
    :cond_16
    const/4 v11, 0x1

    goto/16 :goto_4

    .line 4176
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0xb -> :sswitch_2
        0xd -> :sswitch_0
    .end sparse-switch
.end method

.method private updateInternalState()V
    .locals 9

    .prologue
    const/16 v4, 0x19

    const/4 v8, 0x2

    const/4 v3, 0x5

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 3941
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    if-ne v1, v4, :cond_2

    .line 3942
    const/16 v1, 0xc0

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseSwitch_:I

    .line 3948
    :goto_0
    iget-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isCaseLevel_:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    const/16 v2, 0x10

    if-ne v1, v2, :cond_3

    .line 3949
    :cond_0
    const/16 v1, 0x3f

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_mask3_:I

    .line 3950
    iput v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_common3_:I

    .line 3951
    const/16 v1, 0x80

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_addition3_:I

    .line 3952
    const/16 v1, 0x85

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_top3_:I

    .line 3953
    iput v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottom3_:I

    .line 3970
    :goto_1
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_top3_:I

    add-int/lit8 v1, v1, -0x5

    add-int/lit8 v0, v1, -0x1

    .line 3972
    .local v0, "total3":I
    const-wide v2, 0x3fe55810624dd2f2L    # 0.667

    int-to-double v4, v0

    mul-double/2addr v2, v4

    double-to-int v1, v2

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_topCount3_:I

    .line 3973
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_topCount3_:I

    sub-int v1, v0, v1

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottomCount3_:I

    .line 3975
    iget-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isCaseLevel_:Z

    if-nez v1, :cond_5

    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v1

    if-ne v1, v8, :cond_5

    iget-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    if-nez v1, :cond_5

    iget-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isAlternateHandlingShifted_:Z

    if-nez v1, :cond_5

    .line 3977
    iput-boolean v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isSimple3_:Z

    .line 3982
    :goto_2
    iget-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isCaseLevel_:Z

    if-nez v1, :cond_8

    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v1

    if-gt v1, v8, :cond_8

    iget-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isNumericCollation_:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isAlternateHandlingShifted_:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneFailed_:Z

    if-nez v1, :cond_8

    .line 3984
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneRegenTable_:Z

    if-eqz v1, :cond_7

    .line 3985
    :cond_1
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->setUpLatinOne()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3986
    iput-boolean v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneUse_:Z

    .line 3991
    :goto_3
    iput-boolean v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneRegenTable_:Z

    .line 3999
    :goto_4
    return-void

    .line 3945
    .end local v0    # "total3":I
    :cond_2
    iput v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseSwitch_:I

    goto :goto_0

    .line 3956
    :cond_3
    const/16 v1, 0xff

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_mask3_:I

    .line 3957
    const/16 v1, 0x40

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_addition3_:I

    .line 3958
    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    if-ne v1, v4, :cond_4

    .line 3959
    const/16 v1, 0xc5

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_common3_:I

    .line 3960
    const/16 v1, 0xc5

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_top3_:I

    .line 3961
    const/16 v1, 0x86

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottom3_:I

    goto :goto_1

    .line 3963
    :cond_4
    iput v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_common3_:I

    .line 3964
    const/16 v1, 0x45

    iput v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_top3_:I

    .line 3965
    iput v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottom3_:I

    goto :goto_1

    .line 3980
    .restart local v0    # "total3":I
    :cond_5
    iput-boolean v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isSimple3_:Z

    goto :goto_2

    .line 3988
    :cond_6
    iput-boolean v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneUse_:Z

    .line 3989
    iput-boolean v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneFailed_:Z

    goto :goto_3

    .line 3993
    :cond_7
    iput-boolean v7, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneUse_:Z

    goto :goto_4

    .line 3996
    :cond_8
    iput-boolean v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneUse_:Z

    goto :goto_4
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 233
    invoke-super {p0}, Lcom/ibm/icu/text/Collator;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/RuleBasedCollator;

    .line 234
    .local v0, "result":Lcom/ibm/icu/text/RuleBasedCollator;
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneCEs_:[I

    if-eqz v1, :cond_0

    .line 235
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_reallocLatinOneCEs_:Z

    .line 236
    new-instance v1, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;-><init>(Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/RuleBasedCollator$1;)V

    iput-object v1, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_ContInfo_:Lcom/ibm/icu/text/RuleBasedCollator$ContractionInfo;

    .line 241
    :cond_0
    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->initUtility(Z)V

    .line 243
    return-object v0
.end method

.method public compare(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0xff

    .line 1274
    if-ne p1, p2, :cond_0

    .line 1275
    const/4 v1, 0x0

    .line 1292
    :goto_0
    return v1

    .line 1279
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/RuleBasedCollator;->getFirstUnmatchedOffset(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1281
    .local v0, "offset":I
    iget-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneUse_:Z

    if-eqz v1, :cond_4

    .line 1282
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-gt v1, v2, :cond_2

    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-le v1, v2, :cond_3

    .line 1287
    :cond_2
    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->compareRegular(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    goto :goto_0

    .line 1289
    :cond_3
    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->compareUseLatin1(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    goto :goto_0

    .line 1292
    :cond_4
    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->compareRegular(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const v11, 0xffffff

    const/high16 v10, -0x1000000

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1138
    if-nez p1, :cond_1

    move v4, v6

    .line 1222
    :cond_0
    :goto_0
    return v4

    .line 1141
    :cond_1
    if-ne p0, p1, :cond_2

    move v4, v7

    .line 1142
    goto :goto_0

    .line 1144
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    if-eq v8, v9, :cond_3

    move v4, v6

    .line 1145
    goto :goto_0

    :cond_3
    move-object v2, p1

    .line 1147
    check-cast v2, Lcom/ibm/icu/text/RuleBasedCollator;

    .line 1149
    .local v2, "other":Lcom/ibm/icu/text/RuleBasedCollator;
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v8

    invoke-virtual {v2}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v9

    if-ne v8, v9, :cond_4

    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->getDecomposition()I

    move-result v8

    invoke-virtual {v2}, Lcom/ibm/icu/text/RuleBasedCollator;->getDecomposition()I

    move-result v9

    if-ne v8, v9, :cond_4

    iget v8, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    iget v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    if-ne v8, v9, :cond_4

    iget v8, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseSwitch_:I

    iget v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseSwitch_:I

    if-ne v8, v9, :cond_4

    iget-boolean v8, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_isAlternateHandlingShifted_:Z

    iget-boolean v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isAlternateHandlingShifted_:Z

    if-ne v8, v9, :cond_4

    iget-boolean v8, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_isCaseLevel_:Z

    iget-boolean v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isCaseLevel_:Z

    if-ne v8, v9, :cond_4

    iget-boolean v8, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    iget-boolean v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    if-ne v8, v9, :cond_4

    iget-boolean v8, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_isHiragana4_:Z

    iget-boolean v9, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isHiragana4_:Z

    if-eq v8, v9, :cond_5

    :cond_4
    move v4, v6

    .line 1158
    goto :goto_0

    .line 1160
    :cond_5
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    iget-object v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    if-ne v8, v9, :cond_8

    move v4, v7

    .line 1161
    .local v4, "rules":Z
    :goto_1
    if-nez v4, :cond_6

    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    if-eqz v8, :cond_6

    iget-object v8, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    if-eqz v8, :cond_6

    .line 1162
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    iget-object v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 1164
    :cond_6
    if-eqz v4, :cond_0

    const-string/jumbo v8, "collation"

    invoke-static {v8}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1167
    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_addition3_:I

    iget v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_addition3_:I

    if-ne v8, v9, :cond_7

    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottom3_:I

    iget v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottom3_:I

    if-ne v8, v9, :cond_7

    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottomCount3_:I

    iget v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottomCount3_:I

    if-ne v8, v9, :cond_7

    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_common3_:I

    iget v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_common3_:I

    if-ne v8, v9, :cond_7

    iget-boolean v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isSimple3_:Z

    iget-boolean v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_isSimple3_:Z

    if-ne v8, v9, :cond_7

    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_mask3_:I

    iget v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_mask3_:I

    if-ne v8, v9, :cond_7

    iget-char v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minContractionEnd_:C

    iget-char v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_minContractionEnd_:C

    if-ne v8, v9, :cond_7

    iget-char v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minUnsafe_:C

    iget-char v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_minUnsafe_:C

    if-ne v8, v9, :cond_7

    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_top3_:I

    iget v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_top3_:I

    if-ne v8, v9, :cond_7

    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_topCount3_:I

    iget v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_topCount3_:I

    if-ne v8, v9, :cond_7

    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_unsafe_:[B

    iget-object v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_unsafe_:[B

    invoke-static {v8, v9}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v8

    if-nez v8, :cond_9

    :cond_7
    move v4, v6

    .line 1178
    goto/16 :goto_0

    .end local v4    # "rules":Z
    :cond_8
    move v4, v6

    .line 1160
    goto :goto_1

    .line 1180
    .restart local v4    # "rules":Z
    :cond_9
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    iget-object v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v8, v9}, Lcom/ibm/icu/impl/IntTrie;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_e

    .line 1183
    const v0, 0x10ffff

    .local v0, "i":I
    :goto_2
    if-ltz v0, :cond_e

    .line 1185
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v8, v0}, Lcom/ibm/icu/impl/IntTrie;->getCodePointValue(I)I

    move-result v5

    .line 1186
    .local v5, "v":I
    iget-object v8, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v8, v0}, Lcom/ibm/icu/impl/IntTrie;->getCodePointValue(I)I

    move-result v3

    .line 1187
    .local v3, "otherv":I
    if-eq v5, v3, :cond_b

    .line 1188
    and-int v1, v5, v10

    .line 1189
    .local v1, "mask":I
    and-int v8, v3, v10

    if-ne v1, v8, :cond_d

    .line 1190
    and-int/2addr v5, v11

    .line 1191
    and-int/2addr v3, v11

    .line 1192
    const/high16 v8, -0xf000000

    if-ne v1, v8, :cond_c

    .line 1193
    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionOffset_:I

    shl-int/lit8 v8, v8, 0x4

    sub-int/2addr v5, v8

    .line 1194
    iget v8, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionOffset_:I

    shl-int/lit8 v8, v8, 0x4

    sub-int/2addr v3, v8

    .line 1200
    :cond_a
    :goto_3
    if-ne v5, v3, :cond_d

    .line 1183
    .end local v1    # "mask":I
    :cond_b
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 1196
    .restart local v1    # "mask":I
    :cond_c
    const/high16 v8, -0xe000000

    if-ne v1, v8, :cond_a

    .line 1197
    iget v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    sub-int/2addr v5, v8

    .line 1198
    iget v8, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    sub-int/2addr v3, v8

    goto :goto_3

    :cond_d
    move v4, v6

    .line 1204
    goto/16 :goto_0

    .line 1208
    .end local v0    # "i":I
    .end local v1    # "mask":I
    .end local v3    # "otherv":I
    .end local v5    # "v":I
    :cond_e
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    iget-object v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    invoke-static {v8, v9}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v8

    if-eqz v8, :cond_10

    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionEnd_:[B

    iget-object v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionEnd_:[B

    invoke-static {v8, v9}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v8

    if-eqz v8, :cond_10

    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    iget-object v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    invoke-static {v8, v9}, Ljava/util/Arrays;->equals([C[C)Z

    move-result v8

    if-eqz v8, :cond_10

    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    iget-object v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    invoke-static {v8, v9}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v8

    if-eqz v8, :cond_10

    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    iget-object v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    invoke-static {v8, v9}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 1214
    const/4 v0, 0x0

    .restart local v0    # "i":I
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    array-length v8, v8

    if-ge v0, v8, :cond_10

    .line 1215
    iget-object v8, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCEMaxSize_:[B

    aget-byte v8, v8, v0

    iget-object v9, v2, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCEMaxSize_:[B

    aget-byte v9, v9, v0

    if-eq v8, v9, :cond_f

    move v4, v6

    .line 1217
    goto/16 :goto_0

    :cond_f
    move v4, v7

    .line 1219
    goto/16 :goto_0

    .end local v0    # "i":I
    :cond_10
    move v4, v6

    .line 1222
    goto/16 :goto_0
.end method

.method public getCollationElementIterator(Lcom/ibm/icu/text/UCharacterIterator;)Lcom/ibm/icu/text/CollationElementIterator;
    .locals 1
    .param p1, "source"    # Lcom/ibm/icu/text/UCharacterIterator;

    .prologue
    .line 280
    new-instance v0, Lcom/ibm/icu/text/CollationElementIterator;

    invoke-direct {v0, p1, p0}, Lcom/ibm/icu/text/CollationElementIterator;-><init>(Lcom/ibm/icu/text/UCharacterIterator;Lcom/ibm/icu/text/RuleBasedCollator;)V

    return-object v0
.end method

.method public getCollationElementIterator(Ljava/lang/String;)Lcom/ibm/icu/text/CollationElementIterator;
    .locals 1
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 253
    new-instance v0, Lcom/ibm/icu/text/CollationElementIterator;

    invoke-direct {v0, p1, p0}, Lcom/ibm/icu/text/CollationElementIterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/RuleBasedCollator;)V

    return-object v0
.end method

.method public getCollationElementIterator(Ljava/text/CharacterIterator;)Lcom/ibm/icu/text/CollationElementIterator;
    .locals 2
    .param p1, "source"    # Ljava/text/CharacterIterator;

    .prologue
    .line 266
    invoke-interface {p1}, Ljava/text/CharacterIterator;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/CharacterIterator;

    .line 267
    .local v0, "newsource":Ljava/text/CharacterIterator;
    new-instance v1, Lcom/ibm/icu/text/CollationElementIterator;

    invoke-direct {v1, v0, p0}, Lcom/ibm/icu/text/CollationElementIterator;-><init>(Ljava/text/CharacterIterator;Lcom/ibm/icu/text/RuleBasedCollator;)V

    return-object v1
.end method

.method public getCollationKey(Ljava/lang/String;)Lcom/ibm/icu/text/CollationKey;
    .locals 2
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 921
    if-nez p1, :cond_0

    .line 922
    const/4 v0, 0x0

    .line 926
    :goto_0
    return-object v0

    .line 924
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilRawCollationKey_:Lcom/ibm/icu/text/RawCollationKey;

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getRawCollationKey(Ljava/lang/String;Lcom/ibm/icu/text/RawCollationKey;)Lcom/ibm/icu/text/RawCollationKey;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilRawCollationKey_:Lcom/ibm/icu/text/RawCollationKey;

    .line 926
    new-instance v0, Lcom/ibm/icu/text/CollationKey;

    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilRawCollationKey_:Lcom/ibm/icu/text/RawCollationKey;

    invoke-direct {v0, p1, v1}, Lcom/ibm/icu/text/CollationKey;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/RawCollationKey;)V

    goto :goto_0
.end method

.method public getContractionsAndExpansions(Lcom/ibm/icu/text/UnicodeSet;Lcom/ibm/icu/text/UnicodeSet;Z)V
    .locals 10
    .param p1, "contractions"    # Lcom/ibm/icu/text/UnicodeSet;
    .param p2, "expansions"    # Lcom/ibm/icu/text/UnicodeSet;
    .param p3, "addPrefixes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 872
    if-eqz p1, :cond_0

    .line 873
    invoke-virtual {p1}, Lcom/ibm/icu/text/UnicodeSet;->clear()Lcom/ibm/icu/text/UnicodeSet;

    .line 875
    :cond_0
    if-eqz p2, :cond_1

    .line 876
    invoke-virtual {p2}, Lcom/ibm/icu/text/UnicodeSet;->clear()Lcom/ibm/icu/text/UnicodeSet;

    .line 878
    :cond_1
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->getRules()Ljava/lang/String;

    move-result-object v8

    .line 880
    .local v8, "rules":Ljava/lang/String;
    :try_start_0
    new-instance v9, Lcom/ibm/icu/text/CollationRuleParser;

    invoke-direct {v9, v8}, Lcom/ibm/icu/text/CollationRuleParser;-><init>(Ljava/lang/String;)V

    .line 881
    .local v9, "src":Lcom/ibm/icu/text/CollationRuleParser;
    new-instance v0, Lcom/ibm/icu/text/RuleBasedCollator$contContext;

    sget-object v2, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v5, v9, Lcom/ibm/icu/text/CollationRuleParser;->m_removeSet_:Lcom/ibm/icu/text/UnicodeSet;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/RuleBasedCollator$contContext;-><init>(Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/RuleBasedCollator;Lcom/ibm/icu/text/UnicodeSet;Lcom/ibm/icu/text/UnicodeSet;Lcom/ibm/icu/text/UnicodeSet;Z)V

    .line 885
    .local v0, "c":Lcom/ibm/icu/text/RuleBasedCollator$contContext;
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->processSpecials(Lcom/ibm/icu/text/RuleBasedCollator$contContext;)V

    .line 887
    iput-object p0, v0, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->coll:Lcom/ibm/icu/text/RuleBasedCollator;

    .line 888
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/ibm/icu/text/RuleBasedCollator$contContext;->removedContractions:Lcom/ibm/icu/text/UnicodeSet;

    .line 889
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->processSpecials(Lcom/ibm/icu/text/RuleBasedCollator$contContext;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 893
    return-void

    .line 890
    .end local v0    # "c":Lcom/ibm/icu/text/RuleBasedCollator$contContext;
    .end local v9    # "src":Lcom/ibm/icu/text/CollationRuleParser;
    :catch_0
    move-exception v7

    .line 891
    .local v7, "e":Ljava/lang/Exception;
    throw v7
.end method

.method public getNumericCollation()Z
    .locals 1

    .prologue
    .line 1122
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isNumericCollation_:Z

    return v0
.end method

.method public getRawCollationKey(Ljava/lang/String;Lcom/ibm/icu/text/RawCollationKey;)Lcom/ibm/icu/text/RawCollationKey;
    .locals 13
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "key"    # Lcom/ibm/icu/text/RawCollationKey;

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 949
    if-nez p1, :cond_0

    .line 950
    const/4 v0, 0x0

    .line 1006
    :goto_0
    return-object v0

    .line 952
    :cond_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v12

    .line 953
    .local v12, "strength":I
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isCaseLevel_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare0_:Z

    .line 955
    if-lt v12, v1, :cond_4

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare2_:Z

    .line 956
    const/4 v0, 0x2

    if-lt v12, v0, :cond_5

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare3_:Z

    .line 957
    const/4 v0, 0x3

    if-lt v12, v0, :cond_6

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare4_:Z

    .line 958
    const/16 v0, 0xf

    if-ne v12, v0, :cond_7

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare5_:Z

    .line 960
    iput v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount0_:I

    .line 961
    iput v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount1_:I

    .line 962
    iput v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount2_:I

    .line 963
    iput v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount3_:I

    .line 964
    iput v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilBytesCount4_:I

    .line 968
    iput v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount2_:I

    .line 969
    iput v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount3_:I

    .line 970
    iput v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCount4_:I

    .line 972
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare2_:Z

    if-eqz v0, :cond_8

    move v2, v1

    .line 976
    .local v2, "doFrench":Z
    :goto_5
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_variableTopValue_:I

    ushr-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x1

    and-int/lit16 v4, v0, 0xff

    .line 977
    .local v4, "commonBottom4":I
    const/4 v3, 0x0

    .line 978
    .local v3, "hiragana4":B
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isHiragana4_:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare4_:Z

    if-eqz v0, :cond_1

    .line 980
    int-to-byte v3, v4

    .line 981
    add-int/lit8 v4, v4, 0x1

    .line 984
    :cond_1
    rsub-int v5, v4, 0xff

    .line 986
    .local v5, "bottomCount4":I
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_utilCompare5_:Z

    if-eqz v0, :cond_9

    sget-object v0, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {p1, v0, v6}, Lcom/ibm/icu/text/Normalizer;->quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v0

    sget-object v1, Lcom/ibm/icu/text/Normalizer;->YES:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-eq v0, v1, :cond_9

    .line 991
    invoke-static {p1, v6}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    :cond_2
    :goto_6
    move-object v0, p0

    move-object v1, p1

    .line 1000
    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/RuleBasedCollator;->getSortKeyBytes(Ljava/lang/String;ZBII)V

    .line 1002
    if-nez p2, :cond_3

    .line 1003
    new-instance p2, Lcom/ibm/icu/text/RawCollationKey;

    .end local p2    # "key":Lcom/ibm/icu/text/RawCollationKey;
    invoke-direct {p2}, Lcom/ibm/icu/text/RawCollationKey;-><init>()V

    .restart local p2    # "key":Lcom/ibm/icu/text/RawCollationKey;
    :cond_3
    move-object v6, p0

    move-object v7, p1

    move v8, v2

    move v9, v4

    move v10, v5

    move-object v11, p2

    .line 1005
    invoke-direct/range {v6 .. v11}, Lcom/ibm/icu/text/RuleBasedCollator;->getSortKey(Ljava/lang/String;ZIILcom/ibm/icu/text/RawCollationKey;)V

    move-object v0, p2

    .line 1006
    goto :goto_0

    .end local v2    # "doFrench":Z
    .end local v3    # "hiragana4":B
    .end local v4    # "commonBottom4":I
    .end local v5    # "bottomCount4":I
    :cond_4
    move v0, v6

    .line 955
    goto :goto_1

    :cond_5
    move v0, v6

    .line 956
    goto :goto_2

    :cond_6
    move v0, v6

    .line 957
    goto :goto_3

    :cond_7
    move v0, v6

    .line 958
    goto :goto_4

    :cond_8
    move v2, v6

    .line 972
    goto :goto_5

    .line 993
    .restart local v2    # "doFrench":Z
    .restart local v3    # "hiragana4":B
    .restart local v4    # "commonBottom4":I
    .restart local v5    # "bottomCount4":I
    :cond_9
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->getDecomposition()I

    move-result v0

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    sget-object v0, Lcom/ibm/icu/text/Normalizer;->FCD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {p1, v0, v6}, Lcom/ibm/icu/text/Normalizer;->quickCheck(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v0

    sget-object v1, Lcom/ibm/icu/text/Normalizer;->YES:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    if-eq v0, v1, :cond_2

    .line 998
    sget-object v0, Lcom/ibm/icu/text/Normalizer;->FCD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {p1, v0}, Lcom/ibm/icu/text/Normalizer;->normalize(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;)Ljava/lang/String;

    move-result-object p1

    goto :goto_6
.end method

.method public getRules()Ljava/lang/String;
    .locals 1

    .prologue
    .line 701
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    return-object v0
.end method

.method public getRules(Z)Ljava/lang/String;
    .locals 2
    .param p1, "fullrules"    # Z

    .prologue
    .line 716
    if-nez p1, :cond_0

    .line 717
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    .line 720
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTailoredSet()Lcom/ibm/icu/text/UnicodeSet;
    .locals 4

    .prologue
    .line 736
    :try_start_0
    new-instance v1, Lcom/ibm/icu/text/CollationRuleParser;

    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->getRules()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/ibm/icu/text/CollationRuleParser;-><init>(Ljava/lang/String;)V

    .line 737
    .local v1, "src":Lcom/ibm/icu/text/CollationRuleParser;
    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationRuleParser;->getTailoredSet()Lcom/ibm/icu/text/UnicodeSet;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 738
    .end local v1    # "src":Lcom/ibm/icu/text/CollationRuleParser;
    :catch_0
    move-exception v0

    .line 739
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string/jumbo v3, "A tailoring rule should not have errors. Something is quite wrong!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getUCAVersion()Lcom/ibm/icu/util/VersionInfo;
    .locals 1

    .prologue
    .line 4685
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_UCA_version_:Lcom/ibm/icu/util/VersionInfo;

    return-object v0
.end method

.method public getVariableTop()I
    .locals 1

    .prologue
    .line 1107
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_variableTopValue_:I

    shl-int/lit8 v0, v0, 0x10

    return v0
.end method

.method public getVersion()Lcom/ibm/icu/util/VersionInfo;
    .locals 8

    .prologue
    .line 4654
    sget-object v4, Lcom/ibm/icu/util/VersionInfo;->UCOL_RUNTIME_VERSION:Lcom/ibm/icu/util/VersionInfo;

    invoke-virtual {v4}, Lcom/ibm/icu/util/VersionInfo;->getMajor()I

    move-result v3

    .line 4656
    .local v3, "rtVersion":I
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_version_:Lcom/ibm/icu/util/VersionInfo;

    invoke-virtual {v4}, Lcom/ibm/icu/util/VersionInfo;->getMajor()I

    move-result v0

    .line 4662
    .local v0, "bdVersion":I
    const/4 v2, 0x0

    .line 4665
    .local v2, "csVersion":I
    shl-int/lit8 v4, v3, 0xb

    shl-int/lit8 v5, v0, 0x6

    or-int/2addr v4, v5

    or-int/2addr v4, v2

    const v5, 0xffff

    and-int v1, v4, v5

    .line 4668
    .local v1, "cmbVersion":I
    shr-int/lit8 v4, v1, 0x8

    and-int/lit16 v5, v1, 0xff

    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_version_:Lcom/ibm/icu/util/VersionInfo;

    invoke-virtual {v6}, Lcom/ibm/icu/util/VersionInfo;->getMinor()I

    move-result v6

    sget-object v7, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v7, v7, Lcom/ibm/icu/text/RuleBasedCollator;->m_UCA_version_:Lcom/ibm/icu/util/VersionInfo;

    invoke-virtual {v7}, Lcom/ibm/icu/util/VersionInfo;->getMajor()I

    move-result v7

    invoke-static {v4, v5, v6, v7}, Lcom/ibm/icu/util/VersionInfo;->getInstance(IIII)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v4

    return-object v4
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 1232
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->getRules()Ljava/lang/String;

    move-result-object v0

    .line 1233
    .local v0, "rules":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1234
    const-string/jumbo v0, ""

    .line 1236
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    return v1
.end method

.method public isAlternateHandlingShifted()Z
    .locals 1

    .prologue
    .line 1055
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isAlternateHandlingShifted_:Z

    return v0
.end method

.method public isCaseLevel()Z
    .locals 1

    .prologue
    .line 1069
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isCaseLevel_:Z

    return v0
.end method

.method final isContractionEnd(C)Z
    .locals 5
    .param p1, "ch"    # C

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1990
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2003
    :cond_0
    :goto_0
    return v1

    .line 1994
    :cond_1
    iget-char v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minContractionEnd_:C

    if-ge p1, v3, :cond_2

    move v1, v2

    .line 1995
    goto :goto_0

    .line 1998
    :cond_2
    const/16 v3, 0x2100

    if-lt p1, v3, :cond_3

    .line 1999
    and-int/lit16 v3, p1, 0x1fff

    int-to-char p1, v3

    .line 2000
    add-int/lit16 v3, p1, 0x100

    int-to-char p1, v3

    .line 2002
    :cond_3
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionEnd_:[B

    shr-int/lit8 v4, p1, 0x3

    aget-byte v0, v3, v4

    .line 2003
    .local v0, "value":I
    and-int/lit8 v3, p1, 0x7

    shr-int v3, v0, v3

    and-int/lit8 v3, v3, 0x1

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public isFrenchCollation()Z
    .locals 1

    .prologue
    .line 1082
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    return v0
.end method

.method public isHiraganaQuaternary()Z
    .locals 1

    .prologue
    .line 1095
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isHiragana4_:Z

    return v0
.end method

.method public isLowerCaseFirst()Z
    .locals 2

    .prologue
    .line 1038
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    const/16 v1, 0x18

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final isUnsafe(C)Z
    .locals 5
    .param p1, "ch"    # C

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1965
    iget-char v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minUnsafe_:C

    if-ge p1, v3, :cond_1

    move v1, v2

    .line 1979
    :cond_0
    :goto_0
    return v1

    .line 1969
    :cond_1
    const/16 v3, 0x2100

    if-lt p1, v3, :cond_2

    .line 1970
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1975
    and-int/lit16 v3, p1, 0x1fff

    int-to-char p1, v3

    .line 1976
    add-int/lit16 v3, p1, 0x100

    int-to-char p1, v3

    .line 1978
    :cond_2
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_unsafe_:[B

    shr-int/lit8 v4, p1, 0x3

    aget-byte v0, v3, v4

    .line 1979
    .local v0, "value":I
    and-int/lit8 v3, p1, 0x7

    shr-int v3, v0, v3

    and-int/lit8 v3, v3, 0x1

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public isUpperCaseFirst()Z
    .locals 2

    .prologue
    .line 1022
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    const/16 v1, 0x19

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAlternateHandlingDefault()V
    .locals 1

    .prologue
    .line 413
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsAlternateHandlingShifted_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isAlternateHandlingShifted_:Z

    .line 414
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 415
    return-void
.end method

.method public setAlternateHandlingShifted(Z)V
    .locals 0
    .param p1, "shifted"    # Z

    .prologue
    .line 537
    iput-boolean p1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isAlternateHandlingShifted_:Z

    .line 538
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 539
    return-void
.end method

.method public final setCaseFirstDefault()V
    .locals 2

    .prologue
    .line 396
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    iget v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultCaseFirst_:I

    if-eq v0, v1, :cond_0

    .line 397
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneRegenTable_:Z

    .line 399
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultCaseFirst_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    .line 400
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 401
    return-void
.end method

.method public setCaseLevel(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 565
    iput-boolean p1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isCaseLevel_:Z

    .line 566
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 567
    return-void
.end method

.method public setCaseLevelDefault()V
    .locals 1

    .prologue
    .line 427
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsCaseLevel_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isCaseLevel_:Z

    .line 428
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 429
    return-void
.end method

.method public setDecompositionDefault()V
    .locals 1

    .prologue
    .line 441
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultDecomposition_:I

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->setDecomposition(I)V

    .line 442
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 443
    return-void
.end method

.method public setFrenchCollation(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 507
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    if-eq v0, p1, :cond_0

    .line 508
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneRegenTable_:Z

    .line 510
    :cond_0
    iput-boolean p1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    .line 511
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 512
    return-void
.end method

.method public setFrenchCollationDefault()V
    .locals 2

    .prologue
    .line 455
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    iget-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsFrenchCollation_:Z

    if-eq v0, v1, :cond_0

    .line 456
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneRegenTable_:Z

    .line 458
    :cond_0
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsFrenchCollation_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    .line 459
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 460
    return-void
.end method

.method public setHiraganaQuaternary(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 299
    iput-boolean p1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isHiragana4_:Z

    .line 300
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 301
    return-void
.end method

.method public setHiraganaQuaternaryDefault()V
    .locals 1

    .prologue
    .line 313
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsHiragana4_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isHiragana4_:Z

    .line 314
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 315
    return-void
.end method

.method public setLowerCaseFirst(Z)V
    .locals 4
    .param p1, "lowerfirst"    # Z

    .prologue
    const/16 v3, 0x18

    const/16 v2, 0x10

    const/4 v1, 0x1

    .line 368
    if-eqz p1, :cond_1

    .line 369
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    if-eq v0, v3, :cond_0

    .line 370
    iput-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneRegenTable_:Z

    .line 372
    :cond_0
    iput v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    .line 380
    :goto_0
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 381
    return-void

    .line 375
    :cond_1
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    if-eq v0, v2, :cond_2

    .line 376
    iput-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneRegenTable_:Z

    .line 378
    :cond_2
    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    goto :goto_0
.end method

.method public setNumericCollation(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 686
    iput-boolean p1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isNumericCollation_:Z

    .line 687
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 688
    return-void
.end method

.method public setNumericCollationDefault()V
    .locals 1

    .prologue
    .line 487
    iget-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsNumericCollation_:Z

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->setNumericCollation(Z)V

    .line 488
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 489
    return-void
.end method

.method public setStrength(I)V
    .locals 0
    .param p1, "newStrength"    # I

    .prologue
    .line 590
    invoke-super {p0, p1}, Lcom/ibm/icu/text/Collator;->setStrength(I)V

    .line 591
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 592
    return-void
.end method

.method public setStrengthDefault()V
    .locals 1

    .prologue
    .line 472
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultStrength_:I

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->setStrength(I)V

    .line 473
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 474
    return-void
.end method

.method public setUpperCaseFirst(Z)V
    .locals 4
    .param p1, "upperfirst"    # Z

    .prologue
    const/16 v3, 0x19

    const/16 v2, 0x10

    const/4 v1, 0x1

    .line 334
    if-eqz p1, :cond_1

    .line 335
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    if-eq v0, v3, :cond_0

    .line 336
    iput-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneRegenTable_:Z

    .line 338
    :cond_0
    iput v3, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    .line 346
    :goto_0
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->updateInternalState()V

    .line 347
    return-void

    .line 341
    :cond_1
    iget v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    if-eq v0, v2, :cond_2

    .line 342
    iput-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneRegenTable_:Z

    .line 344
    :cond_2
    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    goto :goto_0
.end method

.method public setVariableTop(Ljava/lang/String;)I
    .locals 6
    .param p1, "varTop"    # Ljava/lang/String;

    .prologue
    const/4 v5, -0x1

    const/high16 v4, -0x10000

    .line 623
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 624
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Variable top argument string can not be null or zero in length."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 627
    :cond_1
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    if-nez v2, :cond_2

    .line 628
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->initUtility(Z)V

    .line 631
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v2, p1}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    .line 632
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v2}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v0

    .line 637
    .local v0, "ce":I
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v2}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v2, v3, :cond_3

    if-ne v0, v5, :cond_4

    .line 639
    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Variable top argument string is a contraction that does not exist in the Collation order"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 644
    :cond_4
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_srcUtilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v2}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v1

    .line 646
    .local v1, "nextCE":I
    if-eq v1, v5, :cond_6

    invoke-static {v1}, Lcom/ibm/icu/text/RuleBasedCollator;->isContinuation(I)Z

    move-result v2

    if-eqz v2, :cond_5

    and-int v2, v1, v4

    if-eqz v2, :cond_6

    .line 648
    :cond_5
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Variable top argument string can only have a single collation element that has less than or equal to two PRIMARY strength bytes"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 654
    :cond_6
    and-int v2, v0, v4

    shr-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_variableTopValue_:I

    .line 656
    and-int v2, v0, v4

    return v2
.end method

.method public setVariableTop(I)V
    .locals 1
    .param p1, "varTop"    # I

    .prologue
    .line 671
    const/high16 v0, -0x10000

    and-int/2addr v0, p1

    shr-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_variableTopValue_:I

    .line 672
    return-void
.end method

.method final setWithUCAData()V
    .locals 1

    .prologue
    .line 1913
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneFailed_:Z

    .line 1915
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_addition3_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_addition3_:I

    .line 1916
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottom3_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottom3_:I

    .line 1917
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottomCount3_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_bottomCount3_:I

    .line 1918
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    .line 1919
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseSwitch_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseSwitch_:I

    .line 1920
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_common3_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_common3_:I

    .line 1921
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    .line 1922
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getDecomposition()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->setDecomposition(I)V

    .line 1923
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultCaseFirst_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultCaseFirst_:I

    .line 1924
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultDecomposition_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultDecomposition_:I

    .line 1925
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsAlternateHandlingShifted_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsAlternateHandlingShifted_:Z

    .line 1927
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsCaseLevel_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsCaseLevel_:Z

    .line 1928
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsFrenchCollation_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsFrenchCollation_:Z

    .line 1929
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsHiragana4_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsHiragana4_:Z

    .line 1930
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultStrength_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultStrength_:I

    .line 1931
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultVariableTopValue_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultVariableTopValue_:I

    .line 1932
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsNumericCollation_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_defaultIsNumericCollation_:Z

    .line 1933
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionOffset_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionOffset_:I

    .line 1934
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isAlternateHandlingShifted_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isAlternateHandlingShifted_:Z

    .line 1935
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isCaseLevel_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isCaseLevel_:Z

    .line 1936
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isFrenchCollation_:Z

    .line 1937
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isHiragana4_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isHiragana4_:Z

    .line 1938
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isJamoSpecial_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isJamoSpecial_:Z

    .line 1939
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isSimple3_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isSimple3_:Z

    .line 1940
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_mask3_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_mask3_:I

    .line 1941
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-char v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minContractionEnd_:C

    iput-char v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minContractionEnd_:C

    .line 1942
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-char v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minUnsafe_:C

    iput-char v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minUnsafe_:C

    .line 1943
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_rules_:Ljava/lang/String;

    .line 1944
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->setStrength(I)V

    .line 1945
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_top3_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_top3_:I

    .line 1946
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_topCount3_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_topCount3_:I

    .line 1947
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_variableTopValue_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_variableTopValue_:I

    .line 1948
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isNumericCollation_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_isNumericCollation_:Z

    .line 1949
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedCollator;->setWithUCATables()V

    .line 1950
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->latinOneFailed_:Z

    .line 1951
    return-void
.end method

.method final setWithUCATables()V
    .locals 1

    .prologue
    .line 1894
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    .line 1895
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionOffset_:I

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionOffset_:I

    .line 1896
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    .line 1897
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    .line 1898
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    .line 1899
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    .line 1900
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    .line 1901
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCEMaxSize_:[B

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCEMaxSize_:[B

    .line 1902
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_unsafe_:[B

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_unsafe_:[B

    .line 1903
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionEnd_:[B

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionEnd_:[B

    .line 1904
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-char v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minUnsafe_:C

    iput-char v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minUnsafe_:C

    .line 1905
    sget-object v0, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-char v0, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minContractionEnd_:C

    iput-char v0, p0, Lcom/ibm/icu/text/RuleBasedCollator;->m_minContractionEnd_:C

    .line 1906
    return-void
.end method
