.class final Lcom/ibm/icu/text/BreakTransliterator;
.super Lcom/ibm/icu/text/Transliterator;
.source "BreakTransliterator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;
    }
.end annotation


# static fields
.field static final LETTER_OR_MARK_MASK:I = 0x1fe


# instance fields
.field private bi:Lcom/ibm/icu/text/BreakIterator;

.field private boundaries:[I

.field private boundaryCount:I

.field private insertion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V
    .locals 2
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "filter"    # Lcom/ibm/icu/text/UnicodeFilter;

    .prologue
    .line 32
    const/4 v0, 0x0

    const-string/jumbo v1, " "

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/ibm/icu/text/BreakTransliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;Lcom/ibm/icu/text/BreakIterator;Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;Lcom/ibm/icu/text/BreakIterator;Ljava/lang/String;)V
    .locals 1
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "filter"    # Lcom/ibm/icu/text/UnicodeFilter;
    .param p3, "bi"    # Lcom/ibm/icu/text/BreakIterator;
    .param p4, "insertion"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/Transliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 22
    const/16 v0, 0x32

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaries:[I

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaryCount:I

    .line 27
    iput-object p3, p0, Lcom/ibm/icu/text/BreakTransliterator;->bi:Lcom/ibm/icu/text/BreakIterator;

    .line 28
    iput-object p4, p0, Lcom/ibm/icu/text/BreakTransliterator;->insertion:Ljava/lang/String;

    .line 29
    return-void
.end method

.method static register()V
    .locals 3

    .prologue
    .line 132
    new-instance v0, Lcom/ibm/icu/text/BreakTransliterator;

    const-string/jumbo v1, "Any-BreakInternal"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/BreakTransliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 133
    .local v0, "trans":Lcom/ibm/icu/text/Transliterator;
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerInstance(Lcom/ibm/icu/text/Transliterator;Z)V

    .line 141
    return-void
.end method


# virtual methods
.method public getBreakIterator()Lcom/ibm/icu/text/BreakIterator;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/ibm/icu/text/BreakTransliterator;->bi:Lcom/ibm/icu/text/BreakIterator;

    if-nez v0, :cond_0

    new-instance v0, Lcom/ibm/icu/util/ULocale;

    const-string/jumbo v1, "th_TH"

    invoke-direct {v0, v1}, Lcom/ibm/icu/util/ULocale;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/ibm/icu/text/BreakIterator;->getWordInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/BreakTransliterator;->bi:Lcom/ibm/icu/text/BreakIterator;

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/BreakTransliterator;->bi:Lcom/ibm/icu/text/BreakIterator;

    return-object v0
.end method

.method public getInsertion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/ibm/icu/text/BreakTransliterator;->insertion:Ljava/lang/String;

    return-object v0
.end method

.method protected handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V
    .locals 11
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "pos"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "incremental"    # Z

    .prologue
    .line 65
    const/4 v6, 0x0

    iput v6, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaryCount:I

    .line 66
    const/4 v0, 0x0

    .line 67
    .local v0, "boundary":I
    invoke-virtual {p0}, Lcom/ibm/icu/text/BreakTransliterator;->getBreakIterator()Lcom/ibm/icu/text/BreakIterator;

    .line 68
    iget-object v6, p0, Lcom/ibm/icu/text/BreakTransliterator;->bi:Lcom/ibm/icu/text/BreakIterator;

    new-instance v7, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;

    iget v8, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    iget v9, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    iget v10, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    invoke-direct {v7, p1, v8, v9, v10}, Lcom/ibm/icu/text/BreakTransliterator$ReplaceableCharacterIterator;-><init>(Lcom/ibm/icu/text/Replaceable;III)V

    invoke-virtual {v6, v7}, Lcom/ibm/icu/text/BreakIterator;->setText(Ljava/text/CharacterIterator;)V

    .line 80
    iget-object v6, p0, Lcom/ibm/icu/text/BreakTransliterator;->bi:Lcom/ibm/icu/text/BreakIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/BreakIterator;->first()I

    move-result v0

    :goto_0
    const/4 v6, -0x1

    if-eq v0, v6, :cond_3

    iget v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    if-ge v0, v6, :cond_3

    .line 81
    if-nez v0, :cond_1

    .line 80
    :cond_0
    :goto_1
    iget-object v6, p0, Lcom/ibm/icu/text/BreakTransliterator;->bi:Lcom/ibm/icu/text/BreakIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/BreakIterator;->next()I

    move-result v0

    goto :goto_0

    .line 84
    :cond_1
    add-int/lit8 v6, v0, -0x1

    invoke-static {p1, v6}, Lcom/ibm/icu/text/UTF16;->charAt(Lcom/ibm/icu/text/Replaceable;I)I

    move-result v1

    .line 85
    .local v1, "cp":I
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v5

    .line 87
    .local v5, "type":I
    const/4 v6, 0x1

    shl-int/2addr v6, v5

    and-int/lit16 v6, v6, 0x1fe

    if-eqz v6, :cond_0

    .line 89
    invoke-static {p1, v0}, Lcom/ibm/icu/text/UTF16;->charAt(Lcom/ibm/icu/text/Replaceable;I)I

    move-result v1

    .line 90
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v5

    .line 92
    const/4 v6, 0x1

    shl-int/2addr v6, v5

    and-int/lit16 v6, v6, 0x1fe

    if-eqz v6, :cond_0

    .line 94
    iget v6, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaryCount:I

    iget-object v7, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaries:[I

    array-length v7, v7

    if-lt v6, v7, :cond_2

    .line 95
    iget-object v6, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaries:[I

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x2

    new-array v4, v6, [I

    .line 96
    .local v4, "temp":[I
    iget-object v6, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaries:[I

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaries:[I

    array-length v9, v9

    invoke-static {v6, v7, v4, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 97
    iput-object v4, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaries:[I

    .line 100
    .end local v4    # "temp":[I
    :cond_2
    iget-object v6, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaries:[I

    iget v7, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaryCount:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaryCount:I

    aput v0, v6, v7

    goto :goto_1

    .line 104
    .end local v1    # "cp":I
    .end local v5    # "type":I
    :cond_3
    const/4 v2, 0x0

    .line 105
    .local v2, "delta":I
    const/4 v3, 0x0

    .line 107
    .local v3, "lastBoundary":I
    iget v6, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaryCount:I

    if-eqz v6, :cond_4

    .line 108
    iget v6, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaryCount:I

    iget-object v7, p0, Lcom/ibm/icu/text/BreakTransliterator;->insertion:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    mul-int v2, v6, v7

    .line 109
    iget-object v6, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaries:[I

    iget v7, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaryCount:I

    add-int/lit8 v7, v7, -0x1

    aget v3, v6, v7

    .line 113
    :goto_2
    iget v6, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaryCount:I

    if-lez v6, :cond_4

    .line 114
    iget-object v6, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaries:[I

    iget v7, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaryCount:I

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lcom/ibm/icu/text/BreakTransliterator;->boundaryCount:I

    aget v0, v6, v7

    .line 115
    iget-object v6, p0, Lcom/ibm/icu/text/BreakTransliterator;->insertion:Ljava/lang/String;

    invoke-interface {p1, v0, v0, v6}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    goto :goto_2

    .line 120
    :cond_4
    iget v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    add-int/2addr v6, v2

    iput v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    .line 121
    iget v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    add-int/2addr v6, v2

    iput v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 122
    if-eqz p3, :cond_5

    add-int v6, v3, v2

    :goto_3
    iput v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 123
    return-void

    .line 122
    :cond_5
    iget v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    goto :goto_3
.end method

.method public setBreakIterator(Lcom/ibm/icu/text/BreakIterator;)V
    .locals 0
    .param p1, "bi"    # Lcom/ibm/icu/text/BreakIterator;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/ibm/icu/text/BreakTransliterator;->bi:Lcom/ibm/icu/text/BreakIterator;

    .line 52
    return-void
.end method

.method public setInsertion(Ljava/lang/String;)V
    .locals 0
    .param p1, "insertion"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/ibm/icu/text/BreakTransliterator;->insertion:Ljava/lang/String;

    .line 41
    return-void
.end method
