.class public Lcom/ibm/icu/text/ChineseDateFormat$Field;
.super Lcom/ibm/icu/text/DateFormat$Field;
.source "ChineseDateFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/ChineseDateFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Field"
.end annotation


# static fields
.field public static final IS_LEAP_MONTH:Lcom/ibm/icu/text/ChineseDateFormat$Field;

.field private static final serialVersionUID:J = -0x46ce68aff2525d8aL


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 214
    new-instance v0, Lcom/ibm/icu/text/ChineseDateFormat$Field;

    const-string/jumbo v1, "is leap month"

    sget v2, Lcom/ibm/icu/util/ChineseCalendar;->IS_LEAP_MONTH:I

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/ChineseDateFormat$Field;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/ibm/icu/text/ChineseDateFormat$Field;->IS_LEAP_MONTH:Lcom/ibm/icu/text/ChineseDateFormat$Field;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "calendarField"    # I

    .prologue
    .line 228
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/DateFormat$Field;-><init>(Ljava/lang/String;I)V

    .line 229
    return-void
.end method

.method public static ofCalendarField(I)Lcom/ibm/icu/text/DateFormat$Field;
    .locals 1
    .param p0, "calendarField"    # I

    .prologue
    .line 245
    sget v0, Lcom/ibm/icu/util/ChineseCalendar;->IS_LEAP_MONTH:I

    if-ne p0, v0, :cond_0

    .line 246
    sget-object v0, Lcom/ibm/icu/text/ChineseDateFormat$Field;->IS_LEAP_MONTH:Lcom/ibm/icu/text/ChineseDateFormat$Field;

    .line 248
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/ibm/icu/text/DateFormat$Field;->ofCalendarField(I)Lcom/ibm/icu/text/DateFormat$Field;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected readResolve()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InvalidObjectException;
        }
    .end annotation

    .prologue
    .line 257
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sget-object v0, Lcom/ibm/icu/text/ChineseDateFormat;->class$com$ibm$icu$text$ChineseDateFormat$Field:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string/jumbo v0, "com.ibm.icu.text.ChineseDateFormat$Field"

    invoke-static {v0}, Lcom/ibm/icu/text/ChineseDateFormat;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/ibm/icu/text/ChineseDateFormat;->class$com$ibm$icu$text$ChineseDateFormat$Field:Ljava/lang/Class;

    :goto_0
    if-eq v1, v0, :cond_1

    .line 258
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string/jumbo v1, "A subclass of ChineseDateFormat.Field must implement readResolve."

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_0
    sget-object v0, Lcom/ibm/icu/text/ChineseDateFormat;->class$com$ibm$icu$text$ChineseDateFormat$Field:Ljava/lang/Class;

    goto :goto_0

    .line 260
    :cond_1
    invoke-virtual {p0}, Lcom/ibm/icu/text/ChineseDateFormat$Field;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/ibm/icu/text/ChineseDateFormat$Field;->IS_LEAP_MONTH:Lcom/ibm/icu/text/ChineseDateFormat$Field;

    invoke-virtual {v1}, Lcom/ibm/icu/text/ChineseDateFormat$Field;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 261
    sget-object v0, Lcom/ibm/icu/text/ChineseDateFormat$Field;->IS_LEAP_MONTH:Lcom/ibm/icu/text/ChineseDateFormat$Field;

    return-object v0

    .line 263
    :cond_2
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string/jumbo v1, "Unknown attribute name."

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
