.class Lcom/ibm/icu/text/RBBISymbolTable;
.super Ljava/lang/Object;
.source "RBBISymbolTable.java"

# interfaces
.implements Lcom/ibm/icu/text/SymbolTable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;
    }
.end annotation


# instance fields
.field fCachedSetLookup:Lcom/ibm/icu/text/UnicodeSet;

.field fHashTable:Ljava/util/HashMap;

.field fRuleScanner:Lcom/ibm/icu/text/RBBIRuleScanner;

.field fRules:Ljava/lang/String;

.field ffffString:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/ibm/icu/text/RBBIRuleScanner;Ljava/lang/String;)V
    .locals 1
    .param p1, "rs"    # Lcom/ibm/icu/text/RBBIRuleScanner;
    .param p2, "rules"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p2, p0, Lcom/ibm/icu/text/RBBISymbolTable;->fRules:Ljava/lang/String;

    .line 37
    iput-object p1, p0, Lcom/ibm/icu/text/RBBISymbolTable;->fRuleScanner:Lcom/ibm/icu/text/RBBIRuleScanner;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBISymbolTable;->fHashTable:Ljava/util/HashMap;

    .line 39
    const-string/jumbo v0, "\uffff"

    iput-object v0, p0, Lcom/ibm/icu/text/RBBISymbolTable;->ffffString:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method addEntry(Ljava/lang/String;Lcom/ibm/icu/text/RBBINode;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "val"    # Lcom/ibm/icu/text/RBBINode;

    .prologue
    .line 159
    iget-object v1, p0, Lcom/ibm/icu/text/RBBISymbolTable;->fHashTable:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;

    .line 160
    .local v0, "e":Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;
    if-eqz v0, :cond_0

    .line 161
    iget-object v1, p0, Lcom/ibm/icu/text/RBBISymbolTable;->fRuleScanner:Lcom/ibm/icu/text/RBBIRuleScanner;

    const v2, 0x10207

    invoke-virtual {v1, v2}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    .line 169
    :goto_0
    return-void

    .line 165
    :cond_0
    new-instance v0, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;

    .end local v0    # "e":Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;
    invoke-direct {v0}, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;-><init>()V

    .line 166
    .restart local v0    # "e":Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;
    iput-object p1, v0, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;->key:Ljava/lang/String;

    .line 167
    iput-object p2, v0, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;->val:Lcom/ibm/icu/text/RBBINode;

    .line 168
    iget-object v1, p0, Lcom/ibm/icu/text/RBBISymbolTable;->fHashTable:Ljava/util/HashMap;

    iget-object v2, v0, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;->key:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public lookup(Ljava/lang/String;)[C
    .locals 8
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 57
    iget-object v6, p0, Lcom/ibm/icu/text/RBBISymbolTable;->fHashTable:Ljava/util/HashMap;

    invoke-virtual {v6, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;

    .line 58
    .local v0, "el":Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;
    if-nez v0, :cond_0

    .line 85
    :goto_0
    return-object v5

    .line 63
    :cond_0
    iget-object v4, v0, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;->val:Lcom/ibm/icu/text/RBBINode;

    .line 64
    .local v4, "varRefNode":Lcom/ibm/icu/text/RBBINode;
    :goto_1
    iget-object v6, v4, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget v6, v6, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    .line 65
    iget-object v4, v4, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 66
    goto :goto_1

    .line 68
    :cond_1
    iget-object v1, v4, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 69
    .local v1, "exprNode":Lcom/ibm/icu/text/RBBINode;
    iget v6, v1, Lcom/ibm/icu/text/RBBINode;->fType:I

    if-nez v6, :cond_2

    .line 73
    iget-object v3, v1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 74
    .local v3, "usetNode":Lcom/ibm/icu/text/RBBINode;
    iget-object v5, v3, Lcom/ibm/icu/text/RBBINode;->fInputSet:Lcom/ibm/icu/text/UnicodeSet;

    iput-object v5, p0, Lcom/ibm/icu/text/RBBISymbolTable;->fCachedSetLookup:Lcom/ibm/icu/text/UnicodeSet;

    .line 75
    iget-object v2, p0, Lcom/ibm/icu/text/RBBISymbolTable;->ffffString:Ljava/lang/String;

    .line 85
    .end local v3    # "usetNode":Lcom/ibm/icu/text/RBBINode;
    .local v2, "retString":Ljava/lang/String;
    :goto_2
    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    goto :goto_0

    .line 81
    .end local v2    # "retString":Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/ibm/icu/text/RBBISymbolTable;->fRuleScanner:Lcom/ibm/icu/text/RBBIRuleScanner;

    const v7, 0x1020f

    invoke-virtual {v6, v7}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    .line 82
    iget-object v2, v1, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    .line 83
    .restart local v2    # "retString":Ljava/lang/String;
    iput-object v5, p0, Lcom/ibm/icu/text/RBBISymbolTable;->fCachedSetLookup:Lcom/ibm/icu/text/UnicodeSet;

    goto :goto_2
.end method

.method public lookupMatcher(I)Lcom/ibm/icu/text/UnicodeMatcher;
    .locals 2
    .param p1, "ch"    # I

    .prologue
    .line 100
    const/4 v0, 0x0

    .line 101
    .local v0, "retVal":Lcom/ibm/icu/text/UnicodeSet;
    const v1, 0xffff

    if-ne p1, v1, :cond_0

    .line 102
    iget-object v0, p0, Lcom/ibm/icu/text/RBBISymbolTable;->fCachedSetLookup:Lcom/ibm/icu/text/UnicodeSet;

    .line 103
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/text/RBBISymbolTable;->fCachedSetLookup:Lcom/ibm/icu/text/UnicodeSet;

    .line 105
    :cond_0
    return-object v0
.end method

.method lookupNode(Ljava/lang/String;)Lcom/ibm/icu/text/RBBINode;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 141
    const/4 v1, 0x0

    .line 144
    .local v1, "retNode":Lcom/ibm/icu/text/RBBINode;
    iget-object v2, p0, Lcom/ibm/icu/text/RBBISymbolTable;->fHashTable:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;

    .line 145
    .local v0, "el":Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;
    if-eqz v0, :cond_0

    .line 146
    iget-object v1, v0, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;->val:Lcom/ibm/icu/text/RBBINode;

    .line 148
    :cond_0
    return-object v1
.end method

.method public parseReference(Ljava/lang/String;Ljava/text/ParsePosition;I)Ljava/lang/String;
    .locals 6
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;
    .param p3, "limit"    # I

    .prologue
    .line 115
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    .line 116
    .local v4, "start":I
    move v1, v4

    .line 117
    .local v1, "i":I
    const-string/jumbo v2, ""

    .line 118
    .local v2, "result":Ljava/lang/String;
    :goto_0
    if-ge v1, p3, :cond_1

    .line 119
    invoke-static {p1, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 120
    .local v0, "c":I
    if-ne v1, v4, :cond_0

    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->isUnicodeIdentifierStart(I)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->isUnicodeIdentifierPart(I)Z

    move-result v5

    if-nez v5, :cond_2

    .line 126
    .end local v0    # "c":I
    :cond_1
    if-ne v1, v4, :cond_3

    move-object v3, v2

    .line 131
    .end local v2    # "result":Ljava/lang/String;
    .local v3, "result":Ljava/lang/String;
    :goto_1
    return-object v3

    .line 124
    .end local v3    # "result":Ljava/lang/String;
    .restart local v0    # "c":I
    .restart local v2    # "result":Ljava/lang/String;
    :cond_2
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v5

    add-int/2addr v1, v5

    .line 125
    goto :goto_0

    .line 129
    .end local v0    # "c":I
    :cond_3
    invoke-virtual {p2, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 130
    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 131
    .end local v2    # "result":Ljava/lang/String;
    .restart local v3    # "result":Ljava/lang/String;
    goto :goto_1
.end method

.method rbbiSymtablePrint()V
    .locals 7

    .prologue
    .line 176
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v5, "Variable Definitions\nName               Node Val     String Val\n----------------------------------------------------------------------\n"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 181
    const/4 v4, 0x0

    new-array v2, v4, [Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;

    .line 182
    .local v2, "syms":[Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;
    iget-object v4, p0, Lcom/ibm/icu/text/RBBISymbolTable;->fHashTable:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    .line 183
    .local v3, "t":Ljava/util/Collection;
    invoke-interface {v3, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;

    move-object v2, v4

    check-cast v2, [Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;

    .line 185
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_0

    .line 186
    aget-object v1, v2, v0

    .line 188
    .local v1, "s":Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, v1, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;->key:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 189
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, v1, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;->val:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 190
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v5, v1, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;->val:Lcom/ibm/icu/text/RBBINode;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 191
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v5, "\n"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 185
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 194
    .end local v1    # "s":Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;
    :cond_0
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v5, "\nParsed Variable Definitions\n"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 195
    const/4 v0, 0x0

    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 196
    aget-object v1, v2, v0

    .line 197
    .restart local v1    # "s":Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v5, v1, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;->key:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 198
    iget-object v4, v1, Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;->val:Lcom/ibm/icu/text/RBBINode;

    iget-object v4, v4, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/ibm/icu/text/RBBINode;->printTree(Z)V

    .line 199
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v5, "\n"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 195
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 201
    .end local v1    # "s":Lcom/ibm/icu/text/RBBISymbolTable$RBBISymbolTableEntry;
    :cond_1
    return-void
.end method
