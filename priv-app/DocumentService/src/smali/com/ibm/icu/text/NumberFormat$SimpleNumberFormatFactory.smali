.class public abstract Lcom/ibm/icu/text/NumberFormat$SimpleNumberFormatFactory;
.super Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;
.source "NumberFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/NumberFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SimpleNumberFormatFactory"
.end annotation


# instance fields
.field final localeNames:Ljava/util/Set;

.field final visible:Z


# direct methods
.method public constructor <init>(Lcom/ibm/icu/util/ULocale;)V
    .locals 1
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 798
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/NumberFormat$SimpleNumberFormatFactory;-><init>(Lcom/ibm/icu/util/ULocale;Z)V

    .line 799
    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/util/ULocale;Z)V
    .locals 1
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "visible"    # Z

    .prologue
    .line 806
    invoke-direct {p0}, Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;-><init>()V

    .line 807
    invoke-virtual {p1}, Lcom/ibm/icu/util/ULocale;->getBaseName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/NumberFormat$SimpleNumberFormatFactory;->localeNames:Ljava/util/Set;

    .line 808
    iput-boolean p2, p0, Lcom/ibm/icu/text/NumberFormat$SimpleNumberFormatFactory;->visible:Z

    .line 809
    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 780
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/NumberFormat$SimpleNumberFormatFactory;-><init>(Ljava/util/Locale;Z)V

    .line 781
    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;Z)V
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;
    .param p2, "visible"    # Z

    .prologue
    .line 788
    invoke-direct {p0}, Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;-><init>()V

    .line 789
    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/util/ULocale;->getBaseName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/NumberFormat$SimpleNumberFormatFactory;->localeNames:Ljava/util/Set;

    .line 790
    iput-boolean p2, p0, Lcom/ibm/icu/text/NumberFormat$SimpleNumberFormatFactory;->visible:Z

    .line 791
    return-void
.end method


# virtual methods
.method public final getSupportedLocaleNames()Ljava/util/Set;
    .locals 1

    .prologue
    .line 824
    iget-object v0, p0, Lcom/ibm/icu/text/NumberFormat$SimpleNumberFormatFactory;->localeNames:Ljava/util/Set;

    return-object v0
.end method

.method public final visible()Z
    .locals 1

    .prologue
    .line 816
    iget-boolean v0, p0, Lcom/ibm/icu/text/NumberFormat$SimpleNumberFormatFactory;->visible:Z

    return v0
.end method
