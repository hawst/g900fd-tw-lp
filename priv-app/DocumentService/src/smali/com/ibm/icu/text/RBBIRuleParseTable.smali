.class Lcom/ibm/icu/text/RBBIRuleParseTable;
.super Ljava/lang/Object;
.source "RBBIRuleParseTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;
    }
.end annotation


# static fields
.field static final doCheckVarDef:S = 0x1s

.field static final doDotAny:S = 0x2s

.field static final doEndAssign:S = 0x3s

.field static final doEndOfRule:S = 0x4s

.field static final doEndVariableName:S = 0x5s

.field static final doExit:S = 0x6s

.field static final doExprCatOperator:S = 0x7s

.field static final doExprFinished:S = 0x8s

.field static final doExprOrOperator:S = 0x9s

.field static final doExprRParen:S = 0xas

.field static final doExprStart:S = 0xbs

.field static final doLParen:S = 0xcs

.field static final doNOP:S = 0xds

.field static final doOptionEnd:S = 0xes

.field static final doOptionStart:S = 0xfs

.field static final doReverseDir:S = 0x10s

.field static final doRuleChar:S = 0x11s

.field static final doRuleError:S = 0x12s

.field static final doRuleErrorAssignExpr:S = 0x13s

.field static final doScanUnicodeSet:S = 0x14s

.field static final doSlash:S = 0x15s

.field static final doStartAssign:S = 0x16s

.field static final doStartTagValue:S = 0x17s

.field static final doStartVariableName:S = 0x18s

.field static final doTagDigit:S = 0x19s

.field static final doTagExpectedError:S = 0x1as

.field static final doTagValue:S = 0x1bs

.field static final doUnaryOpPlus:S = 0x1cs

.field static final doUnaryOpQuestion:S = 0x1ds

.field static final doUnaryOpStar:S = 0x1es

.field static final doVariableNameExpectedErr:S = 0x1fs

.field static gRuleParseStateTable:[Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement; = null

.field static final kRuleSet_default:S = 0xffs

.field static final kRuleSet_digit_char:S = 0x80s

.field static final kRuleSet_eof:S = 0xfcs

.field static final kRuleSet_escaped:S = 0xfes

.field static final kRuleSet_name_char:S = 0x81s

.field static final kRuleSet_name_start_char:S = 0x82s

.field static final kRuleSet_rule_char:S = 0x83s

.field static final kRuleSet_white_space:S = 0x84s


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/16 v1, 0xd

    const/16 v15, 0xff

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 80
    const/16 v0, 0x60

    new-array v14, v0, [Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    new-instance v0, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v0, v14, v2

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0xb

    const/16 v9, 0xfe

    const/16 v10, 0x15

    const/16 v11, 0x8

    const-string/jumbo v13, "start"

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v5

    const/4 v0, 0x2

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x84

    move v8, v1

    move v10, v5

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/4 v0, 0x3

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0xb

    const/16 v9, 0x24

    const/16 v10, 0x50

    const/16 v11, 0x5a

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/4 v0, 0x4

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x21

    const/16 v10, 0xb

    move v8, v1

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/4 v0, 0x5

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x3b

    move v8, v1

    move v10, v5

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/4 v0, 0x6

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0xfc

    move v8, v1

    move v10, v2

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/4 v0, 0x7

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0xb

    const/16 v10, 0x15

    const/16 v11, 0x8

    move v9, v15

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x8

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x4

    const/16 v9, 0x3b

    const-string/jumbo v13, "break-rule-end"

    move v10, v5

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x9

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x84

    const/16 v10, 0x8

    move v8, v1

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0xa

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x12

    const/16 v10, 0x5f

    move v9, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0xb

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x21

    const-string/jumbo v13, "rev-option"

    move v8, v1

    move v10, v1

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0xc

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x10

    const/16 v10, 0x14

    const/16 v11, 0x8

    move v9, v15

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0xf

    const/16 v9, 0x82

    const/16 v10, 0xf

    const-string/jumbo v13, "option-scan1"

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v1

    const/16 v0, 0xe

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x12

    const/16 v10, 0x5f

    move v9, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0xf

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x81

    const/16 v10, 0xf

    const-string/jumbo v13, "option-scan2"

    move v8, v1

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x10

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0xe

    const/16 v10, 0x11

    move v9, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x11

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x3b

    const-string/jumbo v13, "option-scan3"

    move v8, v1

    move v10, v5

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x12

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x84

    const/16 v10, 0x11

    move v8, v1

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x13

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x12

    const/16 v10, 0x5f

    move v9, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x14

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0xb

    const/16 v10, 0x15

    const/16 v11, 0x8

    const-string/jumbo v13, "reverse-rule"

    move v9, v15

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x15

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x11

    const/16 v9, 0xfe

    const/16 v10, 0x1e

    const-string/jumbo v13, "term"

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x16

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x84

    const/16 v10, 0x15

    move v8, v1

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x17

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x11

    const/16 v9, 0x83

    const/16 v10, 0x1e

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x18

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x5b

    const/16 v10, 0x56

    const/16 v11, 0x1e

    move v8, v1

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x19

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0xc

    const/16 v9, 0x28

    const/16 v10, 0x15

    const/16 v11, 0x1e

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x1a

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x24

    const/16 v10, 0x50

    const/16 v11, 0x1d

    move v8, v1

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x1b

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x2

    const/16 v9, 0x2e

    const/16 v10, 0x1e

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x1c

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x12

    const/16 v10, 0x5f

    move v9, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x1d

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v10, 0x1e

    const-string/jumbo v13, "term-var-ref"

    move v8, v5

    move v9, v15

    move v11, v2

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x1e

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x84

    const/16 v10, 0x1e

    const-string/jumbo v13, "expr-mod"

    move v8, v1

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x1f

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x1e

    const/16 v9, 0x2a

    const/16 v10, 0x23

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x20

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x1c

    const/16 v9, 0x2b

    const/16 v10, 0x23

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x21

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x1d

    const/16 v9, 0x3f

    const/16 v10, 0x23

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x22

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v10, 0x23

    move v8, v1

    move v9, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x23

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0xfe

    const/16 v10, 0x15

    const-string/jumbo v13, "expr-cont"

    move v11, v2

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x24

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x84

    const/16 v10, 0x23

    move v8, v1

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x25

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x83

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x26

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x5b

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x27

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x28

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x28

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x24

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x29

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x2e

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x2a

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x2f

    const/16 v10, 0x2f

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x2b

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x7b

    const/16 v10, 0x3b

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x2c

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x9

    const/16 v9, 0x7c

    const/16 v10, 0x15

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x2d

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0xa

    const/16 v9, 0x29

    move v10, v15

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x2e

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x8

    move v9, v15

    move v10, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x2f

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x15

    const/16 v9, 0x2f

    const/16 v10, 0x31

    const-string/jumbo v13, "look-ahead"

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x30

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v10, 0x5f

    move v8, v1

    move v9, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x31

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0xfe

    const/16 v10, 0x15

    const-string/jumbo v13, "expr-cont-no-slash"

    move v11, v2

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x32

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x84

    const/16 v10, 0x23

    move v8, v1

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x33

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x83

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x34

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x5b

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x35

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x28

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x36

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x24

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x37

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x2e

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x38

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x9

    const/16 v9, 0x7c

    const/16 v10, 0x15

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x39

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0xa

    const/16 v9, 0x29

    move v10, v15

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x3a

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x8

    move v9, v15

    move v10, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x3b

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x84

    const/16 v10, 0x3b

    const-string/jumbo v13, "tag-open"

    move v8, v1

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x3c

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x17

    const/16 v9, 0x80

    const/16 v10, 0x3e

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x3d

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x1a

    const/16 v10, 0x5f

    move v9, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x3e

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x84

    const/16 v10, 0x42

    const-string/jumbo v13, "tag-value"

    move v8, v1

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x3f

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x7d

    const/16 v10, 0x42

    move v8, v1

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x40

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x19

    const/16 v9, 0x80

    const/16 v10, 0x3e

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x41

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x1a

    const/16 v10, 0x5f

    move v9, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x42

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x84

    const/16 v10, 0x42

    const-string/jumbo v13, "tag-close"

    move v8, v1

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x43

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x1b

    const/16 v9, 0x7d

    const/16 v10, 0x45

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x44

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x1a

    const/16 v10, 0x5f

    move v9, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x45

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0xfe

    const/16 v10, 0x15

    const-string/jumbo v13, "expr-cont-no-tag"

    move v11, v2

    move v12, v2

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x46

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x84

    const/16 v10, 0x45

    move v8, v1

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x47

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x83

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x48

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x5b

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x49

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x28

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x4a

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x24

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x4b

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x2e

    const/16 v10, 0x15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x4c

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x7

    const/16 v9, 0x2f

    const/16 v10, 0x2f

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x4d

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x9

    const/16 v9, 0x7c

    const/16 v10, 0x15

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x4e

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0xa

    const/16 v9, 0x29

    move v10, v15

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x4f

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x8

    move v9, v15

    move v10, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x50

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x18

    const/16 v9, 0x24

    const/16 v10, 0x52

    const-string/jumbo v13, "scan-var-name"

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x51

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v10, 0x5f

    move v8, v1

    move v9, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x52

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x82

    const/16 v10, 0x54

    const-string/jumbo v13, "scan-var-start"

    move v8, v1

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x53

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x1f

    const/16 v10, 0x5f

    move v9, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x54

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x81

    const/16 v10, 0x54

    const-string/jumbo v13, "scan-var-body"

    move v8, v1

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x55

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x5

    move v9, v15

    move v10, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x56

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x14

    const/16 v9, 0x5b

    const-string/jumbo v13, "scan-unicode-set"

    move v10, v15

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x57

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x14

    const/16 v9, 0x70

    move v10, v15

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x58

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x14

    const/16 v9, 0x50

    move v10, v15

    move v11, v2

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x59

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v10, 0x5f

    move v8, v1

    move v9, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x5a

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v9, 0x84

    const/16 v10, 0x5a

    const-string/jumbo v13, "assign-or-rule"

    move v8, v1

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x5b

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x16

    const/16 v9, 0x3d

    const/16 v10, 0x15

    const/16 v11, 0x5d

    move v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x5c

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v10, 0x1d

    const/16 v11, 0x8

    move v8, v1

    move v9, v15

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x5d

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v8, 0x3

    const/16 v9, 0x3b

    const-string/jumbo v13, "assign-end"

    move v10, v5

    move v11, v2

    move v12, v5

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x5e

    new-instance v7, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/16 v8, 0x13

    const/16 v10, 0x5f

    move v9, v15

    move v11, v2

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v7, v14, v0

    const/16 v0, 0x5f

    new-instance v6, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    const/4 v7, 0x6

    const/16 v9, 0x5f

    const-string/jumbo v12, "errorDeath"

    move v8, v15

    move v10, v2

    move v11, v5

    invoke-direct/range {v6 .. v12}, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;-><init>(SIIIZLjava/lang/String;)V

    aput-object v6, v14, v0

    sput-object v14, Lcom/ibm/icu/text/RBBIRuleParseTable;->gRuleParseStateTable:[Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    return-void
.end method
