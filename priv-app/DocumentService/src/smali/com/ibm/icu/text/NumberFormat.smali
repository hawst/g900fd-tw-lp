.class public abstract Lcom/ibm/icu/text/NumberFormat;
.super Lcom/ibm/icu/text/UFormat;
.source "NumberFormat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/NumberFormat$Field;,
        Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;,
        Lcom/ibm/icu/text/NumberFormat$SimpleNumberFormatFactory;,
        Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;
    }
.end annotation


# static fields
.field private static final CURRENCYSTYLE:I = 0x1

.field public static final FRACTION_FIELD:I = 0x1

.field private static final INTEGERSTYLE:I = 0x4

.field public static final INTEGER_FIELD:I = 0x0

.field private static final NUMBERSTYLE:I = 0x0

.field private static final PERCENTSTYLE:I = 0x2

.field private static final SCIENTIFICSTYLE:I = 0x3

.field static final currentSerialVersion:I = 0x1

.field private static final serialVersionUID:J = -0x20094c40ec82f818L

.field private static shim:Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;


# instance fields
.field private currency:Lcom/ibm/icu/util/Currency;

.field private groupingUsed:Z

.field private maxFractionDigits:B

.field private maxIntegerDigits:B

.field private maximumFractionDigits:I

.field private maximumIntegerDigits:I

.field private minFractionDigits:B

.field private minIntegerDigits:B

.field private minimumFractionDigits:I

.field private minimumIntegerDigits:I

.field private parseIntegerOnly:Z

.field private parseStrict:Z

.field private serialVersionOnStream:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x28

    const/4 v2, 0x3

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1558
    invoke-direct {p0}, Lcom/ibm/icu/text/UFormat;-><init>()V

    .line 1392
    iput-boolean v0, p0, Lcom/ibm/icu/text/NumberFormat;->groupingUsed:Z

    .line 1410
    iput-byte v3, p0, Lcom/ibm/icu/text/NumberFormat;->maxIntegerDigits:B

    .line 1428
    iput-byte v0, p0, Lcom/ibm/icu/text/NumberFormat;->minIntegerDigits:B

    .line 1446
    iput-byte v2, p0, Lcom/ibm/icu/text/NumberFormat;->maxFractionDigits:B

    .line 1464
    iput-byte v1, p0, Lcom/ibm/icu/text/NumberFormat;->minFractionDigits:B

    .line 1472
    iput-boolean v1, p0, Lcom/ibm/icu/text/NumberFormat;->parseIntegerOnly:Z

    .line 1484
    iput v3, p0, Lcom/ibm/icu/text/NumberFormat;->maximumIntegerDigits:I

    .line 1494
    iput v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumIntegerDigits:I

    .line 1504
    iput v2, p0, Lcom/ibm/icu/text/NumberFormat;->maximumFractionDigits:I

    .line 1514
    iput v1, p0, Lcom/ibm/icu/text/NumberFormat;->minimumFractionDigits:I

    .line 1546
    iput v0, p0, Lcom/ibm/icu/text/NumberFormat;->serialVersionOnStream:I

    .line 1559
    return-void
.end method

.method static createInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;
    .locals 9
    .param p0, "desiredLocale"    # Lcom/ibm/icu/util/ULocale;
    .param p1, "choice"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1202
    invoke-static {p0, p1}, Lcom/ibm/icu/text/NumberFormat;->getPattern(Lcom/ibm/icu/util/ULocale;I)Ljava/lang/String;

    move-result-object v2

    .line 1203
    .local v2, "pattern":Ljava/lang/String;
    new-instance v3, Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-direct {v3, p0}, Lcom/ibm/icu/text/DecimalFormatSymbols;-><init>(Lcom/ibm/icu/util/ULocale;)V

    .line 1207
    .local v3, "symbols":Lcom/ibm/icu/text/DecimalFormatSymbols;
    if-ne p1, v8, :cond_0

    .line 1208
    invoke-virtual {v3}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getCurrencyPattern()Ljava/lang/String;

    move-result-object v4

    .line 1209
    .local v4, "temp":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 1210
    move-object v2, v4

    .line 1214
    .end local v4    # "temp":Ljava/lang/String;
    :cond_0
    new-instance v1, Lcom/ibm/icu/text/DecimalFormat;

    invoke-direct {v1, v2, v3}, Lcom/ibm/icu/text/DecimalFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/DecimalFormatSymbols;)V

    .line 1222
    .local v1, "format":Lcom/ibm/icu/text/DecimalFormat;
    const/4 v6, 0x4

    if-ne p1, v6, :cond_1

    .line 1223
    invoke-virtual {v1, v7}, Lcom/ibm/icu/text/DecimalFormat;->setMaximumFractionDigits(I)V

    .line 1224
    invoke-virtual {v1, v7}, Lcom/ibm/icu/text/DecimalFormat;->setDecimalSeparatorAlwaysShown(Z)V

    .line 1225
    invoke-virtual {v1, v8}, Lcom/ibm/icu/text/DecimalFormat;->setParseIntegerOnly(Z)V

    .line 1231
    :cond_1
    sget-object v6, Lcom/ibm/icu/util/ULocale;->VALID_LOCALE:Lcom/ibm/icu/util/ULocale$Type;

    invoke-virtual {v3, v6}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getLocale(Lcom/ibm/icu/util/ULocale$Type;)Lcom/ibm/icu/util/ULocale;

    move-result-object v5

    .line 1232
    .local v5, "valid":Lcom/ibm/icu/util/ULocale;
    sget-object v6, Lcom/ibm/icu/util/ULocale;->ACTUAL_LOCALE:Lcom/ibm/icu/util/ULocale$Type;

    invoke-virtual {v3, v6}, Lcom/ibm/icu/text/DecimalFormatSymbols;->getLocale(Lcom/ibm/icu/util/ULocale$Type;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    .line 1233
    .local v0, "actual":Lcom/ibm/icu/util/ULocale;
    invoke-virtual {v1, v5, v0}, Lcom/ibm/icu/text/DecimalFormat;->setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V

    .line 1235
    return-object v1
.end method

.method public static getAvailableLocales()[Ljava/util/Locale;
    .locals 1

    .prologue
    .line 867
    sget-object v0, Lcom/ibm/icu/text/NumberFormat;->shim:Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;

    if-nez v0, :cond_0

    .line 868
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableLocales(Ljava/lang/String;)[Ljava/util/Locale;

    move-result-object v0

    .line 870
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/ibm/icu/text/NumberFormat;->getShim()Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v0

    goto :goto_0
.end method

.method public static getAvailableULocales()[Lcom/ibm/icu/util/ULocale;
    .locals 1

    .prologue
    .line 880
    sget-object v0, Lcom/ibm/icu/text/NumberFormat;->shim:Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;

    if-nez v0, :cond_0

    .line 881
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableULocales(Ljava/lang/String;)[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    .line 883
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/ibm/icu/text/NumberFormat;->getShim()Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;->getAvailableULocales()[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    goto :goto_0
.end method

.method public static final getCurrencyInstance()Lcom/ibm/icu/text/NumberFormat;
    .locals 2

    .prologue
    .line 583
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getCurrencyInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;
    .locals 1
    .param p0, "inLocale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 601
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getCurrencyInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/NumberFormat;
    .locals 2
    .param p0, "inLocale"    # Ljava/util/Locale;

    .prologue
    .line 592
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static final getInstance()Lcom/ibm/icu/text/NumberFormat;
    .locals 2

    .prologue
    .line 479
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;
    .locals 1
    .param p0, "inLocale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 501
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method private static getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;
    .locals 1
    .param p0, "desiredLocale"    # Lcom/ibm/icu/util/ULocale;
    .param p1, "choice"    # I

    .prologue
    .line 1197
    invoke-static {}, Lcom/ibm/icu/text/NumberFormat;->getShim()Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;->createInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/NumberFormat;
    .locals 2
    .param p0, "inLocale"    # Ljava/util/Locale;

    .prologue
    .line 490
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static final getIntegerInstance()Lcom/ibm/icu/text/NumberFormat;
    .locals 2

    .prologue
    .line 541
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getIntegerInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;
    .locals 1
    .param p0, "inLocale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 574
    const/4 v0, 0x4

    invoke-static {p0, v0}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getIntegerInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/NumberFormat;
    .locals 2
    .param p0, "inLocale"    # Ljava/util/Locale;

    .prologue
    .line 558
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static final getNumberInstance()Lcom/ibm/icu/text/NumberFormat;
    .locals 2

    .prologue
    .line 509
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getNumberInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;
    .locals 1
    .param p0, "inLocale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 525
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getNumberInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/NumberFormat;
    .locals 2
    .param p0, "inLocale"    # Ljava/util/Locale;

    .prologue
    .line 517
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method protected static getPattern(Lcom/ibm/icu/util/ULocale;I)Ljava/lang/String;
    .locals 4
    .param p0, "forLocale"    # Lcom/ibm/icu/util/ULocale;
    .param p1, "choice"    # I

    .prologue
    .line 1277
    const/4 v3, 0x3

    if-ne p1, v3, :cond_0

    .line 1280
    const-string/jumbo v3, "#E0"

    .line 1315
    :goto_0
    return-object v3

    .line 1295
    :cond_0
    const-string/jumbo v3, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v3, p0}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 1297
    .local v2, "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v3, "NumberPatterns"

    invoke-virtual {v2, v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1314
    .local v1, "numberPatterns":[Ljava/lang/String;
    const/4 v3, 0x4

    if-ne p1, v3, :cond_1

    const/4 v0, 0x0

    .line 1315
    .local v0, "entry":I
    :goto_1
    aget-object v3, v1, v0

    goto :goto_0

    .end local v0    # "entry":I
    :cond_1
    move v0, p1

    .line 1314
    goto :goto_1
.end method

.method protected static getPattern(Ljava/util/Locale;I)Ljava/lang/String;
    .locals 1
    .param p0, "forLocale"    # Ljava/util/Locale;
    .param p1, "choice"    # I

    .prologue
    .line 1246
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/ibm/icu/text/NumberFormat;->getPattern(Lcom/ibm/icu/util/ULocale;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final getPercentInstance()Lcom/ibm/icu/text/NumberFormat;
    .locals 2

    .prologue
    .line 610
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getPercentInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;
    .locals 1
    .param p0, "inLocale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 628
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getPercentInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/NumberFormat;
    .locals 2
    .param p0, "inLocale"    # Ljava/util/Locale;

    .prologue
    .line 619
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static final getScientificInstance()Lcom/ibm/icu/text/NumberFormat;
    .locals 2

    .prologue
    .line 638
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getScientificInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/NumberFormat;
    .locals 1
    .param p0, "inLocale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 658
    const/4 v0, 0x3

    invoke-static {p0, v0}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getScientificInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/NumberFormat;
    .locals 2
    .param p0, "inLocale"    # Ljava/util/Locale;

    .prologue
    .line 648
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/ibm/icu/text/NumberFormat;->getInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/NumberFormat;

    move-result-object v0

    return-object v0
.end method

.method private static getShim()Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;
    .locals 4

    .prologue
    .line 843
    sget-object v2, Lcom/ibm/icu/text/NumberFormat;->shim:Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;

    if-nez v2, :cond_0

    .line 845
    :try_start_0
    const-string/jumbo v2, "com.ibm.icu.text.NumberFormatServiceShim"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 846
    .local v0, "cls":Ljava/lang/Class;
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;

    sput-object v2, Lcom/ibm/icu/text/NumberFormat;->shim:Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 858
    :cond_0
    sget-object v2, Lcom/ibm/icu/text/NumberFormat;->shim:Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;

    return-object v2

    .line 848
    :catch_0
    move-exception v1

    .line 849
    .local v1, "e":Ljava/util/MissingResourceException;
    throw v1

    .line 851
    .end local v1    # "e":Ljava/util/MissingResourceException;
    :catch_1
    move-exception v1

    .line 854
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 3
    .param p1, "stream"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1332
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 1335
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->serialVersionOnStream:I

    if-ge v0, v2, :cond_0

    .line 1337
    iget-byte v0, p0, Lcom/ibm/icu/text/NumberFormat;->maxIntegerDigits:B

    iput v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumIntegerDigits:I

    .line 1338
    iget-byte v0, p0, Lcom/ibm/icu/text/NumberFormat;->minIntegerDigits:B

    iput v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumIntegerDigits:I

    .line 1339
    iget-byte v0, p0, Lcom/ibm/icu/text/NumberFormat;->maxFractionDigits:B

    iput v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumFractionDigits:I

    .line 1340
    iget-byte v0, p0, Lcom/ibm/icu/text/NumberFormat;->minFractionDigits:B

    iput v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumFractionDigits:I

    .line 1346
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumIntegerDigits:I

    iget v1, p0, Lcom/ibm/icu/text/NumberFormat;->maximumIntegerDigits:I

    if-gt v0, v1, :cond_1

    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumFractionDigits:I

    iget v1, p0, Lcom/ibm/icu/text/NumberFormat;->maximumFractionDigits:I

    if-gt v0, v1, :cond_1

    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumIntegerDigits:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumFractionDigits:I

    if-gez v0, :cond_2

    .line 1349
    :cond_1
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string/jumbo v1, "Digit count range invalid"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1351
    :cond_2
    iput v2, p0, Lcom/ibm/icu/text/NumberFormat;->serialVersionOnStream:I

    .line 1352
    return-void
.end method

.method public static registerFactory(Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;)Ljava/lang/Object;
    .locals 2
    .param p0, "factory"    # Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;

    .prologue
    .line 895
    if-nez p0, :cond_0

    .line 896
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "factory must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898
    :cond_0
    invoke-static {}, Lcom/ibm/icu/text/NumberFormat;->getShim()Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;->registerFactory(Lcom/ibm/icu/text/NumberFormat$NumberFormatFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static unregister(Ljava/lang/Object;)Z
    .locals 2
    .param p0, "registryKey"    # Ljava/lang/Object;

    .prologue
    .line 909
    if-nez p0, :cond_0

    .line 910
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "registryKey must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 913
    :cond_0
    sget-object v0, Lcom/ibm/icu/text/NumberFormat;->shim:Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;

    if-nez v0, :cond_1

    .line 914
    const/4 v0, 0x0

    .line 917
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/ibm/icu/text/NumberFormat;->shim:Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/text/NumberFormat$NumberFormatShim;->unregister(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2
    .param p1, "stream"    # Ljava/io/ObjectOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x7f

    .line 1364
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumIntegerDigits:I

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-byte v0, p0, Lcom/ibm/icu/text/NumberFormat;->maxIntegerDigits:B

    .line 1366
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumIntegerDigits:I

    if-le v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-byte v0, p0, Lcom/ibm/icu/text/NumberFormat;->minIntegerDigits:B

    .line 1368
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumFractionDigits:I

    if-le v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-byte v0, p0, Lcom/ibm/icu/text/NumberFormat;->maxFractionDigits:B

    .line 1370
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumFractionDigits:I

    if-le v0, v1, :cond_3

    :goto_3
    iput-byte v1, p0, Lcom/ibm/icu/text/NumberFormat;->minFractionDigits:B

    .line 1372
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 1373
    return-void

    .line 1364
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumIntegerDigits:I

    int-to-byte v0, v0

    goto :goto_0

    .line 1366
    :cond_1
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumIntegerDigits:I

    int-to-byte v0, v0

    goto :goto_1

    .line 1368
    :cond_2
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumFractionDigits:I

    int-to-byte v0, v0

    goto :goto_2

    .line 1370
    :cond_3
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumFractionDigits:I

    int-to-byte v1, v0

    goto :goto_3
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 961
    invoke-super {p0}, Lcom/ibm/icu/text/UFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/NumberFormat;

    .line 962
    .local v0, "other":Lcom/ibm/icu/text/NumberFormat;
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 940
    if-nez p1, :cond_1

    .line 946
    :cond_0
    :goto_0
    return v2

    .line 941
    :cond_1
    if-ne p0, p1, :cond_2

    move v2, v1

    .line 942
    goto :goto_0

    .line 943
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-ne v3, v4, :cond_0

    move-object v0, p1

    .line 945
    check-cast v0, Lcom/ibm/icu/text/NumberFormat;

    .line 946
    .local v0, "other":Lcom/ibm/icu/text/NumberFormat;
    iget v3, p0, Lcom/ibm/icu/text/NumberFormat;->maximumIntegerDigits:I

    iget v4, v0, Lcom/ibm/icu/text/NumberFormat;->maximumIntegerDigits:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/ibm/icu/text/NumberFormat;->minimumIntegerDigits:I

    iget v4, v0, Lcom/ibm/icu/text/NumberFormat;->minimumIntegerDigits:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/ibm/icu/text/NumberFormat;->maximumFractionDigits:I

    iget v4, v0, Lcom/ibm/icu/text/NumberFormat;->maximumFractionDigits:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/ibm/icu/text/NumberFormat;->minimumFractionDigits:I

    iget v4, v0, Lcom/ibm/icu/text/NumberFormat;->minimumFractionDigits:I

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/ibm/icu/text/NumberFormat;->groupingUsed:Z

    iget-boolean v4, v0, Lcom/ibm/icu/text/NumberFormat;->groupingUsed:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/ibm/icu/text/NumberFormat;->parseIntegerOnly:Z

    iget-boolean v4, v0, Lcom/ibm/icu/text/NumberFormat;->parseIntegerOnly:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/ibm/icu/text/NumberFormat;->parseStrict:Z

    iget-boolean v4, v0, Lcom/ibm/icu/text/NumberFormat;->parseStrict:Z

    if-ne v3, v4, :cond_3

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public final format(D)Ljava/lang/String;
    .locals 3
    .param p1, "number"    # D

    .prologue
    .line 223
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v1, Ljava/text/FieldPosition;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/text/FieldPosition;-><init>(I)V

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/ibm/icu/text/NumberFormat;->format(DLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final format(J)Ljava/lang/String;
    .locals 3
    .param p1, "number"    # J

    .prologue
    .line 233
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v2, 0x13

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 234
    .local v0, "buf":Ljava/lang/StringBuffer;
    new-instance v1, Ljava/text/FieldPosition;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/text/FieldPosition;-><init>(I)V

    .line 235
    .local v1, "pos":Ljava/text/FieldPosition;
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/ibm/icu/text/NumberFormat;->format(JLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 236
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public final format(Lcom/ibm/icu/math/BigDecimal;)Ljava/lang/String;
    .locals 3
    .param p1, "number"    # Lcom/ibm/icu/math/BigDecimal;

    .prologue
    .line 268
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v1, Ljava/text/FieldPosition;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/text/FieldPosition;-><init>(I)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/ibm/icu/text/NumberFormat;->format(Lcom/ibm/icu/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final format(Lcom/ibm/icu/util/CurrencyAmount;)Ljava/lang/String;
    .locals 3
    .param p1, "currAmt"    # Lcom/ibm/icu/util/CurrencyAmount;

    .prologue
    .line 278
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v1, Ljava/text/FieldPosition;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/text/FieldPosition;-><init>(I)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/ibm/icu/text/NumberFormat;->format(Lcom/ibm/icu/util/CurrencyAmount;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final format(Ljava/math/BigDecimal;)Ljava/lang/String;
    .locals 3
    .param p1, "number"    # Ljava/math/BigDecimal;

    .prologue
    .line 257
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v1, Ljava/text/FieldPosition;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/text/FieldPosition;-><init>(I)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/ibm/icu/text/NumberFormat;->format(Ljava/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final format(Ljava/math/BigInteger;)Ljava/lang/String;
    .locals 3
    .param p1, "number"    # Ljava/math/BigInteger;

    .prologue
    .line 245
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v1, Ljava/text/FieldPosition;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/text/FieldPosition;-><init>(I)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/ibm/icu/text/NumberFormat;->format(Ljava/math/BigInteger;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract format(DLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
.end method

.method public abstract format(JLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
.end method

.method public abstract format(Lcom/ibm/icu/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
.end method

.method public format(Lcom/ibm/icu/util/CurrencyAmount;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 4
    .param p1, "currAmt"    # Lcom/ibm/icu/util/CurrencyAmount;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 341
    invoke-virtual {p0}, Lcom/ibm/icu/text/NumberFormat;->getCurrency()Lcom/ibm/icu/util/Currency;

    move-result-object v2

    .local v2, "save":Lcom/ibm/icu/util/Currency;
    invoke-virtual {p1}, Lcom/ibm/icu/util/CurrencyAmount;->getCurrency()Lcom/ibm/icu/util/Currency;

    move-result-object v0

    .line 342
    .local v0, "curr":Lcom/ibm/icu/util/Currency;
    invoke-virtual {v0, v2}, Lcom/ibm/icu/util/Currency;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 343
    .local v1, "same":Z
    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/NumberFormat;->setCurrency(Lcom/ibm/icu/util/Currency;)V

    .line 344
    :cond_0
    invoke-virtual {p1}, Lcom/ibm/icu/util/CurrencyAmount;->getNumber()Ljava/lang/Number;

    move-result-object v3

    invoke-virtual {p0, v3, p2, p3}, Lcom/ibm/icu/text/NumberFormat;->format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    .line 345
    if-nez v1, :cond_1

    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/NumberFormat;->setCurrency(Lcom/ibm/icu/util/Currency;)V

    .line 346
    :cond_1
    return-object p2
.end method

.method public format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 2
    .param p1, "number"    # Ljava/lang/Object;
    .param p2, "toAppendTo"    # Ljava/lang/StringBuffer;
    .param p3, "pos"    # Ljava/text/FieldPosition;

    .prologue
    .line 188
    instance-of v0, p1, Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 189
    check-cast p1, Ljava/lang/Long;

    .end local p1    # "number":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1, p2, p3}, Lcom/ibm/icu/text/NumberFormat;->format(JLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 202
    :goto_0
    return-object v0

    .line 190
    .restart local p1    # "number":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Ljava/math/BigInteger;

    if-eqz v0, :cond_1

    .line 191
    check-cast p1, Ljava/math/BigInteger;

    .end local p1    # "number":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3}, Lcom/ibm/icu/text/NumberFormat;->format(Ljava/math/BigInteger;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    goto :goto_0

    .line 194
    .restart local p1    # "number":Ljava/lang/Object;
    :cond_1
    instance-of v0, p1, Ljava/math/BigDecimal;

    if-eqz v0, :cond_2

    .line 195
    check-cast p1, Ljava/math/BigDecimal;

    .end local p1    # "number":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3}, Lcom/ibm/icu/text/NumberFormat;->format(Ljava/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    goto :goto_0

    .line 197
    .restart local p1    # "number":Ljava/lang/Object;
    :cond_2
    instance-of v0, p1, Lcom/ibm/icu/math/BigDecimal;

    if-eqz v0, :cond_3

    .line 198
    check-cast p1, Lcom/ibm/icu/math/BigDecimal;

    .end local p1    # "number":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3}, Lcom/ibm/icu/text/NumberFormat;->format(Lcom/ibm/icu/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    goto :goto_0

    .line 199
    .restart local p1    # "number":Ljava/lang/Object;
    :cond_3
    instance-of v0, p1, Lcom/ibm/icu/util/CurrencyAmount;

    if-eqz v0, :cond_4

    .line 200
    check-cast p1, Lcom/ibm/icu/util/CurrencyAmount;

    .end local p1    # "number":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2, p3}, Lcom/ibm/icu/text/NumberFormat;->format(Lcom/ibm/icu/util/CurrencyAmount;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    goto :goto_0

    .line 201
    .restart local p1    # "number":Ljava/lang/Object;
    :cond_4
    instance-of v0, p1, Ljava/lang/Number;

    if-eqz v0, :cond_5

    .line 202
    check-cast p1, Ljava/lang/Number;

    .end local p1    # "number":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1, p2, p3}, Lcom/ibm/icu/text/NumberFormat;->format(DLjava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;

    move-result-object v0

    goto :goto_0

    .line 204
    .restart local p1    # "number":Ljava/lang/Object;
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Cannot format given Object as a Number"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract format(Ljava/math/BigDecimal;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
.end method

.method public abstract format(Ljava/math/BigInteger;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
.end method

.method public getCurrency()Lcom/ibm/icu/util/Currency;
    .locals 1

    .prologue
    .line 1137
    iget-object v0, p0, Lcom/ibm/icu/text/NumberFormat;->currency:Lcom/ibm/icu/util/Currency;

    return-object v0
.end method

.method protected getEffectiveCurrency()Lcom/ibm/icu/util/Currency;
    .locals 3

    .prologue
    .line 1149
    invoke-virtual {p0}, Lcom/ibm/icu/text/NumberFormat;->getCurrency()Lcom/ibm/icu/util/Currency;

    move-result-object v0

    .line 1150
    .local v0, "c":Lcom/ibm/icu/util/Currency;
    if-nez v0, :cond_1

    .line 1151
    sget-object v2, Lcom/ibm/icu/util/ULocale;->VALID_LOCALE:Lcom/ibm/icu/util/ULocale$Type;

    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/NumberFormat;->getLocale(Lcom/ibm/icu/util/ULocale$Type;)Lcom/ibm/icu/util/ULocale;

    move-result-object v1

    .line 1152
    .local v1, "uloc":Lcom/ibm/icu/util/ULocale;
    if-nez v1, :cond_0

    .line 1153
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v1

    .line 1155
    :cond_0
    invoke-static {v1}, Lcom/ibm/icu/util/Currency;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/Currency;

    move-result-object v0

    .line 1157
    .end local v1    # "uloc":Lcom/ibm/icu/util/ULocale;
    :cond_1
    return-object v0
.end method

.method public getMaximumFractionDigits()I
    .locals 1

    .prologue
    .line 1064
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumFractionDigits:I

    return v0
.end method

.method public getMaximumIntegerDigits()I
    .locals 1

    .prologue
    .line 1000
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumIntegerDigits:I

    return v0
.end method

.method public getMinimumFractionDigits()I
    .locals 1

    .prologue
    .line 1096
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumFractionDigits:I

    return v0
.end method

.method public getMinimumIntegerDigits()I
    .locals 1

    .prologue
    .line 1032
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumIntegerDigits:I

    return v0
.end method

.method public getRoundingMode()I
    .locals 2

    .prologue
    .line 1170
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "getRoundingMode must be implemented by the subclass implementation."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 927
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumIntegerDigits:I

    mul-int/lit8 v0, v0, 0x25

    iget-byte v1, p0, Lcom/ibm/icu/text/NumberFormat;->maxFractionDigits:B

    add-int/2addr v0, v1

    return v0
.end method

.method public isGroupingUsed()Z
    .locals 1

    .prologue
    .line 976
    iget-boolean v0, p0, Lcom/ibm/icu/text/NumberFormat;->groupingUsed:Z

    return v0
.end method

.method public isParseIntegerOnly()Z
    .locals 1

    .prologue
    .line 421
    iget-boolean v0, p0, Lcom/ibm/icu/text/NumberFormat;->parseIntegerOnly:Z

    return v0
.end method

.method public isParseStrict()Z
    .locals 1

    .prologue
    .line 464
    iget-boolean v0, p0, Lcom/ibm/icu/text/NumberFormat;->parseStrict:Z

    return v0
.end method

.method public parse(Ljava/lang/String;)Ljava/lang/Number;
    .locals 5
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 376
    new-instance v0, Ljava/text/ParsePosition;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/text/ParsePosition;-><init>(I)V

    .line 377
    .local v0, "parsePosition":Ljava/text/ParsePosition;
    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/text/NumberFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v1

    .line 378
    .local v1, "result":Ljava/lang/Number;
    invoke-virtual {v0}, Ljava/text/ParsePosition;->getIndex()I

    move-result v2

    if-nez v2, :cond_0

    .line 379
    new-instance v2, Ljava/text/ParseException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "Unparseable number: \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const/16 v4, 0x22

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/text/ParsePosition;->getErrorIndex()I

    move-result v4

    invoke-direct {v2, v3, v4}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 382
    :cond_0
    return-object v1
.end method

.method public abstract parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;
.end method

.method parseCurrency(Ljava/lang/String;Ljava/text/ParsePosition;)Lcom/ibm/icu/util/CurrencyAmount;
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;

    .prologue
    .line 406
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/NumberFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v0

    .line 407
    .local v0, "n":Ljava/lang/Number;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/ibm/icu/util/CurrencyAmount;

    invoke-virtual {p0}, Lcom/ibm/icu/text/NumberFormat;->getEffectiveCurrency()Lcom/ibm/icu/util/Currency;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/ibm/icu/util/CurrencyAmount;-><init>(Ljava/lang/Number;Lcom/ibm/icu/util/Currency;)V

    goto :goto_0
.end method

.method public final parseObject(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Object;
    .locals 1
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "parsePosition"    # Ljava/text/ParsePosition;

    .prologue
    .line 214
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/NumberFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public setCurrency(Lcom/ibm/icu/util/Currency;)V
    .locals 0
    .param p1, "theCurrency"    # Lcom/ibm/icu/util/Currency;

    .prologue
    .line 1128
    iput-object p1, p0, Lcom/ibm/icu/text/NumberFormat;->currency:Lcom/ibm/icu/util/Currency;

    .line 1129
    return-void
.end method

.method public setGroupingUsed(Z)V
    .locals 0
    .param p1, "newValue"    # Z

    .prologue
    .line 987
    iput-boolean p1, p0, Lcom/ibm/icu/text/NumberFormat;->groupingUsed:Z

    .line 988
    return-void
.end method

.method public setMaximumFractionDigits(I)V
    .locals 2
    .param p1, "newValue"    # I

    .prologue
    .line 1080
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumFractionDigits:I

    .line 1081
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumFractionDigits:I

    iget v1, p0, Lcom/ibm/icu/text/NumberFormat;->minimumFractionDigits:I

    if-ge v0, v1, :cond_0

    .line 1082
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumFractionDigits:I

    iput v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumFractionDigits:I

    .line 1083
    :cond_0
    return-void
.end method

.method public setMaximumIntegerDigits(I)V
    .locals 2
    .param p1, "newValue"    # I

    .prologue
    .line 1016
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumIntegerDigits:I

    .line 1017
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumIntegerDigits:I

    iget v1, p0, Lcom/ibm/icu/text/NumberFormat;->maximumIntegerDigits:I

    if-le v0, v1, :cond_0

    .line 1018
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumIntegerDigits:I

    iput v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumIntegerDigits:I

    .line 1019
    :cond_0
    return-void
.end method

.method public setMinimumFractionDigits(I)V
    .locals 2
    .param p1, "newValue"    # I

    .prologue
    .line 1112
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumFractionDigits:I

    .line 1113
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumFractionDigits:I

    iget v1, p0, Lcom/ibm/icu/text/NumberFormat;->minimumFractionDigits:I

    if-ge v0, v1, :cond_0

    .line 1114
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumFractionDigits:I

    iput v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumFractionDigits:I

    .line 1115
    :cond_0
    return-void
.end method

.method public setMinimumIntegerDigits(I)V
    .locals 2
    .param p1, "newValue"    # I

    .prologue
    .line 1048
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumIntegerDigits:I

    .line 1049
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumIntegerDigits:I

    iget v1, p0, Lcom/ibm/icu/text/NumberFormat;->maximumIntegerDigits:I

    if-le v0, v1, :cond_0

    .line 1050
    iget v0, p0, Lcom/ibm/icu/text/NumberFormat;->minimumIntegerDigits:I

    iput v0, p0, Lcom/ibm/icu/text/NumberFormat;->maximumIntegerDigits:I

    .line 1051
    :cond_0
    return-void
.end method

.method public setParseIntegerOnly(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 431
    iput-boolean p1, p0, Lcom/ibm/icu/text/NumberFormat;->parseIntegerOnly:Z

    .line 432
    return-void
.end method

.method public setParseStrict(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 454
    iput-boolean p1, p0, Lcom/ibm/icu/text/NumberFormat;->parseStrict:Z

    .line 455
    return-void
.end method

.method public setRoundingMode(I)V
    .locals 2
    .param p1, "roundingMode"    # I

    .prologue
    .line 1184
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "setRoundingMode must be implemented by the subclass implementation."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
