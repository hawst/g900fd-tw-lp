.class public abstract Lcom/ibm/icu/text/MeasureFormat;
.super Lcom/ibm/icu/text/UFormat;
.source "MeasureFormat.java"


# static fields
.field static final serialVersionUID:J = -0x63abaa69b1b00340L


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/ibm/icu/text/UFormat;-><init>()V

    return-void
.end method

.method public static getCurrencyFormat()Lcom/ibm/icu/text/MeasureFormat;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0}, Lcom/ibm/icu/text/MeasureFormat;->getCurrencyFormat(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/MeasureFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getCurrencyFormat(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/MeasureFormat;
    .locals 1
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 44
    new-instance v0, Lcom/ibm/icu/text/CurrencyFormat;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/CurrencyFormat;-><init>(Lcom/ibm/icu/util/ULocale;)V

    return-object v0
.end method
