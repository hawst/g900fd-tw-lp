.class Lcom/ibm/icu/text/CollationRuleParser$OptionSet;
.super Ljava/lang/Object;
.source "CollationRuleParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CollationRuleParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "OptionSet"
.end annotation


# instance fields
.field m_caseFirst_:I

.field m_decomposition_:I

.field m_isAlternateHandlingShifted_:Z

.field m_isCaseLevel_:Z

.field m_isFrenchCollation_:Z

.field m_isHiragana4_:Z

.field m_strength_:I

.field m_variableTopValue_:I


# direct methods
.method constructor <init>(Lcom/ibm/icu/text/RuleBasedCollator;)V
    .locals 1
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iget v0, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_variableTopValue_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_variableTopValue_:I

    .line 70
    invoke-virtual {p1}, Lcom/ibm/icu/text/RuleBasedCollator;->isFrenchCollation()Z

    move-result v0

    iput-boolean v0, p0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isFrenchCollation_:Z

    .line 71
    invoke-virtual {p1}, Lcom/ibm/icu/text/RuleBasedCollator;->isAlternateHandlingShifted()Z

    move-result v0

    iput-boolean v0, p0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isAlternateHandlingShifted_:Z

    .line 73
    iget v0, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_caseFirst_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_caseFirst_:I

    .line 74
    invoke-virtual {p1}, Lcom/ibm/icu/text/RuleBasedCollator;->isCaseLevel()Z

    move-result v0

    iput-boolean v0, p0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isCaseLevel_:Z

    .line 75
    invoke-virtual {p1}, Lcom/ibm/icu/text/RuleBasedCollator;->getDecomposition()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_decomposition_:I

    .line 76
    invoke-virtual {p1}, Lcom/ibm/icu/text/RuleBasedCollator;->getStrength()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_strength_:I

    .line 77
    iget-boolean v0, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_isHiragana4_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/CollationRuleParser$OptionSet;->m_isHiragana4_:Z

    .line 78
    return-void
.end method
