.class public abstract Lcom/ibm/icu/text/Collator;
.super Ljava/lang/Object;
.source "Collator.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/Collator$ServiceShim;,
        Lcom/ibm/icu/text/Collator$CollatorFactory;
    }
.end annotation


# static fields
.field private static final BASE:Ljava/lang/String; = "com/ibm/icu/impl/data/icudt40b/coll"

.field public static final CANONICAL_DECOMPOSITION:I = 0x11

.field private static final DEBUG:Z

.field public static final FULL_DECOMPOSITION:I = 0xf

.field public static final IDENTICAL:I = 0xf

.field private static final KEYWORDS:[Ljava/lang/String;

.field public static final NO_DECOMPOSITION:I = 0x10

.field public static final PRIMARY:I = 0x0

.field public static final QUATERNARY:I = 0x3

.field private static final RESOURCE:Ljava/lang/String; = "collations"

.field public static final SECONDARY:I = 0x1

.field public static final TERTIARY:I = 0x2

.field private static shim:Lcom/ibm/icu/text/Collator$ServiceShim;


# instance fields
.field private actualLocale:Lcom/ibm/icu/util/ULocale;

.field private m_decomposition_:I

.field private m_strength_:I

.field private validLocale:Lcom/ibm/icu/util/ULocale;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 573
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "collation"

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/Collator;->KEYWORDS:[Ljava/lang/String;

    .line 946
    const-string/jumbo v0, "collator"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/ibm/icu/text/Collator;->DEBUG:Z

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 939
    const/4 v0, 0x2

    iput v0, p0, Lcom/ibm/icu/text/Collator;->m_strength_:I

    .line 944
    const/16 v0, 0x11

    iput v0, p0, Lcom/ibm/icu/text/Collator;->m_decomposition_:I

    .line 930
    return-void
.end method

.method public static getAvailableLocales()[Ljava/util/Locale;
    .locals 1

    .prologue
    .line 547
    sget-object v0, Lcom/ibm/icu/text/Collator;->shim:Lcom/ibm/icu/text/Collator$ServiceShim;

    if-nez v0, :cond_0

    .line 548
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b/coll"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableLocales(Ljava/lang/String;)[Ljava/util/Locale;

    move-result-object v0

    .line 550
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/text/Collator;->shim:Lcom/ibm/icu/text/Collator$ServiceShim;

    invoke-virtual {v0}, Lcom/ibm/icu/text/Collator$ServiceShim;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v0

    goto :goto_0
.end method

.method public static final getAvailableULocales()[Lcom/ibm/icu/util/ULocale;
    .locals 1

    .prologue
    .line 562
    sget-object v0, Lcom/ibm/icu/text/Collator;->shim:Lcom/ibm/icu/text/Collator$ServiceShim;

    if-nez v0, :cond_0

    .line 563
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b/coll"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableULocales(Ljava/lang/String;)[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    .line 565
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/text/Collator;->shim:Lcom/ibm/icu/text/Collator$ServiceShim;

    invoke-virtual {v0}, Lcom/ibm/icu/text/Collator$ServiceShim;->getAvailableULocales()[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    goto :goto_0
.end method

.method public static getDisplayName(Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;
    .locals 2
    .param p0, "objectLocale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 702
    invoke-static {}, Lcom/ibm/icu/text/Collator;->getShim()Lcom/ibm/icu/text/Collator$ServiceShim;

    move-result-object v0

    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/ibm/icu/text/Collator$ServiceShim;->getDisplayName(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDisplayName(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;
    .locals 1
    .param p0, "objectLocale"    # Lcom/ibm/icu/util/ULocale;
    .param p1, "displayLocale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 682
    invoke-static {}, Lcom/ibm/icu/text/Collator;->getShim()Lcom/ibm/icu/text/Collator$ServiceShim;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/ibm/icu/text/Collator$ServiceShim;->getDisplayName(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDisplayName(Ljava/util/Locale;)Ljava/lang/String;
    .locals 3
    .param p0, "objectLocale"    # Ljava/util/Locale;

    .prologue
    .line 692
    invoke-static {}, Lcom/ibm/icu/text/Collator;->getShim()Lcom/ibm/icu/text/Collator$ServiceShim;

    move-result-object v0

    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v1

    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/Collator$ServiceShim;->getDisplayName(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDisplayName(Ljava/util/Locale;Ljava/util/Locale;)Ljava/lang/String;
    .locals 3
    .param p0, "objectLocale"    # Ljava/util/Locale;
    .param p1, "displayLocale"    # Ljava/util/Locale;

    .prologue
    .line 670
    invoke-static {}, Lcom/ibm/icu/text/Collator;->getShim()Lcom/ibm/icu/text/Collator$ServiceShim;

    move-result-object v0

    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v1

    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/Collator$ServiceShim;->getDisplayName(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final getFunctionalEquivalent(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/ULocale;
    .locals 1
    .param p0, "keyword"    # Ljava/lang/String;
    .param p1, "locID"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 659
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/ibm/icu/text/Collator;->getFunctionalEquivalent(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;[Z)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    return-object v0
.end method

.method public static final getFunctionalEquivalent(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;[Z)Lcom/ibm/icu/util/ULocale;
    .locals 6
    .param p0, "keyword"    # Ljava/lang/String;
    .param p1, "locID"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "isAvailable"    # [Z

    .prologue
    .line 642
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b/coll"

    const-string/jumbo v1, "collations"

    const/4 v5, 0x1

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/ibm/icu/impl/ICUResourceBundle;->getFunctionalEquivalent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/ibm/icu/util/ULocale;[ZZ)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    return-object v0
.end method

.method public static final getInstance()Lcom/ibm/icu/text/Collator;
    .locals 1

    .prologue
    .line 312
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0}, Lcom/ibm/icu/text/Collator;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/Collator;

    move-result-object v0

    return-object v0
.end method

.method public static final getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/Collator;
    .locals 1
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 478
    invoke-static {}, Lcom/ibm/icu/text/Collator;->getShim()Lcom/ibm/icu/text/Collator$ServiceShim;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/ibm/icu/text/Collator$ServiceShim;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/Collator;

    move-result-object v0

    return-object v0
.end method

.method public static final getInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/Collator;
    .locals 1
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    .line 495
    invoke-static {p0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {v0}, Lcom/ibm/icu/text/Collator;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/Collator;

    move-result-object v0

    return-object v0
.end method

.method public static final getKeywordValues(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p0, "keyword"    # Ljava/lang/String;

    .prologue
    .line 608
    sget-object v0, Lcom/ibm/icu/text/Collator;->KEYWORDS:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 609
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Invalid keyword: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 611
    :cond_0
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b/coll"

    const-string/jumbo v1, "collations"

    invoke-static {v0, v1}, Lcom/ibm/icu/impl/ICUResourceBundle;->getKeywordValues(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final getKeywords()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 597
    sget-object v0, Lcom/ibm/icu/text/Collator;->KEYWORDS:[Ljava/lang/String;

    return-object v0
.end method

.method private static getShim()Lcom/ibm/icu/text/Collator$ServiceShim;
    .locals 4

    .prologue
    .line 442
    sget-object v2, Lcom/ibm/icu/text/Collator;->shim:Lcom/ibm/icu/text/Collator$ServiceShim;

    if-nez v2, :cond_0

    .line 444
    :try_start_0
    const-string/jumbo v2, "com.ibm.icu.text.CollatorServiceShim"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 445
    .local v0, "cls":Ljava/lang/Class;
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/text/Collator$ServiceShim;

    sput-object v2, Lcom/ibm/icu/text/Collator;->shim:Lcom/ibm/icu/text/Collator$ServiceShim;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 460
    :cond_0
    sget-object v2, Lcom/ibm/icu/text/Collator;->shim:Lcom/ibm/icu/text/Collator$ServiceShim;

    return-object v2

    .line 447
    :catch_0
    move-exception v1

    .line 449
    .local v1, "e":Ljava/util/MissingResourceException;
    throw v1

    .line 451
    .end local v1    # "e":Ljava/util/MissingResourceException;
    :catch_1
    move-exception v1

    .line 453
    .local v1, "e":Ljava/lang/Exception;
    sget-boolean v2, Lcom/ibm/icu/text/Collator;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 454
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 456
    :cond_1
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static final registerFactory(Lcom/ibm/icu/text/Collator$CollatorFactory;)Ljava/lang/Object;
    .locals 1
    .param p0, "factory"    # Lcom/ibm/icu/text/Collator$CollatorFactory;

    .prologue
    .line 521
    invoke-static {}, Lcom/ibm/icu/text/Collator;->getShim()Lcom/ibm/icu/text/Collator$ServiceShim;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/ibm/icu/text/Collator$ServiceShim;->registerFactory(Lcom/ibm/icu/text/Collator$CollatorFactory;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static final registerInstance(Lcom/ibm/icu/text/Collator;Lcom/ibm/icu/util/ULocale;)Ljava/lang/Object;
    .locals 1
    .param p0, "collator"    # Lcom/ibm/icu/text/Collator;
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 509
    invoke-static {}, Lcom/ibm/icu/text/Collator;->getShim()Lcom/ibm/icu/text/Collator$ServiceShim;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/ibm/icu/text/Collator$ServiceShim;->registerInstance(Lcom/ibm/icu/text/Collator;Lcom/ibm/icu/util/ULocale;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static final unregister(Ljava/lang/Object;)Z
    .locals 1
    .param p0, "registryKey"    # Ljava/lang/Object;

    .prologue
    .line 531
    sget-object v0, Lcom/ibm/icu/text/Collator;->shim:Lcom/ibm/icu/text/Collator$ServiceShim;

    if-nez v0, :cond_0

    .line 532
    const/4 v0, 0x0

    .line 534
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/text/Collator;->shim:Lcom/ibm/icu/text/Collator$ServiceShim;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/text/Collator$ServiceShim;->unregister(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 321
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2
    .param p1, "source"    # Ljava/lang/Object;
    .param p2, "target"    # Ljava/lang/Object;

    .prologue
    .line 768
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_0

    instance-of v0, p2, Ljava/lang/String;

    if-nez v0, :cond_1

    .line 769
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Arguments have to be of type String"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 771
    :cond_1
    check-cast p1, Ljava/lang/String;

    .end local p1    # "source":Ljava/lang/Object;
    check-cast p2, Ljava/lang/String;

    .end local p2    # "target":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public abstract compare(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public equals(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    .line 789
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract getCollationKey(Ljava/lang/String;)Lcom/ibm/icu/text/CollationKey;
.end method

.method public getDecomposition()I
    .locals 1

    .prologue
    .line 742
    iget v0, p0, Lcom/ibm/icu/text/Collator;->m_decomposition_:I

    return v0
.end method

.method public final getLocale(Lcom/ibm/icu/util/ULocale$Type;)Lcom/ibm/icu/util/ULocale;
    .locals 1
    .param p1, "type"    # Lcom/ibm/icu/util/ULocale$Type;

    .prologue
    .line 979
    sget-object v0, Lcom/ibm/icu/util/ULocale;->ACTUAL_LOCALE:Lcom/ibm/icu/util/ULocale$Type;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/Collator;->actualLocale:Lcom/ibm/icu/util/ULocale;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/Collator;->validLocale:Lcom/ibm/icu/util/ULocale;

    goto :goto_0
.end method

.method public abstract getRawCollationKey(Ljava/lang/String;Lcom/ibm/icu/text/RawCollationKey;)Lcom/ibm/icu/text/RawCollationKey;
.end method

.method public getStrength()I
    .locals 1

    .prologue
    .line 723
    iget v0, p0, Lcom/ibm/icu/text/Collator;->m_strength_:I

    return v0
.end method

.method public getTailoredSet()Lcom/ibm/icu/text/UnicodeSet;
    .locals 3

    .prologue
    .line 802
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    const/4 v1, 0x0

    const v2, 0x10ffff

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/UnicodeSet;-><init>(II)V

    return-object v0
.end method

.method public abstract getUCAVersion()Lcom/ibm/icu/util/VersionInfo;
.end method

.method public abstract getVariableTop()I
.end method

.method public abstract getVersion()Lcom/ibm/icu/util/VersionInfo;
.end method

.method public setDecomposition(I)V
    .locals 2
    .param p1, "decomposition"    # I

    .prologue
    .line 290
    const/16 v0, 0x10

    if-eq p1, v0, :cond_0

    const/16 v0, 0x11

    if-eq p1, v0, :cond_0

    .line 292
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Wrong decomposition mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 294
    :cond_0
    iput p1, p0, Lcom/ibm/icu/text/Collator;->m_decomposition_:I

    .line 295
    return-void
.end method

.method final setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V
    .locals 3
    .param p1, "valid"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "actual"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1002
    if-nez p1, :cond_0

    move v2, v0

    :goto_0
    if-nez p2, :cond_1

    :goto_1
    if-eq v2, v0, :cond_2

    .line 1004
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v2, v1

    .line 1002
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 1009
    :cond_2
    iput-object p1, p0, Lcom/ibm/icu/text/Collator;->validLocale:Lcom/ibm/icu/util/ULocale;

    .line 1010
    iput-object p2, p0, Lcom/ibm/icu/text/Collator;->actualLocale:Lcom/ibm/icu/util/ULocale;

    .line 1011
    return-void
.end method

.method public setStrength(I)V
    .locals 2
    .param p1, "newStrength"    # I

    .prologue
    .line 248
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/16 v0, 0xf

    if-eq p1, v0, :cond_0

    .line 253
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Incorrect comparison level."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 255
    :cond_0
    iput p1, p0, Lcom/ibm/icu/text/Collator;->m_strength_:I

    .line 256
    return-void
.end method

.method public abstract setVariableTop(Ljava/lang/String;)I
.end method

.method public abstract setVariableTop(I)V
.end method
