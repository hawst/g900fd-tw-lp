.class Lcom/ibm/icu/text/CollationRuleParser$Token;
.super Ljava/lang/Object;
.source "CollationRuleParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CollationRuleParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Token"
.end annotation


# instance fields
.field m_CELength_:I

.field m_CE_:[I

.field m_expCELength_:I

.field m_expCE_:[I

.field m_expansion_:I

.field m_flags_:C

.field m_listHeader_:Lcom/ibm/icu/text/CollationRuleParser$TokenListHeader;

.field m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

.field m_polarity_:I

.field m_prefix_:I

.field m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

.field m_rules_:Ljava/lang/StringBuffer;

.field m_source_:I

.field m_strength_:I

.field m_toInsert_:I


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x80

    const/4 v1, 0x0

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CE_:[I

    .line 161
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCE_:[I

    .line 163
    const/4 v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_polarity_:I

    .line 164
    iput-object v3, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_next_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 165
    iput-object v3, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_previous_:Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 166
    iput v1, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_CELength_:I

    .line 167
    iput v1, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_expCELength_:I

    .line 168
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 11
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    const v10, 0xffffff

    const/high16 v9, -0x1000000

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 199
    if-ne p1, p0, :cond_1

    .line 231
    :cond_0
    :goto_0
    return v6

    .line 202
    :cond_1
    instance-of v8, p1, Lcom/ibm/icu/text/CollationRuleParser$Token;

    if-eqz v8, :cond_6

    move-object v3, p1

    .line 203
    check-cast v3, Lcom/ibm/icu/text/CollationRuleParser$Token;

    .line 204
    .local v3, "t":Lcom/ibm/icu/text/CollationRuleParser$Token;
    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    and-int v2, v8, v10

    .line 205
    .local v2, "sstart":I
    iget v8, v3, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    and-int v5, v8, v10

    .line 206
    .local v5, "tstart":I
    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    and-int/2addr v8, v9

    shr-int/lit8 v1, v8, 0x18

    .line 207
    .local v1, "slimit":I
    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    and-int/2addr v8, v9

    shr-int/lit8 v4, v8, 0x18

    .line 209
    .local v4, "tlimit":I
    add-int v8, v2, v1

    add-int/lit8 v0, v8, -0x1

    .line 211
    .local v0, "end":I
    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    if-eqz v8, :cond_2

    iget v8, v3, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    if-nez v8, :cond_3

    :cond_2
    move v6, v7

    .line 212
    goto :goto_0

    .line 214
    :cond_3
    if-eq v1, v4, :cond_4

    move v6, v7

    .line 215
    goto :goto_0

    .line 217
    :cond_4
    iget v8, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    iget v9, v3, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    if-eq v8, v9, :cond_0

    .line 222
    :goto_1
    if-ge v2, v0, :cond_5

    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_rules_:Ljava/lang/StringBuffer;

    invoke-virtual {v8, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v8

    iget-object v9, v3, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_rules_:Ljava/lang/StringBuffer;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v9

    if-ne v8, v9, :cond_5

    .line 224
    add-int/lit8 v2, v2, 0x1

    .line 225
    add-int/lit8 v5, v5, 0x1

    .line 226
    goto :goto_1

    .line 227
    :cond_5
    iget-object v8, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_rules_:Ljava/lang/StringBuffer;

    invoke-virtual {v8, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v8

    iget-object v9, v3, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_rules_:Ljava/lang/StringBuffer;

    invoke-virtual {v9, v5}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v9

    if-eq v8, v9, :cond_0

    .end local v0    # "end":I
    .end local v1    # "slimit":I
    .end local v2    # "sstart":I
    .end local v3    # "t":Lcom/ibm/icu/text/CollationRuleParser$Token;
    .end local v4    # "tlimit":I
    .end local v5    # "tstart":I
    :cond_6
    move v6, v7

    .line 231
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 178
    const/4 v3, 0x0

    .line 179
    .local v3, "result":I
    iget v5, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    const/high16 v6, -0x1000000

    and-int/2addr v5, v6

    ushr-int/lit8 v1, v5, 0x18

    .line 180
    .local v1, "len":I
    add-int/lit8 v5, v1, -0x20

    div-int/lit8 v5, v5, 0x20

    add-int/lit8 v0, v5, 0x1

    .line 182
    .local v0, "inc":I
    iget v5, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_source_:I

    const v6, 0xffffff

    and-int v4, v5, v6

    .line 183
    .local v4, "start":I
    add-int v2, v4, v1

    .line 185
    .local v2, "limit":I
    :goto_0
    if-ge v4, v2, :cond_0

    .line 186
    mul-int/lit8 v5, v3, 0x25

    iget-object v6, p0, Lcom/ibm/icu/text/CollationRuleParser$Token;->m_rules_:Ljava/lang/StringBuffer;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v6

    add-int v3, v5, v6

    .line 187
    add-int/2addr v4, v0

    .line 188
    goto :goto_0

    .line 189
    :cond_0
    return v3
.end method
