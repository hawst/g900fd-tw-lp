.class Lcom/ibm/icu/text/CharsetRecog_UTF8;
.super Lcom/ibm/icu/text/CharsetRecognizer;
.source "CharsetRecog_UTF8.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/ibm/icu/text/CharsetRecognizer;-><init>()V

    return-void
.end method


# virtual methods
.method getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    const-string/jumbo v0, "UTF-8"

    return-object v0
.end method

.method match(Lcom/ibm/icu/text/CharsetDetector;)I
    .locals 14
    .param p1, "det"    # Lcom/ibm/icu/text/CharsetDetector;

    .prologue
    const/4 v13, 0x3

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 24
    const/4 v2, 0x0

    .line 25
    .local v2, "hasBOM":Z
    const/4 v6, 0x0

    .line 26
    .local v6, "numValid":I
    const/4 v5, 0x0

    .line 27
    .local v5, "numInvalid":I
    iget-object v4, p1, Lcom/ibm/icu/text/CharsetDetector;->fRawInput:[B

    .line 29
    .local v4, "input":[B
    const/4 v7, 0x0

    .line 32
    .local v7, "trailBytes":I
    iget v8, p1, Lcom/ibm/icu/text/CharsetDetector;->fRawLength:I

    if-lt v8, v13, :cond_0

    aget-byte v8, v4, v10

    and-int/lit16 v8, v8, 0xff

    const/16 v11, 0xef

    if-ne v8, v11, :cond_0

    aget-byte v8, v4, v9

    and-int/lit16 v8, v8, 0xff

    const/16 v11, 0xbb

    if-ne v8, v11, :cond_2

    move v8, v9

    :goto_0
    const/4 v11, 0x2

    aget-byte v11, v4, v11

    and-int/lit16 v11, v11, 0xff

    const/16 v12, 0xbf

    if-ne v11, v12, :cond_3

    :goto_1
    and-int/2addr v8, v9

    if-eqz v8, :cond_0

    .line 34
    const/4 v2, 0x1

    .line 38
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    iget v8, p1, Lcom/ibm/icu/text/CharsetDetector;->fRawLength:I

    if-ge v3, v8, :cond_9

    .line 39
    aget-byte v0, v4, v3

    .line 40
    .local v0, "b":I
    and-int/lit16 v8, v0, 0x80

    if-nez v8, :cond_4

    .line 38
    :cond_1
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v0    # "b":I
    .end local v3    # "i":I
    :cond_2
    move v8, v10

    .line 32
    goto :goto_0

    :cond_3
    move v9, v10

    goto :goto_1

    .line 45
    .restart local v0    # "b":I
    .restart local v3    # "i":I
    :cond_4
    and-int/lit16 v8, v0, 0xe0

    const/16 v9, 0xc0

    if-ne v8, v9, :cond_6

    .line 46
    const/4 v7, 0x1

    .line 61
    :cond_5
    :goto_4
    add-int/lit8 v3, v3, 0x1

    .line 62
    iget v8, p1, Lcom/ibm/icu/text/CharsetDetector;->fRawLength:I

    if-ge v3, v8, :cond_1

    .line 65
    aget-byte v0, v4, v3

    .line 66
    and-int/lit16 v8, v0, 0xc0

    const/16 v9, 0x80

    if-eq v8, v9, :cond_c

    .line 67
    add-int/lit8 v5, v5, 0x1

    .line 68
    goto :goto_3

    .line 47
    :cond_6
    and-int/lit16 v8, v0, 0xf0

    const/16 v9, 0xe0

    if-ne v8, v9, :cond_7

    .line 48
    const/4 v7, 0x2

    .line 49
    goto :goto_4

    :cond_7
    and-int/lit16 v8, v0, 0xf8

    const/16 v9, 0xf0

    if-ne v8, v9, :cond_8

    .line 50
    const/4 v7, 0x3

    .line 51
    goto :goto_4

    .line 52
    :cond_8
    add-int/lit8 v5, v5, 0x1

    .line 53
    const/4 v8, 0x5

    if-le v5, v8, :cond_b

    .line 80
    .end local v0    # "b":I
    :cond_9
    const/4 v1, 0x0

    .line 81
    .local v1, "confidence":I
    if-eqz v2, :cond_d

    if-nez v5, :cond_d

    .line 82
    const/16 v1, 0x64

    .line 96
    :cond_a
    :goto_5
    return v1

    .line 56
    .end local v1    # "confidence":I
    .restart local v0    # "b":I
    :cond_b
    const/4 v7, 0x0

    goto :goto_4

    .line 70
    :cond_c
    add-int/lit8 v7, v7, -0x1

    if-nez v7, :cond_5

    .line 71
    add-int/lit8 v6, v6, 0x1

    .line 72
    goto :goto_3

    .line 83
    .end local v0    # "b":I
    .restart local v1    # "confidence":I
    :cond_d
    if-eqz v2, :cond_e

    mul-int/lit8 v8, v5, 0xa

    if-le v6, v8, :cond_e

    .line 84
    const/16 v1, 0x50

    .line 85
    goto :goto_5

    :cond_e
    if-le v6, v13, :cond_f

    if-nez v5, :cond_f

    .line 86
    const/16 v1, 0x64

    .line 87
    goto :goto_5

    :cond_f
    if-lez v6, :cond_10

    if-nez v5, :cond_10

    .line 88
    const/16 v1, 0x50

    .line 89
    goto :goto_5

    :cond_10
    if-nez v6, :cond_11

    if-nez v5, :cond_11

    .line 91
    const/16 v1, 0xa

    .line 92
    goto :goto_5

    :cond_11
    mul-int/lit8 v8, v5, 0xa

    if-le v6, v8, :cond_a

    .line 94
    const/16 v1, 0x19

    goto :goto_5
.end method
