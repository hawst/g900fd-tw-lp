.class Lcom/ibm/icu/text/TransliteratorParser$ParseData;
.super Ljava/lang/Object;
.source "TransliteratorParser.java"

# interfaces
.implements Lcom/ibm/icu/text/SymbolTable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/TransliteratorParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ParseData"
.end annotation


# instance fields
.field private final this$0:Lcom/ibm/icu/text/TransliteratorParser;


# direct methods
.method private constructor <init>(Lcom/ibm/icu/text/TransliteratorParser;)V
    .locals 0

    .prologue
    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/ibm/icu/text/TransliteratorParser$ParseData;->this$0:Lcom/ibm/icu/text/TransliteratorParser;

    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/text/TransliteratorParser;Lcom/ibm/icu/text/TransliteratorParser$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/ibm/icu/text/TransliteratorParser;
    .param p2, "x1"    # Lcom/ibm/icu/text/TransliteratorParser$1;

    .prologue
    .line 195
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/TransliteratorParser$ParseData;-><init>(Lcom/ibm/icu/text/TransliteratorParser;)V

    return-void
.end method


# virtual methods
.method public isMatcher(I)Z
    .locals 2
    .param p1, "ch"    # I

    .prologue
    .line 246
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser$ParseData;->this$0:Lcom/ibm/icu/text/TransliteratorParser;

    invoke-static {v1}, Lcom/ibm/icu/text/TransliteratorParser;->access$100(Lcom/ibm/icu/text/TransliteratorParser;)Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    move-result-object v1

    iget-char v1, v1, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variablesBase:C

    sub-int v0, p1, v1

    .line 247
    .local v0, "i":I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser$ParseData;->this$0:Lcom/ibm/icu/text/TransliteratorParser;

    invoke-static {v1}, Lcom/ibm/icu/text/TransliteratorParser;->access$200(Lcom/ibm/icu/text/TransliteratorParser;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 248
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser$ParseData;->this$0:Lcom/ibm/icu/text/TransliteratorParser;

    invoke-static {v1}, Lcom/ibm/icu/text/TransliteratorParser;->access$200(Lcom/ibm/icu/text/TransliteratorParser;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/ibm/icu/text/UnicodeMatcher;

    .line 250
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isReplacer(I)Z
    .locals 2
    .param p1, "ch"    # I

    .prologue
    .line 260
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser$ParseData;->this$0:Lcom/ibm/icu/text/TransliteratorParser;

    invoke-static {v1}, Lcom/ibm/icu/text/TransliteratorParser;->access$100(Lcom/ibm/icu/text/TransliteratorParser;)Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    move-result-object v1

    iget-char v1, v1, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variablesBase:C

    sub-int v0, p1, v1

    .line 261
    .local v0, "i":I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser$ParseData;->this$0:Lcom/ibm/icu/text/TransliteratorParser;

    invoke-static {v1}, Lcom/ibm/icu/text/TransliteratorParser;->access$200(Lcom/ibm/icu/text/TransliteratorParser;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 262
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser$ParseData;->this$0:Lcom/ibm/icu/text/TransliteratorParser;

    invoke-static {v1}, Lcom/ibm/icu/text/TransliteratorParser;->access$200(Lcom/ibm/icu/text/TransliteratorParser;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/ibm/icu/text/UnicodeReplacer;

    .line 264
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public lookup(Ljava/lang/String;)[C
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorParser$ParseData;->this$0:Lcom/ibm/icu/text/TransliteratorParser;

    invoke-static {v0}, Lcom/ibm/icu/text/TransliteratorParser;->access$000(Lcom/ibm/icu/text/TransliteratorParser;)Ljava/util/Hashtable;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [C

    check-cast v0, [C

    return-object v0
.end method

.method public lookupMatcher(I)Lcom/ibm/icu/text/UnicodeMatcher;
    .locals 2
    .param p1, "ch"    # I

    .prologue
    .line 210
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser$ParseData;->this$0:Lcom/ibm/icu/text/TransliteratorParser;

    invoke-static {v1}, Lcom/ibm/icu/text/TransliteratorParser;->access$100(Lcom/ibm/icu/text/TransliteratorParser;)Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    move-result-object v1

    iget-char v1, v1, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variablesBase:C

    sub-int v0, p1, v1

    .line 211
    .local v0, "i":I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser$ParseData;->this$0:Lcom/ibm/icu/text/TransliteratorParser;

    invoke-static {v1}, Lcom/ibm/icu/text/TransliteratorParser;->access$200(Lcom/ibm/icu/text/TransliteratorParser;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 212
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorParser$ParseData;->this$0:Lcom/ibm/icu/text/TransliteratorParser;

    invoke-static {v1}, Lcom/ibm/icu/text/TransliteratorParser;->access$200(Lcom/ibm/icu/text/TransliteratorParser;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/UnicodeMatcher;

    .line 214
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public parseReference(Ljava/lang/String;Ljava/text/ParsePosition;I)Ljava/lang/String;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "pos"    # Ljava/text/ParsePosition;
    .param p3, "limit"    # I

    .prologue
    .line 222
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v2

    .line 223
    .local v2, "start":I
    move v1, v2

    .line 224
    .local v1, "i":I
    :goto_0
    if-ge v1, p3, :cond_1

    .line 225
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 226
    .local v0, "c":C
    if-ne v1, v2, :cond_0

    invoke-static {v0}, Ljava/lang/Character;->isUnicodeIdentifierStart(C)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    invoke-static {v0}, Ljava/lang/Character;->isUnicodeIdentifierPart(C)Z

    move-result v3

    if-nez v3, :cond_2

    .line 232
    .end local v0    # "c":C
    :cond_1
    if-ne v1, v2, :cond_3

    .line 233
    const/4 v3, 0x0

    .line 236
    :goto_1
    return-object v3

    .line 230
    .restart local v0    # "c":C
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 231
    goto :goto_0

    .line 235
    .end local v0    # "c":C
    :cond_3
    invoke-virtual {p2, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 236
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method
