.class Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
.super Ljava/lang/Object;
.source "RBBISetBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/RBBISetBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RangeDescriptor"
.end annotation


# instance fields
.field fEndChar:I

.field fIncludesSets:Ljava/util/List;

.field fNext:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

.field fNum:I

.field fStartChar:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fIncludesSets:Ljava/util/List;

    .line 50
    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;)V
    .locals 2
    .param p1, "other"    # Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iget v0, p1, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fStartChar:I

    iput v0, p0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fStartChar:I

    .line 54
    iget v0, p1, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fEndChar:I

    iput v0, p0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fEndChar:I

    .line 55
    iget v0, p1, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    iput v0, p0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fIncludesSets:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fIncludesSets:Ljava/util/List;

    .line 57
    return-void
.end method


# virtual methods
.method setDictionaryFlag()V
    .locals 7

    .prologue
    .line 102
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v5, p0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fIncludesSets:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 103
    iget-object v5, p0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fIncludesSets:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/text/RBBINode;

    .line 104
    .local v3, "usetNode":Lcom/ibm/icu/text/RBBINode;
    const-string/jumbo v1, ""

    .line 105
    .local v1, "setName":Ljava/lang/String;
    iget-object v2, v3, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 106
    .local v2, "setRef":Lcom/ibm/icu/text/RBBINode;
    if-eqz v2, :cond_0

    .line 107
    iget-object v4, v2, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 108
    .local v4, "varRef":Lcom/ibm/icu/text/RBBINode;
    if-eqz v4, :cond_0

    iget v5, v4, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 109
    iget-object v1, v4, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    .line 112
    .end local v4    # "varRef":Lcom/ibm/icu/text/RBBINode;
    :cond_0
    const-string/jumbo v5, "dictionary"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 113
    iget v5, p0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    or-int/lit16 v5, v5, 0x4000

    iput v5, p0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNum:I

    .line 118
    .end local v1    # "setName":Ljava/lang/String;
    .end local v2    # "setRef":Lcom/ibm/icu/text/RBBINode;
    .end local v3    # "usetNode":Lcom/ibm/icu/text/RBBINode;
    :cond_1
    return-void

    .line 102
    .restart local v1    # "setName":Ljava/lang/String;
    .restart local v2    # "setRef":Lcom/ibm/icu/text/RBBINode;
    .restart local v3    # "usetNode":Lcom/ibm/icu/text/RBBINode;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method split(I)V
    .locals 2
    .param p1, "where"    # I

    .prologue
    .line 65
    iget v1, p0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fStartChar:I

    if-le p1, v1, :cond_0

    iget v1, p0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fEndChar:I

    if-gt p1, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 66
    new-instance v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;-><init>(Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;)V

    .line 70
    .local v0, "nr":Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
    iput p1, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fStartChar:I

    .line 71
    add-int/lit8 v1, p1, -0x1

    iput v1, p0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fEndChar:I

    .line 72
    iget-object v1, p0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNext:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    iput-object v1, v0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNext:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    .line 73
    iput-object v0, p0, Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;->fNext:Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;

    .line 78
    return-void

    .line 65
    .end local v0    # "nr":Lcom/ibm/icu/text/RBBISetBuilder$RangeDescriptor;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
