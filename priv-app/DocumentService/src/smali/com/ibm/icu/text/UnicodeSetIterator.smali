.class public Lcom/ibm/icu/text/UnicodeSetIterator;
.super Ljava/lang/Object;
.source "UnicodeSetIterator.java"


# static fields
.field public static IS_STRING:I


# instance fields
.field public codepoint:I

.field public codepointEnd:I

.field protected endElement:I

.field private endRange:I

.field protected nextElement:I

.field private range:I

.field private set:Lcom/ibm/icu/text/UnicodeSet;

.field public string:Ljava/lang/String;

.field private stringIterator:Ljava/util/Iterator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, -0x1

    sput v0, Lcom/ibm/icu/text/UnicodeSetIterator;->IS_STRING:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->endRange:I

    .line 223
    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->range:I

    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->stringIterator:Ljava/util/Iterator;

    .line 90
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeSetIterator;->reset(Lcom/ibm/icu/text/UnicodeSet;)V

    .line 91
    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 1
    .param p1, "set"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->endRange:I

    .line 223
    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->range:I

    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->stringIterator:Ljava/util/Iterator;

    .line 80
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/UnicodeSetIterator;->reset(Lcom/ibm/icu/text/UnicodeSet;)V

    .line 81
    return-void
.end method


# virtual methods
.method public getString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 213
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->codepoint:I

    sget v1, Lcom/ibm/icu/text/UnicodeSetIterator;->IS_STRING:I

    if-eq v0, v1, :cond_0

    .line 214
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->codepoint:I

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 216
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->string:Ljava/lang/String;

    goto :goto_0
.end method

.method protected loadRange(I)V
    .locals 1
    .param p1, "aRange"    # I

    .prologue
    .line 245
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->set:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/UnicodeSet;->getRangeStart(I)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->nextElement:I

    .line 246
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->set:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/UnicodeSet;->getRangeEnd(I)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->endElement:I

    .line 247
    return-void
.end method

.method public next()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 113
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->nextElement:I

    iget v2, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->endElement:I

    if-gt v0, v2, :cond_0

    .line 114
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->nextElement:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->nextElement:I

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->codepointEnd:I

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->codepoint:I

    move v0, v1

    .line 129
    :goto_0
    return v0

    .line 117
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->range:I

    iget v2, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->endRange:I

    if-ge v0, v2, :cond_1

    .line 118
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->range:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->range:I

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeSetIterator;->loadRange(I)V

    .line 119
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->nextElement:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->nextElement:I

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->codepointEnd:I

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->codepoint:I

    move v0, v1

    .line 120
    goto :goto_0

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->stringIterator:Ljava/util/Iterator;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    .line 126
    :cond_2
    sget v0, Lcom/ibm/icu/text/UnicodeSetIterator;->IS_STRING:I

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->codepoint:I

    .line 127
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->stringIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->string:Ljava/lang/String;

    .line 128
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->stringIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->stringIterator:Ljava/util/Iterator;

    :cond_3
    move v0, v1

    .line 129
    goto :goto_0
.end method

.method public nextRange()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 153
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->nextElement:I

    iget v2, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->endElement:I

    if-gt v0, v2, :cond_0

    .line 154
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->endElement:I

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->codepointEnd:I

    .line 155
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->nextElement:I

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->codepoint:I

    .line 156
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->endElement:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->nextElement:I

    move v0, v1

    .line 173
    :goto_0
    return v0

    .line 159
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->range:I

    iget v2, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->endRange:I

    if-ge v0, v2, :cond_1

    .line 160
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->range:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->range:I

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeSetIterator;->loadRange(I)V

    .line 161
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->endElement:I

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->codepointEnd:I

    .line 162
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->nextElement:I

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->codepoint:I

    .line 163
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->endElement:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->nextElement:I

    move v0, v1

    .line 164
    goto :goto_0

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->stringIterator:Ljava/util/Iterator;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    .line 170
    :cond_2
    sget v0, Lcom/ibm/icu/text/UnicodeSetIterator;->IS_STRING:I

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->codepoint:I

    .line 171
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->stringIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->string:Ljava/lang/String;

    .line 172
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->stringIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->stringIterator:Ljava/util/Iterator;

    :cond_3
    move v0, v1

    .line 173
    goto :goto_0
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 193
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->set:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UnicodeSet;->getRangeCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->endRange:I

    .line 194
    iput v1, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->range:I

    .line 195
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->endElement:I

    .line 196
    iput v1, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->nextElement:I

    .line 197
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->endRange:I

    if-ltz v0, :cond_0

    .line 198
    iget v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->range:I

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeSetIterator;->loadRange(I)V

    .line 200
    :cond_0
    iput-object v2, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->stringIterator:Ljava/util/Iterator;

    .line 201
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->set:Lcom/ibm/icu/text/UnicodeSet;

    iget-object v0, v0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->set:Lcom/ibm/icu/text/UnicodeSet;

    iget-object v0, v0, Lcom/ibm/icu/text/UnicodeSet;->strings:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->stringIterator:Ljava/util/Iterator;

    .line 203
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->stringIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    iput-object v2, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->stringIterator:Ljava/util/Iterator;

    .line 205
    :cond_1
    return-void
.end method

.method public reset(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 0
    .param p1, "uset"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/ibm/icu/text/UnicodeSetIterator;->set:Lcom/ibm/icu/text/UnicodeSet;

    .line 185
    invoke-virtual {p0}, Lcom/ibm/icu/text/UnicodeSetIterator;->reset()V

    .line 186
    return-void
.end method
