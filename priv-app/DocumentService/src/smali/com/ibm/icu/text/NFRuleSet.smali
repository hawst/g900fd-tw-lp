.class final Lcom/ibm/icu/text/NFRuleSet;
.super Ljava/lang/Object;
.source "NFRuleSet.java"


# static fields
.field private static final RECURSION_LIMIT:I = 0x32


# instance fields
.field private fractionRules:[Lcom/ibm/icu/text/NFRule;

.field private isFractionRuleSet:Z

.field private name:Ljava/lang/String;

.field private negativeNumberRule:Lcom/ibm/icu/text/NFRule;

.field private recursionCount:I

.field private rules:[Lcom/ibm/icu/text/NFRule;


# direct methods
.method public constructor <init>([Ljava/lang/String;I)V
    .locals 5
    .param p1, "descriptions"    # [Ljava/lang/String;
    .param p2, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/ibm/icu/text/NFRuleSet;->negativeNumberRule:Lcom/ibm/icu/text/NFRule;

    .line 48
    const/4 v2, 0x3

    new-array v2, v2, [Lcom/ibm/icu/text/NFRule;

    iput-object v2, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    .line 58
    iput-boolean v4, p0, Lcom/ibm/icu/text/NFRuleSet;->isFractionRuleSet:Z

    .line 63
    iput v4, p0, Lcom/ibm/icu/text/NFRuleSet;->recursionCount:I

    .line 83
    aget-object v0, p1, p2

    .line 85
    .local v0, "description":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 86
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Empty rule set description"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 93
    :cond_0
    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x25

    if-ne v2, v3, :cond_4

    .line 94
    const/16 v2, 0x3a

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 95
    .local v1, "pos":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 96
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Rule set name doesn\'t end in colon"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 98
    :cond_1
    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/ibm/icu/text/NFRuleSet;->name:Ljava/lang/String;

    .line 99
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_3

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 102
    :cond_3
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 103
    aput-object v0, p1, p2

    .line 112
    .end local v1    # "pos":I
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    .line 113
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Empty rule set description"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 109
    :cond_4
    const-string/jumbo v2, "%default"

    iput-object v2, p0, Lcom/ibm/icu/text/NFRuleSet;->name:Ljava/lang/String;

    goto :goto_0

    .line 118
    :cond_5
    return-void
.end method

.method private findFractionRuleSetRule(D)Lcom/ibm/icu/text/NFRule;
    .locals 15
    .param p1, "number"    # D

    .prologue
    .line 575
    iget-object v10, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    const/4 v11, 0x0

    aget-object v10, v10, v11

    invoke-virtual {v10}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v4

    .line 576
    .local v4, "leastCommonMultiple":J
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    iget-object v10, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    array-length v10, v10

    if-ge v2, v10, :cond_0

    .line 577
    iget-object v10, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    aget-object v10, v10, v2

    invoke-virtual {v10}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v10

    invoke-static {v4, v5, v10, v11}, Lcom/ibm/icu/text/NFRuleSet;->lcm(JJ)J

    move-result-wide v4

    .line 576
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 579
    :cond_0
    long-to-double v10, v4

    mul-double v10, v10, p1

    invoke-static {v10, v11}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    .line 583
    .local v6, "numerator":J
    const-wide v0, 0x7fffffffffffffffL

    .line 584
    .local v0, "difference":J
    const/4 v3, 0x0

    .line 585
    .local v3, "winner":I
    const/4 v2, 0x0

    :goto_1
    iget-object v10, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    array-length v10, v10

    if-ge v2, v10, :cond_2

    .line 592
    iget-object v10, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    aget-object v10, v10, v2

    invoke-virtual {v10}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v10

    mul-long/2addr v10, v6

    rem-long v8, v10, v4

    .line 597
    .local v8, "tempDifference":J
    sub-long v10, v4, v8

    cmp-long v10, v10, v8

    if-gez v10, :cond_1

    .line 598
    sub-long v8, v4, v8

    .line 605
    :cond_1
    cmp-long v10, v8, v0

    if-gez v10, :cond_5

    .line 606
    move-wide v0, v8

    .line 607
    move v3, v2

    .line 608
    const-wide/16 v10, 0x0

    cmp-long v10, v0, v10

    if-nez v10, :cond_5

    .line 620
    .end local v8    # "tempDifference":J
    :cond_2
    add-int/lit8 v10, v3, 0x1

    iget-object v11, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    array-length v11, v11

    if-ge v10, v11, :cond_4

    iget-object v10, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    add-int/lit8 v11, v3, 0x1

    aget-object v10, v10, v11

    invoke-virtual {v10}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v10

    iget-object v12, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    aget-object v12, v12, v3

    invoke-virtual {v12}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-nez v10, :cond_4

    .line 622
    iget-object v10, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    aget-object v10, v10, v3

    invoke-virtual {v10}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v10

    long-to-double v10, v10

    mul-double v10, v10, p1

    invoke-static {v10, v11}, Ljava/lang/Math;->round(D)J

    move-result-wide v10

    const-wide/16 v12, 0x1

    cmp-long v10, v10, v12

    if-ltz v10, :cond_3

    iget-object v10, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    aget-object v10, v10, v3

    invoke-virtual {v10}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v10

    long-to-double v10, v10

    mul-double v10, v10, p1

    invoke-static {v10, v11}, Ljava/lang/Math;->round(D)J

    move-result-wide v10

    const-wide/16 v12, 0x2

    cmp-long v10, v10, v12

    if-ltz v10, :cond_4

    .line 624
    :cond_3
    add-int/lit8 v3, v3, 0x1

    .line 629
    :cond_4
    iget-object v10, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    aget-object v10, v10, v3

    return-object v10

    .line 585
    .restart local v8    # "tempDifference":J
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private findNormalRule(J)Lcom/ibm/icu/text/NFRule;
    .locals 7
    .param p1, "number"    # J

    .prologue
    .line 486
    iget-boolean v4, p0, Lcom/ibm/icu/text/NFRuleSet;->isFractionRuleSet:Z

    if-eqz v4, :cond_1

    .line 487
    long-to-double v4, p1

    invoke-direct {p0, v4, v5}, Lcom/ibm/icu/text/NFRuleSet;->findFractionRuleSetRule(D)Lcom/ibm/icu/text/NFRule;

    move-result-object v3

    .line 547
    :cond_0
    :goto_0
    return-object v3

    .line 492
    :cond_1
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-gez v4, :cond_3

    .line 493
    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->negativeNumberRule:Lcom/ibm/icu/text/NFRule;

    if-eqz v4, :cond_2

    .line 494
    iget-object v3, p0, Lcom/ibm/icu/text/NFRuleSet;->negativeNumberRule:Lcom/ibm/icu/text/NFRule;

    goto :goto_0

    .line 496
    :cond_2
    neg-long p1, p1

    .line 512
    :cond_3
    const/4 v1, 0x0

    .line 513
    .local v1, "lo":I
    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    array-length v0, v4

    .line 514
    .local v0, "hi":I
    if-lez v0, :cond_9

    .line 515
    :goto_1
    if-ge v1, v0, :cond_6

    .line 516
    add-int v4, v1, v0

    div-int/lit8 v2, v4, 0x2

    .line 517
    .local v2, "mid":I
    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-nez v4, :cond_4

    .line 518
    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    aget-object v3, v4, v2

    goto :goto_0

    .line 520
    :cond_4
    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-lez v4, :cond_5

    .line 521
    move v0, v2

    .line 522
    goto :goto_1

    .line 524
    :cond_5
    add-int/lit8 v1, v2, 0x1

    goto :goto_1

    .line 527
    .end local v2    # "mid":I
    :cond_6
    if-nez v0, :cond_7

    .line 528
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "The rule set "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lcom/ibm/icu/text/NFRuleSet;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, " cannot format the value "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 530
    :cond_7
    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    add-int/lit8 v5, v0, -0x1

    aget-object v3, v4, v5

    .line 537
    .local v3, "result":Lcom/ibm/icu/text/NFRule;
    long-to-double v4, p1

    invoke-virtual {v3, v4, v5}, Lcom/ibm/icu/text/NFRule;->shouldRollBack(D)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 538
    const/4 v4, 0x1

    if-ne v0, v4, :cond_8

    .line 539
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "The rule set "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lcom/ibm/icu/text/NFRuleSet;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, " cannot roll back from the rule \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 542
    :cond_8
    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    add-int/lit8 v5, v0, -0x2

    aget-object v3, v4, v5

    goto/16 :goto_0

    .line 547
    .end local v3    # "result":Lcom/ibm/icu/text/NFRule;
    :cond_9
    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    const/4 v5, 0x2

    aget-object v3, v4, v5

    goto/16 :goto_0
.end method

.method private findRule(D)Lcom/ibm/icu/text/NFRule;
    .locals 5
    .param p1, "number"    # D

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 426
    iget-boolean v0, p0, Lcom/ibm/icu/text/NFRuleSet;->isFractionRuleSet:Z

    if-eqz v0, :cond_0

    .line 427
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/NFRuleSet;->findFractionRuleSetRule(D)Lcom/ibm/icu/text/NFRule;

    move-result-object v0

    .line 462
    :goto_0
    return-object v0

    .line 433
    :cond_0
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_2

    .line 434
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->negativeNumberRule:Lcom/ibm/icu/text/NFRule;

    if-eqz v0, :cond_1

    .line 435
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->negativeNumberRule:Lcom/ibm/icu/text/NFRule;

    goto :goto_0

    .line 437
    :cond_1
    neg-double p1, p1

    .line 442
    :cond_2
    invoke-static {p1, p2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    cmpl-double v0, p1, v0

    if-eqz v0, :cond_4

    .line 445
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_3

    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v0, v0, v3

    if-eqz v0, :cond_3

    .line 446
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v0, v0, v3

    goto :goto_0

    .line 450
    :cond_3
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v0, v0, v2

    if-eqz v0, :cond_4

    .line 451
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v0, v0, v2

    goto :goto_0

    .line 456
    :cond_4
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v0, v0, v4

    if-eqz v0, :cond_5

    .line 457
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v0, v0, v4

    goto :goto_0

    .line 462
    :cond_5
    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/NFRuleSet;->findNormalRule(J)Lcom/ibm/icu/text/NFRule;

    move-result-object v0

    goto :goto_0
.end method

.method private static lcm(JJ)J
    .locals 14
    .param p0, "x"    # J
    .param p2, "y"    # J

    .prologue
    .line 638
    move-wide v6, p0

    .line 639
    .local v6, "x1":J
    move-wide/from16 v8, p2

    .line 641
    .local v8, "y1":J
    const/4 v2, 0x0

    .line 642
    .local v2, "p2":I
    :goto_0
    const-wide/16 v10, 0x1

    and-long/2addr v10, v6

    const-wide/16 v12, 0x0

    cmp-long v3, v10, v12

    if-nez v3, :cond_0

    const-wide/16 v10, 0x1

    and-long/2addr v10, v8

    const-wide/16 v12, 0x0

    cmp-long v3, v10, v12

    if-nez v3, :cond_0

    .line 643
    add-int/lit8 v2, v2, 0x1

    .line 644
    const/4 v3, 0x1

    shr-long/2addr v6, v3

    .line 645
    const/4 v3, 0x1

    shr-long/2addr v8, v3

    .line 646
    goto :goto_0

    .line 649
    :cond_0
    const-wide/16 v10, 0x1

    and-long/2addr v10, v6

    const-wide/16 v12, 0x1

    cmp-long v3, v10, v12

    if-nez v3, :cond_1

    .line 650
    neg-long v4, v8

    .line 655
    .local v4, "t":J
    :goto_1
    const-wide/16 v10, 0x0

    cmp-long v3, v4, v10

    if-eqz v3, :cond_4

    .line 656
    :goto_2
    const-wide/16 v10, 0x1

    and-long/2addr v10, v4

    const-wide/16 v12, 0x0

    cmp-long v3, v10, v12

    if-nez v3, :cond_2

    .line 657
    const/4 v3, 0x1

    shr-long/2addr v4, v3

    .line 658
    goto :goto_2

    .line 652
    .end local v4    # "t":J
    :cond_1
    move-wide v4, v6

    .restart local v4    # "t":J
    goto :goto_1

    .line 659
    :cond_2
    const-wide/16 v10, 0x0

    cmp-long v3, v4, v10

    if-lez v3, :cond_3

    .line 660
    move-wide v6, v4

    .line 664
    :goto_3
    sub-long v4, v6, v8

    .line 665
    goto :goto_1

    .line 662
    :cond_3
    neg-long v8, v4

    goto :goto_3

    .line 666
    :cond_4
    shl-long v0, v6, v2

    .line 669
    .local v0, "gcd":J
    div-long v10, p0, v0

    mul-long v10, v10, p2

    return-wide v10
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "that"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 285
    instance-of v4, p1, Lcom/ibm/icu/text/NFRuleSet;

    if-nez v4, :cond_1

    .line 310
    :cond_0
    :goto_0
    return v2

    :cond_1
    move-object v1, p1

    .line 289
    check-cast v1, Lcom/ibm/icu/text/NFRuleSet;

    .line 291
    .local v1, "that2":Lcom/ibm/icu/text/NFRuleSet;
    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->name:Ljava/lang/String;

    iget-object v5, v1, Lcom/ibm/icu/text/NFRuleSet;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->negativeNumberRule:Lcom/ibm/icu/text/NFRule;

    iget-object v5, v1, Lcom/ibm/icu/text/NFRuleSet;->negativeNumberRule:Lcom/ibm/icu/text/NFRule;

    invoke-static {v4, v5}, Lcom/ibm/icu/impl/Utility;->objectEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v4, v4, v2

    iget-object v5, v1, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v5, v5, v2

    invoke-static {v4, v5}, Lcom/ibm/icu/impl/Utility;->objectEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v4, v4, v3

    iget-object v5, v1, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v5, v5, v3

    invoke-static {v4, v5}, Lcom/ibm/icu/impl/Utility;->objectEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v4, v4, v6

    iget-object v5, v1, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v5, v5, v6

    invoke-static {v4, v5}, Lcom/ibm/icu/impl/Utility;->objectEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    array-length v4, v4

    iget-object v5, v1, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    array-length v5, v5

    if-ne v4, v5, :cond_0

    iget-boolean v4, p0, Lcom/ibm/icu/text/NFRuleSet;->isFractionRuleSet:Z

    iget-boolean v5, v1, Lcom/ibm/icu/text/NFRuleSet;->isFractionRuleSet:Z

    if-ne v4, v5, :cond_0

    .line 303
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    array-length v4, v4

    if-ge v0, v4, :cond_2

    .line 304
    iget-object v4, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    aget-object v4, v4, v0

    iget-object v5, v1, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Lcom/ibm/icu/text/NFRule;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 303
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v2, v3

    .line 310
    goto :goto_0
.end method

.method public format(DLjava/lang/StringBuffer;I)V
    .locals 5
    .param p1, "number"    # D
    .param p3, "toInsertInto"    # Ljava/lang/StringBuffer;
    .param p4, "pos"    # I

    .prologue
    .line 409
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/NFRuleSet;->findRule(D)Lcom/ibm/icu/text/NFRule;

    move-result-object v0

    .line 411
    .local v0, "applicableRule":Lcom/ibm/icu/text/NFRule;
    iget v1, p0, Lcom/ibm/icu/text/NFRuleSet;->recursionCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/NFRuleSet;->recursionCount:I

    const/16 v2, 0x32

    if-lt v1, v2, :cond_0

    .line 412
    const/4 v1, 0x0

    iput v1, p0, Lcom/ibm/icu/text/NFRuleSet;->recursionCount:I

    .line 413
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Recursion limit exceeded when applying ruleSet "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/ibm/icu/text/NFRuleSet;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 415
    :cond_0
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/ibm/icu/text/NFRule;->doFormat(DLjava/lang/StringBuffer;I)V

    .line 416
    iget v1, p0, Lcom/ibm/icu/text/NFRuleSet;->recursionCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/ibm/icu/text/NFRuleSet;->recursionCount:I

    .line 417
    return-void
.end method

.method public format(JLjava/lang/StringBuffer;I)V
    .locals 5
    .param p1, "number"    # J
    .param p3, "toInsertInto"    # Ljava/lang/StringBuffer;
    .param p4, "pos"    # I

    .prologue
    .line 390
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/NFRuleSet;->findNormalRule(J)Lcom/ibm/icu/text/NFRule;

    move-result-object v0

    .line 392
    .local v0, "applicableRule":Lcom/ibm/icu/text/NFRule;
    iget v1, p0, Lcom/ibm/icu/text/NFRuleSet;->recursionCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/NFRuleSet;->recursionCount:I

    const/16 v2, 0x32

    if-lt v1, v2, :cond_0

    .line 393
    const/4 v1, 0x0

    iput v1, p0, Lcom/ibm/icu/text/NFRuleSet;->recursionCount:I

    .line 394
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Recursion limit exceeded when applying ruleSet "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/ibm/icu/text/NFRuleSet;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 396
    :cond_0
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/ibm/icu/text/NFRule;->doFormat(JLjava/lang/StringBuffer;I)V

    .line 397
    iget v1, p0, Lcom/ibm/icu/text/NFRuleSet;->recursionCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/ibm/icu/text/NFRuleSet;->recursionCount:I

    .line 398
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->name:Ljava/lang/String;

    return-object v0
.end method

.method public isFractionSet()Z
    .locals 1

    .prologue
    .line 358
    iget-boolean v0, p0, Lcom/ibm/icu/text/NFRuleSet;->isFractionRuleSet:Z

    return v0
.end method

.method public isPublic()Z
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->name:Ljava/lang/String;

    const-string/jumbo v1, "%%"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public makeIntoFractionRuleSet()V
    .locals 1

    .prologue
    .line 271
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/NFRuleSet;->isFractionRuleSet:Z

    .line 272
    return-void
.end method

.method public parse(Ljava/lang/String;Ljava/text/ParsePosition;D)Ljava/lang/Number;
    .locals 11
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "parsePosition"    # Ljava/text/ParsePosition;
    .param p3, "upperBound"    # D

    .prologue
    .line 702
    new-instance v6, Ljava/text/ParsePosition;

    const/4 v0, 0x0

    invoke-direct {v6, v0}, Ljava/text/ParsePosition;-><init>(I)V

    .line 703
    .local v6, "highWaterMark":Ljava/text/ParsePosition;
    new-instance v8, Ljava/lang/Long;

    const-wide/16 v0, 0x0

    invoke-direct {v8, v0, v1}, Ljava/lang/Long;-><init>(J)V

    .line 704
    .local v8, "result":Ljava/lang/Number;
    const/4 v10, 0x0

    .line 707
    .local v10, "tempResult":Ljava/lang/Number;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    move-object v9, v8

    .line 776
    .end local v8    # "result":Ljava/lang/Number;
    .local v9, "result":Ljava/lang/Object;
    :goto_0
    return-object v9

    .line 712
    .end local v9    # "result":Ljava/lang/Object;
    .restart local v8    # "result":Ljava/lang/Number;
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->negativeNumberRule:Lcom/ibm/icu/text/NFRule;

    if-eqz v0, :cond_2

    .line 713
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->negativeNumberRule:Lcom/ibm/icu/text/NFRule;

    const/4 v3, 0x0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/text/NFRule;->doParse(Ljava/lang/String;Ljava/text/ParsePosition;ZD)Ljava/lang/Number;

    move-result-object v10

    .line 714
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v0

    invoke-virtual {v6}, Ljava/text/ParsePosition;->getIndex()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 715
    move-object v8, v10

    .line 716
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v0

    invoke-virtual {v6, v0}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 722
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 726
    :cond_2
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    const/4 v0, 0x3

    if-ge v7, v0, :cond_5

    .line 727
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v0, v0, v7

    if-eqz v0, :cond_4

    .line 728
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v0, v0, v7

    const/4 v3, 0x0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/text/NFRule;->doParse(Ljava/lang/String;Ljava/text/ParsePosition;ZD)Ljava/lang/Number;

    move-result-object v10

    .line 729
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v0

    invoke-virtual {v6}, Ljava/text/ParsePosition;->getIndex()I

    move-result v1

    if-le v0, v1, :cond_3

    .line 730
    move-object v8, v10

    .line 731
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v0

    invoke-virtual {v6, v0}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 737
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 726
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 750
    :cond_5
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    array-length v0, v0

    add-int/lit8 v7, v0, -0x1

    :goto_2
    if-ltz v7, :cond_8

    invoke-virtual {v6}, Ljava/text/ParsePosition;->getIndex()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_8

    .line 751
    iget-boolean v0, p0, Lcom/ibm/icu/text/NFRuleSet;->isFractionRuleSet:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v0

    long-to-double v0, v0

    cmpl-double v0, v0, p3

    if-ltz v0, :cond_6

    .line 750
    :goto_3
    add-int/lit8 v7, v7, -0x1

    goto :goto_2

    .line 755
    :cond_6
    iget-object v0, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    aget-object v0, v0, v7

    iget-boolean v3, p0, Lcom/ibm/icu/text/NFRuleSet;->isFractionRuleSet:Z

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/text/NFRule;->doParse(Ljava/lang/String;Ljava/text/ParsePosition;ZD)Ljava/lang/Number;

    move-result-object v10

    .line 756
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v0

    invoke-virtual {v6}, Ljava/text/ParsePosition;->getIndex()I

    move-result v1

    if-le v0, v1, :cond_7

    .line 757
    move-object v8, v10

    .line 758
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v0

    invoke-virtual {v6, v0}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 764
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/text/ParsePosition;->setIndex(I)V

    goto :goto_3

    .line 770
    :cond_8
    invoke-virtual {v6}, Ljava/text/ParsePosition;->getIndex()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/text/ParsePosition;->setIndex(I)V

    move-object v9, v8

    .line 776
    .restart local v9    # "result":Ljava/lang/Object;
    goto/16 :goto_0
.end method

.method public parseRules(Ljava/lang/String;Lcom/ibm/icu/text/RuleBasedNumberFormat;)V
    .locals 18
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "owner"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;

    .prologue
    .line 136
    new-instance v10, Ljava/util/Vector;

    invoke-direct {v10}, Ljava/util/Vector;-><init>()V

    .line 138
    .local v10, "ruleDescriptions":Ljava/util/Vector;
    const/4 v6, 0x0

    .line 139
    .local v6, "oldP":I
    const/16 v14, 0x3b

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    .line 140
    .local v7, "p":I
    :goto_0
    const/4 v14, -0x1

    if-eq v6, v14, :cond_2

    .line 141
    const/4 v14, -0x1

    if-eq v7, v14, :cond_0

    .line 142
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 143
    add-int/lit8 v6, v7, 0x1

    .line 150
    :goto_1
    const/16 v14, 0x3b

    add-int/lit8 v15, v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Ljava/lang/String;->indexOf(II)I

    move-result v7

    .line 151
    goto :goto_0

    .line 145
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v14

    if-ge v6, v14, :cond_1

    .line 146
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 148
    :cond_1
    move v6, v7

    goto :goto_1

    .line 156
    :cond_2
    new-instance v13, Ljava/util/Vector;

    invoke-direct {v13}, Ljava/util/Vector;-><init>()V

    .line 160
    .local v13, "tempRules":Ljava/util/Vector;
    const/4 v8, 0x0

    .line 161
    .local v8, "predecessor":Lcom/ibm/icu/text/NFRule;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v14

    if-ge v4, v14, :cond_5

    .line 165
    invoke-virtual {v10, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v14, v0, v8, v1}, Lcom/ibm/icu/text/NFRule;->makeRules(Ljava/lang/String;Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/RuleBasedNumberFormat;)Ljava/lang/Object;

    move-result-object v12

    .line 168
    .local v12, "temp":Ljava/lang/Object;
    instance-of v14, v12, Lcom/ibm/icu/text/NFRule;

    if-eqz v14, :cond_4

    .line 169
    invoke-virtual {v13, v12}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    move-object v8, v12

    .line 170
    check-cast v8, Lcom/ibm/icu/text/NFRule;

    .line 161
    .end local v12    # "temp":Ljava/lang/Object;
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 172
    .restart local v12    # "temp":Ljava/lang/Object;
    :cond_4
    instance-of v14, v12, [Lcom/ibm/icu/text/NFRule;

    if-eqz v14, :cond_3

    .line 173
    check-cast v12, [Lcom/ibm/icu/text/NFRule;

    .end local v12    # "temp":Ljava/lang/Object;
    move-object v11, v12

    check-cast v11, [Lcom/ibm/icu/text/NFRule;

    .line 175
    .local v11, "rulesToAdd":[Lcom/ibm/icu/text/NFRule;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_3
    array-length v14, v11

    if-ge v5, v14, :cond_3

    .line 176
    aget-object v14, v11, v5

    invoke-virtual {v13, v14}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 177
    aget-object v8, v11, v5

    .line 175
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 182
    .end local v5    # "j":I
    .end local v11    # "rulesToAdd":[Lcom/ibm/icu/text/NFRule;
    :cond_5
    const/4 v10, 0x0

    .line 188
    const-wide/16 v2, 0x0

    .line 193
    .local v2, "defaultBaseValue":J
    const/4 v4, 0x0

    .line 194
    :goto_4
    invoke-virtual {v13}, Ljava/util/Vector;->size()I

    move-result v14

    if-ge v4, v14, :cond_9

    .line 195
    invoke-virtual {v13, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/ibm/icu/text/NFRule;

    .line 197
    .local v9, "rule":Lcom/ibm/icu/text/NFRule;
    invoke-virtual {v9}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v14

    long-to-int v14, v14

    packed-switch v14, :pswitch_data_0

    .line 243
    invoke-virtual {v9}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v14

    cmp-long v14, v14, v2

    if-gez v14, :cond_7

    .line 244
    new-instance v14, Ljava/lang/IllegalArgumentException;

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v16, "Rules are not in order, base: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v9}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v15

    const-string/jumbo v16, " < "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 204
    :pswitch_0
    invoke-virtual {v9, v2, v3}, Lcom/ibm/icu/text/NFRule;->setBaseValue(J)V

    .line 205
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/ibm/icu/text/NFRuleSet;->isFractionRuleSet:Z

    if-nez v14, :cond_6

    .line 206
    const-wide/16 v14, 0x1

    add-long/2addr v2, v14

    .line 208
    :cond_6
    add-int/lit8 v4, v4, 0x1

    .line 209
    goto :goto_4

    .line 214
    :pswitch_1
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/ibm/icu/text/NFRuleSet;->negativeNumberRule:Lcom/ibm/icu/text/NFRule;

    .line 215
    invoke-virtual {v13, v4}, Ljava/util/Vector;->removeElementAt(I)V

    goto :goto_4

    .line 221
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    const/4 v15, 0x0

    aput-object v9, v14, v15

    .line 222
    invoke-virtual {v13, v4}, Ljava/util/Vector;->removeElementAt(I)V

    goto :goto_4

    .line 228
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    const/4 v15, 0x1

    aput-object v9, v14, v15

    .line 229
    invoke-virtual {v13, v4}, Ljava/util/Vector;->removeElementAt(I)V

    goto :goto_4

    .line 235
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    const/4 v15, 0x2

    aput-object v9, v14, v15

    .line 236
    invoke-virtual {v13, v4}, Ljava/util/Vector;->removeElementAt(I)V

    goto :goto_4

    .line 247
    :cond_7
    invoke-virtual {v9}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v2

    .line 248
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/ibm/icu/text/NFRuleSet;->isFractionRuleSet:Z

    if-nez v14, :cond_8

    .line 249
    const-wide/16 v14, 0x1

    add-long/2addr v2, v14

    .line 251
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_4

    .line 258
    .end local v9    # "rule":Lcom/ibm/icu/text/NFRule;
    :cond_9
    invoke-virtual {v13}, Ljava/util/Vector;->size()I

    move-result v14

    new-array v14, v14, [Lcom/ibm/icu/text/NFRule;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    .line 259
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    check-cast v14, [Ljava/lang/Object;

    invoke-virtual {v13, v14}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    .line 260
    return-void

    .line 197
    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 322
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 325
    .local v1, "result":Ljava/lang/StringBuffer;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v3, p0, Lcom/ibm/icu/text/NFRuleSet;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, ":\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 328
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 329
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "    "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/ibm/icu/text/NFRuleSet;->rules:[Lcom/ibm/icu/text/NFRule;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/ibm/icu/text/NFRule;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 328
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 333
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/NFRuleSet;->negativeNumberRule:Lcom/ibm/icu/text/NFRule;

    if-eqz v2, :cond_1

    .line 334
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "    "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/ibm/icu/text/NFRuleSet;->negativeNumberRule:Lcom/ibm/icu/text/NFRule;

    invoke-virtual {v3}, Lcom/ibm/icu/text/NFRule;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 336
    :cond_1
    iget-object v2, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v2, v2, v4

    if-eqz v2, :cond_2

    .line 337
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "    "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/ibm/icu/text/NFRule;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 339
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v2, v2, v5

    if-eqz v2, :cond_3

    .line 340
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "    "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v3, v3, v5

    invoke-virtual {v3}, Lcom/ibm/icu/text/NFRule;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 342
    :cond_3
    iget-object v2, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v2, v2, v6

    if-eqz v2, :cond_4

    .line 343
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "    "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/ibm/icu/text/NFRuleSet;->fractionRules:[Lcom/ibm/icu/text/NFRule;

    aget-object v3, v3, v6

    invoke-virtual {v3}, Lcom/ibm/icu/text/NFRule;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 346
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
