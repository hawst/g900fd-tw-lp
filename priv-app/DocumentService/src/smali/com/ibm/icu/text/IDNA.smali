.class public final Lcom/ibm/icu/text/IDNA;
.super Ljava/lang/Object;
.source "IDNA.java"


# static fields
.field private static ACE_PREFIX:[C = null

.field public static final ALLOW_UNASSIGNED:I = 0x1

.field private static final CAPITAL_A:I = 0x41

.field private static final CAPITAL_Z:I = 0x5a

.field public static final DEFAULT:I = 0x0

.field private static final FULL_STOP:I = 0x2e

.field private static final HYPHEN:I = 0x2d

.field private static final LOWER_CASE_DELTA:I = 0x20

.field private static final MAX_DOMAIN_NAME_LENGTH:I = 0xff

.field private static final MAX_LABEL_LENGTH:I = 0x3f

.field public static final USE_STD3_RULES:I = 0x2

.field private static final singleton:Lcom/ibm/icu/text/IDNA;


# instance fields
.field private namePrep:Lcom/ibm/icu/text/StringPrep;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x4

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/text/IDNA;->ACE_PREFIX:[C

    .line 80
    new-instance v0, Lcom/ibm/icu/text/IDNA;

    invoke-direct {v0}, Lcom/ibm/icu/text/IDNA;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/IDNA;->singleton:Lcom/ibm/icu/text/IDNA;

    return-void

    .line 44
    :array_0
    .array-data 2
        0x78s
        0x6es
        0x2ds
        0x2ds
    .end array-data
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    :try_start_0
    const-string/jumbo v2, "data/icudt40b/uidna.spp"

    invoke-static {v2}, Lcom/ibm/icu/impl/ICUData;->getRequiredStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 89
    .local v1, "stream":Ljava/io/InputStream;
    new-instance v2, Lcom/ibm/icu/text/StringPrep;

    invoke-direct {v2, v1}, Lcom/ibm/icu/text/StringPrep;-><init>(Ljava/io/InputStream;)V

    iput-object v2, p0, Lcom/ibm/icu/text/IDNA;->namePrep:Lcom/ibm/icu/text/StringPrep;

    .line 90
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    return-void

    .line 91
    .end local v1    # "stream":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/util/MissingResourceException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ""

    const-string/jumbo v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v2
.end method

.method public static compare(Lcom/ibm/icu/text/UCharacterIterator;Lcom/ibm/icu/text/UCharacterIterator;I)I
    .locals 4
    .param p0, "s1"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p1, "s2"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p2, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 966
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 967
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "One of the source buffers is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 969
    :cond_1
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/ibm/icu/text/IDNA;->convertIDNToASCII(Ljava/lang/String;I)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 970
    .local v0, "s1Out":Ljava/lang/StringBuffer;
    invoke-virtual {p1}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/ibm/icu/text/IDNA;->convertIDNToASCII(Ljava/lang/String;I)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 971
    .local v1, "s2Out":Ljava/lang/StringBuffer;
    invoke-static {v0, v1}, Lcom/ibm/icu/text/IDNA;->compareCaseInsensitiveASCII(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)I

    move-result v2

    return v2
.end method

.method public static compare(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 4
    .param p0, "s1"    # Ljava/lang/String;
    .param p1, "s2"    # Ljava/lang/String;
    .param p2, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 928
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 929
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "One of the source buffers is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 931
    :cond_1
    invoke-static {p0, p2}, Lcom/ibm/icu/text/IDNA;->convertIDNToASCII(Ljava/lang/String;I)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 932
    .local v0, "s1Out":Ljava/lang/StringBuffer;
    invoke-static {p1, p2}, Lcom/ibm/icu/text/IDNA;->convertIDNToASCII(Ljava/lang/String;I)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 933
    .local v1, "s2Out":Ljava/lang/StringBuffer;
    invoke-static {v0, v1}, Lcom/ibm/icu/text/IDNA;->compareCaseInsensitiveASCII(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)I

    move-result v2

    return v2
.end method

.method public static compare(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;I)I
    .locals 4
    .param p0, "s1"    # Ljava/lang/StringBuffer;
    .param p1, "s2"    # Ljava/lang/StringBuffer;
    .param p2, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 889
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 890
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "One of the source buffers is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 892
    :cond_1
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/ibm/icu/text/IDNA;->convertIDNToASCII(Ljava/lang/String;I)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 893
    .local v0, "s1Out":Ljava/lang/StringBuffer;
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/ibm/icu/text/IDNA;->convertIDNToASCII(Ljava/lang/String;I)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 894
    .local v1, "s2Out":Ljava/lang/StringBuffer;
    invoke-static {v0, v1}, Lcom/ibm/icu/text/IDNA;->compareCaseInsensitiveASCII(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)I

    move-result v2

    return v2
.end method

.method private static compareCaseInsensitiveASCII(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)I
    .locals 6
    .param p0, "s1"    # Ljava/lang/StringBuffer;
    .param p1, "s2"    # Ljava/lang/StringBuffer;

    .prologue
    .line 128
    const/4 v2, 0x0

    .line 130
    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-ne v2, v4, :cond_1

    .line 131
    const/4 v3, 0x0

    .line 141
    :cond_0
    return v3

    .line 134
    :cond_1
    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 135
    .local v0, "c1":C
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    .line 138
    .local v1, "c2":C
    if-eq v0, v1, :cond_2

    .line 139
    invoke-static {v0}, Lcom/ibm/icu/text/IDNA;->toASCIILower(C)C

    move-result v4

    invoke-static {v1}, Lcom/ibm/icu/text/IDNA;->toASCIILower(C)C

    move-result v5

    sub-int v3, v4, v5

    .line 140
    .local v3, "rc":I
    if-nez v3, :cond_0

    .line 128
    .end local v3    # "rc":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static convertIDNToASCII(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;
    .locals 1
    .param p0, "src"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p1, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 441
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/ibm/icu/text/IDNA;->convertIDNToASCII(Ljava/lang/String;I)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static convertIDNToASCII(Ljava/lang/String;I)Ljava/lang/StringBuffer;
    .locals 9
    .param p0, "src"    # Ljava/lang/String;
    .param p1, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 512
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    .line 513
    .local v5, "srcArr":[C
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 514
    .local v3, "result":Ljava/lang/StringBuffer;
    const/4 v4, 0x0

    .line 515
    .local v4, "sepIndex":I
    const/4 v2, 0x0

    .line 517
    .local v2, "oldSepIndex":I
    :goto_0
    array-length v6, v5

    invoke-static {v5, v4, v6}, Lcom/ibm/icu/text/IDNA;->getSeparatorIndex([CII)I

    move-result v4

    .line 518
    new-instance v1, Ljava/lang/String;

    sub-int v6, v4, v2

    invoke-direct {v1, v5, v2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 520
    .local v1, "label":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_0

    array-length v6, v5

    if-eq v4, v6, :cond_1

    .line 521
    :cond_0
    invoke-static {v1}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v0

    .line 522
    .local v0, "iter":Lcom/ibm/icu/text/UCharacterIterator;
    invoke-static {v0, p1}, Lcom/ibm/icu/text/IDNA;->convertToASCII(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 524
    .end local v0    # "iter":Lcom/ibm/icu/text/UCharacterIterator;
    :cond_1
    array-length v6, v5

    if-ne v4, v6, :cond_2

    .line 533
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    const/16 v7, 0xff

    if-le v6, v7, :cond_3

    .line 534
    new-instance v6, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v7, "The output exceed the max allowed length."

    const/16 v8, 0xb

    invoke-direct {v6, v7, v8}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;I)V

    throw v6

    .line 529
    :cond_2
    add-int/lit8 v4, v4, 0x1

    .line 530
    move v2, v4

    .line 531
    const/16 v6, 0x2e

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 536
    :cond_3
    return-object v3
.end method

.method public static convertIDNToASCII(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;
    .locals 1
    .param p0, "src"    # Ljava/lang/StringBuffer;
    .param p1, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 476
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/ibm/icu/text/IDNA;->convertIDNToASCII(Ljava/lang/String;I)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static convertIDNToUnicode(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;
    .locals 1
    .param p0, "src"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p1, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 766
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/ibm/icu/text/IDNA;->convertIDNToUnicode(Ljava/lang/String;I)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static convertIDNToUnicode(Ljava/lang/String;I)Ljava/lang/StringBuffer;
    .locals 9
    .param p0, "src"    # Ljava/lang/String;
    .param p1, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 831
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    .line 832
    .local v5, "srcArr":[C
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 833
    .local v3, "result":Ljava/lang/StringBuffer;
    const/4 v4, 0x0

    .line 834
    .local v4, "sepIndex":I
    const/4 v2, 0x0

    .line 836
    .local v2, "oldSepIndex":I
    :goto_0
    array-length v6, v5

    invoke-static {v5, v4, v6}, Lcom/ibm/icu/text/IDNA;->getSeparatorIndex([CII)I

    move-result v4

    .line 837
    new-instance v1, Ljava/lang/String;

    sub-int v6, v4, v2

    invoke-direct {v1, v5, v2, v6}, Ljava/lang/String;-><init>([CII)V

    .line 838
    .local v1, "label":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_0

    array-length v6, v5

    if-eq v4, v6, :cond_0

    .line 839
    new-instance v6, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v7, "Found zero length lable after NamePrep."

    const/16 v8, 0xa

    invoke-direct {v6, v7, v8}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;I)V

    throw v6

    .line 841
    :cond_0
    invoke-static {v1}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v0

    .line 842
    .local v0, "iter":Lcom/ibm/icu/text/UCharacterIterator;
    invoke-static {v0, p1}, Lcom/ibm/icu/text/IDNA;->convertToUnicode(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 843
    array-length v6, v5

    if-ne v4, v6, :cond_1

    .line 852
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    const/16 v7, 0xff

    if-le v6, v7, :cond_2

    .line 853
    new-instance v6, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v7, "The output exceed the max allowed length."

    const/16 v8, 0xb

    invoke-direct {v6, v7, v8}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;I)V

    throw v6

    .line 847
    :cond_1
    aget-char v6, v5, v4

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 849
    add-int/lit8 v4, v4, 0x1

    .line 850
    move v2, v4

    .line 851
    goto :goto_0

    .line 855
    :cond_2
    return-object v3
.end method

.method public static convertIDNToUnicode(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;
    .locals 1
    .param p0, "src"    # Ljava/lang/StringBuffer;
    .param p1, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 798
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/ibm/icu/text/IDNA;->convertIDNToUnicode(Ljava/lang/String;I)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static convertToASCII(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;
    .locals 19
    .param p0, "src"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p1, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 303
    const/4 v2, 0x0

    .line 306
    .local v2, "caseFlags":[Z
    const/4 v11, 0x1

    .line 308
    .local v11, "srcIsASCII":Z
    const/4 v12, 0x1

    .line 311
    .local v12, "srcIsLDH":Z
    and-int/lit8 v14, p1, 0x2

    if-eqz v14, :cond_1

    const/4 v13, 0x1

    .line 314
    .local v13, "useSTD3ASCIIRules":Z
    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    move-result v3

    .local v3, "ch":I
    const/4 v14, -0x1

    if-eq v3, v14, :cond_2

    .line 315
    const/16 v14, 0x7f

    if-le v3, v14, :cond_0

    .line 316
    const/4 v11, 0x0

    .line 317
    goto :goto_0

    .line 311
    .end local v3    # "ch":I
    .end local v13    # "useSTD3ASCIIRules":Z
    :cond_1
    const/4 v13, 0x0

    goto :goto_0

    .line 319
    .restart local v3    # "ch":I
    .restart local v13    # "useSTD3ASCIIRules":Z
    :cond_2
    const/4 v5, -0x1

    .line 320
    .local v5, "failPos":I
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/UCharacterIterator;->setToStart()V

    .line 321
    const/4 v9, 0x0

    .line 323
    .local v9, "processOut":Ljava/lang/StringBuffer;
    if-nez v11, :cond_3

    .line 325
    sget-object v14, Lcom/ibm/icu/text/IDNA;->singleton:Lcom/ibm/icu/text/IDNA;

    iget-object v14, v14, Lcom/ibm/icu/text/IDNA;->namePrep:Lcom/ibm/icu/text/StringPrep;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v14, v0, v1}, Lcom/ibm/icu/text/StringPrep;->prepare(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;

    move-result-object v9

    .line 329
    :goto_1
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    .line 331
    .local v8, "poLen":I
    if-nez v8, :cond_4

    .line 332
    new-instance v14, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v15, "Found zero length lable after NamePrep."

    const/16 v16, 0xa

    invoke-direct/range {v14 .. v16}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;I)V

    throw v14

    .line 327
    .end local v8    # "poLen":I
    :cond_3
    new-instance v9, Ljava/lang/StringBuffer;

    .end local v9    # "processOut":Ljava/lang/StringBuffer;
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v9, v14}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .restart local v9    # "processOut":Ljava/lang/StringBuffer;
    goto :goto_1

    .line 334
    .restart local v8    # "poLen":I
    :cond_4
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 337
    .local v4, "dest":Ljava/lang/StringBuffer;
    const/4 v11, 0x1

    .line 340
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_2
    if-ge v6, v8, :cond_7

    .line 341
    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    .line 342
    const/16 v14, 0x7f

    if-le v3, v14, :cond_6

    .line 343
    const/4 v11, 0x0

    .line 340
    :cond_5
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 344
    :cond_6
    invoke-static {v3}, Lcom/ibm/icu/text/IDNA;->isLDHChar(I)Z

    move-result v14

    if-nez v14, :cond_5

    .line 348
    const/4 v12, 0x0

    .line 349
    move v5, v6

    goto :goto_3

    .line 353
    :cond_7
    const/4 v14, 0x1

    if-ne v13, v14, :cond_d

    .line 355
    if-eqz v12, :cond_8

    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v14

    const/16 v15, 0x2d

    if-eq v14, v15, :cond_8

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->length()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    invoke-virtual {v9, v14}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v14

    const/16 v15, 0x2d

    if-ne v14, v15, :cond_d

    .line 360
    :cond_8
    if-nez v12, :cond_a

    .line 361
    new-instance v14, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v15, "The input does not conform to the STD 3 ASCII rules"

    const/16 v16, 0x5

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    if-lez v5, :cond_9

    add-int/lit8 v5, v5, -0x1

    .end local v5    # "failPos":I
    :cond_9
    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v14, v15, v0, v1, v5}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    throw v14

    .line 365
    .restart local v5    # "failPos":I
    :cond_a
    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v14

    const/16 v15, 0x2d

    if-ne v14, v15, :cond_b

    .line 366
    new-instance v14, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v15, "The input does not conform to the STD 3 ASCII rules"

    const/16 v16, 0x5

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    invoke-direct/range {v14 .. v18}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    throw v14

    .line 370
    :cond_b
    new-instance v14, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v15, "The input does not conform to the STD 3 ASCII rules"

    const/16 v16, 0x5

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    if-lez v8, :cond_c

    add-int/lit8 v8, v8, -0x1

    .end local v8    # "poLen":I
    :cond_c
    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v14, v15, v0, v1, v8}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    throw v14

    .line 378
    .restart local v8    # "poLen":I
    :cond_d
    if-eqz v11, :cond_e

    .line 379
    move-object v4, v9

    .line 402
    :goto_4
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v14

    const/16 v15, 0x3f

    if-le v14, v15, :cond_10

    .line 403
    new-instance v14, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v15, "The labels in the input are too long. Length > 63."

    const/16 v16, 0x8

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    invoke-direct/range {v14 .. v18}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    throw v14

    .line 382
    :cond_e
    invoke-static {v9}, Lcom/ibm/icu/text/IDNA;->startsWithPrefix(Ljava/lang/StringBuffer;)Z

    move-result v14

    if-nez v14, :cond_f

    .line 385
    new-array v2, v8, [Z

    .line 387
    invoke-static {v9, v2}, Lcom/ibm/icu/text/Punycode;->encode(Ljava/lang/StringBuffer;[Z)Ljava/lang/StringBuffer;

    move-result-object v10

    .line 390
    .local v10, "punyout":Ljava/lang/StringBuffer;
    invoke-static {v10}, Lcom/ibm/icu/text/IDNA;->toASCIILower(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v7

    .line 393
    .local v7, "lowerOut":Ljava/lang/StringBuffer;
    sget-object v14, Lcom/ibm/icu/text/IDNA;->ACE_PREFIX:[C

    const/4 v15, 0x0

    sget-object v16, Lcom/ibm/icu/text/IDNA;->ACE_PREFIX:[C

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v4, v14, v15, v0}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 395
    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    goto :goto_4

    .line 398
    .end local v7    # "lowerOut":Ljava/lang/StringBuffer;
    .end local v10    # "punyout":Ljava/lang/StringBuffer;
    :cond_f
    new-instance v14, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v15, "The input does not start with the ACE Prefix."

    const/16 v16, 0x6

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    invoke-direct/range {v14 .. v18}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    throw v14

    .line 406
    :cond_10
    return-object v4
.end method

.method public static convertToASCII(Ljava/lang/String;I)Ljava/lang/StringBuffer;
    .locals 2
    .param p0, "src"    # Ljava/lang/String;
    .param p1, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 240
    invoke-static {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v0

    .line 241
    .local v0, "iter":Lcom/ibm/icu/text/UCharacterIterator;
    invoke-static {v0, p1}, Lcom/ibm/icu/text/IDNA;->convertToASCII(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;

    move-result-object v1

    return-object v1
.end method

.method public static convertToASCII(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;
    .locals 2
    .param p0, "src"    # Ljava/lang/StringBuffer;
    .param p1, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 271
    invoke-static {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance(Ljava/lang/StringBuffer;)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v0

    .line 272
    .local v0, "iter":Lcom/ibm/icu/text/UCharacterIterator;
    invoke-static {v0, p1}, Lcom/ibm/icu/text/IDNA;->convertToASCII(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;

    move-result-object v1

    return-object v1
.end method

.method public static convertToUnicode(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;
    .locals 12
    .param p0, "src"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p1, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 630
    const/4 v0, 0x0

    .line 633
    .local v0, "caseFlags":[Z
    const/4 v7, 0x1

    .line 642
    .local v7, "srcIsASCII":Z
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v6

    .line 644
    .local v6, "saveIndex":I
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    move-result v1

    .local v1, "ch":I
    const/4 v10, -0x1

    if-eq v1, v10, :cond_1

    .line 645
    const/16 v10, 0x7f

    if-le v1, v10, :cond_0

    .line 646
    const/4 v7, 0x0

    .line 647
    goto :goto_0

    .line 653
    :cond_1
    if-nez v7, :cond_3

    .line 656
    :try_start_0
    invoke-virtual {p0, v6}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 657
    sget-object v10, Lcom/ibm/icu/text/IDNA;->singleton:Lcom/ibm/icu/text/IDNA;

    iget-object v10, v10, Lcom/ibm/icu/text/IDNA;->namePrep:Lcom/ibm/icu/text/StringPrep;

    invoke-virtual {v10, p0, p1}, Lcom/ibm/icu/text/StringPrep;->prepare(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Lcom/ibm/icu/text/StringPrepParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 674
    .local v5, "processOut":Ljava/lang/StringBuffer;
    :goto_1
    invoke-static {v5}, Lcom/ibm/icu/text/IDNA;->startsWithPrefix(Ljava/lang/StringBuffer;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 675
    const/4 v2, 0x0

    .line 678
    .local v2, "decodeOut":Ljava/lang/StringBuffer;
    sget-object v10, Lcom/ibm/icu/text/IDNA;->ACE_PREFIX:[C

    array-length v10, v10

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v11

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 682
    .local v8, "temp":Ljava/lang/String;
    :try_start_1
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10, v8}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-static {v10, v0}, Lcom/ibm/icu/text/Punycode;->decode(Ljava/lang/StringBuffer;[Z)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Lcom/ibm/icu/text/StringPrepParseException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 688
    :goto_2
    if-eqz v2, :cond_2

    .line 689
    invoke-static {v2, p1}, Lcom/ibm/icu/text/IDNA;->convertToASCII(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    move-result-object v9

    .line 692
    .local v9, "toASCIIOut":Ljava/lang/StringBuffer;
    invoke-static {v5, v9}, Lcom/ibm/icu/text/IDNA;->compareCaseInsensitiveASCII(Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)I

    move-result v10

    if-eqz v10, :cond_2

    .line 695
    const/4 v2, 0x0

    .line 700
    .end local v9    # "toASCIIOut":Ljava/lang/StringBuffer;
    :cond_2
    if-eqz v2, :cond_4

    .line 734
    .end local v2    # "decodeOut":Ljava/lang/StringBuffer;
    .end local v5    # "processOut":Ljava/lang/StringBuffer;
    .end local v8    # "temp":Ljava/lang/String;
    :goto_3
    return-object v2

    .line 658
    :catch_0
    move-exception v4

    .line 659
    .local v4, "ex":Lcom/ibm/icu/text/StringPrepParseException;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v10}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 664
    .end local v4    # "ex":Lcom/ibm/icu/text/StringPrepParseException;
    :cond_3
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .restart local v5    # "processOut":Ljava/lang/StringBuffer;
    goto :goto_1

    .line 683
    .restart local v2    # "decodeOut":Ljava/lang/StringBuffer;
    .restart local v8    # "temp":Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 684
    .local v3, "e":Lcom/ibm/icu/text/StringPrepParseException;
    const/4 v2, 0x0

    goto :goto_2

    .line 734
    .end local v2    # "decodeOut":Ljava/lang/StringBuffer;
    .end local v3    # "e":Lcom/ibm/icu/text/StringPrepParseException;
    .end local v8    # "temp":Ljava/lang/String;
    :cond_4
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v10}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static convertToUnicode(Ljava/lang/String;I)Ljava/lang/StringBuffer;
    .locals 2
    .param p0, "src"    # Ljava/lang/String;
    .param p1, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 567
    invoke-static {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v0

    .line 568
    .local v0, "iter":Lcom/ibm/icu/text/UCharacterIterator;
    invoke-static {v0, p1}, Lcom/ibm/icu/text/IDNA;->convertToUnicode(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;

    move-result-object v1

    return-object v1
.end method

.method public static convertToUnicode(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;
    .locals 2
    .param p0, "src"    # Ljava/lang/StringBuffer;
    .param p1, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 598
    invoke-static {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance(Ljava/lang/StringBuffer;)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v0

    .line 599
    .local v0, "iter":Lcom/ibm/icu/text/UCharacterIterator;
    invoke-static {v0, p1}, Lcom/ibm/icu/text/IDNA;->convertToUnicode(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;

    move-result-object v1

    return-object v1
.end method

.method private static getSeparatorIndex([CII)I
    .locals 1
    .param p0, "src"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    .line 148
    :goto_0
    if-ge p1, p2, :cond_0

    .line 149
    aget-char v0, p0, p1

    invoke-static {v0}, Lcom/ibm/icu/text/IDNA;->isLabelSeparator(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    :cond_0
    return p1

    .line 148
    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0
.end method

.method private static isLDHChar(I)Z
    .locals 3
    .param p0, "ch"    # I

    .prologue
    const/16 v2, 0x7a

    const/4 v0, 0x0

    .line 179
    if-le p0, v2, :cond_1

    .line 190
    :cond_0
    :goto_0
    return v0

    .line 183
    :cond_1
    const/16 v1, 0x2d

    if-eq p0, v1, :cond_4

    const/16 v1, 0x30

    if-gt v1, p0, :cond_2

    const/16 v1, 0x39

    if-le p0, v1, :cond_4

    :cond_2
    const/16 v1, 0x41

    if-gt v1, p0, :cond_3

    const/16 v1, 0x5a

    if-le p0, v1, :cond_4

    :cond_3
    const/16 v1, 0x61

    if-gt v1, p0, :cond_0

    if-gt p0, v2, :cond_0

    .line 188
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static isLabelSeparator(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 202
    sparse-switch p0, :sswitch_data_0

    .line 209
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 207
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 202
    nop

    :sswitch_data_0
    .sparse-switch
        0x2e -> :sswitch_0
        0x3002 -> :sswitch_0
        0xff0e -> :sswitch_0
        0xff61 -> :sswitch_0
    .end sparse-switch
.end method

.method private static startsWithPrefix(Ljava/lang/StringBuffer;)Z
    .locals 4
    .param p0, "src"    # Ljava/lang/StringBuffer;

    .prologue
    .line 97
    const/4 v1, 0x1

    .line 99
    .local v1, "startsWithPrefix":Z
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    sget-object v3, Lcom/ibm/icu/text/IDNA;->ACE_PREFIX:[C

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 100
    const/4 v2, 0x0

    .line 107
    :goto_0
    return v2

    .line 102
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v2, Lcom/ibm/icu/text/IDNA;->ACE_PREFIX:[C

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 103
    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/IDNA;->toASCIILower(C)C

    move-result v2

    sget-object v3, Lcom/ibm/icu/text/IDNA;->ACE_PREFIX:[C

    aget-char v3, v3, v0

    if-eq v2, v3, :cond_1

    .line 104
    const/4 v1, 0x0

    .line 102
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v2, v1

    .line 107
    goto :goto_0
.end method

.method private static toASCIILower(C)C
    .locals 1
    .param p0, "ch"    # C

    .prologue
    .line 111
    const/16 v0, 0x41

    if-gt v0, p0, :cond_0

    const/16 v0, 0x5a

    if-gt p0, v0, :cond_0

    .line 112
    add-int/lit8 v0, p0, 0x20

    int-to-char p0, v0

    .line 114
    .end local p0    # "ch":C
    :cond_0
    return p0
.end method

.method private static toASCIILower(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 3
    .param p0, "src"    # Ljava/lang/StringBuffer;

    .prologue
    .line 118
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 119
    .local v0, "dest":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 120
    invoke-virtual {p0, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/IDNA;->toASCIILower(C)C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 119
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 122
    :cond_0
    return-object v0
.end method
