.class public final Lcom/ibm/icu/text/ComposedCharIter;
.super Ljava/lang/Object;
.source "ComposedCharIter.java"


# static fields
.field public static final DONE:C = '\uffff'


# instance fields
.field private bufLen:I

.field private compat:Z

.field private curChar:I

.field private decompBuf:[C

.field private nextChar:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    const/16 v0, 0x64

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/ibm/icu/text/ComposedCharIter;->decompBuf:[C

    .line 159
    iput v1, p0, Lcom/ibm/icu/text/ComposedCharIter;->bufLen:I

    .line 160
    iput v1, p0, Lcom/ibm/icu/text/ComposedCharIter;->curChar:I

    .line 161
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/ComposedCharIter;->nextChar:I

    .line 69
    iput-boolean v1, p0, Lcom/ibm/icu/text/ComposedCharIter;->compat:Z

    .line 71
    return-void
.end method

.method public constructor <init>(ZI)V
    .locals 2
    .param p1, "compat"    # Z
    .param p2, "options"    # I

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    const/16 v0, 0x64

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/ibm/icu/text/ComposedCharIter;->decompBuf:[C

    .line 159
    iput v1, p0, Lcom/ibm/icu/text/ComposedCharIter;->bufLen:I

    .line 160
    iput v1, p0, Lcom/ibm/icu/text/ComposedCharIter;->curChar:I

    .line 161
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/ComposedCharIter;->nextChar:I

    .line 89
    iput-boolean p1, p0, Lcom/ibm/icu/text/ComposedCharIter;->compat:Z

    .line 91
    return-void
.end method

.method private findNextChar()V
    .locals 5

    .prologue
    .line 136
    iget v1, p0, Lcom/ibm/icu/text/ComposedCharIter;->curChar:I

    add-int/lit8 v0, v1, 0x1

    .line 138
    .local v0, "c":I
    :goto_0
    const v1, 0xffff

    if-ge v0, v1, :cond_1

    .line 139
    iget-boolean v1, p0, Lcom/ibm/icu/text/ComposedCharIter;->compat:Z

    iget-object v2, p0, Lcom/ibm/icu/text/ComposedCharIter;->decompBuf:[C

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/ibm/icu/text/ComposedCharIter;->decompBuf:[C

    array-length v4, v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/ibm/icu/impl/NormalizerImpl;->getDecomposition(IZ[CII)I

    move-result v1

    iput v1, p0, Lcom/ibm/icu/text/ComposedCharIter;->bufLen:I

    .line 142
    iget v1, p0, Lcom/ibm/icu/text/ComposedCharIter;->bufLen:I

    if-lez v1, :cond_0

    .line 153
    :goto_1
    iput v0, p0, Lcom/ibm/icu/text/ComposedCharIter;->nextChar:I

    .line 154
    return-void

    .line 147
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 148
    goto :goto_0

    .line 149
    :cond_1
    const/4 v0, -0x1

    .line 150
    goto :goto_1
.end method


# virtual methods
.method public decomposition()Ljava/lang/String;
    .locals 4

    .prologue
    .line 132
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/ibm/icu/text/ComposedCharIter;->decompBuf:[C

    const/4 v2, 0x0

    iget v3, p0, Lcom/ibm/icu/text/ComposedCharIter;->bufLen:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 99
    iget v0, p0, Lcom/ibm/icu/text/ComposedCharIter;->nextChar:I

    if-ne v0, v1, :cond_0

    .line 100
    invoke-direct {p0}, Lcom/ibm/icu/text/ComposedCharIter;->findNextChar()V

    .line 102
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/ComposedCharIter;->nextChar:I

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()C
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 114
    iget v0, p0, Lcom/ibm/icu/text/ComposedCharIter;->nextChar:I

    if-ne v0, v1, :cond_0

    .line 115
    invoke-direct {p0}, Lcom/ibm/icu/text/ComposedCharIter;->findNextChar()V

    .line 117
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/ComposedCharIter;->nextChar:I

    iput v0, p0, Lcom/ibm/icu/text/ComposedCharIter;->curChar:I

    .line 118
    iput v1, p0, Lcom/ibm/icu/text/ComposedCharIter;->nextChar:I

    .line 119
    iget v0, p0, Lcom/ibm/icu/text/ComposedCharIter;->curChar:I

    int-to-char v0, v0

    return v0
.end method
