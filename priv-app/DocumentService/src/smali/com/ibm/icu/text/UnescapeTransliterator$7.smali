.class Lcom/ibm/icu/text/UnescapeTransliterator$7;
.super Ljava/lang/Object;
.source "UnescapeTransliterator.java"

# interfaces
.implements Lcom/ibm/icu/text/Transliterator$Factory;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;
    .locals 3
    .param p1, "ID"    # Ljava/lang/String;

    .prologue
    .line 113
    new-instance v0, Lcom/ibm/icu/text/UnescapeTransliterator;

    const-string/jumbo v1, "Hex-Any"

    const/16 v2, 0x30

    new-array v2, v2, [C

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Lcom/ibm/icu/text/UnescapeTransliterator;-><init>(Ljava/lang/String;[C)V

    return-object v0

    :array_0
    .array-data 2
        0x2s
        0x0s
        0x10s
        0x4s
        0x6s
        0x55s
        0x2bs
        0x2s
        0x0s
        0x10s
        0x4s
        0x4s
        0x5cs
        0x75s
        0x2s
        0x0s
        0x10s
        0x8s
        0x8s
        0x5cs
        0x55s
        0x3s
        0x1s
        0x10s
        0x1s
        0x6s
        0x26s
        0x23s
        0x78s
        0x3bs
        0x2s
        0x1s
        0xas
        0x1s
        0x7s
        0x26s
        0x23s
        0x3bs
        0x3s
        0x1s
        0x10s
        0x1s
        0x6s
        0x5cs
        0x78s
        0x7bs
        0x7ds
        -0x1s
    .end array-data
.end method
