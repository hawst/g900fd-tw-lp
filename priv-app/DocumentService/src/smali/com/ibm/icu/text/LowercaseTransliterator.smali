.class Lcom/ibm/icu/text/LowercaseTransliterator;
.super Lcom/ibm/icu/text/Transliterator;
.source "LowercaseTransliterator.java"


# static fields
.field static final _ID:Ljava/lang/String; = "Any-Lower"


# instance fields
.field private csp:Lcom/ibm/icu/impl/UCaseProps;

.field private iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

.field private locCache:[I

.field private locale:Lcom/ibm/icu/util/ULocale;

.field private result:Ljava/lang/StringBuffer;


# direct methods
.method public constructor <init>(Lcom/ibm/icu/util/ULocale;)V
    .locals 4
    .param p1, "loc"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 55
    const-string/jumbo v1, "Any-Lower"

    invoke-direct {p0, v1, v3}, Lcom/ibm/icu/text/Transliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 56
    iput-object p1, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->locale:Lcom/ibm/icu/util/ULocale;

    .line 58
    :try_start_0
    invoke-static {}, Lcom/ibm/icu/impl/UCaseProps;->getSingleton()Lcom/ibm/icu/impl/UCaseProps;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->csp:Lcom/ibm/icu/impl/UCaseProps;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    new-instance v1, Lcom/ibm/icu/text/ReplaceableContextIterator;

    invoke-direct {v1}, Lcom/ibm/icu/text/ReplaceableContextIterator;-><init>()V

    iput-object v1, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    .line 63
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v1, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->result:Ljava/lang/StringBuffer;

    .line 64
    const/4 v1, 0x1

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->locCache:[I

    .line 65
    iget-object v1, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->locCache:[I

    aput v2, v1, v2

    .line 66
    return-void

    .line 59
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Ljava/io/IOException;
    iput-object v3, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->csp:Lcom/ibm/icu/impl/UCaseProps;

    goto :goto_0
.end method

.method static register()V
    .locals 3

    .prologue
    .line 34
    const-string/jumbo v0, "Any-Lower"

    new-instance v1, Lcom/ibm/icu/text/LowercaseTransliterator$1;

    invoke-direct {v1}, Lcom/ibm/icu/text/LowercaseTransliterator$1;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 40
    const-string/jumbo v0, "Lower"

    const-string/jumbo v1, "Upper"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/Transliterator;->registerSpecialInverse(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 41
    return-void
.end method


# virtual methods
.method protected handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V
    .locals 8
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "offsets"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "isIncremental"    # Z

    .prologue
    const/4 v7, 0x0

    .line 73
    iget-object v0, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->csp:Lcom/ibm/icu/impl/UCaseProps;

    if-nez v0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    iget v2, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    if-ge v0, v2, :cond_0

    .line 81
    iget-object v0, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/ReplaceableContextIterator;->setText(Lcom/ibm/icu/text/Replaceable;)V

    .line 82
    iget-object v0, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->result:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 88
    iget-object v0, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    iget v2, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/ReplaceableContextIterator;->setIndex(I)V

    .line 89
    iget-object v0, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    iget v2, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/ReplaceableContextIterator;->setLimit(I)V

    .line 90
    iget-object v0, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    iget v2, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    iget v3, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    invoke-virtual {v0, v2, v3}, Lcom/ibm/icu/text/ReplaceableContextIterator;->setContextLimits(II)V

    .line 91
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/ReplaceableContextIterator;->nextCaseMapCP()I

    move-result v1

    .local v1, "c":I
    if-ltz v1, :cond_5

    .line 92
    iget-object v0, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->csp:Lcom/ibm/icu/impl/UCaseProps;

    iget-object v2, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    iget-object v3, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->result:Ljava/lang/StringBuffer;

    iget-object v4, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->locale:Lcom/ibm/icu/util/ULocale;

    iget-object v5, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->locCache:[I

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/impl/UCaseProps;->toFullLower(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[I)I

    move-result v1

    .line 94
    iget-object v0, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/ReplaceableContextIterator;->didReachLimit()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p3, :cond_3

    .line 97
    iget-object v0, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/ReplaceableContextIterator;->getCaseMapCPStart()I

    move-result v0

    iput v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    goto :goto_0

    .line 102
    :cond_3
    if-ltz v1, :cond_2

    .line 105
    const/16 v0, 0x1f

    if-gt v1, v0, :cond_4

    .line 107
    iget-object v0, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    iget-object v2, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->result:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/ReplaceableContextIterator;->replace(Ljava/lang/String;)I

    move-result v6

    .line 108
    .local v6, "delta":I
    iget-object v0, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->result:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 114
    :goto_2
    if-eqz v6, :cond_2

    .line 115
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    add-int/2addr v0, v6

    iput v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 116
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    add-int/2addr v0, v6

    iput v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    goto :goto_1

    .line 111
    .end local v6    # "delta":I
    :cond_4
    iget-object v0, p0, Lcom/ibm/icu/text/LowercaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/ReplaceableContextIterator;->replace(Ljava/lang/String;)I

    move-result v6

    .restart local v6    # "delta":I
    goto :goto_2

    .line 119
    .end local v6    # "delta":I
    :cond_5
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    iput v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    goto/16 :goto_0
.end method
