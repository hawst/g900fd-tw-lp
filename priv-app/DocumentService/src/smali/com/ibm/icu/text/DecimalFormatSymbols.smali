.class public Lcom/ibm/icu/text/DecimalFormatSymbols;
.super Ljava/lang/Object;
.source "DecimalFormatSymbols.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final cachedLocaleData:Ljava/util/Hashtable;

.field private static final currentSerialVersion:I = 0x5

.field private static final serialVersionUID:J = 0x501d17990868939cL


# instance fields
.field private NaN:Ljava/lang/String;

.field private actualLocale:Lcom/ibm/icu/util/ULocale;

.field private transient currency:Lcom/ibm/icu/util/Currency;

.field private currencyPattern:Ljava/lang/String;

.field private currencySymbol:Ljava/lang/String;

.field private decimalSeparator:C

.field private digit:C

.field private exponentSeparator:Ljava/lang/String;

.field private exponential:C

.field private groupingSeparator:C

.field private infinity:Ljava/lang/String;

.field private intlCurrencySymbol:Ljava/lang/String;

.field private minusSign:C

.field private monetaryGroupingSeparator:C

.field private monetarySeparator:C

.field private padEscape:C

.field private patternSeparator:C

.field private perMill:C

.field private percent:C

.field private plusSign:C

.field private requestedLocale:Ljava/util/Locale;

.field private serialVersionOnStream:I

.field private sigDigit:C

.field private ulocale:Lcom/ibm/icu/util/ULocale;

.field private validLocale:Lcom/ibm/icu/util/ULocale;

.field private zeroDigit:C


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1008
    new-instance v0, Ljava/util/Hashtable;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/Hashtable;-><init>(I)V

    sput-object v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->cachedLocaleData:Ljava/util/Hashtable;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1003
    const/4 v0, 0x5

    iput v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->serialVersionOnStream:I

    .line 1013
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currencyPattern:Ljava/lang/String;

    .line 50
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DecimalFormatSymbols;->initialize(Lcom/ibm/icu/util/ULocale;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/util/ULocale;)V
    .locals 1
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1003
    const/4 v0, 0x5

    iput v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->serialVersionOnStream:I

    .line 1013
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currencyPattern:Ljava/lang/String;

    .line 68
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DecimalFormatSymbols;->initialize(Lcom/ibm/icu/util/ULocale;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1003
    const/4 v0, 0x5

    iput v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->serialVersionOnStream:I

    .line 1013
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currencyPattern:Ljava/lang/String;

    .line 59
    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DecimalFormatSymbols;->initialize(Lcom/ibm/icu/util/ULocale;)V

    .line 60
    return-void
.end method

.method public static getAvailableLocales()[Ljava/util/Locale;
    .locals 1

    .prologue
    .line 132
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableLocales(Ljava/lang/String;)[Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public static getAvailableULocales()[Lcom/ibm/icu/util/ULocale;
    .locals 1

    .prologue
    .line 149
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableULocales(Ljava/lang/String;)[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/ibm/icu/text/DecimalFormatSymbols;
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-direct {v0}, Lcom/ibm/icu/text/DecimalFormatSymbols;-><init>()V

    return-object v0
.end method

.method public static getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DecimalFormatSymbols;
    .locals 1
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 116
    new-instance v0, Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/DecimalFormatSymbols;-><init>(Lcom/ibm/icu/util/ULocale;)V

    return-object v0
.end method

.method public static getInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/DecimalFormatSymbols;
    .locals 1
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    .line 99
    new-instance v0, Lcom/ibm/icu/text/DecimalFormatSymbols;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    return-object v0
.end method

.method private initialize(Lcom/ibm/icu/util/ULocale;)V
    .locals 16
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 651
    invoke-virtual/range {p1 .. p1}, Lcom/ibm/icu/util/ULocale;->toLocale()Ljava/util/Locale;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->requestedLocale:Ljava/util/Locale;

    .line 652
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/DecimalFormatSymbols;->ulocale:Lcom/ibm/icu/util/ULocale;

    .line 655
    sget-object v13, Lcom/ibm/icu/text/DecimalFormatSymbols;->cachedLocaleData:Ljava/util/Hashtable;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [[Ljava/lang/String;

    move-object v6, v13

    check-cast v6, [[Ljava/lang/String;

    .line 657
    .local v6, "data":[[Ljava/lang/String;
    if-nez v6, :cond_0

    .line 658
    const/4 v13, 0x1

    new-array v6, v13, [[Ljava/lang/String;

    .line 659
    const-string/jumbo v13, "com/ibm/icu/impl/data/icudt40b"

    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v10

    check-cast v10, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 661
    .local v10, "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    const/4 v13, 0x0

    const-string/jumbo v14, "NumberElements"

    invoke-virtual {v10, v14}, Lcom/ibm/icu/impl/ICUResourceBundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    aput-object v14, v6, v13

    .line 663
    sget-object v13, Lcom/ibm/icu/text/DecimalFormatSymbols;->cachedLocaleData:Ljava/util/Hashtable;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 665
    .end local v10    # "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_0
    const/4 v13, 0x0

    aget-object v8, v6, v13

    .line 667
    .local v8, "numberElements":[Ljava/lang/String;
    const-string/jumbo v13, "com/ibm/icu/impl/data/icudt40b"

    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v9

    check-cast v9, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 671
    .local v9, "r":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-virtual {v9}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v12

    .line 672
    .local v12, "uloc":Lcom/ibm/icu/util/ULocale;
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v12}, Lcom/ibm/icu/text/DecimalFormatSymbols;->setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V

    .line 675
    const/4 v13, 0x0

    aget-object v13, v8, v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/String;->charAt(I)C

    move-result v13

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->decimalSeparator:C

    .line 676
    const/4 v13, 0x1

    aget-object v13, v8, v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/String;->charAt(I)C

    move-result v13

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->groupingSeparator:C

    .line 680
    const/4 v13, 0x2

    aget-object v13, v8, v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/String;->charAt(I)C

    move-result v13

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->patternSeparator:C

    .line 681
    const/4 v13, 0x3

    aget-object v13, v8, v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/String;->charAt(I)C

    move-result v13

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->percent:C

    .line 682
    const/4 v13, 0x4

    aget-object v13, v8, v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/String;->charAt(I)C

    move-result v13

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->zeroDigit:C

    .line 683
    const/4 v13, 0x5

    aget-object v13, v8, v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/String;->charAt(I)C

    move-result v13

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->digit:C

    .line 684
    const/4 v13, 0x6

    aget-object v13, v8, v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/String;->charAt(I)C

    move-result v13

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->minusSign:C

    .line 695
    const/4 v13, 0x7

    aget-object v13, v8, v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->exponentSeparator:Ljava/lang/String;

    .line 696
    const/16 v13, 0x8

    aget-object v13, v8, v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/String;->charAt(I)C

    move-result v13

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->perMill:C

    .line 697
    const/16 v13, 0x9

    aget-object v13, v8, v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->infinity:Ljava/lang/String;

    .line 698
    const/16 v13, 0xa

    aget-object v13, v8, v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->NaN:Ljava/lang/String;

    .line 700
    const/16 v13, 0xb

    aget-object v13, v8, v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/String;->charAt(I)C

    move-result v13

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->plusSign:C

    .line 701
    const/16 v13, 0x2a

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->padEscape:C

    .line 702
    const/16 v13, 0x40

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->sigDigit:C

    .line 707
    const/4 v5, 0x0

    .line 708
    .local v5, "currname":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/ibm/icu/util/Currency;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/Currency;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currency:Lcom/ibm/icu/util/Currency;

    .line 709
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currency:Lcom/ibm/icu/util/Currency;

    if-eqz v13, :cond_3

    .line 710
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currency:Lcom/ibm/icu/util/Currency;

    invoke-virtual {v13}, Lcom/ibm/icu/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->intlCurrencySymbol:Ljava/lang/String;

    .line 711
    const/4 v13, 0x1

    new-array v7, v13, [Z

    .line 712
    .local v7, "isChoiceFormat":[Z
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currency:Lcom/ibm/icu/util/Currency;

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14, v7}, Lcom/ibm/icu/util/Currency;->getName(Lcom/ibm/icu/util/ULocale;I[Z)Ljava/lang/String;

    move-result-object v5

    .line 717
    const/4 v13, 0x0

    aget-boolean v13, v7, v13

    if-eqz v13, :cond_2

    new-instance v13, Ljava/text/ChoiceFormat;

    invoke-direct {v13, v5}, Ljava/text/ChoiceFormat;-><init>(Ljava/lang/String;)V

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    invoke-virtual {v13, v14, v15}, Ljava/text/ChoiceFormat;->format(D)Ljava/lang/String;

    move-result-object v13

    :goto_0
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currencySymbol:Ljava/lang/String;

    .line 725
    .end local v7    # "isChoiceFormat":[Z
    :goto_1
    move-object/from16 v0, p0

    iget-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->decimalSeparator:C

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->monetarySeparator:C

    .line 726
    move-object/from16 v0, p0

    iget-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->groupingSeparator:C

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->monetaryGroupingSeparator:C

    .line 727
    invoke-static/range {p1 .. p1}, Lcom/ibm/icu/util/Currency;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/Currency;

    move-result-object v2

    .line 728
    .local v2, "curr":Lcom/ibm/icu/util/Currency;
    if-eqz v2, :cond_1

    .line 729
    invoke-virtual {v2}, Lcom/ibm/icu/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v3

    .line 730
    .local v3, "currencyCode":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 732
    const-string/jumbo v13, "com/ibm/icu/impl/data/icudt40b"

    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v11

    check-cast v11, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 733
    .local v11, "resource":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v13, "Currencies"

    invoke-virtual {v11, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v4

    .line 735
    .local v4, "currencyRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    :try_start_0
    invoke-virtual {v4, v3}, Lcom/ibm/icu/impl/ICUResourceBundle;->getWithFallback(Ljava/lang/String;)Lcom/ibm/icu/impl/ICUResourceBundle;

    move-result-object v4

    .line 736
    invoke-virtual {v4}, Lcom/ibm/icu/impl/ICUResourceBundle;->getSize()I

    move-result v13

    const/4 v14, 0x2

    if-le v13, v14, :cond_1

    .line 737
    const/4 v13, 0x2

    invoke-virtual {v4, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v13

    move-object v0, v13

    check-cast v0, Lcom/ibm/icu/impl/ICUResourceBundle;

    move-object v4, v0

    .line 738
    const/4 v13, 0x0

    invoke-virtual {v4, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currencyPattern:Ljava/lang/String;

    .line 739
    const/4 v13, 0x1

    invoke-virtual {v4, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/String;->charAt(I)C

    move-result v13

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->monetarySeparator:C

    .line 740
    const/4 v13, 0x2

    invoke-virtual {v4, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/String;->charAt(I)C

    move-result v13

    move-object/from16 v0, p0

    iput-char v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->monetaryGroupingSeparator:C
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 750
    .end local v3    # "currencyCode":Ljava/lang/String;
    .end local v4    # "currencyRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v11    # "resource":Lcom/ibm/icu/impl/ICUResourceBundle;
    :cond_1
    :goto_2
    return-void

    .end local v2    # "curr":Lcom/ibm/icu/util/Currency;
    .restart local v7    # "isChoiceFormat":[Z
    :cond_2
    move-object v13, v5

    .line 717
    goto :goto_0

    .line 721
    .end local v7    # "isChoiceFormat":[Z
    :cond_3
    const-string/jumbo v13, "XXX"

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->intlCurrencySymbol:Ljava/lang/String;

    .line 722
    const-string/jumbo v13, "\u00a4"

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currencySymbol:Ljava/lang/String;

    goto :goto_1

    .line 742
    .restart local v2    # "curr":Lcom/ibm/icu/util/Currency;
    .restart local v3    # "currencyCode":Ljava/lang/String;
    .restart local v4    # "currencyRes":Lcom/ibm/icu/impl/ICUResourceBundle;
    .restart local v11    # "resource":Lcom/ibm/icu/impl/ICUResourceBundle;
    :catch_0
    move-exception v13

    goto :goto_2
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 3
    .param p1, "stream"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x5

    .line 766
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 769
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->serialVersionOnStream:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 772
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->decimalSeparator:C

    iput-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->monetarySeparator:C

    .line 773
    const/16 v0, 0x45

    iput-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->exponential:C

    .line 775
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->serialVersionOnStream:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 776
    const/16 v0, 0x2a

    iput-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->padEscape:C

    .line 777
    const/16 v0, 0x2b

    iput-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->plusSign:C

    .line 778
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->exponential:C

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->exponentSeparator:Ljava/lang/String;

    .line 785
    :cond_1
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->serialVersionOnStream:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_2

    .line 791
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->requestedLocale:Ljava/util/Locale;

    .line 793
    :cond_2
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->serialVersionOnStream:I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_3

    .line 795
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->requestedLocale:Ljava/util/Locale;

    invoke-static {v0}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->ulocale:Lcom/ibm/icu/util/ULocale;

    .line 797
    :cond_3
    iget v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->serialVersionOnStream:I

    if-ge v0, v2, :cond_4

    .line 799
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->groupingSeparator:C

    iput-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->monetaryGroupingSeparator:C

    .line 801
    :cond_4
    iput v2, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->serialVersionOnStream:I

    .line 804
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->intlCurrencySymbol:Ljava/lang/String;

    invoke-static {v0}, Lcom/ibm/icu/util/Currency;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/util/Currency;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currency:Lcom/ibm/icu/util/Currency;

    .line 805
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 599
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/DecimalFormatSymbols;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 601
    :catch_0
    move-exception v0

    .line 603
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 613
    if-nez p1, :cond_1

    move v1, v2

    .line 616
    :cond_0
    :goto_0
    return v1

    .line 614
    :cond_1
    if-eq p0, p1, :cond_0

    move-object v0, p1

    .line 615
    check-cast v0, Lcom/ibm/icu/text/DecimalFormatSymbols;

    .line 616
    .local v0, "other":Lcom/ibm/icu/text/DecimalFormatSymbols;
    iget-char v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->zeroDigit:C

    iget-char v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->zeroDigit:C

    if-ne v3, v4, :cond_2

    iget-char v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->groupingSeparator:C

    iget-char v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->groupingSeparator:C

    if-ne v3, v4, :cond_2

    iget-char v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->decimalSeparator:C

    iget-char v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->decimalSeparator:C

    if-ne v3, v4, :cond_2

    iget-char v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->percent:C

    iget-char v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->percent:C

    if-ne v3, v4, :cond_2

    iget-char v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->perMill:C

    iget-char v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->perMill:C

    if-ne v3, v4, :cond_2

    iget-char v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->digit:C

    iget-char v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->digit:C

    if-ne v3, v4, :cond_2

    iget-char v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->minusSign:C

    iget-char v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->minusSign:C

    if-ne v3, v4, :cond_2

    iget-char v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->patternSeparator:C

    iget-char v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->patternSeparator:C

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->infinity:Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->infinity:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->NaN:Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->NaN:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currencySymbol:Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currencySymbol:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->intlCurrencySymbol:Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->intlCurrencySymbol:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-char v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->padEscape:C

    iget-char v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->padEscape:C

    if-ne v3, v4, :cond_2

    iget-char v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->plusSign:C

    iget-char v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->plusSign:C

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->exponentSeparator:Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->exponentSeparator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-char v3, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->monetarySeparator:C

    iget-char v4, v0, Lcom/ibm/icu/text/DecimalFormatSymbols;->monetarySeparator:C

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public getCurrency()Lcom/ibm/icu/util/Currency;
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currency:Lcom/ibm/icu/util/Currency;

    return-object v0
.end method

.method getCurrencyPattern()Ljava/lang/String;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currencyPattern:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrencySymbol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currencySymbol:Ljava/lang/String;

    return-object v0
.end method

.method public getDecimalSeparator()C
    .locals 1

    .prologue
    .line 213
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->decimalSeparator:C

    return v0
.end method

.method public getDigit()C
    .locals 1

    .prologue
    .line 267
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->digit:C

    return v0
.end method

.method public getExponentSeparator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->exponentSeparator:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupingSeparator()C
    .locals 1

    .prologue
    .line 195
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->groupingSeparator:C

    return v0
.end method

.method public getInfinity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->infinity:Ljava/lang/String;

    return-object v0
.end method

.method public getInternationalCurrencySymbol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->intlCurrencySymbol:Ljava/lang/String;

    return-object v0
.end method

.method public final getLocale(Lcom/ibm/icu/util/ULocale$Type;)Lcom/ibm/icu/util/ULocale;
    .locals 1
    .param p1, "type"    # Lcom/ibm/icu/util/ULocale$Type;

    .prologue
    .line 1042
    sget-object v0, Lcom/ibm/icu/util/ULocale;->ACTUAL_LOCALE:Lcom/ibm/icu/util/ULocale$Type;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->actualLocale:Lcom/ibm/icu/util/ULocale;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->validLocale:Lcom/ibm/icu/util/ULocale;

    goto :goto_0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->requestedLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public getMinusSign()C
    .locals 1

    .prologue
    .line 350
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->minusSign:C

    return v0
.end method

.method public getMonetaryDecimalSeparator()C
    .locals 1

    .prologue
    .line 446
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->monetarySeparator:C

    return v0
.end method

.method public getMonetaryGroupingSeparator()C
    .locals 1

    .prologue
    .line 456
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->monetaryGroupingSeparator:C

    return v0
.end method

.method public getNaN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->NaN:Ljava/lang/String;

    return-object v0
.end method

.method public getPadEscape()C
    .locals 1

    .prologue
    .line 556
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->padEscape:C

    return v0
.end method

.method public getPatternSeparator()C
    .locals 1

    .prologue
    .line 286
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->patternSeparator:C

    return v0
.end method

.method public getPerMill()C
    .locals 1

    .prologue
    .line 231
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->perMill:C

    return v0
.end method

.method public getPercent()C
    .locals 1

    .prologue
    .line 249
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->percent:C

    return v0
.end method

.method public getPlusSign()C
    .locals 1

    .prologue
    .line 525
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->plusSign:C

    return v0
.end method

.method public getSignificantDigit()C
    .locals 1

    .prologue
    .line 177
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->sigDigit:C

    return v0
.end method

.method public getULocale()Lcom/ibm/icu/util/ULocale;
    .locals 1

    .prologue
    .line 590
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->ulocale:Lcom/ibm/icu/util/ULocale;

    return-object v0
.end method

.method public getZeroDigit()C
    .locals 1

    .prologue
    .line 159
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->zeroDigit:C

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 639
    iget-char v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->zeroDigit:C

    .line 640
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x25

    iget-char v2, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->groupingSeparator:C

    add-int v0, v1, v2

    .line 641
    mul-int/lit8 v1, v0, 0x25

    iget-char v2, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->decimalSeparator:C

    add-int v0, v1, v2

    .line 642
    return v0
.end method

.method public setCurrency(Lcom/ibm/icu/util/Currency;)V
    .locals 1
    .param p1, "currency"    # Lcom/ibm/icu/util/Currency;

    .prologue
    .line 431
    if-nez p1, :cond_0

    .line 432
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 434
    :cond_0
    iput-object p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currency:Lcom/ibm/icu/util/Currency;

    .line 435
    invoke-virtual {p1}, Lcom/ibm/icu/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->intlCurrencySymbol:Ljava/lang/String;

    .line 436
    iget-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->requestedLocale:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Lcom/ibm/icu/util/Currency;->getSymbol(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currencySymbol:Ljava/lang/String;

    .line 437
    return-void
.end method

.method public setCurrencySymbol(Ljava/lang/String;)V
    .locals 0
    .param p1, "currency"    # Ljava/lang/String;

    .prologue
    .line 381
    iput-object p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->currencySymbol:Ljava/lang/String;

    .line 382
    return-void
.end method

.method public setDecimalSeparator(C)V
    .locals 0
    .param p1, "decimalSeparator"    # C

    .prologue
    .line 222
    iput-char p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->decimalSeparator:C

    .line 223
    return-void
.end method

.method public setDigit(C)V
    .locals 0
    .param p1, "digit"    # C

    .prologue
    .line 276
    iput-char p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->digit:C

    .line 277
    return-void
.end method

.method public setExponentSeparator(Ljava/lang/String;)V
    .locals 0
    .param p1, "exp"    # Ljava/lang/String;

    .prologue
    .line 511
    iput-object p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->exponentSeparator:Ljava/lang/String;

    .line 512
    return-void
.end method

.method public setGroupingSeparator(C)V
    .locals 0
    .param p1, "groupingSeparator"    # C

    .prologue
    .line 204
    iput-char p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->groupingSeparator:C

    .line 205
    return-void
.end method

.method public setInfinity(Ljava/lang/String;)V
    .locals 0
    .param p1, "infinity"    # Ljava/lang/String;

    .prologue
    .line 318
    iput-object p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->infinity:Ljava/lang/String;

    .line 319
    return-void
.end method

.method public setInternationalCurrencySymbol(Ljava/lang/String;)V
    .locals 0
    .param p1, "currency"    # Ljava/lang/String;

    .prologue
    .line 401
    iput-object p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->intlCurrencySymbol:Ljava/lang/String;

    .line 402
    return-void
.end method

.method final setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V
    .locals 3
    .param p1, "valid"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "actual"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1065
    if-nez p1, :cond_0

    move v2, v0

    :goto_0
    if-nez p2, :cond_1

    :goto_1
    if-eq v2, v0, :cond_2

    .line 1067
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v2, v1

    .line 1065
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 1072
    :cond_2
    iput-object p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->validLocale:Lcom/ibm/icu/util/ULocale;

    .line 1073
    iput-object p2, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->actualLocale:Lcom/ibm/icu/util/ULocale;

    .line 1074
    return-void
.end method

.method public setMinusSign(C)V
    .locals 0
    .param p1, "minusSign"    # C

    .prologue
    .line 361
    iput-char p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->minusSign:C

    .line 362
    return-void
.end method

.method public setMonetaryDecimalSeparator(C)V
    .locals 0
    .param p1, "sep"    # C

    .prologue
    .line 474
    iput-char p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->monetarySeparator:C

    .line 475
    return-void
.end method

.method public setMonetaryGroupingSeparator(C)V
    .locals 0
    .param p1, "sep"    # C

    .prologue
    .line 483
    iput-char p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->monetaryGroupingSeparator:C

    .line 484
    return-void
.end method

.method public setNaN(Ljava/lang/String;)V
    .locals 0
    .param p1, "NaN"    # Ljava/lang/String;

    .prologue
    .line 339
    iput-object p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->NaN:Ljava/lang/String;

    .line 340
    return-void
.end method

.method public setPadEscape(C)V
    .locals 0
    .param p1, "c"    # C

    .prologue
    .line 572
    iput-char p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->padEscape:C

    .line 573
    return-void
.end method

.method public setPatternSeparator(C)V
    .locals 0
    .param p1, "patternSeparator"    # C

    .prologue
    .line 296
    iput-char p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->patternSeparator:C

    .line 297
    return-void
.end method

.method public setPerMill(C)V
    .locals 0
    .param p1, "perMill"    # C

    .prologue
    .line 240
    iput-char p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->perMill:C

    .line 241
    return-void
.end method

.method public setPercent(C)V
    .locals 0
    .param p1, "percent"    # C

    .prologue
    .line 258
    iput-char p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->percent:C

    .line 259
    return-void
.end method

.method public setPlusSign(C)V
    .locals 0
    .param p1, "plus"    # C

    .prologue
    .line 539
    iput-char p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->plusSign:C

    .line 540
    return-void
.end method

.method public setSignificantDigit(C)V
    .locals 0
    .param p1, "sigDigit"    # C

    .prologue
    .line 186
    iput-char p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->sigDigit:C

    .line 187
    return-void
.end method

.method public setZeroDigit(C)V
    .locals 0
    .param p1, "zeroDigit"    # C

    .prologue
    .line 168
    iput-char p1, p0, Lcom/ibm/icu/text/DecimalFormatSymbols;->zeroDigit:C

    .line 169
    return-void
.end method
