.class Lcom/ibm/icu/text/RBBIRuleBuilder;
.super Ljava/lang/Object;
.source "RBBIRuleBuilder.java"


# static fields
.field static final U_BRK_ASSIGN_ERROR:I = 0x10206

.field static final U_BRK_ERROR_LIMIT:I = 0x10210

.field static final U_BRK_ERROR_START:I = 0x10200

.field static final U_BRK_HEX_DIGITS_EXPECTED:I = 0x10202

.field static final U_BRK_INIT_ERROR:I = 0x1020b

.field static final U_BRK_INTERNAL_ERROR:I = 0x10201

.field static final U_BRK_MALFORMED_RULE_TAG:I = 0x1020e

.field static final U_BRK_MALFORMED_SET:I = 0x1020f

.field static final U_BRK_MISMATCHED_PAREN:I = 0x10208

.field static final U_BRK_NEW_LINE_IN_QUOTED_STRING:I = 0x10209

.field static final U_BRK_RULE_EMPTY_SET:I = 0x1020c

.field static final U_BRK_RULE_SYNTAX:I = 0x10204

.field static final U_BRK_SEMICOLON_EXPECTED:I = 0x10203

.field static final U_BRK_UNCLOSED_SET:I = 0x10205

.field static final U_BRK_UNDEFINED_VARIABLE:I = 0x1020a

.field static final U_BRK_UNRECOGNIZED_OPTION:I = 0x1020d

.field static final U_BRK_VARIABLE_REDFINITION:I = 0x10207

.field static final fForwardTree:I = 0x0

.field static final fReverseTree:I = 0x1

.field static final fSafeFwdTree:I = 0x2

.field static final fSafeRevTree:I = 0x3


# instance fields
.field fChainRules:Z

.field fDebugEnv:Ljava/lang/String;

.field fDefaultTree:I

.field fForwardTables:Lcom/ibm/icu/text/RBBITableBuilder;

.field fLBCMNoChain:Z

.field fLookAheadHardBreak:Z

.field fReverseTables:Lcom/ibm/icu/text/RBBITableBuilder;

.field fRuleStatusVals:Ljava/util/List;

.field fRules:Ljava/lang/String;

.field fSafeFwdTables:Lcom/ibm/icu/text/RBBITableBuilder;

.field fSafeRevTables:Lcom/ibm/icu/text/RBBITableBuilder;

.field fScanner:Lcom/ibm/icu/text/RBBIRuleScanner;

.field fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

.field fStatusSets:Ljava/util/Map;

.field fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

.field fUSetNodes:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "rules"    # Ljava/lang/String;

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/ibm/icu/text/RBBINode;

    iput-object v0, p0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDefaultTree:I

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fStatusSets:Ljava/util/Map;

    .line 136
    const-string/jumbo v0, "rbbi"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "rbbi"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUDebug;->value(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    .line 138
    iput-object p1, p0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fUSetNodes:Ljava/util/List;

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRuleStatusVals:Ljava/util/List;

    .line 141
    new-instance v0, Lcom/ibm/icu/text/RBBIRuleScanner;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/RBBIRuleScanner;-><init>(Lcom/ibm/icu/text/RBBIRuleBuilder;)V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fScanner:Lcom/ibm/icu/text/RBBIRuleScanner;

    .line 142
    new-instance v0, Lcom/ibm/icu/text/RBBISetBuilder;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/RBBISetBuilder;-><init>(Lcom/ibm/icu/text/RBBIRuleBuilder;)V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    .line 143
    return-void

    .line 136
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final align8(I)I
    .locals 1
    .param p0, "i"    # I

    .prologue
    .line 155
    add-int/lit8 v0, p0, 0x7

    and-int/lit8 v0, v0, -0x8

    return v0
.end method

.method static compileRules(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 3
    .param p0, "rules"    # Ljava/lang/String;
    .param p1, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 299
    new-instance v0, Lcom/ibm/icu/text/RBBIRuleBuilder;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/RBBIRuleBuilder;-><init>(Ljava/lang/String;)V

    .line 300
    .local v0, "builder":Lcom/ibm/icu/text/RBBIRuleBuilder;
    iget-object v1, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fScanner:Lcom/ibm/icu/text/RBBIRuleScanner;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->parse()V

    .line 308
    iget-object v1, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RBBISetBuilder;->build()V

    .line 313
    new-instance v1, Lcom/ibm/icu/text/RBBITableBuilder;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/ibm/icu/text/RBBITableBuilder;-><init>(Lcom/ibm/icu/text/RBBIRuleBuilder;I)V

    iput-object v1, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fForwardTables:Lcom/ibm/icu/text/RBBITableBuilder;

    .line 314
    new-instance v1, Lcom/ibm/icu/text/RBBITableBuilder;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lcom/ibm/icu/text/RBBITableBuilder;-><init>(Lcom/ibm/icu/text/RBBIRuleBuilder;I)V

    iput-object v1, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fReverseTables:Lcom/ibm/icu/text/RBBITableBuilder;

    .line 315
    new-instance v1, Lcom/ibm/icu/text/RBBITableBuilder;

    const/4 v2, 0x2

    invoke-direct {v1, v0, v2}, Lcom/ibm/icu/text/RBBITableBuilder;-><init>(Lcom/ibm/icu/text/RBBIRuleBuilder;I)V

    iput-object v1, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSafeFwdTables:Lcom/ibm/icu/text/RBBITableBuilder;

    .line 316
    new-instance v1, Lcom/ibm/icu/text/RBBITableBuilder;

    const/4 v2, 0x3

    invoke-direct {v1, v0, v2}, Lcom/ibm/icu/text/RBBITableBuilder;-><init>(Lcom/ibm/icu/text/RBBIRuleBuilder;I)V

    iput-object v1, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSafeRevTables:Lcom/ibm/icu/text/RBBITableBuilder;

    .line 317
    iget-object v1, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fForwardTables:Lcom/ibm/icu/text/RBBITableBuilder;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RBBITableBuilder;->build()V

    .line 318
    iget-object v1, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fReverseTables:Lcom/ibm/icu/text/RBBITableBuilder;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RBBITableBuilder;->build()V

    .line 319
    iget-object v1, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSafeFwdTables:Lcom/ibm/icu/text/RBBITableBuilder;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RBBITableBuilder;->build()V

    .line 320
    iget-object v1, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSafeRevTables:Lcom/ibm/icu/text/RBBITableBuilder;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RBBITableBuilder;->build()V

    .line 321
    iget-object v1, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v2, "states"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_0

    .line 323
    iget-object v1, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fForwardTables:Lcom/ibm/icu/text/RBBITableBuilder;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RBBITableBuilder;->printRuleStatusTable()V

    .line 330
    :cond_0
    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/RBBIRuleBuilder;->flattenData(Ljava/io/OutputStream;)V

    .line 331
    return-void
.end method


# virtual methods
.method flattenData(Ljava/io/OutputStream;)V
    .locals 22
    .param p1, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    new-instance v3, Ljava/io/DataOutputStream;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 163
    .local v3, "dos":Ljava/io/DataOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/text/RBBIRuleScanner;->stripRules(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 170
    .local v14, "strippedRules":Ljava/lang/String;
    const/16 v6, 0x60

    .line 171
    .local v6, "headerSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fForwardTables:Lcom/ibm/icu/text/RBBITableBuilder;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/text/RBBITableBuilder;->getTableSize()I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/text/RBBIRuleBuilder;->align8(I)I

    move-result v4

    .line 172
    .local v4, "forwardTableSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fReverseTables:Lcom/ibm/icu/text/RBBITableBuilder;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/text/RBBITableBuilder;->getTableSize()I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/text/RBBIRuleBuilder;->align8(I)I

    move-result v9

    .line 173
    .local v9, "reverseTableSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSafeFwdTables:Lcom/ibm/icu/text/RBBITableBuilder;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/text/RBBITableBuilder;->getTableSize()I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/text/RBBIRuleBuilder;->align8(I)I

    move-result v11

    .line 174
    .local v11, "safeFwdTableSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSafeRevTables:Lcom/ibm/icu/text/RBBITableBuilder;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/text/RBBITableBuilder;->getTableSize()I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/text/RBBIRuleBuilder;->align8(I)I

    move-result v12

    .line 175
    .local v12, "safeRevTableSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/text/RBBISetBuilder;->getTrieSize()I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/text/RBBIRuleBuilder;->align8(I)I

    move-result v17

    .line 176
    .local v17, "trieSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRuleStatusVals:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    mul-int/lit8 v19, v19, 0x4

    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/text/RBBIRuleBuilder;->align8(I)I

    move-result v13

    .line 177
    .local v13, "statusTableSize":I
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v19

    mul-int/lit8 v19, v19, 0x2

    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/text/RBBIRuleBuilder;->align8(I)I

    move-result v10

    .line 178
    .local v10, "rulesSize":I
    add-int v19, v6, v4

    add-int v19, v19, v9

    add-int v19, v19, v11

    add-int v19, v19, v12

    add-int v19, v19, v13

    add-int v19, v19, v17

    add-int v16, v19, v10

    .line 181
    .local v16, "totalSize":I
    const/4 v8, 0x0

    .line 189
    .local v8, "outputPos":I
    const/16 v19, 0x80

    move/from16 v0, v19

    new-array v2, v0, [B

    .line 190
    .local v2, "ICUDataHeader":[B
    invoke-virtual {v3, v2}, Ljava/io/DataOutputStream;->write([B)V

    .line 195
    const/16 v19, 0x18

    move/from16 v0, v19

    new-array v5, v0, [I

    .line 196
    .local v5, "header":[I
    const/16 v19, 0x0

    const v20, 0xb1a0

    aput v20, v5, v19

    .line 197
    const/16 v19, 0x1

    const/high16 v20, 0x3010000

    aput v20, v5, v19

    .line 198
    const/16 v19, 0x2

    aput v16, v5, v19

    .line 199
    const/16 v19, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/ibm/icu/text/RBBISetBuilder;->getNumCharCategories()I

    move-result v20

    aput v20, v5, v19

    .line 200
    const/16 v19, 0x4

    aput v6, v5, v19

    .line 201
    const/16 v19, 0x5

    aput v4, v5, v19

    .line 202
    const/16 v19, 0x6

    const/16 v20, 0x4

    aget v20, v5, v20

    add-int v20, v20, v4

    aput v20, v5, v19

    .line 203
    const/16 v19, 0x7

    aput v9, v5, v19

    .line 204
    const/16 v19, 0x8

    const/16 v20, 0x6

    aget v20, v5, v20

    add-int v20, v20, v9

    aput v20, v5, v19

    .line 206
    const/16 v19, 0x9

    aput v11, v5, v19

    .line 207
    const/16 v19, 0xa

    const/16 v20, 0x8

    aget v20, v5, v20

    add-int v20, v20, v11

    aput v20, v5, v19

    .line 209
    const/16 v19, 0xb

    aput v12, v5, v19

    .line 210
    const/16 v19, 0xc

    const/16 v20, 0xa

    aget v20, v5, v20

    add-int v20, v20, v12

    aput v20, v5, v19

    .line 212
    const/16 v19, 0xd

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/ibm/icu/text/RBBISetBuilder;->getTrieSize()I

    move-result v20

    aput v20, v5, v19

    .line 213
    const/16 v19, 0x10

    const/16 v20, 0xc

    aget v20, v5, v20

    const/16 v21, 0xd

    aget v21, v5, v21

    add-int v20, v20, v21

    aput v20, v5, v19

    .line 215
    const/16 v19, 0x11

    aput v13, v5, v19

    .line 216
    const/16 v19, 0xe

    const/16 v20, 0x10

    aget v20, v5, v20

    add-int v20, v20, v13

    aput v20, v5, v19

    .line 218
    const/16 v19, 0xf

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v20

    mul-int/lit8 v20, v20, 0x2

    aput v20, v5, v19

    .line 219
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    array-length v0, v5

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v7, v0, :cond_0

    .line 220
    aget v19, v5, v7

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 221
    add-int/lit8 v8, v8, 0x4

    .line 219
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 226
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fForwardTables:Lcom/ibm/icu/text/RBBITableBuilder;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/text/RBBITableBuilder;->exportTable()[S

    move-result-object v15

    .line 227
    .local v15, "tableData":[S
    const/16 v19, 0x4

    aget v19, v5, v19

    move/from16 v0, v19

    if-ne v8, v0, :cond_1

    const/16 v19, 0x1

    :goto_1
    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 228
    const/4 v7, 0x0

    :goto_2
    array-length v0, v15

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v7, v0, :cond_2

    .line 229
    aget-short v19, v15, v7

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 230
    add-int/lit8 v8, v8, 0x2

    .line 228
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 227
    :cond_1
    const/16 v19, 0x0

    goto :goto_1

    .line 233
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fReverseTables:Lcom/ibm/icu/text/RBBITableBuilder;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/text/RBBITableBuilder;->exportTable()[S

    move-result-object v15

    .line 234
    const/16 v19, 0x6

    aget v19, v5, v19

    move/from16 v0, v19

    if-ne v8, v0, :cond_3

    const/16 v19, 0x1

    :goto_3
    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 235
    const/4 v7, 0x0

    :goto_4
    array-length v0, v15

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v7, v0, :cond_4

    .line 236
    aget-short v19, v15, v7

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 237
    add-int/lit8 v8, v8, 0x2

    .line 235
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 234
    :cond_3
    const/16 v19, 0x0

    goto :goto_3

    .line 240
    :cond_4
    const/16 v19, 0x8

    aget v19, v5, v19

    move/from16 v0, v19

    if-ne v8, v0, :cond_5

    const/16 v19, 0x1

    :goto_5
    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSafeFwdTables:Lcom/ibm/icu/text/RBBITableBuilder;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/text/RBBITableBuilder;->exportTable()[S

    move-result-object v15

    .line 242
    const/4 v7, 0x0

    :goto_6
    array-length v0, v15

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v7, v0, :cond_6

    .line 243
    aget-short v19, v15, v7

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 244
    add-int/lit8 v8, v8, 0x2

    .line 242
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 240
    :cond_5
    const/16 v19, 0x0

    goto :goto_5

    .line 247
    :cond_6
    const/16 v19, 0xa

    aget v19, v5, v19

    move/from16 v0, v19

    if-ne v8, v0, :cond_7

    const/16 v19, 0x1

    :goto_7
    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSafeRevTables:Lcom/ibm/icu/text/RBBITableBuilder;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/ibm/icu/text/RBBITableBuilder;->exportTable()[S

    move-result-object v15

    .line 249
    const/4 v7, 0x0

    :goto_8
    array-length v0, v15

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v7, v0, :cond_8

    .line 250
    aget-short v19, v15, v7

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 251
    add-int/lit8 v8, v8, 0x2

    .line 249
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 247
    :cond_7
    const/16 v19, 0x0

    goto :goto_7

    .line 255
    :cond_8
    const/16 v19, 0xc

    aget v19, v5, v19

    move/from16 v0, v19

    if-ne v8, v0, :cond_9

    const/16 v19, 0x1

    :goto_9
    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fSetBuilder:Lcom/ibm/icu/text/RBBISetBuilder;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBISetBuilder;->serializeTrie(Ljava/io/OutputStream;)V

    .line 257
    const/16 v19, 0xd

    aget v19, v5, v19

    add-int v8, v8, v19

    .line 258
    :goto_a
    rem-int/lit8 v19, v8, 0x8

    if-eqz v19, :cond_a

    .line 259
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->write(I)V

    .line 260
    add-int/lit8 v8, v8, 0x1

    .line 261
    goto :goto_a

    .line 255
    :cond_9
    const/16 v19, 0x0

    goto :goto_9

    .line 264
    :cond_a
    const/16 v19, 0x10

    aget v19, v5, v19

    move/from16 v0, v19

    if-ne v8, v0, :cond_b

    const/16 v19, 0x1

    :goto_b
    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 265
    const/4 v7, 0x0

    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRuleStatusVals:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v7, v0, :cond_c

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRuleStatusVals:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    .line 267
    .local v18, "val":Ljava/lang/Integer;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 268
    add-int/lit8 v8, v8, 0x4

    .line 265
    add-int/lit8 v7, v7, 0x1

    goto :goto_c

    .line 264
    .end local v18    # "val":Ljava/lang/Integer;
    :cond_b
    const/16 v19, 0x0

    goto :goto_b

    .line 271
    :cond_c
    :goto_d
    rem-int/lit8 v19, v8, 0x8

    if-eqz v19, :cond_d

    .line 272
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->write(I)V

    .line 273
    add-int/lit8 v8, v8, 0x1

    .line 274
    goto :goto_d

    .line 278
    :cond_d
    const/16 v19, 0xe

    aget v19, v5, v19

    move/from16 v0, v19

    if-ne v8, v0, :cond_e

    const/16 v19, 0x1

    :goto_e
    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 279
    invoke-virtual {v3, v14}, Ljava/io/DataOutputStream;->writeChars(Ljava/lang/String;)V

    .line 280
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v19

    mul-int/lit8 v19, v19, 0x2

    add-int v8, v8, v19

    .line 281
    :goto_f
    rem-int/lit8 v19, v8, 0x8

    if-eqz v19, :cond_f

    .line 282
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->write(I)V

    .line 283
    add-int/lit8 v8, v8, 0x1

    .line 284
    goto :goto_f

    .line 278
    :cond_e
    const/16 v19, 0x0

    goto :goto_e

    .line 285
    :cond_f
    return-void
.end method
