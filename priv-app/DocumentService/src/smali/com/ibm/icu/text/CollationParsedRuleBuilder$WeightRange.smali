.class Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;
.super Ljava/lang/Object;
.source "CollationParsedRuleBuilder.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CollationParsedRuleBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WeightRange"
.end annotation


# instance fields
.field m_count2_:I

.field m_count_:I

.field m_end_:I

.field m_length2_:I

.field m_length_:I

.field m_start_:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 795
    invoke-virtual {p0}, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->clear()V

    .line 796
    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;)V
    .locals 1
    .param p1, "source"    # Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    .prologue
    .line 804
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 805
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    .line 806
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    .line 807
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length_:I

    .line 808
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    .line 809
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length2_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length2_:I

    .line 810
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count2_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count2_:I

    .line 811
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 774
    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    .line 775
    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_end_:I

    .line 776
    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length_:I

    .line 777
    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count_:I

    .line 778
    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_length2_:I

    .line 779
    iput v0, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_count2_:I

    .line 780
    return-void
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 3
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 756
    if-ne p0, p1, :cond_1

    .line 766
    .end local p1    # "target":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v1

    .line 759
    .restart local p1    # "target":Ljava/lang/Object;
    :cond_1
    check-cast p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;

    .end local p1    # "target":Ljava/lang/Object;
    iget v0, p1, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    .line 760
    .local v0, "tstart":I
    iget v2, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    if-eq v2, v0, :cond_0

    .line 763
    iget v1, p0, Lcom/ibm/icu/text/CollationParsedRuleBuilder$WeightRange;->m_start_:I

    if-le v1, v0, :cond_2

    .line 764
    const/4 v1, 0x1

    goto :goto_0

    .line 766
    :cond_2
    const/4 v1, -0x1

    goto :goto_0
.end method
