.class final Lcom/ibm/icu/text/Normalizer$NFKCMode;
.super Lcom/ibm/icu/text/Normalizer$Mode;
.source "Normalizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/Normalizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "NFKCMode"
.end annotation


# direct methods
.method private constructor <init>(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 458
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/Normalizer$Mode;-><init>(ILcom/ibm/icu/text/Normalizer$1;)V

    .line 459
    return-void
.end method

.method constructor <init>(ILcom/ibm/icu/text/Normalizer$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Lcom/ibm/icu/text/Normalizer$1;

    .prologue
    .line 456
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/Normalizer$NFKCMode;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected getMask()I
    .locals 1

    .prologue
    .line 483
    const v0, 0xff22

    return v0
.end method

.method protected getMinC()I
    .locals 1

    .prologue
    .line 472
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/ibm/icu/impl/NormalizerImpl;->getFromIndexesArr(I)I

    move-result v0

    return v0
.end method

.method protected getNextBoundary()Lcom/ibm/icu/text/Normalizer$IsNextBoundary;
    .locals 2

    .prologue
    .line 480
    new-instance v0, Lcom/ibm/icu/text/Normalizer$IsNextTrueStarter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/Normalizer$IsNextTrueStarter;-><init>(Lcom/ibm/icu/text/Normalizer$1;)V

    return-object v0
.end method

.method protected getPrevBoundary()Lcom/ibm/icu/text/Normalizer$IsPrevBoundary;
    .locals 2

    .prologue
    .line 477
    new-instance v0, Lcom/ibm/icu/text/Normalizer$IsPrevTrueStarter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/Normalizer$IsPrevTrueStarter;-><init>(Lcom/ibm/icu/text/Normalizer$1;)V

    return-object v0
.end method

.method protected isNFSkippable(I)Z
    .locals 2
    .param p1, "c"    # I

    .prologue
    .line 500
    const-wide/32 v0, 0xffc2

    invoke-static {p1, p0, v0, v1}, Lcom/ibm/icu/impl/NormalizerImpl;->isNFSkippable(ILcom/ibm/icu/text/Normalizer$Mode;J)Z

    move-result v0

    return v0
.end method

.method protected normalize([CII[CIILcom/ibm/icu/text/UnicodeSet;)I
    .locals 8
    .param p1, "src"    # [C
    .param p2, "srcStart"    # I
    .param p3, "srcLimit"    # I
    .param p4, "dest"    # [C
    .param p5, "destStart"    # I
    .param p6, "destLimit"    # I
    .param p7, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 463
    const/16 v6, 0x1000

    move-object v0, p1

    move v1, p2

    move v2, p3

    move-object v3, p4

    move v4, p5

    move v5, p6

    move-object v7, p7

    invoke-static/range {v0 .. v7}, Lcom/ibm/icu/impl/NormalizerImpl;->compose([CII[CIIILcom/ibm/icu/text/UnicodeSet;)I

    move-result v0

    return v0
.end method

.method protected normalize(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1, "src"    # Ljava/lang/String;
    .param p2, "options"    # I

    .prologue
    .line 469
    const/4 v0, 0x1

    invoke-static {p1, v0, p2}, Lcom/ibm/icu/text/Normalizer;->compose(Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected quickCheck([CIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;
    .locals 8
    .param p1, "src"    # [C
    .param p2, "start"    # I
    .param p3, "limit"    # I
    .param p4, "allowMaybe"    # Z
    .param p5, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 488
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/ibm/icu/impl/NormalizerImpl;->getFromIndexesArr(I)I

    move-result v3

    const/16 v4, 0x22

    const/16 v5, 0x1000

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v6, p4

    move-object v7, p5

    invoke-static/range {v0 .. v7}, Lcom/ibm/icu/impl/NormalizerImpl;->quickCheck([CIIIIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    move-result-object v0

    return-object v0
.end method
