.class Lcom/ibm/icu/text/CollationRuleParser$TokenOption;
.super Ljava/lang/Object;
.source "CollationRuleParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CollationRuleParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TokenOption"
.end annotation


# instance fields
.field private m_attribute_:I

.field private m_name_:Ljava/lang/String;

.field private m_subOptionAttributeValues_:[I

.field private m_subOptions_:[Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;I[Ljava/lang/String;[I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "attribute"    # I
    .param p3, "suboptions"    # [Ljava/lang/String;
    .param p4, "suboptionattributevalue"    # [I

    .prologue
    .line 360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361
    iput-object p1, p0, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->m_name_:Ljava/lang/String;

    .line 362
    iput p2, p0, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->m_attribute_:I

    .line 363
    iput-object p3, p0, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->m_subOptions_:[Ljava/lang/String;

    .line 364
    iput-object p4, p0, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->m_subOptionAttributeValues_:[I

    .line 365
    return-void
.end method

.method static access$000(Lcom/ibm/icu/text/CollationRuleParser$TokenOption;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->m_name_:Ljava/lang/String;

    return-object v0
.end method

.method static access$100(Lcom/ibm/icu/text/CollationRuleParser$TokenOption;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->m_subOptions_:[Ljava/lang/String;

    return-object v0
.end method

.method static access$200(Lcom/ibm/icu/text/CollationRuleParser$TokenOption;)I
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    .prologue
    .line 354
    iget v0, p0, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->m_attribute_:I

    return v0
.end method

.method static access$300(Lcom/ibm/icu/text/CollationRuleParser$TokenOption;)[I
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/text/CollationRuleParser$TokenOption;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/ibm/icu/text/CollationRuleParser$TokenOption;->m_subOptionAttributeValues_:[I

    return-object v0
.end method
