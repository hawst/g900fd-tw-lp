.class public abstract Lcom/ibm/icu/text/Transliterator;
.super Ljava/lang/Object;
.source "Transliterator.java"

# interfaces
.implements Lcom/ibm/icu/text/StringTransform;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/Transliterator$Factory;,
        Lcom/ibm/icu/text/Transliterator$Position;
    }
.end annotation


# static fields
.field static final DEBUG:Z = false

.field public static final FORWARD:I = 0x0

.field static final ID_DELIM:C = ';'

.field static final ID_SEP:C = '-'

.field private static final INDEX:Ljava/lang/String; = "index"

.field private static final RB_DISPLAY_NAME_PATTERN:Ljava/lang/String; = "TransliteratorNamePattern"

.field private static final RB_DISPLAY_NAME_PREFIX:Ljava/lang/String; = "%Translit%%"

.field private static final RB_RULE_BASED_IDS:Ljava/lang/String; = "RuleBasedTransliteratorIDs"

.field private static final RB_SCRIPT_DISPLAY_NAME_PREFIX:Ljava/lang/String; = "%Translit%"

.field public static final REVERSE:I = 0x1

.field static final VARIANT_SEP:C = '/'

.field static class$com$ibm$icu$text$NullTransliterator:Ljava/lang/Class;

.field private static displayNameCache:Ljava/util/Hashtable;

.field private static registry:Lcom/ibm/icu/text/TransliteratorRegistry;


# instance fields
.field private ID:Ljava/lang/String;

.field private filter:Lcom/ibm/icu/text/UnicodeFilter;

.field private maximumContextLength:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v14, 0x1

    const/4 v15, 0x0

    .line 1785
    new-instance v0, Lcom/ibm/icu/text/TransliteratorRegistry;

    invoke-direct {v0}, Lcom/ibm/icu/text/TransliteratorRegistry;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/Transliterator;->registry:Lcom/ibm/icu/text/TransliteratorRegistry;

    .line 1788
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/Transliterator;->displayNameCache:Ljava/util/Hashtable;

    .line 1824
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b/translit"

    const-string/jumbo v3, "index"

    invoke-static {v0, v3}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v6

    .line 1825
    .local v6, "bundle":Lcom/ibm/icu/util/UResourceBundle;
    const-string/jumbo v0, "RuleBasedTransliteratorIDs"

    invoke-virtual {v6, v0}, Lcom/ibm/icu/util/UResourceBundle;->get(Ljava/lang/String;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v12

    .line 1828
    .local v12, "transIDs":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v12}, Lcom/ibm/icu/util/UResourceBundle;->getSize()I

    move-result v9

    .line 1829
    .local v9, "maxRows":I
    const/4 v11, 0x0

    .local v11, "row":I
    :goto_0
    if-ge v11, v9, :cond_4

    .line 1830
    invoke-virtual {v12, v11}, Lcom/ibm/icu/util/UResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v7

    .line 1831
    .local v7, "colBund":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v7}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 1832
    .local v1, "ID":Ljava/lang/String;
    invoke-virtual {v7, v15}, Lcom/ibm/icu/util/UResourceBundle;->get(I)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v10

    .line 1833
    .local v10, "res":Lcom/ibm/icu/util/UResourceBundle;
    invoke-virtual {v10}, Lcom/ibm/icu/util/UResourceBundle;->getKey()Ljava/lang/String;

    move-result-object v13

    .line 1834
    .local v13, "type":Ljava/lang/String;
    const-string/jumbo v0, "file"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "internal"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1837
    :cond_0
    const-string/jumbo v0, "resource"

    invoke-virtual {v10, v0}, Lcom/ibm/icu/util/UResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1839
    .local v2, "resString":Ljava/lang/String;
    const-string/jumbo v0, "direction"

    invoke-virtual {v10, v0}, Lcom/ibm/icu/util/UResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1840
    .local v8, "direction":Ljava/lang/String;
    invoke-virtual {v8, v15}, Ljava/lang/String;->charAt(I)C

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1848
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "Can\'t parse direction: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1842
    :sswitch_0
    const/4 v4, 0x0

    .line 1850
    .local v4, "dir":I
    :goto_1
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->registry:Lcom/ibm/icu/text/TransliteratorRegistry;

    const-string/jumbo v3, "UTF-16"

    const-string/jumbo v5, "internal"

    invoke-virtual {v13, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    move v5, v14

    :goto_2
    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/text/TransliteratorRegistry;->put(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 1829
    .end local v4    # "dir":I
    .end local v8    # "direction":Ljava/lang/String;
    :goto_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 1845
    .restart local v8    # "direction":Ljava/lang/String;
    :sswitch_1
    const/4 v4, 0x1

    .line 1846
    .restart local v4    # "dir":I
    goto :goto_1

    :cond_1
    move v5, v15

    .line 1850
    goto :goto_2

    .line 1855
    .end local v2    # "resString":Ljava/lang/String;
    .end local v4    # "dir":I
    .end local v8    # "direction":Ljava/lang/String;
    :cond_2
    const-string/jumbo v0, "alias"

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1857
    invoke-virtual {v10}, Lcom/ibm/icu/util/UResourceBundle;->getString()Ljava/lang/String;

    move-result-object v2

    .line 1858
    .restart local v2    # "resString":Ljava/lang/String;
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->registry:Lcom/ibm/icu/text/TransliteratorRegistry;

    invoke-virtual {v0, v1, v2, v14}, Lcom/ibm/icu/text/TransliteratorRegistry;->put(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_3

    .line 1861
    .end local v2    # "resString":Ljava/lang/String;
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "Unknow type: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1865
    .end local v1    # "ID":Ljava/lang/String;
    .end local v7    # "colBund":Lcom/ibm/icu/util/UResourceBundle;
    .end local v10    # "res":Lcom/ibm/icu/util/UResourceBundle;
    .end local v13    # "type":Ljava/lang/String;
    :cond_4
    sget-object v0, Lcom/ibm/icu/text/NullTransliterator;->SHORT_ID:Ljava/lang/String;

    sget-object v3, Lcom/ibm/icu/text/NullTransliterator;->SHORT_ID:Ljava/lang/String;

    invoke-static {v0, v3, v15}, Lcom/ibm/icu/text/Transliterator;->registerSpecialInverse(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1868
    sget-object v3, Lcom/ibm/icu/text/NullTransliterator;->_ID:Ljava/lang/String;

    sget-object v0, Lcom/ibm/icu/text/Transliterator;->class$com$ibm$icu$text$NullTransliterator:Ljava/lang/Class;

    if-nez v0, :cond_5

    const-string/jumbo v0, "com.ibm.icu.text.NullTransliterator"

    invoke-static {v0}, Lcom/ibm/icu/text/Transliterator;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/ibm/icu/text/Transliterator;->class$com$ibm$icu$text$NullTransliterator:Ljava/lang/Class;

    :goto_4
    const/4 v5, 0x0

    invoke-static {v3, v0, v5}, Lcom/ibm/icu/text/Transliterator;->registerClass(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    .line 1870
    invoke-static {}, Lcom/ibm/icu/text/RemoveTransliterator;->register()V

    .line 1871
    invoke-static {}, Lcom/ibm/icu/text/EscapeTransliterator;->register()V

    .line 1872
    invoke-static {}, Lcom/ibm/icu/text/UnescapeTransliterator;->register()V

    .line 1873
    invoke-static {}, Lcom/ibm/icu/text/LowercaseTransliterator;->register()V

    .line 1874
    invoke-static {}, Lcom/ibm/icu/text/UppercaseTransliterator;->register()V

    .line 1875
    invoke-static {}, Lcom/ibm/icu/text/TitlecaseTransliterator;->register()V

    .line 1876
    invoke-static {}, Lcom/ibm/icu/text/UnicodeNameTransliterator;->register()V

    .line 1877
    invoke-static {}, Lcom/ibm/icu/text/NameUnicodeTransliterator;->register()V

    .line 1878
    invoke-static {}, Lcom/ibm/icu/text/NormalizationTransliterator;->register()V

    .line 1879
    invoke-static {}, Lcom/ibm/icu/text/BreakTransliterator;->register()V

    .line 1880
    invoke-static {}, Lcom/ibm/icu/text/AnyTransliterator;->register()V

    .line 1881
    return-void

    .line 1868
    :cond_5
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->class$com$ibm$icu$text$NullTransliterator:Ljava/lang/Class;

    goto :goto_4

    .line 1840
    nop

    :sswitch_data_0
    .sparse-switch
        0x46 -> :sswitch_0
        0x52 -> :sswitch_1
    .end sparse-switch
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V
    .locals 1
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "filter"    # Lcom/ibm/icu/text/UnicodeFilter;

    .prologue
    .line 511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 441
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/Transliterator;->maximumContextLength:I

    .line 512
    if-nez p1, :cond_0

    .line 513
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 515
    :cond_0
    iput-object p1, p0, Lcom/ibm/icu/text/Transliterator;->ID:Ljava/lang/String;

    .line 516
    iput-object p2, p0, Lcom/ibm/icu/text/Transliterator;->filter:Lcom/ibm/icu/text/UnicodeFilter;

    .line 517
    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 1869
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static final createFromRules(Ljava/lang/String;Ljava/lang/String;I)Lcom/ibm/icu/text/Transliterator;
    .locals 13
    .param p0, "ID"    # Ljava/lang/String;
    .param p1, "rules"    # Ljava/lang/String;
    .param p2, "dir"    # I

    .prologue
    .line 1409
    const/4 v7, 0x0

    .line 1411
    .local v7, "t":Lcom/ibm/icu/text/Transliterator;
    new-instance v4, Lcom/ibm/icu/text/TransliteratorParser;

    invoke-direct {v4}, Lcom/ibm/icu/text/TransliteratorParser;-><init>()V

    .line 1412
    .local v4, "parser":Lcom/ibm/icu/text/TransliteratorParser;
    invoke-virtual {v4, p1, p2}, Lcom/ibm/icu/text/TransliteratorParser;->parse(Ljava/lang/String;I)V

    .line 1415
    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    if-nez v10, :cond_1

    .line 1416
    new-instance v7, Lcom/ibm/icu/text/NullTransliterator;

    .end local v7    # "t":Lcom/ibm/icu/text/Transliterator;
    invoke-direct {v7}, Lcom/ibm/icu/text/NullTransliterator;-><init>()V

    .line 1464
    .restart local v7    # "t":Lcom/ibm/icu/text/Transliterator;
    :cond_0
    :goto_0
    return-object v7

    .line 1418
    :cond_1
    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    if-nez v10, :cond_2

    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_2

    .line 1419
    new-instance v7, Lcom/ibm/icu/text/RuleBasedTransliterator;

    .end local v7    # "t":Lcom/ibm/icu/text/Transliterator;
    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    const/4 v11, 0x0

    invoke-direct {v7, p0, v10, v11}, Lcom/ibm/icu/text/RuleBasedTransliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/RuleBasedTransliterator$Data;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 1420
    .restart local v7    # "t":Lcom/ibm/icu/text/Transliterator;
    goto :goto_0

    .line 1421
    :cond_2
    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_4

    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    if-nez v10, :cond_4

    .line 1426
    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v10, :cond_3

    .line 1427
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v11, v4, Lcom/ibm/icu/text/TransliteratorParser;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lcom/ibm/icu/text/UnicodeSet;->toPattern(Z)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string/jumbo v11, ";"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/ibm/icu/text/Transliterator;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;

    move-result-object v7

    .line 1433
    :goto_1
    if-eqz v7, :cond_0

    .line 1434
    invoke-virtual {v7, p0}, Lcom/ibm/icu/text/Transliterator;->setID(Ljava/lang/String;)V

    goto :goto_0

    .line 1430
    :cond_3
    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {v10}, Lcom/ibm/icu/text/Transliterator;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;

    move-result-object v7

    goto :goto_1

    .line 1438
    :cond_4
    new-instance v9, Ljava/util/Vector;

    invoke-direct {v9}, Ljava/util/Vector;-><init>()V

    .line 1439
    .local v9, "transliterators":Ljava/util/Vector;
    const/4 v5, 0x1

    .line 1441
    .local v5, "passNumber":I
    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    iget-object v11, v4, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    invoke-virtual {v11}, Ljava/util/Vector;->size()I

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1442
    .local v3, "limit":I
    const/4 v1, 0x0

    .local v1, "i":I
    move v6, v5

    .end local v5    # "passNumber":I
    .local v6, "passNumber":I
    :goto_2
    if-ge v1, v3, :cond_6

    .line 1443
    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    if-ge v1, v10, :cond_5

    .line 1444
    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    invoke-virtual {v10, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1445
    .local v2, "idBlock":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_5

    .line 1446
    invoke-static {v2}, Lcom/ibm/icu/text/Transliterator;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;

    move-result-object v8

    .line 1447
    .local v8, "temp":Lcom/ibm/icu/text/Transliterator;
    instance-of v10, v8, Lcom/ibm/icu/text/NullTransliterator;

    if-nez v10, :cond_5

    .line 1448
    invoke-static {v2}, Lcom/ibm/icu/text/Transliterator;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1451
    .end local v2    # "idBlock":Ljava/lang/String;
    .end local v8    # "temp":Lcom/ibm/icu/text/Transliterator;
    :cond_5
    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    if-ge v1, v10, :cond_7

    .line 1452
    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    invoke-virtual {v10, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .line 1453
    .local v0, "data":Lcom/ibm/icu/text/RuleBasedTransliterator$Data;
    new-instance v10, Lcom/ibm/icu/text/RuleBasedTransliterator;

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v12, "%Pass"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "passNumber":I
    .restart local v5    # "passNumber":I
    invoke-virtual {v11, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-direct {v10, v11, v0, v12}, Lcom/ibm/icu/text/RuleBasedTransliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/RuleBasedTransliterator$Data;Lcom/ibm/icu/text/UnicodeFilter;)V

    invoke-virtual {v9, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1442
    .end local v0    # "data":Lcom/ibm/icu/text/RuleBasedTransliterator$Data;
    :goto_3
    add-int/lit8 v1, v1, 0x1

    move v6, v5

    .end local v5    # "passNumber":I
    .restart local v6    # "passNumber":I
    goto :goto_2

    .line 1457
    :cond_6
    new-instance v7, Lcom/ibm/icu/text/CompoundTransliterator;

    .end local v7    # "t":Lcom/ibm/icu/text/Transliterator;
    add-int/lit8 v10, v6, -0x1

    invoke-direct {v7, v9, v10}, Lcom/ibm/icu/text/CompoundTransliterator;-><init>(Ljava/util/Vector;I)V

    .line 1458
    .restart local v7    # "t":Lcom/ibm/icu/text/Transliterator;
    invoke-virtual {v7, p0}, Lcom/ibm/icu/text/Transliterator;->setID(Ljava/lang/String;)V

    .line 1459
    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v10, :cond_0

    .line 1460
    iget-object v10, v4, Lcom/ibm/icu/text/TransliteratorParser;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    invoke-virtual {v7, v10}, Lcom/ibm/icu/text/Transliterator;->setFilter(Lcom/ibm/icu/text/UnicodeFilter;)V

    goto/16 :goto_0

    :cond_7
    move v5, v6

    .end local v6    # "passNumber":I
    .restart local v5    # "passNumber":I
    goto :goto_3
.end method

.method private filteredTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;ZZ)V
    .locals 24
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "index"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "incremental"    # Z
    .param p4, "rollback"    # Z

    .prologue
    .line 815
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Transliterator;->filter:Lcom/ibm/icu/text/UnicodeFilter;

    move-object/from16 v21, v0

    if-nez v21, :cond_0

    if-nez p4, :cond_0

    .line 816
    invoke-virtual/range {p0 .. p3}, Lcom/ibm/icu/text/Transliterator;->handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V

    .line 1110
    :goto_0
    return-void

    .line 844
    :cond_0
    move-object/from16 v0, p2

    iget v7, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 856
    .local v7, "globalLimit":I
    const/4 v10, 0x0

    .line 863
    .local v10, "log":Ljava/lang/StringBuffer;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Transliterator;->filter:Lcom/ibm/icu/text/UnicodeFilter;

    move-object/from16 v21, v0

    if-eqz v21, :cond_3

    .line 869
    :goto_1
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v0, v7, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Transliterator;->filter:Lcom/ibm/icu/text/UnicodeFilter;

    move-object/from16 v21, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    move/from16 v22, v0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v4

    .local v4, "c":I
    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Lcom/ibm/icu/text/UnicodeFilter;->contains(I)Z

    move-result v21

    if-nez v21, :cond_2

    .line 871
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    move/from16 v21, v0

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v22

    add-int v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, p2

    iput v0, v1, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    goto :goto_1

    .line 875
    .end local v4    # "c":I
    :cond_2
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    move/from16 v21, v0

    move/from16 v0, v21

    move-object/from16 v1, p2

    iput v0, v1, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 876
    :goto_2
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v0, v7, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Transliterator;->filter:Lcom/ibm/icu/text/UnicodeFilter;

    move-object/from16 v21, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    move/from16 v22, v0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v4

    .restart local v4    # "c":I
    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Lcom/ibm/icu/text/UnicodeFilter;->contains(I)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 878
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    move/from16 v21, v0

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v22

    add-int v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, p2

    iput v0, v1, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    goto :goto_2

    .line 885
    .end local v4    # "c":I
    :cond_3
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    move/from16 v21, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_5

    .line 1104
    :cond_4
    :goto_3
    move-object/from16 v0, p2

    iput v7, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    goto/16 :goto_0

    .line 893
    :cond_5
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v0, v7, :cond_6

    const/4 v8, 0x0

    .line 926
    .local v8, "isIncrementalRun":Z
    :goto_4
    if-eqz p4, :cond_9

    if-eqz v8, :cond_9

    .line 934
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    move/from16 v18, v0

    .line 935
    .local v18, "runStart":I
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    move/from16 v17, v0

    .line 936
    .local v17, "runLimit":I
    sub-int v16, v17, v18

    .line 939
    .local v16, "runLength":I
    invoke-interface/range {p1 .. p1}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v13

    .line 940
    .local v13, "rollbackOrigin":I
    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-interface {v0, v1, v2, v13}, Lcom/ibm/icu/text/Replaceable;->copy(III)V

    .line 947
    move/from16 v12, v18

    .line 948
    .local v12, "passStart":I
    move v14, v13

    .line 952
    .local v14, "rollbackStart":I
    move-object/from16 v0, p2

    iget v11, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 956
    .local v11, "passLimit":I
    const/16 v20, 0x0

    .line 959
    .local v20, "uncommittedLength":I
    const/16 v19, 0x0

    .line 966
    .local v19, "totalDelta":I
    :goto_5
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v21

    invoke-static/range {v21 .. v21}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v5

    .line 968
    .local v5, "charLength":I
    add-int/2addr v11, v5

    .line 969
    move/from16 v0, v17

    if-le v11, v0, :cond_7

    .line 1046
    add-int v13, v13, v19

    .line 1047
    add-int v7, v7, v19

    .line 1050
    add-int v21, v13, v16

    const-string/jumbo v22, ""

    move-object/from16 v0, p1

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-interface {v0, v13, v1, v2}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 1053
    move-object/from16 v0, p2

    iput v12, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 1094
    .end local v5    # "charLength":I
    .end local v11    # "passLimit":I
    .end local v12    # "passStart":I
    .end local v13    # "rollbackOrigin":I
    .end local v14    # "rollbackStart":I
    .end local v16    # "runLength":I
    .end local v17    # "runLimit":I
    .end local v18    # "runStart":I
    .end local v19    # "totalDelta":I
    .end local v20    # "uncommittedLength":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Transliterator;->filter:Lcom/ibm/icu/text/UnicodeFilter;

    move-object/from16 v21, v0

    if-eqz v21, :cond_4

    if-eqz v8, :cond_1

    goto :goto_3

    .end local v8    # "isIncrementalRun":Z
    :cond_6
    move/from16 v8, p3

    .line 893
    goto :goto_4

    .line 972
    .restart local v5    # "charLength":I
    .restart local v8    # "isIncrementalRun":Z
    .restart local v11    # "passLimit":I
    .restart local v12    # "passStart":I
    .restart local v13    # "rollbackOrigin":I
    .restart local v14    # "rollbackStart":I
    .restart local v16    # "runLength":I
    .restart local v17    # "runLimit":I
    .restart local v18    # "runStart":I
    .restart local v19    # "totalDelta":I
    .restart local v20    # "uncommittedLength":I
    :cond_7
    add-int v20, v20, v5

    .line 974
    move-object/from16 v0, p2

    iput v11, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 986
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Lcom/ibm/icu/text/Transliterator;->handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V

    .line 993
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    move/from16 v21, v0

    sub-int v6, v21, v11

    .line 998
    .local v6, "delta":I
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    move/from16 v21, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_8

    .line 1001
    add-int v21, v14, v6

    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    move/from16 v22, v0

    sub-int v22, v22, v12

    sub-int v15, v21, v22

    .line 1004
    .local v15, "rs":I
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    move/from16 v21, v0

    const-string/jumbo v22, ""

    move-object/from16 v0, p1

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-interface {v0, v12, v1, v2}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 1007
    add-int v21, v15, v20

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-interface {v0, v15, v1, v12}, Lcom/ibm/icu/text/Replaceable;->copy(III)V

    .line 1010
    move-object/from16 v0, p2

    iput v12, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 1011
    move-object/from16 v0, p2

    iput v11, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 1012
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    move/from16 v21, v0

    sub-int v21, v21, v6

    move/from16 v0, v21

    move-object/from16 v1, p2

    iput v0, v1, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    goto/16 :goto_5

    .line 1024
    .end local v15    # "rs":I
    :cond_8
    move-object/from16 v0, p2

    iget v11, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    move v12, v11

    .line 1030
    add-int v21, v6, v20

    add-int v14, v14, v21

    .line 1031
    const/16 v20, 0x0

    .line 1034
    add-int v17, v17, v6

    .line 1035
    add-int v19, v19, v6

    goto/16 :goto_5

    .line 1064
    .end local v5    # "charLength":I
    .end local v6    # "delta":I
    .end local v11    # "passLimit":I
    .end local v12    # "passStart":I
    .end local v13    # "rollbackOrigin":I
    .end local v14    # "rollbackStart":I
    .end local v16    # "runLength":I
    .end local v17    # "runLimit":I
    .end local v18    # "runStart":I
    .end local v19    # "totalDelta":I
    .end local v20    # "uncommittedLength":I
    :cond_9
    move-object/from16 v0, p2

    iget v9, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 1065
    .local v9, "limit":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v8}, Lcom/ibm/icu/text/Transliterator;->handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V

    .line 1066
    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    move/from16 v21, v0

    sub-int v6, v21, v9

    .line 1080
    .restart local v6    # "delta":I
    if-nez v8, :cond_a

    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    move/from16 v21, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_a

    .line 1081
    new-instance v21, Ljava/lang/RuntimeException;

    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v23, "ERROR: Incomplete non-incremental transliteration by "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/Transliterator;->getID()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v21

    .line 1087
    :cond_a
    add-int/2addr v7, v6

    goto/16 :goto_6
.end method

.method public static final getAvailableIDs()Ljava/util/Enumeration;
    .locals 1

    .prologue
    .line 1748
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->registry:Lcom/ibm/icu/text/TransliteratorRegistry;

    invoke-virtual {v0}, Lcom/ibm/icu/text/TransliteratorRegistry;->getAvailableIDs()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public static final getAvailableSources()Ljava/util/Enumeration;
    .locals 1

    .prologue
    .line 1759
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->registry:Lcom/ibm/icu/text/TransliteratorRegistry;

    invoke-virtual {v0}, Lcom/ibm/icu/text/TransliteratorRegistry;->getAvailableSources()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public static final getAvailableTargets(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 1
    .param p0, "source"    # Ljava/lang/String;

    .prologue
    .line 1770
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->registry:Lcom/ibm/icu/text/TransliteratorRegistry;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/text/TransliteratorRegistry;->getAvailableTargets(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public static final getAvailableVariants(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 1
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 1780
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->registry:Lcom/ibm/icu/text/TransliteratorRegistry;

    invoke-virtual {v0, p0, p1}, Lcom/ibm/icu/text/TransliteratorRegistry;->getAvailableVariants(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method static getBasicInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;
    .locals 4
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "canonID"    # Ljava/lang/String;

    .prologue
    .line 1386
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1387
    .local v0, "s":Ljava/lang/StringBuffer;
    sget-object v2, Lcom/ibm/icu/text/Transliterator;->registry:Lcom/ibm/icu/text/TransliteratorRegistry;

    invoke-virtual {v2, p0, v0}, Lcom/ibm/icu/text/TransliteratorRegistry;->get(Ljava/lang/String;Ljava/lang/StringBuffer;)Lcom/ibm/icu/text/Transliterator;

    move-result-object v1

    .line 1388
    .local v1, "t":Lcom/ibm/icu/text/Transliterator;
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 1391
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/ibm/icu/text/Transliterator;->getInstance(Ljava/lang/String;I)Lcom/ibm/icu/text/Transliterator;

    move-result-object v1

    .line 1393
    :cond_0
    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    .line 1394
    invoke-virtual {v1, p1}, Lcom/ibm/icu/text/Transliterator;->setID(Ljava/lang/String;)V

    .line 1396
    :cond_1
    return-object v1
.end method

.method public static final getDisplayName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "ID"    # Ljava/lang/String;

    .prologue
    .line 1185
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/ibm/icu/text/Transliterator;->getDisplayName(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDisplayName(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;
    .locals 12
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "inLocale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/16 v11, 0x2f

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v10, 0x2

    .line 1238
    const-string/jumbo v7, "com/ibm/icu/impl/data/icudt40b/translit"

    invoke-static {v7, p1}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v2

    check-cast v2, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 1242
    .local v2, "bundle":Lcom/ibm/icu/impl/ICUResourceBundle;
    invoke-static {p0}, Lcom/ibm/icu/text/TransliteratorIDParser;->IDtoSTV(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1243
    .local v6, "stv":[Ljava/lang/String;
    if-nez v6, :cond_1

    .line 1245
    const-string/jumbo v5, ""

    .line 1281
    :cond_0
    :goto_0
    return-object v5

    .line 1247
    :cond_1
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    aget-object v8, v6, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const/16 v8, 0x2d

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v7

    aget-object v8, v6, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1248
    .local v0, "ID":Ljava/lang/String;
    aget-object v7, v6, v10

    if-eqz v7, :cond_2

    aget-object v7, v6, v10

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_2

    .line 1249
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v7

    aget-object v8, v6, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1253
    :cond_2
    sget-object v7, Lcom/ibm/icu/text/Transliterator;->displayNameCache:Ljava/util/Hashtable;

    new-instance v8, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v8, v0}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1254
    .local v5, "n":Ljava/lang/String;
    if-nez v5, :cond_0

    .line 1261
    :try_start_0
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v8, "%Translit%%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/ibm/icu/impl/ICUResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    goto :goto_0

    .line 1262
    :catch_0
    move-exception v7

    .line 1267
    :try_start_1
    new-instance v3, Ljava/text/MessageFormat;

    const-string/jumbo v7, "TransliteratorNamePattern"

    invoke-virtual {v2, v7}, Lcom/ibm/icu/impl/ICUResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/text/MessageFormat;-><init>(Ljava/lang/String;)V

    .line 1270
    .local v3, "format":Ljava/text/MessageFormat;
    const/4 v7, 0x3

    new-array v1, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/Integer;

    const/4 v9, 0x2

    invoke-direct {v8, v9}, Ljava/lang/Integer;-><init>(I)V

    aput-object v8, v1, v7

    const/4 v7, 0x1

    const/4 v8, 0x0

    aget-object v8, v6, v8

    aput-object v8, v1, v7

    const/4 v7, 0x2

    const/4 v8, 0x1

    aget-object v8, v6, v8

    aput-object v8, v1, v7
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1273
    .local v1, "args":[Ljava/lang/Object;
    const/4 v4, 0x1

    .local v4, "j":I
    :goto_1
    if-gt v4, v10, :cond_3

    .line 1275
    :try_start_2
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v8, "%Translit%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    aget-object v7, v1, v4

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/ibm/icu/impl/ICUResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v4
    :try_end_2
    .catch Ljava/util/MissingResourceException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1273
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1281
    :cond_3
    const/4 v7, 0x2

    :try_start_3
    aget-object v7, v6, v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_4

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, v1}, Ljava/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const/16 v8, 0x2f

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v7

    const/4 v8, 0x2

    aget-object v8, v6, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    :goto_3
    move-object v5, v7

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v3, v1}, Ljava/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/util/MissingResourceException; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v7

    goto :goto_3

    .line 1284
    .end local v1    # "args":[Ljava/lang/Object;
    .end local v3    # "format":Ljava/text/MessageFormat;
    .end local v4    # "j":I
    :catch_1
    move-exception v7

    .line 1289
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7}, Ljava/lang/RuntimeException;-><init>()V

    throw v7

    .line 1277
    .restart local v1    # "args":[Ljava/lang/Object;
    .restart local v3    # "format":Ljava/text/MessageFormat;
    .restart local v4    # "j":I
    :catch_2
    move-exception v7

    goto :goto_2
.end method

.method public static getDisplayName(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "inLocale"    # Ljava/util/Locale;

    .prologue
    .line 1208
    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/ibm/icu/text/Transliterator;->getDisplayName(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;
    .locals 1
    .param p0, "ID"    # Ljava/lang/String;

    .prologue
    .line 1325
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/ibm/icu/text/Transliterator;->getInstance(Ljava/lang/String;I)Lcom/ibm/icu/text/Transliterator;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Ljava/lang/String;I)Lcom/ibm/icu/text/Transliterator;
    .locals 7
    .param p0, "ID"    # Ljava/lang/String;
    .param p1, "dir"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1345
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1346
    .local v0, "canonID":Ljava/lang/StringBuffer;
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 1347
    .local v2, "list":Ljava/util/Vector;
    new-array v1, v6, [Lcom/ibm/icu/text/UnicodeSet;

    .line 1348
    .local v1, "globalFilter":[Lcom/ibm/icu/text/UnicodeSet;
    invoke-static {p0, p1, v0, v2, v1}, Lcom/ibm/icu/text/TransliteratorIDParser;->parseCompoundID(Ljava/lang/String;ILjava/lang/StringBuffer;Ljava/util/Vector;[Lcom/ibm/icu/text/UnicodeSet;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1349
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "Invalid ID "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1352
    :cond_0
    invoke-static {v2}, Lcom/ibm/icu/text/TransliteratorIDParser;->instantiateList(Ljava/util/Vector;)V

    .line 1355
    const/4 v3, 0x0

    .line 1356
    .local v3, "t":Lcom/ibm/icu/text/Transliterator;
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v4

    if-gt v4, v6, :cond_1

    const-string/jumbo v4, ";"

    invoke-static {v0, v4}, Lcom/ibm/icu/impl/Utility;->indexOf(Ljava/lang/StringBuffer;Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_3

    .line 1362
    :cond_1
    new-instance v3, Lcom/ibm/icu/text/CompoundTransliterator;

    .end local v3    # "t":Lcom/ibm/icu/text/Transliterator;
    invoke-direct {v3, v2}, Lcom/ibm/icu/text/CompoundTransliterator;-><init>(Ljava/util/Vector;)V

    .line 1368
    .restart local v3    # "t":Lcom/ibm/icu/text/Transliterator;
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/Transliterator;->setID(Ljava/lang/String;)V

    .line 1369
    aget-object v4, v1, v5

    if-eqz v4, :cond_2

    .line 1370
    aget-object v4, v1, v5

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/Transliterator;->setFilter(Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 1372
    :cond_2
    return-object v3

    .line 1365
    :cond_3
    invoke-virtual {v2, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "t":Lcom/ibm/icu/text/Transliterator;
    check-cast v3, Lcom/ibm/icu/text/Transliterator;

    .restart local v3    # "t":Lcom/ibm/icu/text/Transliterator;
    goto :goto_0
.end method

.method public static registerAlias(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "aliasID"    # Ljava/lang/String;
    .param p1, "realID"    # Ljava/lang/String;

    .prologue
    .line 1680
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->registry:Lcom/ibm/icu/text/TransliteratorRegistry;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, p1, v1}, Lcom/ibm/icu/text/TransliteratorRegistry;->put(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1681
    return-void
.end method

.method public static registerClass(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 2
    .param p0, "ID"    # Ljava/lang/String;
    .param p1, "transClass"    # Ljava/lang/Class;
    .param p2, "displayName"    # Ljava/lang/String;

    .prologue
    .line 1635
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->registry:Lcom/ibm/icu/text/TransliteratorRegistry;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, p1, v1}, Lcom/ibm/icu/text/TransliteratorRegistry;->put(Ljava/lang/String;Ljava/lang/Class;Z)V

    .line 1636
    if-eqz p2, :cond_0

    .line 1637
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->displayNameCache:Ljava/util/Hashtable;

    new-instance v1, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v1, p0}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1639
    :cond_0
    return-void
.end method

.method public static registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V
    .locals 2
    .param p0, "ID"    # Ljava/lang/String;
    .param p1, "factory"    # Lcom/ibm/icu/text/Transliterator$Factory;

    .prologue
    .line 1649
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->registry:Lcom/ibm/icu/text/TransliteratorRegistry;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, p1, v1}, Lcom/ibm/icu/text/TransliteratorRegistry;->put(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;Z)V

    .line 1650
    return-void
.end method

.method public static registerInstance(Lcom/ibm/icu/text/Transliterator;)V
    .locals 3
    .param p0, "trans"    # Lcom/ibm/icu/text/Transliterator;

    .prologue
    .line 1658
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->registry:Lcom/ibm/icu/text/TransliteratorRegistry;

    invoke-virtual {p0}, Lcom/ibm/icu/text/Transliterator;->getID()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Lcom/ibm/icu/text/TransliteratorRegistry;->put(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator;Z)V

    .line 1659
    return-void
.end method

.method static registerInstance(Lcom/ibm/icu/text/Transliterator;Z)V
    .locals 2
    .param p0, "trans"    # Lcom/ibm/icu/text/Transliterator;
    .param p1, "visible"    # Z

    .prologue
    .line 1668
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->registry:Lcom/ibm/icu/text/TransliteratorRegistry;

    invoke-virtual {p0}, Lcom/ibm/icu/text/Transliterator;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0, p1}, Lcom/ibm/icu/text/TransliteratorRegistry;->put(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator;Z)V

    .line 1669
    return-void
.end method

.method static registerSpecialInverse(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "target"    # Ljava/lang/String;
    .param p1, "inverseTarget"    # Ljava/lang/String;
    .param p2, "bidirectional"    # Z

    .prologue
    .line 1719
    invoke-static {p0, p1, p2}, Lcom/ibm/icu/text/TransliteratorIDParser;->registerSpecialInverse(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1720
    return-void
.end method

.method public static unregister(Ljava/lang/String;)V
    .locals 2
    .param p0, "ID"    # Ljava/lang/String;

    .prologue
    .line 1731
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->displayNameCache:Ljava/util/Hashtable;

    new-instance v1, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v1, p0}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1732
    sget-object v0, Lcom/ibm/icu/text/Transliterator;->registry:Lcom/ibm/icu/text/TransliteratorRegistry;

    invoke-virtual {v0, p0}, Lcom/ibm/icu/text/TransliteratorRegistry;->remove(Ljava/lang/String;)V

    .line 1733
    return-void
.end method


# virtual methods
.method protected final baseToRules(Z)Ljava/lang/String;
    .locals 7
    .param p1, "escapeUnprintable"    # Z

    .prologue
    const/16 v6, 0x3b

    .line 1492
    if-eqz p1, :cond_2

    .line 1493
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 1494
    .local v3, "rulesSource":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/ibm/icu/text/Transliterator;->getID()Ljava/lang/String;

    move-result-object v2

    .line 1495
    .local v2, "id":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 1496
    invoke-static {v2, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 1497
    .local v0, "c":I
    invoke-static {v3, v0}, Lcom/ibm/icu/impl/Utility;->escapeUnprintable(Ljava/lang/StringBuffer;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1498
    invoke-static {v3, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 1500
    :cond_0
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 1501
    goto :goto_0

    .line 1502
    .end local v0    # "c":I
    :cond_1
    const/4 v4, 0x0

    const-string/jumbo v5, "::"

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 1503
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1504
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1506
    .end local v1    # "i":I
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "rulesSource":Ljava/lang/StringBuffer;
    :goto_1
    return-object v4

    :cond_2
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v5, "::"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/ibm/icu/text/Transliterator;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public filteredTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V
    .locals 1
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "index"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "incremental"    # Z

    .prologue
    .line 1126
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/ibm/icu/text/Transliterator;->filteredTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;ZZ)V

    .line 1127
    return-void
.end method

.method public final finishTransliteration(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;)V
    .locals 2
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "index"    # Lcom/ibm/icu/text/Transliterator$Position;

    .prologue
    .line 696
    invoke-interface {p1}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/ibm/icu/text/Transliterator$Position;->validate(I)V

    .line 697
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/ibm/icu/text/Transliterator;->filteredTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;ZZ)V

    .line 698
    return-void
.end method

.method public getElements()[Lcom/ibm/icu/text/Transliterator;
    .locals 4

    .prologue
    .line 1525
    instance-of v3, p0, Lcom/ibm/icu/text/CompoundTransliterator;

    if-eqz v3, :cond_0

    move-object v0, p0

    .line 1526
    check-cast v0, Lcom/ibm/icu/text/CompoundTransliterator;

    .line 1527
    .local v0, "cpd":Lcom/ibm/icu/text/CompoundTransliterator;
    invoke-virtual {v0}, Lcom/ibm/icu/text/CompoundTransliterator;->getCount()I

    move-result v3

    new-array v2, v3, [Lcom/ibm/icu/text/Transliterator;

    .line 1528
    .local v2, "result":[Lcom/ibm/icu/text/Transliterator;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 1529
    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/CompoundTransliterator;->getTransliterator(I)Lcom/ibm/icu/text/Transliterator;

    move-result-object v3

    aput-object v3, v2, v1

    .line 1528
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1532
    .end local v0    # "cpd":Lcom/ibm/icu/text/CompoundTransliterator;
    .end local v1    # "i":I
    .end local v2    # "result":[Lcom/ibm/icu/text/Transliterator;
    :cond_0
    const/4 v3, 0x1

    new-array v2, v3, [Lcom/ibm/icu/text/Transliterator;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    .line 1534
    .restart local v2    # "result":[Lcom/ibm/icu/text/Transliterator;
    :cond_1
    return-object v2
.end method

.method public final getFilter()Lcom/ibm/icu/text/UnicodeFilter;
    .locals 1

    .prologue
    .line 1298
    iget-object v0, p0, Lcom/ibm/icu/text/Transliterator;->filter:Lcom/ibm/icu/text/UnicodeFilter;

    return-object v0
.end method

.method public final getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1166
    iget-object v0, p0, Lcom/ibm/icu/text/Transliterator;->ID:Ljava/lang/String;

    return-object v0
.end method

.method public final getInverse()Lcom/ibm/icu/text/Transliterator;
    .locals 2

    .prologue
    .line 1618
    iget-object v0, p0, Lcom/ibm/icu/text/Transliterator;->ID:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->getInstance(Ljava/lang/String;I)Lcom/ibm/icu/text/Transliterator;

    move-result-object v0

    return-object v0
.end method

.method public final getMaximumContextLength()I
    .locals 1

    .prologue
    .line 1142
    iget v0, p0, Lcom/ibm/icu/text/Transliterator;->maximumContextLength:I

    return v0
.end method

.method public final getSourceSet()Lcom/ibm/icu/text/UnicodeSet;
    .locals 4

    .prologue
    .line 1551
    invoke-virtual {p0}, Lcom/ibm/icu/text/Transliterator;->handleGetSourceSet()Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v2

    .line 1552
    .local v2, "set":Lcom/ibm/icu/text/UnicodeSet;
    iget-object v3, p0, Lcom/ibm/icu/text/Transliterator;->filter:Lcom/ibm/icu/text/UnicodeFilter;

    if-eqz v3, :cond_0

    .line 1557
    :try_start_0
    iget-object v1, p0, Lcom/ibm/icu/text/Transliterator;->filter:Lcom/ibm/icu/text/UnicodeFilter;

    check-cast v1, Lcom/ibm/icu/text/UnicodeSet;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1562
    .local v1, "filterSet":Lcom/ibm/icu/text/UnicodeSet;
    :goto_0
    invoke-virtual {v2, v1}, Lcom/ibm/icu/text/UnicodeSet;->retainAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    .line 1564
    .end local v1    # "filterSet":Lcom/ibm/icu/text/UnicodeSet;
    :cond_0
    return-object v2

    .line 1558
    :catch_0
    move-exception v0

    .line 1559
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v1, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 1560
    .restart local v1    # "filterSet":Lcom/ibm/icu/text/UnicodeSet;
    iget-object v3, p0, Lcom/ibm/icu/text/Transliterator;->filter:Lcom/ibm/icu/text/UnicodeFilter;

    invoke-virtual {v3, v1}, Lcom/ibm/icu/text/UnicodeFilter;->addMatchSetTo(Lcom/ibm/icu/text/UnicodeSet;)V

    goto :goto_0
.end method

.method public getTargetSet()Lcom/ibm/icu/text/UnicodeSet;
    .locals 1

    .prologue
    .line 1596
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    return-object v0
.end method

.method protected handleGetSourceSet()Lcom/ibm/icu/text/UnicodeSet;
    .locals 1

    .prologue
    .line 1581
    new-instance v0, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    return-object v0
.end method

.method protected abstract handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V
.end method

.method public setFilter(Lcom/ibm/icu/text/UnicodeFilter;)V
    .locals 0
    .param p1, "filter"    # Lcom/ibm/icu/text/UnicodeFilter;

    .prologue
    .line 1311
    iput-object p1, p0, Lcom/ibm/icu/text/Transliterator;->filter:Lcom/ibm/icu/text/UnicodeFilter;

    .line 1312
    return-void
.end method

.method protected final setID(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 1175
    iput-object p1, p0, Lcom/ibm/icu/text/Transliterator;->ID:Ljava/lang/String;

    .line 1176
    return-void
.end method

.method protected setMaximumContextLength(I)V
    .locals 3
    .param p1, "a"    # I

    .prologue
    .line 1151
    if-gez p1, :cond_0

    .line 1152
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Invalid context length "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1154
    :cond_0
    iput p1, p0, Lcom/ibm/icu/text/Transliterator;->maximumContextLength:I

    .line 1155
    return-void
.end method

.method public toRules(Z)Ljava/lang/String;
    .locals 1
    .param p1, "escapeUnprintable"    # Z

    .prologue
    .line 1475
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/Transliterator;->baseToRules(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transform(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 1908
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/Transliterator;->transliterate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final transliterate(Lcom/ibm/icu/text/Replaceable;II)I
    .locals 3
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "start"    # I
    .param p3, "limit"    # I

    .prologue
    .line 535
    if-ltz p2, :cond_0

    if-lt p3, p2, :cond_0

    invoke-interface {p1}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v1

    if-ge v1, p3, :cond_1

    .line 538
    :cond_0
    const/4 v1, -0x1

    .line 543
    :goto_0
    return v1

    .line 541
    :cond_1
    new-instance v0, Lcom/ibm/icu/text/Transliterator$Position;

    invoke-direct {v0, p2, p3, p2}, Lcom/ibm/icu/text/Transliterator$Position;-><init>(III)V

    .line 542
    .local v0, "pos":Lcom/ibm/icu/text/Transliterator$Position;
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/ibm/icu/text/Transliterator;->filteredTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;ZZ)V

    .line 543
    iget v1, v0, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    goto :goto_0
.end method

.method public final transliterate(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 563
    new-instance v0, Lcom/ibm/icu/text/ReplaceableString;

    invoke-direct {v0, p1}, Lcom/ibm/icu/text/ReplaceableString;-><init>(Ljava/lang/String;)V

    .line 564
    .local v0, "result":Lcom/ibm/icu/text/ReplaceableString;
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/Transliterator;->transliterate(Lcom/ibm/icu/text/Replaceable;)V

    .line 565
    invoke-virtual {v0}, Lcom/ibm/icu/text/ReplaceableString;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public final transliterate(Lcom/ibm/icu/text/Replaceable;)V
    .locals 2
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;

    .prologue
    .line 552
    const/4 v0, 0x0

    invoke-interface {p1}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/ibm/icu/text/Transliterator;->transliterate(Lcom/ibm/icu/text/Replaceable;II)I

    .line 553
    return-void
.end method

.method public final transliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;)V
    .locals 1
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "index"    # Lcom/ibm/icu/text/Transliterator$Position;

    .prologue
    .line 680
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/ibm/icu/text/Transliterator;->transliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Ljava/lang/String;)V

    .line 681
    return-void
.end method

.method public final transliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;I)V
    .locals 1
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "index"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "insertion"    # I

    .prologue
    .line 664
    invoke-static {p3}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/ibm/icu/text/Transliterator;->transliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Ljava/lang/String;)V

    .line 665
    return-void
.end method

.method public final transliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "index"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "insertion"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 618
    invoke-interface {p1}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/ibm/icu/text/Transliterator$Position;->validate(I)V

    .line 621
    if-eqz p3, :cond_0

    .line 622
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    iget v1, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    invoke-interface {p1, v0, v1, p3}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 623
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 624
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    .line 627
    :cond_0
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    if-lez v0, :cond_1

    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 644
    :goto_0
    return-void

    .line 636
    :cond_1
    invoke-direct {p0, p1, p2, v2, v2}, Lcom/ibm/icu/text/Transliterator;->filteredTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;ZZ)V

    goto :goto_0
.end method
