.class Lcom/ibm/icu/text/MultiplierSubstitution;
.super Lcom/ibm/icu/text/NFSubstitution;
.source "NFSubstitution.java"


# instance fields
.field divisor:D


# direct methods
.method constructor <init>(IDLcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V
    .locals 4
    .param p1, "pos"    # I
    .param p2, "divisor"    # D
    .param p4, "ruleSet"    # Lcom/ibm/icu/text/NFRuleSet;
    .param p5, "formatter"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;
    .param p6, "description"    # Ljava/lang/String;

    .prologue
    .line 653
    invoke-direct {p0, p1, p4, p5, p6}, Lcom/ibm/icu/text/NFSubstitution;-><init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    .line 658
    iput-wide p2, p0, Lcom/ibm/icu/text/MultiplierSubstitution;->divisor:D

    .line 660
    const-wide/16 v0, 0x0

    cmpl-double v0, p2, v0

    if-nez v0, :cond_0

    .line 661
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Substitution with bad divisor ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ") "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p6, v2, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p6, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 664
    :cond_0
    return-void
.end method


# virtual methods
.method public calcUpperBound(D)D
    .locals 2
    .param p1, "oldUpperBound"    # D

    .prologue
    .line 751
    iget-wide v0, p0, Lcom/ibm/icu/text/MultiplierSubstitution;->divisor:D

    return-wide v0
.end method

.method public composeRuleValue(DD)D
    .locals 3
    .param p1, "newRuleValue"    # D
    .param p3, "oldRuleValue"    # D

    .prologue
    .line 742
    iget-wide v0, p0, Lcom/ibm/icu/text/MultiplierSubstitution;->divisor:D

    mul-double/2addr v0, p1

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "that"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 689
    invoke-super {p0, p1}, Lcom/ibm/icu/text/NFSubstitution;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 690
    check-cast v0, Lcom/ibm/icu/text/MultiplierSubstitution;

    .line 692
    .local v0, "that2":Lcom/ibm/icu/text/MultiplierSubstitution;
    iget-wide v2, p0, Lcom/ibm/icu/text/MultiplierSubstitution;->divisor:D

    iget-wide v4, v0, Lcom/ibm/icu/text/MultiplierSubstitution;->divisor:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 694
    .end local v0    # "that2":Lcom/ibm/icu/text/MultiplierSubstitution;
    :cond_0
    return v1
.end method

.method public setDivisor(II)V
    .locals 4
    .param p1, "radix"    # I
    .param p2, "exponent"    # I

    .prologue
    .line 672
    int-to-double v0, p1

    int-to-double v2, p2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/ibm/icu/text/MultiplierSubstitution;->divisor:D

    .line 674
    iget-wide v0, p0, Lcom/ibm/icu/text/MultiplierSubstitution;->divisor:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 675
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Substitution with divisor 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 677
    :cond_0
    return-void
.end method

.method tokenChar()C
    .locals 1

    .prologue
    .line 763
    const/16 v0, 0x3c

    return v0
.end method

.method public transformNumber(D)D
    .locals 3
    .param p1, "number"    # D

    .prologue
    .line 721
    iget-object v0, p0, Lcom/ibm/icu/text/MultiplierSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    if-nez v0, :cond_0

    .line 722
    iget-wide v0, p0, Lcom/ibm/icu/text/MultiplierSubstitution;->divisor:D

    div-double v0, p1, v0

    .line 724
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/ibm/icu/text/MultiplierSubstitution;->divisor:D

    div-double v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method public transformNumber(J)J
    .locals 5
    .param p1, "number"    # J

    .prologue
    .line 708
    long-to-double v0, p1

    iget-wide v2, p0, Lcom/ibm/icu/text/MultiplierSubstitution;->divisor:D

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    return-wide v0
.end method
