.class Lcom/ibm/icu/text/NumeratorSubstitution;
.super Lcom/ibm/icu/text/NFSubstitution;
.source "NFSubstitution.java"


# instance fields
.field denominator:D

.field withZeros:Z


# direct methods
.method constructor <init>(IDLcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V
    .locals 2
    .param p1, "pos"    # I
    .param p2, "denominator"    # D
    .param p4, "ruleSet"    # Lcom/ibm/icu/text/NFRuleSet;
    .param p5, "formatter"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;
    .param p6, "description"    # Ljava/lang/String;

    .prologue
    .line 1512
    invoke-static {p6}, Lcom/ibm/icu/text/NumeratorSubstitution;->fixdesc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p4, p5, v0}, Lcom/ibm/icu/text/NFSubstitution;-><init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    .line 1517
    iput-wide p2, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->denominator:D

    .line 1519
    const-string/jumbo v0, "<<"

    invoke-virtual {p6, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->withZeros:Z

    .line 1520
    return-void
.end method

.method static fixdesc(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "description"    # Ljava/lang/String;

    .prologue
    .line 1523
    const-string/jumbo v0, "<<"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .end local p0    # "description":Ljava/lang/String;
    :cond_0
    return-object p0
.end method


# virtual methods
.method public calcUpperBound(D)D
    .locals 2
    .param p1, "oldUpperBound"    # D

    .prologue
    .line 1700
    iget-wide v0, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->denominator:D

    return-wide v0
.end method

.method public composeRuleValue(DD)D
    .locals 3
    .param p1, "newRuleValue"    # D
    .param p3, "oldRuleValue"    # D

    .prologue
    .line 1691
    div-double v0, p1, p3

    return-wide v0
.end method

.method public doParse(Ljava/lang/String;Ljava/text/ParsePosition;DDZ)Ljava/lang/Number;
    .locals 19
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "parsePosition"    # Ljava/text/ParsePosition;
    .param p3, "baseValue"    # D
    .param p5, "upperBound"    # D
    .param p7, "lenientParse"    # Z

    .prologue
    .line 1628
    const/16 v18, 0x0

    .line 1629
    .local v18, "zeroCount":I
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/NumeratorSubstitution;->withZeros:Z

    if-eqz v3, :cond_2

    .line 1630
    new-instance v17, Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 1631
    .local v17, "workText":Ljava/lang/String;
    new-instance v16, Ljava/text/ParsePosition;

    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Ljava/text/ParsePosition;-><init>(I)V

    .line 1634
    .local v16, "workPos":Ljava/text/ParsePosition;
    :cond_0
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    invoke-virtual/range {v16 .. v16}, Ljava/text/ParsePosition;->getIndex()I

    move-result v3

    if-eqz v3, :cond_1

    .line 1635
    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1636
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/ibm/icu/text/NumeratorSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/ibm/icu/text/NFRuleSet;->parse(Ljava/lang/String;Ljava/text/ParsePosition;D)Ljava/lang/Number;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    .line 1637
    invoke-virtual/range {v16 .. v16}, Ljava/text/ParsePosition;->getIndex()I

    move-result v3

    if-nez v3, :cond_3

    .line 1652
    :cond_1
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 1653
    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1657
    .end local v16    # "workPos":Ljava/text/ParsePosition;
    .end local v17    # "workText":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/NumeratorSubstitution;->withZeros:Z

    if-eqz v3, :cond_4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    :goto_0
    const/4 v10, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v8, p5

    invoke-super/range {v3 .. v10}, Lcom/ibm/icu/text/NFSubstitution;->doParse(Ljava/lang/String;Ljava/text/ParsePosition;DDZ)Ljava/lang/Number;

    move-result-object v11

    .line 1659
    .local v11, "result":Ljava/lang/Number;
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/ibm/icu/text/NumeratorSubstitution;->withZeros:Z

    if-eqz v3, :cond_7

    .line 1664
    invoke-virtual {v11}, Ljava/lang/Number;->longValue()J

    move-result-wide v14

    .line 1665
    .local v14, "n":J
    const-wide/16 v12, 0x1

    .line 1666
    .local v12, "d":J
    const/4 v2, 0x0

    .line 1667
    .local v2, "pow":I
    :goto_1
    cmp-long v3, v12, v14

    if-gtz v3, :cond_5

    .line 1668
    const-wide/16 v4, 0xa

    mul-long/2addr v12, v4

    .line 1669
    add-int/lit8 v2, v2, 0x1

    .line 1670
    goto :goto_1

    .line 1643
    .end local v2    # "pow":I
    .end local v11    # "result":Ljava/lang/Number;
    .end local v12    # "d":J
    .end local v14    # "n":J
    .restart local v16    # "workPos":Ljava/text/ParsePosition;
    .restart local v17    # "workText":Ljava/lang/String;
    :cond_3
    add-int/lit8 v18, v18, 0x1

    .line 1644
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v3

    invoke-virtual/range {v16 .. v16}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    add-int/2addr v3, v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1645
    invoke-virtual/range {v16 .. v16}, Ljava/text/ParsePosition;->getIndex()I

    move-result v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    .line 1646
    :goto_2
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x20

    if-ne v3, v4, :cond_0

    .line 1647
    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    .line 1648
    invoke-virtual/range {p2 .. p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/text/ParsePosition;->setIndex(I)V

    goto :goto_2

    .end local v16    # "workPos":Ljava/text/ParsePosition;
    .end local v17    # "workText":Ljava/lang/String;
    :cond_4
    move-wide/from16 v6, p3

    .line 1657
    goto :goto_0

    .line 1672
    .restart local v2    # "pow":I
    .restart local v11    # "result":Ljava/lang/Number;
    .restart local v12    # "d":J
    .restart local v14    # "n":J
    :cond_5
    :goto_3
    if-lez v18, :cond_6

    .line 1673
    const-wide/16 v4, 0xa

    mul-long/2addr v12, v4

    .line 1674
    add-int/lit8 v18, v18, -0x1

    .line 1675
    goto :goto_3

    .line 1677
    :cond_6
    new-instance v11, Ljava/lang/Double;

    .end local v11    # "result":Ljava/lang/Number;
    long-to-double v4, v14

    long-to-double v6, v12

    div-double/2addr v4, v6

    invoke-direct {v11, v4, v5}, Ljava/lang/Double;-><init>(D)V

    .line 1680
    .end local v2    # "pow":I
    .end local v12    # "d":J
    .end local v14    # "n":J
    .restart local v11    # "result":Ljava/lang/Number;
    :cond_7
    return-object v11
.end method

.method public doSubstitution(DLjava/lang/StringBuffer;I)V
    .locals 11
    .param p1, "number"    # D
    .param p3, "toInsertInto"    # Ljava/lang/StringBuffer;
    .param p4, "position"    # I

    .prologue
    .line 1564
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/NumeratorSubstitution;->transformNumber(D)D

    move-result-wide v4

    .line 1566
    .local v4, "numberToFormat":D
    iget-boolean v1, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->withZeros:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    if-eqz v1, :cond_1

    .line 1568
    double-to-long v2, v4

    .line 1569
    .local v2, "nf":J
    invoke-virtual {p3}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    .line 1570
    .local v0, "len":I
    :goto_0
    const-wide/16 v6, 0xa

    mul-long/2addr v2, v6

    long-to-double v6, v2

    iget-wide v8, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->denominator:D

    cmpg-double v1, v6, v8

    if-gez v1, :cond_0

    .line 1571
    iget v1, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->pos:I

    add-int/2addr v1, p4

    const/16 v6, 0x20

    invoke-virtual {p3, v1, v6}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 1572
    iget-object v1, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    const-wide/16 v6, 0x0

    iget v8, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->pos:I

    add-int/2addr v8, p4

    invoke-virtual {v1, v6, v7, p3, v8}, Lcom/ibm/icu/text/NFRuleSet;->format(JLjava/lang/StringBuffer;I)V

    goto :goto_0

    .line 1574
    :cond_0
    invoke-virtual {p3}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    sub-int/2addr v1, v0

    add-int/2addr p4, v1

    .line 1579
    .end local v0    # "len":I
    .end local v2    # "nf":J
    :cond_1
    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    cmpl-double v1, v4, v6

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    if-eqz v1, :cond_2

    .line 1580
    iget-object v1, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    double-to-long v6, v4

    iget v8, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->pos:I

    add-int/2addr v8, p4

    invoke-virtual {v1, v6, v7, p3, v8}, Lcom/ibm/icu/text/NFRuleSet;->format(JLjava/lang/StringBuffer;I)V

    .line 1592
    :goto_1
    return-void

    .line 1586
    :cond_2
    iget-object v1, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    if-eqz v1, :cond_3

    .line 1587
    iget-object v1, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->ruleSet:Lcom/ibm/icu/text/NFRuleSet;

    iget v6, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->pos:I

    add-int/2addr v6, p4

    invoke-virtual {v1, v4, v5, p3, v6}, Lcom/ibm/icu/text/NFRuleSet;->format(DLjava/lang/StringBuffer;I)V

    goto :goto_1

    .line 1589
    :cond_3
    iget v1, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->pos:I

    add-int/2addr v1, p4

    iget-object v6, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->numberFormat:Lcom/ibm/icu/text/DecimalFormat;

    invoke-virtual {v6, v4, v5}, Lcom/ibm/icu/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3, v1, v6}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "that"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 1538
    invoke-super {p0, p1}, Lcom/ibm/icu/text/NFSubstitution;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 1539
    check-cast v0, Lcom/ibm/icu/text/NumeratorSubstitution;

    .line 1540
    .local v0, "that2":Lcom/ibm/icu/text/NumeratorSubstitution;
    iget-wide v2, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->denominator:D

    iget-wide v4, v0, Lcom/ibm/icu/text/NumeratorSubstitution;->denominator:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 1542
    .end local v0    # "that2":Lcom/ibm/icu/text/NumeratorSubstitution;
    :cond_0
    return v1
.end method

.method tokenChar()C
    .locals 1

    .prologue
    .line 1712
    const/16 v0, 0x3c

    return v0
.end method

.method public transformNumber(D)D
    .locals 3
    .param p1, "number"    # D

    .prologue
    .line 1609
    iget-wide v0, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->denominator:D

    mul-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    return-wide v0
.end method

.method public transformNumber(J)J
    .locals 5
    .param p1, "number"    # J

    .prologue
    .line 1600
    long-to-double v0, p1

    iget-wide v2, p0, Lcom/ibm/icu/text/NumeratorSubstitution;->denominator:D

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    return-wide v0
.end method
