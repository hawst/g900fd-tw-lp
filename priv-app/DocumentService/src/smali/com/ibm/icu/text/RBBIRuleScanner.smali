.class Lcom/ibm/icu/text/RBBIRuleScanner;
.super Ljava/lang/Object;
.source "RBBIRuleScanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/RBBIRuleScanner$RBBISetTableEl;,
        Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;
    }
.end annotation


# static fields
.field static final chLS:I = 0x2028

.field static final chNEL:I = 0x85

.field private static gRuleSet_digit_char_pattern:Ljava/lang/String;

.field private static gRuleSet_name_char_pattern:Ljava/lang/String;

.field private static gRuleSet_name_start_char_pattern:Ljava/lang/String;

.field private static gRuleSet_rule_char_pattern:Ljava/lang/String;

.field private static gRuleSet_white_space_pattern:Ljava/lang/String;

.field private static kAny:Ljava/lang/String;


# instance fields
.field fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

.field fCharNum:I

.field fLastChar:I

.field fLineNum:I

.field fLookAheadRule:Z

.field fNextIndex:I

.field fNodeStack:[Lcom/ibm/icu/text/RBBINode;

.field fNodeStackPtr:I

.field fOptionStart:I

.field fQuoteMode:Z

.field fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

.field fReverseRule:Z

.field fRuleNum:I

.field fRuleSets:[Lcom/ibm/icu/text/UnicodeSet;

.field fScanIndex:I

.field fSetTable:Ljava/util/HashMap;

.field fStack:[S

.field fStackPtr:I

.field fSymbolTable:Lcom/ibm/icu/text/RBBISymbolTable;

.field fVarName:Ljava/lang/String;

.field private final kStackSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    const-string/jumbo v0, "[^[\\p{Z}\\u0020-\\u007f]-[\\p{L}]-[\\p{N}]]"

    sput-object v0, Lcom/ibm/icu/text/RBBIRuleScanner;->gRuleSet_rule_char_pattern:Ljava/lang/String;

    .line 94
    const-string/jumbo v0, "[_\\p{L}\\p{N}]"

    sput-object v0, Lcom/ibm/icu/text/RBBIRuleScanner;->gRuleSet_name_char_pattern:Ljava/lang/String;

    .line 95
    const-string/jumbo v0, "[0-9]"

    sput-object v0, Lcom/ibm/icu/text/RBBIRuleScanner;->gRuleSet_digit_char_pattern:Ljava/lang/String;

    .line 96
    const-string/jumbo v0, "[_\\p{L}]"

    sput-object v0, Lcom/ibm/icu/text/RBBIRuleScanner;->gRuleSet_name_start_char_pattern:Ljava/lang/String;

    .line 97
    const-string/jumbo v0, "[\\p{Pattern_White_Space}]"

    sput-object v0, Lcom/ibm/icu/text/RBBIRuleScanner;->gRuleSet_white_space_pattern:Ljava/lang/String;

    .line 98
    const-string/jumbo v0, "any"

    sput-object v0, Lcom/ibm/icu/text/RBBIRuleScanner;->kAny:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/text/RBBIRuleBuilder;)V
    .locals 5
    .param p1, "rb"    # Lcom/ibm/icu/text/RBBIRuleBuilder;

    .prologue
    const/4 v4, 0x1

    const/16 v1, 0x64

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->kStackSize:I

    .line 50
    new-instance v0, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    invoke-direct {v0}, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    .line 56
    new-array v0, v1, [S

    iput-object v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fStack:[S

    .line 60
    new-array v0, v1, [Lcom/ibm/icu/text/RBBINode;

    iput-object v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fSetTable:Ljava/util/HashMap;

    .line 80
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/ibm/icu/text/UnicodeSet;

    iput-object v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRuleSets:[Lcom/ibm/icu/text/UnicodeSet;

    .line 109
    iput-object p1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    .line 110
    iput v4, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fLineNum:I

    .line 116
    iget-object v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRuleSets:[Lcom/ibm/icu/text/UnicodeSet;

    const/4 v1, 0x3

    new-instance v2, Lcom/ibm/icu/text/UnicodeSet;

    sget-object v3, Lcom/ibm/icu/text/RBBIRuleScanner;->gRuleSet_rule_char_pattern:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 117
    iget-object v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRuleSets:[Lcom/ibm/icu/text/UnicodeSet;

    const/4 v1, 0x4

    new-instance v2, Lcom/ibm/icu/text/UnicodeSet;

    sget-object v3, Lcom/ibm/icu/text/RBBIRuleScanner;->gRuleSet_white_space_pattern:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 118
    iget-object v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRuleSets:[Lcom/ibm/icu/text/UnicodeSet;

    new-instance v1, Lcom/ibm/icu/text/UnicodeSet;

    sget-object v2, Lcom/ibm/icu/text/RBBIRuleScanner;->gRuleSet_name_char_pattern:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    .line 119
    iget-object v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRuleSets:[Lcom/ibm/icu/text/UnicodeSet;

    const/4 v1, 0x2

    new-instance v2, Lcom/ibm/icu/text/UnicodeSet;

    sget-object v3, Lcom/ibm/icu/text/RBBIRuleScanner;->gRuleSet_name_start_char_pattern:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 120
    iget-object v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRuleSets:[Lcom/ibm/icu/text/UnicodeSet;

    const/4 v1, 0x0

    new-instance v2, Lcom/ibm/icu/text/UnicodeSet;

    sget-object v3, Lcom/ibm/icu/text/RBBIRuleScanner;->gRuleSet_digit_char_pattern:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 122
    new-instance v0, Lcom/ibm/icu/text/RBBISymbolTable;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/ibm/icu/text/RBBISymbolTable;-><init>(Lcom/ibm/icu/text/RBBIRuleScanner;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fSymbolTable:Lcom/ibm/icu/text/RBBISymbolTable;

    .line 123
    return-void
.end method

.method static stripRules(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "rules"    # Ljava/lang/String;

    .prologue
    .line 670
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 671
    .local v4, "strippedRules":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 672
    .local v3, "rulesLength":I
    const/4 v1, 0x0

    .local v1, "idx":I
    move v2, v1

    .end local v1    # "idx":I
    .local v2, "idx":I
    :goto_0
    if-ge v2, v3, :cond_3

    .line 673
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 674
    .local v0, "ch":C
    const/16 v5, 0x23

    if-ne v0, v5, :cond_1

    move v2, v1

    .line 676
    .end local v1    # "idx":I
    .restart local v2    # "idx":I
    :goto_1
    if-ge v2, v3, :cond_0

    const/16 v5, 0xd

    if-eq v0, v5, :cond_0

    const/16 v5, 0xa

    if-eq v0, v5, :cond_0

    const/16 v5, 0x85

    if-eq v0, v5, :cond_0

    .line 677
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v2, v1

    .line 678
    .end local v1    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_1

    :cond_0
    move v1, v2

    .line 680
    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    :cond_1
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->isISOControl(I)Z

    move-result v5

    if-nez v5, :cond_2

    .line 681
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_2
    move v2, v1

    .line 683
    .end local v1    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_0

    .line 684
    .end local v0    # "ch":C
    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method


# virtual methods
.method doParseActions(I)Z
    .locals 24
    .param p1, "action"    # I

    .prologue
    .line 134
    const/4 v7, 0x0

    .line 136
    .local v7, "n":Lcom/ibm/icu/text/RBBINode;
    const/4 v14, 0x1

    .line 138
    .local v14, "returnVal":Z
    packed-switch p1, :pswitch_data_0

    .line 485
    const v21, 0x10201

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    .line 486
    const/4 v14, 0x0

    .line 489
    :cond_0
    :goto_0
    :pswitch_0
    return v14

    .line 141
    :pswitch_1
    const/16 v21, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    .line 142
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRuleNum:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/RBBIRuleScanner;->fRuleNum:I

    goto :goto_0

    .line 146
    :pswitch_2
    const/16 v21, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->fixOpStack(I)V

    .line 147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    add-int/lit8 v23, v22, -0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    aget-object v8, v21, v22

    .line 148
    .local v8, "operandNode":Lcom/ibm/icu/text/RBBINode;
    const/16 v21, 0x9

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v10

    .line 149
    .local v10, "orNode":Lcom/ibm/icu/text/RBBINode;
    iput-object v8, v10, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 150
    iput-object v10, v8, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    goto :goto_0

    .line 161
    .end local v8    # "operandNode":Lcom/ibm/icu/text/RBBINode;
    .end local v10    # "orNode":Lcom/ibm/icu/text/RBBINode;
    :pswitch_3
    const/16 v21, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->fixOpStack(I)V

    .line 162
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    add-int/lit8 v23, v22, -0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    aget-object v8, v21, v22

    .line 163
    .restart local v8    # "operandNode":Lcom/ibm/icu/text/RBBINode;
    const/16 v21, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v4

    .line 164
    .local v4, "catNode":Lcom/ibm/icu/text/RBBINode;
    iput-object v8, v4, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 165
    iput-object v4, v8, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    goto :goto_0

    .line 176
    .end local v4    # "catNode":Lcom/ibm/icu/text/RBBINode;
    .end local v8    # "operandNode":Lcom/ibm/icu/text/RBBINode;
    :pswitch_4
    const/16 v21, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    goto :goto_0

    .line 180
    :pswitch_5
    const/16 v21, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->fixOpStack(I)V

    goto/16 :goto_0

    .line 196
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    aget-object v7, v21, v22

    .line 197
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    .line 201
    const/16 v21, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    goto/16 :goto_0

    .line 210
    :pswitch_7
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->fixOpStack(I)V

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x2

    aget-object v17, v21, v22

    .line 213
    .local v17, "startExprNode":Lcom/ibm/icu/text/RBBINode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    aget-object v20, v21, v22

    .line 214
    .local v20, "varRefNode":Lcom/ibm/icu/text/RBBINode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    aget-object v3, v21, v22

    .line 219
    .local v3, "RHSExprNode":Lcom/ibm/icu/text/RBBINode;
    move-object/from16 v0, v17

    iget v0, v0, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v3, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    .line 220
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fScanIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v3, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    .line 223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    move-object/from16 v21, v0

    iget v0, v3, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    move/from16 v22, v0

    iget v0, v3, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    move/from16 v23, v0

    invoke-virtual/range {v21 .. v23}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    iput-object v0, v3, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    .line 228
    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 229
    move-object/from16 v0, v20

    iput-object v0, v3, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fSymbolTable:Lcom/ibm/icu/text/RBBISymbolTable;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/RBBISymbolTable;->addEntry(Ljava/lang/String;Lcom/ibm/icu/text/RBBINode;)V

    .line 235
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x3

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    goto/16 :goto_0

    .line 240
    .end local v3    # "RHSExprNode":Lcom/ibm/icu/text/RBBINode;
    .end local v17    # "startExprNode":Lcom/ibm/icu/text/RBBINode;
    .end local v20    # "varRefNode":Lcom/ibm/icu/text/RBBINode;
    :pswitch_8
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->fixOpStack(I)V

    .line 243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    move-object/from16 v21, v0

    if-eqz v21, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string/jumbo v22, "rtree"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v21

    if-ltz v21, :cond_1

    .line 244
    const-string/jumbo v21, "end of rule"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->printNodeStack(Ljava/lang/String;)V

    .line 246
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_3

    const/16 v21, 0x1

    :goto_1
    invoke-static/range {v21 .. v21}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 250
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fLookAheadRule:Z

    move/from16 v21, v0

    if-eqz v21, :cond_2

    .line 251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    aget-object v18, v21, v22

    .line 252
    .local v18, "thisRule":Lcom/ibm/icu/text/RBBINode;
    const/16 v21, 0x6

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v6

    .line 253
    .local v6, "endNode":Lcom/ibm/icu/text/RBBINode;
    const/16 v21, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v4

    .line 254
    .restart local v4    # "catNode":Lcom/ibm/icu/text/RBBINode;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x2

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    .line 255
    move-object/from16 v0, v18

    iput-object v0, v4, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 256
    iput-object v6, v4, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    aput-object v4, v21, v22

    .line 258
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRuleNum:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v6, Lcom/ibm/icu/text/RBBINode;->fVal:I

    .line 259
    const/16 v21, 0x1

    move/from16 v0, v21

    iput-boolean v0, v6, Lcom/ibm/icu/text/RBBINode;->fLookAheadEnd:Z

    .line 272
    .end local v4    # "catNode":Lcom/ibm/icu/text/RBBINode;
    .end local v6    # "endNode":Lcom/ibm/icu/text/RBBINode;
    .end local v18    # "thisRule":Lcom/ibm/icu/text/RBBINode;
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fReverseRule:Z

    move/from16 v21, v0

    if-eqz v21, :cond_4

    const/4 v5, 0x1

    .line 274
    .local v5, "destRules":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    aget-object v21, v21, v5

    if-eqz v21, :cond_5

    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    aget-object v18, v21, v22

    .line 281
    .restart local v18    # "thisRule":Lcom/ibm/icu/text/RBBINode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    aget-object v12, v21, v5

    .line 282
    .local v12, "prevRules":Lcom/ibm/icu/text/RBBINode;
    const/16 v21, 0x9

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v10

    .line 283
    .restart local v10    # "orNode":Lcom/ibm/icu/text/RBBINode;
    iput-object v12, v10, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 284
    iput-object v10, v12, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 285
    move-object/from16 v0, v18

    iput-object v0, v10, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    .line 286
    move-object/from16 v0, v18

    iput-object v10, v0, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    aput-object v10, v21, v5

    .line 293
    .end local v10    # "orNode":Lcom/ibm/icu/text/RBBINode;
    .end local v12    # "prevRules":Lcom/ibm/icu/text/RBBINode;
    .end local v18    # "thisRule":Lcom/ibm/icu/text/RBBINode;
    :goto_3
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/ibm/icu/text/RBBIRuleScanner;->fReverseRule:Z

    .line 294
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/ibm/icu/text/RBBIRuleScanner;->fLookAheadRule:Z

    .line 295
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    goto/16 :goto_0

    .line 246
    .end local v5    # "destRules":I
    :cond_3
    const/16 v21, 0x0

    goto/16 :goto_1

    .line 272
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v5, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDefaultTree:I

    goto :goto_2

    .line 291
    .restart local v5    # "destRules":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v23, v0

    aget-object v22, v22, v23

    aput-object v22, v21, v5

    goto :goto_3

    .line 300
    .end local v5    # "destRules":I
    :pswitch_9
    const v21, 0x10204

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    .line 301
    const/4 v14, 0x0

    .line 302
    goto/16 :goto_0

    .line 305
    :pswitch_a
    const v21, 0x10204

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    goto/16 :goto_0

    .line 315
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    add-int/lit8 v23, v22, -0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    aget-object v8, v21, v22

    .line 316
    .restart local v8    # "operandNode":Lcom/ibm/icu/text/RBBINode;
    const/16 v21, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v11

    .line 317
    .local v11, "plusNode":Lcom/ibm/icu/text/RBBINode;
    iput-object v8, v11, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 318
    iput-object v11, v8, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    goto/16 :goto_0

    .line 323
    .end local v8    # "operandNode":Lcom/ibm/icu/text/RBBINode;
    .end local v11    # "plusNode":Lcom/ibm/icu/text/RBBINode;
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    add-int/lit8 v23, v22, -0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    aget-object v8, v21, v22

    .line 324
    .restart local v8    # "operandNode":Lcom/ibm/icu/text/RBBINode;
    const/16 v21, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v13

    .line 325
    .local v13, "qNode":Lcom/ibm/icu/text/RBBINode;
    iput-object v8, v13, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 326
    iput-object v13, v8, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    goto/16 :goto_0

    .line 331
    .end local v8    # "operandNode":Lcom/ibm/icu/text/RBBINode;
    .end local v13    # "qNode":Lcom/ibm/icu/text/RBBINode;
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    add-int/lit8 v23, v22, -0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    aget-object v8, v21, v22

    .line 332
    .restart local v8    # "operandNode":Lcom/ibm/icu/text/RBBINode;
    const/16 v21, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v16

    .line 333
    .local v16, "starNode":Lcom/ibm/icu/text/RBBINode;
    move-object/from16 v0, v16

    iput-object v8, v0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 334
    move-object/from16 v0, v16

    iput-object v0, v8, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    goto/16 :goto_0

    .line 346
    .end local v8    # "operandNode":Lcom/ibm/icu/text/RBBINode;
    .end local v16    # "starNode":Lcom/ibm/icu/text/RBBINode;
    :pswitch_e
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v7

    .line 347
    new-instance v21, Ljava/lang/StringBuffer;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-char v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    .line 348
    .local v15, "s":Ljava/lang/String;
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v15, v7, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->findSetFor(Ljava/lang/String;Lcom/ibm/icu/text/RBBINode;Lcom/ibm/icu/text/UnicodeSet;)V

    .line 349
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fScanIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    .line 350
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    .line 351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    move-object/from16 v21, v0

    iget v0, v7, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    move/from16 v22, v0

    iget v0, v7, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    move/from16 v23, v0

    invoke-virtual/range {v21 .. v23}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    iput-object v0, v7, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    goto/16 :goto_0

    .line 358
    .end local v15    # "s":Ljava/lang/String;
    :pswitch_f
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v7

    .line 359
    sget-object v21, Lcom/ibm/icu/text/RBBIRuleScanner;->kAny:Ljava/lang/String;

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v7, v2}, Lcom/ibm/icu/text/RBBIRuleScanner;->findSetFor(Ljava/lang/String;Lcom/ibm/icu/text/RBBINode;Lcom/ibm/icu/text/UnicodeSet;)V

    .line 360
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fScanIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    .line 361
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    .line 362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    move-object/from16 v21, v0

    iget v0, v7, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    move/from16 v22, v0

    iget v0, v7, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    move/from16 v23, v0

    invoke-virtual/range {v21 .. v23}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    iput-object v0, v7, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    goto/16 :goto_0

    .line 369
    :pswitch_10
    const/16 v21, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v7

    .line 370
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRuleNum:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fVal:I

    .line 371
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fScanIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    .line 372
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    .line 373
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    move-object/from16 v21, v0

    iget v0, v7, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    move/from16 v22, v0

    iget v0, v7, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    move/from16 v23, v0

    invoke-virtual/range {v21 .. v23}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    iput-object v0, v7, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    .line 374
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/ibm/icu/text/RBBIRuleScanner;->fLookAheadRule:Z

    goto/16 :goto_0

    .line 380
    :pswitch_11
    const/16 v21, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v7

    .line 381
    const/16 v21, 0x0

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fVal:I

    .line 382
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fScanIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    .line 383
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    goto/16 :goto_0

    .line 389
    :pswitch_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    aget-object v7, v21, v22

    .line 390
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-char v0, v0

    move/from16 v21, v0

    const/16 v22, 0xa

    invoke-static/range {v21 .. v22}, Ljava/lang/Character;->digit(CI)I

    move-result v19

    .line 391
    .local v19, "v":I
    iget v0, v7, Lcom/ibm/icu/text/RBBINode;->fVal:I

    move/from16 v21, v0

    mul-int/lit8 v21, v21, 0xa

    add-int v21, v21, v19

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fVal:I

    goto/16 :goto_0

    .line 396
    .end local v19    # "v":I
    :pswitch_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    aget-object v7, v21, v22

    .line 397
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    .line 398
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    move-object/from16 v21, v0

    iget v0, v7, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    move/from16 v22, v0

    iget v0, v7, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    move/from16 v23, v0

    invoke-virtual/range {v21 .. v23}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    iput-object v0, v7, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    goto/16 :goto_0

    .line 402
    :pswitch_14
    const v21, 0x1020e

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    .line 403
    const/4 v14, 0x0

    .line 404
    goto/16 :goto_0

    .line 408
    :pswitch_15
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fScanIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/RBBIRuleScanner;->fOptionStart:I

    goto/16 :goto_0

    .line 412
    :pswitch_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fOptionStart:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fScanIndex:I

    move/from16 v23, v0

    invoke-virtual/range {v21 .. v23}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 413
    .local v9, "opt":Ljava/lang/String;
    const-string/jumbo v21, "chain"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput-boolean v0, v1, Lcom/ibm/icu/text/RBBIRuleBuilder;->fChainRules:Z

    goto/16 :goto_0

    .line 415
    :cond_6
    const-string/jumbo v21, "LBCMNoChain"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput-boolean v0, v1, Lcom/ibm/icu/text/RBBIRuleBuilder;->fLBCMNoChain:Z

    goto/16 :goto_0

    .line 417
    :cond_7
    const-string/jumbo v21, "forward"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 418
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDefaultTree:I

    goto/16 :goto_0

    .line 419
    :cond_8
    const-string/jumbo v21, "reverse"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 420
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDefaultTree:I

    goto/16 :goto_0

    .line 421
    :cond_9
    const-string/jumbo v21, "safe_forward"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 422
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDefaultTree:I

    goto/16 :goto_0

    .line 423
    :cond_a
    const-string/jumbo v21, "safe_reverse"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    const/16 v22, 0x3

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDefaultTree:I

    goto/16 :goto_0

    .line 425
    :cond_b
    const-string/jumbo v21, "lookAheadHardBreak"

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 426
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput-boolean v0, v1, Lcom/ibm/icu/text/RBBIRuleBuilder;->fLookAheadHardBreak:Z

    goto/16 :goto_0

    .line 428
    :cond_c
    const v21, 0x1020d

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    goto/16 :goto_0

    .line 434
    .end local v9    # "opt":Ljava/lang/String;
    :pswitch_17
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/ibm/icu/text/RBBIRuleScanner;->fReverseRule:Z

    goto/16 :goto_0

    .line 438
    :pswitch_18
    const/16 v21, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v7

    .line 439
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fScanIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    goto/16 :goto_0

    .line 443
    :pswitch_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    aget-object v7, v21, v22

    .line 444
    if-eqz v7, :cond_d

    iget v0, v7, Lcom/ibm/icu/text/RBBINode;->fType:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_e

    .line 445
    :cond_d
    const v21, 0x10201

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    goto/16 :goto_0

    .line 448
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fScanIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v7, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    .line 449
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    move-object/from16 v21, v0

    iget v0, v7, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, 0x1

    iget v0, v7, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    move/from16 v23, v0

    invoke-virtual/range {v21 .. v23}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    iput-object v0, v7, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    .line 457
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fSymbolTable:Lcom/ibm/icu/text/RBBISymbolTable;

    move-object/from16 v21, v0

    iget-object v0, v7, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/ibm/icu/text/RBBISymbolTable;->lookupNode(Ljava/lang/String;)Lcom/ibm/icu/text/RBBINode;

    move-result-object v21

    move-object/from16 v0, v21

    iput-object v0, v7, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    goto/16 :goto_0

    .line 461
    :pswitch_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    move/from16 v22, v0

    aget-object v7, v21, v22

    .line 462
    iget-object v0, v7, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    move-object/from16 v21, v0

    if-nez v21, :cond_0

    .line 463
    const v21, 0x1020a

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    .line 464
    const/4 v14, 0x0

    .line 465
    goto/16 :goto_0

    .line 472
    :pswitch_1b
    const v21, 0x10206

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    .line 473
    const/4 v14, 0x0

    .line 474
    goto/16 :goto_0

    .line 477
    :pswitch_1c
    const/4 v14, 0x0

    .line 478
    goto/16 :goto_0

    .line 481
    :pswitch_1d
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/RBBIRuleScanner;->scanSet()V

    goto/16 :goto_0

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1a
        :pswitch_f
        :pswitch_7
        :pswitch_8
        :pswitch_19
        :pswitch_1c
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_16
        :pswitch_15
        :pswitch_17
        :pswitch_e
        :pswitch_9
        :pswitch_1b
        :pswitch_1d
        :pswitch_10
        :pswitch_6
        :pswitch_11
        :pswitch_18
        :pswitch_12
        :pswitch_14
        :pswitch_13
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_a
    .end packed-switch
.end method

.method error(I)V
    .locals 4
    .param p1, "e"    # I

    .prologue
    .line 499
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v3, "Error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, " at line "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget v3, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fLineNum:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, " column "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget v3, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fCharNum:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 501
    .local v1, "s":Ljava/lang/String;
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 502
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    throw v0
.end method

.method findSetFor(Ljava/lang/String;Lcom/ibm/icu/text/RBBINode;Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 6
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "node"    # Lcom/ibm/icu/text/RBBINode;
    .param p3, "setToAdopt"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 607
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fSetTable:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBISetTableEl;

    .line 608
    .local v1, "el":Lcom/ibm/icu/text/RBBIRuleScanner$RBBISetTableEl;
    if-eqz v1, :cond_1

    .line 609
    iget-object v5, v1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBISetTableEl;->val:Lcom/ibm/icu/text/RBBINode;

    iput-object v5, p2, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 610
    iget-object v5, p2, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget v5, v5, Lcom/ibm/icu/text/RBBINode;->fType:I

    if-ne v5, v3, :cond_0

    :goto_0
    invoke-static {v3}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 651
    :goto_1
    return-void

    :cond_0
    move v3, v4

    .line 610
    goto :goto_0

    .line 617
    :cond_1
    if-nez p3, :cond_2

    .line 618
    sget-object v5, Lcom/ibm/icu/text/RBBIRuleScanner;->kAny:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 619
    new-instance p3, Lcom/ibm/icu/text/UnicodeSet;

    .end local p3    # "setToAdopt":Lcom/ibm/icu/text/UnicodeSet;
    const v5, 0x10ffff

    invoke-direct {p3, v4, v5}, Lcom/ibm/icu/text/UnicodeSet;-><init>(II)V

    .line 632
    .restart local p3    # "setToAdopt":Lcom/ibm/icu/text/UnicodeSet;
    :cond_2
    :goto_2
    new-instance v2, Lcom/ibm/icu/text/RBBINode;

    invoke-direct {v2, v3}, Lcom/ibm/icu/text/RBBINode;-><init>(I)V

    .line 633
    .local v2, "usetNode":Lcom/ibm/icu/text/RBBINode;
    iput-object p3, v2, Lcom/ibm/icu/text/RBBINode;->fInputSet:Lcom/ibm/icu/text/UnicodeSet;

    .line 634
    iput-object p2, v2, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 635
    iput-object v2, p2, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 636
    iput-object p1, v2, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    .line 641
    iget-object v3, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIRuleBuilder;->fUSetNodes:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 646
    new-instance v1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBISetTableEl;

    .end local v1    # "el":Lcom/ibm/icu/text/RBBIRuleScanner$RBBISetTableEl;
    invoke-direct {v1}, Lcom/ibm/icu/text/RBBIRuleScanner$RBBISetTableEl;-><init>()V

    .line 647
    .restart local v1    # "el":Lcom/ibm/icu/text/RBBIRuleScanner$RBBISetTableEl;
    iput-object p1, v1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBISetTableEl;->key:Ljava/lang/String;

    .line 648
    iput-object v2, v1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBISetTableEl;->val:Lcom/ibm/icu/text/RBBINode;

    .line 649
    iget-object v3, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fSetTable:Ljava/util/HashMap;

    iget-object v4, v1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBISetTableEl;->key:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 622
    .end local v2    # "usetNode":Lcom/ibm/icu/text/RBBINode;
    :cond_3
    invoke-static {p1, v4}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 623
    .local v0, "c":I
    new-instance p3, Lcom/ibm/icu/text/UnicodeSet;

    .end local p3    # "setToAdopt":Lcom/ibm/icu/text/UnicodeSet;
    invoke-direct {p3, v0, v0}, Lcom/ibm/icu/text/UnicodeSet;-><init>(II)V

    .restart local p3    # "setToAdopt":Lcom/ibm/icu/text/UnicodeSet;
    goto :goto_2
.end method

.method fixOpStack(I)V
    .locals 5
    .param p1, "p"    # I

    .prologue
    const/4 v3, 0x2

    .line 526
    :goto_0
    iget-object v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    iget v2, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    add-int/lit8 v2, v2, -0x1

    aget-object v0, v1, v2

    .line 527
    .local v0, "n":Lcom/ibm/icu/text/RBBINode;
    iget v1, v0, Lcom/ibm/icu/text/RBBINode;->fPrecedence:I

    if-nez v1, :cond_1

    .line 528
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v2, "RBBIRuleScanner.fixOpStack, bad operator node"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 529
    const v1, 0x10201

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    .line 564
    :cond_0
    :goto_1
    return-void

    .line 533
    :cond_1
    iget v1, v0, Lcom/ibm/icu/text/RBBINode;->fPrecedence:I

    if-lt v1, p1, :cond_2

    iget v1, v0, Lcom/ibm/icu/text/RBBINode;->fPrecedence:I

    if-gt v1, v3, :cond_4

    .line 547
    :cond_2
    if-gt p1, v3, :cond_0

    .line 553
    iget v1, v0, Lcom/ibm/icu/text/RBBINode;->fPrecedence:I

    if-eq v1, p1, :cond_3

    .line 556
    const v1, 0x10208

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    .line 558
    :cond_3
    iget-object v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    iget v2, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    iget v4, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    aget-object v3, v3, v4

    aput-object v3, v1, v2

    .line 559
    iget v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    goto :goto_1

    .line 541
    :cond_4
    iget-object v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    iget v2, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    .line 542
    iget-object v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    iget v2, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    aget-object v1, v1, v2

    iput-object v0, v1, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 543
    iget v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    goto :goto_0
.end method

.method nextChar(Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;)V
    .locals 7
    .param p1, "c"    # Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    .prologue
    const/16 v6, 0x27

    const/4 v5, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 738
    iget v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    iput v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fScanIndex:I

    .line 739
    invoke-virtual {p0}, Lcom/ibm/icu/text/RBBIRuleScanner;->nextCharLL()I

    move-result v1

    iput v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    .line 740
    iput-boolean v3, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fEscaped:Z

    .line 746
    iget v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    if-ne v1, v6, :cond_0

    .line 747
    iget-object v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    iget v4, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    invoke-static {v1, v4}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 748
    invoke-virtual {p0}, Lcom/ibm/icu/text/RBBIRuleScanner;->nextCharLL()I

    move-result v1

    iput v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    .line 749
    iput-boolean v2, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fEscaped:Z

    .line 765
    :cond_0
    iget-boolean v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fQuoteMode:Z

    if-eqz v1, :cond_5

    .line 766
    iput-boolean v2, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fEscaped:Z

    .line 810
    :cond_1
    :goto_0
    return-void

    .line 754
    :cond_2
    iget-boolean v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fQuoteMode:Z

    if-nez v1, :cond_3

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fQuoteMode:Z

    .line 755
    iget-boolean v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fQuoteMode:Z

    if-ne v1, v2, :cond_4

    .line 756
    const/16 v1, 0x28

    iput v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    .line 760
    :goto_2
    iput-boolean v3, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fEscaped:Z

    goto :goto_0

    :cond_3
    move v1, v3

    .line 754
    goto :goto_1

    .line 758
    :cond_4
    const/16 v1, 0x29

    iput v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    goto :goto_2

    .line 770
    :cond_5
    iget v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    const/16 v4, 0x23

    if-ne v1, v4, :cond_7

    .line 777
    :cond_6
    invoke-virtual {p0}, Lcom/ibm/icu/text/RBBIRuleScanner;->nextCharLL()I

    move-result v1

    iput v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    .line 778
    iget v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    if-eq v1, v5, :cond_7

    iget v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    const/16 v4, 0xd

    if-eq v1, v4, :cond_7

    iget v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    const/16 v4, 0xa

    if-eq v1, v4, :cond_7

    iget v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    const/16 v4, 0x85

    if-eq v1, v4, :cond_7

    iget v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    const/16 v4, 0x2028

    if-ne v1, v4, :cond_6

    .line 788
    :cond_7
    iget v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    if-eq v1, v5, :cond_1

    .line 796
    iget v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    const/16 v4, 0x5c

    if-ne v1, v4, :cond_1

    .line 797
    iput-boolean v2, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fEscaped:Z

    .line 798
    new-array v0, v2, [I

    .line 799
    .local v0, "unescapeIndex":[I
    iget v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    aput v1, v0, v3

    .line 800
    iget-object v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/ibm/icu/impl/Utility;->unescapeAt(Ljava/lang/String;[I)I

    move-result v1

    iput v1, p1, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    .line 801
    aget v1, v0, v3

    iget v2, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    if-ne v1, v2, :cond_8

    .line 802
    const v1, 0x10202

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    .line 805
    :cond_8
    iget v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fCharNum:I

    aget v2, v0, v3

    iget v4, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    sub-int/2addr v2, v4

    add-int/2addr v1, v2

    iput v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fCharNum:I

    .line 806
    aget v1, v0, v3

    iput v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    goto :goto_0
.end method

.method nextCharLL()I
    .locals 7

    .prologue
    const/16 v6, 0xd

    const/16 v5, 0xa

    const/4 v4, 0x0

    .line 697
    iget v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    iget-object v2, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 698
    const/4 v0, -0x1

    .line 723
    :goto_0
    return v0

    .line 700
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    iget v2, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    invoke-static {v1, v2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 701
    .local v0, "ch":I
    iget-object v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    iget v2, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/UTF16;->moveCodePointOffset(Ljava/lang/String;II)I

    move-result v1

    iput v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    .line 703
    if-eq v0, v6, :cond_1

    const/16 v1, 0x85

    if-eq v0, v1, :cond_1

    const/16 v1, 0x2028

    if-eq v0, v1, :cond_1

    if-ne v0, v5, :cond_3

    iget v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fLastChar:I

    if-eq v1, v6, :cond_3

    .line 709
    :cond_1
    iget v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fLineNum:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fLineNum:I

    .line 710
    iput v4, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fCharNum:I

    .line 711
    iget-boolean v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fQuoteMode:Z

    if-eqz v1, :cond_2

    .line 712
    const v1, 0x10209

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    .line 713
    iput-boolean v4, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fQuoteMode:Z

    .line 722
    :cond_2
    :goto_1
    iput v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fLastChar:I

    goto :goto_0

    .line 718
    :cond_3
    if-eq v0, v5, :cond_2

    .line 719
    iget v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fCharNum:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fCharNum:I

    goto :goto_1
.end method

.method parse()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v9, -0x1

    const/4 v8, 0x1

    .line 824
    const/4 v1, 0x1

    .line 825
    .local v1, "state":I
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    invoke-virtual {p0, v5}, Lcom/ibm/icu/text/RBBIRuleScanner;->nextChar(Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;)V

    .line 837
    :cond_0
    :goto_0
    if-nez v1, :cond_5

    .line 941
    :cond_1
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    aget-object v5, v5, v8

    if-nez v5, :cond_2

    .line 942
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    const/16 v6, 0xa

    invoke-virtual {p0, v6}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v6

    aput-object v6, v5, v8

    .line 943
    invoke-virtual {p0, v10}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v0

    .line 944
    .local v0, "operand":Lcom/ibm/icu/text/RBBINode;
    sget-object v5, Lcom/ibm/icu/text/RBBIRuleScanner;->kAny:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v0, v6}, Lcom/ibm/icu/text/RBBIRuleScanner;->findSetFor(Ljava/lang/String;Lcom/ibm/icu/text/RBBINode;Lcom/ibm/icu/text/UnicodeSet;)V

    .line 945
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    aget-object v5, v5, v8

    iput-object v0, v5, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 946
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    aget-object v5, v5, v8

    iput-object v5, v0, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 947
    iget v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    add-int/lit8 v5, v5, -0x2

    iput v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    .line 955
    .end local v0    # "operand":Lcom/ibm/icu/text/RBBINode;
    :cond_2
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v6, "symbols"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ltz v5, :cond_3

    .line 956
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fSymbolTable:Lcom/ibm/icu/text/RBBISymbolTable;

    invoke-virtual {v5}, Lcom/ibm/icu/text/RBBISymbolTable;->rbbiSymtablePrint()V

    .line 958
    :cond_3
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v6, "ptree"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ltz v5, :cond_4

    .line 959
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "Completed Forward Rules Parse Tree..."

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 960
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    aget-object v5, v5, v10

    invoke-virtual {v5, v8}, Lcom/ibm/icu/text/RBBINode;->printTree(Z)V

    .line 961
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "\nCompleted Reverse Rules Parse Tree..."

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 962
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    aget-object v5, v5, v8

    invoke-virtual {v5, v8}, Lcom/ibm/icu/text/RBBINode;->printTree(Z)V

    .line 963
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "\nCompleted Safe Point Forward Rules Parse Tree..."

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 964
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    aget-object v5, v5, v11

    if-nez v5, :cond_13

    .line 965
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "  -- null -- "

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 969
    :goto_1
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "\nCompleted Safe Point Reverse Rules Parse Tree..."

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 970
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    aget-object v5, v5, v12

    if-nez v5, :cond_14

    .line 971
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "  -- null -- "

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 976
    :cond_4
    :goto_2
    return-void

    .line 847
    :cond_5
    sget-object v5, Lcom/ibm/icu/text/RBBIRuleParseTable;->gRuleParseStateTable:[Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    aget-object v2, v5, v1

    .line 848
    .local v2, "tableEl":Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v6, "scan"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ltz v5, :cond_6

    .line 849
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "char, line, col = (\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    iget v7, v7, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    int-to-char v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "\', "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget v7, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fLineNum:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget v7, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fCharNum:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, "    state = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v7, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fStateName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 854
    :cond_6
    move v3, v1

    .line 855
    .local v3, "tableRow":I
    :goto_3
    sget-object v5, Lcom/ibm/icu/text/RBBIRuleParseTable;->gRuleParseStateTable:[Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;

    aget-object v2, v5, v3

    .line 856
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v6, "scan"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ltz v5, :cond_7

    .line 857
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "."

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 859
    :cond_7
    iget-short v5, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fCharClass:S

    const/16 v6, 0x7f

    if-ge v5, v6, :cond_d

    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    iget-boolean v5, v5, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fEscaped:Z

    if-nez v5, :cond_d

    iget-short v5, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fCharClass:S

    iget-object v6, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    iget v6, v6, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    if-ne v5, v6, :cond_d

    .line 896
    :cond_8
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fDebugEnv:Ljava/lang/String;

    const-string/jumbo v6, "scan"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ltz v5, :cond_9

    .line 897
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, ""

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 903
    :cond_9
    iget-short v5, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fAction:S

    invoke-virtual {p0, v5}, Lcom/ibm/icu/text/RBBIRuleScanner;->doParseActions(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 910
    iget-short v5, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fPushState:S

    if-eqz v5, :cond_b

    .line 911
    iget v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fStackPtr:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fStackPtr:I

    .line 912
    iget v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fStackPtr:I

    const/16 v6, 0x64

    if-lt v5, v6, :cond_a

    .line 913
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "RBBIRuleScanner.parse() - state stack overflow."

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 914
    const v5, 0x10201

    invoke-virtual {p0, v5}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    .line 916
    :cond_a
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fStack:[S

    iget v6, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fStackPtr:I

    iget-short v7, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fPushState:S

    aput-short v7, v5, v6

    .line 919
    :cond_b
    iget-boolean v5, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fNextChar:Z

    if-eqz v5, :cond_c

    .line 920
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    invoke-virtual {p0, v5}, Lcom/ibm/icu/text/RBBIRuleScanner;->nextChar(Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;)V

    .line 925
    :cond_c
    iget-short v5, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fNextState:S

    const/16 v6, 0xff

    if-eq v5, v6, :cond_12

    .line 926
    iget-short v1, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fNextState:S

    .line 927
    goto/16 :goto_0

    .line 866
    :cond_d
    iget-short v5, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fCharClass:S

    const/16 v6, 0xff

    if-eq v5, v6, :cond_8

    .line 870
    iget-short v5, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fCharClass:S

    const/16 v6, 0xfe

    if-ne v5, v6, :cond_e

    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    iget-boolean v5, v5, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fEscaped:Z

    if-nez v5, :cond_8

    .line 874
    :cond_e
    iget-short v5, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fCharClass:S

    const/16 v6, 0xfd

    if-ne v5, v6, :cond_f

    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    iget-boolean v5, v5, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fEscaped:Z

    if-eqz v5, :cond_f

    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    iget v5, v5, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    const/16 v6, 0x50

    if-eq v5, v6, :cond_8

    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    iget v5, v5, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    const/16 v6, 0x70

    if-eq v5, v6, :cond_8

    .line 879
    :cond_f
    iget-short v5, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fCharClass:S

    const/16 v6, 0xfc

    if-ne v5, v6, :cond_10

    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    iget v5, v5, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    if-eq v5, v9, :cond_8

    .line 884
    :cond_10
    iget-short v5, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fCharClass:S

    const/16 v6, 0x80

    if-lt v5, v6, :cond_11

    iget-short v5, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fCharClass:S

    const/16 v6, 0xf0

    if-ge v5, v6, :cond_11

    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    iget-boolean v5, v5, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fEscaped:Z

    if-nez v5, :cond_11

    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    iget v5, v5, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    if-eq v5, v9, :cond_11

    .line 887
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRuleSets:[Lcom/ibm/icu/text/UnicodeSet;

    iget-short v6, v2, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fCharClass:S

    add-int/lit8 v6, v6, -0x80

    aget-object v4, v5, v6

    .line 888
    .local v4, "uniset":Lcom/ibm/icu/text/UnicodeSet;
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fC:Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;

    iget v5, v5, Lcom/ibm/icu/text/RBBIRuleScanner$RBBIRuleChar;->fChar:I

    invoke-virtual {v4, v5}, Lcom/ibm/icu/text/UnicodeSet;->contains(I)Z

    move-result v5

    if-nez v5, :cond_8

    .line 854
    .end local v4    # "uniset":Lcom/ibm/icu/text/UnicodeSet;
    :cond_11
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    .line 928
    :cond_12
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fStack:[S

    iget v6, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fStackPtr:I

    aget-short v1, v5, v6

    .line 929
    iget v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fStackPtr:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fStackPtr:I

    .line 930
    iget v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fStackPtr:I

    if-gez v5, :cond_0

    .line 931
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v6, "RBBIRuleScanner.parse() - state stack underflow."

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 932
    const v5, 0x10201

    invoke-virtual {p0, v5}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    goto/16 :goto_0

    .line 967
    .end local v2    # "tableEl":Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;
    .end local v3    # "tableRow":I
    :cond_13
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    aget-object v5, v5, v11

    invoke-virtual {v5, v8}, Lcom/ibm/icu/text/RBBINode;->printTree(Z)V

    goto/16 :goto_1

    .line 973
    :cond_14
    iget-object v5, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIRuleBuilder;->fTreeRoots:[Lcom/ibm/icu/text/RBBINode;

    aget-object v5, v5, v12

    invoke-virtual {v5, v8}, Lcom/ibm/icu/text/RBBINode;->printTree(Z)V

    goto/16 :goto_2
.end method

.method printNodeStack(Ljava/lang/String;)V
    .locals 4
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 986
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string/jumbo v3, ".  Dumping node stack...\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 987
    iget v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    .local v0, "i":I
    :goto_0
    if-lez v0, :cond_0

    .line 988
    iget-object v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    aget-object v1, v1, v0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/ibm/icu/text/RBBINode;->printTree(Z)V

    .line 987
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 990
    :cond_0
    return-void
.end method

.method pushNewNode(I)Lcom/ibm/icu/text/RBBINode;
    .locals 3
    .param p1, "nodeType"    # I

    .prologue
    .line 1000
    iget v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    .line 1001
    iget v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 1002
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "RBBIRuleScanner.pushNewNode - stack overflow."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1003
    const v0, 0x10201

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    .line 1005
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    iget v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    new-instance v2, Lcom/ibm/icu/text/RBBINode;

    invoke-direct {v2, p1}, Lcom/ibm/icu/text/RBBINode;-><init>(I)V

    aput-object v2, v0, v1

    .line 1006
    iget-object v0, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStack:[Lcom/ibm/icu/text/RBBINode;

    iget v1, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNodeStackPtr:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method scanSet()V
    .locals 10

    .prologue
    .line 1024
    const/4 v5, 0x0

    .line 1026
    .local v5, "uset":Lcom/ibm/icu/text/UnicodeSet;
    new-instance v3, Ljava/text/ParsePosition;

    iget v7, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fScanIndex:I

    invoke-direct {v3, v7}, Ljava/text/ParsePosition;-><init>(I)V

    .line 1029
    .local v3, "pos":Ljava/text/ParsePosition;
    iget v4, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fScanIndex:I

    .line 1031
    .local v4, "startPos":I
    :try_start_0
    new-instance v6, Lcom/ibm/icu/text/UnicodeSet;

    iget-object v7, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v7, v7, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    iget-object v8, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fSymbolTable:Lcom/ibm/icu/text/RBBISymbolTable;

    const/4 v9, 0x1

    invoke-direct {v6, v7, v3, v8, v9}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/SymbolTable;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v5    # "uset":Lcom/ibm/icu/text/UnicodeSet;
    .local v6, "uset":Lcom/ibm/icu/text/UnicodeSet;
    move-object v5, v6

    .line 1039
    .end local v6    # "uset":Lcom/ibm/icu/text/UnicodeSet;
    .restart local v5    # "uset":Lcom/ibm/icu/text/UnicodeSet;
    :goto_0
    invoke-virtual {v5}, Lcom/ibm/icu/text/UnicodeSet;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1045
    const v7, 0x1020c

    invoke-virtual {p0, v7}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    .line 1051
    :cond_0
    invoke-virtual {v3}, Ljava/text/ParsePosition;->getIndex()I

    move-result v1

    .line 1053
    .local v1, "i":I
    :goto_1
    iget v7, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    if-lt v7, v1, :cond_1

    .line 1061
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/ibm/icu/text/RBBIRuleScanner;->pushNewNode(I)Lcom/ibm/icu/text/RBBINode;

    move-result-object v2

    .line 1062
    .local v2, "n":Lcom/ibm/icu/text/RBBINode;
    iput v4, v2, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    .line 1063
    iget v7, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fNextIndex:I

    iput v7, v2, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    .line 1064
    iget-object v7, p0, Lcom/ibm/icu/text/RBBIRuleScanner;->fRB:Lcom/ibm/icu/text/RBBIRuleBuilder;

    iget-object v7, v7, Lcom/ibm/icu/text/RBBIRuleBuilder;->fRules:Ljava/lang/String;

    iget v8, v2, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    iget v9, v2, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    .line 1071
    iget-object v7, v2, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    invoke-virtual {p0, v7, v2, v5}, Lcom/ibm/icu/text/RBBIRuleScanner;->findSetFor(Ljava/lang/String;Lcom/ibm/icu/text/RBBINode;Lcom/ibm/icu/text/UnicodeSet;)V

    .line 1072
    return-void

    .line 1032
    .end local v1    # "i":I
    .end local v2    # "n":Lcom/ibm/icu/text/RBBINode;
    :catch_0
    move-exception v0

    .line 1034
    .local v0, "e":Ljava/lang/Exception;
    const v7, 0x1020f

    invoke-virtual {p0, v7}, Lcom/ibm/icu/text/RBBIRuleScanner;->error(I)V

    goto :goto_0

    .line 1056
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "i":I
    :cond_1
    invoke-virtual {p0}, Lcom/ibm/icu/text/RBBIRuleScanner;->nextCharLL()I

    goto :goto_1
.end method
