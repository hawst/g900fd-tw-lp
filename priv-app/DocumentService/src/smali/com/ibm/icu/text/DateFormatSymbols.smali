.class public Lcom/ibm/icu/text/DateFormatSymbols;
.super Ljava/lang/Object;
.source "DateFormatSymbols.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final ABBREVIATED:I = 0x0

.field static final DEFAULT_GMT_HOUR_PATTERNS:[[Ljava/lang/String;

.field static final DEFAULT_GMT_PATTERN:Ljava/lang/String; = "GMT{0}"

.field private static DFSCACHE:Lcom/ibm/icu/impl/ICUCache; = null

.field public static final DT_CONTEXT_COUNT:I = 0x2

.field public static final DT_WIDTH_COUNT:I = 0x3

.field public static final FORMAT:I = 0x0

.field public static final NARROW:I = 0x2

.field static final OFFSET_HM:I = 0x1

.field static final OFFSET_HMS:I = 0x0

.field static final OFFSET_NEGATIVE:I = 0x0

.field static final OFFSET_POSITIVE:I = 0x1

.field public static final STANDALONE:I = 0x1

.field public static final WIDE:I = 0x1

.field static final millisPerHour:I = 0x36ee80

.field static final patternChars:Ljava/lang/String; = "GyMdkHmsSEDFwWahKzYeugAZvcLQqV"

.field private static final serialVersionUID:J = -0x53198e36cae8e83eL


# instance fields
.field private actualLocale:Lcom/ibm/icu/util/ULocale;

.field ampms:[Ljava/lang/String;

.field eraNames:[Ljava/lang/String;

.field eras:[Ljava/lang/String;

.field gmtFormat:Ljava/lang/String;

.field gmtHourFormats:[[Ljava/lang/String;

.field localPatternChars:Ljava/lang/String;

.field months:[Ljava/lang/String;

.field narrowEras:[Ljava/lang/String;

.field narrowMonths:[Ljava/lang/String;

.field narrowWeekdays:[Ljava/lang/String;

.field quarters:[Ljava/lang/String;

.field private requestedLocale:Lcom/ibm/icu/util/ULocale;

.field shortMonths:[Ljava/lang/String;

.field shortQuarters:[Ljava/lang/String;

.field shortWeekdays:[Ljava/lang/String;

.field standaloneMonths:[Ljava/lang/String;

.field standaloneNarrowMonths:[Ljava/lang/String;

.field standaloneNarrowWeekdays:[Ljava/lang/String;

.field standaloneQuarters:[Ljava/lang/String;

.field standaloneShortMonths:[Ljava/lang/String;

.field standaloneShortQuarters:[Ljava/lang/String;

.field standaloneShortWeekdays:[Ljava/lang/String;

.field standaloneWeekdays:[Ljava/lang/String;

.field private validLocale:Lcom/ibm/icu/util/ULocale;

.field weekdays:[Ljava/lang/String;

.field private zoneStrings:[[Ljava/lang/String;

.field private transient zsformat:Lcom/ibm/icu/impl/ZoneStringFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 981
    new-instance v0, Lcom/ibm/icu/impl/SimpleCache;

    invoke-direct {v0}, Lcom/ibm/icu/impl/SimpleCache;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/DateFormatSymbols;->DFSCACHE:Lcom/ibm/icu/impl/ICUCache;

    .line 1218
    new-array v0, v5, [[Ljava/lang/String;

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "-HH:mm:ss"

    aput-object v2, v1, v3

    const-string/jumbo v2, "-HH:mm"

    aput-object v2, v1, v4

    aput-object v1, v0, v3

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "+HH:mm:ss"

    aput-object v2, v1, v3

    const-string/jumbo v2, "+HH:mm"

    aput-object v2, v1, v4

    aput-object v1, v0, v4

    sput-object v0, Lcom/ibm/icu/text/DateFormatSymbols;->DEFAULT_GMT_HOUR_PATTERNS:[[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 139
    invoke-static {}, Lcom/ibm/icu/util/ULocale;->getDefault()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateFormatSymbols;-><init>(Lcom/ibm/icu/util/ULocale;)V

    .line 140
    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/ULocale;)V
    .locals 2
    .param p1, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v1, 0x0

    .line 1488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eras:[Ljava/lang/String;

    .line 263
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;

    .line 270
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowEras:[Ljava/lang/String;

    .line 278
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    .line 287
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    .line 296
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;

    .line 304
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;

    .line 313
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;

    .line 322
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;

    .line 331
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    .line 340
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    .line 349
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    .line 358
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    .line 367
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    .line 376
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    .line 384
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->ampms:[Ljava/lang/String;

    .line 391
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortQuarters:[Ljava/lang/String;

    .line 398
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->quarters:[Ljava/lang/String;

    .line 405
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortQuarters:[Ljava/lang/String;

    .line 412
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneQuarters:[Ljava/lang/String;

    .line 418
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    move-object v0, v1

    .line 429
    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    move-object v0, v1

    .line 464
    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    .line 471
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zsformat:Lcom/ibm/icu/impl/ZoneStringFormat;

    .line 489
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->localPatternChars:Ljava/lang/String;

    .line 1489
    invoke-virtual {p1}, Lcom/ibm/icu/util/Calendar;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->initializeData(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)V

    .line 1490
    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/util/Calendar;Ljava/util/Locale;)V
    .locals 2
    .param p1, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p2, "locale"    # Ljava/util/Locale;

    .prologue
    const/4 v1, 0x0

    .line 1424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eras:[Ljava/lang/String;

    .line 263
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;

    .line 270
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowEras:[Ljava/lang/String;

    .line 278
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    .line 287
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    .line 296
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;

    .line 304
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;

    .line 313
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;

    .line 322
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;

    .line 331
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    .line 340
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    .line 349
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    .line 358
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    .line 367
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    .line 376
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    .line 384
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->ampms:[Ljava/lang/String;

    .line 391
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortQuarters:[Ljava/lang/String;

    .line 398
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->quarters:[Ljava/lang/String;

    .line 405
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortQuarters:[Ljava/lang/String;

    .line 412
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneQuarters:[Ljava/lang/String;

    .line 418
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    move-object v0, v1

    .line 429
    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    move-object v0, v1

    .line 464
    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    .line 471
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zsformat:Lcom/ibm/icu/impl/ZoneStringFormat;

    .line 489
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->localPatternChars:Ljava/lang/String;

    .line 1425
    invoke-static {p2}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-virtual {p1}, Lcom/ibm/icu/util/Calendar;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/ibm/icu/text/DateFormatSymbols;->initializeData(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)V

    .line 1426
    return-void
.end method

.method public constructor <init>(Lcom/ibm/icu/util/ULocale;)V
    .locals 2
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v1, 0x0

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eras:[Ljava/lang/String;

    .line 263
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;

    .line 270
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowEras:[Ljava/lang/String;

    .line 278
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    .line 287
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    .line 296
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;

    .line 304
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;

    .line 313
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;

    .line 322
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;

    .line 331
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    .line 340
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    .line 349
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    .line 358
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    .line 367
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    .line 376
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    .line 384
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->ampms:[Ljava/lang/String;

    .line 391
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortQuarters:[Ljava/lang/String;

    .line 398
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->quarters:[Ljava/lang/String;

    .line 405
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortQuarters:[Ljava/lang/String;

    .line 412
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneQuarters:[Ljava/lang/String;

    .line 418
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    move-object v0, v1

    .line 429
    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    move-object v0, v1

    .line 464
    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    .line 471
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zsformat:Lcom/ibm/icu/impl/ZoneStringFormat;

    .line 489
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->localPatternChars:Ljava/lang/String;

    .line 167
    invoke-static {p1}, Lcom/ibm/icu/text/DateFormatSymbols;->getCalendarType(Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->initializeData(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)V

    .line 168
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Lcom/ibm/icu/util/ULocale;)V
    .locals 6
    .param p1, "calendarClass"    # Ljava/lang/Class;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v5, 0x0

    .line 1508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eras:[Ljava/lang/String;

    .line 263
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;

    .line 270
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowEras:[Ljava/lang/String;

    .line 278
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    .line 287
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    .line 296
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;

    .line 304
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;

    .line 313
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;

    .line 322
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;

    .line 331
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    .line 340
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    .line 349
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    .line 358
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    .line 367
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    .line 376
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    .line 384
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->ampms:[Ljava/lang/String;

    .line 391
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortQuarters:[Ljava/lang/String;

    .line 398
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->quarters:[Ljava/lang/String;

    .line 405
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortQuarters:[Ljava/lang/String;

    .line 412
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneQuarters:[Ljava/lang/String;

    .line 418
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    move-object v4, v5

    .line 429
    check-cast v4, [[Ljava/lang/String;

    iput-object v4, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    move-object v4, v5

    .line 464
    check-cast v4, [[Ljava/lang/String;

    iput-object v4, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    .line 471
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zsformat:Lcom/ibm/icu/impl/ZoneStringFormat;

    .line 489
    iput-object v5, p0, Lcom/ibm/icu/text/DateFormatSymbols;->localPatternChars:Ljava/lang/String;

    .line 1509
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 1510
    .local v2, "fullName":Ljava/lang/String;
    const/16 v4, 0x2e

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 1511
    .local v3, "lastDot":I
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 1512
    .local v1, "className":Ljava/lang/String;
    const-string/jumbo v4, "Calendar"

    const-string/jumbo v5, ""

    invoke-static {v1, v4, v5}, Lcom/ibm/icu/impl/Utility;->replaceAll(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 1514
    .local v0, "calType":Ljava/lang/String;
    invoke-virtual {p0, p2, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->initializeData(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)V

    .line 1515
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Ljava/util/Locale;)V
    .locals 1
    .param p1, "calendarClass"    # Ljava/lang/Class;
    .param p2, "locale"    # Ljava/util/Locale;

    .prologue
    .line 1499
    invoke-static {p2}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/DateFormatSymbols;-><init>(Ljava/lang/Class;Lcom/ibm/icu/util/ULocale;)V

    .line 1500
    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;)V
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 153
    invoke-static {p1}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateFormatSymbols;-><init>(Lcom/ibm/icu/util/ULocale;)V

    .line 154
    return-void
.end method

.method public constructor <init>(Ljava/util/ResourceBundle;Lcom/ibm/icu/util/ULocale;)V
    .locals 2
    .param p1, "bundle"    # Ljava/util/ResourceBundle;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v1, 0x0

    .line 1535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eras:[Ljava/lang/String;

    .line 263
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;

    .line 270
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowEras:[Ljava/lang/String;

    .line 278
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    .line 287
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    .line 296
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;

    .line 304
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;

    .line 313
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;

    .line 322
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;

    .line 331
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    .line 340
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    .line 349
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    .line 358
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    .line 367
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    .line 376
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    .line 384
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->ampms:[Ljava/lang/String;

    .line 391
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortQuarters:[Ljava/lang/String;

    .line 398
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->quarters:[Ljava/lang/String;

    .line 405
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortQuarters:[Ljava/lang/String;

    .line 412
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneQuarters:[Ljava/lang/String;

    .line 418
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    move-object v0, v1

    .line 429
    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    move-object v0, v1

    .line 464
    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    .line 471
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zsformat:Lcom/ibm/icu/impl/ZoneStringFormat;

    .line 489
    iput-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->localPatternChars:Ljava/lang/String;

    .line 1536
    new-instance v0, Lcom/ibm/icu/impl/CalendarData;

    check-cast p1, Lcom/ibm/icu/impl/ICUResourceBundle;

    .end local p1    # "bundle":Ljava/util/ResourceBundle;
    invoke-static {p2}, Lcom/ibm/icu/text/DateFormatSymbols;->getCalendarType(Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/ibm/icu/impl/CalendarData;-><init>(Lcom/ibm/icu/impl/ICUResourceBundle;Ljava/lang/String;)V

    invoke-virtual {p0, p2, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->initializeData(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/impl/CalendarData;)V

    .line 1538
    return-void
.end method

.method public constructor <init>(Ljava/util/ResourceBundle;Ljava/util/Locale;)V
    .locals 1
    .param p1, "bundle"    # Ljava/util/ResourceBundle;
    .param p2, "locale"    # Ljava/util/Locale;

    .prologue
    .line 1525
    invoke-static {p2}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/DateFormatSymbols;-><init>(Ljava/util/ResourceBundle;Lcom/ibm/icu/util/ULocale;)V

    .line 1526
    return-void
.end method

.method private static final arrayOfArrayEquals([[Ljava/lang/Object;[[Ljava/lang/Object;)Z
    .locals 4
    .param p0, "aa1"    # [[Ljava/lang/Object;
    .param p1, "aa2"    # [[Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 1266
    if-ne p0, p1, :cond_1

    .line 1267
    const/4 v0, 0x1

    .line 1282
    :cond_0
    return v0

    .line 1269
    :cond_1
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 1272
    array-length v2, p0

    array-length v3, p1

    if-ne v2, v3, :cond_0

    .line 1275
    const/4 v0, 0x1

    .line 1276
    .local v0, "equal":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 1277
    aget-object v2, p0, v1

    aget-object v3, p1, v1

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 1278
    if-eqz v0, :cond_0

    .line 1276
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private final duplicate([Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p1, "srcArray"    # [Ljava/lang/String;

    .prologue
    .line 1336
    invoke-virtual {p1}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method private final duplicate([[Ljava/lang/String;)[[Ljava/lang/String;
    .locals 3
    .param p1, "srcArray"    # [[Ljava/lang/String;

    .prologue
    .line 1341
    array-length v2, p1

    new-array v0, v2, [[Ljava/lang/String;

    .line 1342
    .local v0, "aCopy":[[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    .line 1343
    aget-object v2, p1, v1

    invoke-direct {p0, v2}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1342
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1344
    :cond_0
    return-object v0
.end method

.method public static getAvailableLocales()[Ljava/util/Locale;
    .locals 1

    .prologue
    .line 231
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableLocales(Ljava/lang/String;)[Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public static getAvailableULocales()[Lcom/ibm/icu/util/ULocale;
    .locals 1

    .prologue
    .line 248
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableULocales(Ljava/lang/String;)[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    return-object v0
.end method

.method private static getCalendarType(Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;
    .locals 7
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 1612
    const-string/jumbo v0, "calendar"

    invoke-virtual {p0, v0}, Lcom/ibm/icu/util/ULocale;->getKeywordValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1613
    .local v6, "calType":Ljava/lang/String;
    if-nez v6, :cond_0

    .line 1614
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    const-string/jumbo v1, "calendar"

    const-string/jumbo v2, "calendar"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v3, p0

    invoke-static/range {v0 .. v5}, Lcom/ibm/icu/impl/ICUResourceBundle;->getFunctionalEquivalent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/ibm/icu/util/ULocale;[ZZ)Lcom/ibm/icu/util/ULocale;

    move-result-object p0

    .line 1616
    const-string/jumbo v0, "calendar"

    invoke-virtual {p0, v0}, Lcom/ibm/icu/util/ULocale;->getKeywordValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1618
    :cond_0
    return-object v6
.end method

.method public static getDateFormatBundle(Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/ULocale;)Ljava/util/ResourceBundle;
    .locals 1
    .param p0, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/MissingResourceException;
        }
    .end annotation

    .prologue
    .line 1607
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getDateFormatBundle(Lcom/ibm/icu/util/Calendar;Ljava/util/Locale;)Ljava/util/ResourceBundle;
    .locals 1
    .param p0, "cal"    # Lcom/ibm/icu/util/Calendar;
    .param p1, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/MissingResourceException;
        }
    .end annotation

    .prologue
    .line 1592
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getDateFormatBundle(Ljava/lang/Class;Lcom/ibm/icu/util/ULocale;)Ljava/util/ResourceBundle;
    .locals 1
    .param p0, "calendarClass"    # Ljava/lang/Class;
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/MissingResourceException;
        }
    .end annotation

    .prologue
    .line 1577
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getDateFormatBundle(Ljava/lang/Class;Ljava/util/Locale;)Ljava/util/ResourceBundle;
    .locals 1
    .param p0, "calendarClass"    # Ljava/lang/Class;
    .param p1, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/MissingResourceException;
        }
    .end annotation

    .prologue
    .line 1557
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getInstance()Lcom/ibm/icu/text/DateFormatSymbols;
    .locals 1

    .prologue
    .line 182
    new-instance v0, Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-direct {v0}, Lcom/ibm/icu/text/DateFormatSymbols;-><init>()V

    return-object v0
.end method

.method public static getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/DateFormatSymbols;
    .locals 1
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 215
    new-instance v0, Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/DateFormatSymbols;-><init>(Lcom/ibm/icu/util/ULocale;)V

    return-object v0
.end method

.method public static getInstance(Ljava/util/Locale;)Lcom/ibm/icu/text/DateFormatSymbols;
    .locals 1
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    .line 198
    new-instance v0, Lcom/ibm/icu/text/DateFormatSymbols;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    return-object v0
.end method

.method private initializeGMTFormat(Lcom/ibm/icu/util/ULocale;)V
    .locals 9
    .param p1, "desiredLocale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v8, -0x1

    .line 1228
    const-string/jumbo v3, "gmtFormat"

    invoke-static {p1, v3}, Lcom/ibm/icu/impl/ZoneMeta;->getTZLocalizationInfo(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    .line 1229
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 1230
    const-string/jumbo v3, "GMT{0}"

    iput-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    .line 1234
    :cond_0
    :try_start_0
    const-string/jumbo v3, "hourFormat"

    invoke-static {p1, v3}, Lcom/ibm/icu/impl/ZoneMeta;->getTZLocalizationInfo(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1235
    .local v1, "offsetHM":Ljava/lang/String;
    const/4 v3, 0x2

    const/4 v4, 0x2

    filled-new-array {v3, v4}, [I

    move-result-object v3

    const-class v4, Ljava/lang/String;

    invoke-static {v4, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[Ljava/lang/String;

    iput-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    .line 1236
    const/16 v3, 0x3b

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 1237
    .local v2, "sepIdx":I
    if-eq v2, v8, :cond_1

    .line 1238
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1239
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const/4 v4, 0x1

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1246
    :goto_0
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/16 v4, 0x3a

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v8, :cond_2

    .line 1247
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v6, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    const/4 v7, 0x1

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, ":ss"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1253
    :goto_1
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/16 v4, 0x3a

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v8, :cond_4

    .line 1254
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v6, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    const/4 v7, 0x1

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, ":ss"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1263
    .end local v1    # "offsetHM":Ljava/lang/String;
    .end local v2    # "sepIdx":I
    :goto_2
    return-void

    .line 1241
    .restart local v1    # "offsetHM":Ljava/lang/String;
    .restart local v2    # "sepIdx":I
    :cond_1
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "+HH:mm"

    aput-object v5, v3, v4

    .line 1242
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "-HH:mm"

    aput-object v5, v3, v4
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1260
    .end local v1    # "offsetHM":Ljava/lang/String;
    .end local v2    # "sepIdx":I
    :catch_0
    move-exception v0

    .line 1261
    .local v0, "e":Ljava/util/MissingResourceException;
    sget-object v3, Lcom/ibm/icu/text/DateFormatSymbols;->DEFAULT_GMT_HOUR_PATTERNS:[[Ljava/lang/String;

    iput-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    goto :goto_2

    .line 1248
    .end local v0    # "e":Ljava/util/MissingResourceException;
    .restart local v1    # "offsetHM":Ljava/lang/String;
    .restart local v2    # "sepIdx":I
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/16 v4, 0x2e

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v8, :cond_3

    .line 1249
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v6, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    const/4 v7, 0x1

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, ".ss"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    goto/16 :goto_1

    .line 1251
    :cond_3
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v6, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    const/4 v7, 0x1

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, "ss"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    goto/16 :goto_1

    .line 1255
    :cond_4
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const/4 v4, 0x1

    aget-object v3, v3, v4

    const/16 v4, 0x2e

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v8, :cond_5

    .line 1256
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v6, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    const/4 v7, 0x1

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, ".ss"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    goto/16 :goto_2

    .line 1258
    :cond_5
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v6, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    const/4 v7, 0x1

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string/jumbo v6, "ss"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1
    .param p1, "stream"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 1701
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 1702
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1703
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->requestedLocale:Lcom/ibm/icu/util/ULocale;

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->initializeGMTFormat(Lcom/ibm/icu/util/ULocale;)V

    .line 1705
    :cond_0
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 919
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/DateFormatSymbols;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 920
    .local v1, "other":Lcom/ibm/icu/text/DateFormatSymbols;
    return-object v1

    .line 921
    .end local v1    # "other":Lcom/ibm/icu/text/DateFormatSymbols;
    :catch_0
    move-exception v0

    .line 923
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 944
    if-ne p0, p1, :cond_1

    .line 947
    :cond_0
    :goto_0
    return v1

    .line 945
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 946
    check-cast v0, Lcom/ibm/icu/text/DateFormatSymbols;

    .line 947
    .local v0, "that":Lcom/ibm/icu/text/DateFormatSymbols;
    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eras:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->eras:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->ampms:[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->ampms:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/text/DateFormatSymbols;->arrayOfArrayEquals([[Ljava/lang/Object;[[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/text/DateFormatSymbols;->arrayOfArrayEquals([[Ljava/lang/Object;[[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->requestedLocale:Lcom/ibm/icu/util/ULocale;

    invoke-virtual {v3}, Lcom/ibm/icu/util/ULocale;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->requestedLocale:Lcom/ibm/icu/util/ULocale;

    invoke-virtual {v4}, Lcom/ibm/icu/util/ULocale;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/ibm/icu/text/DateFormatSymbols;->localPatternChars:Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/DateFormatSymbols;->localPatternChars:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/ibm/icu/impl/Utility;->arrayEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto/16 :goto_0
.end method

.method public getAmPmStrings()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 855
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->ampms:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEraNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEras()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eras:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getGmtFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1289
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    return-object v0
.end method

.method getGmtHourFormat(II)Ljava/lang/String;
    .locals 1
    .param p1, "sign"    # I
    .param p2, "width"    # I

    .prologue
    .line 1302
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    aget-object v0, v0, p1

    aget-object v0, v0, p2

    return-object v0
.end method

.method public getLocalPatternChars()Ljava/lang/String;
    .locals 2

    .prologue
    .line 899
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->localPatternChars:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final getLocale(Lcom/ibm/icu/util/ULocale$Type;)Lcom/ibm/icu/util/ULocale;
    .locals 1
    .param p1, "type"    # Lcom/ibm/icu/util/ULocale$Type;

    .prologue
    .line 1648
    sget-object v0, Lcom/ibm/icu/util/ULocale;->ACTUAL_LOCALE:Lcom/ibm/icu/util/ULocale$Type;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->actualLocale:Lcom/ibm/icu/util/ULocale;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->validLocale:Lcom/ibm/icu/util/ULocale;

    goto :goto_0
.end method

.method public getMonths()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 536
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMonths(II)[Ljava/lang/String;
    .locals 2
    .param p1, "context"    # I
    .param p2, "width"    # I

    .prologue
    .line 548
    const/4 v0, 0x0

    .line 549
    .local v0, "returnValue":[Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 577
    :goto_0
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 551
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 556
    :pswitch_1
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    .line 557
    goto :goto_0

    .line 553
    :pswitch_2
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    .line 554
    goto :goto_0

    .line 559
    :pswitch_3
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;

    goto :goto_0

    .line 564
    :pswitch_4
    packed-switch p2, :pswitch_data_2

    goto :goto_0

    .line 569
    :pswitch_5
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;

    .line 570
    goto :goto_0

    .line 566
    :pswitch_6
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;

    .line 567
    goto :goto_0

    .line 572
    :pswitch_7
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;

    goto :goto_0

    .line 549
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 551
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 564
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public getQuarters(II)[Ljava/lang/String;
    .locals 2
    .param p1, "context"    # I
    .param p2, "width"    # I

    .prologue
    .line 777
    const/4 v0, 0x0

    .line 778
    .local v0, "returnValue":[Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 807
    :goto_0
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 780
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 785
    :pswitch_1
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortQuarters:[Ljava/lang/String;

    .line 786
    goto :goto_0

    .line 782
    :pswitch_2
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->quarters:[Ljava/lang/String;

    .line 783
    goto :goto_0

    .line 788
    :pswitch_3
    const/4 v0, 0x0

    goto :goto_0

    .line 794
    :pswitch_4
    packed-switch p2, :pswitch_data_2

    goto :goto_0

    .line 799
    :pswitch_5
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortQuarters:[Ljava/lang/String;

    .line 800
    goto :goto_0

    .line 796
    :pswitch_6
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneQuarters:[Ljava/lang/String;

    .line 797
    goto :goto_0

    .line 802
    :pswitch_7
    const/4 v0, 0x0

    goto :goto_0

    .line 778
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 780
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 794
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public getShortMonths()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 634
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShortWeekdays()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 755
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWeekdays()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 653
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWeekdays(II)[Ljava/lang/String;
    .locals 2
    .param p1, "context"    # I
    .param p2, "width"    # I

    .prologue
    .line 666
    const/4 v0, 0x0

    .line 667
    .local v0, "returnValue":[Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 695
    :goto_0
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 669
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 674
    :pswitch_1
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    .line 675
    goto :goto_0

    .line 671
    :pswitch_2
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    .line 672
    goto :goto_0

    .line 677
    :pswitch_3
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    goto :goto_0

    .line 682
    :pswitch_4
    packed-switch p2, :pswitch_data_2

    goto :goto_0

    .line 687
    :pswitch_5
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    .line 688
    goto :goto_0

    .line 684
    :pswitch_6
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    .line 685
    goto :goto_0

    .line 690
    :pswitch_7
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    goto :goto_0

    .line 667
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 669
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 682
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method getZoneStringFormat()Lcom/ibm/icu/impl/ZoneStringFormat;
    .locals 2

    .prologue
    .line 1310
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zsformat:Lcom/ibm/icu/impl/ZoneStringFormat;

    if-eqz v0, :cond_0

    .line 1311
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zsformat:Lcom/ibm/icu/impl/ZoneStringFormat;

    .line 1321
    :goto_0
    return-object v0

    .line 1313
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1314
    new-instance v0, Lcom/ibm/icu/impl/ZoneStringFormat;

    iget-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat;-><init>([[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zsformat:Lcom/ibm/icu/impl/ZoneStringFormat;

    .line 1315
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zsformat:Lcom/ibm/icu/impl/ZoneStringFormat;

    goto :goto_0

    .line 1321
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->requestedLocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v0}, Lcom/ibm/icu/impl/ZoneStringFormat;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/impl/ZoneStringFormat;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoneStrings()[[Ljava/lang/String;
    .locals 1

    .prologue
    .line 873
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 874
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([[Ljava/lang/String;)[[Ljava/lang/String;

    move-result-object v0

    .line 876
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->requestedLocale:Lcom/ibm/icu/util/ULocale;

    invoke-static {v0}, Lcom/ibm/icu/impl/ZoneStringFormat;->getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/impl/ZoneStringFormat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ZoneStringFormat;->getZoneStrings()[[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 935
    iget-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->requestedLocale:Lcom/ibm/icu/util/ULocale;

    invoke-virtual {v0}, Lcom/ibm/icu/util/ULocale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method initializeData(Lcom/ibm/icu/text/DateFormatSymbols;)V
    .locals 1
    .param p1, "dfs"    # Lcom/ibm/icu/text/DateFormatSymbols;

    .prologue
    .line 1012
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->eras:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eras:[Ljava/lang/String;

    .line 1013
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;

    .line 1014
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->narrowEras:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowEras:[Ljava/lang/String;

    .line 1015
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    .line 1016
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    .line 1017
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;

    .line 1018
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;

    .line 1019
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;

    .line 1020
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;

    .line 1021
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    .line 1022
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    .line 1023
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    .line 1024
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    .line 1025
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    .line 1026
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    .line 1027
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->ampms:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->ampms:[Ljava/lang/String;

    .line 1028
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->shortQuarters:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortQuarters:[Ljava/lang/String;

    .line 1029
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->quarters:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->quarters:[Ljava/lang/String;

    .line 1030
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortQuarters:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortQuarters:[Ljava/lang/String;

    .line 1031
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneQuarters:[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneQuarters:[Ljava/lang/String;

    .line 1033
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtFormat:Ljava/lang/String;

    .line 1034
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->gmtHourFormats:[[Ljava/lang/String;

    .line 1036
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    .line 1037
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->localPatternChars:Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->localPatternChars:Ljava/lang/String;

    .line 1039
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->actualLocale:Lcom/ibm/icu/util/ULocale;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->actualLocale:Lcom/ibm/icu/util/ULocale;

    .line 1040
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->validLocale:Lcom/ibm/icu/util/ULocale;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->validLocale:Lcom/ibm/icu/util/ULocale;

    .line 1041
    iget-object v0, p1, Lcom/ibm/icu/text/DateFormatSymbols;->requestedLocale:Lcom/ibm/icu/util/ULocale;

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->requestedLocale:Lcom/ibm/icu/util/ULocale;

    .line 1042
    return-void
.end method

.method protected initializeData(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/impl/CalendarData;)V
    .locals 15
    .param p1, "desiredLocale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "calData"    # Lcom/ibm/icu/impl/CalendarData;

    .prologue
    .line 1057
    const-string/jumbo v11, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/ibm/icu/impl/CalendarData;->getEras(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eras:[Ljava/lang/String;

    .line 1060
    :try_start_0
    const-string/jumbo v11, "wide"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/ibm/icu/impl/CalendarData;->getEras(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1069
    :goto_0
    :try_start_1
    const-string/jumbo v11, "narrow"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/ibm/icu/impl/CalendarData;->getEras(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowEras:[Ljava/lang/String;
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1074
    :goto_1
    const-string/jumbo v11, "monthNames"

    const-string/jumbo v12, "wide"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    .line 1075
    const-string/jumbo v11, "monthNames"

    const-string/jumbo v12, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    .line 1078
    :try_start_2
    const-string/jumbo v11, "monthNames"

    const-string/jumbo v12, "narrow"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;
    :try_end_2
    .catch Ljava/util/MissingResourceException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1090
    :goto_2
    :try_start_3
    const-string/jumbo v11, "monthNames"

    const-string/jumbo v12, "stand-alone"

    const-string/jumbo v13, "wide"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;
    :try_end_3
    .catch Ljava/util/MissingResourceException; {:try_start_3 .. :try_end_3} :catch_4

    .line 1097
    :goto_3
    :try_start_4
    const-string/jumbo v11, "monthNames"

    const-string/jumbo v12, "stand-alone"

    const-string/jumbo v13, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;
    :try_end_4
    .catch Ljava/util/MissingResourceException; {:try_start_4 .. :try_end_4} :catch_5

    .line 1104
    :goto_4
    :try_start_5
    const-string/jumbo v11, "monthNames"

    const-string/jumbo v12, "stand-alone"

    const-string/jumbo v13, "narrow"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;
    :try_end_5
    .catch Ljava/util/MissingResourceException; {:try_start_5 .. :try_end_5} :catch_6

    .line 1115
    :goto_5
    const-string/jumbo v11, "dayNames"

    const-string/jumbo v12, "wide"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1116
    .local v3, "lWeekdays":[Ljava/lang/String;
    const/16 v11, 0x8

    new-array v11, v11, [Ljava/lang/String;

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    .line 1117
    iget-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    const/4 v12, 0x0

    const-string/jumbo v13, ""

    aput-object v13, v11, v12

    .line 1118
    const/4 v11, 0x0

    iget-object v12, p0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    const/4 v13, 0x1

    array-length v14, v3

    invoke-static {v3, v11, v12, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1120
    const-string/jumbo v11, "dayNames"

    const-string/jumbo v12, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1121
    .local v6, "sWeekdays":[Ljava/lang/String;
    const/16 v11, 0x8

    new-array v11, v11, [Ljava/lang/String;

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    .line 1122
    iget-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    const/4 v12, 0x0

    const-string/jumbo v13, ""

    aput-object v13, v11, v12

    .line 1123
    const/4 v11, 0x0

    iget-object v12, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    const/4 v13, 0x1

    array-length v14, v6

    invoke-static {v6, v11, v12, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1125
    const/4 v4, 0x0

    .line 1127
    .local v4, "nWeekdays":[Ljava/lang/String;
    :try_start_6
    const-string/jumbo v11, "dayNames"

    const-string/jumbo v12, "narrow"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    :try_end_6
    .catch Ljava/util/MissingResourceException; {:try_start_6 .. :try_end_6} :catch_8

    move-result-object v4

    .line 1137
    :goto_6
    const/16 v11, 0x8

    new-array v11, v11, [Ljava/lang/String;

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    .line 1138
    iget-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    const/4 v12, 0x0

    const-string/jumbo v13, ""

    aput-object v13, v11, v12

    .line 1139
    const/4 v11, 0x0

    iget-object v12, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    const/4 v13, 0x1

    array-length v14, v4

    invoke-static {v4, v11, v12, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1141
    const/4 v7, 0x0

    .line 1143
    .local v7, "saWeekdays":[Ljava/lang/String;
    :try_start_7
    const-string/jumbo v11, "dayNames"

    const-string/jumbo v12, "stand-alone"

    const-string/jumbo v13, "wide"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    :try_end_7
    .catch Ljava/util/MissingResourceException; {:try_start_7 .. :try_end_7} :catch_a

    move-result-object v7

    .line 1148
    :goto_7
    const/16 v11, 0x8

    new-array v11, v11, [Ljava/lang/String;

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    .line 1149
    iget-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    const/4 v12, 0x0

    const-string/jumbo v13, ""

    aput-object v13, v11, v12

    .line 1150
    const/4 v11, 0x0

    iget-object v12, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    const/4 v13, 0x1

    array-length v14, v7

    invoke-static {v7, v11, v12, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1152
    const/4 v9, 0x0

    .line 1154
    .local v9, "ssWeekdays":[Ljava/lang/String;
    :try_start_8
    const-string/jumbo v11, "dayNames"

    const-string/jumbo v12, "stand-alone"

    const-string/jumbo v13, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    :try_end_8
    .catch Ljava/util/MissingResourceException; {:try_start_8 .. :try_end_8} :catch_b

    move-result-object v9

    .line 1159
    :goto_8
    const/16 v11, 0x8

    new-array v11, v11, [Ljava/lang/String;

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    .line 1160
    iget-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    const/4 v12, 0x0

    const-string/jumbo v13, ""

    aput-object v13, v11, v12

    .line 1161
    const/4 v11, 0x0

    iget-object v12, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    const/4 v13, 0x1

    array-length v14, v9

    invoke-static {v9, v11, v12, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1163
    const/4 v8, 0x0

    .line 1165
    .local v8, "snWeekdays":[Ljava/lang/String;
    :try_start_9
    const-string/jumbo v11, "dayNames"

    const-string/jumbo v12, "stand-alone"

    const-string/jumbo v13, "narrow"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    :try_end_9
    .catch Ljava/util/MissingResourceException; {:try_start_9 .. :try_end_9} :catch_c

    move-result-object v8

    .line 1175
    :goto_9
    const/16 v11, 0x8

    new-array v11, v11, [Ljava/lang/String;

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    .line 1176
    iget-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    const/4 v12, 0x0

    const-string/jumbo v13, ""

    aput-object v13, v11, v12

    .line 1177
    const/4 v11, 0x0

    iget-object v12, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    const/4 v13, 0x1

    array-length v14, v8

    invoke-static {v8, v11, v12, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1179
    const-string/jumbo v11, "AmPmMarkers"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->ampms:[Ljava/lang/String;

    .line 1181
    const-string/jumbo v11, "quarters"

    const-string/jumbo v12, "wide"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->quarters:[Ljava/lang/String;

    .line 1182
    const-string/jumbo v11, "quarters"

    const-string/jumbo v12, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortQuarters:[Ljava/lang/String;

    .line 1185
    :try_start_a
    const-string/jumbo v11, "quarters"

    const-string/jumbo v12, "stand-alone"

    const-string/jumbo v13, "wide"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneQuarters:[Ljava/lang/String;
    :try_end_a
    .catch Ljava/util/MissingResourceException; {:try_start_a .. :try_end_a} :catch_e

    .line 1192
    :goto_a
    :try_start_b
    const-string/jumbo v11, "quarters"

    const-string/jumbo v12, "stand-alone"

    const-string/jumbo v13, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortQuarters:[Ljava/lang/String;
    :try_end_b
    .catch Ljava/util/MissingResourceException; {:try_start_b .. :try_end_b} :catch_f

    .line 1199
    :goto_b
    invoke-direct/range {p0 .. p1}, Lcom/ibm/icu/text/DateFormatSymbols;->initializeGMTFormat(Lcom/ibm/icu/util/ULocale;)V

    .line 1201
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->requestedLocale:Lcom/ibm/icu/util/ULocale;

    .line 1203
    const-string/jumbo v11, "com/ibm/icu/impl/data/icudt40b"

    move-object/from16 v0, p1

    invoke-static {v11, v0}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v5

    check-cast v5, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 1210
    .local v5, "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    const-string/jumbo v11, "GyMdkHmsSEDFwWahKzYeugAZvcLQqV"

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->localPatternChars:Ljava/lang/String;

    .line 1213
    invoke-virtual {v5}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v10

    .line 1214
    .local v10, "uloc":Lcom/ibm/icu/util/ULocale;
    invoke-virtual {p0, v10, v10}, Lcom/ibm/icu/text/DateFormatSymbols;->setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V

    .line 1215
    return-void

    .line 1062
    .end local v3    # "lWeekdays":[Ljava/lang/String;
    .end local v4    # "nWeekdays":[Ljava/lang/String;
    .end local v5    # "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    .end local v6    # "sWeekdays":[Ljava/lang/String;
    .end local v7    # "saWeekdays":[Ljava/lang/String;
    .end local v8    # "snWeekdays":[Ljava/lang/String;
    .end local v9    # "ssWeekdays":[Ljava/lang/String;
    .end local v10    # "uloc":Lcom/ibm/icu/util/ULocale;
    :catch_0
    move-exception v1

    .line 1063
    .local v1, "e":Ljava/util/MissingResourceException;
    const-string/jumbo v11, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/ibm/icu/impl/CalendarData;->getEras(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;

    goto/16 :goto_0

    .line 1070
    .end local v1    # "e":Ljava/util/MissingResourceException;
    :catch_1
    move-exception v1

    .line 1071
    .restart local v1    # "e":Ljava/util/MissingResourceException;
    const-string/jumbo v11, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/ibm/icu/impl/CalendarData;->getEras(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowEras:[Ljava/lang/String;

    goto/16 :goto_1

    .line 1080
    .end local v1    # "e":Ljava/util/MissingResourceException;
    :catch_2
    move-exception v1

    .line 1082
    .restart local v1    # "e":Ljava/util/MissingResourceException;
    :try_start_c
    const-string/jumbo v11, "monthNames"

    const-string/jumbo v12, "stand-alone"

    const-string/jumbo v13, "narrow"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;
    :try_end_c
    .catch Ljava/util/MissingResourceException; {:try_start_c .. :try_end_c} :catch_3

    goto/16 :goto_2

    .line 1084
    :catch_3
    move-exception v2

    .line 1085
    .local v2, "e1":Ljava/util/MissingResourceException;
    const-string/jumbo v11, "monthNames"

    const-string/jumbo v12, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;

    goto/16 :goto_2

    .line 1092
    .end local v1    # "e":Ljava/util/MissingResourceException;
    .end local v2    # "e1":Ljava/util/MissingResourceException;
    :catch_4
    move-exception v1

    .line 1093
    .restart local v1    # "e":Ljava/util/MissingResourceException;
    const-string/jumbo v11, "monthNames"

    const-string/jumbo v12, "format"

    const-string/jumbo v13, "wide"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;

    goto/16 :goto_3

    .line 1099
    .end local v1    # "e":Ljava/util/MissingResourceException;
    :catch_5
    move-exception v1

    .line 1100
    .restart local v1    # "e":Ljava/util/MissingResourceException;
    const-string/jumbo v11, "monthNames"

    const-string/jumbo v12, "format"

    const-string/jumbo v13, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;

    goto/16 :goto_4

    .line 1106
    .end local v1    # "e":Ljava/util/MissingResourceException;
    :catch_6
    move-exception v1

    .line 1108
    .restart local v1    # "e":Ljava/util/MissingResourceException;
    :try_start_d
    const-string/jumbo v11, "monthNames"

    const-string/jumbo v12, "format"

    const-string/jumbo v13, "narrow"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;
    :try_end_d
    .catch Ljava/util/MissingResourceException; {:try_start_d .. :try_end_d} :catch_7

    goto/16 :goto_5

    .line 1110
    :catch_7
    move-exception v2

    .line 1111
    .restart local v2    # "e1":Ljava/util/MissingResourceException;
    const-string/jumbo v11, "monthNames"

    const-string/jumbo v12, "format"

    const-string/jumbo v13, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;

    goto/16 :goto_5

    .line 1129
    .end local v1    # "e":Ljava/util/MissingResourceException;
    .end local v2    # "e1":Ljava/util/MissingResourceException;
    .restart local v3    # "lWeekdays":[Ljava/lang/String;
    .restart local v4    # "nWeekdays":[Ljava/lang/String;
    .restart local v6    # "sWeekdays":[Ljava/lang/String;
    :catch_8
    move-exception v1

    .line 1131
    .restart local v1    # "e":Ljava/util/MissingResourceException;
    :try_start_e
    const-string/jumbo v11, "dayNames"

    const-string/jumbo v12, "stand-alone"

    const-string/jumbo v13, "narrow"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    :try_end_e
    .catch Ljava/util/MissingResourceException; {:try_start_e .. :try_end_e} :catch_9

    move-result-object v4

    goto/16 :goto_6

    .line 1133
    :catch_9
    move-exception v2

    .line 1134
    .restart local v2    # "e1":Ljava/util/MissingResourceException;
    const-string/jumbo v11, "dayNames"

    const-string/jumbo v12, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    .line 1145
    .end local v1    # "e":Ljava/util/MissingResourceException;
    .end local v2    # "e1":Ljava/util/MissingResourceException;
    .restart local v7    # "saWeekdays":[Ljava/lang/String;
    :catch_a
    move-exception v1

    .line 1146
    .restart local v1    # "e":Ljava/util/MissingResourceException;
    const-string/jumbo v11, "dayNames"

    const-string/jumbo v12, "format"

    const-string/jumbo v13, "wide"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_7

    .line 1156
    .end local v1    # "e":Ljava/util/MissingResourceException;
    .restart local v9    # "ssWeekdays":[Ljava/lang/String;
    :catch_b
    move-exception v1

    .line 1157
    .restart local v1    # "e":Ljava/util/MissingResourceException;
    const-string/jumbo v11, "dayNames"

    const-string/jumbo v12, "format"

    const-string/jumbo v13, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_8

    .line 1167
    .end local v1    # "e":Ljava/util/MissingResourceException;
    .restart local v8    # "snWeekdays":[Ljava/lang/String;
    :catch_c
    move-exception v1

    .line 1169
    .restart local v1    # "e":Ljava/util/MissingResourceException;
    :try_start_f
    const-string/jumbo v11, "dayNames"

    const-string/jumbo v12, "format"

    const-string/jumbo v13, "narrow"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    :try_end_f
    .catch Ljava/util/MissingResourceException; {:try_start_f .. :try_end_f} :catch_d

    move-result-object v8

    goto/16 :goto_9

    .line 1171
    :catch_d
    move-exception v2

    .line 1172
    .restart local v2    # "e1":Ljava/util/MissingResourceException;
    const-string/jumbo v11, "dayNames"

    const-string/jumbo v12, "format"

    const-string/jumbo v13, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_9

    .line 1187
    .end local v1    # "e":Ljava/util/MissingResourceException;
    .end local v2    # "e1":Ljava/util/MissingResourceException;
    :catch_e
    move-exception v1

    .line 1188
    .restart local v1    # "e":Ljava/util/MissingResourceException;
    const-string/jumbo v11, "quarters"

    const-string/jumbo v12, "format"

    const-string/jumbo v13, "wide"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneQuarters:[Ljava/lang/String;

    goto/16 :goto_a

    .line 1194
    .end local v1    # "e":Ljava/util/MissingResourceException;
    :catch_f
    move-exception v1

    .line 1195
    .restart local v1    # "e":Ljava/util/MissingResourceException;
    const-string/jumbo v11, "quarters"

    const-string/jumbo v12, "format"

    const-string/jumbo v13, "abbreviated"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13}, Lcom/ibm/icu/impl/CalendarData;->getStringArray(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortQuarters:[Ljava/lang/String;

    goto/16 :goto_b
.end method

.method protected initializeData(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)V
    .locals 5
    .param p1, "desiredLocale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 993
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1}, Lcom/ibm/icu/util/ULocale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 994
    .local v2, "key":Ljava/lang/String;
    sget-object v3, Lcom/ibm/icu/text/DateFormatSymbols;->DFSCACHE:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v3, v2}, Lcom/ibm/icu/impl/ICUCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/DateFormatSymbols;

    .line 995
    .local v1, "dfs":Lcom/ibm/icu/text/DateFormatSymbols;
    if-nez v1, :cond_0

    .line 997
    new-instance v0, Lcom/ibm/icu/impl/CalendarData;

    invoke-direct {v0, p1, p2}, Lcom/ibm/icu/impl/CalendarData;-><init>(Lcom/ibm/icu/util/ULocale;Ljava/lang/String;)V

    .line 998
    .local v0, "calData":Lcom/ibm/icu/impl/CalendarData;
    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/text/DateFormatSymbols;->initializeData(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/impl/CalendarData;)V

    .line 999
    invoke-virtual {p0}, Lcom/ibm/icu/text/DateFormatSymbols;->clone()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "dfs":Lcom/ibm/icu/text/DateFormatSymbols;
    check-cast v1, Lcom/ibm/icu/text/DateFormatSymbols;

    .line 1000
    .restart local v1    # "dfs":Lcom/ibm/icu/text/DateFormatSymbols;
    sget-object v3, Lcom/ibm/icu/text/DateFormatSymbols;->DFSCACHE:Lcom/ibm/icu/impl/ICUCache;

    invoke-interface {v3, v2, v1}, Lcom/ibm/icu/impl/ICUCache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1004
    .end local v0    # "calData":Lcom/ibm/icu/impl/CalendarData;
    :goto_0
    return-void

    .line 1002
    :cond_0
    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/DateFormatSymbols;->initializeData(Lcom/ibm/icu/text/DateFormatSymbols;)V

    goto :goto_0
.end method

.method public setAmPmStrings([Ljava/lang/String;)V
    .locals 1
    .param p1, "newAmpms"    # [Ljava/lang/String;

    .prologue
    .line 864
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->ampms:[Ljava/lang/String;

    .line 865
    return-void
.end method

.method public setEraNames([Ljava/lang/String;)V
    .locals 1
    .param p1, "newEraNames"    # [Ljava/lang/String;

    .prologue
    .line 527
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eraNames:[Ljava/lang/String;

    .line 528
    return-void
.end method

.method public setEras([Ljava/lang/String;)V
    .locals 1
    .param p1, "newEras"    # [Ljava/lang/String;

    .prologue
    .line 509
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->eras:[Ljava/lang/String;

    .line 510
    return-void
.end method

.method public setLocalPatternChars(Ljava/lang/String;)V
    .locals 0
    .param p1, "newLocalPatternChars"    # Ljava/lang/String;

    .prologue
    .line 909
    iput-object p1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->localPatternChars:Ljava/lang/String;

    .line 910
    return-void
.end method

.method final setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V
    .locals 3
    .param p1, "valid"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "actual"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1670
    if-nez p1, :cond_0

    move v2, v0

    :goto_0
    if-nez p2, :cond_1

    :goto_1
    if-eq v2, v0, :cond_2

    .line 1672
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v2, v1

    .line 1670
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 1677
    :cond_2
    iput-object p1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->validLocale:Lcom/ibm/icu/util/ULocale;

    .line 1678
    iput-object p2, p0, Lcom/ibm/icu/text/DateFormatSymbols;->actualLocale:Lcom/ibm/icu/util/ULocale;

    .line 1679
    return-void
.end method

.method public setMonths([Ljava/lang/String;)V
    .locals 1
    .param p1, "newMonths"    # [Ljava/lang/String;

    .prologue
    .line 586
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    .line 587
    return-void
.end method

.method public setMonths([Ljava/lang/String;II)V
    .locals 1
    .param p1, "newMonths"    # [Ljava/lang/String;
    .param p2, "context"    # I
    .param p3, "width"    # I

    .prologue
    .line 598
    packed-switch p2, :pswitch_data_0

    .line 626
    :goto_0
    return-void

    .line 600
    :pswitch_0
    packed-switch p3, :pswitch_data_1

    goto :goto_0

    .line 605
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    goto :goto_0

    .line 602
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->months:[Ljava/lang/String;

    goto :goto_0

    .line 608
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowMonths:[Ljava/lang/String;

    goto :goto_0

    .line 613
    :pswitch_4
    packed-switch p3, :pswitch_data_2

    goto :goto_0

    .line 618
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortMonths:[Ljava/lang/String;

    goto :goto_0

    .line 615
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneMonths:[Ljava/lang/String;

    goto :goto_0

    .line 621
    :pswitch_7
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowMonths:[Ljava/lang/String;

    goto :goto_0

    .line 598
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 600
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 613
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public setQuarters([Ljava/lang/String;II)V
    .locals 1
    .param p1, "newQuarters"    # [Ljava/lang/String;
    .param p2, "context"    # I
    .param p3, "width"    # I

    .prologue
    .line 819
    packed-switch p2, :pswitch_data_0

    .line 847
    :goto_0
    return-void

    .line 821
    :pswitch_0
    packed-switch p3, :pswitch_data_1

    goto :goto_0

    .line 826
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortQuarters:[Ljava/lang/String;

    goto :goto_0

    .line 823
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->quarters:[Ljava/lang/String;

    goto :goto_0

    .line 834
    :pswitch_3
    packed-switch p3, :pswitch_data_2

    goto :goto_0

    .line 839
    :pswitch_4
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortQuarters:[Ljava/lang/String;

    goto :goto_0

    .line 836
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneQuarters:[Ljava/lang/String;

    goto :goto_0

    .line 819
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 821
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 834
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public setShortMonths([Ljava/lang/String;)V
    .locals 1
    .param p1, "newShortMonths"    # [Ljava/lang/String;

    .prologue
    .line 643
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortMonths:[Ljava/lang/String;

    .line 644
    return-void
.end method

.method public setShortWeekdays([Ljava/lang/String;)V
    .locals 1
    .param p1, "newShortWeekdays"    # [Ljava/lang/String;

    .prologue
    .line 766
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    .line 767
    return-void
.end method

.method public setWeekdays([Ljava/lang/String;)V
    .locals 1
    .param p1, "newWeekdays"    # [Ljava/lang/String;

    .prologue
    .line 745
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    .line 746
    return-void
.end method

.method public setWeekdays([Ljava/lang/String;II)V
    .locals 1
    .param p1, "newWeekdays"    # [Ljava/lang/String;
    .param p2, "context"    # I
    .param p3, "width"    # I

    .prologue
    .line 707
    packed-switch p2, :pswitch_data_0

    .line 735
    :goto_0
    return-void

    .line 709
    :pswitch_0
    packed-switch p3, :pswitch_data_1

    goto :goto_0

    .line 714
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->shortWeekdays:[Ljava/lang/String;

    goto :goto_0

    .line 711
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->weekdays:[Ljava/lang/String;

    goto :goto_0

    .line 717
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->narrowWeekdays:[Ljava/lang/String;

    goto :goto_0

    .line 722
    :pswitch_4
    packed-switch p3, :pswitch_data_2

    goto :goto_0

    .line 727
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneShortWeekdays:[Ljava/lang/String;

    goto :goto_0

    .line 724
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneWeekdays:[Ljava/lang/String;

    goto :goto_0

    .line 730
    :pswitch_7
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->standaloneNarrowWeekdays:[Ljava/lang/String;

    goto :goto_0

    .line 707
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 709
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 722
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public setZoneStrings([[Ljava/lang/String;)V
    .locals 2
    .param p1, "newZoneStrings"    # [[Ljava/lang/String;

    .prologue
    .line 885
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/DateFormatSymbols;->duplicate([[Ljava/lang/String;)[[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    .line 886
    new-instance v0, Lcom/ibm/icu/impl/ZoneStringFormat;

    iget-object v1, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zoneStrings:[[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/ibm/icu/impl/ZoneStringFormat;-><init>([[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/ibm/icu/text/DateFormatSymbols;->zsformat:Lcom/ibm/icu/impl/ZoneStringFormat;

    .line 887
    return-void
.end method
