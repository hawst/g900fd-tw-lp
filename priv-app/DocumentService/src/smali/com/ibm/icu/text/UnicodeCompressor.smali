.class public final Lcom/ibm/icu/text/UnicodeCompressor;
.super Ljava/lang/Object;
.source "UnicodeCompressor.java"

# interfaces
.implements Lcom/ibm/icu/text/SCSU;


# static fields
.field private static sSingleTagTable:[Z

.field private static sUnicodeTagTable:[Z


# instance fields
.field private fCurrentWindow:I

.field private fIndexCount:[I

.field private fMode:I

.field private fOffsets:[I

.field private fTimeStamp:I

.field private fTimeStamps:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x100

    .line 193
    new-array v0, v1, [Z

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/text/UnicodeCompressor;->sSingleTagTable:[Z

    .line 227
    new-array v0, v1, [Z

    fill-array-data v0, :array_1

    sput-object v0, Lcom/ibm/icu/text/UnicodeCompressor;->sUnicodeTagTable:[Z

    return-void

    .line 193
    nop

    :array_0
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 227
    :array_1
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 265
    iput v1, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fCurrentWindow:I

    .line 268
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    .line 271
    iput v1, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fMode:I

    .line 274
    const/16 v0, 0x100

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fIndexCount:[I

    .line 277
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamps:[I

    .line 280
    iput v1, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamp:I

    .line 291
    invoke-virtual {p0}, Lcom/ibm/icu/text/UnicodeCompressor;->reset()V

    .line 292
    return-void
.end method

.method public static compress(Ljava/lang/String;)[B
    .locals 3
    .param p0, "buffer"    # Ljava/lang/String;

    .prologue
    .line 303
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/UnicodeCompressor;->compress([CII)[B

    move-result-object v0

    return-object v0
.end method

.method public static compress([CII)[B
    .locals 10
    .param p0, "buffer"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    const/4 v6, 0x0

    .line 319
    new-instance v0, Lcom/ibm/icu/text/UnicodeCompressor;

    invoke-direct {v0}, Lcom/ibm/icu/text/UnicodeCompressor;-><init>()V

    .line 326
    .local v0, "comp":Lcom/ibm/icu/text/UnicodeCompressor;
    const/4 v1, 0x4

    sub-int v2, p2, p1

    mul-int/lit8 v2, v2, 0x3

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 327
    .local v7, "len":I
    new-array v5, v7, [B

    .line 329
    .local v5, "temp":[B
    const/4 v4, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    invoke-virtual/range {v0 .. v7}, Lcom/ibm/icu/text/UnicodeCompressor;->compress([CII[I[BII)I

    move-result v8

    .line 332
    .local v8, "byteCount":I
    new-array v9, v8, [B

    .line 333
    .local v9, "result":[B
    invoke-static {v5, v6, v9, v6, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 334
    return-object v9
.end method

.method private findDynamicWindow(I)I
    .locals 3
    .param p1, "c"    # I

    .prologue
    .line 954
    const/4 v0, 0x7

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 955
    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/UnicodeCompressor;->inDynamicWindow(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 956
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamps:[I

    aget v2, v1, v0

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v0

    .line 961
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 954
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 961
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static findStaticWindow(I)I
    .locals 2
    .param p0, "c"    # I

    .prologue
    .line 974
    const/4 v0, 0x7

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 975
    invoke-static {p0, v0}, Lcom/ibm/icu/text/UnicodeCompressor;->inStaticWindow(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 980
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 974
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 980
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private getLRDefinedWindow()I
    .locals 4

    .prologue
    .line 990
    const v1, 0x7fffffff

    .line 991
    .local v1, "leastRU":I
    const/4 v2, -0x1

    .line 996
    .local v2, "whichWindow":I
    const/4 v0, 0x7

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 997
    iget-object v3, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamps:[I

    aget v3, v3, v0

    if-ge v3, v1, :cond_0

    .line 998
    iget-object v3, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamps:[I

    aget v1, v3, v0

    .line 999
    move v2, v0

    .line 996
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1003
    :cond_1
    return v2
.end method

.method private inDynamicWindow(II)Z
    .locals 1
    .param p1, "c"    # I
    .param p2, "whichWindow"    # I

    .prologue
    .line 908
    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    aget v0, v0, p2

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    aget v0, v0, p2

    add-int/lit16 v0, v0, 0x80

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static inStaticWindow(II)Z
    .locals 1
    .param p0, "c"    # I
    .param p1, "whichWindow"    # I

    .prologue
    .line 922
    sget-object v0, Lcom/ibm/icu/text/UnicodeCompressor;->sOffsets:[I

    aget v0, v0, p1

    if-lt p0, v0, :cond_0

    sget-object v0, Lcom/ibm/icu/text/UnicodeCompressor;->sOffsets:[I

    aget v0, v0, p1

    add-int/lit16 v0, v0, 0x80

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isCompressible(I)Z
    .locals 1
    .param p0, "c"    # I

    .prologue
    .line 937
    const/16 v0, 0x3400

    if-lt p0, v0, :cond_0

    const v0, 0xe000

    if-lt p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static makeIndex(I)I
    .locals 2
    .param p0, "c"    # I

    .prologue
    const/16 v1, 0x30a0

    .line 867
    const/16 v0, 0xc0

    if-lt p0, v0, :cond_0

    const/16 v0, 0x140

    if-ge p0, v0, :cond_0

    .line 868
    const/16 v0, 0xf9

    .line 890
    :goto_0
    return v0

    .line 869
    :cond_0
    const/16 v0, 0x250

    if-lt p0, v0, :cond_1

    const/16 v0, 0x2d0

    if-ge p0, v0, :cond_1

    .line 870
    const/16 v0, 0xfa

    goto :goto_0

    .line 871
    :cond_1
    const/16 v0, 0x370

    if-lt p0, v0, :cond_2

    const/16 v0, 0x3f0

    if-ge p0, v0, :cond_2

    .line 872
    const/16 v0, 0xfb

    goto :goto_0

    .line 873
    :cond_2
    const/16 v0, 0x530

    if-lt p0, v0, :cond_3

    const/16 v0, 0x590

    if-ge p0, v0, :cond_3

    .line 874
    const/16 v0, 0xfc

    goto :goto_0

    .line 875
    :cond_3
    const/16 v0, 0x3040

    if-lt p0, v0, :cond_4

    if-ge p0, v1, :cond_4

    .line 876
    const/16 v0, 0xfd

    goto :goto_0

    .line 877
    :cond_4
    if-lt p0, v1, :cond_5

    const/16 v0, 0x3120

    if-ge p0, v0, :cond_5

    .line 878
    const/16 v0, 0xfe

    goto :goto_0

    .line 879
    :cond_5
    const v0, 0xff60

    if-lt p0, v0, :cond_6

    const v0, 0xff9f

    if-ge p0, v0, :cond_6

    .line 880
    const/16 v0, 0xff

    goto :goto_0

    .line 883
    :cond_6
    const/16 v0, 0x80

    if-lt p0, v0, :cond_7

    const/16 v0, 0x3400

    if-ge p0, v0, :cond_7

    .line 884
    div-int/lit16 v0, p0, 0x80

    and-int/lit16 v0, v0, 0xff

    goto :goto_0

    .line 885
    :cond_7
    const v0, 0xe000

    if-lt p0, v0, :cond_8

    const v0, 0xffff

    if-gt p0, v0, :cond_8

    .line 886
    const v0, 0xac00

    sub-int v0, p0, v0

    div-int/lit16 v0, v0, 0x80

    and-int/lit16 v0, v0, 0xff

    goto :goto_0

    .line 890
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public compress([CII[I[BII)I
    .locals 14
    .param p1, "charBuffer"    # [C
    .param p2, "charBufferStart"    # I
    .param p3, "charBufferLimit"    # I
    .param p4, "charsRead"    # [I
    .param p5, "byteBuffer"    # [B
    .param p6, "byteBufferStart"    # I
    .param p7, "byteBufferLimit"    # I

    .prologue
    .line 365
    move/from16 v1, p6

    .line 368
    .local v1, "bytePos":I
    move/from16 v9, p2

    .line 371
    .local v9, "ucPos":I
    const/4 v4, -0x1

    .line 374
    .local v4, "curUC":I
    const/4 v3, -0x1

    .line 377
    .local v3, "curIndex":I
    const/4 v8, -0x1

    .line 378
    .local v8, "nextUC":I
    const/4 v5, -0x1

    .line 381
    .local v5, "forwardUC":I
    const/4 v11, 0x0

    .line 384
    .local v11, "whichWindow":I
    const/4 v6, 0x0

    .line 385
    .local v6, "hiByte":I
    const/4 v7, 0x0

    .line 389
    .local v7, "loByte":I
    move-object/from16 v0, p5

    array-length v12, v0

    const/4 v13, 0x4

    if-lt v12, v13, :cond_0

    sub-int v12, p7, p6

    const/4 v13, 0x4

    if-ge v12, v13, :cond_1

    .line 390
    :cond_0
    new-instance v12, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v13, "byteBuffer.length < 4"

    invoke-direct {v12, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 393
    :cond_1
    :goto_0
    move/from16 v0, p3

    if-ge v9, v0, :cond_2

    move/from16 v0, p7

    if-ge v1, v0, :cond_2

    .line 394
    iget v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fMode:I

    packed-switch v12, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move v10, v9

    .end local v9    # "ucPos":I
    .local v10, "ucPos":I
    move v2, v1

    .line 398
    .end local v1    # "bytePos":I
    .local v2, "bytePos":I
    :goto_1
    move/from16 v0, p3

    if-ge v10, v0, :cond_2e

    move/from16 v0, p7

    if-ge v2, v0, :cond_2e

    .line 400
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "ucPos":I
    .restart local v9    # "ucPos":I
    aget-char v4, p1, v10

    .line 403
    move/from16 v0, p3

    if-ge v9, v0, :cond_4

    .line 404
    aget-char v8, p1, v9

    .line 410
    :goto_2
    const/16 v12, 0x80

    if-ge v4, v12, :cond_6

    .line 411
    and-int/lit16 v7, v4, 0xff

    .line 416
    sget-object v12, Lcom/ibm/icu/text/UnicodeCompressor;->sSingleTagTable:[Z

    aget-boolean v12, v12, v7

    if-eqz v12, :cond_2f

    .line 420
    add-int/lit8 v12, v2, 0x1

    move/from16 v0, p7

    if-lt v12, v0, :cond_5

    .line 421
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .line 812
    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    :cond_2
    :goto_3
    if-eqz p4, :cond_3

    .line 813
    const/4 v12, 0x0

    sub-int v13, v9, p2

    aput v13, p4, v12

    .line 816
    :cond_3
    sub-int v12, v1, p6

    return v12

    .line 406
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_4
    const/4 v8, -0x1

    goto :goto_2

    .line 425
    :cond_5
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    const/4 v12, 0x1

    aput-byte v12, p5, v2

    .line 428
    :goto_4
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    int-to-byte v12, v7

    aput-byte v12, p5, v1

    move v10, v9

    .line 429
    .end local v9    # "ucPos":I
    .restart local v10    # "ucPos":I
    goto :goto_1

    .line 434
    .end local v10    # "ucPos":I
    .restart local v9    # "ucPos":I
    :cond_6
    iget v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fCurrentWindow:I

    invoke-direct {p0, v4, v12}, Lcom/ibm/icu/text/UnicodeCompressor;->inDynamicWindow(II)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 435
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    iget v13, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fCurrentWindow:I

    aget v12, v12, v13

    sub-int v12, v4, v12

    add-int/lit16 v12, v12, 0x80

    int-to-byte v12, v12

    aput-byte v12, p5, v2

    move v10, v9

    .end local v9    # "ucPos":I
    .restart local v10    # "ucPos":I
    move v2, v1

    .line 438
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    goto :goto_1

    .line 442
    .end local v10    # "ucPos":I
    .restart local v9    # "ucPos":I
    :cond_7
    invoke-static {v4}, Lcom/ibm/icu/text/UnicodeCompressor;->isCompressible(I)Z

    move-result v12

    if-nez v12, :cond_c

    .line 444
    const/4 v12, -0x1

    if-eq v8, v12, :cond_9

    invoke-static {v8}, Lcom/ibm/icu/text/UnicodeCompressor;->isCompressible(I)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 449
    add-int/lit8 v12, v2, 0x2

    move/from16 v0, p7

    if-lt v12, v0, :cond_8

    .line 450
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto :goto_3

    .line 452
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_8
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    const/16 v12, 0xe

    aput-byte v12, p5, v2

    .line 453
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    ushr-int/lit8 v12, v4, 0x8

    int-to-byte v12, v12

    aput-byte v12, p5, v1

    .line 454
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    and-int/lit16 v12, v4, 0xff

    int-to-byte v12, v12

    aput-byte v12, p5, v2

    move v10, v9

    .end local v9    # "ucPos":I
    .restart local v10    # "ucPos":I
    move v2, v1

    .line 455
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    goto/16 :goto_1

    .line 460
    .end local v10    # "ucPos":I
    .restart local v9    # "ucPos":I
    :cond_9
    add-int/lit8 v12, v2, 0x3

    move/from16 v0, p7

    if-lt v12, v0, :cond_a

    .line 461
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto :goto_3

    .line 463
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_a
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    const/16 v12, 0xf

    aput-byte v12, p5, v2

    .line 465
    ushr-int/lit8 v6, v4, 0x8

    .line 466
    and-int/lit16 v7, v4, 0xff

    .line 468
    sget-object v12, Lcom/ibm/icu/text/UnicodeCompressor;->sUnicodeTagTable:[Z

    aget-boolean v12, v12, v6

    if-eqz v12, :cond_b

    .line 470
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    const/16 v12, -0x10

    aput-byte v12, p5, v1

    move v1, v2

    .line 472
    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    :cond_b
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    int-to-byte v12, v6

    aput-byte v12, p5, v1

    .line 473
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    int-to-byte v12, v7

    aput-byte v12, p5, v2

    .line 475
    const/4 v12, 0x1

    iput v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fMode:I

    goto/16 :goto_0

    .line 483
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_c
    invoke-direct {p0, v4}, Lcom/ibm/icu/text/UnicodeCompressor;->findDynamicWindow(I)I

    move-result v11

    const/4 v12, -0x1

    if-eq v11, v12, :cond_11

    .line 486
    add-int/lit8 v12, v9, 0x1

    move/from16 v0, p3

    if-ge v12, v0, :cond_d

    .line 487
    add-int/lit8 v12, v9, 0x1

    aget-char v5, p1, v12

    .line 494
    :goto_5
    invoke-direct {p0, v8, v11}, Lcom/ibm/icu/text/UnicodeCompressor;->inDynamicWindow(II)Z

    move-result v12

    if-eqz v12, :cond_f

    invoke-direct {p0, v5, v11}, Lcom/ibm/icu/text/UnicodeCompressor;->inDynamicWindow(II)Z

    move-result v12

    if-eqz v12, :cond_f

    .line 499
    add-int/lit8 v12, v2, 0x1

    move/from16 v0, p7

    if-lt v12, v0, :cond_e

    .line 500
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_3

    .line 489
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_d
    const/4 v5, -0x1

    goto :goto_5

    .line 502
    :cond_e
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    add-int/lit8 v12, v11, 0x10

    int-to-byte v12, v12

    aput-byte v12, p5, v2

    .line 503
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    aget v12, v12, v11

    sub-int v12, v4, v12

    add-int/lit16 v12, v12, 0x80

    int-to-byte v12, v12

    aput-byte v12, p5, v1

    .line 506
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamps:[I

    iget v13, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamp:I

    add-int/lit8 v13, v13, 0x1

    iput v13, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamp:I

    aput v13, v12, v11

    .line 507
    iput v11, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fCurrentWindow:I

    move v10, v9

    .line 508
    .end local v9    # "ucPos":I
    .restart local v10    # "ucPos":I
    goto/16 :goto_1

    .line 516
    .end local v10    # "ucPos":I
    .restart local v9    # "ucPos":I
    :cond_f
    add-int/lit8 v12, v2, 0x1

    move/from16 v0, p7

    if-lt v12, v0, :cond_10

    .line 517
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_3

    .line 519
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_10
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    add-int/lit8 v12, v11, 0x1

    int-to-byte v12, v12

    aput-byte v12, p5, v2

    .line 520
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    aget v12, v12, v11

    sub-int v12, v4, v12

    add-int/lit16 v12, v12, 0x80

    int-to-byte v12, v12

    aput-byte v12, p5, v1

    move v10, v9

    .line 524
    .end local v9    # "ucPos":I
    .restart local v10    # "ucPos":I
    goto/16 :goto_1

    .line 530
    .end local v10    # "ucPos":I
    .restart local v9    # "ucPos":I
    :cond_11
    invoke-static {v4}, Lcom/ibm/icu/text/UnicodeCompressor;->findStaticWindow(I)I

    move-result v11

    const/4 v12, -0x1

    if-eq v11, v12, :cond_13

    invoke-static {v8, v11}, Lcom/ibm/icu/text/UnicodeCompressor;->inStaticWindow(II)Z

    move-result v12

    if-nez v12, :cond_13

    .line 536
    add-int/lit8 v12, v2, 0x1

    move/from16 v0, p7

    if-lt v12, v0, :cond_12

    .line 537
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_3

    .line 539
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_12
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    add-int/lit8 v12, v11, 0x1

    int-to-byte v12, v12

    aput-byte v12, p5, v2

    .line 540
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    sget-object v12, Lcom/ibm/icu/text/UnicodeCompressor;->sOffsets:[I

    aget v12, v12, v11

    sub-int v12, v4, v12

    int-to-byte v12, v12

    aput-byte v12, p5, v1

    move v10, v9

    .line 542
    .end local v9    # "ucPos":I
    .restart local v10    # "ucPos":I
    goto/16 :goto_1

    .line 548
    .end local v10    # "ucPos":I
    .restart local v9    # "ucPos":I
    :cond_13
    invoke-static {v4}, Lcom/ibm/icu/text/UnicodeCompressor;->makeIndex(I)I

    move-result v3

    .line 549
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fIndexCount:[I

    aget v13, v12, v3

    add-int/lit8 v13, v13, 0x1

    aput v13, v12, v3

    .line 552
    add-int/lit8 v12, v9, 0x1

    move/from16 v0, p3

    if-ge v12, v0, :cond_15

    .line 553
    add-int/lit8 v12, v9, 0x1

    aget-char v5, p1, v12

    .line 563
    :goto_6
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fIndexCount:[I

    aget v12, v12, v3

    const/4 v13, 0x1

    if-gt v12, v13, :cond_14

    invoke-static {v8}, Lcom/ibm/icu/text/UnicodeCompressor;->makeIndex(I)I

    move-result v12

    if-ne v3, v12, :cond_17

    invoke-static {v5}, Lcom/ibm/icu/text/UnicodeCompressor;->makeIndex(I)I

    move-result v12

    if-ne v3, v12, :cond_17

    .line 569
    :cond_14
    add-int/lit8 v12, v2, 0x2

    move/from16 v0, p7

    if-lt v12, v0, :cond_16

    .line 570
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_3

    .line 555
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_15
    const/4 v5, -0x1

    goto :goto_6

    .line 573
    :cond_16
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeCompressor;->getLRDefinedWindow()I

    move-result v11

    .line 575
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    add-int/lit8 v12, v11, 0x18

    int-to-byte v12, v12

    aput-byte v12, p5, v2

    .line 576
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    int-to-byte v12, v3

    aput-byte v12, p5, v1

    .line 577
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    sget-object v12, Lcom/ibm/icu/text/UnicodeCompressor;->sOffsetTable:[I

    aget v12, v12, v3

    sub-int v12, v4, v12

    add-int/lit16 v12, v12, 0x80

    int-to-byte v12, v12

    aput-byte v12, p5, v2

    .line 581
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    sget-object v13, Lcom/ibm/icu/text/UnicodeCompressor;->sOffsetTable:[I

    aget v13, v13, v3

    aput v13, v12, v11

    .line 582
    iput v11, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fCurrentWindow:I

    .line 583
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamps:[I

    iget v13, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamp:I

    add-int/lit8 v13, v13, 0x1

    iput v13, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamp:I

    aput v13, v12, v11

    move v10, v9

    .end local v9    # "ucPos":I
    .restart local v10    # "ucPos":I
    move v2, v1

    .line 584
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    goto/16 :goto_1

    .line 596
    .end local v10    # "ucPos":I
    .restart local v9    # "ucPos":I
    :cond_17
    add-int/lit8 v12, v2, 0x3

    move/from16 v0, p7

    if-lt v12, v0, :cond_18

    .line 597
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_3

    .line 599
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_18
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    const/16 v12, 0xf

    aput-byte v12, p5, v2

    .line 601
    ushr-int/lit8 v6, v4, 0x8

    .line 602
    and-int/lit16 v7, v4, 0xff

    .line 604
    sget-object v12, Lcom/ibm/icu/text/UnicodeCompressor;->sUnicodeTagTable:[Z

    aget-boolean v12, v12, v6

    if-eqz v12, :cond_19

    .line 606
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    const/16 v12, -0x10

    aput-byte v12, p5, v1

    move v1, v2

    .line 608
    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    :cond_19
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    int-to-byte v12, v6

    aput-byte v12, p5, v1

    .line 609
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    int-to-byte v12, v7

    aput-byte v12, p5, v2

    .line 611
    const/4 v12, 0x1

    iput v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fMode:I

    goto/16 :goto_0

    .line 640
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_1a
    ushr-int/lit8 v6, v4, 0x8

    .line 641
    and-int/lit16 v7, v4, 0xff

    .line 643
    sget-object v12, Lcom/ibm/icu/text/UnicodeCompressor;->sUnicodeTagTable:[Z

    aget-boolean v12, v12, v6

    if-eqz v12, :cond_2d

    .line 645
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    const/16 v12, -0x10

    aput-byte v12, p5, v2

    .line 647
    :goto_7
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    int-to-byte v12, v6

    aput-byte v12, p5, v1

    .line 648
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    int-to-byte v12, v7

    aput-byte v12, p5, v2

    move v10, v9

    .end local v9    # "ucPos":I
    .restart local v10    # "ucPos":I
    move v2, v1

    .line 621
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :goto_8
    move/from16 v0, p3

    if-ge v10, v0, :cond_2e

    move/from16 v0, p7

    if-ge v2, v0, :cond_2e

    .line 623
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "ucPos":I
    .restart local v9    # "ucPos":I
    aget-char v4, p1, v10

    .line 626
    move/from16 v0, p3

    if-ge v9, v0, :cond_1c

    .line 627
    aget-char v8, p1, v9

    .line 633
    :goto_9
    invoke-static {v4}, Lcom/ibm/icu/text/UnicodeCompressor;->isCompressible(I)Z

    move-result v12

    if-eqz v12, :cond_1b

    const/4 v12, -0x1

    if-eq v8, v12, :cond_1d

    invoke-static {v8}, Lcom/ibm/icu/text/UnicodeCompressor;->isCompressible(I)Z

    move-result v12

    if-nez v12, :cond_1d

    .line 637
    :cond_1b
    add-int/lit8 v12, v2, 0x2

    move/from16 v0, p7

    if-lt v12, v0, :cond_1a

    .line 638
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_3

    .line 629
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_1c
    const/4 v8, -0x1

    goto :goto_9

    .line 653
    :cond_1d
    const/16 v12, 0x80

    if-ge v4, v12, :cond_21

    .line 654
    and-int/lit16 v7, v4, 0xff

    .line 659
    const/4 v12, -0x1

    if-eq v8, v12, :cond_1f

    const/16 v12, 0x80

    if-ge v8, v12, :cond_1f

    sget-object v12, Lcom/ibm/icu/text/UnicodeCompressor;->sSingleTagTable:[Z

    aget-boolean v12, v12, v7

    if-nez v12, :cond_1f

    .line 664
    add-int/lit8 v12, v2, 0x1

    move/from16 v0, p7

    if-lt v12, v0, :cond_1e

    .line 665
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_3

    .line 668
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_1e
    iget v11, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fCurrentWindow:I

    .line 669
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    add-int/lit16 v12, v11, 0xe0

    int-to-byte v12, v12

    aput-byte v12, p5, v2

    .line 670
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    int-to-byte v12, v7

    aput-byte v12, p5, v1

    .line 673
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamps:[I

    iget v13, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamp:I

    add-int/lit8 v13, v13, 0x1

    iput v13, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamp:I

    aput v13, v12, v11

    .line 674
    const/4 v12, 0x0

    iput v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fMode:I

    move v1, v2

    .line 675
    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_0

    .line 685
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_1f
    add-int/lit8 v12, v2, 0x1

    move/from16 v0, p7

    if-lt v12, v0, :cond_20

    .line 686
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_3

    .line 691
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_20
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    const/4 v12, 0x0

    aput-byte v12, p5, v2

    .line 692
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    int-to-byte v12, v7

    aput-byte v12, p5, v1

    move v10, v9

    .line 694
    .end local v9    # "ucPos":I
    .restart local v10    # "ucPos":I
    goto :goto_8

    .line 697
    .end local v10    # "ucPos":I
    .restart local v9    # "ucPos":I
    :cond_21
    invoke-direct {p0, v4}, Lcom/ibm/icu/text/UnicodeCompressor;->findDynamicWindow(I)I

    move-result v11

    const/4 v12, -0x1

    if-eq v11, v12, :cond_25

    .line 702
    invoke-direct {p0, v8, v11}, Lcom/ibm/icu/text/UnicodeCompressor;->inDynamicWindow(II)Z

    move-result v12

    if-eqz v12, :cond_23

    .line 706
    add-int/lit8 v12, v2, 0x1

    move/from16 v0, p7

    if-lt v12, v0, :cond_22

    .line 707
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_3

    .line 709
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_22
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    add-int/lit16 v12, v11, 0xe0

    int-to-byte v12, v12

    aput-byte v12, p5, v2

    .line 710
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    aget v12, v12, v11

    sub-int v12, v4, v12

    add-int/lit16 v12, v12, 0x80

    int-to-byte v12, v12

    aput-byte v12, p5, v1

    .line 714
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamps:[I

    iget v13, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamp:I

    add-int/lit8 v13, v13, 0x1

    iput v13, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamp:I

    aput v13, v12, v11

    .line 715
    iput v11, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fCurrentWindow:I

    .line 716
    const/4 v12, 0x0

    iput v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fMode:I

    move v1, v2

    .line 717
    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_0

    .line 726
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_23
    add-int/lit8 v12, v2, 0x2

    move/from16 v0, p7

    if-lt v12, v0, :cond_24

    .line 727
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_3

    .line 729
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_24
    ushr-int/lit8 v6, v4, 0x8

    .line 730
    and-int/lit16 v7, v4, 0xff

    .line 732
    sget-object v12, Lcom/ibm/icu/text/UnicodeCompressor;->sUnicodeTagTable:[Z

    aget-boolean v12, v12, v6

    if-eqz v12, :cond_2c

    .line 734
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    const/16 v12, -0x10

    aput-byte v12, p5, v2

    .line 736
    :goto_a
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    int-to-byte v12, v6

    aput-byte v12, p5, v1

    .line 737
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    int-to-byte v12, v7

    aput-byte v12, p5, v2

    move v10, v9

    .end local v9    # "ucPos":I
    .restart local v10    # "ucPos":I
    move v2, v1

    .line 739
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    goto/16 :goto_8

    .line 744
    .end local v10    # "ucPos":I
    .restart local v9    # "ucPos":I
    :cond_25
    invoke-static {v4}, Lcom/ibm/icu/text/UnicodeCompressor;->makeIndex(I)I

    move-result v3

    .line 745
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fIndexCount:[I

    aget v13, v12, v3

    add-int/lit8 v13, v13, 0x1

    aput v13, v12, v3

    .line 748
    add-int/lit8 v12, v9, 0x1

    move/from16 v0, p3

    if-ge v12, v0, :cond_27

    .line 749
    add-int/lit8 v12, v9, 0x1

    aget-char v5, p1, v12

    .line 760
    :goto_b
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fIndexCount:[I

    aget v12, v12, v3

    const/4 v13, 0x1

    if-gt v12, v13, :cond_26

    invoke-static {v8}, Lcom/ibm/icu/text/UnicodeCompressor;->makeIndex(I)I

    move-result v12

    if-ne v3, v12, :cond_29

    invoke-static {v5}, Lcom/ibm/icu/text/UnicodeCompressor;->makeIndex(I)I

    move-result v12

    if-ne v3, v12, :cond_29

    .line 767
    :cond_26
    add-int/lit8 v12, v2, 0x2

    move/from16 v0, p7

    if-lt v12, v0, :cond_28

    .line 768
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_3

    .line 751
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_27
    const/4 v5, -0x1

    goto :goto_b

    .line 771
    :cond_28
    invoke-direct {p0}, Lcom/ibm/icu/text/UnicodeCompressor;->getLRDefinedWindow()I

    move-result v11

    .line 773
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    add-int/lit16 v12, v11, 0xe8

    int-to-byte v12, v12

    aput-byte v12, p5, v2

    .line 774
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    int-to-byte v12, v3

    aput-byte v12, p5, v1

    .line 775
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    sget-object v12, Lcom/ibm/icu/text/UnicodeCompressor;->sOffsetTable:[I

    aget v12, v12, v3

    sub-int v12, v4, v12

    add-int/lit16 v12, v12, 0x80

    int-to-byte v12, v12

    aput-byte v12, p5, v2

    .line 779
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    sget-object v13, Lcom/ibm/icu/text/UnicodeCompressor;->sOffsetTable:[I

    aget v13, v13, v3

    aput v13, v12, v11

    .line 780
    iput v11, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fCurrentWindow:I

    .line 781
    iget-object v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamps:[I

    iget v13, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamp:I

    add-int/lit8 v13, v13, 0x1

    iput v13, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamp:I

    aput v13, v12, v11

    .line 782
    const/4 v12, 0x0

    iput v12, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fMode:I

    goto/16 :goto_0

    .line 793
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_29
    add-int/lit8 v12, v2, 0x2

    move/from16 v0, p7

    if-lt v12, v0, :cond_2a

    .line 794
    add-int/lit8 v9, v9, -0x1

    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_3

    .line 796
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_2a
    ushr-int/lit8 v6, v4, 0x8

    .line 797
    and-int/lit16 v7, v4, 0xff

    .line 799
    sget-object v12, Lcom/ibm/icu/text/UnicodeCompressor;->sUnicodeTagTable:[Z

    aget-boolean v12, v12, v6

    if-eqz v12, :cond_2b

    .line 801
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    const/16 v12, -0x10

    aput-byte v12, p5, v2

    .line 803
    :goto_c
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    int-to-byte v12, v6

    aput-byte v12, p5, v1

    .line 804
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    int-to-byte v12, v7

    aput-byte v12, p5, v2

    move v10, v9

    .end local v9    # "ucPos":I
    .restart local v10    # "ucPos":I
    move v2, v1

    .line 807
    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    goto/16 :goto_8

    .end local v10    # "ucPos":I
    .restart local v9    # "ucPos":I
    :cond_2b
    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto :goto_c

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_2c
    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_a

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_2d
    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_7

    .end local v1    # "bytePos":I
    .end local v9    # "ucPos":I
    .restart local v2    # "bytePos":I
    .restart local v10    # "ucPos":I
    :cond_2e
    move v9, v10

    .end local v10    # "ucPos":I
    .restart local v9    # "ucPos":I
    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_0

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    :cond_2f
    move v1, v2

    .end local v2    # "bytePos":I
    .restart local v1    # "bytePos":I
    goto/16 :goto_4

    :pswitch_1
    move v10, v9

    .end local v9    # "ucPos":I
    .restart local v10    # "ucPos":I
    move v2, v1

    .end local v1    # "bytePos":I
    .restart local v2    # "bytePos":I
    goto/16 :goto_8

    .line 394
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public reset()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 828
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    const/16 v2, 0x80

    aput v2, v1, v4

    .line 829
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    const/4 v2, 0x1

    const/16 v3, 0xc0

    aput v3, v1, v2

    .line 830
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    const/4 v2, 0x2

    const/16 v3, 0x400

    aput v3, v1, v2

    .line 831
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    const/4 v2, 0x3

    const/16 v3, 0x600

    aput v3, v1, v2

    .line 832
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    const/4 v2, 0x4

    const/16 v3, 0x900

    aput v3, v1, v2

    .line 833
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    const/4 v2, 0x5

    const/16 v3, 0x3040

    aput v3, v1, v2

    .line 834
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    const/4 v2, 0x6

    const/16 v3, 0x30a0

    aput v3, v1, v2

    .line 835
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fOffsets:[I

    const/4 v2, 0x7

    const v3, 0xff00

    aput v3, v1, v2

    .line 839
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 840
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamps:[I

    aput v4, v1, v0

    .line 839
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 844
    :cond_0
    const/4 v0, 0x0

    :goto_1
    const/16 v1, 0xff

    if-gt v0, v1, :cond_1

    .line 845
    iget-object v1, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fIndexCount:[I

    aput v4, v1, v0

    .line 844
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 848
    :cond_1
    iput v4, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fTimeStamp:I

    .line 849
    iput v4, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fCurrentWindow:I

    .line 850
    iput v4, p0, Lcom/ibm/icu/text/UnicodeCompressor;->fMode:I

    .line 851
    return-void
.end method
