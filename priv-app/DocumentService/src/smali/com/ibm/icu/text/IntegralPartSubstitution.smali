.class Lcom/ibm/icu/text/IntegralPartSubstitution;
.super Lcom/ibm/icu/text/NFSubstitution;
.source "NFSubstitution.java"


# direct methods
.method constructor <init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "ruleSet"    # Lcom/ibm/icu/text/NFRuleSet;
    .param p3, "formatter"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;
    .param p4, "description"    # Ljava/lang/String;

    .prologue
    .line 1053
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/ibm/icu/text/NFSubstitution;-><init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    .line 1054
    return-void
.end method


# virtual methods
.method public calcUpperBound(D)D
    .locals 2
    .param p1, "oldUpperBound"    # D

    .prologue
    .line 1104
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    return-wide v0
.end method

.method public composeRuleValue(DD)D
    .locals 3
    .param p1, "newRuleValue"    # D
    .param p3, "oldRuleValue"    # D

    .prologue
    .line 1094
    add-double v0, p1, p3

    return-wide v0
.end method

.method tokenChar()C
    .locals 1

    .prologue
    .line 1116
    const/16 v0, 0x3c

    return v0
.end method

.method public transformNumber(D)D
    .locals 3
    .param p1, "number"    # D

    .prologue
    .line 1076
    invoke-static {p1, p2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public transformNumber(J)J
    .locals 1
    .param p1, "number"    # J

    .prologue
    .line 1067
    return-wide p1
.end method
