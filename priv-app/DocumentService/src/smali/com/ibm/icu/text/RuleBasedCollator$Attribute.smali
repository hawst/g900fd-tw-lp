.class interface abstract Lcom/ibm/icu/text/RuleBasedCollator$Attribute;
.super Ljava/lang/Object;
.source "RuleBasedCollator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/RuleBasedCollator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "Attribute"
.end annotation


# static fields
.field public static final ALTERNATE_HANDLING_:I = 0x1

.field public static final CASE_FIRST_:I = 0x2

.field public static final CASE_LEVEL_:I = 0x3

.field public static final FRENCH_COLLATION_:I = 0x0

.field public static final HIRAGANA_QUATERNARY_MODE_:I = 0x6

.field public static final LIMIT_:I = 0x7

.field public static final NORMALIZATION_MODE_:I = 0x4

.field public static final STRENGTH_:I = 0x5
