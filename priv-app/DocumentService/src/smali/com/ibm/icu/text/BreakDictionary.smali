.class public Lcom/ibm/icu/text/BreakDictionary;
.super Ljava/lang/Object;
.source "BreakDictionary.java"


# instance fields
.field private columnMap:Lcom/ibm/icu/util/CompactByteArray;

.field private numCols:I

.field private reverseColumnMap:[C

.field private rowIndex:[S

.field private rowIndexFlags:[I

.field private rowIndexFlagsIndex:[S

.field private rowIndexShifts:[B

.field private table:[S


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "dictionaryStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object v0, p0, Lcom/ibm/icu/text/BreakDictionary;->reverseColumnMap:[C

    .line 105
    iput-object v0, p0, Lcom/ibm/icu/text/BreakDictionary;->columnMap:Lcom/ibm/icu/util/CompactByteArray;

    .line 130
    iput-object v0, p0, Lcom/ibm/icu/text/BreakDictionary;->table:[S

    .line 135
    iput-object v0, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndex:[S

    .line 143
    iput-object v0, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexFlags:[I

    .line 153
    iput-object v0, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexFlagsIndex:[S

    .line 159
    iput-object v0, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexShifts:[B

    .line 170
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/BreakDictionary;->readDictionaryFile(Ljava/io/DataInputStream;)V

    .line 171
    return-void
.end method

.method private final cellIsPopulated(II)Z
    .locals 6
    .param p1, "row"    # I
    .param p2, "col"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 294
    iget-object v3, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexFlagsIndex:[S

    aget-short v3, v3, p1

    if-gez v3, :cond_2

    .line 295
    iget-object v3, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexFlagsIndex:[S

    aget-short v3, v3, p1

    neg-int v3, v3

    if-ne p2, v3, :cond_1

    .line 306
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 295
    goto :goto_0

    .line 305
    :cond_2
    iget-object v3, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexFlags:[I

    iget-object v4, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexFlagsIndex:[S

    aget-short v4, v4, p1

    shr-int/lit8 v5, p2, 0x5

    add-int/2addr v4, v5

    aget v0, v3, v4

    .line 306
    .local v0, "flags":I
    and-int/lit8 v3, p2, 0x1f

    shl-int v3, v1, v3

    and-int/2addr v3, v0

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method private final internalAt(II)S
    .locals 2
    .param p1, "row"    # I
    .param p2, "col"    # I

    .prologue
    .line 320
    iget-object v0, p0, Lcom/ibm/icu/text/BreakDictionary;->table:[S

    iget v1, p0, Lcom/ibm/icu/text/BreakDictionary;->numCols:I

    mul-int/2addr v1, p1

    add-int/2addr v1, p2

    aget-short v0, v0, v1

    return v0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 7
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/UnsupportedEncodingException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 44
    aget-object v1, p0, v6

    .line 46
    .local v1, "filename":Ljava/lang/String;
    new-instance v0, Lcom/ibm/icu/text/BreakDictionary;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v3}, Lcom/ibm/icu/text/BreakDictionary;-><init>(Ljava/io/InputStream;)V

    .line 48
    .local v0, "dictionary":Lcom/ibm/icu/text/BreakDictionary;
    const/4 v2, 0x0

    .line 50
    .local v2, "out":Ljava/io/PrintWriter;
    array-length v3, p0

    const/4 v4, 0x2

    if-lt v3, v4, :cond_0

    .line 51
    new-instance v2, Ljava/io/PrintWriter;

    .end local v2    # "out":Ljava/io/PrintWriter;
    new-instance v3, Ljava/io/OutputStreamWriter;

    new-instance v4, Ljava/io/FileOutputStream;

    const/4 v5, 0x1

    aget-object v5, p0, v5

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, "UnicodeLittle"

    invoke-direct {v3, v4, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 54
    .restart local v2    # "out":Ljava/io/PrintWriter;
    :cond_0
    const-string/jumbo v3, ""

    invoke-virtual {v0, v3, v6, v2}, Lcom/ibm/icu/text/BreakDictionary;->printWordList(Ljava/lang/String;ILjava/io/PrintWriter;)V

    .line 56
    if-eqz v2, :cond_1

    .line 57
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    .line 59
    :cond_1
    return-void
.end method


# virtual methods
.method public final at(IC)S
    .locals 2
    .param p1, "row"    # I
    .param p2, "ch"    # C

    .prologue
    .line 254
    iget-object v1, p0, Lcom/ibm/icu/text/BreakDictionary;->columnMap:Lcom/ibm/icu/util/CompactByteArray;

    invoke-virtual {v1, p2}, Lcom/ibm/icu/util/CompactByteArray;->elementAt(C)B

    move-result v0

    .line 255
    .local v0, "col":I
    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/text/BreakDictionary;->at(II)S

    move-result v1

    return v1
.end method

.method public final at(II)S
    .locals 2
    .param p1, "row"    # I
    .param p2, "col"    # I

    .prologue
    .line 272
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/BreakDictionary;->cellIsPopulated(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndex:[S

    aget-short v0, v0, p1

    iget-object v1, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexShifts:[B

    aget-byte v1, v1, p1

    add-int/2addr v1, p2

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/BreakDictionary;->internalAt(II)S

    move-result v0

    .line 282
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public printWordList(Ljava/lang/String;ILjava/io/PrintWriter;)V
    .locals 6
    .param p1, "partialWord"    # Ljava/lang/String;
    .param p2, "state"    # I
    .param p3, "out"    # Ljava/io/PrintWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v5, 0xffff

    .line 67
    if-ne p2, v5, :cond_1

    .line 68
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 69
    if-eqz p3, :cond_0

    .line 70
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 89
    :cond_0
    return-void

    .line 74
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lcom/ibm/icu/text/BreakDictionary;->numCols:I

    if-ge v0, v4, :cond_0

    .line 75
    invoke-virtual {p0, p2, v0}, Lcom/ibm/icu/text/BreakDictionary;->at(II)S

    move-result v4

    and-int v3, v4, v5

    .line 77
    .local v3, "newState":I
    if-eqz v3, :cond_3

    .line 78
    iget-object v4, p0, Lcom/ibm/icu/text/BreakDictionary;->reverseColumnMap:[C

    aget-char v1, v4, v0

    .line 79
    .local v1, "newChar":C
    move-object v2, p1

    .line 81
    .local v2, "newPartialWord":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 82
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 85
    :cond_2
    invoke-virtual {p0, v2, v3, p3}, Lcom/ibm/icu/text/BreakDictionary;->printWordList(Ljava/lang/String;ILjava/io/PrintWriter;)V

    .line 74
    .end local v1    # "newChar":C
    .end local v2    # "newPartialWord":Ljava/lang/String;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public readDictionaryFile(Ljava/io/DataInputStream;)V
    .locals 8
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    .line 185
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 186
    .local v3, "l":I
    new-array v4, v3, [C

    .line 187
    .local v4, "temp":[C
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, v4

    if-ge v2, v6, :cond_0

    .line 188
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readShort()S

    move-result v6

    int-to-char v6, v6

    aput-char v6, v4, v2

    .line 187
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 189
    :cond_0
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 190
    new-array v5, v3, [B

    .line 191
    .local v5, "temp2":[B
    const/4 v2, 0x0

    :goto_1
    array-length v6, v5

    if-ge v2, v6, :cond_1

    .line 192
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readByte()B

    move-result v6

    aput-byte v6, v5, v2

    .line 191
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 193
    :cond_1
    new-instance v6, Lcom/ibm/icu/util/CompactByteArray;

    invoke-direct {v6, v4, v5}, Lcom/ibm/icu/util/CompactByteArray;-><init>([C[B)V

    iput-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->columnMap:Lcom/ibm/icu/util/CompactByteArray;

    .line 196
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    iput v6, p0, Lcom/ibm/icu/text/BreakDictionary;->numCols:I

    .line 197
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    .line 200
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 201
    new-array v6, v3, [S

    iput-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndex:[S

    .line 202
    const/4 v2, 0x0

    :goto_2
    iget-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndex:[S

    array-length v6, v6

    if-ge v2, v6, :cond_2

    .line 203
    iget-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndex:[S

    invoke-virtual {p1}, Ljava/io/DataInputStream;->readShort()S

    move-result v7

    aput-short v7, v6, v2

    .line 202
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 206
    :cond_2
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 207
    new-array v6, v3, [S

    iput-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexFlagsIndex:[S

    .line 208
    const/4 v2, 0x0

    :goto_3
    iget-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexFlagsIndex:[S

    array-length v6, v6

    if-ge v2, v6, :cond_3

    .line 209
    iget-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexFlagsIndex:[S

    invoke-virtual {p1}, Ljava/io/DataInputStream;->readShort()S

    move-result v7

    aput-short v7, v6, v2

    .line 208
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 210
    :cond_3
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 211
    new-array v6, v3, [I

    iput-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexFlags:[I

    .line 212
    const/4 v2, 0x0

    :goto_4
    iget-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexFlags:[I

    array-length v6, v6

    if-ge v2, v6, :cond_4

    .line 213
    iget-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexFlags:[I

    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    aput v7, v6, v2

    .line 212
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 216
    :cond_4
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 217
    new-array v6, v3, [B

    iput-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexShifts:[B

    .line 218
    const/4 v2, 0x0

    :goto_5
    iget-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexShifts:[B

    array-length v6, v6

    if-ge v2, v6, :cond_5

    .line 219
    iget-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->rowIndexShifts:[B

    invoke-virtual {p1}, Ljava/io/DataInputStream;->readByte()B

    move-result v7

    aput-byte v7, v6, v2

    .line 218
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 222
    :cond_5
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 223
    new-array v6, v3, [S

    iput-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->table:[S

    .line 224
    const/4 v2, 0x0

    :goto_6
    iget-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->table:[S

    array-length v6, v6

    if-ge v2, v6, :cond_6

    .line 225
    iget-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->table:[S

    invoke-virtual {p1}, Ljava/io/DataInputStream;->readShort()S

    move-result v7

    aput-short v7, v6, v2

    .line 224
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 228
    :cond_6
    iget v6, p0, Lcom/ibm/icu/text/BreakDictionary;->numCols:I

    new-array v6, v6, [C

    iput-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->reverseColumnMap:[C

    .line 229
    const/4 v0, 0x0

    .local v0, "c":C
    :goto_7
    const v6, 0xffff

    if-ge v0, v6, :cond_8

    .line 230
    iget-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->columnMap:Lcom/ibm/icu/util/CompactByteArray;

    invoke-virtual {v6, v0}, Lcom/ibm/icu/util/CompactByteArray;->elementAt(C)B

    move-result v1

    .line 231
    .local v1, "col":I
    if-eqz v1, :cond_7

    .line 232
    iget-object v6, p0, Lcom/ibm/icu/text/BreakDictionary;->reverseColumnMap:[C

    aput-char v0, v6, v1

    .line 229
    :cond_7
    add-int/lit8 v6, v0, 0x1

    int-to-char v0, v6

    goto :goto_7

    .line 237
    .end local v1    # "col":I
    :cond_8
    invoke-virtual {p1}, Ljava/io/DataInputStream;->close()V

    .line 238
    return-void
.end method
