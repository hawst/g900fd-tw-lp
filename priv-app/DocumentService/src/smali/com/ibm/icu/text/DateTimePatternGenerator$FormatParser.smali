.class public Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;
.super Ljava/lang/Object;
.source "DateTimePatternGenerator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/DateTimePatternGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FormatParser"
.end annotation


# instance fields
.field private items:Ljava/util/List;

.field private transient tokenizer:Lcom/ibm/icu/impl/PatternTokenizer;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 1012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1000
    new-instance v0, Lcom/ibm/icu/impl/PatternTokenizer;

    invoke-direct {v0}, Lcom/ibm/icu/impl/PatternTokenizer;-><init>()V

    new-instance v1, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v2, "[a-zA-Z]"

    invoke-direct {v1, v2}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/PatternTokenizer;->setSyntaxCharacters(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/impl/PatternTokenizer;

    move-result-object v0

    new-instance v1, Lcom/ibm/icu/text/UnicodeSet;

    const-string/jumbo v2, "[[[:script=Latn:][:script=Cyrl:]]&[[:L:][:M:]]]"

    invoke-direct {v1, v2}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/PatternTokenizer;->setExtraQuotingCharacters(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/impl/PatternTokenizer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/PatternTokenizer;->setUsingQuote(Z)Lcom/ibm/icu/impl/PatternTokenizer;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->tokenizer:Lcom/ibm/icu/impl/PatternTokenizer;

    .line 1005
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->items:Ljava/util/List;

    .line 1013
    return-void
.end method

.method static access$000(Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    .prologue
    .line 999
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->items:Ljava/util/List;

    return-object v0
.end method

.method private addVariable(Ljava/lang/StringBuffer;Z)V
    .locals 3
    .param p1, "variable"    # Ljava/lang/StringBuffer;
    .param p2, "strict"    # Z

    .prologue
    .line 1059
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1060
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->items:Ljava/util/List;

    new-instance v1, Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1061
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1063
    :cond_0
    return-void
.end method


# virtual methods
.method public getItems()Ljava/util/List;
    .locals 1

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->items:Ljava/util/List;

    return-object v0
.end method

.method public hasDateAndTimeFields()Z
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1157
    const/4 v0, 0x0

    .line 1158
    .local v0, "foundMask":I
    iget-object v8, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->items:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1159
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 1160
    .local v4, "item":Ljava/lang/Object;
    instance-of v8, v4, Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;

    if-eqz v8, :cond_0

    .line 1161
    check-cast v4, Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;

    .end local v4    # "item":Ljava/lang/Object;
    invoke-virtual {v4}, Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;->getType()I

    move-result v5

    .line 1162
    .local v5, "type":I
    shl-int v8, v6, v5

    or-int/2addr v0, v8

    goto :goto_0

    .line 1165
    .end local v5    # "type":I
    :cond_1
    and-int/lit16 v8, v0, 0x3ff

    if-eqz v8, :cond_2

    move v1, v6

    .line 1166
    .local v1, "isDate":Z
    :goto_1
    const v8, 0xfc00

    and-int/2addr v8, v0

    if-eqz v8, :cond_3

    move v2, v6

    .line 1167
    .local v2, "isTime":Z
    :goto_2
    if-eqz v1, :cond_4

    if-eqz v2, :cond_4

    :goto_3
    return v6

    .end local v1    # "isDate":Z
    .end local v2    # "isTime":Z
    :cond_2
    move v1, v7

    .line 1165
    goto :goto_1

    .restart local v1    # "isDate":Z
    :cond_3
    move v2, v7

    .line 1166
    goto :goto_2

    .restart local v2    # "isTime":Z
    :cond_4
    move v6, v7

    .line 1167
    goto :goto_3
.end method

.method public quoteLiteral(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 1271
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->tokenizer:Lcom/ibm/icu/impl/PatternTokenizer;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/PatternTokenizer;->quoteLiteral(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final set(Ljava/lang/String;)Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 1023
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->set(Ljava/lang/String;Z)Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Z)Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;
    .locals 6
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "strict"    # Z

    .prologue
    const/4 v5, 0x0

    .line 1035
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->items:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1036
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    .line 1055
    :goto_0
    return-object p0

    .line 1037
    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->tokenizer:Lcom/ibm/icu/impl/PatternTokenizer;

    invoke-virtual {v3, p1}, Lcom/ibm/icu/impl/PatternTokenizer;->setPattern(Ljava/lang/String;)Lcom/ibm/icu/impl/PatternTokenizer;

    .line 1038
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1039
    .local v0, "buffer":Ljava/lang/StringBuffer;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 1041
    .local v2, "variable":Ljava/lang/StringBuffer;
    :goto_1
    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1042
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->tokenizer:Lcom/ibm/icu/impl/PatternTokenizer;

    invoke-virtual {v3, v0}, Lcom/ibm/icu/impl/PatternTokenizer;->next(Ljava/lang/StringBuffer;)I

    move-result v1

    .line 1043
    .local v1, "status":I
    if-nez v1, :cond_1

    .line 1054
    invoke-direct {p0, v2, v5}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->addVariable(Ljava/lang/StringBuffer;Z)V

    goto :goto_0

    .line 1044
    :cond_1
    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    .line 1045
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    if-eq v3, v4, :cond_2

    .line 1046
    invoke-direct {p0, v2, v5}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->addVariable(Ljava/lang/StringBuffer;Z)V

    .line 1048
    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1050
    :cond_3
    invoke-direct {p0, v2, v5}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->addVariable(Ljava/lang/StringBuffer;Z)V

    .line 1051
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->items:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1125
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->toString(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(II)Ljava/lang/String;
    .locals 5
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    .line 1137
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 1138
    .local v3, "result":Ljava/lang/StringBuffer;
    move v0, p1

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_1

    .line 1139
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->items:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 1140
    .local v1, "item":Ljava/lang/Object;
    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_0

    move-object v2, v1

    .line 1141
    check-cast v2, Ljava/lang/String;

    .line 1142
    .local v2, "itemString":Ljava/lang/String;
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->tokenizer:Lcom/ibm/icu/impl/PatternTokenizer;

    invoke-virtual {v4, v2}, Lcom/ibm/icu/impl/PatternTokenizer;->quoteLiteral(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1138
    .end local v2    # "itemString":Ljava/lang/String;
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1144
    :cond_0
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->items:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1147
    .end local v1    # "item":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
