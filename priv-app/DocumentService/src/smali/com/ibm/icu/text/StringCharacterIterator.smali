.class public final Lcom/ibm/icu/text/StringCharacterIterator;
.super Ljava/lang/Object;
.source "StringCharacterIterator.java"

# interfaces
.implements Ljava/text/CharacterIterator;


# instance fields
.field private begin:I

.field private end:I

.field private pos:I

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/StringCharacterIterator;-><init>(Ljava/lang/String;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "pos"    # I

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/ibm/icu/text/StringCharacterIterator;-><init>(Ljava/lang/String;III)V

    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;III)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "begin"    # I
    .param p3, "end"    # I
    .param p4, "pos"    # I

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    if-nez p1, :cond_0

    .line 69
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 71
    :cond_0
    iput-object p1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->text:Ljava/lang/String;

    .line 73
    if-ltz p2, :cond_1

    if-gt p2, p3, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le p3, v0, :cond_2

    .line 74
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid substring range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_2
    if-lt p4, p2, :cond_3

    if-le p4, p3, :cond_4

    .line 78
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid position"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_4
    iput p2, p0, Lcom/ibm/icu/text/StringCharacterIterator;->begin:I

    .line 82
    iput p3, p0, Lcom/ibm/icu/text/StringCharacterIterator;->end:I

    .line 83
    iput p4, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    .line 84
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 271
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/StringCharacterIterator;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    .local v1, "other":Lcom/ibm/icu/text/StringCharacterIterator;
    return-object v1

    .line 275
    .end local v1    # "other":Lcom/ibm/icu/text/StringCharacterIterator;
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2
.end method

.method public current()C
    .locals 2

    .prologue
    .line 152
    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    iget v1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->begin:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    iget v1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->end:I

    if-ge v0, v1, :cond_0

    .line 153
    iget-object v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->text:Ljava/lang/String;

    iget v1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 156
    :goto_0
    return v0

    :cond_0
    const v0, 0xffff

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 232
    if-ne p0, p1, :cond_1

    .line 250
    :cond_0
    :goto_0
    return v1

    .line 235
    :cond_1
    instance-of v3, p1, Lcom/ibm/icu/text/StringCharacterIterator;

    if-nez v3, :cond_2

    move v1, v2

    .line 236
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 239
    check-cast v0, Lcom/ibm/icu/text/StringCharacterIterator;

    .line 241
    .local v0, "that":Lcom/ibm/icu/text/StringCharacterIterator;
    invoke-virtual {p0}, Lcom/ibm/icu/text/StringCharacterIterator;->hashCode()I

    move-result v3

    invoke-virtual {v0}, Lcom/ibm/icu/text/StringCharacterIterator;->hashCode()I

    move-result v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 242
    goto :goto_0

    .line 244
    :cond_3
    iget-object v3, p0, Lcom/ibm/icu/text/StringCharacterIterator;->text:Ljava/lang/String;

    iget-object v4, v0, Lcom/ibm/icu/text/StringCharacterIterator;->text:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 245
    goto :goto_0

    .line 247
    :cond_4
    iget v3, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    iget v4, v0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    if-ne v3, v4, :cond_5

    iget v3, p0, Lcom/ibm/icu/text/StringCharacterIterator;->begin:I

    iget v4, v0, Lcom/ibm/icu/text/StringCharacterIterator;->begin:I

    if-ne v3, v4, :cond_5

    iget v3, p0, Lcom/ibm/icu/text/StringCharacterIterator;->end:I

    iget v4, v0, Lcom/ibm/icu/text/StringCharacterIterator;->end:I

    if-eq v3, v4, :cond_0

    :cond_5
    move v1, v2

    .line 248
    goto :goto_0
.end method

.method public first()C
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->begin:I

    iput v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    .line 113
    invoke-virtual {p0}, Lcom/ibm/icu/text/StringCharacterIterator;->current()C

    move-result v0

    return v0
.end method

.method public getBeginIndex()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->begin:I

    return v0
.end method

.method public getEndIndex()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->end:I

    return v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->text:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    xor-int/2addr v0, v1

    iget v1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->begin:I

    xor-int/2addr v0, v1

    iget v1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->end:I

    xor-int/2addr v0, v1

    return v0
.end method

.method public last()C
    .locals 2

    .prologue
    .line 123
    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->end:I

    iget v1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->begin:I

    if-eq v0, v1, :cond_0

    .line 124
    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->end:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    .line 128
    :goto_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/StringCharacterIterator;->current()C

    move-result v0

    return v0

    .line 126
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->end:I

    iput v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    goto :goto_0
.end method

.method public next()C
    .locals 2

    .prologue
    .line 167
    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    iget v1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->end:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 168
    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    .line 169
    iget-object v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->text:Ljava/lang/String;

    iget v1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 173
    :goto_0
    return v0

    .line 172
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->end:I

    iput v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    .line 173
    const v0, 0xffff

    goto :goto_0
.end method

.method public previous()C
    .locals 2

    .prologue
    .line 184
    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    iget v1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->begin:I

    if-le v0, v1, :cond_0

    .line 185
    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    .line 186
    iget-object v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->text:Ljava/lang/String;

    iget v1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 189
    :goto_0
    return v0

    :cond_0
    const v0, 0xffff

    goto :goto_0
.end method

.method public setIndex(I)C
    .locals 2
    .param p1, "p"    # I

    .prologue
    .line 138
    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->begin:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->end:I

    if-le p1, v0, :cond_1

    .line 139
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_1
    iput p1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    .line 142
    invoke-virtual {p0}, Lcom/ibm/icu/text/StringCharacterIterator;->current()C

    move-result v0

    return v0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 96
    if-nez p1, :cond_0

    .line 97
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 99
    :cond_0
    iput-object p1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->text:Ljava/lang/String;

    .line 100
    iput v1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->begin:I

    .line 101
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/StringCharacterIterator;->end:I

    .line 102
    iput v1, p0, Lcom/ibm/icu/text/StringCharacterIterator;->pos:I

    .line 103
    return-void
.end method
