.class Lcom/ibm/icu/text/PluralRules$1;
.super Ljava/lang/Object;
.source "PluralRules.java"

# interfaces
.implements Lcom/ibm/icu/text/PluralRules$Constraint;


# static fields
.field private static final serialVersionUID:J = 0x7f2b2ad155970a18L


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isFulfilled(D)Z
    .locals 1
    .param p1, "n"    # D

    .prologue
    .line 150
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    const-string/jumbo v0, "n is any"

    return-object v0
.end method

.method public updateRepeatLimit(I)I
    .locals 0
    .param p1, "limit"    # I

    .prologue
    .line 157
    return p1
.end method
