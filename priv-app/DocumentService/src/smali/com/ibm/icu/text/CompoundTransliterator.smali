.class Lcom/ibm/icu/text/CompoundTransliterator;
.super Lcom/ibm/icu/text/Transliterator;
.source "CompoundTransliterator.java"


# instance fields
.field private numAnonymousRBTs:I

.field private trans:[Lcom/ibm/icu/text/Transliterator;


# direct methods
.method constructor <init>(Ljava/util/Vector;)V
    .locals 1
    .param p1, "list"    # Ljava/util/Vector;

    .prologue
    .line 108
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/CompoundTransliterator;-><init>(Ljava/util/Vector;I)V

    .line 109
    return-void
.end method

.method constructor <init>(Ljava/util/Vector;I)V
    .locals 3
    .param p1, "list"    # Ljava/util/Vector;
    .param p2, "numAnonymousRBTs"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 112
    const-string/jumbo v0, ""

    invoke-direct {p0, v0, v2}, Lcom/ibm/icu/text/Transliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 33
    iput v1, p0, Lcom/ibm/icu/text/CompoundTransliterator;->numAnonymousRBTs:I

    .line 113
    iput-object v2, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    .line 114
    invoke-direct {p0, p1, v1, v1}, Lcom/ibm/icu/text/CompoundTransliterator;->init(Ljava/util/Vector;IZ)V

    .line 115
    iput p2, p0, Lcom/ibm/icu/text/CompoundTransliterator;->numAnonymousRBTs:I

    .line 117
    return-void
.end method

.method private static _smartAppend(Ljava/lang/StringBuffer;C)V
    .locals 1
    .param p0, "buf"    # Ljava/lang/StringBuffer;
    .param p1, "c"    # C

    .prologue
    .line 240
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    if-eq v0, p1, :cond_0

    .line 242
    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 244
    :cond_0
    return-void
.end method

.method private computeMaximumContextLength()V
    .locals 4

    .prologue
    .line 503
    const/4 v2, 0x0

    .line 504
    .local v2, "max":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 505
    iget-object v3, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/ibm/icu/text/Transliterator;->getMaximumContextLength()I

    move-result v1

    .line 506
    .local v1, "len":I
    if-le v1, v2, :cond_0

    .line 507
    move v2, v1

    .line 504
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 510
    .end local v1    # "len":I
    :cond_1
    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/CompoundTransliterator;->setMaximumContextLength(I)V

    .line 511
    return-void
.end method

.method private init(Ljava/util/Vector;IZ)V
    .locals 6
    .param p1, "list"    # Ljava/util/Vector;
    .param p2, "direction"    # I
    .param p3, "fixReverseID"    # Z

    .prologue
    .line 174
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v0

    .line 175
    .local v0, "count":I
    new-array v4, v0, [Lcom/ibm/icu/text/Transliterator;

    iput-object v4, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    .line 180
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 181
    if-nez p2, :cond_0

    move v2, v1

    .line 182
    .local v2, "j":I
    :goto_1
    iget-object v5, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    invoke-virtual {p1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/text/Transliterator;

    aput-object v4, v5, v1

    .line 180
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 181
    .end local v2    # "j":I
    :cond_0
    add-int/lit8 v4, v0, -0x1

    sub-int v2, v4, v1

    goto :goto_1

    .line 187
    :cond_1
    const/4 v4, 0x1

    if-ne p2, v4, :cond_4

    if-eqz p3, :cond_4

    .line 188
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 189
    .local v3, "newID":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v0, :cond_3

    .line 190
    if-lez v1, :cond_2

    .line 191
    const/16 v4, 0x3b

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 193
    :cond_2
    iget-object v4, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    aget-object v4, v4, v1

    invoke-virtual {v4}, Lcom/ibm/icu/text/Transliterator;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 189
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 195
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/ibm/icu/text/CompoundTransliterator;->setID(Ljava/lang/String;)V

    .line 198
    .end local v3    # "newID":Ljava/lang/StringBuffer;
    :cond_4
    invoke-direct {p0}, Lcom/ibm/icu/text/CompoundTransliterator;->computeMaximumContextLength()V

    .line 199
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    array-length v0, v0

    return v0
.end method

.method public getTargetSet()Lcom/ibm/icu/text/UnicodeSet;
    .locals 3

    .prologue
    .line 330
    new-instance v1, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 331
    .local v1, "set":Lcom/ibm/icu/text/UnicodeSet;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 333
    iget-object v2, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/ibm/icu/text/Transliterator;->getTargetSet()Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    .line 331
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 335
    :cond_0
    return-object v1
.end method

.method public getTransliterator(I)Lcom/ibm/icu/text/Transliterator;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 233
    iget-object v0, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    aget-object v0, v0, p1

    return-object v0
.end method

.method protected handleGetSourceSet()Lcom/ibm/icu/text/UnicodeSet;
    .locals 3

    .prologue
    .line 306
    new-instance v1, Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v1}, Lcom/ibm/icu/text/UnicodeSet;-><init>()V

    .line 307
    .local v1, "set":Lcom/ibm/icu/text/UnicodeSet;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 308
    iget-object v2, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/ibm/icu/text/Transliterator;->getSourceSet()Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/ibm/icu/text/UnicodeSet;->addAll(Lcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/UnicodeSet;

    .line 317
    invoke-virtual {v1}, Lcom/ibm/icu/text/UnicodeSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 321
    :cond_0
    return-object v1

    .line 307
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V
    .locals 9
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "index"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "incremental"    # Z

    .prologue
    .line 399
    iget-object v6, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    array-length v6, v6

    const/4 v7, 0x1

    if-ge v6, v7, :cond_0

    .line 400
    iget v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    iput v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 496
    :goto_0
    return-void

    .line 408
    :cond_0
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 412
    .local v0, "compoundLimit":I
    iget v1, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 414
    .local v1, "compoundStart":I
    const/4 v2, 0x0

    .line 416
    .local v2, "delta":I
    const/4 v5, 0x0

    .line 426
    .local v5, "log":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v6, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    array-length v6, v6

    if-ge v3, v6, :cond_1

    .line 427
    iput v1, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 428
    iget v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 430
    .local v4, "limit":I
    iget v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    iget v7, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    if-ne v6, v7, :cond_2

    .line 482
    .end local v4    # "limit":I
    :cond_1
    add-int/2addr v0, v2

    .line 487
    iput v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    goto :goto_0

    .line 450
    .restart local v4    # "limit":I
    :cond_2
    iget-object v6, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    aget-object v6, v6, v3

    invoke-virtual {v6, p1, p2, p3}, Lcom/ibm/icu/text/Transliterator;->filteredTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V

    .line 459
    if-nez p3, :cond_3

    iget v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    iget v7, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    if-eq v6, v7, :cond_3

    .line 460
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v8, "ERROR: Incomplete non-incremental transliteration by "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget-object v8, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    aget-object v8, v8, v3

    invoke-virtual {v8}, Lcom/ibm/icu/text/Transliterator;->getID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 470
    :cond_3
    iget v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    sub-int/2addr v6, v4

    add-int/2addr v2, v6

    .line 472
    if-eqz p3, :cond_4

    .line 478
    iget v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    iput v6, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 426
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public toRules(Z)Ljava/lang/String;
    .locals 7
    .param p1, "escapeUnprintable"    # Z

    .prologue
    const/4 v6, 0x1

    const/16 v5, 0x3b

    .line 264
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 265
    .local v2, "rulesSource":Ljava/lang/StringBuffer;
    iget v3, p0, Lcom/ibm/icu/text/CompoundTransliterator;->numAnonymousRBTs:I

    if-lt v3, v6, :cond_0

    invoke-virtual {p0}, Lcom/ibm/icu/text/CompoundTransliterator;->getFilter()Lcom/ibm/icu/text/UnicodeFilter;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 268
    const-string/jumbo v3, "::"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p0}, Lcom/ibm/icu/text/CompoundTransliterator;->getFilter()Lcom/ibm/icu/text/UnicodeFilter;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/ibm/icu/text/UnicodeFilter;->toPattern(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 270
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 277
    iget-object v3, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/ibm/icu/text/Transliterator;->getID()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "%Pass"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 278
    iget-object v3, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    aget-object v3, v3, v0

    invoke-virtual {v3, p1}, Lcom/ibm/icu/text/Transliterator;->toRules(Z)Ljava/lang/String;

    move-result-object v1

    .line 279
    .local v1, "rule":Ljava/lang/String;
    iget v3, p0, Lcom/ibm/icu/text/CompoundTransliterator;->numAnonymousRBTs:I

    if-le v3, v6, :cond_1

    if-lez v0, :cond_1

    iget-object v3, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    add-int/lit8 v4, v0, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/ibm/icu/text/Transliterator;->getID()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "%Pass"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 280
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "::Null;"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 293
    :cond_1
    :goto_1
    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/ibm/icu/text/CompoundTransliterator;->_smartAppend(Ljava/lang/StringBuffer;C)V

    .line 294
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 295
    invoke-static {v2, v5}, Lcom/ibm/icu/text/CompoundTransliterator;->_smartAppend(Ljava/lang/StringBuffer;C)V

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 286
    .end local v1    # "rule":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/ibm/icu/text/Transliterator;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-ltz v3, :cond_3

    .line 287
    iget-object v3, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    aget-object v3, v3, v0

    invoke-virtual {v3, p1}, Lcom/ibm/icu/text/Transliterator;->toRules(Z)Ljava/lang/String;

    move-result-object v1

    .line 290
    .restart local v1    # "rule":Ljava/lang/String;
    goto :goto_1

    .line 291
    .end local v1    # "rule":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/ibm/icu/text/CompoundTransliterator;->trans:[Lcom/ibm/icu/text/Transliterator;

    aget-object v3, v3, v0

    invoke-virtual {v3, p1}, Lcom/ibm/icu/text/Transliterator;->baseToRules(Z)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "rule":Ljava/lang/String;
    goto :goto_1

    .line 297
    .end local v1    # "rule":Ljava/lang/String;
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
