.class Lcom/ibm/icu/text/StringReplacer;
.super Ljava/lang/Object;
.source "StringReplacer.java"

# interfaces
.implements Lcom/ibm/icu/text/UnicodeReplacer;


# instance fields
.field private cursorPos:I

.field private final data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

.field private hasCursor:Z

.field private isComplex:Z

.field private output:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILcom/ibm/icu/text/RuleBasedTransliterator$Data;)V
    .locals 1
    .param p1, "theOutput"    # Ljava/lang/String;
    .param p2, "theCursorPos"    # I
    .param p3, "theData"    # Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .prologue
    const/4 v0, 0x1

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    .line 71
    iput p2, p0, Lcom/ibm/icu/text/StringReplacer;->cursorPos:I

    .line 72
    iput-boolean v0, p0, Lcom/ibm/icu/text/StringReplacer;->hasCursor:Z

    .line 73
    iput-object p3, p0, Lcom/ibm/icu/text/StringReplacer;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .line 74
    iput-boolean v0, p0, Lcom/ibm/icu/text/StringReplacer;->isComplex:Z

    .line 75
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/ibm/icu/text/RuleBasedTransliterator$Data;)V
    .locals 1
    .param p1, "theOutput"    # Ljava/lang/String;
    .param p2, "theData"    # Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .prologue
    const/4 v0, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    .line 89
    iput v0, p0, Lcom/ibm/icu/text/StringReplacer;->cursorPos:I

    .line 90
    iput-boolean v0, p0, Lcom/ibm/icu/text/StringReplacer;->hasCursor:Z

    .line 91
    iput-object p2, p0, Lcom/ibm/icu/text/StringReplacer;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/StringReplacer;->isComplex:Z

    .line 93
    return-void
.end method


# virtual methods
.method public addReplacementSetTo(Lcom/ibm/icu/text/UnicodeSet;)V
    .locals 4
    .param p1, "toUnionTo"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 321
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 322
    iget-object v3, p0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 323
    .local v0, "ch":I
    iget-object v3, p0, Lcom/ibm/icu/text/StringReplacer;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-virtual {v3, v0}, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->lookupReplacer(I)Lcom/ibm/icu/text/UnicodeReplacer;

    move-result-object v2

    .line 324
    .local v2, "r":Lcom/ibm/icu/text/UnicodeReplacer;
    if-nez v2, :cond_0

    .line 325
    invoke-virtual {p1, v0}, Lcom/ibm/icu/text/UnicodeSet;->add(I)Lcom/ibm/icu/text/UnicodeSet;

    .line 321
    :goto_1
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_0

    .line 327
    :cond_0
    invoke-interface {v2, p1}, Lcom/ibm/icu/text/UnicodeReplacer;->addReplacementSetTo(Lcom/ibm/icu/text/UnicodeSet;)V

    goto :goto_1

    .line 330
    .end local v0    # "ch":I
    .end local v2    # "r":Lcom/ibm/icu/text/UnicodeReplacer;
    :cond_1
    return-void
.end method

.method public replace(Lcom/ibm/icu/text/Replaceable;II[I)I
    .locals 20
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "start"    # I
    .param p3, "limit"    # I
    .param p4, "cursor"    # [I

    .prologue
    .line 116
    const/4 v10, 0x0

    .line 123
    .local v10, "newStart":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/StringReplacer;->isComplex:Z

    move/from16 v17, v0

    if-nez v17, :cond_0

    .line 124
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    move-object/from16 v3, v17

    invoke-interface {v0, v1, v2, v3}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v13

    .line 128
    .local v13, "outLen":I
    move-object/from16 v0, p0

    iget v10, v0, Lcom/ibm/icu/text/StringReplacer;->cursorPos:I

    .line 227
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/StringReplacer;->hasCursor:Z

    move/from16 v17, v0

    if-eqz v17, :cond_a

    .line 232
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/StringReplacer;->cursorPos:I

    move/from16 v17, v0

    if-gez v17, :cond_b

    .line 233
    move/from16 v10, p2

    .line 234
    move-object/from16 v0, p0

    iget v9, v0, Lcom/ibm/icu/text/StringReplacer;->cursorPos:I

    .line 236
    .local v9, "n":I
    :goto_1
    if-gez v9, :cond_9

    if-lez v10, :cond_9

    .line 237
    add-int/lit8 v17, v10, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v17

    sub-int v10, v10, v17

    .line 238
    add-int/lit8 v9, v9, 0x1

    .line 239
    goto :goto_1

    .line 139
    .end local v9    # "n":I
    .end local v13    # "outLen":I
    :cond_0
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 141
    .local v4, "buf":Ljava/lang/StringBuffer;
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/ibm/icu/text/StringReplacer;->isComplex:Z

    .line 152
    invoke-interface/range {p1 .. p1}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v16

    .line 153
    .local v16, "tempStart":I
    move/from16 v7, v16

    .line 154
    .local v7, "destStart":I
    if-lez p2, :cond_3

    .line 155
    add-int/lit8 v17, p2, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v8

    .line 156
    .local v8, "len":I
    sub-int v17, p2, v8

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, p2

    move/from16 v3, v16

    invoke-interface {v0, v1, v2, v3}, Lcom/ibm/icu/text/Replaceable;->copy(III)V

    .line 157
    add-int/2addr v7, v8

    .line 162
    .end local v8    # "len":I
    :goto_2
    move v6, v7

    .line 163
    .local v6, "destLimit":I
    const/4 v15, 0x0

    .line 165
    .local v15, "tempExtra":I
    const/4 v12, 0x0

    .local v12, "oOutput":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v12, v0, :cond_6

    .line 166
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/StringReplacer;->cursorPos:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v12, v0, :cond_1

    .line 168
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    add-int v17, v17, v6

    sub-int v10, v17, v7

    .line 175
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v0, v12}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v5

    .line 181
    .local v5, "c":I
    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v17

    add-int v11, v12, v17

    .line 182
    .local v11, "nextIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    move/from16 v0, v17

    if-ne v11, v0, :cond_2

    .line 183
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-interface {v0, v1}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v15

    .line 184
    add-int v17, p3, v15

    move-object/from16 v0, p1

    move/from16 v1, p3

    move/from16 v2, v17

    invoke-interface {v0, v1, v2, v6}, Lcom/ibm/icu/text/Replaceable;->copy(III)V

    .line 187
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/StringReplacer;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->lookupReplacer(I)Lcom/ibm/icu/text/UnicodeReplacer;

    move-result-object v14

    .line 188
    .local v14, "r":Lcom/ibm/icu/text/UnicodeReplacer;
    if-nez v14, :cond_4

    .line 190
    invoke-static {v4, v5}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 205
    :goto_4
    move v12, v11

    .line 206
    goto :goto_3

    .line 159
    .end local v5    # "c":I
    .end local v6    # "destLimit":I
    .end local v11    # "nextIndex":I
    .end local v12    # "oOutput":I
    .end local v14    # "r":Lcom/ibm/icu/text/UnicodeReplacer;
    .end local v15    # "tempExtra":I
    :cond_3
    const-string/jumbo v17, "\uffff"

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v16

    move-object/from16 v3, v17

    invoke-interface {v0, v1, v2, v3}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 160
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 192
    .restart local v5    # "c":I
    .restart local v6    # "destLimit":I
    .restart local v11    # "nextIndex":I
    .restart local v12    # "oOutput":I
    .restart local v14    # "r":Lcom/ibm/icu/text/UnicodeReplacer;
    .restart local v15    # "tempExtra":I
    :cond_4
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/ibm/icu/text/StringReplacer;->isComplex:Z

    .line 195
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    if-lez v17, :cond_5

    .line 196
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v6, v6, v1}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 197
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    add-int v6, v6, v17

    .line 198
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 202
    :cond_5
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-interface {v14, v0, v6, v6, v1}, Lcom/ibm/icu/text/UnicodeReplacer;->replace(Lcom/ibm/icu/text/Replaceable;II[I)I

    move-result v8

    .line 203
    .restart local v8    # "len":I
    add-int/2addr v6, v8

    goto :goto_4

    .line 208
    .end local v5    # "c":I
    .end local v8    # "len":I
    .end local v11    # "nextIndex":I
    .end local v14    # "r":Lcom/ibm/icu/text/UnicodeReplacer;
    :cond_6
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    if-lez v17, :cond_7

    .line 209
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v6, v6, v1}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 210
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v17

    add-int v6, v6, v17

    .line 212
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/StringReplacer;->cursorPos:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v12, v0, :cond_8

    .line 214
    sub-int v10, v6, v7

    .line 217
    :cond_8
    sub-int v13, v6, v7

    .line 220
    .restart local v13    # "outLen":I
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-interface {v0, v7, v6, v1}, Lcom/ibm/icu/text/Replaceable;->copy(III)V

    .line 221
    add-int v17, v16, v13

    add-int v18, v6, v15

    add-int v18, v18, v13

    const-string/jumbo v19, ""

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    move-object/from16 v3, v19

    invoke-interface {v0, v1, v2, v3}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 224
    add-int v17, p2, v13

    add-int v18, p3, v13

    const-string/jumbo v19, ""

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    move-object/from16 v3, v19

    invoke-interface {v0, v1, v2, v3}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 240
    .end local v4    # "buf":Ljava/lang/StringBuffer;
    .end local v6    # "destLimit":I
    .end local v7    # "destStart":I
    .end local v12    # "oOutput":I
    .end local v15    # "tempExtra":I
    .end local v16    # "tempStart":I
    .restart local v9    # "n":I
    :cond_9
    add-int/2addr v10, v9

    .line 256
    .end local v9    # "n":I
    :goto_5
    const/16 v17, 0x0

    aput v10, p4, v17

    .line 259
    :cond_a
    return v13

    .line 241
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/StringReplacer;->cursorPos:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_d

    .line 242
    add-int v10, p2, v13

    .line 243
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/StringReplacer;->cursorPos:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    sub-int v9, v17, v18

    .line 245
    .restart local v9    # "n":I
    :goto_6
    if-lez v9, :cond_c

    invoke-interface/range {p1 .. p1}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v10, v0, :cond_c

    .line 246
    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v17

    add-int v10, v10, v17

    .line 247
    add-int/lit8 v9, v9, -0x1

    .line 248
    goto :goto_6

    .line 249
    :cond_c
    add-int/2addr v10, v9

    .line 250
    goto :goto_5

    .line 253
    .end local v9    # "n":I
    :cond_d
    add-int v10, v10, p2

    goto :goto_5
.end method

.method public toReplacerPattern(Z)Ljava/lang/String;
    .locals 12
    .param p1, "escapeUnprintable"    # Z

    .prologue
    const/16 v11, 0x7c

    const/16 v10, 0x40

    const/4 v9, 0x1

    .line 266
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    .line 267
    .local v7, "rule":Ljava/lang/StringBuffer;
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 269
    .local v5, "quoteBuf":Ljava/lang/StringBuffer;
    iget v2, p0, Lcom/ibm/icu/text/StringReplacer;->cursorPos:I

    .line 272
    .local v2, "cursor":I
    iget-boolean v8, p0, Lcom/ibm/icu/text/StringReplacer;->hasCursor:Z

    if-eqz v8, :cond_0

    if-gez v2, :cond_0

    move v3, v2

    .line 273
    .end local v2    # "cursor":I
    .local v3, "cursor":I
    :goto_0
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "cursor":I
    .restart local v2    # "cursor":I
    if-gez v3, :cond_0

    .line 274
    invoke-static {v7, v10, v9, p1, v5}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;IZZLjava/lang/StringBuffer;)V

    move v3, v2

    .line 275
    .end local v2    # "cursor":I
    .restart local v3    # "cursor":I
    goto :goto_0

    .line 279
    .end local v3    # "cursor":I
    .restart local v2    # "cursor":I
    :cond_0
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v8, p0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v4, v8, :cond_3

    .line 280
    iget-boolean v8, p0, Lcom/ibm/icu/text/StringReplacer;->hasCursor:Z

    if-eqz v8, :cond_1

    if-ne v4, v2, :cond_1

    .line 281
    invoke-static {v7, v11, v9, p1, v5}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;IZZLjava/lang/StringBuffer;)V

    .line 283
    :cond_1
    iget-object v8, p0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    invoke-virtual {v8, v4}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 285
    .local v1, "c":C
    iget-object v8, p0, Lcom/ibm/icu/text/StringReplacer;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    invoke-virtual {v8, v1}, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->lookupReplacer(I)Lcom/ibm/icu/text/UnicodeReplacer;

    move-result-object v6

    .line 286
    .local v6, "r":Lcom/ibm/icu/text/UnicodeReplacer;
    if-nez v6, :cond_2

    .line 287
    const/4 v8, 0x0

    invoke-static {v7, v1, v8, p1, v5}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;IZZLjava/lang/StringBuffer;)V

    .line 279
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 289
    :cond_2
    new-instance v0, Ljava/lang/StringBuffer;

    const-string/jumbo v8, " "

    invoke-direct {v0, v8}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 290
    .local v0, "buf":Ljava/lang/StringBuffer;
    invoke-interface {v6, p1}, Lcom/ibm/icu/text/UnicodeReplacer;->toReplacerPattern(Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 291
    const/16 v8, 0x20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 292
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v9, p1, v5}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;Ljava/lang/String;ZZLjava/lang/StringBuffer;)V

    goto :goto_2

    .line 300
    .end local v0    # "buf":Ljava/lang/StringBuffer;
    .end local v1    # "c":C
    .end local v6    # "r":Lcom/ibm/icu/text/UnicodeReplacer;
    :cond_3
    iget-boolean v8, p0, Lcom/ibm/icu/text/StringReplacer;->hasCursor:Z

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-le v2, v8, :cond_5

    .line 301
    iget-object v8, p0, Lcom/ibm/icu/text/StringReplacer;->output:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    sub-int/2addr v2, v8

    move v3, v2

    .line 302
    .end local v2    # "cursor":I
    .restart local v3    # "cursor":I
    :goto_3
    add-int/lit8 v2, v3, -0x1

    .end local v3    # "cursor":I
    .restart local v2    # "cursor":I
    if-lez v3, :cond_4

    .line 303
    invoke-static {v7, v10, v9, p1, v5}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;IZZLjava/lang/StringBuffer;)V

    move v3, v2

    .line 304
    .end local v2    # "cursor":I
    .restart local v3    # "cursor":I
    goto :goto_3

    .line 305
    .end local v3    # "cursor":I
    .restart local v2    # "cursor":I
    :cond_4
    invoke-static {v7, v11, v9, p1, v5}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;IZZLjava/lang/StringBuffer;)V

    .line 308
    :cond_5
    const/4 v8, -0x1

    invoke-static {v7, v8, v9, p1, v5}, Lcom/ibm/icu/impl/Utility;->appendToRule(Ljava/lang/StringBuffer;IZZLjava/lang/StringBuffer;)V

    .line 311
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method
