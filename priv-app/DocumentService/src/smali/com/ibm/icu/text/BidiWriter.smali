.class final Lcom/ibm/icu/text/BidiWriter;
.super Ljava/lang/Object;
.source "BidiWriter.java"


# static fields
.field static final LRM_CHAR:C = '\u200e'

.field static final MASK_R_AL:I = 0x2002

.field static final RLM_CHAR:C = '\u200f'


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static IsCombining(I)Z
    .locals 2
    .param p0, "type"    # I

    .prologue
    const/4 v0, 0x1

    .line 26
    shl-int v1, v0, p0

    and-int/lit16 v1, v1, 0x1c0

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static doWriteForward(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p0, "src"    # Ljava/lang/String;
    .param p1, "options"    # I

    .prologue
    .line 43
    and-int/lit8 v4, p1, 0xa

    sparse-switch v4, :sswitch_data_0

    .line 77
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 80
    .local v1, "dest":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .line 83
    .local v2, "i":I
    :cond_0
    invoke-static {p0, v2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 84
    .local v0, "c":I
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v4

    add-int/2addr v2, v4

    .line 85
    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->IsBidiControlChar(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 86
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->getMirror(I)I

    move-result v4

    invoke-static {v1, v4}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 88
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v2, v4, :cond_0

    .line 89
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .end local v0    # "c":I
    .end local v1    # "dest":Ljava/lang/StringBuffer;
    .end local v2    # "i":I
    :goto_0
    return-object v4

    .line 46
    :sswitch_0
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 52
    .restart local v1    # "dest":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .line 56
    .restart local v2    # "i":I
    :cond_2
    invoke-static {p0, v2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 57
    .restart local v0    # "c":I
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v4

    add-int/2addr v2, v4

    .line 58
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->getMirror(I)I

    move-result v4

    invoke-static {v1, v4}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 59
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v2, v4, :cond_2

    .line 60
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 63
    .end local v0    # "c":I
    .end local v1    # "dest":Ljava/lang/StringBuffer;
    .end local v2    # "i":I
    :sswitch_2
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 66
    .restart local v1    # "dest":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .line 69
    .restart local v2    # "i":I
    :goto_1
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 70
    .local v0, "c":C
    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->IsBidiControlChar(I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 71
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 73
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v3, v4, :cond_4

    .line 74
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_4
    move v2, v3

    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_1

    .line 43
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method private static doWriteForward([CIII)Ljava/lang/String;
    .locals 2
    .param p0, "text"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "options"    # I

    .prologue
    .line 97
    new-instance v0, Ljava/lang/String;

    sub-int v1, p2, p1

    invoke-direct {v0, p0, p1, v1}, Ljava/lang/String;-><init>([CII)V

    invoke-static {v0, p3}, Lcom/ibm/icu/text/BidiWriter;->doWriteForward(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static doWriteReverse([CIII)Ljava/lang/String;
    .locals 2
    .param p0, "text"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "options"    # I

    .prologue
    .line 234
    new-instance v0, Ljava/lang/String;

    sub-int v1, p2, p1

    invoke-direct {v0, p0, p1, v1}, Ljava/lang/String;-><init>([CII)V

    invoke-static {v0, p3}, Lcom/ibm/icu/text/BidiWriter;->writeReverse(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static writeReordered(Lcom/ibm/icu/text/Bidi;I)Ljava/lang/String;
    .locals 13
    .param p0, "bidi"    # Lcom/ibm/icu/text/Bidi;
    .param p1, "options"    # I

    .prologue
    const/16 v12, 0x200f

    const/16 v11, 0x200e

    .line 241
    iget-object v6, p0, Lcom/ibm/icu/text/Bidi;->text:[C

    .line 242
    .local v6, "text":[C
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->countRuns()I

    move-result v5

    .line 248
    .local v5, "runCount":I
    iget v8, p0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_0

    .line 249
    or-int/lit8 p1, p1, 0x4

    .line 250
    and-int/lit8 p1, p1, -0x9

    .line 256
    :cond_0
    iget v8, p0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    and-int/lit8 v8, v8, 0x2

    if-eqz v8, :cond_1

    .line 257
    or-int/lit8 p1, p1, 0x8

    .line 258
    and-int/lit8 p1, p1, -0x5

    .line 264
    :cond_1
    iget v8, p0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    const/4 v9, 0x4

    if-eq v8, v9, :cond_2

    iget v8, p0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    const/4 v9, 0x5

    if-eq v8, v9, :cond_2

    iget v8, p0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    const/4 v9, 0x6

    if-eq v8, v9, :cond_2

    iget v8, p0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    const/4 v9, 0x3

    if-eq v8, v9, :cond_2

    .line 268
    and-int/lit8 p1, p1, -0x5

    .line 270
    :cond_2
    new-instance v1, Ljava/lang/StringBuffer;

    and-int/lit8 v8, p1, 0x4

    if-eqz v8, :cond_3

    iget v8, p0, Lcom/ibm/icu/text/Bidi;->length:I

    mul-int/lit8 v8, v8, 0x2

    :goto_0
    invoke-direct {v1, v8}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 281
    .local v1, "dest":Ljava/lang/StringBuffer;
    and-int/lit8 v8, p1, 0x10

    if-nez v8, :cond_17

    .line 283
    and-int/lit8 v8, p1, 0x4

    if-nez v8, :cond_5

    .line 285
    const/4 v4, 0x0

    .local v4, "run":I
    :goto_1
    if-ge v4, v5, :cond_1e

    .line 286
    invoke-virtual {p0, v4}, Lcom/ibm/icu/text/Bidi;->getVisualRun(I)Lcom/ibm/icu/text/BidiRun;

    move-result-object v0

    .line 287
    .local v0, "bidiRun":Lcom/ibm/icu/text/BidiRun;
    invoke-virtual {v0}, Lcom/ibm/icu/text/BidiRun;->isEvenRun()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 288
    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    iget v9, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    and-int/lit8 v10, p1, -0x3

    invoke-static {v6, v8, v9, v10}, Lcom/ibm/icu/text/BidiWriter;->doWriteForward([CIII)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 285
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 270
    .end local v0    # "bidiRun":Lcom/ibm/icu/text/BidiRun;
    .end local v1    # "dest":Ljava/lang/StringBuffer;
    .end local v4    # "run":I
    :cond_3
    iget v8, p0, Lcom/ibm/icu/text/Bidi;->length:I

    goto :goto_0

    .line 292
    .restart local v0    # "bidiRun":Lcom/ibm/icu/text/BidiRun;
    .restart local v1    # "dest":Ljava/lang/StringBuffer;
    .restart local v4    # "run":I
    :cond_4
    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    iget v9, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    invoke-static {v6, v8, v9, p1}, Lcom/ibm/icu/text/BidiWriter;->doWriteReverse([CIII)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 298
    .end local v0    # "bidiRun":Lcom/ibm/icu/text/BidiRun;
    .end local v4    # "run":I
    :cond_5
    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    .line 302
    .local v2, "dirProps":[B
    const/4 v4, 0x0

    .restart local v4    # "run":I
    :goto_3
    if-ge v4, v5, :cond_1e

    .line 303
    invoke-virtual {p0, v4}, Lcom/ibm/icu/text/Bidi;->getVisualRun(I)Lcom/ibm/icu/text/BidiRun;

    move-result-object v0

    .line 304
    .restart local v0    # "bidiRun":Lcom/ibm/icu/text/BidiRun;
    const/4 v3, 0x0

    .line 306
    .local v3, "markFlag":I
    iget-object v8, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    aget-object v8, v8, v4

    iget v3, v8, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 307
    if-gez v3, :cond_6

    .line 308
    const/4 v3, 0x0

    .line 310
    :cond_6
    invoke-virtual {v0}, Lcom/ibm/icu/text/BidiRun;->isEvenRun()Z

    move-result v8

    if-eqz v8, :cond_f

    .line 311
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->isInverse()Z

    move-result v8

    if-eqz v8, :cond_7

    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    aget-byte v8, v2, v8

    if-eqz v8, :cond_7

    .line 313
    or-int/lit8 v3, v3, 0x1

    .line 315
    :cond_7
    and-int/lit8 v8, v3, 0x1

    if-eqz v8, :cond_b

    .line 316
    const/16 v7, 0x200e

    .line 322
    .local v7, "uc":C
    :goto_4
    if-eqz v7, :cond_8

    .line 323
    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 325
    :cond_8
    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    iget v9, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    and-int/lit8 v10, p1, -0x3

    invoke-static {v6, v8, v9, v10}, Lcom/ibm/icu/text/BidiWriter;->doWriteForward([CIII)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 329
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->isInverse()Z

    move-result v8

    if-eqz v8, :cond_9

    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    add-int/lit8 v8, v8, -0x1

    aget-byte v8, v2, v8

    if-eqz v8, :cond_9

    .line 331
    or-int/lit8 v3, v3, 0x2

    .line 333
    :cond_9
    and-int/lit8 v8, v3, 0x2

    if-eqz v8, :cond_d

    .line 334
    const/16 v7, 0x200e

    .line 340
    :goto_5
    if-eqz v7, :cond_a

    .line 341
    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 302
    :cond_a
    :goto_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 317
    .end local v7    # "uc":C
    :cond_b
    and-int/lit8 v8, v3, 0x4

    if-eqz v8, :cond_c

    .line 318
    const/16 v7, 0x200f

    .line 319
    .restart local v7    # "uc":C
    goto :goto_4

    .line 320
    .end local v7    # "uc":C
    :cond_c
    const/4 v7, 0x0

    .restart local v7    # "uc":C
    goto :goto_4

    .line 335
    :cond_d
    and-int/lit8 v8, v3, 0x8

    if-eqz v8, :cond_e

    .line 336
    const/16 v7, 0x200f

    .line 337
    goto :goto_5

    .line 338
    :cond_e
    const/4 v7, 0x0

    goto :goto_5

    .line 344
    .end local v7    # "uc":C
    :cond_f
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->isInverse()Z

    move-result v8

    if-eqz v8, :cond_10

    const/16 v8, 0x2002

    iget v9, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {p0, v8, v9}, Lcom/ibm/icu/text/Bidi;->testDirPropFlagAt(II)Z

    move-result v8

    if-nez v8, :cond_10

    .line 347
    or-int/lit8 v3, v3, 0x4

    .line 349
    :cond_10
    and-int/lit8 v8, v3, 0x1

    if-eqz v8, :cond_13

    .line 350
    const/16 v7, 0x200e

    .line 356
    .restart local v7    # "uc":C
    :goto_7
    if-eqz v7, :cond_11

    .line 357
    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 359
    :cond_11
    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    iget v9, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    invoke-static {v6, v8, v9, p1}, Lcom/ibm/icu/text/BidiWriter;->doWriteReverse([CIII)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 362
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->isInverse()Z

    move-result v8

    if-eqz v8, :cond_12

    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    aget-byte v8, v2, v8

    invoke-static {v8}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v8

    and-int/lit16 v8, v8, 0x2002

    if-nez v8, :cond_12

    .line 364
    or-int/lit8 v3, v3, 0x8

    .line 366
    :cond_12
    and-int/lit8 v8, v3, 0x2

    if-eqz v8, :cond_15

    .line 367
    const/16 v7, 0x200e

    .line 373
    :goto_8
    if-eqz v7, :cond_a

    .line 374
    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_6

    .line 351
    .end local v7    # "uc":C
    :cond_13
    and-int/lit8 v8, v3, 0x4

    if-eqz v8, :cond_14

    .line 352
    const/16 v7, 0x200f

    .line 353
    .restart local v7    # "uc":C
    goto :goto_7

    .line 354
    .end local v7    # "uc":C
    :cond_14
    const/4 v7, 0x0

    .restart local v7    # "uc":C
    goto :goto_7

    .line 368
    :cond_15
    and-int/lit8 v8, v3, 0x8

    if-eqz v8, :cond_16

    .line 369
    const/16 v7, 0x200f

    .line 370
    goto :goto_8

    .line 371
    :cond_16
    const/4 v7, 0x0

    goto :goto_8

    .line 381
    .end local v0    # "bidiRun":Lcom/ibm/icu/text/BidiRun;
    .end local v2    # "dirProps":[B
    .end local v3    # "markFlag":I
    .end local v4    # "run":I
    .end local v7    # "uc":C
    :cond_17
    and-int/lit8 v8, p1, 0x4

    if-nez v8, :cond_19

    .line 383
    move v4, v5

    .restart local v4    # "run":I
    :goto_9
    add-int/lit8 v4, v4, -0x1

    if-ltz v4, :cond_1e

    .line 384
    invoke-virtual {p0, v4}, Lcom/ibm/icu/text/Bidi;->getVisualRun(I)Lcom/ibm/icu/text/BidiRun;

    move-result-object v0

    .line 385
    .restart local v0    # "bidiRun":Lcom/ibm/icu/text/BidiRun;
    invoke-virtual {v0}, Lcom/ibm/icu/text/BidiRun;->isEvenRun()Z

    move-result v8

    if-eqz v8, :cond_18

    .line 386
    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    iget v9, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    and-int/lit8 v10, p1, -0x3

    invoke-static {v6, v8, v9, v10}, Lcom/ibm/icu/text/BidiWriter;->doWriteReverse([CIII)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_9

    .line 390
    :cond_18
    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    iget v9, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    invoke-static {v6, v8, v9, p1}, Lcom/ibm/icu/text/BidiWriter;->doWriteForward([CIII)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_9

    .line 397
    .end local v0    # "bidiRun":Lcom/ibm/icu/text/BidiRun;
    .end local v4    # "run":I
    :cond_19
    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    .line 399
    .restart local v2    # "dirProps":[B
    move v4, v5

    .restart local v4    # "run":I
    :cond_1a
    :goto_a
    add-int/lit8 v4, v4, -0x1

    if-ltz v4, :cond_1e

    .line 401
    invoke-virtual {p0, v4}, Lcom/ibm/icu/text/Bidi;->getVisualRun(I)Lcom/ibm/icu/text/BidiRun;

    move-result-object v0

    .line 402
    .restart local v0    # "bidiRun":Lcom/ibm/icu/text/BidiRun;
    invoke-virtual {v0}, Lcom/ibm/icu/text/BidiRun;->isEvenRun()Z

    move-result v8

    if-eqz v8, :cond_1c

    .line 403
    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    add-int/lit8 v8, v8, -0x1

    aget-byte v8, v2, v8

    if-eqz v8, :cond_1b

    .line 404
    invoke-virtual {v1, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 407
    :cond_1b
    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    iget v9, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    and-int/lit8 v10, p1, -0x3

    invoke-static {v6, v8, v9, v10}, Lcom/ibm/icu/text/BidiWriter;->doWriteReverse([CIII)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 410
    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    aget-byte v8, v2, v8

    if-eqz v8, :cond_1a

    .line 411
    invoke-virtual {v1, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_a

    .line 414
    :cond_1c
    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    aget-byte v8, v2, v8

    invoke-static {v8}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v8

    and-int/lit16 v8, v8, 0x2002

    if-nez v8, :cond_1d

    .line 415
    invoke-virtual {v1, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 418
    :cond_1d
    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    iget v9, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    invoke-static {v6, v8, v9, p1}, Lcom/ibm/icu/text/BidiWriter;->doWriteForward([CIII)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 421
    iget v8, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    add-int/lit8 v8, v8, -0x1

    aget-byte v8, v2, v8

    invoke-static {v8}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v8

    and-int/lit16 v8, v8, 0x2002

    if-nez v8, :cond_1a

    .line 422
    invoke-virtual {v1, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_a

    .line 429
    .end local v0    # "bidiRun":Lcom/ibm/icu/text/BidiRun;
    .end local v2    # "dirProps":[B
    :cond_1e
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method static writeReverse(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p0, "src"    # Ljava/lang/String;
    .param p1, "options"    # I

    .prologue
    .line 119
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-direct {v1, v5}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 122
    .local v1, "dest":Ljava/lang/StringBuffer;
    and-int/lit8 v5, p1, 0xb

    packed-switch v5, :pswitch_data_0

    .line 191
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 197
    .local v4, "srcLength":I
    :cond_0
    move v2, v4

    .line 200
    .local v2, "i":I
    add-int/lit8 v5, v4, -0x1

    invoke-static {p0, v5}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 201
    .local v0, "c":I
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v5

    sub-int/2addr v4, v5

    .line 202
    and-int/lit8 v5, p1, 0x1

    if-eqz v5, :cond_5

    .line 204
    :goto_0
    if-lez v4, :cond_5

    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v5

    invoke-static {v5}, Lcom/ibm/icu/text/BidiWriter;->IsCombining(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 205
    add-int/lit8 v5, v4, -0x1

    invoke-static {p0, v5}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 206
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v5

    sub-int/2addr v4, v5

    .line 207
    goto :goto_0

    .line 137
    .end local v0    # "c":I
    .end local v2    # "i":I
    .end local v4    # "srcLength":I
    :pswitch_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 143
    .restart local v4    # "srcLength":I
    :cond_1
    move v2, v4

    .line 146
    .restart local v2    # "i":I
    add-int/lit8 v5, v4, -0x1

    invoke-static {p0, v5}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v5

    sub-int/2addr v4, v5

    .line 150
    invoke-virtual {p0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 151
    if-gtz v4, :cond_1

    .line 229
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 162
    .end local v2    # "i":I
    .end local v4    # "srcLength":I
    :pswitch_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 169
    .restart local v4    # "srcLength":I
    :cond_2
    move v2, v4

    .line 174
    .restart local v2    # "i":I
    :cond_3
    add-int/lit8 v5, v4, -0x1

    invoke-static {p0, v5}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 175
    .restart local v0    # "c":I
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v5

    sub-int/2addr v4, v5

    .line 176
    if-lez v4, :cond_4

    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->getType(I)I

    move-result v5

    invoke-static {v5}, Lcom/ibm/icu/text/BidiWriter;->IsCombining(I)Z

    move-result v5

    if-nez v5, :cond_3

    .line 179
    :cond_4
    invoke-virtual {p0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 180
    if-gtz v4, :cond_2

    goto :goto_1

    .line 210
    :cond_5
    and-int/lit8 v5, p1, 0x8

    if-eqz v5, :cond_6

    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->IsBidiControlChar(I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 225
    :goto_2
    if-gtz v4, :cond_0

    goto :goto_1

    .line 217
    :cond_6
    move v3, v4

    .line 218
    .local v3, "j":I
    and-int/lit8 v5, p1, 0x2

    if-eqz v5, :cond_7

    .line 220
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->getMirror(I)I

    move-result v0

    .line 221
    invoke-static {v1, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    .line 222
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v5

    add-int/2addr v3, v5

    .line 224
    :cond_7
    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 122
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
