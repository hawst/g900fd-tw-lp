.class public final Lcom/ibm/icu/text/RawCollationKey;
.super Lcom/ibm/icu/util/ByteArrayWrapper;
.source "RawCollationKey.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/ibm/icu/util/ByteArrayWrapper;-><init>()V

    .line 52
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/ibm/icu/util/ByteArrayWrapper;-><init>()V

    .line 62
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/ibm/icu/text/RawCollationKey;->bytes:[B

    .line 63
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "bytes"    # [B

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/ibm/icu/util/ByteArrayWrapper;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/ibm/icu/text/RawCollationKey;->bytes:[B

    .line 74
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0
    .param p1, "bytesToAdopt"    # [B
    .param p2, "size"    # I

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/util/ByteArrayWrapper;-><init>([BI)V

    .line 87
    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 2
    .param p1, "rhs"    # Ljava/lang/Object;

    .prologue
    .line 99
    check-cast p1, Lcom/ibm/icu/text/RawCollationKey;

    .end local p1    # "rhs":Ljava/lang/Object;
    invoke-super {p0, p1}, Lcom/ibm/icu/util/ByteArrayWrapper;->compareTo(Ljava/lang/Object;)I

    move-result v0

    .line 100
    .local v0, "result":I
    if-gez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    if-nez v0, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method
