.class Lcom/ibm/icu/text/TransliteratorRegistry$Spec;
.super Ljava/lang/Object;
.source "TransliteratorRegistry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/TransliteratorRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Spec"
.end annotation


# instance fields
.field private isNextLocale:Z

.field private isSpecLocale:Z

.field private nextSpec:Ljava/lang/String;

.field private res:Lcom/ibm/icu/impl/ICUResourceBundle;

.field private scriptName:Ljava/lang/String;

.field private spec:Ljava/lang/String;

.field private top:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 7
    .param p1, "theSpec"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->top:Ljava/lang/String;

    .line 98
    iput-object v6, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->spec:Ljava/lang/String;

    .line 99
    iput-object v6, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->scriptName:Ljava/lang/String;

    .line 103
    :try_start_0
    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->top:Ljava/lang/String;

    invoke-static {v4}, Lcom/ibm/icu/lang/UScript;->getCodeFromName(Ljava/lang/String;)I

    move-result v2

    .line 106
    .local v2, "script":I
    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->top:Ljava/lang/String;

    invoke-static {v4}, Lcom/ibm/icu/lang/UScript;->getCode(Ljava/lang/String;)[I

    move-result-object v1

    .line 107
    .local v1, "s":[I
    if-eqz v1, :cond_0

    .line 108
    const/4 v4, 0x0

    aget v4, v1, v4

    invoke-static {v4}, Lcom/ibm/icu/lang/UScript;->getName(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->scriptName:Ljava/lang/String;

    .line 110
    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->scriptName:Ljava/lang/String;

    iget-object v5, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->top:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 111
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->scriptName:Ljava/lang/String;

    .line 115
    :cond_0
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->isSpecLocale:Z

    .line 116
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->res:Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 118
    const/4 v4, -0x1

    if-ne v2, v4, :cond_1

    .line 119
    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->top:Ljava/lang/String;

    invoke-static {v4}, Lcom/ibm/icu/impl/LocaleUtility;->getLocaleFromName(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v3

    .line 120
    .local v3, "toploc":Ljava/util/Locale;
    const-string/jumbo v4, "com/ibm/icu/impl/data/icudt40b/translit"

    invoke-static {v4, v3}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Ljava/util/Locale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v4

    check-cast v4, Lcom/ibm/icu/impl/ICUResourceBundle;

    iput-object v4, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->res:Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 122
    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->res:Lcom/ibm/icu/impl/ICUResourceBundle;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->res:Lcom/ibm/icu/impl/ICUResourceBundle;

    invoke-virtual {v4}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v4

    invoke-virtual {v4}, Lcom/ibm/icu/util/ULocale;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->top:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/ibm/icu/impl/LocaleUtility;->isFallbackOf(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 123
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->isSpecLocale:Z
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    .end local v1    # "s":[I
    .end local v2    # "script":I
    .end local v3    # "toploc":Ljava/util/Locale;
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->reset()V

    .line 131
    return-void

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/util/MissingResourceException;
    iput-object v6, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->scriptName:Ljava/lang/String;

    goto :goto_0
.end method

.method private setupNext()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 146
    iput-boolean v3, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->isNextLocale:Z

    .line 147
    iget-boolean v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->isSpecLocale:Z

    if-eqz v1, :cond_1

    .line 148
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->spec:Ljava/lang/String;

    iput-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->nextSpec:Ljava/lang/String;

    .line 149
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->nextSpec:Ljava/lang/String;

    const/16 v2, 0x5f

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 152
    .local v0, "i":I
    if-lez v0, :cond_0

    .line 153
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->spec:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->nextSpec:Ljava/lang/String;

    .line 154
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->isNextLocale:Z

    .line 166
    .end local v0    # "i":I
    :goto_0
    return-void

    .line 156
    .restart local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->scriptName:Ljava/lang/String;

    iput-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->nextSpec:Ljava/lang/String;

    goto :goto_0

    .line 160
    .end local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->nextSpec:Ljava/lang/String;

    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->scriptName:Ljava/lang/String;

    if-eq v1, v2, :cond_2

    .line 161
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->scriptName:Ljava/lang/String;

    iput-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->nextSpec:Ljava/lang/String;

    goto :goto_0

    .line 163
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->nextSpec:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public get()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->spec:Ljava/lang/String;

    return-object v0
.end method

.method public getBundle()Ljava/util/ResourceBundle;
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->res:Lcom/ibm/icu/impl/ICUResourceBundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->res:Lcom/ibm/icu/impl/ICUResourceBundle;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getULocale()Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ibm/icu/util/ULocale;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->spec:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->res:Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 199
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTop()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->top:Ljava/lang/String;

    return-object v0
.end method

.method public hasFallback()Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->nextSpec:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLocale()Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->isSpecLocale:Z

    return v0
.end method

.method public next()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->nextSpec:Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->spec:Ljava/lang/String;

    .line 174
    iget-boolean v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->isNextLocale:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->isSpecLocale:Z

    .line 175
    invoke-direct {p0}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->setupNext()V

    .line 176
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->spec:Ljava/lang/String;

    return-object v0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->spec:Ljava/lang/String;

    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->top:Ljava/lang/String;

    if-eq v0, v1, :cond_0

    .line 139
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->top:Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->spec:Ljava/lang/String;

    .line 140
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->res:Lcom/ibm/icu/impl/ICUResourceBundle;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->isSpecLocale:Z

    .line 141
    invoke-direct {p0}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->setupNext()V

    .line 143
    :cond_0
    return-void

    .line 140
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
