.class final Lcom/ibm/icu/text/BreakIteratorFactory;
.super Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;
.source "BreakIteratorFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/BreakIteratorFactory$BFService;
    }
.end annotation


# static fields
.field private static final DICTIONARY_POSSIBLE:[Z

.field private static final KIND_NAMES:[Ljava/lang/String;

.field static final service:Lcom/ibm/icu/impl/ICULocaleService;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 87
    new-instance v0, Lcom/ibm/icu/text/BreakIteratorFactory$BFService;

    invoke-direct {v0}, Lcom/ibm/icu/text/BreakIteratorFactory$BFService;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/BreakIteratorFactory;->service:Lcom/ibm/icu/impl/ICULocaleService;

    .line 100
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "grapheme"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "word"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "line"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "sentence"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "title"

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/BreakIteratorFactory;->KIND_NAMES:[Ljava/lang/String;

    .line 103
    new-array v0, v3, [Z

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/text/BreakIteratorFactory;->DICTIONARY_POSSIBLE:[Z

    return-void

    :array_0
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/ibm/icu/text/BreakIterator$BreakIteratorServiceShim;-><init>()V

    .line 73
    return-void
.end method

.method static access$000(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/util/ULocale;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-static {p0, p1}, Lcom/ibm/icu/text/BreakIteratorFactory;->createBreakInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v0

    return-object v0
.end method

.method private static createBreakInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;
    .locals 17
    .param p0, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p1, "kind"    # I

    .prologue
    .line 110
    const/4 v6, 0x0

    .line 111
    .local v6, "iter":Lcom/ibm/icu/text/BreakIterator;
    const-string/jumbo v13, "com/ibm/icu/impl/data/icudt40b/brkitr"

    move-object/from16 v0, p0

    invoke-static {v13, v0}, Lcom/ibm/icu/util/UResourceBundle;->getBundleInstance(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/util/UResourceBundle;

    move-result-object v8

    check-cast v8, Lcom/ibm/icu/impl/ICUResourceBundle;

    .line 117
    .local v8, "rb":Lcom/ibm/icu/impl/ICUResourceBundle;
    const/4 v9, 0x0

    .line 119
    .local v9, "ruleStream":Ljava/io/InputStream;
    :try_start_0
    sget-object v13, Lcom/ibm/icu/text/BreakIteratorFactory;->KIND_NAMES:[Ljava/lang/String;

    aget-object v11, v13, p1

    .line 120
    .local v11, "typeKey":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v14, "boundaries/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->getStringWithFallback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 121
    .local v1, "brkfname":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v14, "data/icudt40b/brkitr/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    .line 122
    .local v10, "rulesFileName":Ljava/lang/String;
    invoke-static {v10}, Lcom/ibm/icu/impl/ICUData;->getStream(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 132
    sget-object v13, Lcom/ibm/icu/text/BreakIteratorFactory;->DICTIONARY_POSSIBLE:[Z

    aget-boolean v13, v13, p1

    if-eqz v13, :cond_0

    .line 136
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/util/ULocale;->getLanguage()Ljava/lang/String;

    move-result-object v13

    const-string/jumbo v14, "th"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 138
    const-string/jumbo v3, "Thai"

    .line 139
    .local v3, "dictType":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v14, "dictionaries/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Lcom/ibm/icu/impl/ICUResourceBundle;->getStringWithFallback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 140
    .local v2, "dictFileName":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v14, "data/icudt40b/brkitr/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 141
    invoke-static {v2}, Lcom/ibm/icu/impl/ICUData;->getStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 142
    .local v5, "is":Ljava/io/InputStream;
    new-instance v7, Lcom/ibm/icu/text/ThaiBreakIterator;

    invoke-direct {v7, v9, v5}, Lcom/ibm/icu/text/ThaiBreakIterator;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/util/MissingResourceException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v6    # "iter":Lcom/ibm/icu/text/BreakIterator;
    .local v7, "iter":Lcom/ibm/icu/text/BreakIterator;
    move-object v6, v7

    .line 155
    .end local v2    # "dictFileName":Ljava/lang/String;
    .end local v3    # "dictType":Ljava/lang/String;
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v7    # "iter":Lcom/ibm/icu/text/BreakIterator;
    .restart local v6    # "iter":Lcom/ibm/icu/text/BreakIterator;
    :cond_0
    :goto_0
    if-nez v6, :cond_1

    .line 161
    :try_start_2
    invoke-static {v9}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->getInstanceFromCompiledRules(Ljava/io/InputStream;)Lcom/ibm/icu/text/RuleBasedBreakIterator;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v6

    .line 170
    :cond_1
    :goto_1
    invoke-virtual {v8}, Lcom/ibm/icu/impl/ICUResourceBundle;->getLocale()Ljava/util/Locale;

    move-result-object v13

    invoke-static {v13}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v12

    .line 171
    .local v12, "uloc":Lcom/ibm/icu/util/ULocale;
    invoke-virtual {v6, v12, v12}, Lcom/ibm/icu/text/BreakIterator;->setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V

    .line 173
    return-object v6

    .line 124
    .end local v1    # "brkfname":Ljava/lang/String;
    .end local v10    # "rulesFileName":Ljava/lang/String;
    .end local v11    # "typeKey":Ljava/lang/String;
    .end local v12    # "uloc":Lcom/ibm/icu/util/ULocale;
    :catch_0
    move-exception v4

    .line 125
    .local v4, "e":Ljava/lang/Exception;
    new-instance v13, Ljava/util/MissingResourceException;

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    const-string/jumbo v15, ""

    const-string/jumbo v16, ""

    invoke-direct/range {v13 .. v16}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v13

    .line 150
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v1    # "brkfname":Ljava/lang/String;
    .restart local v10    # "rulesFileName":Ljava/lang/String;
    .restart local v11    # "typeKey":Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 151
    .local v4, "e":Ljava/io/IOException;
    invoke-static {v4}, Lcom/ibm/icu/impl/Assert;->fail(Ljava/lang/Exception;)V

    goto :goto_0

    .line 163
    .end local v4    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v4

    .line 166
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-static {v4}, Lcom/ibm/icu/impl/Assert;->fail(Ljava/lang/Exception;)V

    goto :goto_1

    .line 144
    .end local v4    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v13

    goto :goto_0
.end method


# virtual methods
.method public createBreakIterator(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;
    .locals 4
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "kind"    # I

    .prologue
    const/4 v3, 0x0

    .line 64
    sget-object v2, Lcom/ibm/icu/text/BreakIteratorFactory;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v2}, Lcom/ibm/icu/impl/ICULocaleService;->isDefault()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 65
    invoke-static {p1, p2}, Lcom/ibm/icu/text/BreakIteratorFactory;->createBreakInstance(Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/text/BreakIterator;

    move-result-object v1

    .line 70
    :goto_0
    return-object v1

    .line 67
    :cond_0
    const/4 v2, 0x1

    new-array v0, v2, [Lcom/ibm/icu/util/ULocale;

    .line 68
    .local v0, "actualLoc":[Lcom/ibm/icu/util/ULocale;
    sget-object v2, Lcom/ibm/icu/text/BreakIteratorFactory;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v2, p1, p2, v0}, Lcom/ibm/icu/impl/ICULocaleService;->get(Lcom/ibm/icu/util/ULocale;I[Lcom/ibm/icu/util/ULocale;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/BreakIterator;

    .line 69
    .local v1, "iter":Lcom/ibm/icu/text/BreakIterator;
    aget-object v2, v0, v3

    aget-object v3, v0, v3

    invoke-virtual {v1, v2, v3}, Lcom/ibm/icu/text/BreakIterator;->setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V

    goto :goto_0
.end method

.method public getAvailableLocales()[Ljava/util/Locale;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/ibm/icu/text/BreakIteratorFactory;->service:Lcom/ibm/icu/impl/ICULocaleService;

    if-nez v0, :cond_0

    .line 48
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableLocales(Ljava/lang/String;)[Ljava/util/Locale;

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/text/BreakIteratorFactory;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICULocaleService;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v0

    goto :goto_0
.end method

.method public getAvailableULocales()[Lcom/ibm/icu/util/ULocale;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/ibm/icu/text/BreakIteratorFactory;->service:Lcom/ibm/icu/impl/ICULocaleService;

    if-nez v0, :cond_0

    .line 56
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableULocales(Ljava/lang/String;)[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/text/BreakIteratorFactory;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICULocaleService;->getAvailableULocales()[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    goto :goto_0
.end method

.method public registerInstance(Lcom/ibm/icu/text/BreakIterator;Lcom/ibm/icu/util/ULocale;I)Ljava/lang/Object;
    .locals 2
    .param p1, "iter"    # Lcom/ibm/icu/text/BreakIterator;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;
    .param p3, "kind"    # I

    .prologue
    .line 35
    new-instance v0, Ljava/text/StringCharacterIterator;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Ljava/text/StringCharacterIterator;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/ibm/icu/text/BreakIterator;->setText(Ljava/text/CharacterIterator;)V

    .line 36
    sget-object v0, Lcom/ibm/icu/text/BreakIteratorFactory;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/ibm/icu/impl/ICULocaleService;->registerObject(Ljava/lang/Object;Lcom/ibm/icu/util/ULocale;I)Lcom/ibm/icu/impl/ICUService$Factory;

    move-result-object v0

    return-object v0
.end method

.method public unregister(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 40
    sget-object v0, Lcom/ibm/icu/text/BreakIteratorFactory;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICULocaleService;->isDefault()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    const/4 v0, 0x0

    .line 43
    .end local p1    # "key":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "key":Ljava/lang/Object;
    :cond_0
    sget-object v0, Lcom/ibm/icu/text/BreakIteratorFactory;->service:Lcom/ibm/icu/impl/ICULocaleService;

    check-cast p1, Lcom/ibm/icu/impl/ICUService$Factory;

    .end local p1    # "key":Ljava/lang/Object;
    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/ICULocaleService;->unregisterFactory(Lcom/ibm/icu/impl/ICUService$Factory;)Z

    move-result v0

    goto :goto_0
.end method
