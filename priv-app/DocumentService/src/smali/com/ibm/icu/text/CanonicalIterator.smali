.class public final Lcom/ibm/icu/text/CanonicalIterator;
.super Ljava/lang/Object;
.source "CanonicalIterator.java"


# static fields
.field private static PROGRESS:Z

.field private static final SET_WITH_NULL_STRING:Ljava/util/Set;

.field private static SKIP_ZEROS:Z


# instance fields
.field private transient buffer:Ljava/lang/StringBuffer;

.field private current:[I

.field private done:Z

.field private pieces:[[Ljava/lang/String;

.field private source:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 222
    const/4 v0, 0x0

    sput-boolean v0, Lcom/ibm/icu/text/CanonicalIterator;->PROGRESS:Z

    .line 224
    const/4 v0, 0x1

    sput-boolean v0, Lcom/ibm/icu/text/CanonicalIterator;->SKIP_ZEROS:Z

    .line 444
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/CanonicalIterator;->SET_WITH_NULL_STRING:Ljava/util/Set;

    .line 446
    sget-object v0, Lcom/ibm/icu/text/CanonicalIterator;->SET_WITH_NULL_STRING:Ljava/util/Set;

    const-string/jumbo v1, ""

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 447
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CanonicalIterator;->buffer:Ljava/lang/StringBuffer;

    .line 46
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/CanonicalIterator;->setSource(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method private extract(ILjava/lang/String;ILjava/lang/StringBuffer;)Ljava/util/Set;
    .locals 10
    .param p1, "comp"    # I
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "segmentPos"    # I
    .param p4, "buf"    # Ljava/lang/StringBuffer;

    .prologue
    .line 370
    sget-boolean v7, Lcom/ibm/icu/text/CanonicalIterator;->PROGRESS:Z

    if-eqz v7, :cond_0

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v9, " extract: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/ibm/icu/impl/Utility;->hex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string/jumbo v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {p2, p3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/ibm/icu/impl/Utility;->hex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 374
    :cond_0
    sget-object v7, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {p1, v7}, Lcom/ibm/icu/text/Normalizer;->normalize(ILcom/ibm/icu/text/Normalizer$Mode;)Ljava/lang/String;

    move-result-object v1

    .line 377
    .local v1, "decomp":Ljava/lang/String;
    const/4 v5, 0x0

    .line 379
    .local v5, "ok":Z
    const/4 v3, 0x0

    .line 380
    .local v3, "decompPos":I
    const/4 v7, 0x0

    invoke-static {v1, v7}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v2

    .line 381
    .local v2, "decompCp":I
    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v7

    add-int/2addr v3, v7

    .line 383
    const/4 v7, 0x0

    invoke-virtual {p4, v7}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 385
    move v4, p3

    .local v4, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v4, v7, :cond_2

    .line 386
    invoke-static {p2, v4}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 387
    .local v0, "cp":I
    if-ne v0, v2, :cond_4

    .line 388
    sget-boolean v7, Lcom/ibm/icu/text/CanonicalIterator;->PROGRESS:Z

    if-eqz v7, :cond_1

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v9, "  matches: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/ibm/icu/impl/Utility;->hex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 389
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v3, v7, :cond_3

    .line 390
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v7

    add-int/2addr v7, v4

    invoke-virtual {p2, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 391
    const/4 v5, 0x1

    .line 414
    .end local v0    # "cp":I
    :cond_2
    if-nez v5, :cond_6

    const/4 v7, 0x0

    .line 429
    :goto_1
    return-object v7

    .line 394
    .restart local v0    # "cp":I
    :cond_3
    invoke-static {v1, v3}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v2

    .line 395
    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v7

    add-int/2addr v3, v7

    .line 385
    :goto_2
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v7

    add-int/2addr v4, v7

    goto :goto_0

    .line 398
    :cond_4
    sget-boolean v7, Lcom/ibm/icu/text/CanonicalIterator;->PROGRESS:Z

    if-eqz v7, :cond_5

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v9, "  buffer: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/ibm/icu/impl/Utility;->hex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 400
    :cond_5
    invoke-static {p4, v0}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 415
    .end local v0    # "cp":I
    :cond_6
    sget-boolean v7, Lcom/ibm/icu/text/CanonicalIterator;->PROGRESS:Z

    if-eqz v7, :cond_7

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v8, "Matches"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 416
    :cond_7
    invoke-virtual {p4}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    if-nez v7, :cond_8

    sget-object v7, Lcom/ibm/icu/text/CanonicalIterator;->SET_WITH_NULL_STRING:Ljava/util/Set;

    goto :goto_1

    .line 417
    :cond_8
    invoke-virtual {p4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    .line 426
    .local v6, "remainder":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, p3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/ibm/icu/text/Normalizer;->compare(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v7

    if-eqz v7, :cond_9

    const/4 v7, 0x0

    goto :goto_1

    .line 429
    :cond_9
    invoke-direct {p0, v6}, Lcom/ibm/icu/text/CanonicalIterator;->getEquivalents2(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v7

    goto :goto_1
.end method

.method private getEquivalents(Ljava/lang/String;)[Ljava/lang/String;
    .locals 11
    .param p1, "segment"    # Ljava/lang/String;

    .prologue
    .line 241
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 242
    .local v7, "result":Ljava/util/Set;
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/CanonicalIterator;->getEquivalents2(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 243
    .local v0, "basic":Ljava/util/Set;
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 248
    .local v5, "permutations":Ljava/util/Set;
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 249
    .local v2, "it":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 250
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 251
    .local v4, "item":Ljava/lang/String;
    invoke-interface {v5}, Ljava/util/Set;->clear()V

    .line 252
    sget-boolean v8, Lcom/ibm/icu/text/CanonicalIterator;->SKIP_ZEROS:Z

    invoke-static {v4, v8, v5}, Lcom/ibm/icu/text/CanonicalIterator;->permute(Ljava/lang/String;ZLjava/util/Set;)V

    .line 253
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 254
    .local v3, "it2":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 255
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 261
    .local v6, "possible":Ljava/lang/String;
    const/4 v8, 0x0

    invoke-static {v6, p1, v8}, Lcom/ibm/icu/text/Normalizer;->compare(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v8

    if-nez v8, :cond_3

    .line 263
    sget-boolean v8, Lcom/ibm/icu/text/CanonicalIterator;->PROGRESS:Z

    if-eqz v8, :cond_2

    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v10, "Adding Permutation: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-static {v6}, Lcom/ibm/icu/impl/Utility;->hex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 264
    :cond_2
    invoke-interface {v7, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 267
    :cond_3
    sget-boolean v8, Lcom/ibm/icu/text/CanonicalIterator;->PROGRESS:Z

    if-eqz v8, :cond_1

    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v10, "-Skipping Permutation: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-static {v6}, Lcom/ibm/icu/impl/Utility;->hex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 273
    .end local v3    # "it2":Ljava/util/Iterator;
    .end local v4    # "item":Ljava/lang/String;
    .end local v6    # "possible":Ljava/lang/String;
    :cond_4
    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v8

    new-array v1, v8, [Ljava/lang/String;

    .line 274
    .local v1, "finalResult":[Ljava/lang/String;
    invoke-interface {v7, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 275
    return-object v1
.end method

.method private getEquivalents2(Ljava/lang/String;)Ljava/util/Set;
    .locals 21
    .param p1, "segment"    # Ljava/lang/String;

    .prologue
    .line 281
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    .line 283
    .local v14, "result":Ljava/util/Set;
    sget-boolean v18, Lcom/ibm/icu/text/CanonicalIterator;->PROGRESS:Z

    if-eqz v18, :cond_0

    sget-object v18, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v20, "Adding: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-static/range {p1 .. p1}, Lcom/ibm/icu/impl/Utility;->hex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 285
    :cond_0
    move-object/from16 v0, p1

    invoke-interface {v14, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 286
    new-instance v17, Ljava/lang/StringBuffer;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuffer;-><init>()V

    .line 289
    .local v17, "workingBuffer":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .line 290
    .local v3, "cp":I
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v11, v0, [I

    .line 291
    .local v11, "range":[I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v18

    if-ge v6, v0, :cond_6

    .line 294
    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v3

    .line 295
    new-instance v15, Lcom/ibm/icu/impl/USerializedSet;

    invoke-direct {v15}, Lcom/ibm/icu/impl/USerializedSet;-><init>()V

    .line 297
    .local v15, "starts":Lcom/ibm/icu/impl/USerializedSet;
    invoke-static {v3, v15}, Lcom/ibm/icu/impl/NormalizerImpl;->getCanonStartSet(ILcom/ibm/icu/impl/USerializedSet;)Z

    move-result v18

    if-nez v18, :cond_2

    .line 291
    :cond_1
    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v18

    add-int v6, v6, v18

    goto :goto_0

    .line 300
    :cond_2
    const/4 v9, 0x0

    .line 302
    .local v9, "j":I
    invoke-virtual {v15}, Lcom/ibm/icu/impl/USerializedSet;->countRanges()I

    move-result v12

    .line 303
    .local v12, "rangeCount":I
    const/4 v9, 0x0

    :goto_1
    if-ge v9, v12, :cond_1

    .line 304
    invoke-virtual {v15, v9, v11}, Lcom/ibm/icu/impl/USerializedSet;->getRange(I[I)Z

    .line 305
    const/16 v18, 0x1

    aget v5, v11, v18

    .line 306
    .local v5, "end":I
    const/16 v18, 0x0

    aget v4, v11, v18

    .local v4, "cp2":I
    :goto_2
    if-gt v4, v5, :cond_5

    .line 307
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v4, v1, v6, v2}, Lcom/ibm/icu/text/CanonicalIterator;->extract(ILjava/lang/String;ILjava/lang/StringBuffer;)Ljava/util/Set;

    move-result-object v13

    .line 308
    .local v13, "remainder":Ljava/util/Set;
    if-nez v13, :cond_4

    .line 306
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 311
    :cond_4
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 312
    .local v10, "prefix":Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    .line 314
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 315
    .local v8, "iter":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 316
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 317
    .local v7, "item":Ljava/lang/String;
    new-instance v16, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 318
    .local v16, "toAdd":Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v16

    .line 319
    move-object/from16 v0, v16

    invoke-interface {v14, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 303
    .end local v7    # "item":Ljava/lang/String;
    .end local v8    # "iter":Ljava/util/Iterator;
    .end local v10    # "prefix":Ljava/lang/String;
    .end local v13    # "remainder":Ljava/util/Set;
    .end local v16    # "toAdd":Ljava/lang/String;
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 325
    .end local v4    # "cp2":I
    .end local v5    # "end":I
    .end local v9    # "j":I
    .end local v12    # "rangeCount":I
    .end local v15    # "starts":Lcom/ibm/icu/impl/USerializedSet;
    :cond_6
    return-object v14
.end method

.method public static permute(Ljava/lang/String;ZLjava/util/Set;)V
    .locals 8
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "skipZeros"    # Z
    .param p2, "output"    # Ljava/util/Set;

    .prologue
    .line 163
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x2

    if-gt v6, v7, :cond_1

    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->countCodePoint(Ljava/lang/String;)I

    move-result v6

    const/4 v7, 0x1

    if-gt v6, v7, :cond_1

    .line 164
    invoke-interface {p2, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 196
    :cond_0
    return-void

    .line 169
    :cond_1
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 171
    .local v5, "subpermute":Ljava/util/Set;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v2, v6, :cond_0

    .line 172
    invoke-static {p0, v2}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v1

    .line 177
    .local v1, "cp":I
    if-eqz p1, :cond_3

    if-eqz v2, :cond_3

    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->getCombiningClass(I)I

    move-result v6

    if-nez v6, :cond_3

    .line 171
    :cond_2
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v6

    add-int/2addr v2, v6

    goto :goto_0

    .line 183
    :cond_3
    invoke-interface {v5}, Ljava/util/Set;->clear()V

    .line 184
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v7, 0x0

    invoke-virtual {p0, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v7

    add-int/2addr v7, v2

    invoke-virtual {p0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, p1, v5}, Lcom/ibm/icu/text/CanonicalIterator;->permute(Ljava/lang/String;ZLjava/util/Set;)V

    .line 188
    invoke-static {p0, v2}, Lcom/ibm/icu/text/UTF16;->valueOf(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 189
    .local v0, "chStr":Ljava/lang/String;
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 190
    .local v3, "it":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 191
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 193
    .local v4, "piece":Ljava/lang/String;
    invoke-interface {p2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/ibm/icu/text/CanonicalIterator;->source:Ljava/lang/String;

    return-object v0
.end method

.method public next()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 77
    iget-boolean v2, p0, Lcom/ibm/icu/text/CanonicalIterator;->done:Z

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    .line 98
    :cond_0
    :goto_0
    return-object v1

    .line 81
    :cond_1
    iget-object v2, p0, Lcom/ibm/icu/text/CanonicalIterator;->buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 82
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/ibm/icu/text/CanonicalIterator;->pieces:[[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 83
    iget-object v2, p0, Lcom/ibm/icu/text/CanonicalIterator;->buffer:Ljava/lang/StringBuffer;

    iget-object v3, p0, Lcom/ibm/icu/text/CanonicalIterator;->pieces:[[Ljava/lang/String;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/ibm/icu/text/CanonicalIterator;->current:[I

    aget v4, v4, v0

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 85
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/CanonicalIterator;->buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 89
    .local v1, "result":Ljava/lang/String;
    iget-object v2, p0, Lcom/ibm/icu/text/CanonicalIterator;->current:[I

    array-length v2, v2

    add-int/lit8 v0, v2, -0x1

    .line 90
    :goto_2
    if-gez v0, :cond_3

    .line 91
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/ibm/icu/text/CanonicalIterator;->done:Z

    goto :goto_0

    .line 94
    :cond_3
    iget-object v2, p0, Lcom/ibm/icu/text/CanonicalIterator;->current:[I

    aget v3, v2, v0

    add-int/lit8 v3, v3, 0x1

    aput v3, v2, v0

    .line 95
    iget-object v2, p0, Lcom/ibm/icu/text/CanonicalIterator;->current:[I

    aget v2, v2, v0

    iget-object v3, p0, Lcom/ibm/icu/text/CanonicalIterator;->pieces:[[Ljava/lang/String;

    aget-object v3, v3, v0

    array-length v3, v3

    if-lt v2, v3, :cond_0

    .line 96
    iget-object v2, p0, Lcom/ibm/icu/text/CanonicalIterator;->current:[I

    aput v5, v2, v0

    .line 89
    add-int/lit8 v0, v0, -0x1

    goto :goto_2
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 63
    iput-boolean v2, p0, Lcom/ibm/icu/text/CanonicalIterator;->done:Z

    .line 64
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/ibm/icu/text/CanonicalIterator;->current:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/ibm/icu/text/CanonicalIterator;->current:[I

    aput v2, v1, v0

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_0
    return-void
.end method

.method public setSource(Ljava/lang/String;)V
    .locals 8
    .param p1, "newSource"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 108
    sget-object v4, Lcom/ibm/icu/text/Normalizer;->NFD:Lcom/ibm/icu/text/Normalizer$Mode;

    invoke-static {p1, v4}, Lcom/ibm/icu/text/Normalizer;->normalize(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/ibm/icu/text/CanonicalIterator;->source:Ljava/lang/String;

    .line 109
    iput-boolean v7, p0, Lcom/ibm/icu/text/CanonicalIterator;->done:Z

    .line 112
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 113
    new-array v4, v5, [[Ljava/lang/String;

    iput-object v4, p0, Lcom/ibm/icu/text/CanonicalIterator;->pieces:[[Ljava/lang/String;

    .line 114
    new-array v4, v5, [I

    iput-object v4, p0, Lcom/ibm/icu/text/CanonicalIterator;->current:[I

    .line 115
    iget-object v4, p0, Lcom/ibm/icu/text/CanonicalIterator;->pieces:[[Ljava/lang/String;

    new-array v5, v5, [Ljava/lang/String;

    const-string/jumbo v6, ""

    aput-object v6, v5, v7

    aput-object v5, v4, v7

    .line 145
    :cond_0
    return-void

    .line 120
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 122
    .local v2, "segmentList":Ljava/util/List;
    const/4 v3, 0x0

    .line 127
    .local v3, "start":I
    iget-object v4, p0, Lcom/ibm/icu/text/CanonicalIterator;->source:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/ibm/icu/text/UTF16;->findOffsetFromCodePoint(Ljava/lang/String;I)I

    move-result v1

    .line 129
    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/ibm/icu/text/CanonicalIterator;->source:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 130
    iget-object v4, p0, Lcom/ibm/icu/text/CanonicalIterator;->source:Ljava/lang/String;

    invoke-static {v4, v1}, Lcom/ibm/icu/text/UTF16;->charAt(Ljava/lang/String;I)I

    move-result v0

    .line 131
    .local v0, "cp":I
    invoke-static {v0}, Lcom/ibm/icu/impl/NormalizerImpl;->isCanonSafeStart(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 132
    iget-object v4, p0, Lcom/ibm/icu/text/CanonicalIterator;->source:Ljava/lang/String;

    invoke-virtual {v4, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    move v3, v1

    .line 129
    :cond_2
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v4

    add-int/2addr v1, v4

    goto :goto_0

    .line 136
    .end local v0    # "cp":I
    :cond_3
    iget-object v4, p0, Lcom/ibm/icu/text/CanonicalIterator;->source:Ljava/lang/String;

    invoke-virtual {v4, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [[Ljava/lang/String;

    iput-object v4, p0, Lcom/ibm/icu/text/CanonicalIterator;->pieces:[[Ljava/lang/String;

    .line 140
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [I

    iput-object v4, p0, Lcom/ibm/icu/text/CanonicalIterator;->current:[I

    .line 141
    const/4 v1, 0x0

    :goto_1
    iget-object v4, p0, Lcom/ibm/icu/text/CanonicalIterator;->pieces:[[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_0

    .line 142
    sget-boolean v4, Lcom/ibm/icu/text/CanonicalIterator;->PROGRESS:Z

    if-eqz v4, :cond_4

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v5, "SEGMENT"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 143
    :cond_4
    iget-object v5, p0, Lcom/ibm/icu/text/CanonicalIterator;->pieces:[[Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/ibm/icu/text/CanonicalIterator;->getEquivalents(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v1

    .line 141
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
