.class public Lcom/ibm/icu/text/ChineseDateFormat;
.super Lcom/ibm/icu/text/SimpleDateFormat;
.source "ChineseDateFormat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/ChineseDateFormat$Field;
    }
.end annotation


# static fields
.field static class$com$ibm$icu$text$ChineseDateFormat$Field:Ljava/lang/Class; = null

.field static final serialVersionUID:J = -0x3ffb141bc87c5a3bL


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V
    .locals 6
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 70
    new-instance v2, Lcom/ibm/icu/text/ChineseDateFormatSymbols;

    invoke-direct {v2, p2}, Lcom/ibm/icu/text/ChineseDateFormatSymbols;-><init>(Lcom/ibm/icu/util/ULocale;)V

    new-instance v3, Lcom/ibm/icu/util/ChineseCalendar;

    invoke-static {}, Lcom/ibm/icu/util/TimeZone;->getDefault()Lcom/ibm/icu/util/TimeZone;

    move-result-object v0

    invoke-direct {v3, v0, p2}, Lcom/ibm/icu/util/ChineseCalendar;-><init>(Lcom/ibm/icu/util/TimeZone;Lcom/ibm/icu/util/ULocale;)V

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/SimpleDateFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/DateFormatSymbols;Lcom/ibm/icu/util/Calendar;Lcom/ibm/icu/util/ULocale;Z)V

    .line 72
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Locale;)V
    .locals 1
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/util/Locale;

    .prologue
    .line 60
    invoke-static {p2}, Lcom/ibm/icu/util/ULocale;->forLocale(Ljava/util/Locale;)Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/ChineseDateFormat;-><init>(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)V

    .line 61
    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 257
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method protected patternCharToDateFormatField(C)Lcom/ibm/icu/text/DateFormat$Field;
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 190
    const/16 v0, 0x6c

    if-ne p1, v0, :cond_0

    .line 191
    sget-object v0, Lcom/ibm/icu/text/ChineseDateFormat$Field;->IS_LEAP_MONTH:Lcom/ibm/icu/text/ChineseDateFormat$Field;

    .line 193
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/ibm/icu/text/SimpleDateFormat;->patternCharToDateFormatField(C)Lcom/ibm/icu/text/DateFormat$Field;

    move-result-object v0

    goto :goto_0
.end method

.method protected subFormat(Ljava/lang/StringBuffer;CIILjava/text/FieldPosition;Lcom/ibm/icu/util/Calendar;)V
    .locals 3
    .param p1, "buf"    # Ljava/lang/StringBuffer;
    .param p2, "ch"    # C
    .param p3, "count"    # I
    .param p4, "beginOffset"    # I
    .param p5, "pos"    # Ljava/text/FieldPosition;
    .param p6, "cal"    # Lcom/ibm/icu/util/Calendar;

    .prologue
    .line 106
    sparse-switch p2, :sswitch_data_0

    .line 115
    invoke-super/range {p0 .. p6}, Lcom/ibm/icu/text/SimpleDateFormat;->subFormat(Ljava/lang/StringBuffer;CIILjava/text/FieldPosition;Lcom/ibm/icu/util/Calendar;)V

    .line 123
    :goto_0
    return-void

    .line 108
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p6, v0}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x1

    const/16 v2, 0x9

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/ibm/icu/text/ChineseDateFormat;->zeroPaddingNumber(Ljava/lang/StringBuffer;III)V

    goto :goto_0

    .line 111
    :sswitch_1
    invoke-virtual {p0}, Lcom/ibm/icu/text/ChineseDateFormat;->getSymbols()Lcom/ibm/icu/text/DateFormatSymbols;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/ChineseDateFormatSymbols;

    sget v1, Lcom/ibm/icu/util/ChineseCalendar;->IS_LEAP_MONTH:I

    invoke-virtual {p6, v1}, Lcom/ibm/icu/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/ChineseDateFormatSymbols;->getLeapMonth(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 106
    nop

    :sswitch_data_0
    .sparse-switch
        0x47 -> :sswitch_0
        0x6c -> :sswitch_1
    .end sparse-switch
.end method

.method protected subParse(Ljava/lang/String;ICIZZ[ZLcom/ibm/icu/util/Calendar;)I
    .locals 12
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "ch"    # C
    .param p4, "count"    # I
    .param p5, "obeyCount"    # Z
    .param p6, "allowNegative"    # Z
    .param p7, "ambiguousYear"    # [Z
    .param p8, "cal"    # Lcom/ibm/icu/util/Calendar;

    .prologue
    .line 131
    const/16 v1, 0x47

    if-eq p3, v1, :cond_1

    const/16 v1, 0x6c

    if-eq p3, v1, :cond_1

    const/16 v1, 0x79

    if-eq p3, v1, :cond_1

    .line 132
    invoke-super/range {p0 .. p8}, Lcom/ibm/icu/text/SimpleDateFormat;->subParse(Ljava/lang/String;ICIZZ[ZLcom/ibm/icu/util/Calendar;)I

    move-result v9

    .line 176
    :cond_0
    :goto_0
    return v9

    .line 136
    :cond_1
    invoke-static {p1, p2}, Lcom/ibm/icu/impl/Utility;->skipWhitespace(Ljava/lang/String;I)I

    move-result p2

    .line 138
    new-instance v8, Ljava/text/ParsePosition;

    invoke-direct {v8, p2}, Ljava/text/ParsePosition;-><init>(I)V

    .line 140
    .local v8, "pos":Ljava/text/ParsePosition;
    sparse-switch p3, :sswitch_data_0

    .line 176
    const/4 v9, 0x0

    goto :goto_0

    .line 144
    :sswitch_0
    const/4 v7, 0x0

    .line 145
    .local v7, "number":Ljava/lang/Number;
    if-eqz p5, :cond_3

    .line 146
    add-int v1, p2, p4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v1, v2, :cond_2

    .line 147
    neg-int v9, p2

    goto :goto_0

    .line 149
    :cond_2
    iget-object v1, p0, Lcom/ibm/icu/text/ChineseDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    const/4 v2, 0x0

    add-int v3, p2, p4

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v8}, Lcom/ibm/icu/text/NumberFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v7

    .line 153
    :goto_1
    if-nez v7, :cond_4

    .line 154
    neg-int v9, p2

    goto :goto_0

    .line 151
    :cond_3
    iget-object v1, p0, Lcom/ibm/icu/text/ChineseDateFormat;->numberFormat:Lcom/ibm/icu/text/NumberFormat;

    invoke-virtual {v1, p1, v8}, Lcom/ibm/icu/text/NumberFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Number;

    move-result-object v7

    goto :goto_1

    .line 156
    :cond_4
    invoke-virtual {v7}, Ljava/lang/Number;->intValue()I

    move-result v11

    .line 157
    .local v11, "value":I
    const/16 v1, 0x47

    if-ne p3, v1, :cond_5

    const/4 v1, 0x0

    :goto_2
    move-object/from16 v0, p8

    invoke-virtual {v0, v1, v11}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 158
    invoke-virtual {v8}, Ljava/text/ParsePosition;->getIndex()I

    move-result v9

    goto :goto_0

    .line 157
    :cond_5
    const/4 v1, 0x1

    goto :goto_2

    .line 162
    .end local v7    # "number":Ljava/lang/Number;
    .end local v11    # "value":I
    :sswitch_1
    invoke-virtual {p0}, Lcom/ibm/icu/text/ChineseDateFormat;->getSymbols()Lcom/ibm/icu/text/DateFormatSymbols;

    move-result-object v10

    check-cast v10, Lcom/ibm/icu/text/ChineseDateFormatSymbols;

    .line 164
    .local v10, "symbols":Lcom/ibm/icu/text/ChineseDateFormatSymbols;
    sget v4, Lcom/ibm/icu/util/ChineseCalendar;->IS_LEAP_MONTH:I

    iget-object v5, v10, Lcom/ibm/icu/text/ChineseDateFormatSymbols;->isLeapMonth:[Ljava/lang/String;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object/from16 v6, p8

    invoke-virtual/range {v1 .. v6}, Lcom/ibm/icu/text/ChineseDateFormat;->matchString(Ljava/lang/String;II[Ljava/lang/String;Lcom/ibm/icu/util/Calendar;)I

    move-result v9

    .line 168
    .local v9, "result":I
    if-gez v9, :cond_0

    .line 169
    sget v1, Lcom/ibm/icu/util/ChineseCalendar;->IS_LEAP_MONTH:I

    const/4 v2, 0x0

    move-object/from16 v0, p8

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/util/Calendar;->set(II)V

    .line 170
    move v9, p2

    goto :goto_0

    .line 140
    :sswitch_data_0
    .sparse-switch
        0x47 -> :sswitch_0
        0x6c -> :sswitch_1
        0x79 -> :sswitch_0
    .end sparse-switch
.end method
