.class Lcom/ibm/icu/text/RBBINode;
.super Ljava/lang/Object;
.source "RBBINode.java"


# static fields
.field static final endMark:I = 0x6

.field static gLastSerial:I = 0x0

.field static final leafChar:I = 0x3

.field static final lookAhead:I = 0x4

.field static final nodeTypeLimit:I = 0x10

.field static final nodeTypeNames:[Ljava/lang/String;

.field static final opBreak:I = 0xd

.field static final opCat:I = 0x8

.field static final opLParen:I = 0xf

.field static final opOr:I = 0x9

.field static final opPlus:I = 0xb

.field static final opQuestion:I = 0xc

.field static final opReverse:I = 0xe

.field static final opStar:I = 0xa

.field static final opStart:I = 0x7

.field static final precLParen:I = 0x2

.field static final precOpCat:I = 0x4

.field static final precOpOr:I = 0x3

.field static final precStart:I = 0x1

.field static final precZero:I = 0x0

.field static final setRef:I = 0x0

.field static final tag:I = 0x5

.field static final uset:I = 0x1

.field static final varRef:I = 0x2


# instance fields
.field fFirstPos:I

.field fFirstPosSet:Ljava/util/Set;

.field fFollowPos:Ljava/util/Set;

.field fInputSet:Lcom/ibm/icu/text/UnicodeSet;

.field fLastPos:I

.field fLastPosSet:Ljava/util/Set;

.field fLeftChild:Lcom/ibm/icu/text/RBBINode;

.field fLookAheadEnd:Z

.field fNullable:Z

.field fParent:Lcom/ibm/icu/text/RBBINode;

.field fPrecedence:I

.field fRightChild:Lcom/ibm/icu/text/RBBINode;

.field fSerialNum:I

.field fText:Ljava/lang/String;

.field fType:I

.field fVal:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "setRef"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "uset"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "varRef"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "leafChar"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "lookAhead"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "tag"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "endMark"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "opStart"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "opCat"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "opOr"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "opStar"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "opPlus"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "opQuestion"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "opBreak"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "opReverse"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "opLParen"

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/RBBINode;->nodeTypeNames:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(I)V
    .locals 3
    .param p1, "t"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput v2, p0, Lcom/ibm/icu/text/RBBINode;->fPrecedence:I

    .line 104
    const/16 v0, 0x10

    if-ge p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 105
    sget v0, Lcom/ibm/icu/text/RBBINode;->gLastSerial:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/ibm/icu/text/RBBINode;->gLastSerial:I

    iput v0, p0, Lcom/ibm/icu/text/RBBINode;->fSerialNum:I

    .line 106
    iput p1, p0, Lcom/ibm/icu/text/RBBINode;->fType:I

    .line 108
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    .line 109
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    .line 110
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fFollowPos:Ljava/util/Set;

    .line 111
    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    .line 112
    const/4 v0, 0x4

    iput v0, p0, Lcom/ibm/icu/text/RBBINode;->fPrecedence:I

    .line 122
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 104
    goto :goto_0

    .line 113
    :cond_1
    const/16 v0, 0x9

    if-ne p1, v0, :cond_2

    .line 114
    const/4 v0, 0x3

    iput v0, p0, Lcom/ibm/icu/text/RBBINode;->fPrecedence:I

    goto :goto_1

    .line 115
    :cond_2
    const/4 v0, 0x7

    if-ne p1, v0, :cond_3

    .line 116
    iput v1, p0, Lcom/ibm/icu/text/RBBINode;->fPrecedence:I

    goto :goto_1

    .line 117
    :cond_3
    const/16 v0, 0xf

    if-ne p1, v0, :cond_4

    .line 118
    const/4 v0, 0x2

    iput v0, p0, Lcom/ibm/icu/text/RBBINode;->fPrecedence:I

    goto :goto_1

    .line 120
    :cond_4
    iput v2, p0, Lcom/ibm/icu/text/RBBINode;->fPrecedence:I

    goto :goto_1
.end method

.method constructor <init>(Lcom/ibm/icu/text/RBBINode;)V
    .locals 2
    .param p1, "other"    # Lcom/ibm/icu/text/RBBINode;

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/RBBINode;->fPrecedence:I

    .line 125
    sget v0, Lcom/ibm/icu/text/RBBINode;->gLastSerial:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/ibm/icu/text/RBBINode;->gLastSerial:I

    iput v0, p0, Lcom/ibm/icu/text/RBBINode;->fSerialNum:I

    .line 126
    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fType:I

    iput v0, p0, Lcom/ibm/icu/text/RBBINode;->fType:I

    .line 127
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fInputSet:Lcom/ibm/icu/text/UnicodeSet;

    iput-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fInputSet:Lcom/ibm/icu/text/UnicodeSet;

    .line 128
    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fPrecedence:I

    iput v0, p0, Lcom/ibm/icu/text/RBBINode;->fPrecedence:I

    .line 129
    iget-object v0, p1, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    .line 130
    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    iput v0, p0, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    .line 131
    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    iput v0, p0, Lcom/ibm/icu/text/RBBINode;->fLastPos:I

    .line 132
    iget-boolean v0, p1, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/RBBINode;->fNullable:Z

    .line 133
    iget v0, p1, Lcom/ibm/icu/text/RBBINode;->fVal:I

    iput v0, p0, Lcom/ibm/icu/text/RBBINode;->fVal:I

    .line 134
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fFirstPosSet:Ljava/util/Set;

    .line 135
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fLastPosSet:Ljava/util/Set;

    .line 136
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p1, Lcom/ibm/icu/text/RBBINode;->fFollowPos:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fFollowPos:Ljava/util/Set;

    .line 137
    return-void
.end method

.method static printHex(II)V
    .locals 5
    .param p0, "i"    # I
    .param p1, "minWidth"    # I

    .prologue
    const/4 v4, 0x0

    .line 322
    const/16 v2, 0x10

    invoke-static {p0, v2}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v1

    .line 323
    .local v1, "s":Ljava/lang/String;
    const-string/jumbo v2, "00000"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    rsub-int/lit8 v3, v3, 0x5

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 325
    .local v0, "leadingZeroes":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 326
    invoke-static {v1, p1}, Lcom/ibm/icu/text/RBBINode;->printString(Ljava/lang/String;I)V

    .line 327
    return-void
.end method

.method static printInt(II)V
    .locals 2
    .param p0, "i"    # I
    .param p1, "minWidth"    # I

    .prologue
    .line 315
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 316
    .local v0, "s":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/ibm/icu/text/RBBINode;->printString(Ljava/lang/String;I)V

    .line 317
    return-void
.end method

.method static printNode(Lcom/ibm/icu/text/RBBINode;)V
    .locals 5
    .param p0, "n"    # Lcom/ibm/icu/text/RBBINode;

    .prologue
    const/16 v4, 0xc

    const/16 v3, 0xb

    const/4 v1, 0x0

    .line 274
    if-nez p0, :cond_1

    .line 275
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, " -- null --\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 289
    :cond_0
    :goto_0
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 290
    return-void

    .line 277
    :cond_1
    iget v0, p0, Lcom/ibm/icu/text/RBBINode;->fSerialNum:I

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 278
    sget-object v0, Lcom/ibm/icu/text/RBBINode;->nodeTypeNames:[Ljava/lang/String;

    iget v2, p0, Lcom/ibm/icu/text/RBBINode;->fType:I

    aget-object v0, v0, v2

    invoke-static {v0, v3}, Lcom/ibm/icu/text/RBBINode;->printString(Ljava/lang/String;I)V

    .line 279
    iget-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0, v3}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 280
    iget-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v0, v3}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 281
    iget-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    if-nez v0, :cond_4

    :goto_3
    invoke-static {v1, v4}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 282
    iget v0, p0, Lcom/ibm/icu/text/RBBINode;->fFirstPos:I

    invoke-static {v0, v4}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 283
    iget v0, p0, Lcom/ibm/icu/text/RBBINode;->fVal:I

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/ibm/icu/text/RBBINode;->printInt(II)V

    .line 285
    iget v0, p0, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 286
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/ibm/icu/text/RBBINode;->fText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_0

    .line 279
    :cond_2
    iget-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    iget v0, v0, Lcom/ibm/icu/text/RBBINode;->fSerialNum:I

    goto :goto_1

    .line 280
    :cond_3
    iget-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget v0, v0, Lcom/ibm/icu/text/RBBINode;->fSerialNum:I

    goto :goto_2

    .line 281
    :cond_4
    iget-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iget v1, v0, Lcom/ibm/icu/text/RBBINode;->fSerialNum:I

    goto :goto_3
.end method

.method static printString(Ljava/lang/String;I)V
    .locals 3
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "minWidth"    # I

    .prologue
    const/16 v2, 0x20

    .line 298
    move v0, p1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_0

    .line 300
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(C)V

    .line 298
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 302
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    :goto_1
    if-ge v0, p1, :cond_1

    .line 303
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(C)V

    .line 302
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 305
    :cond_1
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, p0}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 306
    return-void
.end method


# virtual methods
.method cloneTree()Lcom/ibm/icu/text/RBBINode;
    .locals 3

    .prologue
    .line 151
    iget v1, p0, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 154
    iget-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RBBINode;->cloneTree()Lcom/ibm/icu/text/RBBINode;

    move-result-object v0

    .line 168
    .local v0, "n":Lcom/ibm/icu/text/RBBINode;
    :cond_0
    :goto_0
    return-object v0

    .line 155
    .end local v0    # "n":Lcom/ibm/icu/text/RBBINode;
    :cond_1
    iget v1, p0, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 156
    move-object v0, p0

    .line 157
    .restart local v0    # "n":Lcom/ibm/icu/text/RBBINode;
    goto :goto_0

    .line 158
    .end local v0    # "n":Lcom/ibm/icu/text/RBBINode;
    :cond_2
    new-instance v0, Lcom/ibm/icu/text/RBBINode;

    invoke-direct {v0, p0}, Lcom/ibm/icu/text/RBBINode;-><init>(Lcom/ibm/icu/text/RBBINode;)V

    .line 159
    .restart local v0    # "n":Lcom/ibm/icu/text/RBBINode;
    iget-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    if-eqz v1, :cond_3

    .line 160
    iget-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RBBINode;->cloneTree()Lcom/ibm/icu/text/RBBINode;

    move-result-object v1

    iput-object v1, v0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 161
    iget-object v1, v0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iput-object v0, v1, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 163
    :cond_3
    iget-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    if-eqz v1, :cond_0

    .line 164
    iget-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RBBINode;->cloneTree()Lcom/ibm/icu/text/RBBINode;

    move-result-object v1

    iput-object v1, v0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    .line 165
    iget-object v1, v0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iput-object v0, v1, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    goto :goto_0
.end method

.method findNodes(Ljava/util/List;I)V
    .locals 1
    .param p1, "dest"    # Ljava/util/List;
    .param p2, "kind"    # I

    .prologue
    .line 253
    iget v0, p0, Lcom/ibm/icu/text/RBBINode;->fType:I

    if-ne v0, p2, :cond_0

    .line 254
    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {v0, p1, p2}, Lcom/ibm/icu/text/RBBINode;->findNodes(Ljava/util/List;I)V

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    if-eqz v0, :cond_2

    .line 260
    iget-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {v0, p1, p2}, Lcom/ibm/icu/text/RBBINode;->findNodes(Ljava/util/List;I)V

    .line 262
    :cond_2
    return-void
.end method

.method flattenSets()V
    .locals 4

    .prologue
    .line 218
    iget v3, p0, Lcom/ibm/icu/text/RBBINode;->fType:I

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 220
    iget-object v3, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    if-eqz v3, :cond_0

    .line 221
    iget-object v3, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iget v3, v3, Lcom/ibm/icu/text/RBBINode;->fType:I

    if-nez v3, :cond_3

    .line 222
    iget-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 223
    .local v1, "setRefNode":Lcom/ibm/icu/text/RBBINode;
    iget-object v2, v1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 224
    .local v2, "usetNode":Lcom/ibm/icu/text/RBBINode;
    iget-object v0, v2, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 225
    .local v0, "replTree":Lcom/ibm/icu/text/RBBINode;
    invoke-virtual {v0}, Lcom/ibm/icu/text/RBBINode;->cloneTree()Lcom/ibm/icu/text/RBBINode;

    move-result-object v3

    iput-object v3, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 226
    iget-object v3, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iput-object p0, v3, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 232
    .end local v0    # "replTree":Lcom/ibm/icu/text/RBBINode;
    .end local v1    # "setRefNode":Lcom/ibm/icu/text/RBBINode;
    .end local v2    # "usetNode":Lcom/ibm/icu/text/RBBINode;
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    if-eqz v3, :cond_1

    .line 233
    iget-object v3, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iget v3, v3, Lcom/ibm/icu/text/RBBINode;->fType:I

    if-nez v3, :cond_4

    .line 234
    iget-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    .line 235
    .restart local v1    # "setRefNode":Lcom/ibm/icu/text/RBBINode;
    iget-object v2, v1, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 236
    .restart local v2    # "usetNode":Lcom/ibm/icu/text/RBBINode;
    iget-object v0, v2, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 237
    .restart local v0    # "replTree":Lcom/ibm/icu/text/RBBINode;
    invoke-virtual {v0}, Lcom/ibm/icu/text/RBBINode;->cloneTree()Lcom/ibm/icu/text/RBBINode;

    move-result-object v3

    iput-object v3, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    .line 238
    iget-object v3, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iput-object p0, v3, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 244
    .end local v0    # "replTree":Lcom/ibm/icu/text/RBBINode;
    .end local v1    # "setRefNode":Lcom/ibm/icu/text/RBBINode;
    .end local v2    # "usetNode":Lcom/ibm/icu/text/RBBINode;
    :cond_1
    :goto_2
    return-void

    .line 218
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 228
    :cond_3
    iget-object v3, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {v3}, Lcom/ibm/icu/text/RBBINode;->flattenSets()V

    goto :goto_1

    .line 241
    :cond_4
    iget-object v3, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {v3}, Lcom/ibm/icu/text/RBBINode;->flattenSets()V

    goto :goto_2
.end method

.method flattenVariables()Lcom/ibm/icu/text/RBBINode;
    .locals 3

    .prologue
    .line 192
    iget v1, p0, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 193
    iget-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RBBINode;->cloneTree()Lcom/ibm/icu/text/RBBINode;

    move-result-object v0

    .line 206
    :goto_0
    return-object v0

    .line 198
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    if-eqz v1, :cond_1

    .line 199
    iget-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RBBINode;->flattenVariables()Lcom/ibm/icu/text/RBBINode;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    .line 200
    iget-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    iput-object p0, v1, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    .line 202
    :cond_1
    iget-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    if-eqz v1, :cond_2

    .line 203
    iget-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {v1}, Lcom/ibm/icu/text/RBBINode;->flattenVariables()Lcom/ibm/icu/text/RBBINode;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    .line 204
    iget-object v1, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    iput-object p0, v1, Lcom/ibm/icu/text/RBBINode;->fParent:Lcom/ibm/icu/text/RBBINode;

    :cond_2
    move-object v0, p0

    .line 206
    goto :goto_0
.end method

.method printTree(Z)V
    .locals 3
    .param p1, "printHeading"    # Z

    .prologue
    const/4 v2, 0x0

    .line 338
    if-eqz p1, :cond_0

    .line 339
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "-------------------------------------------------------------------"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 340
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v1, "    Serial       type     Parent  LeftChild  RightChild    position  value"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 342
    :cond_0
    invoke-static {p0}, Lcom/ibm/icu/text/RBBINode;->printNode(Lcom/ibm/icu/text/RBBINode;)V

    .line 345
    iget v0, p0, Lcom/ibm/icu/text/RBBINode;->fType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    .line 346
    iget-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    if-eqz v0, :cond_1

    .line 347
    iget-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fLeftChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/RBBINode;->printTree(Z)V

    .line 350
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    if-eqz v0, :cond_2

    .line 351
    iget-object v0, p0, Lcom/ibm/icu/text/RBBINode;->fRightChild:Lcom/ibm/icu/text/RBBINode;

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/RBBINode;->printTree(Z)V

    .line 354
    :cond_2
    return-void
.end method
