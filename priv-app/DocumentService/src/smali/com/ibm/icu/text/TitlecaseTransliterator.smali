.class Lcom/ibm/icu/text/TitlecaseTransliterator;
.super Lcom/ibm/icu/text/Transliterator;
.source "TitlecaseTransliterator.java"


# static fields
.field static final _ID:Ljava/lang/String; = "Any-Title"


# instance fields
.field private csp:Lcom/ibm/icu/impl/UCaseProps;

.field private iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

.field private locCache:[I

.field private locale:Lcom/ibm/icu/util/ULocale;

.field private result:Ljava/lang/StringBuffer;


# direct methods
.method public constructor <init>(Lcom/ibm/icu/util/ULocale;)V
    .locals 4
    .param p1, "loc"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 51
    const-string/jumbo v1, "Any-Title"

    invoke-direct {p0, v1, v3}, Lcom/ibm/icu/text/Transliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 52
    iput-object p1, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->locale:Lcom/ibm/icu/util/ULocale;

    .line 54
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/TitlecaseTransliterator;->setMaximumContextLength(I)V

    .line 56
    :try_start_0
    invoke-static {}, Lcom/ibm/icu/impl/UCaseProps;->getSingleton()Lcom/ibm/icu/impl/UCaseProps;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->csp:Lcom/ibm/icu/impl/UCaseProps;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :goto_0
    new-instance v1, Lcom/ibm/icu/text/ReplaceableContextIterator;

    invoke-direct {v1}, Lcom/ibm/icu/text/ReplaceableContextIterator;-><init>()V

    iput-object v1, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    .line 61
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v1, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->result:Ljava/lang/StringBuffer;

    .line 62
    const/4 v1, 0x1

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->locCache:[I

    .line 63
    iget-object v1, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->locCache:[I

    aput v2, v1, v2

    .line 64
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Ljava/io/IOException;
    iput-object v3, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->csp:Lcom/ibm/icu/impl/UCaseProps;

    goto :goto_0
.end method

.method static register()V
    .locals 3

    .prologue
    .line 31
    const-string/jumbo v0, "Any-Title"

    new-instance v1, Lcom/ibm/icu/text/TitlecaseTransliterator$1;

    invoke-direct {v1}, Lcom/ibm/icu/text/TitlecaseTransliterator$1;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 37
    const-string/jumbo v0, "Title"

    const-string/jumbo v1, "Lower"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/TitlecaseTransliterator;->registerSpecialInverse(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 38
    return-void
.end method


# virtual methods
.method protected handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V
    .locals 11
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "offsets"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "isIncremental"    # Z

    .prologue
    const/4 v10, 0x0

    .line 78
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    iget v2, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    if-lt v0, v2, :cond_0

    .line 158
    :goto_0
    return-void

    .line 87
    :cond_0
    const/4 v7, 0x1

    .line 94
    .local v7, "doTitle":Z
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    add-int/lit8 v8, v0, -0x1

    .local v8, "start":I
    :goto_1
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    if-lt v8, v0, :cond_1

    .line 95
    invoke-interface {p1, v8}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v1

    .line 96
    .local v1, "c":I
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->csp:Lcom/ibm/icu/impl/UCaseProps;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/UCaseProps;->getTypeOrIgnorable(I)I

    move-result v9

    .line 97
    .local v9, "type":I
    if-lez v9, :cond_3

    .line 98
    const/4 v7, 0x0

    .line 110
    .end local v1    # "c":I
    .end local v9    # "type":I
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/ReplaceableContextIterator;->setText(Lcom/ibm/icu/text/Replaceable;)V

    .line 111
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    iget v2, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/ReplaceableContextIterator;->setIndex(I)V

    .line 112
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    iget v2, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/ReplaceableContextIterator;->setLimit(I)V

    .line 113
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    iget v2, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    iget v3, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    invoke-virtual {v0, v2, v3}, Lcom/ibm/icu/text/ReplaceableContextIterator;->setContextLimits(II)V

    .line 115
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->result:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 121
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/ReplaceableContextIterator;->nextCaseMapCP()I

    move-result v1

    .restart local v1    # "c":I
    if-ltz v1, :cond_8

    .line 122
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->csp:Lcom/ibm/icu/impl/UCaseProps;

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/UCaseProps;->getTypeOrIgnorable(I)I

    move-result v9

    .line 123
    .restart local v9    # "type":I
    if-ltz v9, :cond_2

    .line 124
    if-eqz v7, :cond_4

    .line 125
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->csp:Lcom/ibm/icu/impl/UCaseProps;

    iget-object v2, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    iget-object v3, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->result:Ljava/lang/StringBuffer;

    iget-object v4, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->locale:Lcom/ibm/icu/util/ULocale;

    iget-object v5, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->locCache:[I

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/impl/UCaseProps;->toFullTitle(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[I)I

    move-result v1

    .line 129
    :goto_3
    if-nez v9, :cond_5

    const/4 v7, 0x1

    .line 131
    :goto_4
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/ReplaceableContextIterator;->didReachLimit()Z

    move-result v0

    if-eqz v0, :cond_6

    if-eqz p3, :cond_6

    .line 134
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/ReplaceableContextIterator;->getCaseMapCPStart()I

    move-result v0

    iput v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    goto :goto_0

    .line 100
    :cond_3
    if-eqz v9, :cond_1

    .line 94
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v0

    sub-int/2addr v8, v0

    goto :goto_1

    .line 127
    :cond_4
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->csp:Lcom/ibm/icu/impl/UCaseProps;

    iget-object v2, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    iget-object v3, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->result:Ljava/lang/StringBuffer;

    iget-object v4, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->locale:Lcom/ibm/icu/util/ULocale;

    iget-object v5, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->locCache:[I

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/impl/UCaseProps;->toFullLower(ILcom/ibm/icu/impl/UCaseProps$ContextIterator;Ljava/lang/StringBuffer;Lcom/ibm/icu/util/ULocale;[I)I

    move-result v1

    goto :goto_3

    :cond_5
    move v7, v10

    .line 129
    goto :goto_4

    .line 139
    :cond_6
    if-ltz v1, :cond_2

    .line 142
    const/16 v0, 0x1f

    if-gt v1, v0, :cond_7

    .line 144
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    iget-object v2, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->result:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/ReplaceableContextIterator;->replace(Ljava/lang/String;)I

    move-result v6

    .line 145
    .local v6, "delta":I
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->result:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 151
    :goto_5
    if-eqz v6, :cond_2

    .line 152
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    add-int/2addr v0, v6

    iput v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 153
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    add-int/2addr v0, v6

    iput v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    goto :goto_2

    .line 148
    .end local v6    # "delta":I
    :cond_7
    iget-object v0, p0, Lcom/ibm/icu/text/TitlecaseTransliterator;->iter:Lcom/ibm/icu/text/ReplaceableContextIterator;

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/ibm/icu/text/ReplaceableContextIterator;->replace(Ljava/lang/String;)I

    move-result v6

    .restart local v6    # "delta":I
    goto :goto_5

    .line 157
    .end local v6    # "delta":I
    .end local v9    # "type":I
    :cond_8
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    iput v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    goto/16 :goto_0
.end method
