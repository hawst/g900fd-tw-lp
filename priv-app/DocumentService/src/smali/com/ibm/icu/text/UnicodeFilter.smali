.class public abstract Lcom/ibm/icu/text/UnicodeFilter;
.super Ljava/lang/Object;
.source "UnicodeFilter.java"

# interfaces
.implements Lcom/ibm/icu/text/UnicodeMatcher;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract contains(I)Z
.end method

.method public matches(Lcom/ibm/icu/text/Replaceable;[IIZ)I
    .locals 5
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "offset"    # [I
    .param p3, "limit"    # I
    .param p4, "incremental"    # Z

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 38
    aget v3, p2, v2

    if-ge v3, p3, :cond_1

    aget v3, p2, v2

    invoke-interface {p1, v3}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v0

    .local v0, "c":I
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeFilter;->contains(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 40
    aget v3, p2, v2

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v4

    add-int/2addr v3, v4

    aput v3, p2, v2

    .line 57
    .end local v0    # "c":I
    :cond_0
    :goto_0
    return v1

    .line 43
    :cond_1
    aget v3, p2, v2

    if-le v3, p3, :cond_2

    aget v3, p2, v2

    invoke-interface {p1, v3}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v0

    .restart local v0    # "c":I
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UnicodeFilter;->contains(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 48
    aget v3, p2, v2

    add-int/lit8 v3, v3, -0x1

    aput v3, p2, v2

    .line 49
    aget v3, p2, v2

    if-ltz v3, :cond_0

    .line 50
    aget v3, p2, v2

    aget v4, p2, v2

    invoke-interface {p1, v4}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v4

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v3, v4

    aput v3, p2, v2

    goto :goto_0

    .line 54
    .end local v0    # "c":I
    :cond_2
    if-eqz p4, :cond_3

    aget v1, p2, v2

    if-ne v1, p3, :cond_3

    .line 55
    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    move v1, v2

    .line 57
    goto :goto_0
.end method
