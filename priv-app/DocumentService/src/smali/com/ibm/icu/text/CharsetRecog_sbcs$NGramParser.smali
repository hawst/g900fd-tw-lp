.class Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;
.super Ljava/lang/Object;
.source "CharsetRecog_sbcs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CharsetRecog_sbcs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "NGramParser"
.end annotation


# static fields
.field private static final N_GRAM_MASK:I = 0xffffff


# instance fields
.field private byteIndex:I

.field private byteMap:[B

.field private hitCount:I

.field private ngram:I

.field private ngramCount:I

.field private ngramList:[I


# direct methods
.method public constructor <init>([I[B)V
    .locals 1
    .param p1, "theNgramList"    # [I
    .param p2, "theByteMap"    # [B

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput v0, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->byteIndex:I

    .line 35
    iput v0, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->ngram:I

    .line 45
    iput-object p1, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->ngramList:[I

    .line 46
    iput-object p2, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->byteMap:[B

    .line 48
    iput v0, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->ngram:I

    .line 50
    iput v0, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->hitCount:I

    iput v0, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->ngramCount:I

    .line 51
    return-void
.end method

.method private addByte(I)V
    .locals 2
    .param p1, "b"    # I

    .prologue
    .line 107
    iget v0, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->ngram:I

    shl-int/lit8 v0, v0, 0x8

    and-int/lit16 v1, p1, 0xff

    add-int/2addr v0, v1

    const v1, 0xffffff

    and-int/2addr v0, v1

    iput v0, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->ngram:I

    .line 108
    iget v0, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->ngram:I

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->lookup(I)V

    .line 109
    return-void
.end method

.method private lookup(I)V
    .locals 1
    .param p1, "thisNgram"    # I

    .prologue
    .line 97
    iget v0, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->ngramCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->ngramCount:I

    .line 99
    iget-object v0, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->ngramList:[I

    invoke-static {v0, p1}, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->search([II)I

    move-result v0

    if-ltz v0, :cond_0

    .line 100
    iget v0, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->hitCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->hitCount:I

    .line 103
    :cond_0
    return-void
.end method

.method private nextByte(Lcom/ibm/icu/text/CharsetDetector;)I
    .locals 3
    .param p1, "det"    # Lcom/ibm/icu/text/CharsetDetector;

    .prologue
    .line 113
    iget v0, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->byteIndex:I

    iget v1, p1, Lcom/ibm/icu/text/CharsetDetector;->fInputLen:I

    if-lt v0, v1, :cond_0

    .line 114
    const/4 v0, -0x1

    .line 117
    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Lcom/ibm/icu/text/CharsetDetector;->fInputBytes:[B

    iget v1, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->byteIndex:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->byteIndex:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method private static search([II)I
    .locals 2
    .param p0, "table"    # [I
    .param p1, "value"    # I

    .prologue
    .line 58
    const/4 v0, 0x0

    .line 60
    .local v0, "index":I
    const/16 v1, 0x20

    aget v1, p0, v1

    if-gt v1, p1, :cond_0

    .line 61
    add-int/lit8 v0, v0, 0x20

    .line 64
    :cond_0
    add-int/lit8 v1, v0, 0x10

    aget v1, p0, v1

    if-gt v1, p1, :cond_1

    .line 65
    add-int/lit8 v0, v0, 0x10

    .line 68
    :cond_1
    add-int/lit8 v1, v0, 0x8

    aget v1, p0, v1

    if-gt v1, p1, :cond_2

    .line 69
    add-int/lit8 v0, v0, 0x8

    .line 72
    :cond_2
    add-int/lit8 v1, v0, 0x4

    aget v1, p0, v1

    if-gt v1, p1, :cond_3

    .line 73
    add-int/lit8 v0, v0, 0x4

    .line 76
    :cond_3
    add-int/lit8 v1, v0, 0x2

    aget v1, p0, v1

    if-gt v1, p1, :cond_4

    .line 77
    add-int/lit8 v0, v0, 0x2

    .line 80
    :cond_4
    add-int/lit8 v1, v0, 0x1

    aget v1, p0, v1

    if-gt v1, p1, :cond_5

    .line 81
    add-int/lit8 v0, v0, 0x1

    .line 84
    :cond_5
    aget v1, p0, v0

    if-le v1, p1, :cond_6

    .line 85
    add-int/lit8 v0, v0, -0x1

    .line 88
    :cond_6
    if-ltz v0, :cond_7

    aget v1, p0, v0

    if-eq v1, p1, :cond_8

    .line 89
    :cond_7
    const/4 v0, -0x1

    .line 92
    .end local v0    # "index":I
    :cond_8
    return v0
.end method


# virtual methods
.method public parse(Lcom/ibm/icu/text/CharsetDetector;)I
    .locals 10
    .param p1, "det"    # Lcom/ibm/icu/text/CharsetDetector;

    .prologue
    const/16 v6, 0x20

    .line 123
    const/4 v1, 0x0

    .line 125
    .local v1, "ignoreSpace":Z
    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->nextByte(Lcom/ibm/icu/text/CharsetDetector;)I

    move-result v0

    .local v0, "b":I
    if-ltz v0, :cond_4

    .line 126
    iget-object v3, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->byteMap:[B

    aget-byte v2, v3, v0

    .line 129
    .local v2, "mb":B
    if-eqz v2, :cond_0

    .line 130
    if-ne v2, v6, :cond_1

    if-nez v1, :cond_2

    .line 131
    :cond_1
    invoke-direct {p0, v2}, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->addByte(I)V

    .line 134
    :cond_2
    if-ne v2, v6, :cond_3

    const/4 v1, 0x1

    :goto_1
    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 139
    .end local v2    # "mb":B
    :cond_4
    invoke-direct {p0, v6}, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->addByte(I)V

    .line 141
    iget v3, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->hitCount:I

    int-to-double v6, v3

    iget v3, p0, Lcom/ibm/icu/text/CharsetRecog_sbcs$NGramParser;->ngramCount:I

    int-to-double v8, v3

    div-double v4, v6, v8

    .line 149
    .local v4, "rawPercent":D
    const-wide v6, 0x3fd51eb851eb851fL    # 0.33

    cmpl-double v3, v4, v6

    if-lez v3, :cond_5

    .line 150
    const/16 v3, 0x62

    .line 153
    :goto_2
    return v3

    :cond_5
    const-wide v6, 0x4072c00000000000L    # 300.0

    mul-double/2addr v6, v4

    double-to-int v3, v6

    goto :goto_2
.end method
