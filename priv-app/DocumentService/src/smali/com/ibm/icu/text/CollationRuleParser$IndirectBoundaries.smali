.class Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;
.super Ljava/lang/Object;
.source "CollationRuleParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CollationRuleParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IndirectBoundaries"
.end annotation


# instance fields
.field m_limitCE_:I

.field m_limitContCE_:I

.field m_startCE_:I

.field m_startContCE_:I


# direct methods
.method constructor <init>([I[I)V
    .locals 3
    .param p1, "startce"    # [I
    .param p2, "limitce"    # [I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331
    aget v0, p1, v1

    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_startCE_:I

    .line 332
    aget v0, p1, v2

    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_startContCE_:I

    .line 333
    if-eqz p2, :cond_0

    .line 334
    aget v0, p2, v1

    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_limitCE_:I

    .line 335
    aget v0, p2, v2

    iput v0, p0, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_limitContCE_:I

    .line 341
    :goto_0
    return-void

    .line 338
    :cond_0
    iput v1, p0, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_limitCE_:I

    .line 339
    iput v1, p0, Lcom/ibm/icu/text/CollationRuleParser$IndirectBoundaries;->m_limitContCE_:I

    goto :goto_0
.end method
