.class public final Lcom/ibm/icu/text/UTF16;
.super Ljava/lang/Object;
.source "UTF16.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/UTF16$StringComparator;
    }
.end annotation


# static fields
.field public static final CODEPOINT_MAX_VALUE:I = 0x10ffff

.field public static final CODEPOINT_MIN_VALUE:I = 0x0

.field private static final LEAD_SURROGATE_BITMASK:I = -0x400

.field private static final LEAD_SURROGATE_BITS:I = 0xd800

.field public static final LEAD_SURROGATE_BOUNDARY:I = 0x2

.field public static final LEAD_SURROGATE_MAX_VALUE:I = 0xdbff

.field public static final LEAD_SURROGATE_MIN_VALUE:I = 0xd800

.field private static final LEAD_SURROGATE_OFFSET_:I = 0xd7c0

.field private static final LEAD_SURROGATE_SHIFT_:I = 0xa

.field public static final SINGLE_CHAR_BOUNDARY:I = 0x1

.field public static final SUPPLEMENTARY_MIN_VALUE:I = 0x10000

.field private static final SURROGATE_BITMASK:I = -0x800

.field private static final SURROGATE_BITS:I = 0xd800

.field public static final SURROGATE_MAX_VALUE:I = 0xdfff

.field public static final SURROGATE_MIN_VALUE:I = 0xd800

.field private static final TRAIL_SURROGATE_BITMASK:I = -0x400

.field private static final TRAIL_SURROGATE_BITS:I = 0xdc00

.field public static final TRAIL_SURROGATE_BOUNDARY:I = 0x5

.field private static final TRAIL_SURROGATE_MASK_:I = 0x3ff

.field public static final TRAIL_SURROGATE_MAX_VALUE:I = 0xdfff

.field public static final TRAIL_SURROGATE_MIN_VALUE:I = 0xdc00


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    return-void
.end method

.method private static _charAt(Ljava/lang/CharSequence;IC)I
    .locals 5
    .param p0, "source"    # Ljava/lang/CharSequence;
    .param p1, "offset16"    # I
    .param p2, "single"    # C

    .prologue
    const v4, 0xdfff

    const v3, 0xdbff

    .line 291
    if-le p2, v4, :cond_1

    .line 319
    .end local p2    # "single":C
    :cond_0
    :goto_0
    return p2

    .line 299
    .restart local p2    # "single":C
    :cond_1
    if-gt p2, v3, :cond_2

    .line 300
    add-int/lit8 p1, p1, 0x1

    .line 301
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-eq v2, p1, :cond_0

    .line 302
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 303
    .local v1, "trail":C
    const v2, 0xdc00

    if-lt v1, v2, :cond_0

    if-gt v1, v4, :cond_0

    .line 305
    invoke-static {p2, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result p2

    goto :goto_0

    .line 309
    .end local v1    # "trail":C
    :cond_2
    add-int/lit8 p1, p1, -0x1

    .line 310
    if-ltz p1, :cond_0

    .line 312
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 313
    .local v0, "lead":C
    const v2, 0xd800

    if-lt v0, v2, :cond_0

    if-gt v0, v3, :cond_0

    .line 315
    invoke-static {v0, p2}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result p2

    goto :goto_0
.end method

.method private static _charAt(Ljava/lang/String;IC)I
    .locals 5
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "offset16"    # I
    .param p2, "single"    # C

    .prologue
    const v4, 0xdfff

    const v3, 0xdbff

    .line 232
    if-le p2, v4, :cond_1

    .line 258
    .end local p2    # "single":C
    :cond_0
    :goto_0
    return p2

    .line 240
    .restart local p2    # "single":C
    :cond_1
    if-gt p2, v3, :cond_2

    .line 241
    add-int/lit8 p1, p1, 0x1

    .line 242
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v2, p1, :cond_0

    .line 243
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 244
    .local v1, "trail":C
    const v2, 0xdc00

    if-lt v1, v2, :cond_0

    if-gt v1, v4, :cond_0

    .line 245
    invoke-static {p2, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result p2

    goto :goto_0

    .line 249
    .end local v1    # "trail":C
    :cond_2
    add-int/lit8 p1, p1, -0x1

    .line 250
    if-ltz p1, :cond_0

    .line 252
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 253
    .local v0, "lead":C
    const v2, 0xd800

    if-lt v0, v2, :cond_0

    if-gt v0, v3, :cond_0

    .line 254
    invoke-static {v0, p2}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result p2

    goto :goto_0
.end method

.method public static append([CII)I
    .locals 3
    .param p0, "target"    # [C
    .param p1, "limit"    # I
    .param p2, "char32"    # I

    .prologue
    .line 1155
    if-ltz p2, :cond_0

    const v1, 0x10ffff

    if-le p2, v1, :cond_1

    .line 1156
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Illegal codepoint"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1159
    :cond_1
    const/high16 v1, 0x10000

    if-lt p2, v1, :cond_2

    .line 1160
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "limit":I
    .local v0, "limit":I
    invoke-static {p2}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v1

    aput-char v1, p0, p1

    .line 1161
    add-int/lit8 p1, v0, 0x1

    .end local v0    # "limit":I
    .restart local p1    # "limit":I
    invoke-static {p2}, Lcom/ibm/icu/text/UTF16;->getTrailSurrogate(I)C

    move-result v1

    aput-char v1, p0, v0

    .line 1165
    :goto_0
    return p1

    .line 1163
    :cond_2
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "limit":I
    .restart local v0    # "limit":I
    int-to-char v1, p2

    aput-char v1, p0, p1

    move p1, v0

    .end local v0    # "limit":I
    .restart local p1    # "limit":I
    goto :goto_0
.end method

.method public static append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;
    .locals 3
    .param p0, "target"    # Ljava/lang/StringBuffer;
    .param p1, "char32"    # I

    .prologue
    .line 1107
    if-ltz p1, :cond_0

    const v0, 0x10ffff

    if-le p1, v0, :cond_1

    .line 1108
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Illegal codepoint: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1112
    :cond_1
    const/high16 v0, 0x10000

    if-lt p1, v0, :cond_2

    .line 1113
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1114
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->getTrailSurrogate(I)C

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1118
    :goto_0
    return-object p0

    .line 1116
    :cond_2
    int-to-char v0, p1

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public static appendCodePoint(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;
    .locals 1
    .param p0, "target"    # Ljava/lang/StringBuffer;
    .param p1, "cp"    # I

    .prologue
    .line 1135
    invoke-static {p0, p1}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static bounds(Ljava/lang/String;I)I
    .locals 2
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "offset16"    # I

    .prologue
    .line 527
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 528
    .local v0, "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 529
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 530
    add-int/lit8 p1, p1, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge p1, v1, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 531
    const/4 v1, 0x2

    .line 541
    :goto_0
    return v1

    .line 535
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 536
    if-ltz p1, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 537
    const/4 v1, 0x5

    goto :goto_0

    .line 541
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static bounds(Ljava/lang/StringBuffer;I)I
    .locals 2
    .param p0, "source"    # Ljava/lang/StringBuffer;
    .param p1, "offset16"    # I

    .prologue
    .line 566
    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 567
    .local v0, "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 568
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 569
    add-int/lit8 p1, p1, 0x1

    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-ge p1, v1, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 570
    const/4 v1, 0x2

    .line 580
    :goto_0
    return v1

    .line 574
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 575
    if-ltz p1, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 576
    const/4 v1, 0x5

    goto :goto_0

    .line 580
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static bounds([CIII)I
    .locals 2
    .param p0, "source"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "offset16"    # I

    .prologue
    .line 611
    add-int/2addr p3, p1

    .line 612
    if-lt p3, p1, :cond_0

    if-lt p3, p2, :cond_1

    .line 613
    :cond_0
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v1, p3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v1

    .line 615
    :cond_1
    aget-char v0, p0, p3

    .line 616
    .local v0, "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 617
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 618
    add-int/lit8 p3, p3, 0x1

    .line 619
    if-ge p3, p2, :cond_3

    aget-char v1, p0, p3

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 620
    const/4 v1, 0x2

    .line 629
    :goto_0
    return v1

    .line 623
    :cond_2
    add-int/lit8 p3, p3, -0x1

    .line 624
    if-lt p3, p1, :cond_3

    aget-char v1, p0, p3

    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 625
    const/4 v1, 0x5

    goto :goto_0

    .line 629
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static charAt(Lcom/ibm/icu/text/Replaceable;I)I
    .locals 4
    .param p0, "source"    # Lcom/ibm/icu/text/Replaceable;
    .param p1, "offset16"    # I

    .prologue
    .line 455
    if-ltz p1, :cond_0

    invoke-interface {p0}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v3

    if-lt p1, v3, :cond_1

    .line 456
    :cond_0
    new-instance v3, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v3, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v3

    .line 459
    :cond_1
    invoke-interface {p0, p1}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v1

    .line 460
    .local v1, "single":C
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v3

    if-nez v3, :cond_3

    .line 485
    .end local v1    # "single":C
    :cond_2
    :goto_0
    return v1

    .line 468
    .restart local v1    # "single":C
    :cond_3
    const v3, 0xdbff

    if-gt v1, v3, :cond_4

    .line 469
    add-int/lit8 p1, p1, 0x1

    .line 470
    invoke-interface {p0}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v3

    if-eq v3, p1, :cond_2

    .line 471
    invoke-interface {p0, p1}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v2

    .line 472
    .local v2, "trail":C
    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 473
    invoke-static {v1, v2}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v1

    goto :goto_0

    .line 476
    .end local v2    # "trail":C
    :cond_4
    add-int/lit8 p1, p1, -0x1

    .line 477
    if-ltz p1, :cond_2

    .line 479
    invoke-interface {p0, p1}, Lcom/ibm/icu/text/Replaceable;->charAt(I)C

    move-result v0

    .line 480
    .local v0, "lead":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 481
    invoke-static {v0, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v1

    goto :goto_0
.end method

.method public static charAt(Ljava/lang/CharSequence;I)I
    .locals 2
    .param p0, "source"    # Ljava/lang/CharSequence;
    .param p1, "offset16"    # I

    .prologue
    .line 283
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 284
    .local v0, "single":C
    const v1, 0xd800

    if-ge v0, v1, :cond_0

    .line 287
    .end local v0    # "single":C
    :goto_0
    return v0

    .restart local v0    # "single":C
    :cond_0
    invoke-static {p0, p1, v0}, Lcom/ibm/icu/text/UTF16;->_charAt(Ljava/lang/CharSequence;IC)I

    move-result v0

    goto :goto_0
.end method

.method public static charAt(Ljava/lang/String;I)I
    .locals 2
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "offset16"    # I

    .prologue
    .line 224
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 225
    .local v0, "single":C
    const v1, 0xd800

    if-ge v0, v1, :cond_0

    .line 228
    .end local v0    # "single":C
    :goto_0
    return v0

    .restart local v0    # "single":C
    :cond_0
    invoke-static {p0, p1, v0}, Lcom/ibm/icu/text/UTF16;->_charAt(Ljava/lang/String;IC)I

    move-result v0

    goto :goto_0
.end method

.method public static charAt(Ljava/lang/StringBuffer;I)I
    .locals 4
    .param p0, "source"    # Ljava/lang/StringBuffer;
    .param p1, "offset16"    # I

    .prologue
    .line 344
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-lt p1, v3, :cond_1

    .line 345
    :cond_0
    new-instance v3, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v3, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v3

    .line 348
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    .line 349
    .local v1, "single":C
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v3

    if-nez v3, :cond_3

    .line 374
    .end local v1    # "single":C
    :cond_2
    :goto_0
    return v1

    .line 357
    .restart local v1    # "single":C
    :cond_3
    const v3, 0xdbff

    if-gt v1, v3, :cond_4

    .line 358
    add-int/lit8 p1, p1, 0x1

    .line 359
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-eq v3, p1, :cond_2

    .line 360
    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    .line 361
    .local v2, "trail":C
    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 362
    invoke-static {v1, v2}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v1

    goto :goto_0

    .line 365
    .end local v2    # "trail":C
    :cond_4
    add-int/lit8 p1, p1, -0x1

    .line 366
    if-ltz p1, :cond_2

    .line 368
    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 369
    .local v0, "lead":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 370
    invoke-static {v0, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v1

    goto :goto_0
.end method

.method public static charAt([CIII)I
    .locals 4
    .param p0, "source"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "offset16"    # I

    .prologue
    .line 401
    add-int/2addr p3, p1

    .line 402
    if-lt p3, p1, :cond_0

    if-lt p3, p2, :cond_1

    .line 403
    :cond_0
    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v3, p3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v3

    .line 406
    :cond_1
    aget-char v1, p0, p3

    .line 407
    .local v1, "single":C
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v3

    if-nez v3, :cond_3

    .line 432
    .end local v1    # "single":C
    :cond_2
    :goto_0
    return v1

    .line 414
    .restart local v1    # "single":C
    :cond_3
    const v3, 0xdbff

    if-gt v1, v3, :cond_4

    .line 415
    add-int/lit8 p3, p3, 0x1

    .line 416
    if-ge p3, p2, :cond_2

    .line 419
    aget-char v2, p0, p3

    .line 420
    .local v2, "trail":C
    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 421
    invoke-static {v1, v2}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v1

    goto :goto_0

    .line 424
    .end local v2    # "trail":C
    :cond_4
    if-eq p3, p1, :cond_2

    .line 427
    add-int/lit8 p3, p3, -0x1

    .line 428
    aget-char v0, p0, p3

    .line 429
    .local v0, "lead":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 430
    invoke-static {v0, v1}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v1

    goto :goto_0
.end method

.method public static countCodePoint(Ljava/lang/String;)I
    .locals 1
    .param p0, "source"    # Ljava/lang/String;

    .prologue
    .line 1177
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1178
    :cond_0
    const/4 v0, 0x0

    .line 1180
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {p0, v0}, Lcom/ibm/icu/text/UTF16;->findCodePointOffset(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public static countCodePoint(Ljava/lang/StringBuffer;)I
    .locals 1
    .param p0, "source"    # Ljava/lang/StringBuffer;

    .prologue
    .line 1192
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1193
    :cond_0
    const/4 v0, 0x0

    .line 1195
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    invoke-static {p0, v0}, Lcom/ibm/icu/text/UTF16;->findCodePointOffset(Ljava/lang/StringBuffer;I)I

    move-result v0

    goto :goto_0
.end method

.method public static countCodePoint([CII)I
    .locals 1
    .param p0, "source"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    .line 1213
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    .line 1214
    :cond_0
    const/4 v0, 0x0

    .line 1216
    :goto_0
    return v0

    :cond_1
    sub-int v0, p2, p1

    invoke-static {p0, p1, p2, v0}, Lcom/ibm/icu/text/UTF16;->findCodePointOffset([CIII)I

    move-result v0

    goto :goto_0
.end method

.method public static delete([CII)I
    .locals 4
    .param p0, "target"    # [C
    .param p1, "limit"    # I
    .param p2, "offset16"    # I

    .prologue
    const/4 v3, 0x0

    .line 1631
    const/4 v0, 0x1

    .line 1632
    .local v0, "count":I
    invoke-static {p0, v3, p1, p2}, Lcom/ibm/icu/text/UTF16;->bounds([CIII)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1641
    :goto_0
    :pswitch_0
    add-int v1, p2, v0

    add-int v2, p2, v0

    sub-int v2, p1, v2

    invoke-static {p0, v1, p0, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1642
    sub-int v1, p1, v0

    aput-char v3, p0, v1

    .line 1643
    sub-int v1, p1, v0

    return v1

    .line 1634
    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    .line 1635
    goto :goto_0

    .line 1637
    :pswitch_2
    add-int/lit8 v0, v0, 0x1

    .line 1638
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 1632
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static delete(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;
    .locals 2
    .param p0, "target"    # Ljava/lang/StringBuffer;
    .param p1, "offset16"    # I

    .prologue
    .line 1601
    const/4 v0, 0x1

    .line 1602
    .local v0, "count":I
    invoke-static {p0, p1}, Lcom/ibm/icu/text/UTF16;->bounds(Ljava/lang/StringBuffer;I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1611
    :goto_0
    :pswitch_0
    add-int v1, p1, v0

    invoke-virtual {p0, p1, v1}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 1612
    return-object p0

    .line 1604
    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    .line 1605
    goto :goto_0

    .line 1607
    :pswitch_2
    add-int/lit8 v0, v0, 0x1

    .line 1608
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 1602
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static findCodePointOffset(Ljava/lang/String;I)I
    .locals 6
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "offset16"    # I

    .prologue
    .line 941
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-le p1, v5, :cond_1

    .line 942
    :cond_0
    new-instance v5, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v5, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v5

    .line 945
    :cond_1
    const/4 v3, 0x0

    .line 947
    .local v3, "result":I
    const/4 v1, 0x0

    .line 949
    .local v1, "hadLeadSurrogate":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p1, :cond_3

    .line 950
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 951
    .local v0, "ch":C
    if-eqz v1, :cond_2

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 952
    const/4 v1, 0x0

    .line 949
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 954
    :cond_2
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v1

    .line 955
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 959
    .end local v0    # "ch":C
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ne p1, v5, :cond_4

    move v4, v3

    .line 969
    .end local v3    # "result":I
    .local v4, "result":I
    :goto_2
    return v4

    .line 965
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_4
    if-eqz v1, :cond_5

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 966
    add-int/lit8 v3, v3, -0x1

    :cond_5
    move v4, v3

    .line 969
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_2
.end method

.method public static findCodePointOffset(Ljava/lang/StringBuffer;I)I
    .locals 6
    .param p0, "source"    # Ljava/lang/StringBuffer;
    .param p1, "offset16"    # I

    .prologue
    .line 998
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-le p1, v5, :cond_1

    .line 999
    :cond_0
    new-instance v5, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v5, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v5

    .line 1002
    :cond_1
    const/4 v3, 0x0

    .line 1004
    .local v3, "result":I
    const/4 v1, 0x0

    .line 1006
    .local v1, "hadLeadSurrogate":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p1, :cond_3

    .line 1007
    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 1008
    .local v0, "ch":C
    if-eqz v1, :cond_2

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1009
    const/4 v1, 0x0

    .line 1006
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1011
    :cond_2
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v1

    .line 1012
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1016
    .end local v0    # "ch":C
    :cond_3
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-ne p1, v5, :cond_4

    move v4, v3

    .line 1026
    .end local v3    # "result":I
    .local v4, "result":I
    :goto_2
    return v4

    .line 1022
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_4
    if-eqz v1, :cond_5

    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1023
    add-int/lit8 v3, v3, -0x1

    :cond_5
    move v4, v3

    .line 1026
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_2
.end method

.method public static findCodePointOffset([CIII)I
    .locals 6
    .param p0, "source"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "offset16"    # I

    .prologue
    .line 1059
    add-int/2addr p3, p1

    .line 1060
    if-le p3, p2, :cond_0

    .line 1061
    new-instance v5, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v5, p3}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v5

    .line 1064
    :cond_0
    const/4 v3, 0x0

    .line 1066
    .local v3, "result":I
    const/4 v1, 0x0

    .line 1068
    .local v1, "hadLeadSurrogate":Z
    move v2, p1

    .local v2, "i":I
    :goto_0
    if-ge v2, p3, :cond_2

    .line 1069
    aget-char v0, p0, v2

    .line 1070
    .local v0, "ch":C
    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1071
    const/4 v1, 0x0

    .line 1068
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1073
    :cond_1
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v1

    .line 1074
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1078
    .end local v0    # "ch":C
    :cond_2
    if-ne p3, p2, :cond_3

    move v4, v3

    .line 1088
    .end local v3    # "result":I
    .local v4, "result":I
    :goto_2
    return v4

    .line 1084
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_3
    if-eqz v1, :cond_4

    aget-char v5, p0, p3

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1085
    add-int/lit8 v3, v3, -0x1

    :cond_4
    move v4, v3

    .line 1088
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_2
.end method

.method public static findOffsetFromCodePoint(Ljava/lang/String;I)I
    .locals 5
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "offset32"    # I

    .prologue
    .line 821
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .local v3, "size":I
    const/4 v2, 0x0

    .local v2, "result":I
    move v1, p1

    .line 822
    .local v1, "count":I
    if-ltz p1, :cond_0

    if-le p1, v3, :cond_1

    .line 823
    :cond_0
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 825
    :cond_1
    :goto_0
    if-ge v2, v3, :cond_3

    if-lez v1, :cond_3

    .line 826
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 827
    .local v0, "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_2

    add-int/lit8 v4, v2, 0x1

    if-ge v4, v3, :cond_2

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 829
    add-int/lit8 v2, v2, 0x1

    .line 832
    :cond_2
    add-int/lit8 v1, v1, -0x1

    .line 833
    add-int/lit8 v2, v2, 0x1

    .line 834
    goto :goto_0

    .line 835
    .end local v0    # "ch":C
    :cond_3
    if-eqz v1, :cond_4

    .line 836
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 838
    :cond_4
    return v2
.end method

.method public static findOffsetFromCodePoint(Ljava/lang/StringBuffer;I)I
    .locals 5
    .param p0, "source"    # Ljava/lang/StringBuffer;
    .param p1, "offset32"    # I

    .prologue
    .line 856
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    .local v3, "size":I
    const/4 v2, 0x0

    .local v2, "result":I
    move v1, p1

    .line 857
    .local v1, "count":I
    if-ltz p1, :cond_0

    if-le p1, v3, :cond_1

    .line 858
    :cond_0
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 860
    :cond_1
    :goto_0
    if-ge v2, v3, :cond_3

    if-lez v1, :cond_3

    .line 861
    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 862
    .local v0, "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_2

    add-int/lit8 v4, v2, 0x1

    if-ge v4, v3, :cond_2

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 864
    add-int/lit8 v2, v2, 0x1

    .line 867
    :cond_2
    add-int/lit8 v1, v1, -0x1

    .line 868
    add-int/lit8 v2, v2, 0x1

    .line 869
    goto :goto_0

    .line 870
    .end local v0    # "ch":C
    :cond_3
    if-eqz v1, :cond_4

    .line 871
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 873
    :cond_4
    return v2
.end method

.method public static findOffsetFromCodePoint([CIII)I
    .locals 4
    .param p0, "source"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "offset32"    # I

    .prologue
    .line 895
    move v2, p1

    .local v2, "result":I
    move v1, p3

    .line 896
    .local v1, "count":I
    sub-int v3, p2, p1

    if-le p3, v3, :cond_0

    .line 897
    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v3, p3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v3

    .line 899
    :cond_0
    :goto_0
    if-ge v2, p2, :cond_2

    if-lez v1, :cond_2

    .line 900
    aget-char v0, p0, v2

    .line 901
    .local v0, "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_1

    add-int/lit8 v3, v2, 0x1

    if-ge v3, p2, :cond_1

    add-int/lit8 v3, v2, 0x1

    aget-char v3, p0, v3

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 903
    add-int/lit8 v2, v2, 0x1

    .line 906
    :cond_1
    add-int/lit8 v1, v1, -0x1

    .line 907
    add-int/lit8 v2, v2, 0x1

    .line 908
    goto :goto_0

    .line 909
    .end local v0    # "ch":C
    :cond_2
    if-eqz v1, :cond_3

    .line 910
    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v3, p3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v3

    .line 912
    :cond_3
    sub-int v3, v2, p1

    return v3
.end method

.method public static getCharCount(I)I
    .locals 1
    .param p0, "char32"    # I

    .prologue
    .line 499
    const/high16 v0, 0x10000

    if-ge p0, v0, :cond_0

    .line 500
    const/4 v0, 0x1

    .line 502
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static getLeadSurrogate(I)C
    .locals 2
    .param p0, "char32"    # I

    .prologue
    .line 680
    const/high16 v0, 0x10000

    if-lt p0, v0, :cond_0

    .line 681
    const v0, 0xd7c0

    shr-int/lit8 v1, p0, 0xa

    add-int/2addr v0, v1

    int-to-char v0, v0

    .line 683
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getTrailSurrogate(I)C
    .locals 2
    .param p0, "char32"    # I

    .prologue
    .line 698
    const/high16 v0, 0x10000

    if-lt p0, v0, :cond_0

    .line 699
    const v0, 0xdc00

    and-int/lit16 v1, p0, 0x3ff

    add-int/2addr v0, v1

    int-to-char v0, v0

    .line 701
    :goto_0
    return v0

    :cond_0
    int-to-char v0, p0

    goto :goto_0
.end method

.method public static hasMoreCodePointsThan(Ljava/lang/String;I)Z
    .locals 7
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "number"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2256
    if-gez p1, :cond_1

    .line 2295
    :cond_0
    :goto_0
    return v4

    .line 2259
    :cond_1
    if-nez p0, :cond_2

    move v4, v5

    .line 2260
    goto :goto_0

    .line 2262
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 2267
    .local v0, "length":I
    add-int/lit8 v6, v0, 0x1

    shr-int/lit8 v6, v6, 0x1

    if-gt v6, p1, :cond_0

    .line 2272
    sub-int v1, v0, p1

    .line 2273
    .local v1, "maxsupplementary":I
    if-gtz v1, :cond_3

    move v4, v5

    .line 2274
    goto :goto_0

    .line 2282
    :cond_3
    const/4 v2, 0x0

    .local v2, "start":I
    move v3, v2

    .line 2284
    .end local v2    # "start":I
    .local v3, "start":I
    :goto_1
    if-nez v0, :cond_4

    move v4, v5

    .line 2285
    goto :goto_0

    .line 2287
    :cond_4
    if-eqz p1, :cond_0

    .line 2290
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "start":I
    .restart local v2    # "start":I
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_5

    if-eq v2, v0, :cond_5

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2292
    add-int/lit8 v2, v2, 0x1

    .line 2293
    add-int/lit8 v1, v1, -0x1

    if-gtz v1, :cond_5

    move v4, v5

    .line 2295
    goto :goto_0

    .line 2298
    :cond_5
    add-int/lit8 p1, p1, -0x1

    move v3, v2

    .line 2299
    .end local v2    # "start":I
    .restart local v3    # "start":I
    goto :goto_1
.end method

.method public static hasMoreCodePointsThan(Ljava/lang/StringBuffer;I)Z
    .locals 7
    .param p0, "source"    # Ljava/lang/StringBuffer;
    .param p1, "number"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2393
    if-gez p1, :cond_1

    .line 2432
    :cond_0
    :goto_0
    return v4

    .line 2396
    :cond_1
    if-nez p0, :cond_2

    move v4, v5

    .line 2397
    goto :goto_0

    .line 2399
    :cond_2
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    .line 2404
    .local v0, "length":I
    add-int/lit8 v6, v0, 0x1

    shr-int/lit8 v6, v6, 0x1

    if-gt v6, p1, :cond_0

    .line 2409
    sub-int v1, v0, p1

    .line 2410
    .local v1, "maxsupplementary":I
    if-gtz v1, :cond_3

    move v4, v5

    .line 2411
    goto :goto_0

    .line 2419
    :cond_3
    const/4 v2, 0x0

    .local v2, "start":I
    move v3, v2

    .line 2421
    .end local v2    # "start":I
    .local v3, "start":I
    :goto_1
    if-nez v0, :cond_4

    move v4, v5

    .line 2422
    goto :goto_0

    .line 2424
    :cond_4
    if-eqz p1, :cond_0

    .line 2427
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "start":I
    .restart local v2    # "start":I
    invoke-virtual {p0, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_5

    if-eq v2, v0, :cond_5

    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2429
    add-int/lit8 v2, v2, 0x1

    .line 2430
    add-int/lit8 v1, v1, -0x1

    if-gtz v1, :cond_5

    move v4, v5

    .line 2432
    goto :goto_0

    .line 2435
    :cond_5
    add-int/lit8 p1, p1, -0x1

    move v3, v2

    .line 2436
    .end local v2    # "start":I
    .restart local v3    # "start":I
    goto :goto_1
.end method

.method public static hasMoreCodePointsThan([CIII)Z
    .locals 6
    .param p0, "source"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "number"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2326
    sub-int v0, p2, p1

    .line 2327
    .local v0, "length":I
    if-ltz v0, :cond_0

    if-ltz p1, :cond_0

    if-gez p2, :cond_1

    .line 2328
    :cond_0
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    const-string/jumbo v4, "Start and limit indexes should be non-negative and start <= limit"

    invoke-direct {v3, v4}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 2331
    :cond_1
    if-gez p3, :cond_3

    .line 2368
    :cond_2
    :goto_0
    return v3

    .line 2334
    :cond_3
    if-nez p0, :cond_4

    move v3, v4

    .line 2335
    goto :goto_0

    .line 2341
    :cond_4
    add-int/lit8 v5, v0, 0x1

    shr-int/lit8 v5, v5, 0x1

    if-gt v5, p3, :cond_2

    .line 2346
    sub-int v1, v0, p3

    .line 2347
    .local v1, "maxsupplementary":I
    if-gtz v1, :cond_8

    move v3, v4

    .line 2348
    goto :goto_0

    .line 2371
    :cond_5
    add-int/lit8 p3, p3, -0x1

    move v2, p1

    .line 2357
    .end local p1    # "start":I
    .local v2, "start":I
    :goto_1
    if-nez v0, :cond_6

    move p1, v2

    .end local v2    # "start":I
    .restart local p1    # "start":I
    move v3, v4

    .line 2358
    goto :goto_0

    .line 2360
    .end local p1    # "start":I
    .restart local v2    # "start":I
    :cond_6
    if-nez p3, :cond_7

    move p1, v2

    .line 2361
    .end local v2    # "start":I
    .restart local p1    # "start":I
    goto :goto_0

    .line 2363
    .end local p1    # "start":I
    .restart local v2    # "start":I
    :cond_7
    add-int/lit8 p1, v2, 0x1

    .end local v2    # "start":I
    .restart local p1    # "start":I
    aget-char v5, p0, v2

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_5

    if-eq p1, p2, :cond_5

    aget-char v5, p0, p1

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2365
    add-int/lit8 p1, p1, 0x1

    .line 2366
    add-int/lit8 v1, v1, -0x1

    if-gtz v1, :cond_5

    move v3, v4

    .line 2368
    goto :goto_0

    :cond_8
    move v2, p1

    .end local p1    # "start":I
    .restart local v2    # "start":I
    goto :goto_1
.end method

.method public static indexOf(Ljava/lang/String;I)I
    .locals 4
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "char32"    # I

    .prologue
    const/high16 v3, 0x10000

    .line 1672
    if-ltz p1, :cond_0

    const v2, 0x10ffff

    if-le p1, v2, :cond_1

    .line 1673
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Argument char32 is not a valid codepoint"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1676
    :cond_1
    const v2, 0xd800

    if-lt p1, v2, :cond_2

    const v2, 0xdfff

    if-le p1, v2, :cond_4

    if-ge p1, v3, :cond_4

    .line 1678
    :cond_2
    int-to-char v2, p1

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 1697
    :cond_3
    :goto_0
    return v1

    .line 1681
    :cond_4
    if-ge p1, v3, :cond_6

    .line 1682
    int-to-char v2, p1

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 1683
    .local v1, "result":I
    if-ltz v1, :cond_3

    .line 1684
    int-to-char v2, p1

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_5

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1686
    add-int/lit8 v2, v1, 0x1

    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/UTF16;->indexOf(Ljava/lang/String;II)I

    move-result v1

    goto :goto_0

    .line 1689
    :cond_5
    if-lez v1, :cond_3

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1690
    add-int/lit8 v2, v1, 0x1

    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/UTF16;->indexOf(Ljava/lang/String;II)I

    move-result v1

    goto :goto_0

    .line 1696
    .end local v1    # "result":I
    :cond_6
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 1697
    .local v0, "char32str":Ljava/lang/String;
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public static indexOf(Ljava/lang/String;II)I
    .locals 4
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "char32"    # I
    .param p2, "fromIndex"    # I

    .prologue
    const/high16 v3, 0x10000

    .line 1778
    if-ltz p1, :cond_0

    const v2, 0x10ffff

    if-le p1, v2, :cond_1

    .line 1779
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Argument char32 is not a valid codepoint"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1782
    :cond_1
    const v2, 0xd800

    if-lt p1, v2, :cond_2

    const v2, 0xdfff

    if-le p1, v2, :cond_4

    if-ge p1, v3, :cond_4

    .line 1784
    :cond_2
    int-to-char v2, p1

    invoke-virtual {p0, v2, p2}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 1803
    :cond_3
    :goto_0
    return v1

    .line 1787
    :cond_4
    if-ge p1, v3, :cond_6

    .line 1788
    int-to-char v2, p1

    invoke-virtual {p0, v2, p2}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 1789
    .local v1, "result":I
    if-ltz v1, :cond_3

    .line 1790
    int-to-char v2, p1

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_5

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1792
    add-int/lit8 v2, v1, 0x1

    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/UTF16;->indexOf(Ljava/lang/String;II)I

    move-result v1

    goto :goto_0

    .line 1795
    :cond_5
    if-lez v1, :cond_3

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1796
    add-int/lit8 v2, v1, 0x1

    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/UTF16;->indexOf(Ljava/lang/String;II)I

    move-result v1

    goto :goto_0

    .line 1802
    .end local v1    # "result":I
    :cond_6
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 1803
    .local v0, "char32str":Ljava/lang/String;
    invoke-virtual {p0, v0, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method public static indexOf(Ljava/lang/String;Ljava/lang/String;)I
    .locals 5
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 1728
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 1730
    .local v2, "strLength":I
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-nez v3, :cond_1

    add-int/lit8 v3, v2, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1731
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1748
    :cond_0
    :goto_0
    return v0

    .line 1734
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1735
    .local v0, "result":I
    add-int v1, v0, v2

    .line 1736
    .local v1, "resultEnd":I
    if-ltz v0, :cond_0

    .line 1738
    add-int/lit8 v3, v2, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_2

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1740
    add-int/lit8 v3, v1, 0x1

    invoke-static {p0, p1, v3}, Lcom/ibm/icu/text/UTF16;->indexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 1743
    :cond_2
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_0

    if-lez v0, :cond_0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1745
    add-int/lit8 v3, v1, 0x1

    invoke-static {p0, p1, v3}, Lcom/ibm/icu/text/UTF16;->indexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public static indexOf(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 5
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "fromIndex"    # I

    .prologue
    const/4 v4, 0x0

    .line 1837
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 1839
    .local v2, "strLength":I
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-nez v3, :cond_1

    add-int/lit8 v3, v2, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1840
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 1857
    :cond_0
    :goto_0
    return v0

    .line 1843
    :cond_1
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 1844
    .local v0, "result":I
    add-int v1, v0, v2

    .line 1845
    .local v1, "resultEnd":I
    if-ltz v0, :cond_0

    .line 1847
    add-int/lit8 v3, v2, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_2

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1849
    add-int/lit8 v3, v1, 0x1

    invoke-static {p0, p1, v3}, Lcom/ibm/icu/text/UTF16;->indexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 1852
    :cond_2
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_0

    if-lez v0, :cond_0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1854
    add-int/lit8 v3, v1, 0x1

    invoke-static {p0, p1, v3}, Lcom/ibm/icu/text/UTF16;->indexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public static insert([CIII)I
    .locals 5
    .param p0, "target"    # [C
    .param p1, "limit"    # I
    .param p2, "offset16"    # I
    .param p3, "char32"    # I

    .prologue
    const/4 v4, 0x0

    .line 1571
    invoke-static {p3}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 1572
    .local v1, "str":Ljava/lang/String;
    if-eq p2, p1, :cond_0

    invoke-static {p0, v4, p1, p2}, Lcom/ibm/icu/text/UTF16;->bounds([CIII)I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    .line 1573
    add-int/lit8 p2, p2, 0x1

    .line 1575
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .line 1576
    .local v0, "size":I
    add-int v2, p1, v0

    array-length v3, p0

    if-le v2, v3, :cond_1

    .line 1577
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    add-int v3, p2, v0

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v2

    .line 1579
    :cond_1
    add-int v2, p2, v0

    sub-int v3, p1, p2

    invoke-static {p0, p2, p0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1580
    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    aput-char v2, p0, p2

    .line 1581
    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 1582
    add-int/lit8 v2, p2, 0x1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    aput-char v3, p0, v2

    .line 1584
    :cond_2
    add-int v2, p1, v0

    return v2
.end method

.method public static insert(Ljava/lang/StringBuffer;II)Ljava/lang/StringBuffer;
    .locals 3
    .param p0, "target"    # Ljava/lang/StringBuffer;
    .param p1, "offset16"    # I
    .param p2, "char32"    # I

    .prologue
    .line 1537
    invoke-static {p2}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 1538
    .local v0, "str":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-eq p1, v1, :cond_0

    invoke-static {p0, p1}, Lcom/ibm/icu/text/UTF16;->bounds(Ljava/lang/StringBuffer;I)I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 1539
    add-int/lit8 p1, p1, 0x1

    .line 1541
    :cond_0
    invoke-virtual {p0, p1, v0}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 1542
    return-object p0
.end method

.method public static isLeadSurrogate(C)Z
    .locals 2
    .param p0, "char16"    # C

    .prologue
    .line 665
    and-int/lit16 v0, p0, -0x400

    const v1, 0xd800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSurrogate(C)Z
    .locals 2
    .param p0, "char16"    # C

    .prologue
    .line 641
    and-int/lit16 v0, p0, -0x800

    const v1, 0xd800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isTrailSurrogate(C)Z
    .locals 2
    .param p0, "char16"    # C

    .prologue
    .line 653
    and-int/lit16 v0, p0, -0x400

    const v1, 0xdc00

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static lastIndexOf(Ljava/lang/String;I)I
    .locals 4
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "char32"    # I

    .prologue
    const/high16 v3, 0x10000

    .line 1885
    if-ltz p1, :cond_0

    const v2, 0x10ffff

    if-le p1, v2, :cond_1

    .line 1886
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Argument char32 is not a valid codepoint"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1889
    :cond_1
    const v2, 0xd800

    if-lt p1, v2, :cond_2

    const v2, 0xdfff

    if-le p1, v2, :cond_4

    if-ge p1, v3, :cond_4

    .line 1891
    :cond_2
    int-to-char v2, p1

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 1910
    :cond_3
    :goto_0
    return v1

    .line 1894
    :cond_4
    if-ge p1, v3, :cond_6

    .line 1895
    int-to-char v2, p1

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 1896
    .local v1, "result":I
    if-ltz v1, :cond_3

    .line 1897
    int-to-char v2, p1

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_5

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1899
    add-int/lit8 v2, v1, -0x1

    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/UTF16;->lastIndexOf(Ljava/lang/String;II)I

    move-result v1

    goto :goto_0

    .line 1902
    :cond_5
    if-lez v1, :cond_3

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1903
    add-int/lit8 v2, v1, -0x1

    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/UTF16;->lastIndexOf(Ljava/lang/String;II)I

    move-result v1

    goto :goto_0

    .line 1909
    .end local v1    # "result":I
    :cond_6
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 1910
    .local v0, "char32str":Ljava/lang/String;
    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public static lastIndexOf(Ljava/lang/String;II)I
    .locals 4
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "char32"    # I
    .param p2, "fromIndex"    # I

    .prologue
    const/high16 v3, 0x10000

    .line 2001
    if-ltz p1, :cond_0

    const v2, 0x10ffff

    if-le p1, v2, :cond_1

    .line 2002
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Argument char32 is not a valid codepoint"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2005
    :cond_1
    const v2, 0xd800

    if-lt p1, v2, :cond_2

    const v2, 0xdfff

    if-le p1, v2, :cond_4

    if-ge p1, v3, :cond_4

    .line 2007
    :cond_2
    int-to-char v2, p1

    invoke-virtual {p0, v2, p2}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v1

    .line 2026
    :cond_3
    :goto_0
    return v1

    .line 2010
    :cond_4
    if-ge p1, v3, :cond_6

    .line 2011
    int-to-char v2, p1

    invoke-virtual {p0, v2, p2}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v1

    .line 2012
    .local v1, "result":I
    if-ltz v1, :cond_3

    .line 2013
    int-to-char v2, p1

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_5

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2015
    add-int/lit8 v2, v1, -0x1

    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/UTF16;->lastIndexOf(Ljava/lang/String;II)I

    move-result v1

    goto :goto_0

    .line 2018
    :cond_5
    if-lez v1, :cond_3

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2019
    add-int/lit8 v2, v1, -0x1

    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/UTF16;->lastIndexOf(Ljava/lang/String;II)I

    move-result v1

    goto :goto_0

    .line 2025
    .end local v1    # "result":I
    :cond_6
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 2026
    .local v0, "char32str":Ljava/lang/String;
    invoke-virtual {p0, v0, p2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method public static lastIndexOf(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1941
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 1943
    .local v1, "strLength":I
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-nez v2, :cond_1

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1944
    invoke-virtual {p0, p1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 1960
    :cond_0
    :goto_0
    return v0

    .line 1947
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 1948
    .local v0, "result":I
    if-ltz v0, :cond_0

    .line 1950
    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    add-int v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1952
    add-int/lit8 v2, v0, -0x1

    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/UTF16;->lastIndexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 1955
    :cond_2
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    if-lez v0, :cond_0

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1957
    add-int/lit8 v2, v0, -0x1

    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/UTF16;->lastIndexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public static lastIndexOf(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 4
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "fromIndex"    # I

    .prologue
    const/4 v3, 0x0

    .line 2070
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 2072
    .local v1, "strLength":I
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-nez v2, :cond_1

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2073
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result v0

    .line 2089
    :cond_0
    :goto_0
    return v0

    .line 2076
    :cond_1
    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result v0

    .line 2077
    .local v0, "result":I
    if-ltz v0, :cond_0

    .line 2079
    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    add-int v2, v0, v1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2081
    add-int/lit8 v2, v0, -0x1

    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/UTF16;->lastIndexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 2084
    :cond_2
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    if-lez v0, :cond_0

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2086
    add-int/lit8 v2, v0, -0x1

    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/UTF16;->lastIndexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public static moveCodePointOffset(Ljava/lang/String;II)I
    .locals 5
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "offset16"    # I
    .param p2, "shift32"    # I

    .prologue
    .line 1340
    move v2, p1

    .line 1341
    .local v2, "result":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 1344
    .local v3, "size":I
    if-ltz p1, :cond_0

    if-le p1, v3, :cond_1

    .line 1345
    :cond_0
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 1347
    :cond_1
    if-lez p2, :cond_4

    .line 1348
    add-int v4, p2, p1

    if-le v4, v3, :cond_2

    .line 1349
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 1351
    :cond_2
    move v1, p2

    .line 1352
    .local v1, "count":I
    :goto_0
    if-ge v2, v3, :cond_6

    if-lez v1, :cond_6

    .line 1353
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1354
    .local v0, "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_3

    add-int/lit8 v4, v2, 0x1

    if-ge v4, v3, :cond_3

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1356
    add-int/lit8 v2, v2, 0x1

    .line 1358
    :cond_3
    add-int/lit8 v1, v1, -0x1

    .line 1359
    add-int/lit8 v2, v2, 0x1

    .line 1360
    goto :goto_0

    .line 1362
    .end local v0    # "ch":C
    .end local v1    # "count":I
    :cond_4
    add-int v4, p1, p2

    if-gez v4, :cond_5

    .line 1363
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 1365
    :cond_5
    neg-int v1, p2

    .restart local v1    # "count":I
    :goto_1
    if-lez v1, :cond_6

    .line 1366
    add-int/lit8 v2, v2, -0x1

    .line 1367
    if-gez v2, :cond_7

    .line 1377
    :cond_6
    if-eqz v1, :cond_9

    .line 1378
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 1370
    :cond_7
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1371
    .restart local v0    # "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_8

    if-lez v2, :cond_8

    add-int/lit8 v4, v2, -0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1373
    add-int/lit8 v2, v2, -0x1

    .line 1365
    :cond_8
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 1380
    .end local v0    # "ch":C
    :cond_9
    return v2
.end method

.method public static moveCodePointOffset(Ljava/lang/StringBuffer;II)I
    .locals 5
    .param p0, "source"    # Ljava/lang/StringBuffer;
    .param p1, "offset16"    # I
    .param p2, "shift32"    # I

    .prologue
    .line 1398
    move v2, p1

    .line 1399
    .local v2, "result":I
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    .line 1402
    .local v3, "size":I
    if-ltz p1, :cond_0

    if-le p1, v3, :cond_1

    .line 1403
    :cond_0
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 1405
    :cond_1
    if-lez p2, :cond_4

    .line 1406
    add-int v4, p2, p1

    if-le v4, v3, :cond_2

    .line 1407
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 1409
    :cond_2
    move v1, p2

    .line 1410
    .local v1, "count":I
    :goto_0
    if-ge v2, v3, :cond_6

    if-lez v1, :cond_6

    .line 1411
    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 1412
    .local v0, "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_3

    add-int/lit8 v4, v2, 0x1

    if-ge v4, v3, :cond_3

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1414
    add-int/lit8 v2, v2, 0x1

    .line 1416
    :cond_3
    add-int/lit8 v1, v1, -0x1

    .line 1417
    add-int/lit8 v2, v2, 0x1

    .line 1418
    goto :goto_0

    .line 1420
    .end local v0    # "ch":C
    .end local v1    # "count":I
    :cond_4
    add-int v4, p1, p2

    if-gez v4, :cond_5

    .line 1421
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 1423
    :cond_5
    neg-int v1, p2

    .restart local v1    # "count":I
    :goto_1
    if-lez v1, :cond_6

    .line 1424
    add-int/lit8 v2, v2, -0x1

    .line 1425
    if-gez v2, :cond_7

    .line 1435
    :cond_6
    if-eqz v1, :cond_9

    .line 1436
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 1428
    :cond_7
    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 1429
    .restart local v0    # "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_8

    if-lez v2, :cond_8

    add-int/lit8 v4, v2, -0x1

    invoke-virtual {p0, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1431
    add-int/lit8 v2, v2, -0x1

    .line 1423
    :cond_8
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 1438
    .end local v0    # "ch":C
    :cond_9
    return v2
.end method

.method public static moveCodePointOffset([CIIII)I
    .locals 5
    .param p0, "source"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "offset16"    # I
    .param p4, "shift32"    # I

    .prologue
    .line 1462
    array-length v3, p0

    .line 1465
    .local v3, "size":I
    add-int v2, p3, p1

    .line 1466
    .local v2, "result":I
    if-ltz p1, :cond_0

    if-ge p2, p1, :cond_1

    .line 1467
    :cond_0
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p1}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 1469
    :cond_1
    if-le p2, v3, :cond_2

    .line 1470
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 1472
    :cond_2
    if-ltz p3, :cond_3

    if-le v2, p2, :cond_4

    .line 1473
    :cond_3
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p3}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 1475
    :cond_4
    if-lez p4, :cond_7

    .line 1476
    add-int v4, p4, v2

    if-le v4, v3, :cond_5

    .line 1477
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, v2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 1479
    :cond_5
    move v1, p4

    .line 1480
    .local v1, "count":I
    :goto_0
    if-ge v2, p2, :cond_9

    if-lez v1, :cond_9

    .line 1481
    aget-char v0, p0, v2

    .line 1482
    .local v0, "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_6

    add-int/lit8 v4, v2, 0x1

    if-ge v4, p2, :cond_6

    add-int/lit8 v4, v2, 0x1

    aget-char v4, p0, v4

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1484
    add-int/lit8 v2, v2, 0x1

    .line 1486
    :cond_6
    add-int/lit8 v1, v1, -0x1

    .line 1487
    add-int/lit8 v2, v2, 0x1

    .line 1488
    goto :goto_0

    .line 1490
    .end local v0    # "ch":C
    .end local v1    # "count":I
    :cond_7
    add-int v4, v2, p4

    if-ge v4, p1, :cond_8

    .line 1491
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, v2}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 1493
    :cond_8
    neg-int v1, p4

    .restart local v1    # "count":I
    :goto_1
    if-lez v1, :cond_9

    .line 1494
    add-int/lit8 v2, v2, -0x1

    .line 1495
    if-ge v2, p1, :cond_a

    .line 1504
    :cond_9
    if-eqz v1, :cond_c

    .line 1505
    new-instance v4, Ljava/lang/StringIndexOutOfBoundsException;

    invoke-direct {v4, p4}, Ljava/lang/StringIndexOutOfBoundsException;-><init>(I)V

    throw v4

    .line 1498
    :cond_a
    aget-char v0, p0, v2

    .line 1499
    .restart local v0    # "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_b

    if-le v2, p1, :cond_b

    add-int/lit8 v4, v2, -0x1

    aget-char v4, p0, v4

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1500
    add-int/lit8 v2, v2, -0x1

    .line 1493
    :cond_b
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 1507
    .end local v0    # "ch":C
    :cond_c
    sub-int/2addr v2, p1

    .line 1508
    return v2
.end method

.method public static newString([III)Ljava/lang/String;
    .locals 13
    .param p0, "codePoints"    # [I
    .param p1, "offset"    # I
    .param p2, "count"    # I

    .prologue
    const/4 v12, 0x0

    .line 2456
    if-gez p2, :cond_0

    .line 2457
    new-instance v8, Ljava/lang/IllegalArgumentException;

    invoke-direct {v8}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v8

    .line 2459
    :cond_0
    new-array v0, p2, [C

    .line 2460
    .local v0, "chars":[C
    const/4 v7, 0x0

    .line 2461
    .local v7, "w":I
    move v5, p1

    .local v5, "r":I
    add-int v2, p1, p2

    .local v2, "e":I
    :goto_0
    if-ge v5, v2, :cond_4

    .line 2462
    aget v1, p0, v5

    .line 2463
    .local v1, "cp":I
    if-ltz v1, :cond_1

    const v8, 0x10ffff

    if-le v1, v8, :cond_2

    .line 2464
    :cond_1
    new-instance v8, Ljava/lang/IllegalArgumentException;

    invoke-direct {v8}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v8

    .line 2477
    :catch_0
    move-exception v3

    .line 2478
    .local v3, "ex":Ljava/lang/IndexOutOfBoundsException;
    array-length v8, p0

    int-to-double v8, v8

    add-int/lit8 v10, v7, 0x2

    int-to-double v10, v10

    mul-double/2addr v8, v10

    sub-int v10, v5, p1

    add-int/lit8 v10, v10, 0x1

    int-to-double v10, v10

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v4, v8

    .line 2480
    .local v4, "newlen":I
    new-array v6, v4, [C

    .line 2481
    .local v6, "temp":[C
    invoke-static {v0, v12, v6, v12, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2482
    move-object v0, v6

    .line 2468
    .end local v3    # "ex":Ljava/lang/IndexOutOfBoundsException;
    .end local v4    # "newlen":I
    .end local v6    # "temp":[C
    :cond_2
    const/high16 v8, 0x10000

    if-ge v1, v8, :cond_3

    .line 2469
    int-to-char v8, v1

    :try_start_0
    aput-char v8, v0, v7

    .line 2470
    add-int/lit8 v7, v7, 0x1

    .line 2461
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2472
    :cond_3
    const v8, 0xd7c0

    shr-int/lit8 v9, v1, 0xa

    add-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v0, v7

    .line 2473
    add-int/lit8 v8, v7, 0x1

    const v9, 0xdc00

    and-int/lit16 v10, v1, 0x3ff

    add-int/2addr v9, v10

    int-to-char v9, v9

    aput-char v9, v0, v8
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2474
    add-int/lit8 v7, v7, 0x2

    goto :goto_1

    .line 2486
    .end local v1    # "cp":I
    :cond_4
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v0, v12, v7}, Ljava/lang/String;-><init>([CII)V

    return-object v8
.end method

.method public static replace(Ljava/lang/String;II)Ljava/lang/String;
    .locals 10
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "oldChar32"    # I
    .param p2, "newChar32"    # I

    .prologue
    const v8, 0x10ffff

    const/4 v9, -0x1

    .line 2124
    if-lez p1, :cond_0

    if-le p1, v8, :cond_1

    .line 2125
    :cond_0
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v9, "Argument oldChar32 is not a valid codepoint"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2127
    :cond_1
    if-lez p2, :cond_2

    if-le p2, v8, :cond_3

    .line 2128
    :cond_2
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v9, "Argument newChar32 is not a valid codepoint"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2131
    :cond_3
    invoke-static {p0, p1}, Lcom/ibm/icu/text/UTF16;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 2132
    .local v1, "index":I
    if-ne v1, v9, :cond_4

    .line 2152
    .end local p0    # "source":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 2135
    .restart local p0    # "source":Ljava/lang/String;
    :cond_4
    invoke-static {p2}, Lcom/ibm/icu/text/UTF16;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 2136
    .local v4, "newChar32Str":Ljava/lang/String;
    const/4 v5, 0x1

    .line 2137
    .local v5, "oldChar32Size":I
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    .line 2138
    .local v3, "newChar32Size":I
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2139
    .local v6, "result":Ljava/lang/StringBuffer;
    move v7, v1

    .line 2141
    .local v7, "resultIndex":I
    const/high16 v8, 0x10000

    if-lt p1, v8, :cond_5

    .line 2142
    const/4 v5, 0x2

    .line 2145
    :cond_5
    :goto_1
    if-eq v1, v9, :cond_6

    .line 2146
    add-int v0, v7, v5

    .line 2147
    .local v0, "endResultIndex":I
    invoke-virtual {v6, v7, v0, v4}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 2148
    add-int v2, v1, v5

    .line 2149
    .local v2, "lastEndIndex":I
    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/UTF16;->indexOf(Ljava/lang/String;II)I

    move-result v1

    .line 2150
    add-int v8, v3, v1

    sub-int/2addr v8, v2

    add-int/2addr v7, v8

    .line 2151
    goto :goto_1

    .line 2152
    .end local v0    # "endResultIndex":I
    .end local v2    # "lastEndIndex":I
    :cond_6
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static replace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "oldStr"    # Ljava/lang/String;
    .param p2, "newStr"    # Ljava/lang/String;

    .prologue
    const/4 v8, -0x1

    .line 2188
    invoke-static {p0, p1}, Lcom/ibm/icu/text/UTF16;->indexOf(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 2189
    .local v1, "index":I
    if-ne v1, v8, :cond_0

    .line 2204
    .end local p0    # "source":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 2192
    .restart local p0    # "source":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 2193
    .local v4, "oldStrSize":I
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    .line 2194
    .local v3, "newStrSize":I
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, p0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2195
    .local v5, "result":Ljava/lang/StringBuffer;
    move v6, v1

    .line 2197
    .local v6, "resultIndex":I
    :goto_1
    if-eq v1, v8, :cond_1

    .line 2198
    add-int v0, v6, v4

    .line 2199
    .local v0, "endResultIndex":I
    invoke-virtual {v5, v6, v0, p2}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 2200
    add-int v2, v1, v4

    .line 2201
    .local v2, "lastEndIndex":I
    invoke-static {p0, p1, v2}, Lcom/ibm/icu/text/UTF16;->indexOf(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    .line 2202
    add-int v7, v3, v1

    sub-int/2addr v7, v2

    add-int/2addr v6, v7

    .line 2203
    goto :goto_1

    .line 2204
    .end local v0    # "endResultIndex":I
    .end local v2    # "lastEndIndex":I
    :cond_1
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static reverse(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 7
    .param p0, "source"    # Ljava/lang/StringBuffer;

    .prologue
    .line 2221
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    .line 2222
    .local v4, "length":I
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 2223
    .local v5, "result":Ljava/lang/StringBuffer;
    move v2, v4

    .local v2, "i":I
    move v3, v2

    .end local v2    # "i":I
    .local v3, "i":I
    :goto_0
    add-int/lit8 v2, v3, -0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    if-lez v3, :cond_1

    .line 2224
    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    .line 2225
    .local v0, "ch":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_0

    if-lez v2, :cond_0

    .line 2226
    add-int/lit8 v6, v2, -0x1

    invoke-virtual {p0, v6}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    .line 2227
    .local v1, "ch2":C
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2228
    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2229
    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2230
    add-int/lit8 v2, v2, -0x1

    move v3, v2

    .line 2231
    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 2234
    .end local v1    # "ch2":C
    .end local v3    # "i":I
    .restart local v2    # "i":I
    :cond_0
    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v3, v2

    .line 2235
    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 2236
    .end local v0    # "ch":C
    .end local v3    # "i":I
    .restart local v2    # "i":I
    :cond_1
    return-object v5
.end method

.method public static setCharAt([CIII)I
    .locals 10
    .param p0, "target"    # [C
    .param p1, "limit"    # I
    .param p2, "offset16"    # I
    .param p3, "char32"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1272
    if-lt p2, p1, :cond_0

    .line 1273
    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v5, p2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v5

    .line 1275
    :cond_0
    const/4 v0, 0x1

    .line 1276
    .local v0, "count":I
    aget-char v2, p0, p2

    .line 1278
    .local v2, "single":C
    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1280
    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_3

    array-length v5, p0

    add-int/lit8 v6, p2, 0x1

    if-le v5, v6, :cond_3

    add-int/lit8 v5, p2, 0x1

    aget-char v5, p0, v5

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1282
    add-int/lit8 v0, v0, 0x1

    .line 1294
    :cond_1
    :goto_0
    invoke-static {p3}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 1295
    .local v3, "str":Ljava/lang/String;
    move v1, p1

    .line 1296
    .local v1, "result":I
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    .line 1297
    .local v4, "strlength":I
    invoke-virtual {v3, v8}, Ljava/lang/String;->charAt(I)C

    move-result v5

    aput-char v5, p0, p2

    .line 1298
    if-ne v0, v4, :cond_4

    .line 1299
    const/4 v5, 0x2

    if-ne v0, v5, :cond_2

    .line 1300
    add-int/lit8 v5, p2, 0x1

    invoke-virtual {v3, v9}, Ljava/lang/String;->charAt(I)C

    move-result v6

    aput-char v6, p0, v5

    .line 1322
    :cond_2
    :goto_1
    return v1

    .line 1286
    .end local v1    # "result":I
    .end local v3    # "str":Ljava/lang/String;
    .end local v4    # "strlength":I
    :cond_3
    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_1

    if-lez p2, :cond_1

    add-int/lit8 v5, p2, -0x1

    aget-char v5, p0, v5

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1288
    add-int/lit8 p2, p2, -0x1

    .line 1289
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1305
    .restart local v1    # "result":I
    .restart local v3    # "str":Ljava/lang/String;
    .restart local v4    # "strlength":I
    :cond_4
    add-int v5, p2, v0

    add-int v6, p2, v4

    add-int v7, p2, v0

    sub-int v7, p1, v7

    invoke-static {p0, v5, p0, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1307
    if-ge v0, v4, :cond_5

    .line 1310
    add-int/lit8 v5, p2, 0x1

    invoke-virtual {v3, v9}, Ljava/lang/String;->charAt(I)C

    move-result v6

    aput-char v6, p0, v5

    .line 1311
    add-int/lit8 v1, v1, 0x1

    .line 1312
    array-length v5, p0

    if-ge v1, v5, :cond_2

    .line 1313
    aput-char v8, p0, v1

    goto :goto_1

    .line 1318
    :cond_5
    add-int/lit8 v1, v1, -0x1

    .line 1319
    aput-char v8, p0, v1

    goto :goto_1
.end method

.method public static setCharAt(Ljava/lang/StringBuffer;II)V
    .locals 4
    .param p0, "target"    # Ljava/lang/StringBuffer;
    .param p1, "offset16"    # I
    .param p2, "char32"    # I

    .prologue
    .line 1232
    const/4 v0, 0x1

    .line 1233
    .local v0, "count":I
    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    .line 1235
    .local v1, "single":C
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1237
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    add-int/lit8 v3, p1, 0x1

    if-le v2, v3, :cond_1

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1239
    add-int/lit8 v0, v0, 0x1

    .line 1250
    :cond_0
    :goto_0
    add-int v2, p1, v0

    invoke-static {p2}, Lcom/ibm/icu/text/UTF16;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1, v2, v3}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 1251
    return-void

    .line 1243
    :cond_1
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    if-lez p1, :cond_0

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1245
    add-int/lit8 p1, p1, -0x1

    .line 1246
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static toString(I)Ljava/lang/String;
    .locals 2
    .param p0, "ch"    # I

    .prologue
    .line 2848
    const/high16 v1, 0x10000

    if-ge p0, v1, :cond_0

    .line 2849
    int-to-char v1, p0

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    .line 2855
    :goto_0
    return-object v1

    .line 2852
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 2853
    .local v0, "result":Ljava/lang/StringBuffer;
    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2854
    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->getTrailSurrogate(I)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2855
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static valueOf(I)Ljava/lang/String;
    .locals 2
    .param p0, "char32"    # I

    .prologue
    .line 717
    if-ltz p0, :cond_0

    const v0, 0x10ffff

    if-le p0, v0, :cond_1

    .line 718
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Illegal codepoint"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 720
    :cond_1
    invoke-static {p0}, Lcom/ibm/icu/text/UTF16;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "offset16"    # I

    .prologue
    .line 739
    invoke-static {p0, p1}, Lcom/ibm/icu/text/UTF16;->bounds(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 745
    :pswitch_0
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 741
    :pswitch_1
    add-int/lit8 v0, p1, 0x2

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 743
    :pswitch_2
    add-int/lit8 v0, p1, -0x1

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 739
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/StringBuffer;I)Ljava/lang/String;
    .locals 2
    .param p0, "source"    # Ljava/lang/StringBuffer;
    .param p1, "offset16"    # I

    .prologue
    .line 765
    invoke-static {p0, p1}, Lcom/ibm/icu/text/UTF16;->bounds(Ljava/lang/StringBuffer;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 771
    :pswitch_0
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, p1, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 767
    :pswitch_1
    add-int/lit8 v0, p1, 0x2

    invoke-virtual {p0, p1, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 769
    :pswitch_2
    add-int/lit8 v0, p1, -0x1

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 765
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf([CIII)Ljava/lang/String;
    .locals 3
    .param p0, "source"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "offset16"    # I

    .prologue
    const/4 v2, 0x2

    .line 797
    invoke-static {p0, p1, p2, p3}, Lcom/ibm/icu/text/UTF16;->bounds([CIII)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 803
    :pswitch_0
    new-instance v0, Ljava/lang/String;

    add-int v1, p1, p3

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Ljava/lang/String;-><init>([CII)V

    :goto_0
    return-object v0

    .line 799
    :pswitch_1
    new-instance v0, Ljava/lang/String;

    add-int v1, p1, p3

    invoke-direct {v0, p0, v1, v2}, Ljava/lang/String;-><init>([CII)V

    goto :goto_0

    .line 801
    :pswitch_2
    new-instance v0, Ljava/lang/String;

    add-int v1, p1, p3

    add-int/lit8 v1, v1, -0x1

    invoke-direct {v0, p0, v1, v2}, Ljava/lang/String;-><init>([CII)V

    goto :goto_0

    .line 797
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
