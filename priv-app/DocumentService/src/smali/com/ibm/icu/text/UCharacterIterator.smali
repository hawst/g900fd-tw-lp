.class public abstract Lcom/ibm/icu/text/UCharacterIterator;
.super Ljava/lang/Object;
.source "UCharacterIterator.java"

# interfaces
.implements Lcom/ibm/icu/text/UForwardCharacterIterator;
.implements Ljava/lang/Cloneable;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method

.method public static final getInstance(Lcom/ibm/icu/text/Replaceable;)Lcom/ibm/icu/text/UCharacterIterator;
    .locals 1
    .param p0, "source"    # Lcom/ibm/icu/text/Replaceable;

    .prologue
    .line 54
    new-instance v0, Lcom/ibm/icu/impl/ReplaceableUCharacterIterator;

    invoke-direct {v0, p0}, Lcom/ibm/icu/impl/ReplaceableUCharacterIterator;-><init>(Lcom/ibm/icu/text/Replaceable;)V

    return-object v0
.end method

.method public static final getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/UCharacterIterator;
    .locals 1
    .param p0, "source"    # Ljava/lang/String;

    .prologue
    .line 66
    new-instance v0, Lcom/ibm/icu/impl/ReplaceableUCharacterIterator;

    invoke-direct {v0, p0}, Lcom/ibm/icu/impl/ReplaceableUCharacterIterator;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static final getInstance(Ljava/lang/StringBuffer;)Lcom/ibm/icu/text/UCharacterIterator;
    .locals 1
    .param p0, "source"    # Ljava/lang/StringBuffer;

    .prologue
    .line 101
    new-instance v0, Lcom/ibm/icu/impl/ReplaceableUCharacterIterator;

    invoke-direct {v0, p0}, Lcom/ibm/icu/impl/ReplaceableUCharacterIterator;-><init>(Ljava/lang/StringBuffer;)V

    return-object v0
.end method

.method public static final getInstance(Ljava/text/CharacterIterator;)Lcom/ibm/icu/text/UCharacterIterator;
    .locals 1
    .param p0, "source"    # Ljava/text/CharacterIterator;

    .prologue
    .line 113
    new-instance v0, Lcom/ibm/icu/impl/CharacterIteratorWrapper;

    invoke-direct {v0, p0}, Lcom/ibm/icu/impl/CharacterIteratorWrapper;-><init>(Ljava/text/CharacterIterator;)V

    return-object v0
.end method

.method public static final getInstance([C)Lcom/ibm/icu/text/UCharacterIterator;
    .locals 2
    .param p0, "source"    # [C

    .prologue
    .line 78
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance([CII)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v0

    return-object v0
.end method

.method public static final getInstance([CII)Lcom/ibm/icu/text/UCharacterIterator;
    .locals 1
    .param p0, "source"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    .line 90
    new-instance v0, Lcom/ibm/icu/impl/UCharArrayIterator;

    invoke-direct {v0, p0, p1, p2}, Lcom/ibm/icu/impl/UCharArrayIterator;-><init>([CII)V

    return-object v0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 405
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract current()I
.end method

.method public currentCodePoint()I
    .locals 4

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v0

    .line 147
    .local v0, "ch":I
    int-to-char v2, v0

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 150
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    .line 154
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v1

    .line 157
    .local v1, "ch2":I
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    .line 159
    int-to-char v2, v1

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 162
    int-to-char v2, v0

    int-to-char v3, v1

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v0

    .line 167
    .end local v0    # "ch":I
    .end local v1    # "ch2":I
    :cond_0
    return v0
.end method

.method public getCharacterIterator()Ljava/text/CharacterIterator;
    .locals 1

    .prologue
    .line 125
    new-instance v0, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;

    invoke-direct {v0, p0}, Lcom/ibm/icu/impl/UCharacterIteratorWrapper;-><init>(Lcom/ibm/icu/text/UCharacterIterator;)V

    return-object v0
.end method

.method public abstract getIndex()I
.end method

.method public abstract getLength()I
.end method

.method public final getText([C)I
    .locals 1
    .param p1, "fillIn"    # [C

    .prologue
    .line 334
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/ibm/icu/text/UCharacterIterator;->getText([CI)I

    move-result v0

    return v0
.end method

.method public abstract getText([CI)I
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 343
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getLength()I

    move-result v1

    new-array v0, v1, [C

    .line 344
    .local v0, "text":[C
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UCharacterIterator;->getText([C)I

    .line 345
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    return-object v1
.end method

.method public moveCodePointIndex(I)I
    .locals 2
    .param p1, "delta"    # I

    .prologue
    const/4 v1, -0x1

    .line 386
    if-lez p1, :cond_0

    .line 387
    :goto_0
    if-lez p1, :cond_1

    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->nextCodePoint()I

    move-result v0

    if-eq v0, v1, :cond_1

    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 389
    :cond_0
    :goto_1
    if-gez p1, :cond_1

    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->previousCodePoint()I

    move-result v0

    if-eq v0, v1, :cond_1

    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 391
    :cond_1
    if-eqz p1, :cond_2

    .line 392
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 395
    :cond_2
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v0

    return v0
.end method

.method public moveIndex(I)I
    .locals 4
    .param p1, "delta"    # I

    .prologue
    .line 365
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v2

    add-int/2addr v2, p1

    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getLength()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 366
    .local v0, "x":I
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 367
    return v0
.end method

.method public abstract next()I
.end method

.method public nextCodePoint()I
    .locals 4

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    move-result v0

    .line 210
    .local v0, "ch1":I
    int-to-char v2, v0

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 211
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    move-result v1

    .line 212
    .local v1, "ch2":I
    int-to-char v2, v1

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 213
    int-to-char v2, v0

    int-to-char v3, v1

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v0

    .line 220
    .end local v0    # "ch1":I
    .end local v1    # "ch2":I
    :cond_0
    :goto_0
    return v0

    .line 215
    .restart local v0    # "ch1":I
    .restart local v1    # "ch2":I
    :cond_1
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 217
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    goto :goto_0
.end method

.method public abstract previous()I
.end method

.method public previousCodePoint()I
    .locals 4

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v0

    .line 248
    .local v0, "ch1":I
    int-to-char v2, v0

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 249
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v1

    .line 250
    .local v1, "ch2":I
    int-to-char v2, v1

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 251
    int-to-char v2, v1

    int-to-char v3, v0

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v0

    .line 258
    .end local v0    # "ch1":I
    .end local v1    # "ch2":I
    :cond_0
    :goto_0
    return v0

    .line 253
    .restart local v0    # "ch1":I
    .restart local v1    # "ch2":I
    :cond_1
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 255
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    goto :goto_0
.end method

.method public abstract setIndex(I)V
.end method

.method public setToLimit()V
    .locals 1

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/ibm/icu/text/UCharacterIterator;->getLength()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 276
    return-void
.end method

.method public setToStart()V
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 284
    return-void
.end method
