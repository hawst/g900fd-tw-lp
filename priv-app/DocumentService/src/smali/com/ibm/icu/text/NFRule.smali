.class final Lcom/ibm/icu/text/NFRule;
.super Ljava/lang/Object;
.source "NFRule.java"


# static fields
.field public static final IMPROPER_FRACTION_RULE:I = -0x2

.field public static final MASTER_RULE:I = -0x4

.field public static final NEGATIVE_NUMBER_RULE:I = -0x1

.field public static final PROPER_FRACTION_RULE:I = -0x3


# instance fields
.field private baseValue:J

.field private exponent:S

.field private formatter:Lcom/ibm/icu/text/RuleBasedNumberFormat;

.field private radix:I

.field private ruleText:Ljava/lang/String;

.field private sub1:Lcom/ibm/icu/text/NFSubstitution;

.field private sub2:Lcom/ibm/icu/text/NFSubstitution;


# direct methods
.method public constructor <init>(Lcom/ibm/icu/text/RuleBasedNumberFormat;)V
    .locals 2
    .param p1, "formatter"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;

    .prologue
    const/4 v1, 0x0

    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/16 v0, 0xa

    iput v0, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    .line 62
    const/4 v0, 0x0

    iput-short v0, p0, Lcom/ibm/icu/text/NFRule;->exponent:S

    .line 69
    iput-object v1, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    .line 75
    iput-object v1, p0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    .line 81
    iput-object v1, p0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    .line 86
    iput-object v1, p0, Lcom/ibm/icu/text/NFRule;->formatter:Lcom/ibm/icu/text/RuleBasedNumberFormat;

    .line 215
    iput-object p1, p0, Lcom/ibm/icu/text/NFRule;->formatter:Lcom/ibm/icu/text/RuleBasedNumberFormat;

    .line 216
    return-void
.end method

.method private allIgnorable(Ljava/lang/String;)Z
    .locals 7
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, -0x1

    .line 1308
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    .line 1328
    :goto_0
    return v4

    .line 1315
    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/text/NFRule;->formatter:Lcom/ibm/icu/text/RuleBasedNumberFormat;

    invoke-virtual {v3}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParseEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1316
    iget-object v3, p0, Lcom/ibm/icu/text/NFRule;->formatter:Lcom/ibm/icu/text/RuleBasedNumberFormat;

    invoke-virtual {v3}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->getCollator()Lcom/ibm/icu/text/Collator;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/text/RuleBasedCollator;

    move-object v0, v3

    check-cast v0, Lcom/ibm/icu/text/RuleBasedCollator;

    .line 1317
    .local v0, "collator":Lcom/ibm/icu/text/RuleBasedCollator;
    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/RuleBasedCollator;->getCollationElementIterator(Ljava/lang/String;)Lcom/ibm/icu/text/CollationElementIterator;

    move-result-object v1

    .line 1319
    .local v1, "iter":Lcom/ibm/icu/text/CollationElementIterator;
    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v2

    .line 1321
    .local v2, "o":I
    :goto_1
    if-eq v2, v6, :cond_1

    invoke-static {v2}, Lcom/ibm/icu/text/CollationElementIterator;->primaryOrder(I)I

    move-result v3

    if-nez v3, :cond_1

    .line 1322
    invoke-virtual {v1}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v2

    .line 1323
    goto :goto_1

    .line 1324
    :cond_1
    if-ne v2, v6, :cond_2

    move v3, v4

    :goto_2
    move v4, v3

    goto :goto_0

    :cond_2
    move v3, v5

    goto :goto_2

    .end local v0    # "collator":Lcom/ibm/icu/text/RuleBasedCollator;
    .end local v1    # "iter":Lcom/ibm/icu/text/CollationElementIterator;
    .end local v2    # "o":I
    :cond_3
    move v4, v5

    .line 1328
    goto :goto_0
.end method

.method private expectedExponent()S
    .locals 6

    .prologue
    .line 494
    iget v1, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    const-wide/16 v4, 0x1

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    .line 495
    :cond_0
    const/4 v0, 0x0

    .line 505
    :cond_1
    :goto_0
    return v0

    .line 501
    :cond_2
    iget-wide v2, p0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    long-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    iget v1, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    int-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-int v1, v2

    int-to-short v0, v1

    .line 502
    .local v0, "tempResult":S
    iget v1, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    int-to-double v2, v1

    add-int/lit8 v1, v0, 0x1

    int-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    iget-wide v4, p0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    long-to-double v4, v4

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_1

    .line 503
    add-int/lit8 v1, v0, 0x1

    int-to-short v0, v1

    goto :goto_0
.end method

.method private extractSubstitution(Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/RuleBasedNumberFormat;)Lcom/ibm/icu/text/NFSubstitution;
    .locals 10
    .param p1, "owner"    # Lcom/ibm/icu/text/NFRuleSet;
    .param p2, "predecessor"    # Lcom/ibm/icu/text/NFRule;
    .param p3, "ownersOwner"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;

    .prologue
    const/4 v9, 0x0

    const/4 v4, -0x1

    .line 390
    const/4 v7, 0x0

    .line 396
    .local v7, "result":Lcom/ibm/icu/text/NFSubstitution;
    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "<<"

    aput-object v2, v1, v9

    const/4 v2, 0x1

    const-string/jumbo v3, "<%"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "<#"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "<0"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, ">>"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string/jumbo v3, ">%"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, ">#"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, ">0"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "=%"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "=#"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "=0"

    aput-object v3, v1, v2

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/NFRule;->indexOfAny([Ljava/lang/String;)I

    move-result v0

    .line 402
    .local v0, "subStart":I
    if-ne v0, v4, :cond_0

    .line 403
    iget-object v1, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .end local v0    # "subStart":I
    const-string/jumbo v5, ""

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/ibm/icu/text/NFSubstitution;->makeSubstitution(ILcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)Lcom/ibm/icu/text/NFSubstitution;

    move-result-object v1

    .line 443
    :goto_0
    return-object v1

    .line 409
    .restart local v0    # "subStart":I
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ">>>"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 410
    add-int/lit8 v8, v0, 0x2

    .line 430
    .local v8, "subEnd":I
    :cond_1
    :goto_1
    if-ne v8, v4, :cond_3

    .line 431
    iget-object v1, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .end local v0    # "subStart":I
    const-string/jumbo v5, ""

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/ibm/icu/text/NFSubstitution;->makeSubstitution(ILcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)Lcom/ibm/icu/text/NFSubstitution;

    move-result-object v1

    goto :goto_0

    .line 415
    .end local v8    # "subEnd":I
    .restart local v0    # "subStart":I
    :cond_2
    iget-object v1, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 416
    .local v6, "c":C
    iget-object v1, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v6, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v8

    .line 418
    .restart local v8    # "subEnd":I
    const/16 v1, 0x3c

    if-ne v6, v1, :cond_1

    if-eq v8, v4, :cond_1

    iget-object v1, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v8, v1, :cond_1

    iget-object v1, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    add-int/lit8 v2, v8, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v6, :cond_1

    .line 423
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 438
    .end local v6    # "c":C
    :cond_3
    iget-object v1, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    add-int/lit8 v2, v8, 0x1

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/ibm/icu/text/NFSubstitution;->makeSubstitution(ILcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)Lcom/ibm/icu/text/NFSubstitution;

    move-result-object v7

    .line 442
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    invoke-virtual {v2, v9, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    add-int/lit8 v3, v8, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    move-object v1, v7

    .line 443
    goto/16 :goto_0
.end method

.method private extractSubstitutions(Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/RuleBasedNumberFormat;)V
    .locals 1
    .param p1, "owner"    # Lcom/ibm/icu/text/NFRuleSet;
    .param p2, "predecessor"    # Lcom/ibm/icu/text/NFRule;
    .param p3, "ownersOwner"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;

    .prologue
    .line 371
    invoke-direct {p0, p1, p2, p3}, Lcom/ibm/icu/text/NFRule;->extractSubstitution(Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/RuleBasedNumberFormat;)Lcom/ibm/icu/text/NFSubstitution;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    .line 372
    invoke-direct {p0, p1, p2, p3}, Lcom/ibm/icu/text/NFRule;->extractSubstitution(Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/RuleBasedNumberFormat;)Lcom/ibm/icu/text/NFSubstitution;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    .line 373
    return-void
.end method

.method private findText(Ljava/lang/String;Ljava/lang/String;I)[I
    .locals 6
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "startingAt"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 1209
    iget-object v2, p0, Lcom/ibm/icu/text/NFRule;->formatter:Lcom/ibm/icu/text/RuleBasedNumberFormat;

    invoke-virtual {v2}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParseEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1210
    new-array v2, v3, [I

    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    aput v3, v2, v4

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    aput v3, v2, v5

    .line 1243
    :goto_0
    return-object v2

    .line 1223
    :cond_0
    move v1, p3

    .line 1224
    .local v1, "p":I
    const/4 v0, 0x0

    .line 1233
    .local v0, "keyLen":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_2

    if-nez v0, :cond_2

    .line 1234
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lcom/ibm/icu/text/NFRule;->prefixLength(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1235
    if-eqz v0, :cond_1

    .line 1236
    new-array v2, v3, [I

    aput v1, v2, v4

    aput v0, v2, v5

    goto :goto_0

    .line 1238
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 1239
    goto :goto_1

    .line 1243
    :cond_2
    new-array v2, v3, [I

    fill-array-data v2, :array_0

    goto :goto_0

    :array_0
    .array-data 4
        -0x1
        0x0
    .end array-data
.end method

.method private indexOfAny([Ljava/lang/String;)I
    .locals 6
    .param p1, "strings"    # [Ljava/lang/String;

    .prologue
    const/4 v5, -0x1

    .line 520
    const/4 v2, -0x1

    .line 521
    .local v2, "result":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_2

    .line 522
    iget-object v3, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    aget-object v4, p1, v0

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 523
    .local v1, "pos":I
    if-eq v1, v5, :cond_1

    if-eq v2, v5, :cond_0

    if-ge v1, v2, :cond_1

    .line 524
    :cond_0
    move v2, v1

    .line 521
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 527
    .end local v1    # "pos":I
    :cond_2
    return v2
.end method

.method public static makeRules(Ljava/lang/String;Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/RuleBasedNumberFormat;)Ljava/lang/Object;
    .locals 12
    .param p0, "description"    # Ljava/lang/String;
    .param p1, "owner"    # Lcom/ibm/icu/text/NFRuleSet;
    .param p2, "predecessor"    # Lcom/ibm/icu/text/NFRule;
    .param p3, "ownersOwner"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;

    .prologue
    .line 110
    new-instance v2, Lcom/ibm/icu/text/NFRule;

    invoke-direct {v2, p3}, Lcom/ibm/icu/text/NFRule;-><init>(Lcom/ibm/icu/text/RuleBasedNumberFormat;)V

    .line 111
    .local v2, "rule1":Lcom/ibm/icu/text/NFRule;
    invoke-direct {v2, p0}, Lcom/ibm/icu/text/NFRule;->parseRuleDescriptor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 115
    const-string/jumbo v5, "["

    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 116
    .local v0, "brack1":I
    const-string/jumbo v5, "]"

    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 122
    .local v1, "brack2":I
    const/4 v5, -0x1

    if-eq v0, v5, :cond_0

    const/4 v5, -0x1

    if-eq v1, v5, :cond_0

    if-gt v0, v1, :cond_0

    invoke-virtual {v2}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v6

    const-wide/16 v8, -0x3

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    invoke-virtual {v2}, Lcom/ibm/icu/text/NFRule;->getBaseValue()J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-nez v5, :cond_2

    .line 125
    :cond_0
    iput-object p0, v2, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    .line 126
    invoke-direct {v2, p1, p2, p3}, Lcom/ibm/icu/text/NFRule;->extractSubstitutions(Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/RuleBasedNumberFormat;)V

    .line 205
    .end local v2    # "rule1":Lcom/ibm/icu/text/NFRule;
    :cond_1
    :goto_0
    return-object v2

    .line 131
    .restart local v2    # "rule1":Lcom/ibm/icu/text/NFRule;
    :cond_2
    const/4 v3, 0x0

    .line 132
    .local v3, "rule2":Lcom/ibm/icu/text/NFRule;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 137
    .local v4, "sbuf":Ljava/lang/StringBuffer;
    iget-wide v6, v2, Lcom/ibm/icu/text/NFRule;->baseValue:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_3

    iget-wide v6, v2, Lcom/ibm/icu/text/NFRule;->baseValue:J

    long-to-double v6, v6

    iget v5, v2, Lcom/ibm/icu/text/NFRule;->radix:I

    int-to-double v8, v5

    iget-short v5, v2, Lcom/ibm/icu/text/NFRule;->exponent:S

    int-to-double v10, v5

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    rem-double/2addr v6, v8

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-eqz v5, :cond_4

    :cond_3
    iget-wide v6, v2, Lcom/ibm/icu/text/NFRule;->baseValue:J

    const-wide/16 v8, -0x2

    cmp-long v5, v6, v8

    if-eqz v5, :cond_4

    iget-wide v6, v2, Lcom/ibm/icu/text/NFRule;->baseValue:J

    const-wide/16 v8, -0x4

    cmp-long v5, v6, v8

    if-nez v5, :cond_7

    .line 147
    :cond_4
    new-instance v3, Lcom/ibm/icu/text/NFRule;

    .end local v3    # "rule2":Lcom/ibm/icu/text/NFRule;
    invoke-direct {v3, p3}, Lcom/ibm/icu/text/NFRule;-><init>(Lcom/ibm/icu/text/RuleBasedNumberFormat;)V

    .line 148
    .restart local v3    # "rule2":Lcom/ibm/icu/text/NFRule;
    iget-wide v6, v2, Lcom/ibm/icu/text/NFRule;->baseValue:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-ltz v5, :cond_9

    .line 149
    iget-wide v6, v2, Lcom/ibm/icu/text/NFRule;->baseValue:J

    iput-wide v6, v3, Lcom/ibm/icu/text/NFRule;->baseValue:J

    .line 150
    invoke-virtual {p1}, Lcom/ibm/icu/text/NFRuleSet;->isFractionSet()Z

    move-result v5

    if-nez v5, :cond_5

    .line 151
    iget-wide v6, v2, Lcom/ibm/icu/text/NFRule;->baseValue:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v2, Lcom/ibm/icu/text/NFRule;->baseValue:J

    .line 172
    :cond_5
    :goto_1
    iget v5, v2, Lcom/ibm/icu/text/NFRule;->radix:I

    iput v5, v3, Lcom/ibm/icu/text/NFRule;->radix:I

    .line 173
    iget-short v5, v2, Lcom/ibm/icu/text/NFRule;->exponent:S

    iput-short v5, v3, Lcom/ibm/icu/text/NFRule;->exponent:S

    .line 177
    const/4 v5, 0x0

    invoke-virtual {p0, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 178
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v5, v6, :cond_6

    .line 179
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 181
    :cond_6
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    .line 182
    invoke-direct {v3, p1, p2, p3}, Lcom/ibm/icu/text/NFRule;->extractSubstitutions(Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/RuleBasedNumberFormat;)V

    .line 188
    :cond_7
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 189
    const/4 v5, 0x0

    invoke-virtual {p0, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 190
    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 191
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v5, v6, :cond_8

    .line 192
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 194
    :cond_8
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    .line 195
    invoke-direct {v2, p1, p2, p3}, Lcom/ibm/icu/text/NFRule;->extractSubstitutions(Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/RuleBasedNumberFormat;)V

    .line 202
    if-eqz v3, :cond_1

    .line 205
    const/4 v5, 0x2

    new-array v5, v5, [Lcom/ibm/icu/text/NFRule;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v6, 0x1

    aput-object v2, v5, v6

    move-object v2, v5

    goto/16 :goto_0

    .line 158
    :cond_9
    iget-wide v6, v2, Lcom/ibm/icu/text/NFRule;->baseValue:J

    const-wide/16 v8, -0x2

    cmp-long v5, v6, v8

    if-nez v5, :cond_a

    .line 159
    const-wide/16 v6, -0x3

    iput-wide v6, v3, Lcom/ibm/icu/text/NFRule;->baseValue:J

    goto :goto_1

    .line 165
    :cond_a
    iget-wide v6, v2, Lcom/ibm/icu/text/NFRule;->baseValue:J

    const-wide/16 v8, -0x4

    cmp-long v5, v6, v8

    if-nez v5, :cond_5

    .line 166
    iget-wide v6, v2, Lcom/ibm/icu/text/NFRule;->baseValue:J

    iput-wide v6, v3, Lcom/ibm/icu/text/NFRule;->baseValue:J

    .line 167
    const-wide/16 v6, -0x2

    iput-wide v6, v2, Lcom/ibm/icu/text/NFRule;->baseValue:J

    goto/16 :goto_1
.end method

.method private matchToDelimiter(Ljava/lang/String;IDLjava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/NFSubstitution;D)Ljava/lang/Number;
    .locals 21
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "startPos"    # I
    .param p3, "baseVal"    # D
    .param p5, "delimiter"    # Ljava/lang/String;
    .param p6, "pp"    # Ljava/text/ParsePosition;
    .param p7, "sub"    # Lcom/ibm/icu/text/NFSubstitution;
    .param p8, "upperBound"    # D

    .prologue
    .line 946
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/NFRule;->allIgnorable(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 947
    new-instance v7, Ljava/text/ParsePosition;

    const/4 v5, 0x0

    invoke-direct {v7, v5}, Ljava/text/ParsePosition;-><init>(I)V

    .line 954
    .local v7, "tempPP":Ljava/text/ParsePosition;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    move/from16 v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/ibm/icu/text/NFRule;->findText(Ljava/lang/String;Ljava/lang/String;I)[I

    move-result-object v19

    .line 955
    .local v19, "temp":[I
    const/4 v5, 0x0

    aget v17, v19, v5

    .line 956
    .local v17, "dPos":I
    const/4 v5, 0x1

    aget v4, v19, v5

    .line 960
    .local v4, "dLen":I
    :goto_0
    if-ltz v17, :cond_1

    .line 961
    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 962
    .local v6, "subText":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 963
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/NFRule;->formatter:Lcom/ibm/icu/text/RuleBasedNumberFormat;

    invoke-virtual {v5}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParseEnabled()Z

    move-result v12

    move-object/from16 v5, p7

    move-wide/from16 v8, p3

    move-wide/from16 v10, p8

    invoke-virtual/range {v5 .. v12}, Lcom/ibm/icu/text/NFSubstitution;->doParse(Ljava/lang/String;Ljava/text/ParsePosition;DDZ)Ljava/lang/Number;

    move-result-object v20

    .line 972
    .local v20, "tempResult":Ljava/lang/Number;
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v5

    move/from16 v0, v17

    if-ne v5, v0, :cond_0

    .line 973
    add-int v5, v17, v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v5}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1028
    .end local v4    # "dLen":I
    .end local v6    # "subText":Ljava/lang/String;
    .end local v17    # "dPos":I
    .end local v19    # "temp":[I
    .end local v20    # "tempResult":Ljava/lang/Number;
    :goto_1
    return-object v20

    .line 989
    .restart local v4    # "dLen":I
    .restart local v6    # "subText":Ljava/lang/String;
    .restart local v17    # "dPos":I
    .restart local v19    # "temp":[I
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {v7, v5}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 990
    add-int v5, v17, v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v2, v5}, Lcom/ibm/icu/text/NFRule;->findText(Ljava/lang/String;Ljava/lang/String;I)[I

    move-result-object v19

    .line 991
    const/4 v5, 0x0

    aget v17, v19, v5

    .line 992
    const/4 v5, 0x1

    aget v4, v19, v5

    .line 993
    goto :goto_0

    .line 996
    .end local v6    # "subText":Ljava/lang/String;
    :cond_1
    const/4 v5, 0x0

    move-object/from16 v0, p6

    invoke-virtual {v0, v5}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 997
    new-instance v20, Ljava/lang/Long;

    const-wide/16 v8, 0x0

    move-object/from16 v0, v20

    invoke-direct {v0, v8, v9}, Ljava/lang/Long;-><init>(J)V

    goto :goto_1

    .line 1004
    .end local v4    # "dLen":I
    .end local v7    # "tempPP":Ljava/text/ParsePosition;
    .end local v17    # "dPos":I
    .end local v19    # "temp":[I
    :cond_2
    new-instance v7, Ljava/text/ParsePosition;

    const/4 v5, 0x0

    invoke-direct {v7, v5}, Ljava/text/ParsePosition;-><init>(I)V

    .line 1005
    .restart local v7    # "tempPP":Ljava/text/ParsePosition;
    new-instance v18, Ljava/lang/Long;

    const-wide/16 v8, 0x0

    move-object/from16 v0, v18

    invoke-direct {v0, v8, v9}, Ljava/lang/Long;-><init>(J)V

    .line 1009
    .local v18, "result":Ljava/lang/Number;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/ibm/icu/text/NFRule;->formatter:Lcom/ibm/icu/text/RuleBasedNumberFormat;

    invoke-virtual {v5}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParseEnabled()Z

    move-result v16

    move-object/from16 v9, p7

    move-object/from16 v10, p1

    move-object v11, v7

    move-wide/from16 v12, p3

    move-wide/from16 v14, p8

    invoke-virtual/range {v9 .. v16}, Lcom/ibm/icu/text/NFSubstitution;->doParse(Ljava/lang/String;Ljava/text/ParsePosition;DDZ)Ljava/lang/Number;

    move-result-object v20

    .line 1011
    .restart local v20    # "tempResult":Ljava/lang/Number;
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual/range {p7 .. p7}, Lcom/ibm/icu/text/NFSubstitution;->isNullSubstitution()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1016
    :cond_3
    invoke-virtual {v7}, Ljava/text/ParsePosition;->getIndex()I

    move-result v5

    move-object/from16 v0, p6

    invoke-virtual {v0, v5}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 1017
    if-eqz v20, :cond_4

    .line 1018
    move-object/from16 v18, v20

    :cond_4
    move-object/from16 v20, v18

    .line 1028
    goto :goto_1
.end method

.method private parseRuleDescriptor(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    const/16 v10, 0x2c

    const/16 v9, 0x39

    const/16 v8, 0x30

    const/16 v7, 0x3e

    const/4 v6, 0x0

    .line 236
    const-string/jumbo v4, ":"

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 237
    .local v2, "p":I
    const/4 v4, -0x1

    if-ne v2, v4, :cond_2

    .line 238
    const-wide/16 v4, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/ibm/icu/text/NFRule;->setBaseValue(J)V

    .line 351
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x27

    if-ne v4, v5, :cond_1

    .line 352
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 357
    :cond_1
    return-object p1

    .line 243
    :cond_2
    invoke-virtual {p1, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 244
    .local v1, "descriptor":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    .line 245
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_3

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 246
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 247
    :cond_3
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 252
    const-string/jumbo v4, "-x"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 253
    const-wide/16 v4, -0x1

    invoke-virtual {p0, v4, v5}, Lcom/ibm/icu/text/NFRule;->setBaseValue(J)V

    goto :goto_0

    .line 255
    :cond_4
    const-string/jumbo v4, "x.x"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 256
    const-wide/16 v4, -0x2

    invoke-virtual {p0, v4, v5}, Lcom/ibm/icu/text/NFRule;->setBaseValue(J)V

    goto :goto_0

    .line 258
    :cond_5
    const-string/jumbo v4, "0.x"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 259
    const-wide/16 v4, -0x3

    invoke-virtual {p0, v4, v5}, Lcom/ibm/icu/text/NFRule;->setBaseValue(J)V

    goto :goto_0

    .line 261
    :cond_6
    const-string/jumbo v4, "x.0"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 262
    const-wide/16 v4, -0x4

    invoke-virtual {p0, v4, v5}, Lcom/ibm/icu/text/NFRule;->setBaseValue(J)V

    goto :goto_0

    .line 267
    :cond_7
    invoke-virtual {v1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-lt v4, v8, :cond_0

    invoke-virtual {v1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-gt v4, v9, :cond_0

    .line 268
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 269
    .local v3, "tempValue":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .line 270
    const/16 v0, 0x20

    .line 276
    .local v0, "c":C
    :goto_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_a

    .line 277
    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 278
    if-lt v0, v8, :cond_9

    if-gt v0, v9, :cond_9

    .line 279
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 289
    :cond_8
    add-int/lit8 v2, v2, 0x1

    .line 290
    goto :goto_2

    .line 281
    :cond_9
    const/16 v4, 0x2f

    if-eq v0, v4, :cond_a

    if-ne v0, v7, :cond_c

    .line 295
    :cond_a
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/ibm/icu/text/NFRule;->setBaseValue(J)V

    .line 301
    const/16 v4, 0x2f

    if-ne v0, v4, :cond_11

    .line 302
    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 303
    add-int/lit8 v2, v2, 0x1

    .line 304
    :goto_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_e

    .line 305
    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 306
    if-lt v0, v8, :cond_d

    if-gt v0, v9, :cond_d

    .line 307
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 317
    :cond_b
    add-int/lit8 v2, v2, 0x1

    .line 318
    goto :goto_3

    .line 284
    :cond_c
    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v4

    if-nez v4, :cond_8

    if-eq v0, v10, :cond_8

    const/16 v4, 0x2e

    if-eq v0, v4, :cond_8

    .line 287
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "Illegal character in rule descriptor"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 309
    :cond_d
    if-ne v0, v7, :cond_f

    .line 322
    :cond_e
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    .line 323
    iget v4, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    if-nez v4, :cond_10

    .line 324
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "Rule can\'t have radix of 0"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 312
    :cond_f
    invoke-static {v0}, Lcom/ibm/icu/impl/UCharacterProperty;->isRuleWhiteSpace(I)Z

    move-result v4

    if-nez v4, :cond_b

    if-eq v0, v10, :cond_b

    const/16 v4, 0x2e

    if-eq v0, v4, :cond_b

    .line 315
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "Illegal character is rule descriptor"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 326
    :cond_10
    invoke-direct {p0}, Lcom/ibm/icu/text/NFRule;->expectedExponent()S

    move-result v4

    iput-short v4, p0, Lcom/ibm/icu/text/NFRule;->exponent:S

    .line 334
    :cond_11
    if-ne v0, v7, :cond_0

    .line 335
    :goto_4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 336
    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 337
    if-ne v0, v7, :cond_12

    iget-short v4, p0, Lcom/ibm/icu/text/NFRule;->exponent:S

    if-lez v4, :cond_12

    .line 338
    iget-short v4, p0, Lcom/ibm/icu/text/NFRule;->exponent:S

    add-int/lit8 v4, v4, -0x1

    int-to-short v4, v4

    iput-short v4, p0, Lcom/ibm/icu/text/NFRule;->exponent:S

    .line 342
    add-int/lit8 v2, v2, 0x1

    .line 343
    goto :goto_4

    .line 340
    :cond_12
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "Illegal character in rule descriptor"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private prefixLength(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "prefix"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v8, -0x1

    .line 1049
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_1

    .line 1169
    :cond_0
    :goto_0
    return v4

    .line 1054
    :cond_1
    iget-object v6, p0, Lcom/ibm/icu/text/NFRule;->formatter:Lcom/ibm/icu/text/RuleBasedNumberFormat;

    invoke-virtual {v6}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->lenientParseEnabled()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1068
    iget-object v6, p0, Lcom/ibm/icu/text/NFRule;->formatter:Lcom/ibm/icu/text/RuleBasedNumberFormat;

    invoke-virtual {v6}, Lcom/ibm/icu/text/RuleBasedNumberFormat;->getCollator()Lcom/ibm/icu/text/Collator;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/RuleBasedCollator;

    .line 1069
    .local v0, "collator":Lcom/ibm/icu/text/RuleBasedCollator;
    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/RuleBasedCollator;->getCollationElementIterator(Ljava/lang/String;)Lcom/ibm/icu/text/CollationElementIterator;

    move-result-object v5

    .line 1070
    .local v5, "strIter":Lcom/ibm/icu/text/CollationElementIterator;
    invoke-virtual {v0, p2}, Lcom/ibm/icu/text/RuleBasedCollator;->getCollationElementIterator(Ljava/lang/String;)Lcom/ibm/icu/text/CollationElementIterator;

    move-result-object v3

    .line 1073
    .local v3, "prefixIter":Lcom/ibm/icu/text/CollationElementIterator;
    invoke-virtual {v5}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v2

    .line 1074
    .local v2, "oStr":I
    invoke-virtual {v3}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v1

    .line 1076
    .local v1, "oPrefix":I
    :goto_1
    if-eq v1, v8, :cond_4

    .line 1078
    :goto_2
    invoke-static {v2}, Lcom/ibm/icu/text/CollationElementIterator;->primaryOrder(I)I

    move-result v6

    if-nez v6, :cond_2

    if-eq v2, v8, :cond_2

    .line 1080
    invoke-virtual {v5}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v2

    .line 1081
    goto :goto_2

    .line 1084
    :cond_2
    :goto_3
    invoke-static {v1}, Lcom/ibm/icu/text/CollationElementIterator;->primaryOrder(I)I

    move-result v6

    if-nez v6, :cond_3

    if-eq v1, v8, :cond_3

    .line 1086
    invoke-virtual {v3}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v1

    .line 1087
    goto :goto_3

    .line 1091
    :cond_3
    if-ne v1, v8, :cond_5

    .line 1117
    :cond_4
    invoke-virtual {v5}, Lcom/ibm/icu/text/CollationElementIterator;->getOffset()I

    move-result v4

    .line 1118
    .local v4, "result":I
    if-eq v2, v8, :cond_0

    .line 1119
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    .line 1097
    .end local v4    # "result":I
    :cond_5
    if-eq v2, v8, :cond_0

    .line 1104
    invoke-static {v2}, Lcom/ibm/icu/text/CollationElementIterator;->primaryOrder(I)I

    move-result v6

    invoke-static {v1}, Lcom/ibm/icu/text/CollationElementIterator;->primaryOrder(I)I

    move-result v7

    if-ne v6, v7, :cond_0

    .line 1112
    invoke-virtual {v5}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v2

    .line 1113
    invoke-virtual {v3}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result v1

    .line 1114
    goto :goto_1

    .line 1166
    .end local v0    # "collator":Lcom/ibm/icu/text/RuleBasedCollator;
    .end local v1    # "oPrefix":I
    .end local v2    # "oStr":I
    .end local v3    # "prefixIter":Lcom/ibm/icu/text/CollationElementIterator;
    .end local v5    # "strIter":Lcom/ibm/icu/text/CollationElementIterator;
    :cond_6
    invoke-virtual {p1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1167
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    goto :goto_0
.end method

.method private stripPrefix(Ljava/lang/String;Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "prefix"    # Ljava/lang/String;
    .param p3, "pp"    # Ljava/text/ParsePosition;

    .prologue
    .line 893
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 909
    .end local p1    # "text":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 900
    .restart local p1    # "text":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/NFRule;->prefixLength(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 901
    .local v0, "pfl":I
    if-eqz v0, :cond_0

    .line 904
    invoke-virtual {p3}, Ljava/text/ParsePosition;->getIndex()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {p3, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 905
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public doFormat(DLjava/lang/StringBuffer;I)V
    .locals 1
    .param p1, "number"    # D
    .param p3, "toInsertInto"    # Ljava/lang/StringBuffer;
    .param p4, "pos"    # I

    .prologue
    .line 675
    iget-object v0, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    invoke-virtual {p3, p4, v0}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 676
    iget-object v0, p0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/ibm/icu/text/NFSubstitution;->doSubstitution(DLjava/lang/StringBuffer;I)V

    .line 677
    iget-object v0, p0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/ibm/icu/text/NFSubstitution;->doSubstitution(DLjava/lang/StringBuffer;I)V

    .line 678
    return-void
.end method

.method public doFormat(JLjava/lang/StringBuffer;I)V
    .locals 1
    .param p1, "number"    # J
    .param p3, "toInsertInto"    # Ljava/lang/StringBuffer;
    .param p4, "pos"    # I

    .prologue
    .line 654
    iget-object v0, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    invoke-virtual {p3, p4, v0}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 655
    iget-object v0, p0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/ibm/icu/text/NFSubstitution;->doSubstitution(JLjava/lang/StringBuffer;I)V

    .line 656
    iget-object v0, p0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/ibm/icu/text/NFSubstitution;->doSubstitution(JLjava/lang/StringBuffer;I)V

    .line 657
    return-void
.end method

.method public doParse(Ljava/lang/String;Ljava/text/ParsePosition;ZD)Ljava/lang/Number;
    .locals 28
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "parsePosition"    # Ljava/text/ParsePosition;
    .param p3, "isFractionRule"    # Z
    .param p4, "upperBound"    # D

    .prologue
    .line 739
    new-instance v10, Ljava/text/ParsePosition;

    const/4 v4, 0x0

    invoke-direct {v10, v4}, Ljava/text/ParsePosition;-><init>(I)V

    .line 740
    .local v10, "pp":Ljava/text/ParsePosition;
    new-instance v5, Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 746
    .local v5, "workText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v11}, Lcom/ibm/icu/text/NFSubstitution;->getPos()I

    move-result v11

    invoke-virtual {v4, v9, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4, v10}, Lcom/ibm/icu/text/NFRule;->stripPrefix(Ljava/lang/String;Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/String;

    move-result-object v5

    .line 747
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    sub-int v23, v4, v9

    .line 749
    .local v23, "prefixLength":I
    invoke-virtual {v10}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v4}, Lcom/ibm/icu/text/NFSubstitution;->getPos()I

    move-result v4

    if-eqz v4, :cond_0

    .line 752
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v20, 0x0

    move-wide/from16 v0, v20

    invoke-direct {v4, v0, v1}, Ljava/lang/Long;-><init>(J)V

    .line 871
    :goto_0
    return-object v4

    .line 784
    :cond_0
    const/16 v22, 0x0

    .line 785
    .local v22, "highWaterMark":I
    const-wide/16 v24, 0x0

    .line 786
    .local v24, "result":D
    const/4 v6, 0x0

    .line 787
    .local v6, "start":I
    const-wide/16 v20, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    move-wide/from16 v26, v0

    move-wide/from16 v0, v20

    move-wide/from16 v2, v26

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-double v7, v0

    .line 794
    .local v7, "tempBaseValue":D
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v10, v4}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 795
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v9}, Lcom/ibm/icu/text/NFSubstitution;->getPos()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v11}, Lcom/ibm/icu/text/NFSubstitution;->getPos()I

    move-result v11

    invoke-virtual {v4, v9, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    move-object/from16 v4, p0

    move-wide/from16 v12, p4

    invoke-direct/range {v4 .. v13}, Lcom/ibm/icu/text/NFRule;->matchToDelimiter(Ljava/lang/String;IDLjava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/NFSubstitution;D)Ljava/lang/Number;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v15

    .line 803
    .local v15, "partialResult":D
    invoke-virtual {v10}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    if-nez v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v4}, Lcom/ibm/icu/text/NFSubstitution;->isNullSubstitution()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 804
    :cond_2
    invoke-virtual {v10}, Ljava/text/ParsePosition;->getIndex()I

    move-result v6

    .line 806
    invoke-virtual {v10}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 807
    .local v13, "workText2":Ljava/lang/String;
    new-instance v18, Ljava/text/ParsePosition;

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Ljava/text/ParsePosition;-><init>(I)V

    .line 813
    .local v18, "pp2":Ljava/text/ParsePosition;
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v9}, Lcom/ibm/icu/text/NFSubstitution;->getPos()I

    move-result v9

    invoke-virtual {v4, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    move-object/from16 v19, v0

    move-object/from16 v12, p0

    move-wide/from16 v20, p4

    invoke-direct/range {v12 .. v21}, Lcom/ibm/icu/text/NFRule;->matchToDelimiter(Ljava/lang/String;IDLjava/lang/String;Ljava/text/ParsePosition;Lcom/ibm/icu/text/NFSubstitution;D)Ljava/lang/Number;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v15

    .line 820
    invoke-virtual/range {v18 .. v18}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    if-nez v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v4}, Lcom/ibm/icu/text/NFSubstitution;->isNullSubstitution()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 821
    :cond_3
    invoke-virtual {v10}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    add-int v4, v4, v23

    invoke-virtual/range {v18 .. v18}, Ljava/text/ParsePosition;->getIndex()I

    move-result v9

    add-int/2addr v4, v9

    move/from16 v0, v22

    if-le v4, v0, :cond_4

    .line 822
    invoke-virtual {v10}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    add-int v4, v4, v23

    invoke-virtual/range {v18 .. v18}, Ljava/text/ParsePosition;->getIndex()I

    move-result v9

    add-int v22, v4, v9

    .line 823
    move-wide/from16 v24, v15

    .line 845
    .end local v13    # "workText2":Ljava/lang/String;
    .end local v18    # "pp2":Ljava/text/ParsePosition;
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v4}, Lcom/ibm/icu/text/NFSubstitution;->getPos()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v9}, Lcom/ibm/icu/text/NFSubstitution;->getPos()I

    move-result v9

    if-eq v4, v9, :cond_5

    invoke-virtual {v10}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    if-lez v4, :cond_5

    invoke-virtual {v10}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v4, v9, :cond_5

    invoke-virtual {v10}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    if-ne v4, v6, :cond_1

    .line 851
    :cond_5
    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 863
    if-eqz p3, :cond_6

    if-lez v22, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v4}, Lcom/ibm/icu/text/NFSubstitution;->isNullSubstitution()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 864
    const-wide/high16 v20, 0x3ff0000000000000L    # 1.0

    div-double v24, v20, v24

    .line 868
    :cond_6
    move-wide/from16 v0, v24

    double-to-long v0, v0

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    long-to-double v0, v0

    move-wide/from16 v20, v0

    cmpl-double v4, v24, v20

    if-nez v4, :cond_7

    .line 869
    new-instance v4, Ljava/lang/Long;

    move-wide/from16 v0, v24

    double-to-long v0, v0

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-direct {v4, v0, v1}, Ljava/lang/Long;-><init>(J)V

    goto/16 :goto_0

    .line 871
    :cond_7
    new-instance v4, Ljava/lang/Double;

    move-wide/from16 v0, v24

    invoke-direct {v4, v0, v1}, Ljava/lang/Double;-><init>(D)V

    goto/16 :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "that"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 540
    instance-of v2, p1, Lcom/ibm/icu/text/NFRule;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 541
    check-cast v0, Lcom/ibm/icu/text/NFRule;

    .line 543
    .local v0, "that2":Lcom/ibm/icu/text/NFRule;
    iget-wide v2, p0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    iget-wide v4, v0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget v2, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    iget v3, v0, Lcom/ibm/icu/text/NFRule;->radix:I

    if-ne v2, v3, :cond_0

    iget-short v2, p0, Lcom/ibm/icu/text/NFRule;->exponent:S

    iget-short v3, v0, Lcom/ibm/icu/text/NFRule;->exponent:S

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    iget-object v3, v0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    iget-object v3, v0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/NFSubstitution;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    iget-object v3, v0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v2, v3}, Lcom/ibm/icu/text/NFSubstitution;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 550
    .end local v0    # "that2":Lcom/ibm/icu/text/NFRule;
    :cond_0
    return v1
.end method

.method public final getBaseValue()J
    .locals 2

    .prologue
    .line 623
    iget-wide v0, p0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    return-wide v0
.end method

.method public getDivisor()D
    .locals 4

    .prologue
    .line 632
    iget v0, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    int-to-double v0, v0

    iget-short v2, p0, Lcom/ibm/icu/text/NFRule;->exponent:S

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public final setBaseValue(J)V
    .locals 5
    .param p1, "newBaseValue"    # J

    .prologue
    const/16 v4, 0xa

    .line 455
    iput-wide p1, p0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    .line 462
    iget-wide v0, p0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 463
    iput v4, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    .line 464
    invoke-direct {p0}, Lcom/ibm/icu/text/NFRule;->expectedExponent()S

    move-result v0

    iput-short v0, p0, Lcom/ibm/icu/text/NFRule;->exponent:S

    .line 470
    iget-object v0, p0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    iget v1, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    iget-short v2, p0, Lcom/ibm/icu/text/NFRule;->exponent:S

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/NFSubstitution;->setDivisor(II)V

    .line 473
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    if-eqz v0, :cond_1

    .line 474
    iget-object v0, p0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    iget v1, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    iget-short v2, p0, Lcom/ibm/icu/text/NFRule;->exponent:S

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/NFSubstitution;->setDivisor(II)V

    .line 483
    :cond_1
    :goto_0
    return-void

    .line 480
    :cond_2
    iput v4, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    .line 481
    const/4 v0, 0x0

    iput-short v0, p0, Lcom/ibm/icu/text/NFRule;->exponent:S

    goto :goto_0
.end method

.method public shouldRollBack(D)Z
    .locals 11
    .param p1, "number"    # D

    .prologue
    const/4 v0, 0x0

    const-wide/16 v8, 0x0

    .line 705
    iget-object v1, p0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v1}, Lcom/ibm/icu/text/NFSubstitution;->isModulusSubstitution()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v1}, Lcom/ibm/icu/text/NFSubstitution;->isModulusSubstitution()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 706
    :cond_0
    iget v1, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    int-to-double v2, v1

    iget-short v1, p0, Lcom/ibm/icu/text/NFRule;->exponent:S

    int-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    rem-double v2, p1, v2

    cmpl-double v1, v2, v8

    if-nez v1, :cond_1

    iget-wide v2, p0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    long-to-double v2, v2

    iget v1, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    int-to-double v4, v1

    iget-short v1, p0, Lcom/ibm/icu/text/NFRule;->exponent:S

    int-to-double v6, v1

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    rem-double/2addr v2, v4

    cmpl-double v1, v2, v8

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 709
    :cond_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 560
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 563
    .local v2, "result":Ljava/lang/StringBuffer;
    iget-wide v4, p0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    .line 564
    const-string/jumbo v4, "-x: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 597
    :goto_0
    iget-object v4, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v4}, Lcom/ibm/icu/text/NFSubstitution;->getPos()I

    move-result v4

    if-eqz v4, :cond_1

    .line 598
    :cond_0
    const-string/jumbo v4, "\'"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 603
    :cond_1
    new-instance v3, Ljava/lang/StringBuffer;

    iget-object v4, p0, Lcom/ibm/icu/text/NFRule;->ruleText:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 604
    .local v3, "ruleTextCopy":Ljava/lang/StringBuffer;
    iget-object v4, p0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v4}, Lcom/ibm/icu/text/NFSubstitution;->getPos()I

    move-result v4

    iget-object v5, p0, Lcom/ibm/icu/text/NFRule;->sub2:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v5}, Lcom/ibm/icu/text/NFSubstitution;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 605
    iget-object v4, p0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v4}, Lcom/ibm/icu/text/NFSubstitution;->getPos()I

    move-result v4

    iget-object v5, p0, Lcom/ibm/icu/text/NFRule;->sub1:Lcom/ibm/icu/text/NFSubstitution;

    invoke-virtual {v5}, Lcom/ibm/icu/text/NFSubstitution;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 606
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 610
    const/16 v4, 0x3b

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 611
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 566
    .end local v3    # "ruleTextCopy":Ljava/lang/StringBuffer;
    :cond_2
    iget-wide v4, p0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    const-wide/16 v6, -0x2

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    .line 567
    const-string/jumbo v4, "x.x: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 569
    :cond_3
    iget-wide v4, p0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    const-wide/16 v6, -0x3

    cmp-long v4, v4, v6

    if-nez v4, :cond_4

    .line 570
    const-string/jumbo v4, "0.x: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 572
    :cond_4
    iget-wide v4, p0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    const-wide/16 v6, -0x4

    cmp-long v4, v4, v6

    if-nez v4, :cond_5

    .line 573
    const-string/jumbo v4, "x.0: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 583
    :cond_5
    iget-wide v4, p0, Lcom/ibm/icu/text/NFRule;->baseValue:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 584
    iget v4, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    const/16 v5, 0xa

    if-eq v4, v5, :cond_6

    .line 585
    const/16 v4, 0x2f

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 586
    iget v4, p0, Lcom/ibm/icu/text/NFRule;->radix:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 588
    :cond_6
    invoke-direct {p0}, Lcom/ibm/icu/text/NFRule;->expectedExponent()S

    move-result v4

    iget-short v5, p0, Lcom/ibm/icu/text/NFRule;->exponent:S

    sub-int v1, v4, v5

    .line 589
    .local v1, "numCarets":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_7

    .line 590
    const/16 v4, 0x3e

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 589
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 591
    :cond_7
    const-string/jumbo v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0
.end method
