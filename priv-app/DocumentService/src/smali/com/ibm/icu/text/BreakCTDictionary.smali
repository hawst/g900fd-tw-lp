.class Lcom/ibm/icu/text/BreakCTDictionary;
.super Ljava/lang/Object;
.source "BreakCTDictionary.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;,
        Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;,
        Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHorizontalNode;,
        Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodeFlags;,
        Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;
    }
.end annotation


# static fields
.field private static final DATA_FORMAT_ID:[B


# instance fields
.field private fData:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;

.field private nodes:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/text/BreakCTDictionary;->DATA_FORMAT_ID:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x54t
        0x72t
        0x44t
        0x63t
    .end array-data
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 3
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    sget-object v1, Lcom/ibm/icu/text/BreakCTDictionary;->DATA_FORMAT_ID:[B

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/ibm/icu/impl/ICUBinary;->readHeader(Ljava/io/InputStream;[BLcom/ibm/icu/impl/ICUBinary$Authenticate;)[B

    .line 111
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 113
    .local v0, "in":Ljava/io/DataInputStream;
    new-instance v1, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;

    invoke-direct {v1}, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;-><init>()V

    iput-object v1, p0, Lcom/ibm/icu/text/BreakCTDictionary;->fData:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;

    .line 114
    iget-object v1, p0, Lcom/ibm/icu/text/BreakCTDictionary;->fData:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iput v2, v1, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;->size:I

    .line 115
    iget-object v1, p0, Lcom/ibm/icu/text/BreakCTDictionary;->fData:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iput v2, v1, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;->magic:I

    .line 116
    iget-object v1, p0, Lcom/ibm/icu/text/BreakCTDictionary;->fData:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readShort()S

    move-result v2

    iput v2, v1, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;->nodeCount:I

    .line 117
    iget-object v1, p0, Lcom/ibm/icu/text/BreakCTDictionary;->fData:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readShort()S

    move-result v2

    iput v2, v1, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;->root:I

    .line 119
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/BreakCTDictionary;->loadBreakCTDictionary(Ljava/io/DataInputStream;)V

    .line 120
    return-void
.end method

.method private getCompactTrieNode(I)Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;
    .locals 1
    .param p1, "node"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lcom/ibm/icu/text/BreakCTDictionary;->nodes:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    aget-object v0, v0, p1

    return-object v0
.end method

.method private loadBreakCTDictionary(Ljava/io/DataInputStream;)V
    .locals 11
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 125
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->fData:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;

    iget v7, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;->nodeCount:I

    if-ge v1, v7, :cond_0

    .line 126
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    .line 125
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 130
    :cond_0
    iget-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->fData:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;

    iget v7, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;->nodeCount:I

    new-array v7, v7, [Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    iput-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->nodes:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    .line 131
    iget-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->nodes:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    new-instance v8, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    invoke-direct {v8}, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;-><init>()V

    aput-object v8, v7, v6

    .line 134
    const/4 v3, 0x1

    .local v3, "j":I
    :goto_1
    iget-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->fData:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;

    iget v7, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;->nodeCount:I

    if-ge v3, v7, :cond_4

    .line 135
    iget-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->nodes:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    new-instance v8, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    invoke-direct {v8}, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;-><init>()V

    aput-object v8, v7, v3

    .line 136
    iget-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->nodes:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    aget-object v7, v7, v3

    invoke-virtual {p1}, Ljava/io/DataInputStream;->readShort()S

    move-result v8

    iput-short v8, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;->flagscount:S

    .line 138
    iget-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->nodes:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    aget-object v7, v7, v3

    iget-short v7, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;->flagscount:S

    and-int/lit16 v0, v7, 0xfff

    .line 140
    .local v0, "count":I
    if-eqz v0, :cond_3

    .line 141
    iget-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->nodes:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    aget-object v7, v7, v3

    iget-short v7, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;->flagscount:S

    and-int/lit16 v7, v7, 0x1000

    if-eqz v7, :cond_1

    const/4 v2, 0x1

    .line 144
    .local v2, "isVerticalNode":Z
    :goto_2
    if-eqz v2, :cond_2

    .line 145
    iget-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->nodes:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    aget-object v7, v7, v3

    new-instance v8, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;

    invoke-direct {v8}, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;-><init>()V

    iput-object v8, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;->vnode:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;

    .line 146
    iget-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->nodes:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;->vnode:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;

    invoke-virtual {p1}, Ljava/io/DataInputStream;->readShort()S

    move-result v8

    iput v8, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;->equal:I

    .line 148
    iget-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->nodes:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;->vnode:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;

    new-array v8, v0, [C

    iput-object v8, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;->chars:[C

    .line 149
    const/4 v4, 0x0

    .local v4, "l":I
    :goto_3
    if-ge v4, v0, :cond_3

    .line 150
    iget-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->nodes:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;->vnode:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;

    iget-object v7, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;->chars:[C

    invoke-virtual {p1}, Ljava/io/DataInputStream;->readChar()C

    move-result v8

    aput-char v8, v7, v4

    .line 149
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .end local v2    # "isVerticalNode":Z
    .end local v4    # "l":I
    :cond_1
    move v2, v6

    .line 141
    goto :goto_2

    .line 153
    .restart local v2    # "isVerticalNode":Z
    :cond_2
    iget-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->nodes:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    aget-object v7, v7, v3

    new-array v8, v0, [Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHorizontalNode;

    iput-object v8, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;->hnode:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHorizontalNode;

    .line 154
    const/4 v5, 0x0

    .local v5, "n":I
    :goto_4
    if-ge v5, v0, :cond_3

    .line 155
    iget-object v7, p0, Lcom/ibm/icu/text/BreakCTDictionary;->nodes:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;->hnode:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHorizontalNode;

    new-instance v8, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHorizontalNode;

    invoke-virtual {p1}, Ljava/io/DataInputStream;->readChar()C

    move-result v9

    invoke-virtual {p1}, Ljava/io/DataInputStream;->readShort()S

    move-result v10

    invoke-direct {v8, v9, v10}, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHorizontalNode;-><init>(CI)V

    aput-object v8, v7, v5

    .line 154
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 134
    .end local v2    # "isVerticalNode":Z
    .end local v5    # "n":I
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 161
    .end local v0    # "count":I
    :cond_4
    return-void
.end method


# virtual methods
.method public matches(Ljava/text/CharacterIterator;I[I[II)I
    .locals 15
    .param p1, "text"    # Ljava/text/CharacterIterator;
    .param p2, "maxLength"    # I
    .param p3, "lengths"    # [I
    .param p4, "count"    # [I
    .param p5, "limit"    # I

    .prologue
    .line 184
    iget-object v14, p0, Lcom/ibm/icu/text/BreakCTDictionary;->fData:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;

    iget v14, v14, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHeader;->root:I

    invoke-direct {p0, v14}, Lcom/ibm/icu/text/BreakCTDictionary;->getCompactTrieNode(I)Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    move-result-object v10

    .line 185
    .local v10, "node":Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;
    const/4 v8, 0x0

    .line 187
    .local v8, "mycount":I
    invoke-interface/range {p1 .. p1}, Ljava/text/CharacterIterator;->current()C

    move-result v12

    .line 188
    .local v12, "uc":C
    const/4 v4, 0x0

    .line 189
    .local v4, "i":I
    const/4 v1, 0x0

    .local v1, "exitFlag":Z
    move v9, v8

    .line 191
    .end local v8    # "mycount":I
    .local v9, "mycount":I
    :goto_0
    if-eqz v10, :cond_9

    .line 193
    if-lez p5, :cond_8

    iget-short v14, v10, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;->flagscount:S

    and-int/lit16 v14, v14, 0x2000

    if-eqz v14, :cond_8

    .line 195
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "mycount":I
    .restart local v8    # "mycount":I
    aput v4, p3, v9

    .line 196
    add-int/lit8 p5, p5, -0x1

    .line 203
    :goto_1
    move/from16 v0, p2

    if-lt v4, v0, :cond_1

    .line 257
    :cond_0
    :goto_2
    const/4 v14, 0x0

    aput v8, p4, v14

    .line 258
    return v4

    .line 207
    :cond_1
    iget-short v14, v10, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;->flagscount:S

    and-int/lit16 v11, v14, 0xfff

    .line 208
    .local v11, "nodeCount":I
    if-eqz v11, :cond_0

    .line 212
    iget-short v14, v10, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;->flagscount:S

    and-int/lit16 v14, v14, 0x1000

    if-eqz v14, :cond_5

    .line 214
    iget-object v13, v10, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;->vnode:Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;

    .line 215
    .local v13, "vnode":Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_3
    if-ge v5, v11, :cond_2

    move/from16 v0, p2

    if-ge v4, v0, :cond_2

    .line 216
    iget-object v14, v13, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;->chars:[C

    aget-char v14, v14, v5

    if-eq v12, v14, :cond_4

    .line 218
    const/4 v1, 0x1

    .line 225
    :cond_2
    if-nez v1, :cond_0

    .line 231
    iget v14, v13, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;->equal:I

    invoke-direct {p0, v14}, Lcom/ibm/icu/text/BreakCTDictionary;->getCompactTrieNode(I)Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    move-result-object v10

    .end local v5    # "j":I
    .end local v13    # "vnode":Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;
    :cond_3
    :goto_4
    move v9, v8

    .line 255
    .end local v8    # "mycount":I
    .restart local v9    # "mycount":I
    goto :goto_0

    .line 221
    .end local v9    # "mycount":I
    .restart local v5    # "j":I
    .restart local v8    # "mycount":I
    .restart local v13    # "vnode":Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;
    :cond_4
    invoke-interface/range {p1 .. p1}, Ljava/text/CharacterIterator;->next()C

    .line 222
    invoke-interface/range {p1 .. p1}, Ljava/text/CharacterIterator;->current()C

    move-result v12

    .line 223
    add-int/lit8 v4, v4, 0x1

    .line 215
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 234
    .end local v5    # "j":I
    .end local v13    # "vnode":Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieVerticalNode;
    :cond_5
    iget-object v3, v10, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;->hnode:[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHorizontalNode;

    .line 235
    .local v3, "hnode":[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHorizontalNode;
    const/4 v6, 0x0

    .line 236
    .local v6, "low":I
    add-int/lit8 v2, v11, -0x1

    .line 238
    .local v2, "high":I
    const/4 v10, 0x0

    .line 239
    :goto_5
    if-lt v2, v6, :cond_3

    .line 240
    add-int v14, v2, v6

    div-int/lit8 v7, v14, 0x2

    .line 241
    .local v7, "middle":I
    aget-object v14, v3, v7

    iget-char v14, v14, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHorizontalNode;->ch:C

    if-ne v12, v14, :cond_6

    .line 243
    aget-object v14, v3, v7

    iget v14, v14, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHorizontalNode;->equal:I

    invoke-direct {p0, v14}, Lcom/ibm/icu/text/BreakCTDictionary;->getCompactTrieNode(I)Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieNodes;

    move-result-object v10

    .line 244
    invoke-interface/range {p1 .. p1}, Ljava/text/CharacterIterator;->next()C

    .line 245
    invoke-interface/range {p1 .. p1}, Ljava/text/CharacterIterator;->current()C

    move-result v12

    .line 246
    add-int/lit8 v4, v4, 0x1

    .line 247
    goto :goto_4

    .line 248
    :cond_6
    aget-object v14, v3, v7

    iget-char v14, v14, Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHorizontalNode;->ch:C

    if-ge v12, v14, :cond_7

    .line 249
    add-int/lit8 v2, v7, -0x1

    .line 250
    goto :goto_5

    .line 251
    :cond_7
    add-int/lit8 v6, v7, 0x1

    .line 253
    goto :goto_5

    .end local v2    # "high":I
    .end local v3    # "hnode":[Lcom/ibm/icu/text/BreakCTDictionary$CompactTrieHorizontalNode;
    .end local v6    # "low":I
    .end local v7    # "middle":I
    .end local v8    # "mycount":I
    .end local v11    # "nodeCount":I
    .restart local v9    # "mycount":I
    :cond_8
    move v8, v9

    .end local v9    # "mycount":I
    .restart local v8    # "mycount":I
    goto :goto_1

    .end local v8    # "mycount":I
    .restart local v9    # "mycount":I
    :cond_9
    move v8, v9

    .end local v9    # "mycount":I
    .restart local v8    # "mycount":I
    goto :goto_2
.end method
