.class Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;
.super Ljava/lang/Object;
.source "TransliteratorIDParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/TransliteratorIDParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SingleID"
.end annotation


# instance fields
.field public basicID:Ljava/lang/String;

.field public canonID:Ljava/lang/String;

.field public filter:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "c"    # Ljava/lang/String;
    .param p2, "b"    # Ljava/lang/String;

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "c"    # Ljava/lang/String;
    .param p2, "b"    # Ljava/lang/String;
    .param p3, "f"    # Ljava/lang/String;

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->canonID:Ljava/lang/String;

    .line 111
    iput-object p2, p0, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->basicID:Ljava/lang/String;

    .line 112
    iput-object p3, p0, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->filter:Ljava/lang/String;

    .line 113
    return-void
.end method


# virtual methods
.method getInstance()Lcom/ibm/icu/text/Transliterator;
    .locals 3

    .prologue
    .line 119
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->basicID:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->basicID:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 120
    :cond_0
    const-string/jumbo v1, "Any-Null"

    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->canonID:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/ibm/icu/text/Transliterator;->getBasicInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;

    move-result-object v0

    .line 124
    .local v0, "t":Lcom/ibm/icu/text/Transliterator;
    :goto_0
    if-eqz v0, :cond_1

    .line 125
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->filter:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 126
    new-instance v1, Lcom/ibm/icu/text/UnicodeSet;

    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->filter:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/ibm/icu/text/UnicodeSet;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/Transliterator;->setFilter(Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 129
    :cond_1
    return-object v0

    .line 122
    .end local v0    # "t":Lcom/ibm/icu/text/Transliterator;
    :cond_2
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->basicID:Ljava/lang/String;

    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorIDParser$SingleID;->canonID:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/ibm/icu/text/Transliterator;->getBasicInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;

    move-result-object v0

    .restart local v0    # "t":Lcom/ibm/icu/text/Transliterator;
    goto :goto_0
.end method
