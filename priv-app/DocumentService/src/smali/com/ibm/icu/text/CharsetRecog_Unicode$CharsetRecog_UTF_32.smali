.class abstract Lcom/ibm/icu/text/CharsetRecog_Unicode$CharsetRecog_UTF_32;
.super Lcom/ibm/icu/text/CharsetRecog_Unicode;
.source "CharsetRecog_Unicode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/CharsetRecog_Unicode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "CharsetRecog_UTF_32"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/ibm/icu/text/CharsetRecog_Unicode;-><init>()V

    return-void
.end method


# virtual methods
.method abstract getChar([BI)I
.end method

.method abstract getName()Ljava/lang/String;
.end method

.method match(Lcom/ibm/icu/text/CharsetDetector;)I
    .locals 10
    .param p1, "det"    # Lcom/ibm/icu/text/CharsetDetector;

    .prologue
    const/4 v8, 0x0

    .line 83
    iget-object v4, p1, Lcom/ibm/icu/text/CharsetDetector;->fRawInput:[B

    .line 84
    .local v4, "input":[B
    iget v9, p1, Lcom/ibm/icu/text/CharsetDetector;->fRawLength:I

    div-int/lit8 v9, v9, 0x4

    mul-int/lit8 v5, v9, 0x4

    .line 85
    .local v5, "limit":I
    const/4 v7, 0x0

    .line 86
    .local v7, "numValid":I
    const/4 v6, 0x0

    .line 87
    .local v6, "numInvalid":I
    const/4 v2, 0x0

    .line 88
    .local v2, "hasBOM":Z
    const/4 v1, 0x0

    .line 90
    .local v1, "confidence":I
    if-nez v5, :cond_0

    .line 123
    :goto_0
    return v8

    .line 93
    :cond_0
    invoke-virtual {p0, v4, v8}, Lcom/ibm/icu/text/CharsetRecog_Unicode$CharsetRecog_UTF_32;->getChar([BI)I

    move-result v8

    const v9, 0xfeff

    if-ne v8, v9, :cond_1

    .line 94
    const/4 v2, 0x1

    .line 97
    :cond_1
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v5, :cond_4

    .line 98
    invoke-virtual {p0, v4, v3}, Lcom/ibm/icu/text/CharsetRecog_Unicode$CharsetRecog_UTF_32;->getChar([BI)I

    move-result v0

    .line 100
    .local v0, "ch":I
    if-ltz v0, :cond_2

    const v8, 0x10ffff

    if-ge v0, v8, :cond_2

    const v8, 0xd800

    if-lt v0, v8, :cond_3

    const v8, 0xdfff

    if-gt v0, v8, :cond_3

    .line 101
    :cond_2
    add-int/lit8 v6, v6, 0x1

    .line 97
    :goto_2
    add-int/lit8 v3, v3, 0x4

    goto :goto_1

    .line 103
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 110
    .end local v0    # "ch":I
    :cond_4
    if-eqz v2, :cond_6

    if-nez v6, :cond_6

    .line 111
    const/16 v1, 0x64

    :cond_5
    :goto_3
    move v8, v1

    .line 123
    goto :goto_0

    .line 112
    :cond_6
    if-eqz v2, :cond_7

    mul-int/lit8 v8, v6, 0xa

    if-le v7, v8, :cond_7

    .line 113
    const/16 v1, 0x50

    .line 114
    goto :goto_3

    :cond_7
    const/4 v8, 0x3

    if-le v7, v8, :cond_8

    if-nez v6, :cond_8

    .line 115
    const/16 v1, 0x64

    .line 116
    goto :goto_3

    :cond_8
    if-lez v7, :cond_9

    if-nez v6, :cond_9

    .line 117
    const/16 v1, 0x50

    .line 118
    goto :goto_3

    :cond_9
    mul-int/lit8 v8, v6, 0xa

    if-le v7, v8, :cond_5

    .line 120
    const/16 v1, 0x19

    goto :goto_3
.end method
