.class Lcom/ibm/icu/text/ModulusSubstitution;
.super Lcom/ibm/icu/text/NFSubstitution;
.source "NFSubstitution.java"


# instance fields
.field divisor:D

.field ruleToUse:Lcom/ibm/icu/text/NFRule;


# direct methods
.method constructor <init>(IDLcom/ibm/icu/text/NFRule;Lcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V
    .locals 4
    .param p1, "pos"    # I
    .param p2, "divisor"    # D
    .param p4, "rulePredecessor"    # Lcom/ibm/icu/text/NFRule;
    .param p5, "ruleSet"    # Lcom/ibm/icu/text/NFRuleSet;
    .param p6, "formatter"    # Lcom/ibm/icu/text/RuleBasedNumberFormat;
    .param p7, "description"    # Ljava/lang/String;

    .prologue
    .line 815
    invoke-direct {p0, p1, p5, p6, p7}, Lcom/ibm/icu/text/NFSubstitution;-><init>(ILcom/ibm/icu/text/NFRuleSet;Lcom/ibm/icu/text/RuleBasedNumberFormat;Ljava/lang/String;)V

    .line 820
    iput-wide p2, p0, Lcom/ibm/icu/text/ModulusSubstitution;->divisor:D

    .line 822
    const-wide/16 v0, 0x0

    cmpl-double v0, p2, v0

    if-nez v0, :cond_0

    .line 823
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Substitution with bad divisor ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, ") "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p7, v2, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p7, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 832
    :cond_0
    const-string/jumbo v0, ">>>"

    invoke-virtual {p7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 833
    iput-object p4, p0, Lcom/ibm/icu/text/ModulusSubstitution;->ruleToUse:Lcom/ibm/icu/text/NFRule;

    .line 837
    :goto_0
    return-void

    .line 835
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/ModulusSubstitution;->ruleToUse:Lcom/ibm/icu/text/NFRule;

    goto :goto_0
.end method


# virtual methods
.method public calcUpperBound(D)D
    .locals 2
    .param p1, "oldUpperBound"    # D

    .prologue
    .line 1008
    iget-wide v0, p0, Lcom/ibm/icu/text/ModulusSubstitution;->divisor:D

    return-wide v0
.end method

.method public composeRuleValue(DD)D
    .locals 3
    .param p1, "newRuleValue"    # D
    .param p3, "oldRuleValue"    # D

    .prologue
    .line 999
    iget-wide v0, p0, Lcom/ibm/icu/text/ModulusSubstitution;->divisor:D

    rem-double v0, p3, v0

    sub-double v0, p3, v0

    add-double/2addr v0, p1

    return-wide v0
.end method

.method public doParse(Ljava/lang/String;Ljava/text/ParsePosition;DDZ)Ljava/lang/Number;
    .locals 9
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "parsePosition"    # Ljava/text/ParsePosition;
    .param p3, "baseValue"    # D
    .param p5, "upperBound"    # D
    .param p7, "lenientParse"    # Z

    .prologue
    .line 961
    iget-object v0, p0, Lcom/ibm/icu/text/ModulusSubstitution;->ruleToUse:Lcom/ibm/icu/text/NFRule;

    if-nez v0, :cond_1

    .line 962
    invoke-super/range {p0 .. p7}, Lcom/ibm/icu/text/NFSubstitution;->doParse(Ljava/lang/String;Ljava/text/ParsePosition;DDZ)Ljava/lang/Number;

    move-result-object v8

    .line 980
    :cond_0
    :goto_0
    return-object v8

    .line 968
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/ModulusSubstitution;->ruleToUse:Lcom/ibm/icu/text/NFRule;

    const/4 v3, 0x0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/text/NFRule;->doParse(Ljava/lang/String;Ljava/text/ParsePosition;ZD)Ljava/lang/Number;

    move-result-object v8

    .line 970
    .local v8, "tempResult":Ljava/lang/Number;
    invoke-virtual {p2}, Ljava/text/ParsePosition;->getIndex()I

    move-result v0

    if-eqz v0, :cond_0

    .line 971
    invoke-virtual {v8}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v6

    .line 973
    .local v6, "result":D
    invoke-virtual {p0, v6, v7, p3, p4}, Lcom/ibm/icu/text/ModulusSubstitution;->composeRuleValue(DD)D

    move-result-wide v6

    .line 974
    double-to-long v0, v6

    long-to-double v0, v0

    cmpl-double v0, v6, v0

    if-nez v0, :cond_2

    .line 975
    new-instance v8, Ljava/lang/Long;

    .end local v8    # "tempResult":Ljava/lang/Number;
    double-to-long v0, v6

    invoke-direct {v8, v0, v1}, Ljava/lang/Long;-><init>(J)V

    goto :goto_0

    .line 977
    .restart local v8    # "tempResult":Ljava/lang/Number;
    :cond_2
    new-instance v8, Ljava/lang/Double;

    .end local v8    # "tempResult":Ljava/lang/Number;
    invoke-direct {v8, v6, v7}, Ljava/lang/Double;-><init>(D)V

    goto :goto_0
.end method

.method public doSubstitution(DLjava/lang/StringBuffer;I)V
    .locals 5
    .param p1, "number"    # D
    .param p3, "toInsertInto"    # Ljava/lang/StringBuffer;
    .param p4, "position"    # I

    .prologue
    .line 912
    iget-object v2, p0, Lcom/ibm/icu/text/ModulusSubstitution;->ruleToUse:Lcom/ibm/icu/text/NFRule;

    if-nez v2, :cond_0

    .line 913
    invoke-super {p0, p1, p2, p3, p4}, Lcom/ibm/icu/text/NFSubstitution;->doSubstitution(DLjava/lang/StringBuffer;I)V

    .line 922
    :goto_0
    return-void

    .line 918
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/ModulusSubstitution;->transformNumber(D)D

    move-result-wide v0

    .line 920
    .local v0, "numberToFormat":D
    iget-object v2, p0, Lcom/ibm/icu/text/ModulusSubstitution;->ruleToUse:Lcom/ibm/icu/text/NFRule;

    iget v3, p0, Lcom/ibm/icu/text/ModulusSubstitution;->pos:I

    add-int/2addr v3, p4

    invoke-virtual {v2, v0, v1, p3, v3}, Lcom/ibm/icu/text/NFRule;->doFormat(DLjava/lang/StringBuffer;I)V

    goto :goto_0
.end method

.method public doSubstitution(JLjava/lang/StringBuffer;I)V
    .locals 5
    .param p1, "number"    # J
    .param p3, "toInsertInto"    # Ljava/lang/StringBuffer;
    .param p4, "position"    # I

    .prologue
    .line 889
    iget-object v2, p0, Lcom/ibm/icu/text/ModulusSubstitution;->ruleToUse:Lcom/ibm/icu/text/NFRule;

    if-nez v2, :cond_0

    .line 890
    invoke-super {p0, p1, p2, p3, p4}, Lcom/ibm/icu/text/NFSubstitution;->doSubstitution(JLjava/lang/StringBuffer;I)V

    .line 898
    :goto_0
    return-void

    .line 895
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/ModulusSubstitution;->transformNumber(J)J

    move-result-wide v0

    .line 896
    .local v0, "numberToFormat":J
    iget-object v2, p0, Lcom/ibm/icu/text/ModulusSubstitution;->ruleToUse:Lcom/ibm/icu/text/NFRule;

    iget v3, p0, Lcom/ibm/icu/text/ModulusSubstitution;->pos:I

    add-int/2addr v3, p4

    invoke-virtual {v2, v0, v1, p3, v3}, Lcom/ibm/icu/text/NFRule;->doFormat(JLjava/lang/StringBuffer;I)V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "that"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 864
    invoke-super {p0, p1}, Lcom/ibm/icu/text/NFSubstitution;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 865
    check-cast v0, Lcom/ibm/icu/text/ModulusSubstitution;

    .line 867
    .local v0, "that2":Lcom/ibm/icu/text/ModulusSubstitution;
    iget-wide v2, p0, Lcom/ibm/icu/text/ModulusSubstitution;->divisor:D

    iget-wide v4, v0, Lcom/ibm/icu/text/ModulusSubstitution;->divisor:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 869
    .end local v0    # "that2":Lcom/ibm/icu/text/ModulusSubstitution;
    :cond_0
    return v1
.end method

.method public isModulusSubstitution()Z
    .locals 1

    .prologue
    .line 1020
    const/4 v0, 0x1

    return v0
.end method

.method public setDivisor(II)V
    .locals 4
    .param p1, "radix"    # I
    .param p2, "exponent"    # I

    .prologue
    .line 846
    int-to-double v0, p1

    int-to-double v2, p2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/ibm/icu/text/ModulusSubstitution;->divisor:D

    .line 848
    iget-wide v0, p0, Lcom/ibm/icu/text/ModulusSubstitution;->divisor:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 849
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Substitution with bad divisor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 851
    :cond_0
    return-void
.end method

.method tokenChar()C
    .locals 1

    .prologue
    .line 1028
    const/16 v0, 0x3e

    return v0
.end method

.method public transformNumber(D)D
    .locals 3
    .param p1, "number"    # D

    .prologue
    .line 941
    iget-wide v0, p0, Lcom/ibm/icu/text/ModulusSubstitution;->divisor:D

    rem-double v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public transformNumber(J)J
    .locals 5
    .param p1, "number"    # J

    .prologue
    .line 931
    long-to-double v0, p1

    iget-wide v2, p0, Lcom/ibm/icu/text/ModulusSubstitution;->divisor:D

    rem-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    return-wide v0
.end method
