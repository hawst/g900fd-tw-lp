.class Lcom/ibm/icu/text/ReplaceableContextIterator;
.super Ljava/lang/Object;
.source "ReplaceableContextIterator.java"

# interfaces
.implements Lcom/ibm/icu/impl/UCaseProps$ContextIterator;


# instance fields
.field protected contextLimit:I

.field protected contextStart:I

.field protected cpLimit:I

.field protected cpStart:I

.field protected dir:I

.field protected index:I

.field protected limit:I

.field protected reachedLimit:Z

.field protected rep:Lcom/ibm/icu/text/Replaceable;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->rep:Lcom/ibm/icu/text/Replaceable;

    .line 37
    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextLimit:I

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextStart:I

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->index:I

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpLimit:I

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpStart:I

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->limit:I

    .line 38
    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->dir:I

    .line 39
    iput-boolean v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->reachedLimit:Z

    .line 40
    return-void
.end method


# virtual methods
.method public didReachLimit()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->reachedLimit:Z

    return v0
.end method

.method public getCaseMapCPStart()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpStart:I

    return v0
.end method

.method public next()I
    .locals 3

    .prologue
    .line 175
    iget v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->dir:I

    if-lez v1, :cond_2

    .line 176
    iget v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->index:I

    iget v2, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextLimit:I

    if-ge v1, v2, :cond_0

    .line 177
    iget-object v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->rep:Lcom/ibm/icu/text/Replaceable;

    iget v2, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->index:I

    invoke-interface {v1, v2}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v0

    .line 178
    .local v0, "c":I
    iget v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->index:I

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->index:I

    .line 189
    .end local v0    # "c":I
    :goto_0
    return v0

    .line 182
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->reachedLimit:Z

    .line 189
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 184
    :cond_2
    iget v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->dir:I

    if-gez v1, :cond_1

    iget v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->index:I

    iget v2, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextStart:I

    if-le v1, v2, :cond_1

    .line 185
    iget-object v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->rep:Lcom/ibm/icu/text/Replaceable;

    iget v2, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->index:I

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v0

    .line 186
    .restart local v0    # "c":I
    iget v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->index:I

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->index:I

    goto :goto_0
.end method

.method public nextCaseMapCP()I
    .locals 3

    .prologue
    .line 120
    iget v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpLimit:I

    iget v2, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->limit:I

    if-ge v1, v2, :cond_0

    .line 121
    iget v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpLimit:I

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpStart:I

    .line 122
    iget-object v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->rep:Lcom/ibm/icu/text/Replaceable;

    iget v2, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpLimit:I

    invoke-interface {v1, v2}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v0

    .line 123
    .local v0, "c":I
    iget v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpLimit:I

    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpLimit:I

    .line 126
    .end local v0    # "c":I
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public replace(Ljava/lang/String;)I
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 138
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    iget v2, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpLimit:I

    iget v3, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpStart:I

    sub-int/2addr v2, v3

    sub-int v0, v1, v2

    .line 139
    .local v0, "delta":I
    iget-object v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->rep:Lcom/ibm/icu/text/Replaceable;

    iget v2, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpStart:I

    iget v3, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpLimit:I

    invoke-interface {v1, v2, v3, p1}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 140
    iget v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpLimit:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpLimit:I

    .line 141
    iget v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->limit:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->limit:I

    .line 142
    iget v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextLimit:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextLimit:I

    .line 143
    return v0
.end method

.method public reset(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    const/4 v1, 0x0

    .line 156
    if-lez p1, :cond_0

    .line 158
    const/4 v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->dir:I

    .line 159
    iget v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpLimit:I

    iput v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->index:I

    .line 169
    :goto_0
    iput-boolean v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->reachedLimit:Z

    .line 170
    return-void

    .line 160
    :cond_0
    if-gez p1, :cond_1

    .line 162
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->dir:I

    .line 163
    iget v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpStart:I

    iput v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->index:I

    goto :goto_0

    .line 166
    :cond_1
    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->dir:I

    .line 167
    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->index:I

    goto :goto_0
.end method

.method public setContextLimits(II)V
    .locals 2
    .param p1, "contextStart"    # I
    .param p2, "contextLimit"    # I

    .prologue
    const/4 v1, 0x0

    .line 95
    if-gez p1, :cond_0

    .line 96
    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextStart:I

    .line 102
    :goto_0
    iget v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextStart:I

    if-ge p2, v0, :cond_2

    .line 103
    iget v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextStart:I

    iput v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextLimit:I

    .line 109
    :goto_1
    iput-boolean v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->reachedLimit:Z

    .line 110
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->rep:Lcom/ibm/icu/text/Replaceable;

    invoke-interface {v0}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v0

    if-gt p1, v0, :cond_1

    .line 98
    iput p1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextStart:I

    goto :goto_0

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->rep:Lcom/ibm/icu/text/Replaceable;

    invoke-interface {v0}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextStart:I

    goto :goto_0

    .line 104
    :cond_2
    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->rep:Lcom/ibm/icu/text/Replaceable;

    invoke-interface {v0}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v0

    if-gt p2, v0, :cond_3

    .line 105
    iput p2, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextLimit:I

    goto :goto_1

    .line 107
    :cond_3
    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->rep:Lcom/ibm/icu/text/Replaceable;

    invoke-interface {v0}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextLimit:I

    goto :goto_1
.end method

.method public setIndex(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 59
    iput p1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpLimit:I

    iput p1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpStart:I

    .line 60
    iput v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->index:I

    .line 61
    iput v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->dir:I

    .line 62
    iput-boolean v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->reachedLimit:Z

    .line 63
    return-void
.end method

.method public setLimit(I)V
    .locals 1
    .param p1, "lim"    # I

    .prologue
    .line 81
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->rep:Lcom/ibm/icu/text/Replaceable;

    invoke-interface {v0}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v0

    if-gt p1, v0, :cond_0

    .line 82
    iput p1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->limit:I

    .line 86
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->reachedLimit:Z

    .line 87
    return-void

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->rep:Lcom/ibm/icu/text/Replaceable;

    invoke-interface {v0}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->limit:I

    goto :goto_0
.end method

.method public setText(Lcom/ibm/icu/text/Replaceable;)V
    .locals 2
    .param p1, "rep"    # Lcom/ibm/icu/text/Replaceable;

    .prologue
    const/4 v1, 0x0

    .line 47
    iput-object p1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->rep:Lcom/ibm/icu/text/Replaceable;

    .line 48
    invoke-interface {p1}, Lcom/ibm/icu/text/Replaceable;->length()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextLimit:I

    iput v0, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->limit:I

    .line 49
    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->contextStart:I

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->index:I

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpLimit:I

    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->cpStart:I

    .line 50
    iput v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->dir:I

    .line 51
    iput-boolean v1, p0, Lcom/ibm/icu/text/ReplaceableContextIterator;->reachedLimit:Z

    .line 52
    return-void
.end method
