.class Lcom/ibm/icu/text/TransliteratorRegistry;
.super Ljava/lang/Object;
.source "TransliteratorRegistry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;,
        Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;,
        Lcom/ibm/icu/text/TransliteratorRegistry$AliasEntry;,
        Lcom/ibm/icu/text/TransliteratorRegistry$LocaleEntry;,
        Lcom/ibm/icu/text/TransliteratorRegistry$ResourceEntry;,
        Lcom/ibm/icu/text/TransliteratorRegistry$Spec;
    }
.end annotation


# static fields
.field private static final ANY:Ljava/lang/String; = "Any"

.field private static final DEBUG:Z = false

.field private static final LOCALE_SEP:C = '_'

.field private static final NO_VARIANT:Ljava/lang/String; = ""


# instance fields
.field private availableIDs:Ljava/util/Vector;

.field private registry:Ljava/util/Hashtable;

.field private specDAG:Ljava/util/Hashtable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 285
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->registry:Ljava/util/Hashtable;

    .line 286
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->specDAG:Ljava/util/Hashtable;

    .line 287
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->availableIDs:Ljava/util/Vector;

    .line 288
    return-void
.end method

.method private find(Ljava/lang/String;)[Ljava/lang/Object;
    .locals 4
    .param p1, "ID"    # Ljava/lang/String;

    .prologue
    .line 717
    invoke-static {p1}, Lcom/ibm/icu/text/TransliteratorIDParser;->IDtoSTV(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 718
    .local v0, "stv":[Ljava/lang/String;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v2, 0x1

    aget-object v2, v0, v2

    const/4 v3, 0x2

    aget-object v3, v0, v3

    invoke-direct {p0, v1, v2, v3}, Lcom/ibm/icu/text/TransliteratorRegistry;->find(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method private find(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/Object;
    .locals 4
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "variant"    # Ljava/lang/String;

    .prologue
    .line 746
    new-instance v1, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;

    invoke-direct {v1, p1}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;-><init>(Ljava/lang/String;)V

    .line 747
    .local v1, "src":Lcom/ibm/icu/text/TransliteratorRegistry$Spec;
    new-instance v2, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;

    invoke-direct {v2, p2}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;-><init>(Ljava/lang/String;)V

    .line 748
    .local v2, "trg":Lcom/ibm/icu/text/TransliteratorRegistry$Spec;
    const/4 v0, 0x0

    .line 750
    .local v0, "entry":[Ljava/lang/Object;
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    .line 753
    invoke-direct {p0, v1, v2, p3}, Lcom/ibm/icu/text/TransliteratorRegistry;->findInDynamicStore(Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    .line 754
    if-eqz v0, :cond_0

    move-object v3, v0

    .line 790
    :goto_0
    return-object v3

    .line 759
    :cond_0
    invoke-direct {p0, v1, v2, p3}, Lcom/ibm/icu/text/TransliteratorRegistry;->findInStaticStore(Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    .line 760
    if-eqz v0, :cond_2

    move-object v3, v0

    .line 761
    goto :goto_0

    .line 787
    :cond_1
    invoke-virtual {v2}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->next()Ljava/lang/String;

    .line 766
    :cond_2
    invoke-virtual {v1}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->reset()V

    .line 769
    :goto_1
    const-string/jumbo v3, ""

    invoke-direct {p0, v1, v2, v3}, Lcom/ibm/icu/text/TransliteratorRegistry;->findInDynamicStore(Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    .line 770
    if-eqz v0, :cond_3

    move-object v3, v0

    .line 771
    goto :goto_0

    .line 775
    :cond_3
    const-string/jumbo v3, ""

    invoke-direct {p0, v1, v2, v3}, Lcom/ibm/icu/text/TransliteratorRegistry;->findInStaticStore(Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    .line 776
    if-eqz v0, :cond_4

    move-object v3, v0

    .line 777
    goto :goto_0

    .line 779
    :cond_4
    invoke-virtual {v1}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->hasFallback()Z

    move-result v3

    if-nez v3, :cond_5

    .line 784
    invoke-virtual {v2}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->hasFallback()Z

    move-result v3

    if-nez v3, :cond_1

    .line 790
    const/4 v3, 0x0

    goto :goto_0

    .line 782
    :cond_5
    invoke-virtual {v1}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->next()Ljava/lang/String;

    goto :goto_1
.end method

.method private findInBundle(Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Ljava/lang/String;I)[Ljava/lang/Object;
    .locals 10
    .param p1, "specToOpen"    # Lcom/ibm/icu/text/TransliteratorRegistry$Spec;
    .param p2, "specToFind"    # Lcom/ibm/icu/text/TransliteratorRegistry$Spec;
    .param p3, "variant"    # Ljava/lang/String;
    .param p4, "direction"    # I

    .prologue
    .line 648
    invoke-virtual {p1}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->getBundle()Ljava/util/ResourceBundle;

    move-result-object v3

    .line 650
    .local v3, "res":Ljava/util/ResourceBundle;
    if-nez v3, :cond_0

    .line 653
    const/4 v6, 0x0

    .line 710
    :goto_0
    return-object v6

    .line 656
    :cond_0
    const/4 v2, 0x0

    .local v2, "pass":I
    :goto_1
    const/4 v6, 0x2

    if-ge v2, v6, :cond_7

    .line 657
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 662
    .local v5, "tag":Ljava/lang/StringBuffer;
    if-nez v2, :cond_4

    .line 663
    if-nez p4, :cond_3

    const-string/jumbo v6, "TransliterateTo"

    :goto_2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 668
    :goto_3
    invoke-virtual {p2}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->get()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 674
    :try_start_0
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ResourceBundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 678
    .local v4, "subres":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 679
    .local v1, "i":I
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    .line 680
    const/4 v1, 0x0

    :goto_4
    array-length v6, v4

    if-ge v1, v6, :cond_1

    .line 681
    aget-object v6, v4, v1

    invoke-virtual {v6, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 687
    :cond_1
    array-length v6, v4

    if-ge v1, v6, :cond_2

    .line 699
    if-nez v2, :cond_6

    const/4 v0, 0x0

    .line 700
    .local v0, "dir":I
    :goto_5
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    new-instance v8, Lcom/ibm/icu/text/TransliteratorRegistry$LocaleEntry;

    add-int/lit8 v9, v1, 0x1

    aget-object v9, v4, v9

    invoke-direct {v8, v9, v0}, Lcom/ibm/icu/text/TransliteratorRegistry$LocaleEntry;-><init>(Ljava/lang/String;I)V

    aput-object v8, v6, v7
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 703
    .end local v0    # "dir":I
    .end local v1    # "i":I
    .end local v4    # "subres":[Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 656
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 663
    :cond_3
    const-string/jumbo v6, "TransliterateFrom"

    goto :goto_2

    .line 666
    :cond_4
    const-string/jumbo v6, "Transliterate"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 680
    .restart local v1    # "i":I
    .restart local v4    # "subres":[Ljava/lang/String;
    :cond_5
    add-int/lit8 v1, v1, 0x2

    goto :goto_4

    :cond_6
    move v0, p4

    .line 699
    goto :goto_5

    .line 710
    .end local v1    # "i":I
    .end local v4    # "subres":[Ljava/lang/String;
    .end local v5    # "tag":Ljava/lang/StringBuffer;
    :cond_7
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private findInDynamicStore(Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Ljava/lang/String;)[Ljava/lang/Object;
    .locals 3
    .param p1, "src"    # Lcom/ibm/icu/text/TransliteratorRegistry$Spec;
    .param p2, "trg"    # Lcom/ibm/icu/text/TransliteratorRegistry$Spec;
    .param p3, "variant"    # Ljava/lang/String;

    .prologue
    .line 592
    invoke-virtual {p1}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->get()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->get()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p3}, Lcom/ibm/icu/text/TransliteratorIDParser;->STVtoID(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 597
    .local v0, "ID":Ljava/lang/String;
    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->registry:Ljava/util/Hashtable;

    new-instance v2, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v2, v0}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    return-object v1
.end method

.method private findInStaticStore(Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Ljava/lang/String;)[Ljava/lang/Object;
    .locals 6
    .param p1, "src"    # Lcom/ibm/icu/text/TransliteratorRegistry$Spec;
    .param p2, "trg"    # Lcom/ibm/icu/text/TransliteratorRegistry$Spec;
    .param p3, "variant"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 617
    const/4 v4, 0x0

    .line 618
    .local v4, "entry":[Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->isLocale()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 619
    invoke-direct {p0, p1, p2, p3, v5}, Lcom/ibm/icu/text/TransliteratorRegistry;->findInBundle(Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Ljava/lang/String;I)[Ljava/lang/Object;

    move-result-object v4

    .line 626
    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    .line 627
    invoke-virtual {p1}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->getTop()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->getTop()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/TransliteratorRegistry;->registerEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 630
    :cond_1
    return-object v4

    .line 620
    :cond_2
    invoke-virtual {p2}, Lcom/ibm/icu/text/TransliteratorRegistry$Spec;->isLocale()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621
    const/4 v0, 0x1

    invoke-direct {p0, p2, p1, p3, v0}, Lcom/ibm/icu/text/TransliteratorRegistry;->findInBundle(Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Lcom/ibm/icu/text/TransliteratorRegistry$Spec;Ljava/lang/String;I)[Ljava/lang/Object;

    move-result-object v4

    goto :goto_0
.end method

.method private instantiateEntry(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/StringBuffer;)Lcom/ibm/icu/text/Transliterator;
    .locals 12
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "entryWrapper"    # [Ljava/lang/Object;
    .param p3, "aliasReturn"    # Ljava/lang/StringBuffer;

    .prologue
    .line 817
    :goto_0
    const/4 v7, 0x0

    aget-object v3, p2, v7

    .line 819
    .local v3, "entry":Ljava/lang/Object;
    instance-of v7, v3, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    if-eqz v7, :cond_0

    move-object v1, v3

    .line 820
    check-cast v1, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .line 821
    .local v1, "data":Lcom/ibm/icu/text/RuleBasedTransliterator$Data;
    new-instance v7, Lcom/ibm/icu/text/RuleBasedTransliterator;

    const/4 v8, 0x0

    invoke-direct {v7, p1, v1, v8}, Lcom/ibm/icu/text/RuleBasedTransliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/RuleBasedTransliterator$Data;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 836
    .end local v1    # "data":Lcom/ibm/icu/text/RuleBasedTransliterator$Data;
    .end local v3    # "entry":Ljava/lang/Object;
    :goto_1
    return-object v7

    .line 822
    .restart local v3    # "entry":Ljava/lang/Object;
    :cond_0
    instance-of v7, v3, Ljava/lang/Class;

    if-eqz v7, :cond_1

    .line 824
    :try_start_0
    check-cast v3, Ljava/lang/Class;

    .end local v3    # "entry":Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/ibm/icu/text/Transliterator;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 826
    :catch_0
    move-exception v7

    .line 827
    :goto_2
    const/4 v7, 0x0

    goto :goto_1

    .line 828
    .restart local v3    # "entry":Ljava/lang/Object;
    :cond_1
    instance-of v7, v3, Lcom/ibm/icu/text/TransliteratorRegistry$AliasEntry;

    if-eqz v7, :cond_2

    .line 829
    check-cast v3, Lcom/ibm/icu/text/TransliteratorRegistry$AliasEntry;

    .end local v3    # "entry":Ljava/lang/Object;
    iget-object v7, v3, Lcom/ibm/icu/text/TransliteratorRegistry$AliasEntry;->alias:Ljava/lang/String;

    invoke-virtual {p3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 830
    const/4 v7, 0x0

    goto :goto_1

    .line 831
    .restart local v3    # "entry":Ljava/lang/Object;
    :cond_2
    instance-of v7, v3, Lcom/ibm/icu/text/Transliterator$Factory;

    if-eqz v7, :cond_3

    .line 832
    check-cast v3, Lcom/ibm/icu/text/Transliterator$Factory;

    .end local v3    # "entry":Ljava/lang/Object;
    invoke-interface {v3, p1}, Lcom/ibm/icu/text/Transliterator$Factory;->getInstance(Ljava/lang/String;)Lcom/ibm/icu/text/Transliterator;

    move-result-object v7

    goto :goto_1

    .line 833
    .restart local v3    # "entry":Ljava/lang/Object;
    :cond_3
    instance-of v7, v3, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;

    if-eqz v7, :cond_4

    .line 834
    check-cast v3, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;

    .end local v3    # "entry":Ljava/lang/Object;
    invoke-virtual {v3}, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;->getInstance()Lcom/ibm/icu/text/Transliterator;

    move-result-object v7

    goto :goto_1

    .line 835
    .restart local v3    # "entry":Ljava/lang/Object;
    :cond_4
    instance-of v7, v3, Lcom/ibm/icu/text/Transliterator;

    if-eqz v7, :cond_5

    .line 836
    check-cast v3, Lcom/ibm/icu/text/Transliterator;

    .end local v3    # "entry":Ljava/lang/Object;
    move-object v7, v3

    goto :goto_1

    .line 845
    .restart local v3    # "entry":Ljava/lang/Object;
    :cond_5
    new-instance v5, Lcom/ibm/icu/text/TransliteratorParser;

    invoke-direct {v5}, Lcom/ibm/icu/text/TransliteratorParser;-><init>()V

    .line 849
    .local v5, "parser":Lcom/ibm/icu/text/TransliteratorParser;
    :try_start_1
    move-object v0, v3

    check-cast v0, Lcom/ibm/icu/text/TransliteratorRegistry$ResourceEntry;

    move-object v6, v0

    .line 850
    .local v6, "re":Lcom/ibm/icu/text/TransliteratorRegistry$ResourceEntry;
    iget-object v7, v6, Lcom/ibm/icu/text/TransliteratorRegistry$ResourceEntry;->resource:Ljava/lang/String;

    iget v8, v6, Lcom/ibm/icu/text/TransliteratorRegistry$ResourceEntry;->direction:I

    invoke-virtual {v5, v7, v8}, Lcom/ibm/icu/text/TransliteratorParser;->parse(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_1

    .line 864
    .end local v6    # "re":Lcom/ibm/icu/text/TransliteratorRegistry$ResourceEntry;
    :goto_3
    iget-object v7, v5, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v7

    if-nez v7, :cond_6

    iget-object v7, v5, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v7

    if-nez v7, :cond_6

    .line 867
    const/4 v7, 0x0

    new-instance v8, Lcom/ibm/icu/text/TransliteratorRegistry$AliasEntry;

    sget-object v9, Lcom/ibm/icu/text/NullTransliterator;->_ID:Ljava/lang/String;

    invoke-direct {v8, v9}, Lcom/ibm/icu/text/TransliteratorRegistry$AliasEntry;-><init>(Ljava/lang/String;)V

    aput-object v8, p2, v7

    goto :goto_0

    .line 852
    :catch_1
    move-exception v2

    .local v2, "e":Ljava/lang/ClassCastException;
    move-object v4, v3

    .line 855
    check-cast v4, Lcom/ibm/icu/text/TransliteratorRegistry$LocaleEntry;

    .line 856
    .local v4, "le":Lcom/ibm/icu/text/TransliteratorRegistry$LocaleEntry;
    iget-object v7, v4, Lcom/ibm/icu/text/TransliteratorRegistry$LocaleEntry;->rule:Ljava/lang/String;

    iget v8, v4, Lcom/ibm/icu/text/TransliteratorRegistry$LocaleEntry;->direction:I

    invoke-virtual {v5, v7, v8}, Lcom/ibm/icu/text/TransliteratorParser;->parse(Ljava/lang/String;I)V

    goto :goto_3

    .line 869
    .end local v2    # "e":Ljava/lang/ClassCastException;
    .end local v4    # "le":Lcom/ibm/icu/text/TransliteratorRegistry$LocaleEntry;
    :cond_6
    iget-object v7, v5, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v7

    if-nez v7, :cond_7

    iget-object v7, v5, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_7

    .line 872
    const/4 v7, 0x0

    iget-object v8, v5, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, p2, v7

    goto/16 :goto_0

    .line 874
    :cond_7
    iget-object v7, v5, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_9

    iget-object v7, v5, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v7

    if-nez v7, :cond_9

    .line 879
    iget-object v7, v5, Lcom/ibm/icu/text/TransliteratorParser;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    if-eqz v7, :cond_8

    .line 880
    const/4 v8, 0x0

    new-instance v9, Lcom/ibm/icu/text/TransliteratorRegistry$AliasEntry;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v10, v5, Lcom/ibm/icu/text/TransliteratorParser;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/ibm/icu/text/UnicodeSet;->toPattern(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string/jumbo v10, ";"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    iget-object v7, v5, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    const/4 v11, 0x0

    invoke-virtual {v7, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v10, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v9, v7}, Lcom/ibm/icu/text/TransliteratorRegistry$AliasEntry;-><init>(Ljava/lang/String;)V

    aput-object v9, p2, v8

    goto/16 :goto_0

    .line 883
    :cond_8
    const/4 v8, 0x0

    new-instance v9, Lcom/ibm/icu/text/TransliteratorRegistry$AliasEntry;

    iget-object v7, v5, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    const/4 v10, 0x0

    invoke-virtual {v7, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct {v9, v7}, Lcom/ibm/icu/text/TransliteratorRegistry$AliasEntry;-><init>(Ljava/lang/String;)V

    aput-object v9, p2, v8

    goto/16 :goto_0

    .line 886
    :cond_9
    const/4 v7, 0x0

    new-instance v8, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;

    iget-object v9, v5, Lcom/ibm/icu/text/TransliteratorParser;->idBlockVector:Ljava/util/Vector;

    iget-object v10, v5, Lcom/ibm/icu/text/TransliteratorParser;->dataVector:Ljava/util/Vector;

    iget-object v11, v5, Lcom/ibm/icu/text/TransliteratorParser;->compoundFilter:Lcom/ibm/icu/text/UnicodeSet;

    invoke-direct {v8, p1, v9, v10, v11}, Lcom/ibm/icu/text/TransliteratorRegistry$CompoundRBTEntry;-><init>(Ljava/lang/String;Ljava/util/Vector;Ljava/util/Vector;Lcom/ibm/icu/text/UnicodeSet;)V

    aput-object v8, p2, v7

    goto/16 :goto_0

    .line 825
    .end local v3    # "entry":Ljava/lang/Object;
    .end local v5    # "parser":Lcom/ibm/icu/text/TransliteratorParser;
    :catch_2
    move-exception v7

    goto/16 :goto_2
.end method

.method private registerEntry(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 8
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "entry"    # Ljava/lang/Object;
    .param p3, "visible"    # Z

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 484
    invoke-static {p1}, Lcom/ibm/icu/text/TransliteratorIDParser;->IDtoSTV(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 486
    .local v7, "stv":[Ljava/lang/String;
    aget-object v0, v7, v4

    aget-object v2, v7, v5

    aget-object v3, v7, v6

    invoke-static {v0, v2, v3}, Lcom/ibm/icu/text/TransliteratorIDParser;->STVtoID(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 487
    .local v1, "id":Ljava/lang/String;
    aget-object v2, v7, v4

    aget-object v3, v7, v5

    aget-object v4, v7, v6

    move-object v0, p0

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/TransliteratorRegistry;->registerEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 488
    return-void
.end method

.method private registerEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 7
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "variant"    # Ljava/lang/String;
    .param p4, "entry"    # Ljava/lang/Object;
    .param p5, "visible"    # Z

    .prologue
    .line 470
    move-object v2, p1

    .line 471
    .local v2, "s":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 472
    const-string/jumbo v2, "Any"

    .line 474
    :cond_0
    invoke-static {p1, p2, p3}, Lcom/ibm/icu/text/TransliteratorIDParser;->STVtoID(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .local v1, "ID":Ljava/lang/String;
    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    .line 475
    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/TransliteratorRegistry;->registerEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 476
    return-void
.end method

.method private registerEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 3
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "source"    # Ljava/lang/String;
    .param p3, "target"    # Ljava/lang/String;
    .param p4, "variant"    # Ljava/lang/String;
    .param p5, "entry"    # Ljava/lang/Object;
    .param p6, "visible"    # Z

    .prologue
    .line 500
    new-instance v0, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v0, p1}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    .line 503
    .local v0, "ciID":Lcom/ibm/icu/util/CaseInsensitiveString;
    instance-of v2, p5, [Ljava/lang/Object;

    if-nez v2, :cond_0

    .line 504
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p5, v1, v2

    .local v1, "entry":[Ljava/lang/Object;
    move-object p5, v1

    .line 507
    .end local v1    # "entry":[Ljava/lang/Object;
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->registry:Ljava/util/Hashtable;

    invoke-virtual {v2, v0, p5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 508
    if-eqz p6, :cond_2

    .line 509
    invoke-direct {p0, p2, p3, p4}, Lcom/ibm/icu/text/TransliteratorRegistry;->registerSTV(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->availableIDs:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 511
    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->availableIDs:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 517
    :cond_1
    :goto_0
    return-void

    .line 514
    :cond_2
    invoke-direct {p0, p2, p3, p4}, Lcom/ibm/icu/text/TransliteratorRegistry;->removeSTV(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->availableIDs:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private registerSTV(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "variant"    # Ljava/lang/String;

    .prologue
    .line 530
    new-instance v0, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v0, p1}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    .line 531
    .local v0, "cisrc":Lcom/ibm/icu/util/CaseInsensitiveString;
    new-instance v1, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v1, p2}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    .line 532
    .local v1, "citrg":Lcom/ibm/icu/util/CaseInsensitiveString;
    new-instance v2, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v2, p3}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    .line 533
    .local v2, "civar":Lcom/ibm/icu/util/CaseInsensitiveString;
    iget-object v5, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->specDAG:Ljava/util/Hashtable;

    invoke-virtual {v5, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Hashtable;

    .line 534
    .local v3, "targets":Ljava/util/Hashtable;
    if-nez v3, :cond_0

    .line 535
    new-instance v3, Ljava/util/Hashtable;

    .end local v3    # "targets":Ljava/util/Hashtable;
    invoke-direct {v3}, Ljava/util/Hashtable;-><init>()V

    .line 536
    .restart local v3    # "targets":Ljava/util/Hashtable;
    iget-object v5, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->specDAG:Ljava/util/Hashtable;

    invoke-virtual {v5, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    :cond_0
    invoke-virtual {v3, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Vector;

    .line 539
    .local v4, "variants":Ljava/util/Vector;
    if-nez v4, :cond_1

    .line 540
    new-instance v4, Ljava/util/Vector;

    .end local v4    # "variants":Ljava/util/Vector;
    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 541
    .restart local v4    # "variants":Ljava/util/Vector;
    invoke-virtual {v3, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    :cond_1
    invoke-virtual {v4, v2}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 547
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_3

    .line 548
    invoke-virtual {v4, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 553
    :cond_2
    :goto_0
    return-void

    .line 550
    :cond_3
    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method private removeSTV(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "variant"    # Ljava/lang/String;

    .prologue
    .line 563
    new-instance v0, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v0, p1}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    .line 564
    .local v0, "cisrc":Lcom/ibm/icu/util/CaseInsensitiveString;
    new-instance v1, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v1, p2}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    .line 565
    .local v1, "citrg":Lcom/ibm/icu/util/CaseInsensitiveString;
    new-instance v2, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v2, p3}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    .line 566
    .local v2, "civar":Lcom/ibm/icu/util/CaseInsensitiveString;
    iget-object v5, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->specDAG:Ljava/util/Hashtable;

    invoke-virtual {v5, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Hashtable;

    .line 567
    .local v3, "targets":Ljava/util/Hashtable;
    if-nez v3, :cond_1

    .line 581
    :cond_0
    :goto_0
    return-void

    .line 570
    :cond_1
    invoke-virtual {v3, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Vector;

    .line 571
    .local v4, "variants":Ljava/util/Vector;
    if-eqz v4, :cond_0

    .line 574
    invoke-virtual {v4, v2}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 575
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v5

    if-nez v5, :cond_0

    .line 576
    invoke-virtual {v3, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 577
    invoke-virtual {v3}, Ljava/util/Hashtable;->size()I

    move-result v5

    if-nez v5, :cond_0

    .line 578
    iget-object v5, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->specDAG:Ljava/util/Hashtable;

    invoke-virtual {v5, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public get(Ljava/lang/String;Ljava/lang/StringBuffer;)Lcom/ibm/icu/text/Transliterator;
    .locals 2
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "aliasReturn"    # Ljava/lang/StringBuffer;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/TransliteratorRegistry;->find(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    .line 303
    .local v0, "entry":[Ljava/lang/Object;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, p1, v0, p2}, Lcom/ibm/icu/text/TransliteratorRegistry;->instantiateEntry(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/StringBuffer;)Lcom/ibm/icu/text/Transliterator;

    move-result-object v1

    goto :goto_0
.end method

.method public getAvailableIDs()Ljava/util/Enumeration;
    .locals 2

    .prologue
    .line 411
    new-instance v0, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;

    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->availableIDs:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;-><init>(Ljava/util/Enumeration;)V

    return-object v0
.end method

.method public getAvailableSources()Ljava/util/Enumeration;
    .locals 2

    .prologue
    .line 420
    new-instance v0, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;

    iget-object v1, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->specDAG:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;-><init>(Ljava/util/Enumeration;)V

    return-object v0
.end method

.method public getAvailableTargets(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 4
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 430
    new-instance v0, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v0, p1}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    .line 431
    .local v0, "cisrc":Lcom/ibm/icu/util/CaseInsensitiveString;
    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->specDAG:Ljava/util/Hashtable;

    invoke-virtual {v2, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Hashtable;

    .line 432
    .local v1, "targets":Ljava/util/Hashtable;
    if-nez v1, :cond_0

    .line 433
    new-instance v2, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;-><init>(Ljava/util/Enumeration;)V

    .line 435
    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;

    invoke-virtual {v1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;-><init>(Ljava/util/Enumeration;)V

    goto :goto_0
.end method

.method public getAvailableVariants(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 6
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 445
    new-instance v0, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v0, p1}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    .line 446
    .local v0, "cisrc":Lcom/ibm/icu/util/CaseInsensitiveString;
    new-instance v1, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v1, p2}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    .line 447
    .local v1, "citrg":Lcom/ibm/icu/util/CaseInsensitiveString;
    iget-object v4, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->specDAG:Ljava/util/Hashtable;

    invoke-virtual {v4, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Hashtable;

    .line 448
    .local v2, "targets":Ljava/util/Hashtable;
    if-nez v2, :cond_0

    .line 449
    new-instance v4, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;

    invoke-direct {v4, v5}, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;-><init>(Ljava/util/Enumeration;)V

    .line 455
    :goto_0
    return-object v4

    .line 451
    :cond_0
    invoke-virtual {v2, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Vector;

    .line 452
    .local v3, "variants":Ljava/util/Vector;
    if-nez v3, :cond_1

    .line 453
    new-instance v4, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;

    invoke-direct {v4, v5}, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;-><init>(Ljava/util/Enumeration;)V

    goto :goto_0

    .line 455
    :cond_1
    new-instance v4, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;

    invoke-virtual {v3}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;-><init>(Ljava/util/Enumeration;)V

    goto :goto_0
.end method

.method public put(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;Z)V
    .locals 0
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "factory"    # Lcom/ibm/icu/text/Transliterator$Factory;
    .param p3, "visible"    # Z

    .prologue
    .line 326
    invoke-direct {p0, p1, p2, p3}, Lcom/ibm/icu/text/TransliteratorRegistry;->registerEntry(Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 327
    return-void
.end method

.method public put(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator;Z)V
    .locals 0
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "trans"    # Lcom/ibm/icu/text/Transliterator;
    .param p3, "visible"    # Z

    .prologue
    .line 361
    invoke-direct {p0, p1, p2, p3}, Lcom/ibm/icu/text/TransliteratorRegistry;->registerEntry(Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 362
    return-void
.end method

.method public put(Ljava/lang/String;Ljava/lang/Class;Z)V
    .locals 0
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "transliteratorSubclass"    # Ljava/lang/Class;
    .param p3, "visible"    # Z

    .prologue
    .line 315
    invoke-direct {p0, p1, p2, p3}, Lcom/ibm/icu/text/TransliteratorRegistry;->registerEntry(Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 316
    return-void
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "resourceName"    # Ljava/lang/String;
    .param p3, "encoding"    # Ljava/lang/String;
    .param p4, "dir"    # I
    .param p5, "visible"    # Z

    .prologue
    .line 339
    new-instance v0, Lcom/ibm/icu/text/TransliteratorRegistry$ResourceEntry;

    invoke-direct {v0, p2, p3, p4}, Lcom/ibm/icu/text/TransliteratorRegistry$ResourceEntry;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {p0, p1, v0, p5}, Lcom/ibm/icu/text/TransliteratorRegistry;->registerEntry(Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 340
    return-void
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "alias"    # Ljava/lang/String;
    .param p3, "visible"    # Z

    .prologue
    .line 350
    new-instance v0, Lcom/ibm/icu/text/TransliteratorRegistry$AliasEntry;

    invoke-direct {v0, p2}, Lcom/ibm/icu/text/TransliteratorRegistry$AliasEntry;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, v0, p3}, Lcom/ibm/icu/text/TransliteratorRegistry;->registerEntry(Ljava/lang/String;Ljava/lang/Object;Z)V

    .line 351
    return-void
.end method

.method public remove(Ljava/lang/String;)V
    .locals 8
    .param p1, "ID"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 370
    invoke-static {p1}, Lcom/ibm/icu/text/TransliteratorIDParser;->IDtoSTV(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 372
    .local v1, "stv":[Ljava/lang/String;
    aget-object v2, v1, v5

    aget-object v3, v1, v6

    aget-object v4, v1, v7

    invoke-static {v2, v3, v4}, Lcom/ibm/icu/text/TransliteratorIDParser;->STVtoID(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 373
    .local v0, "id":Ljava/lang/String;
    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->registry:Ljava/util/Hashtable;

    new-instance v3, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v3, v0}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    aget-object v2, v1, v5

    aget-object v3, v1, v6

    aget-object v4, v1, v7

    invoke-direct {p0, v2, v3, v4}, Lcom/ibm/icu/text/TransliteratorRegistry;->removeSTV(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    iget-object v2, p0, Lcom/ibm/icu/text/TransliteratorRegistry;->availableIDs:Ljava/util/Vector;

    new-instance v3, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-direct {v3, v0}, Lcom/ibm/icu/util/CaseInsensitiveString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 376
    return-void
.end method
