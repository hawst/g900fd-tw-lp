.class public Lcom/ibm/icu/text/DictionaryBasedBreakIterator;
.super Lcom/ibm/icu/text/RuleBasedBreakIterator;
.source "DictionaryBasedBreakIterator.java"


# instance fields
.field cachedBreakPositions:[I

.field private dictionary:Lcom/ibm/icu/text/BreakDictionary;

.field positionInCache:I

.field private usingCTDictionary:Z


# direct methods
.method protected constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "compiledRules"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->usingCTDictionary:Z

    .line 89
    invoke-static {p1}, Lcom/ibm/icu/text/RBBIDataWrapper;->get(Ljava/io/InputStream;)Lcom/ibm/icu/text/RBBIDataWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->dictionary:Lcom/ibm/icu/text/BreakDictionary;

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->usingCTDictionary:Z

    .line 92
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/io/InputStream;)V
    .locals 1
    .param p1, "compiledRules"    # Ljava/io/InputStream;
    .param p2, "dictionaryStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->usingCTDictionary:Z

    .line 117
    invoke-static {p1}, Lcom/ibm/icu/text/RBBIDataWrapper;->get(Ljava/io/InputStream;)Lcom/ibm/icu/text/RBBIDataWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    .line 118
    new-instance v0, Lcom/ibm/icu/text/BreakDictionary;

    invoke-direct {v0, p2}, Lcom/ibm/icu/text/BreakDictionary;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->dictionary:Lcom/ibm/icu/text/BreakDictionary;

    .line 119
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/InputStream;)V
    .locals 1
    .param p1, "rules"    # Ljava/lang/String;
    .param p2, "dictionaryStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/RuleBasedBreakIterator;-><init>(Ljava/lang/String;)V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->usingCTDictionary:Z

    .line 104
    new-instance v0, Lcom/ibm/icu/text/BreakDictionary;

    invoke-direct {v0, p2}, Lcom/ibm/icu/text/BreakDictionary;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->dictionary:Lcom/ibm/icu/text/BreakDictionary;

    .line 105
    return-void
.end method

.method private divideUpDictionaryRange(II)V
    .locals 15
    .param p1, "startPos"    # I
    .param p2, "endPos"    # I

    .prologue
    .line 375
    invoke-virtual {p0}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->getText()Ljava/text/CharacterIterator;

    move-result-object v10

    .line 381
    .local v10, "text":Ljava/text/CharacterIterator;
    move/from16 v0, p1

    invoke-interface {v10, v0}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 382
    invoke-static {v10}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->CICurrent32(Ljava/text/CharacterIterator;)I

    move-result v2

    .line 383
    .local v2, "c":I
    :goto_0
    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->isDictionaryChar(I)Z

    move-result v12

    if-nez v12, :cond_0

    .line 384
    invoke-static {v10}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->CINext32(Ljava/text/CharacterIterator;)I

    move-result v2

    .line 385
    goto :goto_0

    .line 400
    :cond_0
    new-instance v3, Ljava/util/Stack;

    invoke-direct {v3}, Ljava/util/Stack;-><init>()V

    .line 401
    .local v3, "currentBreakPositions":Ljava/util/Stack;
    new-instance v6, Ljava/util/Stack;

    invoke-direct {v6}, Ljava/util/Stack;-><init>()V

    .line 402
    .local v6, "possibleBreakPositions":Ljava/util/Stack;
    new-instance v11, Ljava/util/Vector;

    invoke-direct {v11}, Ljava/util/Vector;-><init>()V

    .line 408
    .local v11, "wrongBreakPositions":Ljava/util/Vector;
    const/4 v7, 0x0

    .line 417
    .local v7, "state":I
    invoke-interface {v10}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v4

    .line 418
    .local v4, "farthestEndPoint":I
    const/4 v1, 0x0

    .line 421
    .local v1, "bestBreakPositions":Ljava/util/Stack;
    invoke-static {v10}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->CICurrent32(Ljava/text/CharacterIterator;)I

    move-result v2

    .line 428
    :cond_1
    :goto_1
    iget-object v12, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->dictionary:Lcom/ibm/icu/text/BreakDictionary;

    const/4 v13, 0x0

    invoke-virtual {v12, v7, v13}, Lcom/ibm/icu/text/BreakDictionary;->at(II)S

    move-result v12

    const/4 v13, -0x1

    if-ne v12, v13, :cond_2

    .line 429
    new-instance v12, Ljava/lang/Integer;

    invoke-interface {v10}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v13

    invoke-direct {v12, v13}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v6, v12}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    :cond_2
    iget-object v12, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->dictionary:Lcom/ibm/icu/text/BreakDictionary;

    int-to-char v13, v2

    invoke-virtual {v12, v7, v13}, Lcom/ibm/icu/text/BreakDictionary;->at(IC)S

    move-result v12

    const v13, 0xffff

    and-int v7, v12, v13

    .line 443
    const v12, 0xffff

    if-ne v7, v12, :cond_5

    .line 444
    new-instance v12, Ljava/lang/Integer;

    invoke-interface {v10}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v13

    invoke-direct {v12, v13}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v12}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 542
    :cond_3
    :goto_2
    invoke-virtual {v3}, Ljava/util/Stack;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_4

    .line 543
    invoke-virtual {v3}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 545
    :cond_4
    new-instance v12, Ljava/lang/Integer;

    move/from16 v0, p2

    invoke-direct {v12, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v12}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    invoke-virtual {v3}, Ljava/util/Stack;->size()I

    move-result v12

    add-int/lit8 v12, v12, 0x1

    new-array v12, v12, [I

    iput-object v12, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    .line 553
    iget-object v12, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    const/4 v13, 0x0

    aput p1, v12, v13

    .line 555
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3
    invoke-virtual {v3}, Ljava/util/Stack;->size()I

    move-result v12

    if-ge v5, v12, :cond_f

    .line 556
    iget-object v13, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    add-int/lit8 v14, v5, 0x1

    invoke-virtual {v3, v5}, Ljava/util/Stack;->elementAt(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    aput v12, v13, v14

    .line 555
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 452
    .end local v5    # "i":I
    :cond_5
    if-eqz v7, :cond_6

    invoke-interface {v10}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v12

    move/from16 v0, p2

    if-lt v12, v0, :cond_e

    .line 456
    :cond_6
    invoke-interface {v10}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v12

    if-le v12, v4, :cond_7

    .line 457
    invoke-interface {v10}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v4

    .line 458
    invoke-virtual {v3}, Ljava/util/Stack;->clone()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Stack;

    move-object v1, v12

    check-cast v1, Ljava/util/Stack;

    .line 472
    :cond_7
    :goto_4
    invoke-virtual {v6}, Ljava/util/Stack;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_8

    invoke-virtual {v6}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 474
    invoke-virtual {v6}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_4

    .line 482
    :cond_8
    invoke-virtual {v6}, Ljava/util/Stack;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_c

    .line 483
    if-eqz v1, :cond_9

    .line 484
    move-object v3, v1

    .line 485
    move/from16 v0, p2

    if-ge v4, v0, :cond_3

    .line 486
    add-int/lit8 v12, v4, 0x1

    invoke-interface {v10, v12}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 522
    :goto_5
    invoke-static {v10}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->CICurrent32(Ljava/text/CharacterIterator;)I

    move-result v2

    .line 523
    const/4 v7, 0x0

    .line 524
    invoke-interface {v10}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v12

    move/from16 v0, p2

    if-lt v12, v0, :cond_1

    goto/16 :goto_2

    .line 493
    :cond_9
    invoke-virtual {v3}, Ljava/util/Stack;->size()I

    move-result v12

    if-eqz v12, :cond_a

    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-interface {v10}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v13

    if-eq v12, v13, :cond_b

    :cond_a
    invoke-interface {v10}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v12

    move/from16 v0, p1

    if-eq v12, v0, :cond_b

    .line 496
    new-instance v12, Ljava/lang/Integer;

    invoke-interface {v10}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v13

    invoke-direct {v12, v13}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v12}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    :cond_b
    invoke-static {v10}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->CINext32(Ljava/text/CharacterIterator;)I

    .line 499
    new-instance v12, Ljava/lang/Integer;

    invoke-interface {v10}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v13

    invoke-direct {v12, v13}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v12}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 509
    :cond_c
    invoke-virtual {v6}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 510
    .local v8, "temp":Ljava/lang/Integer;
    const/4 v9, 0x0

    .line 511
    :goto_6
    invoke-virtual {v3}, Ljava/util/Stack;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_d

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v13

    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    if-ge v13, v12, :cond_d

    .line 513
    invoke-virtual {v3}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v9

    .line 514
    .local v9, "temp2":Ljava/lang/Object;
    invoke-virtual {v11, v9}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_6

    .line 516
    .end local v9    # "temp2":Ljava/lang/Object;
    :cond_d
    invoke-virtual {v3, v8}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-interface {v10, v12}, Ljava/text/CharacterIterator;->setIndex(I)C

    goto :goto_5

    .line 532
    .end local v8    # "temp":Ljava/lang/Integer;
    :cond_e
    invoke-static {v10}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->CINext32(Ljava/text/CharacterIterator;)I

    move-result v2

    .line 536
    goto/16 :goto_1

    .line 558
    .restart local v5    # "i":I
    :cond_f
    const/4 v12, 0x0

    iput v12, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    .line 559
    return-void
.end method


# virtual methods
.method public first()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    .line 138
    iput v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->fDictionaryCharCount:I

    .line 139
    iput v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    .line 140
    invoke-super {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->first()I

    move-result v0

    return v0
.end method

.method public following(I)I
    .locals 4
    .param p1, "offset"    # I

    .prologue
    const/4 v3, 0x0

    .line 253
    invoke-virtual {p0}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->getText()Ljava/text/CharacterIterator;

    move-result-object v0

    .line 254
    .local v0, "text":Ljava/text/CharacterIterator;
    invoke-static {p1, v0}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->checkOffset(ILjava/text/CharacterIterator;)V

    .line 260
    iget-object v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    aget v1, v1, v3

    if-lt p1, v1, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    iget-object v2, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    if-lt p1, v1, :cond_1

    .line 262
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    .line 263
    invoke-super {p0, p1}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->following(I)I

    move-result v1

    .line 275
    :goto_0
    return v1

    .line 270
    :cond_1
    iput v3, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    .line 272
    :goto_1
    iget v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    iget-object v2, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    array-length v2, v2

    if-ge v1, v2, :cond_2

    iget-object v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    iget v2, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    aget v1, v1, v2

    if-lt p1, v1, :cond_2

    .line 273
    iget v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    goto :goto_1

    .line 274
    :cond_2
    iget-object v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    iget v2, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    aget v1, v1, v2

    invoke-interface {v0, v1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 275
    invoke-interface {v0}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v1

    goto :goto_0
.end method

.method public getRuleStatus()I
    .locals 1

    .prologue
    .line 292
    const/4 v0, 0x0

    return v0
.end method

.method public getRuleStatusVec([I)I
    .locals 3
    .param p1, "fillInArray"    # [I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 314
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lt v0, v2, :cond_0

    .line 315
    aput v1, p1, v1

    .line 317
    :cond_0
    return v2
.end method

.method protected handleNext()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 325
    invoke-virtual {p0}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->getText()Ljava/text/CharacterIterator;

    move-result-object v2

    .line 330
    .local v2, "text":Ljava/text/CharacterIterator;
    iget-object v3, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    iget-object v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_1

    .line 335
    :cond_0
    invoke-interface {v2}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v1

    .line 336
    .local v1, "startPos":I
    iput v5, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->fDictionaryCharCount:I

    .line 337
    invoke-super {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->handleNext()I

    move-result v0

    .line 342
    .local v0, "result":I
    iget-boolean v3, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->usingCTDictionary:Z

    if-nez v3, :cond_2

    iget v3, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->fDictionaryCharCount:I

    if-le v3, v6, :cond_2

    sub-int v3, v0, v1

    if-le v3, v6, :cond_2

    .line 343
    invoke-direct {p0, v1, v0}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->divideUpDictionaryRange(II)V

    .line 357
    .end local v0    # "result":I
    .end local v1    # "startPos":I
    :cond_1
    iget-object v3, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    if-eqz v3, :cond_3

    .line 358
    iget v3, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    .line 359
    iget-object v3, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    iget v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    aget v3, v3, v4

    invoke-interface {v2, v3}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 360
    iget-object v3, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    iget v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    aget v0, v3, v4

    .line 363
    :goto_0
    return v0

    .line 349
    .restart local v0    # "result":I
    .restart local v1    # "startPos":I
    :cond_2
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    goto :goto_0

    .line 362
    .end local v0    # "result":I
    .end local v1    # "startPos":I
    :cond_3
    invoke-static {v5}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 363
    const/16 v0, -0x270f

    goto :goto_0
.end method

.method public last()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    .line 151
    iput v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->fDictionaryCharCount:I

    .line 152
    iput v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    .line 153
    invoke-super {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->last()I

    move-result v0

    return v0
.end method

.method public preceding(I)I
    .locals 4
    .param p1, "offset"    # I

    .prologue
    const/4 v3, 0x0

    .line 218
    invoke-virtual {p0}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->getText()Ljava/text/CharacterIterator;

    move-result-object v0

    .line 219
    .local v0, "text":Ljava/text/CharacterIterator;
    invoke-static {p1, v0}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->checkOffset(ILjava/text/CharacterIterator;)V

    .line 225
    iget-object v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    aget v1, v1, v3

    if-le p1, v1, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    iget-object v2, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    if-le p1, v1, :cond_1

    .line 227
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    .line 228
    invoke-super {p0, p1}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->preceding(I)I

    move-result v1

    .line 241
    :goto_0
    return v1

    .line 235
    :cond_1
    iput v3, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    .line 237
    :goto_1
    iget v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    iget-object v2, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    array-length v2, v2

    if-ge v1, v2, :cond_2

    iget-object v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    iget v2, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    aget v1, v1, v2

    if-le p1, v1, :cond_2

    .line 238
    iget v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    goto :goto_1

    .line 239
    :cond_2
    iget v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    .line 240
    iget-object v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    iget v2, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    aget v1, v1, v2

    invoke-interface {v0, v1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 241
    invoke-interface {v0}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v1

    goto :goto_0
.end method

.method public previous()I
    .locals 6

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->getText()Ljava/text/CharacterIterator;

    move-result-object v3

    .line 167
    .local v3, "text":Ljava/text/CharacterIterator;
    iget-object v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    if-lez v4, :cond_1

    .line 168
    iget v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    .line 169
    iget-object v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    iget v5, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    aget v4, v4, v5

    invoke-interface {v3, v4}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 170
    iget-object v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    iget v5, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    aget v2, v4, v5

    .line 206
    :cond_0
    :goto_0
    return v2

    .line 179
    :cond_1
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    .line 180
    invoke-virtual {p0}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->current()I

    move-result v1

    .line 181
    .local v1, "offset":I
    invoke-super {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->previous()I

    move-result v2

    .line 183
    .local v2, "result":I
    iget-object v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    if-eqz v4, :cond_3

    .line 184
    iget-object v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    goto :goto_0

    .line 195
    .local v0, "nextResult":I
    :cond_2
    move v2, v0

    .line 188
    .end local v0    # "nextResult":I
    :cond_3
    if-ge v2, v1, :cond_4

    .line 189
    invoke-virtual {p0}, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->next()I

    move-result v0

    .line 191
    .restart local v0    # "nextResult":I
    if-lt v0, v1, :cond_2

    .line 198
    .end local v0    # "nextResult":I
    :cond_4
    iget-object v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    if-eqz v4, :cond_5

    .line 199
    iget-object v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    .line 202
    :cond_5
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 203
    invoke-interface {v3, v2}, Ljava/text/CharacterIterator;->setIndex(I)C

    goto :goto_0
.end method

.method public setText(Ljava/text/CharacterIterator;)V
    .locals 2
    .param p1, "newText"    # Ljava/text/CharacterIterator;

    .prologue
    const/4 v1, 0x0

    .line 124
    invoke-super {p0, p1}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->setText(Ljava/text/CharacterIterator;)V

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->cachedBreakPositions:[I

    .line 126
    iput v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->fDictionaryCharCount:I

    .line 127
    iput v1, p0, Lcom/ibm/icu/text/DictionaryBasedBreakIterator;->positionInCache:I

    .line 128
    return-void
.end method
