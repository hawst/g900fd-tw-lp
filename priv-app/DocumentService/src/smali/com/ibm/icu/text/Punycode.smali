.class final Lcom/ibm/icu/text/Punycode;
.super Ljava/lang/Object;
.source "Punycode.java"


# static fields
.field private static final BASE:I = 0x24

.field private static final CAPITAL_A:I = 0x41

.field private static final CAPITAL_Z:I = 0x5a

.field private static final DAMP:I = 0x2bc

.field private static final DELIMITER:I = 0x2d

.field private static final HYPHEN:I = 0x2d

.field private static final INITIAL_BIAS:I = 0x48

.field private static final INITIAL_N:I = 0x80

.field private static final MAX_CP_COUNT:I = 0xc8

.field private static final SKEW:I = 0x26

.field private static final SMALL_A:I = 0x61

.field private static final SMALL_Z:I = 0x7a

.field private static final TMAX:I = 0x1a

.field private static final TMIN:I = 0x1

.field private static final ZERO:I = 0x30

.field static final basicToDigit:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const/16 v0, 0x100

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/text/Punycode;->basicToDigit:[I

    return-void

    :array_0
    .array-data 4
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x1a
        0x1b
        0x1c
        0x1d
        0x1e
        0x1f
        0x20
        0x21
        0x22
        0x23
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
        0x11
        0x12
        0x13
        0x14
        0x15
        0x16
        0x17
        0x18
        0x19
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
        0x11
        0x12
        0x13
        0x14
        0x15
        0x16
        0x17
        0x18
        0x19
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static adaptBias(IIZ)I
    .locals 3
    .param p0, "delta"    # I
    .param p1, "length"    # I
    .param p2, "firstTime"    # Z

    .prologue
    .line 45
    if-eqz p2, :cond_0

    .line 46
    div-int/lit16 p0, p0, 0x2bc

    .line 50
    :goto_0
    div-int v1, p0, p1

    add-int/2addr p0, v1

    .line 52
    const/4 v0, 0x0

    .line 53
    .local v0, "count":I
    :goto_1
    const/16 v1, 0x1c7

    if-le p0, v1, :cond_1

    .line 54
    div-int/lit8 p0, p0, 0x23

    .line 53
    add-int/lit8 v0, v0, 0x24

    goto :goto_1

    .line 48
    .end local v0    # "count":I
    :cond_0
    div-int/lit8 p0, p0, 0x2

    goto :goto_0

    .line 57
    .restart local v0    # "count":I
    :cond_1
    mul-int/lit8 v1, p0, 0x24

    add-int/lit8 v2, p0, 0x26

    div-int/2addr v1, v2

    add-int/2addr v1, v0

    return v1
.end method

.method private static asciiCaseMap(CZ)C
    .locals 1
    .param p0, "b"    # C
    .param p1, "uppercase"    # Z

    .prologue
    .line 93
    if-eqz p1, :cond_1

    .line 94
    const/16 v0, 0x61

    if-gt v0, p0, :cond_0

    const/16 v0, 0x7a

    if-gt p0, v0, :cond_0

    .line 95
    add-int/lit8 v0, p0, -0x20

    int-to-char p0, v0

    .line 102
    :cond_0
    :goto_0
    return p0

    .line 98
    :cond_1
    const/16 v0, 0x41

    if-gt v0, p0, :cond_0

    const/16 v0, 0x5a

    if-gt p0, v0, :cond_0

    .line 99
    add-int/lit8 v0, p0, 0x20

    int-to-char p0, v0

    goto :goto_0
.end method

.method public static decode(Ljava/lang/StringBuffer;[Z)Ljava/lang/StringBuffer;
    .locals 29
    .param p0, "src"    # Ljava/lang/StringBuffer;
    .param p1, "caseFlags"    # [Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 297
    invoke-virtual/range {p0 .. p0}, Ljava/lang/StringBuffer;->length()I

    move-result v23

    .line 298
    .local v23, "srcLength":I
    new-instance v22, Ljava/lang/StringBuffer;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuffer;-><init>()V

    .line 302
    .local v22, "result":Ljava/lang/StringBuffer;
    const/16 v11, 0xc8

    .line 303
    .local v11, "destCapacity":I
    new-array v9, v11, [C

    .line 313
    .local v9, "dest":[C
    move/from16 v18, v23

    .local v18, "j":I
    :cond_0
    if-lez v18, :cond_1

    .line 314
    add-int/lit8 v18, v18, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v26

    const/16 v27, 0x2d

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_0

    .line 318
    :cond_1
    move/from16 v10, v18

    .local v10, "destCPCount":I
    move/from16 v5, v18

    .local v5, "basicLength":I
    move/from16 v12, v18

    .line 320
    .local v12, "destLength":I
    :cond_2
    :goto_0
    if-lez v18, :cond_4

    .line 321
    add-int/lit8 v18, v18, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    .line 322
    .local v4, "b":C
    invoke-static {v4}, Lcom/ibm/icu/text/Punycode;->isBasic(I)Z

    move-result v26

    if-nez v26, :cond_3

    .line 323
    new-instance v26, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v27, "Illegal char found"

    const/16 v28, 0x0

    invoke-direct/range {v26 .. v28}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;I)V

    throw v26

    .line 326
    :cond_3
    move/from16 v0, v18

    if-ge v0, v11, :cond_2

    .line 327
    aput-char v4, v9, v18

    .line 329
    if-eqz p1, :cond_2

    .line 330
    invoke-static {v4}, Lcom/ibm/icu/text/Punycode;->isBasicUpperCase(I)Z

    move-result v26

    aput-boolean v26, p1, v18

    goto :goto_0

    .line 336
    .end local v4    # "b":C
    :cond_4
    const/16 v20, 0x80

    .line 337
    .local v20, "n":I
    const/4 v15, 0x0

    .line 338
    .local v15, "i":I
    const/16 v6, 0x48

    .line 339
    .local v6, "bias":I
    const v14, 0x3b9aca00

    .line 346
    .local v14, "firstSupplementaryIndex":I
    if-lez v5, :cond_5

    add-int/lit8 v16, v5, 0x1

    .local v16, "in":I
    :goto_1
    move/from16 v0, v16

    move/from16 v1, v23

    if-ge v0, v1, :cond_16

    .line 356
    move/from16 v21, v15

    .local v21, "oldi":I
    const/16 v25, 0x1

    .local v25, "w":I
    const/16 v19, 0x24

    .local v19, "k":I
    move/from16 v17, v16

    .line 357
    .end local v16    # "in":I
    .local v17, "in":I
    :goto_2
    move/from16 v0, v17

    move/from16 v1, v23

    if-lt v0, v1, :cond_6

    .line 358
    new-instance v26, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v27, "Illegal char found"

    const/16 v28, 0x1

    invoke-direct/range {v26 .. v28}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;I)V

    throw v26

    .line 346
    .end local v17    # "in":I
    .end local v19    # "k":I
    .end local v21    # "oldi":I
    .end local v25    # "w":I
    :cond_5
    const/16 v16, 0x0

    goto :goto_1

    .line 361
    .restart local v17    # "in":I
    .restart local v19    # "k":I
    .restart local v21    # "oldi":I
    .restart local v25    # "w":I
    :cond_6
    sget-object v26, Lcom/ibm/icu/text/Punycode;->basicToDigit:[I

    add-int/lit8 v16, v17, 0x1

    .end local v17    # "in":I
    .restart local v16    # "in":I
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v27

    move/from16 v0, v27

    and-int/lit16 v0, v0, 0xff

    move/from16 v27, v0

    aget v13, v26, v27

    .line 362
    .local v13, "digit":I
    if-gez v13, :cond_7

    .line 363
    new-instance v26, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v27, "Invalid char found"

    const/16 v28, 0x0

    invoke-direct/range {v26 .. v28}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;I)V

    throw v26

    .line 365
    :cond_7
    const v26, 0x7fffffff

    sub-int v26, v26, v15

    div-int v26, v26, v25

    move/from16 v0, v26

    if-le v13, v0, :cond_8

    .line 367
    new-instance v26, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v27, "Illegal char found"

    const/16 v28, 0x1

    invoke-direct/range {v26 .. v28}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;I)V

    throw v26

    .line 370
    :cond_8
    mul-int v26, v13, v25

    add-int v15, v15, v26

    .line 371
    sub-int v24, v19, v6

    .line 372
    .local v24, "t":I
    const/16 v26, 0x1

    move/from16 v0, v24

    move/from16 v1, v26

    if-ge v0, v1, :cond_a

    .line 373
    const/16 v24, 0x1

    .line 377
    :cond_9
    :goto_3
    move/from16 v0, v24

    if-ge v13, v0, :cond_b

    .line 393
    add-int/lit8 v10, v10, 0x1

    .line 394
    sub-int v27, v15, v21

    if-nez v21, :cond_d

    const/16 v26, 0x1

    :goto_4
    move/from16 v0, v27

    move/from16 v1, v26

    invoke-static {v0, v10, v1}, Lcom/ibm/icu/text/Punycode;->adaptBias(IIZ)I

    move-result v6

    .line 400
    div-int v26, v15, v10

    const v27, 0x7fffffff

    sub-int v27, v27, v20

    move/from16 v0, v26

    move/from16 v1, v27

    if-le v0, v1, :cond_e

    .line 402
    new-instance v26, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v27, "Illegal char found"

    const/16 v28, 0x1

    invoke-direct/range {v26 .. v28}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;I)V

    throw v26

    .line 374
    :cond_a
    add-int/lit8 v26, v6, 0x1a

    move/from16 v0, v19

    move/from16 v1, v26

    if-lt v0, v1, :cond_9

    .line 375
    const/16 v24, 0x1a

    goto :goto_3

    .line 381
    :cond_b
    const v26, 0x7fffffff

    rsub-int/lit8 v27, v24, 0x24

    div-int v26, v26, v27

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_c

    .line 383
    new-instance v26, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v27, "Illegal char found"

    const/16 v28, 0x1

    invoke-direct/range {v26 .. v28}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;I)V

    throw v26

    .line 385
    :cond_c
    rsub-int/lit8 v26, v24, 0x24

    mul-int v25, v25, v26

    .line 356
    add-int/lit8 v19, v19, 0x24

    move/from16 v17, v16

    .end local v16    # "in":I
    .restart local v17    # "in":I
    goto/16 :goto_2

    .line 394
    .end local v17    # "in":I
    .restart local v16    # "in":I
    :cond_d
    const/16 v26, 0x0

    goto :goto_4

    .line 405
    :cond_e
    div-int v26, v15, v10

    add-int v20, v20, v26

    .line 406
    rem-int/2addr v15, v10

    .line 410
    const v26, 0x10ffff

    move/from16 v0, v20

    move/from16 v1, v26

    if-gt v0, v1, :cond_f

    invoke-static/range {v20 .. v20}, Lcom/ibm/icu/text/Punycode;->isSurrogate(I)Z

    move-result v26

    if-eqz v26, :cond_10

    .line 412
    :cond_f
    new-instance v26, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v27, "Illegal char found"

    const/16 v28, 0x1

    invoke-direct/range {v26 .. v28}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;I)V

    throw v26

    .line 416
    :cond_10
    invoke-static/range {v20 .. v20}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v8

    .line 417
    .local v8, "cpLength":I
    add-int v26, v12, v8

    move/from16 v0, v26

    if-ge v0, v11, :cond_12

    .line 430
    if-gt v15, v14, :cond_14

    .line 431
    move v7, v15

    .line 432
    .local v7, "codeUnitIndex":I
    const/16 v26, 0x1

    move/from16 v0, v26

    if-le v8, v0, :cond_13

    .line 433
    move v14, v7

    .line 443
    :goto_5
    if-ge v7, v12, :cond_11

    .line 444
    add-int v26, v7, v8

    sub-int v27, v12, v7

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-static {v9, v7, v9, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 447
    if-eqz p1, :cond_11

    .line 448
    add-int v26, v7, v8

    sub-int v27, v12, v7

    move-object/from16 v0, p1

    move-object/from16 v1, p1

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-static {v0, v7, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 453
    :cond_11
    const/16 v26, 0x1

    move/from16 v0, v26

    if-ne v8, v0, :cond_15

    .line 455
    move/from16 v0, v20

    int-to-char v0, v0

    move/from16 v26, v0

    aput-char v26, v9, v7

    .line 461
    :goto_6
    if-eqz p1, :cond_12

    .line 463
    add-int/lit8 v26, v16, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v26

    invoke-static/range {v26 .. v26}, Lcom/ibm/icu/text/Punycode;->isBasicUpperCase(I)Z

    move-result v26

    aput-boolean v26, p1, v7

    .line 464
    const/16 v26, 0x2

    move/from16 v0, v26

    if-ne v8, v0, :cond_12

    .line 465
    add-int/lit8 v26, v7, 0x1

    const/16 v27, 0x0

    aput-boolean v27, p1, v26

    .line 469
    .end local v7    # "codeUnitIndex":I
    :cond_12
    add-int/2addr v12, v8

    .line 470
    add-int/lit8 v15, v15, 0x1

    .line 471
    goto/16 :goto_1

    .line 435
    .restart local v7    # "codeUnitIndex":I
    :cond_13
    add-int/lit8 v14, v14, 0x1

    .line 437
    goto :goto_5

    .line 438
    .end local v7    # "codeUnitIndex":I
    :cond_14
    move v7, v14

    .line 439
    .restart local v7    # "codeUnitIndex":I
    const/16 v26, 0x0

    sub-int v27, v15, v7

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-static {v9, v0, v12, v7, v1}, Lcom/ibm/icu/text/UTF16;->moveCodePointOffset([CIIII)I

    move-result v7

    goto :goto_5

    .line 458
    :cond_15
    invoke-static/range {v20 .. v20}, Lcom/ibm/icu/text/UTF16;->getLeadSurrogate(I)C

    move-result v26

    aput-char v26, v9, v7

    .line 459
    add-int/lit8 v26, v7, 0x1

    invoke-static/range {v20 .. v20}, Lcom/ibm/icu/text/UTF16;->getTrailSurrogate(I)C

    move-result v27

    aput-char v27, v9, v26

    goto :goto_6

    .line 472
    .end local v7    # "codeUnitIndex":I
    .end local v8    # "cpLength":I
    .end local v13    # "digit":I
    .end local v19    # "k":I
    .end local v21    # "oldi":I
    .end local v24    # "t":I
    .end local v25    # "w":I
    :cond_16
    const/16 v26, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-virtual {v0, v9, v1, v12}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 473
    return-object v22
.end method

.method private static digitToBasic(IZ)C
    .locals 1
    .param p0, "digit"    # I
    .param p1, "uppercase"    # Z

    .prologue
    .line 114
    const/16 v0, 0x1a

    if-ge p0, v0, :cond_1

    .line 115
    if-eqz p1, :cond_0

    .line 116
    add-int/lit8 v0, p0, 0x41

    int-to-char v0, v0

    .line 121
    :goto_0
    return v0

    .line 118
    :cond_0
    add-int/lit8 v0, p0, 0x61

    int-to-char v0, v0

    goto :goto_0

    .line 121
    :cond_1
    add-int/lit8 v0, p0, 0x16

    int-to-char v0, v0

    goto :goto_0
.end method

.method public static encode(Ljava/lang/StringBuffer;[Z)Ljava/lang/StringBuffer;
    .locals 26
    .param p0, "src"    # Ljava/lang/StringBuffer;
    .param p1, "caseFlags"    # [Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 136
    const/16 v23, 0xc8

    move/from16 v0, v23

    new-array v6, v0, [I

    .line 139
    .local v6, "cpBuffer":[I
    invoke-virtual/range {p0 .. p0}, Ljava/lang/StringBuffer;->length()I

    move-result v21

    .line 140
    .local v21, "srcLength":I
    const/16 v9, 0xc8

    .line 141
    .local v9, "destCapacity":I
    new-array v8, v9, [C

    .line 142
    .local v8, "dest":[C
    new-instance v18, Ljava/lang/StringBuffer;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuffer;-><init>()V

    .line 147
    .local v18, "result":Ljava/lang/StringBuffer;
    const/4 v10, 0x0

    .local v10, "destLength":I
    move/from16 v19, v10

    .line 149
    .local v19, "srcCPCount":I
    const/4 v13, 0x0

    .local v13, "j":I
    move/from16 v20, v19

    .end local v19    # "srcCPCount":I
    .local v20, "srcCPCount":I
    :goto_0
    move/from16 v0, v21

    if-ge v13, v0, :cond_6

    .line 150
    const/16 v23, 0xc8

    move/from16 v0, v20

    move/from16 v1, v23

    if-ne v0, v1, :cond_0

    .line 152
    new-instance v23, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct/range {v23 .. v23}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v23

    .line 154
    :cond_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    .line 155
    .local v4, "c":C
    invoke-static {v4}, Lcom/ibm/icu/text/Punycode;->isBasic(I)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 156
    if-ge v10, v9, :cond_17

    .line 157
    add-int/lit8 v19, v20, 0x1

    .end local v20    # "srcCPCount":I
    .restart local v19    # "srcCPCount":I
    const/16 v23, 0x0

    aput v23, v6, v20

    .line 158
    if-eqz p1, :cond_1

    aget-boolean v23, p1, v13

    move/from16 v0, v23

    invoke-static {v4, v0}, Lcom/ibm/icu/text/Punycode;->asciiCaseMap(CZ)C

    move-result v4

    .end local v4    # "c":C
    :cond_1
    aput-char v4, v8, v10

    .line 163
    :goto_1
    add-int/lit8 v10, v10, 0x1

    .line 149
    :goto_2
    add-int/lit8 v13, v13, 0x1

    move/from16 v20, v19

    .end local v19    # "srcCPCount":I
    .restart local v20    # "srcCPCount":I
    goto :goto_0

    .line 165
    .restart local v4    # "c":C
    :cond_2
    if-eqz p1, :cond_3

    aget-boolean v23, p1, v13

    if-eqz v23, :cond_3

    const/16 v23, 0x1

    :goto_3
    shl-int/lit8 v16, v23, 0x1f

    .line 166
    .local v16, "n":I
    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v23

    if-nez v23, :cond_4

    .line 167
    or-int v16, v16, v4

    .line 176
    :goto_4
    add-int/lit8 v19, v20, 0x1

    .end local v20    # "srcCPCount":I
    .restart local v19    # "srcCPCount":I
    aput v16, v6, v20

    goto :goto_2

    .line 165
    .end local v16    # "n":I
    .end local v19    # "srcCPCount":I
    .restart local v20    # "srcCPCount":I
    :cond_3
    const/16 v23, 0x0

    goto :goto_3

    .line 168
    .restart local v16    # "n":I
    :cond_4
    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v23

    if-eqz v23, :cond_5

    add-int/lit8 v23, v13, 0x1

    move/from16 v0, v23

    move/from16 v1, v21

    if-ge v0, v1, :cond_5

    add-int/lit8 v23, v13, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v5

    .local v5, "c2":C
    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 169
    add-int/lit8 v13, v13, 0x1

    .line 171
    invoke-static {v4, v5}, Lcom/ibm/icu/lang/UCharacter;->getCodePoint(CC)I

    move-result v23

    or-int v16, v16, v23

    .line 172
    goto :goto_4

    .line 174
    .end local v5    # "c2":C
    :cond_5
    new-instance v23, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v24, "Illegal char found"

    const/16 v25, 0x1

    invoke-direct/range {v23 .. v25}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;I)V

    throw v23

    .line 181
    .end local v4    # "c":C
    .end local v16    # "n":I
    :cond_6
    move v2, v10

    .line 182
    .local v2, "basicLength":I
    if-lez v2, :cond_8

    .line 183
    if-ge v10, v9, :cond_7

    .line 184
    const/16 v23, 0x2d

    aput-char v23, v8, v10

    .line 186
    :cond_7
    add-int/lit8 v10, v10, 0x1

    .line 196
    :cond_8
    const/16 v16, 0x80

    .line 197
    .restart local v16    # "n":I
    const/4 v7, 0x0

    .line 198
    .local v7, "delta":I
    const/16 v3, 0x48

    .line 201
    .local v3, "bias":I
    move v12, v2

    .local v12, "handledCPCount":I
    :goto_5
    move/from16 v0, v20

    if-ge v12, v0, :cond_14

    .line 206
    const v15, 0x7fffffff

    .local v15, "m":I
    const/4 v13, 0x0

    :goto_6
    move/from16 v0, v20

    if-ge v13, v0, :cond_a

    .line 207
    aget v23, v6, v13

    const v24, 0x7fffffff

    and-int v17, v23, v24

    .line 208
    .local v17, "q":I
    move/from16 v0, v16

    move/from16 v1, v17

    if-gt v0, v1, :cond_9

    move/from16 v0, v17

    if-ge v0, v15, :cond_9

    .line 209
    move/from16 v15, v17

    .line 206
    :cond_9
    add-int/lit8 v13, v13, 0x1

    goto :goto_6

    .line 217
    .end local v17    # "q":I
    :cond_a
    sub-int v23, v15, v16

    const v24, 0x7fffff37

    sub-int v24, v24, v7

    add-int/lit8 v25, v12, 0x1

    div-int v24, v24, v25

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_b

    .line 218
    new-instance v23, Ljava/lang/IllegalStateException;

    const-string/jumbo v24, "Internal program error"

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 220
    :cond_b
    sub-int v23, v15, v16

    add-int/lit8 v24, v12, 0x1

    mul-int v23, v23, v24

    add-int v7, v7, v23

    .line 221
    move/from16 v16, v15

    .line 224
    const/4 v13, 0x0

    :goto_7
    move/from16 v0, v20

    if-ge v13, v0, :cond_13

    .line 225
    aget v23, v6, v13

    const v24, 0x7fffffff

    and-int v17, v23, v24

    .line 226
    .restart local v17    # "q":I
    move/from16 v0, v17

    move/from16 v1, v16

    if-ge v0, v1, :cond_d

    .line 227
    add-int/lit8 v7, v7, 0x1

    .line 224
    :cond_c
    :goto_8
    add-int/lit8 v13, v13, 0x1

    goto :goto_7

    .line 228
    :cond_d
    move/from16 v0, v17

    move/from16 v1, v16

    if-ne v0, v1, :cond_c

    .line 230
    move/from16 v17, v7

    const/16 v14, 0x24

    .local v14, "k":I
    move v11, v10

    .line 242
    .end local v10    # "destLength":I
    .local v11, "destLength":I
    :goto_9
    sub-int v22, v14, v3

    .line 243
    .local v22, "t":I
    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_f

    .line 244
    const/16 v22, 0x1

    .line 249
    :cond_e
    :goto_a
    move/from16 v0, v17

    move/from16 v1, v22

    if-ge v0, v1, :cond_10

    .line 259
    if-ge v11, v9, :cond_15

    .line 260
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "destLength":I
    .restart local v10    # "destLength":I
    aget v23, v6, v13

    if-gez v23, :cond_11

    const/16 v23, 0x1

    :goto_b
    move/from16 v0, v17

    move/from16 v1, v23

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Punycode;->digitToBasic(IZ)C

    move-result v23

    aput-char v23, v8, v11

    .line 262
    :goto_c
    add-int/lit8 v24, v12, 0x1

    if-ne v12, v2, :cond_12

    const/16 v23, 0x1

    :goto_d
    move/from16 v0, v24

    move/from16 v1, v23

    invoke-static {v7, v0, v1}, Lcom/ibm/icu/text/Punycode;->adaptBias(IIZ)I

    move-result v3

    .line 263
    const/4 v7, 0x0

    .line 264
    add-int/lit8 v12, v12, 0x1

    goto :goto_8

    .line 245
    .end local v10    # "destLength":I
    .restart local v11    # "destLength":I
    :cond_f
    add-int/lit8 v23, v3, 0x1a

    move/from16 v0, v23

    if-lt v14, v0, :cond_e

    .line 246
    const/16 v22, 0x1a

    goto :goto_a

    .line 253
    :cond_10
    if-ge v11, v9, :cond_16

    .line 254
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "destLength":I
    .restart local v10    # "destLength":I
    sub-int v23, v17, v22

    rsub-int/lit8 v24, v22, 0x24

    rem-int v23, v23, v24

    add-int v23, v23, v22

    const/16 v24, 0x0

    invoke-static/range {v23 .. v24}, Lcom/ibm/icu/text/Punycode;->digitToBasic(IZ)C

    move-result v23

    aput-char v23, v8, v11

    .line 256
    :goto_e
    sub-int v23, v17, v22

    rsub-int/lit8 v24, v22, 0x24

    div-int v17, v23, v24

    .line 230
    add-int/lit8 v14, v14, 0x24

    move v11, v10

    .end local v10    # "destLength":I
    .restart local v11    # "destLength":I
    goto :goto_9

    .line 260
    .end local v11    # "destLength":I
    .restart local v10    # "destLength":I
    :cond_11
    const/16 v23, 0x0

    goto :goto_b

    .line 262
    :cond_12
    const/16 v23, 0x0

    goto :goto_d

    .line 268
    .end local v14    # "k":I
    .end local v17    # "q":I
    .end local v22    # "t":I
    :cond_13
    add-int/lit8 v7, v7, 0x1

    .line 269
    add-int/lit8 v16, v16, 0x1

    .line 270
    goto/16 :goto_5

    .line 272
    .end local v15    # "m":I
    :cond_14
    const/16 v23, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v8, v1, v10}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    move-result-object v23

    return-object v23

    .end local v10    # "destLength":I
    .restart local v11    # "destLength":I
    .restart local v14    # "k":I
    .restart local v15    # "m":I
    .restart local v17    # "q":I
    .restart local v22    # "t":I
    :cond_15
    move v10, v11

    .end local v11    # "destLength":I
    .restart local v10    # "destLength":I
    goto :goto_c

    .end local v10    # "destLength":I
    .restart local v11    # "destLength":I
    :cond_16
    move v10, v11

    .end local v11    # "destLength":I
    .restart local v10    # "destLength":I
    goto :goto_e

    .end local v2    # "basicLength":I
    .end local v3    # "bias":I
    .end local v7    # "delta":I
    .end local v12    # "handledCPCount":I
    .end local v14    # "k":I
    .end local v15    # "m":I
    .end local v16    # "n":I
    .end local v17    # "q":I
    .end local v22    # "t":I
    .restart local v4    # "c":C
    :cond_17
    move/from16 v19, v20

    .end local v20    # "srcCPCount":I
    .restart local v19    # "srcCPCount":I
    goto/16 :goto_1
.end method

.method private static isBasic(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 276
    const/16 v0, 0x80

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isBasicUpperCase(I)Z
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 280
    const/16 v0, 0x41

    if-gt v0, p0, :cond_0

    const/16 v0, 0x5a

    if-lt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isSurrogate(I)Z
    .locals 2
    .param p0, "ch"    # I

    .prologue
    .line 284
    and-int/lit16 v0, p0, -0x800

    const v1, 0xd800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
