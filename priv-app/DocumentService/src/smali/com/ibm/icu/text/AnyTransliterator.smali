.class Lcom/ibm/icu/text/AnyTransliterator;
.super Lcom/ibm/icu/text/Transliterator;
.source "AnyTransliterator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;
    }
.end annotation


# static fields
.field static final ANY:Ljava/lang/String; = "Any"

.field static final LATIN_PIVOT:Ljava/lang/String; = "-Latin;Latin-"

.field static final NULL_ID:Ljava/lang/String; = "Null"

.field static final TARGET_SEP:C = '-'

.field static final VARIANT_SEP:C = '/'


# instance fields
.field private cache:Ljava/util/Map;

.field private target:Ljava/lang/String;

.field private targetScript:I


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "theTarget"    # Ljava/lang/String;
    .param p3, "theVariant"    # Ljava/lang/String;
    .param p4, "theTargetScript"    # I

    .prologue
    .line 127
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/Transliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 128
    iput p4, p0, Lcom/ibm/icu/text/AnyTransliterator;->targetScript:I

    .line 129
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/AnyTransliterator;->cache:Ljava/util/Map;

    .line 131
    iput-object p2, p0, Lcom/ibm/icu/text/AnyTransliterator;->target:Ljava/lang/String;

    .line 132
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 133
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/ibm/icu/text/AnyTransliterator;->target:Ljava/lang/String;

    .line 135
    :cond_0
    return-void
.end method

.method private getTransliterator(I)Lcom/ibm/icu/text/Transliterator;
    .locals 6
    .param p1, "source"    # I

    .prologue
    .line 146
    iget v4, p0, Lcom/ibm/icu/text/AnyTransliterator;->targetScript:I

    if-eq p1, v4, :cond_0

    const/4 v4, -0x1

    if-ne p1, v4, :cond_2

    .line 147
    :cond_0
    const/4 v3, 0x0

    .line 173
    :cond_1
    :goto_0
    return-object v3

    .line 150
    :cond_2
    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    .line 151
    .local v1, "key":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/ibm/icu/text/AnyTransliterator;->cache:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ibm/icu/text/Transliterator;

    .line 152
    .local v3, "t":Lcom/ibm/icu/text/Transliterator;
    if-nez v3, :cond_1

    .line 153
    invoke-static {p1}, Lcom/ibm/icu/lang/UScript;->getName(I)Ljava/lang/String;

    move-result-object v2

    .line 154
    .local v2, "sourceName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const/16 v5, 0x2d

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lcom/ibm/icu/text/AnyTransliterator;->target:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "id":Ljava/lang/String;
    const/4 v4, 0x0

    :try_start_0
    invoke-static {v0, v4}, Lcom/ibm/icu/text/Transliterator;->getInstance(Ljava/lang/String;I)Lcom/ibm/icu/text/Transliterator;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 159
    :goto_1
    if-nez v3, :cond_3

    .line 162
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string/jumbo v5, "-Latin;Latin-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lcom/ibm/icu/text/AnyTransliterator;->target:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 164
    const/4 v4, 0x0

    :try_start_1
    invoke-static {v0, v4}, Lcom/ibm/icu/text/Transliterator;->getInstance(Ljava/lang/String;I)Lcom/ibm/icu/text/Transliterator;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 168
    :cond_3
    :goto_2
    if-eqz v3, :cond_1

    .line 169
    iget-object v4, p0, Lcom/ibm/icu/text/AnyTransliterator;->cache:Ljava/util/Map;

    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 158
    :catch_0
    move-exception v4

    goto :goto_1

    .line 165
    :catch_1
    move-exception v4

    goto :goto_2
.end method

.method static register()V
    .locals 12

    .prologue
    .line 183
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 185
    .local v2, "seen":Ljava/util/HashSet;
    invoke-static {}, Lcom/ibm/icu/text/Transliterator;->getAvailableSources()Ljava/util/Enumeration;

    move-result-object v1

    .local v1, "s":Ljava/util/Enumeration;
    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 186
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 189
    .local v3, "source":Ljava/lang/String;
    const-string/jumbo v10, "Any"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 191
    invoke-static {v3}, Lcom/ibm/icu/text/Transliterator;->getAvailableTargets(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v4

    .line 192
    .local v4, "t":Ljava/util/Enumeration;
    :cond_1
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 193
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 196
    .local v5, "target":Ljava/lang/String;
    invoke-virtual {v2, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 197
    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 200
    invoke-static {v5}, Lcom/ibm/icu/text/AnyTransliterator;->scriptNameToCode(Ljava/lang/String;)I

    move-result v6

    .line 201
    .local v6, "targetScript":I
    const/4 v10, -0x1

    if-eq v6, v10, :cond_1

    .line 203
    invoke-static {v3, v5}, Lcom/ibm/icu/text/Transliterator;->getAvailableVariants(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v8

    .line 204
    .local v8, "v":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v8}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 205
    invoke-interface {v8}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 208
    .local v9, "variant":Ljava/lang/String;
    const-string/jumbo v10, "Any"

    invoke-static {v10, v5, v9}, Lcom/ibm/icu/text/TransliteratorIDParser;->STVtoID(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, "id":Ljava/lang/String;
    new-instance v7, Lcom/ibm/icu/text/AnyTransliterator;

    invoke-direct {v7, v0, v5, v9, v6}, Lcom/ibm/icu/text/AnyTransliterator;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 211
    .local v7, "trans":Lcom/ibm/icu/text/AnyTransliterator;
    invoke-static {v7}, Lcom/ibm/icu/text/Transliterator;->registerInstance(Lcom/ibm/icu/text/Transliterator;)V

    .line 212
    const-string/jumbo v10, "Null"

    const/4 v11, 0x0

    invoke-static {v5, v10, v11}, Lcom/ibm/icu/text/Transliterator;->registerSpecialInverse(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 216
    .end local v0    # "id":Ljava/lang/String;
    .end local v3    # "source":Ljava/lang/String;
    .end local v4    # "t":Ljava/util/Enumeration;
    .end local v5    # "target":Ljava/lang/String;
    .end local v6    # "targetScript":I
    .end local v7    # "trans":Lcom/ibm/icu/text/AnyTransliterator;
    .end local v8    # "v":Ljava/util/Enumeration;
    .end local v9    # "variant":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private static scriptNameToCode(Ljava/lang/String;)I
    .locals 4
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/4 v2, -0x1

    .line 224
    :try_start_0
    invoke-static {p0}, Lcom/ibm/icu/lang/UScript;->getCode(Ljava/lang/String;)[I

    move-result-object v0

    .line 225
    .local v0, "codes":[I
    if-eqz v0, :cond_0

    const/4 v3, 0x0

    aget v2, v0, v3
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    .end local v0    # "codes":[I
    :cond_0
    :goto_0
    return v2

    .line 226
    :catch_0
    move-exception v1

    .line 227
    .local v1, "e":Ljava/util/MissingResourceException;
    goto :goto_0
.end method


# virtual methods
.method protected handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V
    .locals 9
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "pos"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "isIncremental"    # Z

    .prologue
    .line 69
    iget v1, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 70
    .local v1, "allStart":I
    iget v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 72
    .local v0, "allLimit":I
    new-instance v4, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;

    iget v7, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextStart:I

    iget v8, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    invoke-direct {v4, p1, v7, v8}, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;-><init>(Lcom/ibm/icu/text/Replaceable;II)V

    .line 75
    .local v4, "it":Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;
    :cond_0
    :goto_0
    invoke-virtual {v4}, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->next()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 77
    iget v7, v4, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->limit:I

    if-le v7, v1, :cond_0

    .line 81
    iget v7, v4, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->scriptCode:I

    invoke-direct {p0, v7}, Lcom/ibm/icu/text/AnyTransliterator;->getTransliterator(I)Lcom/ibm/icu/text/Transliterator;

    move-result-object v6

    .line 83
    .local v6, "t":Lcom/ibm/icu/text/Transliterator;
    if-nez v6, :cond_1

    .line 86
    iget v7, v4, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->limit:I

    iput v7, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    goto :goto_0

    .line 93
    :cond_1
    if-eqz p3, :cond_3

    iget v7, v4, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->limit:I

    if-lt v7, v0, :cond_3

    const/4 v3, 0x1

    .line 95
    .local v3, "incremental":Z
    :goto_1
    iget v7, v4, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->start:I

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 96
    iget v7, v4, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->limit:I

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    iput v7, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 97
    iget v5, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 98
    .local v5, "limit":I
    invoke-virtual {v6, p1, p2, v3}, Lcom/ibm/icu/text/Transliterator;->filteredTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V

    .line 99
    iget v7, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    sub-int v2, v7, v5

    .line 100
    .local v2, "delta":I
    add-int/2addr v0, v2

    .line 101
    invoke-virtual {v4, v2}, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->adjustLimit(I)V

    .line 104
    iget v7, v4, Lcom/ibm/icu/text/AnyTransliterator$ScriptRunIterator;->limit:I

    if-lt v7, v0, :cond_0

    .line 109
    .end local v2    # "delta":I
    .end local v3    # "incremental":Z
    .end local v5    # "limit":I
    .end local v6    # "t":Lcom/ibm/icu/text/Transliterator;
    :cond_2
    iput v0, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 110
    return-void

    .line 93
    .restart local v6    # "t":Lcom/ibm/icu/text/Transliterator;
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method
