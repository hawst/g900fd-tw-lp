.class public Lcom/ibm/icu/text/BidiRun;
.super Ljava/lang/Object;
.source "BidiRun.java"


# instance fields
.field insertRemove:I

.field level:B

.field limit:I

.field start:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0, v0, v0, v0}, Lcom/ibm/icu/text/BidiRun;-><init>(IIB)V

    .line 57
    return-void
.end method

.method constructor <init>(IIB)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "embeddingLevel"    # B

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput p1, p0, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 66
    iput p2, p0, Lcom/ibm/icu/text/BidiRun;->limit:I

    .line 67
    iput-byte p3, p0, Lcom/ibm/icu/text/BidiRun;->level:B

    .line 68
    return-void
.end method


# virtual methods
.method copyFrom(Lcom/ibm/icu/text/BidiRun;)V
    .locals 1
    .param p1, "run"    # Lcom/ibm/icu/text/BidiRun;

    .prologue
    .line 76
    iget v0, p1, Lcom/ibm/icu/text/BidiRun;->start:I

    iput v0, p0, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 77
    iget v0, p1, Lcom/ibm/icu/text/BidiRun;->limit:I

    iput v0, p0, Lcom/ibm/icu/text/BidiRun;->limit:I

    .line 78
    iget-byte v0, p1, Lcom/ibm/icu/text/BidiRun;->level:B

    iput-byte v0, p0, Lcom/ibm/icu/text/BidiRun;->level:B

    .line 79
    iget v0, p1, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    iput v0, p0, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 80
    return-void
.end method

.method public getDirection()B
    .locals 1

    .prologue
    .line 146
    iget-byte v0, p0, Lcom/ibm/icu/text/BidiRun;->level:B

    and-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    return v0
.end method

.method public getEmbeddingLevel()B
    .locals 1

    .prologue
    .line 115
    iget-byte v0, p0, Lcom/ibm/icu/text/BidiRun;->level:B

    return v0
.end method

.method public getLength()I
    .locals 2

    .prologue
    .line 106
    iget v0, p0, Lcom/ibm/icu/text/BidiRun;->limit:I

    iget v1, p0, Lcom/ibm/icu/text/BidiRun;->start:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getLimit()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/ibm/icu/text/BidiRun;->limit:I

    return v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/ibm/icu/text/BidiRun;->start:I

    return v0
.end method

.method public isEvenRun()Z
    .locals 1

    .prologue
    .line 137
    iget-byte v0, p0, Lcom/ibm/icu/text/BidiRun;->level:B

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOddRun()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 126
    iget-byte v1, p0, Lcom/ibm/icu/text/BidiRun;->level:B

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 155
    new-instance v0, Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "BidiRun "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/ibm/icu/text/BidiRun;->start:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lcom/ibm/icu/text/BidiRun;->limit:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " @ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-byte v2, p0, Lcom/ibm/icu/text/BidiRun;->level:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
