.class final Lcom/ibm/icu/text/Normalizer$FCDMode;
.super Lcom/ibm/icu/text/Normalizer$Mode;
.source "Normalizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/Normalizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FCDMode"
.end annotation


# direct methods
.method private constructor <init>(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 516
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/Normalizer$Mode;-><init>(ILcom/ibm/icu/text/Normalizer$1;)V

    .line 517
    return-void
.end method

.method constructor <init>(ILcom/ibm/icu/text/Normalizer$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Lcom/ibm/icu/text/Normalizer$1;

    .prologue
    .line 514
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/Normalizer$FCDMode;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected getMask()I
    .locals 1

    .prologue
    .line 537
    const v0, 0xff04

    return v0
.end method

.method protected getMinC()I
    .locals 1

    .prologue
    .line 528
    const/16 v0, 0x300

    return v0
.end method

.method protected getNextBoundary()Lcom/ibm/icu/text/Normalizer$IsNextBoundary;
    .locals 2

    .prologue
    .line 534
    new-instance v0, Lcom/ibm/icu/text/Normalizer$IsNextNFDSafe;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/Normalizer$IsNextNFDSafe;-><init>(Lcom/ibm/icu/text/Normalizer$1;)V

    return-object v0
.end method

.method protected getPrevBoundary()Lcom/ibm/icu/text/Normalizer$IsPrevBoundary;
    .locals 2

    .prologue
    .line 531
    new-instance v0, Lcom/ibm/icu/text/Normalizer$IsPrevNFDSafe;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/Normalizer$IsPrevNFDSafe;-><init>(Lcom/ibm/icu/text/Normalizer$1;)V

    return-object v0
.end method

.method protected isNFSkippable(I)Z
    .locals 2
    .param p1, "c"    # I

    .prologue
    const/4 v0, 0x1

    .line 546
    invoke-static {p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(I)I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected normalize([CII[CIILcom/ibm/icu/text/UnicodeSet;)I
    .locals 1
    .param p1, "src"    # [C
    .param p2, "srcStart"    # I
    .param p3, "srcLimit"    # I
    .param p4, "dest"    # [C
    .param p5, "destStart"    # I
    .param p6, "destLimit"    # I
    .param p7, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 521
    invoke-static/range {p1 .. p7}, Lcom/ibm/icu/impl/NormalizerImpl;->makeFCD([CII[CIILcom/ibm/icu/text/UnicodeSet;)I

    move-result v0

    return v0
.end method

.method protected normalize(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1, "src"    # Ljava/lang/String;
    .param p2, "options"    # I

    .prologue
    .line 525
    invoke-static {p1, p2}, Lcom/ibm/icu/text/Normalizer;->access$1000(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected quickCheck([CIIZLcom/ibm/icu/text/UnicodeSet;)Lcom/ibm/icu/text/Normalizer$QuickCheckResult;
    .locals 1
    .param p1, "src"    # [C
    .param p2, "start"    # I
    .param p3, "limit"    # I
    .param p4, "allowMaybe"    # Z
    .param p5, "nx"    # Lcom/ibm/icu/text/UnicodeSet;

    .prologue
    .line 542
    invoke-static {p1, p2, p3, p5}, Lcom/ibm/icu/impl/NormalizerImpl;->checkFCD([CIILcom/ibm/icu/text/UnicodeSet;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/ibm/icu/text/Normalizer;->YES:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/text/Normalizer;->NO:Lcom/ibm/icu/text/Normalizer$QuickCheckResult;

    goto :goto_0
.end method
