.class Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;
.super Ljava/lang/Object;
.source "RBBIRuleParseTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/RBBIRuleParseTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RBBIRuleTableElement"
.end annotation


# instance fields
.field fAction:S

.field fCharClass:S

.field fNextChar:Z

.field fNextState:S

.field fPushState:S

.field fStateName:Ljava/lang/String;


# direct methods
.method constructor <init>(SIIIZLjava/lang/String;)V
    .locals 1
    .param p1, "a"    # S
    .param p2, "cc"    # I
    .param p3, "ns"    # I
    .param p4, "ps"    # I
    .param p5, "nc"    # Z
    .param p6, "sn"    # Ljava/lang/String;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-short p1, p0, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fAction:S

    .line 72
    int-to-short v0, p2

    iput-short v0, p0, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fCharClass:S

    .line 73
    int-to-short v0, p3

    iput-short v0, p0, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fNextState:S

    .line 74
    int-to-short v0, p4

    iput-short v0, p0, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fPushState:S

    .line 75
    iput-boolean p5, p0, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fNextChar:Z

    .line 76
    iput-object p6, p0, Lcom/ibm/icu/text/RBBIRuleParseTable$RBBIRuleTableElement;->fStateName:Ljava/lang/String;

    .line 77
    return-void
.end method
