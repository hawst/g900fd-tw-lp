.class public Lcom/ibm/icu/text/RuleBasedBreakIterator;
.super Lcom/ibm/icu/text/BreakIterator;
.source "RuleBasedBreakIterator.java"


# static fields
.field private static CI_DONE32:I = 0x0

.field private static final RBBI_DEBUG_ARG:Ljava/lang/String; = "rbbi"

.field private static final RBBI_END:I = 0x2

.field private static final RBBI_RUN:I = 0x1

.field private static final RBBI_START:I = 0x0

.field private static final START_STATE:I = 0x1

.field private static final STOP_STATE:I = 0x0

.field public static final WORD_IDEO:I = 0x190

.field public static final WORD_IDEO_LIMIT:I = 0x1f4

.field public static final WORD_KANA:I = 0x12c

.field public static final WORD_KANA_LIMIT:I = 0x190

.field public static final WORD_LETTER:I = 0xc8

.field public static final WORD_LETTER_LIMIT:I = 0x12c

.field public static final WORD_NONE:I = 0x0

.field public static final WORD_NONE_LIMIT:I = 0x64

.field public static final WORD_NUMBER:I = 0x64

.field public static final WORD_NUMBER_LIMIT:I = 0xc8

.field private static debugInitDone:Z

.field protected static fDebugEnv:Ljava/lang/String;

.field public static fTrace:Z


# instance fields
.field protected fDictionaryCharCount:I

.field private fLastRuleStatusIndex:I

.field private fLastStatusIndexValid:Z

.field protected fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

.field private fText:Ljava/text/CharacterIterator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x0

    sput-boolean v0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->debugInitDone:Z

    .line 799
    const-string/jumbo v0, "rbbi"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "rbbi"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUDebug;->value(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    sput-object v0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fDebugEnv:Ljava/lang/String;

    .line 806
    const v0, 0x7fffffff

    sput v0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CI_DONE32:I

    return-void

    .line 799
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/ibm/icu/text/BreakIterator;-><init>()V

    .line 241
    new-instance v0, Ljava/text/StringCharacterIterator;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Ljava/text/StringCharacterIterator;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 7
    .param p1, "rules"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/ibm/icu/text/BreakIterator;-><init>()V

    .line 241
    new-instance v5, Ljava/text/StringCharacterIterator;

    const-string/jumbo v6, ""

    invoke-direct {v5, v6}, Ljava/text/StringCharacterIterator;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    .line 65
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->init()V

    .line 67
    :try_start_0
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 68
    .local v4, "ruleOS":Ljava/io/ByteArrayOutputStream;
    invoke-static {p1, v4}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->compileRules(Ljava/lang/String;Ljava/io/OutputStream;)V

    .line 69
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 70
    .local v2, "ruleBA":[B
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 71
    .local v3, "ruleIS":Ljava/io/InputStream;
    invoke-static {v3}, Lcom/ibm/icu/text/RBBIDataWrapper;->get(Ljava/io/InputStream;)Lcom/ibm/icu/text/RBBIDataWrapper;

    move-result-object v5

    iput-object v5, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    return-void

    .line 72
    .end local v2    # "ruleBA":[B
    .end local v3    # "ruleIS":Ljava/io/InputStream;
    .end local v4    # "ruleOS":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v6, "RuleBasedBreakIterator rule compilation internal error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 77
    .local v1, "rte":Ljava/lang/RuntimeException;
    throw v1
.end method

.method static CICurrent32(Ljava/text/CharacterIterator;)I
    .locals 6
    .param p0, "ci"    # Ljava/text/CharacterIterator;

    .prologue
    const v5, 0xd800

    .line 890
    invoke-interface {p0}, Ljava/text/CharacterIterator;->current()C

    move-result v0

    .line 891
    .local v0, "lead":C
    move v1, v0

    .line 892
    .local v1, "retVal":I
    if-ge v1, v5, :cond_0

    move v2, v1

    .line 910
    .end local v1    # "retVal":I
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 895
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":I
    :cond_0
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 896
    invoke-interface {p0}, Ljava/text/CharacterIterator;->next()C

    move-result v3

    .line 897
    .local v3, "trail":I
    invoke-interface {p0}, Ljava/text/CharacterIterator;->previous()C

    .line 898
    int-to-char v4, v3

    invoke-static {v4}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 899
    sub-int v4, v0, v5

    shl-int/lit8 v4, v4, 0xa

    const v5, 0xdc00

    sub-int v5, v3, v5

    add-int/2addr v4, v5

    const/high16 v5, 0x10000

    add-int v1, v4, v5

    .end local v3    # "trail":I
    :cond_1
    :goto_1
    move v2, v1

    .line 910
    .end local v1    # "retVal":I
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 904
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":I
    :cond_2
    const v4, 0xffff

    if-ne v0, v4, :cond_1

    .line 905
    invoke-interface {p0}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v4

    invoke-interface {p0}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v5

    if-lt v4, v5, :cond_1

    .line 906
    sget v1, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CI_DONE32:I

    goto :goto_1
.end method

.method static CINext32(Ljava/text/CharacterIterator;)I
    .locals 3
    .param p0, "ci"    # Ljava/text/CharacterIterator;

    .prologue
    const v2, 0xd800

    .line 818
    invoke-interface {p0}, Ljava/text/CharacterIterator;->current()C

    move-result v0

    .line 819
    .local v0, "c":I
    if-lt v0, v2, :cond_1

    const v1, 0xdbff

    if-gt v0, v1, :cond_1

    .line 820
    invoke-interface {p0}, Ljava/text/CharacterIterator;->next()C

    move-result v0

    .line 821
    const v1, 0xdc00

    if-lt v0, v1, :cond_0

    const v1, 0xdfff

    if-le v0, v1, :cond_1

    .line 822
    :cond_0
    invoke-interface {p0}, Ljava/text/CharacterIterator;->previous()C

    move-result v0

    .line 827
    :cond_1
    invoke-interface {p0}, Ljava/text/CharacterIterator;->next()C

    move-result v0

    .line 831
    if-lt v0, v2, :cond_2

    .line 832
    invoke-static {p0, v0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CINextTrail32(Ljava/text/CharacterIterator;I)I

    move-result v0

    .line 835
    :cond_2
    const/high16 v1, 0x10000

    if-lt v0, v1, :cond_3

    sget v1, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CI_DONE32:I

    if-eq v0, v1, :cond_3

    .line 838
    invoke-interface {p0}, Ljava/text/CharacterIterator;->previous()C

    .line 840
    :cond_3
    return v0
.end method

.method private static CINextTrail32(Ljava/text/CharacterIterator;I)I
    .locals 4
    .param p0, "ci"    # Ljava/text/CharacterIterator;
    .param p1, "lead"    # I

    .prologue
    .line 852
    move v1, p1

    .line 853
    .local v1, "retVal":I
    const v2, 0xdbff

    if-gt p1, v2, :cond_2

    .line 854
    invoke-interface {p0}, Ljava/text/CharacterIterator;->next()C

    move-result v0

    .line 855
    .local v0, "cTrail":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 856
    const v2, 0xd800

    sub-int v2, p1, v2

    shl-int/lit8 v2, v2, 0xa

    const v3, 0xdc00

    sub-int v3, v0, v3

    add-int/2addr v2, v3

    const/high16 v3, 0x10000

    add-int v1, v2, v3

    .line 867
    .end local v0    # "cTrail":C
    :cond_0
    :goto_0
    return v1

    .line 860
    .restart local v0    # "cTrail":C
    :cond_1
    invoke-interface {p0}, Ljava/text/CharacterIterator;->previous()C

    goto :goto_0

    .line 863
    .end local v0    # "cTrail":C
    :cond_2
    const v2, 0xffff

    if-ne p1, v2, :cond_0

    invoke-interface {p0}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v2

    invoke-interface {p0}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 864
    sget v1, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CI_DONE32:I

    goto :goto_0
.end method

.method private static CIPrevious32(Ljava/text/CharacterIterator;)I
    .locals 5
    .param p0, "ci"    # Ljava/text/CharacterIterator;

    .prologue
    .line 871
    invoke-interface {p0}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v3

    invoke-interface {p0}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v4

    if-gt v3, v4, :cond_1

    .line 872
    sget v1, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CI_DONE32:I

    .line 886
    :cond_0
    :goto_0
    return v1

    .line 874
    :cond_1
    invoke-interface {p0}, Ljava/text/CharacterIterator;->previous()C

    move-result v2

    .line 875
    .local v2, "trail":C
    move v1, v2

    .line 876
    .local v1, "retVal":I
    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p0}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v3

    invoke-interface {p0}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v4

    if-le v3, v4, :cond_0

    .line 877
    invoke-interface {p0}, Ljava/text/CharacterIterator;->previous()C

    move-result v0

    .line 878
    .local v0, "lead":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 879
    const v3, 0xd800

    sub-int v3, v0, v3

    shl-int/lit8 v3, v3, 0xa

    const v4, 0xdc00

    sub-int v4, v2, v4

    add-int/2addr v3, v4

    const/high16 v4, 0x10000

    add-int v1, v3, v4

    .line 882
    goto :goto_0

    .line 883
    :cond_2
    invoke-interface {p0}, Ljava/text/CharacterIterator;->next()C

    goto :goto_0
.end method

.method protected static final checkOffset(ILjava/text/CharacterIterator;)V
    .locals 2
    .param p0, "offset"    # I
    .param p1, "text"    # Ljava/text/CharacterIterator;

    .prologue
    .line 621
    invoke-interface {p1}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v0

    if-lt p0, v0, :cond_0

    invoke-interface {p1}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v0

    if-le p0, v0, :cond_1

    .line 622
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "offset out of bounds"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 624
    :cond_1
    return-void
.end method

.method private static compileRules(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 0
    .param p0, "rules"    # Ljava/lang/String;
    .param p1, "ruleBinary"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 309
    invoke-static {p0, p1}, Lcom/ibm/icu/text/RBBIRuleBuilder;->compileRules(Ljava/lang/String;Ljava/io/OutputStream;)V

    .line 310
    return-void
.end method

.method public static getInstanceFromCompiledRules(Ljava/io/InputStream;)Lcom/ibm/icu/text/RuleBasedBreakIterator;
    .locals 2
    .param p0, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Lcom/ibm/icu/text/RuleBasedBreakIterator;

    invoke-direct {v0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;-><init>()V

    .line 47
    .local v0, "This":Lcom/ibm/icu/text/RuleBasedBreakIterator;
    invoke-static {p0}, Lcom/ibm/icu/text/RBBIDataWrapper;->get(Ljava/io/InputStream;)Lcom/ibm/icu/text/RBBIDataWrapper;

    move-result-object v1

    iput-object v1, v0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    .line 48
    return-object v0
.end method

.method private handleNext([S)I
    .locals 15
    .param p1, "stateTable"    # [S

    .prologue
    .line 945
    const/4 v1, 0x0

    .line 949
    .local v1, "category":S
    const/4 v5, 0x0

    .line 950
    .local v5, "lookaheadStatus":I
    const/4 v6, 0x0

    .line 951
    .local v6, "lookaheadTagIdx":I
    const/4 v8, 0x0

    .line 952
    .local v8, "result":I
    const/4 v2, 0x0

    .line 953
    .local v2, "initialPosition":I
    const/4 v4, 0x0

    .line 954
    .local v4, "lookaheadResult":I
    const/4 v11, 0x5

    aget-short v11, p1, v11

    and-int/lit8 v11, v11, 0x1

    if-eqz v11, :cond_1

    const/4 v3, 0x1

    .line 957
    .local v3, "lookAheadHardBreak":Z
    :goto_0
    sget-boolean v11, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fTrace:Z

    if-eqz v11, :cond_0

    .line 958
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v12, "Handle Next   pos      char  state category"

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 962
    :cond_0
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastStatusIndexValid:Z

    .line 963
    const/4 v11, 0x0

    iput v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    .line 966
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    if-nez v11, :cond_2

    .line 967
    const/4 v11, 0x0

    iput v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    .line 968
    const/4 v11, -0x1

    .line 1140
    :goto_1
    return v11

    .line 954
    .end local v3    # "lookAheadHardBreak":Z
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 972
    .restart local v3    # "lookAheadHardBreak":Z
    :cond_2
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v11}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v2

    .line 973
    move v8, v2

    .line 974
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v11}, Ljava/text/CharacterIterator;->current()C

    move-result v0

    .line 975
    .local v0, "c":I
    const v11, 0xd800

    if-lt v0, v11, :cond_3

    .line 976
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {v11, v0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CINextTrail32(Ljava/text/CharacterIterator;I)I

    move-result v0

    .line 977
    sget v11, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CI_DONE32:I

    if-ne v0, v11, :cond_3

    .line 978
    const/4 v11, 0x0

    iput v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    .line 979
    const/4 v11, -0x1

    goto :goto_1

    .line 984
    :cond_3
    const/4 v10, 0x1

    .line 985
    .local v10, "state":I
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    invoke-virtual {v11, v10}, Lcom/ibm/icu/text/RBBIDataWrapper;->getRowIndex(I)I

    move-result v9

    .line 986
    .local v9, "row":I
    const/4 v1, 0x3

    .line 987
    const/4 v7, 0x1

    .line 988
    .local v7, "mode":I
    const/4 v11, 0x5

    aget-short v11, p1, v11

    and-int/lit8 v11, v11, 0x2

    if-eqz v11, :cond_4

    .line 989
    const/4 v1, 0x2

    .line 990
    const/4 v7, 0x0

    .line 995
    :cond_4
    :goto_2
    if-eqz v10, :cond_5

    .line 996
    sget v11, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CI_DONE32:I

    if-ne v0, v11, :cond_a

    .line 998
    const/4 v11, 0x2

    if-ne v7, v11, :cond_9

    .line 1003
    if-le v4, v8, :cond_8

    .line 1009
    move v8, v4

    .line 1010
    iput v6, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    .line 1011
    const/4 v5, 0x0

    .line 1127
    :cond_5
    :goto_3
    if-ne v8, v2, :cond_6

    .line 1128
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v11, v2}, Ljava/text/CharacterIterator;->setIndex(I)C

    move-result v8

    .line 1129
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {v11}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CINext32(Ljava/text/CharacterIterator;)I

    .line 1130
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v11}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v8

    .line 1136
    :cond_6
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v11, v8}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1137
    sget-boolean v11, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fTrace:Z

    if-eqz v11, :cond_7

    .line 1138
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v13, "result = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_7
    move v11, v8

    .line 1140
    goto :goto_1

    .line 1012
    :cond_8
    if-ne v8, v2, :cond_5

    .line 1015
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v11, v2}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1016
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {v11}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CINext32(Ljava/text/CharacterIterator;)I

    goto :goto_3

    .line 1021
    :cond_9
    const/4 v7, 0x2

    .line 1022
    const/4 v1, 0x1

    .line 1029
    :cond_a
    const/4 v11, 0x1

    if-ne v7, v11, :cond_b

    .line 1033
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v11, v11, Lcom/ibm/icu/text/RBBIDataWrapper;->fTrie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v11, v0}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v11

    int-to-short v1, v11

    .line 1040
    and-int/lit16 v11, v1, 0x4000

    if-eqz v11, :cond_b

    .line 1041
    iget v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fDictionaryCharCount:I

    add-int/lit8 v11, v11, 0x1

    iput v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fDictionaryCharCount:I

    .line 1043
    and-int/lit16 v11, v1, -0x4001

    int-to-short v1, v11

    .line 1047
    :cond_b
    sget-boolean v11, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fTrace:Z

    if-eqz v11, :cond_c

    .line 1048
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v13, "            "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    iget-object v13, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v13}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v13

    const/4 v14, 0x5

    invoke-static {v13, v14}, Lcom/ibm/icu/text/RBBIDataWrapper;->intToString(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1049
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/16 v12, 0xa

    invoke-static {v0, v12}, Lcom/ibm/icu/text/RBBIDataWrapper;->intToHexString(II)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1050
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v13, 0x7

    invoke-static {v10, v13}, Lcom/ibm/icu/text/RBBIDataWrapper;->intToString(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    const/4 v13, 0x6

    invoke-static {v1, v13}, Lcom/ibm/icu/text/RBBIDataWrapper;->intToString(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1055
    :cond_c
    add-int/lit8 v11, v9, 0x4

    add-int/2addr v11, v1

    aget-short v10, p1, v11

    .line 1056
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    invoke-virtual {v11, v10}, Lcom/ibm/icu/text/RBBIDataWrapper;->getRowIndex(I)I

    move-result v9

    .line 1061
    const/4 v11, 0x1

    if-ne v7, v11, :cond_10

    .line 1062
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v11}, Ljava/text/CharacterIterator;->next()C

    move-result v0

    .line 1063
    const v11, 0xd800

    if-lt v0, v11, :cond_d

    .line 1064
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {v11, v0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CINextTrail32(Ljava/text/CharacterIterator;I)I

    move-result v0

    .line 1072
    :cond_d
    :goto_4
    add-int/lit8 v11, v9, 0x0

    aget-short v11, p1, v11

    const/4 v12, -0x1

    if-ne v11, v12, :cond_f

    .line 1074
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v11}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v8

    .line 1075
    const/high16 v11, 0x10000

    if-lt v0, v11, :cond_e

    sget v11, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CI_DONE32:I

    if-eq v0, v11, :cond_e

    .line 1078
    add-int/lit8 v8, v8, -0x1

    .line 1082
    :cond_e
    add-int/lit8 v11, v9, 0x2

    aget-short v11, p1, v11

    iput v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    .line 1085
    :cond_f
    add-int/lit8 v11, v9, 0x1

    aget-short v11, p1, v11

    if-eqz v11, :cond_13

    .line 1086
    if-eqz v5, :cond_11

    add-int/lit8 v11, v9, 0x0

    aget-short v11, p1, v11

    if-ne v11, v5, :cond_11

    .line 1090
    move v8, v4

    .line 1091
    iput v6, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    .line 1092
    const/4 v5, 0x0

    .line 1094
    if-eqz v3, :cond_4

    move v11, v8

    .line 1095
    goto/16 :goto_1

    .line 1067
    :cond_10
    if-nez v7, :cond_d

    .line 1068
    const/4 v7, 0x1

    goto :goto_4

    .line 1102
    :cond_11
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v11}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v4

    .line 1103
    const/high16 v11, 0x10000

    if-lt v0, v11, :cond_12

    sget v11, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CI_DONE32:I

    if-eq v0, v11, :cond_12

    .line 1106
    add-int/lit8 v4, v4, -0x1

    .line 1108
    :cond_12
    add-int/lit8 v11, v9, 0x1

    aget-short v5, p1, v11

    .line 1109
    add-int/lit8 v11, v9, 0x2

    aget-short v6, p1, v11

    .line 1110
    goto/16 :goto_2

    .line 1114
    :cond_13
    add-int/lit8 v11, v9, 0x0

    aget-short v11, p1, v11

    if-eqz v11, :cond_4

    .line 1117
    const/4 v5, 0x0

    .line 1118
    goto/16 :goto_2
.end method

.method private handlePrevious([S)I
    .locals 14
    .param p1, "stateTable"    # [S

    .prologue
    const/4 v13, 0x5

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1147
    const/4 v1, 0x0

    .line 1151
    .local v1, "category":I
    const/4 v5, 0x0

    .line 1152
    .local v5, "lookaheadStatus":I
    const/4 v7, 0x0

    .line 1153
    .local v7, "result":I
    const/4 v2, 0x0

    .line 1154
    .local v2, "initialPosition":I
    const/4 v4, 0x0

    .line 1155
    .local v4, "lookaheadResult":I
    aget-short v12, p1, v13

    and-int/lit8 v12, v12, 0x1

    if-eqz v12, :cond_1

    move v3, v10

    .line 1159
    .local v3, "lookAheadHardBreak":Z
    :goto_0
    iget-object v12, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    if-eqz v12, :cond_0

    if-nez p1, :cond_2

    .line 1335
    :cond_0
    :goto_1
    return v11

    .end local v3    # "lookAheadHardBreak":Z
    :cond_1
    move v3, v11

    .line 1155
    goto :goto_0

    .line 1166
    .restart local v3    # "lookAheadHardBreak":Z
    :cond_2
    iput-boolean v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastStatusIndexValid:Z

    .line 1167
    iput v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    .line 1170
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v11}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v2

    .line 1171
    move v7, v2

    .line 1172
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {v11}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CIPrevious32(Ljava/text/CharacterIterator;)I

    move-result v0

    .line 1175
    .local v0, "c":I
    const/4 v9, 0x1

    .line 1176
    .local v9, "state":I
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    invoke-virtual {v11, v9}, Lcom/ibm/icu/text/RBBIDataWrapper;->getRowIndex(I)I

    move-result v8

    .line 1177
    .local v8, "row":I
    const/4 v1, 0x3

    .line 1178
    const/4 v6, 0x1

    .line 1179
    .local v6, "mode":I
    aget-short v11, p1, v13

    and-int/lit8 v11, v11, 0x2

    if-eqz v11, :cond_3

    .line 1180
    const/4 v1, 0x2

    .line 1181
    const/4 v6, 0x0

    .line 1184
    :cond_3
    sget-boolean v11, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fTrace:Z

    if-eqz v11, :cond_4

    .line 1185
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string/jumbo v12, "Handle Prev   pos   char  state category "

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1192
    :cond_4
    :goto_2
    sget v11, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CI_DONE32:I

    if-ne v0, v11, :cond_b

    .line 1194
    const/4 v11, 0x2

    if-eq v6, v11, :cond_5

    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v11, v11, Lcom/ibm/icu/text/RBBIDataWrapper;->fHeader:Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;

    iget v11, v11, Lcom/ibm/icu/text/RBBIDataWrapper$RBBIDataHeader;->fVersion:I

    if-ne v11, v10, :cond_a

    .line 1199
    :cond_5
    if-ge v4, v7, :cond_9

    .line 1203
    move v7, v4

    .line 1204
    const/4 v5, 0x0

    .line 1324
    :cond_6
    :goto_3
    if-ne v7, v2, :cond_7

    .line 1325
    iget-object v10, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v10, v2}, Ljava/text/CharacterIterator;->setIndex(I)C

    move-result v7

    .line 1326
    iget-object v10, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {v10}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CIPrevious32(Ljava/text/CharacterIterator;)I

    .line 1327
    iget-object v10, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v10}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v7

    .line 1330
    :cond_7
    iget-object v10, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v10, v7}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1331
    sget-boolean v10, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fTrace:Z

    if-eqz v10, :cond_8

    .line 1332
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v12, "Result = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_8
    move v11, v7

    .line 1335
    goto :goto_1

    .line 1205
    :cond_9
    if-ne v7, v2, :cond_6

    .line 1208
    iget-object v10, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v10, v2}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 1209
    iget-object v10, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {v10}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CIPrevious32(Ljava/text/CharacterIterator;)I

    goto :goto_3

    .line 1213
    :cond_a
    const/4 v6, 0x2

    .line 1214
    const/4 v1, 0x1

    .line 1217
    :cond_b
    if-ne v6, v10, :cond_c

    .line 1221
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v11, v11, Lcom/ibm/icu/text/RBBIDataWrapper;->fTrie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v11, v0}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v11

    int-to-short v1, v11

    .line 1228
    and-int/lit16 v11, v1, 0x4000

    if-eqz v11, :cond_c

    .line 1229
    iget v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fDictionaryCharCount:I

    add-int/lit8 v11, v11, 0x1

    iput v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fDictionaryCharCount:I

    .line 1231
    and-int/lit16 v1, v1, -0x4001

    .line 1236
    :cond_c
    sget-boolean v11, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fTrace:Z

    if-eqz v11, :cond_d

    .line 1237
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v13, "             "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    iget-object v13, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v13}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string/jumbo v13, "   "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1238
    const/16 v11, 0x20

    if-gt v11, v0, :cond_10

    const/16 v11, 0x7f

    if-ge v0, v11, :cond_10

    .line 1239
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v13, "  "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string/jumbo v13, "  "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1243
    :goto_4
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string/jumbo v13, "  "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string/jumbo v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1248
    :cond_d
    add-int/lit8 v11, v8, 0x4

    add-int/2addr v11, v1

    aget-short v9, p1, v11

    .line 1249
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    invoke-virtual {v11, v9}, Lcom/ibm/icu/text/RBBIDataWrapper;->getRowIndex(I)I

    move-result v8

    .line 1251
    add-int/lit8 v11, v8, 0x0

    aget-short v11, p1, v11

    const/4 v12, -0x1

    if-ne v11, v12, :cond_e

    .line 1254
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v11}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v7

    .line 1257
    :cond_e
    add-int/lit8 v11, v8, 0x1

    aget-short v11, p1, v11

    if-eqz v11, :cond_12

    .line 1258
    if-eqz v5, :cond_11

    add-int/lit8 v11, v8, 0x0

    aget-short v11, p1, v11

    if-ne v11, v5, :cond_11

    .line 1264
    move v7, v4

    .line 1265
    const/4 v5, 0x0

    .line 1268
    if-nez v3, :cond_6

    .line 1301
    :cond_f
    :goto_5
    if-eqz v9, :cond_6

    .line 1308
    if-ne v6, v10, :cond_13

    .line 1309
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {v11}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CIPrevious32(Ljava/text/CharacterIterator;)I

    move-result v0

    .line 1310
    goto/16 :goto_2

    .line 1241
    :cond_10
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    const-string/jumbo v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1278
    :cond_11
    iget-object v11, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v11}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v4

    .line 1279
    add-int/lit8 v11, v8, 0x1

    aget-short v5, p1, v11

    .line 1280
    goto :goto_5

    .line 1284
    :cond_12
    add-int/lit8 v11, v8, 0x0

    aget-short v11, p1, v11

    if-eqz v11, :cond_f

    .line 1286
    if-nez v3, :cond_f

    .line 1294
    const/4 v5, 0x0

    goto :goto_5

    .line 1311
    :cond_13
    if-nez v6, :cond_4

    .line 1312
    const/4 v6, 0x1

    .line 1313
    goto/16 :goto_2
.end method

.method private init()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 297
    iput-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastStatusIndexValid:Z

    .line 298
    iput v0, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fDictionaryCharCount:I

    .line 301
    sget-boolean v2, Lcom/ibm/icu/text/RuleBasedBreakIterator;->debugInitDone:Z

    if-nez v2, :cond_1

    .line 302
    const-string/jumbo v2, "rbbi"

    invoke-static {v2}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "rbbi"

    invoke-static {v2}, Lcom/ibm/icu/impl/ICUDebug;->value(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "trace"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_0

    move v0, v1

    :cond_0
    sput-boolean v0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fTrace:Z

    .line 304
    sput-boolean v1, Lcom/ibm/icu/text/RuleBasedBreakIterator;->debugInitDone:Z

    .line 306
    :cond_1
    return-void
.end method

.method private makeRuleStatusValid()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 675
    iget-boolean v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastStatusIndexValid:Z

    if-nez v2, :cond_1

    .line 677
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->current()I

    move-result v2

    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v5}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v5

    if-ne v2, v5, :cond_2

    .line 679
    :cond_0
    iput v4, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    .line 680
    iput-boolean v3, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastStatusIndexValid:Z

    .line 688
    :goto_0
    iget-boolean v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastStatusIndexValid:Z

    if-ne v2, v3, :cond_4

    move v2, v3

    :goto_1
    invoke-static {v2}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 689
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    if-ltz v2, :cond_5

    iget v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper;->fStatusTable:[I

    array-length v5, v5

    if-ge v2, v5, :cond_5

    :goto_2
    invoke-static {v3}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    .line 691
    :cond_1
    return-void

    .line 683
    :cond_2
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->current()I

    move-result v0

    .line 684
    .local v0, "pa":I
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->previous()I

    .line 685
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->next()I

    move-result v1

    .line 686
    .local v1, "pb":I
    if-ne v0, v1, :cond_3

    move v2, v3

    :goto_3
    invoke-static {v2}, Lcom/ibm/icu/impl/Assert;->assrt(Z)V

    goto :goto_0

    :cond_3
    move v2, v4

    goto :goto_3

    .end local v0    # "pa":I
    .end local v1    # "pb":I
    :cond_4
    move v2, v4

    .line 688
    goto :goto_1

    :cond_5
    move v3, v4

    .line 689
    goto :goto_2
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 94
    invoke-super {p0}, Lcom/ibm/icu/text/BreakIterator;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/text/RuleBasedBreakIterator;

    .line 95
    .local v0, "result":Lcom/ibm/icu/text/RuleBasedBreakIterator;
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    if-eqz v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v1}, Ljava/text/CharacterIterator;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/CharacterIterator;

    check-cast v1, Ljava/text/CharacterIterator;

    iput-object v1, v0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    .line 98
    :cond_0
    return-object v0
.end method

.method public current()I
    .locals 1

    .prologue
    .line 669
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v0}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public dump()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    invoke-virtual {v0}, Lcom/ibm/icu/text/RBBIDataWrapper;->dump()V

    .line 292
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "that"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 108
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/ibm/icu/text/RuleBasedBreakIterator;

    move-object v2, v0

    .line 109
    .local v2, "other":Lcom/ibm/icu/text/RuleBasedBreakIterator;
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v5, v2, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    if-eq v4, v5, :cond_1

    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    if-eqz v4, :cond_0

    iget-object v4, v2, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    if-nez v4, :cond_1

    .line 125
    .end local v2    # "other":Lcom/ibm/icu/text/RuleBasedBreakIterator;
    :cond_0
    :goto_0
    return v3

    .line 112
    .restart local v2    # "other":Lcom/ibm/icu/text/RuleBasedBreakIterator;
    :cond_1
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    if-eqz v4, :cond_2

    iget-object v4, v2, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v4, v4, Lcom/ibm/icu/text/RBBIDataWrapper;->fRuleSource:Ljava/lang/String;

    iget-object v5, v2, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper;->fRuleSource:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 116
    :cond_2
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    if-nez v4, :cond_3

    iget-object v4, v2, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    if-nez v4, :cond_3

    .line 117
    const/4 v3, 0x1

    goto :goto_0

    .line 119
    :cond_3
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    if-eqz v4, :cond_0

    iget-object v4, v2, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    if-eqz v4, :cond_0

    .line 122
    iget-object v4, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    iget-object v5, v2, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_0

    .line 124
    .end local v2    # "other":Lcom/ibm/icu/text/RuleBasedBreakIterator;
    :catch_0
    move-exception v1

    .line 125
    .local v1, "e":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public first()I
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    .line 324
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastStatusIndexValid:Z

    .line 325
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    if-nez v0, :cond_0

    .line 326
    const/4 v0, -0x1

    .line 329
    :goto_0
    return v0

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v0}, Ljava/text/CharacterIterator;->first()C

    .line 329
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v0}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v0

    goto :goto_0
.end method

.method public following(I)I
    .locals 3
    .param p1, "offset"    # I

    .prologue
    .line 469
    const/4 v2, 0x0

    iput v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    .line 470
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastStatusIndexValid:Z

    .line 471
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v2

    if-lt p1, v2, :cond_2

    .line 472
    :cond_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->last()I

    .line 473
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->next()I

    move-result v1

    .line 544
    :cond_1
    :goto_0
    return v1

    .line 475
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v2

    if-ge p1, v2, :cond_3

    .line 476
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->first()I

    move-result v1

    goto :goto_0

    .line 483
    :cond_3
    const/4 v1, 0x0

    .line 485
    .local v1, "result":I
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIDataWrapper;->fSRTable:[S

    if-eqz v2, :cond_4

    .line 488
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 492
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CINext32(Ljava/text/CharacterIterator;)I

    .line 494
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIDataWrapper;->fSRTable:[S

    invoke-direct {p0, v2}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->handlePrevious([S)I

    .line 495
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->next()I

    move-result v1

    .line 496
    :goto_1
    if-gt v1, p1, :cond_1

    .line 497
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->next()I

    move-result v1

    .line 498
    goto :goto_1

    .line 501
    :cond_4
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIDataWrapper;->fSFTable:[S

    if-eqz v2, :cond_7

    .line 504
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 505
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CIPrevious32(Ljava/text/CharacterIterator;)I

    .line 507
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIDataWrapper;->fSFTable:[S

    invoke-direct {p0, v2}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->handleNext([S)I

    .line 511
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->previous()I

    move-result v0

    .line 512
    .local v0, "oldresult":I
    :goto_2
    if-le v0, p1, :cond_6

    .line 513
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->previous()I

    move-result v1

    .line 514
    if-gt v1, p1, :cond_5

    move v1, v0

    .line 515
    goto :goto_0

    .line 517
    :cond_5
    move v0, v1

    .line 518
    goto :goto_2

    .line 519
    :cond_6
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->next()I

    move-result v1

    .line 520
    if-gt v1, p1, :cond_1

    .line 521
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->next()I

    move-result v1

    goto :goto_0

    .line 534
    .end local v0    # "oldresult":I
    :cond_7
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 535
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v2

    if-ne p1, v2, :cond_8

    .line 536
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->handleNext()I

    move-result v1

    goto :goto_0

    .line 538
    :cond_8
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->previous()I

    move-result v1

    .line 540
    :goto_3
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-gt v1, p1, :cond_1

    .line 541
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->next()I

    move-result v1

    .line 542
    goto :goto_3
.end method

.method public getRuleStatus()I
    .locals 5

    .prologue
    .line 718
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->makeRuleStatusValid()V

    .line 727
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIDataWrapper;->fStatusTable:[I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    aget v3, v3, v4

    add-int v0, v2, v3

    .line 728
    .local v0, "idx":I
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIDataWrapper;->fStatusTable:[I

    aget v1, v2, v0

    .line 730
    .local v1, "tagVal":I
    return v1
.end method

.method public getRuleStatusVec([I)I
    .locals 5
    .param p1, "fillInArray"    # [I

    .prologue
    .line 758
    invoke-direct {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->makeRuleStatusValid()V

    .line 759
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIDataWrapper;->fStatusTable:[I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    aget v1, v3, v4

    .line 760
    .local v1, "numStatusVals":I
    if-eqz p1, :cond_0

    .line 761
    array-length v3, p1

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 762
    .local v2, "numToCopy":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 763
    iget-object v3, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v3, v3, Lcom/ibm/icu/text/RBBIDataWrapper;->fStatusTable:[I

    iget v4, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    add-int/2addr v4, v0

    add-int/lit8 v4, v4, 0x1

    aget v3, v3, v4

    aput v3, p1, v0

    .line 762
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 766
    .end local v0    # "i":I
    .end local v2    # "numToCopy":I
    :cond_0
    return v1
.end method

.method public getText()Ljava/text/CharacterIterator;
    .locals 1

    .prologue
    .line 779
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    return-object v0
.end method

.method handleNext()I
    .locals 1

    .prologue
    .line 925
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fFTable:[S

    invoke-direct {p0, v0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->handleNext([S)I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v0, v0, Lcom/ibm/icu/text/RBBIDataWrapper;->fRuleSource:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isBoundary(I)Z
    .locals 3
    .param p1, "offset"    # I

    .prologue
    const/4 v1, 0x1

    .line 636
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {p1, v2}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->checkOffset(ILjava/text/CharacterIterator;)V

    .line 639
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v2

    if-ne p1, v2, :cond_0

    .line 640
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->first()I

    .line 660
    :goto_0
    return v1

    .line 644
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v2

    if-ne p1, v2, :cond_1

    .line 645
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->last()I

    goto :goto_0

    .line 656
    :cond_1
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 657
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CIPrevious32(Ljava/text/CharacterIterator;)I

    .line 658
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v0

    .line 659
    .local v0, "pos":I
    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->following(I)I

    move-result v2

    if-ne v2, p1, :cond_2

    .line 660
    .local v1, "result":Z
    :goto_1
    goto :goto_0

    .line 659
    .end local v1    # "result":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method isDictionaryChar(I)Z
    .locals 2
    .param p1, "c"    # I

    .prologue
    .line 1364
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v1, v1, Lcom/ibm/icu/text/RBBIDataWrapper;->fTrie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v1, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v1

    int-to-short v0, v1

    .line 1366
    .local v0, "category":S
    and-int/lit16 v1, v0, 0x4000

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public last()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 340
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    if-nez v1, :cond_0

    .line 341
    iput v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    .line 342
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastStatusIndexValid:Z

    .line 343
    const/4 v0, -0x1

    .line 356
    :goto_0
    return v0

    .line 353
    :cond_0
    iput-boolean v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastStatusIndexValid:Z

    .line 354
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v1}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v0

    .line 355
    .local v0, "pos":I
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v1, v0}, Ljava/text/CharacterIterator;->setIndex(I)C

    goto :goto_0
.end method

.method public next()I
    .locals 1

    .prologue
    .line 390
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->handleNext()I

    move-result v0

    return v0
.end method

.method public next(I)I
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 371
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->current()I

    move-result v0

    .line 372
    .local v0, "result":I
    :goto_0
    if-lez p1, :cond_0

    .line 373
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->handleNext()I

    move-result v0

    .line 374
    add-int/lit8 p1, p1, -0x1

    .line 375
    goto :goto_0

    .line 376
    :cond_0
    :goto_1
    if-gez p1, :cond_1

    .line 377
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->previous()I

    move-result v0

    .line 378
    add-int/lit8 p1, p1, 0x1

    .line 379
    goto :goto_1

    .line 380
    :cond_1
    return v0
.end method

.method public preceding(I)I
    .locals 3
    .param p1, "offset"    # I

    .prologue
    .line 558
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getEndIndex()I

    move-result v2

    if-le p1, v2, :cond_2

    .line 560
    :cond_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->last()I

    move-result v1

    .line 613
    :cond_1
    :goto_0
    return v1

    .line 562
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v2

    if-ge p1, v2, :cond_3

    .line 563
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->first()I

    move-result v1

    goto :goto_0

    .line 571
    :cond_3
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIDataWrapper;->fSFTable:[S

    if-eqz v2, :cond_4

    .line 574
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 578
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CIPrevious32(Ljava/text/CharacterIterator;)I

    .line 579
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIDataWrapper;->fSFTable:[S

    invoke-direct {p0, v2}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->handleNext([S)I

    .line 580
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->previous()I

    move-result v1

    .line 581
    .local v1, "result":I
    :goto_1
    if-lt v1, p1, :cond_1

    .line 582
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->previous()I

    move-result v1

    .line 583
    goto :goto_1

    .line 586
    .end local v1    # "result":I
    :cond_4
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIDataWrapper;->fSRTable:[S

    if-eqz v2, :cond_7

    .line 588
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 589
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CINext32(Ljava/text/CharacterIterator;)I

    .line 591
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v2, v2, Lcom/ibm/icu/text/RBBIDataWrapper;->fSRTable:[S

    invoke-direct {p0, v2}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->handlePrevious([S)I

    .line 596
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->next()I

    move-result v0

    .line 597
    .local v0, "oldresult":I
    :goto_2
    if-ge v0, p1, :cond_6

    .line 598
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->next()I

    move-result v1

    .line 599
    .restart local v1    # "result":I
    if-lt v1, p1, :cond_5

    move v1, v0

    .line 600
    goto :goto_0

    .line 602
    :cond_5
    move v0, v1

    .line 603
    goto :goto_2

    .line 604
    .end local v1    # "result":I
    :cond_6
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->previous()I

    move-result v1

    .line 605
    .restart local v1    # "result":I
    if-lt v1, p1, :cond_1

    .line 606
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->previous()I

    move-result v1

    goto :goto_0

    .line 612
    .end local v0    # "oldresult":I
    .end local v1    # "result":I
    :cond_7
    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v2, p1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 613
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->previous()I

    move-result v1

    goto :goto_0
.end method

.method public previous()I
    .locals 8

    .prologue
    const/4 v5, -0x1

    .line 401
    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->current()I

    move-result v6

    iget-object v7, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v7}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v7

    if-ne v6, v7, :cond_1

    .line 402
    :cond_0
    const/4 v6, 0x0

    iput v6, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    .line 403
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastStatusIndexValid:Z

    move v1, v5

    .line 456
    :goto_0
    return v1

    .line 407
    :cond_1
    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v6, v6, Lcom/ibm/icu/text/RBBIDataWrapper;->fSRTable:[S

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v6, v6, Lcom/ibm/icu/text/RBBIDataWrapper;->fSFTable:[S

    if-eqz v6, :cond_3

    .line 408
    :cond_2
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v5, v5, Lcom/ibm/icu/text/RBBIDataWrapper;->fRTable:[S

    invoke-direct {p0, v5}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->handlePrevious([S)I

    move-result v1

    goto :goto_0

    .line 418
    :cond_3
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->current()I

    move-result v4

    .line 420
    .local v4, "start":I
    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-static {v6}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->CIPrevious32(Ljava/text/CharacterIterator;)I

    .line 421
    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v6, v6, Lcom/ibm/icu/text/RBBIDataWrapper;->fRTable:[S

    invoke-direct {p0, v6}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->handlePrevious([S)I

    move-result v1

    .line 422
    .local v1, "lastResult":I
    if-ne v1, v5, :cond_4

    .line 423
    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v6}, Ljava/text/CharacterIterator;->getBeginIndex()I

    move-result v1

    .line 424
    iget-object v6, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v6, v1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 426
    :cond_4
    move v3, v1

    .line 427
    .local v3, "result":I
    const/4 v2, 0x0

    .line 428
    .local v2, "lastTag":I
    const/4 v0, 0x0

    .line 435
    .local v0, "breakTagValid":Z
    :goto_1
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->handleNext()I

    move-result v3

    .line 436
    if-eq v3, v5, :cond_5

    if-lt v3, v4, :cond_6

    .line 453
    :cond_5
    iget-object v5, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    invoke-interface {v5, v1}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 454
    iput v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    .line 455
    iput-boolean v0, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastStatusIndexValid:Z

    goto :goto_0

    .line 439
    :cond_6
    move v1, v3

    .line 440
    iget v2, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fLastRuleStatusIndex:I

    .line 441
    const/4 v0, 0x1

    .line 442
    goto :goto_1
.end method

.method public setText(Ljava/text/CharacterIterator;)V
    .locals 0
    .param p1, "newText"    # Ljava/text/CharacterIterator;

    .prologue
    .line 790
    iput-object p1, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fText:Ljava/text/CharacterIterator;

    .line 791
    invoke-virtual {p0}, Lcom/ibm/icu/text/RuleBasedBreakIterator;->first()I

    .line 792
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 136
    .local v0, "retStr":Ljava/lang/String;
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    if-eqz v1, :cond_0

    .line 137
    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedBreakIterator;->fRData:Lcom/ibm/icu/text/RBBIDataWrapper;

    iget-object v0, v1, Lcom/ibm/icu/text/RBBIDataWrapper;->fRuleSource:Ljava/lang/String;

    .line 139
    :cond_0
    return-object v0
.end method
