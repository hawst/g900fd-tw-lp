.class final Lcom/ibm/icu/text/CollatorServiceShim;
.super Lcom/ibm/icu/text/Collator$ServiceShim;
.source "CollatorServiceShim.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/CollatorServiceShim$CService;
    }
.end annotation


# static fields
.field private static service:Lcom/ibm/icu/impl/ICULocaleService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/ibm/icu/text/CollatorServiceShim$CService;

    invoke-direct {v0}, Lcom/ibm/icu/text/CollatorServiceShim$CService;-><init>()V

    sput-object v0, Lcom/ibm/icu/text/CollatorServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/ibm/icu/text/Collator$ServiceShim;-><init>()V

    .line 102
    return-void
.end method


# virtual methods
.method getAvailableLocales()[Ljava/util/Locale;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/ibm/icu/text/CollatorServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICULocaleService;->isDefault()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b/coll"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableLocales(Ljava/lang/String;)[Ljava/util/Locale;

    move-result-object v0

    .line 87
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/text/CollatorServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICULocaleService;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v0

    goto :goto_0
.end method

.method getAvailableULocales()[Lcom/ibm/icu/util/ULocale;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/ibm/icu/text/CollatorServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICULocaleService;->isDefault()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    const-string/jumbo v0, "com/ibm/icu/impl/data/icudt40b/coll"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUResourceBundle;->getAvailableULocales(Ljava/lang/String;)[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/ibm/icu/text/CollatorServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v0}, Lcom/ibm/icu/impl/ICULocaleService;->getAvailableULocales()[Lcom/ibm/icu/util/ULocale;

    move-result-object v0

    goto :goto_0
.end method

.method getDisplayName(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;
    .locals 2
    .param p1, "objectLocale"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "displayLocale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 98
    invoke-virtual {p1}, Lcom/ibm/icu/util/ULocale;->getName()Ljava/lang/String;

    move-result-object v0

    .line 99
    .local v0, "id":Ljava/lang/String;
    sget-object v1, Lcom/ibm/icu/text/CollatorServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v1, v0, p2}, Lcom/ibm/icu/impl/ICULocaleService;->getDisplayName(Ljava/lang/String;Lcom/ibm/icu/util/ULocale;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method getInstance(Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/text/Collator;
    .locals 7
    .param p1, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 31
    const/4 v3, 0x1

    :try_start_0
    new-array v0, v3, [Lcom/ibm/icu/util/ULocale;

    .line 32
    .local v0, "actualLoc":[Lcom/ibm/icu/util/ULocale;
    sget-object v3, Lcom/ibm/icu/text/CollatorServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v3, p1, v0}, Lcom/ibm/icu/impl/ICULocaleService;->get(Lcom/ibm/icu/util/ULocale;[Lcom/ibm/icu/util/ULocale;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ibm/icu/text/Collator;

    .line 33
    .local v1, "coll":Lcom/ibm/icu/text/Collator;
    if-nez v1, :cond_0

    .line 34
    new-instance v3, Ljava/util/MissingResourceException;

    const-string/jumbo v4, "Could not locate Collator data"

    const-string/jumbo v5, ""

    const-string/jumbo v6, ""

    invoke-direct {v3, v4, v5, v6}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    .end local v0    # "actualLoc":[Lcom/ibm/icu/util/ULocale;
    .end local v1    # "coll":Lcom/ibm/icu/text/Collator;
    :catch_0
    move-exception v2

    .line 42
    .local v2, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-virtual {v2}, Ljava/lang/CloneNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 36
    .end local v2    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "actualLoc":[Lcom/ibm/icu/util/ULocale;
    .restart local v1    # "coll":Lcom/ibm/icu/text/Collator;
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Lcom/ibm/icu/text/Collator;->clone()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "coll":Lcom/ibm/icu/text/Collator;
    check-cast v1, Lcom/ibm/icu/text/Collator;

    .line 37
    .restart local v1    # "coll":Lcom/ibm/icu/text/Collator;
    const/4 v3, 0x0

    aget-object v3, v0, v3

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-virtual {v1, v3, v4}, Lcom/ibm/icu/text/Collator;->setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 38
    return-object v1
.end method

.method registerFactory(Lcom/ibm/icu/text/Collator$CollatorFactory;)Ljava/lang/Object;
    .locals 2
    .param p1, "f"    # Lcom/ibm/icu/text/Collator$CollatorFactory;

    .prologue
    .line 75
    sget-object v0, Lcom/ibm/icu/text/CollatorServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    new-instance v1, Lcom/ibm/icu/text/CollatorServiceShim$1CFactory;

    invoke-direct {v1, p0, p1}, Lcom/ibm/icu/text/CollatorServiceShim$1CFactory;-><init>(Lcom/ibm/icu/text/CollatorServiceShim;Lcom/ibm/icu/text/Collator$CollatorFactory;)V

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/ICULocaleService;->registerFactory(Lcom/ibm/icu/impl/ICUService$Factory;)Lcom/ibm/icu/impl/ICUService$Factory;

    move-result-object v0

    return-object v0
.end method

.method registerInstance(Lcom/ibm/icu/text/Collator;Lcom/ibm/icu/util/ULocale;)Ljava/lang/Object;
    .locals 1
    .param p1, "collator"    # Lcom/ibm/icu/text/Collator;
    .param p2, "locale"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    .line 48
    sget-object v0, Lcom/ibm/icu/text/CollatorServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    invoke-virtual {v0, p1, p2}, Lcom/ibm/icu/impl/ICULocaleService;->registerObject(Ljava/lang/Object;Lcom/ibm/icu/util/ULocale;)Lcom/ibm/icu/impl/ICUService$Factory;

    move-result-object v0

    return-object v0
.end method

.method unregister(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "registryKey"    # Ljava/lang/Object;

    .prologue
    .line 79
    sget-object v0, Lcom/ibm/icu/text/CollatorServiceShim;->service:Lcom/ibm/icu/impl/ICULocaleService;

    check-cast p1, Lcom/ibm/icu/impl/ICUService$Factory;

    .end local p1    # "registryKey":Ljava/lang/Object;
    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/ICULocaleService;->unregisterFactory(Lcom/ibm/icu/impl/ICUService$Factory;)Z

    move-result v0

    return v0
.end method
