.class public abstract Lcom/ibm/icu/text/UFormat;
.super Ljava/text/Format;
.source "UFormat.java"


# static fields
.field private static final serialVersionUID:J = -0x44e50edf26f8a240L


# instance fields
.field private actualLocale:Lcom/ibm/icu/util/ULocale;

.field private validLocale:Lcom/ibm/icu/util/ULocale;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/text/Format;-><init>()V

    return-void
.end method


# virtual methods
.method public final getLocale(Lcom/ibm/icu/util/ULocale$Type;)Lcom/ibm/icu/util/ULocale;
    .locals 1
    .param p1, "type"    # Lcom/ibm/icu/util/ULocale$Type;

    .prologue
    .line 60
    sget-object v0, Lcom/ibm/icu/util/ULocale;->ACTUAL_LOCALE:Lcom/ibm/icu/util/ULocale$Type;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/UFormat;->actualLocale:Lcom/ibm/icu/util/ULocale;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/UFormat;->validLocale:Lcom/ibm/icu/util/ULocale;

    goto :goto_0
.end method

.method final setLocale(Lcom/ibm/icu/util/ULocale;Lcom/ibm/icu/util/ULocale;)V
    .locals 3
    .param p1, "valid"    # Lcom/ibm/icu/util/ULocale;
    .param p2, "actual"    # Lcom/ibm/icu/util/ULocale;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 83
    if-nez p1, :cond_0

    move v2, v0

    :goto_0
    if-nez p2, :cond_1

    :goto_1
    if-eq v2, v0, :cond_2

    .line 85
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v2, v1

    .line 83
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 90
    :cond_2
    iput-object p1, p0, Lcom/ibm/icu/text/UFormat;->validLocale:Lcom/ibm/icu/util/ULocale;

    .line 91
    iput-object p2, p0, Lcom/ibm/icu/text/UFormat;->actualLocale:Lcom/ibm/icu/util/ULocale;

    .line 92
    return-void
.end method
