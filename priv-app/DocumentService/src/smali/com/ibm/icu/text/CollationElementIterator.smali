.class public final Lcom/ibm/icu/text/CollationElementIterator;
.super Ljava/lang/Object;
.source "CollationElementIterator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/CollationElementIterator$Backup;
    }
.end annotation


# static fields
.field private static final CE_BUFFER_INIT_SIZE_:I = 0x200

.field private static final CE_BYTE_COMMON_:I = 0x5

.field private static final CE_CHARSET_TAG_:I = 0x4

.field private static final CE_CJK_IMPLICIT_TAG_:I = 0x9

.field private static final CE_CONTRACTION_:I = -0xe000000

.field static final CE_CONTRACTION_TAG_:I = 0x2

.field static final CE_DIGIT_TAG_:I = 0xd

.field static final CE_EXPANSION_TAG_:I = 0x1

.field private static final CE_HANGUL_SYLLABLE_TAG_:I = 0x6

.field private static final CE_IMPLICIT_TAG_:I = 0xa

.field private static final CE_LEAD_SURROGATE_TAG_:I = 0x7

.field private static final CE_LONG_PRIMARY_TAG_:I = 0xc

.field static final CE_NOT_FOUND_:I = -0x10000000

.field private static final CE_NOT_FOUND_TAG_:I = 0x0

.field static final CE_SPEC_PROC_TAG_:I = 0xb

.field private static final CE_TRAIL_SURROGATE_TAG_:I = 0x8

.field private static final DEBUG:Z

.field private static final FULL_ZERO_COMBINING_CLASS_FAST_LIMIT_:I = 0xc0

.field private static final HANGUL_LBASE_:I = 0x1100

.field private static final HANGUL_SBASE_:I = 0xac00

.field private static final HANGUL_TBASE_:I = 0x11a7

.field private static final HANGUL_TCOUNT_:I = 0x1c

.field private static final HANGUL_VBASE_:I = 0x1161

.field private static final HANGUL_VCOUNT_:I = 0x15

.field public static final IGNORABLE:I = 0x0

.field private static final LAST_BYTE_MASK_:I = 0xff

.field private static final LEAD_ZERO_COMBINING_CLASS_FAST_LIMIT_:I = 0x300

.field public static final NULLORDER:I = -0x1

.field private static final SECOND_LAST_BYTE_SHIFT_:I = 0x8


# instance fields
.field m_CEBufferOffset_:I

.field m_CEBufferSize_:I

.field private m_CEBuffer_:[I

.field private m_FCDLimit_:I

.field m_FCDStart_:I

.field private m_bufferOffset_:I

.field private m_buffer_:Ljava/lang/StringBuffer;

.field private m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

.field m_isCodePointHiragana_:Z

.field private m_isForwards_:Z

.field private m_source_:Lcom/ibm/icu/text/UCharacterIterator;

.field private m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

.field private m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

.field private m_utilSkippedBuffer_:Ljava/lang/StringBuffer;

.field private m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

.field private m_utilSpecialDiscontiguousBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

.field private m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

.field private m_utilStringBuffer_:Ljava/lang/StringBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 943
    const-string/jumbo v0, "collator"

    invoke-static {v0}, Lcom/ibm/icu/impl/ICUDebug;->enabled(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/ibm/icu/text/CollationElementIterator;->DEBUG:Z

    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/text/UCharacterIterator;Lcom/ibm/icu/text/RuleBasedCollator;)V
    .locals 2
    .param p1, "source"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p2, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;

    .prologue
    .line 641
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 642
    new-instance v0, Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-direct {v0}, Lcom/ibm/icu/impl/StringUCharacterIterator;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    .line 643
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    .line 644
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-virtual {p1}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/StringUCharacterIterator;->setText(Ljava/lang/String;)V

    .line 645
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    .line 646
    iput-object p2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    .line 647
    const/16 v0, 0x200

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    .line 648
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    .line 649
    new-instance v0, Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {v0}, Lcom/ibm/icu/text/CollationElementIterator$Backup;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    .line 650
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState()V

    .line 651
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/ibm/icu/text/RuleBasedCollator;)V
    .locals 1
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;

    .prologue
    .line 593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 594
    new-instance v0, Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-direct {v0, p1}, Lcom/ibm/icu/impl/StringUCharacterIterator;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    .line 595
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    .line 596
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    .line 597
    iput-object p2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    .line 598
    const/16 v0, 0x200

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    .line 599
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    .line 600
    new-instance v0, Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {v0}, Lcom/ibm/icu/text/CollationElementIterator$Backup;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    .line 601
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState()V

    .line 602
    return-void
.end method

.method constructor <init>(Ljava/text/CharacterIterator;Lcom/ibm/icu/text/RuleBasedCollator;)V
    .locals 1
    .param p1, "source"    # Ljava/text/CharacterIterator;
    .param p2, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;

    .prologue
    .line 617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 618
    new-instance v0, Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-direct {v0}, Lcom/ibm/icu/impl/StringUCharacterIterator;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    .line 619
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    .line 620
    new-instance v0, Lcom/ibm/icu/impl/CharacterIteratorWrapper;

    invoke-direct {v0, p1}, Lcom/ibm/icu/impl/CharacterIteratorWrapper;-><init>(Ljava/text/CharacterIterator;)V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    .line 621
    iput-object p2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    .line 622
    const/16 v0, 0x200

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    .line 623
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    .line 624
    new-instance v0, Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {v0}, Lcom/ibm/icu/text/CollationElementIterator$Backup;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    .line 625
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState()V

    .line 626
    return-void
.end method

.method private FCDCheck(CI)Z
    .locals 7
    .param p1, "ch"    # C
    .param p2, "offset"    # I

    .prologue
    .line 1057
    const/4 v4, 0x1

    .line 1061
    .local v4, "result":Z
    iput p2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    .line 1062
    iget-object v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v5, p2}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 1064
    invoke-static {p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v1

    .line 1065
    .local v1, "fcd":C
    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1066
    iget-object v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    .line 1067
    iget-object v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v5

    int-to-char p1, v5

    .line 1069
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1070
    invoke-static {v1, p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16FromSurrogatePair(CC)C

    move-result v1

    .line 1076
    :cond_0
    :goto_0
    and-int/lit16 v3, v1, 0xff

    .line 1078
    .local v3, "prevTrailCC":I
    if-eqz v3, :cond_1

    .line 1082
    :goto_1
    iget-object v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    .line 1083
    iget-object v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v0

    .line 1084
    .local v0, "ch_int":I
    const/4 v5, -0x1

    if-ne v0, v5, :cond_3

    .line 1112
    .end local v0    # "ch_int":I
    :cond_1
    iget-object v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v5

    iput v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    .line 1113
    iget-object v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    iget v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    invoke-virtual {v5, v6}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 1114
    iget-object v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    .line 1115
    return v4

    .line 1072
    .end local v3    # "prevTrailCC":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 1087
    .restart local v0    # "ch_int":I
    .restart local v3    # "prevTrailCC":I
    :cond_3
    int-to-char p1, v0

    .line 1089
    invoke-static {p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v1

    .line 1090
    if-eqz v1, :cond_4

    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1091
    iget-object v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    .line 1092
    iget-object v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v5

    int-to-char p1, v5

    .line 1093
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1094
    invoke-static {v1, p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16FromSurrogatePair(CC)C

    move-result v1

    .line 1099
    :cond_4
    :goto_2
    ushr-int/lit8 v2, v1, 0x8

    .line 1100
    .local v2, "leadCC":I
    if-eqz v2, :cond_1

    .line 1105
    if-ge v2, v3, :cond_5

    .line 1106
    const/4 v4, 0x0

    .line 1109
    :cond_5
    and-int/lit16 v3, v1, 0xff

    .line 1110
    goto :goto_1

    .line 1096
    .end local v2    # "leadCC":I
    :cond_6
    const/4 v1, 0x0

    goto :goto_2
.end method

.method private FCDCheckBackwards(CI)Z
    .locals 8
    .param p1, "ch"    # C
    .param p2, "offset"    # I

    .prologue
    .line 1209
    const/4 v3, 0x1

    .line 1210
    .local v3, "result":Z
    const/4 v0, 0x0

    .line 1211
    .local v0, "fcd":C
    add-int/lit8 v6, p2, 0x1

    iput v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    .line 1212
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v6, p2}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 1213
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1214
    invoke-static {p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v0

    .line 1232
    :cond_0
    :goto_0
    ushr-int/lit8 v1, v0, 0x8

    .line 1236
    .local v1, "leadCC":I
    :goto_1
    if-eqz v1, :cond_1

    .line 1237
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result p2

    .line 1238
    if-nez p2, :cond_4

    .line 1267
    :cond_1
    if-nez v0, :cond_a

    .line 1268
    iput p2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    .line 1273
    :goto_2
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    iget v7, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    invoke-virtual {v6, v7}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 1274
    return v3

    .line 1216
    .end local v1    # "leadCC":I
    :cond_2
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    if-lez v6, :cond_0

    .line 1218
    move v5, p1

    .line 1219
    .local v5, "trailch":C
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v6

    int-to-char p1, v6

    .line 1220
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1221
    invoke-static {p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v0

    .line 1222
    if-eqz v0, :cond_0

    .line 1223
    invoke-static {v0, v5}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16FromSurrogatePair(CC)C

    move-result v0

    .line 1225
    goto :goto_0

    .line 1228
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1241
    .end local v5    # "trailch":C
    .restart local v1    # "leadCC":I
    :cond_4
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v6

    int-to-char p1, v6

    .line 1242
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isSurrogate(C)Z

    move-result v6

    if-nez v6, :cond_7

    .line 1243
    invoke-static {p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v0

    .line 1258
    :cond_5
    :goto_3
    and-int/lit16 v2, v0, 0xff

    .line 1259
    .local v2, "prevTrailCC":I
    if-ge v1, v2, :cond_6

    .line 1260
    const/4 v3, 0x0

    .line 1262
    :cond_6
    ushr-int/lit8 v1, v0, 0x8

    .line 1263
    goto :goto_1

    .line 1245
    .end local v2    # "prevTrailCC":I
    :cond_7
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v6

    if-lez v6, :cond_9

    .line 1246
    move v4, p1

    .line 1247
    .local v4, "trail":C
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v6

    int-to-char p1, v6

    .line 1248
    invoke-static {p1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1249
    invoke-static {p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16(C)C

    move-result v0

    .line 1251
    :cond_8
    if-eqz v0, :cond_5

    .line 1252
    invoke-static {v0, v4}, Lcom/ibm/icu/impl/NormalizerImpl;->getFCD16FromSurrogatePair(CC)C

    move-result v0

    goto :goto_3

    .line 1256
    .end local v4    # "trail":C
    :cond_9
    const/4 v0, 0x0

    goto :goto_3

    .line 1271
    :cond_a
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v6

    iput v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    goto :goto_2
.end method

.method private backupInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V
    .locals 2
    .param p1, "backup"    # Lcom/ibm/icu/text/CollationElementIterator$Backup;

    .prologue
    .line 969
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v0

    iput v0, p1, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_offset_:I

    .line 970
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    iput v0, p1, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_FCDLimit_:I

    .line 971
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    iput v0, p1, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_FCDStart_:I

    .line 972
    iget-boolean v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_isCodePointHiragana_:Z

    iput-boolean v0, p1, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_isCodePointHiragana_:Z

    .line 973
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    iput v0, p1, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_bufferOffset_:I

    .line 974
    iget-object v0, p1, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_buffer_:Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 975
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-ltz v0, :cond_0

    .line 977
    sget-boolean v0, Lcom/ibm/icu/impl/ICUDebug;->isJDK14OrHigher:Z

    if-eqz v0, :cond_1

    .line 978
    iget-object v0, p1, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_buffer_:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 983
    :cond_0
    :goto_0
    return-void

    .line 980
    :cond_1
    iget-object v0, p1, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_buffer_:Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method private currentChar()I
    .locals 2

    .prologue
    .line 1532
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-gez v0, :cond_0

    .line 1533
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    .line 1534
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    move-result v0

    .line 1540
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v0

    goto :goto_0
.end method

.method private getCombiningClass(I)I
    .locals 2
    .param p1, "ch"    # I

    .prologue
    .line 1010
    const/16 v0, 0x300

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    int-to-char v1, p1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const v0, 0xffff

    if-le p1, v0, :cond_2

    .line 1012
    :cond_1
    invoke-static {p1}, Lcom/ibm/icu/impl/NormalizerImpl;->getCombiningClass(I)I

    move-result v0

    .line 1014
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getContractionOffset(Lcom/ibm/icu/text/RuleBasedCollator;I)I
    .locals 2
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ce"    # I

    .prologue
    .line 1406
    const v0, 0xffffff

    and-int/2addr v0, p2

    iget v1, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionOffset_:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private getExpansionCount(I)I
    .locals 1
    .param p1, "ce"    # I

    .prologue
    .line 1775
    and-int/lit8 v0, p1, 0xf

    return v0
.end method

.method private getExpansionOffset(Lcom/ibm/icu/text/RuleBasedCollator;I)I
    .locals 2
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ce"    # I

    .prologue
    .line 1394
    const v0, 0xfffff0

    and-int/2addr v0, p2

    shr-int/lit8 v0, v0, 0x4

    iget v1, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionOffset_:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private goBackOne()V
    .locals 2

    .prologue
    .line 2857
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-ltz v0, :cond_0

    .line 2858
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    .line 2863
    :goto_0
    return-void

    .line 2861
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v1}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    goto :goto_0
.end method

.method private goForwardOne()V
    .locals 2

    .prologue
    .line 2876
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-gez v0, :cond_0

    .line 2879
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    iget-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v1}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 2885
    :goto_0
    return-void

    .line 2883
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    goto :goto_0
.end method

.method private final isBackwardsStart()Z
    .locals 1

    .prologue
    .line 1337
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-nez v0, :cond_2

    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    if-gtz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isContractionTag(I)Z
    .locals 2
    .param p1, "ce"    # I

    .prologue
    .line 1495
    invoke-static {p1}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final isEnd()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1347
    iget v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-ltz v2, :cond_2

    .line 1348
    iget v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-eq v2, v3, :cond_1

    move v0, v1

    .line 1356
    :cond_0
    :goto_0
    return v0

    .line 1353
    :cond_1
    iget v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->getLength()I

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 1356
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v2}, Lcom/ibm/icu/text/UCharacterIterator;->getLength()I

    move-result v2

    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private isSpecialPrefixTag(I)Z
    .locals 2
    .param p1, "ce"    # I

    .prologue
    .line 1416
    invoke-static {p1}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private nextChar()I
    .locals 8

    .prologue
    const/16 v7, 0x300

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 1129
    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-gez v3, :cond_2

    .line 1132
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v1

    .line 1146
    .local v1, "result":I
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v2

    .line 1147
    .local v2, "startoffset":I
    const/16 v3, 0xc0

    if-lt v1, v3, :cond_0

    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/RuleBasedCollator;->getDecomposition()I

    move-result v3

    const/16 v4, 0x10

    if-eq v3, v4, :cond_0

    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-gez v3, :cond_0

    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    if-le v3, v2, :cond_4

    .line 1152
    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    .line 1174
    .end local v1    # "result":I
    .end local v2    # "startoffset":I
    :cond_1
    :goto_0
    return v1

    .line 1136
    :cond_2
    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-lt v3, v4, :cond_3

    .line 1139
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 1140
    iput v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    .line 1141
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1142
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->nextChar()I

    move-result v1

    goto :goto_0

    .line 1144
    :cond_3
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    goto :goto_0

    .line 1156
    .restart local v1    # "result":I
    .restart local v2    # "startoffset":I
    :cond_4
    if-ge v1, v7, :cond_5

    .line 1159
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    .line 1160
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v0

    .line 1161
    .local v0, "next":I
    if-eq v0, v5, :cond_1

    if-lt v0, v7, :cond_1

    .line 1169
    .end local v0    # "next":I
    :cond_5
    int-to-char v3, v1

    invoke-direct {p0, v3, v2}, Lcom/ibm/icu/text/CollationElementIterator;->FCDCheck(CI)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1170
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->normalize()V

    .line 1171
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    .line 1172
    const/4 v3, 0x1

    iput v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    goto :goto_0
.end method

.method private nextContraction(Lcom/ibm/icu/text/RuleBasedCollator;I)I
    .locals 12
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ce"    # I

    .prologue
    .line 1647
    iget-object v10, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {p0, v10}, Lcom/ibm/icu/text/CollationElementIterator;->backupInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 1648
    iget-object v10, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/CollationElementIterator;->getContractionOffset(Lcom/ibm/icu/text/RuleBasedCollator;I)I

    move-result v11

    aget v3, v10, v11

    .line 1650
    .local v3, "entryce":I
    :cond_0
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/CollationElementIterator;->getContractionOffset(Lcom/ibm/icu/text/RuleBasedCollator;I)I

    move-result v4

    .line 1651
    .local v4, "entryoffset":I
    move v8, v4

    .line 1653
    .local v8, "offset":I
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->isEnd()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1654
    iget-object v10, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget p2, v10, v8

    .line 1655
    const/high16 v10, -0x10000000

    if-ne p2, v10, :cond_1

    .line 1658
    move p2, v3

    .line 1659
    iget-object v10, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {p0, v10}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 1748
    :cond_1
    :goto_1
    return p2

    .line 1665
    :cond_2
    iget-object v10, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v10, v10, v8

    and-int/lit16 v5, v10, 0xff

    .line 1667
    .local v5, "maxCC":I
    iget-object v10, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v10, v10, v8

    shr-int/lit8 v10, v10, 0x8

    int-to-byte v0, v10

    .line 1668
    .local v0, "allSame":B
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->nextChar()I

    move-result v10

    int-to-char v1, v10

    .line 1669
    .local v1, "ch":C
    add-int/lit8 v8, v8, 0x1

    .line 1670
    :goto_2
    iget-object v10, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v10, v10, v8

    if-le v1, v10, :cond_3

    .line 1672
    add-int/lit8 v8, v8, 0x1

    .line 1673
    goto :goto_2

    .line 1675
    :cond_3
    iget-object v10, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v10, v10, v8

    if-ne v1, v10, :cond_4

    .line 1678
    iget-object v10, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget p2, v10, v8

    .line 1721
    :goto_3
    const/high16 v10, -0x10000000

    if-ne p2, v10, :cond_d

    .line 1723
    iget-object v10, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {p0, v10}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 1724
    move p2, v3

    .line 1725
    goto :goto_1

    .line 1683
    :cond_4
    move v6, v1

    .line 1684
    .local v6, "miss":I
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1686
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->nextChar()I

    move-result v10

    int-to-char v10, v10

    invoke-static {v1, v10}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v6

    .line 1689
    :cond_5
    if-eqz v5, :cond_7

    invoke-direct {p0, v6}, Lcom/ibm/icu/text/CollationElementIterator;->getCombiningClass(I)I

    move-result v9

    .local v9, "sCC":I
    if-eqz v9, :cond_7

    if-gt v9, v5, :cond_7

    if-eqz v0, :cond_6

    if-eq v9, v5, :cond_7

    :cond_6
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->isEnd()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1693
    .end local v9    # "sCC":I
    :cond_7
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    .line 1694
    const v10, 0xffff

    if-le v6, v10, :cond_8

    .line 1695
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    .line 1697
    :cond_8
    iget-object v10, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget p2, v10, v4

    .line 1698
    goto :goto_3

    .line 1702
    .restart local v9    # "sCC":I
    :cond_9
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->nextChar()I

    move-result v2

    .line 1703
    .local v2, "ch_int":I
    const/4 v10, -0x1

    if-eq v2, v10, :cond_a

    .line 1704
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    .line 1706
    :cond_a
    int-to-char v7, v2

    .line 1707
    .local v7, "nextch":C
    invoke-direct {p0, v7}, Lcom/ibm/icu/text/CollationElementIterator;->getCombiningClass(I)I

    move-result v10

    if-nez v10, :cond_c

    .line 1708
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    .line 1709
    const v10, 0xffff

    if-le v6, v10, :cond_b

    .line 1710
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    .line 1713
    :cond_b
    iget-object v10, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget p2, v10, v4

    .line 1714
    goto :goto_3

    .line 1716
    :cond_c
    invoke-direct {p0, p1, v4}, Lcom/ibm/icu/text/CollationElementIterator;->nextDiscontiguous(Lcom/ibm/icu/text/RuleBasedCollator;I)I

    move-result p2

    goto :goto_3

    .line 1729
    .end local v2    # "ch_int":I
    .end local v6    # "miss":I
    .end local v7    # "nextch":C
    .end local v9    # "sCC":I
    :cond_d
    invoke-direct {p0, p2}, Lcom/ibm/icu/text/CollationElementIterator;->isContractionTag(I)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1734
    iget-object v10, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget v10, v10, v4

    const/high16 v11, -0x10000000

    if-eq v10, v11, :cond_0

    .line 1738
    iget-object v10, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget v3, v10, v4

    .line 1739
    iget-object v10, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {p0, v10}, Lcom/ibm/icu/text/CollationElementIterator;->backupInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 1740
    iget-object v10, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    iget v10, v10, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_bufferOffset_:I

    if-ltz v10, :cond_e

    .line 1741
    iget-object v10, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    iget v11, v10, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_bufferOffset_:I

    add-int/lit8 v11, v11, -0x1

    iput v11, v10, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_bufferOffset_:I

    goto/16 :goto_0

    .line 1744
    :cond_e
    iget-object v10, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    iget v11, v10, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_offset_:I

    add-int/lit8 v11, v11, -0x1

    iput v11, v10, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_offset_:I

    goto/16 :goto_0
.end method

.method private nextDigit(Lcom/ibm/icu/text/RuleBasedCollator;II)I
    .locals 17
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ce"    # I
    .param p3, "cp"    # I

    .prologue
    .line 1830
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v13, v13, Lcom/ibm/icu/text/RuleBasedCollator;->m_isNumericCollation_:Z

    if-eqz v13, :cond_11

    .line 1831
    const/4 v3, 0x0

    .line 1832
    .local v3, "collateVal":I
    const/4 v12, 0x0

    .line 1833
    .local v12, "trailingZeroIndex":I
    const/4 v9, 0x0

    .line 1839
    .local v9, "nonZeroValReached":Z
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v14, 0x3

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1845
    invoke-static/range {p3 .. p3}, Lcom/ibm/icu/lang/UCharacter;->digit(I)I

    move-result v5

    .line 1852
    .local v5, "digVal":I
    const/4 v4, 0x1

    .line 1855
    .local v4, "digIndx":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    add-int/lit8 v13, v13, -0x2

    shl-int/lit8 v13, v13, 0x1

    if-lt v4, v13, :cond_1

    .line 1856
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->length()I

    move-result v14

    shl-int/lit8 v14, v14, 0x1

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1860
    :cond_1
    if-nez v5, :cond_2

    if-eqz v9, :cond_5

    .line 1861
    :cond_2
    if-eqz v5, :cond_3

    if-nez v9, :cond_3

    .line 1862
    const/4 v9, 0x1

    .line 1875
    :cond_3
    rem-int/lit8 v13, v4, 0x2

    const/4 v14, 0x1

    if-ne v13, v14, :cond_a

    .line 1876
    add-int/2addr v3, v5

    .line 1878
    if-nez v3, :cond_9

    if-nez v12, :cond_9

    .line 1879
    add-int/lit8 v13, v4, -0x1

    ushr-int/lit8 v13, v13, 0x1

    add-int/lit8 v12, v13, 0x2

    .line 1884
    :cond_4
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    add-int/lit8 v14, v4, -0x1

    ushr-int/lit8 v14, v14, 0x1

    add-int/lit8 v14, v14, 0x2

    shl-int/lit8 v15, v3, 0x1

    add-int/lit8 v15, v15, 0x6

    int-to-char v15, v15

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 1887
    const/4 v3, 0x0

    .line 1897
    :goto_1
    add-int/lit8 v4, v4, 0x1

    .line 1901
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationElementIterator;->isEnd()Z

    move-result v13

    if-nez v13, :cond_7

    .line 1902
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/ibm/icu/text/CollationElementIterator;->backupInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 1903
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationElementIterator;->nextChar()I

    move-result v2

    .line 1904
    .local v2, "char32":I
    int-to-char v1, v2

    .line 1905
    .local v1, "ch":C
    invoke-static {v1}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 1906
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationElementIterator;->isEnd()Z

    move-result v13

    if-nez v13, :cond_6

    .line 1907
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationElementIterator;->nextChar()I

    move-result v13

    int-to-char v11, v13

    .line 1908
    .local v11, "trail":C
    invoke-static {v11}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 1909
    invoke-static {v1, v11}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v2

    .line 1918
    .end local v11    # "trail":C
    :cond_6
    :goto_2
    invoke-static {v2}, Lcom/ibm/icu/lang/UCharacter;->digit(I)I

    move-result v5

    .line 1919
    const/4 v13, -0x1

    if-ne v5, v13, :cond_0

    .line 1923
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 1932
    .end local v1    # "ch":C
    .end local v2    # "char32":I
    :cond_7
    if-nez v9, :cond_8

    .line 1933
    const/4 v4, 0x2

    .line 1934
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v14, 0x2

    const/4 v15, 0x6

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 1937
    :cond_8
    if-eqz v12, :cond_c

    move v6, v12

    .line 1939
    .local v6, "endIndex":I
    :goto_3
    rem-int/lit8 v13, v4, 0x2

    if-eqz v13, :cond_e

    .line 1948
    const/4 v7, 0x2

    .local v7, "i":I
    :goto_4
    if-ge v7, v6, :cond_d

    .line 1949
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v14, v7}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v14

    add-int/lit8 v14, v14, -0x6

    ushr-int/lit8 v14, v14, 0x1

    rem-int/lit8 v14, v14, 0xa

    mul-int/lit8 v14, v14, 0xa

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    add-int/lit8 v16, v7, 0x1

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    add-int/lit8 v15, v15, -0x6

    ushr-int/lit8 v15, v15, 0x1

    div-int/lit8 v15, v15, 0xa

    add-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x1

    add-int/lit8 v14, v14, 0x6

    int-to-char v14, v14

    invoke-virtual {v13, v7, v14}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 1948
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 1881
    .end local v6    # "endIndex":I
    .end local v7    # "i":I
    :cond_9
    if-eqz v12, :cond_4

    .line 1882
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 1893
    :cond_a
    mul-int/lit8 v3, v5, 0xa

    .line 1894
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    ushr-int/lit8 v14, v4, 0x1

    add-int/lit8 v14, v14, 0x2

    shl-int/lit8 v15, v3, 0x1

    add-int/lit8 v15, v15, 0x6

    int-to-char v15, v15

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    goto/16 :goto_1

    .line 1913
    .restart local v1    # "ch":C
    .restart local v2    # "char32":I
    .restart local v11    # "trail":C
    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationElementIterator;->goBackOne()V

    goto :goto_2

    .line 1937
    .end local v1    # "ch":C
    .end local v2    # "char32":I
    .end local v11    # "trail":C
    :cond_c
    ushr-int/lit8 v13, v4, 0x1

    add-int/lit8 v6, v13, 0x2

    goto :goto_3

    .line 1955
    .restart local v6    # "endIndex":I
    .restart local v7    # "i":I
    :cond_d
    add-int/lit8 v4, v4, -0x1

    .line 1959
    .end local v7    # "i":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    add-int/lit8 v14, v6, -0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    add-int/lit8 v16, v6, -0x1

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    add-int/lit8 v15, v15, -0x1

    int-to-char v15, v15

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 1966
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v14, 0x0

    const/16 v15, 0x24

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 1967
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v14, 0x1

    ushr-int/lit8 v15, v4, 0x1

    and-int/lit8 v15, v15, 0x7f

    add-int/lit16 v15, v15, 0x80

    int-to-char v15, v15

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 1972
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    shl-int/lit8 v13, v13, 0x8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v14

    or-int/2addr v13, v14

    shl-int/lit8 v13, v13, 0x10

    or-int/lit16 v13, v13, 0x500

    or-int/lit8 p2, v13, 0x5

    .line 1980
    const/4 v7, 0x2

    .line 1982
    .restart local v7    # "i":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    const/4 v14, 0x0

    aput p2, v13, v14

    .line 1983
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 1984
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    move v8, v7

    .line 1985
    .end local v7    # "i":I
    .local v8, "i":I
    :goto_5
    if-ge v8, v6, :cond_10

    .line 1987
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "i":I
    .restart local v7    # "i":I
    invoke-virtual {v13, v8}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    shl-int/lit8 v10, v13, 0x8

    .line 1988
    .local v10, "primWeight":I
    if-ge v7, v6, :cond_f

    .line 1989
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "i":I
    .restart local v8    # "i":I
    invoke-virtual {v13, v7}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    or-int/2addr v10, v13

    move v7, v8

    .line 1991
    .end local v8    # "i":I
    .restart local v7    # "i":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v15, v14, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    shl-int/lit8 v15, v10, 0x10

    or-int/lit16 v15, v15, 0xc0

    aput v15, v13, v14

    move v8, v7

    .line 1994
    .end local v7    # "i":I
    .restart local v8    # "i":I
    goto :goto_5

    .end local v10    # "primWeight":I
    :cond_10
    move/from16 v13, p2

    .line 2001
    .end local v3    # "collateVal":I
    .end local v4    # "digIndx":I
    .end local v5    # "digVal":I
    .end local v6    # "endIndex":I
    .end local v8    # "i":I
    .end local v9    # "nonZeroValReached":Z
    .end local v12    # "trailingZeroIndex":I
    :goto_6
    return v13

    :cond_11
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    invoke-direct/range {p0 .. p2}, Lcom/ibm/icu/text/CollationElementIterator;->getExpansionOffset(Lcom/ibm/icu/text/RuleBasedCollator;I)I

    move-result v14

    aget v13, v13, v14

    goto :goto_6
.end method

.method private nextDiscontiguous(Lcom/ibm/icu/text/RuleBasedCollator;I)I
    .locals 11
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "entryoffset"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v9, -0x1

    const/high16 v8, -0x10000000

    .line 1554
    move v5, p2

    .line 1555
    .local v5, "offset":I
    const/4 v3, 0x0

    .line 1557
    .local v3, "multicontraction":Z
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSkippedBuffer_:Ljava/lang/StringBuffer;

    if-nez v6, :cond_4

    .line 1558
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSkippedBuffer_:Ljava/lang/StringBuffer;

    .line 1563
    :goto_0
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->currentChar()I

    move-result v6

    int-to-char v1, v6

    .line 1564
    .local v1, "ch":C
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSkippedBuffer_:Ljava/lang/StringBuffer;

    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->currentChar()I

    move-result v7

    int-to-char v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1566
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialDiscontiguousBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    if-nez v6, :cond_0

    .line 1567
    new-instance v6, Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {v6}, Lcom/ibm/icu/text/CollationElementIterator$Backup;-><init>()V

    iput-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialDiscontiguousBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    .line 1569
    :cond_0
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialDiscontiguousBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {p0, v6}, Lcom/ibm/icu/text/CollationElementIterator;->backupInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 1570
    move v4, v1

    .line 1572
    .local v4, "nextch":C
    :cond_1
    :goto_1
    move v1, v4

    .line 1573
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->nextChar()I

    move-result v2

    .line 1574
    .local v2, "ch_int":I
    int-to-char v4, v2

    .line 1575
    if-eq v2, v9, :cond_2

    invoke-direct {p0, v4}, Lcom/ibm/icu/text/CollationElementIterator;->getCombiningClass(I)I

    move-result v6

    if-nez v6, :cond_5

    .line 1580
    :cond_2
    if-eqz v3, :cond_7

    .line 1581
    if-eq v2, v9, :cond_3

    .line 1582
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    .line 1584
    :cond_3
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSkippedBuffer_:Ljava/lang/StringBuffer;

    invoke-direct {p0, v6}, Lcom/ibm/icu/text/CollationElementIterator;->setDiscontiguous(Ljava/lang/StringBuffer;)V

    .line 1585
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget v0, v6, v5

    .line 1636
    :goto_2
    return v0

    .line 1561
    .end local v1    # "ch":C
    .end local v2    # "ch_int":I
    .end local v4    # "nextch":C
    :cond_4
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSkippedBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v6, v10}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_0

    .line 1590
    .restart local v1    # "ch":C
    .restart local v2    # "ch_int":I
    .restart local v4    # "nextch":C
    :cond_5
    add-int/lit8 v5, v5, 0x1

    .line 1591
    :goto_3
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    array-length v6, v6

    if-ge v5, v6, :cond_6

    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v6, v6, v5

    if-le v4, v6, :cond_6

    .line 1593
    add-int/lit8 v5, v5, 0x1

    .line 1594
    goto :goto_3

    .line 1596
    :cond_6
    const/high16 v0, -0x10000000

    .line 1597
    .local v0, "ce":I
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    array-length v6, v6

    if-lt v5, v6, :cond_8

    .line 1632
    .end local v0    # "ce":I
    :cond_7
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialDiscontiguousBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {p0, v6}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 1635
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    .line 1636
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget v0, v6, p2

    goto :goto_2

    .line 1600
    .restart local v0    # "ce":I
    :cond_8
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v6, v6, v5

    if-ne v4, v6, :cond_9

    invoke-direct {p0, v4}, Lcom/ibm/icu/text/CollationElementIterator;->getCombiningClass(I)I

    move-result v6

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/CollationElementIterator;->getCombiningClass(I)I

    move-result v7

    if-ne v6, v7, :cond_c

    .line 1603
    :cond_9
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSkippedBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_a

    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSkippedBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v6, v10}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v6

    if-eq v6, v4, :cond_b

    iget v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-gez v6, :cond_b

    .line 1606
    :cond_a
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSkippedBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1608
    :cond_b
    move v5, p2

    .line 1609
    goto :goto_1

    .line 1612
    :cond_c
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget v0, v6, v5

    .line 1615
    if-eq v0, v8, :cond_7

    .line 1618
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/CollationElementIterator;->isContractionTag(I)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1620
    invoke-direct {p0, p1, v0}, Lcom/ibm/icu/text/CollationElementIterator;->getContractionOffset(Lcom/ibm/icu/text/RuleBasedCollator;I)I

    move-result v5

    .line 1621
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget v6, v6, v5

    if-eq v6, v8, :cond_1

    .line 1622
    const/4 v3, 0x1

    .line 1623
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialDiscontiguousBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {p0, v6}, Lcom/ibm/icu/text/CollationElementIterator;->backupInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    goto/16 :goto_1

    .line 1627
    :cond_d
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSkippedBuffer_:Ljava/lang/StringBuffer;

    invoke-direct {p0, v6}, Lcom/ibm/icu/text/CollationElementIterator;->setDiscontiguous(Ljava/lang/StringBuffer;)V

    goto :goto_2
.end method

.method private nextExpansion(Lcom/ibm/icu/text/RuleBasedCollator;I)I
    .locals 7
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ce"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1790
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/CollationElementIterator;->getExpansionOffset(Lcom/ibm/icu/text/RuleBasedCollator;I)I

    move-result v1

    .line 1791
    .local v1, "offset":I
    invoke-direct {p0, p2}, Lcom/ibm/icu/text/CollationElementIterator;->getExpansionCount(I)I

    move-result v2

    iput v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 1792
    iput v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 1793
    iget-object v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    aget v3, v3, v1

    aput v3, v2, v5

    .line 1794
    iget v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    if-eqz v2, :cond_0

    .line 1796
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    if-ge v0, v2, :cond_1

    .line 1797
    iget-object v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    add-int v4, v1, v0

    aget v3, v3, v4

    aput v3, v2, v0

    .line 1796
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1802
    .end local v0    # "i":I
    :cond_0
    iput v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 1803
    :goto_1
    iget-object v2, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    aget v2, v2, v1

    if-eqz v2, :cond_1

    .line 1804
    iget-object v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    iget-object v4, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    add-int/lit8 v1, v1, 0x1

    aget v4, v4, v1

    aput v4, v2, v3

    goto :goto_1

    .line 1810
    :cond_1
    iget v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    if-ne v2, v6, :cond_2

    .line 1811
    iput v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 1812
    iput v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 1814
    :cond_2
    iget-object v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    aget v2, v2, v5

    return v2
.end method

.method private nextHangul(Lcom/ibm/icu/text/RuleBasedCollator;C)I
    .locals 8
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ch"    # C

    .prologue
    const/16 v7, 0x11a7

    const/4 v3, 0x0

    .line 2054
    const v4, 0xac00

    sub-int v4, p2, v4

    int-to-char v0, v4

    .line 2059
    .local v0, "L":C
    rem-int/lit8 v4, v0, 0x1c

    int-to-char v1, v4

    .line 2060
    .local v1, "T":C
    div-int/lit8 v4, v0, 0x1c

    int-to-char v0, v4

    .line 2061
    rem-int/lit8 v4, v0, 0x15

    int-to-char v2, v4

    .line 2062
    .local v2, "V":C
    div-int/lit8 v4, v0, 0x15

    int-to-char v0, v4

    .line 2065
    add-int/lit16 v4, v0, 0x1100

    int-to-char v0, v4

    .line 2066
    add-int/lit16 v4, v2, 0x1161

    int-to-char v2, v4

    .line 2067
    add-int/lit16 v4, v1, 0x11a7

    int-to-char v1, v4

    .line 2071
    iput v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 2072
    iget-boolean v4, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_isJamoSpecial_:Z

    if-nez v4, :cond_1

    .line 2073
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v6, v0}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v6

    aput v6, v4, v5

    .line 2075
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v6, v2}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v6

    aput v6, v4, v5

    .line 2078
    if-eq v1, v7, :cond_0

    .line 2079
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v6, v1}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v6

    aput v6, v4, v5

    .line 2082
    :cond_0
    const/4 v4, 0x1

    iput v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 2083
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    aget v3, v4, v3

    .line 2100
    :goto_0
    return v3

    .line 2091
    :cond_1
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2092
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2093
    if-eq v1, v7, :cond_2

    .line 2094
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2096
    :cond_2
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v4

    iput v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    .line 2097
    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    goto :goto_0
.end method

.method private nextImplicit(I)I
    .locals 5
    .param p1, "codepoint"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2011
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->isLegal(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2022
    :goto_0
    return v1

    .line 2016
    :cond_0
    sget-object v2, Lcom/ibm/icu/text/RuleBasedCollator;->impCEGen_:Lcom/ibm/icu/impl/ImplicitCEGenerator;

    invoke-virtual {v2, p1}, Lcom/ibm/icu/impl/ImplicitCEGenerator;->getImplicitFromCodePoint(I)I

    move-result v0

    .line 2017
    .local v0, "result":I
    iget-object v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    const/high16 v3, -0x10000

    and-int/2addr v3, v0

    or-int/lit16 v3, v3, 0x505

    aput v3, v2, v1

    .line 2019
    iget-object v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    const v3, 0xffff

    and-int/2addr v3, v0

    shl-int/lit8 v3, v3, 0x10

    or-int/lit16 v3, v3, 0xc0

    aput v3, v2, v4

    .line 2020
    iput v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 2021
    const/4 v2, 0x2

    iput v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 2022
    iget-object v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    aget v1, v2, v1

    goto :goto_0
.end method

.method private nextLongPrimary(I)I
    .locals 4
    .param p1, "ce"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1759
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    and-int/lit16 v1, p1, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/lit16 v1, v1, 0xc0

    aput v1, v0, v3

    .line 1761
    iput v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 1762
    const/4 v0, 0x2

    iput v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 1763
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    const v1, 0xffff00

    and-int/2addr v1, p1

    shl-int/lit8 v1, v1, 0x8

    or-int/lit16 v1, v1, 0x500

    or-int/lit8 v1, v1, 0x5

    aput v1, v0, v2

    .line 1765
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    aget v0, v0, v2

    return v0
.end method

.method private nextSpecial(Lcom/ibm/icu/text/RuleBasedCollator;IC)I
    .locals 5
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ce"    # I
    .param p3, "ch"    # C

    .prologue
    const/4 v3, 0x0

    .line 2113
    move v0, p3

    .line 2114
    .local v0, "codepoint":I
    iget-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    .line 2116
    .local v1, "entrybackup":Lcom/ibm/icu/text/CollationElementIterator$Backup;
    if-eqz v1, :cond_1

    .line 2117
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    .line 2122
    :goto_0
    invoke-direct {p0, v1}, Lcom/ibm/icu/text/CollationElementIterator;->backupInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 2127
    :cond_0
    :try_start_0
    invoke-static {p2}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 2172
    :pswitch_0
    const/4 p2, 0x0

    .line 2175
    :goto_1
    invoke-static {p2}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 2181
    iput-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    move v3, p2

    .line 2183
    :goto_2
    return v3

    .line 2120
    :cond_1
    new-instance v1, Lcom/ibm/icu/text/CollationElementIterator$Backup;

    .end local v1    # "entrybackup":Lcom/ibm/icu/text/CollationElementIterator$Backup;
    invoke-direct {v1}, Lcom/ibm/icu/text/CollationElementIterator$Backup;-><init>()V

    .restart local v1    # "entrybackup":Lcom/ibm/icu/text/CollationElementIterator$Backup;
    goto :goto_0

    .line 2181
    :pswitch_1
    iput-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    move v3, p2

    .line 2182
    goto :goto_2

    .line 2132
    :pswitch_2
    :try_start_1
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->isEnd()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_2

    .line 2181
    iput-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    goto :goto_2

    .line 2135
    :cond_2
    :try_start_2
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {p0, v4}, Lcom/ibm/icu/text/CollationElementIterator;->backupInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 2136
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->nextChar()I

    move-result v4

    int-to-char v2, v4

    .line 2137
    .local v2, "trail":C
    invoke-direct {p0, p1, p2, v2}, Lcom/ibm/icu/text/CollationElementIterator;->nextSurrogate(Lcom/ibm/icu/text/RuleBasedCollator;IC)I

    move-result p2

    .line 2140
    invoke-static {p3, v2}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v0

    .line 2142
    goto :goto_1

    .line 2144
    .end local v2    # "trail":C
    :pswitch_3
    invoke-direct {p0, p1, p2, v1}, Lcom/ibm/icu/text/CollationElementIterator;->nextSpecialPrefix(Lcom/ibm/icu/text/RuleBasedCollator;ILcom/ibm/icu/text/CollationElementIterator$Backup;)I

    move-result p2

    .line 2145
    goto :goto_1

    .line 2147
    :pswitch_4
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/CollationElementIterator;->nextContraction(Lcom/ibm/icu/text/RuleBasedCollator;I)I

    move-result p2

    .line 2148
    goto :goto_1

    .line 2150
    :pswitch_5
    invoke-direct {p0, p2}, Lcom/ibm/icu/text/CollationElementIterator;->nextLongPrimary(I)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    .line 2181
    iput-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    goto :goto_2

    .line 2152
    :pswitch_6
    :try_start_3
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/CollationElementIterator;->nextExpansion(Lcom/ibm/icu/text/RuleBasedCollator;I)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v3

    .line 2181
    iput-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    goto :goto_2

    .line 2154
    :pswitch_7
    :try_start_4
    invoke-direct {p0, p1, p2, v0}, Lcom/ibm/icu/text/CollationElementIterator;->nextDigit(Lcom/ibm/icu/text/RuleBasedCollator;II)I

    move-result p2

    .line 2155
    goto :goto_1

    .line 2159
    :pswitch_8
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/CollationElementIterator;->nextImplicit(I)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v3

    .line 2181
    iput-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    goto :goto_2

    .line 2161
    :pswitch_9
    :try_start_5
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/CollationElementIterator;->nextImplicit(I)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v3

    .line 2181
    iput-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    goto :goto_2

    :pswitch_a
    iput-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    goto :goto_2

    .line 2165
    :pswitch_b
    :try_start_6
    invoke-direct {p0, p3}, Lcom/ibm/icu/text/CollationElementIterator;->nextSurrogate(C)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v3

    .line 2181
    iput-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    goto :goto_2

    .line 2167
    :pswitch_c
    :try_start_7
    invoke-direct {p0, p1, p3}, Lcom/ibm/icu/text/CollationElementIterator;->nextHangul(Lcom/ibm/icu/text/RuleBasedCollator;C)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result v3

    .line 2181
    iput-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    goto :goto_2

    .line 2170
    :pswitch_d
    const/high16 v3, -0x10000000

    .line 2181
    iput-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    goto :goto_2

    :catchall_0
    move-exception v3

    iput-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialEntryBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    .line 2182
    throw v3

    .line 2127
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_6
        :pswitch_4
        :pswitch_0
        :pswitch_d
        :pswitch_2
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_8
        :pswitch_9
        :pswitch_3
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method private nextSpecialPrefix(Lcom/ibm/icu/text/RuleBasedCollator;ILcom/ibm/icu/text/CollationElementIterator$Backup;)I
    .locals 4
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ce"    # I
    .param p3, "entrybackup"    # Lcom/ibm/icu/text/CollationElementIterator$Backup;

    .prologue
    .line 1436
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {p0, v3}, Lcom/ibm/icu/text/CollationElementIterator;->backupInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 1437
    invoke-direct {p0, p3}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 1438
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    .line 1446
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/CollationElementIterator;->getContractionOffset(Lcom/ibm/icu/text/RuleBasedCollator;I)I

    move-result v0

    .line 1447
    .local v0, "entryoffset":I
    move v1, v0

    .line 1448
    .local v1, "offset":I
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->isBackwardsStart()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1449
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget p2, v3, v1

    .line 1477
    :goto_0
    const/high16 v3, -0x10000000

    if-eq p2, v3, :cond_4

    .line 1479
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {p0, v3}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 1485
    :goto_1
    return p2

    .line 1452
    :cond_1
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    move-result v3

    int-to-char v2, v3

    .line 1453
    .local v2, "previous":C
    :goto_2
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v3, v3, v1

    if-le v2, v3, :cond_2

    .line 1455
    add-int/lit8 v1, v1, 0x1

    .line 1456
    goto :goto_2

    .line 1458
    :cond_2
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v3, v3, v1

    if-ne v2, v3, :cond_3

    .line 1461
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget p2, v3, v1

    .line 1468
    :goto_3
    invoke-direct {p0, p2}, Lcom/ibm/icu/text/CollationElementIterator;->isSpecialPrefixTag(I)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    .line 1465
    :cond_3
    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget p2, v3, v0

    goto :goto_3

    .line 1483
    .end local v2    # "previous":C
    :cond_4
    invoke-direct {p0, p3}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    goto :goto_1
.end method

.method private nextSurrogate(C)I
    .locals 5
    .param p1, "ch"    # C

    .prologue
    const v4, 0xffff

    .line 2032
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->nextChar()I

    move-result v0

    .line 2033
    .local v0, "ch_int":I
    int-to-char v2, v0

    .line 2034
    .local v2, "nextch":C
    if-eq v0, v4, :cond_0

    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2036
    invoke-static {p1, v2}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v1

    .line 2037
    .local v1, "codepoint":I
    invoke-direct {p0, v1}, Lcom/ibm/icu/text/CollationElementIterator;->nextImplicit(I)I

    move-result v3

    .line 2042
    .end local v1    # "codepoint":I
    :goto_0
    return v3

    .line 2039
    :cond_0
    if-eq v2, v4, :cond_1

    .line 2040
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    .line 2042
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private final nextSurrogate(Lcom/ibm/icu/text/RuleBasedCollator;IC)I
    .locals 2
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ce"    # I
    .param p3, "trail"    # C

    .prologue
    .line 1373
    invoke-static {p3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1374
    iget-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 1375
    const/4 v0, 0x0

    .line 1383
    :cond_0
    :goto_0
    return v0

    .line 1379
    :cond_1
    iget-object v1, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v1, p2, p3}, Lcom/ibm/icu/impl/IntTrie;->getTrailValue(IC)I

    move-result v0

    .line 1380
    .local v0, "result":I
    const/high16 v1, -0x10000000

    if-ne v0, v1, :cond_0

    .line 1381
    iget-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    goto :goto_0
.end method

.method private normalize()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1026
    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    sub-int v2, v3, v4

    .line 1027
    .local v2, "size":I
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1028
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 1029
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 1030
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    move-result v4

    int-to-char v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1029
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1032
    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v5}, Lcom/ibm/icu/text/Normalizer;->decompose(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1033
    .local v0, "decomp":Ljava/lang/String;
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1034
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1035
    iput v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    .line 1036
    return-void
.end method

.method private normalizeBackwards()V
    .locals 1

    .prologue
    .line 1185
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->normalize()V

    .line 1186
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    .line 1187
    return-void
.end method

.method private previousChar()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, -0x1

    .line 1285
    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-ltz v3, :cond_3

    .line 1286
    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    .line 1287
    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-ltz v3, :cond_1

    .line 1288
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    .line 1328
    :cond_0
    :goto_0
    return v1

    .line 1292
    :cond_1
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1293
    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    if-nez v3, :cond_2

    .line 1294
    iput v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    .line 1295
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    goto :goto_0

    .line 1299
    :cond_2
    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    iput v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    .line 1300
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 1301
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    move-result v1

    goto :goto_0

    .line 1305
    :cond_3
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v1

    .line 1306
    .local v1, "result":I
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v2

    .line 1307
    .local v2, "startoffset":I
    const/16 v3, 0x300

    if-lt v1, v3, :cond_0

    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/RuleBasedCollator;->getDecomposition()I

    move-result v3

    const/16 v4, 0x10

    if-eq v3, v4, :cond_0

    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    if-le v3, v2, :cond_0

    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v3

    if-eqz v3, :cond_0

    .line 1312
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v0

    .line 1313
    .local v0, "ch":I
    const/16 v3, 0xc0

    if-ge v0, v3, :cond_4

    .line 1315
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->next()I

    goto :goto_0

    .line 1319
    :cond_4
    int-to-char v3, v1

    invoke-direct {p0, v3, v2}, Lcom/ibm/icu/text/CollationElementIterator;->FCDCheckBackwards(CI)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1320
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->normalizeBackwards()V

    .line 1321
    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    .line 1322
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    .line 1323
    goto :goto_0

    .line 1326
    :cond_5
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3, v2}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    goto :goto_0
.end method

.method private previousContraction(Lcom/ibm/icu/text/RuleBasedCollator;IC)I
    .locals 10
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ce"    # I
    .param p3, "ch"    # C

    .prologue
    const/4 v5, -0x1

    const/4 v8, 0x0

    .line 2293
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 2296
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    move-result v6

    int-to-char v3, v6

    .line 2297
    .local v3, "prevch":C
    const/4 v0, 0x0

    .line 2300
    .local v0, "atStart":Z
    :goto_0
    invoke-virtual {p1, p3}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2301
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v6, v8, p3}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 2302
    move p3, v3

    .line 2303
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->isBackwardsStart()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2304
    const/4 v0, 0x1

    .line 2309
    :cond_0
    if-nez v0, :cond_1

    .line 2311
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->nextChar()I

    .line 2314
    :cond_1
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v6, v8, p3}, Ljava/lang/StringBuffer;->insert(IC)Ljava/lang/StringBuffer;

    .line 2320
    invoke-virtual {p1}, Lcom/ibm/icu/text/RuleBasedCollator;->getDecomposition()I

    move-result v2

    .line 2322
    .local v2, "originaldecomp":I
    const/16 v6, 0x10

    invoke-virtual {p1, v6}, Lcom/ibm/icu/text/RuleBasedCollator;->setDecomposition(I)V

    .line 2323
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    if-nez v6, :cond_4

    .line 2324
    new-instance v6, Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v7, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, p1}, Lcom/ibm/icu/text/CollationElementIterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/RuleBasedCollator;)V

    iput-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    .line 2332
    :goto_1
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result p2

    .line 2333
    iput v8, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 2334
    :goto_2
    if-eq p2, v5, :cond_6

    .line 2335
    iget v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    iget-object v7, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    array-length v7, v7

    if-ne v6, v7, :cond_2

    .line 2338
    :try_start_0
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    array-length v6, v6

    add-int/lit8 v6, v6, 0x32

    new-array v4, v6, [I

    .line 2339
    .local v4, "tempbuffer":[I
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    array-length v9, v9

    invoke-static {v6, v7, v4, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2341
    iput-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2354
    .end local v4    # "tempbuffer":[I
    :cond_2
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v7, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    aput p2, v6, v7

    .line 2355
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    invoke-virtual {v6}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    move-result p2

    .line 2356
    goto :goto_2

    .line 2307
    .end local v2    # "originaldecomp":I
    :cond_3
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    move-result v6

    int-to-char v3, v6

    .line 2308
    goto :goto_0

    .line 2329
    .restart local v2    # "originaldecomp":I
    :cond_4
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iput-object p1, v6, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    .line 2330
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilColEIter_:Lcom/ibm/icu/text/CollationElementIterator;

    iget-object v7, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/ibm/icu/text/CollationElementIterator;->setText(Ljava/lang/String;)V

    goto :goto_1

    .line 2343
    :catch_0
    move-exception v1

    .line 2345
    .local v1, "e":Ljava/util/MissingResourceException;
    throw v1

    .line 2347
    .end local v1    # "e":Ljava/util/MissingResourceException;
    :catch_1
    move-exception v1

    .line 2348
    .local v1, "e":Ljava/lang/Exception;
    sget-boolean v6, Lcom/ibm/icu/text/CollationElementIterator;->DEBUG:Z

    if-eqz v6, :cond_5

    .line 2349
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 2359
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_3
    return v5

    .line 2357
    :cond_6
    invoke-virtual {p1, v2}, Lcom/ibm/icu/text/RuleBasedCollator;->setDecomposition(I)V

    .line 2358
    iget v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 2359
    iget-object v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    aget v5, v5, v6

    goto :goto_3
.end method

.method private previousDigit(Lcom/ibm/icu/text/RuleBasedCollator;IC)I
    .locals 17
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ce"    # I
    .param p3, "ch"    # C

    .prologue
    .line 2419
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v12, v12, Lcom/ibm/icu/text/RuleBasedCollator;->m_isNumericCollation_:Z

    if-eqz v12, :cond_13

    .line 2420
    const/4 v9, 0x0

    .line 2421
    .local v9, "leadingZeroIndex":I
    const/4 v2, 0x0

    .line 2422
    .local v2, "collateVal":I
    const/4 v10, 0x0

    .line 2425
    .local v10, "nonZeroValReached":Z
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v13, 0x3

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 2430
    move/from16 v1, p3

    .line 2431
    .local v1, "char32":I
    invoke-static/range {p3 .. p3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 2432
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationElementIterator;->isBackwardsStart()Z

    move-result v12

    if-nez v12, :cond_0

    .line 2433
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    move-result v12

    int-to-char v8, v12

    .line 2434
    .local v8, "lead":C
    invoke-static {v8}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 2435
    move/from16 v0, p3

    invoke-static {v8, v0}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v1

    .line 2443
    .end local v8    # "lead":C
    :cond_0
    :goto_0
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->digit(I)I

    move-result v4

    .line 2444
    .local v4, "digVal":I
    const/4 v3, 0x0

    .line 2447
    .local v3, "digIndx":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x2

    shl-int/lit8 v12, v12, 0x1

    if-lt v3, v12, :cond_2

    .line 2448
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    shl-int/lit8 v13, v13, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 2452
    :cond_2
    if-nez v4, :cond_3

    if-eqz v10, :cond_6

    .line 2453
    :cond_3
    if-eqz v4, :cond_4

    if-nez v10, :cond_4

    .line 2454
    const/4 v10, 0x1

    .line 2471
    :cond_4
    rem-int/lit8 v12, v3, 0x2

    const/4 v13, 0x1

    if-ne v12, v13, :cond_e

    .line 2472
    mul-int/lit8 v12, v4, 0xa

    add-int/2addr v2, v12

    .line 2475
    if-nez v2, :cond_d

    if-nez v9, :cond_d

    .line 2476
    add-int/lit8 v12, v3, -0x1

    ushr-int/lit8 v12, v12, 0x1

    add-int/lit8 v9, v12, 0x2

    .line 2482
    :cond_5
    :goto_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    add-int/lit8 v13, v3, -0x1

    ushr-int/lit8 v13, v13, 0x1

    add-int/lit8 v13, v13, 0x2

    shl-int/lit8 v14, v2, 0x1

    add-int/lit8 v14, v14, 0x6

    int-to-char v14, v14

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 2484
    const/4 v2, 0x0

    .line 2490
    :cond_6
    :goto_2
    add-int/lit8 v3, v3, 0x1

    .line 2492
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationElementIterator;->isBackwardsStart()Z

    move-result v12

    if-nez v12, :cond_8

    .line 2493
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/ibm/icu/text/CollationElementIterator;->backupInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 2494
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    move-result v1

    .line 2496
    invoke-static/range {p3 .. p3}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2497
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationElementIterator;->isBackwardsStart()Z

    move-result v12

    if-nez v12, :cond_7

    .line 2498
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    move-result v12

    int-to-char v8, v12

    .line 2499
    .restart local v8    # "lead":C
    invoke-static {v8}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v12

    if-eqz v12, :cond_f

    .line 2500
    move/from16 v0, p3

    invoke-static {v8, v0}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v1

    .line 2510
    .end local v8    # "lead":C
    :cond_7
    :goto_3
    invoke-static {v1}, Lcom/ibm/icu/lang/UCharacter;->digit(I)I

    move-result v4

    .line 2511
    const/4 v12, -0x1

    if-ne v4, v12, :cond_1

    .line 2512
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 2521
    :cond_8
    if-nez v10, :cond_9

    .line 2522
    const/4 v3, 0x2

    .line 2523
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v13, 0x2

    const/4 v14, 0x6

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 2526
    :cond_9
    rem-int/lit8 v12, v3, 0x2

    if-eqz v12, :cond_a

    .line 2527
    if-nez v2, :cond_10

    if-nez v9, :cond_10

    .line 2530
    add-int/lit8 v12, v3, -0x1

    ushr-int/lit8 v12, v12, 0x1

    add-int/lit8 v9, v12, 0x2

    .line 2540
    :cond_a
    :goto_4
    if-eqz v9, :cond_11

    move v5, v9

    .line 2542
    .local v5, "endIndex":I
    :goto_5
    add-int/lit8 v12, v5, -0x2

    shl-int/lit8 v12, v12, 0x1

    add-int/lit8 v3, v12, 0x1

    .line 2545
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v13, 0x2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v15, 0x2

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v14

    add-int/lit8 v14, v14, -0x1

    int-to-char v14, v14

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 2551
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v13, 0x0

    const/16 v14, 0x24

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 2552
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v13, 0x1

    ushr-int/lit8 v14, v3, 0x1

    and-int/lit8 v14, v14, 0x7f

    add-int/lit16 v14, v14, 0x80

    int-to-char v14, v14

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 2558
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 2559
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v14, v13, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v14

    shl-int/lit8 v14, v14, 0x8

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v15

    or-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x10

    or-int/lit16 v14, v14, 0x500

    or-int/lit8 v14, v14, 0x5

    aput v14, v12, v13

    .line 2569
    add-int/lit8 v6, v5, -0x1

    .local v6, "i":I
    move v7, v6

    .line 2570
    .end local v6    # "i":I
    .local v7, "i":I
    :goto_6
    const/4 v12, 0x2

    if-lt v7, v12, :cond_12

    .line 2571
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    add-int/lit8 v6, v7, -0x1

    .end local v7    # "i":I
    .restart local v6    # "i":I
    invoke-virtual {v12, v7}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v12

    shl-int/lit8 v11, v12, 0x8

    .line 2572
    .local v11, "primWeight":I
    const/4 v12, 0x2

    if-lt v6, v12, :cond_b

    .line 2573
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    add-int/lit8 v7, v6, -0x1

    .end local v6    # "i":I
    .restart local v7    # "i":I
    invoke-virtual {v12, v6}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v12

    or-int/2addr v11, v12

    move v6, v7

    .line 2575
    .end local v7    # "i":I
    .restart local v6    # "i":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v14, v13, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    shl-int/lit8 v14, v11, 0x10

    or-int/lit16 v14, v14, 0xc0

    aput v14, v12, v13

    move v7, v6

    .line 2578
    .end local v6    # "i":I
    .restart local v7    # "i":I
    goto :goto_6

    .line 2439
    .end local v3    # "digIndx":I
    .end local v4    # "digVal":I
    .end local v5    # "endIndex":I
    .end local v7    # "i":I
    .end local v11    # "primWeight":I
    .restart local v8    # "lead":C
    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/CollationElementIterator;->goForwardOne()V

    goto/16 :goto_0

    .line 2478
    .end local v8    # "lead":C
    .restart local v3    # "digIndx":I
    .restart local v4    # "digVal":I
    :cond_d
    if-eqz v9, :cond_5

    .line 2479
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 2487
    :cond_e
    move v2, v4

    goto/16 :goto_2

    .line 2505
    .restart local v8    # "lead":C
    :cond_f
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    goto/16 :goto_3

    .line 2534
    .end local v8    # "lead":C
    :cond_10
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilStringBuffer_:Ljava/lang/StringBuffer;

    ushr-int/lit8 v13, v3, 0x1

    add-int/lit8 v13, v13, 0x2

    shl-int/lit8 v14, v2, 0x1

    add-int/lit8 v14, v14, 0x6

    int-to-char v14, v14

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 2536
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    .line 2540
    :cond_11
    ushr-int/lit8 v12, v3, 0x1

    add-int/lit8 v5, v12, 0x2

    goto/16 :goto_5

    .line 2579
    .restart local v5    # "endIndex":I
    .restart local v7    # "i":I
    :cond_12
    move-object/from16 v0, p0

    iget v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v12, v12, -0x1

    move-object/from16 v0, p0

    iput v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 2580
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    aget v12, v12, v13

    .line 2583
    .end local v1    # "char32":I
    .end local v2    # "collateVal":I
    .end local v3    # "digIndx":I
    .end local v4    # "digVal":I
    .end local v5    # "endIndex":I
    .end local v7    # "i":I
    .end local v9    # "leadingZeroIndex":I
    .end local v10    # "nonZeroValReached":Z
    :goto_7
    return v12

    :cond_13
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    invoke-direct/range {p0 .. p2}, Lcom/ibm/icu/text/CollationElementIterator;->getExpansionOffset(Lcom/ibm/icu/text/RuleBasedCollator;I)I

    move-result v13

    aget v12, v12, v13

    goto :goto_7
.end method

.method private previousExpansion(Lcom/ibm/icu/text/RuleBasedCollator;I)I
    .locals 6
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ce"    # I

    .prologue
    .line 2387
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/CollationElementIterator;->getExpansionOffset(Lcom/ibm/icu/text/RuleBasedCollator;I)I

    move-result v1

    .line 2388
    .local v1, "offset":I
    invoke-direct {p0, p2}, Lcom/ibm/icu/text/CollationElementIterator;->getExpansionCount(I)I

    move-result v2

    iput v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 2389
    iget v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    if-eqz v2, :cond_0

    .line 2391
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    if-ge v0, v2, :cond_1

    .line 2392
    iget-object v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget-object v3, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    add-int v4, v1, v0

    aget v3, v3, v4

    aput v3, v2, v0

    .line 2391
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2398
    .end local v0    # "i":I
    :cond_0
    :goto_1
    iget-object v2, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/2addr v3, v1

    aget v2, v2, v3

    if-eqz v2, :cond_1

    .line 2399
    iget-object v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    iget-object v4, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansion_:[I

    iget v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/2addr v5, v1

    aget v4, v4, v5

    aput v4, v2, v3

    .line 2401
    iget v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    goto :goto_1

    .line 2404
    :cond_1
    iget v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 2405
    iget-object v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    aget v2, v2, v3

    return v2
.end method

.method private previousHangul(Lcom/ibm/icu/text/RuleBasedCollator;C)I
    .locals 7
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ch"    # C

    .prologue
    const/16 v6, 0x11a7

    const/4 v3, 0x0

    .line 2595
    const v4, 0xac00

    sub-int v4, p2, v4

    int-to-char v0, v4

    .line 2598
    .local v0, "L":C
    rem-int/lit8 v4, v0, 0x1c

    int-to-char v1, v4

    .line 2599
    .local v1, "T":C
    div-int/lit8 v4, v0, 0x1c

    int-to-char v0, v4

    .line 2600
    rem-int/lit8 v4, v0, 0x15

    int-to-char v2, v4

    .line 2601
    .local v2, "V":C
    div-int/lit8 v4, v0, 0x15

    int-to-char v0, v4

    .line 2604
    add-int/lit16 v4, v0, 0x1100

    int-to-char v0, v4

    .line 2605
    add-int/lit16 v4, v2, 0x1161

    int-to-char v2, v4

    .line 2606
    add-int/lit16 v4, v1, 0x11a7

    int-to-char v1, v4

    .line 2608
    iput v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 2609
    iget-boolean v4, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_isJamoSpecial_:Z

    if-nez v4, :cond_1

    .line 2610
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    iget-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v5, v0}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v5

    aput v5, v3, v4

    .line 2612
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    iget-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v5, v2}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v5

    aput v5, v3, v4

    .line 2614
    if-eq v1, v6, :cond_0

    .line 2615
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    iget-object v5, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v5, v1}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v5

    aput v5, v3, v4

    .line 2618
    :cond_0
    iget v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 2619
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    aget v3, v3, v4

    .line 2633
    :goto_0
    return v3

    .line 2625
    :cond_1
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2626
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2627
    if-eq v1, v6, :cond_2

    .line 2628
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2631
    :cond_2
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v4

    iput v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    .line 2632
    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    goto :goto_0
.end method

.method private previousImplicit(I)I
    .locals 5
    .param p1, "codepoint"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 2644
    invoke-static {p1}, Lcom/ibm/icu/lang/UCharacter;->isLegal(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2653
    :goto_0
    return v1

    .line 2647
    :cond_0
    sget-object v2, Lcom/ibm/icu/text/RuleBasedCollator;->impCEGen_:Lcom/ibm/icu/impl/ImplicitCEGenerator;

    invoke-virtual {v2, p1}, Lcom/ibm/icu/impl/ImplicitCEGenerator;->getImplicitFromCodePoint(I)I

    move-result v0

    .line 2648
    .local v0, "result":I
    const/4 v2, 0x2

    iput v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 2649
    iput v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 2650
    iget-object v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    const/high16 v3, -0x10000

    and-int/2addr v3, v0

    or-int/lit16 v3, v3, 0x505

    aput v3, v2, v1

    .line 2652
    iget-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    const v2, 0xffff

    and-int/2addr v2, v0

    shl-int/lit8 v2, v2, 0x10

    or-int/lit16 v2, v2, 0xc0

    aput v2, v1, v4

    .line 2653
    iget-object v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    aget v1, v1, v4

    goto :goto_0
.end method

.method private previousLongPrimary(I)I
    .locals 3
    .param p1, "ce"    # I

    .prologue
    .line 2369
    const/4 v0, 0x0

    iput v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 2370
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    const v2, 0xffff00

    and-int/2addr v2, p1

    shl-int/lit8 v2, v2, 0x8

    or-int/lit16 v2, v2, 0x500

    or-int/lit8 v2, v2, 0x5

    aput v2, v0, v1

    .line 2372
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    and-int/lit16 v2, p1, 0xff

    shl-int/lit8 v2, v2, 0x18

    or-int/lit16 v2, v2, 0xc0

    aput v2, v0, v1

    .line 2374
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 2375
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    aget v0, v0, v1

    return v0
.end method

.method private previousSpecial(Lcom/ibm/icu/text/RuleBasedCollator;IC)I
    .locals 3
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ce"    # I
    .param p3, "ch"    # C

    .prologue
    const/4 v0, 0x0

    .line 2691
    :cond_0
    invoke-static {p2}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2732
    :pswitch_0
    const/4 p2, 0x0

    .line 2734
    :goto_0
    invoke-static {p2}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v1

    if-nez v1, :cond_0

    move v0, p2

    .line 2738
    :goto_1
    :pswitch_1
    return v0

    :pswitch_2
    move v0, p2

    .line 2693
    goto :goto_1

    .line 2699
    :pswitch_3
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/CollationElementIterator;->previousSpecialPrefix(Lcom/ibm/icu/text/RuleBasedCollator;I)I

    move-result p2

    .line 2700
    goto :goto_0

    .line 2703
    :pswitch_4
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->isBackwardsStart()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2705
    iget-object v1, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/CollationElementIterator;->getContractionOffset(Lcom/ibm/icu/text/RuleBasedCollator;I)I

    move-result v2

    aget p2, v1, v2

    .line 2707
    goto :goto_0

    .line 2709
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/ibm/icu/text/CollationElementIterator;->previousContraction(Lcom/ibm/icu/text/RuleBasedCollator;IC)I

    move-result v0

    goto :goto_1

    .line 2711
    :pswitch_5
    invoke-direct {p0, p2}, Lcom/ibm/icu/text/CollationElementIterator;->previousLongPrimary(I)I

    move-result v0

    goto :goto_1

    .line 2713
    :pswitch_6
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/CollationElementIterator;->previousExpansion(Lcom/ibm/icu/text/RuleBasedCollator;I)I

    move-result v0

    goto :goto_1

    .line 2715
    :pswitch_7
    invoke-direct {p0, p1, p2, p3}, Lcom/ibm/icu/text/CollationElementIterator;->previousDigit(Lcom/ibm/icu/text/RuleBasedCollator;IC)I

    move-result p2

    .line 2716
    goto :goto_0

    .line 2718
    :pswitch_8
    invoke-direct {p0, p1, p3}, Lcom/ibm/icu/text/CollationElementIterator;->previousHangul(Lcom/ibm/icu/text/RuleBasedCollator;C)I

    move-result v0

    goto :goto_1

    .line 2722
    :pswitch_9
    invoke-direct {p0, p3}, Lcom/ibm/icu/text/CollationElementIterator;->previousSurrogate(C)I

    move-result v0

    goto :goto_1

    .line 2725
    :pswitch_a
    invoke-direct {p0, p3}, Lcom/ibm/icu/text/CollationElementIterator;->previousImplicit(I)I

    move-result v0

    goto :goto_1

    .line 2728
    :pswitch_b
    invoke-direct {p0, p3}, Lcom/ibm/icu/text/CollationElementIterator;->previousImplicit(I)I

    move-result v0

    goto :goto_1

    .line 2730
    :pswitch_c
    const/high16 v0, -0x10000000

    goto :goto_1

    .line 2691
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_6
        :pswitch_4
        :pswitch_0
        :pswitch_c
        :pswitch_1
        :pswitch_8
        :pswitch_1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_3
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method private previousSpecialPrefix(Lcom/ibm/icu/text/RuleBasedCollator;I)I
    .locals 8
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;
    .param p2, "ce"    # I

    .prologue
    .line 2199
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {p0, v6}, Lcom/ibm/icu/text/CollationElementIterator;->backupInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 2202
    :cond_0
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/ibm/icu/text/CollationElementIterator;->getContractionOffset(Lcom/ibm/icu/text/RuleBasedCollator;I)I

    move-result v4

    .line 2203
    .local v4, "offset":I
    move v0, v4

    .line 2204
    .local v0, "entryoffset":I
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->isBackwardsStart()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2205
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget p2, v6, v4

    .line 2277
    :goto_1
    iget-object v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_utilSpecialBackUp_:Lcom/ibm/icu/text/CollationElementIterator$Backup;

    invoke-direct {p0, v6}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V

    .line 2278
    return p2

    .line 2208
    :cond_1
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    move-result v6

    int-to-char v5, v6

    .line 2209
    .local v5, "prevch":C
    :goto_2
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v6, v6, v4

    if-le v5, v6, :cond_2

    .line 2212
    add-int/lit8 v4, v4, 0x1

    .line 2213
    goto :goto_2

    .line 2214
    :cond_2
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionIndex_:[C

    aget-char v6, v6, v4

    if-ne v5, v6, :cond_3

    .line 2215
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget p2, v6, v4

    .line 2270
    :goto_3
    invoke-direct {p0, p2}, Lcom/ibm/icu/text/CollationElementIterator;->isSpecialPrefixTag(I)Z

    move-result v6

    if-nez v6, :cond_0

    goto :goto_1

    .line 2224
    :cond_3
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v6, v5}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v2

    .line 2226
    .local v2, "isZeroCE":I
    if-eqz v2, :cond_0

    .line 2229
    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-static {v5}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2237
    :cond_4
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->isBackwardsStart()Z

    move-result v6

    if-nez v6, :cond_0

    .line 2238
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    move-result v6

    int-to-char v3, v6

    .line 2239
    .local v3, "lead":C
    invoke-static {v3}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2240
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v6, v3}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v2

    .line 2241
    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedCollator;->getTag(I)I

    move-result v6

    const/4 v7, 0x5

    if-ne v6, v7, :cond_5

    .line 2243
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v6, v2, v5}, Lcom/ibm/icu/impl/IntTrie;->getTrailValue(IC)I

    move-result v1

    .line 2246
    .local v1, "finalCE":I
    if-eqz v1, :cond_0

    .line 2258
    .end local v1    # "finalCE":I
    :cond_5
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->nextChar()I

    .line 2267
    .end local v3    # "lead":C
    :cond_6
    iget-object v6, p1, Lcom/ibm/icu/text/RuleBasedCollator;->m_contractionCE_:[I

    aget p2, v6, v0

    goto :goto_3

    .line 2254
    .restart local v3    # "lead":C
    :cond_7
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->nextChar()I

    goto :goto_0
.end method

.method private previousSurrogate(C)I
    .locals 3
    .param p1, "ch"    # C

    .prologue
    const/4 v1, 0x0

    .line 2663
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->isBackwardsStart()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2676
    :cond_0
    :goto_0
    return v1

    .line 2667
    :cond_1
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    move-result v2

    int-to-char v0, v2

    .line 2669
    .local v0, "prevch":C
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2670
    invoke-static {v0, p1}, Lcom/ibm/icu/impl/UCharacterProperty;->getRawSupplementary(CC)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/ibm/icu/text/CollationElementIterator;->previousImplicit(I)I

    move-result v1

    goto :goto_0

    .line 2673
    :cond_2
    const v2, 0xffff

    if-eq v0, v2, :cond_0

    .line 2674
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->nextChar()I

    goto :goto_0
.end method

.method public static final primaryOrder(I)I
    .locals 1
    .param p0, "ce"    # I

    .prologue
    .line 407
    const/high16 v0, -0x10000

    and-int/2addr v0, p0

    ushr-int/lit8 v0, v0, 0x10

    return v0
.end method

.method public static final secondaryOrder(I)I
    .locals 1
    .param p0, "ce"    # I

    .prologue
    .line 419
    const v0, 0xff00

    and-int/2addr v0, p0

    shr-int/lit8 v0, v0, 0x8

    return v0
.end method

.method private setDiscontiguous(Ljava/lang/StringBuffer;)V
    .locals 4
    .param p1, "skipped"    # Ljava/lang/StringBuffer;

    .prologue
    const/4 v3, 0x0

    .line 1514
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-ltz v0, :cond_0

    .line 1515
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v1, v2}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 1523
    :goto_0
    iput v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    .line 1524
    return-void

    .line 1518
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    .line 1519
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 1520
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public static final tertiaryOrder(I)I
    .locals 1
    .param p0, "ce"    # I

    .prologue
    .line 432
    and-int/lit16 v0, p0, 0xff

    return v0
.end method

.method private updateInternalState()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 952
    iput-boolean v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_isCodePointHiragana_:Z

    .line 953
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 954
    iput v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    .line 955
    iput v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 956
    iput v1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 957
    iput v2, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    .line 958
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->getLength()I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    .line 960
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_isForwards_:Z

    .line 961
    return-void
.end method

.method private updateInternalState(Lcom/ibm/icu/text/CollationElementIterator$Backup;)V
    .locals 2
    .param p1, "backup"    # Lcom/ibm/icu/text/CollationElementIterator$Backup;

    .prologue
    .line 991
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    iget v1, p1, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_offset_:I

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 992
    iget-boolean v0, p1, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_isCodePointHiragana_:Z

    iput-boolean v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_isCodePointHiragana_:Z

    .line 993
    iget v0, p1, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_bufferOffset_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    .line 994
    iget v0, p1, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_FCDLimit_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    .line 995
    iget v0, p1, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_FCDStart_:I

    iput v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    .line 996
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 997
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-ltz v0, :cond_0

    .line 999
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_buffer_:Ljava/lang/StringBuffer;

    iget-object v1, p1, Lcom/ibm/icu/text/CollationElementIterator$Backup;->m_buffer_:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1001
    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "that"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 562
    if-ne p1, p0, :cond_1

    .line 576
    :cond_0
    :goto_0
    return v1

    .line 565
    :cond_1
    instance-of v3, p1, Lcom/ibm/icu/text/CollationElementIterator;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 566
    check-cast v0, Lcom/ibm/icu/text/CollationElementIterator;

    .line 568
    .local v0, "thatceiter":Lcom/ibm/icu/text/CollationElementIterator;
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v4, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v3, v4}, Lcom/ibm/icu/text/RuleBasedCollator;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 569
    goto :goto_0

    .line 572
    :cond_2
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v3

    iget-object v4, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v4

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v3}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .end local v0    # "thatceiter":Lcom/ibm/icu/text/CollationElementIterator;
    :cond_4
    move v1, v2

    .line 576
    goto :goto_0
.end method

.method public getMaxExpansion(I)I
    .locals 12
    .param p1, "ce"    # I

    .prologue
    const-wide v10, 0xffffffffL

    .line 185
    const/4 v5, 0x0

    .line 186
    .local v5, "start":I
    iget-object v8, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v8, v8, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    array-length v0, v8

    .line 187
    .local v0, "limit":I
    int-to-long v8, p1

    and-long v6, v8, v10

    .line 188
    .local v6, "unsignedce":J
    :goto_0
    add-int/lit8 v8, v0, -0x1

    if-ge v5, v8, :cond_1

    .line 189
    sub-int v8, v0, v5

    shr-int/lit8 v8, v8, 0x1

    add-int v1, v5, v8

    .line 190
    .local v1, "mid":I
    iget-object v8, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v8, v8, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    aget v8, v8, v1

    int-to-long v8, v8

    and-long v2, v8, v10

    .line 191
    .local v2, "midce":J
    cmp-long v8, v6, v2

    if-gtz v8, :cond_0

    .line 192
    move v0, v1

    .line 193
    goto :goto_0

    .line 195
    :cond_0
    move v5, v1

    goto :goto_0

    .line 198
    .end local v1    # "mid":I
    .end local v2    # "midce":J
    :cond_1
    const/4 v4, 0x1

    .line 199
    .local v4, "result":I
    iget-object v8, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v8, v8, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    aget v8, v8, v5

    if-ne v8, p1, :cond_3

    .line 200
    iget-object v8, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v8, v8, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCEMaxSize_:[B

    aget-byte v4, v8, v5

    .line 209
    :cond_2
    :goto_1
    return v4

    .line 202
    :cond_3
    iget-object v8, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v8, v8, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    array-length v8, v8

    if-ge v0, v8, :cond_4

    iget-object v8, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v8, v8, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCE_:[I

    aget v8, v8, v0

    if-ne v8, p1, :cond_4

    .line 204
    iget-object v8, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v8, v8, Lcom/ibm/icu/text/RuleBasedCollator;->m_expansionEndCEMaxSize_:[B

    aget-byte v4, v8, v0

    .line 205
    goto :goto_1

    .line 206
    :cond_4
    const v8, 0xffff

    and-int/2addr v8, p1

    const/16 v9, 0xc0

    if-ne v8, v9, :cond_2

    .line 207
    const/4 v4, 0x2

    goto :goto_1
.end method

.method public getOffset()I
    .locals 2

    .prologue
    .line 163
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 164
    iget-boolean v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_isForwards_:Z

    if-eqz v0, :cond_0

    .line 165
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDLimit_:I

    .line 169
    :goto_0
    return v0

    .line 167
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_FCDStart_:I

    goto :goto_0

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v0

    goto :goto_0
.end method

.method isInBuffer()Z
    .locals 1

    .prologue
    .line 725
    iget v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_bufferOffset_:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()I
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/high16 v7, -0x10000000

    const/4 v4, 0x0

    .line 255
    iput-boolean v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_isForwards_:Z

    .line 256
    iget v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    if-lez v5, :cond_2

    .line 257
    iget v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    iget v6, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    if-ge v5, v6, :cond_1

    .line 259
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    aget v2, v3, v4

    .line 311
    :cond_0
    :goto_0
    return v2

    .line 261
    :cond_1
    iput v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 262
    iput v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 265
    :cond_2
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->nextChar()I

    move-result v1

    .line 267
    .local v1, "ch_int":I
    if-eq v1, v2, :cond_0

    .line 270
    int-to-char v0, v1

    .line 271
    .local v0, "ch":C
    iget-object v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v5, v5, Lcom/ibm/icu/text/RuleBasedCollator;->m_isHiragana4_:Z

    if-eqz v5, :cond_5

    .line 275
    iget-boolean v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_isCodePointHiragana_:Z

    if-eqz v5, :cond_3

    const/16 v5, 0x3099

    if-lt v0, v5, :cond_3

    const/16 v5, 0x309c

    if-le v0, v5, :cond_4

    :cond_3
    const/16 v5, 0x3040

    if-lt v0, v5, :cond_7

    const/16 v5, 0x309e

    if-gt v0, v5, :cond_7

    const/16 v5, 0x3094

    if-le v0, v5, :cond_4

    const/16 v5, 0x309d

    if-lt v0, v5, :cond_7

    :cond_4
    :goto_1
    iput-boolean v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_isCodePointHiragana_:Z

    .line 279
    :cond_5
    const/4 v2, -0x1

    .line 280
    .local v2, "result":I
    const/16 v3, 0xff

    if-gt v0, v3, :cond_8

    .line 284
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v3, v3, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v3, v0}, Lcom/ibm/icu/impl/IntTrie;->getLatin1LinearValue(C)I

    move-result v2

    .line 285
    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 286
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-direct {p0, v3, v2, v0}, Lcom/ibm/icu/text/CollationElementIterator;->nextSpecial(Lcom/ibm/icu/text/RuleBasedCollator;IC)I

    move-result v2

    .line 307
    :cond_6
    :goto_2
    if-ne v2, v7, :cond_0

    .line 309
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/CollationElementIterator;->nextImplicit(I)I

    move-result v2

    goto :goto_0

    .end local v2    # "result":I
    :cond_7
    move v3, v4

    .line 275
    goto :goto_1

    .line 290
    .restart local v2    # "result":I
    :cond_8
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v3, v3, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v3, v0}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v2

    .line 292
    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 294
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-direct {p0, v3, v2, v0}, Lcom/ibm/icu/text/CollationElementIterator;->nextSpecial(Lcom/ibm/icu/text/RuleBasedCollator;IC)I

    move-result v2

    .line 296
    :cond_9
    if-ne v2, v7, :cond_6

    sget-object v3, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    if-eqz v3, :cond_6

    .line 300
    sget-object v3, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v3, v3, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v3, v0}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v2

    .line 301
    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 303
    sget-object v3, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-direct {p0, v3, v2, v0}, Lcom/ibm/icu/text/CollationElementIterator;->nextSpecial(Lcom/ibm/icu/text/RuleBasedCollator;IC)I

    move-result v2

    goto :goto_2
.end method

.method public previous()I
    .locals 7

    .prologue
    const/4 v4, -0x1

    const/high16 v6, -0x10000000

    const/4 v3, 0x0

    .line 338
    iget-object v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v5

    if-gtz v5, :cond_0

    iget-boolean v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_isForwards_:Z

    if-eqz v5, :cond_0

    .line 341
    iget-object v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v5}, Lcom/ibm/icu/text/UCharacterIterator;->setToLimit()V

    .line 342
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState()V

    .line 344
    :cond_0
    iput-boolean v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_isForwards_:Z

    .line 345
    const/4 v2, -0x1

    .line 346
    .local v2, "result":I
    iget v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    if-lez v5, :cond_2

    .line 347
    iget v5, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    if-lez v5, :cond_1

    .line 348
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBuffer_:[I

    iget v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    aget v3, v3, v4

    .line 395
    :goto_0
    return v3

    .line 350
    :cond_1
    iput v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferSize_:I

    .line 351
    iput v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_CEBufferOffset_:I

    .line 353
    :cond_2
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->previousChar()I

    move-result v1

    .line 354
    .local v1, "ch_int":I
    if-ne v1, v4, :cond_3

    move v3, v4

    .line 355
    goto :goto_0

    .line 357
    :cond_3
    int-to-char v0, v1

    .line 358
    .local v0, "ch":C
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-boolean v4, v4, Lcom/ibm/icu/text/RuleBasedCollator;->m_isHiragana4_:Z

    if-eqz v4, :cond_5

    .line 359
    const/16 v4, 0x3040

    if-lt v0, v4, :cond_4

    const/16 v4, 0x309f

    if-gt v0, v4, :cond_4

    const/4 v3, 0x1

    :cond_4
    iput-boolean v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_isCodePointHiragana_:Z

    .line 361
    :cond_5
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v3, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->isContractionEnd(C)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->isBackwardsStart()Z

    move-result v3

    if-nez v3, :cond_8

    .line 362
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    const/high16 v4, -0xe000000

    invoke-direct {p0, v3, v4, v0}, Lcom/ibm/icu/text/CollationElementIterator;->previousSpecial(Lcom/ibm/icu/text/RuleBasedCollator;IC)I

    move-result v2

    .line 392
    :cond_6
    :goto_1
    if-ne v2, v6, :cond_7

    .line 393
    invoke-direct {p0, v0}, Lcom/ibm/icu/text/CollationElementIterator;->previousImplicit(I)I

    move-result v2

    :cond_7
    move v3, v2

    .line 395
    goto :goto_0

    .line 365
    :cond_8
    const/16 v3, 0xff

    if-gt v0, v3, :cond_b

    .line 366
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v3, v3, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v3, v0}, Lcom/ibm/icu/impl/IntTrie;->getLatin1LinearValue(C)I

    move-result v2

    .line 371
    :goto_2
    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 372
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-direct {p0, v3, v2, v0}, Lcom/ibm/icu/text/CollationElementIterator;->previousSpecial(Lcom/ibm/icu/text/RuleBasedCollator;IC)I

    move-result v2

    .line 374
    :cond_9
    if-ne v2, v6, :cond_6

    .line 375
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->isBackwardsStart()Z

    move-result v3

    if-nez v3, :cond_c

    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v3, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->isContractionEnd(C)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 377
    const/high16 v2, -0xe000000

    .line 385
    :cond_a
    :goto_3
    invoke-static {v2}, Lcom/ibm/icu/text/RuleBasedCollator;->isSpecial(I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 386
    sget-object v3, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    if-eqz v3, :cond_6

    .line 387
    sget-object v3, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-direct {p0, v3, v2, v0}, Lcom/ibm/icu/text/CollationElementIterator;->previousSpecial(Lcom/ibm/icu/text/RuleBasedCollator;IC)I

    move-result v2

    goto :goto_1

    .line 369
    :cond_b
    iget-object v3, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v3, v3, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v3, v0}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v2

    goto :goto_2

    .line 380
    :cond_c
    sget-object v3, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    if-eqz v3, :cond_a

    .line 381
    sget-object v3, Lcom/ibm/icu/text/RuleBasedCollator;->UCA_:Lcom/ibm/icu/text/RuleBasedCollator;

    iget-object v3, v3, Lcom/ibm/icu/text/RuleBasedCollator;->m_trie_:Lcom/ibm/icu/impl/IntTrie;

    invoke-virtual {v3, v0}, Lcom/ibm/icu/impl/IntTrie;->getLeadValue(C)I

    move-result v2

    goto :goto_3
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->setToStart()V

    .line 228
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState()V

    .line 229
    return-void
.end method

.method setCollator(Lcom/ibm/icu/text/RuleBasedCollator;)V
    .locals 0
    .param p1, "collator"    # Lcom/ibm/icu/text/RuleBasedCollator;

    .prologue
    .line 697
    iput-object p1, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    .line 698
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState()V

    .line 699
    return-void
.end method

.method setExactOffset(I)V
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 715
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 716
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState()V

    .line 717
    return-void
.end method

.method public setOffset(I)V
    .locals 5
    .param p1, "offset"    # I

    .prologue
    .line 459
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4, p1}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 460
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->current()I

    move-result v1

    .line 461
    .local v1, "ch_int":I
    int-to-char v0, v1

    .line 462
    .local v0, "ch":C
    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v4, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 465
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->isTrailSurrogate(C)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 467
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v4

    int-to-char v2, v4

    .line 468
    .local v2, "prevch":C
    invoke-static {v2}, Lcom/ibm/icu/text/UTF16;->isLeadSurrogate(C)Z

    move-result v4

    if-nez v4, :cond_0

    .line 469
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4, p1}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 490
    .end local v2    # "prevch":C
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState()V

    .line 493
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result p1

    .line 494
    if-nez p1, :cond_6

    .line 497
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_isForwards_:Z

    .line 504
    :cond_1
    :goto_1
    return-void

    .line 479
    :cond_2
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->previous()I

    move-result v4

    int-to-char v0, v4

    .line 475
    :cond_3
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v4

    if-lez v4, :cond_4

    .line 476
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_collator_:Lcom/ibm/icu/text/RuleBasedCollator;

    invoke-virtual {v4, v0}, Lcom/ibm/icu/text/RuleBasedCollator;->isUnsafe(C)Z

    move-result v4

    if-nez v4, :cond_2

    .line 481
    :cond_4
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState()V

    .line 482
    const/4 v3, 0x0

    .line 483
    .local v3, "prevoffset":I
    :goto_2
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v4

    if-gt v4, p1, :cond_5

    .line 484
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v3

    .line 485
    invoke-virtual {p0}, Lcom/ibm/icu/text/CollationElementIterator;->next()I

    goto :goto_2

    .line 487
    :cond_5
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4, v3}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    goto :goto_0

    .line 499
    .end local v3    # "prevoffset":I
    :cond_6
    iget-object v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->getLength()I

    move-result v4

    if-ne p1, v4, :cond_1

    .line 502
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_isForwards_:Z

    goto :goto_1
.end method

.method public setText(Lcom/ibm/icu/text/UCharacterIterator;)V
    .locals 2
    .param p1, "source"    # Lcom/ibm/icu/text/UCharacterIterator;

    .prologue
    .line 531
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-virtual {p1}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/StringUCharacterIterator;->setText(Ljava/lang/String;)V

    .line 532
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    .line 533
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState()V

    .line 534
    return-void
.end method

.method setText(Lcom/ibm/icu/text/UCharacterIterator;I)V
    .locals 2
    .param p1, "source"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p2, "offset"    # I

    .prologue
    .line 745
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-virtual {p1}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/impl/StringUCharacterIterator;->setText(Ljava/lang/String;)V

    .line 746
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    .line 747
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0, p2}, Lcom/ibm/icu/text/UCharacterIterator;->setIndex(I)V

    .line 748
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState()V

    .line 749
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 515
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/StringUCharacterIterator;->setText(Ljava/lang/String;)V

    .line 516
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_srcUtilIter_:Lcom/ibm/icu/impl/StringUCharacterIterator;

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    .line 517
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState()V

    .line 518
    return-void
.end method

.method public setText(Ljava/text/CharacterIterator;)V
    .locals 1
    .param p1, "source"    # Ljava/text/CharacterIterator;

    .prologue
    .line 545
    new-instance v0, Lcom/ibm/icu/impl/CharacterIteratorWrapper;

    invoke-direct {v0, p1}, Lcom/ibm/icu/impl/CharacterIteratorWrapper;-><init>(Ljava/text/CharacterIterator;)V

    iput-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    .line 546
    iget-object v0, p0, Lcom/ibm/icu/text/CollationElementIterator;->m_source_:Lcom/ibm/icu/text/UCharacterIterator;

    invoke-virtual {v0}, Lcom/ibm/icu/text/UCharacterIterator;->setToStart()V

    .line 547
    invoke-direct {p0}, Lcom/ibm/icu/text/CollationElementIterator;->updateInternalState()V

    .line 548
    return-void
.end method
