.class Lcom/ibm/icu/text/RuleBasedTransliterator$Data;
.super Ljava/lang/Object;
.source "RuleBasedTransliterator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/RuleBasedTransliterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Data"
.end annotation


# instance fields
.field public ruleSet:Lcom/ibm/icu/text/TransliterationRuleSet;

.field variableNames:Ljava/util/Hashtable;

.field variables:[Ljava/lang/Object;

.field variablesBase:C


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variableNames:Ljava/util/Hashtable;

    .line 374
    new-instance v0, Lcom/ibm/icu/text/TransliterationRuleSet;

    invoke-direct {v0}, Lcom/ibm/icu/text/TransliterationRuleSet;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->ruleSet:Lcom/ibm/icu/text/TransliterationRuleSet;

    .line 375
    return-void
.end method


# virtual methods
.method public lookupMatcher(I)Lcom/ibm/icu/text/UnicodeMatcher;
    .locals 2
    .param p1, "standIn"    # I

    .prologue
    .line 416
    iget-char v1, p0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variablesBase:C

    sub-int v0, p1, v1

    .line 417
    .local v0, "i":I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variables:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variables:[Ljava/lang/Object;

    aget-object v1, v1, v0

    check-cast v1, Lcom/ibm/icu/text/UnicodeMatcher;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public lookupReplacer(I)Lcom/ibm/icu/text/UnicodeReplacer;
    .locals 2
    .param p1, "standIn"    # I

    .prologue
    .line 426
    iget-char v1, p0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variablesBase:C

    sub-int v0, p1, v1

    .line 427
    .local v0, "i":I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variables:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->variables:[Ljava/lang/Object;

    aget-object v1, v1, v0

    check-cast v1, Lcom/ibm/icu/text/UnicodeReplacer;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
