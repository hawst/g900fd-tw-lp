.class Lcom/ibm/icu/text/PluralRules$RangeConstraint;
.super Ljava/lang/Object;
.source "PluralRules.java"

# interfaces
.implements Lcom/ibm/icu/text/PluralRules$Constraint;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/PluralRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RangeConstraint"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private inRange:Z

.field private integersOnly:Z

.field private lowerBound:J

.field private mod:I

.field private upperBound:J


# direct methods
.method constructor <init>(IZZJJ)V
    .locals 0
    .param p1, "mod"    # I
    .param p2, "inRange"    # Z
    .param p3, "integersOnly"    # Z
    .param p4, "lowerBound"    # J
    .param p6, "upperBound"    # J

    .prologue
    .line 485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486
    iput p1, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->mod:I

    .line 487
    iput-boolean p2, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->inRange:Z

    .line 488
    iput-boolean p3, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->integersOnly:Z

    .line 489
    iput-wide p4, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->lowerBound:J

    .line 490
    iput-wide p6, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->upperBound:J

    .line 491
    return-void
.end method


# virtual methods
.method public isFulfilled(D)Z
    .locals 7
    .param p1, "n"    # D

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 475
    iget-boolean v2, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->integersOnly:Z

    if-eqz v2, :cond_2

    double-to-long v2, p1

    long-to-double v2, v2

    sub-double v2, p1, v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_2

    .line 476
    iget-boolean v2, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->inRange:Z

    if-nez v2, :cond_1

    .line 481
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 476
    goto :goto_0

    .line 478
    :cond_2
    iget v2, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->mod:I

    if-eqz v2, :cond_3

    .line 479
    iget v2, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->mod:I

    int-to-double v2, v2

    rem-double/2addr p1, v2

    .line 481
    :cond_3
    iget-boolean v3, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->inRange:Z

    iget-wide v4, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->lowerBound:J

    long-to-double v4, v4

    cmpl-double v2, p1, v4

    if-ltz v2, :cond_4

    iget-wide v4, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->upperBound:J

    long-to-double v4, v4

    cmpg-double v2, p1, v4

    if-gtz v2, :cond_4

    move v2, v0

    :goto_1
    if-eq v3, v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 499
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v1, "[mod: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v1, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->mod:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, " inRange: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-boolean v1, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->inRange:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, " integersOnly: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-boolean v1, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->integersOnly:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, " low: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-wide v2, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->lowerBound:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, " high: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-wide v2, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->upperBound:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateRepeatLimit(I)I
    .locals 4
    .param p1, "limit"    # I

    .prologue
    .line 494
    iget v1, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->mod:I

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->upperBound:J

    long-to-int v0, v2

    .line 495
    .local v0, "mylimit":I
    :goto_0
    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1

    .line 494
    .end local v0    # "mylimit":I
    :cond_0
    iget v0, p0, Lcom/ibm/icu/text/PluralRules$RangeConstraint;->mod:I

    goto :goto_0
.end method
