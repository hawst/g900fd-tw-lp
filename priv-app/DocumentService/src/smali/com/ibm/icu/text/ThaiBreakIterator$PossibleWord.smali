.class Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;
.super Ljava/lang/Object;
.source "ThaiBreakIterator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/ThaiBreakIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PossibleWord"
.end annotation


# instance fields
.field private final POSSIBLE_WORD_LIST_MAX:I

.field private count:[I

.field private current:I

.field private lengths:[I

.field private mark:I

.field private offset:I

.field private prefix:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/16 v0, 0x14

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->POSSIBLE_WORD_LIST_MAX:I

    .line 36
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->lengths:[I

    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->count:[I

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->offset:I

    .line 39
    return-void
.end method


# virtual methods
.method public acceptMarked(Ljava/text/CharacterIterator;)I
    .locals 3
    .param p1, "fIter"    # Ljava/text/CharacterIterator;

    .prologue
    .line 62
    iget v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->offset:I

    iget-object v1, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->lengths:[I

    iget v2, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->mark:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    invoke-interface {p1, v0}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 63
    iget-object v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->lengths:[I

    iget v1, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->mark:I

    aget v0, v0, v1

    return v0
.end method

.method public backUp(Ljava/text/CharacterIterator;)Z
    .locals 3
    .param p1, "fIter"    # Ljava/text/CharacterIterator;

    .prologue
    .line 69
    iget v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->current:I

    if-lez v0, :cond_0

    .line 70
    iget v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->offset:I

    iget-object v1, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->lengths:[I

    iget v2, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->current:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->current:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    invoke-interface {p1, v0}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 71
    const/4 v0, 0x1

    .line 73
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public candidates(Ljava/text/CharacterIterator;Lcom/ibm/icu/text/BreakCTDictionary;I)I
    .locals 8
    .param p1, "fIter"    # Ljava/text/CharacterIterator;
    .param p2, "dict"    # Lcom/ibm/icu/text/BreakCTDictionary;
    .param p3, "rangeEnd"    # I

    .prologue
    const/4 v7, 0x0

    .line 43
    invoke-interface {p1}, Ljava/text/CharacterIterator;->getIndex()I

    move-result v6

    .line 44
    .local v6, "start":I
    iget v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->offset:I

    if-eq v6, v0, :cond_0

    .line 45
    iput v6, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->offset:I

    .line 46
    sub-int v2, p3, v6

    iget-object v3, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->lengths:[I

    iget-object v4, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->count:[I

    iget-object v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->lengths:[I

    array-length v5, v0

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/ibm/icu/text/BreakCTDictionary;->matches(Ljava/text/CharacterIterator;I[I[II)I

    move-result v0

    iput v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->prefix:I

    .line 48
    iget-object v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->count:[I

    aget v0, v0, v7

    if-gtz v0, :cond_0

    .line 49
    invoke-interface {p1, v6}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->count:[I

    aget v0, v0, v7

    if-lez v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->lengths:[I

    iget-object v1, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->count:[I

    aget v1, v1, v7

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    add-int/2addr v0, v6

    invoke-interface {p1, v0}, Ljava/text/CharacterIterator;->setIndex(I)C

    .line 55
    :cond_1
    iget-object v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->count:[I

    aget v0, v0, v7

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->current:I

    .line 56
    iget v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->current:I

    iput v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->mark:I

    .line 57
    iget-object v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->count:[I

    aget v0, v0, v7

    return v0
.end method

.method public longestPrefix()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->prefix:I

    return v0
.end method

.method public markCurrent()V
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->current:I

    iput v0, p0, Lcom/ibm/icu/text/ThaiBreakIterator$PossibleWord;->mark:I

    .line 84
    return-void
.end method
