.class Lcom/ibm/icu/text/CollatorReader$2;
.super Ljava/lang/Object;
.source "CollatorReader.java"

# interfaces
.implements Lcom/ibm/icu/impl/ICUBinary$Authenticate;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 559
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isDataVersionAcceptable([B)Z
    .locals 4
    .param p1, "version"    # [B

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 561
    aget-byte v2, p1, v1

    invoke-static {}, Lcom/ibm/icu/text/CollatorReader;->access$100()[B

    move-result-object v3

    aget-byte v3, v3, v1

    if-ne v2, v3, :cond_0

    aget-byte v2, p1, v0

    invoke-static {}, Lcom/ibm/icu/text/CollatorReader;->access$100()[B

    move-result-object v3

    aget-byte v3, v3, v0

    if-lt v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
