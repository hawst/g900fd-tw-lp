.class Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;
.super Ljava/lang/Object;
.source "TransliteratorRegistry.java"

# interfaces
.implements Ljava/util/Enumeration;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/TransliteratorRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IDEnumeration"
.end annotation


# instance fields
.field en:Ljava/util/Enumeration;


# direct methods
.method public constructor <init>(Ljava/util/Enumeration;)V
    .locals 0
    .param p1, "e"    # Ljava/util/Enumeration;

    .prologue
    .line 389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390
    iput-object p1, p0, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;->en:Ljava/util/Enumeration;

    .line 391
    return-void
.end method


# virtual methods
.method public hasMoreElements()Z
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;->en:Ljava/util/Enumeration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;->en:Ljava/util/Enumeration;

    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public nextElement()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/ibm/icu/text/TransliteratorRegistry$IDEnumeration;->en:Ljava/util/Enumeration;

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ibm/icu/util/CaseInsensitiveString;

    invoke-virtual {v0}, Lcom/ibm/icu/util/CaseInsensitiveString;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
