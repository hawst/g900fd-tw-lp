.class Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;
.super Ljava/lang/Object;
.source "RBBITableBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/RBBITableBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RBBIStateDescriptor"
.end annotation


# instance fields
.field fAccepting:I

.field fDtran:[I

.field fLookAhead:I

.field fMarked:Z

.field fPositions:Ljava/util/Set;

.field fTagVals:Ljava/util/SortedSet;

.field fTagsIdx:I


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "maxInputSymbol"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fTagVals:Ljava/util/SortedSet;

    .line 55
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fPositions:Ljava/util/Set;

    .line 56
    add-int/lit8 v0, p1, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/ibm/icu/text/RBBITableBuilder$RBBIStateDescriptor;->fDtran:[I

    .line 60
    return-void
.end method
