.class public Lcom/ibm/icu/text/RuleBasedTransliterator;
.super Lcom/ibm/icu/text/Transliterator;
.source "RuleBasedTransliterator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/RuleBasedTransliterator$Data;
    }
.end annotation


# instance fields
.field private data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/ibm/icu/text/RuleBasedTransliterator$Data;Lcom/ibm/icu/text/UnicodeFilter;)V
    .locals 1
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "data"    # Lcom/ibm/icu/text/RuleBasedTransliterator$Data;
    .param p3, "filter"    # Lcom/ibm/icu/text/UnicodeFilter;

    .prologue
    .line 321
    invoke-direct {p0, p1, p3}, Lcom/ibm/icu/text/Transliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 322
    iput-object p2, p0, Lcom/ibm/icu/text/RuleBasedTransliterator;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    .line 323
    iget-object v0, p2, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->ruleSet:Lcom/ibm/icu/text/TransliterationRuleSet;

    invoke-virtual {v0}, Lcom/ibm/icu/text/TransliterationRuleSet;->getMaximumContextLength()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/ibm/icu/text/RuleBasedTransliterator;->setMaximumContextLength(I)V

    .line 324
    return-void
.end method


# virtual methods
.method public getTargetSet()Lcom/ibm/icu/text/UnicodeSet;
    .locals 2

    .prologue
    .line 466
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedTransliterator;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->ruleSet:Lcom/ibm/icu/text/TransliterationRuleSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/TransliterationRuleSet;->getSourceTargetSet(Z)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method protected handleGetSourceSet()Lcom/ibm/icu/text/UnicodeSet;
    .locals 2

    .prologue
    .line 456
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedTransliterator;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->ruleSet:Lcom/ibm/icu/text/TransliterationRuleSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/TransliterationRuleSet;->getSourceTargetSet(Z)Lcom/ibm/icu/text/UnicodeSet;

    move-result-object v0

    return-object v0
.end method

.method protected declared-synchronized handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V
    .locals 4
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "index"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "incremental"    # Z

    .prologue
    .line 357
    monitor-enter p0

    const/4 v0, 0x0

    .line 358
    .local v0, "loopCount":I
    :try_start_0
    iget v2, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    iget v3, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    sub-int/2addr v2, v3

    shl-int/lit8 v1, v2, 0x4

    .line 359
    .local v1, "loopLimit":I
    if-gez v1, :cond_0

    .line 360
    const v1, 0x7fffffff

    .line 364
    :cond_0
    :goto_0
    iget v2, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    iget v3, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    if-ge v2, v3, :cond_1

    if-gt v0, v1, :cond_1

    iget-object v2, p0, Lcom/ibm/icu/text/RuleBasedTransliterator;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    iget-object v2, v2, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->ruleSet:Lcom/ibm/icu/text/TransliterationRuleSet;

    invoke-virtual {v2, p1, p2, p3}, Lcom/ibm/icu/text/TransliterationRuleSet;->transliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    .line 366
    add-int/lit8 v0, v0, 0x1

    .line 367
    goto :goto_0

    .line 368
    :cond_1
    monitor-exit p0

    return-void

    .line 357
    .end local v1    # "loopLimit":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public toRules(Z)Ljava/lang/String;
    .locals 1
    .param p1, "escapeUnprintable"    # Z

    .prologue
    .line 446
    iget-object v0, p0, Lcom/ibm/icu/text/RuleBasedTransliterator;->data:Lcom/ibm/icu/text/RuleBasedTransliterator$Data;

    iget-object v0, v0, Lcom/ibm/icu/text/RuleBasedTransliterator$Data;->ruleSet:Lcom/ibm/icu/text/TransliterationRuleSet;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/text/TransliterationRuleSet;->toRules(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
