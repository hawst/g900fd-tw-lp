.class Lcom/ibm/icu/text/RemoveTransliterator;
.super Lcom/ibm/icu/text/Transliterator;
.source "RemoveTransliterator.java"


# static fields
.field private static _ID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string/jumbo v0, "Any-Remove"

    sput-object v0, Lcom/ibm/icu/text/RemoveTransliterator;->_ID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    sget-object v0, Lcom/ibm/icu/text/RemoveTransliterator;->_ID:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/ibm/icu/text/Transliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 38
    return-void
.end method

.method static register()V
    .locals 3

    .prologue
    .line 25
    sget-object v0, Lcom/ibm/icu/text/RemoveTransliterator;->_ID:Ljava/lang/String;

    new-instance v1, Lcom/ibm/icu/text/RemoveTransliterator$1;

    invoke-direct {v1}, Lcom/ibm/icu/text/RemoveTransliterator$1;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 30
    const-string/jumbo v0, "Remove"

    const-string/jumbo v1, "Null"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/ibm/icu/text/Transliterator;->registerSpecialInverse(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 31
    return-void
.end method


# virtual methods
.method protected handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V
    .locals 4
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "index"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "incremental"    # Z

    .prologue
    .line 47
    iget v1, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    iget v2, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    const-string/jumbo v3, ""

    invoke-interface {p1, v1, v2, v3}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 48
    iget v1, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    iget v2, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    sub-int v0, v1, v2

    .line 49
    .local v0, "len":I
    iget v1, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    sub-int/2addr v1, v0

    iput v1, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    .line 50
    iget v1, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    sub-int/2addr v1, v0

    iput v1, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 51
    return-void
.end method
