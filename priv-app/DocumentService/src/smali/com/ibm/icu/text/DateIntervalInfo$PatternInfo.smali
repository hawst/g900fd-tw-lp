.class public final Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
.super Ljava/lang/Object;
.source "DateIntervalInfo.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/DateIntervalInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PatternInfo"
.end annotation


# static fields
.field static final currentSerialVersion:I = 0x1

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final fFirstDateInPtnIsLaterDate:Z

.field private final fIntervalPatternFirstPart:Ljava/lang/String;

.field private final fIntervalPatternSecondPart:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "firstPart"    # Ljava/lang/String;
    .param p2, "secondPart"    # Ljava/lang/String;
    .param p3, "firstDateInPtnIsLaterDate"    # Z

    .prologue
    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    iput-object p1, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fIntervalPatternFirstPart:Ljava/lang/String;

    .line 192
    iput-object p2, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fIntervalPatternSecondPart:Ljava/lang/String;

    .line 193
    iput-boolean p3, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fFirstDateInPtnIsLaterDate:Z

    .line 194
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "a"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 229
    instance-of v2, p1, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 230
    check-cast v0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;

    .line 231
    .local v0, "patternInfo":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    iget-object v2, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fIntervalPatternFirstPart:Ljava/lang/String;

    iget-object v3, v0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fIntervalPatternFirstPart:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/Utility;->objectEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fIntervalPatternSecondPart:Ljava/lang/String;

    iget-object v3, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fIntervalPatternSecondPart:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/ibm/icu/impl/Utility;->objectEquals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fFirstDateInPtnIsLaterDate:Z

    iget-boolean v3, v0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fFirstDateInPtnIsLaterDate:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 235
    .end local v0    # "patternInfo":Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;
    :cond_0
    return v1
.end method

.method public firstDateInPtnIsLaterDate()Z
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fFirstDateInPtnIsLaterDate:Z

    return v0
.end method

.method public getFirstPart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fIntervalPatternFirstPart:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondPart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fIntervalPatternSecondPart:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 244
    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fIntervalPatternFirstPart:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fIntervalPatternFirstPart:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 245
    .local v0, "hash":I
    :goto_0
    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fIntervalPatternSecondPart:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 246
    iget-object v1, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fIntervalPatternSecondPart:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 248
    :cond_0
    iget-boolean v1, p0, Lcom/ibm/icu/text/DateIntervalInfo$PatternInfo;->fFirstDateInPtnIsLaterDate:Z

    if-eqz v1, :cond_1

    .line 249
    xor-int/lit8 v0, v0, -0x1

    .line 251
    :cond_1
    return v0

    .line 244
    .end local v0    # "hash":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
