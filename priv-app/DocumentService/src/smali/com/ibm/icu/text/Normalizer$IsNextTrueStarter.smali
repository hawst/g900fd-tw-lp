.class final Lcom/ibm/icu/text/Normalizer$IsNextTrueStarter;
.super Ljava/lang/Object;
.source "Normalizer.java"

# interfaces
.implements Lcom/ibm/icu/text/Normalizer$IsNextBoundary;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/Normalizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "IsNextTrueStarter"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/text/Normalizer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/ibm/icu/text/Normalizer$1;

    .prologue
    .line 2487
    invoke-direct {p0}, Lcom/ibm/icu/text/Normalizer$IsNextTrueStarter;-><init>()V

    return-void
.end method


# virtual methods
.method public isNextBoundary(Lcom/ibm/icu/text/UCharacterIterator;II[I)Z
    .locals 4
    .param p1, "src"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p2, "minC"    # I
    .param p3, "ccOrQCMask"    # I
    .param p4, "chars"    # [I

    .prologue
    .line 2495
    shl-int/lit8 v1, p3, 0x2

    and-int/lit8 v0, v1, 0xf

    .line 2496
    .local v0, "decompQCMask":I
    or-int v1, p3, v0

    invoke-static {p1, p2, v1, p4}, Lcom/ibm/icu/text/Normalizer;->access$1300(Lcom/ibm/icu/text/UCharacterIterator;II[I)J

    move-result-wide v2

    .line 2497
    .local v2, "norm32":J
    invoke-static {v2, v3, p3, v0}, Lcom/ibm/icu/impl/NormalizerImpl;->isTrueStarter(JII)Z

    move-result v1

    return v1
.end method
