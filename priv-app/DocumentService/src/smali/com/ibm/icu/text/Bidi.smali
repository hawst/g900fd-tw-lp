.class public Lcom/ibm/icu/text/Bidi;
.super Ljava/lang/Object;
.source "Bidi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/Bidi$1;,
        Lcom/ibm/icu/text/Bidi$LevState;,
        Lcom/ibm/icu/text/Bidi$ImpTabPair;,
        Lcom/ibm/icu/text/Bidi$InsertPoints;,
        Lcom/ibm/icu/text/Bidi$Point;
    }
.end annotation


# static fields
.field static final AL:B = 0xdt

.field static final AN:B = 0x5t

.field static final B:B = 0x7t

.field static final BN:B = 0x12t

.field public static final CLASS_DEFAULT:I = 0x13

.field static final CONTEXT_RTL:B = 0x40t

.field static final CONTEXT_RTL_SHIFT:B = 0x6t

.field private static final CR:C = '\r'

.field static final CS:B = 0x6t

.field public static final DIRECTION_DEFAULT_LEFT_TO_RIGHT:I = 0x7e

.field public static final DIRECTION_DEFAULT_RIGHT_TO_LEFT:I = 0x7f

.field public static final DIRECTION_LEFT_TO_RIGHT:I = 0x0

.field public static final DIRECTION_RIGHT_TO_LEFT:I = 0x1

.field public static final DO_MIRRORING:S = 0x2s

.field static final DirPropFlagE:[I

.field static final DirPropFlagLR:[I

.field static final DirPropFlagMultiRuns:I

.field static final DirPropFlagO:[I

.field static final EN:B = 0x2t

.field static final ES:B = 0x3t

.field static final ET:B = 0x4t

.field static final FIRSTALLOC:I = 0xa

.field private static final IMPTABLEVELS_COLUMNS:I = 0x8

.field private static final IMPTABLEVELS_RES:I = 0x7

.field private static final IMPTABPROPS_COLUMNS:I = 0xe

.field private static final IMPTABPROPS_RES:I = 0xd

.field public static final INSERT_LRM_FOR_NUMERIC:S = 0x4s

.field public static final KEEP_BASE_COMBINING:S = 0x1s

.field static final L:B = 0x0t

.field public static final LEVEL_DEFAULT_LTR:B = 0x7et

.field public static final LEVEL_DEFAULT_RTL:B = 0x7ft

.field public static final LEVEL_OVERRIDE:B = -0x80t

.field private static final LF:C = '\n'

.field static final LRE:B = 0xbt

.field static final LRM_AFTER:I = 0x2

.field static final LRM_BEFORE:I = 0x1

.field static final LRO:B = 0xct

.field public static final LTR:B = 0x0t

.field public static final MAP_NOWHERE:I = -0x1

.field static final MASK_BN_EXPLICIT:I

.field static final MASK_B_S:I

.field static final MASK_EMBEDDING:I

.field static final MASK_ET_NSM_BN:I

.field static final MASK_EXPLICIT:I

.field static final MASK_LRX:I

.field static final MASK_LTR:I

.field static final MASK_N:I

.field static final MASK_OVERRIDE:I

.field static final MASK_POSSIBLE_N:I

.field static final MASK_RLX:I

.field static final MASK_RTL:I

.field static final MASK_R_AL:I = 0x2002

.field static final MASK_WS:I

.field public static final MAX_EXPLICIT_LEVEL:B = 0x3dt

.field public static final MIXED:B = 0x2t

.field static final NSM:B = 0x11t

.field static final ON:B = 0xat

.field public static final OPTION_DEFAULT:I = 0x0

.field public static final OPTION_INSERT_MARKS:I = 0x1

.field public static final OPTION_REMOVE_CONTROLS:I = 0x2

.field public static final OPTION_STREAMING:I = 0x4

.field public static final OUTPUT_REVERSE:S = 0x10s

.field static final PDF:B = 0x10t

.field static final R:B = 0x1t

.field public static final REMOVE_BIDI_CONTROLS:S = 0x8s

.field static final REORDER_COUNT:S = 0x7s

.field public static final REORDER_DEFAULT:S = 0x0s

.field public static final REORDER_GROUP_NUMBERS_WITH_R:S = 0x2s

.field public static final REORDER_INVERSE_FOR_NUMBERS_SPECIAL:S = 0x6s

.field public static final REORDER_INVERSE_LIKE_DIRECT:S = 0x5s

.field public static final REORDER_INVERSE_NUMBERS_AS_L:S = 0x4s

.field static final REORDER_LAST_LOGICAL_TO_VISUAL:S = 0x1s

.field public static final REORDER_NUMBERS_SPECIAL:S = 0x1s

.field public static final REORDER_RUNS_ONLY:S = 0x3s

.field static final RLE:B = 0xet

.field static final RLM_AFTER:I = 0x8

.field static final RLM_BEFORE:I = 0x4

.field static final RLO:B = 0xft

.field public static final RTL:B = 0x1t

.field static final S:B = 0x8t

.field static final WS:B = 0x9t

.field private static final _AN:S = 0x3s

.field private static final _B:S = 0x6s

.field private static final _EN:S = 0x2s

.field private static final _L:S = 0x0s

.field private static final _ON:S = 0x4s

.field private static final _R:S = 0x1s

.field private static final _S:S = 0x5s

.field static class$com$ibm$icu$text$BidiRun:Ljava/lang/Class;

.field private static final groupProp:[S

.field private static final impAct0:[S

.field private static final impAct1:[S

.field private static final impAct2:[S

.field private static final impTabL_DEFAULT:[[B

.field private static final impTabL_GROUP_NUMBERS_WITH_R:[[B

.field private static final impTabL_INVERSE_FOR_NUMBERS_SPECIAL_WITH_MARKS:[[B

.field private static final impTabL_INVERSE_LIKE_DIRECT_WITH_MARKS:[[B

.field private static final impTabL_INVERSE_NUMBERS_AS_L:[[B

.field private static final impTabL_NUMBERS_SPECIAL:[[B

.field private static final impTabProps:[[S

.field private static final impTabR_DEFAULT:[[B

.field private static final impTabR_GROUP_NUMBERS_WITH_R:[[B

.field private static final impTabR_INVERSE_LIKE_DIRECT:[[B

.field private static final impTabR_INVERSE_LIKE_DIRECT_WITH_MARKS:[[B

.field private static final impTabR_INVERSE_NUMBERS_AS_L:[[B

.field private static final impTab_DEFAULT:Lcom/ibm/icu/text/Bidi$ImpTabPair;

.field private static final impTab_GROUP_NUMBERS_WITH_R:Lcom/ibm/icu/text/Bidi$ImpTabPair;

.field private static final impTab_INVERSE_FOR_NUMBERS_SPECIAL:Lcom/ibm/icu/text/Bidi$ImpTabPair;

.field private static final impTab_INVERSE_FOR_NUMBERS_SPECIAL_WITH_MARKS:Lcom/ibm/icu/text/Bidi$ImpTabPair;

.field private static final impTab_INVERSE_LIKE_DIRECT:Lcom/ibm/icu/text/Bidi$ImpTabPair;

.field private static final impTab_INVERSE_LIKE_DIRECT_WITH_MARKS:Lcom/ibm/icu/text/Bidi$ImpTabPair;

.field private static final impTab_INVERSE_NUMBERS_AS_L:Lcom/ibm/icu/text/Bidi$ImpTabPair;

.field private static final impTab_NUMBERS_SPECIAL:Lcom/ibm/icu/text/Bidi$ImpTabPair;


# instance fields
.field final bdp:Lcom/ibm/icu/impl/UBiDiProps;

.field controlCount:I

.field customClassifier:Lcom/ibm/icu/text/BidiClassifier;

.field defaultParaLevel:B

.field dirProps:[B

.field dirPropsMemory:[B

.field direction:B

.field flags:I

.field impTabPair:Lcom/ibm/icu/text/Bidi$ImpTabPair;

.field insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

.field isGoodLogicalToVisualRunsMap:Z

.field isInverse:Z

.field lastArabicPos:I

.field length:I

.field levels:[B

.field levelsMemory:[B

.field logicalToVisualRunsMap:[I

.field mayAllocateRuns:Z

.field mayAllocateText:Z

.field orderParagraphsLTR:Z

.field originalLength:I

.field paraBidi:Lcom/ibm/icu/text/Bidi;

.field paraCount:I

.field paraLevel:B

.field paras:[I

.field parasMemory:[I

.field reorderingMode:I

.field reorderingOptions:I

.field resultLength:I

.field runCount:I

.field runs:[Lcom/ibm/icu/text/BidiRun;

.field runsMemory:[Lcom/ibm/icu/text/BidiRun;

.field simpleParas:[I

.field simpleRuns:[Lcom/ibm/icu/text/BidiRun;

.field text:[C

.field trailingWSStart:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v3, 0xe

    const/16 v5, 0x8

    .line 1016
    const/16 v0, 0x1f

    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v0

    sput v0, Lcom/ibm/icu/text/Bidi;->DirPropFlagMultiRuns:I

    .line 1019
    new-array v0, v8, [I

    invoke-static {v6}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    aput v1, v0, v6

    invoke-static {v7}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    aput v1, v0, v7

    sput-object v0, Lcom/ibm/icu/text/Bidi;->DirPropFlagLR:[I

    .line 1020
    new-array v0, v8, [I

    const/16 v1, 0xb

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    aput v1, v0, v6

    invoke-static {v3}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    aput v1, v0, v7

    sput-object v0, Lcom/ibm/icu/text/Bidi;->DirPropFlagE:[I

    .line 1021
    new-array v0, v8, [I

    const/16 v1, 0xc

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    aput v1, v0, v6

    const/16 v1, 0xf

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    aput v1, v0, v7

    sput-object v0, Lcom/ibm/icu/text/Bidi;->DirPropFlagO:[I

    .line 1030
    invoke-static {v6}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v0

    invoke-static {v8}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    const/4 v1, 0x5

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    const/16 v1, 0xb

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    const/16 v1, 0xc

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/text/Bidi;->MASK_LTR:I

    .line 1036
    invoke-static {v7}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v0

    const/16 v1, 0xd

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    invoke-static {v3}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    const/16 v1, 0xf

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/text/Bidi;->MASK_RTL:I

    .line 1039
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v0

    const/16 v1, 0xc

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/text/Bidi;->MASK_LRX:I

    .line 1040
    invoke-static {v3}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v0

    const/16 v1, 0xf

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/text/Bidi;->MASK_RLX:I

    .line 1041
    const/16 v0, 0xc

    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v0

    const/16 v1, 0xf

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/text/Bidi;->MASK_OVERRIDE:I

    .line 1042
    sget v0, Lcom/ibm/icu/text/Bidi;->MASK_LRX:I

    sget v1, Lcom/ibm/icu/text/Bidi;->MASK_RLX:I

    or-int/2addr v0, v1

    const/16 v1, 0x10

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/text/Bidi;->MASK_EXPLICIT:I

    .line 1043
    const/16 v0, 0x12

    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v0

    sget v1, Lcom/ibm/icu/text/Bidi;->MASK_EXPLICIT:I

    or-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/text/Bidi;->MASK_BN_EXPLICIT:I

    .line 1046
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v0

    invoke-static {v5}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/text/Bidi;->MASK_B_S:I

    .line 1049
    sget v0, Lcom/ibm/icu/text/Bidi;->MASK_B_S:I

    const/16 v1, 0x9

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    sget v1, Lcom/ibm/icu/text/Bidi;->MASK_BN_EXPLICIT:I

    or-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/text/Bidi;->MASK_WS:I

    .line 1050
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v0

    sget v1, Lcom/ibm/icu/text/Bidi;->MASK_WS:I

    or-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/text/Bidi;->MASK_N:I

    .line 1054
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v0

    const/16 v1, 0x11

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    sget v1, Lcom/ibm/icu/text/Bidi;->MASK_BN_EXPLICIT:I

    or-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/text/Bidi;->MASK_ET_NSM_BN:I

    .line 1057
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    const/4 v1, 0x4

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    or-int/2addr v0, v1

    sget v1, Lcom/ibm/icu/text/Bidi;->MASK_N:I

    or-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/text/Bidi;->MASK_POSSIBLE_N:I

    .line 1064
    const/16 v0, 0x11

    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v0

    sget v1, Lcom/ibm/icu/text/Bidi;->MASK_POSSIBLE_N:I

    or-int/2addr v0, v1

    sput v0, Lcom/ibm/icu/text/Bidi;->MASK_EMBEDDING:I

    .line 2058
    const/16 v0, 0x13

    new-array v0, v0, [S

    fill-array-data v0, :array_0

    sput-object v0, Lcom/ibm/icu/text/Bidi;->groupProp:[S

    .line 2105
    const/16 v0, 0x12

    new-array v0, v0, [[S

    new-array v1, v3, [S

    fill-array-data v1, :array_1

    aput-object v1, v0, v6

    new-array v1, v3, [S

    fill-array-data v1, :array_2

    aput-object v1, v0, v7

    new-array v1, v3, [S

    fill-array-data v1, :array_3

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-array v2, v3, [S

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v3, [S

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [S

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [S

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [S

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    new-array v1, v3, [S

    fill-array-data v1, :array_9

    aput-object v1, v0, v5

    const/16 v1, 0x9

    new-array v2, v3, [S

    fill-array-data v2, :array_a

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v3, [S

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v3, [S

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v3, [S

    fill-array-data v2, :array_d

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v3, [S

    fill-array-data v2, :array_e

    aput-object v2, v0, v1

    new-array v1, v3, [S

    fill-array-data v1, :array_f

    aput-object v1, v0, v3

    const/16 v1, 0xf

    new-array v2, v3, [S

    fill-array-data v2, :array_10

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v3, [S

    fill-array-data v2, :array_11

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v3, [S

    fill-array-data v2, :array_12

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTabProps:[[S

    .line 2198
    const/4 v0, 0x6

    new-array v0, v0, [[B

    new-array v1, v5, [B

    fill-array-data v1, :array_13

    aput-object v1, v0, v6

    new-array v1, v5, [B

    fill-array-data v1, :array_14

    aput-object v1, v0, v7

    new-array v1, v5, [B

    fill-array-data v1, :array_15

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-array v2, v5, [B

    fill-array-data v2, :array_16

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [B

    fill-array-data v2, :array_17

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v5, [B

    fill-array-data v2, :array_18

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTabL_DEFAULT:[[B

    .line 2212
    const/4 v0, 0x6

    new-array v0, v0, [[B

    new-array v1, v5, [B

    fill-array-data v1, :array_19

    aput-object v1, v0, v6

    new-array v1, v5, [B

    fill-array-data v1, :array_1a

    aput-object v1, v0, v7

    new-array v1, v5, [B

    fill-array-data v1, :array_1b

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-array v2, v5, [B

    fill-array-data v2, :array_1c

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [B

    fill-array-data v2, :array_1d

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v5, [B

    fill-array-data v2, :array_1e

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTabR_DEFAULT:[[B

    .line 2226
    const/4 v0, 0x7

    new-array v0, v0, [S

    fill-array-data v0, :array_1f

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impAct0:[S

    .line 2228
    new-instance v0, Lcom/ibm/icu/text/Bidi$ImpTabPair;

    sget-object v1, Lcom/ibm/icu/text/Bidi;->impTabL_DEFAULT:[[B

    sget-object v2, Lcom/ibm/icu/text/Bidi;->impTabR_DEFAULT:[[B

    sget-object v3, Lcom/ibm/icu/text/Bidi;->impAct0:[S

    sget-object v4, Lcom/ibm/icu/text/Bidi;->impAct0:[S

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/Bidi$ImpTabPair;-><init>([[B[[B[S[S)V

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTab_DEFAULT:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    .line 2231
    const/4 v0, 0x5

    new-array v0, v0, [[B

    new-array v1, v5, [B

    fill-array-data v1, :array_20

    aput-object v1, v0, v6

    new-array v1, v5, [B

    fill-array-data v1, :array_21

    aput-object v1, v0, v7

    new-array v1, v5, [B

    fill-array-data v1, :array_22

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-array v2, v5, [B

    fill-array-data v2, :array_23

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [B

    fill-array-data v2, :array_24

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTabL_NUMBERS_SPECIAL:[[B

    .line 2242
    new-instance v0, Lcom/ibm/icu/text/Bidi$ImpTabPair;

    sget-object v1, Lcom/ibm/icu/text/Bidi;->impTabL_NUMBERS_SPECIAL:[[B

    sget-object v2, Lcom/ibm/icu/text/Bidi;->impTabR_DEFAULT:[[B

    sget-object v3, Lcom/ibm/icu/text/Bidi;->impAct0:[S

    sget-object v4, Lcom/ibm/icu/text/Bidi;->impAct0:[S

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/Bidi$ImpTabPair;-><init>([[B[[B[S[S)V

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTab_NUMBERS_SPECIAL:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    .line 2245
    const/4 v0, 0x6

    new-array v0, v0, [[B

    new-array v1, v5, [B

    fill-array-data v1, :array_25

    aput-object v1, v0, v6

    new-array v1, v5, [B

    fill-array-data v1, :array_26

    aput-object v1, v0, v7

    new-array v1, v5, [B

    fill-array-data v1, :array_27

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-array v2, v5, [B

    fill-array-data v2, :array_28

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [B

    fill-array-data v2, :array_29

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v5, [B

    fill-array-data v2, :array_2a

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTabL_GROUP_NUMBERS_WITH_R:[[B

    .line 2257
    const/4 v0, 0x5

    new-array v0, v0, [[B

    new-array v1, v5, [B

    fill-array-data v1, :array_2b

    aput-object v1, v0, v6

    new-array v1, v5, [B

    fill-array-data v1, :array_2c

    aput-object v1, v0, v7

    new-array v1, v5, [B

    fill-array-data v1, :array_2d

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-array v2, v5, [B

    fill-array-data v2, :array_2e

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [B

    fill-array-data v2, :array_2f

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTabR_GROUP_NUMBERS_WITH_R:[[B

    .line 2268
    new-instance v0, Lcom/ibm/icu/text/Bidi$ImpTabPair;

    sget-object v1, Lcom/ibm/icu/text/Bidi;->impTabL_GROUP_NUMBERS_WITH_R:[[B

    sget-object v2, Lcom/ibm/icu/text/Bidi;->impTabR_GROUP_NUMBERS_WITH_R:[[B

    sget-object v3, Lcom/ibm/icu/text/Bidi;->impAct0:[S

    sget-object v4, Lcom/ibm/icu/text/Bidi;->impAct0:[S

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/Bidi$ImpTabPair;-><init>([[B[[B[S[S)V

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTab_GROUP_NUMBERS_WITH_R:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    .line 2272
    const/4 v0, 0x6

    new-array v0, v0, [[B

    new-array v1, v5, [B

    fill-array-data v1, :array_30

    aput-object v1, v0, v6

    new-array v1, v5, [B

    fill-array-data v1, :array_31

    aput-object v1, v0, v7

    new-array v1, v5, [B

    fill-array-data v1, :array_32

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-array v2, v5, [B

    fill-array-data v2, :array_33

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [B

    fill-array-data v2, :array_34

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v5, [B

    fill-array-data v2, :array_35

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTabL_INVERSE_NUMBERS_AS_L:[[B

    .line 2284
    const/4 v0, 0x6

    new-array v0, v0, [[B

    new-array v1, v5, [B

    fill-array-data v1, :array_36

    aput-object v1, v0, v6

    new-array v1, v5, [B

    fill-array-data v1, :array_37

    aput-object v1, v0, v7

    new-array v1, v5, [B

    fill-array-data v1, :array_38

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-array v2, v5, [B

    fill-array-data v2, :array_39

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [B

    fill-array-data v2, :array_3a

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v5, [B

    fill-array-data v2, :array_3b

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTabR_INVERSE_NUMBERS_AS_L:[[B

    .line 2296
    new-instance v0, Lcom/ibm/icu/text/Bidi$ImpTabPair;

    sget-object v1, Lcom/ibm/icu/text/Bidi;->impTabL_INVERSE_NUMBERS_AS_L:[[B

    sget-object v2, Lcom/ibm/icu/text/Bidi;->impTabR_INVERSE_NUMBERS_AS_L:[[B

    sget-object v3, Lcom/ibm/icu/text/Bidi;->impAct0:[S

    sget-object v4, Lcom/ibm/icu/text/Bidi;->impAct0:[S

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/Bidi$ImpTabPair;-><init>([[B[[B[S[S)V

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTab_INVERSE_NUMBERS_AS_L:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    .line 2300
    const/4 v0, 0x7

    new-array v0, v0, [[B

    new-array v1, v5, [B

    fill-array-data v1, :array_3c

    aput-object v1, v0, v6

    new-array v1, v5, [B

    fill-array-data v1, :array_3d

    aput-object v1, v0, v7

    new-array v1, v5, [B

    fill-array-data v1, :array_3e

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-array v2, v5, [B

    fill-array-data v2, :array_3f

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [B

    fill-array-data v2, :array_40

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v5, [B

    fill-array-data v2, :array_41

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v5, [B

    fill-array-data v2, :array_42

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTabR_INVERSE_LIKE_DIRECT:[[B

    .line 2313
    const/4 v0, 0x4

    new-array v0, v0, [S

    fill-array-data v0, :array_43

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impAct1:[S

    .line 2314
    new-instance v0, Lcom/ibm/icu/text/Bidi$ImpTabPair;

    sget-object v1, Lcom/ibm/icu/text/Bidi;->impTabL_DEFAULT:[[B

    sget-object v2, Lcom/ibm/icu/text/Bidi;->impTabR_INVERSE_LIKE_DIRECT:[[B

    sget-object v3, Lcom/ibm/icu/text/Bidi;->impAct0:[S

    sget-object v4, Lcom/ibm/icu/text/Bidi;->impAct1:[S

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/Bidi$ImpTabPair;-><init>([[B[[B[S[S)V

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTab_INVERSE_LIKE_DIRECT:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    .line 2317
    const/4 v0, 0x7

    new-array v0, v0, [[B

    new-array v1, v5, [B

    fill-array-data v1, :array_44

    aput-object v1, v0, v6

    new-array v1, v5, [B

    fill-array-data v1, :array_45

    aput-object v1, v0, v7

    new-array v1, v5, [B

    fill-array-data v1, :array_46

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-array v2, v5, [B

    fill-array-data v2, :array_47

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [B

    fill-array-data v2, :array_48

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v5, [B

    fill-array-data v2, :array_49

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v5, [B

    fill-array-data v2, :array_4a

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTabL_INVERSE_LIKE_DIRECT_WITH_MARKS:[[B

    .line 2329
    const/4 v0, 0x7

    new-array v0, v0, [[B

    new-array v1, v5, [B

    fill-array-data v1, :array_4b

    aput-object v1, v0, v6

    new-array v1, v5, [B

    fill-array-data v1, :array_4c

    aput-object v1, v0, v7

    new-array v1, v5, [B

    fill-array-data v1, :array_4d

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-array v2, v5, [B

    fill-array-data v2, :array_4e

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [B

    fill-array-data v2, :array_4f

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v5, [B

    fill-array-data v2, :array_50

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v5, [B

    fill-array-data v2, :array_51

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTabR_INVERSE_LIKE_DIRECT_WITH_MARKS:[[B

    .line 2342
    const/4 v0, 0x6

    new-array v0, v0, [S

    fill-array-data v0, :array_52

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impAct2:[S

    .line 2343
    new-instance v0, Lcom/ibm/icu/text/Bidi$ImpTabPair;

    sget-object v1, Lcom/ibm/icu/text/Bidi;->impTabL_INVERSE_LIKE_DIRECT_WITH_MARKS:[[B

    sget-object v2, Lcom/ibm/icu/text/Bidi;->impTabR_INVERSE_LIKE_DIRECT_WITH_MARKS:[[B

    sget-object v3, Lcom/ibm/icu/text/Bidi;->impAct0:[S

    sget-object v4, Lcom/ibm/icu/text/Bidi;->impAct2:[S

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/Bidi$ImpTabPair;-><init>([[B[[B[S[S)V

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTab_INVERSE_LIKE_DIRECT_WITH_MARKS:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    .line 2347
    new-instance v0, Lcom/ibm/icu/text/Bidi$ImpTabPair;

    sget-object v1, Lcom/ibm/icu/text/Bidi;->impTabL_NUMBERS_SPECIAL:[[B

    sget-object v2, Lcom/ibm/icu/text/Bidi;->impTabR_INVERSE_LIKE_DIRECT:[[B

    sget-object v3, Lcom/ibm/icu/text/Bidi;->impAct0:[S

    sget-object v4, Lcom/ibm/icu/text/Bidi;->impAct1:[S

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/Bidi$ImpTabPair;-><init>([[B[[B[S[S)V

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTab_INVERSE_FOR_NUMBERS_SPECIAL:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    .line 2350
    const/4 v0, 0x5

    new-array v0, v0, [[B

    new-array v1, v5, [B

    fill-array-data v1, :array_53

    aput-object v1, v0, v6

    new-array v1, v5, [B

    fill-array-data v1, :array_54

    aput-object v1, v0, v7

    new-array v1, v5, [B

    fill-array-data v1, :array_55

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-array v2, v5, [B

    fill-array-data v2, :array_56

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [B

    fill-array-data v2, :array_57

    aput-object v2, v0, v1

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTabL_INVERSE_FOR_NUMBERS_SPECIAL_WITH_MARKS:[[B

    .line 2360
    new-instance v0, Lcom/ibm/icu/text/Bidi$ImpTabPair;

    sget-object v1, Lcom/ibm/icu/text/Bidi;->impTabL_INVERSE_FOR_NUMBERS_SPECIAL_WITH_MARKS:[[B

    sget-object v2, Lcom/ibm/icu/text/Bidi;->impTabR_INVERSE_LIKE_DIRECT_WITH_MARKS:[[B

    sget-object v3, Lcom/ibm/icu/text/Bidi;->impAct0:[S

    sget-object v4, Lcom/ibm/icu/text/Bidi;->impAct2:[S

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/ibm/icu/text/Bidi$ImpTabPair;-><init>([[B[[B[S[S)V

    sput-object v0, Lcom/ibm/icu/text/Bidi;->impTab_INVERSE_FOR_NUMBERS_SPECIAL_WITH_MARKS:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    return-void

    .line 2058
    nop

    :array_0
    .array-data 2
        0x0s
        0x1s
        0x2s
        0x7s
        0x8s
        0x3s
        0x9s
        0x6s
        0x5s
        0x4s
        0x4s
        0xas
        0xas
        0xcs
        0xas
        0xas
        0xas
        0xbs
        0xas
    .end array-data

    .line 2105
    nop

    :array_1
    .array-data 2
        0x1s
        0x2s
        0x4s
        0x5s
        0x7s
        0xfs
        0x11s
        0x7s
        0x9s
        0x7s
        0x0s
        0x7s
        0x3s
        0x4s
    .end array-data

    :array_2
    .array-data 2
        0x1s
        0x22s
        0x24s
        0x25s
        0x27s
        0x2fs
        0x31s
        0x27s
        0x29s
        0x27s
        0x1s
        0x1s
        0x23s
        0x0s
    .end array-data

    :array_3
    .array-data 2
        0x21s
        0x2s
        0x24s
        0x25s
        0x27s
        0x2fs
        0x31s
        0x27s
        0x29s
        0x27s
        0x2s
        0x2s
        0x23s
        0x1s
    .end array-data

    :array_4
    .array-data 2
        0x21s
        0x22s
        0x26s
        0x26s
        0x28s
        0x30s
        0x31s
        0x28s
        0x28s
        0x28s
        0x3s
        0x3s
        0x3s
        0x1s
    .end array-data

    :array_5
    .array-data 2
        0x21s
        0x22s
        0x4s
        0x25s
        0x27s
        0x2fs
        0x31s
        0x4as
        0xbs
        0x4as
        0x4s
        0x4s
        0x23s
        0x2s
    .end array-data

    :array_6
    .array-data 2
        0x21s
        0x22s
        0x24s
        0x5s
        0x27s
        0x2fs
        0x31s
        0x27s
        0x29s
        0x4cs
        0x5s
        0x5s
        0x23s
        0x3s
    .end array-data

    :array_7
    .array-data 2
        0x21s
        0x22s
        0x6s
        0x6s
        0x28s
        0x30s
        0x31s
        0x28s
        0x28s
        0x4ds
        0x6s
        0x6s
        0x23s
        0x3s
    .end array-data

    :array_8
    .array-data 2
        0x21s
        0x22s
        0x24s
        0x25s
        0x7s
        0x2fs
        0x31s
        0x7s
        0x4es
        0x7s
        0x7s
        0x7s
        0x23s
        0x4s
    .end array-data

    :array_9
    .array-data 2
        0x21s
        0x22s
        0x26s
        0x26s
        0x8s
        0x30s
        0x31s
        0x8s
        0x8s
        0x8s
        0x8s
        0x8s
        0x23s
        0x4s
    .end array-data

    :array_a
    .array-data 2
        0x21s
        0x22s
        0x4s
        0x25s
        0x7s
        0x2fs
        0x31s
        0x7s
        0x9s
        0x7s
        0x9s
        0x9s
        0x23s
        0x4s
    .end array-data

    :array_b
    .array-data 2
        0x61s
        0x62s
        0x4s
        0x65s
        0x87s
        0x6fs
        0x71s
        0x87s
        0x8es
        0x87s
        0xas
        0x87s
        0x63s
        0x2s
    .end array-data

    :array_c
    .array-data 2
        0x21s
        0x22s
        0x4s
        0x25s
        0x27s
        0x2fs
        0x31s
        0x27s
        0xbs
        0x27s
        0xbs
        0xbs
        0x23s
        0x2s
    .end array-data

    :array_d
    .array-data 2
        0x61s
        0x62s
        0x64s
        0x5s
        0x87s
        0x6fs
        0x71s
        0x87s
        0x8es
        0x87s
        0xcs
        0x87s
        0x63s
        0x3s
    .end array-data

    :array_e
    .array-data 2
        0x61s
        0x62s
        0x6s
        0x6s
        0x88s
        0x70s
        0x71s
        0x88s
        0x88s
        0x88s
        0xds
        0x88s
        0x63s
        0x3s
    .end array-data

    :array_f
    .array-data 2
        0x21s
        0x22s
        0x84s
        0x25s
        0x7s
        0x2fs
        0x31s
        0x7s
        0xes
        0x7s
        0xes
        0xes
        0x23s
        0x4s
    .end array-data

    :array_10
    .array-data 2
        0x21s
        0x22s
        0x24s
        0x25s
        0x27s
        0xfs
        0x31s
        0x27s
        0x29s
        0x27s
        0xfs
        0x27s
        0x23s
        0x5s
    .end array-data

    :array_11
    .array-data 2
        0x21s
        0x22s
        0x26s
        0x26s
        0x28s
        0x10s
        0x31s
        0x28s
        0x28s
        0x28s
        0x10s
        0x28s
        0x23s
        0x5s
    .end array-data

    :array_12
    .array-data 2
        0x21s
        0x22s
        0x24s
        0x25s
        0x27s
        0x2fs
        0x11s
        0x27s
        0x29s
        0x27s
        0x11s
        0x27s
        0x23s
        0x6s
    .end array-data

    .line 2198
    :array_13
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_14
    .array-data 1
        0x0t
        0x1t
        0x3t
        0x3t
        0x14t
        0x14t
        0x0t
        0x1t
    .end array-data

    :array_15
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x2t
        0x15t
        0x15t
        0x0t
        0x2t
    .end array-data

    :array_16
    .array-data 1
        0x0t
        0x1t
        0x3t
        0x3t
        0x14t
        0x14t
        0x0t
        0x2t
    .end array-data

    :array_17
    .array-data 1
        0x20t
        0x1t
        0x3t
        0x3t
        0x4t
        0x4t
        0x20t
        0x1t
    .end array-data

    :array_18
    .array-data 1
        0x20t
        0x1t
        0x20t
        0x2t
        0x5t
        0x5t
        0x20t
        0x1t
    .end array-data

    .line 2212
    :array_19
    .array-data 1
        0x1t
        0x0t
        0x2t
        0x2t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_1a
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x3t
        0x14t
        0x14t
        0x0t
        0x1t
    .end array-data

    :array_1b
    .array-data 1
        0x1t
        0x0t
        0x2t
        0x2t
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    :array_1c
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x3t
        0x5t
        0x5t
        0x0t
        0x1t
    .end array-data

    :array_1d
    .array-data 1
        0x21t
        0x0t
        0x21t
        0x3t
        0x4t
        0x4t
        0x0t
        0x0t
    .end array-data

    :array_1e
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x3t
        0x5t
        0x5t
        0x0t
        0x0t
    .end array-data

    .line 2226
    :array_1f
    .array-data 2
        0x0s
        0x1s
        0x2s
        0x3s
        0x4s
        0x5s
        0x6s
    .end array-data

    .line 2231
    nop

    :array_20
    .array-data 1
        0x0t
        0x2t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_21
    .array-data 1
        0x0t
        0x2t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x2t
    .end array-data

    :array_22
    .array-data 1
        0x0t
        0x2t
        0x4t
        0x4t
        0x13t
        0x0t
        0x0t
        0x1t
    .end array-data

    :array_23
    .array-data 1
        0x20t
        0x2t
        0x4t
        0x4t
        0x3t
        0x3t
        0x20t
        0x1t
    .end array-data

    :array_24
    .array-data 1
        0x0t
        0x2t
        0x4t
        0x4t
        0x13t
        0x13t
        0x0t
        0x2t
    .end array-data

    .line 2245
    :array_25
    .array-data 1
        0x0t
        0x3t
        0x11t
        0x11t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_26
    .array-data 1
        0x20t
        0x3t
        0x1t
        0x1t
        0x2t
        0x20t
        0x20t
        0x2t
    .end array-data

    :array_27
    .array-data 1
        0x20t
        0x3t
        0x1t
        0x1t
        0x2t
        0x20t
        0x20t
        0x1t
    .end array-data

    :array_28
    .array-data 1
        0x0t
        0x3t
        0x5t
        0x5t
        0x14t
        0x0t
        0x0t
        0x1t
    .end array-data

    :array_29
    .array-data 1
        0x20t
        0x3t
        0x5t
        0x5t
        0x4t
        0x20t
        0x20t
        0x1t
    .end array-data

    :array_2a
    .array-data 1
        0x0t
        0x3t
        0x5t
        0x5t
        0x14t
        0x0t
        0x0t
        0x2t
    .end array-data

    .line 2257
    :array_2b
    .array-data 1
        0x2t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_2c
    .array-data 1
        0x2t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    :array_2d
    .array-data 1
        0x2t
        0x0t
        0x14t
        0x14t
        0x13t
        0x0t
        0x0t
        0x1t
    .end array-data

    :array_2e
    .array-data 1
        0x22t
        0x0t
        0x4t
        0x4t
        0x3t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_2f
    .array-data 1
        0x22t
        0x0t
        0x4t
        0x4t
        0x3t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 2272
    :array_30
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_31
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x14t
        0x14t
        0x0t
        0x1t
    .end array-data

    :array_32
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x15t
        0x15t
        0x0t
        0x2t
    .end array-data

    :array_33
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x14t
        0x14t
        0x0t
        0x2t
    .end array-data

    :array_34
    .array-data 1
        0x20t
        0x1t
        0x20t
        0x20t
        0x4t
        0x4t
        0x20t
        0x1t
    .end array-data

    :array_35
    .array-data 1
        0x20t
        0x1t
        0x20t
        0x20t
        0x5t
        0x5t
        0x20t
        0x1t
    .end array-data

    .line 2284
    :array_36
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_37
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x1t
        0x14t
        0x14t
        0x0t
        0x1t
    .end array-data

    :array_38
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    :array_39
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x1t
        0x5t
        0x5t
        0x0t
        0x1t
    .end array-data

    :array_3a
    .array-data 1
        0x21t
        0x0t
        0x21t
        0x21t
        0x4t
        0x4t
        0x0t
        0x0t
    .end array-data

    :array_3b
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x1t
        0x5t
        0x5t
        0x0t
        0x0t
    .end array-data

    .line 2300
    :array_3c
    .array-data 1
        0x1t
        0x0t
        0x2t
        0x2t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_3d
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x2t
        0x13t
        0x13t
        0x0t
        0x1t
    .end array-data

    :array_3e
    .array-data 1
        0x1t
        0x0t
        0x2t
        0x2t
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    :array_3f
    .array-data 1
        0x21t
        0x30t
        0x6t
        0x4t
        0x3t
        0x3t
        0x30t
        0x0t
    .end array-data

    :array_40
    .array-data 1
        0x21t
        0x30t
        0x6t
        0x4t
        0x5t
        0x5t
        0x30t
        0x3t
    .end array-data

    :array_41
    .array-data 1
        0x21t
        0x30t
        0x6t
        0x4t
        0x5t
        0x5t
        0x30t
        0x2t
    .end array-data

    :array_42
    .array-data 1
        0x21t
        0x30t
        0x6t
        0x4t
        0x3t
        0x3t
        0x30t
        0x1t
    .end array-data

    .line 2313
    :array_43
    .array-data 2
        0x0s
        0x1s
        0xbs
        0xcs
    .end array-data

    .line 2317
    :array_44
    .array-data 1
        0x0t
        0x63t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_45
    .array-data 1
        0x0t
        0x63t
        0x0t
        0x1t
        0x12t
        0x30t
        0x0t
        0x4t
    .end array-data

    :array_46
    .array-data 1
        0x20t
        0x63t
        0x20t
        0x1t
        0x2t
        0x30t
        0x20t
        0x3t
    .end array-data

    :array_47
    .array-data 1
        0x0t
        0x63t
        0x55t
        0x56t
        0x14t
        0x30t
        0x0t
        0x3t
    .end array-data

    :array_48
    .array-data 1
        0x30t
        0x43t
        0x55t
        0x56t
        0x4t
        0x30t
        0x30t
        0x3t
    .end array-data

    :array_49
    .array-data 1
        0x30t
        0x43t
        0x5t
        0x56t
        0x14t
        0x30t
        0x30t
        0x4t
    .end array-data

    :array_4a
    .array-data 1
        0x30t
        0x43t
        0x55t
        0x6t
        0x14t
        0x30t
        0x30t
        0x4t
    .end array-data

    .line 2329
    :array_4b
    .array-data 1
        0x13t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_4c
    .array-data 1
        0x23t
        0x0t
        0x1t
        0x1t
        0x2t
        0x40t
        0x0t
        0x1t
    .end array-data

    :array_4d
    .array-data 1
        0x23t
        0x0t
        0x1t
        0x1t
        0x2t
        0x40t
        0x0t
        0x0t
    .end array-data

    :array_4e
    .array-data 1
        0x3t
        0x0t
        0x3t
        0x36t
        0x14t
        0x40t
        0x0t
        0x1t
    .end array-data

    :array_4f
    .array-data 1
        0x53t
        0x40t
        0x5t
        0x36t
        0x4t
        0x40t
        0x40t
        0x0t
    .end array-data

    :array_50
    .array-data 1
        0x53t
        0x40t
        0x5t
        0x36t
        0x4t
        0x40t
        0x40t
        0x1t
    .end array-data

    :array_51
    .array-data 1
        0x53t
        0x40t
        0x6t
        0x6t
        0x4t
        0x40t
        0x40t
        0x3t
    .end array-data

    .line 2342
    :array_52
    .array-data 2
        0x0s
        0x1s
        0x7s
        0x8s
        0x9s
        0xas
    .end array-data

    .line 2350
    :array_53
    .array-data 1
        0x0t
        0x62t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    :array_54
    .array-data 1
        0x0t
        0x62t
        0x1t
        0x1t
        0x0t
        0x30t
        0x0t
        0x4t
    .end array-data

    :array_55
    .array-data 1
        0x0t
        0x62t
        0x54t
        0x54t
        0x13t
        0x30t
        0x0t
        0x3t
    .end array-data

    :array_56
    .array-data 1
        0x30t
        0x42t
        0x54t
        0x54t
        0x3t
        0x30t
        0x30t
        0x3t
    .end array-data

    :array_57
    .array-data 1
        0x30t
        0x42t
        0x4t
        0x4t
        0x13t
        0x30t
        0x30t
        0x4t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1136
    invoke-direct {p0, v0, v0}, Lcom/ibm/icu/text/Bidi;-><init>(II)V

    .line 1137
    return-void
.end method

.method public constructor <init>(II)V
    .locals 5
    .param p1, "maxLength"    # I
    .param p2, "maxRunCount"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 913
    new-array v1, v3, [B

    iput-object v1, p0, Lcom/ibm/icu/text/Bidi;->dirPropsMemory:[B

    .line 914
    new-array v1, v3, [B

    iput-object v1, p0, Lcom/ibm/icu/text/Bidi;->levelsMemory:[B

    .line 954
    new-array v1, v3, [I

    iput-object v1, p0, Lcom/ibm/icu/text/Bidi;->parasMemory:[I

    .line 959
    new-array v1, v3, [I

    aput v4, v1, v4

    iput-object v1, p0, Lcom/ibm/icu/text/Bidi;->simpleParas:[I

    .line 963
    new-array v1, v4, [Lcom/ibm/icu/text/BidiRun;

    iput-object v1, p0, Lcom/ibm/icu/text/Bidi;->runsMemory:[Lcom/ibm/icu/text/BidiRun;

    .line 967
    new-array v1, v3, [Lcom/ibm/icu/text/BidiRun;

    new-instance v2, Lcom/ibm/icu/text/BidiRun;

    invoke-direct {v2}, Lcom/ibm/icu/text/BidiRun;-><init>()V

    aput-object v2, v1, v4

    iput-object v1, p0, Lcom/ibm/icu/text/Bidi;->simpleRuns:[Lcom/ibm/icu/text/BidiRun;

    .line 975
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/ibm/icu/text/Bidi;->customClassifier:Lcom/ibm/icu/text/BidiClassifier;

    .line 978
    new-instance v1, Lcom/ibm/icu/text/Bidi$InsertPoints;

    invoke-direct {v1, p0}, Lcom/ibm/icu/text/Bidi$InsertPoints;-><init>(Lcom/ibm/icu/text/Bidi;)V

    iput-object v1, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    .line 1173
    if-ltz p1, :cond_0

    if-gez p2, :cond_1

    .line 1174
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 1196
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/ibm/icu/impl/UBiDiProps;->getSingleton()Lcom/ibm/icu/impl/UBiDiProps;

    move-result-object v1

    iput-object v1, p0, Lcom/ibm/icu/text/Bidi;->bdp:Lcom/ibm/icu/impl/UBiDiProps;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1203
    if-lez p1, :cond_3

    .line 1204
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/Bidi;->getInitialDirPropsMemory(I)V

    .line 1205
    invoke-direct {p0, p1}, Lcom/ibm/icu/text/Bidi;->getInitialLevelsMemory(I)V

    .line 1210
    :goto_0
    if-lez p2, :cond_4

    .line 1212
    if-le p2, v3, :cond_2

    .line 1213
    invoke-direct {p0, p2}, Lcom/ibm/icu/text/Bidi;->getInitialRunsMemory(I)V

    .line 1218
    :cond_2
    :goto_1
    return-void

    .line 1198
    :catch_0
    move-exception v0

    .line 1199
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/util/MissingResourceException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "(BidiProps)"

    const-string/jumbo v4, ""

    invoke-direct {v1, v2, v3, v4}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v1

    .line 1207
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    iput-boolean v3, p0, Lcom/ibm/icu/text/Bidi;->mayAllocateText:Z

    goto :goto_0

    .line 1216
    :cond_4
    iput-boolean v3, p0, Lcom/ibm/icu/text/Bidi;->mayAllocateRuns:Z

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 7
    .param p1, "paragraph"    # Ljava/lang/String;
    .param p2, "flags"    # I

    .prologue
    const/4 v2, 0x0

    .line 4413
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    move-object v0, p0

    move v4, v2

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/ibm/icu/text/Bidi;-><init>([CI[BIII)V

    .line 4414
    return-void
.end method

.method public constructor <init>(Ljava/text/AttributedCharacterIterator;)V
    .locals 0
    .param p1, "paragraph"    # Ljava/text/AttributedCharacterIterator;

    .prologue
    .line 4447
    invoke-direct {p0}, Lcom/ibm/icu/text/Bidi;-><init>()V

    .line 4448
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/Bidi;->setPara(Ljava/text/AttributedCharacterIterator;)V

    .line 4449
    return-void
.end method

.method public constructor <init>([CI[BIII)V
    .locals 6
    .param p1, "text"    # [C
    .param p2, "textStart"    # I
    .param p3, "embeddings"    # [B
    .param p4, "embStart"    # I
    .param p5, "paragraphLength"    # I
    .param p6, "flags"    # I

    .prologue
    .line 4494
    invoke-direct {p0}, Lcom/ibm/icu/text/Bidi;-><init>()V

    .line 4496
    sparse-switch p6, :sswitch_data_0

    .line 4499
    const/4 v3, 0x0

    .line 4512
    .local v3, "paraLvl":B
    :goto_0
    if-nez p3, :cond_1

    .line 4513
    const/4 v2, 0x0

    .line 4530
    .local v2, "paraEmbeddings":[B
    :cond_0
    if-nez p2, :cond_4

    if-nez p4, :cond_4

    array-length v5, p1

    if-ne p5, v5, :cond_4

    .line 4531
    invoke-virtual {p0, p1, v3, v2}, Lcom/ibm/icu/text/Bidi;->setPara([CB[B)V

    .line 4537
    :goto_1
    return-void

    .line 4502
    .end local v2    # "paraEmbeddings":[B
    .end local v3    # "paraLvl":B
    :sswitch_0
    const/4 v3, 0x1

    .line 4503
    .restart local v3    # "paraLvl":B
    goto :goto_0

    .line 4505
    .end local v3    # "paraLvl":B
    :sswitch_1
    const/16 v3, 0x7e

    .line 4506
    .restart local v3    # "paraLvl":B
    goto :goto_0

    .line 4508
    .end local v3    # "paraLvl":B
    :sswitch_2
    const/16 v3, 0x7f

    .restart local v3    # "paraLvl":B
    goto :goto_0

    .line 4515
    :cond_1
    new-array v2, p5, [B

    .line 4517
    .restart local v2    # "paraEmbeddings":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    if-ge v0, p5, :cond_0

    .line 4518
    add-int v5, v0, p4

    aget-byte v1, p3, v5

    .line 4519
    .local v1, "lev":B
    if-gez v1, :cond_3

    .line 4520
    neg-int v5, v1

    or-int/lit8 v5, v5, -0x80

    int-to-byte v1, v5

    .line 4527
    :cond_2
    :goto_3
    aput-byte v1, v2, v0

    .line 4517
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 4521
    :cond_3
    if-nez v1, :cond_2

    .line 4522
    move v1, v3

    .line 4523
    const/16 v5, 0x3d

    if-le v3, v5, :cond_2

    .line 4524
    and-int/lit8 v5, v1, 0x1

    int-to-byte v1, v5

    goto :goto_3

    .line 4533
    .end local v0    # "i":I
    .end local v1    # "lev":B
    :cond_4
    new-array v4, p5, [C

    .line 4534
    .local v4, "paraText":[C
    const/4 v5, 0x0

    invoke-static {p1, p2, v4, v5, p5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4535
    invoke-virtual {p0, v4, v3, v2}, Lcom/ibm/icu/text/Bidi;->setPara([CB[B)V

    goto :goto_1

    .line 4496
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x7e -> :sswitch_1
        0x7f -> :sswitch_2
    .end sparse-switch
.end method

.method static DirPropFlag(B)I
    .locals 1
    .param p0, "dir"    # B

    .prologue
    .line 990
    const/4 v0, 0x1

    shl-int/2addr v0, p0

    return v0
.end method

.method static final DirPropFlagE(B)I
    .locals 2
    .param p0, "level"    # B

    .prologue
    .line 1024
    sget-object v0, Lcom/ibm/icu/text/Bidi;->DirPropFlagE:[I

    and-int/lit8 v1, p0, 0x1

    aget v0, v0, v1

    return v0
.end method

.method static final DirPropFlagLR(B)I
    .locals 2
    .param p0, "level"    # B

    .prologue
    .line 1023
    sget-object v0, Lcom/ibm/icu/text/Bidi;->DirPropFlagLR:[I

    and-int/lit8 v1, p0, 0x1

    aget v0, v0, v1

    return v0
.end method

.method static DirPropFlagNC(B)I
    .locals 2
    .param p0, "dir"    # B

    .prologue
    .line 1013
    const/4 v0, 0x1

    and-int/lit8 v1, p0, -0x41

    shl-int/2addr v0, v1

    return v0
.end method

.method static final DirPropFlagO(B)I
    .locals 2
    .param p0, "level"    # B

    .prologue
    .line 1025
    sget-object v0, Lcom/ibm/icu/text/Bidi;->DirPropFlagO:[I

    and-int/lit8 v1, p0, 0x1

    aget v0, v0, v1

    return v0
.end method

.method private static GetAction(B)S
    .locals 1
    .param p0, "cell"    # B

    .prologue
    .line 2149
    shr-int/lit8 v0, p0, 0x4

    int-to-short v0, v0

    return v0
.end method

.method private static GetActionProps(S)S
    .locals 1
    .param p0, "cell"    # S

    .prologue
    .line 2055
    shr-int/lit8 v0, p0, 0x5

    int-to-short v0, v0

    return v0
.end method

.method static GetLRFromLevel(B)B
    .locals 1
    .param p0, "level"    # B

    .prologue
    .line 1071
    and-int/lit8 v0, p0, 0x1

    int-to-byte v0, v0

    return v0
.end method

.method private static GetState(B)S
    .locals 1
    .param p0, "cell"    # B

    .prologue
    .line 2148
    and-int/lit8 v0, p0, 0xf

    int-to-short v0, v0

    return v0
.end method

.method private static GetStateProps(S)S
    .locals 1
    .param p0, "cell"    # S

    .prologue
    .line 2052
    and-int/lit8 v0, p0, 0x1f

    int-to-short v0, v0

    return v0
.end method

.method static IsBidiControlChar(I)Z
    .locals 2
    .param p0, "c"    # I

    .prologue
    .line 1089
    and-int/lit8 v0, p0, -0x4

    const/16 v1, 0x200c

    if-eq v0, v1, :cond_0

    const/16 v0, 0x202a

    if-lt p0, v0, :cond_1

    const/16 v0, 0x202e

    if-gt p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static IsDefaultLevel(B)Z
    .locals 2
    .param p0, "level"    # B

    .prologue
    .line 1076
    and-int/lit8 v0, p0, 0x7e

    const/16 v1, 0x7e

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static NoContextRTL(B)B
    .locals 1
    .param p0, "dir"    # B

    .prologue
    .line 1005
    and-int/lit8 v0, p0, -0x41

    int-to-byte v0, v0

    return v0
.end method

.method private addPoint(II)V
    .locals 6
    .param p1, "pos"    # I
    .param p2, "flag"    # I

    .prologue
    const/4 v5, 0x0

    .line 2383
    new-instance v1, Lcom/ibm/icu/text/Bidi$Point;

    invoke-direct {v1, p0}, Lcom/ibm/icu/text/Bidi$Point;-><init>(Lcom/ibm/icu/text/Bidi;)V

    .line 2385
    .local v1, "point":Lcom/ibm/icu/text/Bidi$Point;
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget-object v3, v3, Lcom/ibm/icu/text/Bidi$InsertPoints;->points:[Lcom/ibm/icu/text/Bidi$Point;

    array-length v0, v3

    .line 2386
    .local v0, "len":I
    if-nez v0, :cond_0

    .line 2387
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    const/16 v4, 0xa

    new-array v4, v4, [Lcom/ibm/icu/text/Bidi$Point;

    iput-object v4, v3, Lcom/ibm/icu/text/Bidi$InsertPoints;->points:[Lcom/ibm/icu/text/Bidi$Point;

    .line 2388
    const/16 v0, 0xa

    .line 2390
    :cond_0
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v3, v3, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    if-lt v3, v0, :cond_1

    .line 2391
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget-object v2, v3, Lcom/ibm/icu/text/Bidi$InsertPoints;->points:[Lcom/ibm/icu/text/Bidi$Point;

    .line 2392
    .local v2, "savePoints":[Lcom/ibm/icu/text/Bidi$Point;
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    mul-int/lit8 v4, v0, 0x2

    new-array v4, v4, [Lcom/ibm/icu/text/Bidi$Point;

    iput-object v4, v3, Lcom/ibm/icu/text/Bidi$InsertPoints;->points:[Lcom/ibm/icu/text/Bidi$Point;

    .line 2393
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget-object v3, v3, Lcom/ibm/icu/text/Bidi$InsertPoints;->points:[Lcom/ibm/icu/text/Bidi$Point;

    invoke-static {v2, v5, v3, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2395
    .end local v2    # "savePoints":[Lcom/ibm/icu/text/Bidi$Point;
    :cond_1
    iput p1, v1, Lcom/ibm/icu/text/Bidi$Point;->pos:I

    .line 2396
    iput p2, v1, Lcom/ibm/icu/text/Bidi$Point;->flag:I

    .line 2397
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget-object v3, v3, Lcom/ibm/icu/text/Bidi$InsertPoints;->points:[Lcom/ibm/icu/text/Bidi$Point;

    iget-object v4, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v4, v4, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    aput-object v1, v3, v4

    .line 2398
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v4, v3, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    .line 2399
    return-void
.end method

.method private adjustWSLevels()V
    .locals 7

    .prologue
    const/4 v6, 0x7

    const/4 v5, 0x0

    .line 2714
    iget v2, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    sget v3, Lcom/ibm/icu/text/Bidi;->MASK_WS:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_5

    .line 2716
    iget v1, p0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    .line 2717
    .local v1, "i":I
    :cond_0
    :goto_0
    if-lez v1, :cond_5

    .line 2719
    :goto_1
    if-lez v1, :cond_2

    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    add-int/lit8 v1, v1, -0x1

    aget-byte v2, v2, v1

    invoke-static {v2}, Lcom/ibm/icu/text/Bidi;->DirPropFlagNC(B)I

    move-result v0

    .local v0, "flag":I
    sget v2, Lcom/ibm/icu/text/Bidi;->MASK_WS:I

    and-int/2addr v2, v0

    if-eqz v2, :cond_2

    .line 2720
    iget-boolean v2, p0, Lcom/ibm/icu/text/Bidi;->orderParagraphsLTR:Z

    if-eqz v2, :cond_1

    invoke-static {v6}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v2

    and-int/2addr v2, v0

    if-eqz v2, :cond_1

    .line 2721
    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aput-byte v5, v2, v1

    goto :goto_1

    .line 2723
    :cond_1
    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v3

    aput-byte v3, v2, v1

    goto :goto_1

    .line 2729
    .end local v0    # "flag":I
    :cond_2
    :goto_2
    if-lez v1, :cond_0

    .line 2730
    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    add-int/lit8 v1, v1, -0x1

    aget-byte v2, v2, v1

    invoke-static {v2}, Lcom/ibm/icu/text/Bidi;->DirPropFlagNC(B)I

    move-result v0

    .line 2731
    .restart local v0    # "flag":I
    sget v2, Lcom/ibm/icu/text/Bidi;->MASK_BN_EXPLICIT:I

    and-int/2addr v2, v0

    if-eqz v2, :cond_3

    .line 2732
    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    add-int/lit8 v4, v1, 0x1

    aget-byte v3, v3, v4

    aput-byte v3, v2, v1

    goto :goto_2

    .line 2733
    :cond_3
    iget-boolean v2, p0, Lcom/ibm/icu/text/Bidi;->orderParagraphsLTR:Z

    if-eqz v2, :cond_4

    invoke-static {v6}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v2

    and-int/2addr v2, v0

    if-eqz v2, :cond_4

    .line 2734
    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aput-byte v5, v2, v1

    goto :goto_0

    .line 2736
    :cond_4
    sget v2, Lcom/ibm/icu/text/Bidi;->MASK_B_S:I

    and-int/2addr v2, v0

    if-eqz v2, :cond_2

    .line 2737
    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v3

    aput-byte v3, v2, v1

    goto :goto_0

    .line 2743
    .end local v0    # "flag":I
    .end local v1    # "i":I
    :cond_5
    return-void
.end method

.method private checkExplicitLevels()B
    .locals 9

    .prologue
    const/4 v8, 0x7

    .line 1997
    const/4 v5, 0x0

    iput v5, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 1999
    const/4 v3, 0x0

    .line 2001
    .local v3, "paraIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v5, p0, Lcom/ibm/icu/text/Bidi;->length:I

    if-ge v1, v5, :cond_6

    .line 2002
    iget-object v5, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aget-byte v2, v5, v1

    .line 2003
    .local v2, "level":B
    iget-object v5, p0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    aget-byte v5, v5, v1

    invoke-static {v5}, Lcom/ibm/icu/text/Bidi;->NoContextRTL(B)B

    move-result v0

    .line 2004
    .local v0, "dirProp":B
    and-int/lit8 v5, v2, -0x80

    if-eqz v5, :cond_2

    .line 2006
    and-int/lit8 v5, v2, 0x7f

    int-to-byte v2, v5

    .line 2007
    iget v5, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    invoke-static {v2}, Lcom/ibm/icu/text/Bidi;->DirPropFlagO(B)I

    move-result v6

    or-int/2addr v5, v6

    iput v5, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 2012
    :goto_1
    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v5

    if-ge v2, v5, :cond_0

    if-nez v2, :cond_1

    if-ne v0, v8, :cond_1

    :cond_0
    const/16 v5, 0x3d

    if-ge v5, v2, :cond_3

    .line 2016
    :cond_1
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v7, "level "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string/jumbo v7, " out of bounds at "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2010
    :cond_2
    iget v5, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    invoke-static {v2}, Lcom/ibm/icu/text/Bidi;->DirPropFlagE(B)I

    move-result v6

    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v7

    or-int/2addr v6, v7

    or-int/2addr v5, v6

    iput v5, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    goto :goto_1

    .line 2019
    :cond_3
    if-ne v0, v8, :cond_5

    add-int/lit8 v5, v1, 0x1

    iget v6, p0, Lcom/ibm/icu/text/Bidi;->length:I

    if-ge v5, v6, :cond_5

    .line 2020
    iget-object v5, p0, Lcom/ibm/icu/text/Bidi;->text:[C

    aget-char v5, v5, v1

    const/16 v6, 0xd

    if-ne v5, v6, :cond_4

    iget-object v5, p0, Lcom/ibm/icu/text/Bidi;->text:[C

    add-int/lit8 v6, v1, 0x1

    aget-char v5, v5, v6

    const/16 v6, 0xa

    if-eq v5, v6, :cond_5

    .line 2021
    :cond_4
    iget-object v5, p0, Lcom/ibm/icu/text/Bidi;->paras:[I

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "paraIndex":I
    .local v4, "paraIndex":I
    add-int/lit8 v6, v1, 0x1

    aput v6, v5, v3

    move v3, v4

    .line 2001
    .end local v4    # "paraIndex":I
    .restart local v3    # "paraIndex":I
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 2025
    .end local v0    # "dirProp":B
    .end local v2    # "level":B
    :cond_6
    iget v5, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    sget v6, Lcom/ibm/icu/text/Bidi;->MASK_EMBEDDING:I

    and-int/2addr v5, v6

    if-eqz v5, :cond_7

    .line 2026
    iget v5, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    iget-byte v6, p0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    invoke-static {v6}, Lcom/ibm/icu/text/Bidi;->DirPropFlagLR(B)I

    move-result v6

    or-int/2addr v5, v6

    iput v5, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 2030
    :cond_7
    invoke-direct {p0}, Lcom/ibm/icu/text/Bidi;->directionFromFlags()B

    move-result v5

    return v5
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 1280
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    .local v0, "x1":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private directionFromFlags()B
    .locals 2

    .prologue
    .line 1763
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    sget v1, Lcom/ibm/icu/text/Bidi;->MASK_RTL:I

    and-int/2addr v0, v1

    if-nez v0, :cond_1

    iget v0, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    const/4 v1, 0x5

    invoke-static {v1}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    sget v1, Lcom/ibm/icu/text/Bidi;->MASK_POSSIBLE_N:I

    and-int/2addr v0, v1

    if-nez v0, :cond_1

    .line 1766
    :cond_0
    const/4 v0, 0x0

    .line 1770
    :goto_0
    return v0

    .line 1767
    :cond_1
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    sget v1, Lcom/ibm/icu/text/Bidi;->MASK_LTR:I

    and-int/2addr v0, v1

    if-nez v0, :cond_2

    .line 1768
    const/4 v0, 0x1

    goto :goto_0

    .line 1770
    :cond_2
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private getDirProps()V
    .locals 23

    .prologue
    .line 1613
    const/4 v7, 0x0

    .line 1614
    .local v7, "i":I
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 1617
    const/4 v15, 0x0

    .line 1618
    .local v15, "paraDirDefault":B
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/ibm/icu/text/Bidi;->IsDefaultLevel(B)Z

    move-result v10

    .line 1621
    .local v10, "isDefaultLevel":Z
    if-eqz v10, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    :cond_0
    const/4 v11, 0x1

    .line 1624
    .local v11, "isDefaultLevelInverse":Z
    :goto_0
    const/16 v20, -0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->lastArabicPos:I

    .line 1625
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->controlCount:I

    .line 1626
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    move/from16 v20, v0

    and-int/lit8 v20, v20, 0x2

    if-eqz v20, :cond_6

    const/16 v17, 0x1

    .line 1628
    .local v17, "removeBidiControls":Z
    :goto_1
    const/4 v5, 0x0

    .line 1629
    .local v5, "NOT_CONTEXTUAL":I
    const/4 v4, 0x1

    .line 1630
    .local v4, "LOOKING_FOR_STRONG":I
    const/4 v3, 0x2

    .line 1633
    .local v3, "FOUND_STRONG_CHAR":I
    const/16 v16, 0x0

    .line 1636
    .local v16, "paraStart":I
    const/4 v12, 0x0

    .line 1637
    .local v12, "lastStrongDir":B
    const/4 v13, 0x0

    .line 1639
    .local v13, "lastStrongLTR":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    move/from16 v20, v0

    and-int/lit8 v20, v20, 0x4

    if-lez v20, :cond_1

    .line 1640
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->length:I

    .line 1641
    const/4 v13, 0x0

    .line 1643
    :cond_1
    if-eqz v10, :cond_8

    .line 1644
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    move/from16 v20, v0

    and-int/lit8 v20, v20, 0x1

    if-eqz v20, :cond_7

    const/16 v15, 0x40

    .line 1645
    :goto_2
    move v14, v15

    .line 1646
    .local v14, "paraDir":B
    move v12, v15

    .line 1647
    const/16 v18, 0x1

    .line 1659
    .local v18, "state":I
    :goto_3
    const/4 v7, 0x0

    :cond_2
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->originalLength:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v7, v0, :cond_14

    .line 1660
    move v8, v7

    .line 1661
    .local v8, "i0":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->text:[C

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->originalLength:I

    move/from16 v22, v0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v0, v1, v2, v7}, Lcom/ibm/icu/text/UTF16;->charAt([CIII)I

    move-result v19

    .line 1662
    .local v19, "uchar":I
    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v20

    add-int v7, v7, v20

    .line 1663
    add-int/lit8 v9, v7, -0x1

    .line 1665
    .local v9, "i1":I
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/Bidi;->getCustomizedClass(I)I

    move-result v20

    move/from16 v0, v20

    int-to-byte v6, v0

    .line 1666
    .local v6, "dirProp":B
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->flags:I

    move/from16 v20, v0

    invoke-static {v6}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v21

    or-int v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 1667
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    move-object/from16 v20, v0

    or-int v21, v6, v14

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    aput-byte v21, v20, v9

    .line 1668
    if-le v9, v8, :cond_4

    .line 1669
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->flags:I

    move/from16 v20, v0

    const/16 v21, 0x12

    invoke-static/range {v21 .. v21}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v21

    or-int v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 1671
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    move-object/from16 v20, v0

    add-int/lit8 v9, v9, -0x1

    or-int/lit8 v21, v14, 0x12

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    aput-byte v21, v20, v9

    .line 1672
    if-gt v9, v8, :cond_3

    .line 1674
    :cond_4
    const/16 v20, 0x1

    move/from16 v0, v18

    move/from16 v1, v20

    if-ne v0, v1, :cond_b

    .line 1675
    if-nez v6, :cond_9

    .line 1676
    const/16 v18, 0x2

    .line 1677
    if-eqz v14, :cond_2

    .line 1678
    const/4 v14, 0x0

    .line 1679
    move/from16 v9, v16

    :goto_5
    if-ge v9, v7, :cond_2

    .line 1680
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    move-object/from16 v20, v0

    aget-byte v21, v20, v9

    and-int/lit8 v21, v21, -0x41

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    aput-byte v21, v20, v9

    .line 1679
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 1621
    .end local v3    # "FOUND_STRONG_CHAR":I
    .end local v4    # "LOOKING_FOR_STRONG":I
    .end local v5    # "NOT_CONTEXTUAL":I
    .end local v6    # "dirProp":B
    .end local v8    # "i0":I
    .end local v9    # "i1":I
    .end local v11    # "isDefaultLevelInverse":Z
    .end local v12    # "lastStrongDir":B
    .end local v13    # "lastStrongLTR":I
    .end local v14    # "paraDir":B
    .end local v16    # "paraStart":I
    .end local v17    # "removeBidiControls":Z
    .end local v18    # "state":I
    .end local v19    # "uchar":I
    :cond_5
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 1626
    .restart local v11    # "isDefaultLevelInverse":Z
    :cond_6
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 1644
    .restart local v3    # "FOUND_STRONG_CHAR":I
    .restart local v4    # "LOOKING_FOR_STRONG":I
    .restart local v5    # "NOT_CONTEXTUAL":I
    .restart local v12    # "lastStrongDir":B
    .restart local v13    # "lastStrongLTR":I
    .restart local v16    # "paraStart":I
    .restart local v17    # "removeBidiControls":Z
    :cond_7
    const/4 v15, 0x0

    goto/16 :goto_2

    .line 1649
    :cond_8
    const/16 v18, 0x0

    .line 1650
    .restart local v18    # "state":I
    const/4 v14, 0x0

    .restart local v14    # "paraDir":B
    goto/16 :goto_3

    .line 1685
    .restart local v6    # "dirProp":B
    .restart local v8    # "i0":I
    .restart local v9    # "i1":I
    .restart local v19    # "uchar":I
    :cond_9
    const/16 v20, 0x1

    move/from16 v0, v20

    if-eq v6, v0, :cond_a

    const/16 v20, 0xd

    move/from16 v0, v20

    if-ne v6, v0, :cond_b

    .line 1686
    :cond_a
    const/16 v18, 0x2

    .line 1687
    if-nez v14, :cond_2

    .line 1688
    const/16 v14, 0x40

    .line 1689
    move/from16 v9, v16

    :goto_6
    if-ge v9, v7, :cond_2

    .line 1690
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    move-object/from16 v20, v0

    aget-byte v21, v20, v9

    or-int/lit8 v21, v21, 0x40

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    aput-byte v21, v20, v9

    .line 1689
    add-int/lit8 v9, v9, 0x1

    goto :goto_6

    .line 1696
    :cond_b
    if-nez v6, :cond_d

    .line 1697
    const/4 v12, 0x0

    .line 1698
    move v13, v7

    .line 1728
    :cond_c
    :goto_7
    if-eqz v17, :cond_2

    invoke-static/range {v19 .. v19}, Lcom/ibm/icu/text/Bidi;->IsBidiControlChar(I)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 1729
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->controlCount:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->controlCount:I

    goto/16 :goto_4

    .line 1700
    :cond_d
    const/16 v20, 0x1

    move/from16 v0, v20

    if-ne v6, v0, :cond_e

    .line 1701
    const/16 v12, 0x40

    .line 1702
    goto :goto_7

    .line 1703
    :cond_e
    const/16 v20, 0xd

    move/from16 v0, v20

    if-ne v6, v0, :cond_f

    .line 1704
    const/16 v12, 0x40

    .line 1705
    add-int/lit8 v20, v7, -0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->lastArabicPos:I

    goto :goto_7

    .line 1707
    :cond_f
    const/16 v20, 0x7

    move/from16 v0, v20

    if-ne v6, v0, :cond_c

    .line 1708
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    move/from16 v20, v0

    and-int/lit8 v20, v20, 0x4

    if-eqz v20, :cond_10

    .line 1709
    move-object/from16 v0, p0

    iput v7, v0, Lcom/ibm/icu/text/Bidi;->length:I

    .line 1711
    :cond_10
    if-eqz v11, :cond_11

    const/16 v20, 0x40

    move/from16 v0, v20

    if-ne v12, v0, :cond_11

    if-eq v14, v12, :cond_11

    .line 1712
    :goto_8
    move/from16 v0, v16

    if-ge v0, v7, :cond_11

    .line 1713
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    move-object/from16 v20, v0

    aget-byte v21, v20, v16

    or-int/lit8 v21, v21, 0x40

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    aput-byte v21, v20, v16

    .line 1712
    add-int/lit8 v16, v16, 0x1

    goto :goto_8

    .line 1716
    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->originalLength:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v7, v0, :cond_c

    .line 1717
    const/16 v20, 0xd

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->text:[C

    move-object/from16 v20, v0

    aget-char v20, v20, v7

    const/16 v21, 0xa

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_13

    .line 1718
    :cond_12
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->paraCount:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->paraCount:I

    .line 1720
    :cond_13
    if-eqz v10, :cond_c

    .line 1721
    const/16 v18, 0x1

    .line 1722
    move/from16 v16, v7

    .line 1723
    move v14, v15

    .line 1724
    move v12, v15

    goto/16 :goto_7

    .line 1732
    .end local v6    # "dirProp":B
    .end local v8    # "i0":I
    .end local v9    # "i1":I
    .end local v19    # "uchar":I
    :cond_14
    if-eqz v11, :cond_15

    const/16 v20, 0x40

    move/from16 v0, v20

    if-ne v12, v0, :cond_15

    if-eq v14, v12, :cond_15

    .line 1733
    move/from16 v9, v16

    .restart local v9    # "i1":I
    :goto_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->originalLength:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v9, v0, :cond_15

    .line 1734
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    move-object/from16 v20, v0

    aget-byte v21, v20, v9

    or-int/lit8 v21, v21, 0x40

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    aput-byte v21, v20, v9

    .line 1733
    add-int/lit8 v9, v9, 0x1

    goto :goto_9

    .line 1737
    .end local v9    # "i1":I
    :cond_15
    if-eqz v10, :cond_16

    .line 1738
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-byte v0, v1, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    .line 1740
    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    move/from16 v20, v0

    and-int/lit8 v20, v20, 0x4

    if-lez v20, :cond_18

    .line 1741
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->length:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-le v13, v0, :cond_17

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v20

    if-nez v20, :cond_17

    .line 1743
    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->length:I

    .line 1745
    :cond_17
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->length:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->originalLength:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_18

    .line 1746
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->paraCount:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->paraCount:I

    .line 1751
    :cond_18
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->flags:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/ibm/icu/text/Bidi;->DirPropFlagLR(B)I

    move-result v21

    or-int v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 1753
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/ibm/icu/text/Bidi;->orderParagraphsLTR:Z

    move/from16 v20, v0

    if-eqz v20, :cond_19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->flags:I

    move/from16 v20, v0

    const/16 v21, 0x7

    invoke-static/range {v21 .. v21}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v21

    and-int v20, v20, v21

    if-eqz v20, :cond_19

    .line 1754
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->flags:I

    move/from16 v20, v0

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v21

    or-int v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 1756
    :cond_19
    return-void
.end method

.method private getDirPropsMemory(ZI)V
    .locals 7
    .param p1, "mayAllocate"    # Z
    .param p2, "len"    # I

    .prologue
    .line 1258
    const-string/jumbo v1, "DirProps"

    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->dirPropsMemory:[B

    sget-object v3, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    move-object v0, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/Bidi;->getMemory(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;ZI)Ljava/lang/Object;

    move-result-object v6

    .line 1259
    .local v6, "array":Ljava/lang/Object;
    check-cast v6, [B

    .end local v6    # "array":Ljava/lang/Object;
    check-cast v6, [B

    iput-object v6, p0, Lcom/ibm/icu/text/Bidi;->dirPropsMemory:[B

    .line 1260
    return-void
.end method

.method private getInitialDirPropsMemory(I)V
    .locals 1
    .param p1, "len"    # I

    .prologue
    .line 1292
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/Bidi;->getDirPropsMemory(ZI)V

    .line 1293
    return-void
.end method

.method private getInitialLevelsMemory(I)V
    .locals 1
    .param p1, "len"    # I

    .prologue
    .line 1297
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/Bidi;->getLevelsMemory(ZI)V

    .line 1298
    return-void
.end method

.method private getInitialParasMemory(I)V
    .locals 7
    .param p1, "len"    # I

    .prologue
    .line 1302
    const-string/jumbo v1, "Paras"

    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->parasMemory:[I

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x1

    move-object v0, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/Bidi;->getMemory(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;ZI)Ljava/lang/Object;

    move-result-object v6

    .line 1303
    .local v6, "array":Ljava/lang/Object;
    check-cast v6, [I

    .end local v6    # "array":Ljava/lang/Object;
    check-cast v6, [I

    iput-object v6, p0, Lcom/ibm/icu/text/Bidi;->parasMemory:[I

    .line 1304
    return-void
.end method

.method private getInitialRunsMemory(I)V
    .locals 1
    .param p1, "len"    # I

    .prologue
    .line 1308
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/Bidi;->getRunsMemory(ZI)V

    .line 1309
    return-void
.end method

.method private getLevelsMemory(ZI)V
    .locals 7
    .param p1, "mayAllocate"    # Z
    .param p2, "len"    # I

    .prologue
    .line 1269
    const-string/jumbo v1, "Levels"

    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->levelsMemory:[B

    sget-object v3, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    move-object v0, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/Bidi;->getMemory(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;ZI)Ljava/lang/Object;

    move-result-object v6

    .line 1270
    .local v6, "array":Ljava/lang/Object;
    check-cast v6, [B

    .end local v6    # "array":Ljava/lang/Object;
    check-cast v6, [B

    iput-object v6, p0, Lcom/ibm/icu/text/Bidi;->levelsMemory:[B

    .line 1271
    return-void
.end method

.method private getMemory(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;ZI)Ljava/lang/Object;
    .locals 5
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "array"    # Ljava/lang/Object;
    .param p3, "arrayClass"    # Ljava/lang/Class;
    .param p4, "mayAllocate"    # Z
    .param p5, "sizeNeeded"    # I

    .prologue
    .line 1230
    invoke-static {p2}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    .line 1233
    .local v1, "len":I
    if-ne p5, v1, :cond_1

    .line 1248
    .end local p2    # "array":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object p2

    .line 1236
    .restart local p2    # "array":Ljava/lang/Object;
    :cond_1
    if-nez p4, :cond_2

    .line 1238
    if-le p5, v1, :cond_0

    .line 1241
    new-instance v2, Ljava/lang/OutOfMemoryError;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "Failed to allocate memory for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1248
    :cond_2
    :try_start_0
    invoke-static {p3, p5}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    goto :goto_0

    .line 1249
    :catch_0
    move-exception v0

    .line 1250
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/OutOfMemoryError;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v4, "Failed to allocate memory for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private getRunsMemory(ZI)V
    .locals 7
    .param p1, "mayAllocate"    # Z
    .param p2, "len"    # I

    .prologue
    .line 1280
    const-string/jumbo v1, "Runs"

    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->runsMemory:[Lcom/ibm/icu/text/BidiRun;

    sget-object v0, Lcom/ibm/icu/text/Bidi;->class$com$ibm$icu$text$BidiRun:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string/jumbo v0, "com.ibm.icu.text.BidiRun"

    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lcom/ibm/icu/text/Bidi;->class$com$ibm$icu$text$BidiRun:Ljava/lang/Class;

    :goto_0
    move-object v0, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/ibm/icu/text/Bidi;->getMemory(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;ZI)Ljava/lang/Object;

    move-result-object v6

    .line 1281
    .local v6, "array":Ljava/lang/Object;
    check-cast v6, [Lcom/ibm/icu/text/BidiRun;

    .end local v6    # "array":Ljava/lang/Object;
    check-cast v6, [Lcom/ibm/icu/text/BidiRun;

    iput-object v6, p0, Lcom/ibm/icu/text/Bidi;->runsMemory:[Lcom/ibm/icu/text/BidiRun;

    .line 1282
    return-void

    .line 1280
    :cond_0
    sget-object v3, Lcom/ibm/icu/text/Bidi;->class$com$ibm$icu$text$BidiRun:Ljava/lang/Class;

    goto :goto_0
.end method

.method public static invertMap([I)[I
    .locals 1
    .param p0, "srcMap"    # [I

    .prologue
    .line 4351
    if-nez p0, :cond_0

    .line 4352
    const/4 v0, 0x0

    .line 4354
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/ibm/icu/text/BidiLine;->invertMap([I)[I

    move-result-object v0

    goto :goto_0
.end method

.method private processPropertySeq(Lcom/ibm/icu/text/Bidi$LevState;SII)V
    .locals 13
    .param p1, "levState"    # Lcom/ibm/icu/text/Bidi$LevState;
    .param p2, "_prop"    # S
    .param p3, "start"    # I
    .param p4, "limit"    # I

    .prologue
    .line 2419
    iget-object v5, p1, Lcom/ibm/icu/text/Bidi$LevState;->impTab:[[B

    .line 2420
    .local v5, "impTab":[[B
    iget-object v4, p1, Lcom/ibm/icu/text/Bidi$LevState;->impAct:[S

    .line 2425
    .local v4, "impAct":[S
    move/from16 v10, p3

    .line 2426
    .local v10, "start0":I
    iget-short v9, p1, Lcom/ibm/icu/text/Bidi$LevState;->state:S

    .line 2427
    .local v9, "oldStateSeq":S
    aget-object v11, v5, v9

    aget-byte v3, v11, p2

    .line 2428
    .local v3, "cell":B
    invoke-static {v3}, Lcom/ibm/icu/text/Bidi;->GetState(B)S

    move-result v11

    iput-short v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->state:S

    .line 2429
    invoke-static {v3}, Lcom/ibm/icu/text/Bidi;->GetAction(B)S

    move-result v11

    aget-short v1, v4, v11

    .line 2430
    .local v1, "actionSeq":S
    iget-short v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->state:S

    aget-object v11, v5, v11

    const/4 v12, 0x7

    aget-byte v2, v11, v12

    .line 2432
    .local v2, "addLevel":B
    if-eqz v1, :cond_0

    .line 2433
    packed-switch v1, :pswitch_data_0

    .line 2585
    new-instance v11, Ljava/lang/IllegalStateException;

    const-string/jumbo v12, "Internal ICU error in processPropertySeq"

    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 2435
    :pswitch_0
    iput v10, p1, Lcom/ibm/icu/text/Bidi$LevState;->startON:I

    .line 2588
    :cond_0
    :goto_0
    if-nez v2, :cond_1

    move/from16 v0, p3

    if-ge v0, v10, :cond_13

    .line 2589
    :cond_1
    iget-byte v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->runLevel:B

    add-int/2addr v11, v2

    int-to-byte v8, v11

    .line 2590
    .local v8, "level":B
    move/from16 v6, p3

    .local v6, "k":I
    :goto_1
    move/from16 v0, p4

    if-ge v6, v0, :cond_13

    .line 2591
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aput-byte v8, v11, v6

    .line 2590
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 2439
    .end local v6    # "k":I
    .end local v8    # "level":B
    :pswitch_1
    iget v0, p1, Lcom/ibm/icu/text/Bidi$LevState;->startON:I

    move/from16 p3, v0

    .line 2440
    goto :goto_0

    .line 2444
    :pswitch_2
    iget v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->startL2EN:I

    if-ltz v11, :cond_2

    .line 2445
    iget v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->startL2EN:I

    const/4 v12, 0x1

    invoke-direct {p0, v11, v12}, Lcom/ibm/icu/text/Bidi;->addPoint(II)V

    .line 2447
    :cond_2
    const/4 v11, -0x1

    iput v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->startL2EN:I

    .line 2449
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget-object v11, v11, Lcom/ibm/icu/text/Bidi$InsertPoints;->points:[Lcom/ibm/icu/text/Bidi$Point;

    array-length v11, v11

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v11, v11, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v12, v12, Lcom/ibm/icu/text/Bidi$InsertPoints;->confirmed:I

    if-gt v11, v12, :cond_5

    .line 2452
    :cond_3
    const/4 v11, -0x1

    iput v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->lastStrongRTL:I

    .line 2454
    aget-object v11, v5, v9

    const/4 v12, 0x7

    aget-byte v8, v11, v12

    .line 2455
    .restart local v8    # "level":B
    and-int/lit8 v11, v8, 0x1

    if-eqz v11, :cond_4

    iget v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->startON:I

    if-lez v11, :cond_4

    .line 2456
    iget v0, p1, Lcom/ibm/icu/text/Bidi$LevState;->startON:I

    move/from16 p3, v0

    .line 2458
    :cond_4
    const/4 v11, 0x5

    if-ne p2, v11, :cond_0

    .line 2459
    const/4 v11, 0x1

    invoke-direct {p0, v10, v11}, Lcom/ibm/icu/text/Bidi;->addPoint(II)V

    .line 2460
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v12, v12, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    iput v12, v11, Lcom/ibm/icu/text/Bidi$InsertPoints;->confirmed:I

    goto :goto_0

    .line 2465
    .end local v8    # "level":B
    :cond_5
    iget v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->lastStrongRTL:I

    add-int/lit8 v6, v11, 0x1

    .restart local v6    # "k":I
    :goto_2
    if-ge v6, v10, :cond_6

    .line 2467
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aget-byte v12, v12, v6

    add-int/lit8 v12, v12, -0x2

    and-int/lit8 v12, v12, -0x2

    int-to-byte v12, v12

    aput-byte v12, v11, v6

    .line 2465
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 2470
    :cond_6
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v12, v12, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    iput v12, v11, Lcom/ibm/icu/text/Bidi$InsertPoints;->confirmed:I

    .line 2471
    const/4 v11, -0x1

    iput v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->lastStrongRTL:I

    .line 2472
    const/4 v11, 0x5

    if-ne p2, v11, :cond_0

    .line 2473
    const/4 v11, 0x1

    invoke-direct {p0, v10, v11}, Lcom/ibm/icu/text/Bidi;->addPoint(II)V

    .line 2474
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v12, v12, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    iput v12, v11, Lcom/ibm/icu/text/Bidi$InsertPoints;->confirmed:I

    goto/16 :goto_0

    .line 2480
    .end local v6    # "k":I
    :pswitch_3
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget-object v11, v11, Lcom/ibm/icu/text/Bidi$InsertPoints;->points:[Lcom/ibm/icu/text/Bidi$Point;

    array-length v11, v11

    if-lez v11, :cond_7

    .line 2482
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v12, v12, Lcom/ibm/icu/text/Bidi$InsertPoints;->confirmed:I

    iput v12, v11, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    .line 2483
    :cond_7
    const/4 v11, -0x1

    iput v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->startON:I

    .line 2484
    const/4 v11, -0x1

    iput v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->startL2EN:I

    .line 2485
    add-int/lit8 v11, p4, -0x1

    iput v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->lastStrongRTL:I

    goto/16 :goto_0

    .line 2490
    :pswitch_4
    const/4 v11, 0x3

    if-ne p2, v11, :cond_a

    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    aget-byte v11, v11, v10

    invoke-static {v11}, Lcom/ibm/icu/text/Bidi;->NoContextRTL(B)B

    move-result v11

    const/4 v12, 0x5

    if-ne v11, v12, :cond_a

    iget v11, p0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    const/4 v12, 0x6

    if-eq v11, v12, :cond_a

    .line 2494
    iget v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->startL2EN:I

    const/4 v12, -0x1

    if-ne v11, v12, :cond_8

    .line 2496
    add-int/lit8 v11, p4, -0x1

    iput v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->lastStrongRTL:I

    goto/16 :goto_0

    .line 2499
    :cond_8
    iget v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->startL2EN:I

    if-ltz v11, :cond_9

    .line 2500
    iget v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->startL2EN:I

    const/4 v12, 0x1

    invoke-direct {p0, v11, v12}, Lcom/ibm/icu/text/Bidi;->addPoint(II)V

    .line 2501
    const/4 v11, -0x2

    iput v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->startL2EN:I

    .line 2504
    :cond_9
    const/4 v11, 0x1

    invoke-direct {p0, v10, v11}, Lcom/ibm/icu/text/Bidi;->addPoint(II)V

    goto/16 :goto_0

    .line 2508
    :cond_a
    iget v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->startL2EN:I

    const/4 v12, -0x1

    if-ne v11, v12, :cond_0

    .line 2509
    iput v10, p1, Lcom/ibm/icu/text/Bidi$LevState;->startL2EN:I

    goto/16 :goto_0

    .line 2514
    :pswitch_5
    add-int/lit8 v11, p4, -0x1

    iput v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->lastStrongRTL:I

    .line 2515
    const/4 v11, -0x1

    iput v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->startON:I

    goto/16 :goto_0

    .line 2520
    :pswitch_6
    add-int/lit8 v6, v10, -0x1

    .restart local v6    # "k":I
    :goto_3
    if-ltz v6, :cond_b

    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aget-byte v11, v11, v6

    and-int/lit8 v11, v11, 0x1

    if-nez v11, :cond_b

    add-int/lit8 v6, v6, -0x1

    goto :goto_3

    .line 2522
    :cond_b
    if-ltz v6, :cond_c

    .line 2523
    const/4 v11, 0x4

    invoke-direct {p0, v6, v11}, Lcom/ibm/icu/text/Bidi;->addPoint(II)V

    .line 2524
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v12, v12, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    iput v12, v11, Lcom/ibm/icu/text/Bidi$InsertPoints;->confirmed:I

    .line 2526
    :cond_c
    iput v10, p1, Lcom/ibm/icu/text/Bidi$LevState;->startON:I

    goto/16 :goto_0

    .line 2532
    .end local v6    # "k":I
    :pswitch_7
    const/4 v11, 0x1

    invoke-direct {p0, v10, v11}, Lcom/ibm/icu/text/Bidi;->addPoint(II)V

    .line 2533
    const/4 v11, 0x2

    invoke-direct {p0, v10, v11}, Lcom/ibm/icu/text/Bidi;->addPoint(II)V

    goto/16 :goto_0

    .line 2538
    :pswitch_8
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v12, v12, Lcom/ibm/icu/text/Bidi$InsertPoints;->confirmed:I

    iput v12, v11, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    .line 2539
    const/4 v11, 0x5

    if-ne p2, v11, :cond_0

    .line 2540
    const/4 v11, 0x4

    invoke-direct {p0, v10, v11}, Lcom/ibm/icu/text/Bidi;->addPoint(II)V

    .line 2541
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v12, v12, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    iput v12, v11, Lcom/ibm/icu/text/Bidi$InsertPoints;->confirmed:I

    goto/16 :goto_0

    .line 2546
    :pswitch_9
    iget-byte v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->runLevel:B

    add-int/2addr v11, v2

    int-to-byte v8, v11

    .line 2547
    .restart local v8    # "level":B
    iget v6, p1, Lcom/ibm/icu/text/Bidi$LevState;->startON:I

    .restart local v6    # "k":I
    :goto_4
    if-ge v6, v10, :cond_e

    .line 2548
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aget-byte v11, v11, v6

    if-ge v11, v8, :cond_d

    .line 2549
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aput-byte v8, v11, v6

    .line 2547
    :cond_d
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 2552
    :cond_e
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v12, v12, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    iput v12, v11, Lcom/ibm/icu/text/Bidi$InsertPoints;->confirmed:I

    .line 2553
    iput v10, p1, Lcom/ibm/icu/text/Bidi$LevState;->startON:I

    goto/16 :goto_0

    .line 2557
    .end local v6    # "k":I
    .end local v8    # "level":B
    :pswitch_a
    iget-byte v8, p1, Lcom/ibm/icu/text/Bidi$LevState;->runLevel:B

    .line 2558
    .restart local v8    # "level":B
    add-int/lit8 v6, v10, -0x1

    .restart local v6    # "k":I
    :goto_5
    iget v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->startON:I

    if-lt v6, v11, :cond_0

    .line 2559
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aget-byte v11, v11, v6

    add-int/lit8 v12, v8, 0x3

    if-ne v11, v12, :cond_10

    .line 2560
    :goto_6
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aget-byte v11, v11, v6

    add-int/lit8 v12, v8, 0x3

    if-ne v11, v12, :cond_f

    .line 2561
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    add-int/lit8 v7, v6, -0x1

    .end local v6    # "k":I
    .local v7, "k":I
    aget-byte v12, v11, v6

    add-int/lit8 v12, v12, -0x2

    int-to-byte v12, v12

    aput-byte v12, v11, v6

    move v6, v7

    .line 2562
    .end local v7    # "k":I
    .restart local v6    # "k":I
    goto :goto_6

    .line 2563
    :cond_f
    :goto_7
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aget-byte v11, v11, v6

    if-ne v11, v8, :cond_10

    .line 2564
    add-int/lit8 v6, v6, -0x1

    .line 2565
    goto :goto_7

    .line 2567
    :cond_10
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aget-byte v11, v11, v6

    add-int/lit8 v12, v8, 0x2

    if-ne v11, v12, :cond_11

    .line 2568
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aput-byte v8, v11, v6

    .line 2558
    :goto_8
    add-int/lit8 v6, v6, -0x1

    goto :goto_5

    .line 2571
    :cond_11
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    add-int/lit8 v12, v8, 0x1

    int-to-byte v12, v12

    aput-byte v12, v11, v6

    goto :goto_8

    .line 2576
    .end local v6    # "k":I
    .end local v8    # "level":B
    :pswitch_b
    iget-byte v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->runLevel:B

    add-int/lit8 v11, v11, 0x1

    int-to-byte v8, v11

    .line 2577
    .restart local v8    # "level":B
    add-int/lit8 v6, v10, -0x1

    .restart local v6    # "k":I
    :goto_9
    iget v11, p1, Lcom/ibm/icu/text/Bidi$LevState;->startON:I

    if-lt v6, v11, :cond_0

    .line 2578
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aget-byte v11, v11, v6

    if-le v11, v8, :cond_12

    .line 2579
    iget-object v11, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aget-byte v12, v11, v6

    add-int/lit8 v12, v12, -0x2

    int-to-byte v12, v12

    aput-byte v12, v11, v6

    .line 2577
    :cond_12
    add-int/lit8 v6, v6, -0x1

    goto :goto_9

    .line 2594
    .end local v6    # "k":I
    .end local v8    # "level":B
    :cond_13
    return-void

    .line 2433
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static reorderLogical([B)[I
    .locals 1
    .param p0, "levels"    # [B

    .prologue
    .line 4287
    invoke-static {p0}, Lcom/ibm/icu/text/BidiLine;->reorderLogical([B)[I

    move-result-object v0

    return-object v0
.end method

.method public static reorderVisual([B)[I
    .locals 1
    .param p0, "levels"    # [B

    .prologue
    .line 4311
    invoke-static {p0}, Lcom/ibm/icu/text/BidiLine;->reorderVisual([B)[I

    move-result-object v0

    return-object v0
.end method

.method public static reorderVisually([BI[Ljava/lang/Object;II)V
    .locals 6
    .param p0, "levels"    # [B
    .param p1, "levelStart"    # I
    .param p2, "objects"    # [Ljava/lang/Object;
    .param p3, "objectStart"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v4, 0x0

    .line 4820
    new-array v2, p4, [B

    .line 4821
    .local v2, "reorderLevels":[B
    invoke-static {p0, p1, v2, v4, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4822
    invoke-static {v2}, Lcom/ibm/icu/text/Bidi;->reorderVisual([B)[I

    move-result-object v1

    .line 4823
    .local v1, "indexMap":[I
    new-array v3, p4, [Ljava/lang/Object;

    .line 4824
    .local v3, "temp":[Ljava/lang/Object;
    invoke-static {p2, p3, v3, v4, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4825
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p4, :cond_0

    .line 4826
    add-int v4, p3, v0

    aget v5, v1, v0

    aget-object v5, v3, v5

    aput-object v5, p2, v4

    .line 4825
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4828
    :cond_0
    return-void
.end method

.method public static requiresBidi([CII)Z
    .locals 5
    .param p0, "text"    # [C
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    const/4 v2, 0x1

    .line 4782
    const v0, 0xe022

    .line 4788
    .local v0, "RTLMask":I
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_1

    .line 4789
    aget-char v3, p0, v1

    invoke-static {v3}, Lcom/ibm/icu/lang/UCharacter;->getDirection(I)I

    move-result v3

    shl-int v3, v2, v3

    const v4, 0xe022

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    .line 4793
    :goto_1
    return v2

    .line 4788
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4793
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private resolveExplicitLevels()B
    .locals 15

    .prologue
    .line 1827
    const/4 v5, 0x0

    .line 1829
    .local v5, "i":I
    const/4 v12, 0x0

    invoke-virtual {p0, v12}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v6

    .line 1832
    .local v6, "level":B
    const/4 v8, 0x0

    .line 1835
    .local v8, "paraIndex":I
    invoke-direct {p0}, Lcom/ibm/icu/text/Bidi;->directionFromFlags()B

    move-result v3

    .line 1839
    .local v3, "dirct":B
    const/4 v12, 0x2

    if-eq v3, v12, :cond_1

    iget v12, p0, Lcom/ibm/icu/text/Bidi;->paraCount:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_1

    .line 1981
    :cond_0
    :goto_0
    return v3

    .line 1841
    :cond_1
    iget v12, p0, Lcom/ibm/icu/text/Bidi;->paraCount:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_3

    iget v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    sget v13, Lcom/ibm/icu/text/Bidi;->MASK_EXPLICIT:I

    and-int/2addr v12, v13

    if-eqz v12, :cond_2

    iget v12, p0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    const/4 v13, 0x1

    if-le v12, v13, :cond_3

    .line 1848
    :cond_2
    const/4 v5, 0x0

    :goto_1
    iget v12, p0, Lcom/ibm/icu/text/Bidi;->length:I

    if-ge v5, v12, :cond_0

    .line 1849
    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aput-byte v6, v12, v5

    .line 1848
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1856
    :cond_3
    move v4, v6

    .line 1858
    .local v4, "embeddingLevel":B
    const/4 v11, 0x0

    .line 1860
    .local v11, "stackTop":B
    const/16 v12, 0x3d

    new-array v10, v12, [B

    .line 1861
    .local v10, "stack":[B
    const/4 v0, 0x0

    .line 1862
    .local v0, "countOver60":I
    const/4 v1, 0x0

    .line 1865
    .local v1, "countOver61":I
    const/4 v12, 0x0

    iput v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 1867
    const/4 v5, 0x0

    :goto_2
    iget v12, p0, Lcom/ibm/icu/text/Bidi;->length:I

    if-ge v5, v12, :cond_11

    .line 1868
    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    aget-byte v12, v12, v5

    invoke-static {v12}, Lcom/ibm/icu/text/Bidi;->NoContextRTL(B)B

    move-result v2

    .line 1869
    .local v2, "dirProp":B
    packed-switch v2, :pswitch_data_0

    .line 1948
    :pswitch_0
    if-eq v6, v4, :cond_4

    .line 1949
    move v6, v4

    .line 1950
    and-int/lit8 v12, v6, -0x80

    if-eqz v12, :cond_10

    .line 1951
    iget v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    invoke-static {v6}, Lcom/ibm/icu/text/Bidi;->DirPropFlagO(B)I

    move-result v13

    sget v14, Lcom/ibm/icu/text/Bidi;->DirPropFlagMultiRuns:I

    or-int/2addr v13, v14

    or-int/2addr v12, v13

    iput v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 1956
    :cond_4
    :goto_3
    and-int/lit8 v12, v6, -0x80

    if-nez v12, :cond_5

    .line 1957
    iget v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    invoke-static {v2}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v13

    or-int/2addr v12, v13

    iput v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 1966
    :cond_5
    :goto_4
    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aput-byte v6, v12, v5

    .line 1867
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1873
    :pswitch_1
    add-int/lit8 v12, v4, 0x2

    and-int/lit8 v12, v12, 0x7e

    int-to-byte v7, v12

    .line 1874
    .local v7, "newLevel":B
    const/16 v12, 0x3d

    if-gt v7, v12, :cond_7

    .line 1875
    aput-byte v4, v10, v11

    .line 1876
    add-int/lit8 v12, v11, 0x1

    int-to-byte v11, v12

    .line 1877
    move v4, v7

    .line 1878
    const/16 v12, 0xc

    if-ne v2, v12, :cond_6

    .line 1879
    or-int/lit8 v12, v4, -0x80

    int-to-byte v4, v12

    .line 1890
    :cond_6
    :goto_5
    iget v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    const/16 v13, 0x12

    invoke-static {v13}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v13

    or-int/2addr v12, v13

    iput v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    goto :goto_4

    .line 1885
    :cond_7
    and-int/lit8 v12, v4, 0x7f

    const/16 v13, 0x3d

    if-ne v12, v13, :cond_8

    .line 1886
    add-int/lit8 v1, v1, 0x1

    .line 1887
    goto :goto_5

    .line 1888
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1895
    .end local v7    # "newLevel":B
    :pswitch_2
    and-int/lit8 v12, v4, 0x7f

    add-int/lit8 v12, v12, 0x1

    or-int/lit8 v12, v12, 0x1

    int-to-byte v7, v12

    .line 1896
    .restart local v7    # "newLevel":B
    const/16 v12, 0x3d

    if-gt v7, v12, :cond_a

    .line 1897
    aput-byte v4, v10, v11

    .line 1898
    add-int/lit8 v12, v11, 0x1

    int-to-byte v11, v12

    .line 1899
    move v4, v7

    .line 1900
    const/16 v12, 0xf

    if-ne v2, v12, :cond_9

    .line 1901
    or-int/lit8 v12, v4, -0x80

    int-to-byte v4, v12

    .line 1910
    :cond_9
    :goto_6
    iget v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    const/16 v13, 0x12

    invoke-static {v13}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v13

    or-int/2addr v12, v13

    iput v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    goto :goto_4

    .line 1908
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1915
    .end local v7    # "newLevel":B
    :pswitch_3
    if-lez v1, :cond_c

    .line 1916
    add-int/lit8 v1, v1, -0x1

    .line 1926
    :cond_b
    :goto_7
    iget v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    const/16 v13, 0x12

    invoke-static {v13}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v13

    or-int/2addr v12, v13

    iput v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    goto :goto_4

    .line 1917
    :cond_c
    if-lez v0, :cond_d

    and-int/lit8 v12, v4, 0x7f

    const/16 v13, 0x3d

    if-eq v12, v13, :cond_d

    .line 1919
    add-int/lit8 v0, v0, -0x1

    .line 1920
    goto :goto_7

    :cond_d
    if-lez v11, :cond_b

    .line 1922
    add-int/lit8 v12, v11, -0x1

    int-to-byte v11, v12

    .line 1923
    aget-byte v4, v10, v11

    goto :goto_7

    .line 1929
    :pswitch_4
    const/4 v11, 0x0

    .line 1930
    const/4 v0, 0x0

    .line 1931
    const/4 v1, 0x0

    .line 1932
    invoke-virtual {p0, v5}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v6

    .line 1933
    add-int/lit8 v12, v5, 0x1

    iget v13, p0, Lcom/ibm/icu/text/Bidi;->length:I

    if-ge v12, v13, :cond_f

    .line 1934
    add-int/lit8 v12, v5, 0x1

    invoke-virtual {p0, v12}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v4

    .line 1935
    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->text:[C

    aget-char v12, v12, v5

    const/16 v13, 0xd

    if-ne v12, v13, :cond_e

    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->text:[C

    add-int/lit8 v13, v5, 0x1

    aget-char v12, v12, v13

    const/16 v13, 0xa

    if-eq v12, v13, :cond_f

    .line 1936
    :cond_e
    iget-object v12, p0, Lcom/ibm/icu/text/Bidi;->paras:[I

    add-int/lit8 v9, v8, 0x1

    .end local v8    # "paraIndex":I
    .local v9, "paraIndex":I
    add-int/lit8 v13, v5, 0x1

    aput v13, v12, v8

    move v8, v9

    .line 1939
    .end local v9    # "paraIndex":I
    .restart local v8    # "paraIndex":I
    :cond_f
    iget v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    const/4 v13, 0x7

    invoke-static {v13}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v13

    or-int/2addr v12, v13

    iput v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    goto/16 :goto_4

    .line 1944
    :pswitch_5
    iget v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    const/16 v13, 0x12

    invoke-static {v13}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v13

    or-int/2addr v12, v13

    iput v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    goto/16 :goto_4

    .line 1953
    :cond_10
    iget v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    invoke-static {v6}, Lcom/ibm/icu/text/Bidi;->DirPropFlagE(B)I

    move-result v13

    sget v14, Lcom/ibm/icu/text/Bidi;->DirPropFlagMultiRuns:I

    or-int/2addr v13, v14

    or-int/2addr v12, v13

    iput v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    goto/16 :goto_3

    .line 1968
    .end local v2    # "dirProp":B
    :cond_11
    iget v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    sget v13, Lcom/ibm/icu/text/Bidi;->MASK_EMBEDDING:I

    and-int/2addr v12, v13

    if-eqz v12, :cond_12

    .line 1969
    iget v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    iget-byte v13, p0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    invoke-static {v13}, Lcom/ibm/icu/text/Bidi;->DirPropFlagLR(B)I

    move-result v13

    or-int/2addr v12, v13

    iput v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 1971
    :cond_12
    iget-boolean v12, p0, Lcom/ibm/icu/text/Bidi;->orderParagraphsLTR:Z

    if-eqz v12, :cond_13

    iget v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    const/4 v13, 0x7

    invoke-static {v13}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v13

    and-int/2addr v12, v13

    if-eqz v12, :cond_13

    .line 1972
    iget v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    const/4 v13, 0x0

    invoke-static {v13}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v13

    or-int/2addr v12, v13

    iput v12, p0, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 1978
    :cond_13
    invoke-direct {p0}, Lcom/ibm/icu/text/Bidi;->directionFromFlags()B

    move-result v3

    goto/16 :goto_0

    .line 1869
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method private resolveImplicitLevels(IISS)V
    .locals 22
    .param p1, "start"    # I
    .param p2, "limit"    # I
    .param p3, "sor"    # S
    .param p4, "eor"    # S

    .prologue
    .line 2598
    new-instance v10, Lcom/ibm/icu/text/Bidi$LevState;

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v10, v0, v1}, Lcom/ibm/icu/text/Bidi$LevState;-><init>(Lcom/ibm/icu/text/Bidi;Lcom/ibm/icu/text/Bidi$1;)V

    .line 2603
    .local v10, "levState":Lcom/ibm/icu/text/Bidi$LevState;
    const/4 v12, 0x1

    .line 2604
    .local v12, "nextStrongProp":S
    const/4 v11, -0x1

    .line 2614
    .local v11, "nextStrongPos":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->lastArabicPos:I

    move/from16 v20, v0

    move/from16 v0, p1

    move/from16 v1, v20

    if-ge v0, v1, :cond_2

    invoke-virtual/range {p0 .. p1}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v20

    and-int/lit8 v20, v20, 0x1

    if-lez v20, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    move/from16 v20, v0

    const/16 v21, 0x5

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    move/from16 v20, v0

    const/16 v21, 0x6

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v8, 0x1

    .line 2618
    .local v8, "inverseRTL":Z
    :goto_0
    const/16 v20, -0x1

    move/from16 v0, v20

    iput v0, v10, Lcom/ibm/icu/text/Bidi$LevState;->startL2EN:I

    .line 2619
    const/16 v20, -0x1

    move/from16 v0, v20

    iput v0, v10, Lcom/ibm/icu/text/Bidi$LevState;->lastStrongRTL:I

    .line 2620
    const/16 v20, 0x0

    move/from16 v0, v20

    iput-short v0, v10, Lcom/ibm/icu/text/Bidi$LevState;->state:S

    .line 2621
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->levels:[B

    move-object/from16 v20, v0

    aget-byte v20, v20, p1

    move/from16 v0, v20

    iput-byte v0, v10, Lcom/ibm/icu/text/Bidi$LevState;->runLevel:B

    .line 2622
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->impTabPair:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi$ImpTabPair;->imptab:[[[B

    move-object/from16 v20, v0

    iget-byte v0, v10, Lcom/ibm/icu/text/Bidi$LevState;->runLevel:B

    move/from16 v21, v0

    and-int/lit8 v21, v21, 0x1

    aget-object v20, v20, v21

    move-object/from16 v0, v20

    iput-object v0, v10, Lcom/ibm/icu/text/Bidi$LevState;->impTab:[[B

    .line 2623
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->impTabPair:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi$ImpTabPair;->impact:[[S

    move-object/from16 v20, v0

    iget-byte v0, v10, Lcom/ibm/icu/text/Bidi$LevState;->runLevel:B

    move/from16 v21, v0

    and-int/lit8 v21, v21, 0x1

    aget-object v20, v20, v21

    move-object/from16 v0, v20

    iput-object v0, v10, Lcom/ibm/icu/text/Bidi$LevState;->impAct:[S

    .line 2624
    move-object/from16 v0, p0

    move/from16 v1, p3

    move/from16 v2, p1

    move/from16 v3, p1

    invoke-direct {v0, v10, v1, v2, v3}, Lcom/ibm/icu/text/Bidi;->processPropertySeq(Lcom/ibm/icu/text/Bidi$LevState;SII)V

    .line 2626
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    move-object/from16 v20, v0

    aget-byte v20, v20, p1

    const/16 v21, 0x11

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3

    .line 2627
    add-int/lit8 v20, p3, 0x1

    move/from16 v0, v20

    int-to-short v0, v0

    move/from16 v19, v0

    .line 2631
    .local v19, "stateImp":S
    :goto_1
    move/from16 v17, p1

    .line 2632
    .local v17, "start1":I
    const/16 v18, 0x0

    .line 2634
    .local v18, "start2":I
    move/from16 v7, p1

    .local v7, "i":I
    :goto_2
    move/from16 v0, p2

    if-gt v7, v0, :cond_b

    .line 2635
    move/from16 v0, p2

    if-lt v7, v0, :cond_4

    .line 2636
    move/from16 v6, p4

    .line 2666
    .local v6, "gprop":S
    :goto_3
    move/from16 v13, v19

    .line 2667
    .local v13, "oldStateImp":S
    sget-object v20, Lcom/ibm/icu/text/Bidi;->impTabProps:[[S

    aget-object v20, v20, v13

    aget-short v5, v20, v6

    .line 2668
    .local v5, "cell":S
    invoke-static {v5}, Lcom/ibm/icu/text/Bidi;->GetStateProps(S)S

    move-result v19

    .line 2669
    invoke-static {v5}, Lcom/ibm/icu/text/Bidi;->GetActionProps(S)S

    move-result v4

    .line 2670
    .local v4, "actionImp":S
    move/from16 v0, p2

    if-ne v7, v0, :cond_1

    if-nez v4, :cond_1

    .line 2672
    const/4 v4, 0x1

    .line 2674
    :cond_1
    if-eqz v4, :cond_a

    .line 2675
    sget-object v20, Lcom/ibm/icu/text/Bidi;->impTabProps:[[S

    aget-object v20, v20, v13

    const/16 v21, 0xd

    aget-short v16, v20, v21

    .line 2676
    .local v16, "resProp":S
    packed-switch v4, :pswitch_data_0

    .line 2695
    new-instance v20, Ljava/lang/IllegalStateException;

    const-string/jumbo v21, "Internal ICU error in resolveImplicitLevels"

    invoke-direct/range {v20 .. v21}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 2614
    .end local v4    # "actionImp":S
    .end local v5    # "cell":S
    .end local v6    # "gprop":S
    .end local v7    # "i":I
    .end local v8    # "inverseRTL":Z
    .end local v13    # "oldStateImp":S
    .end local v16    # "resProp":S
    .end local v17    # "start1":I
    .end local v18    # "start2":I
    .end local v19    # "stateImp":S
    :cond_2
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 2629
    .restart local v8    # "inverseRTL":Z
    :cond_3
    const/16 v19, 0x0

    .restart local v19    # "stateImp":S
    goto :goto_1

    .line 2639
    .restart local v7    # "i":I
    .restart local v17    # "start1":I
    .restart local v18    # "start2":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    move-object/from16 v20, v0

    aget-byte v20, v20, v7

    invoke-static/range {v20 .. v20}, Lcom/ibm/icu/text/Bidi;->NoContextRTL(B)B

    move-result v20

    move/from16 v0, v20

    int-to-short v14, v0

    .line 2640
    .local v14, "prop":S
    if-eqz v8, :cond_5

    .line 2641
    const/16 v20, 0xd

    move/from16 v0, v20

    if-ne v14, v0, :cond_6

    .line 2643
    const/4 v14, 0x1

    .line 2664
    :cond_5
    :goto_4
    sget-object v20, Lcom/ibm/icu/text/Bidi;->groupProp:[S

    aget-short v6, v20, v14

    .restart local v6    # "gprop":S
    goto :goto_3

    .line 2644
    .end local v6    # "gprop":S
    :cond_6
    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v14, v0, :cond_5

    .line 2645
    if-gt v11, v7, :cond_8

    .line 2648
    const/4 v12, 0x1

    .line 2649
    move/from16 v11, p2

    .line 2650
    add-int/lit8 v9, v7, 0x1

    .local v9, "j":I
    :goto_5
    move/from16 v0, p2

    if-ge v9, v0, :cond_8

    .line 2651
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    move-object/from16 v20, v0

    aget-byte v20, v20, v9

    invoke-static/range {v20 .. v20}, Lcom/ibm/icu/text/Bidi;->NoContextRTL(B)B

    move-result v20

    move/from16 v0, v20

    int-to-short v15, v0

    .line 2652
    .local v15, "prop1":S
    if-eqz v15, :cond_7

    const/16 v20, 0x1

    move/from16 v0, v20

    if-eq v15, v0, :cond_7

    const/16 v20, 0xd

    move/from16 v0, v20

    if-ne v15, v0, :cond_9

    .line 2653
    :cond_7
    move v12, v15

    .line 2654
    move v11, v9

    .line 2659
    .end local v9    # "j":I
    .end local v15    # "prop1":S
    :cond_8
    const/16 v20, 0xd

    move/from16 v0, v20

    if-ne v12, v0, :cond_5

    .line 2660
    const/4 v14, 0x5

    goto :goto_4

    .line 2650
    .restart local v9    # "j":I
    .restart local v15    # "prop1":S
    :cond_9
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 2678
    .end local v9    # "j":I
    .end local v14    # "prop":S
    .end local v15    # "prop1":S
    .restart local v4    # "actionImp":S
    .restart local v5    # "cell":S
    .restart local v6    # "gprop":S
    .restart local v13    # "oldStateImp":S
    .restart local v16    # "resProp":S
    :pswitch_0
    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-direct {v0, v10, v1, v2, v7}, Lcom/ibm/icu/text/Bidi;->processPropertySeq(Lcom/ibm/icu/text/Bidi$LevState;SII)V

    .line 2679
    move/from16 v17, v7

    .line 2634
    .end local v16    # "resProp":S
    :cond_a
    :goto_6
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    .line 2682
    .restart local v16    # "resProp":S
    :pswitch_1
    move/from16 v18, v7

    .line 2683
    goto :goto_6

    .line 2685
    :pswitch_2
    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v17

    move/from16 v3, v18

    invoke-direct {v0, v10, v1, v2, v3}, Lcom/ibm/icu/text/Bidi;->processPropertySeq(Lcom/ibm/icu/text/Bidi$LevState;SII)V

    .line 2686
    const/16 v20, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v18

    invoke-direct {v0, v10, v1, v2, v7}, Lcom/ibm/icu/text/Bidi;->processPropertySeq(Lcom/ibm/icu/text/Bidi$LevState;SII)V

    .line 2687
    move/from16 v17, v7

    .line 2688
    goto :goto_6

    .line 2690
    :pswitch_3
    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v17

    move/from16 v3, v18

    invoke-direct {v0, v10, v1, v2, v3}, Lcom/ibm/icu/text/Bidi;->processPropertySeq(Lcom/ibm/icu/text/Bidi$LevState;SII)V

    .line 2691
    move/from16 v17, v18

    .line 2692
    move/from16 v18, v7

    .line 2693
    goto :goto_6

    .line 2700
    .end local v4    # "actionImp":S
    .end local v5    # "cell":S
    .end local v6    # "gprop":S
    .end local v13    # "oldStateImp":S
    .end local v16    # "resProp":S
    :cond_b
    move-object/from16 v0, p0

    move/from16 v1, p4

    move/from16 v2, p2

    move/from16 v3, p2

    invoke-direct {v0, v10, v1, v2, v3}, Lcom/ibm/icu/text/Bidi;->processPropertySeq(Lcom/ibm/icu/text/Bidi$LevState;SII)V

    .line 2701
    return-void

    .line 2676
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static writeReverse(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p0, "src"    # Ljava/lang/String;
    .param p1, "options"    # I

    .prologue
    .line 4941
    if-nez p0, :cond_0

    .line 4942
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 4945
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 4946
    invoke-static {p0, p1}, Lcom/ibm/icu/text/BidiWriter;->writeReverse(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 4949
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/String;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method Bidi_Abs(I)I
    .locals 0
    .param p1, "x"    # I

    .prologue
    .line 2750
    if-ltz p1, :cond_0

    .end local p1    # "x":I
    :goto_0
    return p1

    .restart local p1    # "x":I
    :cond_0
    neg-int p1, p1

    goto :goto_0
.end method

.method Bidi_Min(II)I
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2746
    if-ge p1, p2, :cond_0

    .end local p1    # "x":I
    :goto_0
    return p1

    .restart local p1    # "x":I
    :cond_0
    move p1, p2

    goto :goto_0
.end method

.method GetParaLevelAt(I)B
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1081
    iget-byte v0, p0, Lcom/ibm/icu/text/Bidi;->defaultParaLevel:B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    aget-byte v0, v0, p1

    shr-int/lit8 v0, v0, 0x6

    int-to-byte v0, v0

    :goto_0
    return v0

    :cond_0
    iget-byte v0, p0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    goto :goto_0
.end method

.method public baseIsLeftToRight()Z
    .locals 1

    .prologue
    .line 4626
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->getParaLevel()B

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public countParagraphs()I
    .locals 1

    .prologue
    .line 3676
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3677
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->paraCount:I

    return v0
.end method

.method public countRuns()I
    .locals 1

    .prologue
    .line 3989
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3990
    invoke-static {p0}, Lcom/ibm/icu/text/BidiLine;->getRuns(Lcom/ibm/icu/text/Bidi;)V

    .line 3991
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->runCount:I

    return v0
.end method

.method public createLineBidi(II)Lcom/ibm/icu/text/Bidi;
    .locals 1
    .param p1, "lineStart"    # I
    .param p2, "lineLimit"    # I

    .prologue
    .line 4559
    invoke-virtual {p0, p1, p2}, Lcom/ibm/icu/text/Bidi;->setLine(II)Lcom/ibm/icu/text/Bidi;

    move-result-object v0

    return-object v0
.end method

.method public getBaseLevel()I
    .locals 1

    .prologue
    .line 4642
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->getParaLevel()B

    move-result v0

    return v0
.end method

.method public getCustomClassifier()Lcom/ibm/icu/text/BidiClassifier;
    .locals 1

    .prologue
    .line 3812
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->customClassifier:Lcom/ibm/icu/text/BidiClassifier;

    return-object v0
.end method

.method public getCustomizedClass(I)I
    .locals 2
    .param p1, "c"    # I

    .prologue
    .line 3833
    iget-object v1, p0, Lcom/ibm/icu/text/Bidi;->customClassifier:Lcom/ibm/icu/text/BidiClassifier;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/ibm/icu/text/Bidi;->customClassifier:Lcom/ibm/icu/text/BidiClassifier;

    invoke-virtual {v1, p1}, Lcom/ibm/icu/text/BidiClassifier;->classify(I)I

    move-result v0

    .local v0, "dir":I
    const/16 v1, 0x13

    if-ne v0, v1, :cond_1

    .line 3835
    .end local v0    # "dir":I
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/text/Bidi;->bdp:Lcom/ibm/icu/impl/UBiDiProps;

    invoke-virtual {v1, p1}, Lcom/ibm/icu/impl/UBiDiProps;->getClass(I)I

    move-result v0

    .line 3837
    :cond_1
    return v0
.end method

.method getDirPropsMemory(I)V
    .locals 1
    .param p1, "len"    # I

    .prologue
    .line 1264
    iget-boolean v0, p0, Lcom/ibm/icu/text/Bidi;->mayAllocateText:Z

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/Bidi;->getDirPropsMemory(ZI)V

    .line 1265
    return-void
.end method

.method public getDirection()B
    .locals 1

    .prologue
    .line 3496
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3497
    iget-byte v0, p0, Lcom/ibm/icu/text/Bidi;->direction:B

    return v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 3553
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3554
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->originalLength:I

    return v0
.end method

.method public getLevelAt(I)B
    .locals 2
    .param p1, "charIndex"    # I

    .prologue
    .line 3911
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3912
    const/4 v0, 0x0

    iget v1, p0, Lcom/ibm/icu/text/Bidi;->length:I

    invoke-virtual {p0, p1, v0, v1}, Lcom/ibm/icu/text/Bidi;->verifyRange(III)V

    .line 3913
    invoke-static {p0, p1}, Lcom/ibm/icu/text/BidiLine;->getLevelAt(Lcom/ibm/icu/text/Bidi;I)B

    move-result v0

    return v0
.end method

.method public getLevels()[B
    .locals 1

    .prologue
    .line 3932
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3933
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->length:I

    if-gtz v0, :cond_0

    .line 3934
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 3936
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/ibm/icu/text/BidiLine;->getLevels(Lcom/ibm/icu/text/Bidi;)[B

    move-result-object v0

    goto :goto_0
.end method

.method getLevelsMemory(I)V
    .locals 1
    .param p1, "len"    # I

    .prologue
    .line 1275
    iget-boolean v0, p0, Lcom/ibm/icu/text/Bidi;->mayAllocateText:Z

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/Bidi;->getLevelsMemory(ZI)V

    .line 1276
    return-void
.end method

.method public getLogicalIndex(I)I
    .locals 2
    .param p1, "visualIndex"    # I

    .prologue
    .line 4158
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 4159
    const/4 v0, 0x0

    iget v1, p0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    invoke-virtual {p0, p1, v0, v1}, Lcom/ibm/icu/text/Bidi;->verifyRange(III)V

    .line 4161
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v0, v0, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/ibm/icu/text/Bidi;->controlCount:I

    if-nez v0, :cond_1

    .line 4162
    iget-byte v0, p0, Lcom/ibm/icu/text/Bidi;->direction:B

    if-nez v0, :cond_0

    .line 4170
    .end local p1    # "visualIndex":I
    :goto_0
    return p1

    .line 4165
    .restart local p1    # "visualIndex":I
    :cond_0
    iget-byte v0, p0, Lcom/ibm/icu/text/Bidi;->direction:B

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 4166
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->length:I

    sub-int/2addr v0, p1

    add-int/lit8 p1, v0, -0x1

    goto :goto_0

    .line 4169
    :cond_1
    invoke-static {p0}, Lcom/ibm/icu/text/BidiLine;->getRuns(Lcom/ibm/icu/text/Bidi;)V

    .line 4170
    invoke-static {p0, p1}, Lcom/ibm/icu/text/BidiLine;->getLogicalIndex(Lcom/ibm/icu/text/Bidi;I)I

    move-result p1

    goto :goto_0
.end method

.method public getLogicalMap()[I
    .locals 1

    .prologue
    .line 4216
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->countRuns()I

    .line 4217
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->length:I

    if-gtz v0, :cond_0

    .line 4218
    const/4 v0, 0x0

    new-array v0, v0, [I

    .line 4220
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/ibm/icu/text/BidiLine;->getLogicalMap(Lcom/ibm/icu/text/Bidi;)[I

    move-result-object v0

    goto :goto_0
.end method

.method public getLogicalRun(I)Lcom/ibm/icu/text/BidiRun;
    .locals 2
    .param p1, "logicalPosition"    # I

    .prologue
    .line 3967
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3968
    const/4 v0, 0x0

    iget v1, p0, Lcom/ibm/icu/text/Bidi;->length:I

    invoke-virtual {p0, p1, v0, v1}, Lcom/ibm/icu/text/Bidi;->verifyRange(III)V

    .line 3969
    invoke-static {p0, p1}, Lcom/ibm/icu/text/BidiLine;->getLogicalRun(Lcom/ibm/icu/text/Bidi;I)Lcom/ibm/icu/text/BidiRun;

    move-result-object v0

    return-object v0
.end method

.method getLogicalToVisualRunsMap()V
    .locals 8

    .prologue
    .line 4666
    iget-boolean v3, p0, Lcom/ibm/icu/text/Bidi;->isGoodLogicalToVisualRunsMap:Z

    if-eqz v3, :cond_0

    .line 4685
    :goto_0
    return-void

    .line 4669
    :cond_0
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->countRuns()I

    move-result v0

    .line 4670
    .local v0, "count":I
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->logicalToVisualRunsMap:[I

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->logicalToVisualRunsMap:[I

    array-length v3, v3

    if-ge v3, v0, :cond_2

    .line 4672
    :cond_1
    new-array v3, v0, [I

    iput-object v3, p0, Lcom/ibm/icu/text/Bidi;->logicalToVisualRunsMap:[I

    .line 4675
    :cond_2
    new-array v2, v0, [J

    .line 4676
    .local v2, "keys":[J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_3

    .line 4677
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    aget-object v3, v3, v1

    iget v3, v3, Lcom/ibm/icu/text/BidiRun;->start:I

    int-to-long v4, v3

    const/16 v3, 0x20

    shl-long/2addr v4, v3

    int-to-long v6, v1

    add-long/2addr v4, v6

    aput-wide v4, v2, v1

    .line 4676
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4679
    :cond_3
    invoke-static {v2}, Ljava/util/Arrays;->sort([J)V

    .line 4680
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v0, :cond_4

    .line 4681
    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->logicalToVisualRunsMap:[I

    aget-wide v4, v2, v1

    const-wide/16 v6, -0x1

    and-long/2addr v4, v6

    long-to-int v4, v4

    aput v4, v3, v1

    .line 4680
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4683
    :cond_4
    const/4 v2, 0x0

    .line 4684
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/ibm/icu/text/Bidi;->isGoodLogicalToVisualRunsMap:Z

    goto :goto_0
.end method

.method public getParaLevel()B
    .locals 1

    .prologue
    .line 3660
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3661
    iget-byte v0, p0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    return v0
.end method

.method public getParagraph(I)Lcom/ibm/icu/text/BidiRun;
    .locals 4
    .param p1, "charIndex"    # I

    .prologue
    .line 3749
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3750
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->paraBidi:Lcom/ibm/icu/text/Bidi;

    .line 3751
    .local v0, "bidi":Lcom/ibm/icu/text/Bidi;
    const/4 v2, 0x0

    iget v3, v0, Lcom/ibm/icu/text/Bidi;->length:I

    invoke-virtual {p0, p1, v2, v3}, Lcom/ibm/icu/text/Bidi;->verifyRange(III)V

    .line 3753
    const/4 v1, 0x0

    .local v1, "paraIndex":I
    :goto_0
    iget-object v2, v0, Lcom/ibm/icu/text/Bidi;->paras:[I

    aget v2, v2, v1

    if-lt p1, v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3755
    :cond_0
    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/Bidi;->getParagraphByIndex(I)Lcom/ibm/icu/text/BidiRun;

    move-result-object v2

    return-object v2
.end method

.method public getParagraphByIndex(I)Lcom/ibm/icu/text/BidiRun;
    .locals 5
    .param p1, "paraIndex"    # I

    .prologue
    .line 3705
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3706
    const/4 v3, 0x0

    iget v4, p0, Lcom/ibm/icu/text/Bidi;->paraCount:I

    invoke-virtual {p0, p1, v3, v4}, Lcom/ibm/icu/text/Bidi;->verifyRange(III)V

    .line 3708
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->paraBidi:Lcom/ibm/icu/text/Bidi;

    .line 3710
    .local v0, "bidi":Lcom/ibm/icu/text/Bidi;
    if-nez p1, :cond_0

    .line 3711
    const/4 v2, 0x0

    .line 3715
    .local v2, "paraStart":I
    :goto_0
    new-instance v1, Lcom/ibm/icu/text/BidiRun;

    invoke-direct {v1}, Lcom/ibm/icu/text/BidiRun;-><init>()V

    .line 3716
    .local v1, "bidiRun":Lcom/ibm/icu/text/BidiRun;
    iput v2, v1, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 3717
    iget-object v3, v0, Lcom/ibm/icu/text/Bidi;->paras:[I

    aget v3, v3, p1

    iput v3, v1, Lcom/ibm/icu/text/BidiRun;->limit:I

    .line 3718
    invoke-virtual {p0, v2}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v3

    iput-byte v3, v1, Lcom/ibm/icu/text/BidiRun;->level:B

    .line 3719
    return-object v1

    .line 3713
    .end local v1    # "bidiRun":Lcom/ibm/icu/text/BidiRun;
    .end local v2    # "paraStart":I
    :cond_0
    iget-object v3, v0, Lcom/ibm/icu/text/Bidi;->paras:[I

    add-int/lit8 v4, p1, -0x1

    aget v2, v3, v4

    .restart local v2    # "paraStart":I
    goto :goto_0
.end method

.method public getParagraphIndex(I)I
    .locals 4
    .param p1, "charIndex"    # I

    .prologue
    .line 3778
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3779
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->paraBidi:Lcom/ibm/icu/text/Bidi;

    .line 3780
    .local v0, "bidi":Lcom/ibm/icu/text/Bidi;
    const/4 v2, 0x0

    iget v3, v0, Lcom/ibm/icu/text/Bidi;->length:I

    invoke-virtual {p0, p1, v2, v3}, Lcom/ibm/icu/text/Bidi;->verifyRange(III)V

    .line 3782
    const/4 v1, 0x0

    .local v1, "paraIndex":I
    :goto_0
    iget-object v2, v0, Lcom/ibm/icu/text/Bidi;->paras:[I

    aget v2, v2, v1

    if-lt p1, v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3784
    :cond_0
    return v1
.end method

.method public getProcessedLength()I
    .locals 1

    .prologue
    .line 3599
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3600
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->length:I

    return v0
.end method

.method public getReorderingMode()I
    .locals 1

    .prologue
    .line 1568
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    return v0
.end method

.method public getReorderingOptions()I
    .locals 1

    .prologue
    .line 1606
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    return v0
.end method

.method public getResultLength()I
    .locals 1

    .prologue
    .line 3634
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3635
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    return v0
.end method

.method public getRunCount()I
    .locals 1

    .prologue
    .line 4658
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->countRuns()I

    move-result v0

    return v0
.end method

.method public getRunLevel(I)I
    .locals 2
    .param p1, "run"    # I

    .prologue
    .line 4703
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 4704
    invoke-static {p0}, Lcom/ibm/icu/text/BidiLine;->getRuns(Lcom/ibm/icu/text/Bidi;)V

    .line 4705
    const/4 v0, 0x0

    iget v1, p0, Lcom/ibm/icu/text/Bidi;->runCount:I

    invoke-virtual {p0, p1, v0, v1}, Lcom/ibm/icu/text/Bidi;->verifyRange(III)V

    .line 4706
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->getLogicalToVisualRunsMap()V

    .line 4707
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    iget-object v1, p0, Lcom/ibm/icu/text/Bidi;->logicalToVisualRunsMap:[I

    aget v1, v1, p1

    aget-object v0, v0, v1

    iget-byte v0, v0, Lcom/ibm/icu/text/BidiRun;->level:B

    return v0
.end method

.method public getRunLimit(I)I
    .locals 5
    .param p1, "run"    # I

    .prologue
    .line 4752
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 4753
    invoke-static {p0}, Lcom/ibm/icu/text/BidiLine;->getRuns(Lcom/ibm/icu/text/Bidi;)V

    .line 4754
    const/4 v2, 0x0

    iget v3, p0, Lcom/ibm/icu/text/Bidi;->runCount:I

    invoke-virtual {p0, p1, v2, v3}, Lcom/ibm/icu/text/Bidi;->verifyRange(III)V

    .line 4755
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->getLogicalToVisualRunsMap()V

    .line 4756
    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->logicalToVisualRunsMap:[I

    aget v0, v2, p1

    .line 4757
    .local v0, "idx":I
    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    aget-object v2, v2, v0

    iget v1, v2, Lcom/ibm/icu/text/BidiRun;->limit:I

    .line 4759
    .local v1, "len":I
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/ibm/icu/text/BidiRun;->start:I

    add-int/2addr v2, v1

    return v2

    .line 4757
    .end local v1    # "len":I
    :cond_0
    iget-object v2, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/ibm/icu/text/BidiRun;->limit:I

    iget-object v3, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    add-int/lit8 v4, v0, -0x1

    aget-object v3, v3, v4

    iget v3, v3, Lcom/ibm/icu/text/BidiRun;->limit:I

    sub-int v1, v2, v3

    goto :goto_0
.end method

.method public getRunStart(I)I
    .locals 2
    .param p1, "run"    # I

    .prologue
    .line 4727
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 4728
    invoke-static {p0}, Lcom/ibm/icu/text/BidiLine;->getRuns(Lcom/ibm/icu/text/Bidi;)V

    .line 4729
    const/4 v0, 0x0

    iget v1, p0, Lcom/ibm/icu/text/Bidi;->runCount:I

    invoke-virtual {p0, p1, v0, v1}, Lcom/ibm/icu/text/Bidi;->verifyRange(III)V

    .line 4730
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->getLogicalToVisualRunsMap()V

    .line 4731
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    iget-object v1, p0, Lcom/ibm/icu/text/Bidi;->logicalToVisualRunsMap:[I

    aget v1, v1, p1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    return v0
.end method

.method getRunsMemory(I)V
    .locals 1
    .param p1, "len"    # I

    .prologue
    .line 1286
    iget-boolean v0, p0, Lcom/ibm/icu/text/Bidi;->mayAllocateRuns:Z

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/Bidi;->getRunsMemory(ZI)V

    .line 1287
    return-void
.end method

.method public getText()[C
    .locals 1

    .prologue
    .line 3536
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3537
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->text:[C

    return-object v0
.end method

.method public getTextAsString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3516
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 3517
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/ibm/icu/text/Bidi;->text:[C

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method public getVisualIndex(I)I
    .locals 2
    .param p1, "logicalIndex"    # I

    .prologue
    .line 4112
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 4113
    const/4 v0, 0x0

    iget v1, p0, Lcom/ibm/icu/text/Bidi;->length:I

    invoke-virtual {p0, p1, v0, v1}, Lcom/ibm/icu/text/Bidi;->verifyRange(III)V

    .line 4114
    invoke-static {p0, p1}, Lcom/ibm/icu/text/BidiLine;->getVisualIndex(Lcom/ibm/icu/text/Bidi;I)I

    move-result v0

    return v0
.end method

.method public getVisualMap()[I
    .locals 1

    .prologue
    .line 4259
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->countRuns()I

    .line 4260
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    if-gtz v0, :cond_0

    .line 4261
    const/4 v0, 0x0

    new-array v0, v0, [I

    .line 4263
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/ibm/icu/text/BidiLine;->getVisualMap(Lcom/ibm/icu/text/Bidi;)[I

    move-result-object v0

    goto :goto_0
.end method

.method public getVisualRun(I)Lcom/ibm/icu/text/BidiRun;
    .locals 2
    .param p1, "runIndex"    # I

    .prologue
    .line 4061
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 4062
    invoke-static {p0}, Lcom/ibm/icu/text/BidiLine;->getRuns(Lcom/ibm/icu/text/Bidi;)V

    .line 4063
    const/4 v0, 0x0

    iget v1, p0, Lcom/ibm/icu/text/Bidi;->runCount:I

    invoke-virtual {p0, p1, v0, v1}, Lcom/ibm/icu/text/Bidi;->verifyRange(III)V

    .line 4064
    invoke-static {p0, p1}, Lcom/ibm/icu/text/BidiLine;->getVisualRun(Lcom/ibm/icu/text/Bidi;I)Lcom/ibm/icu/text/BidiRun;

    move-result-object v0

    return-object v0
.end method

.method public isInverse()Z
    .locals 1

    .prologue
    .line 1383
    iget-boolean v0, p0, Lcom/ibm/icu/text/Bidi;->isInverse:Z

    return v0
.end method

.method public isLeftToRight()Z
    .locals 1

    .prologue
    .line 4593
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->getDirection()B

    move-result v0

    if-nez v0, :cond_0

    iget-byte v0, p0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMixed()Z
    .locals 1

    .prologue
    .line 4576
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->isLeftToRight()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->isRightToLeft()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOrderParagraphsLTR()Z
    .locals 1

    .prologue
    .line 3474
    iget-boolean v0, p0, Lcom/ibm/icu/text/Bidi;->orderParagraphsLTR:Z

    return v0
.end method

.method public isRightToLeft()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4610
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->getDirection()B

    move-result v1

    if-ne v1, v0, :cond_0

    iget-byte v1, p0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public orderParagraphsLTR(Z)V
    .locals 0
    .param p1, "ordarParaLTR"    # Z

    .prologue
    .line 3459
    iput-boolean p1, p0, Lcom/ibm/icu/text/Bidi;->orderParagraphsLTR:Z

    .line 3460
    return-void
.end method

.method public setCustomClassifier(Lcom/ibm/icu/text/BidiClassifier;)V
    .locals 0
    .param p1, "classifier"    # Lcom/ibm/icu/text/BidiClassifier;

    .prologue
    .line 3798
    iput-object p1, p0, Lcom/ibm/icu/text/Bidi;->customClassifier:Lcom/ibm/icu/text/BidiClassifier;

    .line 3799
    return-void
.end method

.method public setInverse(Z)V
    .locals 1
    .param p1, "isInverse"    # Z

    .prologue
    .line 1359
    iput-boolean p1, p0, Lcom/ibm/icu/text/Bidi;->isInverse:Z

    .line 1360
    if-eqz p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    iput v0, p0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    .line 1362
    return-void

    .line 1360
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLine(II)Lcom/ibm/icu/text/Bidi;
    .locals 2
    .param p1, "start"    # I
    .param p2, "limit"    # I

    .prologue
    const/4 v1, 0x0

    .line 3883
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidPara()V

    .line 3884
    invoke-virtual {p0, p1, v1, p2}, Lcom/ibm/icu/text/Bidi;->verifyRange(III)V

    .line 3885
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->length:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, p2, v1, v0}, Lcom/ibm/icu/text/Bidi;->verifyRange(III)V

    .line 3886
    invoke-virtual {p0, p1}, Lcom/ibm/icu/text/Bidi;->getParagraphIndex(I)I

    move-result v0

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {p0, v1}, Lcom/ibm/icu/text/Bidi;->getParagraphIndex(I)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 3888
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 3890
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/ibm/icu/text/BidiLine;->setLine(Lcom/ibm/icu/text/Bidi;II)Lcom/ibm/icu/text/Bidi;

    move-result-object v0

    return-object v0
.end method

.method public setPara(Ljava/lang/String;B[B)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "paraLevel"    # B
    .param p3, "embeddingLevels"    # [B

    .prologue
    .line 2988
    if-nez p1, :cond_0

    .line 2989
    const/4 v0, 0x0

    new-array v0, v0, [C

    invoke-virtual {p0, v0, p2, p3}, Lcom/ibm/icu/text/Bidi;->setPara([CB[B)V

    .line 2993
    :goto_0
    return-void

    .line 2991
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/ibm/icu/text/Bidi;->setPara([CB[B)V

    goto :goto_0
.end method

.method public setPara(Ljava/text/AttributedCharacterIterator;)V
    .locals 14
    .param p1, "paragraph"    # Ljava/text/AttributedCharacterIterator;

    .prologue
    const/4 v11, 0x0

    .line 3395
    sget-object v12, Ljava/awt/font/TextAttribute;->RUN_DIRECTION:Ljava/awt/font/TextAttribute;

    invoke-interface {p1, v12}, Ljava/text/AttributedCharacterIterator;->getAttribute(Ljava/text/AttributedCharacterIterator$Attribute;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    .line 3396
    .local v8, "runDirection":Ljava/lang/Boolean;
    if-nez v8, :cond_1

    .line 3397
    const/16 v7, 0x7e

    .line 3403
    .local v7, "paraLvl":B
    :goto_0
    const/4 v6, 0x0

    .line 3404
    .local v6, "lvls":[B
    invoke-interface {p1}, Ljava/text/AttributedCharacterIterator;->getEndIndex()I

    move-result v12

    invoke-interface {p1}, Ljava/text/AttributedCharacterIterator;->getBeginIndex()I

    move-result v13

    sub-int v4, v12, v13

    .line 3405
    .local v4, "len":I
    new-array v2, v4, [B

    .line 3406
    .local v2, "embeddingLevels":[B
    new-array v10, v4, [C

    .line 3407
    .local v10, "txt":[C
    const/4 v3, 0x0

    .line 3408
    .local v3, "i":I
    invoke-interface {p1}, Ljava/text/AttributedCharacterIterator;->first()C

    move-result v0

    .line 3409
    .local v0, "ch":C
    :goto_1
    const v12, 0xffff

    if-eq v0, v12, :cond_5

    .line 3410
    aput-char v0, v10, v3

    .line 3411
    sget-object v12, Ljava/awt/font/TextAttribute;->BIDI_EMBEDDING:Ljava/awt/font/TextAttribute;

    invoke-interface {p1, v12}, Ljava/text/AttributedCharacterIterator;->getAttribute(Ljava/text/AttributedCharacterIterator$Attribute;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 3412
    .local v1, "embedding":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 3413
    invoke-virtual {v1}, Ljava/lang/Integer;->byteValue()B

    move-result v5

    .line 3414
    .local v5, "level":B
    if-nez v5, :cond_3

    .line 3424
    .end local v5    # "level":B
    :cond_0
    :goto_2
    invoke-interface {p1}, Ljava/text/AttributedCharacterIterator;->next()C

    move-result v0

    .line 3425
    add-int/lit8 v3, v3, 0x1

    .line 3426
    goto :goto_1

    .line 3399
    .end local v0    # "ch":C
    .end local v1    # "embedding":Ljava/lang/Integer;
    .end local v2    # "embeddingLevels":[B
    .end local v3    # "i":I
    .end local v4    # "len":I
    .end local v6    # "lvls":[B
    .end local v7    # "paraLvl":B
    .end local v10    # "txt":[C
    :cond_1
    sget-object v12, Ljava/awt/font/TextAttribute;->RUN_DIRECTION_LTR:Ljava/lang/Boolean;

    invoke-virtual {v8, v12}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    move v7, v11

    .restart local v7    # "paraLvl":B
    :goto_3
    goto :goto_0

    .end local v7    # "paraLvl":B
    :cond_2
    const/4 v7, 0x1

    goto :goto_3

    .line 3416
    .restart local v0    # "ch":C
    .restart local v1    # "embedding":Ljava/lang/Integer;
    .restart local v2    # "embeddingLevels":[B
    .restart local v3    # "i":I
    .restart local v4    # "len":I
    .restart local v5    # "level":B
    .restart local v6    # "lvls":[B
    .restart local v7    # "paraLvl":B
    .restart local v10    # "txt":[C
    :cond_3
    if-gez v5, :cond_4

    .line 3417
    move-object v6, v2

    .line 3418
    rsub-int/lit8 v12, v5, 0x0

    or-int/lit8 v12, v12, -0x80

    int-to-byte v12, v12

    aput-byte v12, v2, v3

    goto :goto_2

    .line 3420
    :cond_4
    move-object v6, v2

    .line 3421
    aput-byte v5, v2, v3

    goto :goto_2

    .line 3430
    .end local v1    # "embedding":Ljava/lang/Integer;
    .end local v5    # "level":B
    :cond_5
    sget-object v12, Ljava/awt/font/TextAttribute;->NUMERIC_SHAPING:Ljava/awt/font/TextAttribute;

    invoke-interface {p1, v12}, Ljava/text/AttributedCharacterIterator;->getAttribute(Ljava/text/AttributedCharacterIterator$Attribute;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/awt/font/NumericShaper;

    .line 3431
    .local v9, "shaper":Ljava/awt/font/NumericShaper;
    if-eqz v9, :cond_6

    .line 3432
    invoke-virtual {v9, v10, v11, v4}, Ljava/awt/font/NumericShaper;->shape([CII)V

    .line 3435
    :cond_6
    invoke-virtual {p0, v10, v7, v6}, Lcom/ibm/icu/text/Bidi;->setPara([CB[B)V

    .line 3436
    return-void
.end method

.method public setPara([CB[B)V
    .locals 17
    .param p1, "chars"    # [C
    .param p2, "paraLevel"    # B
    .param p3, "embeddingLevels"    # [B

    .prologue
    .line 3074
    const/16 v13, 0x7e

    move/from16 v0, p2

    if-ge v0, v13, :cond_0

    .line 3075
    const/4 v13, 0x0

    const/16 v14, 0x3e

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1, v13, v14}, Lcom/ibm/icu/text/Bidi;->verifyRange(III)V

    .line 3077
    :cond_0
    if-nez p1, :cond_1

    .line 3078
    const/4 v13, 0x0

    new-array v0, v13, [C

    move-object/from16 p1, v0

    .line 3082
    :cond_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    const/4 v14, 0x3

    if-ne v13, v14, :cond_2

    .line 3083
    invoke-virtual/range {p0 .. p2}, Lcom/ibm/icu/text/Bidi;->setParaRunsOnly([CB)V

    .line 3347
    :goto_0
    return-void

    .line 3088
    :cond_2
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->paraBidi:Lcom/ibm/icu/text/Bidi;

    .line 3089
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/Bidi;->text:[C

    .line 3090
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->text:[C

    array-length v13, v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->originalLength:I

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->length:I

    .line 3091
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput-byte v0, v1, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    .line 3092
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-byte v13, v0, Lcom/ibm/icu/text/Bidi;->direction:B

    .line 3093
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->paraCount:I

    .line 3098
    const/4 v13, 0x0

    new-array v13, v13, [B

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    .line 3099
    const/4 v13, 0x0

    new-array v13, v13, [B

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->levels:[B

    .line 3100
    const/4 v13, 0x0

    new-array v13, v13, [Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    .line 3101
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/ibm/icu/text/Bidi;->isGoodLogicalToVisualRunsMap:Z

    .line 3102
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    const/4 v14, 0x0

    iput v14, v13, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    .line 3103
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    const/4 v14, 0x0

    iput v14, v13, Lcom/ibm/icu/text/Bidi$InsertPoints;->confirmed:I

    .line 3108
    invoke-static/range {p2 .. p2}, Lcom/ibm/icu/text/Bidi;->IsDefaultLevel(B)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 3109
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput-byte v0, v1, Lcom/ibm/icu/text/Bidi;->defaultParaLevel:B

    .line 3114
    :goto_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->length:I

    if-nez v13, :cond_6

    .line 3120
    invoke-static/range {p2 .. p2}, Lcom/ibm/icu/text/Bidi;->IsDefaultLevel(B)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 3121
    move-object/from16 v0, p0

    iget-byte v13, v0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    and-int/lit8 v13, v13, 0x1

    int-to-byte v13, v13

    move-object/from16 v0, p0

    iput-byte v13, v0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    .line 3122
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-byte v13, v0, Lcom/ibm/icu/text/Bidi;->defaultParaLevel:B

    .line 3124
    :cond_3
    move-object/from16 v0, p0

    iget-byte v13, v0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    and-int/lit8 v13, v13, 0x1

    if-eqz v13, :cond_5

    .line 3125
    const/4 v13, 0x1

    invoke-static {v13}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 3126
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-byte v13, v0, Lcom/ibm/icu/text/Bidi;->direction:B

    .line 3132
    :goto_2
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    .line 3133
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->paraCount:I

    .line 3134
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/Bidi;->paraBidi:Lcom/ibm/icu/text/Bidi;

    goto/16 :goto_0

    .line 3111
    :cond_4
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-byte v13, v0, Lcom/ibm/icu/text/Bidi;->defaultParaLevel:B

    goto :goto_1

    .line 3128
    :cond_5
    const/4 v13, 0x0

    invoke-static {v13}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->flags:I

    .line 3129
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-byte v13, v0, Lcom/ibm/icu/text/Bidi;->direction:B

    goto :goto_2

    .line 3138
    :cond_6
    const/4 v13, -0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    .line 3145
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->length:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/ibm/icu/text/Bidi;->getDirPropsMemory(I)V

    .line 3146
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->dirPropsMemory:[B

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    .line 3147
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/Bidi;->getDirProps()V

    .line 3149
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->length:I

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    .line 3152
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->paraCount:I

    const/4 v14, 0x1

    if-le v13, v14, :cond_9

    .line 3153
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->paraCount:I

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/ibm/icu/text/Bidi;->getInitialParasMemory(I)V

    .line 3154
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->parasMemory:[I

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->paras:[I

    .line 3155
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->paras:[I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/ibm/icu/text/Bidi;->paraCount:I

    add-int/lit8 v14, v14, -0x1

    move-object/from16 v0, p0

    iget v15, v0, Lcom/ibm/icu/text/Bidi;->length:I

    aput v15, v13, v14

    .line 3163
    :goto_3
    if-nez p3, :cond_a

    .line 3165
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->length:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/ibm/icu/text/Bidi;->getLevelsMemory(I)V

    .line 3166
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->levelsMemory:[B

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->levels:[B

    .line 3167
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/Bidi;->resolveExplicitLevels()B

    move-result v13

    move-object/from16 v0, p0

    iput-byte v13, v0, Lcom/ibm/icu/text/Bidi;->direction:B

    .line 3178
    :goto_4
    move-object/from16 v0, p0

    iget-byte v13, v0, Lcom/ibm/icu/text/Bidi;->direction:B

    packed-switch v13, :pswitch_data_0

    .line 3197
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    packed-switch v13, :pswitch_data_1

    .line 3240
    :goto_5
    if-nez p3, :cond_d

    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->paraCount:I

    const/4 v14, 0x1

    if-gt v13, v14, :cond_d

    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->flags:I

    sget v14, Lcom/ibm/icu/text/Bidi;->DirPropFlagMultiRuns:I

    and-int/2addr v13, v14

    if-nez v13, :cond_d

    .line 3242
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget v14, v0, Lcom/ibm/icu/text/Bidi;->length:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v15

    invoke-static {v15}, Lcom/ibm/icu/text/Bidi;->GetLRFromLevel(B)B

    move-result v15

    int-to-short v15, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->length:I

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v16

    invoke-static/range {v16 .. v16}, Lcom/ibm/icu/text/Bidi;->GetLRFromLevel(B)B

    move-result v16

    move/from16 v0, v16

    int-to-short v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v13, v14, v15, v1}, Lcom/ibm/icu/text/Bidi;->resolveImplicitLevels(IISS)V

    .line 3304
    :goto_6
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/Bidi;->adjustWSLevels()V

    .line 3311
    :goto_7
    move-object/from16 v0, p0

    iget-byte v13, v0, Lcom/ibm/icu/text/Bidi;->defaultParaLevel:B

    if-lez v13, :cond_1b

    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    and-int/lit8 v13, v13, 0x1

    if-eqz v13, :cond_1b

    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    const/4 v14, 0x5

    if-eq v13, v14, :cond_7

    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    const/4 v14, 0x6

    if-ne v13, v14, :cond_1b

    .line 3317
    :cond_7
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_8
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->paraCount:I

    if-ge v4, v13, :cond_1b

    .line 3318
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->paras:[I

    aget v13, v13, v4

    add-int/lit8 v6, v13, -0x1

    .line 3319
    .local v6, "last":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    aget-byte v13, v13, v6

    and-int/lit8 v13, v13, 0x40

    if-nez v13, :cond_17

    .line 3317
    :cond_8
    :goto_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 3158
    .end local v4    # "i":I
    .end local v6    # "last":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->simpleParas:[I

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->paras:[I

    .line 3159
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->simpleParas:[I

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/ibm/icu/text/Bidi;->length:I

    aput v15, v13, v14

    goto/16 :goto_3

    .line 3170
    :cond_a
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/Bidi;->levels:[B

    .line 3171
    invoke-direct/range {p0 .. p0}, Lcom/ibm/icu/text/Bidi;->checkExplicitLevels()B

    move-result v13

    move-object/from16 v0, p0

    iput-byte v13, v0, Lcom/ibm/icu/text/Bidi;->direction:B

    goto/16 :goto_4

    .line 3181
    :pswitch_0
    add-int/lit8 v13, p2, 0x1

    and-int/lit8 v13, v13, -0x2

    int-to-byte v0, v13

    move/from16 p2, v0

    .line 3184
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    goto :goto_7

    .line 3188
    :pswitch_1
    or-int/lit8 v13, p2, 0x1

    int-to-byte v0, v13

    move/from16 p2, v0

    .line 3191
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    goto :goto_7

    .line 3199
    :pswitch_2
    sget-object v13, Lcom/ibm/icu/text/Bidi;->impTab_DEFAULT:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->impTabPair:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    goto/16 :goto_5

    .line 3202
    :pswitch_3
    sget-object v13, Lcom/ibm/icu/text/Bidi;->impTab_NUMBERS_SPECIAL:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->impTabPair:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    goto/16 :goto_5

    .line 3205
    :pswitch_4
    sget-object v13, Lcom/ibm/icu/text/Bidi;->impTab_GROUP_NUMBERS_WITH_R:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->impTabPair:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    goto/16 :goto_5

    .line 3209
    :pswitch_5
    new-instance v13, Ljava/lang/InternalError;

    const-string/jumbo v14, "Internal ICU error in setPara"

    invoke-direct {v13, v14}, Ljava/lang/InternalError;-><init>(Ljava/lang/String;)V

    throw v13

    .line 3212
    :pswitch_6
    sget-object v13, Lcom/ibm/icu/text/Bidi;->impTab_INVERSE_NUMBERS_AS_L:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->impTabPair:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    goto/16 :goto_5

    .line 3215
    :pswitch_7
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    and-int/lit8 v13, v13, 0x1

    if-eqz v13, :cond_b

    .line 3216
    sget-object v13, Lcom/ibm/icu/text/Bidi;->impTab_INVERSE_LIKE_DIRECT_WITH_MARKS:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->impTabPair:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    goto/16 :goto_5

    .line 3218
    :cond_b
    sget-object v13, Lcom/ibm/icu/text/Bidi;->impTab_INVERSE_LIKE_DIRECT:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->impTabPair:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    goto/16 :goto_5

    .line 3222
    :pswitch_8
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    and-int/lit8 v13, v13, 0x1

    if-eqz v13, :cond_c

    .line 3223
    sget-object v13, Lcom/ibm/icu/text/Bidi;->impTab_INVERSE_FOR_NUMBERS_SPECIAL_WITH_MARKS:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->impTabPair:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    goto/16 :goto_5

    .line 3225
    :cond_c
    sget-object v13, Lcom/ibm/icu/text/Bidi;->impTab_INVERSE_FOR_NUMBERS_SPECIAL:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/ibm/icu/text/Bidi;->impTabPair:Lcom/ibm/icu/text/Bidi$ImpTabPair;

    goto/16 :goto_5

    .line 3247
    :cond_d
    const/4 v8, 0x0

    .line 3252
    .local v8, "limit":I
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v7

    .line 3253
    .local v7, "level":B
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->levels:[B

    const/4 v14, 0x0

    aget-byte v9, v13, v14

    .line 3254
    .local v9, "nextLevel":B
    if-ge v7, v9, :cond_11

    .line 3255
    invoke-static {v9}, Lcom/ibm/icu/text/Bidi;->GetLRFromLevel(B)B

    move-result v13

    int-to-short v3, v13

    .line 3264
    .local v3, "eor":S
    :cond_e
    :goto_a
    move v11, v8

    .line 3265
    .local v11, "start":I
    move v7, v9

    .line 3266
    if-lez v11, :cond_12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    add-int/lit8 v14, v11, -0x1

    aget-byte v13, v13, v14

    invoke-static {v13}, Lcom/ibm/icu/text/Bidi;->NoContextRTL(B)B

    move-result v13

    const/4 v14, 0x7

    if-ne v13, v14, :cond_12

    .line 3268
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v13

    invoke-static {v13}, Lcom/ibm/icu/text/Bidi;->GetLRFromLevel(B)B

    move-result v13

    int-to-short v10, v13

    .line 3274
    .local v10, "sor":S
    :cond_f
    :goto_b
    add-int/lit8 v8, v8, 0x1

    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->length:I

    if-ge v8, v13, :cond_10

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aget-byte v13, v13, v8

    if-eq v13, v7, :cond_f

    .line 3277
    :cond_10
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->length:I

    if-ge v8, v13, :cond_13

    .line 3278
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->levels:[B

    aget-byte v9, v13, v8

    .line 3284
    :goto_c
    and-int/lit8 v13, v7, 0x7f

    and-int/lit8 v14, v9, 0x7f

    if-ge v13, v14, :cond_14

    .line 3285
    invoke-static {v9}, Lcom/ibm/icu/text/Bidi;->GetLRFromLevel(B)B

    move-result v13

    int-to-short v3, v13

    .line 3292
    :goto_d
    and-int/lit8 v13, v7, -0x80

    if-nez v13, :cond_16

    .line 3293
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v8, v10, v3}, Lcom/ibm/icu/text/Bidi;->resolveImplicitLevels(IISS)V

    .line 3300
    :goto_e
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->length:I

    if-lt v8, v13, :cond_e

    goto/16 :goto_6

    .line 3257
    .end local v3    # "eor":S
    .end local v10    # "sor":S
    .end local v11    # "start":I
    :cond_11
    invoke-static {v7}, Lcom/ibm/icu/text/Bidi;->GetLRFromLevel(B)B

    move-result v13

    int-to-short v3, v13

    .restart local v3    # "eor":S
    goto :goto_a

    .line 3270
    .restart local v11    # "start":I
    :cond_12
    move v10, v3

    .restart local v10    # "sor":S
    goto :goto_b

    .line 3280
    :cond_13
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->length:I

    add-int/lit8 v13, v13, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/ibm/icu/text/Bidi;->GetParaLevelAt(I)B

    move-result v9

    goto :goto_c

    .line 3287
    :cond_14
    invoke-static {v7}, Lcom/ibm/icu/text/Bidi;->GetLRFromLevel(B)B

    move-result v13

    int-to-short v3, v13

    goto :goto_d

    .end local v11    # "start":I
    .local v12, "start":I
    :cond_15
    move v11, v12

    .line 3297
    .end local v12    # "start":I
    .restart local v11    # "start":I
    :cond_16
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->levels:[B

    add-int/lit8 v12, v11, 0x1

    .end local v11    # "start":I
    .restart local v12    # "start":I
    aget-byte v14, v13, v11

    and-int/lit8 v14, v14, 0x7f

    int-to-byte v14, v14

    aput-byte v14, v13, v11

    .line 3298
    if-lt v12, v8, :cond_15

    move v11, v12

    .end local v12    # "start":I
    .restart local v11    # "start":I
    goto :goto_e

    .line 3322
    .end local v3    # "eor":S
    .end local v7    # "level":B
    .end local v8    # "limit":I
    .end local v9    # "nextLevel":B
    .end local v10    # "sor":S
    .end local v11    # "start":I
    .restart local v4    # "i":I
    .restart local v6    # "last":I
    :cond_17
    if-nez v4, :cond_18

    const/4 v11, 0x0

    .line 3323
    .restart local v11    # "start":I
    :goto_f
    move v5, v6

    .local v5, "j":I
    :goto_10
    if-lt v5, v11, :cond_8

    .line 3324
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    aget-byte v13, v13, v5

    invoke-static {v13}, Lcom/ibm/icu/text/Bidi;->NoContextRTL(B)B

    move-result v2

    .line 3325
    .local v2, "dirProp":B
    if-nez v2, :cond_1a

    .line 3326
    if-ge v5, v6, :cond_19

    .line 3327
    :goto_11
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    aget-byte v13, v13, v6

    invoke-static {v13}, Lcom/ibm/icu/text/Bidi;->NoContextRTL(B)B

    move-result v13

    const/4 v14, 0x7

    if-ne v13, v14, :cond_19

    .line 3328
    add-int/lit8 v6, v6, -0x1

    .line 3329
    goto :goto_11

    .line 3322
    .end local v2    # "dirProp":B
    .end local v5    # "j":I
    .end local v11    # "start":I
    :cond_18
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/Bidi;->paras:[I

    add-int/lit8 v14, v4, -0x1

    aget v11, v13, v14

    goto :goto_f

    .line 3331
    .restart local v2    # "dirProp":B
    .restart local v5    # "j":I
    .restart local v11    # "start":I
    :cond_19
    const/4 v13, 0x4

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v13}, Lcom/ibm/icu/text/Bidi;->addPoint(II)V

    goto/16 :goto_9

    .line 3334
    :cond_1a
    invoke-static {v2}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v13

    and-int/lit16 v13, v13, 0x2002

    if-nez v13, :cond_8

    .line 3323
    add-int/lit8 v5, v5, -0x1

    goto :goto_10

    .line 3341
    .end local v2    # "dirProp":B
    .end local v4    # "i":I
    .end local v5    # "j":I
    .end local v6    # "last":I
    .end local v11    # "start":I
    :cond_1b
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    and-int/lit8 v13, v13, 0x2

    if-eqz v13, :cond_1c

    .line 3342
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/ibm/icu/text/Bidi;->controlCount:I

    sub-int/2addr v13, v14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    .line 3346
    :goto_12
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/Bidi;->paraBidi:Lcom/ibm/icu/text/Bidi;

    goto/16 :goto_0

    .line 3344
    :cond_1c
    move-object/from16 v0, p0

    iget v13, v0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/ibm/icu/text/Bidi;->insertPoints:Lcom/ibm/icu/text/Bidi$InsertPoints;

    iget v14, v14, Lcom/ibm/icu/text/Bidi$InsertPoints;->size:I

    add-int/2addr v13, v14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/ibm/icu/text/Bidi;->resultLength:I

    goto :goto_12

    .line 3178
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 3197
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method setParaRunsOnly([CB)V
    .locals 34
    .param p1, "parmText"    # [C
    .param p2, "parmParaLevel"    # B

    .prologue
    .line 2765
    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    .line 2766
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v17, v0

    .line 2767
    .local v17, "parmLength":I
    if-nez v17, :cond_0

    .line 2768
    const/16 v29, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, v29

    invoke-virtual {v0, v1, v2, v3}, Lcom/ibm/icu/text/Bidi;->setPara([CB[B)V

    .line 2769
    const/16 v29, 0x3

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    .line 2907
    :goto_0
    return-void

    .line 2773
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    move/from16 v22, v0

    .line 2774
    .local v22, "saveOptions":I
    and-int/lit8 v29, v22, 0x1

    if-lez v29, :cond_1

    .line 2775
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    move/from16 v29, v0

    and-int/lit8 v29, v29, -0x2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    .line 2776
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    move/from16 v29, v0

    or-int/lit8 v29, v29, 0x2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    .line 2778
    :cond_1
    and-int/lit8 v29, p2, 0x1

    move/from16 v0, v29

    int-to-byte v0, v0

    move/from16 p2, v0

    .line 2779
    const/16 v29, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, v29

    invoke-virtual {v0, v1, v2, v3}, Lcom/ibm/icu/text/Bidi;->setPara([CB[B)V

    .line 2783
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->length:I

    move/from16 v29, v0

    move/from16 v0, v29

    new-array v0, v0, [B

    move-object/from16 v21, v0

    .line 2784
    .local v21, "saveLevels":[B
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/Bidi;->getLevels()[B

    move-result-object v29

    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->length:I

    move/from16 v32, v0

    move-object/from16 v0, v29

    move/from16 v1, v30

    move-object/from16 v2, v21

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2785
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    move/from16 v23, v0

    .line 2793
    .local v23, "saveTrailingWSStart":I
    const/16 v29, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/Bidi;->writeReordered(I)Ljava/lang/String;

    move-result-object v28

    .line 2794
    .local v28, "visualText":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/ibm/icu/text/Bidi;->getVisualMap()[I

    move-result-object v26

    .line 2795
    .local v26, "visualMap":[I
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    .line 2796
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->length:I

    move/from16 v20, v0

    .line 2797
    .local v20, "saveLength":I
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/Bidi;->direction:B

    move/from16 v19, v0

    .line 2799
    .local v19, "saveDirection":B
    const/16 v29, 0x5

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    .line 2800
    xor-int/lit8 v29, p2, 0x1

    move/from16 v0, v29

    int-to-byte v0, v0

    move/from16 p2, v0

    .line 2801
    const/16 v29, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move/from16 v2, p2

    move-object/from16 v3, v29

    invoke-virtual {v0, v1, v2, v3}, Lcom/ibm/icu/text/Bidi;->setPara(Ljava/lang/String;B[B)V

    .line 2802
    invoke-static/range {p0 .. p0}, Lcom/ibm/icu/text/BidiLine;->getRuns(Lcom/ibm/icu/text/Bidi;)V

    .line 2804
    const/4 v5, 0x0

    .line 2805
    .local v5, "addedRuns":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    move/from16 v16, v0

    .line 2806
    .local v16, "oldRunCount":I
    const/16 v27, 0x0

    .line 2807
    .local v27, "visualStart":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    move/from16 v0, v16

    if-ge v6, v0, :cond_6

    .line 2808
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v6

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v29, v0

    sub-int v18, v29, v27

    .line 2809
    .local v18, "runLength":I
    const/16 v29, 0x2

    move/from16 v0, v18

    move/from16 v1, v29

    if-ge v0, v1, :cond_3

    .line 2807
    :cond_2
    add-int/lit8 v6, v6, 0x1

    add-int v27, v27, v18

    goto :goto_1

    .line 2812
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v6

    move-object/from16 v0, v29

    iget v14, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 2813
    .local v14, "logicalStart":I
    add-int/lit8 v11, v14, 0x1

    .local v11, "j":I
    :goto_2
    add-int v29, v14, v18

    move/from16 v0, v29

    if-ge v11, v0, :cond_2

    .line 2814
    aget v7, v26, v11

    .line 2815
    .local v7, "index":I
    add-int/lit8 v29, v11, -0x1

    aget v8, v26, v29

    .line 2816
    .local v8, "index1":I
    sub-int v29, v7, v8

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/Bidi;->Bidi_Abs(I)I

    move-result v29

    const/16 v30, 0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_4

    aget-byte v29, v21, v7

    aget-byte v30, v21, v8

    move/from16 v0, v29

    move/from16 v1, v30

    if-eq v0, v1, :cond_5

    .line 2817
    :cond_4
    add-int/lit8 v5, v5, 0x1

    .line 2813
    :cond_5
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 2821
    .end local v7    # "index":I
    .end local v8    # "index1":I
    .end local v11    # "j":I
    .end local v14    # "logicalStart":I
    .end local v18    # "runLength":I
    :cond_6
    if-lez v5, :cond_9

    .line 2822
    add-int v29, v16, v5

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/Bidi;->getRunsMemory(I)V

    .line 2823
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    move/from16 v29, v0

    const/16 v30, 0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_8

    .line 2825
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runsMemory:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    aget-object v31, v31, v32

    aput-object v31, v29, v30

    .line 2829
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runsMemory:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    .line 2830
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    move/from16 v29, v0

    add-int v29, v29, v5

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->runCount:I

    .line 2831
    move/from16 v6, v16

    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    move/from16 v29, v0

    move/from16 v0, v29

    if-ge v6, v0, :cond_9

    .line 2832
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v6

    if-nez v29, :cond_7

    .line 2833
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    new-instance v30, Lcom/ibm/icu/text/BidiRun;

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    invoke-direct/range {v30 .. v33}, Lcom/ibm/icu/text/BidiRun;-><init>(IIB)V

    aput-object v30, v29, v6

    .line 2831
    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 2827
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runsMemory:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    move/from16 v33, v0

    invoke-static/range {v29 .. v33}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_3

    .line 2839
    :cond_9
    add-int/lit8 v6, v16, -0x1

    :goto_5
    if-ltz v6, :cond_12

    .line 2840
    add-int v15, v6, v5

    .line 2841
    .local v15, "newI":I
    if-nez v6, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    aget-object v29, v29, v30

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v18, v0

    .line 2843
    .restart local v18    # "runLength":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v6

    move-object/from16 v0, v29

    iget v14, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 2844
    .restart local v14    # "logicalStart":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v6

    move-object/from16 v0, v29

    iget-byte v0, v0, Lcom/ibm/icu/text/BidiRun;->level:B

    move/from16 v29, v0

    and-int/lit8 v9, v29, 0x1

    .line 2845
    .local v9, "indexOddBit":I
    const/16 v29, 0x2

    move/from16 v0, v18

    move/from16 v1, v29

    if-ge v0, v1, :cond_c

    .line 2846
    if-lez v5, :cond_a

    .line 2847
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v30, v0

    aget-object v30, v30, v6

    invoke-virtual/range {v29 .. v30}, Lcom/ibm/icu/text/BidiRun;->copyFrom(Lcom/ibm/icu/text/BidiRun;)V

    .line 2849
    :cond_a
    aget v13, v26, v14

    .line 2850
    .local v13, "logicalPos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v15

    move-object/from16 v0, v29

    iput v13, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 2851
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v15

    aget-byte v30, v21, v13

    xor-int v30, v30, v9

    move/from16 v0, v30

    int-to-byte v0, v0

    move/from16 v30, v0

    move/from16 v0, v30

    move-object/from16 v1, v29

    iput-byte v0, v1, Lcom/ibm/icu/text/BidiRun;->level:B

    .line 2839
    :goto_7
    add-int/lit8 v6, v6, -0x1

    goto :goto_5

    .line 2841
    .end local v9    # "indexOddBit":I
    .end local v13    # "logicalPos":I
    .end local v14    # "logicalStart":I
    .end local v18    # "runLength":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v6

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v30, v0

    add-int/lit8 v31, v6, -0x1

    aget-object v30, v30, v31

    move-object/from16 v0, v30

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v30, v0

    sub-int v18, v29, v30

    goto/16 :goto_6

    .line 2854
    .restart local v9    # "indexOddBit":I
    .restart local v14    # "logicalStart":I
    .restart local v18    # "runLength":I
    :cond_c
    if-lez v9, :cond_f

    .line 2855
    move/from16 v24, v14

    .line 2856
    .local v24, "start":I
    add-int v29, v14, v18

    add-int/lit8 v12, v29, -0x1

    .line 2857
    .local v12, "limit":I
    const/16 v25, 0x1

    .line 2863
    .local v25, "step":I
    :goto_8
    move/from16 v11, v24

    .restart local v11    # "j":I
    :goto_9
    if-eq v11, v12, :cond_10

    .line 2864
    aget v7, v26, v11

    .line 2865
    .restart local v7    # "index":I
    add-int v29, v11, v25

    aget v8, v26, v29

    .line 2866
    .restart local v8    # "index1":I
    sub-int v29, v7, v8

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/Bidi;->Bidi_Abs(I)I

    move-result v29

    const/16 v30, 0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_d

    aget-byte v29, v21, v7

    aget-byte v30, v21, v8

    move/from16 v0, v29

    move/from16 v1, v30

    if-eq v0, v1, :cond_e

    .line 2867
    :cond_d
    aget v29, v26, v24

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v1, v7}, Lcom/ibm/icu/text/Bidi;->Bidi_Min(II)I

    move-result v13

    .line 2868
    .restart local v13    # "logicalPos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v15

    move-object/from16 v0, v29

    iput v13, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 2869
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v15

    aget-byte v30, v21, v13

    xor-int v30, v30, v9

    move/from16 v0, v30

    int-to-byte v0, v0

    move/from16 v30, v0

    move/from16 v0, v30

    move-object/from16 v1, v29

    iput-byte v0, v1, Lcom/ibm/icu/text/BidiRun;->level:B

    .line 2870
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v30, v0

    aget-object v30, v30, v6

    move-object/from16 v0, v30

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v30, v0

    move/from16 v0, v30

    move-object/from16 v1, v29

    iput v0, v1, Lcom/ibm/icu/text/BidiRun;->limit:I

    .line 2871
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v6

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->limit:I

    move/from16 v30, v0

    sub-int v31, v11, v24

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/Bidi;->Bidi_Abs(I)I

    move-result v31

    add-int/lit8 v31, v31, 0x1

    sub-int v30, v30, v31

    move/from16 v0, v30

    move-object/from16 v1, v29

    iput v0, v1, Lcom/ibm/icu/text/BidiRun;->limit:I

    .line 2872
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v6

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    move/from16 v29, v0

    and-int/lit8 v10, v29, 0xa

    .line 2873
    .local v10, "insertRemove":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v15

    move-object/from16 v0, v29

    iput v10, v0, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 2874
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v6

    move-object/from16 v0, v29

    iget v0, v0, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    move/from16 v30, v0

    xor-int/lit8 v31, v10, -0x1

    and-int v30, v30, v31

    move/from16 v0, v30

    move-object/from16 v1, v29

    iput v0, v1, Lcom/ibm/icu/text/BidiRun;->insertRemove:I

    .line 2875
    add-int v24, v11, v25

    .line 2876
    add-int/lit8 v5, v5, -0x1

    .line 2877
    add-int/lit8 v15, v15, -0x1

    .line 2863
    .end local v10    # "insertRemove":I
    .end local v13    # "logicalPos":I
    :cond_e
    add-int v11, v11, v25

    goto/16 :goto_9

    .line 2859
    .end local v7    # "index":I
    .end local v8    # "index1":I
    .end local v11    # "j":I
    .end local v12    # "limit":I
    .end local v24    # "start":I
    .end local v25    # "step":I
    :cond_f
    add-int v29, v14, v18

    add-int/lit8 v24, v29, -0x1

    .line 2860
    .restart local v24    # "start":I
    move v12, v14

    .line 2861
    .restart local v12    # "limit":I
    const/16 v25, -0x1

    .restart local v25    # "step":I
    goto/16 :goto_8

    .line 2880
    .restart local v11    # "j":I
    :cond_10
    if-lez v5, :cond_11

    .line 2881
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v30, v0

    aget-object v30, v30, v6

    invoke-virtual/range {v29 .. v30}, Lcom/ibm/icu/text/BidiRun;->copyFrom(Lcom/ibm/icu/text/BidiRun;)V

    .line 2883
    :cond_11
    aget v29, v26, v24

    aget v30, v26, v12

    move-object/from16 v0, p0

    move/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/ibm/icu/text/Bidi;->Bidi_Min(II)I

    move-result v13

    .line 2884
    .restart local v13    # "logicalPos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v15

    move-object/from16 v0, v29

    iput v13, v0, Lcom/ibm/icu/text/BidiRun;->start:I

    .line 2885
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/ibm/icu/text/Bidi;->runs:[Lcom/ibm/icu/text/BidiRun;

    move-object/from16 v29, v0

    aget-object v29, v29, v15

    aget-byte v30, v21, v13

    xor-int v30, v30, v9

    move/from16 v0, v30

    int-to-byte v0, v0

    move/from16 v30, v0

    move/from16 v0, v30

    move-object/from16 v1, v29

    iput-byte v0, v1, Lcom/ibm/icu/text/BidiRun;->level:B

    goto/16 :goto_7

    .line 2890
    .end local v9    # "indexOddBit":I
    .end local v11    # "j":I
    .end local v12    # "limit":I
    .end local v13    # "logicalPos":I
    .end local v14    # "logicalStart":I
    .end local v15    # "newI":I
    .end local v18    # "runLength":I
    .end local v24    # "start":I
    .end local v25    # "step":I
    :cond_12
    move-object/from16 v0, p0

    iget-byte v0, v0, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    move/from16 v29, v0

    xor-int/lit8 v29, v29, 0x1

    move/from16 v0, v29

    int-to-byte v0, v0

    move/from16 v29, v0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-byte v0, v1, Lcom/ibm/icu/text/Bidi;->paraLevel:B

    .line 2893
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/Bidi;->text:[C

    .line 2894
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->length:I

    .line 2895
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->originalLength:I

    .line 2896
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-byte v0, v1, Lcom/ibm/icu/text/Bidi;->direction:B

    .line 2897
    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/ibm/icu/text/Bidi;->levels:[B

    .line 2898
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->trailingWSStart:I

    .line 2900
    const/16 v26, 0x0

    .line 2901
    const/16 v28, 0x0

    .line 2902
    move-object/from16 v0, p0

    iget v0, v0, Lcom/ibm/icu/text/Bidi;->runCount:I

    move/from16 v29, v0

    const/16 v30, 0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-le v0, v1, :cond_13

    .line 2903
    const/16 v29, 0x2

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-byte v0, v1, Lcom/ibm/icu/text/Bidi;->direction:B

    .line 2906
    :cond_13
    const/16 v29, 0x3

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    goto/16 :goto_0
.end method

.method public setReorderingMode(I)V
    .locals 1
    .param p1, "reorderingMode"    # I

    .prologue
    .line 1550
    if-ltz p1, :cond_0

    const/4 v0, 0x7

    if-lt p1, v0, :cond_1

    .line 1556
    :cond_0
    :goto_0
    return-void

    .line 1553
    :cond_1
    iput p1, p0, Lcom/ibm/icu/text/Bidi;->reorderingMode:I

    .line 1554
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/ibm/icu/text/Bidi;->isInverse:Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setReorderingOptions(I)V
    .locals 1
    .param p1, "options"    # I

    .prologue
    .line 1589
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    .line 1590
    and-int/lit8 v0, p1, -0x2

    iput v0, p0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    .line 1594
    :goto_0
    return-void

    .line 1592
    :cond_0
    iput p1, p0, Lcom/ibm/icu/text/Bidi;->reorderingOptions:I

    goto :goto_0
.end method

.method testDirPropFlagAt(II)Z
    .locals 1
    .param p1, "flag"    # I
    .param p2, "index"    # I

    .prologue
    .line 994
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->dirProps:[B

    aget-byte v0, v0, p2

    and-int/lit8 v0, v0, -0x41

    int-to-byte v0, v0

    invoke-static {v0}, Lcom/ibm/icu/text/Bidi;->DirPropFlag(B)I

    move-result v0

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method verifyRange(III)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "start"    # I
    .param p3, "limit"    # I

    .prologue
    .line 1114
    if-lt p1, p2, :cond_0

    if-lt p1, p3, :cond_1

    .line 1115
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v2, "Value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " is out of range "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string/jumbo v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1118
    :cond_1
    return-void
.end method

.method verifyValidPara()V
    .locals 1

    .prologue
    .line 1094
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->paraBidi:Lcom/ibm/icu/text/Bidi;

    if-eq p0, v0, :cond_0

    .line 1095
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1097
    :cond_0
    return-void
.end method

.method verifyValidParaOrLine()V
    .locals 2

    .prologue
    .line 1101
    iget-object v0, p0, Lcom/ibm/icu/text/Bidi;->paraBidi:Lcom/ibm/icu/text/Bidi;

    .line 1103
    .local v0, "para":Lcom/ibm/icu/text/Bidi;
    if-ne p0, v0, :cond_1

    .line 1110
    :cond_0
    return-void

    .line 1107
    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/ibm/icu/text/Bidi;->paraBidi:Lcom/ibm/icu/text/Bidi;

    if-eq v0, v1, :cond_0

    .line 1108
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1
.end method

.method public writeReordered(I)Ljava/lang/String;
    .locals 2
    .param p1, "options"    # I

    .prologue
    .line 4891
    invoke-virtual {p0}, Lcom/ibm/icu/text/Bidi;->verifyValidParaOrLine()V

    .line 4892
    iget v0, p0, Lcom/ibm/icu/text/Bidi;->length:I

    if-nez v0, :cond_0

    .line 4894
    new-instance v0, Ljava/lang/String;

    const-string/jumbo v1, ""

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 4897
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/ibm/icu/text/BidiWriter;->writeReordered(Lcom/ibm/icu/text/Bidi;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
