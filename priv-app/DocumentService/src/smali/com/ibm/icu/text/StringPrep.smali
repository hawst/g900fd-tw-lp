.class public final Lcom/ibm/icu/text/StringPrep;
.super Ljava/lang/Object;
.source "StringPrep.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/ibm/icu/text/StringPrep$1;,
        Lcom/ibm/icu/text/StringPrep$Values;
    }
.end annotation


# static fields
.field public static final ALLOW_UNASSIGNED:I = 0x1

.field private static final CHECK_BIDI_ON:I = 0x2

.field private static final DATA_BUFFER_SIZE:I = 0x61a8

.field public static final DEFAULT:I = 0x0

.field private static final DELETE:I = 0x3

.field private static final FOUR_UCHARS_MAPPING_INDEX_START:I = 0x6

.field private static final INDEX_MAPPING_DATA_SIZE:I = 0x1

.field private static final INDEX_TOP:I = 0x10

.field private static final INDEX_TRIE_SIZE:I = 0x0

.field private static final MAP:I = 0x1

.field private static final MAX_INDEX_VALUE:I = 0x3fbf

.field private static final NORMALIZATION_ON:I = 0x1

.field private static final NORM_CORRECTNS_LAST_UNI_VERSION:I = 0x2

.field private static final ONE_UCHAR_MAPPING_INDEX_START:I = 0x3

.field private static final OPTIONS:I = 0x7

.field private static final PROHIBITED:I = 0x2

.field private static final THREE_UCHARS_MAPPING_INDEX_START:I = 0x5

.field private static final TWO_UCHARS_MAPPING_INDEX_START:I = 0x4

.field private static final TYPE_LIMIT:I = 0x4

.field private static final TYPE_THRESHOLD:I = 0xfff0

.field private static final UNASSIGNED:I


# instance fields
.field private bdp:Lcom/ibm/icu/impl/UBiDiProps;

.field private checkBiDi:Z

.field private doNFKC:Z

.field private indexes:[I

.field private mappingData:[C

.field private normCorrVer:Lcom/ibm/icu/util/VersionInfo;

.field private sprepTrie:Lcom/ibm/icu/impl/CharTrie;

.field private sprepUniVer:Lcom/ibm/icu/util/VersionInfo;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 10
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x7

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    new-instance v0, Ljava/io/BufferedInputStream;

    const/16 v4, 0x61a8

    invoke-direct {v0, p1, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 154
    .local v0, "b":Ljava/io/BufferedInputStream;
    new-instance v2, Lcom/ibm/icu/impl/StringPrepDataReader;

    invoke-direct {v2, v0}, Lcom/ibm/icu/impl/StringPrepDataReader;-><init>(Ljava/io/InputStream;)V

    .line 157
    .local v2, "reader":Lcom/ibm/icu/impl/StringPrepDataReader;
    const/16 v4, 0x10

    invoke-virtual {v2, v4}, Lcom/ibm/icu/impl/StringPrepDataReader;->readIndexes(I)[I

    move-result-object v4

    iput-object v4, p0, Lcom/ibm/icu/text/StringPrep;->indexes:[I

    .line 159
    iget-object v4, p0, Lcom/ibm/icu/text/StringPrep;->indexes:[I

    aget v4, v4, v6

    new-array v3, v4, [B

    .line 163
    .local v3, "sprepBytes":[B
    iget-object v4, p0, Lcom/ibm/icu/text/StringPrep;->indexes:[I

    aget v4, v4, v5

    div-int/lit8 v4, v4, 0x2

    new-array v4, v4, [C

    iput-object v4, p0, Lcom/ibm/icu/text/StringPrep;->mappingData:[C

    .line 165
    iget-object v4, p0, Lcom/ibm/icu/text/StringPrep;->mappingData:[C

    invoke-virtual {v2, v3, v4}, Lcom/ibm/icu/impl/StringPrepDataReader;->read([B[C)V

    .line 167
    new-instance v4, Lcom/ibm/icu/impl/CharTrie;

    new-instance v7, Ljava/io/ByteArrayInputStream;

    invoke-direct {v7, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const/4 v8, 0x0

    invoke-direct {v4, v7, v8}, Lcom/ibm/icu/impl/CharTrie;-><init>(Ljava/io/InputStream;Lcom/ibm/icu/impl/Trie$DataManipulate;)V

    iput-object v4, p0, Lcom/ibm/icu/text/StringPrep;->sprepTrie:Lcom/ibm/icu/impl/CharTrie;

    .line 170
    invoke-virtual {v2}, Lcom/ibm/icu/impl/StringPrepDataReader;->getDataFormatVersion()[B

    .line 173
    iget-object v4, p0, Lcom/ibm/icu/text/StringPrep;->indexes:[I

    aget v4, v4, v9

    and-int/lit8 v4, v4, 0x1

    if-lez v4, :cond_0

    move v4, v5

    :goto_0
    iput-boolean v4, p0, Lcom/ibm/icu/text/StringPrep;->doNFKC:Z

    .line 174
    iget-object v4, p0, Lcom/ibm/icu/text/StringPrep;->indexes:[I

    aget v4, v4, v9

    and-int/lit8 v4, v4, 0x2

    if-lez v4, :cond_1

    :goto_1
    iput-boolean v5, p0, Lcom/ibm/icu/text/StringPrep;->checkBiDi:Z

    .line 175
    invoke-virtual {v2}, Lcom/ibm/icu/impl/StringPrepDataReader;->getUnicodeVersion()[B

    move-result-object v4

    invoke-static {v4}, Lcom/ibm/icu/text/StringPrep;->getVersionInfo([B)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v4

    iput-object v4, p0, Lcom/ibm/icu/text/StringPrep;->sprepUniVer:Lcom/ibm/icu/util/VersionInfo;

    .line 176
    iget-object v4, p0, Lcom/ibm/icu/text/StringPrep;->indexes:[I

    const/4 v5, 0x2

    aget v4, v4, v5

    invoke-static {v4}, Lcom/ibm/icu/text/StringPrep;->getVersionInfo(I)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v4

    iput-object v4, p0, Lcom/ibm/icu/text/StringPrep;->normCorrVer:Lcom/ibm/icu/util/VersionInfo;

    .line 177
    invoke-static {}, Lcom/ibm/icu/text/Normalizer;->getUnicodeVersion()Lcom/ibm/icu/util/VersionInfo;

    move-result-object v1

    .line 178
    .local v1, "normUniVer":Lcom/ibm/icu/util/VersionInfo;
    iget-object v4, p0, Lcom/ibm/icu/text/StringPrep;->sprepUniVer:Lcom/ibm/icu/util/VersionInfo;

    invoke-virtual {v1, v4}, Lcom/ibm/icu/util/VersionInfo;->compareTo(Lcom/ibm/icu/util/VersionInfo;)I

    move-result v4

    if-gez v4, :cond_2

    iget-object v4, p0, Lcom/ibm/icu/text/StringPrep;->normCorrVer:Lcom/ibm/icu/util/VersionInfo;

    invoke-virtual {v1, v4}, Lcom/ibm/icu/util/VersionInfo;->compareTo(Lcom/ibm/icu/util/VersionInfo;)I

    move-result v4

    if-gez v4, :cond_2

    iget-object v4, p0, Lcom/ibm/icu/text/StringPrep;->indexes:[I

    aget v4, v4, v9

    and-int/lit8 v4, v4, 0x1

    if-lez v4, :cond_2

    .line 182
    new-instance v4, Ljava/io/IOException;

    const-string/jumbo v5, "Normalization Correction version not supported"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .end local v1    # "normUniVer":Lcom/ibm/icu/util/VersionInfo;
    :cond_0
    move v4, v6

    .line 173
    goto :goto_0

    :cond_1
    move v5, v6

    .line 174
    goto :goto_1

    .line 184
    .restart local v1    # "normUniVer":Lcom/ibm/icu/util/VersionInfo;
    :cond_2
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 186
    iget-boolean v4, p0, Lcom/ibm/icu/text/StringPrep;->checkBiDi:Z

    if-eqz v4, :cond_3

    .line 187
    invoke-static {}, Lcom/ibm/icu/impl/UBiDiProps;->getSingleton()Lcom/ibm/icu/impl/UBiDiProps;

    move-result-object v4

    iput-object v4, p0, Lcom/ibm/icu/text/StringPrep;->bdp:Lcom/ibm/icu/impl/UBiDiProps;

    .line 189
    :cond_3
    return-void
.end method

.method private getCodePointValue(I)C
    .locals 1
    .param p1, "ch"    # I

    .prologue
    .line 125
    iget-object v0, p0, Lcom/ibm/icu/text/StringPrep;->sprepTrie:Lcom/ibm/icu/impl/CharTrie;

    invoke-virtual {v0, p1}, Lcom/ibm/icu/impl/CharTrie;->getCodePointValue(I)C

    move-result v0

    return v0
.end method

.method private static final getValues(CLcom/ibm/icu/text/StringPrep$Values;)V
    .locals 3
    .param p0, "trieWord"    # C
    .param p1, "values"    # Lcom/ibm/icu/text/StringPrep$Values;

    .prologue
    const v0, 0xfff0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 203
    invoke-virtual {p1}, Lcom/ibm/icu/text/StringPrep$Values;->reset()V

    .line 204
    if-nez p0, :cond_1

    .line 210
    const/4 v0, 0x4

    iput v0, p1, Lcom/ibm/icu/text/StringPrep$Values;->type:I

    .line 234
    :cond_0
    :goto_0
    return-void

    .line 211
    :cond_1
    if-lt p0, v0, :cond_2

    .line 212
    sub-int v0, p0, v0

    iput v0, p1, Lcom/ibm/icu/text/StringPrep$Values;->type:I

    goto :goto_0

    .line 215
    :cond_2
    iput v1, p1, Lcom/ibm/icu/text/StringPrep$Values;->type:I

    .line 217
    and-int/lit8 v0, p0, 0x2

    if-lez v0, :cond_3

    .line 218
    iput-boolean v1, p1, Lcom/ibm/icu/text/StringPrep$Values;->isIndex:Z

    .line 219
    shr-int/lit8 v0, p0, 0x2

    iput v0, p1, Lcom/ibm/icu/text/StringPrep$Values;->value:I

    .line 228
    :goto_1
    shr-int/lit8 v0, p0, 0x2

    const/16 v1, 0x3fbf

    if-ne v0, v1, :cond_0

    .line 229
    const/4 v0, 0x3

    iput v0, p1, Lcom/ibm/icu/text/StringPrep$Values;->type:I

    .line 230
    iput-boolean v2, p1, Lcom/ibm/icu/text/StringPrep$Values;->isIndex:Z

    .line 231
    iput v2, p1, Lcom/ibm/icu/text/StringPrep$Values;->value:I

    goto :goto_0

    .line 222
    :cond_3
    iput-boolean v2, p1, Lcom/ibm/icu/text/StringPrep$Values;->isIndex:Z

    .line 223
    shl-int/lit8 v0, p0, 0x10

    shr-int/lit8 v0, v0, 0x10

    iput v0, p1, Lcom/ibm/icu/text/StringPrep$Values;->value:I

    .line 224
    iget v0, p1, Lcom/ibm/icu/text/StringPrep$Values;->value:I

    shr-int/lit8 v0, v0, 0x2

    iput v0, p1, Lcom/ibm/icu/text/StringPrep$Values;->value:I

    goto :goto_1
.end method

.method private static getVersionInfo(I)Lcom/ibm/icu/util/VersionInfo;
    .locals 5
    .param p0, "comp"    # I

    .prologue
    .line 129
    and-int/lit16 v1, p0, 0xff

    .line 130
    .local v1, "micro":I
    shr-int/lit8 v4, p0, 0x8

    and-int/lit16 v2, v4, 0xff

    .line 131
    .local v2, "milli":I
    shr-int/lit8 v4, p0, 0x10

    and-int/lit16 v3, v4, 0xff

    .line 132
    .local v3, "minor":I
    shr-int/lit8 v4, p0, 0x18

    and-int/lit16 v0, v4, 0xff

    .line 133
    .local v0, "major":I
    invoke-static {v0, v3, v2, v1}, Lcom/ibm/icu/util/VersionInfo;->getInstance(IIII)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v4

    return-object v4
.end method

.method private static getVersionInfo([B)Lcom/ibm/icu/util/VersionInfo;
    .locals 4
    .param p0, "version"    # [B

    .prologue
    .line 136
    array-length v0, p0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 137
    const/4 v0, 0x0

    .line 139
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    aget-byte v0, p0, v0

    const/4 v1, 0x1

    aget-byte v1, p0, v1

    const/4 v2, 0x2

    aget-byte v2, p0, v2

    const/4 v3, 0x3

    aget-byte v3, p0, v3

    invoke-static {v0, v1, v2, v3}, Lcom/ibm/icu/util/VersionInfo;->getInstance(IIII)Lcom/ibm/icu/util/VersionInfo;

    move-result-object v0

    goto :goto_0
.end method

.method private map(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;
    .locals 13
    .param p1, "iter"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p2, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 241
    new-instance v7, Lcom/ibm/icu/text/StringPrep$Values;

    const/4 v8, 0x0

    invoke-direct {v7, v8}, Lcom/ibm/icu/text/StringPrep$Values;-><init>(Lcom/ibm/icu/text/StringPrep$1;)V

    .line 242
    .local v7, "val":Lcom/ibm/icu/text/StringPrep$Values;
    const/4 v6, 0x0

    .line 243
    .local v6, "result":C
    const/4 v1, -0x1

    .line 244
    .local v1, "ch":I
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 245
    .local v2, "dest":Ljava/lang/StringBuffer;
    and-int/lit8 v8, p2, 0x1

    if-lez v8, :cond_0

    const/4 v0, 0x1

    .line 247
    .local v0, "allowUnassigned":Z
    :goto_0
    invoke-virtual {p1}, Lcom/ibm/icu/text/UCharacterIterator;->nextCodePoint()I

    move-result v1

    const/4 v8, -0x1

    if-eq v1, v8, :cond_8

    .line 249
    invoke-direct {p0, v1}, Lcom/ibm/icu/text/StringPrep;->getCodePointValue(I)C

    move-result v6

    .line 250
    invoke-static {v6, v7}, Lcom/ibm/icu/text/StringPrep;->getValues(CLcom/ibm/icu/text/StringPrep$Values;)V

    .line 253
    iget v8, v7, Lcom/ibm/icu/text/StringPrep$Values;->type:I

    if-nez v8, :cond_1

    if-nez v0, :cond_1

    .line 254
    new-instance v8, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v9, "An unassigned code point was found in the input"

    const/4 v10, 0x3

    invoke-virtual {p1}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v12

    invoke-direct {v8, v9, v10, v11, v12}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    throw v8

    .line 245
    .end local v0    # "allowUnassigned":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 257
    .restart local v0    # "allowUnassigned":Z
    :cond_1
    iget v8, v7, Lcom/ibm/icu/text/StringPrep$Values;->type:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_7

    .line 260
    iget-boolean v8, v7, Lcom/ibm/icu/text/StringPrep$Values;->isIndex:Z

    if-eqz v8, :cond_5

    .line 261
    iget v3, v7, Lcom/ibm/icu/text/StringPrep$Values;->value:I

    .line 262
    .local v3, "index":I
    iget-object v8, p0, Lcom/ibm/icu/text/StringPrep;->indexes:[I

    const/4 v9, 0x3

    aget v8, v8, v9

    if-lt v3, v8, :cond_2

    iget-object v8, p0, Lcom/ibm/icu/text/StringPrep;->indexes:[I

    const/4 v9, 0x4

    aget v8, v8, v9

    if-ge v3, v8, :cond_2

    .line 264
    const/4 v5, 0x1

    .line 275
    .local v5, "length":I
    :goto_1
    iget-object v8, p0, Lcom/ibm/icu/text/StringPrep;->mappingData:[C

    invoke-virtual {v2, v8, v3, v5}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 265
    .end local v5    # "length":I
    :cond_2
    iget-object v8, p0, Lcom/ibm/icu/text/StringPrep;->indexes:[I

    const/4 v9, 0x4

    aget v8, v8, v9

    if-lt v3, v8, :cond_3

    iget-object v8, p0, Lcom/ibm/icu/text/StringPrep;->indexes:[I

    const/4 v9, 0x5

    aget v8, v8, v9

    if-ge v3, v8, :cond_3

    .line 267
    const/4 v5, 0x2

    .line 268
    .restart local v5    # "length":I
    goto :goto_1

    .end local v5    # "length":I
    :cond_3
    iget-object v8, p0, Lcom/ibm/icu/text/StringPrep;->indexes:[I

    const/4 v9, 0x5

    aget v8, v8, v9

    if-lt v3, v8, :cond_4

    iget-object v8, p0, Lcom/ibm/icu/text/StringPrep;->indexes:[I

    const/4 v9, 0x6

    aget v8, v8, v9

    if-ge v3, v8, :cond_4

    .line 270
    const/4 v5, 0x3

    .line 271
    .restart local v5    # "length":I
    goto :goto_1

    .line 272
    .end local v5    # "length":I
    :cond_4
    iget-object v8, p0, Lcom/ibm/icu/text/StringPrep;->mappingData:[C

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "index":I
    .local v4, "index":I
    aget-char v5, v8, v3

    .restart local v5    # "length":I
    move v3, v4

    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_1

    .line 279
    .end local v3    # "index":I
    .end local v5    # "length":I
    :cond_5
    iget v8, v7, Lcom/ibm/icu/text/StringPrep$Values;->value:I

    sub-int/2addr v1, v8

    .line 286
    :cond_6
    invoke-static {v2, v1}, Lcom/ibm/icu/text/UTF16;->append(Ljava/lang/StringBuffer;I)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 281
    :cond_7
    iget v8, v7, Lcom/ibm/icu/text/StringPrep$Values;->type:I

    const/4 v9, 0x3

    if-ne v8, v9, :cond_6

    goto :goto_0

    .line 289
    :cond_8
    return-object v2
.end method

.method private normalize(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 4
    .param p1, "src"    # Ljava/lang/StringBuffer;

    .prologue
    .line 305
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/ibm/icu/text/Normalizer;->NFKC:Lcom/ibm/icu/text/Normalizer$Mode;

    const/16 v3, 0x120

    invoke-static {v1, v2, v3}, Lcom/ibm/icu/text/Normalizer;->normalize(Ljava/lang/String;Lcom/ibm/icu/text/Normalizer$Mode;I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public prepare(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;
    .locals 18
    .param p1, "src"    # Lcom/ibm/icu/text/UCharacterIterator;
    .param p2, "options"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/ibm/icu/text/StringPrepParseException;
        }
    .end annotation

    .prologue
    .line 379
    invoke-direct/range {p0 .. p2}, Lcom/ibm/icu/text/StringPrep;->map(Lcom/ibm/icu/text/UCharacterIterator;I)Ljava/lang/StringBuffer;

    move-result-object v7

    .line 380
    .local v7, "mapOut":Ljava/lang/StringBuffer;
    move-object v8, v7

    .line 382
    .local v8, "normOut":Ljava/lang/StringBuffer;
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/ibm/icu/text/StringPrep;->doNFKC:Z

    if-eqz v13, :cond_0

    .line 384
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/ibm/icu/text/StringPrep;->normalize(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v8

    .line 389
    :cond_0
    invoke-static {v8}, Lcom/ibm/icu/text/UCharacterIterator;->getInstance(Ljava/lang/StringBuffer;)Lcom/ibm/icu/text/UCharacterIterator;

    move-result-object v4

    .line 390
    .local v4, "iter":Lcom/ibm/icu/text/UCharacterIterator;
    new-instance v12, Lcom/ibm/icu/text/StringPrep$Values;

    const/4 v13, 0x0

    invoke-direct {v12, v13}, Lcom/ibm/icu/text/StringPrep$Values;-><init>(Lcom/ibm/icu/text/StringPrep$1;)V

    .line 391
    .local v12, "val":Lcom/ibm/icu/text/StringPrep$Values;
    const/16 v2, 0x13

    .line 392
    .local v2, "direction":I
    const/16 v3, 0x13

    .line 393
    .local v3, "firstCharDir":I
    const/4 v11, -0x1

    .local v11, "rtlPos":I
    const/4 v6, -0x1

    .line 394
    .local v6, "ltrPos":I
    const/4 v10, 0x0

    .local v10, "rightToLeft":Z
    const/4 v5, 0x0

    .line 396
    .local v5, "leftToRight":Z
    :cond_1
    :goto_0
    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->nextCodePoint()I

    move-result v1

    .local v1, "ch":I
    const/4 v13, -0x1

    if-eq v1, v13, :cond_6

    .line 397
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/ibm/icu/text/StringPrep;->getCodePointValue(I)C

    move-result v9

    .line 398
    .local v9, "result":C
    invoke-static {v9, v12}, Lcom/ibm/icu/text/StringPrep;->getValues(CLcom/ibm/icu/text/StringPrep$Values;)V

    .line 400
    iget v13, v12, Lcom/ibm/icu/text/StringPrep$Values;->type:I

    const/4 v14, 0x2

    if-ne v13, v14, :cond_2

    .line 401
    new-instance v13, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v14, "A prohibited code point was found in the input"

    const/4 v15, 0x2

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v16

    iget v0, v12, Lcom/ibm/icu/text/StringPrep$Values;->value:I

    move/from16 v17, v0

    invoke-direct/range {v13 .. v17}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    throw v13

    .line 405
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/ibm/icu/text/StringPrep;->checkBiDi:Z

    if-eqz v13, :cond_1

    .line 406
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/StringPrep;->bdp:Lcom/ibm/icu/impl/UBiDiProps;

    invoke-virtual {v13, v1}, Lcom/ibm/icu/impl/UBiDiProps;->getClass(I)I

    move-result v2

    .line 407
    const/16 v13, 0x13

    if-ne v3, v13, :cond_3

    .line 408
    move v3, v2

    .line 410
    :cond_3
    if-nez v2, :cond_4

    .line 411
    const/4 v5, 0x1

    .line 412
    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v13

    add-int/lit8 v6, v13, -0x1

    .line 414
    :cond_4
    const/4 v13, 0x1

    if-eq v2, v13, :cond_5

    const/16 v13, 0xd

    if-ne v2, v13, :cond_1

    .line 415
    :cond_5
    const/4 v10, 0x1

    .line 416
    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->getIndex()I

    move-result v13

    add-int/lit8 v11, v13, -0x1

    .line 417
    goto :goto_0

    .line 420
    .end local v9    # "result":C
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/ibm/icu/text/StringPrep;->checkBiDi:Z

    const/4 v14, 0x1

    if-ne v13, v14, :cond_c

    .line 422
    const/4 v13, 0x1

    if-ne v5, v13, :cond_8

    const/4 v13, 0x1

    if-ne v10, v13, :cond_8

    .line 423
    new-instance v13, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v14, "The input does not conform to the rules for BiDi code points."

    const/4 v15, 0x4

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v16

    if-le v11, v6, :cond_7

    .end local v11    # "rtlPos":I
    :goto_1
    move-object/from16 v0, v16

    invoke-direct {v13, v14, v15, v0, v11}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    throw v13

    .restart local v11    # "rtlPos":I
    :cond_7
    move v11, v6

    goto :goto_1

    .line 429
    :cond_8
    const/4 v13, 0x1

    if-ne v10, v13, :cond_c

    const/4 v13, 0x1

    if-eq v3, v13, :cond_9

    const/16 v13, 0xd

    if-ne v3, v13, :cond_a

    :cond_9
    const/4 v13, 0x1

    if-eq v2, v13, :cond_c

    const/16 v13, 0xd

    if-eq v2, v13, :cond_c

    .line 433
    :cond_a
    new-instance v13, Lcom/ibm/icu/text/StringPrepParseException;

    const-string/jumbo v14, "The input does not conform to the rules for BiDi code points."

    const/4 v15, 0x4

    invoke-virtual {v4}, Lcom/ibm/icu/text/UCharacterIterator;->getText()Ljava/lang/String;

    move-result-object v16

    if-le v11, v6, :cond_b

    .end local v11    # "rtlPos":I
    :goto_2
    move-object/from16 v0, v16

    invoke-direct {v13, v14, v15, v0, v11}, Lcom/ibm/icu/text/StringPrepParseException;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    throw v13

    .restart local v11    # "rtlPos":I
    :cond_b
    move v11, v6

    goto :goto_2

    .line 438
    :cond_c
    return-object v8
.end method
