.class Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
.super Ljava/lang/Object;
.source "DateTimePatternGenerator.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/ibm/icu/text/DateTimePatternGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DateTimeMatcher"
.end annotation


# instance fields
.field private baseOriginal:[Ljava/lang/String;

.field private original:[Ljava/lang/String;

.field private type:[I


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 1650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1652
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->type:[I

    .line 1653
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    .line 1654
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->baseOriginal:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/ibm/icu/text/DateTimePatternGenerator$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/ibm/icu/text/DateTimePatternGenerator$1;

    .prologue
    .line 1650
    invoke-direct {p0}, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;-><init>()V

    return-void
.end method

.method static access$500(Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    .prologue
    .line 1650
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    return-object v0
.end method

.method static access$600(Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;)[I
    .locals 1
    .param p0, "x0"    # Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    .prologue
    .line 1650
    iget-object v0, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->type:[I

    return-object v0
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1761
    move-object v2, p1

    check-cast v2, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    .line 1762
    .local v2, "that":Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 1763
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    aget-object v3, v3, v1

    iget-object v4, v2, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 1764
    .local v0, "comp":I
    if-eqz v0, :cond_0

    neg-int v3, v0

    .line 1766
    .end local v0    # "comp":I
    :goto_1
    return v3

    .line 1762
    .restart local v0    # "comp":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1766
    .end local v0    # "comp":I
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 1770
    if-nez p1, :cond_1

    .line 1775
    :cond_0
    :goto_0
    return v2

    :cond_1
    move-object v1, p1

    .line 1771
    check-cast v1, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;

    .line 1772
    .local v1, "that":Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 1773
    iget-object v3, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    aget-object v3, v3, v0

    iget-object v4, v1, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1772
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1775
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method extractFrom(Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;I)V
    .locals 3
    .param p1, "source"    # Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
    .param p2, "fieldMask"    # I

    .prologue
    .line 1729
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->type:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1730
    const/4 v1, 0x1

    shl-int/2addr v1, v0

    and-int/2addr v1, p2

    if-eqz v1, :cond_0

    .line 1731
    iget-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->type:[I

    iget-object v2, p1, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->type:[I

    aget v2, v2, v0

    aput v2, v1, v0

    .line 1732
    iget-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    iget-object v2, p1, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    aget-object v2, v2, v0

    aput-object v2, v1, v0

    .line 1729
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1734
    :cond_0
    iget-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->type:[I

    const/4 v2, 0x0

    aput v2, v1, v0

    .line 1735
    iget-object v1, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    const-string/jumbo v2, ""

    aput-object v2, v1, v0

    goto :goto_1

    .line 1738
    :cond_1
    return-void
.end method

.method getBasePattern()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1668
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1669
    .local v1, "result":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x10

    if-ge v0, v2, :cond_1

    .line 1670
    iget-object v2, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->baseOriginal:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->baseOriginal:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1669
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1672
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method getDistance(Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;ILcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;)I
    .locals 5
    .param p1, "other"    # Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
    .param p2, "includeMask"    # I
    .param p3, "distanceInfo"    # Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;

    .prologue
    .line 1741
    const/4 v3, 0x0

    .line 1742
    .local v3, "result":I
    invoke-virtual {p3}, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;->clear()V

    .line 1743
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->type:[I

    array-length v4, v4

    if-ge v0, v4, :cond_4

    .line 1744
    const/4 v4, 0x1

    shl-int/2addr v4, v0

    and-int/2addr v4, p2

    if-nez v4, :cond_0

    const/4 v1, 0x0

    .line 1745
    .local v1, "myType":I
    :goto_1
    iget-object v4, p1, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->type:[I

    aget v2, v4, v0

    .line 1746
    .local v2, "otherType":I
    if-ne v1, v2, :cond_1

    .line 1743
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1744
    .end local v1    # "myType":I
    .end local v2    # "otherType":I
    :cond_0
    iget-object v4, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->type:[I

    aget v1, v4, v0

    goto :goto_1

    .line 1747
    .restart local v1    # "myType":I
    .restart local v2    # "otherType":I
    :cond_1
    if-nez v1, :cond_2

    .line 1748
    const/high16 v4, 0x10000

    add-int/2addr v3, v4

    .line 1749
    invoke-virtual {p3, v0}, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;->addExtra(I)V

    goto :goto_2

    .line 1750
    :cond_2
    if-nez v2, :cond_3

    .line 1751
    add-int/lit16 v3, v3, 0x1000

    .line 1752
    invoke-virtual {p3, v0}, Lcom/ibm/icu/text/DateTimePatternGenerator$DistanceInfo;->addMissing(I)V

    goto :goto_2

    .line 1754
    :cond_3
    sub-int v4, v1, v2

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_2

    .line 1757
    .end local v1    # "myType":I
    .end local v2    # "otherType":I
    :cond_4
    return v3
.end method

.method getFieldMask()I
    .locals 3

    .prologue
    .line 1718
    const/4 v1, 0x0

    .line 1719
    .local v1, "result":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->type:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1720
    iget-object v2, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->type:[I

    aget v2, v2, v0

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    shl-int/2addr v2, v0

    or-int/2addr v1, v2

    .line 1719
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1722
    :cond_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 1778
    const/4 v1, 0x0

    .line 1779
    .local v1, "result":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1780
    iget-object v2, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v1, v2

    .line 1779
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1782
    :cond_0
    return v1
.end method

.method set(Ljava/lang/String;Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;)Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;
    .locals 16
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "fp"    # Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    .prologue
    .line 1676
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    const/16 v13, 0x10

    if-ge v4, v13, :cond_0

    .line 1677
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->type:[I

    const/4 v14, 0x0

    aput v14, v13, v4

    .line 1678
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    const-string/jumbo v14, ""

    aput-object v14, v13, v4

    .line 1679
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->baseOriginal:[Ljava/lang/String;

    const-string/jumbo v14, ""

    aput-object v14, v13, v4

    .line 1676
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1681
    :cond_0
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->set(Ljava/lang/String;)Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;

    .line 1682
    invoke-virtual/range {p2 .. p2}, Lcom/ibm/icu/text/DateTimePatternGenerator$FormatParser;->getItems()Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "it":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 1683
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 1684
    .local v7, "obj":Ljava/lang/Object;
    instance-of v13, v7, Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;

    if-eqz v13, :cond_1

    move-object v6, v7

    .line 1687
    check-cast v6, Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;

    .line 1688
    .local v6, "item":Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;
    invoke-virtual {v6}, Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1689
    .local v3, "field":Ljava/lang/String;
    const/4 v13, 0x0

    invoke-virtual {v3, v13}, Ljava/lang/String;->charAt(I)C

    move-result v13

    const/16 v14, 0x61

    if-eq v13, v14, :cond_1

    .line 1690
    invoke-static {v6}, Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;->access$700(Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;)I

    move-result v2

    .line 1695
    .local v2, "canonicalIndex":I
    invoke-static {}, Lcom/ibm/icu/text/DateTimePatternGenerator;->access$400()[[I

    move-result-object v13

    aget-object v10, v13, v2

    .line 1696
    .local v10, "row":[I
    const/4 v13, 0x1

    aget v12, v10, v13

    .line 1697
    .local v12, "typeValue":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    aget-object v13, v13, v12

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    if-eqz v13, :cond_2

    .line 1698
    new-instance v13, Ljava/lang/IllegalArgumentException;

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string/jumbo v15, "Conflicting fields:\t"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    aget-object v15, v15, v12

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string/jumbo v15, ", "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string/jumbo v15, "\t in "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 1701
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    aput-object v3, v13, v12

    .line 1702
    const/4 v13, 0x0

    aget v13, v10, v13

    int-to-char v8, v13

    .line 1703
    .local v8, "repeatChar":C
    const/4 v13, 0x3

    aget v9, v10, v13

    .line 1704
    .local v9, "repeatCount":I
    const/4 v13, 0x3

    if-le v9, v13, :cond_3

    const/4 v9, 0x3

    .line 1705
    :cond_3
    const-string/jumbo v13, "GEzvQ"

    invoke-virtual {v13, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v13

    if-ltz v13, :cond_4

    const/4 v9, 0x1

    .line 1706
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->baseOriginal:[Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14, v9}, Lcom/ibm/icu/impl/Utility;->repeat(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v12

    .line 1707
    const/4 v13, 0x2

    aget v11, v10, v13

    .line 1708
    .local v11, "subTypeValue":I
    if-lez v11, :cond_5

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v11, v13

    .line 1709
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->type:[I

    int-to-byte v14, v11

    aput v14, v13, v12

    goto/16 :goto_1

    .line 1711
    .end local v2    # "canonicalIndex":I
    .end local v3    # "field":Ljava/lang/String;
    .end local v6    # "item":Lcom/ibm/icu/text/DateTimePatternGenerator$VariableField;
    .end local v7    # "obj":Ljava/lang/Object;
    .end local v8    # "repeatChar":C
    .end local v9    # "repeatCount":I
    .end local v10    # "row":[I
    .end local v11    # "subTypeValue":I
    .end local v12    # "typeValue":I
    :cond_6
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1660
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1661
    .local v1, "result":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x10

    if-ge v0, v2, :cond_1

    .line 1662
    iget-object v2, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/ibm/icu/text/DateTimePatternGenerator$DateTimeMatcher;->original:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1661
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1664
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
