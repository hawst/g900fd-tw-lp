.class Lcom/ibm/icu/text/UnicodeNameTransliterator;
.super Lcom/ibm/icu/text/Transliterator;
.source "UnicodeNameTransliterator.java"


# static fields
.field static final CLOSE_DELIM:C = '}'

.field static final OPEN_DELIM:Ljava/lang/String; = "\\N{"

.field static final OPEN_DELIM_LEN:I = 0x3

.field static final _ID:Ljava/lang/String; = "Any-Name"


# direct methods
.method public constructor <init>(Lcom/ibm/icu/text/UnicodeFilter;)V
    .locals 1
    .param p1, "filter"    # Lcom/ibm/icu/text/UnicodeFilter;

    .prologue
    .line 36
    const-string/jumbo v0, "Any-Name"

    invoke-direct {p0, v0, p1}, Lcom/ibm/icu/text/Transliterator;-><init>(Ljava/lang/String;Lcom/ibm/icu/text/UnicodeFilter;)V

    .line 37
    return-void
.end method

.method static register()V
    .locals 2

    .prologue
    .line 25
    const-string/jumbo v0, "Any-Name"

    new-instance v1, Lcom/ibm/icu/text/UnicodeNameTransliterator$1;

    invoke-direct {v1}, Lcom/ibm/icu/text/UnicodeNameTransliterator$1;-><init>()V

    invoke-static {v0, v1}, Lcom/ibm/icu/text/Transliterator;->registerFactory(Ljava/lang/String;Lcom/ibm/icu/text/Transliterator$Factory;)V

    .line 30
    return-void
.end method


# virtual methods
.method protected handleTransliterate(Lcom/ibm/icu/text/Replaceable;Lcom/ibm/icu/text/Transliterator$Position;Z)V
    .locals 9
    .param p1, "text"    # Lcom/ibm/icu/text/Replaceable;
    .param p2, "offsets"    # Lcom/ibm/icu/text/Transliterator$Position;
    .param p3, "isIncremental"    # Z

    .prologue
    .line 44
    iget v2, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 45
    .local v2, "cursor":I
    iget v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 47
    .local v4, "limit":I
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 48
    .local v6, "str":Ljava/lang/StringBuffer;
    const-string/jumbo v7, "\\N{"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 52
    :goto_0
    if-ge v2, v4, :cond_1

    .line 53
    invoke-interface {p1, v2}, Lcom/ibm/icu/text/Replaceable;->char32At(I)I

    move-result v0

    .line 54
    .local v0, "c":I
    invoke-static {v0}, Lcom/ibm/icu/lang/UCharacter;->getExtendedName(I)Ljava/lang/String;

    move-result-object v5

    .local v5, "name":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 56
    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 57
    invoke-virtual {v6, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const/16 v8, 0x7d

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 59
    invoke-static {v0}, Lcom/ibm/icu/text/UTF16;->getCharCount(I)I

    move-result v1

    .line 60
    .local v1, "clen":I
    add-int v7, v2, v1

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p1, v2, v7, v8}, Lcom/ibm/icu/text/Replaceable;->replace(IILjava/lang/String;)V

    .line 61
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    .line 62
    .local v3, "len":I
    add-int/2addr v2, v3

    .line 63
    sub-int v7, v3, v1

    add-int/2addr v4, v7

    .line 64
    goto :goto_0

    .line 65
    .end local v1    # "clen":I
    .end local v3    # "len":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 69
    .end local v0    # "c":I
    .end local v5    # "name":Ljava/lang/String;
    :cond_1
    iget v7, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    iget v8, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    sub-int v8, v4, v8

    add-int/2addr v7, v8

    iput v7, p2, Lcom/ibm/icu/text/Transliterator$Position;->contextLimit:I

    .line 70
    iput v4, p2, Lcom/ibm/icu/text/Transliterator$Position;->limit:I

    .line 71
    iput v2, p2, Lcom/ibm/icu/text/Transliterator$Position;->start:I

    .line 72
    return-void
.end method
