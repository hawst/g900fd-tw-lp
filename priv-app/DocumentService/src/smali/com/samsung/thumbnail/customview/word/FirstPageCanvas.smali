.class public Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
.super Ljava/lang/Object;
.source "FirstPageCanvas.java"


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCanvas:Landroid/graphics/Canvas;

.field private mCanvasDraw:Lcom/samsung/thumbnail/customview/draw/CanvasDraw;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 1
    .param p1, "elementCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;

    invoke-direct {v0, p1}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->mCanvasDraw:Lcom/samsung/thumbnail/customview/draw/CanvasDraw;

    .line 18
    return-void
.end method


# virtual methods
.method public clearCanvas()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->mCanvas:Landroid/graphics/Canvas;

    .line 37
    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->mBitmap:Landroid/graphics/Bitmap;

    .line 38
    return-void
.end method

.method public drawOnCanvas(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->mCanvasDraw:Lcom/samsung/thumbnail/customview/draw/CanvasDraw;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1, p1}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->handleDraw(Landroid/graphics/Canvas;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public getCanvas()Landroid/graphics/Canvas;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->mCanvas:Landroid/graphics/Canvas;

    return-object v0
.end method

.method public getCanvasBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getCanvasDraw()Lcom/samsung/thumbnail/customview/draw/CanvasDraw;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->mCanvasDraw:Lcom/samsung/thumbnail/customview/draw/CanvasDraw;

    return-object v0
.end method

.method public setCanvas(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->mCanvas:Landroid/graphics/Canvas;

    if-nez v0, :cond_0

    .line 28
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->mCanvas:Landroid/graphics/Canvas;

    .line 29
    :cond_0
    return-void
.end method

.method public setCanvasBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 50
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->mBitmap:Landroid/graphics/Bitmap;

    .line 51
    :cond_0
    return-void
.end method
