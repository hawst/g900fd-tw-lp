.class public Lcom/samsung/thumbnail/customview/hslf/TabValues;
.super Ljava/lang/Object;
.source "TabValues.java"


# instance fields
.field private mPosition:I

.field private mVal:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "position"    # I

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/TabValues;->mVal:Ljava/lang/String;

    .line 10
    iput p2, p0, Lcom/samsung/thumbnail/customview/hslf/TabValues;->mPosition:I

    .line 11
    return-void
.end method


# virtual methods
.method public getPosition()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/TabValues;->mPosition:I

    return v0
.end method

.method public getVal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/TabValues;->mVal:Ljava/lang/String;

    return-object v0
.end method

.method public setPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/TabValues;->mPosition:I

    .line 27
    return-void
.end method

.method public setVal(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/TabValues;->mVal:Ljava/lang/String;

    .line 19
    return-void
.end method
