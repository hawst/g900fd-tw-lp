.class public Lcom/samsung/thumbnail/customview/draw/CanvasDraw;
.super Ljava/lang/Object;
.source "CanvasDraw.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/customview/draw/CanvasDraw$5;
    }
.end annotation


# static fields
.field private static final FIRST_BORDER_PADDING:S = 0x5s

.field public static final INITIAL_XVAL:I = 0x0

.field public static final INITIAL_YVAL:I = -0x1

.field private static final SECOND_BORDER_PADDING:S = 0xas

.field private static final TAG:Ljava/lang/String; = "CanvasDraw"

.field private static final THIRD_BORDER_PADDING:S = 0xfs


# instance fields
.field private backgroundColorStatus:Z

.field private elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field private footerDrawStarted:Z

.field private footerHeight:I

.field private headerDrawStarted:Z

.field private isContentDrawn:Z

.field private mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

.field private mCurrentParagraph:I

.field private mFooterRefVal:I

.field private mHeaderRefval:I

.field private mPreYValue:I

.field private mPrevShapeHeight:F

.field private mPreviousImgHei:I

.field private mPreviousImgWid:I

.field private mPreviousParagraph:I

.field private mTableHeight:I

.field private maxWaterMarkId:I

.field private normalDrawStarted:Z

.field private x:I

.field private y:I


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 2
    .param p1, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    const/4 v1, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 73
    iput v1, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    .line 75
    iput v1, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mTableHeight:I

    .line 76
    iput v1, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentParagraph:I

    .line 77
    iput v1, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousParagraph:I

    .line 81
    iput v1, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->maxWaterMarkId:I

    .line 104
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->isContentDrawn:Z

    .line 112
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 113
    return-void
.end method

.method private drawBitMap(Landroid/graphics/Bitmap;IIIIILjava/lang/String;Landroid/graphics/Canvas;Z)V
    .locals 9
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "rotation"    # I
    .param p7, "enclosedSpName"    # Ljava/lang/String;
    .param p8, "canvas"    # Landroid/graphics/Canvas;
    .param p9, "headerOrFooter"    # Z

    .prologue
    .line 1721
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 1723
    .local v6, "p":Landroid/graphics/Paint;
    if-eqz p9, :cond_0

    .line 1724
    const/16 v1, 0x32

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1726
    :cond_0
    if-eqz p1, :cond_5

    .line 1727
    const/4 v8, 0x0

    .line 1737
    .local v8, "scalableBitmap":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    :try_start_0
    invoke-static {p1, p4, p5, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 1743
    :goto_0
    if-eqz p6, :cond_1

    .line 1744
    invoke-virtual/range {p8 .. p8}, Landroid/graphics/Canvas;->save()I

    .line 1745
    int-to-float v1, p6

    div-int/lit8 v2, p4, 0x2

    add-int/2addr v2, p2

    int-to-float v2, v2

    div-int/lit8 v3, p5, 0x2

    add-int/2addr v3, p3

    int-to-float v3, v3

    move-object/from16 v0, p8

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1749
    :cond_1
    if-eqz v8, :cond_7

    .line 1750
    if-eqz p9, :cond_2

    .line 1751
    const/16 v1, 0x64

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1752
    const/4 v1, -0x1

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1753
    int-to-float v2, p2

    int-to-float v3, p3

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    add-int/2addr v1, p2

    int-to-float v4, v1

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    add-int/2addr v1, p3

    int-to-float v5, v1

    move-object/from16 v1, p8

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1756
    const/16 v1, 0x32

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1759
    :cond_2
    if-eqz p7, :cond_3

    const-string/jumbo v1, "rect"

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1761
    :cond_3
    int-to-float v1, p2

    int-to-float v2, p3

    move-object/from16 v0, p8

    invoke-virtual {v0, v8, v1, v2, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1800
    :cond_4
    :goto_1
    if-eqz p6, :cond_5

    .line 1802
    :try_start_1
    invoke-virtual/range {p8 .. p8}, Landroid/graphics/Canvas;->restore()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1808
    .end local v8    # "scalableBitmap":Landroid/graphics/Bitmap;
    :cond_5
    :goto_2
    return-void

    .line 1739
    .restart local v8    # "scalableBitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v7

    .line 1740
    .local v7, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "CanvasDraw"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1763
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_6
    const-string/jumbo v1, "ellipse"

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1765
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1766
    new-instance v1, Landroid/graphics/BitmapShader;

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v3, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-direct {v1, v8, v2, v3}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1768
    int-to-float v1, p2

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    int-to-float v2, p3

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p8

    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 1775
    :cond_7
    if-eqz p9, :cond_8

    .line 1776
    const/16 v1, 0x64

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1777
    const/4 v1, -0x1

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1778
    int-to-float v2, p2

    int-to-float v3, p3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    add-int/2addr v1, p2

    int-to-float v4, v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    add-int/2addr v1, p3

    int-to-float v5, v1

    move-object/from16 v1, p8

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1780
    const/16 v1, 0x32

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1783
    :cond_8
    if-eqz p7, :cond_9

    const-string/jumbo v1, "rect"

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1785
    :cond_9
    int-to-float v1, p2

    int-to-float v2, p3

    move-object/from16 v0, p8

    invoke-virtual {v0, p1, v1, v2, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 1787
    :cond_a
    const-string/jumbo v1, "ellipse"

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1789
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1790
    new-instance v1, Landroid/graphics/BitmapShader;

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v3, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-direct {v1, p1, v2, v3}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1792
    int-to-float v1, p2

    int-to-float v2, p3

    int-to-float v3, p4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p8

    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1793
    int-to-float v1, p2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    int-to-float v2, p3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p8

    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 1803
    :catch_1
    move-exception v7

    .line 1804
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string/jumbo v1, "CanvasDraw"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method private drawBitMapXlsx(Landroid/graphics/Bitmap;IIIIILandroid/graphics/Canvas;)V
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I
    .param p6, "rotation"    # I
    .param p7, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x0

    .line 1813
    if-eqz p1, :cond_1

    .line 1814
    const/4 v1, 0x0

    .line 1817
    .local v1, "scalableBitmap":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    :try_start_0
    invoke-static {p1, p4, p5, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1824
    :goto_0
    if-eqz p6, :cond_0

    .line 1825
    invoke-virtual {p7}, Landroid/graphics/Canvas;->save()I

    .line 1826
    int-to-float v2, p6

    div-int/lit8 v3, p4, 0x2

    add-int/2addr v3, p2

    int-to-float v3, v3

    div-int/lit8 v4, p5, 0x2

    add-int/2addr v4, p3

    int-to-float v4, v4

    invoke-virtual {p7, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1830
    :cond_0
    if-eqz v1, :cond_2

    .line 1831
    int-to-float v2, p2

    int-to-float v3, p3

    invoke-virtual {p7, v1, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1835
    :goto_1
    if-eqz p6, :cond_1

    .line 1836
    invoke-virtual {p7}, Landroid/graphics/Canvas;->restore()V

    .line 1839
    .end local v1    # "scalableBitmap":Landroid/graphics/Bitmap;
    :cond_1
    return-void

    .line 1820
    .restart local v1    # "scalableBitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v0

    .line 1821
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "CanvasDraw"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1833
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    int-to-float v2, p2

    int-to-float v3, p3

    invoke-virtual {p7, p1, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method private drawFooter(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V
    .locals 10
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "sd"    # Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/CanvasElementPart;",
            ">;",
            "Landroid/graphics/Canvas;",
            "Landroid/content/Context;",
            "Lcom/samsung/thumbnail/customview/hslf/SlideDimention;",
            ")V"
        }
    .end annotation

    .prologue
    .line 931
    .local p1, "footerArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 932
    .local v7, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    iget v2, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->footerHeight:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/word/Page;->setFooterStartPoint(F)V

    .line 934
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->backgroundColorStatus:Z

    if-eqz v0, :cond_0

    .line 935
    invoke-virtual {p4}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v2

    iget-object v0, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v0

    float-to-double v8, v0

    sub-double/2addr v2, v8

    double-to-int v0, v2

    iput v0, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 939
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 940
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .line 941
    .local v6, "cnPart":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    instance-of v0, v6, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v0, :cond_0

    move-object v1, v6

    .line 942
    check-cast v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 943
    .local v1, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 945
    invoke-virtual {p4}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v2

    iget-object v0, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v0

    float-to-int v0, v0

    int-to-double v8, v0

    sub-double/2addr v2, v8

    iget-object v0, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v0

    float-to-int v0, v0

    int-to-double v8, v0

    sub-double/2addr v2, v8

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v0

    float-to-double v8, v0

    sub-double/2addr v2, v8

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v0

    float-to-double v8, v0

    sub-double/2addr v2, v8

    double-to-int v4, v2

    .line 949
    .local v4, "paraWidth":I
    invoke-virtual {p4}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v2

    iget-object v0, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v0

    float-to-int v0, v0

    int-to-double v8, v0

    sub-double/2addr v2, v8

    iget-object v0, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v0

    float-to-int v0, v0

    int-to-double v8, v0

    sub-double/2addr v2, v8

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getFirstLineMargin()F

    move-result v0

    float-to-double v8, v0

    sub-double/2addr v2, v8

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v0

    float-to-double v8, v0

    sub-double/2addr v2, v8

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v0

    float-to-double v8, v0

    sub-double/2addr v2, v8

    double-to-int v5, v2

    .local v5, "firstLineWidth":I
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    .line 954
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawCurrentPara(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;II)I

    goto :goto_0

    .line 958
    .end local v1    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v4    # "paraWidth":I
    .end local v5    # "firstLineWidth":I
    .end local v6    # "cnPart":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    :cond_1
    return-void
.end method

.method private drawPageBorder(Lcom/samsung/thumbnail/office/word/Page;Landroid/graphics/Canvas;)V
    .locals 18
    .param p1, "page"    # Lcom/samsung/thumbnail/office/word/Page;
    .param p2, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1353
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getPgBorDisplay()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getPgBorDisplay()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_18

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getPgBorDisplay()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "notFirstPage"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_18

    .line 1358
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getTopBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 1359
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getTopBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v12

    .line 1360
    .local v12, "topBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1361
    .local v7, "p":Landroid/graphics/Paint;
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1364
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_19

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "auto"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 1368
    :cond_1
    const/high16 v2, -0x1000000

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1381
    :cond_2
    :goto_0
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPointsToPoints(I)F

    move-result v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1383
    new-instance v2, Landroid/graphics/DashPathEffect;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v3

    invoke-virtual {v12, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getPathEffect(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)[F

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1387
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v3, 0x40800000    # 4.0f

    div-float v3, v2, v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v2, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v5, v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v2, v6

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1392
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_3

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_3

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_3

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_3

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_3

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_3

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_4

    .line 1400
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x40a00000    # 5.0f

    add-float/2addr v3, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    const/high16 v4, 0x40a00000    # 5.0f

    add-float/2addr v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x40a00000    # 5.0f

    sub-float v5, v2, v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    const/high16 v6, 0x40a00000    # 5.0f

    add-float/2addr v6, v2

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1412
    :cond_4
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_5

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_5

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_5

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_6

    .line 1417
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x40a00000    # 5.0f

    add-float/2addr v3, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    const/high16 v4, 0x40a00000    # 5.0f

    add-float/2addr v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x40a00000    # 5.0f

    sub-float v5, v2, v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    const/high16 v6, 0x40a00000    # 5.0f

    add-float/2addr v6, v2

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1429
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v3, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    const/high16 v4, 0x41200000    # 10.0f

    add-float/2addr v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x41200000    # 10.0f

    sub-float v5, v2, v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    const/high16 v6, 0x41200000    # 10.0f

    add-float/2addr v6, v2

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1443
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v12    # "topBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getBottomBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 1444
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getBottomBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v8

    .line 1445
    .local v8, "bottomBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1446
    .restart local v7    # "p":Landroid/graphics/Paint;
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 1449
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1a

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "auto"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1453
    :cond_7
    const/high16 v2, -0x1000000

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1467
    :cond_8
    :goto_1
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPointsToPoints(I)F

    move-result v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1469
    new-instance v2, Landroid/graphics/DashPathEffect;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v3

    invoke-virtual {v8, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getPathEffect(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)[F

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1472
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v3, 0x40800000    # 4.0f

    div-float v3, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v14, v2

    sub-double/2addr v4, v14

    double-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v6, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1483
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_9

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_9

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_9

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_9

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_9

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_9

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_a

    .line 1491
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x40a00000    # 5.0f

    add-float/2addr v3, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v14, v2

    sub-double/2addr v4, v14

    const-wide/high16 v14, 0x4014000000000000L    # 5.0

    sub-double/2addr v4, v14

    double-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4014000000000000L    # 5.0

    sub-double v14, v14, v16

    double-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4014000000000000L    # 5.0

    sub-double v14, v14, v16

    double-to-float v6, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1505
    :cond_a
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_b

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_b

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_b

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_c

    .line 1510
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x40a00000    # 5.0f

    add-float/2addr v3, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v14, v2

    sub-double/2addr v4, v14

    const-wide/high16 v14, 0x4014000000000000L    # 5.0

    sub-double/2addr v4, v14

    double-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4014000000000000L    # 5.0

    sub-double v14, v14, v16

    double-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4014000000000000L    # 5.0

    sub-double v14, v14, v16

    double-to-float v6, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1524
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v3, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v14, v2

    sub-double/2addr v4, v14

    const-wide/high16 v14, 0x4024000000000000L    # 10.0

    sub-double/2addr v4, v14

    double-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4024000000000000L    # 10.0

    sub-double v14, v14, v16

    double-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4024000000000000L    # 10.0

    sub-double v14, v14, v16

    double-to-float v6, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1540
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v8    # "bottomBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v2

    if-eqz v2, :cond_12

    .line 1541
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v10

    .line 1542
    .local v10, "leftBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1543
    .restart local v7    # "p":Landroid/graphics/Paint;
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    if-eqz v2, :cond_e

    .line 1546
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_d

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1b

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "auto"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 1550
    :cond_d
    const/high16 v2, -0x1000000

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1563
    :cond_e
    :goto_2
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPointsToPoints(I)F

    move-result v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1565
    new-instance v2, Landroid/graphics/DashPathEffect;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getPathEffect(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)[F

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1567
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v3, 0x40800000    # 4.0f

    div-float v3, v2, v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v2, v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float v5, v2, v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v6, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1573
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_f

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_f

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_f

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_f

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_f

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_f

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_10

    .line 1581
    :cond_f
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x40a00000    # 5.0f

    add-float/2addr v3, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    const/high16 v4, 0x40a00000    # 5.0f

    add-float/2addr v4, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    const/high16 v5, 0x40a00000    # 5.0f

    add-float/2addr v5, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4014000000000000L    # 5.0

    sub-double v14, v14, v16

    double-to-float v6, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1592
    :cond_10
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_11

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_11

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_11

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_12

    .line 1597
    :cond_11
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x40a00000    # 5.0f

    add-float/2addr v3, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    const/high16 v4, 0x40a00000    # 5.0f

    add-float/2addr v4, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    const/high16 v5, 0x40a00000    # 5.0f

    add-float/2addr v5, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4014000000000000L    # 5.0

    sub-double v14, v14, v16

    double-to-float v6, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1608
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v3, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    const/high16 v4, 0x41200000    # 10.0f

    add-float/2addr v4, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    const/high16 v5, 0x41200000    # 10.0f

    add-float/2addr v5, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4024000000000000L    # 10.0

    sub-double v14, v14, v16

    double-to-float v6, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1621
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v10    # "leftBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v2

    if-eqz v2, :cond_18

    .line 1622
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v11

    .line 1623
    .local v11, "rightBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1624
    .restart local v7    # "p":Landroid/graphics/Paint;
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    if-eqz v2, :cond_14

    .line 1627
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_13

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1c

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "auto"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 1631
    :cond_13
    const/high16 v2, -0x1000000

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1644
    :cond_14
    :goto_3
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPointsToPoints(I)F

    move-result v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1646
    new-instance v2, Landroid/graphics/DashPathEffect;

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v3

    invoke-virtual {v11, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getPathEffect(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)[F

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1648
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v4

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v3, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v2, v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v6, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1658
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_15

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_15

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_15

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_15

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_15

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_15

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_16

    .line 1666
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v4

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    sub-double/2addr v2, v4

    double-to-float v3, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    const/high16 v4, 0x40a00000    # 5.0f

    add-float/2addr v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4014000000000000L    # 5.0

    sub-double v14, v14, v16

    double-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4014000000000000L    # 5.0

    sub-double v14, v14, v16

    double-to-float v6, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1681
    :cond_16
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_17

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_17

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_17

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_18

    .line 1686
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v4

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    sub-double/2addr v2, v4

    double-to-float v3, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    const/high16 v4, 0x40a00000    # 5.0f

    add-float/2addr v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4014000000000000L    # 5.0

    sub-double v14, v14, v16

    double-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4014000000000000L    # 5.0

    sub-double v14, v14, v16

    double-to-float v6, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1701
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v4

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    sub-double/2addr v2, v4

    double-to-float v3, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    const/high16 v4, 0x41200000    # 10.0f

    add-float/2addr v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4024000000000000L    # 10.0

    sub-double v14, v14, v16

    double-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    const-wide/high16 v16, 0x4024000000000000L    # 10.0

    sub-double v14, v14, v16

    double-to-float v6, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1717
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v11    # "rightBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_18
    return-void

    .line 1371
    .restart local v7    # "p":Landroid/graphics/Paint;
    .restart local v12    # "topBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_19
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1372
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->hexToRGB(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v9

    .line 1374
    .local v9, "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_0

    .line 1456
    .end local v9    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v12    # "topBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .restart local v8    # "bottomBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_1a
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 1457
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->hexToRGB(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v9

    .line 1460
    .restart local v9    # "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_1

    .line 1553
    .end local v8    # "bottomBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .end local v9    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v10    # "leftBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_1b
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_e

    .line 1554
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->hexToRGB(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v9

    .line 1556
    .restart local v9    # "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_2

    .line 1634
    .end local v9    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v10    # "leftBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .restart local v11    # "rightBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_1c
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_14

    .line 1635
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->hexToRGB(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v9

    .line 1637
    .restart local v9    # "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_3
.end method

.method private drawParaBorder(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/hslf/DocParagraph;II)V
    .locals 18
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paragraph"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .param p3, "top"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 977
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTopBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 978
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTopBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v12

    .line 979
    .local v12, "topBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 980
    .local v7, "p":Landroid/graphics/Paint;
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 983
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_18

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "auto"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 986
    :cond_0
    const/high16 v2, -0x1000000

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 999
    :cond_1
    :goto_0
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPointsToPoints(I)F

    move-result v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1001
    new-instance v2, Landroid/graphics/DashPathEffect;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v3

    invoke-virtual {v12, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getPathEffect(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)[F

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1003
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x40a00000    # 5.0f

    sub-float v3, v2, v3

    add-int/lit8 v2, p3, -0x5

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x40a00000    # 5.0f

    add-float/2addr v5, v2

    add-int/lit8 v2, p3, -0x5

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1013
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_2

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_2

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_2

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_2

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_2

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_2

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_3

    .line 1022
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    sub-float v3, v2, v3

    add-int/lit8 v2, p3, -0xa

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x41200000    # 10.0f

    add-float/2addr v5, v2

    add-int/lit8 v2, p3, -0xa

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1035
    :cond_3
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_4

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_4

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_4

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_5

    .line 1040
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    sub-float v3, v2, v3

    add-int/lit8 v2, p3, -0xa

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x41200000    # 10.0f

    add-float/2addr v5, v2

    add-int/lit8 v2, p3, -0xa

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1053
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x41700000    # 15.0f

    sub-float v3, v2, v3

    add-int/lit8 v2, p3, -0xf

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x41700000    # 15.0f

    add-float/2addr v5, v2

    add-int/lit8 v2, p3, -0xf

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1068
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v12    # "topBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_5
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 1069
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v10

    .line 1070
    .local v10, "leftBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1073
    .restart local v7    # "p":Landroid/graphics/Paint;
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 1074
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_19

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "auto"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 1077
    :cond_6
    const/high16 v2, -0x1000000

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1090
    :cond_7
    :goto_1
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPointsToPoints(I)F

    move-result v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1092
    new-instance v2, Landroid/graphics/DashPathEffect;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getPathEffect(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)[F

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1094
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x40a00000    # 5.0f

    sub-float v3, v2, v3

    add-int/lit8 v2, p3, -0x5

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v5

    add-float/2addr v2, v5

    const/high16 v5, 0x40a00000    # 5.0f

    sub-float v5, v2, v5

    add-int/lit8 v2, p4, 0x5

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1102
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_8

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_8

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_8

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_8

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_8

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_8

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_9

    .line 1110
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    sub-float v3, v2, v3

    add-int/lit8 v2, p3, -0xa

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v5

    add-float/2addr v2, v5

    const/high16 v5, 0x41200000    # 10.0f

    sub-float v5, v2, v5

    add-int/lit8 v2, p4, 0xa

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1121
    :cond_9
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_a

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_a

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_a

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_b

    .line 1126
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    sub-float v3, v2, v3

    add-int/lit8 v2, p3, -0xa

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v5

    add-float/2addr v2, v5

    const/high16 v5, 0x41200000    # 10.0f

    sub-float v5, v2, v5

    add-int/lit8 v2, p4, 0xa

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1137
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x41700000    # 15.0f

    sub-float v3, v2, v3

    add-int/lit8 v2, p3, -0xf

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v5

    add-float/2addr v2, v5

    const/high16 v5, 0x41700000    # 15.0f

    sub-float v5, v2, v5

    add-int/lit8 v2, p4, 0xf

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1150
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v10    # "leftBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_b
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v2

    if-eqz v2, :cond_11

    .line 1151
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v11

    .line 1152
    .local v11, "rightBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1155
    .restart local v7    # "p":Landroid/graphics/Paint;
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 1156
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1a

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "auto"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1159
    :cond_c
    const/high16 v2, -0x1000000

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1172
    :cond_d
    :goto_2
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPointsToPoints(I)F

    move-result v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1174
    new-instance v2, Landroid/graphics/DashPathEffect;

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v3

    invoke-virtual {v11, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getPathEffect(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)[F

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1177
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    const/high16 v3, 0x40a00000    # 5.0f

    add-float/2addr v3, v2

    add-int/lit8 v2, p3, -0x5

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x40a00000    # 5.0f

    add-float/2addr v5, v2

    add-int/lit8 v2, p4, 0x5

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1189
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_e

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_e

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_e

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_e

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_e

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_e

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_f

    .line 1197
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v3, v2

    add-int/lit8 v2, p3, -0xa

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x41200000    # 10.0f

    add-float/2addr v5, v2

    add-int/lit8 v2, p4, 0xa

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1212
    :cond_f
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_10

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_10

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_10

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_11

    .line 1217
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v3, v2

    add-int/lit8 v2, p3, -0xa

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x41200000    # 10.0f

    add-float/2addr v5, v2

    add-int/lit8 v2, p4, 0xa

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1232
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    const/high16 v3, 0x41700000    # 15.0f

    add-float/2addr v3, v2

    add-int/lit8 v2, p3, -0xf

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x41700000    # 15.0f

    add-float/2addr v5, v2

    add-int/lit8 v2, p4, 0xf

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1249
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v11    # "rightBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_11
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBottomBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v2

    if-eqz v2, :cond_17

    .line 1250
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBottomBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v8

    .line 1251
    .local v8, "bottomBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1254
    .restart local v7    # "p":Landroid/graphics/Paint;
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    if-eqz v2, :cond_13

    .line 1255
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_12

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1b

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "auto"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 1258
    :cond_12
    const/high16 v2, -0x1000000

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1271
    :cond_13
    :goto_3
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPointsToPoints(I)F

    move-result v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1273
    new-instance v2, Landroid/graphics/DashPathEffect;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v3

    invoke-virtual {v8, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getPathEffect(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)[F

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1276
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x40a00000    # 5.0f

    sub-float v3, v2, v3

    add-int/lit8 v2, p4, 0x5

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x40a00000    # 5.0f

    add-float/2addr v5, v2

    add-int/lit8 v2, p4, 0x5

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1286
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_14

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_14

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_14

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_14

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_14

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_14

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_15

    .line 1294
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    sub-float v3, v2, v3

    add-int/lit8 v2, p4, 0xa

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x41200000    # 10.0f

    add-float/2addr v5, v2

    add-int/lit8 v2, p4, 0xa

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1307
    :cond_15
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->TRIPLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_16

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_SMALL_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_16

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_MEDIUM_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-eq v2, v3, :cond_16

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN_THICK_THIN_LARGE_GAP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v2, v3, :cond_17

    .line 1312
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    sub-float v3, v2, v3

    add-int/lit8 v2, p4, 0xa

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x41200000    # 10.0f

    add-float/2addr v5, v2

    add-int/lit8 v2, p4, 0xa

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x41700000    # 15.0f

    sub-float v3, v2, v3

    add-int/lit8 v2, p4, 0xf

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v5

    add-float/2addr v2, v5

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    const/high16 v5, 0x41700000    # 15.0f

    add-float/2addr v5, v2

    add-int/lit8 v2, p4, 0xf

    int-to-float v6, v2

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1338
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v8    # "bottomBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_17
    return-void

    .line 989
    .restart local v7    # "p":Landroid/graphics/Paint;
    .restart local v12    # "topBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_18
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 990
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->hexToRGB(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v9

    .line 992
    .local v9, "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_0

    .line 1080
    .end local v9    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v12    # "topBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .restart local v10    # "leftBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_19
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 1081
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->hexToRGB(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v9

    .line 1083
    .restart local v9    # "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_1

    .line 1162
    .end local v9    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v10    # "leftBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .restart local v11    # "rightBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_1a
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 1163
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->hexToRGB(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v9

    .line 1165
    .restart local v9    # "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_2

    .line 1261
    .end local v9    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v11    # "rightBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .restart local v8    # "bottomBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_1b
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_13

    .line 1262
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->hexToRGB(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v9

    .line 1264
    .restart local v9    # "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual {v9}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_3
.end method

.method private handleTablesDraw(Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/tables/CanvasTable;FF)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "canEleCreater"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "tableInst"    # Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    .param p5, "x"    # F
    .param p6, "y"    # F

    .prologue
    .line 125
    invoke-virtual {p4, p5}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableX(F)V

    .line 126
    invoke-virtual {p4, p6}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableY(F)V

    .line 128
    invoke-virtual {p4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->isHeightCalculated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    invoke-virtual {p4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->calculateHeight()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mTableHeight:I

    .line 132
    :cond_0
    if-nez p2, :cond_1

    .line 134
    invoke-virtual {p4, p3}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->paint(Landroid/graphics/Canvas;)V

    .line 139
    :goto_0
    return-void

    .line 137
    :cond_1
    invoke-virtual {p4, p3, p1, p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->paint(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    goto :goto_0
.end method

.method private writeWaterMark(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/word/ShapesController;)V
    .locals 18
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "shapeCont"    # Lcom/samsung/thumbnail/customview/word/ShapesController;

    .prologue
    .line 1843
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 1845
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v7

    .line 1846
    .local v7, "leftMargin":F
    const/4 v13, 0x0

    cmpg-float v13, v7, v13

    if-gez v13, :cond_7

    .line 1847
    sget v13, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    int-to-float v13, v13

    sub-float v7, v13, v7

    .line 1852
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v11

    .line 1853
    .local v11, "topMargin":F
    const/4 v13, 0x0

    cmpg-float v13, v11, v13

    if-gez v13, :cond_8

    .line 1854
    sget v13, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    int-to-float v13, v13

    sub-float v11, v13, v11

    .line 1859
    :goto_1
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v5

    .line 1864
    .local v5, "docType":Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;
    const/4 v4, 0x0

    .line 1865
    .local v4, "decMargin":Z
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v14

    double-to-int v13, v14

    int-to-float v13, v13

    cmpl-float v13, v11, v13

    if-lez v13, :cond_0

    if-eqz v5, :cond_0

    sget-object v13, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne v5, v13, :cond_0

    .line 1868
    const/4 v4, 0x1

    .line 1871
    :cond_0
    const/4 v13, 0x0

    cmpl-float v13, v11, v13

    if-eqz v13, :cond_1

    if-eqz v4, :cond_2

    .line 1872
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v14

    double-to-int v13, v14

    shr-int/lit8 v13, v13, 0x1

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v14

    float-to-int v14, v14

    shr-int/lit8 v14, v14, 0x1

    sub-int/2addr v13, v14

    int-to-float v11, v13

    .line 1875
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setTopMargin(F)V

    .line 1879
    :cond_2
    const/4 v13, 0x0

    cmpl-float v13, v7, v13

    if-eqz v13, :cond_3

    if-eqz v4, :cond_4

    .line 1880
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v14

    double-to-int v13, v14

    shr-int/lit8 v13, v13, 0x1

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v14

    float-to-int v14, v14

    shr-int/lit8 v14, v14, 0x1

    sub-int/2addr v13, v14

    int-to-float v7, v13

    .line 1883
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setLeftMargin(F)V

    .line 1886
    :cond_4
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v12

    .line 1887
    .local v12, "width":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v6

    .line 1889
    .local v6, "height":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getRotationAngle()J

    move-result-wide v14

    long-to-float v13, v14

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v14

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    add-float/2addr v14, v7

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v15

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    add-float/2addr v15, v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14, v15}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1892
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    .line 1893
    .local v8, "paintFill":Landroid/graphics/Paint;
    sget-object v13, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1895
    sget-object v13, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1896
    const/high16 v13, 0x40000000    # 2.0f

    div-float v13, v6, v13

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1897
    new-instance v10, Landroid/graphics/Rect;

    float-to-int v13, v7

    float-to-int v14, v11

    add-float v15, v12, v7

    float-to-int v15, v15

    add-float v16, v6, v11

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-direct {v10, v13, v14, v15, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1900
    .local v10, "tb":Landroid/graphics/Rect;
    const-string/jumbo v9, ""

    .line 1902
    .local v9, "shapeText":Ljava/lang/String;
    if-eqz v5, :cond_5

    sget-object v13, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne v5, v13, :cond_5

    .line 1903
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillColor()Ljava/lang/String;

    move-result-object v2

    .line 1905
    .local v2, "color":Ljava/lang/String;
    const-string/jumbo v13, "silver"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 1906
    const v13, -0x777778

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1907
    const/16 v13, 0x4d

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1945
    :goto_2
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStringText()Ljava/lang/String;

    move-result-object v9

    .line 1946
    const/4 v13, 0x0

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v8, v9, v13, v14, v10}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1950
    .end local v2    # "color":Ljava/lang/String;
    :cond_5
    if-eqz v5, :cond_6

    sget-object v13, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne v5, v13, :cond_6

    .line 1951
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getShapeColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    .line 1952
    .local v3, "color1":Lorg/apache/poi/java/awt/Color;
    if-eqz v3, :cond_13

    .line 1953
    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v13

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1954
    const/16 v13, 0x4d

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1961
    :goto_3
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getShapeText()Ljava/lang/String;

    move-result-object v9

    .line 1962
    const/4 v13, 0x0

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v8, v9, v13, v14, v10}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1965
    .end local v3    # "color1":Lorg/apache/poi/java/awt/Color;
    :cond_6
    float-to-int v13, v7

    int-to-float v13, v13

    const/high16 v14, 0x40000000    # 2.0f

    div-float v14, v12, v14

    add-float/2addr v13, v14

    float-to-int v14, v11

    int-to-float v14, v14

    const/high16 v15, 0x40000000    # 2.0f

    div-float v15, v6, v15

    add-float/2addr v14, v15

    iget v15, v10, Landroid/graphics/Rect;->top:I

    invoke-static {v15}, Ljava/lang/Math;->abs(I)I

    move-result v15

    int-to-float v15, v15

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    add-float/2addr v14, v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v13, v14, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1968
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 1969
    return-void

    .line 1849
    .end local v4    # "decMargin":Z
    .end local v5    # "docType":Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;
    .end local v6    # "height":F
    .end local v8    # "paintFill":Landroid/graphics/Paint;
    .end local v9    # "shapeText":Ljava/lang/String;
    .end local v10    # "tb":Landroid/graphics/Rect;
    .end local v11    # "topMargin":F
    .end local v12    # "width":F
    :cond_7
    sget v13, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    int-to-float v13, v13

    add-float/2addr v7, v13

    goto/16 :goto_0

    .line 1856
    .restart local v11    # "topMargin":F
    :cond_8
    sget v13, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    int-to-float v13, v13

    add-float/2addr v11, v13

    goto/16 :goto_1

    .line 1908
    .restart local v2    # "color":Ljava/lang/String;
    .restart local v4    # "decMargin":Z
    .restart local v5    # "docType":Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;
    .restart local v6    # "height":F
    .restart local v8    # "paintFill":Landroid/graphics/Paint;
    .restart local v9    # "shapeText":Ljava/lang/String;
    .restart local v10    # "tb":Landroid/graphics/Rect;
    .restart local v12    # "width":F
    :cond_9
    const-string/jumbo v13, "red"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 1909
    const/high16 v13, -0x10000

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1910
    const/16 v13, 0x4d

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_2

    .line 1911
    :cond_a
    const-string/jumbo v13, "black"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 1912
    const/high16 v13, -0x1000000

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1913
    const/16 v13, 0x4d

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_2

    .line 1914
    :cond_b
    const-string/jumbo v13, "blue"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_c

    .line 1915
    const v13, -0xffff01

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1916
    const/16 v13, 0x4d

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_2

    .line 1917
    :cond_c
    const-string/jumbo v13, "cyan"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_d

    .line 1918
    const v13, -0xff0001

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1919
    const/16 v13, 0x4d

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_2

    .line 1920
    :cond_d
    const-string/jumbo v13, "green"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_e

    .line 1921
    const v13, -0xff0100

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1922
    const/16 v13, 0x4d

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_2

    .line 1923
    :cond_e
    const-string/jumbo v13, "purple"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_f

    .line 1925
    const/16 v13, 0x4d

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_2

    .line 1926
    :cond_f
    const-string/jumbo v13, "yellow"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_10

    .line 1927
    const/16 v13, -0x100

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1928
    const/16 v13, 0x4d

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_2

    .line 1929
    :cond_10
    const-string/jumbo v13, "white"

    invoke-virtual {v2, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_11

    .line 1930
    const/4 v13, -0x1

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1931
    const/16 v13, 0x4d

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_2

    .line 1933
    :cond_11
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v13

    const/4 v14, 0x7

    if-lt v13, v14, :cond_12

    .line 1934
    const/16 v13, 0x4d

    const/4 v14, 0x1

    const/4 v15, 0x3

    invoke-virtual {v2, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    const/16 v15, 0x10

    invoke-static {v14, v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v14

    const/4 v15, 0x3

    const/16 v16, 0x5

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x10

    invoke-static/range {v15 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v15

    const/16 v16, 0x5

    const/16 v17, 0x7

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x10

    invoke-static/range {v16 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v8, v13, v14, v15, v0}, Landroid/graphics/Paint;->setARGB(IIII)V

    goto/16 :goto_2

    .line 1940
    :cond_12
    const v13, -0x777778

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1941
    const/16 v13, 0x4d

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_2

    .line 1957
    .end local v2    # "color":Ljava/lang/String;
    .restart local v3    # "color1":Lorg/apache/poi/java/awt/Color;
    :cond_13
    const v13, -0x333334

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1958
    const/16 v13, 0x4d

    invoke-virtual {v8, v13}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_3
.end method


# virtual methods
.method public drawCurrentPara(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;II)I
    .locals 41
    .param p1, "paragraph"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "paraWidth"    # I
    .param p5, "firstLineWidth"    # I

    .prologue
    .line 1974
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-boolean v4, v4, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    const/4 v5, 0x1

    if-eq v4, v5, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    if-nez v4, :cond_2

    .line 1975
    :cond_0
    const/16 v34, 0x0

    .line 2207
    :cond_1
    :goto_0
    return v34

    .line 1977
    :cond_2
    const/16 v34, 0x0

    .line 1978
    .local v34, "paraHeight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v32, v0

    .line 1980
    .local v32, "innitialYval":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getContents()Ljava/util/ArrayList;

    move-result-object v33

    .line 1981
    .local v33, "paraContents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    new-instance v38, Ljava/util/ArrayList;

    invoke-direct/range {v38 .. v38}, Ljava/util/ArrayList;-><init>()V

    .line 1982
    .local v38, "shapesBelowText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ShapesController;>;"
    new-instance v37, Ljava/util/ArrayList;

    invoke-direct/range {v37 .. v37}, Ljava/util/ArrayList;-><init>()V

    .line 1983
    .local v37, "shapesAboveText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ShapesController;>;"
    new-instance v40, Ljava/util/ArrayList;

    invoke-direct/range {v40 .. v40}, Ljava/util/ArrayList;-><init>()V

    .line 1984
    .local v40, "waterMarkImages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/BitmapContents;>;"
    new-instance v31, Ljava/util/ArrayList;

    invoke-direct/range {v31 .. v31}, Ljava/util/ArrayList;-><init>()V

    .line 1985
    .local v31, "imagesBelowText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/BitmapContents;>;"
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 1987
    .local v30, "imagesAboveText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/BitmapContents;>;"
    const/16 v29, 0x0

    .local v29, "i":I
    :goto_1
    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v29

    if-ge v0, v4, :cond_9

    .line 1988
    move-object/from16 v0, v33

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .line 1990
    .local v28, "canElePart":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    move-object/from16 v0, v28

    instance-of v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;

    if-eqz v4, :cond_5

    move-object/from16 v8, v28

    .line 1991
    check-cast v8, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .line 1992
    .local v8, "shapeCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getZorder()J

    move-result-wide v4

    const-wide/16 v10, 0x0

    cmp-long v4, v4, v10

    if-eqz v4, :cond_3

    .line 1993
    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getZorder()J

    move-result-wide v4

    const-wide/16 v10, 0x0

    cmp-long v4, v4, v10

    if-gez v4, :cond_4

    .line 1994
    move-object/from16 v0, v38

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1987
    .end local v8    # "shapeCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_3
    :goto_2
    add-int/lit8 v29, v29, 0x1

    goto :goto_1

    .line 1996
    .restart local v8    # "shapeCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_4
    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getZorder()J

    move-result-wide v4

    const-wide/16 v10, 0x0

    cmp-long v4, v4, v10

    if-lez v4, :cond_3

    .line 1997
    move-object/from16 v0, v37

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2000
    .end local v8    # "shapeCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_5
    move-object/from16 v0, v28

    instance-of v4, v0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    if-eqz v4, :cond_3

    move-object/from16 v27, v28

    .line 2001
    check-cast v27, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .line 2002
    .local v27, "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isHeaderImage()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isWaterMarkImage()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2004
    move-object/from16 v0, v40

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2005
    :cond_6
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isFooterImage()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getFooterName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "footer"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isWaterMarkImage()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2008
    move-object/from16 v0, v40

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2009
    :cond_7
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isInLine()Z

    move-result v4

    if-nez v4, :cond_3

    .line 2010
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isBehindDoc()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2011
    move-object/from16 v0, v31

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2013
    :cond_8
    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2018
    .end local v27    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v28    # "canElePart":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    :cond_9
    invoke-static/range {v40 .. v40}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 2020
    const/16 v29, 0x0

    :goto_3
    invoke-virtual/range {v40 .. v40}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v29

    if-ge v0, v4, :cond_d

    .line 2021
    move-object/from16 v0, v40

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .line 2022
    .local v25, "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v4

    if-eqz v4, :cond_c

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 2025
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v4

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v10

    double-to-float v4, v4

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v5

    int-to-float v5, v5

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v5, v10

    sub-float/2addr v4, v5

    float-to-int v6, v4

    .line 2030
    .local v6, "xPosition":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v4

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v10

    double-to-float v4, v4

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v5

    int-to-float v5, v5

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v5, v10

    sub-float/2addr v4, v5

    float-to-int v7, v4

    .line 2034
    .local v7, "yPosition":I
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v4

    if-eqz v4, :cond_a

    .line 2035
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v6

    .line 2036
    :cond_a
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getTopCoOrdinate()I

    move-result v4

    if-eqz v4, :cond_b

    .line 2037
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getTopCoOrdinate()I

    move-result v7

    .line 2039
    :cond_b
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v8

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v9

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v10

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v11

    const/4 v13, 0x1

    move-object/from16 v4, p0

    move-object/from16 v12, p2

    invoke-direct/range {v4 .. v13}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIIIILjava/lang/String;Landroid/graphics/Canvas;Z)V

    .line 2020
    .end local v6    # "xPosition":I
    .end local v7    # "yPosition":I
    :cond_c
    add-int/lit8 v29, v29, 0x1

    goto/16 :goto_3

    .line 2046
    .end local v25    # "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    :cond_d
    new-instance v4, Lcom/samsung/thumbnail/customview/draw/CanvasDraw$1;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw$1;-><init>(Lcom/samsung/thumbnail/customview/draw/CanvasDraw;)V

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2055
    const/16 v29, 0x0

    :goto_4
    invoke-virtual/range {v38 .. v38}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v29

    if-ge v0, v4, :cond_16

    .line 2056
    move-object/from16 v0, v38

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .line 2057
    .restart local v8    # "shapeCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    const-string/jumbo v36, ""

    .line 2059
    .local v36, "shapeId":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v4

    if-eqz v4, :cond_e

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne v4, v5, :cond_e

    .line 2061
    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getId()Ljava/lang/String;

    move-result-object v36

    .line 2063
    :cond_e
    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v4

    if-eqz v4, :cond_f

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne v4, v5, :cond_f

    .line 2065
    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/word/WordShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v36

    .line 2067
    :cond_f
    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->isHeader()Z

    move-result v4

    if-nez v4, :cond_10

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->isFooter()Z

    move-result v4

    if-eqz v4, :cond_11

    :cond_10
    const-string/jumbo v4, "PowerPlusWaterMark"

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_12

    :cond_11
    const-string/jumbo v4, "TextPlainText"

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 2070
    :cond_12
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v8}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->writeWaterMark(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/word/ShapesController;)V

    .line 2055
    :goto_5
    add-int/lit8 v29, v29, 0x1

    goto :goto_4

    .line 2072
    :cond_13
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-eq v4, v5, :cond_14

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v4, v5, :cond_15

    .line 2074
    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v4

    float-to-int v11, v4

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move/from16 v12, v32

    invoke-virtual/range {v8 .. v14}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawShape(Landroid/graphics/Canvas;Landroid/content/Context;IIZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    goto :goto_5

    .line 2078
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v4

    float-to-int v11, v4

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move/from16 v12, v32

    invoke-virtual/range {v8 .. v14}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawShape(Landroid/graphics/Canvas;Landroid/content/Context;IIZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    goto :goto_5

    .line 2084
    .end local v8    # "shapeCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .end local v36    # "shapeId":Ljava/lang/String;
    :cond_16
    new-instance v4, Lcom/samsung/thumbnail/customview/draw/CanvasDraw$2;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw$2;-><init>(Lcom/samsung/thumbnail/customview/draw/CanvasDraw;)V

    move-object/from16 v0, v31

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2094
    const/16 v29, 0x0

    :goto_6
    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v29

    if-ge v0, v4, :cond_1a

    .line 2095
    move-object/from16 v0, v31

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .line 2096
    .restart local v25    # "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v35

    .line 2097
    .local v35, "paraType":Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isWaterMarkImage()Z

    move-result v4

    if-nez v4, :cond_18

    .line 2098
    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-object/from16 v0, v35

    if-eq v0, v4, :cond_17

    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-object/from16 v0, v35

    if-ne v0, v4, :cond_19

    .line 2099
    :cond_17
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v11

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getTopCoOrdinate()I

    move-result v4

    add-int v12, v32, v4

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v13

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v14

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v15

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v16

    const/16 v18, 0x1

    move-object/from16 v9, p0

    move-object/from16 v17, p2

    invoke-direct/range {v9 .. v18}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIIIILjava/lang/String;Landroid/graphics/Canvas;Z)V

    .line 2094
    :cond_18
    :goto_7
    add-int/lit8 v29, v29, 0x1

    goto :goto_6

    .line 2105
    :cond_19
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v11

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getTopCoOrdinate()I

    move-result v4

    add-int v12, v32, v4

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v13

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v14

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v15

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v16

    const/16 v18, 0x0

    move-object/from16 v9, p0

    move-object/from16 v17, p2

    invoke-direct/range {v9 .. v18}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIIIILjava/lang/String;Landroid/graphics/Canvas;Z)V

    goto :goto_7

    .line 2113
    .end local v25    # "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v35    # "paraType":Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;
    :cond_1a
    move/from16 v0, v32

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 2115
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    int-to-float v4, v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTopSpacing()F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 2116
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    .line 2117
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getX()I

    move-result v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v5

    float-to-int v5, v5

    add-int/2addr v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 2118
    new-instance v9, Lcom/samsung/thumbnail/customview/word/LineBreaker;

    invoke-direct {v9}, Lcom/samsung/thumbnail/customview/word/LineBreaker;-><init>()V

    .line 2119
    .local v9, "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    move-object/from16 v10, p1

    move/from16 v14, p4

    move/from16 v15, p5

    invoke-virtual/range {v9 .. v15}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I

    .line 2122
    new-instance v14, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v4

    float-to-int v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/word/Page;->getTopMargin()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v10

    float-to-int v10, v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/word/Page;->getBottomMargin()F

    move-result v11

    float-to-int v11, v11

    invoke-direct {v14, v4, v5, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2126
    .local v14, "rect":Landroid/graphics/Rect;
    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v19, v0

    move-object/from16 v10, p1

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    move/from16 v15, p4

    move/from16 v16, p5

    move-object/from16 v17, v9

    invoke-virtual/range {v10 .. v19}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILcom/samsung/thumbnail/customview/word/LineBreaker;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 2130
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getY()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawParaBorder(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/hslf/DocParagraph;II)V

    .line 2132
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getY()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 2133
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    int-to-float v4, v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBottomSpacing()F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 2135
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    sub-int v34, v4, v32

    .line 2137
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v26, v0

    .line 2138
    .local v26, "beforeYval":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-boolean v0, v4, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    move/from16 v39, v0

    .line 2140
    .local v39, "stopParsingState":Z
    new-instance v4, Lcom/samsung/thumbnail/customview/draw/CanvasDraw$3;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw$3;-><init>(Lcom/samsung/thumbnail/customview/draw/CanvasDraw;)V

    move-object/from16 v0, v37

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2149
    const/16 v29, 0x0

    :goto_8
    invoke-virtual/range {v37 .. v37}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v29

    if-ge v0, v4, :cond_1e

    .line 2150
    move-object/from16 v0, v37

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .line 2151
    .restart local v8    # "shapeCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-eq v4, v5, :cond_1b

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v4, v5, :cond_1c

    .line 2153
    :cond_1b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v4

    float-to-int v0, v4

    move/from16 v18, v0

    const/16 v20, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v21, v0

    move-object v15, v8

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    move/from16 v19, v32

    invoke-virtual/range {v15 .. v21}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawShape(Landroid/graphics/Canvas;Landroid/content/Context;IIZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 2149
    :goto_9
    add-int/lit8 v29, v29, 0x1

    goto :goto_8

    .line 2157
    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/word/Page;->isCoverPage()Z

    move-result v4

    if-nez v4, :cond_1d

    .line 2158
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v4

    float-to-int v0, v4

    move/from16 v18, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v21, v0

    move-object v15, v8

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    move/from16 v19, v32

    invoke-virtual/range {v15 .. v21}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawShape(Landroid/graphics/Canvas;Landroid/content/Context;IIZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    goto :goto_9

    .line 2162
    :cond_1d
    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v4

    float-to-int v0, v4

    move/from16 v18, v0

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v4

    float-to-int v0, v4

    move/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v21, v0

    move-object v15, v8

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    invoke-virtual/range {v15 .. v21}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawShape(Landroid/graphics/Canvas;Landroid/content/Context;IIZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    goto :goto_9

    .line 2170
    .end local v8    # "shapeCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_1e
    new-instance v4, Lcom/samsung/thumbnail/customview/draw/CanvasDraw$4;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw$4;-><init>(Lcom/samsung/thumbnail/customview/draw/CanvasDraw;)V

    move-object/from16 v0, v30

    invoke-static {v0, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2180
    const/16 v29, 0x0

    :goto_a
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v29

    if-ge v0, v4, :cond_22

    .line 2181
    move-object/from16 v0, v30

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .line 2182
    .restart local v25    # "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v35

    .line 2183
    .restart local v35    # "paraType":Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isWaterMarkImage()Z

    move-result v4

    if-nez v4, :cond_20

    .line 2184
    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-object/from16 v0, v35

    if-eq v0, v4, :cond_1f

    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-object/from16 v0, v35

    if-ne v0, v4, :cond_21

    .line 2185
    :cond_1f
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v16

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v17

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getTopCoOrdinate()I

    move-result v4

    add-int v18, v32, v4

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v19

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v20

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v21

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v22

    const/16 v24, 0x1

    move-object/from16 v15, p0

    move-object/from16 v23, p2

    invoke-direct/range {v15 .. v24}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIIIILjava/lang/String;Landroid/graphics/Canvas;Z)V

    .line 2180
    :cond_20
    :goto_b
    add-int/lit8 v29, v29, 0x1

    goto :goto_a

    .line 2191
    :cond_21
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v16

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v17

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getTopCoOrdinate()I

    move-result v4

    add-int v18, v32, v4

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v19

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v20

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v21

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v22

    const/16 v24, 0x0

    move-object/from16 v15, p0

    move-object/from16 v23, p2

    invoke-direct/range {v15 .. v24}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIIIILjava/lang/String;Landroid/graphics/Canvas;Z)V

    goto :goto_b

    .line 2199
    .end local v25    # "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v35    # "paraType":Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;
    :cond_22
    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 2200
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move/from16 v0, v39

    iput-boolean v0, v4, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    .line 2202
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v4, v5, :cond_23

    .line 2203
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mHeaderRefval:I

    goto/16 :goto_0

    .line 2204
    :cond_23
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v4, v5, :cond_1

    .line 2205
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mFooterRefVal:I

    goto/16 :goto_0
.end method

.method public handleDraw(Landroid/graphics/Canvas;Landroid/content/Context;)Z
    .locals 65
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 149
    const/16 v54, 0x1

    .line 151
    .local v54, "isDrawn":Z
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    if-nez v6, :cond_0

    .line 152
    const/16 v54, 0x0

    move/from16 v55, v54

    .line 926
    .end local v54    # "isDrawn":Z
    .local v55, "isDrawn":I
    :goto_0
    return v55

    .line 156
    .end local v55    # "isDrawn":I
    .restart local v54    # "isDrawn":Z
    :cond_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getPageList()Ljava/util/ArrayList;

    move-result-object v56

    .line 159
    .local v56, "pageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/word/Page;>;"
    if-eqz v56, :cond_28

    invoke-virtual/range {v56 .. v56}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_28

    .line 161
    const/4 v6, 0x0

    move-object/from16 v0, v56

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/word/Page;

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    .line 163
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    if-nez v6, :cond_1

    .line 164
    const/16 v54, 0x0

    move/from16 v55, v54

    .line 165
    .restart local v55    # "isDrawn":I
    goto :goto_0

    .line 168
    .end local v55    # "isDrawn":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getElementList()Ljava/util/ArrayList;

    move-result-object v48

    .line 170
    .local v48, "elementList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    if-nez v48, :cond_2

    .line 171
    const/16 v54, 0x0

    move/from16 v55, v54

    .line 172
    .restart local v55    # "isDrawn":I
    goto :goto_0

    .line 174
    .end local v55    # "isDrawn":I
    :cond_2
    const/16 v44, 0x0

    .line 175
    .local v44, "currentPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual/range {v48 .. v48}, Ljava/util/ArrayList;->size()I

    move-result v49

    .line 177
    .local v49, "elementSize":I
    if-lez v49, :cond_3

    .line 179
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->isContentDrawn:Z

    .line 185
    :cond_3
    const/4 v6, 0x1

    move/from16 v0, v49

    if-lt v0, v6, :cond_4

    const/4 v6, 0x0

    move-object/from16 v0, v48

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/customview/CanvasElementPart;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/CanvasElementPart;->getDrawnStatus()Z

    move-result v6

    if-nez v6, :cond_4

    .line 188
    const/4 v6, -0x1

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 189
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    .line 191
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mHeaderRefval:I

    .line 192
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mFooterRefVal:I

    .line 193
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentParagraph:I

    .line 194
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousParagraph:I

    .line 195
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgHei:I

    .line 196
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPrevShapeHeight:F

    .line 197
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->headerDrawStarted:Z

    .line 198
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->footerDrawStarted:Z

    .line 199
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->normalDrawStarted:Z

    .line 202
    :cond_4
    new-instance v51, Ljava/util/ArrayList;

    invoke-direct/range {v51 .. v51}, Ljava/util/ArrayList;-><init>()V

    .line 205
    .local v51, "footerElements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v6

    if-eqz v6, :cond_8

    const/4 v6, 0x1

    move/from16 v0, v49

    if-lt v0, v6, :cond_8

    .line 211
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->backgroundColorStatus:Z

    if-nez v6, :cond_8

    .line 212
    const/16 v52, 0x0

    .local v52, "i":I
    :goto_1
    move/from16 v0, v52

    move/from16 v1, v49

    if-ge v0, v1, :cond_8

    .line 214
    move-object/from16 v0, v48

    move/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v47

    check-cast v47, Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .line 215
    .local v47, "element":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    move-object/from16 v0, v47

    instance-of v6, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    if-eqz v6, :cond_5

    move-object/from16 v62, v47

    .line 216
    check-cast v62, Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    .line 220
    .local v62, "table":Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getTableType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->NORMAL:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v6, v8, :cond_5

    .line 221
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getBackgroundColor()I

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 223
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->backgroundColorStatus:Z

    .line 227
    .end local v62    # "table":Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    :cond_5
    move-object/from16 v0, v47

    instance-of v6, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v6, :cond_6

    move-object/from16 v7, v47

    .line 228
    check-cast v7, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 232
    .local v7, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->NORMAL:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v6, v8, :cond_7

    .line 233
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getBackgroundColor()I

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 235
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->backgroundColorStatus:Z

    .line 212
    .end local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    :cond_6
    :goto_2
    add-int/lit8 v52, v52, 0x1

    goto :goto_1

    .line 236
    .restart local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    :cond_7
    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v6, v8, :cond_6

    .line 237
    move-object/from16 v0, v51

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 245
    .end local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v47    # "element":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .end local v52    # "i":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    if-eqz v6, :cond_9

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->backgroundColorStatus:Z

    if-eqz v6, :cond_9

    .line 246
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v6, v1}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawPageBorder(Lcom/samsung/thumbnail/office/word/Page;Landroid/graphics/Canvas;)V

    .line 250
    :cond_9
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-boolean v6, v6, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    if-eqz v6, :cond_a

    move/from16 v55, v54

    .line 251
    .restart local v55    # "isDrawn":I
    goto/16 :goto_0

    .line 257
    .end local v55    # "isDrawn":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v61

    .line 259
    .local v61, "slideDimention":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    const/16 v52, 0x0

    .restart local v52    # "i":I
    :goto_3
    move/from16 v0, v52

    move/from16 v1, v49

    if-ge v0, v1, :cond_13

    .line 261
    move-object/from16 v0, v48

    move/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v47

    check-cast v47, Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .line 263
    .restart local v47    # "element":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    invoke-virtual/range {v47 .. v47}, Lcom/samsung/thumbnail/customview/CanvasElementPart;->getDrawnStatus()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 259
    :cond_b
    :goto_4
    add-int/lit8 v52, v52, 0x1

    goto :goto_3

    .line 267
    :cond_c
    invoke-virtual/range {v47 .. v47}, Lcom/samsung/thumbnail/customview/CanvasElementPart;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    if-eqz v6, :cond_d

    invoke-virtual/range {v47 .. v47}, Lcom/samsung/thumbnail/customview/CanvasElementPart;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->TXT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v6, v8}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 270
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->backgroundColorStatus:Z

    .line 273
    :cond_d
    move-object/from16 v0, v47

    instance-of v6, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    if-eqz v6, :cond_1e

    .line 275
    move-object/from16 v0, v47

    check-cast v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v62, v0

    .line 277
    .restart local v62    # "table":Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getTableType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v6, v8, :cond_15

    .line 281
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->backgroundColorStatus:Z

    if-eqz v6, :cond_e

    .line 282
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->headerDrawStarted:Z

    if-nez v6, :cond_14

    .line 283
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v6

    float-to-int v6, v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 287
    :goto_5
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->headerDrawStarted:Z

    .line 315
    :cond_e
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->backgroundColorStatus:Z

    if-eqz v6, :cond_12

    .line 316
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v6

    float-to-int v6, v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    .line 317
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v58, v0

    .line 319
    .local v58, "prevVal":I
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getXPos()Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    if-eqz v6, :cond_10

    .line 321
    :try_start_1
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getXPos()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    invoke-static {v6}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(F)F

    move-result v6

    float-to-int v6, v6

    move-object/from16 v0, v61

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/util/ConcurrentModificationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 331
    :goto_7
    :try_start_2
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getXPos()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v8, "center"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 332
    invoke-virtual/range {v61 .. v61}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v8

    double-to-int v6, v8

    shr-int/lit8 v6, v6, 0x1

    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->calculateWidth()F

    move-result v8

    float-to-int v8, v8

    shr-int/lit8 v8, v8, 0x1

    sub-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    .line 337
    :cond_f
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getXPos()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v8, "right"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 338
    invoke-virtual/range {v61 .. v61}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v8

    double-to-int v6, v8

    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->calculateWidth()F

    move-result v8

    float-to-int v8, v8

    sub-int/2addr v6, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v8

    float-to-int v8, v8

    sub-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    .line 345
    :cond_10
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getYPos()Ljava/lang/String;
    :try_end_2
    .catch Ljava/util/ConcurrentModificationException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v6

    if-eqz v6, :cond_11

    .line 347
    :try_start_3
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getYPos()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    int-to-float v8, v8

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(F)F

    move-result v8

    float-to-double v8, v8

    move-object/from16 v0, v61

    invoke-virtual {v0, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v8

    add-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/util/ConcurrentModificationException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 358
    :goto_8
    :try_start_4
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getYPos()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v8, "bottom"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 359
    invoke-virtual/range {v61 .. v61}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, v62

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v6}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->calculateHeightForWord(Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)F

    move-result v6

    float-to-double v12, v6

    sub-double/2addr v8, v12

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getBottomMargin()F

    move-result v6

    float-to-double v12, v6

    sub-double/2addr v8, v12

    double-to-int v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 378
    :cond_11
    :goto_9
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    int-to-float v6, v6

    move-object/from16 v0, v62

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableX(F)V

    .line 379
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    int-to-float v6, v6

    move-object/from16 v0, v62

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableY(F)V

    .line 380
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, v62

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2, v6}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->paint(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 381
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getTableHeight()F

    move-result v41

    .line 383
    .local v41, "calculatedHeight":F
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getYPos()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1c

    .line 384
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    int-to-float v6, v6

    add-float v6, v6, v41

    float-to-int v6, v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 388
    :goto_a
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getTableType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v6, v8, :cond_1d

    .line 389
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mHeaderRefval:I

    .line 468
    .end local v41    # "calculatedHeight":F
    .end local v58    # "prevVal":I
    .end local v62    # "table":Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    :cond_12
    :goto_b
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->backgroundColorStatus:Z

    if-eqz v6, :cond_b

    .line 470
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getElementList()Ljava/util/ArrayList;

    move-result-object v6

    move/from16 v0, v52

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/customview/CanvasElementPart;

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lcom/samsung/thumbnail/customview/CanvasElementPart;->setDrawnStatus(Z)V
    :try_end_4
    .catch Ljava/util/ConcurrentModificationException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_4

    .line 474
    .end local v47    # "element":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .end local v52    # "i":I
    .end local v61    # "slideDimention":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    :catch_0
    move-exception v46

    .line 475
    .local v46, "e":Ljava/util/ConcurrentModificationException;
    const-string/jumbo v6, "CanvasDraw"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "ConcurrentModificationException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v46 .. v46}, Ljava/util/ConcurrentModificationException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    .end local v46    # "e":Ljava/util/ConcurrentModificationException;
    :cond_13
    :goto_c
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    int-to-double v8, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getBottomMargin()F

    move-result v6

    float-to-double v0, v6

    move-wide/from16 v18, v0

    sub-double v12, v12, v18

    cmpl-double v6, v8, v12

    if-ltz v6, :cond_3c

    if-eqz v44, :cond_3c

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-eq v6, v8, :cond_3c

    .line 484
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    const/4 v8, 0x1

    iput-boolean v8, v6, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    move/from16 v55, v54

    .line 485
    .restart local v55    # "isDrawn":I
    goto/16 :goto_0

    .line 286
    .end local v55    # "isDrawn":I
    .restart local v47    # "element":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .restart local v52    # "i":I
    .restart local v61    # "slideDimention":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .restart local v62    # "table":Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    :cond_14
    :try_start_5
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mHeaderRefval:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I
    :try_end_5
    .catch Ljava/util/ConcurrentModificationException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_5

    .line 477
    .end local v47    # "element":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .end local v52    # "i":I
    .end local v61    # "slideDimention":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .end local v62    # "table":Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    :catch_1
    move-exception v46

    .line 478
    .local v46, "e":Ljava/lang/Exception;
    const-string/jumbo v6, "CanvasDraw"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v46 .. v46}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 289
    .end local v46    # "e":Ljava/lang/Exception;
    .restart local v47    # "element":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .restart local v52    # "i":I
    .restart local v61    # "slideDimention":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .restart local v62    # "table":Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    :cond_15
    :try_start_6
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getTableType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v6, v8, :cond_17

    .line 293
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->backgroundColorStatus:Z

    if-eqz v6, :cond_e

    .line 294
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->footerDrawStarted:Z

    if-nez v6, :cond_16

    .line 295
    invoke-virtual/range {v61 .. v61}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v6

    float-to-double v12, v6

    sub-double/2addr v8, v12

    double-to-int v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 300
    :goto_d
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->footerDrawStarted:Z

    goto/16 :goto_6

    .line 299
    :cond_16
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mFooterRefVal:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto :goto_d

    .line 302
    :cond_17
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getTableType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->NORMAL:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v6, v8, :cond_e

    .line 304
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->normalDrawStarted:Z

    if-nez v6, :cond_18

    .line 305
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mHeaderRefval:I

    int-to-float v6, v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/word/Page;->getTopMargin()F

    move-result v8

    cmpg-float v6, v6, v8

    if-gez v6, :cond_19

    .line 306
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getTopMargin()F

    move-result v6

    float-to-int v6, v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 310
    :cond_18
    :goto_e
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->normalDrawStarted:Z

    goto/16 :goto_6

    .line 308
    :cond_19
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mHeaderRefval:I

    add-int/lit8 v6, v6, 0xa

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto :goto_e

    .line 326
    .restart local v58    # "prevVal":I
    :catch_2
    move-exception v46

    .line 327
    .local v46, "e":Ljava/lang/NumberFormatException;
    const-string/jumbo v6, "CanvasDraw"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "NumberFormatException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v46 .. v46}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 353
    .end local v46    # "e":Ljava/lang/NumberFormatException;
    :catch_3
    move-exception v46

    .line 354
    .restart local v46    # "e":Ljava/lang/NumberFormatException;
    const-string/jumbo v6, "CanvasDraw"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "NumberFormatException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v46 .. v46}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    .line 364
    .end local v46    # "e":Ljava/lang/NumberFormatException;
    :cond_1a
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getYPos()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v8, "center"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 366
    invoke-virtual/range {v61 .. v61}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v8

    double-to-int v6, v8

    shr-int/lit8 v6, v6, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, v62

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v8}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->calculateHeightForWord(Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)F

    move-result v8

    float-to-int v8, v8

    shr-int/lit8 v8, v8, 0x1

    sub-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto/16 :goto_9

    .line 370
    :cond_1b
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getYPos()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v8, "top"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 372
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getTopMargin()F

    move-result v6

    float-to-int v6, v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto/16 :goto_9

    .line 386
    .restart local v41    # "calculatedHeight":F
    :cond_1c
    move/from16 v0, v58

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto/16 :goto_a

    .line 390
    :cond_1d
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getTableType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v6, v8, :cond_12

    .line 391
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mFooterRefVal:I

    goto/16 :goto_b

    .line 394
    .end local v41    # "calculatedHeight":F
    .end local v58    # "prevVal":I
    .end local v62    # "table":Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    :cond_1e
    move-object/from16 v0, v47

    instance-of v6, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v6, :cond_12

    .line 395
    move-object/from16 v0, v47

    check-cast v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-object v7, v0

    .line 396
    .restart local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    sget-object v6, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->DOCUMENT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParentType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;)V

    .line 397
    move-object/from16 v44, v7

    .line 399
    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v6, v8, :cond_22

    .line 403
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->backgroundColorStatus:Z

    if-eqz v6, :cond_1f

    .line 404
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->headerDrawStarted:Z

    if-nez v6, :cond_21

    .line 405
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getHeaderStartPoint()F

    move-result v6

    float-to-int v6, v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 409
    :goto_f
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->headerDrawStarted:Z

    .line 438
    :cond_1f
    :goto_10
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->backgroundColorStatus:Z

    if-eqz v6, :cond_20

    .line 439
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 441
    invoke-virtual/range {v61 .. v61}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v6

    float-to-int v6, v6

    int-to-double v12, v6

    sub-double/2addr v8, v12

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v6

    float-to-int v6, v6

    int-to-double v12, v6

    sub-double/2addr v8, v12

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v6

    float-to-double v12, v6

    sub-double/2addr v8, v12

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v6

    float-to-double v12, v6

    sub-double/2addr v8, v12

    double-to-int v10, v8

    .line 446
    .local v10, "paraWidth":I
    invoke-virtual/range {v61 .. v61}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v6

    float-to-int v6, v6

    int-to-double v12, v6

    sub-double/2addr v8, v12

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v6

    float-to-int v6, v6

    int-to-double v12, v6

    sub-double/2addr v8, v12

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getFirstLineMargin()F

    move-result v6

    float-to-double v12, v6

    sub-double/2addr v8, v12

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v6

    float-to-double v12, v6

    sub-double/2addr v8, v12

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v6

    float-to-double v12, v6

    sub-double/2addr v8, v12

    double-to-int v11, v8

    .line 452
    .local v11, "firstLineWidth":I
    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v6, v8, :cond_27

    .line 455
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->footerHeight:I

    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8}, Landroid/graphics/Canvas;-><init>()V

    move-object/from16 v6, p0

    move-object/from16 v9, p2

    invoke-virtual/range {v6 .. v11}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawCurrentPara(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;II)I

    move-result v6

    add-int/2addr v6, v12

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->footerHeight:I

    .line 463
    .end local v10    # "paraWidth":I
    .end local v11    # "firstLineWidth":I
    :cond_20
    :goto_11
    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->isPgBrBefStatus()Z

    move-result v6

    if-eqz v6, :cond_12

    .line 464
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    const/4 v8, 0x1

    iput-boolean v8, v6, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    goto/16 :goto_b

    .line 408
    :cond_21
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mHeaderRefval:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto/16 :goto_f

    .line 411
    :cond_22
    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v6, v8, :cond_24

    .line 414
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->backgroundColorStatus:Z

    if-eqz v6, :cond_1f

    .line 415
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->footerDrawStarted:Z

    if-nez v6, :cond_23

    .line 416
    invoke-virtual/range {v61 .. v61}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v6

    float-to-double v12, v6

    sub-double/2addr v8, v12

    double-to-int v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 421
    :goto_12
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->footerDrawStarted:Z

    goto/16 :goto_10

    .line 420
    :cond_23
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mFooterRefVal:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto :goto_12

    .line 423
    :cond_24
    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->NORMAL:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v6, v8, :cond_1f

    .line 425
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->normalDrawStarted:Z

    if-nez v6, :cond_25

    .line 427
    move-object/from16 v0, p0

    move-object/from16 v1, v51

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, v61

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawFooter(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V

    .line 428
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mHeaderRefval:I

    int-to-float v6, v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/word/Page;->getTopMargin()F

    move-result v8

    cmpg-float v6, v6, v8

    if-gez v6, :cond_26

    .line 429
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentPage:Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getTopMargin()F

    move-result v6

    float-to-int v6, v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 434
    :cond_25
    :goto_13
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->normalDrawStarted:Z

    goto/16 :goto_10

    .line 431
    :cond_26
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mHeaderRefval:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto :goto_13

    .restart local v10    # "paraWidth":I
    .restart local v11    # "firstLineWidth":I
    :cond_27
    move-object/from16 v6, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    .line 459
    invoke-virtual/range {v6 .. v11}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawCurrentPara(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;II)I
    :try_end_6
    .catch Ljava/util/ConcurrentModificationException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_11

    .line 488
    .end local v7    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v10    # "paraWidth":I
    .end local v11    # "firstLineWidth":I
    .end local v44    # "currentPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v47    # "element":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .end local v48    # "elementList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    .end local v49    # "elementSize":I
    .end local v51    # "footerElements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    .end local v52    # "i":I
    .end local v61    # "slideDimention":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    :cond_28
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getCanvasElementsList()Ljava/util/ArrayList;

    move-result-object v50

    .line 493
    .local v50, "elementsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    const/4 v6, -0x1

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 494
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    .line 496
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mHeaderRefval:I

    .line 497
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mFooterRefVal:I

    .line 498
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentParagraph:I

    .line 499
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousParagraph:I

    .line 500
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgHei:I

    .line 501
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPrevShapeHeight:F

    .line 503
    if-nez v50, :cond_29

    .line 504
    const/16 v54, 0x0

    move/from16 v55, v54

    .line 505
    .restart local v55    # "isDrawn":I
    goto/16 :goto_0

    .line 508
    .end local v55    # "isDrawn":I
    :cond_29
    invoke-virtual/range {v50 .. v50}, Ljava/util/ArrayList;->size()I

    move-result v49

    .line 510
    .restart local v49    # "elementSize":I
    if-lez v49, :cond_2a

    .line 512
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->isContentDrawn:Z

    .line 515
    :cond_2a
    new-instance v63, Ljava/util/ArrayList;

    invoke-direct/range {v63 .. v63}, Ljava/util/ArrayList;-><init>()V

    .line 517
    .local v63, "waterMarkImages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/BitmapContents;>;"
    const/16 v52, 0x0

    .restart local v52    # "i":I
    :goto_14
    move/from16 v0, v52

    move/from16 v1, v49

    if-ge v0, v1, :cond_2c

    .line 518
    move-object/from16 v0, v50

    move/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    if-eqz v6, :cond_2b

    .line 519
    move-object/from16 v0, v50

    move/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .line 520
    .local v39, "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isHeaderImage()Z

    move-result v6

    if-eqz v6, :cond_2b

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeaderName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v8, "header3.xml"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2b

    .line 522
    move-object/from16 v0, v63

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 517
    .end local v39    # "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    :cond_2b
    add-int/lit8 v52, v52, 0x1

    goto :goto_14

    .line 527
    :cond_2c
    invoke-static/range {v63 .. v63}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 529
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    if-gez v6, :cond_2d

    .line 530
    sget v6, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 533
    :cond_2d
    const/16 v64, 0x0

    .local v64, "x1":I
    :goto_15
    move/from16 v0, v64

    move/from16 v1, v49

    if-ge v0, v1, :cond_3c

    .line 535
    move-object/from16 v0, v50

    move/from16 v1, v64

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .line 537
    .local v42, "canvasElem":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    if-nez v64, :cond_31

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/CanvasElementPart;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    if-eqz v6, :cond_31

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/CanvasElementPart;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v6, v8}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_31

    .line 541
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/word/Page;->getBackgroundColor()I

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 543
    invoke-virtual/range {v63 .. v63}, Ljava/util/ArrayList;->size()I

    move-result v53

    .line 544
    .local v53, "imageSize":I
    const/16 v52, 0x0

    :goto_16
    move/from16 v0, v52

    move/from16 v1, v53

    if-ge v0, v1, :cond_31

    .line 545
    move-object/from16 v0, v63

    move/from16 v1, v52

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .line 546
    .restart local v39    # "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    if-eqz v6, :cond_30

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v6, v8}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_30

    .line 550
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v8

    double-to-int v6, v8

    shr-int/lit8 v6, v6, 0x1

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v8

    shr-int/lit8 v8, v8, 0x1

    sub-int v14, v6, v8

    .line 555
    .local v14, "xPosition":I
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v8

    double-to-int v6, v8

    shr-int/lit8 v6, v6, 0x1

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v8

    shr-int/lit8 v8, v8, 0x1

    sub-int v15, v6, v8

    .line 559
    .local v15, "yPosition":I
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v6

    if-eqz v6, :cond_2e

    .line 560
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v14

    .line 561
    :cond_2e
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getTopCoOrdinate()I

    move-result v6

    if-eqz v6, :cond_2f

    .line 562
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getTopCoOrdinate()I

    move-result v15

    .line 564
    :cond_2f
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v16

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v17

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v18

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v19

    const/16 v21, 0x1

    move-object/from16 v12, p0

    move-object/from16 v20, p1

    invoke-direct/range {v12 .. v21}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIIIILjava/lang/String;Landroid/graphics/Canvas;Z)V

    .line 544
    .end local v14    # "xPosition":I
    .end local v15    # "yPosition":I
    :cond_30
    add-int/lit8 v52, v52, 0x1

    goto/16 :goto_16

    .line 573
    .end local v39    # "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v53    # "imageSize":I
    :cond_31
    move-object/from16 v0, v42

    instance-of v6, v0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;

    if-eqz v6, :cond_35

    move-object/from16 v40, v42

    .line 574
    check-cast v40, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;

    .line 575
    .local v40, "bgCont":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getIsBackgroundImage()Z

    move-result v6

    if-eqz v6, :cond_34

    .line 576
    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getBGBitMap()Landroid/graphics/Bitmap;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getBGBitMap()Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getBGBitMap()Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v25, 0x0

    move-object/from16 v16, p0

    move-object/from16 v24, p1

    invoke-direct/range {v16 .. v25}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIIIILjava/lang/String;Landroid/graphics/Canvas;Z)V

    .line 895
    .end local v40    # "bgCont":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    :cond_32
    :goto_17
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/CanvasElementPart;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    if-eqz v6, :cond_4a

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/CanvasElementPart;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-eq v6, v8, :cond_33

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/CanvasElementPart;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne v6, v8, :cond_4a

    .line 898
    :cond_33
    move-object/from16 v0, v42

    instance-of v6, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;

    if-eqz v6, :cond_56

    .line 899
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    int-to-double v8, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v12

    sget v6, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_BOTTOM:I

    int-to-double v0, v6

    move-wide/from16 v18, v0

    sub-double v12, v12, v18

    cmpl-double v6, v8, v12

    if-ltz v6, :cond_4a

    check-cast v42, Lcom/samsung/thumbnail/customview/word/Paragraph;

    .end local v42    # "canvasElem":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/word/Paragraph;->isFooter()Z

    move-result v6

    if-nez v6, :cond_4a

    .line 905
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    const/4 v8, 0x1

    iput-boolean v8, v6, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    move/from16 v55, v54

    .line 906
    .restart local v55    # "isDrawn":I
    goto/16 :goto_0

    .line 581
    .end local v55    # "isDrawn":I
    .restart local v40    # "bgCont":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    .restart local v42    # "canvasElem":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    :cond_34
    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getBGColor()I

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_17

    .line 584
    .end local v40    # "bgCont":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    :cond_35
    move-object/from16 v0, v42

    instance-of v6, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;

    if-eqz v6, :cond_3e

    move-object/from16 v57, v42

    .line 586
    check-cast v57, Lcom/samsung/thumbnail/customview/word/Paragraph;

    .line 588
    .local v57, "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    const/16 v59, 0x0

    .line 589
    .local v59, "r":Landroid/graphics/Rect;
    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    if-eqz v6, :cond_39

    .line 590
    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v6, v8}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_38

    .line 591
    const/4 v6, -0x1

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 592
    new-instance v59, Landroid/graphics/Rect;

    .end local v59    # "r":Landroid/graphics/Rect;
    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginLeft()I

    move-result v6

    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginTop()I

    move-result v8

    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginRight()I

    move-result v9

    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginBottom()I

    move-result v12

    move-object/from16 v0, v59

    invoke-direct {v0, v6, v8, v9, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 605
    .restart local v59    # "r":Landroid/graphics/Rect;
    :goto_18
    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->isHeader()Z

    move-result v6

    if-eqz v6, :cond_3a

    .line 606
    sget v6, Lcom/samsung/thumbnail/customview/word/Param;->HEADER_START_POINT:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 607
    sget v6, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    .line 612
    :cond_36
    :goto_19
    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->isFooter()Z

    move-result v6

    if-eqz v6, :cond_37

    .line 613
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreYValue:I

    .line 614
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v8

    sget v6, Lcom/samsung/thumbnail/customview/word/Param;->FOOTER_START_POINT:I

    int-to-double v12, v6

    sub-double/2addr v8, v12

    double-to-int v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 616
    sget v6, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    .line 618
    :cond_37
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move-object/from16 v0, v57

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setYValue(I)V

    .line 619
    move-object/from16 v0, v57

    move-object/from16 v1, v57

    move-object/from16 v2, p1

    move-object/from16 v3, v59

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/thumbnail/customview/word/Paragraph;->handleDrawTextPara(Lcom/samsung/thumbnail/customview/word/Paragraph;Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 620
    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->isFooter()Z

    move-result v6

    if-eqz v6, :cond_3b

    .line 621
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreYValue:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 628
    :goto_1a
    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginLeft()I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    .line 629
    const/4 v6, 0x0

    move-object/from16 v0, v57

    iput v6, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    .line 630
    const/4 v6, 0x0

    move-object/from16 v0, v57

    iput-boolean v6, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mAppendLine:Z

    goto/16 :goto_17

    .line 596
    :cond_38
    new-instance v59, Landroid/graphics/Rect;

    .end local v59    # "r":Landroid/graphics/Rect;
    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginLeft()I

    move-result v6

    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginTop()I

    move-result v8

    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginRight()I

    move-result v9

    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginBottom()I

    move-result v12

    move-object/from16 v0, v59

    invoke-direct {v0, v6, v8, v9, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v59    # "r":Landroid/graphics/Rect;
    goto/16 :goto_18

    .line 601
    :cond_39
    new-instance v59, Landroid/graphics/Rect;

    .end local v59    # "r":Landroid/graphics/Rect;
    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginLeft()I

    move-result v6

    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginTop()I

    move-result v8

    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginRight()I

    move-result v9

    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginBottom()I

    move-result v12

    move-object/from16 v0, v59

    invoke-direct {v0, v6, v8, v9, v12}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v59    # "r":Landroid/graphics/Rect;
    goto/16 :goto_18

    .line 608
    :cond_3a
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    sget v8, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    if-gt v6, v8, :cond_36

    .line 609
    sget v6, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto/16 :goto_19

    .line 623
    :cond_3b
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-boolean v6, v6, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    if-eqz v6, :cond_3d

    .line 922
    .end local v42    # "canvasElem":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .end local v50    # "elementsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    .end local v52    # "i":I
    .end local v57    # "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    .end local v59    # "r":Landroid/graphics/Rect;
    .end local v63    # "waterMarkImages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/BitmapContents;>;"
    .end local v64    # "x1":I
    :cond_3c
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-boolean v6, v6, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    const/4 v8, 0x1

    if-ne v6, v8, :cond_57

    move/from16 v55, v54

    .line 923
    .restart local v55    # "isDrawn":I
    goto/16 :goto_0

    .line 626
    .end local v55    # "isDrawn":I
    .restart local v42    # "canvasElem":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .restart local v50    # "elementsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    .restart local v52    # "i":I
    .restart local v57    # "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    .restart local v59    # "r":Landroid/graphics/Rect;
    .restart local v63    # "waterMarkImages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/BitmapContents;>;"
    .restart local v64    # "x1":I
    :cond_3d
    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getYValue()I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto :goto_1a

    .line 631
    .end local v57    # "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    .end local v59    # "r":Landroid/graphics/Rect;
    :cond_3e
    move-object/from16 v0, v42

    instance-of v6, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    if-eqz v6, :cond_40

    .line 633
    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/CanvasElementPart;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3f

    move-object/from16 v62, v42

    .line 635
    check-cast v62, Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    .line 636
    .restart local v62    # "table":Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getTableX()F

    move-result v6

    float-to-int v6, v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    .line 637
    invoke-virtual/range {v62 .. v62}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getTableY()F

    move-result v6

    float-to-int v6, v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 638
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v18, v0

    move-object/from16 v20, v42

    check-cast v20, Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    int-to-float v0, v6

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    int-to-float v0, v6

    move/from16 v22, v0

    move-object/from16 v16, p0

    move-object/from16 v17, p2

    move-object/from16 v19, p1

    invoke-direct/range {v16 .. v22}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->handleTablesDraw(Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/tables/CanvasTable;FF)V

    .line 644
    .end local v62    # "table":Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    :goto_1b
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mTableHeight:I

    add-int/lit8 v8, v8, 0x5

    add-int/lit8 v8, v8, 0xc

    add-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto/16 :goto_17

    .line 641
    :cond_3f
    const/16 v18, 0x0

    move-object/from16 v20, v42

    check-cast v20, Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    int-to-float v0, v6

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    int-to-float v0, v6

    move/from16 v22, v0

    move-object/from16 v16, p0

    move-object/from16 v17, p2

    move-object/from16 v19, p1

    invoke-direct/range {v16 .. v22}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->handleTablesDraw(Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/tables/CanvasTable;FF)V

    goto :goto_1b

    .line 646
    :cond_40
    move-object/from16 v0, v42

    instance-of v6, v0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    if-eqz v6, :cond_47

    move-object/from16 v39, v42

    .line 647
    check-cast v39, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .line 649
    .restart local v39    # "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    if-eqz v6, :cond_32

    .line 651
    sget-object v6, Lcom/samsung/thumbnail/customview/draw/CanvasDraw$5;->$SwitchMap$com$samsung$thumbnail$customview$CanvasElementPart$DocumentType:[I

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->ordinal()I

    move-result v8

    aget v6, v6, v8

    packed-switch v6, :pswitch_data_0

    .line 735
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v17

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v18

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getTopCoOrdinate()I

    move-result v19

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v20

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v21

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v22

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v23

    const/16 v25, 0x0

    move-object/from16 v16, p0

    move-object/from16 v24, p1

    invoke-direct/range {v16 .. v25}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIIIILjava/lang/String;Landroid/graphics/Canvas;Z)V

    goto/16 :goto_17

    .line 653
    :pswitch_0
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgHei:I

    .line 654
    const-string/jumbo v6, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "CanvasElementPart Docx : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgHei:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isHeaderImage()Z

    move-result v6

    if-nez v6, :cond_41

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isFooterImage()Z

    move-result v6

    if-eqz v6, :cond_42

    .line 683
    :cond_41
    :goto_1c
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getParaNumber()I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentParagraph:I

    goto/16 :goto_17

    .line 659
    :cond_42
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentParagraph:I

    if-eqz v6, :cond_43

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentParagraph:I

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getParaNumber()I

    move-result v8

    if-eq v6, v8, :cond_44

    .line 661
    :cond_43
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgWid:I

    .line 662
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v17

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v19, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v20

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v21

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v22

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v23

    const/16 v25, 0x0

    move-object/from16 v16, p0

    move-object/from16 v24, p1

    invoke-direct/range {v16 .. v25}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIIIILjava/lang/String;Landroid/graphics/Canvas;Z)V

    .line 668
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v8

    add-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto :goto_1c

    .line 670
    :cond_44
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgHei:I

    add-int/lit8 v8, v8, 0x5

    sub-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 672
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v17

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgWid:I

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v8

    add-int v18, v6, v8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v19, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v20

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v21

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v22

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v23

    const/16 v25, 0x0

    move-object/from16 v16, p0

    move-object/from16 v24, p1

    invoke-direct/range {v16 .. v25}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIIIILjava/lang/String;Landroid/graphics/Canvas;Z)V

    .line 680
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgWid:I

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v8

    add-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgWid:I

    .line 681
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v8

    add-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto/16 :goto_1c

    .line 687
    :pswitch_1
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgHei:I

    .line 689
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentParagraph:I

    if-eqz v6, :cond_45

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentParagraph:I

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getParaNumber()I

    move-result v8

    if-eq v6, v8, :cond_46

    .line 691
    :cond_45
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgWid:I

    .line 692
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v17

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v19, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v20

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v21

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v22

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v23

    const/16 v25, 0x0

    move-object/from16 v16, p0

    move-object/from16 v24, p1

    invoke-direct/range {v16 .. v25}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIIIILjava/lang/String;Landroid/graphics/Canvas;Z)V

    .line 698
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v8

    add-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 712
    :goto_1d
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getParaNumber()I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentParagraph:I

    goto/16 :goto_17

    .line 700
    :cond_46
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgHei:I

    sub-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 701
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v17

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgWid:I

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v8

    add-int v18, v6, v8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v19, v0

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v20

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v21

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v22

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v23

    const/16 v25, 0x0

    move-object/from16 v16, p0

    move-object/from16 v24, p1

    invoke-direct/range {v16 .. v25}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIIIILjava/lang/String;Landroid/graphics/Canvas;Z)V

    .line 709
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgWid:I

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v8

    add-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousImgWid:I

    .line 710
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v8

    add-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto :goto_1d

    .line 717
    :pswitch_2
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v17

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v18

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getTopCoOrdinate()I

    move-result v19

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v20

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v21

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v22

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v23

    const/16 v25, 0x0

    move-object/from16 v16, p0

    move-object/from16 v24, p1

    invoke-direct/range {v16 .. v25}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIIIILjava/lang/String;Landroid/graphics/Canvas;Z)V

    goto/16 :goto_17

    .line 726
    :pswitch_3
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v17

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v18

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getTopCoOrdinate()I

    move-result v19

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v20

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v21

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v22

    move-object/from16 v16, p0

    move-object/from16 v23, p1

    invoke-direct/range {v16 .. v23}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawBitMapXlsx(Landroid/graphics/Bitmap;IIIIILandroid/graphics/Canvas;)V

    goto/16 :goto_17

    .line 744
    .end local v39    # "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    :cond_47
    move-object/from16 v0, v42

    instance-of v6, v0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    if-eqz v6, :cond_4c

    move-object/from16 v43, v42

    .line 745
    check-cast v43, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    .line 746
    .local v43, "chart":Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    new-instance v22, Landroid/graphics/Paint;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Paint;-><init>()V

    .line 748
    .local v22, "p":Landroid/graphics/Paint;
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/CanvasElementPart;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-eq v6, v8, :cond_48

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/CanvasElementPart;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne v6, v8, :cond_49

    .line 749
    :cond_48
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getX()I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    .line 750
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getY()I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 753
    :cond_49
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v6

    const/4 v8, 0x0

    cmpg-float v6, v6, v8

    if-lez v6, :cond_4a

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v6

    const/4 v8, 0x0

    cmpg-float v6, v6, v8

    if-gtz v6, :cond_4b

    .line 533
    .end local v22    # "p":Landroid/graphics/Paint;
    .end local v42    # "canvasElem":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .end local v43    # "chart":Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    :cond_4a
    add-int/lit8 v64, v64, 0x1

    goto/16 :goto_15

    .line 760
    .restart local v22    # "p":Landroid/graphics/Paint;
    .restart local v42    # "canvasElem":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .restart local v43    # "chart":Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    :cond_4b
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartType()I

    move-result v6

    packed-switch v6, :pswitch_data_1

    .line 828
    :goto_1e
    :pswitch_4
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v8

    float-to-int v8, v8

    add-int/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    goto/16 :goto_17

    .line 763
    :pswitch_5
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getColumnBarChart()Lorg/achartengine/chart/AbstractChart;

    move-result-object v16

    .line 764
    .local v16, "abChart":Lorg/achartengine/chart/AbstractChart;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v19, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v6

    float-to-int v0, v6

    move/from16 v20, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v6

    float-to-int v0, v6

    move/from16 v21, v0

    move-object/from16 v17, p1

    invoke-virtual/range {v16 .. v22}, Lorg/achartengine/chart/AbstractChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto :goto_1e

    .line 768
    .end local v16    # "abChart":Lorg/achartengine/chart/AbstractChart;
    :pswitch_6
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getDoughnutChart()Lorg/achartengine/chart/DoughnutChart;

    move-result-object v23

    .line 769
    .local v23, "doughnutChart":Lorg/achartengine/chart/DoughnutChart;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v26, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v6

    float-to-int v0, v6

    move/from16 v27, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v6

    float-to-int v0, v6

    move/from16 v28, v0

    move-object/from16 v24, p1

    move-object/from16 v29, v22

    invoke-virtual/range {v23 .. v29}, Lorg/achartengine/chart/DoughnutChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto :goto_1e

    .line 774
    .end local v23    # "doughnutChart":Lorg/achartengine/chart/DoughnutChart;
    :pswitch_7
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getPieChart()Lorg/achartengine/chart/PieChart;

    move-result-object v24

    .line 775
    .local v24, "pieChart":Lorg/achartengine/chart/PieChart;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v27, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v6

    float-to-int v0, v6

    move/from16 v28, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v6

    float-to-int v0, v6

    move/from16 v29, v0

    move-object/from16 v25, p1

    move-object/from16 v30, v22

    invoke-virtual/range {v24 .. v30}, Lorg/achartengine/chart/PieChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto :goto_1e

    .line 780
    .end local v24    # "pieChart":Lorg/achartengine/chart/PieChart;
    :pswitch_8
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getScatterChart()Lorg/achartengine/chart/XYChart;

    move-result-object v25

    .line 781
    .local v25, "scatterChart":Lorg/achartengine/chart/XYChart;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v28, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v6

    float-to-int v0, v6

    move/from16 v29, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v6

    float-to-int v0, v6

    move/from16 v30, v0

    move-object/from16 v26, p1

    move-object/from16 v31, v22

    invoke-virtual/range {v25 .. v31}, Lorg/achartengine/chart/XYChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto/16 :goto_1e

    .line 786
    .end local v25    # "scatterChart":Lorg/achartengine/chart/XYChart;
    :pswitch_9
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getBubbleChart()Lorg/achartengine/chart/XYChart;

    move-result-object v26

    .line 787
    .local v26, "bubbleChart":Lorg/achartengine/chart/XYChart;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v29, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v6

    float-to-int v0, v6

    move/from16 v30, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v6

    float-to-int v0, v6

    move/from16 v31, v0

    move-object/from16 v27, p1

    move-object/from16 v32, v22

    invoke-virtual/range {v26 .. v32}, Lorg/achartengine/chart/XYChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto/16 :goto_1e

    .line 793
    .end local v26    # "bubbleChart":Lorg/achartengine/chart/XYChart;
    :pswitch_a
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getAreaChart()Lorg/achartengine/chart/TimeChart;

    move-result-object v27

    .line 794
    .local v27, "timeChart":Lorg/achartengine/chart/TimeChart;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v30, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v6

    float-to-int v0, v6

    move/from16 v31, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v6

    float-to-int v0, v6

    move/from16 v32, v0

    move-object/from16 v28, p1

    move-object/from16 v33, v22

    invoke-virtual/range {v27 .. v33}, Lorg/achartengine/chart/TimeChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto/16 :goto_1e

    .line 799
    .end local v27    # "timeChart":Lorg/achartengine/chart/TimeChart;
    :pswitch_b
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getLineChart()Lorg/achartengine/chart/XYChart;

    move-result-object v28

    .line 800
    .local v28, "lineChart":Lorg/achartengine/chart/XYChart;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v31, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v6

    float-to-int v0, v6

    move/from16 v32, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v6

    float-to-int v0, v6

    move/from16 v33, v0

    move-object/from16 v29, p1

    move-object/from16 v34, v22

    invoke-virtual/range {v28 .. v34}, Lorg/achartengine/chart/XYChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto/16 :goto_1e

    .line 805
    .end local v28    # "lineChart":Lorg/achartengine/chart/XYChart;
    :pswitch_c
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getRadarChart()Lorg/achartengine/chart/RadarChart;

    move-result-object v29

    .line 806
    .local v29, "radarChart":Lorg/achartengine/chart/RadarChart;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v32, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v6

    float-to-int v0, v6

    move/from16 v33, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v6

    float-to-int v0, v6

    move/from16 v34, v0

    move-object/from16 v30, p1

    move-object/from16 v35, v22

    invoke-virtual/range {v29 .. v35}, Lorg/achartengine/chart/RadarChart;->drawChart(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto/16 :goto_1e

    .line 811
    .end local v29    # "radarChart":Lorg/achartengine/chart/RadarChart;
    :pswitch_d
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getHorizontalBarChart()Lorg/achartengine/chart/ColumnChart;

    move-result-object v30

    .line 813
    .local v30, "barChart":Lorg/achartengine/chart/ColumnChart;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v33, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v6

    float-to-int v0, v6

    move/from16 v34, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v6

    float-to-int v6, v6

    mul-int/lit8 v35, v6, 0x2

    move-object/from16 v31, p1

    move-object/from16 v36, v22

    invoke-virtual/range {v30 .. v36}, Lorg/achartengine/chart/ColumnChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto/16 :goto_1e

    .line 818
    .end local v30    # "barChart":Lorg/achartengine/chart/ColumnChart;
    :pswitch_e
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getStockChart()Lorg/achartengine/chart/RangeBarChart;

    move-result-object v31

    .line 819
    .local v31, "stockChart":Lorg/achartengine/chart/RangeBarChart;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v34, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v6

    float-to-int v0, v6

    move/from16 v35, v0

    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v6

    float-to-int v0, v6

    move/from16 v36, v0

    move-object/from16 v32, p1

    move-object/from16 v37, v22

    invoke-virtual/range {v31 .. v37}, Lorg/achartengine/chart/RangeBarChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto/16 :goto_1e

    .line 830
    .end local v22    # "p":Landroid/graphics/Paint;
    .end local v31    # "stockChart":Lorg/achartengine/chart/RangeBarChart;
    .end local v43    # "chart":Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    :cond_4c
    move-object/from16 v0, v42

    instance-of v6, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;

    if-eqz v6, :cond_32

    move-object/from16 v32, v42

    .line 831
    check-cast v32, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .line 832
    .local v32, "shapeCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    const-string/jumbo v60, ""

    .line 834
    .local v60, "shapeId":Ljava/lang/String;
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    if-eqz v6, :cond_4d

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne v6, v8, :cond_4d

    .line 836
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getId()Ljava/lang/String;

    move-result-object v60

    .line 838
    :cond_4d
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->isHeader()Z

    move-result v6

    if-nez v6, :cond_4e

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->isFooter()Z

    move-result v6

    if-eqz v6, :cond_51

    .line 841
    :cond_4e
    const-string/jumbo v6, "PowerPlusWaterMark"

    move-object/from16 v0, v60

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_32

    .line 844
    const/16 v6, 0x18

    :try_start_7
    move-object/from16 v0, v60

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v45

    .line 846
    .local v45, "currentWaterMarkId":I
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->maxWaterMarkId:I

    move/from16 v0, v45

    if-ge v6, v0, :cond_50

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getHeaderName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v8, "header2.xml"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_50

    .line 849
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->writeWaterMark(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/word/ShapesController;)V

    .line 854
    :cond_4f
    :goto_1f
    move/from16 v0, v45

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->maxWaterMarkId:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_17

    .line 855
    .end local v45    # "currentWaterMarkId":I
    :catch_4
    move-exception v46

    .line 856
    .local v46, "e":Ljava/lang/Exception;
    const-string/jumbo v6, "CanvasDraw"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v46 .. v46}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_17

    .line 850
    .end local v46    # "e":Ljava/lang/Exception;
    .restart local v45    # "currentWaterMarkId":I
    :cond_50
    :try_start_8
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getFooterName()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v8, "footer"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4f

    .line 852
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->writeWaterMark(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/word/ShapesController;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_1f

    .line 861
    .end local v45    # "currentWaterMarkId":I
    :cond_51
    const-string/jumbo v6, "TextPlainText"

    move-object/from16 v0, v60

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_52

    .line 862
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->writeWaterMark(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/word/ShapesController;)V

    goto/16 :goto_17

    .line 865
    :cond_52
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getParaNumber()I

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentParagraph:I

    .line 866
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousParagraph:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentParagraph:I

    if-ne v6, v8, :cond_53

    .line 867
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    int-to-float v6, v6

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPrevShapeHeight:F

    sub-float/2addr v6, v8

    float-to-int v6, v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 870
    :cond_53
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    sget v8, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    if-ge v6, v8, :cond_54

    .line 872
    sget v6, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 875
    :cond_54
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentParagraph:I

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPreviousParagraph:I

    .line 876
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->x:I

    move/from16 v35, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    move/from16 v36, v0

    const/16 v37, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v38, v0

    move-object/from16 v33, p1

    move-object/from16 v34, p2

    invoke-virtual/range {v32 .. v38}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawShape(Landroid/graphics/Canvas;Landroid/content/Context;IIZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 879
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    if-eqz v6, :cond_32

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v6

    sget-object v8, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-eq v6, v8, :cond_32

    .line 882
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mCurrentParagraph:I

    if-nez v6, :cond_55

    .line 883
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    int-to-float v6, v6

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v8

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v9

    add-float/2addr v8, v9

    add-float/2addr v6, v8

    float-to-int v6, v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 885
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v6

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v8

    add-float/2addr v6, v8

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPrevShapeHeight:F

    goto/16 :goto_17

    .line 888
    :cond_55
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    int-to-float v6, v6

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v8

    add-float/2addr v6, v8

    float-to-int v6, v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    .line 889
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->mPrevShapeHeight:F

    goto/16 :goto_17

    .line 909
    .end local v32    # "shapeCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .end local v60    # "shapeId":Ljava/lang/String;
    :cond_56
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->y:I

    int-to-double v8, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v12

    cmpl-double v6, v8, v12

    if-ltz v6, :cond_4a

    .line 911
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    const/4 v8, 0x1

    iput-boolean v8, v6, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    move/from16 v55, v54

    .line 915
    .restart local v55    # "isDrawn":I
    goto/16 :goto_0

    .line 925
    .end local v42    # "canvasElem":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .end local v50    # "elementsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    .end local v52    # "i":I
    .end local v55    # "isDrawn":I
    .end local v63    # "waterMarkImages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/BitmapContents;>;"
    .end local v64    # "x1":I
    :cond_57
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    const/4 v8, 0x0

    iput-boolean v8, v6, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    move/from16 v55, v54

    .line 926
    .restart local v55    # "isDrawn":I
    goto/16 :goto_0

    .line 651
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 760
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_4
        :pswitch_e
        :pswitch_c
        :pswitch_8
        :pswitch_7
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_9
        :pswitch_d
    .end packed-switch
.end method

.method public isDrawn()Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->isContentDrawn:Z

    return v0
.end method

.method public setCanvas(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 1
    .param p1, "elementCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    if-nez v0, :cond_0

    .line 117
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 119
    :cond_0
    return-void
.end method
