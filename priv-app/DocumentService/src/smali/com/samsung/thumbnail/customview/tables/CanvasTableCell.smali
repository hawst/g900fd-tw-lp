.class public Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
.super Ljava/lang/Object;
.source "CanvasTableCell.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/customview/tables/CanvasTableCell$CellType;
    }
.end annotation


# static fields
.field public static final DEFAULT_XY_POS:I = 0x32


# instance fields
.field protected availableLeft:I

.field protected availableRight:I

.field private bottomBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

.field private cellFillStyle:Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

.field private cellGridSpanValue:I

.field private cellParagraphs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/Paragraph;",
            ">;"
        }
    .end annotation
.end field

.field private cellText:Ljava/lang/String;

.field private cellTextIndent:I

.field private height:F

.field hideCell:Z

.field horizontalAlignment:Ljava/lang/String;

.field private isDrawLeftBorder:Z

.field private isDrawRightBorder:Z

.field private leftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

.field private mDocParaList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;"
        }
    .end annotation
.end field

.field private mIsWrapText:Z

.field private mergedCellKey:Ljava/lang/String;

.field private rightBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

.field private shrinkToFit:Z

.field private singleLineTextWidth:F

.field private textHeight:F

.field private textRotation:S

.field private topBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

.field private txtDirection:Ljava/lang/String;

.field private vMergeValue:Ljava/lang/String;

.field verticalAlignment:Ljava/lang/String;

.field private width:F

.field private x:F

.field private y:F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->singleLineTextWidth:F

    .line 39
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->isDrawLeftBorder:Z

    .line 40
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->isDrawRightBorder:Z

    .line 42
    iput-boolean v3, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->hideCell:Z

    .line 43
    iput-boolean v3, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->mIsWrapText:Z

    .line 45
    iput v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellGridSpanValue:I

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->vMergeValue:Ljava/lang/String;

    .line 65
    new-instance v0, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellFillStyle:Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    .line 73
    iput v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableLeft:I

    .line 74
    iput v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableRight:I

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellParagraphs:Ljava/util/ArrayList;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->mDocParaList:Ljava/util/ArrayList;

    .line 84
    return-void
.end method

.method public constructor <init>(F)V
    .locals 4
    .param p1, "inHeight"    # F

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->singleLineTextWidth:F

    .line 39
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->isDrawLeftBorder:Z

    .line 40
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->isDrawRightBorder:Z

    .line 42
    iput-boolean v3, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->hideCell:Z

    .line 43
    iput-boolean v3, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->mIsWrapText:Z

    .line 45
    iput v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellGridSpanValue:I

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->vMergeValue:Ljava/lang/String;

    .line 65
    new-instance v0, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellFillStyle:Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    .line 73
    iput v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableLeft:I

    .line 74
    iput v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableRight:I

    .line 87
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->height:F

    .line 88
    return-void
.end method


# virtual methods
.method public addDocParaList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 428
    .local p1, "docParaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->mDocParaList:Ljava/util/ArrayList;

    .line 429
    return-void
.end method

.method public calculateTextDimensions()V
    .locals 13

    .prologue
    const/16 v12, 0x32

    const/high16 v11, 0x42480000    # 50.0f

    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    .line 268
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->textHeight:F

    .line 270
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getParagraphList()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 271
    const/4 v8, 0x0

    .line 272
    .local v8, "i":I
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getParagraphList()Ljava/util/ArrayList;

    move-result-object v9

    .line 273
    .local v9, "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 275
    .local v10, "paraSize":I
    const/4 v8, 0x0

    :goto_0
    if-ge v8, v10, :cond_1

    .line 276
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/customview/word/Paragraph;

    .line 278
    .local v0, "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    new-instance v3, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v1

    add-float/2addr v1, v11

    float-to-int v1, v1

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v4

    add-float/2addr v4, v11

    float-to-int v4, v4

    invoke-direct {v3, v12, v12, v1, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 282
    .local v3, "r":Landroid/graphics/Rect;
    iget v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->textHeight:F

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setYValue(I)V

    .line 284
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getWrapContent()Z

    move-result v1

    if-ne v1, v7, :cond_0

    .line 285
    iget v1, v3, Landroid/graphics/Rect;->right:I

    iget v4, v3, Landroid/graphics/Rect;->left:I

    sub-int v5, v1, v4

    move-object v1, v0

    move-object v4, v2

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/thumbnail/customview/word/Paragraph;->paintText(Lcom/samsung/thumbnail/customview/word/Paragraph;Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Rect;IIZ)V

    .line 291
    :goto_1
    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getYValue()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->textHeight:F

    .line 275
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 288
    :cond_0
    const/4 v5, -0x1

    move-object v1, v0

    move-object v4, v2

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/thumbnail/customview/word/Paragraph;->paintText(Lcom/samsung/thumbnail/customview/word/Paragraph;Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Rect;IIZ)V

    goto :goto_1

    .line 295
    .end local v0    # "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    .end local v3    # "r":Landroid/graphics/Rect;
    .end local v8    # "i":I
    .end local v9    # "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    .end local v10    # "paraSize":I
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getParagraphList()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getParagraphList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v7, :cond_2

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getWrapContent()Z

    move-result v1

    if-nez v1, :cond_2

    .line 297
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getParagraphList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/customview/word/Paragraph;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getNonWrapTextWidth()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setSingleLineTextWidth(F)V

    .line 301
    :cond_2
    iget v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->textHeight:F

    iget v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->height:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 302
    iget v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->textHeight:F

    iput v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->height:F

    .line 305
    :cond_3
    return-void
.end method

.method public drawLeftBorder()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->isDrawLeftBorder:Z

    return v0
.end method

.method public drawRightBorder()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->isDrawRightBorder:Z

    return v0
.end method

.method public getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->bottomBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    return-object v0
.end method

.method public getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellFillStyle:Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    return-object v0
.end method

.method public getCellGradSpan()I
    .locals 1

    .prologue
    .line 390
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellGridSpanValue:I

    return v0
.end method

.method public getCellHeight()F
    .locals 1

    .prologue
    .line 226
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->height:F

    return v0
.end method

.method public getCellMergeValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->vMergeValue:Ljava/lang/String;

    return-object v0
.end method

.method public getCellText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellText:Ljava/lang/String;

    return-object v0
.end method

.method public getCellWidth()F
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->width:F

    return v0
.end method

.method public getCellX()F
    .locals 1

    .prologue
    .line 246
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->x:F

    return v0
.end method

.method public getCellY()F
    .locals 1

    .prologue
    .line 254
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->y:F

    return v0
.end method

.method public getDocParaList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 432
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->mDocParaList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDocParagraphList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 214
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->mDocParaList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getHideCell()Z
    .locals 1

    .prologue
    .line 394
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->hideCell:Z

    return v0
.end method

.method public getHorizontalAlignment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->horizontalAlignment:Ljava/lang/String;

    return-object v0
.end method

.method public getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->leftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    return-object v0
.end method

.method public getMergedCellKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->mergedCellKey:Ljava/lang/String;

    return-object v0
.end method

.method public getParagraphList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/Paragraph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellParagraphs:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->rightBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    return-object v0
.end method

.method public getShrinkToFit()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->shrinkToFit:Z

    return v0
.end method

.method public getSingleLineTextWidth()F
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->singleLineTextWidth:F

    return v0
.end method

.method public getTextDirection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->txtDirection:Ljava/lang/String;

    return-object v0
.end method

.method public getTextHeight()F
    .locals 1

    .prologue
    .line 230
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->textHeight:F

    return v0
.end method

.method public getTextIndent()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellTextIndent:I

    return v0
.end method

.method public getTextRotation()S
    .locals 1

    .prologue
    .line 135
    iget-short v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->textRotation:S

    return v0
.end method

.method public getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->topBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    return-object v0
.end method

.method public getVerticalAlignment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->verticalAlignment:Ljava/lang/String;

    return-object v0
.end method

.method public getWrapContent()Z
    .locals 1

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->mIsWrapText:Z

    return v0
.end method

.method public isEmptyText()Z
    .locals 5

    .prologue
    .line 410
    const/4 v3, 0x1

    .line 411
    .local v3, "ret":Z
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellParagraphs:Ljava/util/ArrayList;

    .line 413
    .local v2, "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    if-nez v2, :cond_0

    .line 414
    const/4 v4, 0x1

    .line 424
    :goto_0
    return v4

    .line 417
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/customview/word/Paragraph;

    .line 418
    .local v1, "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->isEmptyText()Z

    move-result v4

    if-nez v4, :cond_1

    .line 419
    const/4 v3, 0x0

    .end local v1    # "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    :cond_2
    move v4, v3

    .line 424
    goto :goto_0
.end method

.method public setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V
    .locals 1
    .param p1, "borderStyle"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .param p2, "borderColor"    # Lorg/apache/poi/java/awt/Color;
    .param p3, "borderWidth"    # F

    .prologue
    .line 345
    new-instance v0, Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/tables/TableBorder;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->bottomBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    .line 346
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->bottomBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderColor(Lorg/apache/poi/java/awt/Color;)V

    .line 347
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->bottomBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)V

    .line 348
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->bottomBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v0, p3}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderWidth(F)V

    .line 349
    return-void
.end method

.method public setBottomBorderToNull()V
    .locals 1

    .prologue
    .line 368
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->bottomBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    .line 369
    return-void
.end method

.method public setCellFillStyle(Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;)V
    .locals 2
    .param p1, "gradientFill"    # Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    .prologue
    .line 377
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellFillStyle:Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->setGradientFill(Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;)V

    .line 378
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellFillStyle:Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->setGradient(Z)V

    .line 379
    return-void
.end method

.method public setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V
    .locals 1
    .param p1, "fillColor"    # Lorg/apache/poi/java/awt/Color;
    .param p2, "gradientFill"    # Z

    .prologue
    .line 372
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellFillStyle:Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->setfillColor(Lorg/apache/poi/java/awt/Color;)V

    .line 373
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellFillStyle:Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->setGradient(Z)V

    .line 374
    return-void
.end method

.method public setCellGradSpan(I)V
    .locals 0
    .param p1, "gradspan"    # I

    .prologue
    .line 386
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellGridSpanValue:I

    .line 387
    return-void
.end method

.method public setCellHeight(F)V
    .locals 0
    .param p1, "cellheight"    # F

    .prologue
    .line 222
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->height:F

    .line 223
    return-void
.end method

.method public setCellMergeValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "mergeVal"    # Ljava/lang/String;

    .prologue
    .line 258
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->vMergeValue:Ljava/lang/String;

    .line 259
    return-void
.end method

.method public setCellText(Ljava/lang/String;)V
    .locals 0
    .param p1, "cellText"    # Ljava/lang/String;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellText:Ljava/lang/String;

    .line 180
    return-void
.end method

.method public setCellWidth(F)V
    .locals 0
    .param p1, "cellwidth"    # F

    .prologue
    .line 234
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->width:F

    .line 235
    return-void
.end method

.method public setCellX(F)V
    .locals 0
    .param p1, "x"    # F

    .prologue
    .line 242
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->x:F

    .line 243
    return-void
.end method

.method public setCellY(F)V
    .locals 0
    .param p1, "y"    # F

    .prologue
    .line 250
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->y:F

    .line 251
    return-void
.end method

.method public setDocParagraphList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 218
    .local p1, "docParaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->mDocParaList:Ljava/util/ArrayList;

    .line 219
    return-void
.end method

.method public setHideCell(Z)V
    .locals 0
    .param p1, "bHide"    # Z

    .prologue
    .line 398
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->hideCell:Z

    .line 399
    return-void
.end method

.method public setHorizontalAlignment(Ljava/lang/String;)V
    .locals 0
    .param p1, "alignment"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->horizontalAlignment:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setIsDrawLeftBorder(Z)V
    .locals 0
    .param p1, "drawLeftBorder"    # Z

    .prologue
    .line 147
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->isDrawLeftBorder:Z

    .line 148
    return-void
.end method

.method public setIsDrawRightBorder(Z)V
    .locals 0
    .param p1, "drawRightBorder"    # Z

    .prologue
    .line 155
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->isDrawRightBorder:Z

    .line 156
    return-void
.end method

.method public setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V
    .locals 1
    .param p1, "borderStyle"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .param p2, "borderColor"    # Lorg/apache/poi/java/awt/Color;
    .param p3, "borderWidth"    # F

    .prologue
    .line 309
    new-instance v0, Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/tables/TableBorder;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->leftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    .line 310
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->leftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderColor(Lorg/apache/poi/java/awt/Color;)V

    .line 311
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->leftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)V

    .line 312
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->leftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v0, p3}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderWidth(F)V

    .line 313
    return-void
.end method

.method public setLeftBorderToNull()V
    .locals 1

    .prologue
    .line 356
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->leftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    .line 357
    return-void
.end method

.method public setMergedCellKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "mergedCellKey"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->mergedCellKey:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public setParagraphList(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/Paragraph;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    if-eqz p1, :cond_3

    .line 191
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/customview/word/Paragraph;

    .line 192
    .local v1, "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    if-eqz v1, :cond_0

    .line 195
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->horizontalAlignment:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 196
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->horizontalAlignment:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setHorizontalAlignment(Ljava/lang/String;)V

    .line 198
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->verticalAlignment:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 199
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->verticalAlignment:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setVerticalAlignment(Ljava/lang/String;)V

    .line 202
    :cond_2
    iget v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellTextIndent:I

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setTextIndent(I)V

    .line 203
    iget-short v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->textRotation:S

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setTextRotation(S)V

    goto :goto_0

    .line 206
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    :cond_3
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellParagraphs:Ljava/util/ArrayList;

    .line 207
    return-void
.end method

.method public setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V
    .locals 1
    .param p1, "borderStyle"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .param p2, "borderColor"    # Lorg/apache/poi/java/awt/Color;
    .param p3, "borderWidth"    # F

    .prologue
    .line 321
    new-instance v0, Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/tables/TableBorder;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->rightBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    .line 322
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->rightBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderColor(Lorg/apache/poi/java/awt/Color;)V

    .line 323
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->rightBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)V

    .line 324
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->rightBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v0, p3}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderWidth(F)V

    .line 325
    return-void
.end method

.method public setRightBorderToNull()V
    .locals 1

    .prologue
    .line 360
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->rightBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    .line 361
    return-void
.end method

.method public setShrinkToFit(Z)V
    .locals 0
    .param p1, "shrinkToFit"    # Z

    .prologue
    .line 139
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->shrinkToFit:Z

    .line 140
    return-void
.end method

.method public setSingleLineTextWidth(F)V
    .locals 0
    .param p1, "nonWrapTextWidth"    # F

    .prologue
    .line 163
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->singleLineTextWidth:F

    .line 164
    return-void
.end method

.method public setTextDirection(Ljava/lang/String;)V
    .locals 0
    .param p1, "txtDirection"    # Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->txtDirection:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public setTextIndent(I)V
    .locals 0
    .param p1, "textIndent"    # I

    .prologue
    .line 123
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->cellTextIndent:I

    .line 124
    return-void
.end method

.method public setTextRotation(S)V
    .locals 0
    .param p1, "textRotation"    # S

    .prologue
    .line 131
    iput-short p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->textRotation:S

    .line 132
    return-void
.end method

.method public setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V
    .locals 1
    .param p1, "borderStyle"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .param p2, "borderColor"    # Lorg/apache/poi/java/awt/Color;
    .param p3, "borderWidth"    # F

    .prologue
    .line 333
    new-instance v0, Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/tables/TableBorder;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->topBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    .line 334
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->topBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderColor(Lorg/apache/poi/java/awt/Color;)V

    .line 335
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->topBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)V

    .line 336
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->topBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v0, p3}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderWidth(F)V

    .line 337
    return-void
.end method

.method public setTopBorderToNull()V
    .locals 1

    .prologue
    .line 364
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->topBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    .line 365
    return-void
.end method

.method public setVerticalAlignment(Ljava/lang/String;)V
    .locals 0
    .param p1, "alignment"    # Ljava/lang/String;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->verticalAlignment:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public setWrapContent(Z)V
    .locals 0
    .param p1, "isWrapText"    # Z

    .prologue
    .line 402
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->mIsWrapText:Z

    .line 403
    return-void
.end method
