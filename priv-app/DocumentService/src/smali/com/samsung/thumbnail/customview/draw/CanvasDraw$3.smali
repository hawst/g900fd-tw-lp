.class Lcom/samsung/thumbnail/customview/draw/CanvasDraw$3;
.super Ljava/lang/Object;
.source "CanvasDraw.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->drawCurrentPara(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/samsung/thumbnail/customview/word/ShapesController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/customview/draw/CanvasDraw;


# direct methods
.method constructor <init>(Lcom/samsung/thumbnail/customview/draw/CanvasDraw;)V
    .locals 0

    .prologue
    .line 2140
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw$3;->this$0:Lcom/samsung/thumbnail/customview/draw/CanvasDraw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/samsung/thumbnail/customview/word/ShapesController;Lcom/samsung/thumbnail/customview/word/ShapesController;)I
    .locals 4
    .param p1, "arg0"    # Lcom/samsung/thumbnail/customview/word/ShapesController;
    .param p2, "arg1"    # Lcom/samsung/thumbnail/customview/word/ShapesController;

    .prologue
    .line 2144
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getZorder()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getZorder()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getZorder()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getZorder()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 2140
    check-cast p1, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw$3;->compare(Lcom/samsung/thumbnail/customview/word/ShapesController;Lcom/samsung/thumbnail/customview/word/ShapesController;)I

    move-result v0

    return v0
.end method
