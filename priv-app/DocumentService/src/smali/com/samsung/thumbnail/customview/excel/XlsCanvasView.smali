.class public Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;
.super Ljava/lang/Object;
.source "XlsCanvasView.java"


# static fields
.field public static final CELLHEIGHT:I = 0x14

.field public static final CELLWIDTH:I = 0x46

.field private static final CHART_DEFAULT_SIZE:I = 0xafc8

.field private static final COLOR_BLACK:Ljava/lang/String; = "#000000"

.field private static final DEFAULT_ADJUST_FONTSIZE:F = 1.0f

.field private static final DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

.field private static final DEFAULT_CELL_HEIGHT:I = 0x14

.field private static final FIRST_CELL_WIDTH:I = 0x19

.field private static final FIRST_COLUMN_HEIGHT:I = 0xf

.field private static final FIRST_COLUMN_WIDTH:I = 0x19

.field private static final MAXIMUMROWTODISPLAYFIRST:I = 0x23

.field private static final NUM_OF_SHAPES_PERSHEET:I = 0x3e8

.field private static final TABLE03_HEIGHT:I = 0x14

.field private static final TABLE03_WIDTH:I = 0x46

.field private static final TAG:Ljava/lang/String; = "XlsCanvasView"

.field private static colmnWdthChartsPicts:Ljava/util/Map; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final formatCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/poi/ss/format/CellFormat;",
            ">;"
        }
    .end annotation
.end field

.field private static final formatPercent:Ljava/lang/String; = "0%"


# instance fields
.field private charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

.field private charts:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/excel/ShapeInformation;",
            ">;"
        }
    .end annotation
.end field

.field containChart:Z

.field private escherClientAnchorList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ddf/EscherClientAnchorRecord;",
            ">;"
        }
    .end annotation
.end field

.field private escherOptRecordList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ddf/EscherRecord;",
            ">;"
        }
    .end annotation
.end field

.field private escherSpRecordList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/poi/ddf/EscherSpRecord;",
            ">;"
        }
    .end annotation
.end field

.field private folderPath:Ljava/io/File;

.field private isGridDisplayed:Z

.field private isHeadingsDisplayed:Z

.field private isThumbnailPreview:Z

.field private mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

.field private mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field private mCanvasRow:Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

.field private mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

.field private mCustomView:Lcom/samsung/thumbnail/customview/word/CustomView;

.field mExcelChartList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;",
            ">;"
        }
    .end annotation
.end field

.field mLeftBorderCellNum:I

.field private mMaxColumninTable:I

.field private mMaxRowinTable:I

.field mNextCellLeftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

.field mNextCellTopBorder:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/tables/TableBorder;",
            ">;"
        }
    .end annotation
.end field

.field mTopBorderCellNum:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mTopBorderRowNum:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

.field private shapes:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/excel/ShapeInformation;",
            ">;"
        }
    .end annotation
.end field

.field sheetIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->formatCache:Ljava/util/Map;

    .line 140
    sget-object v0, Lorg/apache/poi/java/awt/Color;->DARK_GRAY:Lorg/apache/poi/java/awt/Color;

    sput-object v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/customview/word/CustomView;Ljava/io/File;)V
    .locals 3
    .param p1, "customView"    # Lcom/samsung/thumbnail/customview/word/CustomView;
    .param p2, "folderPath"    # Ljava/io/File;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mExcelChartList:Ljava/util/ArrayList;

    .line 109
    iput v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    .line 110
    iput v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxRowinTable:I

    .line 111
    iput v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->sheetIndex:I

    .line 142
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    .line 147
    iput-boolean v2, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isGridDisplayed:Z

    .line 148
    iput-boolean v2, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isHeadingsDisplayed:Z

    .line 155
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCustomView:Lcom/samsung/thumbnail/customview/word/CustomView;

    .line 156
    iput-object p2, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    .line 158
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCustomView:Lcom/samsung/thumbnail/customview/word/CustomView;

    if-nez v0, :cond_0

    .line 159
    iput-boolean v2, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    .line 162
    :cond_0
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->init()V

    .line 163
    return-void
.end method

.method private addBorderToCell(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;II)V
    .locals 26
    .param p1, "wb"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p3, "cellStyle"    # Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    .param p4, "rowNum"    # I
    .param p5, "colNum"    # I

    .prologue
    .line 1724
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBorderLeft()S

    move-result v9

    .line 1725
    .local v9, "leftBorderWidth":S
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBorderRight()S

    move-result v14

    .line 1726
    .local v14, "rightBorderWidth":S
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBorderTop()S

    move-result v17

    .line 1727
    .local v17, "topBorderWidth":S
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBorderBottom()S

    move-result v6

    .line 1728
    .local v6, "bottomBorderWidth":S
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    sget-object v20, Lorg/apache/poi/java/awt/Color;->BLACK:Lorg/apache/poi/java/awt/Color;

    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v20

    move/from16 v0, v20

    invoke-direct {v3, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 1731
    .local v3, "borderColor":Lorg/apache/poi/java/awt/Color;
    if-lez v9, :cond_2

    .line 1732
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getCellBorderStyle(I)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v4

    .line 1733
    .local v4, "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getCellBorderWidth(I)F

    move-result v5

    .line 1736
    .local v5, "borderWidth":F
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getCustomPalette()Lorg/apache/poi/hssf/usermodel/HSSFPalette;

    move-result-object v11

    .line 1737
    .local v11, "pallette":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getLeftBorderColor()S

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/apache/poi/hssf/usermodel/HSSFPalette;->getColor(S)Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v10

    .line 1740
    .local v10, "leftbdColor":Lorg/apache/poi/hssf/util/HSSFColor;
    if-eqz v10, :cond_0

    .line 1741
    invoke-virtual {v10}, Lorg/apache/poi/hssf/util/HSSFColor;->getTriplet()[S

    move-result-object v19

    .line 1744
    .local v19, "tripletBDColor":[S
    const/16 v20, 0x0

    aget-short v12, v19, v20

    .line 1745
    .local v12, "r":I
    const/16 v20, 0x1

    aget-short v8, v19, v20

    .line 1746
    .local v8, "g":I
    const/16 v20, 0x2

    aget-short v2, v19, v20

    .line 1748
    .local v2, "b":I
    shl-int/lit8 v20, v12, 0x10

    shl-int/lit8 v21, v8, 0x8

    or-int v20, v20, v21

    or-int v13, v20, v2

    .line 1749
    .local v13, "rgbColorVal":I
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    .end local v3    # "borderColor":Lorg/apache/poi/java/awt/Color;
    invoke-direct {v3, v13}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 1752
    .end local v2    # "b":I
    .end local v8    # "g":I
    .end local v12    # "r":I
    .end local v13    # "rgbColorVal":I
    .end local v19    # "tripletBDColor":[S
    .restart local v3    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :cond_0
    float-to-double v0, v5

    move-wide/from16 v20, v0

    const-wide v22, 0x3fb999999999999aL    # 0.1

    mul-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v4, v3, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 1767
    sget-object v20, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-object/from16 v0, v20

    if-ne v4, v0, :cond_1

    sget-object v20, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v0, v20

    if-eq v3, v0, :cond_2

    .line 1769
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v20, v0

    add-int/lit8 v21, p4, 0x1

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getRow(I)Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCell(I)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v20

    float-to-double v0, v5

    move-wide/from16 v22, v0

    const-wide v24, 0x3fb999999999999aL    # 0.1

    mul-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v4, v3, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 1776
    .end local v4    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v5    # "borderWidth":F
    .end local v10    # "leftbdColor":Lorg/apache/poi/hssf/util/HSSFColor;
    .end local v11    # "pallette":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    :cond_2
    if-lez v14, :cond_5

    .line 1777
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getCellBorderStyle(I)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v4

    .line 1778
    .restart local v4    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getCellBorderWidth(I)F

    move-result v5

    .line 1781
    .restart local v5    # "borderWidth":F
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getCustomPalette()Lorg/apache/poi/hssf/usermodel/HSSFPalette;

    move-result-object v11

    .line 1782
    .restart local v11    # "pallette":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getRightBorderColor()S

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/apache/poi/hssf/usermodel/HSSFPalette;->getColor(S)Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v15

    .line 1785
    .local v15, "rightbdColor":Lorg/apache/poi/hssf/util/HSSFColor;
    if-eqz v15, :cond_3

    .line 1786
    invoke-virtual {v15}, Lorg/apache/poi/hssf/util/HSSFColor;->getTriplet()[S

    move-result-object v19

    .line 1789
    .restart local v19    # "tripletBDColor":[S
    const/16 v20, 0x0

    aget-short v12, v19, v20

    .line 1790
    .restart local v12    # "r":I
    const/16 v20, 0x1

    aget-short v8, v19, v20

    .line 1791
    .restart local v8    # "g":I
    const/16 v20, 0x2

    aget-short v2, v19, v20

    .line 1793
    .restart local v2    # "b":I
    shl-int/lit8 v20, v12, 0x10

    shl-int/lit8 v21, v8, 0x8

    or-int v20, v20, v21

    or-int v13, v20, v2

    .line 1795
    .restart local v13    # "rgbColorVal":I
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    .end local v3    # "borderColor":Lorg/apache/poi/java/awt/Color;
    invoke-direct {v3, v13}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 1798
    .end local v2    # "b":I
    .end local v8    # "g":I
    .end local v12    # "r":I
    .end local v13    # "rgbColorVal":I
    .end local v19    # "tripletBDColor":[S
    .restart local v3    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :cond_3
    float-to-double v0, v5

    move-wide/from16 v20, v0

    const-wide v22, 0x3fb999999999999aL    # 0.1

    mul-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v4, v3, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 1813
    sget-object v20, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-object/from16 v0, v20

    if-ne v4, v0, :cond_4

    sget-object v20, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v0, v20

    if-eq v3, v0, :cond_5

    .line 1815
    :cond_4
    add-int/lit8 v20, p5, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mLeftBorderCellNum:I

    .line 1816
    new-instance v20, Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-direct/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/TableBorder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mNextCellLeftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    .line 1817
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mNextCellLeftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)V

    .line 1818
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mNextCellLeftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderColor(Lorg/apache/poi/java/awt/Color;)V

    .line 1819
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mNextCellLeftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderWidth(F)V

    .line 1822
    .end local v4    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v5    # "borderWidth":F
    .end local v11    # "pallette":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    .end local v15    # "rightbdColor":Lorg/apache/poi/hssf/util/HSSFColor;
    :cond_5
    if-lez v17, :cond_8

    .line 1823
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getCellBorderStyle(I)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v4

    .line 1824
    .restart local v4    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getCellBorderWidth(I)F

    move-result v5

    .line 1827
    .restart local v5    # "borderWidth":F
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getCustomPalette()Lorg/apache/poi/hssf/usermodel/HSSFPalette;

    move-result-object v11

    .line 1828
    .restart local v11    # "pallette":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getTopBorderColor()S

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/apache/poi/hssf/usermodel/HSSFPalette;->getColor(S)Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v18

    .line 1831
    .local v18, "topbdColor":Lorg/apache/poi/hssf/util/HSSFColor;
    if-eqz v18, :cond_6

    .line 1832
    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/hssf/util/HSSFColor;->getTriplet()[S

    move-result-object v19

    .line 1835
    .restart local v19    # "tripletBDColor":[S
    const/16 v20, 0x0

    aget-short v12, v19, v20

    .line 1836
    .restart local v12    # "r":I
    const/16 v20, 0x1

    aget-short v8, v19, v20

    .line 1837
    .restart local v8    # "g":I
    const/16 v20, 0x2

    aget-short v2, v19, v20

    .line 1839
    .restart local v2    # "b":I
    shl-int/lit8 v20, v12, 0x10

    shl-int/lit8 v21, v8, 0x8

    or-int v20, v20, v21

    or-int v13, v20, v2

    .line 1841
    .restart local v13    # "rgbColorVal":I
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    .end local v3    # "borderColor":Lorg/apache/poi/java/awt/Color;
    invoke-direct {v3, v13}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 1844
    .end local v2    # "b":I
    .end local v8    # "g":I
    .end local v12    # "r":I
    .end local v13    # "rgbColorVal":I
    .end local v19    # "tripletBDColor":[S
    .restart local v3    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :cond_6
    float-to-double v0, v5

    move-wide/from16 v20, v0

    const-wide v22, 0x3fb999999999999aL    # 0.1

    mul-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v4, v3, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 1857
    sget-object v20, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-object/from16 v0, v20

    if-ne v4, v0, :cond_7

    sget-object v20, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v0, v20

    if-eq v3, v0, :cond_8

    .line 1859
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getRow(I)Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v20

    add-int/lit8 v21, p5, 0x1

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCell(I)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v20

    float-to-double v0, v5

    move-wide/from16 v22, v0

    const-wide v24, 0x3fb999999999999aL    # 0.1

    mul-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v4, v3, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 1866
    .end local v4    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v5    # "borderWidth":F
    .end local v11    # "pallette":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    .end local v18    # "topbdColor":Lorg/apache/poi/hssf/util/HSSFColor;
    :cond_8
    if-lez v6, :cond_b

    .line 1867
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getCellBorderStyle(I)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v4

    .line 1868
    .restart local v4    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getCellBorderWidth(I)F

    move-result v5

    .line 1871
    .restart local v5    # "borderWidth":F
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getCustomPalette()Lorg/apache/poi/hssf/usermodel/HSSFPalette;

    move-result-object v11

    .line 1872
    .restart local v11    # "pallette":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    invoke-virtual/range {p3 .. p3}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getBottomBorderColor()S

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/apache/poi/hssf/usermodel/HSSFPalette;->getColor(S)Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v7

    .line 1874
    .local v7, "bottombdColor":Lorg/apache/poi/hssf/util/HSSFColor;
    if-eqz v7, :cond_9

    .line 1875
    invoke-virtual {v7}, Lorg/apache/poi/hssf/util/HSSFColor;->getTriplet()[S

    move-result-object v19

    .line 1878
    .restart local v19    # "tripletBDColor":[S
    const/16 v20, 0x0

    aget-short v12, v19, v20

    .line 1879
    .restart local v12    # "r":I
    const/16 v20, 0x1

    aget-short v8, v19, v20

    .line 1880
    .restart local v8    # "g":I
    const/16 v20, 0x2

    aget-short v2, v19, v20

    .line 1882
    .restart local v2    # "b":I
    shl-int/lit8 v20, v12, 0x10

    shl-int/lit8 v21, v8, 0x8

    or-int v20, v20, v21

    or-int v13, v20, v2

    .line 1884
    .restart local v13    # "rgbColorVal":I
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    .end local v3    # "borderColor":Lorg/apache/poi/java/awt/Color;
    invoke-direct {v3, v13}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 1887
    .end local v2    # "b":I
    .end local v8    # "g":I
    .end local v12    # "r":I
    .end local v13    # "rgbColorVal":I
    .end local v19    # "tripletBDColor":[S
    .restart local v3    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :cond_9
    float-to-double v0, v5

    move-wide/from16 v20, v0

    const-wide v22, 0x3fb999999999999aL    # 0.1

    mul-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v4, v3, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 1901
    sget-object v20, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-object/from16 v0, v20

    if-ne v4, v0, :cond_a

    sget-object v20, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    move-object/from16 v0, v20

    if-eq v3, v0, :cond_b

    .line 1903
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mTopBorderRowNum:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    add-int/lit8 v21, p4, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1905
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mTopBorderCellNum:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1907
    new-instance v16, Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/thumbnail/customview/tables/TableBorder;-><init>()V

    .line 1908
    .local v16, "topBorder":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)V

    .line 1909
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderColor(Lorg/apache/poi/java/awt/Color;)V

    .line 1910
    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->setBorderWidth(F)V

    .line 1911
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mNextCellTopBorder:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1914
    .end local v4    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v5    # "borderWidth":F
    .end local v7    # "bottombdColor":Lorg/apache/poi/hssf/util/HSSFColor;
    .end local v11    # "pallette":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    .end local v16    # "topBorder":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    :cond_b
    return-void
.end method

.method private addSurroundingCellsBorder(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;II)V
    .locals 10
    .param p1, "cell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "rowNumber"    # I
    .param p3, "cellNumber"    # I

    .prologue
    const-wide v8, 0x3fb999999999999aL    # 0.1

    const/4 v6, 0x0

    .line 2024
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mNextCellLeftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mLeftBorderCellNum:I

    if-ne p3, v1, :cond_0

    .line 2025
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mNextCellLeftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mNextCellLeftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mNextCellLeftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v3

    float-to-double v4, v3

    mul-double/2addr v4, v8

    double-to-float v3, v4

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2028
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mNextCellLeftBorder:Lcom/samsung/thumbnail/customview/tables/TableBorder;

    .line 2029
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mLeftBorderCellNum:I

    .line 2031
    :cond_0
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mNextCellTopBorder:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mTopBorderRowNum:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p2, v1, :cond_1

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mTopBorderCellNum:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p3, v1, :cond_1

    .line 2034
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mNextCellTopBorder:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/customview/tables/TableBorder;

    .line 2035
    .local v0, "topBorder":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v3

    float-to-double v4, v3

    mul-double/2addr v4, v8

    double-to-float v3, v4

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2038
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mTopBorderCellNum:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2039
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mTopBorderRowNum:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2040
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mNextCellTopBorder:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2042
    .end local v0    # "topBorder":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    :cond_1
    return-void
.end method

.method private addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 4
    .param p1, "newCanvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    const v3, 0x3dcccccd    # 0.1f

    .line 2702
    const/4 v0, 0x0

    .line 2704
    .local v0, "border":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v0

    .line 2706
    if-nez v0, :cond_0

    iget-boolean v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isGridDisplayed:Z

    if-eqz v1, :cond_0

    .line 2707
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2711
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v0

    .line 2713
    if-nez v0, :cond_1

    iget-boolean v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isGridDisplayed:Z

    if-eqz v1, :cond_1

    .line 2714
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2718
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v0

    .line 2720
    if-nez v0, :cond_2

    iget-boolean v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isGridDisplayed:Z

    if-eqz v1, :cond_2

    .line 2721
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2725
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v0

    .line 2727
    if-nez v0, :cond_3

    iget-boolean v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isGridDisplayed:Z

    if-eqz v1, :cond_3

    .line 2728
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2732
    :cond_3
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->addCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 2733
    return-void
.end method

.method private static alpha()[Ljava/lang/String;
    .locals 7

    .prologue
    .line 2579
    const/16 v5, 0x2bf

    new-array v4, v5, [Ljava/lang/String;

    .line 2580
    .local v4, "result":[Ljava/lang/String;
    const/4 v5, 0x0

    const-string/jumbo v6, ""

    aput-object v6, v4, v5

    .line 2581
    const/4 v3, 0x0

    .local v3, "j":I
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    const/16 v5, 0x2bd

    if-gt v3, v5, :cond_1

    .line 2582
    rem-int/lit8 v5, v3, 0x1a

    add-int/lit8 v5, v5, 0x41

    int-to-char v0, v5

    .line 2583
    .local v0, "a":C
    div-int/lit8 v5, v3, 0x1a

    add-int/lit8 v5, v5, 0x41

    add-int/lit8 v5, v5, -0x1

    int-to-char v1, v5

    .line 2585
    .local v1, "b":C
    div-int/lit8 v5, v3, 0x1a

    const/4 v6, 0x1

    if-lt v5, v6, :cond_0

    .line 2586
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    .line 2581
    :goto_1
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2588
    :cond_0
    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    goto :goto_1

    .line 2591
    .end local v0    # "a":C
    .end local v1    # "b":C
    :cond_1
    return-object v4
.end method

.method private apply_empty_cell(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V
    .locals 3
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "cellNum"    # I

    .prologue
    .line 2692
    sget-object v1, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->colmnWdthChartsPicts:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .local v0, "colWid":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 2693
    const/high16 v1, 0x428c0000    # 70.0f

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 2696
    :goto_0
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellHeight(F)V

    .line 2697
    return-void

    .line 2695
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    goto :goto_0
.end method

.method private apply_firstrowtopborderstyle(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 3
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    .line 2683
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v1, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    const v2, 0x3dcccccd    # 0.1f

    invoke-virtual {p1, v0, v1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2688
    return-void
.end method

.method private apply_table03(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 4
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    const/16 v2, 0xe7

    const v3, 0x3dcccccd    # 0.1f

    .line 2603
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    const/16 v1, 0xe3

    invoke-direct {v0, v2, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 2608
    .local v0, "fillColor":Lorg/apache/poi/java/awt/Color;
    const/high16 v1, 0x428c0000    # 70.0f

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 2609
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellHeight(F)V

    .line 2611
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    .line 2613
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2615
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2617
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2619
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2621
    return-void
.end method

.method private apply_tbl_first_cell(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 4
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    const/16 v2, 0xc6

    const v3, 0x3dcccccd    # 0.1f

    .line 2631
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    const/16 v1, 0xc3

    invoke-direct {v0, v2, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 2636
    .local v0, "fillColor":Lorg/apache/poi/java/awt/Color;
    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 2637
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellHeight(F)V

    .line 2639
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    .line 2641
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2643
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2645
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2647
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2649
    return-void
.end method

.method private apply_tbl_first_column(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 4
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    const/16 v2, 0xde

    const v3, 0x3dcccccd    # 0.1f

    .line 2660
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    const/16 v1, 0xdf

    invoke-direct {v0, v2, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 2664
    .local v0, "fillColor":Lorg/apache/poi/java/awt/Color;
    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 2665
    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {p1, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellHeight(F)V

    .line 2667
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    .line 2669
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2671
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2673
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2675
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->DEFAULT_BORDER_COLOR:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 2677
    return-void
.end method

.method private doMerges(IIII)V
    .locals 11
    .param p1, "chkrow1"    # I
    .param p2, "chkrow2"    # I
    .param p3, "chkcol1"    # I
    .param p4, "chkcol2"    # I

    .prologue
    const/4 v10, 0x1

    .line 869
    const/4 v2, 0x0

    .local v2, "iCount":I
    const/4 v3, 0x0

    .line 871
    .local v3, "jCount":I
    const/4 v5, 0x0

    .line 873
    .local v5, "rowmergeLocal":Z
    if-eq p1, p2, :cond_0

    .line 874
    const/4 v5, 0x1

    .line 876
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 878
    .local v4, "mergedCellKey":Ljava/lang/String;
    move v2, p1

    :goto_0
    if-gt v2, p2, :cond_5

    .line 879
    iget-object v8, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v8, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getRow(I)Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v1

    .line 880
    .local v1, "currentRowTemp":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    const/4 v7, 0x0

    .line 881
    .local v7, "totalWidth":F
    invoke-virtual {v1, p3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCell(I)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v6

    .line 883
    .local v6, "startingCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    if-ne v10, v5, :cond_1

    .line 884
    if-ne p1, v2, :cond_3

    .line 885
    const-string/jumbo v8, "restart"

    invoke-virtual {v6, v8}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellMergeValue(Ljava/lang/String;)V

    .line 889
    :goto_1
    invoke-virtual {v6, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setMergedCellKey(Ljava/lang/String;)V

    .line 892
    :cond_1
    move v3, p3

    :goto_2
    if-gt v3, p4, :cond_4

    .line 893
    invoke-virtual {v1, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCell(I)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v0

    .line 895
    .local v0, "currentCellTemp":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v8

    add-float/2addr v7, v8

    .line 896
    if-eq v3, p3, :cond_2

    .line 897
    invoke-virtual {v0, v10}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setHideCell(Z)V

    .line 892
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 887
    .end local v0    # "currentCellTemp":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    :cond_3
    const-string/jumbo v8, "continue"

    invoke-virtual {v6, v8}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellMergeValue(Ljava/lang/String;)V

    goto :goto_1

    .line 901
    :cond_4
    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 878
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 903
    .end local v1    # "currentRowTemp":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    .end local v6    # "startingCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v7    # "totalWidth":F
    :cond_5
    return-void
.end method

.method private drawShapes(Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/record/EscherAggregate;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 44
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p2, "escherAggregate"    # Lorg/apache/poi/hssf/record/EscherAggregate;
    .param p3, "wb"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 1010
    const/16 v16, 0x0

    .local v16, "x1value":I
    const/4 v15, 0x0

    .local v15, "y1value":I
    const/16 v18, 0x0

    .local v18, "x2value":I
    const/16 v17, 0x0

    .line 1011
    .local v17, "y2value":I
    const/16 v34, 0x0

    .local v34, "row1":I
    const/16 v21, 0x0

    .local v21, "column1":I
    const/16 v35, 0x0

    .local v35, "row2":I
    const/16 v22, 0x0

    .local v22, "column2":I
    const/16 v24, 0x0

    .local v24, "dx1":I
    const/16 v26, 0x0

    .local v26, "dy1":I
    const/16 v25, 0x0

    .local v25, "dx2":I
    const/16 v27, 0x0

    .line 1013
    .local v27, "dy2":I
    const/16 v29, 0x0

    .line 1018
    .local v29, "escherContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-eqz p2, :cond_2

    .line 1020
    const/16 v23, 0x0

    .line 1021
    .local v23, "count":I
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/record/EscherAggregate;->getEscherRecords()Ljava/util/List;

    move-result-object v30

    .line 1024
    .local v30, "escherRecords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-interface/range {v30 .. v30}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v32

    .line 1025
    .local v32, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_0
    :goto_0
    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1026
    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lorg/apache/poi/ddf/EscherRecord;

    .line 1027
    .local v28, "er":Lorg/apache/poi/ddf/EscherRecord;
    move-object/from16 v0, v28

    instance-of v3, v0, Lorg/apache/poi/ddf/EscherContainerRecord;

    if-eqz v3, :cond_0

    move-object/from16 v29, v28

    .line 1028
    check-cast v29, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 1030
    const/16 v3, -0xff5

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1, v3}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getEscherRecordList(Lorg/apache/poi/ddf/EscherContainerRecord;I)Ljava/util/List;

    move-result-object v41

    .line 1033
    .local v41, "tempescherOptRecordList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    const/16 v3, -0xff6

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1, v3}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getEscherRecordList(Lorg/apache/poi/ddf/EscherContainerRecord;I)Ljava/util/List;

    move-result-object v42

    .line 1038
    .local v42, "tempescherSpRecordList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherSpRecord;>;"
    if-nez v23, :cond_1

    .line 1039
    const/4 v3, 0x0

    move-object/from16 v0, v42

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1042
    :cond_1
    const/16 v3, -0xff0

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1, v3}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getEscherRecordList(Lorg/apache/poi/ddf/EscherContainerRecord;I)Ljava/util/List;

    move-result-object v40

    .line 1045
    .local v40, "tempescherClientAnchorList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherClientAnchorRecord;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->escherOptRecordList:Ljava/util/List;

    move-object/from16 v0, v41

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1046
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->escherSpRecordList:Ljava/util/List;

    move-object/from16 v0, v42

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1047
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->escherClientAnchorList:Ljava/util/List;

    move-object/from16 v0, v40

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1049
    add-int/lit8 v23, v23, 0x1

    goto :goto_0

    .line 1053
    .end local v23    # "count":I
    .end local v28    # "er":Lorg/apache/poi/ddf/EscherRecord;
    .end local v30    # "escherRecords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v32    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v40    # "tempescherClientAnchorList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherClientAnchorRecord;>;"
    .end local v41    # "tempescherOptRecordList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v42    # "tempescherSpRecordList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherSpRecord;>;"
    :cond_2
    const/16 v31, 0x0

    .local v31, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->escherSpRecordList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, v31

    if-ge v0, v3, :cond_2d

    .line 1054
    new-instance v3, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    invoke-direct {v3}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    .line 1055
    const/4 v14, 0x0

    .line 1056
    .local v14, "shapeCountForName":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->escherSpRecordList:Ljava/util/List;

    move/from16 v0, v31

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 1057
    .local v39, "spTempRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    invoke-virtual/range {v39 .. v39}, Lorg/apache/poi/ddf/EscherSpRecord;->getOptions()S

    move-result v3

    shr-int/lit8 v3, v3, 0x4

    invoke-static {v3}, Lorg/apache/poi/hslf/model/ShapeTypes;->typeName(I)Ljava/lang/String;

    move-result-object v38

    .line 1060
    .local v38, "shapename":Ljava/lang/String;
    if-eqz v38, :cond_5

    const-string/jumbo v3, "HostControl"

    move-object/from16 v0, v38

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v3, "PictureFrame"

    move-object/from16 v0, v38

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1064
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->escherOptRecordList:Ljava/util/List;

    move/from16 v0, v31

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherOptRecord;

    invoke-virtual {v6, v3}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->setEscherOptRecord(Lorg/apache/poi/ddf/EscherOptRecord;)V

    .line 1067
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->escherSpRecordList:Ljava/util/List;

    move/from16 v0, v31

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherSpRecord;

    invoke-virtual {v6, v3}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->setSpRecord(Lorg/apache/poi/ddf/EscherSpRecord;)V

    .line 1068
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->escherClientAnchorList:Ljava/util/List;

    move/from16 v0, v31

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    invoke-virtual {v6, v3}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->setEscherClientAnchorRecord(Lorg/apache/poi/ddf/EscherClientAnchorRecord;)V

    .line 1071
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v37

    .line 1072
    .local v37, "shapeName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getShapeId()I

    move-result v36

    .line 1073
    .local v36, "shapeID":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v3, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->setShapeColor(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 1077
    const/16 v43, 0x0

    .line 1078
    .local v43, "textObj":Lorg/apache/poi/hssf/record/TextObjectRecord;
    if-eqz p2, :cond_3

    .line 1079
    invoke-static/range {v36 .. v36}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lorg/apache/poi/hssf/record/EscherAggregate;->getTextForShape(Ljava/lang/Integer;)Lorg/apache/poi/hssf/record/TextObjectRecord;

    move-result-object v43

    .line 1080
    :cond_3
    new-instance v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-direct {v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 1081
    .local v4, "para":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    const/4 v8, 0x0

    .line 1082
    .local v8, "horizontalAlign":I
    const/4 v9, 0x0

    .line 1084
    .local v9, "verticalAlign":I
    if-eqz v43, :cond_7

    .line 1085
    invoke-virtual/range {v43 .. v43}, Lorg/apache/poi/hssf/record/TextObjectRecord;->getHorizontalTextAlignment()I

    move-result v8

    .line 1086
    invoke-virtual/range {v43 .. v43}, Lorg/apache/poi/hssf/record/TextObjectRecord;->getVerticalTextAlignment()I

    move-result v9

    .line 1087
    invoke-virtual/range {v43 .. v43}, Lorg/apache/poi/hssf/record/TextObjectRecord;->getStr()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v5

    .line 1088
    .local v5, "textString":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    invoke-virtual/range {v43 .. v43}, Lorg/apache/poi/hssf/record/TextObjectRecord;->getStr()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getFontAtIndex(I)S

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getWorkbook()Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text_hssfFont(SLorg/apache/poi/hssf/usermodel/HSSFWorkbook;)Lorg/apache/poi/hssf/usermodel/HSSFFont;

    move-result-object v7

    .line 1090
    .local v7, "hssfFont":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    const/16 v33, 0x1

    .local v33, "k":I
    :goto_2
    invoke-virtual/range {v43 .. v43}, Lorg/apache/poi/hssf/record/TextObjectRecord;->getStr()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    move/from16 v0, v33

    if-gt v0, v3, :cond_4

    .line 1091
    invoke-virtual/range {v43 .. v43}, Lorg/apache/poi/hssf/record/TextObjectRecord;->getStr()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getFontAtIndex(I)S

    move-result v3

    invoke-virtual/range {v43 .. v43}, Lorg/apache/poi/hssf/record/TextObjectRecord;->getStr()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v6

    move/from16 v0, v33

    invoke-virtual {v6, v0}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getFontAtIndex(I)S

    move-result v6

    if-eq v3, v6, :cond_6

    .line 1093
    const/4 v7, 0x0

    .line 1097
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getWorkbook()Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    move-result-object v6

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text_Shape(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFFont;II)Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-result-object v4

    .line 1106
    .end local v5    # "textString":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    .end local v7    # "hssfFont":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    .end local v33    # "k":I
    :goto_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->sheetIndex:I

    mul-int/lit16 v3, v3, 0x3e8

    add-int v14, v3, v31

    .line 1108
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getRow1()S

    move-result v34

    .line 1109
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getCol1()S

    move-result v21

    .line 1110
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getRow2()S

    move-result v35

    .line 1111
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getCol2()S

    move-result v22

    .line 1112
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getOffsetCol1()S

    move-result v24

    .line 1113
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getOffsetRow1()S

    move-result v26

    .line 1114
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getOffsetCol2()S

    move-result v25

    .line 1115
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getOffsetRow2()S

    move-result v27

    .line 1117
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    if-eqz v3, :cond_8

    .line 1118
    const/16 v3, 0x14

    move/from16 v0, v34

    if-ge v0, v3, :cond_5

    const/16 v3, 0x10

    move/from16 v0, v21

    if-lt v0, v3, :cond_8

    .line 1053
    .end local v4    # "para":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v8    # "horizontalAlign":I
    .end local v9    # "verticalAlign":I
    .end local v36    # "shapeID":I
    .end local v37    # "shapeName":Ljava/lang/String;
    .end local v43    # "textObj":Lorg/apache/poi/hssf/record/TextObjectRecord;
    :cond_5
    :goto_4
    add-int/lit8 v31, v31, 0x1

    goto/16 :goto_1

    .line 1090
    .restart local v4    # "para":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .restart local v5    # "textString":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    .restart local v7    # "hssfFont":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    .restart local v8    # "horizontalAlign":I
    .restart local v9    # "verticalAlign":I
    .restart local v33    # "k":I
    .restart local v36    # "shapeID":I
    .restart local v37    # "shapeName":Ljava/lang/String;
    .restart local v43    # "textObj":Lorg/apache/poi/hssf/record/TextObjectRecord;
    :cond_6
    add-int/lit8 v33, v33, 0x1

    goto/16 :goto_2

    .line 1100
    .end local v5    # "textString":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    .end local v7    # "hssfFont":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    .end local v33    # "k":I
    :cond_7
    const/4 v4, 0x0

    goto :goto_3

    .line 1123
    :cond_8
    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getXvalue(II)I

    move-result v16

    .line 1124
    move-object/from16 v0, p0

    move/from16 v1, v34

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getYvalue(II)I

    move-result v15

    .line 1125
    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getXvalue(II)I

    move-result v18

    .line 1126
    move-object/from16 v0, p0

    move/from16 v1, v35

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getYvalue(II)I

    move-result v17

    .line 1129
    if-eqz v37, :cond_5

    .line 1130
    const-string/jumbo v3, "Rect"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "Rectangle"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "RoundRectangle"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "Diamond"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "Ellipse"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "IsocelesTriangle"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "Triangle"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "RightTriangle"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "Parallelogram"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "Pentagon"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "Hexagon"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "Octagon"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "Plus"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "Can"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "rtTriangle"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "mathPlus"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string/jumbo v3, "TextBox"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1150
    :cond_9
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "BasicShapes"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1155
    .local v10, "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1156
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1157
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_a
    const-string/jumbo v3, "ActionButtonHome"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1158
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "BasicShapes"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1163
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1164
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1165
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_b
    const-string/jumbo v3, "RightArrowCallout"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "LeftArrowCallout"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "UpArrowCallout"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "DownArrowCallout"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "LeftRightArrowCallout"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1170
    :cond_c
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "ArrowCallout"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1175
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1176
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1177
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_d
    const-string/jumbo v3, "BentConnector2"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "BentConnector3"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "CurvedConnector3"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "StraightConnector1"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "Line"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 1183
    :cond_e
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "BentConnector"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1188
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1189
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1191
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_f
    const-string/jumbo v3, "DownArrow"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    const-string/jumbo v3, "UpArrow"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    const-string/jumbo v3, "Arrow"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    const-string/jumbo v3, "RightArrow"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    const-string/jumbo v3, "LeftArrow"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    const-string/jumbo v3, "LeftRightArrow"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    const-string/jumbo v3, "UpDownArrow"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    const-string/jumbo v3, "NotchedRightArrow"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 1199
    :cond_10
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "BlockArrow"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1204
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1205
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1206
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_11
    const-string/jumbo v3, "HomePlate"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string/jumbo v3, "Chevron"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string/jumbo v3, "Plaque"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string/jumbo v3, "LightningBolt"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string/jumbo v3, "Moon"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string/jumbo v3, "Cube"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string/jumbo v3, "FoldedCorner"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string/jumbo v3, "Bevel"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string/jumbo v3, "Sun"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string/jumbo v3, "SmileyFace"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 1216
    :cond_12
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "BlockShapes"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1221
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1222
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1223
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_13
    const-string/jumbo v3, "LeftBrace"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "RightBrace"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "BracePair"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 1226
    :cond_14
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "BraceShapes"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1231
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1232
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1233
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_15
    const-string/jumbo v3, "BracketPair"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    const-string/jumbo v3, "LeftBracket"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    const-string/jumbo v3, "RightBracket"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 1236
    :cond_16
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "BracketShapes"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1241
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1242
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1243
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_17
    const-string/jumbo v3, "Ribbon2"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_18

    const-string/jumbo v3, "Ribbon"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_18

    const-string/jumbo v3, "EllipseRibbon"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_18

    const-string/jumbo v3, "EllipseRibbon2"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 1247
    :cond_18
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "Ribbbonhapes"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1252
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1253
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1254
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_19
    const-string/jumbo v3, "Callout1"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1a

    const-string/jumbo v3, "BorderCallout1"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1a

    const-string/jumbo v3, "AccentCallout1"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1a

    const-string/jumbo v3, "AccentBorderCallout1"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 1258
    :cond_1a
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "Callout1"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1263
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1264
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1265
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_1b
    const-string/jumbo v3, "Callout2"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1c

    const-string/jumbo v3, "BorderCallout2"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1c

    const-string/jumbo v3, "AccentCallout2"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1c

    const-string/jumbo v3, "AccentBorderCallout2"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 1269
    :cond_1c
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "Callout2"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1274
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1275
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1276
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_1d
    const-string/jumbo v3, "Callout3"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1e

    const-string/jumbo v3, "BorderCallout3"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1e

    const-string/jumbo v3, "AccentCallout3"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1e

    const-string/jumbo v3, "AccentBorderCallout3"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 1280
    :cond_1e
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "Callout3"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1285
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1286
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1287
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_1f
    const-string/jumbo v3, "CurvedRightArrow"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_20

    const-string/jumbo v3, "CurvedLeftArrow"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_20

    const-string/jumbo v3, "CurvedUpArrow"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_20

    const-string/jumbo v3, "CurvedDownArrow"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 1291
    :cond_20
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "CurvedBlockArrow"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1296
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1297
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1298
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_21
    const-string/jumbo v3, "FlowChartMultidocument"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartDocument"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartPunchedTape"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartDisplay"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartMagneticDrum"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartMagneticDisk"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartMagneticTape"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartDelay"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartOnlineStorage"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartMerge"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartExtract"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartSort"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartCollate"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartOr"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartSummingJunction"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartPunchedCard"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartOffpageConnector"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartConnector"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartManualOperation"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartManualInput"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartPreparation"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartTerminator"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartInternalStorage"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartPredefinedProcess"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartInputOutput"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartDecision"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartAlternateProcess"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "FlowChartProcess"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_23

    .line 1326
    :cond_22
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "FlowChartProcess"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1331
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1332
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1333
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_23
    const-string/jumbo v3, "IrregularSeal1"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_24

    const-string/jumbo v3, "IrregularSeal2"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_25

    .line 1335
    :cond_24
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "IrregularSeal"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1340
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1341
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1342
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_25
    const-string/jumbo v3, "HorizontalScroll"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_26

    const-string/jumbo v3, "VerticalScroll"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_27

    .line 1344
    :cond_26
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "ScrollShapes"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1349
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1350
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1351
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_27
    const-string/jumbo v3, "Star4"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_28

    const-string/jumbo v3, "Star8"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_28

    const-string/jumbo v3, "Star16"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_28

    const-string/jumbo v3, "Star24"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_28

    const-string/jumbo v3, "Star32"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_28

    const-string/jumbo v3, "Star"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_28

    const-string/jumbo v3, "StarShapes"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_29

    .line 1358
    :cond_28
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "StarShapes"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1363
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1364
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1365
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_29
    const-string/jumbo v3, "Wave"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2a

    const-string/jumbo v3, "DoubleWave"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 1367
    :cond_2a
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "WaveShapes"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1372
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1373
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1374
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_2b
    const-string/jumbo v3, "WedgeRectCallout"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2c

    const-string/jumbo v3, "WedgeRRectCallout"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2c

    const-string/jumbo v3, "wedgeRoundRectCallout"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2c

    const-string/jumbo v3, "WedgeEllipseCallout"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2c

    const-string/jumbo v3, "CloudCallout"

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1379
    :cond_2c
    new-instance v10, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v11, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v12, "WedgeCallouts"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->folderPath:Ljava/io/File;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v20, v0

    invoke-direct/range {v10 .. v20}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1384
    .restart local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v10, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 1385
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_4

    .line 1390
    .end local v4    # "para":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v8    # "horizontalAlign":I
    .end local v9    # "verticalAlign":I
    .end local v10    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .end local v14    # "shapeCountForName":I
    .end local v36    # "shapeID":I
    .end local v37    # "shapeName":Ljava/lang/String;
    .end local v38    # "shapename":Ljava/lang/String;
    .end local v39    # "spTempRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    .end local v43    # "textObj":Lorg/apache/poi/hssf/record/TextObjectRecord;
    :cond_2d
    return-void
.end method

.method private drawShapesPictCharts(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/record/EscherAggregate;)V
    .locals 6
    .param p1, "wb"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p3, "escherAggregate"    # Lorg/apache/poi/hssf/record/EscherAggregate;

    .prologue
    .line 792
    if-eqz p3, :cond_0

    .line 793
    const/16 v3, -0xff8

    :try_start_0
    invoke-virtual {p3, v3}, Lorg/apache/poi/hssf/record/EscherAggregate;->findFirstWithId(S)Lorg/apache/poi/ddf/EscherRecord;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherDgRecord;

    .line 796
    .local v0, "dgRecord":Lorg/apache/poi/ddf/EscherDgRecord;
    if-eqz v0, :cond_0

    .line 797
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherDgRecord;->getNumShapes()I

    move-result v3

    if-eqz v3, :cond_0

    .line 798
    invoke-direct {p0, p2, p3, p1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->drawShapes(Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/record/EscherAggregate;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 803
    .end local v0    # "dgRecord":Lorg/apache/poi/ddf/EscherDgRecord;
    :cond_0
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    move-result v3

    if-eqz v3, :cond_1

    .line 805
    invoke-direct {p0, p1, p2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getPictures(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFSheet;)V

    .line 807
    :cond_1
    iget-boolean v3, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->containChart:Z

    if-eqz v3, :cond_2

    .line 809
    invoke-direct {p0, p2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->setChartValues(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)V

    .line 810
    new-instance v2, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;

    iget-object v3, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v2, v3}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 812
    .local v2, "newXLSChartObj":Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mExcelChartList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;->drawchartForCanvas(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 819
    .end local v2    # "newXLSChartObj":Lcom/samsung/thumbnail/office/excel/ExcelChartDisplay;
    :cond_2
    :goto_0
    return-void

    .line 815
    :catch_0
    move-exception v1

    .line 816
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "XlsCanvasView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getCellBorderStyle(I)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .locals 1
    .param p1, "cellBorder"    # I

    .prologue
    .line 1971
    const/4 v0, 0x0

    .line 1972
    .local v0, "style":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    packed-switch p1, :pswitch_data_0

    .line 2016
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 2019
    :goto_0
    return-object v0

    .line 1974
    :pswitch_0
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 1975
    goto :goto_0

    .line 1977
    :pswitch_1
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 1978
    goto :goto_0

    .line 1980
    :pswitch_2
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MEDIUM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 1981
    goto :goto_0

    .line 1983
    :pswitch_3
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASHED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 1984
    goto :goto_0

    .line 1986
    :pswitch_4
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOTTED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 1987
    goto :goto_0

    .line 1989
    :pswitch_5
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 1990
    goto :goto_0

    .line 1992
    :pswitch_6
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 1993
    goto :goto_0

    .line 1995
    :pswitch_7
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THIN:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 1996
    goto :goto_0

    .line 1998
    :pswitch_8
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MEDIUM_DASHED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 1999
    goto :goto_0

    .line 2001
    :pswitch_9
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASH_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 2002
    goto :goto_0

    .line 2004
    :pswitch_a
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MEDIUM_DASH_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 2005
    goto :goto_0

    .line 2007
    :pswitch_b
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASH_DOT_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 2008
    goto :goto_0

    .line 2010
    :pswitch_c
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->MEDIUM_DASH_DOT_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 2011
    goto :goto_0

    .line 2013
    :pswitch_d
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASH_DOT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 2014
    goto :goto_0

    .line 1972
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method private getCellBorderWidth(I)F
    .locals 1
    .param p1, "cellBorder"    # I

    .prologue
    .line 1917
    const/4 v0, 0x0

    .line 1918
    .local v0, "width":F
    packed-switch p1, :pswitch_data_0

    .line 1964
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1967
    :goto_0
    return v0

    .line 1920
    :pswitch_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1921
    goto :goto_0

    .line 1923
    :pswitch_1
    const/high16 v0, 0x40a00000    # 5.0f

    .line 1924
    goto :goto_0

    .line 1926
    :pswitch_2
    const/high16 v0, 0x40200000    # 2.5f

    .line 1927
    goto :goto_0

    .line 1929
    :pswitch_3
    const/high16 v0, 0x40200000    # 2.5f

    .line 1930
    goto :goto_0

    .line 1932
    :pswitch_4
    const/high16 v0, 0x41200000    # 10.0f

    .line 1935
    goto :goto_0

    .line 1937
    :pswitch_5
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1938
    goto :goto_0

    .line 1940
    :pswitch_6
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1941
    goto :goto_0

    .line 1943
    :pswitch_7
    const/high16 v0, 0x40a00000    # 5.0f

    .line 1944
    goto :goto_0

    .line 1946
    :pswitch_8
    const/high16 v0, 0x40200000    # 2.5f

    .line 1947
    goto :goto_0

    .line 1949
    :pswitch_9
    const/high16 v0, 0x40a00000    # 5.0f

    .line 1950
    goto :goto_0

    .line 1952
    :pswitch_a
    const/high16 v0, 0x40a00000    # 5.0f

    .line 1955
    goto :goto_0

    .line 1957
    :pswitch_b
    const/high16 v0, 0x40200000    # 2.5f

    .line 1958
    goto :goto_0

    .line 1960
    :pswitch_c
    const/high16 v0, 0x40a00000    # 5.0f

    .line 1962
    goto :goto_0

    .line 1918
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_a
        :pswitch_c
    .end packed-switch
.end method

.method private getChartCellValue(Lorg/apache/poi/hssf/usermodel/HSSFCell;)Ljava/lang/String;
    .locals 5
    .param p1, "cell"    # Lorg/apache/poi/hssf/usermodel/HSSFCell;

    .prologue
    .line 3341
    const-string/jumbo v3, ""

    .line 3342
    .local v3, "value":Ljava/lang/String;
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 3367
    :goto_0
    :pswitch_0
    return-object v3

    .line 3345
    :pswitch_1
    invoke-static {p1}, Lorg/apache/poi/hssf/usermodel/HSSFDateUtil;->isCellDateFormatted(Lorg/apache/poi/ss/usermodel/Cell;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3346
    new-instance v0, Lorg/apache/poi/ss/usermodel/DataFormatter;

    invoke-direct {v0}, Lorg/apache/poi/ss/usermodel/DataFormatter;-><init>()V

    .line 3347
    .local v0, "F":Lorg/apache/poi/ss/usermodel/DataFormatter;
    invoke-virtual {v0, p1}, Lorg/apache/poi/ss/usermodel/DataFormatter;->formatCellValue(Lorg/apache/poi/ss/usermodel/Cell;)Ljava/lang/String;

    move-result-object v3

    .line 3348
    goto :goto_0

    .line 3349
    .end local v0    # "F":Lorg/apache/poi/ss/usermodel/DataFormatter;
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getNumericCellStringVal(Lorg/apache/poi/hssf/usermodel/HSSFCell;)Ljava/lang/String;

    move-result-object v3

    .line 3351
    goto :goto_0

    .line 3354
    :pswitch_2
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getRichStringCellValue()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v2

    .line 3355
    .local v2, "s":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getString()Ljava/lang/String;

    move-result-object v3

    .line 3356
    goto :goto_0

    .line 3359
    .end local v2    # "s":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    :pswitch_3
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getBooleanCellValue()Z

    move-result v1

    .line 3360
    .local v1, "cellNumBoolean":Z
    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    .line 3361
    goto :goto_0

    .line 3342
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getChartDimensions(Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/record/EscherAggregate;)V
    .locals 6
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p2, "escherAggregate"    # Lorg/apache/poi/hssf/record/EscherAggregate;

    .prologue
    .line 1611
    const/4 v2, 0x0

    .line 1614
    .local v2, "escherContainer":Lorg/apache/poi/ddf/EscherContainerRecord;
    if-eqz p2, :cond_1

    .line 1615
    invoke-virtual {p2}, Lorg/apache/poi/hssf/record/EscherAggregate;->getEscherContainer()Lorg/apache/poi/ddf/EscherContainerRecord;

    move-result-object v2

    .line 1617
    const/4 v0, 0x0

    .line 1618
    .local v0, "count":I
    invoke-virtual {p2}, Lorg/apache/poi/hssf/record/EscherAggregate;->getEscherRecords()Ljava/util/List;

    move-result-object v3

    .line 1621
    .local v3, "escherRecords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 1622
    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1623
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/poi/ddf/EscherRecord;

    .line 1624
    .local v1, "er":Lorg/apache/poi/ddf/EscherRecord;
    instance-of v5, v1, Lorg/apache/poi/ddf/EscherContainerRecord;

    if-eqz v5, :cond_0

    move-object v2, v1

    .line 1625
    check-cast v2, Lorg/apache/poi/ddf/EscherContainerRecord;

    .line 1626
    const/4 v5, 0x1

    invoke-direct {p0, v2, v5, v0}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->iterateContainer(Lorg/apache/poi/ddf/EscherContainerRecord;II)V

    .line 1627
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1632
    .end local v0    # "count":I
    .end local v1    # "er":Lorg/apache/poi/ddf/EscherRecord;
    .end local v3    # "escherRecords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_1
    return-void
.end method

.method private getColumnWidth(I)I
    .locals 4
    .param p1, "columnNum"    # I

    .prologue
    const/16 v1, 0x46

    .line 1635
    if-nez p1, :cond_1

    .line 1636
    const/16 v1, 0x16

    .line 1645
    :cond_0
    :goto_0
    return v1

    .line 1637
    :cond_1
    const/4 v2, 0x1

    if-eq v2, p1, :cond_0

    .line 1640
    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->colmnWdthChartsPicts:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1642
    .local v0, "widthvalue":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1645
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method private getCustomView()Lcom/samsung/thumbnail/customview/word/CustomView;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCustomView:Lcom/samsung/thumbnail/customview/word/CustomView;

    return-object v0
.end method

.method private getEscherRecordList(Lorg/apache/poi/ddf/EscherContainerRecord;I)Ljava/util/List;
    .locals 8
    .param p1, "records"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "recordId"    # I

    .prologue
    .line 1578
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1580
    .local v6, "list":Ljava/util/List;
    invoke-virtual {p1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildIterator()Ljava/util/Iterator;

    move-result-object v5

    .line 1581
    .local v5, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1582
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/ddf/EscherRecord;

    .line 1583
    .local v2, "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v7

    if-ne v7, p2, :cond_1

    .line 1584
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1586
    :cond_1
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherRecord;->isContainerRecord()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1587
    invoke-virtual {v2}, Lorg/apache/poi/ddf/EscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1588
    .local v1, "childIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1589
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ddf/EscherRecord;

    .line 1590
    .local v0, "childEscherRecord":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v7

    if-ne v7, p2, :cond_3

    .line 1591
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1593
    :cond_3
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherRecord;->isContainerRecord()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1594
    invoke-virtual {v0}, Lorg/apache/poi/ddf/EscherRecord;->getChildRecords()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 1595
    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_4
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1596
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/ddf/EscherRecord;

    .line 1597
    .local v3, "found":Lorg/apache/poi/ddf/EscherRecord;
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherRecord;->getRecordId()S

    move-result v7

    if-ne v7, p2, :cond_4

    .line 1598
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1605
    .end local v0    # "childEscherRecord":Lorg/apache/poi/ddf/EscherRecord;
    .end local v1    # "childIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    .end local v2    # "escherRecord":Lorg/apache/poi/ddf/EscherRecord;
    .end local v3    # "found":Lorg/apache/poi/ddf/EscherRecord;
    .end local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ddf/EscherRecord;>;"
    :cond_5
    return-object v6
.end method

.method private getFontColor(Ljava/lang/String;)I
    .locals 11
    .param p1, "hexString"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x6

    const/16 v9, 0x30

    const/16 v8, 0x3a

    const/4 v7, 0x7

    const/4 v6, 0x4

    .line 2431
    const/4 v4, 0x0

    .line 2433
    .local v4, "rgbVal":I
    const-string/jumbo v3, ""

    .line 2434
    .local v3, "rVal":Ljava/lang/String;
    const-string/jumbo v2, ""

    .line 2435
    .local v2, "gVal":Ljava/lang/String;
    const-string/jumbo v0, ""

    .line 2437
    .local v0, "bVal":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v8, :cond_4

    .line 2438
    const-string/jumbo v3, "00"

    .line 2439
    const/4 v5, 0x3

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v8, :cond_2

    .line 2440
    const-string/jumbo v2, "00"

    .line 2441
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v9, :cond_1

    .line 2442
    const-string/jumbo v0, "00"

    .line 2473
    :cond_0
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2475
    .local v1, "colVal":Ljava/lang/String;
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    .line 2477
    return v4

    .line 2444
    .end local v1    # "colVal":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2445
    :cond_2
    invoke-virtual {p1, v10}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v8, :cond_0

    .line 2446
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 2447
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v9, :cond_3

    .line 2448
    const-string/jumbo v0, "00"

    goto :goto_0

    .line 2450
    :cond_3
    const/16 v5, 0x9

    invoke-virtual {p1, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2454
    :cond_4
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v8, :cond_0

    .line 2455
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "8000"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2456
    const-string/jumbo v3, "7F"

    .line 2459
    :goto_1
    invoke-virtual {p1, v10}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v8, :cond_7

    .line 2460
    const-string/jumbo v2, "00"

    .line 2461
    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v9, :cond_6

    .line 2462
    const-string/jumbo v0, "00"

    goto :goto_0

    .line 2458
    :cond_5
    const/4 v5, 0x0

    const/4 v6, 0x2

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 2464
    :cond_6
    const/16 v5, 0x9

    invoke-virtual {p1, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2466
    :cond_7
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 2467
    const/16 v5, 0xa

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v9, :cond_8

    .line 2468
    const-string/jumbo v0, "00"

    goto/16 :goto_0

    .line 2470
    :cond_8
    const/16 v5, 0xa

    const/16 v6, 0xc

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private getNumericCellStringVal(Lorg/apache/poi/hssf/usermodel/HSSFCell;)Ljava/lang/String;
    .locals 20
    .param p1, "cell"    # Lorg/apache/poi/hssf/usermodel/HSSFCell;

    .prologue
    .line 2508
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getNumericCellValue()D

    move-result-wide v2

    .line 2509
    .local v2, "d":D
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v14

    .line 2511
    .local v14, "value":Ljava/lang/String;
    new-instance v9, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;

    invoke-direct {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;-><init>()V

    .line 2512
    .local v9, "formatind":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v15

    invoke-virtual {v15}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getDataFormat()S

    move-result v7

    .line 2513
    .local v7, "formatIndex":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v15

    invoke-virtual {v15}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getDataFormatString()Ljava/lang/String;

    move-result-object v8

    .line 2514
    .local v8, "formatStr":Ljava/lang/String;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    if-nez v15, :cond_1

    .line 2515
    :cond_0
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v15}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFFormatIndex;->getFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2517
    :cond_1
    if-nez v7, :cond_3

    .line 2523
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->setCellType(I)V

    .line 2524
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getStringCellValue()Ljava/lang/String;

    move-result-object v14

    .line 2575
    :cond_2
    :goto_0
    return-object v14

    .line 2527
    :cond_3
    if-eqz v8, :cond_2

    .line 2528
    const-string/jumbo v15, "0%"

    invoke-virtual {v8, v15}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v15

    if-nez v15, :cond_4

    .line 2531
    const/4 v13, 0x0

    .line 2533
    .local v13, "val":I
    invoke-static {v14}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v16

    const-wide/high16 v18, 0x4059000000000000L    # 100.0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-int v13, v0

    .line 2534
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, "%"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 2536
    goto :goto_0

    .end local v13    # "val":I
    :cond_4
    const-string/jumbo v15, "$"

    invoke-virtual {v8, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_5

    const-string/jumbo v15, "0"

    invoke-virtual {v8, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_5

    const-string/jumbo v15, "#"

    invoke-virtual {v8, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 2540
    :cond_5
    sget-object v15, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->formatCache:Ljava/util/Map;

    invoke-interface {v15, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/poi/ss/format/CellFormat;

    .line 2543
    .local v6, "fmt":Lorg/apache/poi/ss/format/CellFormat;
    if-nez v6, :cond_7

    .line 2544
    const-string/jumbo v15, "General"

    invoke-virtual {v8, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_6

    const-string/jumbo v15, "@"

    invoke-virtual {v8, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 2546
    :cond_6
    sget-object v6, Lorg/apache/poi/ss/format/CellFormat;->GENERAL_FORMAT:Lorg/apache/poi/ss/format/CellFormat;

    .line 2549
    :goto_1
    sget-object v15, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->formatCache:Ljava/util/Map;

    invoke-interface {v15, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2552
    :cond_7
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v15

    invoke-virtual {v6, v15}, Lorg/apache/poi/ss/format/CellFormat;->apply(Ljava/lang/Object;)Lorg/apache/poi/ss/format/CellFormatResult;

    move-result-object v11

    .line 2553
    .local v11, "result1":Lorg/apache/poi/ss/format/CellFormatResult;
    iget-object v14, v11, Lorg/apache/poi/ss/format/CellFormatResult;->text:Ljava/lang/String;

    .line 2555
    const-string/jumbo v15, "?"

    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_8

    const-string/jumbo v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 2560
    :cond_8
    const-string/jumbo v15, "?"

    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 2561
    const/16 v15, 0x3f

    invoke-virtual {v14, v15}, Ljava/lang/String;->indexOf(I)I

    move-result v10

    .line 2562
    .local v10, "index":I
    const/4 v15, 0x0

    invoke-virtual {v14, v15, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 2563
    .local v12, "temp":Ljava/lang/String;
    if-lez v10, :cond_2

    if-eqz v12, :cond_2

    .line 2564
    move-object v14, v12

    goto/16 :goto_0

    .line 2548
    .end local v10    # "index":I
    .end local v11    # "result1":Lorg/apache/poi/ss/format/CellFormatResult;
    .end local v12    # "temp":Ljava/lang/String;
    :cond_9
    new-instance v6, Lorg/apache/poi/ss/format/CellFormat;

    .end local v6    # "fmt":Lorg/apache/poi/ss/format/CellFormat;
    invoke-direct {v6, v8}, Lorg/apache/poi/ss/format/CellFormat;-><init>(Ljava/lang/String;)V

    .restart local v6    # "fmt":Lorg/apache/poi/ss/format/CellFormat;
    goto :goto_1

    .line 2570
    .end local v6    # "fmt":Lorg/apache/poi/ss/format/CellFormat;
    :cond_a
    const-wide/high16 v16, 0x4059000000000000L    # 100.0

    mul-double v16, v16, v2

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->round(D)J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v16, v0

    const-wide/high16 v18, 0x4059000000000000L    # 100.0

    div-double v4, v16, v18

    .line 2571
    .local v4, "finalValue":D
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_0
.end method

.method private getPictures(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFSheet;)V
    .locals 55
    .param p1, "wb"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p2, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .prologue
    .line 1393
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getAllPictures()Ljava/util/List;

    move-result-object v41

    .line 1394
    .local v41, "lst":Ljava/util/List;, "Ljava/util/List<Lorg/apache/poi/hssf/usermodel/HSSFPictureData;>;"
    const/16 v50, 0x0

    .local v50, "sheetPicStartIndex":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    invoke-virtual {v10}, Ljava/util/LinkedHashMap;->size()I

    move-result v49

    .line 1396
    .local v49, "sheetPicEndIndex":I
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 1397
    .local v23, "allPicName":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v52, Ljava/util/ArrayList;

    invoke-direct/range {v52 .. v52}, Ljava/util/ArrayList;-><init>()V

    .line 1399
    .local v52, "uniquePicName":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    invoke-virtual {v10}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v40

    .line 1400
    .local v40, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {v40 .. v40}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v39

    .line 1401
    .local v39, "keyIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    invoke-interface/range {v40 .. v40}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v38

    .line 1403
    .local v38, "keyIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    move/from16 v36, v50

    .local v36, "k":I
    :goto_0
    move/from16 v0, v36

    move/from16 v1, v49

    if-ge v0, v1, :cond_7

    .line 1404
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1405
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    .line 1406
    .local v22, "Key":Ljava/lang/String;
    const/16 v34, 0x1

    .line 1408
    .local v34, "isFirst":Z
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getPictureFilename()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 1409
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getPictureFilename()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v23

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1413
    :goto_1
    if-nez v36, :cond_3

    .line 1414
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getPictureFilename()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_2

    .line 1415
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getPictureFilename()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v52

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1403
    .end local v22    # "Key":Ljava/lang/String;
    .end local v34    # "isFirst":Z
    :cond_0
    :goto_2
    add-int/lit8 v36, v36, 0x1

    goto :goto_0

    .line 1411
    .restart local v22    # "Key":Ljava/lang/String;
    .restart local v34    # "isFirst":Z
    :cond_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "unnamed_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v36

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v23

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1417
    :cond_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "unnamed_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v36

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v52

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1420
    :cond_3
    const/16 v33, 0x0

    .local v33, "i":I
    :goto_3
    move/from16 v0, v33

    move/from16 v1, v36

    if-ge v0, v1, :cond_4

    .line 1421
    move-object/from16 v0, v23

    move/from16 v1, v36

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object/from16 v0, v23

    move/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1422
    const/16 v34, 0x0

    .line 1423
    if-nez v34, :cond_5

    .line 1428
    :cond_4
    if-eqz v34, :cond_0

    .line 1429
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getPictureFilename()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_6

    .line 1430
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getPictureFilename()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v52

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1420
    :cond_5
    add-int/lit8 v33, v33, 0x1

    goto :goto_3

    .line 1432
    :cond_6
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "unnamed_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v36

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v52

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1437
    .end local v22    # "Key":Ljava/lang/String;
    .end local v33    # "i":I
    .end local v34    # "isFirst":Z
    :cond_7
    move/from16 v33, v50

    .restart local v33    # "i":I
    :goto_4
    move/from16 v0, v33

    move/from16 v1, v49

    if-ge v0, v1, :cond_10

    .line 1439
    const/16 v17, 0x0

    .local v17, "x1value":I
    const/16 v18, 0x0

    .local v18, "y1value":I
    const/16 v19, 0x0

    .local v19, "widthvalue":I
    const/16 v20, 0x0

    .local v20, "heightvalue":I
    const/16 v53, 0x0

    .local v53, "x2value":I
    const/16 v54, 0x0

    .line 1440
    .local v54, "y2value":I
    const/16 v44, 0x0

    .local v44, "row1":I
    const/16 v25, 0x0

    .local v25, "column1":I
    const/16 v45, 0x0

    .local v45, "row2":I
    const/16 v26, 0x0

    .local v26, "column2":I
    const/16 v27, 0x0

    .local v27, "dx1":I
    const/16 v29, 0x0

    .local v29, "dy1":I
    const/16 v28, 0x0

    .local v28, "dx2":I
    const/16 v30, 0x0

    .line 1442
    .local v30, "dy2":I
    const/16 v51, 0x0

    .line 1443
    .local v51, "tempShape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    const/16 v48, 0x0

    .line 1446
    .local v48, "shapeRotationDegree":I
    :try_start_0
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_10

    .line 1447
    invoke-interface/range {v38 .. v38}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v37

    check-cast v37, Ljava/lang/String;

    .line 1448
    .local v37, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    move-object/from16 v0, v37

    invoke-virtual {v10, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    .line 1449
    .local v46, "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    move-object/from16 v51, v46

    .line 1451
    if-eqz v46, :cond_8

    .line 1452
    invoke-virtual/range {v46 .. v46}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getRotationAngle()I

    move-result v48

    .line 1454
    invoke-virtual/range {v46 .. v46}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getRow1()S

    move-result v44

    .line 1455
    invoke-virtual/range {v46 .. v46}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getCol1()S

    move-result v25

    .line 1456
    invoke-virtual/range {v46 .. v46}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getRow2()S

    move-result v45

    .line 1457
    invoke-virtual/range {v46 .. v46}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getCol2()S

    move-result v26

    .line 1458
    invoke-virtual/range {v46 .. v46}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getOffsetCol1()S

    move-result v27

    .line 1459
    invoke-virtual/range {v46 .. v46}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getOffsetRow1()S

    move-result v29

    .line 1460
    invoke-virtual/range {v46 .. v46}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getOffsetCol2()S

    move-result v28

    .line 1461
    invoke-virtual/range {v46 .. v46}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getOffsetRow2()S

    move-result v30

    .line 1464
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v10, :cond_a

    .line 1465
    const/16 v10, 0x14

    move/from16 v0, v44

    if-ge v0, v10, :cond_9

    const/16 v10, 0x10

    move/from16 v0, v25

    if-lt v0, v10, :cond_a

    .line 1437
    .end local v37    # "key":Ljava/lang/String;
    .end local v46    # "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    :cond_9
    :goto_5
    add-int/lit8 v33, v33, 0x1

    goto :goto_4

    .line 1473
    :catch_0
    move-exception v31

    .line 1474
    .local v31, "ex":Ljava/lang/Exception;
    const-string/jumbo v10, "XlsCanvasView"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "Exception: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1477
    .end local v31    # "ex":Ljava/lang/Exception;
    :cond_a
    const/16 v21, 0x0

    .line 1478
    .local v21, "Index":I
    const/16 v42, 0x0

    .line 1479
    .local v42, "picture":Lorg/apache/poi/hssf/usermodel/HSSFPictureData;
    const/4 v4, 0x0

    .line 1480
    .local v4, "byteArray":[B
    const/4 v9, 0x0

    .line 1482
    .local v9, "bitmap":Landroid/graphics/Bitmap;
    const/16 v47, 0x0

    .line 1483
    .local v47, "shapeName":Ljava/lang/String;
    if-eqz v51, :cond_e

    invoke-virtual/range {v51 .. v51}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getPictureFilename()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_e

    .line 1484
    invoke-virtual/range {v51 .. v51}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getPictureFilename()Ljava/lang/String;

    move-result-object v47

    .line 1489
    :goto_6
    const/16 v35, 0x0

    .local v35, "j":I
    :goto_7
    invoke-interface/range {v52 .. v52}, Ljava/util/List;->size()I

    move-result v10

    move/from16 v0, v35

    if-ge v0, v10, :cond_b

    .line 1490
    move-object/from16 v0, v52

    move/from16 v1, v35

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object/from16 v0, v47

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 1491
    move/from16 v21, v35

    .line 1496
    :cond_b
    move-object/from16 v0, v41

    move/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_c

    .line 1497
    move-object/from16 v0, v41

    move/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v42

    .end local v42    # "picture":Lorg/apache/poi/hssf/usermodel/HSSFPictureData;
    check-cast v42, Lorg/apache/poi/hssf/usermodel/HSSFPictureData;

    .line 1498
    .restart local v42    # "picture":Lorg/apache/poi/hssf/usermodel/HSSFPictureData;
    invoke-virtual/range {v42 .. v42}, Lorg/apache/poi/hssf/usermodel/HSSFPictureData;->getData()[B

    move-result-object v4

    .line 1501
    :cond_c
    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getXvalue(II)I

    move-result v17

    .line 1502
    move-object/from16 v0, p0

    move/from16 v1, v44

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getYvalue(II)I

    move-result v18

    .line 1503
    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getXvalue(II)I

    move-result v53

    .line 1504
    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getYvalue(II)I

    move-result v54

    .line 1506
    sub-int v19, v53, v17

    .line 1507
    sub-int v20, v54, v18

    .line 1509
    if-eqz v4, :cond_d

    .line 1510
    invoke-virtual/range {v42 .. v42}, Lorg/apache/poi/hssf/usermodel/HSSFPictureData;->getFormat()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 1555
    const/4 v10, 0x0

    array-length v11, v4

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v4, v10, v11, v0, v1}, Lcom/samsung/thumbnail/util/Utils;->decodeSampledBitmapFromByteArray([BIIII)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1561
    :cond_d
    :goto_8
    if-eqz v9, :cond_9

    .line 1562
    new-instance v15, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    move-object/from16 v16, v9

    invoke-direct/range {v15 .. v20}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;-><init>(Landroid/graphics/Bitmap;IIII)V

    .line 1564
    .local v15, "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    sget-object v10, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v15, v10}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 1565
    move/from16 v0, v48

    invoke-virtual {v15, v0}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setRotationAngle(I)V

    .line 1566
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v10, v15}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_5

    .line 1486
    .end local v15    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v35    # "j":I
    :cond_e
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "unnamed_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static/range {v33 .. v33}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v47

    goto/16 :goto_6

    .line 1489
    .restart local v35    # "j":I
    :cond_f
    add-int/lit8 v35, v35, 0x1

    goto/16 :goto_7

    .line 1514
    :pswitch_0
    new-instance v43, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;

    move/from16 v0, v19

    int-to-float v10, v0

    move/from16 v0, v20

    int-to-float v11, v0

    move-object/from16 v0, v43

    invoke-direct {v0, v4, v10, v11}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;-><init>([BFF)V

    .line 1516
    .local v43, "readEMFFile":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawImage()Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1517
    goto :goto_8

    .line 1522
    .end local v43    # "readEMFFile":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;
    :pswitch_1
    const/16 v24, 0x0

    .line 1523
    .local v24, "blipRecord":Lorg/apache/poi/ddf/EscherMetafileBlip;
    invoke-virtual/range {v42 .. v42}, Lorg/apache/poi/hssf/usermodel/HSSFPictureData;->getBlipRecord()Lorg/apache/poi/ddf/EscherBlipRecord;

    move-result-object v24

    .end local v24    # "blipRecord":Lorg/apache/poi/ddf/EscherMetafileBlip;
    check-cast v24, Lorg/apache/poi/ddf/EscherMetafileBlip;

    .line 1525
    .restart local v24    # "blipRecord":Lorg/apache/poi/ddf/EscherMetafileBlip;
    if-eqz v24, :cond_d

    .line 1527
    const/4 v5, 0x0

    .local v5, "left":I
    const/4 v6, 0x0

    .local v6, "right":I
    const/4 v7, 0x0

    .local v7, "top":I
    const/4 v8, 0x0

    .line 1529
    .local v8, "bottom":I
    invoke-virtual/range {v24 .. v24}, Lorg/apache/poi/ddf/EscherMetafileBlip;->getLeft()I

    move-result v5

    .line 1530
    invoke-virtual/range {v24 .. v24}, Lorg/apache/poi/ddf/EscherMetafileBlip;->getRight()I

    move-result v6

    .line 1531
    invoke-virtual/range {v24 .. v24}, Lorg/apache/poi/ddf/EscherMetafileBlip;->getTop()I

    move-result v7

    .line 1532
    invoke-virtual/range {v24 .. v24}, Lorg/apache/poi/ddf/EscherMetafileBlip;->getBottom()I

    move-result v8

    .line 1534
    new-instance v3, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;

    invoke-direct/range {v3 .. v8}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;-><init>([BIIII)V

    .line 1536
    .local v3, "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    sub-int v10, v8, v7

    int-to-float v10, v10

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->setActHeight(F)V

    .line 1537
    sub-int v10, v6, v5

    int-to-float v10, v10

    invoke-virtual {v3, v10}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->setActWidth(F)V

    .line 1538
    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawImageDoc()Landroid/graphics/Bitmap;

    move-result-object v9

    .line 1540
    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->flipTheImage()Z

    move-result v10

    if-eqz v10, :cond_d

    .line 1541
    new-instance v14, Landroid/graphics/Matrix;

    invoke-direct {v14}, Landroid/graphics/Matrix;-><init>()V

    .line 1542
    .local v14, "matrix":Landroid/graphics/Matrix;
    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v11, -0x40800000    # -1.0f

    invoke-virtual {v14, v10, v11}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 1543
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    const/4 v15, 0x1

    invoke-static/range {v9 .. v15}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v32

    .line 1546
    .local v32, "flippedBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v9, v32

    goto/16 :goto_8

    .line 1569
    .end local v3    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    .end local v4    # "byteArray":[B
    .end local v5    # "left":I
    .end local v6    # "right":I
    .end local v7    # "top":I
    .end local v8    # "bottom":I
    .end local v9    # "bitmap":Landroid/graphics/Bitmap;
    .end local v14    # "matrix":Landroid/graphics/Matrix;
    .end local v17    # "x1value":I
    .end local v18    # "y1value":I
    .end local v19    # "widthvalue":I
    .end local v20    # "heightvalue":I
    .end local v21    # "Index":I
    .end local v24    # "blipRecord":Lorg/apache/poi/ddf/EscherMetafileBlip;
    .end local v25    # "column1":I
    .end local v26    # "column2":I
    .end local v27    # "dx1":I
    .end local v28    # "dx2":I
    .end local v29    # "dy1":I
    .end local v30    # "dy2":I
    .end local v32    # "flippedBitmap":Landroid/graphics/Bitmap;
    .end local v35    # "j":I
    .end local v42    # "picture":Lorg/apache/poi/hssf/usermodel/HSSFPictureData;
    .end local v44    # "row1":I
    .end local v45    # "row2":I
    .end local v47    # "shapeName":Ljava/lang/String;
    .end local v48    # "shapeRotationDegree":I
    .end local v51    # "tempShape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    .end local v53    # "x2value":I
    .end local v54    # "y2value":I
    :cond_10
    return-void

    .line 1510
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getXvalue(II)I
    .locals 3
    .param p1, "columnNum"    # I
    .param p2, "dXval"    # I

    .prologue
    .line 1652
    const/4 v1, 0x0

    .line 1653
    .local v1, "xValue":I
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    if-gt v0, p1, :cond_0

    .line 1654
    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getColumnWidth(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 1653
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1658
    :cond_0
    mul-int/lit8 v2, p2, 0x46

    div-int/lit16 v2, v2, 0x400

    add-int/2addr v1, v2

    .line 1661
    return v1
.end method

.method private getYvalue(II)I
    .locals 2
    .param p1, "rowNum"    # I
    .param p2, "dYval"    # I

    .prologue
    .line 1687
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getRowList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getRowPosition(ILjava/util/ArrayList;)F

    move-result v1

    float-to-int v0, v1

    .line 1689
    .local v0, "yValue":I
    mul-int/lit8 v1, p2, 0x14

    div-int/lit16 v1, v1, 0x100

    add-int/2addr v0, v1

    .line 1691
    return v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 166
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;-><init>()V

    .line 167
    .local v0, "slideDimention":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    new-instance v1, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v1, v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;-><init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 168
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->colmnWdthChartsPicts:Ljava/util/Map;

    .line 169
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->escherOptRecordList:Ljava/util/List;

    .line 170
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->escherSpRecordList:Ljava/util/List;

    .line 171
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->escherClientAnchorList:Ljava/util/List;

    .line 173
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    .line 174
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    .line 176
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mTopBorderCellNum:Ljava/util/ArrayList;

    .line 177
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mTopBorderRowNum:Ljava/util/ArrayList;

    .line 178
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mNextCellTopBorder:Ljava/util/ArrayList;

    .line 179
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mLeftBorderCellNum:I

    .line 181
    return-void
.end method

.method private iterateContainer(Lorg/apache/poi/ddf/EscherContainerRecord;II)V
    .locals 18
    .param p1, "escherContainer"    # Lorg/apache/poi/ddf/EscherContainerRecord;
    .param p2, "level"    # I
    .param p3, "higherlevel"    # I

    .prologue
    .line 2759
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/ddf/EscherContainerRecord;->getChildRecords()Ljava/util/List;

    move-result-object v4

    .line 2760
    .local v4, "childRecords":Ljava/util/List;
    const/4 v9, 0x0

    .line 2761
    .local v9, "listIterator":Ljava/util/Iterator;
    const/4 v14, 0x0

    .line 2762
    .local v14, "spRecord":Lorg/apache/poi/ddf/EscherSpRecord;
    const/4 v11, 0x0

    .line 2763
    .local v11, "optRecord":Lorg/apache/poi/ddf/EscherOptRecord;
    const/4 v3, 0x0

    .line 2764
    .local v3, "anchrRecord":Lorg/apache/poi/ddf/EscherClientAnchorRecord;
    const/4 v10, 0x0

    .line 2767
    .local v10, "next":Ljava/lang/Object;
    const/4 v5, 0x0

    .line 2768
    .local v5, "isChartInfo":Z
    const/4 v7, 0x0

    .line 2769
    .local v7, "isPictureInfo":Z
    const/4 v15, 0x0

    .line 2770
    .local v15, "tempShapeId":I
    const/4 v6, 0x0

    .line 2773
    .local v6, "isChartInfoExist":Z
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 2774
    .end local v10    # "next":Ljava/lang/Object;
    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_e

    .line 2775
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    .line 2776
    .restart local v10    # "next":Ljava/lang/Object;
    instance-of v0, v10, Lorg/apache/poi/ddf/EscherContainerRecord;

    move/from16 v16, v0

    if-eqz v16, :cond_1

    .line 2777
    check-cast v10, Lorg/apache/poi/ddf/EscherContainerRecord;

    .end local v10    # "next":Ljava/lang/Object;
    add-int/lit8 p2, p2, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v10, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->iterateContainer(Lorg/apache/poi/ddf/EscherContainerRecord;II)V

    goto :goto_0

    .line 2783
    .restart local v10    # "next":Ljava/lang/Object;
    :cond_1
    instance-of v0, v10, Lorg/apache/poi/ddf/EscherSpRecord;

    move/from16 v16, v0

    if-eqz v16, :cond_4

    move-object v14, v10

    .line 2784
    check-cast v14, Lorg/apache/poi/ddf/EscherSpRecord;

    .line 2785
    mul-int/lit8 v16, p3, 0xa

    add-int v16, v16, p2

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    .line 2786
    .local v8, "key":Ljava/lang/String;
    invoke-virtual {v14}, Lorg/apache/poi/ddf/EscherSpRecord;->getOptions()S

    move-result v16

    shr-int/lit8 v16, v16, 0x4

    invoke-static/range {v16 .. v16}, Lorg/apache/poi/hslf/model/ShapeTypes;->typeName(I)Ljava/lang/String;

    move-result-object v13

    .line 2792
    .local v13, "shapename":Ljava/lang/String;
    const-string/jumbo v16, "HostControl"

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 2799
    invoke-virtual {v14}, Lorg/apache/poi/ddf/EscherSpRecord;->getShapeId()I

    move-result v15

    .line 2801
    const/4 v5, 0x1

    .line 2803
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->wetherChartAlreadyExist(I)Z

    move-result v6

    .line 2805
    if-nez v6, :cond_0

    .line 2806
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 2807
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->setSpRecord(Lorg/apache/poi/ddf/EscherSpRecord;)V

    goto :goto_0

    .line 2809
    :cond_2
    new-instance v12, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    move/from16 v0, p2

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;-><init>(I)V

    .line 2811
    .local v12, "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    invoke-virtual {v12, v14}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->setSpRecord(Lorg/apache/poi/ddf/EscherSpRecord;)V

    .line 2812
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8, v12}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 2817
    .end local v12    # "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    :cond_3
    const-string/jumbo v16, "PictureFrame"

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 2818
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 2822
    .end local v8    # "key":Ljava/lang/String;
    .end local v13    # "shapename":Ljava/lang/String;
    :cond_4
    instance-of v0, v10, Lorg/apache/poi/ddf/EscherOptRecord;

    move/from16 v16, v0

    if-eqz v16, :cond_8

    move-object v11, v10

    .line 2823
    check-cast v11, Lorg/apache/poi/ddf/EscherOptRecord;

    .line 2824
    mul-int/lit8 v16, p3, 0xa

    add-int v16, v16, p2

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    .line 2826
    .restart local v8    # "key":Ljava/lang/String;
    if-eqz v7, :cond_5

    .line 2827
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 2828
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->setOptRecord(Lorg/apache/poi/ddf/EscherOptRecord;)V

    .line 2837
    :cond_5
    :goto_1
    if-eqz v5, :cond_0

    if-nez v6, :cond_0

    .line 2838
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_7

    .line 2839
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->setOptRecord(Lorg/apache/poi/ddf/EscherOptRecord;)V

    goto/16 :goto_0

    .line 2830
    :cond_6
    new-instance v12, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    move/from16 v0, p2

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;-><init>(I)V

    .line 2831
    .restart local v12    # "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    invoke-virtual {v12, v11}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->setOptRecord(Lorg/apache/poi/ddf/EscherOptRecord;)V

    .line 2832
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8, v12}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2841
    .end local v12    # "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    :cond_7
    new-instance v12, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    move/from16 v0, p2

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;-><init>(I)V

    .line 2842
    .restart local v12    # "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    invoke-virtual {v12, v11}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->setOptRecord(Lorg/apache/poi/ddf/EscherOptRecord;)V

    .line 2843
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8, v12}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 2848
    .end local v8    # "key":Ljava/lang/String;
    .end local v12    # "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    :cond_8
    instance-of v0, v10, Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    move/from16 v16, v0

    if-eqz v16, :cond_0

    move-object v3, v10

    .line 2849
    check-cast v3, Lorg/apache/poi/ddf/EscherClientAnchorRecord;

    .line 2850
    mul-int/lit8 v16, p3, 0xa

    add-int v16, v16, p2

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    .line 2853
    .restart local v8    # "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    move/from16 v16, v0

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol2()S

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_9

    .line 2854
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getCol2()S

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    .line 2857
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxRowinTable:I

    move/from16 v16, v0

    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow2()S

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_a

    .line 2858
    invoke-virtual {v3}, Lorg/apache/poi/ddf/EscherClientAnchorRecord;->getRow2()S

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxRowinTable:I

    .line 2862
    :cond_a
    if-eqz v7, :cond_b

    .line 2863
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_c

    .line 2864
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->setAnchorRecord(Lorg/apache/poi/ddf/EscherClientAnchorRecord;)V

    .line 2873
    :cond_b
    :goto_2
    if-eqz v5, :cond_0

    if-nez v6, :cond_0

    .line 2874
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_d

    .line 2875
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->setAnchorRecord(Lorg/apache/poi/ddf/EscherClientAnchorRecord;)V

    goto/16 :goto_0

    .line 2866
    :cond_c
    new-instance v12, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    move/from16 v0, p2

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;-><init>(I)V

    .line 2867
    .restart local v12    # "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    invoke-virtual {v12, v3}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->setAnchorRecord(Lorg/apache/poi/ddf/EscherClientAnchorRecord;)V

    .line 2868
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->shapes:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8, v12}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2877
    .end local v12    # "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    :cond_d
    new-instance v12, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    move/from16 v0, p2

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;-><init>(I)V

    .line 2878
    .restart local v12    # "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    invoke-virtual {v12, v3}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->setAnchorRecord(Lorg/apache/poi/ddf/EscherClientAnchorRecord;)V

    .line 2879
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v8, v12}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 2892
    .end local v8    # "key":Ljava/lang/String;
    .end local v10    # "next":Ljava/lang/Object;
    .end local v12    # "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    :cond_e
    return-void
.end method

.method private mergeRow(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)V
    .locals 11
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .prologue
    const/16 v10, 0x14

    const/16 v9, 0x10

    .line 906
    const/4 v6, 0x0

    .line 907
    .local v6, "merged":Lorg/apache/poi/ss/util/CellRangeAddress;
    invoke-virtual {p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getNumMergedRegions()I

    move-result v7

    .line 909
    .local v7, "numMerged":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v7, :cond_6

    .line 910
    invoke-virtual {p1, v2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getMergedRegion(I)Lorg/apache/poi/ss/util/CellRangeAddress;

    move-result-object v6

    .line 912
    const/4 v1, 0x1

    .line 913
    .local v1, "firstRow":I
    const/4 v4, 0x1

    .line 914
    .local v4, "lastRow":I
    const/4 v0, 0x1

    .line 915
    .local v0, "firstColumn":I
    const/4 v3, 0x1

    .line 918
    .local v3, "lastColumn":I
    if-eqz v6, :cond_0

    .line 919
    invoke-virtual {v6}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstRow()I

    move-result v8

    add-int/lit8 v1, v8, 0x1

    .line 920
    invoke-virtual {v6}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastRow()I

    move-result v8

    add-int/lit8 v4, v8, 0x1

    .line 921
    invoke-virtual {v6}, Lorg/apache/poi/ss/util/CellRangeAddress;->getFirstColumn()I

    move-result v8

    add-int/lit8 v0, v8, 0x1

    .line 922
    invoke-virtual {v6}, Lorg/apache/poi/ss/util/CellRangeAddress;->getLastColumn()I

    move-result v8

    add-int/lit8 v3, v8, 0x1

    .line 926
    :cond_0
    iget-boolean v8, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    if-eqz v8, :cond_2

    .line 927
    if-lt v4, v10, :cond_1

    .line 928
    const/16 v4, 0x14

    .line 930
    :cond_1
    if-lt v3, v9, :cond_2

    .line 931
    const/16 v3, 0x10

    .line 935
    :cond_2
    const/4 v5, 0x0

    .line 936
    .local v5, "mergeCondition":Z
    iget-boolean v8, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    if-eqz v8, :cond_5

    .line 937
    if-ge v0, v9, :cond_3

    if-ge v1, v10, :cond_3

    .line 939
    const/4 v5, 0x1

    .line 941
    :cond_3
    if-eqz v5, :cond_4

    .line 942
    invoke-direct {p0, v1, v4, v0, v3}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->doMerges(IIII)V

    .line 909
    :cond_4
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 946
    :cond_5
    invoke-direct {p0, v1, v4, v0, v3}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->doMerges(IIII)V

    goto :goto_1

    .line 949
    .end local v0    # "firstColumn":I
    .end local v1    # "firstRow":I
    .end local v3    # "lastColumn":I
    .end local v4    # "lastRow":I
    .end local v5    # "mergeCondition":Z
    :cond_6
    return-void
.end method

.method private setChartValues(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)V
    .locals 48
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;

    .prologue
    .line 3013
    invoke-static/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFChart;->getSheetCharts(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[Lorg/apache/poi/hssf/usermodel/HSSFChart;

    move-result-object v13

    .line 3014
    .local v13, "chartList":[Lorg/apache/poi/hssf/usermodel/HSSFChart;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v27

    .line 3015
    .local v27, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {v27 .. v27}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v26

    .line 3017
    .local v26, "keyIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    if-eqz v13, :cond_19

    array-length v2, v13

    if-lez v2, :cond_19

    .line 3021
    const/16 v24, 0x0

    .local v24, "k":I
    :goto_0
    array-length v2, v13

    move/from16 v0, v24

    if-ge v0, v2, :cond_19

    .line 3022
    aget-object v36, v13, v24

    .line 3023
    .local v36, "tempHSSFChart":Lorg/apache/poi/hssf/usermodel/HSSFChart;
    new-instance v35, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;

    aget-object v2, v13, v24

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFChart;->getType()Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFChartType;

    move-result-object v2

    move-object/from16 v0, v35

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFChartType;)V

    .line 3026
    .local v35, "tempExcelChartInfo":Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;
    const/16 v23, 0x1

    .line 3030
    .local v23, "isSeriesInColumn":Z
    const/4 v12, 0x0

    .line 3033
    .local v12, "chartInThumbnail":Z
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFChart;->getSeries()[Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;

    move-result-object v29

    .line 3043
    .local v29, "series":[Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3044
    const/16 v43, 0x0

    .local v43, "x1value":I
    const/16 v46, 0x0

    .local v46, "y1value":I
    const/16 v41, 0x0

    .local v41, "widthvalue":I
    const/16 v21, 0x0

    .local v21, "heightvalue":I
    const/16 v44, 0x0

    .local v44, "x2value":I
    const/16 v47, 0x0

    .line 3045
    .local v47, "y2value":I
    const/4 v4, 0x0

    .local v4, "row1":I
    const/4 v6, 0x0

    .local v6, "column1":I
    const/4 v5, 0x0

    .local v5, "row2":I
    const/4 v7, 0x0

    .local v7, "column2":I
    const/16 v16, 0x0

    .local v16, "dx1":I
    const/16 v18, 0x0

    .local v18, "dy1":I
    const/16 v17, 0x0

    .local v17, "dx2":I
    const/16 v19, 0x0

    .line 3046
    .local v19, "dy2":I
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    .line 3047
    .local v25, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    .line 3048
    .local v30, "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getRow1()S

    move-result v4

    .line 3049
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getCol1()S

    move-result v6

    .line 3050
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getRow2()S

    move-result v5

    .line 3051
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getCol2()S

    move-result v7

    .line 3053
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isChartInThumbnail(ZIIII)Z

    move-result v12

    .line 3056
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getOffsetCol1()S

    move-result v16

    .line 3057
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getOffsetRow1()S

    move-result v18

    .line 3058
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getOffsetCol2()S

    move-result v17

    .line 3059
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getOffsetRow2()S

    move-result v19

    .line 3061
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v6, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getXvalue(II)I

    move-result v43

    .line 3062
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v4, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getYvalue(II)I

    move-result v46

    .line 3063
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v7, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getXvalue(II)I

    move-result v44

    .line 3064
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v5, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getYvalue(II)I

    move-result v47

    .line 3066
    sub-int v41, v44, v43

    .line 3067
    sub-int v21, v47, v46

    .line 3076
    move-object/from16 v0, v35

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->setHeight(I)V

    .line 3077
    move-object/from16 v0, v35

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->setWidth(I)V

    .line 3078
    move-object/from16 v0, v35

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->setX(I)V

    .line 3079
    move-object/from16 v0, v35

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->setY(I)V

    .line 3112
    .end local v4    # "row1":I
    .end local v5    # "row2":I
    .end local v6    # "column1":I
    .end local v7    # "column2":I
    .end local v16    # "dx1":I
    .end local v17    # "dx2":I
    .end local v18    # "dy1":I
    .end local v19    # "dy2":I
    .end local v21    # "heightvalue":I
    .end local v25    # "key":Ljava/lang/String;
    .end local v30    # "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    .end local v41    # "widthvalue":I
    .end local v43    # "x1value":I
    .end local v44    # "x2value":I
    .end local v46    # "y1value":I
    .end local v47    # "y2value":I
    :cond_0
    :goto_1
    if-eqz v12, :cond_18

    .line 3113
    if-eqz v29, :cond_18

    .line 3115
    const/4 v9, 0x0

    .line 3117
    .local v9, "categoryNum":I
    const/4 v2, 0x0

    aget-object v2, v29, v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->getCategoryLabelsCellRange()Lorg/apache/poi/ss/util/CellRangeAddressBase;

    move-result-object v31

    .line 3123
    .local v31, "tempCategoryLabelCellRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    if-eqz v31, :cond_2

    .line 3135
    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstColumn()I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastColumn()I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastRow()I

    move-result v2

    if-nez v2, :cond_1

    .line 3140
    const/4 v2, 0x0

    aget-object v2, v29, v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->getValuesCellRange()Lorg/apache/poi/ss/util/CellRangeAddressBase;

    move-result-object v33

    .line 3142
    .local v33, "tempCellValueRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    if-eqz v33, :cond_1

    .line 3143
    invoke-virtual/range {v33 .. v33}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v2

    invoke-virtual/range {v33 .. v33}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastRow()I

    move-result v3

    if-eq v2, v3, :cond_7

    .line 3152
    invoke-virtual/range {v33 .. v33}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastRow()I

    move-result v2

    invoke-virtual/range {v33 .. v33}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v9, v2, 0x1

    .line 3156
    const/16 v23, 0x0

    .line 3170
    .end local v33    # "tempCellValueRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    :cond_1
    :goto_2
    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v2

    invoke-virtual/range {v31 .. v31}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastRow()I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 3174
    const/16 v23, 0x0

    .line 3178
    :cond_2
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_3
    move-object/from16 v0, v29

    array-length v2, v0

    move/from16 v0, v22

    if-ge v0, v2, :cond_10

    .line 3181
    const-string/jumbo v39, ""

    .line 3182
    .local v39, "tempSeriesTitle":Ljava/lang/String;
    const-string/jumbo v11, ""

    .line 3183
    .local v11, "chartCellValue":Ljava/lang/String;
    aget-object v2, v29, v22

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->getValuesCellRange()Lorg/apache/poi/ss/util/CellRangeAddressBase;

    move-result-object v10

    .line 3186
    .local v10, "cellValueRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    if-eqz v10, :cond_3

    .line 3188
    const/16 v34, 0x0

    .line 3189
    .local v34, "tempCount":I
    const/4 v14, 0x0

    .line 3190
    .local v14, "chartValues":[Ljava/lang/String;
    invoke-virtual {v10}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v38

    .line 3192
    .local v38, "tempHssfRow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    if-nez v38, :cond_8

    .line 3178
    .end local v14    # "chartValues":[Ljava/lang/String;
    .end local v34    # "tempCount":I
    .end local v38    # "tempHssfRow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    :cond_3
    :goto_4
    add-int/lit8 v22, v22, 0x1

    goto :goto_3

    .line 3085
    .end local v9    # "categoryNum":I
    .end local v10    # "cellValueRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .end local v11    # "chartCellValue":Ljava/lang/String;
    .end local v22    # "i":I
    .end local v31    # "tempCategoryLabelCellRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .end local v39    # "tempSeriesTitle":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFChart;->getChartWidth()I

    move-result v40

    .line 3086
    .local v40, "width":I
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFChart;->getChartHeight()I

    move-result v20

    .line 3087
    .local v20, "height":I
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFChart;->getChartX()I

    move-result v42

    .line 3088
    .local v42, "x":I
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFChart;->getChartY()I

    move-result v45

    .line 3089
    .local v45, "y":I
    const v2, 0xafc8

    move/from16 v0, v40

    if-le v0, v2, :cond_0

    const v2, 0xafc8

    move/from16 v0, v20

    if-le v0, v2, :cond_0

    .line 3091
    const/4 v12, 0x1

    .line 3093
    const v2, 0xafc8

    div-int v40, v40, v2

    .line 3094
    const v2, 0xafc8

    div-int v20, v20, v2

    .line 3096
    if-gtz v42, :cond_5

    .line 3098
    const/16 v42, 0x14

    .line 3100
    :cond_5
    if-gtz v45, :cond_6

    .line 3102
    const/16 v45, 0x46

    .line 3105
    :cond_6
    move-object/from16 v0, v35

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->setHeight(I)V

    .line 3106
    move-object/from16 v0, v35

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->setWidth(I)V

    .line 3107
    move-object/from16 v0, v35

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->setX(I)V

    .line 3108
    move-object/from16 v0, v35

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->setY(I)V

    goto/16 :goto_1

    .line 3163
    .end local v20    # "height":I
    .end local v40    # "width":I
    .end local v42    # "x":I
    .end local v45    # "y":I
    .restart local v9    # "categoryNum":I
    .restart local v31    # "tempCategoryLabelCellRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .restart local v33    # "tempCellValueRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    :cond_7
    invoke-virtual/range {v33 .. v33}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastColumn()I

    move-result v2

    invoke-virtual/range {v33 .. v33}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstColumn()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v9, v2, 0x1

    goto/16 :goto_2

    .line 3194
    .end local v33    # "tempCellValueRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .restart local v10    # "cellValueRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .restart local v11    # "chartCellValue":Ljava/lang/String;
    .restart local v14    # "chartValues":[Ljava/lang/String;
    .restart local v22    # "i":I
    .restart local v34    # "tempCount":I
    .restart local v38    # "tempHssfRow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .restart local v39    # "tempSeriesTitle":Ljava/lang/String;
    :cond_8
    invoke-virtual {v10}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstColumn()I

    move-result v2

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getCell(I)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    move-result-object v37

    .line 3198
    .local v37, "tempHssfCell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    if-eqz v23, :cond_a

    .line 3201
    const/16 v34, 0x0

    .line 3202
    invoke-virtual {v10}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastColumn()I

    move-result v2

    invoke-virtual {v10}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstColumn()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    new-array v14, v2, [Ljava/lang/String;

    .line 3206
    invoke-virtual {v10}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstColumn()I

    move-result v15

    .line 3207
    .local v15, "columnNo":I
    :goto_5
    invoke-virtual {v10}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastColumn()I

    move-result v2

    if-gt v15, v2, :cond_d

    .line 3209
    move-object/from16 v0, v38

    invoke-virtual {v0, v15}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getCell(I)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    move-result-object v37

    .line 3212
    if-eqz v37, :cond_9

    .line 3213
    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getChartCellValue(Lorg/apache/poi/hssf/usermodel/HSSFCell;)Ljava/lang/String;

    move-result-object v11

    .line 3215
    :cond_9
    aput-object v11, v14, v34

    .line 3216
    add-int/lit8 v34, v34, 0x1

    .line 3208
    add-int/lit8 v15, v15, 0x1

    goto :goto_5

    .line 3221
    .end local v15    # "columnNo":I
    :cond_a
    const/16 v34, 0x0

    .line 3222
    invoke-virtual {v10}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastRow()I

    move-result v2

    invoke-virtual {v10}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    new-array v14, v2, [Ljava/lang/String;

    .line 3225
    invoke-virtual {v10}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v28

    .line 3226
    .local v28, "rowNo":I
    :goto_6
    invoke-virtual {v10}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastRow()I

    move-result v2

    move/from16 v0, v28

    if-gt v0, v2, :cond_d

    .line 3228
    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v38

    .line 3229
    if-nez v38, :cond_b

    .line 3227
    :goto_7
    add-int/lit8 v28, v28, 0x1

    goto :goto_6

    .line 3231
    :cond_b
    invoke-virtual {v10}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstColumn()I

    move-result v2

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getCell(I)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    move-result-object v37

    .line 3235
    if-eqz v37, :cond_c

    .line 3236
    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getChartCellValue(Lorg/apache/poi/hssf/usermodel/HSSFCell;)Ljava/lang/String;

    move-result-object v11

    .line 3238
    :cond_c
    aput-object v11, v14, v34

    .line 3239
    add-int/lit8 v34, v34, 0x1

    goto :goto_7

    .line 3243
    .end local v28    # "rowNo":I
    :cond_d
    if-eqz v14, :cond_3

    .line 3244
    move-object/from16 v0, v35

    iget-object v2, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartValues:Ljava/util/ArrayList;

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3246
    aget-object v2, v29, v22

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->getSeriesTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_e

    .line 3247
    move-object/from16 v0, v35

    iget-object v2, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    aget-object v3, v29, v22

    invoke-virtual {v3}, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->getSeriesTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3251
    :cond_e
    aget-object v2, v29, v22

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->getSeriesTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_f

    aget-object v2, v29, v22

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->getSeriesTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    .line 3254
    :cond_f
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Series"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v22, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    .line 3256
    move-object/from16 v0, v35

    iget-object v2, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartSeriesNames:Ljava/util/ArrayList;

    move-object/from16 v0, v39

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 3263
    .end local v10    # "cellValueRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .end local v11    # "chartCellValue":Ljava/lang/String;
    .end local v14    # "chartValues":[Ljava/lang/String;
    .end local v34    # "tempCount":I
    .end local v37    # "tempHssfCell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    .end local v38    # "tempHssfRow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .end local v39    # "tempSeriesTitle":Ljava/lang/String;
    :cond_10
    const/4 v2, 0x0

    aget-object v2, v29, v2

    invoke-virtual {v2}, Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;->getCategoryLabelsCellRange()Lorg/apache/poi/ss/util/CellRangeAddressBase;

    move-result-object v8

    .line 3265
    .local v8, "categoryLabelCellRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    if-eqz v8, :cond_18

    .line 3268
    const/4 v11, 0x0

    .line 3269
    .restart local v11    # "chartCellValue":Ljava/lang/String;
    const/16 v32, 0x0

    .line 3270
    .local v32, "tempCategoryLabelCellValue":I
    invoke-virtual {v8}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstColumn()I

    move-result v2

    if-nez v2, :cond_11

    invoke-virtual {v8}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastColumn()I

    move-result v2

    if-nez v2, :cond_11

    invoke-virtual {v8}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v2

    if-nez v2, :cond_11

    invoke-virtual {v8}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastRow()I

    move-result v2

    if-nez v2, :cond_11

    .line 3274
    const/16 v32, 0x0

    :goto_8
    move/from16 v0, v32

    if-ge v0, v9, :cond_18

    .line 3275
    move-object/from16 v0, v35

    iget-object v2, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    add-int/lit8 v3, v32, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3274
    add-int/lit8 v32, v32, 0x1

    goto :goto_8

    .line 3281
    :cond_11
    invoke-virtual {v8}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v38

    .line 3284
    .restart local v38    # "tempHssfRow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    if-nez v38, :cond_13

    .line 3021
    .end local v8    # "categoryLabelCellRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .end local v9    # "categoryNum":I
    .end local v11    # "chartCellValue":Ljava/lang/String;
    .end local v22    # "i":I
    .end local v31    # "tempCategoryLabelCellRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .end local v32    # "tempCategoryLabelCellValue":I
    .end local v38    # "tempHssfRow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    :cond_12
    :goto_9
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_0

    .line 3286
    .restart local v8    # "categoryLabelCellRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .restart local v9    # "categoryNum":I
    .restart local v11    # "chartCellValue":Ljava/lang/String;
    .restart local v22    # "i":I
    .restart local v31    # "tempCategoryLabelCellRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .restart local v32    # "tempCategoryLabelCellValue":I
    .restart local v38    # "tempHssfRow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    :cond_13
    const/16 v37, 0x0

    .line 3288
    .restart local v37    # "tempHssfCell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    if-nez v23, :cond_16

    .line 3290
    invoke-virtual {v8}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstRow()I

    move-result v28

    .line 3291
    .restart local v28    # "rowNo":I
    :goto_a
    invoke-virtual {v8}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastRow()I

    move-result v2

    move/from16 v0, v28

    if-gt v0, v2, :cond_18

    .line 3293
    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getRow(I)Lorg/apache/poi/hssf/usermodel/HSSFRow;

    move-result-object v38

    .line 3294
    if-nez v38, :cond_14

    .line 3292
    :goto_b
    add-int/lit8 v28, v28, 0x1

    goto :goto_a

    .line 3296
    :cond_14
    invoke-virtual {v8}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstColumn()I

    move-result v2

    move-object/from16 v0, v38

    invoke-virtual {v0, v2}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getCell(I)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    move-result-object v37

    .line 3300
    const-string/jumbo v11, ""

    .line 3301
    if-eqz v37, :cond_15

    .line 3302
    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getChartCellValue(Lorg/apache/poi/hssf/usermodel/HSSFCell;)Ljava/lang/String;

    move-result-object v11

    .line 3304
    :cond_15
    move-object/from16 v0, v35

    iget-object v2, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 3310
    .end local v28    # "rowNo":I
    :cond_16
    invoke-virtual {v8}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getFirstColumn()I

    move-result v15

    .line 3311
    .restart local v15    # "columnNo":I
    :goto_c
    invoke-virtual {v8}, Lorg/apache/poi/ss/util/CellRangeAddressBase;->getLastColumn()I

    move-result v2

    if-gt v15, v2, :cond_18

    .line 3313
    move-object/from16 v0, v38

    invoke-virtual {v0, v15}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getCell(I)Lorg/apache/poi/hssf/usermodel/HSSFCell;

    move-result-object v37

    .line 3316
    const-string/jumbo v11, ""

    .line 3317
    if-eqz v37, :cond_17

    .line 3318
    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getChartCellValue(Lorg/apache/poi/hssf/usermodel/HSSFCell;)Ljava/lang/String;

    move-result-object v11

    .line 3320
    :cond_17
    move-object/from16 v0, v35

    iget-object v2, v0, Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;->chartCategoryNames:Ljava/util/ArrayList;

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3312
    add-int/lit8 v15, v15, 0x1

    goto :goto_c

    .line 3328
    .end local v8    # "categoryLabelCellRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .end local v9    # "categoryNum":I
    .end local v11    # "chartCellValue":Ljava/lang/String;
    .end local v15    # "columnNo":I
    .end local v22    # "i":I
    .end local v31    # "tempCategoryLabelCellRange":Lorg/apache/poi/ss/util/CellRangeAddressBase;
    .end local v32    # "tempCategoryLabelCellValue":I
    .end local v37    # "tempHssfCell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    .end local v38    # "tempHssfRow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    :cond_18
    if-eqz v35, :cond_12

    .line 3329
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mExcelChartList:Ljava/util/ArrayList;

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 3332
    .end local v12    # "chartInThumbnail":Z
    .end local v23    # "isSeriesInColumn":Z
    .end local v24    # "k":I
    .end local v29    # "series":[Lorg/apache/poi/hssf/usermodel/HSSFChart$HSSFSeries;
    .end local v35    # "tempExcelChartInfo":Lcom/samsung/thumbnail/office/excel/ExcelChartInfo;
    .end local v36    # "tempHSSFChart":Lorg/apache/poi/hssf/usermodel/HSSFChart;
    :cond_19
    return-void
.end method

.method private setColumnWidth()V
    .locals 6

    .prologue
    .line 1703
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v4, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxRowinTable:I

    if-ge v2, v4, :cond_2

    .line 1704
    iget-object v4, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v4, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getRow(I)Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasRow:Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 1706
    iget-object v4, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasRow:Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellList()Ljava/util/ArrayList;

    move-result-object v0

    .line 1707
    .local v0, "cellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    iget v4, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    if-ge v3, v4, :cond_1

    .line 1708
    sget-object v4, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->colmnWdthChartsPicts:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .local v1, "colWid":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1710
    if-eqz v1, :cond_0

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v4, v5, :cond_0

    .line 1711
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 1707
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1703
    .end local v1    # "colWid":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1720
    .end local v0    # "cellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v3    # "j":I
    :cond_2
    return-void
.end method

.method private setShapeColor(Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 24
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p2, "wb"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 952
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getEscherOptRecord()Lorg/apache/poi/ddf/EscherOptRecord;

    move-result-object v21

    const/16 v22, 0x181

    invoke-static/range {v21 .. v22}, Lcom/samsung/thumbnail/util/Utils;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v10

    check-cast v10, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 955
    .local v10, "p1":Lorg/apache/poi/ddf/EscherSimpleProperty;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getEscherOptRecord()Lorg/apache/poi/ddf/EscherOptRecord;

    move-result-object v21

    const/16 v22, 0x1bf

    invoke-static/range {v21 .. v22}, Lcom/samsung/thumbnail/util/Utils;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v11

    check-cast v11, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 958
    .local v11, "p2":Lorg/apache/poi/ddf/EscherSimpleProperty;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getEscherOptRecord()Lorg/apache/poi/ddf/EscherOptRecord;

    move-result-object v21

    const/16 v22, 0x182

    invoke-static/range {v21 .. v22}, Lcom/samsung/thumbnail/util/Utils;->getEscherProperty(Lorg/apache/poi/ddf/EscherOptRecord;I)Lorg/apache/poi/ddf/EscherProperty;

    move-result-object v13

    check-cast v13, Lorg/apache/poi/ddf/EscherSimpleProperty;

    .line 962
    .local v13, "p3":Lorg/apache/poi/ddf/EscherSimpleProperty;
    sget-object v5, Lorg/apache/poi/java/awt/Color;->blue:Lorg/apache/poi/java/awt/Color;

    .line 963
    .local v5, "clr":Lorg/apache/poi/java/awt/Color;
    if-nez v11, :cond_2

    const/4 v12, 0x0

    .line 964
    .local v12, "p2val":I
    :goto_0
    if-nez v13, :cond_3

    const/16 v3, 0xff

    .line 966
    .local v3, "alpha":I
    :goto_1
    if-eqz v10, :cond_1

    and-int/lit8 v21, v12, 0x10

    if-eqz v21, :cond_1

    .line 967
    invoke-virtual {v10}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v16

    .line 968
    .local v16, "rgb":I
    const/16 v21, 0x10

    move/from16 v0, v16

    move/from16 v1, v21

    invoke-static {v0, v1}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v19

    .line 971
    .local v19, "rgbHexString":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x6

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_5

    .line 972
    const-string/jumbo v7, ""

    .line 973
    .local v7, "flagVal":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x7

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_4

    .line 974
    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 975
    const/16 v21, 0x1

    const/16 v22, 0x7

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v19

    .line 981
    :cond_0
    :goto_2
    const/16 v21, 0x10

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v14

    .line 982
    .local v14, "paletteIndex":I
    const-string/jumbo v21, "8"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 983
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getCustomPalette()Lorg/apache/poi/hssf/usermodel/HSSFPalette;

    move-result-object v9

    .line 984
    .local v9, "p":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    int-to-short v0, v14

    move/from16 v21, v0

    move/from16 v0, v21

    invoke-virtual {v9, v0}, Lorg/apache/poi/hssf/usermodel/HSSFPalette;->getColor(S)Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v6

    .line 986
    .local v6, "color":Lorg/apache/poi/hssf/util/HSSFColor;
    if-eqz v6, :cond_1

    .line 987
    invoke-virtual {v6}, Lorg/apache/poi/hssf/util/HSSFColor;->getTriplet()[S

    move-result-object v17

    .line 990
    .local v17, "rgbColorTriplet":[S
    const/16 v21, 0x0

    aget-short v15, v17, v21

    .line 991
    .local v15, "r":I
    const/16 v21, 0x1

    aget-short v8, v17, v21

    .line 992
    .local v8, "g":I
    const/16 v21, 0x2

    aget-short v4, v17, v21

    .line 994
    .local v4, "b":I
    shl-int/lit8 v21, v15, 0x10

    shl-int/lit8 v22, v8, 0x8

    or-int v21, v21, v22

    or-int v18, v21, v4

    .line 995
    .local v18, "rgbColorVal":I
    new-instance v5, Lorg/apache/poi/java/awt/Color;

    .end local v5    # "clr":Lorg/apache/poi/java/awt/Color;
    move/from16 v0, v18

    invoke-direct {v5, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 1004
    .end local v4    # "b":I
    .end local v6    # "color":Lorg/apache/poi/hssf/util/HSSFColor;
    .end local v7    # "flagVal":Ljava/lang/String;
    .end local v8    # "g":I
    .end local v9    # "p":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    .end local v14    # "paletteIndex":I
    .end local v15    # "r":I
    .end local v16    # "rgb":I
    .end local v17    # "rgbColorTriplet":[S
    .end local v18    # "rgbColorVal":I
    .end local v19    # "rgbHexString":Ljava/lang/String;
    .restart local v5    # "clr":Lorg/apache/poi/java/awt/Color;
    :cond_1
    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->setShapeColor(Lorg/apache/poi/java/awt/Color;)V

    .line 1005
    return-void

    .line 963
    .end local v3    # "alpha":I
    .end local v12    # "p2val":I
    :cond_2
    invoke-virtual {v11}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v12

    goto/16 :goto_0

    .line 964
    .restart local v12    # "p2val":I
    :cond_3
    invoke-virtual {v13}, Lorg/apache/poi/ddf/EscherSimpleProperty;->getPropertyValue()I

    move-result v21

    shr-int/lit8 v21, v21, 0x8

    move/from16 v0, v21

    and-int/lit16 v3, v0, 0xff

    goto/16 :goto_1

    .line 976
    .restart local v3    # "alpha":I
    .restart local v7    # "flagVal":Ljava/lang/String;
    .restart local v16    # "rgb":I
    .restart local v19    # "rgbHexString":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 977
    const/16 v21, 0x0

    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 978
    const/16 v21, 0x2

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v19

    goto/16 :goto_2

    .line 999
    .end local v7    # "flagVal":Ljava/lang/String;
    :cond_5
    new-instance v20, Lorg/apache/poi/java/awt/Color;

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v16

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/java/awt/Color;-><init>(IZ)V

    .line 1000
    .local v20, "tmp":Lorg/apache/poi/java/awt/Color;
    new-instance v5, Lorg/apache/poi/java/awt/Color;

    .end local v5    # "clr":Lorg/apache/poi/java/awt/Color;
    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v21

    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v22

    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v23

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v5, v0, v1, v2, v3}, Lorg/apache/poi/java/awt/Color;-><init>(IIII)V

    .restart local v5    # "clr":Lorg/apache/poi/java/awt/Color;
    goto :goto_3
.end method

.method private text(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFFont;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 20
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "horizontalAlign"    # Ljava/lang/String;
    .param p4, "verticalAlign"    # Ljava/lang/String;
    .param p5, "hssfFont"    # Lorg/apache/poi/hssf/usermodel/HSSFFont;
    .param p6, "wb"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 2099
    if-nez p2, :cond_0

    .line 2100
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setParagraphList(Ljava/util/ArrayList;)V

    .line 2196
    :goto_0
    return-void

    .line 2104
    :cond_0
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setHorizontalAlignment(Ljava/lang/String;)V

    .line 2105
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setVerticalAlignment(Ljava/lang/String;)V

    .line 2106
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 2107
    .local v13, "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    new-instance v12, Lcom/samsung/thumbnail/customview/word/Paragraph;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/customview/word/Paragraph;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 2108
    .local v12, "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    new-instance v11, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct {v11}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 2110
    .local v11, "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    if-eqz p5, :cond_c

    .line 2115
    invoke-virtual/range {p5 .. p6}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getHSSFColor(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v8

    .line 2116
    .local v8, "hssfColor":Lorg/apache/poi/hssf/util/HSSFColor;
    const-string/jumbo v7, ""

    .line 2118
    .local v7, "hexString":Ljava/lang/String;
    if-nez v8, :cond_7

    .line 2119
    const-string/jumbo v7, "0:0:0"

    .line 2123
    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getFontColor(Ljava/lang/String;)I

    move-result v3

    .line 2125
    .local v3, "colorVal":I
    invoke-virtual {v11, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 2127
    invoke-virtual/range {p5 .. p5}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getFontName()Ljava/lang/String;

    move-result-object v5

    .line 2128
    .local v5, "fontName":Ljava/lang/String;
    invoke-virtual/range {p5 .. p5}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getBoldweight()S

    move-result v4

    .line 2129
    .local v4, "fontBold":I
    const/4 v9, 0x0

    .line 2130
    .local v9, "isBold":Z
    const/16 v18, 0x2bc

    move/from16 v0, v18

    if-ne v4, v0, :cond_1

    .line 2131
    const/4 v9, 0x1

    .line 2133
    :cond_1
    if-eqz v5, :cond_8

    .line 2134
    invoke-virtual {v11, v5, v9}, Lcom/samsung/thumbnail/customview/word/Run;->setTypeface(Ljava/lang/String;Z)V

    .line 2140
    :goto_2
    invoke-virtual/range {p5 .. p5}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getFontHeightInPoints()S

    move-result v6

    .local v6, "fontSize":I
    if-eqz v6, :cond_9

    .line 2141
    int-to-float v0, v6

    move/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    mul-float v18, v18, v19

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 2145
    :goto_3
    invoke-virtual/range {p5 .. p5}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getItalic()Z

    move-result v10

    .line 2146
    .local v10, "italic":Z
    if-eqz v10, :cond_2

    .line 2147
    invoke-virtual {v11}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 2150
    :cond_2
    invoke-virtual/range {p5 .. p5}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getStrikeout()Z

    move-result v15

    .line 2151
    .local v15, "strikethrough":Z
    if-eqz v15, :cond_3

    .line 2152
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 2155
    :cond_3
    invoke-virtual/range {p5 .. p5}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getTypeOffset()S

    move-result v14

    .line 2156
    .local v14, "scriptoffset":I
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v14, v0, :cond_a

    .line 2157
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUPERSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    .line 2158
    .local v2, "E":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;
    invoke-virtual {v11, v2}, Lcom/samsung/thumbnail/customview/word/Run;->setVertivalAlignment(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 2159
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setVertAlign(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 2166
    .end local v2    # "E":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;
    :cond_4
    :goto_4
    invoke-virtual/range {p5 .. p5}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getUnderline()B

    move-result v17

    .line 2167
    .local v17, "underlinestyle":I
    new-instance v16, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;-><init>()V

    .line 2168
    .local v16, "underlines":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    if-lez v17, :cond_6

    .line 2170
    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_b

    .line 2172
    const-string/jumbo v18, "single"

    invoke-static/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->setStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;)V

    .line 2178
    :cond_5
    :goto_5
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 2192
    .end local v3    # "colorVal":I
    .end local v4    # "fontBold":I
    .end local v5    # "fontName":Ljava/lang/String;
    .end local v6    # "fontSize":I
    .end local v7    # "hexString":Ljava/lang/String;
    .end local v8    # "hssfColor":Lorg/apache/poi/hssf/util/HSSFColor;
    .end local v9    # "isBold":Z
    .end local v10    # "italic":Z
    .end local v14    # "scriptoffset":I
    .end local v15    # "strikethrough":Z
    .end local v16    # "underlines":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    .end local v17    # "underlinestyle":I
    :cond_6
    :goto_6
    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 2193
    invoke-virtual {v12, v11}, Lcom/samsung/thumbnail/customview/word/Paragraph;->AddRun(Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 2194
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2195
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setParagraphList(Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 2121
    .restart local v7    # "hexString":Ljava/lang/String;
    .restart local v8    # "hssfColor":Lorg/apache/poi/hssf/util/HSSFColor;
    :cond_7
    invoke-virtual {v8}, Lorg/apache/poi/hssf/util/HSSFColor;->getHexString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_1

    .line 2136
    .restart local v3    # "colorVal":I
    .restart local v4    # "fontBold":I
    .restart local v5    # "fontName":Ljava/lang/String;
    .restart local v9    # "isBold":Z
    :cond_8
    invoke-virtual {v11}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    goto/16 :goto_2

    .line 2143
    .restart local v6    # "fontSize":I
    :cond_9
    const/high16 v18, 0x40e00000    # 7.0f

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    goto :goto_3

    .line 2160
    .restart local v10    # "italic":Z
    .restart local v14    # "scriptoffset":I
    .restart local v15    # "strikethrough":Z
    :cond_a
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v14, v0, :cond_4

    .line 2161
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUBSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    .line 2162
    .restart local v2    # "E":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;
    invoke-virtual {v11, v2}, Lcom/samsung/thumbnail/customview/word/Run;->setVertivalAlignment(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 2163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setVertAlign(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    goto :goto_4

    .line 2173
    .end local v2    # "E":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;
    .restart local v16    # "underlines":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    .restart local v17    # "underlinestyle":I
    :cond_b
    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_5

    .line 2175
    const-string/jumbo v18, "double"

    invoke-static/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->setStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;)V

    goto :goto_5

    .line 2184
    .end local v3    # "colorVal":I
    .end local v4    # "fontBold":I
    .end local v5    # "fontName":Ljava/lang/String;
    .end local v6    # "fontSize":I
    .end local v7    # "hexString":Ljava/lang/String;
    .end local v8    # "hssfColor":Lorg/apache/poi/hssf/util/HSSFColor;
    .end local v9    # "isBold":Z
    .end local v10    # "italic":Z
    .end local v14    # "scriptoffset":I
    .end local v15    # "strikethrough":Z
    .end local v16    # "underlines":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    .end local v17    # "underlinestyle":I
    :cond_c
    const/high16 v18, 0x41200000    # 10.0f

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 2185
    const-string/jumbo v18, "Calibri"

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTypeface(Ljava/lang/String;Z)V

    goto :goto_6
.end method

.method private text_Font(Lorg/apache/poi/hssf/usermodel/HSSFFont;Ljava/lang/String;Lcom/samsung/thumbnail/customview/word/Run;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V
    .locals 17
    .param p1, "hssfFont"    # Lorg/apache/poi/hssf/usermodel/HSSFFont;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "newRun"    # Lcom/samsung/thumbnail/customview/word/Run;
    .param p4, "wb"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 2354
    if-eqz p1, :cond_b

    .line 2356
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getHSSFColor(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v8

    .line 2357
    .local v8, "hssfColor":Lorg/apache/poi/hssf/util/HSSFColor;
    const/4 v7, 0x0

    .line 2359
    .local v7, "hexString":Ljava/lang/String;
    if-nez v8, :cond_6

    .line 2360
    const-string/jumbo v7, "0:0:0"

    .line 2364
    :goto_0
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getFontColor(Ljava/lang/String;)I

    move-result v3

    .line 2366
    .local v3, "colorVal":I
    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 2368
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getFontName()Ljava/lang/String;

    move-result-object v5

    .line 2370
    .local v5, "fontName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getBoldweight()S

    move-result v4

    .line 2371
    .local v4, "fontBold":I
    const/4 v9, 0x0

    .line 2372
    .local v9, "isBold":Z
    const/16 v15, 0x2bc

    if-ne v4, v15, :cond_0

    .line 2373
    const/4 v9, 0x1

    .line 2375
    :cond_0
    if-eqz v5, :cond_7

    .line 2376
    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v9}, Lcom/samsung/thumbnail/customview/word/Run;->setTypeface(Ljava/lang/String;Z)V

    .line 2382
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getFontHeightInPoints()S

    move-result v6

    .local v6, "fontSize":I
    if-eqz v6, :cond_8

    .line 2383
    int-to-float v15, v6

    const/high16 v16, 0x3f800000    # 1.0f

    mul-float v15, v15, v16

    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 2387
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getItalic()Z

    move-result v10

    .line 2388
    .local v10, "italic":Z
    if-eqz v10, :cond_1

    .line 2389
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 2392
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getStrikeout()Z

    move-result v12

    .line 2393
    .local v12, "strikethrough":Z
    if-eqz v12, :cond_2

    .line 2394
    const/4 v15, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 2397
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getTypeOffset()S

    move-result v11

    .line 2398
    .local v11, "scriptoffset":I
    const/4 v15, 0x1

    if-ne v11, v15, :cond_9

    .line 2399
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUPERSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    .line 2400
    .local v2, "E":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/Run;->setVertivalAlignment(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 2401
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v15, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setVertAlign(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 2408
    .end local v2    # "E":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;
    :cond_3
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getUnderline()B

    move-result v14

    .line 2409
    .local v14, "underlinestyle":I
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    invoke-direct {v13}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;-><init>()V

    .line 2410
    .local v13, "underlines":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    if-lez v14, :cond_5

    .line 2412
    const/4 v15, 0x1

    if-ne v14, v15, :cond_a

    .line 2414
    const-string/jumbo v15, "single"

    invoke-static {v15}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->setStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;)V

    .line 2420
    :cond_4
    :goto_4
    const/4 v15, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 2427
    .end local v3    # "colorVal":I
    .end local v4    # "fontBold":I
    .end local v5    # "fontName":Ljava/lang/String;
    .end local v6    # "fontSize":I
    .end local v7    # "hexString":Ljava/lang/String;
    .end local v8    # "hssfColor":Lorg/apache/poi/hssf/util/HSSFColor;
    .end local v9    # "isBold":Z
    .end local v10    # "italic":Z
    .end local v11    # "scriptoffset":I
    .end local v12    # "strikethrough":Z
    .end local v13    # "underlines":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    .end local v14    # "underlinestyle":I
    :cond_5
    :goto_5
    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 2428
    return-void

    .line 2362
    .restart local v7    # "hexString":Ljava/lang/String;
    .restart local v8    # "hssfColor":Lorg/apache/poi/hssf/util/HSSFColor;
    :cond_6
    invoke-virtual {v8}, Lorg/apache/poi/hssf/util/HSSFColor;->getHexString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 2378
    .restart local v3    # "colorVal":I
    .restart local v4    # "fontBold":I
    .restart local v5    # "fontName":Ljava/lang/String;
    .restart local v9    # "isBold":Z
    :cond_7
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    goto :goto_1

    .line 2385
    .restart local v6    # "fontSize":I
    :cond_8
    const/high16 v15, 0x40e00000    # 7.0f

    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    goto :goto_2

    .line 2402
    .restart local v10    # "italic":Z
    .restart local v11    # "scriptoffset":I
    .restart local v12    # "strikethrough":Z
    :cond_9
    const/4 v15, 0x2

    if-ne v11, v15, :cond_3

    .line 2403
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUBSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    .line 2404
    .restart local v2    # "E":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/Run;->setVertivalAlignment(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 2405
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v15, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setVertAlign(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    goto :goto_3

    .line 2415
    .end local v2    # "E":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;
    .restart local v13    # "underlines":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    .restart local v14    # "underlinestyle":I
    :cond_a
    const/4 v15, 0x2

    if-ne v14, v15, :cond_4

    .line 2417
    const-string/jumbo v15, "double"

    invoke-static {v15}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->setStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;)V

    goto :goto_4

    .line 2424
    .end local v3    # "colorVal":I
    .end local v4    # "fontBold":I
    .end local v5    # "fontName":Ljava/lang/String;
    .end local v6    # "fontSize":I
    .end local v7    # "hexString":Ljava/lang/String;
    .end local v8    # "hssfColor":Lorg/apache/poi/hssf/util/HSSFColor;
    .end local v9    # "isBold":Z
    .end local v10    # "italic":Z
    .end local v11    # "scriptoffset":I
    .end local v12    # "strikethrough":Z
    .end local v13    # "underlines":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    .end local v14    # "underlinestyle":I
    :cond_b
    const/high16 v15, 0x41200000    # 10.0f

    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 2425
    const-string/jumbo v15, "Calibri"

    const/16 v16, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTypeface(Ljava/lang/String;Z)V

    goto :goto_5
.end method

.method private text_Shape(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFFont;II)Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .locals 14
    .param p1, "para"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .param p2, "text"    # Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    .param p3, "wb"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p4, "hssfFont"    # Lorg/apache/poi/hssf/usermodel/HSSFFont;
    .param p5, "horizontalAlignment"    # I
    .param p6, "verticalAlignment"    # I

    .prologue
    .line 2211
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getString()Ljava/lang/String;

    move-result-object v2

    .line 2212
    .local v2, "completeText":Ljava/lang/String;
    const/4 v8, 0x0

    .line 2214
    .local v8, "runHSSFFont":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    const/4 v4, 0x0

    .local v4, "end":I
    const/4 v9, 0x0

    .local v9, "start":I
    const/4 v3, 0x0

    .local v3, "count":I
    const/4 v5, 0x0

    .local v5, "i":I
    const/4 v6, 0x0

    .local v6, "j":I
    const/4 v11, 0x0

    .line 2216
    .local v11, "temp_count":I
    const/4 v6, 0x0

    :goto_0
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v12

    if-ge v6, v12, :cond_6

    .line 2217
    move v5, v6

    .line 2218
    :goto_1
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getFontAtIndex(I)S

    move-result v12

    add-int/lit8 v13, v5, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getFontAtIndex(I)S

    move-result v13

    if-ne v12, v13, :cond_1

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    if-ge v5, v12, :cond_1

    .line 2220
    add-int/lit8 v3, v3, 0x1

    .line 2217
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 2221
    :cond_1
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getFontAtIndex(I)S

    move-result v12

    add-int/lit8 v13, v5, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getFontAtIndex(I)S

    move-result v13

    if-eq v12, v13, :cond_3

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    if-ge v5, v12, :cond_3

    .line 2223
    add-int/lit8 v3, v3, 0x1

    .line 2236
    :goto_2
    add-int v12, v6, v3

    add-int/lit8 v6, v12, -0x1

    .line 2237
    sub-int v12, v6, v3

    add-int/lit8 v9, v12, 0x1

    .line 2238
    add-int/lit8 v4, v6, 0x1

    .line 2239
    move v11, v3

    .line 2240
    const/4 v3, 0x0

    .line 2242
    invoke-virtual {v2, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 2243
    .local v10, "subString":Ljava/lang/String;
    new-instance v7, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct {v7}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 2245
    .local v7, "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v12

    if-eq v11, v12, :cond_5

    .line 2246
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getFontAtIndex(I)S

    move-result v12

    move-object/from16 v0, p3

    invoke-direct {p0, v12, v0}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text_hssfFont(SLorg/apache/poi/hssf/usermodel/HSSFWorkbook;)Lorg/apache/poi/hssf/usermodel/HSSFFont;

    move-result-object v8

    .line 2247
    invoke-virtual {v8}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getIndex()S

    move-result v12

    if-nez v12, :cond_2

    .line 2248
    move-object/from16 v8, p4

    .line 2250
    :cond_2
    move-object/from16 v0, p3

    invoke-direct {p0, v8, v10, v7, v0}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text_Font(Lorg/apache/poi/hssf/usermodel/HSSFFont;Ljava/lang/String;Lcom/samsung/thumbnail/customview/word/Run;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 2254
    :goto_3
    invoke-virtual {p1, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 2216
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 2225
    .end local v7    # "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v10    # "subString":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    if-ne v5, v12, :cond_0

    .line 2226
    if-nez v3, :cond_4

    .line 2227
    const/4 v3, 0x1

    .line 2228
    goto :goto_2

    .line 2229
    :cond_4
    if-eqz v3, :cond_0

    .line 2230
    add-int/lit8 v3, v3, 0x1

    .line 2231
    goto :goto_2

    .line 2252
    .restart local v7    # "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    .restart local v10    # "subString":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-direct {p0, v0, v10, v7, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text_Font(Lorg/apache/poi/hssf/usermodel/HSSFFont;Ljava/lang/String;Lcom/samsung/thumbnail/customview/word/Run;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    goto :goto_3

    .line 2259
    .end local v7    # "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v10    # "subString":Ljava/lang/String;
    :cond_6
    const/4 v12, 0x2

    move/from16 v0, p5

    if-ne v0, v12, :cond_8

    .line 2260
    sget-object v12, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {p1, v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 2269
    :cond_7
    :goto_4
    const/4 v12, 0x1

    move/from16 v0, p6

    if-ne v0, v12, :cond_b

    .line 2270
    sget-object v12, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->TOP:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    invoke-virtual {p1, v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    .line 2280
    :goto_5
    return-object p1

    .line 2261
    :cond_8
    const/4 v12, 0x4

    move/from16 v0, p5

    if-ne v0, v12, :cond_9

    .line 2262
    sget-object v12, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->JUSTIFY:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {p1, v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto :goto_4

    .line 2263
    :cond_9
    const/4 v12, 0x1

    move/from16 v0, p5

    if-ne v0, v12, :cond_a

    .line 2264
    sget-object v12, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {p1, v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto :goto_4

    .line 2265
    :cond_a
    const/4 v12, 0x3

    move/from16 v0, p5

    if-ne v0, v12, :cond_7

    .line 2266
    sget-object v12, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {p1, v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto :goto_4

    .line 2271
    :cond_b
    const/4 v12, 0x3

    move/from16 v0, p6

    if-ne v0, v12, :cond_c

    .line 2272
    sget-object v12, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    invoke-virtual {p1, v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto :goto_5

    .line 2273
    :cond_c
    const/4 v12, 0x2

    move/from16 v0, p6

    if-ne v0, v12, :cond_d

    .line 2274
    sget-object v12, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    invoke-virtual {p1, v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto :goto_5

    .line 2276
    :cond_d
    sget-object v12, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->TOP:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    invoke-virtual {p1, v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto :goto_5
.end method

.method private text_String(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;Ljava/lang/String;Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFFont;)V
    .locals 17
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "text"    # Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    .param p3, "horizontalAlign"    # Ljava/lang/String;
    .param p4, "verticalAlign"    # Ljava/lang/String;
    .param p5, "wb"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;
    .param p6, "hssfFont"    # Lorg/apache/poi/hssf/usermodel/HSSFFont;

    .prologue
    .line 2289
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setHorizontalAlignment(Ljava/lang/String;)V

    .line 2290
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setVerticalAlignment(Ljava/lang/String;)V

    .line 2291
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 2293
    .local v10, "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    new-instance v9, Lcom/samsung/thumbnail/customview/word/Paragraph;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v9, v15}, Lcom/samsung/thumbnail/customview/word/Paragraph;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 2294
    .local v9, "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getWrapContent()Z

    move-result v15

    if-nez v15, :cond_0

    .line 2295
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getShrinkToFit()Z

    move-result v15

    invoke-virtual {v9, v15}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setShrinkToFit(Z)V

    .line 2298
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getString()Ljava/lang/String;

    move-result-object v3

    .line 2299
    .local v3, "completeText":Ljava/lang/String;
    const/4 v11, 0x0

    .line 2301
    .local v11, "runHSSFFont":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    const/4 v5, 0x0

    .local v5, "end":I
    const/4 v12, 0x0

    .local v12, "start":I
    const/4 v4, 0x0

    .local v4, "count":I
    const/4 v6, 0x0

    .local v6, "i":I
    const/4 v7, 0x0

    .local v7, "j":I
    const/4 v14, 0x0

    .line 2303
    .local v14, "temp_count":I
    const/4 v7, 0x0

    :goto_0
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v15

    if-ge v7, v15, :cond_7

    .line 2304
    move v6, v7

    .line 2305
    :goto_1
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getFontAtIndex(I)S

    move-result v15

    add-int/lit8 v16, v6, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getFontAtIndex(I)S

    move-result v16

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    if-ge v6, v15, :cond_2

    .line 2307
    add-int/lit8 v4, v4, 0x1

    .line 2304
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 2308
    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getFontAtIndex(I)S

    move-result v15

    add-int/lit8 v16, v6, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getFontAtIndex(I)S

    move-result v16

    move/from16 v0, v16

    if-eq v15, v0, :cond_4

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    if-ge v6, v15, :cond_4

    .line 2310
    add-int/lit8 v4, v4, 0x1

    .line 2323
    :goto_2
    add-int v15, v7, v4

    add-int/lit8 v7, v15, -0x1

    .line 2324
    sub-int v15, v7, v4

    add-int/lit8 v12, v15, 0x1

    .line 2325
    add-int/lit8 v5, v7, 0x1

    .line 2326
    move v14, v4

    .line 2327
    const/4 v4, 0x0

    .line 2329
    invoke-virtual {v3, v12, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 2330
    .local v13, "subString":Ljava/lang/String;
    new-instance v8, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct {v8}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 2332
    .local v8, "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v15

    if-eq v14, v15, :cond_6

    .line 2333
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->getFontAtIndex(I)S

    move-result v15

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v0, v15, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text_hssfFont(SLorg/apache/poi/hssf/usermodel/HSSFWorkbook;)Lorg/apache/poi/hssf/usermodel/HSSFFont;

    move-result-object v11

    .line 2334
    invoke-virtual {v11}, Lorg/apache/poi/hssf/usermodel/HSSFFont;->getIndex()S

    move-result v15

    if-nez v15, :cond_3

    .line 2335
    move-object/from16 v11, p6

    .line 2337
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v0, v11, v13, v8, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text_Font(Lorg/apache/poi/hssf/usermodel/HSSFFont;Ljava/lang/String;Lcom/samsung/thumbnail/customview/word/Run;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 2341
    :goto_3
    invoke-virtual {v9, v8}, Lcom/samsung/thumbnail/customview/word/Paragraph;->AddRun(Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 2303
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 2312
    .end local v8    # "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v13    # "subString":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    if-ne v6, v15, :cond_1

    .line 2313
    if-nez v4, :cond_5

    .line 2314
    const/4 v4, 0x1

    .line 2315
    goto :goto_2

    .line 2316
    :cond_5
    if-eqz v4, :cond_1

    .line 2317
    add-int/lit8 v4, v4, 0x1

    .line 2318
    goto :goto_2

    .line 2339
    .restart local v8    # "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    .restart local v13    # "subString":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v13, v8, v2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text_Font(Lorg/apache/poi/hssf/usermodel/HSSFFont;Ljava/lang/String;Lcom/samsung/thumbnail/customview/word/Run;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    goto :goto_3

    .line 2344
    .end local v8    # "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v13    # "subString":Ljava/lang/String;
    :cond_7
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2345
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setParagraphList(Ljava/util/ArrayList;)V

    .line 2346
    return-void
.end method

.method private text_hssfFont(SLorg/apache/poi/hssf/usermodel/HSSFWorkbook;)Lorg/apache/poi/hssf/usermodel/HSSFFont;
    .locals 1
    .param p1, "hssffontIndex"    # S
    .param p2, "wb"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 2349
    invoke-virtual {p2, p1}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getFontAt(S)Lorg/apache/poi/hssf/usermodel/HSSFFont;

    move-result-object v0

    return-object v0
.end method

.method private wetherChartAlreadyExist(I)Z
    .locals 7
    .param p1, "shapeId"    # I

    .prologue
    .line 2736
    const/4 v0, 0x0

    .line 2738
    .local v0, "isChartAlreadyExist":Z
    iget-object v6, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    if-eqz v6, :cond_1

    .line 2739
    iget-object v6, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 2740
    .local v3, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2742
    .local v2, "keyIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2743
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2744
    .local v1, "key":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charts:Ljava/util/LinkedHashMap;

    invoke-virtual {v6, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/excel/ShapeInformation;

    .line 2745
    .local v5, "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/excel/ShapeInformation;->getShapeId()I

    move-result v4

    .line 2746
    .local v4, "localShapeId":I
    if-ne v4, p1, :cond_0

    .line 2748
    const/4 v0, 0x1

    .line 2754
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "keyIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v3    # "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v4    # "localShapeId":I
    .end local v5    # "shape":Lcom/samsung/thumbnail/office/excel/ShapeInformation;
    :cond_1
    return v0
.end method


# virtual methods
.method public getCanvas()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    return-object v0
.end method

.method public getRowPosition(ILjava/util/ArrayList;)F
    .locals 4
    .param p1, "rowNumber"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;",
            ">;)F"
        }
    .end annotation

    .prologue
    .line 1667
    .local p2, "mRowList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;>;"
    const/4 v2, 0x0

    .line 1668
    .local v2, "totalHeight":F
    const/4 v0, 0x0

    .line 1670
    .local v0, "rowCounter":I
    :goto_0
    if-gt v0, p1, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 1671
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getRowHeight()F

    move-result v1

    .line 1672
    .local v1, "rowHeight":F
    add-float/2addr v2, v1

    .line 1673
    add-int/lit8 v0, v0, 0x1

    .line 1674
    goto :goto_0

    .line 1676
    .end local v1    # "rowHeight":F
    :cond_0
    if-ge v0, p1, :cond_1

    .line 1677
    :goto_1
    if-gt v0, p1, :cond_1

    .line 1678
    const/high16 v3, 0x41a00000    # 20.0f

    add-float/2addr v2, v3

    .line 1679
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1683
    :cond_1
    return v2
.end method

.method public isChartInThumbnail(ZIIII)Z
    .locals 3
    .param p1, "isThumbnailPreview"    # Z
    .param p2, "row1"    # I
    .param p3, "row2"    # I
    .param p4, "column1"    # I
    .param p5, "column2"    # I

    .prologue
    const/4 v0, 0x1

    const/16 v2, 0x14

    const/16 v1, 0x10

    .line 2995
    if-eqz p1, :cond_2

    .line 2996
    if-le p3, v2, :cond_0

    if-gt p2, v2, :cond_0

    if-le p4, v1, :cond_2

    :cond_0
    if-le p5, v1, :cond_1

    if-gt p4, v1, :cond_1

    if-le p2, v2, :cond_2

    :cond_1
    if-gt p3, v2, :cond_3

    if-gt p5, v1, :cond_3

    .line 3008
    :cond_2
    :goto_0
    return v0

    .line 3005
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readExcelDoc(Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)I
    .locals 70
    .param p1, "sheet"    # Lorg/apache/poi/hssf/usermodel/HSSFSheet;
    .param p2, "wb"    # Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;

    .prologue
    .line 192
    invoke-static {}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->alpha()[Ljava/lang/String;

    move-result-object v56

    .line 194
    .local v56, "header":[Ljava/lang/String;
    const/16 v35, 0x0

    .line 196
    .local v35, "bTerminate":Z
    const/16 v50, 0x0

    .line 197
    .local v50, "escherAggregate":Lorg/apache/poi/hssf/record/EscherAggregate;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    .line 198
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxRowinTable:I

    .line 200
    :try_start_0
    invoke-static/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFChart;->getSheetCharts(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[Lorg/apache/poi/hssf/usermodel/HSSFChart;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFChart;->getSheetCharts(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)[Lorg/apache/poi/hssf/usermodel/HSSFChart;

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_0

    .line 202
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->containChart:Z

    .line 246
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getDrawingEscherAggregate()Lorg/apache/poi/hssf/record/EscherAggregate;

    move-result-object v50

    .line 247
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v50

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getChartDimensions(Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/record/EscherAggregate;)V

    .line 249
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isDisplayGridlines()Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isGridDisplayed:Z

    .line 250
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isDisplayRowColHeadings()Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isHeadingsDisplayed:Z

    .line 252
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getLastRowNum()I

    move-result v58

    .line 254
    .local v58, "lastRowNum":I
    const/16 v60, 0x0

    .line 256
    .local v60, "maxcolumninTabledummy":I
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    if-nez v4, :cond_a

    .line 257
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->rowIterator()Ljava/util/Iterator;

    move-result-object v67

    .line 258
    .local v67, "rowToCalculate":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/usermodel/Row;>;"
    :cond_1
    :goto_0
    invoke-interface/range {v67 .. v67}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 260
    invoke-interface/range {v67 .. v67}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Lorg/apache/poi/hssf/usermodel/HSSFRow;

    .line 262
    .local v46, "dummyrow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    invoke-virtual/range {v46 .. v46}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getLastCellNum()S

    move-result v59

    .line 263
    .local v59, "maxCellsInRow":I
    move/from16 v0, v60

    move/from16 v1, v59

    if-ge v0, v1, :cond_1

    .line 264
    move/from16 v60, v59

    goto :goto_0

    .line 268
    .end local v46    # "dummyrow":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .end local v59    # "maxCellsInRow":I
    :cond_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    move/from16 v0, v60

    if-ge v4, v0, :cond_3

    .line 269
    move/from16 v0, v60

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    .line 272
    :cond_3
    const/16 v4, 0x23

    move/from16 v0, v58

    if-ge v0, v4, :cond_4

    .line 273
    const/16 v58, 0x28

    .line 276
    :cond_4
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxRowinTable:I

    move/from16 v0, v58

    if-ge v4, v0, :cond_5

    .line 277
    move/from16 v0, v58

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxRowinTable:I

    .line 280
    :cond_5
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    add-int/lit8 v4, v4, 0xa

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    .line 282
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    const/16 v5, 0xf

    if-gt v4, v5, :cond_6

    .line 283
    const/16 v4, 0xf

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    .line 287
    :cond_6
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    const/16 v5, 0x2be

    if-le v4, v5, :cond_7

    .line 288
    const/16 v4, 0x2be

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    .line 294
    .end local v67    # "rowToCalculate":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/usermodel/Row;>;"
    :cond_7
    :goto_1
    new-instance v51, Lorg/apache/poi/hssf/usermodel/HSSFFormulaEvaluator;

    move-object/from16 v0, v51

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFFormulaEvaluator;-><init>(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 296
    .local v51, "evaluator":Lorg/apache/poi/hssf/usermodel/HSSFFormulaEvaluator;
    new-instance v4, Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    .line 298
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isHeadingsDisplayed:Z

    if-eqz v4, :cond_d

    .line 299
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->createNewRow()V

    .line 300
    const/16 v57, 0x0

    .local v57, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    move/from16 v0, v57

    if-ge v0, v4, :cond_d

    .line 301
    new-instance v4, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 302
    if-nez v57, :cond_c

    .line 303
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->apply_tbl_first_cell(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 307
    :goto_3
    if-eqz v57, :cond_9

    .line 308
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->textStyleforBlankRowandColumn()V

    .line 309
    aget-object v4, v56, v57

    if-eqz v4, :cond_9

    aget-object v4, v56, v57

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_9

    .line 310
    const-string/jumbo v7, "center"

    .line 311
    .local v7, "hAlign":Ljava/lang/String;
    aget-object v4, v56, v57

    invoke-static {v4}, Lcom/samsung/thumbnail/util/Utils;->isRTLText(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 312
    const-string/jumbo v7, "right"

    .line 315
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    aget-object v6, v56, v57

    const-string/jumbo v8, "center"

    const/4 v9, 0x0

    move-object/from16 v4, p0

    move-object/from16 v10, p2

    invoke-direct/range {v4 .. v10}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFFont;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 319
    .end local v7    # "hAlign":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 300
    add-int/lit8 v57, v57, 0x1

    goto :goto_2

    .line 290
    .end local v51    # "evaluator":Lorg/apache/poi/hssf/usermodel/HSSFFormulaEvaluator;
    .end local v57    # "i":I
    :cond_a
    const/16 v4, 0x10

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    .line 291
    const/16 v4, 0x14

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxRowinTable:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    .line 775
    .end local v58    # "lastRowNum":I
    .end local v60    # "maxcolumninTabledummy":I
    :catch_0
    move-exception v47

    .line 776
    .local v47, "e":Ljava/lang/Exception;
    :try_start_1
    const-string/jumbo v4, "XlsCanvasView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v47 .. v47}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    invoke-virtual/range {v47 .. v47}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 781
    .end local v47    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-direct/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getCustomView()Lcom/samsung/thumbnail/customview/word/CustomView;

    move-result-object v5

    if-eqz v5, :cond_b

    .line 782
    invoke-direct/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getCustomView()Lcom/samsung/thumbnail/customview/word/CustomView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/CustomView;->Load()V

    :cond_b
    throw v4

    .line 305
    .restart local v51    # "evaluator":Lorg/apache/poi/hssf/usermodel/HSSFFormulaEvaluator;
    .restart local v57    # "i":I
    .restart local v58    # "lastRowNum":I
    .restart local v60    # "maxcolumninTabledummy":I
    :cond_c
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->apply_table03(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    goto/16 :goto_3

    .line 323
    .end local v57    # "i":I
    :cond_d
    const/16 v45, 0x0

    .line 324
    .local v45, "count":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->rowIterator()Ljava/util/Iterator;

    move-result-object v68

    .line 325
    .local v68, "rows":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/usermodel/Row;>;"
    :cond_e
    invoke-interface/range {v68 .. v68}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 326
    invoke-interface/range {v68 .. v68}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v65

    check-cast v65, Lorg/apache/poi/hssf/usermodel/HSSFRow;

    .line 327
    .local v65, "row":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    invoke-virtual/range {v65 .. v65}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->cellIterator()Ljava/util/Iterator;

    move-result-object v42

    .line 329
    .local v42, "cells":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/usermodel/Cell;>;"
    invoke-virtual/range {v65 .. v65}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getHeight()S

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x41a00000    # 20.0f

    div-float v33, v4, v5

    .line 330
    .local v33, "adjRow":F
    invoke-virtual/range {v65 .. v65}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v66

    .line 331
    .local v66, "rowNum":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->createNewRow()V

    .line 332
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasRow:Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 333
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasRow:Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move/from16 v0, v33

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->setRowHeight(F)V

    .line 334
    new-instance v4, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 336
    if-nez v45, :cond_11

    .line 337
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->apply_firstrowtopborderstyle(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 346
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isHeadingsDisplayed:Z

    if-eqz v4, :cond_f

    .line 347
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->apply_tbl_first_column(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 348
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->textStyleforBlankRowandColumn()V

    .line 349
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    add-int/lit8 v4, v45, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "center"

    const-string/jumbo v12, "bottom"

    const/4 v13, 0x0

    move-object/from16 v8, p0

    move-object/from16 v14, p2

    invoke-direct/range {v8 .. v14}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFFont;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 351
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 354
    :cond_f
    invoke-virtual/range {v65 .. v65}, Lorg/apache/poi/hssf/usermodel/HSSFRow;->getRowNum()I

    move-result v4

    move/from16 v0, v45

    if-eq v0, v4, :cond_14

    .line 355
    const/16 v49, 0x1

    .line 356
    .local v49, "emptyCell":I
    :goto_5
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    move/from16 v0, v49

    if-ge v0, v4, :cond_12

    .line 357
    new-instance v48, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    .line 358
    .local v48, "emptyCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    if-nez v45, :cond_10

    .line 359
    move-object/from16 v0, p0

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->apply_firstrowtopborderstyle(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 360
    :cond_10
    add-int/lit8 v4, v49, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v48

    invoke-direct {v0, v1, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->apply_empty_cell(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V

    .line 362
    add-int/lit8 v4, v66, -0x1

    add-int/lit8 v5, v49, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v48

    invoke-direct {v0, v1, v4, v5}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addSurroundingCellsBorder(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;II)V

    .line 365
    move-object/from16 v0, p0

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 366
    add-int/lit8 v49, v49, 0x1

    .line 367
    goto :goto_5

    .line 343
    .end local v48    # "emptyCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v49    # "emptyCell":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasRow:Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isGridDisplayed:Z

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->setGridDisplay(Z)V

    goto :goto_4

    .line 368
    .restart local v49    # "emptyCell":I
    :cond_12
    add-int/lit8 v45, v45, 0x1

    .line 369
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->createNewRow()V

    .line 370
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasRow:Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 371
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasRow:Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move/from16 v0, v33

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->setRowHeight(F)V

    .line 373
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isHeadingsDisplayed:Z

    if-eqz v4, :cond_13

    .line 374
    new-instance v4, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 375
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->apply_tbl_first_column(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 376
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->textStyleforBlankRowandColumn()V

    .line 377
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    add-int/lit8 v4, v45, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "center"

    const-string/jumbo v12, "bottom"

    const/4 v13, 0x0

    move-object/from16 v8, p0

    move-object/from16 v14, p2

    invoke-direct/range {v8 .. v14}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFFont;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 379
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 382
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    if-eqz v4, :cond_f

    .line 383
    const/16 v4, 0x14

    move/from16 v0, v45

    if-le v0, v4, :cond_f

    .line 384
    const/16 v35, 0x1

    .line 390
    .end local v49    # "emptyCell":I
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    if-eqz v4, :cond_17

    .line 391
    if-eqz v35, :cond_17

    .line 731
    .end local v33    # "adjRow":F
    .end local v42    # "cells":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/usermodel/Cell;>;"
    .end local v65    # "row":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .end local v66    # "rowNum":I
    :cond_15
    :goto_6
    move/from16 v57, v45

    .restart local v57    # "i":I
    :goto_7
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxRowinTable:I

    move/from16 v0, v57

    if-ge v0, v4, :cond_3a

    .line 732
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->createNewRow()V

    .line 733
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasRow:Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 735
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isHeadingsDisplayed:Z

    if-eqz v4, :cond_16

    .line 736
    new-instance v4, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 737
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->apply_tbl_first_column(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 738
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->textStyleforBlankRowandColumn()V

    .line 739
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v24, v0

    add-int/lit8 v4, v57, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v25

    const-string/jumbo v26, "center"

    const-string/jumbo v27, "center"

    const/16 v28, 0x0

    move-object/from16 v23, p0

    move-object/from16 v29, p2

    invoke-direct/range {v23 .. v29}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFFont;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 741
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 744
    :cond_16
    const/16 v49, 0x1

    .line 745
    .restart local v49    # "emptyCell":I
    :goto_8
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, v49

    if-gt v0, v4, :cond_39

    .line 746
    new-instance v48, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    .line 747
    .restart local v48    # "emptyCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    add-int/lit8 v4, v49, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v48

    invoke-direct {v0, v1, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->apply_empty_cell(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V

    .line 749
    add-int/lit8 v4, v49, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v48

    move/from16 v2, v57

    invoke-direct {v0, v1, v2, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addSurroundingCellsBorder(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;II)V

    .line 751
    move-object/from16 v0, p0

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 752
    add-int/lit8 v49, v49, 0x1

    .line 753
    goto :goto_8

    .line 396
    .end local v48    # "emptyCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v49    # "emptyCell":I
    .end local v57    # "i":I
    .restart local v33    # "adjRow":F
    .restart local v42    # "cells":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/usermodel/Cell;>;"
    .restart local v65    # "row":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .restart local v66    # "rowNum":I
    :cond_17
    const/16 v38, 0x0

    .line 397
    .local v38, "cellCount":I
    :cond_18
    invoke-interface/range {v42 .. v42}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 398
    invoke-interface/range {v42 .. v42}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Lorg/apache/poi/hssf/usermodel/HSSFCell;

    .line 399
    .local v36, "cell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    if-eqz v4, :cond_19

    .line 400
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getColumnIndex()I

    move-result v4

    const/16 v5, 0xf

    if-ge v4, v5, :cond_18

    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getRowIndex()I

    move-result v4

    const/16 v5, 0x13

    if-ge v4, v5, :cond_18

    .line 405
    :cond_19
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellNum()S

    move-result v13

    .line 407
    .local v13, "cellNum":I
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getFont(Lorg/apache/poi/ss/usermodel/Workbook;)Lorg/apache/poi/hssf/usermodel/HSSFFont;

    move-result-object v19

    .line 408
    .local v19, "hssfFont":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v11

    .line 410
    .local v11, "cellStyle":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->getColumnWidth(I)I

    move-result v4

    div-int/lit8 v44, v4, 0x1e

    .line 420
    .local v44, "columnwidth":I
    sget-object v4, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->colmnWdthChartsPicts:Ljava/util/Map;

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Ljava/lang/String;

    .line 422
    .local v43, "cellwidthpresent":Ljava/lang/String;
    if-nez v43, :cond_1e

    .line 423
    sget-object v4, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->colmnWdthChartsPicts:Ljava/util/Map;

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    :cond_1a
    :goto_9
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellNum()S

    move-result v4

    move/from16 v0, v38

    if-eq v0, v4, :cond_1b

    .line 438
    new-instance v48, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    .line 439
    .restart local v48    # "emptyCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    move-object/from16 v0, p0

    move-object/from16 v1, v48

    move/from16 v2, v38

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->apply_empty_cell(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V

    .line 441
    move-object/from16 v0, p0

    move-object/from16 v1, v48

    move/from16 v2, v66

    move/from16 v3, v38

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addSurroundingCellsBorder(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;II)V

    .line 444
    move-object/from16 v0, p0

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 445
    add-int/lit8 v38, v38, 0x1

    .line 446
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    if-eqz v4, :cond_1a

    .line 447
    const/16 v4, 0x10

    move/from16 v0, v38

    if-le v0, v4, :cond_1a

    .line 453
    .end local v48    # "emptyCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    :cond_1b
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    if-eqz v4, :cond_1f

    .line 454
    if-eqz v35, :cond_1f

    .line 704
    .end local v11    # "cellStyle":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    .end local v13    # "cellNum":I
    .end local v19    # "hssfFont":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    .end local v36    # "cell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    .end local v43    # "cellwidthpresent":Ljava/lang/String;
    .end local v44    # "columnwidth":I
    :cond_1c
    :goto_a
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    if-eqz v4, :cond_1d

    .line 705
    if-nez v35, :cond_15

    .line 710
    :cond_1d
    move/from16 v49, v38

    .line 711
    .restart local v49    # "emptyCell":I
    :goto_b
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mMaxColumninTable:I

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, v49

    if-gt v0, v4, :cond_38

    .line 712
    new-instance v48, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    .line 713
    .restart local v48    # "emptyCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    move-object/from16 v0, p0

    move-object/from16 v1, v48

    move/from16 v2, v49

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->apply_empty_cell(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V

    .line 715
    move-object/from16 v0, p0

    move-object/from16 v1, v48

    move/from16 v2, v66

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addSurroundingCellsBorder(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;II)V

    .line 718
    move-object/from16 v0, p0

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 719
    add-int/lit8 v49, v49, 0x1

    .line 720
    goto :goto_b

    .line 428
    .end local v48    # "emptyCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v49    # "emptyCell":I
    .restart local v11    # "cellStyle":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    .restart local v13    # "cellNum":I
    .restart local v19    # "hssfFont":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    .restart local v36    # "cell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    .restart local v43    # "cellwidthpresent":Ljava/lang/String;
    .restart local v44    # "columnwidth":I
    :cond_1e
    invoke-static/range {v43 .. v43}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v62

    .line 429
    .local v62, "previousvalue":I
    move/from16 v0, v44

    move/from16 v1, v62

    if-le v0, v1, :cond_1a

    .line 430
    sget-object v4, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->colmnWdthChartsPicts:Ljava/util/Map;

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 432
    sget-object v4, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->colmnWdthChartsPicts:Ljava/util/Map;

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_9

    .line 459
    .end local v62    # "previousvalue":I
    :cond_1f
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getAlignment()S

    move-result v37

    .line 460
    .local v37, "cellAlignment":I
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getVerticalAlignment()S

    move-result v41

    .line 462
    .local v41, "cellVerticalAlignment":I
    const-string/jumbo v17, "justify"

    .line 463
    .local v17, "horizontalAlign":Ljava/lang/String;
    const-string/jumbo v18, "top"

    .line 465
    .local v18, "verticalAlign":Ljava/lang/String;
    const/4 v4, 0x1

    move/from16 v0, v37

    if-ne v0, v4, :cond_27

    .line 466
    const-string/jumbo v17, "left"

    .line 485
    :cond_20
    :goto_c
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hssf/usermodel/HSSFSheet;->isRightToLeft()Z

    move-result v4

    if-eqz v4, :cond_21

    .line 486
    const-string/jumbo v17, "right"

    .line 489
    :cond_21
    const/4 v4, 0x1

    move/from16 v0, v41

    if-ne v0, v4, :cond_2b

    .line 490
    const-string/jumbo v18, "center"

    .line 509
    :cond_22
    :goto_d
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getFillForegroundColor()S

    move-result v4

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v32

    .line 514
    .local v32, "Index_F":Ljava/lang/Short;
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;->getCustomPalette()Lorg/apache/poi/hssf/usermodel/HSSFPalette;

    move-result-object v61

    .line 515
    .local v61, "pallette_F":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getFillForegroundColor()S

    move-result v4

    move-object/from16 v0, v61

    invoke-virtual {v0, v4}, Lorg/apache/poi/hssf/usermodel/HSSFPalette;->getColor(S)Lorg/apache/poi/hssf/util/HSSFColor;

    move-result-object v53

    .line 518
    .local v53, "fgcolor":Lorg/apache/poi/hssf/util/HSSFColor;
    new-instance v54, Lorg/apache/poi/java/awt/Color;

    sget-object v4, Lorg/apache/poi/java/awt/Color;->WHITE:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v4

    move-object/from16 v0, v54

    invoke-direct {v0, v4}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 520
    .local v54, "foreGroundColor":Lorg/apache/poi/java/awt/Color;
    if-eqz v53, :cond_23

    .line 521
    invoke-virtual/range {v53 .. v53}, Lorg/apache/poi/hssf/util/HSSFColor;->getTriplet()[S

    move-result-object v52

    .line 525
    .local v52, "fgColorTriplet":[S
    const/4 v4, 0x0

    aget-short v63, v52, v4

    .line 526
    .local v63, "r":I
    const/4 v4, 0x1

    aget-short v55, v52, v4

    .line 527
    .local v55, "g":I
    const/4 v4, 0x2

    aget-short v34, v52, v4

    .line 529
    .local v34, "b":I
    shl-int/lit8 v4, v63, 0x10

    shl-int/lit8 v5, v55, 0x8

    or-int/2addr v4, v5

    or-int v64, v4, v34

    .line 530
    .local v64, "rgbColorVal":I
    new-instance v54, Lorg/apache/poi/java/awt/Color;

    .end local v54    # "foreGroundColor":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v0, v54

    move/from16 v1, v64

    invoke-direct {v0, v1}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 533
    .end local v34    # "b":I
    .end local v52    # "fgColorTriplet":[S
    .end local v55    # "g":I
    .end local v63    # "r":I
    .end local v64    # "rgbColorVal":I
    .restart local v54    # "foreGroundColor":Lorg/apache/poi/java/awt/Color;
    :cond_23
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellStyle()Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getFillBackgroundColor()S

    move-result v4

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v31

    .line 555
    .local v31, "Index_B":Ljava/lang/Short;
    new-instance v4, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 559
    sget-object v4, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->colmnWdthChartsPicts:Ljava/util/Map;

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v69

    check-cast v69, Ljava/lang/String;

    .line 562
    .local v69, "strCellWidth":Ljava/lang/String;
    if-eqz v69, :cond_2e

    invoke-virtual/range {v69 .. v69}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2e

    .line 563
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-static/range {v69 .. v69}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 571
    :goto_e
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    if-nez v4, :cond_2f

    .line 572
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v8, p0

    move-object/from16 v9, p2

    move/from16 v12, v66

    invoke-direct/range {v8 .. v13}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addBorderToCell(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;II)V

    .line 586
    :cond_24
    :goto_f
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Short;->shortValue()S

    move-result v4

    const/16 v5, 0x40

    if-eq v4, v5, :cond_25

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Short;->shortValue()S

    move-result v4

    const/16 v5, 0x40

    if-ne v4, v5, :cond_26

    .line 587
    :cond_25
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Short;->shortValue()S

    move-result v4

    const/16 v5, 0x40

    if-ne v4, v5, :cond_30

    .line 588
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    sget-object v5, Lorg/apache/poi/java/awt/Color;->white:Lorg/apache/poi/java/awt/Color;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    .line 601
    :cond_26
    :goto_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual {v11}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getIndention()S

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTextIndent(I)V

    .line 602
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual {v11}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getRotation()S

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTextRotation(S)V

    .line 603
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual {v11}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getShrinkToFit()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setShrinkToFit(Z)V

    .line 604
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual {v11}, Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;->getWrapText()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setWrapContent(Z)V

    .line 606
    const-string/jumbo v16, ""

    .line 607
    .local v16, "value":Ljava/lang/String;
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getCellType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 696
    :goto_11
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    if-eqz v4, :cond_18

    .line 697
    const/16 v4, 0x10

    move/from16 v0, v38

    if-le v0, v4, :cond_18

    goto/16 :goto_a

    .line 467
    .end local v16    # "value":Ljava/lang/String;
    .end local v31    # "Index_B":Ljava/lang/Short;
    .end local v32    # "Index_F":Ljava/lang/Short;
    .end local v53    # "fgcolor":Lorg/apache/poi/hssf/util/HSSFColor;
    .end local v54    # "foreGroundColor":Lorg/apache/poi/java/awt/Color;
    .end local v61    # "pallette_F":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    .end local v69    # "strCellWidth":Ljava/lang/String;
    :cond_27
    const/4 v4, 0x2

    move/from16 v0, v37

    if-ne v0, v4, :cond_28

    .line 468
    const-string/jumbo v17, "center"

    goto/16 :goto_c

    .line 469
    :cond_28
    const/4 v4, 0x3

    move/from16 v0, v37

    if-ne v0, v4, :cond_29

    .line 470
    const-string/jumbo v17, "right"

    goto/16 :goto_c

    .line 475
    :cond_29
    const/4 v4, 0x5

    move/from16 v0, v37

    if-ne v0, v4, :cond_2a

    .line 476
    const-string/jumbo v17, "justify"

    goto/16 :goto_c

    .line 477
    :cond_2a
    const/4 v4, 0x6

    move/from16 v0, v37

    if-ne v0, v4, :cond_20

    .line 479
    const-string/jumbo v17, "center"

    goto/16 :goto_c

    .line 491
    :cond_2b
    const/4 v4, 0x2

    move/from16 v0, v41

    if-ne v0, v4, :cond_2c

    .line 492
    const-string/jumbo v18, "bottom"

    goto/16 :goto_d

    .line 493
    :cond_2c
    const/4 v4, 0x3

    move/from16 v0, v41

    if-ne v0, v4, :cond_2d

    .line 494
    const-string/jumbo v18, "justify"

    goto/16 :goto_d

    .line 495
    :cond_2d
    if-nez v41, :cond_22

    .line 496
    const-string/jumbo v18, "top"

    goto/16 :goto_d

    .line 566
    .restart local v31    # "Index_B":Ljava/lang/Short;
    .restart local v32    # "Index_F":Ljava/lang/Short;
    .restart local v53    # "fgcolor":Lorg/apache/poi/hssf/util/HSSFColor;
    .restart local v54    # "foreGroundColor":Lorg/apache/poi/java/awt/Color;
    .restart local v61    # "pallette_F":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    .restart local v69    # "strCellWidth":Ljava/lang/String;
    :cond_2e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    const/high16 v5, 0x428c0000    # 70.0f

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    goto/16 :goto_e

    .line 575
    :cond_2f
    const/16 v4, 0x14

    move/from16 v0, v66

    if-ge v0, v4, :cond_24

    const/16 v4, 0x10

    if-ge v13, v4, :cond_24

    .line 577
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v8, p0

    move-object/from16 v9, p2

    move/from16 v12, v66

    invoke-direct/range {v8 .. v13}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addBorderToCell(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;II)V

    goto/16 :goto_f

    .line 589
    :cond_30
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Short;->shortValue()S

    move-result v4

    const/16 v5, 0x40

    if-ne v4, v5, :cond_31

    .line 590
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    const/4 v5, 0x0

    move-object/from16 v0, v54

    invoke-virtual {v4, v0, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    goto/16 :goto_10

    .line 592
    :cond_31
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Short;->shortValue()S

    move-result v4

    const/16 v5, 0x40

    if-ne v4, v5, :cond_26

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Short;->shortValue()S

    move-result v4

    const/16 v5, 0x40

    if-ne v4, v5, :cond_26

    .line 593
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    sget-object v5, Lorg/apache/poi/java/awt/Color;->white:Lorg/apache/poi/java/awt/Color;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    goto/16 :goto_10

    .line 611
    .restart local v16    # "value":Ljava/lang/String;
    :pswitch_0
    invoke-static/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFDateUtil;->isCellDateFormatted(Lorg/apache/poi/ss/usermodel/Cell;)Z

    move-result v4

    if-eqz v4, :cond_33

    .line 612
    new-instance v30, Lorg/apache/poi/ss/usermodel/DataFormatter;

    invoke-direct/range {v30 .. v30}, Lorg/apache/poi/ss/usermodel/DataFormatter;-><init>()V

    .line 613
    .local v30, "F":Lorg/apache/poi/ss/usermodel/DataFormatter;
    move-object/from16 v0, v30

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/poi/ss/usermodel/DataFormatter;->formatCellValue(Lorg/apache/poi/ss/usermodel/Cell;)Ljava/lang/String;

    move-result-object v16

    .line 618
    .end local v30    # "F":Lorg/apache/poi/ss/usermodel/DataFormatter;
    :goto_12
    const-string/jumbo v4, "left"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_32

    const-string/jumbo v4, "center"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_32

    .line 620
    const-string/jumbo v17, "right"

    .line 622
    :cond_32
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v14, p0

    move-object/from16 v20, p2

    invoke-direct/range {v14 .. v20}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFFont;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 624
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    move/from16 v1, v66

    invoke-direct {v0, v4, v1, v13}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addSurroundingCellsBorder(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;II)V

    .line 625
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 626
    add-int/lit8 v38, v38, 0x1

    .line 627
    goto/16 :goto_11

    .line 615
    :cond_33
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getNumericCellStringVal(Lorg/apache/poi/hssf/usermodel/HSSFCell;)Ljava/lang/String;

    move-result-object v16

    goto :goto_12

    .line 631
    :pswitch_1
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getRichStringCellValue()Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;

    move-result-object v22

    .line 632
    .local v22, "s":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    const-string/jumbo v4, "justify"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_34

    .line 633
    const-string/jumbo v17, "left"

    .line 636
    :cond_34
    invoke-virtual/range {v22 .. v22}, Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/thumbnail/util/Utils;->isRTLText(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_35

    .line 637
    const-string/jumbo v17, "right"

    .line 639
    :cond_35
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v21, v0

    move-object/from16 v20, p0

    move-object/from16 v23, v17

    move-object/from16 v24, v18

    move-object/from16 v25, p2

    move-object/from16 v26, v19

    invoke-direct/range {v20 .. v26}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text_String(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;Ljava/lang/String;Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFFont;)V

    .line 642
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    move/from16 v1, v66

    invoke-direct {v0, v4, v1, v13}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addSurroundingCellsBorder(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;II)V

    .line 643
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 644
    add-int/lit8 v38, v38, 0x1

    .line 645
    goto/16 :goto_11

    .line 649
    .end local v22    # "s":Lorg/apache/poi/hssf/usermodel/HSSFRichTextString;
    :pswitch_2
    invoke-virtual/range {v36 .. v36}, Lorg/apache/poi/hssf/usermodel/HSSFCell;->getBooleanCellValue()Z

    move-result v39

    .line 650
    .local v39, "cellNumBoolean":Z
    invoke-static/range {v39 .. v39}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v16

    .line 651
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v14, p0

    move-object/from16 v20, p2

    invoke-direct/range {v14 .. v20}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFFont;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 653
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    move/from16 v1, v66

    invoke-direct {v0, v4, v1, v13}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addSurroundingCellsBorder(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;II)V

    .line 654
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 655
    add-int/lit8 v38, v38, 0x1

    .line 656
    goto/16 :goto_11

    .line 659
    .end local v39    # "cellNumBoolean":Z
    :pswitch_3
    const-string/jumbo v16, ""
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 661
    if-eqz v51, :cond_36

    .line 662
    :try_start_3
    move-object/from16 v0, v51

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lorg/apache/poi/hssf/usermodel/HSSFFormulaEvaluator;->evaluate(Lorg/apache/poi/ss/usermodel/Cell;)Lorg/apache/poi/ss/usermodel/CellValue;

    move-result-object v40

    .line 664
    .local v40, "cellVal":Lorg/apache/poi/ss/usermodel/CellValue;
    if-eqz v40, :cond_36

    .line 665
    invoke-virtual/range {v40 .. v40}, Lorg/apache/poi/ss/usermodel/CellValue;->formatAsString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v16

    .line 674
    .end local v40    # "cellVal":Lorg/apache/poi/ss/usermodel/CellValue;
    :cond_36
    :goto_13
    :try_start_4
    const-string/jumbo v4, "left"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_37

    const-string/jumbo v4, "center"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_37

    .line 676
    const-string/jumbo v17, "right"

    .line 678
    :cond_37
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v14, p0

    move-object/from16 v20, p2

    invoke-direct/range {v14 .. v20}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFFont;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 680
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    move/from16 v1, v66

    invoke-direct {v0, v4, v1, v13}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addSurroundingCellsBorder(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;II)V

    .line 681
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 682
    add-int/lit8 v38, v38, 0x1

    .line 683
    goto/16 :goto_11

    .line 668
    :catch_1
    move-exception v47

    .line 669
    .restart local v47    # "e":Ljava/lang/Exception;
    const-string/jumbo v4, "XlsCanvasView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: CELL_TYPE_FORMULA "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v47 .. v47}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_13

    .line 687
    .end local v47    # "e":Ljava/lang/Exception;
    :pswitch_4
    const/16 v16, 0x0

    .line 688
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v14, p0

    move-object/from16 v20, p2

    invoke-direct/range {v14 .. v20}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->text(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/poi/hssf/usermodel/HSSFFont;Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;)V

    .line 690
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    move/from16 v1, v66

    invoke-direct {v0, v4, v1, v13}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addSurroundingCellsBorder(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;II)V

    .line 691
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasCell:Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 692
    add-int/lit8 v38, v38, 0x1

    goto/16 :goto_11

    .line 721
    .end local v11    # "cellStyle":Lorg/apache/poi/hssf/usermodel/HSSFCellStyle;
    .end local v13    # "cellNum":I
    .end local v16    # "value":Ljava/lang/String;
    .end local v17    # "horizontalAlign":Ljava/lang/String;
    .end local v18    # "verticalAlign":Ljava/lang/String;
    .end local v19    # "hssfFont":Lorg/apache/poi/hssf/usermodel/HSSFFont;
    .end local v31    # "Index_B":Ljava/lang/Short;
    .end local v32    # "Index_F":Ljava/lang/Short;
    .end local v36    # "cell":Lorg/apache/poi/hssf/usermodel/HSSFCell;
    .end local v37    # "cellAlignment":I
    .end local v41    # "cellVerticalAlignment":I
    .end local v43    # "cellwidthpresent":Ljava/lang/String;
    .end local v44    # "columnwidth":I
    .end local v53    # "fgcolor":Lorg/apache/poi/hssf/util/HSSFColor;
    .end local v54    # "foreGroundColor":Lorg/apache/poi/java/awt/Color;
    .end local v61    # "pallette_F":Lorg/apache/poi/hssf/usermodel/HSSFPalette;
    .end local v69    # "strCellWidth":Ljava/lang/String;
    .restart local v49    # "emptyCell":I
    :cond_38
    add-int/lit8 v45, v45, 0x1

    .line 723
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isThumbnailPreview:Z

    if-eqz v4, :cond_e

    .line 724
    const/16 v4, 0x14

    move/from16 v0, v45

    if-le v0, v4, :cond_e

    .line 725
    const/16 v35, 0x1

    .line 726
    goto/16 :goto_6

    .line 731
    .end local v33    # "adjRow":F
    .end local v38    # "cellCount":I
    .end local v42    # "cells":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/poi/ss/usermodel/Cell;>;"
    .end local v65    # "row":Lorg/apache/poi/hssf/usermodel/HSSFRow;
    .end local v66    # "rowNum":I
    .restart local v57    # "i":I
    :cond_39
    add-int/lit8 v57, v57, 0x1

    goto/16 :goto_7

    .line 755
    .end local v49    # "emptyCell":I
    :cond_3a
    invoke-direct/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->setColumnWidth()V

    .line 757
    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mergeRow(Lorg/apache/poi/hssf/usermodel/HSSFSheet;)V

    .line 764
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->isGridDisplayed:Z

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setGridDisplay(Z)V

    .line 765
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 767
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->calculateHeight()F

    .line 770
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move-object/from16 v3, v50

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->drawShapesPictCharts(Lorg/apache/poi/hssf/usermodel/HSSFWorkbook;Lorg/apache/poi/hssf/usermodel/HSSFSheet;Lorg/apache/poi/hssf/record/EscherAggregate;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 781
    invoke-direct/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getCustomView()Lcom/samsung/thumbnail/customview/word/CustomView;

    move-result-object v4

    if-eqz v4, :cond_3b

    .line 782
    invoke-direct/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->getCustomView()Lcom/samsung/thumbnail/customview/word/CustomView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/CustomView;->Load()V

    .line 785
    :cond_3b
    const/4 v4, 0x0

    return v4

    .line 607
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public textStyleforBlankRowandColumn()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2082
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 2083
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setUndreline(Z)V

    .line 2084
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setItalic(Z)V

    .line 2085
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setBold(Z)V

    .line 2086
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontSize(I)V

    .line 2087
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 2088
    .local v0, "docColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    const-string/jumbo v1, "#000000"

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 2089
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 2090
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XlsCanvasView;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    const-string/jumbo v2, "Calibri"

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontName(Ljava/lang/String;)V

    .line 2094
    return-void
.end method
