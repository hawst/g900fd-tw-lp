.class public Lcom/samsung/thumbnail/customview/word/CustomView;
.super Landroid/view/View;
.source "CustomView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/customview/word/CustomView$ScaleListener;
    }
.end annotation


# instance fields
.field private drawObj:Lcom/samsung/thumbnail/customview/draw/CanvasDraw;

.field private mHandler:Landroid/os/Handler;

.field private mScaleDetector:Landroid/view/ScaleGestureDetector;

.field private mScaleFactor:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->mScaleFactor:F

    .line 23
    iput-object v2, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->drawObj:Lcom/samsung/thumbnail/customview/draw/CanvasDraw;

    .line 27
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->mHandler:Landroid/os/Handler;

    .line 28
    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v1, Lcom/samsung/thumbnail/customview/word/CustomView$ScaleListener;

    invoke-direct {v1, p0, v2}, Lcom/samsung/thumbnail/customview/word/CustomView$ScaleListener;-><init>(Lcom/samsung/thumbnail/customview/word/CustomView;Lcom/samsung/thumbnail/customview/word/CustomView$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 29
    new-instance v0, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->drawObj:Lcom/samsung/thumbnail/customview/draw/CanvasDraw;

    .line 30
    return-void
.end method

.method private DoInvalidate()V
    .locals 0

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/CustomView;->invalidate()V

    .line 79
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/thumbnail/customview/word/CustomView;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/thumbnail/customview/word/CustomView;

    .prologue
    .line 13
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->mScaleFactor:F

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/thumbnail/customview/word/CustomView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/thumbnail/customview/word/CustomView;
    .param p1, "x1"    # F

    .prologue
    .line 13
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->mScaleFactor:F

    return p1
.end method

.method static synthetic access$132(Lcom/samsung/thumbnail/customview/word/CustomView;F)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/thumbnail/customview/word/CustomView;
    .param p1, "x1"    # F

    .prologue
    .line 13
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->mScaleFactor:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->mScaleFactor:F

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/thumbnail/customview/word/CustomView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/thumbnail/customview/word/CustomView;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/word/CustomView;->DoInvalidate()V

    return-void
.end method


# virtual methods
.method public Load()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/thumbnail/customview/word/CustomView$1;

    invoke-direct {v1, p0}, Lcom/samsung/thumbnail/customview/word/CustomView$1;-><init>(Lcom/samsung/thumbnail/customview/word/CustomView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 89
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 49
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 50
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->mScaleFactor:F

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->mScaleFactor:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 51
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->drawObj:Lcom/samsung/thumbnail/customview/draw/CanvasDraw;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/CustomView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->handleDraw(Landroid/graphics/Canvas;Landroid/content/Context;)Z

    .line 52
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 53
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 60
    const/4 v0, 0x1

    return v0
.end method

.method public setCanvas(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 1
    .param p1, "elementCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView;->drawObj:Lcom/samsung/thumbnail/customview/draw/CanvasDraw;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/customview/draw/CanvasDraw;->setCanvas(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 44
    return-void
.end method

.method public setDMHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 40
    return-void
.end method

.method public setDMWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 35
    return-void
.end method
