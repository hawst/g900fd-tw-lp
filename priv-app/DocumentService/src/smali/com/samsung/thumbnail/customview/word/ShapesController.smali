.class public Lcom/samsung/thumbnail/customview/word/ShapesController;
.super Lcom/samsung/thumbnail/customview/CanvasElementPart;
.source "ShapesController.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/customview/word/ShapesController$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/thumbnail/customview/CanvasElementPart;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/samsung/thumbnail/customview/word/ShapesController;",
        ">;"
    }
.end annotation


# static fields
.field public static final ACTION_BUTTON_SHAPES:Ljava/lang/String; = "ActionButtonShapes"

.field public static final ARROW_CALL_OUT:Ljava/lang/String; = "ArrowCallout"

.field public static final BASIC_SHAPES:Ljava/lang/String; = "BasicShapes"

.field public static final BENT_CONNECTOR:Ljava/lang/String; = "BentConnector"

.field public static final BLOCK_ARROW:Ljava/lang/String; = "BlockArrow"

.field public static final BLOCK_SHAPES:Ljava/lang/String; = "BlockShapes"

.field public static final BORDER_CALLOUT1:Ljava/lang/String; = "Callout1"

.field public static final BORDER_CALLOUT2:Ljava/lang/String; = "Callout2"

.field public static final BORDER_CALLOUT3:Ljava/lang/String; = "Callout3"

.field public static final BRACE_SHAPES:Ljava/lang/String; = "BraceShapes"

.field public static final BRACKET_SHAPES:Ljava/lang/String; = "BracketShapes"

.field public static final CURVED_BLOCK_ARROW:Ljava/lang/String; = "CurvedBlockArrow"

.field public static final FLOW_CHART_SHAPES:Ljava/lang/String; = "FlowChartProcess"

.field public static final FREE_FORM:Ljava/lang/String; = "Freeform"

.field public static INITIAL_XVAL:I = 0x0

.field public static INITIAL_YVAL:I = 0x0

.field public static final IRREGULAR_SEAL:Ljava/lang/String; = "IrregularSeal"

.field public static final NON_PRIMITIVE_SHAPES:Ljava/lang/String; = "NonPrimitive"

.field public static final RIBBON_SHAPES:Ljava/lang/String; = "Ribbbonhapes"

.field public static final SCROLL_SHAPES:Ljava/lang/String; = "ScrollShapes"

.field public static final STAR_SHAPES:Ljava/lang/String; = "StarShapes"

.field private static final TAG:Ljava/lang/String; = "ShapesController"

.field public static final TEXT_ON_PATH:Ljava/lang/String; = "textOnPath"

.field public static final WAVE_SHAPES:Ljava/lang/String; = "WaveShapes"

.field public static final WEDGE_CALLOUTS:Ljava/lang/String; = "WedgeCallouts"

.field private static final arc:I = 0x4

.field private static final arcTo:I = 0x3

.field private static final clockwiseArc:I = 0x6

.field private static final clockwiseArcTo:I = 0x5

.field public static mNumOfHeader:I = 0x0

.field private static final pathClose:I = 0x3

.field private static final pathCurveTo:I = 0x1

.field private static final pathEnd:I = 0x4

.field private static final pathEscape:I = 0x5

.field private static final pathLineTo:I = 0x0

.field private static final pathMoveTo:I = 0x2

.field private static final quadraticBezier:I = 0x9


# instance fields
.field private autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

.field private elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field private folderPath:Ljava/io/File;

.field private isDiagramShape:Z

.field mAutoShape:Lorg/apache/poi/hslf/model/AutoShape;

.field private mCanvas:Landroid/graphics/Canvas;

.field private mConnectorShapeIndex:I

.field private mContext:Landroid/content/Context;

.field mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

.field private mFlipValue:Ljava/lang/String;

.field mFooterName:Ljava/lang/String;

.field mHeaderName:Ljava/lang/String;

.field private mHorAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

.field private mHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

.field mIndex:I

.field mIsFooter:Z

.field mIsHeader:Z

.field mLeftMargin:F

.field mParaNumber:I

.field private mParagraphArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;"
        }
    .end annotation
.end field

.field mPath:Ljava/lang/String;

.field private mRotationAngle:J

.field mShapeCount:I

.field mShapeHeight:F

.field mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

.field mShapeStringId:Ljava/lang/String;

.field mShapeType:Ljava/lang/String;

.field mShapeWidth:F

.field private mTextBoxContents:Lcom/samsung/thumbnail/customview/word/TextBoxController;

.field mTextRotationAngle:I

.field mTopMargin:F

.field private mVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

.field private mVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

.field mXWPFShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

.field mZorder:J

.field private shape:Lorg/apache/poi/hslf/model/Shape;

.field shapeInfo:Lcom/samsung/thumbnail/office/word/WordShapeInfo;

.field private x:I

.field private xslfShapeCount:I

.field private xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

.field private xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

.field private xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

.field private y:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 103
    const/4 v0, -0x1

    sput v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_YVAL:I

    .line 106
    sput v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_XVAL:I

    .line 137
    sput v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->mNumOfHeader:I

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;ILjava/io/File;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "autoShape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .param p4, "xslfShapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p5, "xslfSlide"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;
    .param p6, "xslfShapeCount"    # I
    .param p7, "folderPath"    # Ljava/io/File;

    .prologue
    const/4 v1, 0x0

    .line 204
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 90
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    .line 94
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHeaderName:Ljava/lang/String;

    .line 95
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mFooterName:Ljava/lang/String;

    .line 98
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mConnectorShapeIndex:I

    .line 113
    sget v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_YVAL:I

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 114
    sget v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_XVAL:I

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 167
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    .line 171
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape:Z

    .line 205
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mCanvas:Landroid/graphics/Canvas;

    .line 206
    iput-object p2, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mContext:Landroid/content/Context;

    .line 207
    iput-object p3, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .line 208
    iput p6, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    .line 209
    iput-object p5, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    .line 210
    iput-object p4, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 211
    iput-object p7, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    .line 212
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->init()V

    .line 213
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;ILjava/io/File;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "autoShape"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;
    .param p4, "xslfShapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p5, "xslfSlideMaster"    # Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;
    .param p6, "xslfShapeCount"    # I
    .param p7, "folderPath"    # Ljava/io/File;

    .prologue
    const/4 v1, 0x0

    .line 217
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 90
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    .line 94
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHeaderName:Ljava/lang/String;

    .line 95
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mFooterName:Ljava/lang/String;

    .line 98
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mConnectorShapeIndex:I

    .line 113
    sget v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_YVAL:I

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 114
    sget v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_XVAL:I

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 167
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    .line 171
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape:Z

    .line 218
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mCanvas:Landroid/graphics/Canvas;

    .line 219
    iput-object p2, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mContext:Landroid/content/Context;

    .line 220
    iput-object p3, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    .line 221
    iput p6, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    .line 222
    iput-object p5, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    .line 223
    iput-object p4, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 224
    iput-object p7, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    .line 225
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->init()V

    .line 226
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 2
    .param p1, "docType"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;
    .param p2, "shapeType"    # Ljava/lang/String;
    .param p3, "shapeInfo"    # Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;
    .param p4, "shapeCount"    # I
    .param p5, "top"    # I
    .param p6, "left"    # I
    .param p7, "bottom"    # I
    .param p8, "right"    # I
    .param p9, "folderPath"    # Ljava/io/File;
    .param p10, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    const/4 v1, 0x0

    .line 305
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 90
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    .line 94
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHeaderName:Ljava/lang/String;

    .line 95
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mFooterName:Ljava/lang/String;

    .line 98
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mConnectorShapeIndex:I

    .line 113
    sget v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_YVAL:I

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 114
    sget v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_XVAL:I

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 167
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    .line 171
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape:Z

    .line 306
    iput-object p3, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    .line 307
    iput p4, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    .line 308
    iput-object p2, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeType:Ljava/lang/String;

    .line 309
    int-to-float v0, p6

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    .line 310
    int-to-float v0, p5

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    .line 311
    iput-object p9, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    .line 312
    iput-object p10, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 315
    sub-int v0, p8, p6

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 316
    sub-int v0, p7, p5

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 317
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 318
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->init()V

    .line 319
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 13
    .param p1, "docType"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;
    .param p2, "shapeType"    # Ljava/lang/String;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "i"    # I
    .param p5, "top"    # I
    .param p6, "left"    # I
    .param p7, "bottom"    # I
    .param p8, "right"    # I
    .param p9, "connectorShapeIndex"    # I
    .param p10, "folderPath"    # Ljava/io/File;
    .param p11, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .param p12, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 260
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v1 .. v12}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 262
    move/from16 v0, p9

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mConnectorShapeIndex:I

    .line 263
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 2
    .param p1, "docType"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;
    .param p2, "shapeType"    # Ljava/lang/String;
    .param p3, "drawingobj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;
    .param p4, "i"    # I
    .param p5, "top"    # I
    .param p6, "left"    # I
    .param p7, "bottom"    # I
    .param p8, "right"    # I
    .param p9, "folderPath"    # Ljava/io/File;
    .param p10, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .param p11, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    const/4 v1, 0x0

    .line 267
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 90
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    .line 94
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHeaderName:Ljava/lang/String;

    .line 95
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mFooterName:Ljava/lang/String;

    .line 98
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mConnectorShapeIndex:I

    .line 113
    sget v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_YVAL:I

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 114
    sget v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_XVAL:I

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 167
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    .line 171
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape:Z

    .line 268
    iput-object p3, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .line 269
    iput p4, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    .line 270
    iput-object p2, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeType:Ljava/lang/String;

    .line 271
    int-to-float v0, p6

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    .line 272
    int-to-float v0, p5

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    .line 273
    sub-int v0, p8, p6

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 274
    sub-int v0, p7, p5

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 275
    iput-object p9, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    .line 276
    iput-object p10, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 279
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 280
    iput-object p11, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .line 282
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->init()V

    .line 283
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Lorg/apache/poi/hslf/model/Shape;IIIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Ljava/io/File;)V
    .locals 2
    .param p1, "docType"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;
    .param p2, "shape"    # Lorg/apache/poi/hslf/model/Shape;
    .param p3, "left"    # I
    .param p4, "top"    # I
    .param p5, "width"    # I
    .param p6, "height"    # I
    .param p7, "shapeCount"    # I
    .param p8, "canvasEleCtr"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .param p9, "filePath"    # Ljava/io/File;

    .prologue
    const/4 v1, 0x0

    .line 288
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 90
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    .line 94
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHeaderName:Ljava/lang/String;

    .line 95
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mFooterName:Ljava/lang/String;

    .line 98
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mConnectorShapeIndex:I

    .line 113
    sget v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_YVAL:I

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 114
    sget v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_XVAL:I

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 167
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    .line 171
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape:Z

    .line 289
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 290
    iput-object p2, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->shape:Lorg/apache/poi/hslf/model/Shape;

    .line 291
    int-to-float v0, p3

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    .line 292
    int-to-float v0, p4

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    .line 293
    int-to-float v0, p5

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 294
    int-to-float v0, p6

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 295
    iput-object p8, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 296
    iput-object p9, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    .line 297
    iput p7, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    .line 298
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->init()V

    .line 299
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;IIFFFFJJLcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;Ljava/lang/String;Ljava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 3
    .param p1, "shapeType"    # Ljava/lang/String;
    .param p2, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p3, "paraNumber"    # I
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # F
    .param p6, "topMargin"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "zOrder"    # J
    .param p11, "rotationAngle"    # J
    .param p13, "horAlignEle"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    .param p14, "horRelEle"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;
    .param p15, "verAlignEle"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    .param p16, "vertRelEle"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
    .param p17, "flipValue"    # Ljava/lang/String;
    .param p18, "folderPath"    # Ljava/io/File;
    .param p19, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 90
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    .line 94
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHeaderName:Ljava/lang/String;

    .line 95
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mFooterName:Ljava/lang/String;

    .line 98
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mConnectorShapeIndex:I

    .line 113
    sget v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_YVAL:I

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 114
    sget v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_XVAL:I

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 167
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    .line 171
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape:Z

    .line 181
    iput-object p2, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mXWPFShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .line 182
    iput p3, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParaNumber:I

    .line 183
    iput p4, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    .line 184
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeType:Ljava/lang/String;

    .line 185
    iput p5, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    .line 186
    iput p6, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    .line 187
    iput p7, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 188
    iput p8, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 189
    iput-wide p9, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mZorder:J

    .line 190
    iput-wide p11, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    .line 191
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHorAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 192
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    .line 193
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 194
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    .line 195
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mFlipValue:Ljava/lang/String;

    .line 196
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    .line 197
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 198
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->init()V

    .line 199
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/word/WordShapeInfo;IIFFFFJJLcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;Ljava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 3
    .param p1, "shapeType"    # Ljava/lang/String;
    .param p2, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .param p3, "paraNumber"    # I
    .param p4, "shapeCount"    # I
    .param p5, "leftMargin"    # F
    .param p6, "topMargin"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "zOrder"    # J
    .param p11, "rotationAngle"    # J
    .param p13, "horAlignEle"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    .param p14, "horRelEle"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;
    .param p15, "verAlignEle"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    .param p16, "vertRelEle"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
    .param p17, "folderPath"    # Ljava/io/File;
    .param p18, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 236
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 90
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    .line 94
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHeaderName:Ljava/lang/String;

    .line 95
    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mFooterName:Ljava/lang/String;

    .line 98
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mConnectorShapeIndex:I

    .line 113
    sget v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_YVAL:I

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 114
    sget v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_XVAL:I

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 167
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    .line 171
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape:Z

    .line 237
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeType:Ljava/lang/String;

    .line 238
    iput-object p2, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->shapeInfo:Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    .line 239
    iput p3, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParaNumber:I

    .line 240
    iput p4, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    .line 241
    iput p5, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    .line 242
    iput p6, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    .line 243
    iput p7, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 244
    iput p8, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 245
    iput-wide p9, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mZorder:J

    .line 246
    iput-wide p11, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    .line 247
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHorAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 248
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    .line 249
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 250
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    .line 251
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    .line 252
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 253
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->init()V

    .line 254
    return-void
.end method

.method private createNonPrimitiveShapes(Lorg/apache/poi/hslf/model/AutoShape;Landroid/graphics/Canvas;II)V
    .locals 42
    .param p1, "shape"    # Lorg/apache/poi/hslf/model/AutoShape;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "leftMargin"    # I
    .param p4, "topMargin"    # I

    .prologue
    .line 3061
    move/from16 v0, p3

    int-to-float v5, v0

    move/from16 v0, p4

    int-to-float v6, v0

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3081
    new-instance v16, Landroid/graphics/Paint;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Paint;-><init>()V

    .line 3082
    .local v16, "paintLine":Landroid/graphics/Paint;
    const/4 v5, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 3083
    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 3084
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v5, v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 3086
    new-instance v15, Landroid/graphics/Paint;

    invoke-direct {v15}, Landroid/graphics/Paint;-><init>()V

    .line 3087
    .local v15, "paintFill":Landroid/graphics/Paint;
    const/4 v5, 0x1

    invoke-virtual {v15, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 3088
    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v15, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 3090
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 3091
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v8

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 3097
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getFill()Lorg/apache/poi/hslf/model/Fill;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v14

    .line 3098
    .local v14, "foregroundColor":Lorg/apache/poi/java/awt/Color;
    if-eqz v14, :cond_1

    .line 3099
    invoke-virtual {v14}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v5

    invoke-virtual {v14}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v6

    invoke-virtual {v14}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v7

    invoke-virtual {v14}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v8

    invoke-virtual {v15, v5, v6, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 3103
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getFill()Lorg/apache/poi/hslf/model/Fill;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 3104
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getFill()Lorg/apache/poi/hslf/model/Fill;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v11

    .line 3105
    .local v11, "color":Lorg/apache/poi/java/awt/Color;
    if-eqz v11, :cond_2

    .line 3106
    invoke-virtual {v11}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v5

    add-int/lit8 v5, v5, -0x14

    invoke-virtual {v11}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v6

    invoke-virtual {v11}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v7

    invoke-virtual {v11}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v8

    invoke-virtual {v15, v5, v6, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 3111
    .end local v11    # "color":Lorg/apache/poi/java/awt/Color;
    :cond_2
    const/16 v5, 0x145

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/poi/hslf/model/AutoShape;->getEscherProperty(S)I

    move-result v26

    .line 3113
    .local v26, "value":I
    const/16 v5, 0x146

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/poi/hslf/model/AutoShape;->getSegmentType(S)[I

    move-result-object v23

    .line 3115
    .local v23, "segmentType":[I
    const/16 v5, 0x146

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/poi/hslf/model/AutoShape;->getSegmentCount(S)[I

    move-result-object v21

    .line 3119
    .local v21, "segmentCount":[I
    if-eqz v21, :cond_3

    if-nez v23, :cond_4

    .line 3293
    :cond_3
    :goto_0
    return-void

    .line 3123
    :cond_4
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 3125
    .local v4, "path":Landroid/graphics/Path;
    const/16 v18, 0x0

    .line 3127
    .local v18, "point_count":I
    if-eqz v26, :cond_f

    .line 3128
    const/16 v5, 0x145

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/apache/poi/hslf/model/AutoShape;->getVertices(S)[Landroid/graphics/Point;

    move-result-object v17

    .line 3131
    .local v17, "point":[Landroid/graphics/Point;
    if-eqz v17, :cond_3

    .line 3137
    const/16 v22, 0x0

    .local v22, "segmentInfo_count":I
    :goto_1
    move-object/from16 v0, v23

    array-length v5, v0

    move/from16 v0, v22

    if-ge v0, v5, :cond_f

    .line 3138
    aget v5, v23, v22

    packed-switch v5, :pswitch_data_0

    .line 3137
    :cond_5
    :goto_2
    :pswitch_0
    add-int/lit8 v22, v22, 0x1

    goto :goto_1

    .line 3140
    :pswitch_1
    aget v5, v21, v22

    if-nez v5, :cond_5

    .line 3141
    aget-object v5, v17, v18

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    add-int/lit8 v19, v18, 0x1

    .end local v18    # "point_count":I
    .local v19, "point_count":I
    aget-object v6, v17, v18

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v8

    double-to-float v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    move/from16 v18, v19

    .end local v19    # "point_count":I
    .restart local v18    # "point_count":I
    goto :goto_2

    .line 3150
    :goto_3
    :pswitch_2
    aget v5, v21, v22

    if-eqz v5, :cond_5

    .line 3151
    aget-object v5, v17, v18

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    add-int/lit8 v19, v18, 0x1

    .end local v18    # "point_count":I
    .restart local v19    # "point_count":I
    aget-object v6, v17, v18

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v8

    double-to-float v7, v8

    add-float/2addr v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 3156
    aget v5, v21, v22

    add-int/lit8 v5, v5, -0x1

    aput v5, v21, v22

    move/from16 v18, v19

    .end local v19    # "point_count":I
    .restart local v18    # "point_count":I
    goto :goto_3

    .line 3161
    :goto_4
    :pswitch_3
    aget v5, v21, v22

    if-eqz v5, :cond_5

    .line 3162
    aget-object v5, v17, v18

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    add-int/lit8 v19, v18, 0x1

    .end local v18    # "point_count":I
    .restart local v19    # "point_count":I
    aget-object v6, v17, v18

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v8

    double-to-float v7, v8

    add-float/2addr v6, v7

    aget-object v7, v17, v19

    iget v7, v7, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v8

    double-to-float v8, v8

    add-float/2addr v7, v8

    add-int/lit8 v18, v19, 0x1

    .end local v19    # "point_count":I
    .restart local v18    # "point_count":I
    aget-object v8, v17, v19

    iget v8, v8, Landroid/graphics/Point;->y:I

    int-to-float v8, v8

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v40

    move-wide/from16 v0, v40

    double-to-float v9, v0

    add-float/2addr v8, v9

    aget-object v9, v17, v18

    iget v9, v9, Landroid/graphics/Point;->x:I

    int-to-float v9, v9

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v40

    move-wide/from16 v0, v40

    double-to-float v10, v0

    add-float/2addr v9, v10

    add-int/lit8 v19, v18, 0x1

    .end local v18    # "point_count":I
    .restart local v19    # "point_count":I
    aget-object v10, v17, v18

    iget v10, v10, Landroid/graphics/Point;->y:I

    int-to-float v10, v10

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v40

    move-wide/from16 v0, v40

    double-to-float v0, v0

    move/from16 v27, v0

    add-float v10, v10, v27

    invoke-virtual/range {v4 .. v10}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 3179
    aget v5, v21, v22

    add-int/lit8 v5, v5, -0x1

    aput v5, v21, v22

    move/from16 v18, v19

    .end local v19    # "point_count":I
    .restart local v18    # "point_count":I
    goto :goto_4

    .line 3184
    :pswitch_4
    aget v5, v21, v22

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    .line 3185
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    goto/16 :goto_2

    .line 3193
    :pswitch_5
    aget v5, v21, v22

    shr-int/lit8 v13, v5, 0x8

    .line 3195
    .local v13, "escape_code":I
    const/4 v5, 0x4

    if-eq v13, v5, :cond_6

    const/4 v5, 0x3

    if-eq v13, v5, :cond_6

    const/4 v5, 0x6

    if-eq v13, v5, :cond_6

    const/4 v5, 0x5

    if-ne v13, v5, :cond_e

    .line 3198
    :cond_6
    new-instance v20, Landroid/graphics/RectF;

    aget-object v5, v17, v18

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    add-int/lit8 v19, v18, 0x1

    .end local v18    # "point_count":I
    .restart local v19    # "point_count":I
    aget-object v6, v17, v18

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v8

    double-to-float v7, v8

    add-float/2addr v6, v7

    aget-object v7, v17, v19

    iget v7, v7, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v8

    double-to-float v8, v8

    add-float/2addr v7, v8

    add-int/lit8 v18, v19, 0x1

    .end local v19    # "point_count":I
    .restart local v18    # "point_count":I
    aget-object v8, v17, v19

    iget v8, v8, Landroid/graphics/Point;->y:I

    int-to-float v8, v8

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v40

    move-wide/from16 v0, v40

    double-to-float v9, v0

    add-float/2addr v8, v9

    move-object/from16 v0, v20

    invoke-direct {v0, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 3206
    .local v20, "rectF":Landroid/graphics/RectF;
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->centerX()F

    move-result v5

    float-to-double v0, v5

    move-wide/from16 v32, v0

    .line 3207
    .local v32, "xc":D
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    float-to-double v0, v5

    move-wide/from16 v38, v0

    .line 3208
    .local v38, "yc":D
    aget-object v5, v17, v18

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    float-to-double v0, v5

    move-wide/from16 v28, v0

    .line 3210
    .local v28, "x1":D
    add-int/lit8 v19, v18, 0x1

    .end local v18    # "point_count":I
    .restart local v19    # "point_count":I
    aget-object v5, v17, v18

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    float-to-double v0, v5

    move-wide/from16 v34, v0

    .line 3212
    .local v34, "y1":D
    sub-double v6, v34, v38

    sub-double v8, v28, v32

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v6

    double-to-float v0, v6

    move/from16 v24, v0

    .line 3214
    .local v24, "startA":F
    aget-object v5, v17, v19

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    float-to-double v0, v5

    move-wide/from16 v30, v0

    .line 3216
    .local v30, "x2":D
    add-int/lit8 v18, v19, 0x1

    .end local v19    # "point_count":I
    .restart local v18    # "point_count":I
    aget-object v5, v17, v19

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineWidth()D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v5, v6

    float-to-double v0, v5

    move-wide/from16 v36, v0

    .line 3218
    .local v36, "y2":D
    sub-double v6, v36, v38

    sub-double v8, v30, v32

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v6

    double-to-float v12, v6

    .line 3220
    .local v12, "endA":F
    sub-float v25, v12, v24

    .line 3222
    .local v25, "sweepA":F
    const/4 v5, 0x4

    if-ne v13, v5, :cond_8

    .line 3223
    const/4 v5, 0x0

    cmpl-float v5, v25, v5

    if-lez v5, :cond_7

    .line 3224
    const/high16 v5, 0x43b40000    # 360.0f

    sub-float v25, v25, v5

    .line 3225
    :cond_7
    move-object/from16 v0, v20

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v4, v0, v1, v2}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    goto/16 :goto_2

    .line 3227
    :cond_8
    const/4 v5, 0x3

    if-ne v13, v5, :cond_a

    .line 3228
    const/4 v5, 0x0

    cmpl-float v5, v25, v5

    if-lez v5, :cond_9

    .line 3229
    const/high16 v5, 0x43b40000    # 360.0f

    sub-float v25, v25, v5

    .line 3230
    :cond_9
    move-object/from16 v0, v20

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v4, v0, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    goto/16 :goto_2

    .line 3232
    :cond_a
    const/4 v5, 0x6

    if-ne v13, v5, :cond_c

    .line 3233
    const/4 v5, 0x0

    cmpg-float v5, v25, v5

    if-gez v5, :cond_b

    .line 3234
    const/high16 v5, 0x43b40000    # 360.0f

    add-float v25, v25, v5

    .line 3235
    :cond_b
    move-object/from16 v0, v20

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v4, v0, v1, v2}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    goto/16 :goto_2

    .line 3237
    :cond_c
    const/4 v5, 0x5

    if-ne v13, v5, :cond_5

    .line 3238
    const/4 v5, 0x0

    cmpg-float v5, v25, v5

    if-gez v5, :cond_d

    .line 3239
    const/high16 v5, 0x43b40000    # 360.0f

    add-float v25, v25, v5

    .line 3240
    :cond_d
    move-object/from16 v0, v20

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v4, v0, v1, v2}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    goto/16 :goto_2

    .line 3243
    .end local v12    # "endA":F
    .end local v20    # "rectF":Landroid/graphics/RectF;
    .end local v24    # "startA":F
    .end local v25    # "sweepA":F
    .end local v28    # "x1":D
    .end local v30    # "x2":D
    .end local v32    # "xc":D
    .end local v34    # "y1":D
    .end local v36    # "y2":D
    .end local v38    # "yc":D
    :cond_e
    const/16 v5, 0x9

    if-ne v13, v5, :cond_5

    .line 3244
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_2

    .line 3262
    .end local v13    # "escape_code":I
    .end local v17    # "point":[Landroid/graphics/Point;
    .end local v22    # "segmentInfo_count":I
    :cond_f
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getFill()Lorg/apache/poi/hslf/model/Fill;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/hslf/model/Fill;->getForegroundColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    if-eqz v5, :cond_10

    .line 3263
    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v15}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 3264
    :cond_10
    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/hslf/model/AutoShape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    if-eqz v5, :cond_11

    .line 3265
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 3292
    :cond_11
    move/from16 v0, p3

    neg-int v5, v0

    int-to-float v5, v5

    move/from16 v0, p4

    neg-int v6, v0

    int-to-float v6, v6

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_0

    .line 3138
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method private drawCustomGeom(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V
    .locals 36
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "theme"    # Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .prologue
    .line 2211
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getPathDescriptor()Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    move-result-object v24

    .line 2212
    .local v24, "pathDesc":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;
    if-eqz v24, :cond_12

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->getPaths()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_12

    .line 2214
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2216
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->getHeight()I

    move-result v15

    .line 2217
    .local v15, "ht":I
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->getWidth()I

    move-result v33

    .line 2219
    .local v33, "wd":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getActualShapeHeight()J

    move-result-wide v28

    .line 2220
    .local v28, "shapeHt":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getActualShapeWidth()J

    move-result-wide v30

    .line 2222
    .local v30, "shapeWt":J
    if-gtz v15, :cond_0

    .line 2223
    const/4 v15, 0x1

    .line 2224
    :cond_0
    if-gtz v33, :cond_1

    .line 2225
    const/16 v33, 0x1

    .line 2227
    :cond_1
    move-wide/from16 v0, v30

    long-to-float v3, v0

    move/from16 v0, v33

    int-to-float v4, v0

    div-float v34, v3, v4

    .line 2228
    .local v34, "xOffset":F
    move-wide/from16 v0, v28

    long-to-float v3, v0

    int-to-float v4, v15

    div-float v35, v3, v4

    .line 2230
    .local v35, "yOffset":F
    const/4 v3, 0x0

    cmpg-float v3, v34, v3

    if-gtz v3, :cond_2

    .line 2231
    const/high16 v34, 0x3f800000    # 1.0f

    .line 2233
    :cond_2
    const/4 v3, 0x0

    cmpg-float v3, v35, v3

    if-gtz v3, :cond_3

    .line 2234
    const/high16 v35, 0x3f800000    # 1.0f

    .line 2237
    :cond_3
    new-instance v23, Landroid/graphics/Paint;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Paint;-><init>()V

    .line 2238
    .local v23, "paintLine":Landroid/graphics/Paint;
    const/4 v3, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2239
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2240
    const/high16 v3, 0x40000000    # 2.0f

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2242
    new-instance v22, Landroid/graphics/Paint;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Paint;-><init>()V

    .line 2243
    .local v22, "paintFill":Landroid/graphics/Paint;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "Freeform"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2244
    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2245
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2246
    const v3, -0xffff01

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2249
    :cond_4
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 2250
    .local v2, "path":Landroid/graphics/Path;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v19

    .line 2251
    .local v19, "lineProp":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    if-eqz v19, :cond_9

    .line 2252
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getColor()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_5

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 2254
    :cond_5
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getLineWidth()Ljava/lang/String;

    move-result-object v21

    .line 2256
    .local v21, "lnWidth":Ljava/lang/String;
    if-eqz v21, :cond_6

    invoke-static/range {v21 .. v21}, Lcom/samsung/thumbnail/util/Utils;->isValidInteger(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2257
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(I)I

    move-result v20

    .line 2259
    .local v20, "lineWidth":I
    move/from16 v0, v20

    int-to-float v3, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2263
    .end local v20    # "lineWidth":I
    :cond_6
    new-instance v17, Lorg/apache/poi/java/awt/Color;

    const/high16 v3, -0x1000000

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 2264
    .local v17, "lineClr":Lorg/apache/poi/java/awt/Color;
    const/16 v18, 0x0

    .line 2266
    .local v18, "lineColor":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 2267
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v18

    .line 2272
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "Freeform"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getColor()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 2274
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getColor()Ljava/lang/String;

    move-result-object v18

    .line 2277
    :cond_8
    if-eqz v18, :cond_9

    if-eqz p3, :cond_9

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 2279
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v17

    .line 2281
    const/16 v3, 0xff

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v4

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v5

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v6

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2286
    .end local v17    # "lineClr":Lorg/apache/poi/java/awt/Color;
    .end local v18    # "lineColor":Ljava/lang/String;
    .end local v21    # "lnWidth":Ljava/lang/String;
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v14

    .line 2287
    .local v14, "fillProp":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    if-eqz v14, :cond_c

    .line 2288
    move-object/from16 v0, p3

    invoke-static {v14, v0}, Lcom/samsung/thumbnail/office/point/shapes/PPTShapes;->getShapeColor(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)Lorg/apache/poi/java/awt/Color;

    move-result-object v13

    .line 2289
    .local v13, "fillColor":Lorg/apache/poi/java/awt/Color;
    if-eqz v13, :cond_c

    .line 2290
    invoke-virtual {v13}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v10

    .line 2292
    .local v10, "alpha":I
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getAlpha()Ljava/lang/String;

    move-result-object v12

    .line 2293
    .local v12, "fillAlpha":Ljava/lang/String;
    if-eqz v12, :cond_b

    .line 2294
    invoke-static {v12}, Lcom/samsung/thumbnail/util/Utils;->isValidInteger(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 2296
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 2297
    mul-int/lit16 v3, v10, 0x100

    const v4, 0x186a0

    div-int v10, v3, v4

    .line 2298
    if-ltz v10, :cond_a

    const/16 v3, 0xff

    if-le v10, v3, :cond_b

    .line 2299
    :cond_a
    const/16 v10, 0x80

    .line 2303
    :cond_b
    invoke-virtual {v13}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    invoke-virtual {v13}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual {v13}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v10, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2308
    .end local v10    # "alpha":I
    .end local v12    # "fillAlpha":Ljava/lang/String;
    .end local v13    # "fillColor":Lorg/apache/poi/java/awt/Color;
    :cond_c
    const/16 v25, 0x0

    .line 2310
    .local v25, "pathSegment":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getPathDescriptor()Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->getPaths()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 2311
    .local v9, "NumberOfPath":I
    const/16 v27, 0x0

    .local v27, "shapesIndex":I
    :goto_0
    move/from16 v0, v27

    if-ge v0, v9, :cond_f

    .line 2312
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->getPaths()Ljava/util/ArrayList;

    move-result-object v3

    move/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;->getSegments()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;

    iget v0, v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;->operation:I

    move/from16 v25, v0

    .line 2314
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->getPaths()Ljava/util/ArrayList;

    move-result-object v3

    move/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;->getSegments()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;

    .line 2316
    .local v26, "segmentInfo":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;->arguments:[Ljava/lang/String;

    array-length v3, v3

    new-array v11, v3, [Ljava/lang/Float;

    .line 2318
    .local v11, "coordinateValue":[Ljava/lang/Float;
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_1
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;->arguments:[Ljava/lang/String;

    array-length v3, v3

    move/from16 v0, v16

    if-ge v0, v3, :cond_e

    .line 2319
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;->arguments:[Ljava/lang/String;

    aget-object v3, v3, v16

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v32

    .line 2321
    .local v32, "val":F
    add-int/lit8 v3, v16, 0x1

    rem-int/lit8 v3, v3, 0x2

    if-nez v3, :cond_d

    .line 2322
    mul-float v32, v32, v35

    .line 2326
    :goto_2
    invoke-static/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(F)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v11, v16

    .line 2318
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 2324
    :cond_d
    mul-float v32, v32, v34

    goto :goto_2

    .line 2328
    .end local v32    # "val":F
    :cond_e
    packed-switch v25, :pswitch_data_0

    .line 2311
    :goto_3
    :pswitch_0
    add-int/lit8 v27, v27, 0x1

    goto :goto_0

    .line 2330
    :pswitch_1
    const/4 v3, 0x0

    aget-object v3, v11, v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v4, 0x1

    aget-object v4, v11, v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    goto :goto_3

    .line 2333
    :pswitch_2
    const/4 v3, 0x0

    aget-object v3, v11, v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v4, 0x1

    aget-object v4, v11, v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_3

    .line 2336
    :pswitch_3
    const/4 v3, 0x0

    aget-object v3, v11, v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v4, 0x1

    aget-object v4, v11, v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/4 v5, 0x2

    aget-object v5, v11, v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/4 v6, 0x3

    aget-object v6, v11, v6

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto :goto_3

    .line 2340
    :pswitch_4
    const/4 v3, 0x0

    aget-object v3, v11, v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v4, 0x1

    aget-object v4, v11, v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const/4 v5, 0x2

    aget-object v5, v11, v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/4 v6, 0x3

    aget-object v6, v11, v6

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    const/4 v7, 0x4

    aget-object v7, v11, v7

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    const/4 v8, 0x5

    aget-object v8, v11, v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto :goto_3

    .line 2363
    :pswitch_5
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    goto :goto_3

    .line 2369
    .end local v11    # "coordinateValue":[Ljava/lang/Float;
    .end local v16    # "i":I
    .end local v26    # "segmentInfo":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;
    :cond_f
    if-eqz v14, :cond_10

    if-eqz v22, :cond_10

    const/4 v3, 0x5

    move/from16 v0, v25

    if-eq v0, v3, :cond_10

    .line 2370
    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2372
    :cond_10
    if-eqz v23, :cond_11

    const/4 v3, 0x5

    move/from16 v0, v25

    if-eq v0, v3, :cond_11

    .line 2373
    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2375
    :cond_11
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    neg-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    neg-float v4, v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2377
    .end local v2    # "path":Landroid/graphics/Path;
    .end local v9    # "NumberOfPath":I
    .end local v14    # "fillProp":Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;
    .end local v15    # "ht":I
    .end local v19    # "lineProp":Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;
    .end local v22    # "paintFill":Landroid/graphics/Paint;
    .end local v23    # "paintLine":Landroid/graphics/Paint;
    .end local v25    # "pathSegment":I
    .end local v27    # "shapesIndex":I
    .end local v28    # "shapeHt":J
    .end local v30    # "shapeWt":J
    .end local v33    # "wd":I
    .end local v34    # "xOffset":F
    .end local v35    # "yOffset":F
    :cond_12
    return-void

    .line 2328
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private drawPaths(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;FFF)V
    .locals 36
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "pathDesc"    # Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;
    .param p3, "leftMargin"    # F
    .param p4, "topMargin"    # F
    .param p5, "ratio"    # F

    .prologue
    .line 2473
    move-object/from16 v0, p1

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2475
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->getHeight()I

    move-result v32

    invoke-static/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(I)I

    move-result v9

    .line 2476
    .local v9, "ht":I
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->getWidth()I

    move-result v32

    invoke-static/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints(I)I

    move-result v31

    .line 2478
    .local v31, "wd":I
    if-gtz v9, :cond_0

    .line 2479
    const/4 v9, 0x1

    .line 2480
    :cond_0
    if-gtz v31, :cond_1

    .line 2481
    const/16 v31, 0x1

    .line 2483
    :cond_1
    new-instance v22, Landroid/graphics/Paint;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Paint;-><init>()V

    .line 2484
    .local v22, "paintFill":Landroid/graphics/Paint;
    const/16 v32, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2485
    sget-object v32, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v22

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2487
    new-instance v23, Landroid/graphics/Paint;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Paint;-><init>()V

    .line 2488
    .local v23, "paintLine":Landroid/graphics/Paint;
    const/16 v32, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2489
    sget-object v32, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v23

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2490
    const/high16 v32, 0x40000000    # 2.0f

    move-object/from16 v0, v23

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2492
    new-instance v15, Lorg/apache/poi/java/awt/Color;

    const/high16 v32, -0x1000000

    move/from16 v0, v32

    invoke-direct {v15, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 2494
    .local v15, "lineColor":Lorg/apache/poi/java/awt/Color;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v32

    sget-object v33, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-ne v0, v1, :cond_6

    .line 2495
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v27

    .line 2496
    .local v27, "shapeProp":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStrokeColor()Ljava/lang/String;

    move-result-object v32

    if-eqz v32, :cond_6

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStrokeStatus()Z

    move-result v32

    if-eqz v32, :cond_6

    .line 2498
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStrokeColor()Ljava/lang/String;

    move-result-object v7

    .line 2499
    .local v7, "clrStr":Ljava/lang/String;
    const-string/jumbo v32, "#"

    const-string/jumbo v33, ""

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 2501
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getLineColorLumMod()J

    move-result-wide v16

    .line 2502
    .local v16, "mod":J
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getLineColorLumOff()J

    move-result-wide v20

    .line 2503
    .local v20, "off":J
    add-long v18, v16, v20

    .line 2505
    .local v18, "modOff":J
    const-wide/16 v32, 0x0

    cmp-long v32, v18, v32

    if-eqz v32, :cond_2

    .line 2506
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v30

    .line 2507
    .local v30, "value":Ljava/lang/String;
    invoke-static/range {v30 .. v30}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v12

    .line 2508
    .local v12, "lMod":D
    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-static {v7, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcLuminousColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v7

    .line 2511
    .end local v12    # "lMod":D
    .end local v30    # "value":Ljava/lang/String;
    :cond_2
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getLineColorSatMod()J

    move-result-wide v16

    .line 2512
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getLineColorSatOff()J

    move-result-wide v20

    .line 2513
    add-long v18, v16, v20

    .line 2514
    const-wide/16 v32, 0x0

    cmp-long v32, v18, v32

    if-eqz v32, :cond_3

    .line 2515
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v30

    .line 2516
    .restart local v30    # "value":Ljava/lang/String;
    invoke-static/range {v30 .. v30}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v12

    .line 2517
    .restart local v12    # "lMod":D
    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-static {v7, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcSaturationColor(Ljava/lang/String;Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v7

    .line 2520
    .end local v12    # "lMod":D
    .end local v30    # "value":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getLineColorShade()J

    move-result-wide v32

    const-wide/16 v34, 0x0

    cmp-long v32, v32, v34

    if-eqz v32, :cond_4

    .line 2521
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillColorShade()J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v30

    .line 2522
    .restart local v30    # "value":Ljava/lang/String;
    move-object/from16 v0, v30

    invoke-static {v7, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calculateShadeColor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2525
    .end local v30    # "value":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getLineColorTint()J

    move-result-wide v32

    const-wide/16 v34, 0x0

    cmp-long v32, v32, v34

    if-eqz v32, :cond_5

    .line 2526
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getFillColorTint()J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v30

    .line 2527
    .restart local v30    # "value":Ljava/lang/String;
    move-object/from16 v0, v30

    invoke-static {v7, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->calcColorTint(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2530
    .end local v30    # "value":Ljava/lang/String;
    :cond_5
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v33, 0x23

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->stringToColor(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v15

    .line 2534
    .end local v7    # "clrStr":Ljava/lang/String;
    .end local v16    # "mod":J
    .end local v18    # "modOff":J
    .end local v20    # "off":J
    .end local v27    # "shapeProp":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v32

    sget-object v33, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-ne v0, v1, :cond_8

    .line 2535
    const/16 v29, 0x0

    .line 2536
    .local v29, "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v32, v0

    if-eqz v32, :cond_a

    .line 2537
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v29

    .line 2541
    :cond_7
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v32, v0

    if-eqz v32, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v32

    if-eqz v32, :cond_8

    .line 2542
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v32

    if-eqz v32, :cond_8

    .line 2543
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->getFillProp()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getForeColor()Ljava/lang/String;

    move-result-object v14

    .line 2546
    .local v14, "lineCol":Ljava/lang/String;
    if-eqz v29, :cond_8

    move-object/from16 v0, v29

    invoke-virtual {v0, v14}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    if-eqz v32, :cond_8

    .line 2550
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v33, 0x23

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v29

    invoke-virtual {v0, v14}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorFromClrMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Lorg/apache/poi/java/awt/Color;->decode(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v15

    .line 2557
    .end local v14    # "lineCol":Ljava/lang/String;
    .end local v29    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    :cond_8
    const/16 v32, 0x4d

    invoke-virtual {v15}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v33

    invoke-virtual {v15}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v34

    invoke-virtual {v15}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v35

    move-object/from16 v0, v23

    move/from16 v1, v32

    move/from16 v2, v33

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2560
    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v32

    move/from16 v2, v33

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 2562
    new-instance v24, Landroid/graphics/Path;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/Path;-><init>()V

    .line 2564
    .local v24, "path":Landroid/graphics/Path;
    const/16 v25, 0x0

    .line 2565
    .local v25, "pathSegment":I
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->getPaths()Ljava/util/ArrayList;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 2566
    .local v6, "NumberOfPath":I
    const/4 v11, 0x1

    .line 2568
    .local v11, "isFirstTime":Z
    const/16 v28, 0x0

    .local v28, "shapesIndex":I
    :goto_1
    move/from16 v0, v28

    if-ge v0, v6, :cond_d

    .line 2569
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->getPaths()Ljava/util/ArrayList;

    move-result-object v32

    move-object/from16 v0, v32

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;->getSegments()Ljava/util/ArrayList;

    move-result-object v32

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;

    move-object/from16 v0, v32

    iget v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;->operation:I

    move/from16 v25, v0

    .line 2572
    const/16 v32, 0x5

    move/from16 v0, v25

    move/from16 v1, v32

    if-ne v0, v1, :cond_9

    .line 2573
    if-eqz v11, :cond_c

    .line 2574
    const/4 v11, 0x0

    .line 2580
    :cond_9
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;->getPaths()Ljava/util/ArrayList;

    move-result-object v32

    move-object/from16 v0, v32

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$Path;->getSegments()Ljava/util/ArrayList;

    move-result-object v32

    const/16 v33, 0x0

    invoke-virtual/range {v32 .. v33}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;

    .line 2582
    .local v26, "segmentInfo":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;->arguments:[Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    array-length v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    new-array v8, v0, [Ljava/lang/Float;

    .line 2584
    .local v8, "coordinateValue":[Ljava/lang/Float;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_2
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;->arguments:[Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    array-length v0, v0

    move/from16 v32, v0

    move/from16 v0, v32

    if-ge v10, v0, :cond_b

    .line 2590
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;->arguments:[Ljava/lang/String;

    move-object/from16 v32, v0

    aget-object v32, v32, v10

    invoke-static/range {v32 .. v32}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v32

    invoke-static/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->emuToPoints1(F)F

    move-result v32

    mul-float v32, v32, p5

    invoke-static/range {v32 .. v32}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v32

    aput-object v32, v8, v10

    .line 2584
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 2538
    .end local v6    # "NumberOfPath":I
    .end local v8    # "coordinateValue":[Ljava/lang/Float;
    .end local v10    # "i":I
    .end local v11    # "isFirstTime":Z
    .end local v24    # "path":Landroid/graphics/Path;
    .end local v25    # "pathSegment":I
    .end local v26    # "segmentInfo":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;
    .end local v28    # "shapesIndex":I
    .restart local v29    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v32, v0

    if-eqz v32, :cond_7

    .line 2539
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v29

    goto/16 :goto_0

    .line 2596
    .end local v29    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    .restart local v6    # "NumberOfPath":I
    .restart local v8    # "coordinateValue":[Ljava/lang/Float;
    .restart local v10    # "i":I
    .restart local v11    # "isFirstTime":Z
    .restart local v24    # "path":Landroid/graphics/Path;
    .restart local v25    # "pathSegment":I
    .restart local v26    # "segmentInfo":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;
    .restart local v28    # "shapesIndex":I
    :cond_b
    packed-switch v25, :pswitch_data_0

    .line 2568
    .end local v8    # "coordinateValue":[Ljava/lang/Float;
    .end local v10    # "i":I
    .end local v26    # "segmentInfo":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;
    :cond_c
    :goto_3
    :pswitch_0
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_1

    .line 2598
    .restart local v8    # "coordinateValue":[Ljava/lang/Float;
    .restart local v10    # "i":I
    .restart local v26    # "segmentInfo":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;
    :pswitch_1
    const/16 v32, 0x0

    aget-object v32, v8, v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Float;->floatValue()F

    move-result v32

    const/16 v33, 0x1

    aget-object v33, v8, v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/Float;->floatValue()F

    move-result v33

    move-object/from16 v0, v24

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    goto :goto_3

    .line 2601
    :pswitch_2
    const/16 v32, 0x0

    aget-object v32, v8, v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Float;->floatValue()F

    move-result v32

    const/16 v33, 0x1

    aget-object v33, v8, v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/Float;->floatValue()F

    move-result v33

    move-object/from16 v0, v24

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_3

    .line 2631
    :pswitch_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    move/from16 v32, v0

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v32, v0

    const/16 v33, 0x0

    aget-object v33, v8, v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/Float;->floatValue()F

    move-result v33

    add-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    move/from16 v33, v0

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    const/16 v34, 0x1

    aget-object v34, v8, v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/Float;->floatValue()F

    move-result v34

    add-float v33, v33, v34

    const/16 v34, 0x0

    aget-object v34, v8, v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/Float;->floatValue()F

    move-result v34

    move-object/from16 v0, p1

    move/from16 v1, v32

    move/from16 v2, v33

    move/from16 v3, v34

    move-object/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_3

    .line 2639
    :pswitch_4
    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Path;->close()V

    goto :goto_3

    .line 2646
    .end local v8    # "coordinateValue":[Ljava/lang/Float;
    .end local v10    # "i":I
    .end local v26    # "segmentInfo":Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor$SegmentInfo;
    :cond_d
    const/16 v32, 0x5

    move/from16 v0, v25

    move/from16 v1, v32

    if-eq v0, v1, :cond_e

    .line 2647
    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2650
    :cond_e
    move/from16 v0, p3

    neg-float v0, v0

    move/from16 v32, v0

    move/from16 v0, p4

    neg-float v0, v0

    move/from16 v33, v0

    move-object/from16 v0, p1

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2651
    return-void

    .line 2596
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private drawText(Landroid/graphics/Canvas;IIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 20
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "textAreaWidth"    # I
    .param p5, "elementCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 2383
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    if-eqz v4, :cond_0

    .line 2384
    move/from16 v0, p2

    int-to-float v4, v0

    move/from16 v0, p3

    int-to-float v5, v0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    int-to-long v6, v6

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lcom/samsung/thumbnail/customview/word/ShapesController;->rotateCanvas(Landroid/graphics/Canvas;FFJ)V

    .line 2387
    :cond_0
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 2389
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    if-nez v4, :cond_2

    .line 2468
    :cond_1
    :goto_0
    return-void

    .line 2392
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .line 2394
    .local v18, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2395
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 2397
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 2399
    .local v3, "shapePara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    if-eqz v3, :cond_1

    .line 2402
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    int-to-float v4, v4

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTopSpacing()F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 2404
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    .line 2405
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 2406
    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->SHAPE:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParentType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;)V

    .line 2407
    new-instance v2, Lcom/samsung/thumbnail/customview/word/LineBreaker;

    invoke-direct {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;-><init>()V

    .line 2409
    .local v2, "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    move/from16 v7, p4

    move/from16 v8, p4

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I

    .line 2412
    new-instance v7, Landroid/graphics/Rect;

    sget v4, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    sget v5, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v6

    float-to-int v6, v6

    sget v8, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_BOTTOM:I

    invoke-direct {v7, v4, v5, v6, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2416
    .local v7, "rect":Landroid/graphics/Rect;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-eq v4, v5, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-eq v4, v5, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne v4, v5, :cond_5

    .line 2419
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x1

    move-object v4, v3

    move/from16 v8, p4

    move/from16 v9, p4

    move-object/from16 v13, p5

    invoke-virtual/range {v3 .. v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 2423
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 2424
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTextHeight()I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    sub-int/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTextHeight(I)V

    .line 2425
    const/16 v19, 0x0

    .line 2426
    .local v19, "verticalAlignOffset":I
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getVerticalAlignmentElement()Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    if-ne v4, v5, :cond_7

    .line 2427
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTextHeight()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v5

    float-to-int v5, v5

    if-le v4, v5, :cond_6

    .line 2428
    const/16 v19, 0x0

    .line 2441
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    add-int v4, v4, v19

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    .line 2444
    .end local v19    # "verticalAlignOffset":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x0

    move-object v4, v3

    move/from16 v8, p4

    move/from16 v9, p4

    move-object/from16 v13, p5

    invoke-virtual/range {v3 .. v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 2448
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getY()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 2449
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getX()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 2450
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    int-to-float v4, v4

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBottomSpacing()F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    goto/16 :goto_1

    .line 2430
    .restart local v19    # "verticalAlignOffset":I
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v4

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTextHeight()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v19, v0

    goto :goto_2

    .line 2433
    :cond_7
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getVerticalAlignmentElement()Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    if-ne v4, v5, :cond_4

    .line 2434
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTextHeight()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v5

    float-to-int v5, v5

    if-le v4, v5, :cond_8

    .line 2435
    const/16 v19, 0x0

    goto :goto_2

    .line 2437
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v4

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTextHeight()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v19, v0

    goto :goto_2

    .line 2453
    .end local v2    # "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    .end local v3    # "shapePara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v7    # "rect":Landroid/graphics/Rect;
    .end local v19    # "verticalAlignOffset":I
    :cond_9
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-nez v4, :cond_1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    if-eqz v4, :cond_1

    .line 2454
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 2455
    const/16 v16, 0x0

    .line 2456
    .local v16, "dx":F
    const/16 v17, 0x0

    .line 2457
    .local v17, "dy":F
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 2458
    .local v14, "actualheight":F
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 2459
    .local v15, "actualwidth":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    const/16 v5, 0x2d

    if-lt v4, v5, :cond_a

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    const/16 v5, 0x87

    if-le v4, v5, :cond_b

    :cond_a
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    const/16 v5, -0x2d

    if-gt v4, v5, :cond_1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    const/16 v5, -0x87

    if-lt v4, v5, :cond_1

    .line 2461
    :cond_b
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 2462
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 2463
    sub-float v4, v14, v15

    const/high16 v5, 0x40000000    # 2.0f

    div-float v16, v4, v5

    .line 2464
    sub-float v4, v15, v14

    const/high16 v5, 0x40000000    # 2.0f

    div-float v17, v4, v5

    .line 2465
    move/from16 v0, v16

    neg-float v4, v0

    move/from16 v0, v17

    neg-float v5, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_0
.end method

.method private drawText(Landroid/graphics/Canvas;IIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;I)V
    .locals 16
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "textAreaWidth"    # I
    .param p5, "elementCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .param p6, "textHeight"    # I

    .prologue
    .line 2746
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    if-eqz v4, :cond_0

    .line 2747
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 2748
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    long-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v6, v8

    add-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    add-float/2addr v6, v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 2753
    :cond_0
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 2755
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    if-nez v4, :cond_2

    .line 2819
    :cond_1
    :goto_0
    return-void

    .line 2758
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 2759
    .local v14, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2760
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 2762
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 2764
    .local v3, "shapePara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    if-eqz v3, :cond_1

    .line 2767
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    int-to-float v4, v4

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTopSpacing()F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 2769
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    .line 2770
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 2771
    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->SHAPE:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParentType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;)V

    .line 2772
    new-instance v2, Lcom/samsung/thumbnail/customview/word/LineBreaker;

    invoke-direct {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;-><init>()V

    .line 2774
    .local v2, "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    move/from16 v7, p4

    move/from16 v8, p4

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I

    .line 2777
    new-instance v7, Landroid/graphics/Rect;

    sget v4, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    sget v5, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v6

    float-to-int v6, v6

    sget v8, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_BOTTOM:I

    invoke-direct {v7, v4, v5, v6, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2780
    .local v7, "rect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x1

    move-object v4, v3

    move/from16 v8, p4

    move/from16 v9, p4

    move-object/from16 v13, p5

    invoke-virtual/range {v3 .. v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 2783
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 2785
    const/4 v15, 0x0

    .line 2786
    .local v15, "verticalAlignOffset":I
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getVerticalAlignmentElement()Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    if-ne v4, v5, :cond_5

    .line 2787
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v4

    float-to-int v4, v4

    move/from16 v0, p6

    if-le v0, v4, :cond_4

    .line 2788
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v5

    float-to-int v5, v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    div-int/lit8 v5, p6, 0x2

    sub-int v15, v4, v5

    .line 2803
    :cond_3
    :goto_2
    if-eqz v15, :cond_7

    .line 2804
    invoke-virtual {v3, v15}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    .line 2807
    :goto_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x0

    move-object v4, v3

    move/from16 v8, p4

    move/from16 v9, p4

    move-object/from16 v13, p5

    invoke-virtual/range {v3 .. v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 2811
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getY()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 2812
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getX()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 2813
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    int-to-float v4, v4

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBottomSpacing()F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    goto/16 :goto_1

    .line 2791
    :cond_4
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v5

    move/from16 v0, p6

    int-to-float v6, v0

    sub-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    float-to-int v5, v5

    add-int v15, v4, v5

    goto :goto_2

    .line 2794
    :cond_5
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getVerticalAlignmentElement()Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    if-ne v4, v5, :cond_3

    .line 2795
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v4

    float-to-int v4, v4

    move/from16 v0, p6

    if-le v0, v4, :cond_6

    .line 2796
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v5

    float-to-int v5, v5

    add-int/2addr v4, v5

    sub-int v15, v4, p6

    goto :goto_2

    .line 2799
    :cond_6
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v5

    move/from16 v0, p6

    int-to-float v6, v0

    sub-float/2addr v5, v6

    float-to-int v5, v5

    add-int v15, v4, v5

    goto/16 :goto_2

    .line 2806
    :cond_7
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    goto/16 :goto_3

    .line 2816
    .end local v2    # "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    .end local v3    # "shapePara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v7    # "rect":Landroid/graphics/Rect;
    .end local v15    # "verticalAlignOffset":I
    :cond_8
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-nez v4, :cond_1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    if-eqz v4, :cond_1

    .line 2817
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method

.method private drawTextDummy(Ljava/util/ArrayList;IIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Landroid/graphics/Canvas;)I
    .locals 17
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "textAreaWidth"    # I
    .param p5, "elementCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .param p6, "dummyCanvas"    # Landroid/graphics/Canvas;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;III",
            "Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;",
            "Landroid/graphics/Canvas;",
            ")I"
        }
    .end annotation

    .prologue
    .line 2827
    .local p1, "mParagraphArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 2829
    if-nez p1, :cond_1

    .line 2830
    const/4 v15, 0x0

    .line 2888
    :cond_0
    :goto_0
    return v15

    .line 2832
    :cond_1
    const/4 v15, 0x0

    .line 2833
    .local v15, "textHeight":I
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 2834
    .local v14, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2835
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 2837
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 2839
    .local v3, "shapePara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    if-nez v3, :cond_2

    .line 2840
    const/4 v15, 0x0

    goto :goto_0

    .line 2842
    :cond_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    int-to-float v4, v4

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTopSpacing()F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 2844
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    .line 2845
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 2846
    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->SHAPE:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParentType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;)V

    .line 2847
    new-instance v2, Lcom/samsung/thumbnail/customview/word/LineBreaker;

    invoke-direct {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;-><init>()V

    .line 2849
    .local v2, "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    move/from16 v7, p4

    move/from16 v8, p4

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I

    .line 2852
    new-instance v7, Landroid/graphics/Rect;

    sget v4, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    sget v5, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v6

    float-to-int v6, v6

    sget v8, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_BOTTOM:I

    invoke-direct {v7, v4, v5, v6, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2854
    .local v7, "rect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x1

    move-object v4, v3

    move-object/from16 v5, p6

    move/from16 v8, p4

    move/from16 v9, p4

    move-object/from16 v13, p5

    invoke-virtual/range {v3 .. v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 2858
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTextHeight()I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    sub-int/2addr v4, v5

    add-int/2addr v15, v4

    .line 2859
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 2860
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTextHeight()I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    sub-int/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTextHeight(I)V

    .line 2861
    const/16 v16, 0x0

    .line 2862
    .local v16, "verticalAlignOffset":I
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getVerticalAlignmentElement()Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    if-ne v4, v5, :cond_5

    .line 2863
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTextHeight()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v5

    float-to-int v5, v5

    if-le v4, v5, :cond_4

    .line 2864
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v4

    float-to-int v4, v4

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTextHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int v16, v4, v5

    .line 2879
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    add-int v4, v4, v16

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    .line 2880
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x0

    move-object v4, v3

    move-object/from16 v5, p6

    move/from16 v8, p4

    move/from16 v9, p4

    move-object/from16 v13, p5

    invoke-virtual/range {v3 .. v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 2884
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getY()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 2885
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getX()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 2886
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    int-to-float v4, v4

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBottomSpacing()F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    goto/16 :goto_1

    .line 2867
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v4

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTextHeight()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v16, v0

    goto :goto_2

    .line 2870
    :cond_5
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getVerticalAlignmentElement()Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    if-ne v4, v5, :cond_3

    .line 2871
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTextHeight()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v5

    float-to-int v5, v5

    if-le v4, v5, :cond_6

    .line 2872
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTextHeight()I

    move-result v5

    sub-int v16, v4, v5

    goto :goto_2

    .line 2875
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v4

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTextHeight()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v16, v0

    goto :goto_2
.end method

.method private drawTextPPT(IIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 21
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "textAreaWidth"    # I
    .param p4, "elementCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 2658
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 2659
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    if-nez v4, :cond_1

    .line 2738
    :cond_0
    :goto_0
    return-void

    .line 2662
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .line 2663
    .local v17, "iteratorforHeight":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    const/16 v18, 0x0

    .line 2664
    .local v18, "textheight":I
    const/4 v15, 0x0

    .line 2665
    .local v15, "dummytogetalignments":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    :cond_2
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2666
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 2667
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 2668
    .local v3, "shapePara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    if-eqz v3, :cond_2

    .line 2670
    move-object v15, v3

    .line 2671
    new-instance v2, Lcom/samsung/thumbnail/customview/word/LineBreaker;

    invoke-direct {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;-><init>()V

    .line 2673
    .local v2, "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    move/from16 v7, p3

    move/from16 v8, p3

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I

    .line 2675
    new-instance v7, Landroid/graphics/Rect;

    sget v4, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    sget v5, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v6

    float-to-int v6, v6

    sget v8, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_BOTTOM:I

    invoke-direct {v7, v4, v5, v6, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2677
    .local v7, "rect":Landroid/graphics/Rect;
    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    .line 2678
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x1

    move-object v4, v3

    move/from16 v8, p3

    move/from16 v9, p3

    move-object/from16 v13, p4

    invoke-virtual/range {v3 .. v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 2681
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 2682
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getY()I

    move-result v4

    add-int v18, v18, v4

    .line 2683
    goto :goto_1

    .line 2686
    .end local v2    # "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    .end local v3    # "shapePara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v7    # "rect":Landroid/graphics/Rect;
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    move/from16 v19, v0

    .line 2687
    .local v19, "verticalAlignOffset":I
    if-eqz v15, :cond_5

    .line 2688
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getVerticalAlignmentElement()Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-result-object v20

    .line 2690
    .local v20, "verticalAlignType":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, v20

    if-ne v0, v4, :cond_7

    .line 2691
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v4

    float-to-int v4, v4

    move/from16 v0, v18

    if-le v0, v4, :cond_6

    .line 2692
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v5

    float-to-int v5, v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    div-int/lit8 v5, v18, 0x2

    sub-int v19, v4, v5

    .line 2707
    :cond_4
    :goto_2
    move/from16 v0, v19

    int-to-float v4, v0

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTopSpacing()F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v19, v0

    .line 2710
    .end local v20    # "verticalAlignType":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    :cond_5
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 2712
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .line 2713
    .local v16, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2714
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    .line 2715
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 2716
    .restart local v3    # "shapePara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    if-eqz v3, :cond_0

    .line 2718
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    .line 2719
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 2720
    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->SHAPE:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParentType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;)V

    .line 2721
    new-instance v2, Lcom/samsung/thumbnail/customview/word/LineBreaker;

    invoke-direct {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;-><init>()V

    .line 2723
    .restart local v2    # "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, -0x1

    move-object v8, v2

    move-object v9, v3

    move/from16 v13, p3

    move/from16 v14, p3

    invoke-virtual/range {v8 .. v14}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I

    .line 2725
    new-instance v7, Landroid/graphics/Rect;

    sget v4, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    sget v5, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getRightMargin()F

    move-result v6

    float-to-int v6, v6

    sget v8, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_BOTTOM:I

    invoke-direct {v7, v4, v5, v6, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2727
    .restart local v7    # "rect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    .line 2728
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x0

    move-object v4, v3

    move/from16 v8, p3

    move/from16 v9, p3

    move-object/from16 v13, p4

    invoke-virtual/range {v3 .. v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 2731
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getY()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    .line 2732
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getX()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->x:I

    goto :goto_3

    .line 2695
    .end local v2    # "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    .end local v3    # "shapePara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v7    # "rect":Landroid/graphics/Rect;
    .end local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    .restart local v20    # "verticalAlignType":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    :cond_6
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v5

    move/from16 v0, v18

    int-to-float v6, v0

    sub-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    float-to-int v5, v5

    add-int v19, v4, v5

    goto/16 :goto_2

    .line 2698
    :cond_7
    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, v20

    if-ne v0, v4, :cond_4

    .line 2699
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v4

    float-to-int v4, v4

    move/from16 v0, v18

    if-le v0, v4, :cond_8

    .line 2700
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v5

    float-to-int v5, v5

    add-int/2addr v4, v5

    sub-int v19, v4, v18

    goto/16 :goto_2

    .line 2703
    :cond_8
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v5

    move/from16 v0, v18

    int-to-float v6, v0

    sub-float/2addr v5, v6

    float-to-int v5, v5

    add-int v19, v4, v5

    goto/16 :goto_2

    .line 2735
    .end local v20    # "verticalAlignType":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    .restart local v16    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    :cond_9
    if-eqz v15, :cond_0

    .line 2736
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    int-to-float v4, v4

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBottomSpacing()F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    goto/16 :goto_0
.end method

.method private getHorizontalAlign(Lcom/samsung/thumbnail/office/word/Page;I)I
    .locals 4
    .param p1, "currentPage"    # Lcom/samsung/thumbnail/office/word/Page;
    .param p2, "x"    # I

    .prologue
    .line 461
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHorAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 463
    sget-object v0, Lcom/samsung/thumbnail/customview/word/ShapesController$1;->$SwitchMap$com$samsung$thumbnail$customview$CanvasElementPart$HorizontalAlignmentElement:[I

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHorAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 615
    :cond_0
    :goto_0
    :pswitch_0
    return p2

    .line 465
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    if-eqz v0, :cond_0

    .line 466
    sget-object v0, Lcom/samsung/thumbnail/customview/word/ShapesController$1;->$SwitchMap$com$samsung$thumbnail$customview$CanvasElementPart$HorizontalRelativeElement:[I

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 468
    :pswitch_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v0

    float-to-int p2, v0

    .line 469
    goto :goto_0

    .line 472
    :pswitch_3
    const/4 p2, 0x0

    .line 474
    goto :goto_0

    .line 482
    :pswitch_4
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int p2, v0

    .line 483
    goto :goto_0

    .line 485
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    double-to-int p2, v0

    .line 488
    goto :goto_0

    .line 499
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    if-eqz v0, :cond_0

    .line 500
    sget-object v0, Lcom/samsung/thumbnail/customview/word/ShapesController$1;->$SwitchMap$com$samsung$thumbnail$customview$CanvasElementPart$HorizontalRelativeElement:[I

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 502
    :pswitch_7
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    double-to-int v0, v0

    shr-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v1

    float-to-int v1, v1

    shr-int/lit8 v1, v1, 0x1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v1

    add-float/2addr v0, v1

    float-to-int p2, v0

    .line 508
    goto :goto_0

    .line 511
    :pswitch_8
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v0

    double-to-int v0, v0

    shr-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v1

    float-to-int v1, v1

    shr-int/lit8 v1, v1, 0x1

    sub-int p2, v0, v1

    .line 514
    goto/16 :goto_0

    .line 522
    :pswitch_9
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    shr-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int p2, v0

    .line 524
    goto/16 :goto_0

    .line 526
    :pswitch_a
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    shr-int/lit8 v2, v2, 0x1

    int-to-double v2, v2

    add-double/2addr v0, v2

    double-to-int p2, v0

    .line 530
    goto/16 :goto_0

    .line 541
    :pswitch_b
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    if-eqz v0, :cond_0

    .line 542
    sget-object v0, Lcom/samsung/thumbnail/customview/word/ShapesController$1;->$SwitchMap$com$samsung$thumbnail$customview$CanvasElementPart$HorizontalRelativeElement:[I

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_0

    .line 544
    :pswitch_c
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v2

    float-to-double v2, v2

    add-double/2addr v0, v2

    double-to-int p2, v0

    .line 549
    goto/16 :goto_0

    .line 552
    :pswitch_d
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    double-to-int p2, v0

    .line 554
    goto/16 :goto_0

    .line 577
    :pswitch_e
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    if-eqz v0, :cond_0

    .line 578
    sget-object v0, Lcom/samsung/thumbnail/customview/word/ShapesController$1;->$SwitchMap$com$samsung$thumbnail$customview$CanvasElementPart$HorizontalRelativeElement:[I

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto/16 :goto_0

    .line 584
    :pswitch_f
    int-to-float v0, p2

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int p2, v0

    .line 586
    goto/16 :goto_0

    .line 598
    :pswitch_10
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v2

    float-to-double v2, v2

    add-double/2addr v0, v2

    double-to-int p2, v0

    .line 601
    goto/16 :goto_0

    .line 463
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_6
        :pswitch_b
        :pswitch_e
    .end packed-switch

    .line 466
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch

    .line 500
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_0
    .end packed-switch

    .line 542
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 578
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_10
        :pswitch_0
    .end packed-switch
.end method

.method private getVerticalAlign(Lcom/samsung/thumbnail/office/word/Page;I)I
    .locals 4
    .param p1, "currentPage"    # Lcom/samsung/thumbnail/office/word/Page;
    .param p2, "y"    # I

    .prologue
    .line 363
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 364
    sget-object v0, Lcom/samsung/thumbnail/customview/word/ShapesController$1;->$SwitchMap$com$samsung$thumbnail$customview$CanvasElementPart$VerticalAlignmentElement:[I

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 457
    :cond_0
    :goto_0
    :pswitch_0
    return p2

    .line 366
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    if-eqz v0, :cond_0

    .line 367
    sget-object v0, Lcom/samsung/thumbnail/customview/word/ShapesController$1;->$SwitchMap$com$samsung$thumbnail$customview$CanvasElementPart$VerticalRelativeElement:[I

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 369
    :pswitch_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getTopMargin()F

    move-result v0

    float-to-int p2, v0

    .line 370
    goto :goto_0

    .line 372
    :pswitch_3
    const/4 p2, 0x0

    .line 373
    goto :goto_0

    .line 390
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    if-eqz v0, :cond_0

    .line 391
    sget-object v0, Lcom/samsung/thumbnail/customview/word/ShapesController$1;->$SwitchMap$com$samsung$thumbnail$customview$CanvasElementPart$VerticalRelativeElement:[I

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 393
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getTopMargin()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getBottomMargin()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    double-to-int v0, v0

    shr-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v1

    float-to-int v1, v1

    shr-int/lit8 v1, v1, 0x1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getTopMargin()F

    move-result v1

    add-float/2addr v0, v1

    float-to-int p2, v0

    .line 399
    goto :goto_0

    .line 401
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v0

    double-to-int v0, v0

    shr-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v1

    float-to-int v1, v1

    shr-int/lit8 v1, v1, 0x1

    sub-int p2, v0, v1

    .line 403
    goto :goto_0

    .line 420
    :pswitch_7
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    if-eqz v0, :cond_0

    .line 421
    sget-object v0, Lcom/samsung/thumbnail/customview/word/ShapesController$1;->$SwitchMap$com$samsung$thumbnail$customview$CanvasElementPart$VerticalRelativeElement:[I

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 423
    :pswitch_8
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/word/Page;->getBottomMargin()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    double-to-int p2, v0

    .line 426
    goto/16 :goto_0

    .line 428
    :pswitch_9
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v2

    float-to-double v2, v2

    sub-double/2addr v0, v2

    double-to-int p2, v0

    .line 430
    goto/16 :goto_0

    .line 364
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_7
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 367
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 391
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 421
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private init()V
    .locals 2

    .prologue
    .line 324
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    .line 325
    new-instance v0, Lcom/samsung/thumbnail/customview/word/TextBoxController;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/word/TextBoxController;-><init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextBoxContents:Lcom/samsung/thumbnail/customview/word/TextBoxController;

    .line 326
    return-void
.end method

.method private restoreCanvas(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2158
    :try_start_0
    iget-object v5, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v5

    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne v5, v6, :cond_1

    .line 2161
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2207
    :cond_0
    :goto_0
    return-void

    .line 2162
    :cond_1
    iget-object v5, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isFlipV()Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v5

    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne v5, v6, :cond_3

    .line 2166
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2204
    :catch_0
    move-exception v4

    .line 2205
    .local v4, "e":Ljava/lang/Exception;
    const-string/jumbo v5, "ShapesController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2167
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    iget-wide v6, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_7

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v5

    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-eq v5, v6, :cond_4

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v5

    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne v5, v6, :cond_7

    .line 2169
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2170
    const/4 v2, 0x0

    .line 2171
    .local v2, "dx":F
    const/4 v3, 0x0

    .line 2172
    .local v3, "dy":F
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 2173
    .local v0, "actualheight":F
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 2174
    .local v1, "actualwidth":F
    iget-wide v6, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v8, 0x2d

    cmp-long v5, v6, v8

    if-ltz v5, :cond_5

    iget-wide v6, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v8, 0x87

    cmp-long v5, v6, v8

    if-lez v5, :cond_6

    :cond_5
    iget-wide v6, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v8, -0x2d

    cmp-long v5, v6, v8

    if-gtz v5, :cond_0

    iget-wide v6, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v8, -0x87

    cmp-long v5, v6, v8

    if-ltz v5, :cond_0

    .line 2176
    :cond_6
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 2177
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 2178
    sub-float v5, v0, v1

    const/high16 v6, 0x40000000    # 2.0f

    div-float v2, v5, v6

    .line 2179
    sub-float v5, v1, v0

    const/high16 v6, 0x40000000    # 2.0f

    div-float v3, v5, v6

    .line 2180
    neg-float v5, v2

    neg-float v6, v3

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_0

    .line 2182
    .end local v0    # "actualheight":F
    .end local v1    # "actualwidth":F
    .end local v2    # "dx":F
    .end local v3    # "dy":F
    :cond_7
    iget-object v5, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getRotationAngle()I

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v5

    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne v5, v6, :cond_8

    .line 2184
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0

    .line 2185
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v5

    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    if-ne v5, v6, :cond_0

    .line 2186
    iget-object v5, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->shape:Lorg/apache/poi/hslf/model/Shape;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->shape:Lorg/apache/poi/hslf/model/Shape;

    instance-of v5, v5, Lorg/apache/poi/hslf/model/AutoShape;

    if-eqz v5, :cond_0

    .line 2187
    iget-wide v6, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 2188
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2189
    const/4 v2, 0x0

    .line 2190
    .restart local v2    # "dx":F
    const/4 v3, 0x0

    .line 2191
    .restart local v3    # "dy":F
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 2192
    .restart local v0    # "actualheight":F
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 2193
    .restart local v1    # "actualwidth":F
    iget-wide v6, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v8, 0x2d

    cmp-long v5, v6, v8

    if-ltz v5, :cond_9

    iget-wide v6, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v8, 0x87

    cmp-long v5, v6, v8

    if-lez v5, :cond_a

    :cond_9
    iget-wide v6, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v8, -0x2d

    cmp-long v5, v6, v8

    if-gtz v5, :cond_0

    iget-wide v6, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v8, -0x87

    cmp-long v5, v6, v8

    if-ltz v5, :cond_0

    .line 2195
    :cond_a
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 2196
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 2197
    sub-float v5, v0, v1

    const/high16 v6, 0x40000000    # 2.0f

    div-float v2, v5, v6

    .line 2198
    sub-float v5, v1, v0

    const/high16 v6, 0x40000000    # 2.0f

    div-float v3, v5, v6

    .line 2199
    neg-float v5, v2

    neg-float v6, v3

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private rotateCanvas(Landroid/graphics/Canvas;FFJ)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "leftMargin"    # F
    .param p3, "topMargin"    # F
    .param p4, "rotationAngle"    # J

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 2136
    const-wide/16 v4, 0x0

    cmp-long v4, p4, v4

    if-eqz v4, :cond_3

    .line 2137
    const/4 v2, 0x0

    .line 2138
    .local v2, "dx":F
    const/4 v3, 0x0

    .line 2139
    .local v3, "dy":F
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 2140
    .local v0, "actualheight":F
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 2141
    .local v1, "actualwidth":F
    iget-wide v4, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v6, 0x2d

    cmp-long v4, v4, v6

    if-ltz v4, :cond_0

    iget-wide v4, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v6, 0x87

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    :cond_0
    iget-wide v4, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v6, -0x2d

    cmp-long v4, v4, v6

    if-gtz v4, :cond_2

    iget-wide v4, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    const-wide/16 v6, -0x87

    cmp-long v4, v4, v6

    if-ltz v4, :cond_2

    .line 2143
    :cond_1
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 2144
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 2145
    sub-float v4, v0, v1

    div-float v2, v4, v8

    .line 2146
    sub-float v4, v1, v0

    div-float v3, v4, v8

    .line 2147
    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2149
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2150
    long-to-float v4, p4

    div-float v5, v1, v8

    add-float/2addr v5, p2

    div-float v6, v0, v8

    add-float/2addr v6, p3

    invoke-virtual {p1, v4, v5, v6}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 2154
    .end local v0    # "actualheight":F
    .end local v1    # "actualwidth":F
    .end local v2    # "dx":F
    .end local v3    # "dy":F
    :cond_3
    return-void
.end method


# virtual methods
.method public addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V
    .locals 1
    .param p1, "paragraph"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .prologue
    .line 329
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 330
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 332
    return-void
.end method

.method public addTextBoxContent(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V
    .locals 2
    .param p1, "textBoxContents"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .prologue
    .line 343
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextBoxContents:Lcom/samsung/thumbnail/customview/word/TextBoxController;

    if-nez v0, :cond_0

    .line 344
    new-instance v0, Lcom/samsung/thumbnail/customview/word/TextBoxController;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/word/TextBoxController;-><init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextBoxContents:Lcom/samsung/thumbnail/customview/word/TextBoxController;

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextBoxContents:Lcom/samsung/thumbnail/customview/word/TextBoxController;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/customview/word/TextBoxController;->addContent(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 346
    return-void
.end method

.method public compareTo(Lcom/samsung/thumbnail/customview/word/ShapesController;)I
    .locals 4
    .param p1, "another"    # Lcom/samsung/thumbnail/customview/word/ShapesController;

    .prologue
    .line 2981
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getZorder()J

    move-result-wide v0

    .line 2982
    .local v0, "zOrder":J
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getZorder()J

    move-result-wide v2

    sub-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 69
    check-cast p1, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/thumbnail/customview/word/ShapesController;->compareTo(Lcom/samsung/thumbnail/customview/word/ShapesController;)I

    move-result v0

    return v0
.end method

.method public drawShape(Landroid/graphics/Canvas;Landroid/content/Context;IIZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 68
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "x"    # I
    .param p4, "y"    # I
    .param p5, "isHeaderOrFooter"    # Z
    .param p6, "elementCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 621
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->mCanvas:Landroid/graphics/Canvas;

    .line 622
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->mContext:Landroid/content/Context;

    .line 623
    const/16 v64, 0x0

    .line 625
    .local v64, "textBoxArea":Landroid/graphics/RectF;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v27

    sget-object v28, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v27

    sget-object v28, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_6

    .line 627
    :cond_0
    invoke-virtual/range {p6 .. p6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v57

    .line 629
    .local v57, "currentPage":Lcom/samsung/thumbnail/office/word/Page;
    invoke-virtual/range {v57 .. v57}, Lcom/samsung/thumbnail/office/word/Page;->isCoverPage()Z

    move-result v27

    if-eqz v27, :cond_1

    if-eqz p5, :cond_2

    .line 630
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    add-int p3, p3, v27

    .line 631
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    add-int p4, p4, v27

    .line 634
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mXWPFShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-object/from16 v27, v0

    if-eqz v27, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mXWPFShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v27

    if-eqz v27, :cond_5

    .line 635
    const-string/jumbo v27, "page"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mXWPFShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getxAnchorRef()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 637
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 p3, v0

    .line 640
    :cond_3
    const-string/jumbo v27, "page"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mXWPFShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getyAnchorRef()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 642
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 p4, v0

    .line 645
    :cond_4
    if-eqz p5, :cond_5

    const-string/jumbo v27, "margin"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mXWPFShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getyAnchorRef()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_5

    .line 648
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 p4, v0

    .line 652
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getHorizontalAlign(Lcom/samsung/thumbnail/office/word/Page;I)I

    move-result p3

    .line 653
    move-object/from16 v0, p0

    move-object/from16 v1, v57

    move/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getVerticalAlign(Lcom/samsung/thumbnail/office/word/Page;I)I

    move-result p4

    .line 656
    .end local v57    # "currentPage":Lcom/samsung/thumbnail/office/word/Page;
    :cond_6
    sget-object v27, Lcom/samsung/thumbnail/customview/word/ShapesController$1;->$SwitchMap$com$samsung$thumbnail$customview$CanvasElementPart$DocumentType:[I

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->ordinal()I

    move-result v28

    aget v27, v27, v28

    packed-switch v27, :pswitch_data_0

    .line 2033
    :cond_7
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextBoxContents:Lcom/samsung/thumbnail/customview/word/TextBoxController;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/word/TextBoxController;->getContents()Ljava/util/ArrayList;

    move-result-object v41

    .line 2036
    .local v41, "mTextBoxContentAL":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    invoke-virtual/range {v41 .. v41}, Ljava/util/ArrayList;->size()I

    move-result v27

    if-eqz v27, :cond_9

    .line 2038
    const/16 v67, 0x0

    .line 2041
    .local v67, "verticalAlignOffset":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v27

    sget-object v28, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_8

    if-eqz v64, :cond_8

    .line 2044
    new-instance v46, Landroid/graphics/Canvas;

    invoke-direct/range {v46 .. v46}, Landroid/graphics/Canvas;-><init>()V

    .line 2045
    .local v46, "dummyCanvas":Landroid/graphics/Canvas;
    invoke-virtual/range {v64 .. v64}, Landroid/graphics/RectF;->width()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    move-object/from16 v40, p0

    move/from16 v42, p3

    move/from16 v43, p4

    move-object/from16 v45, p6

    invoke-direct/range {v40 .. v46}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawTextDummy(Ljava/util/ArrayList;IIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Landroid/graphics/Canvas;)I

    move-result v66

    .line 2048
    .local v66, "totalTextHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getVerticalAlignmentElement()Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-result-object v27

    sget-object v28, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_9f

    .line 2049
    invoke-virtual/range {v64 .. v64}, Landroid/graphics/RectF;->height()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v66

    move/from16 v1, v27

    if-le v0, v1, :cond_9e

    .line 2050
    const/16 v67, 0x0

    .line 2063
    .end local v46    # "dummyCanvas":Landroid/graphics/Canvas;
    .end local v66    # "totalTextHeight":I
    :cond_8
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextBoxContents:Lcom/samsung/thumbnail/customview/word/TextBoxController;

    move-object/from16 v47, v0

    add-int v51, p4, v67

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v52, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v53, v0

    move-object/from16 v48, p1

    move-object/from16 v49, p2

    move/from16 v50, p3

    move-object/from16 v54, p6

    invoke-virtual/range {v47 .. v54}, Lcom/samsung/thumbnail/customview/word/TextBoxController;->drawTextBoxContents(Landroid/graphics/Canvas;Landroid/content/Context;IIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 2068
    .end local v67    # "verticalAlignOffset":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    if-nez v27, :cond_a1

    .line 2132
    .end local v41    # "mTextBoxContentAL":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    :cond_a
    :goto_2
    return-void

    .line 659
    :pswitch_0
    move/from16 v0, p3

    int-to-float v8, v0

    move/from16 v0, p4

    int-to-float v9, v0

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    invoke-direct/range {v6 .. v11}, Lcom/samsung/thumbnail/customview/word/ShapesController;->rotateCanvas(Landroid/graphics/Canvas;FFJ)V

    .line 661
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BasicShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_b

    .line 662
    new-instance v6, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v6, v0}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;-><init>(Ljava/io/File;)V

    .line 663
    .local v6, "basicShapes":Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v12, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v13, v0

    move-object/from16 v9, p1

    move/from16 v10, p3

    move/from16 v11, p4

    invoke-virtual/range {v6 .. v13}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 666
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 667
    goto/16 :goto_0

    .end local v6    # "basicShapes":Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BlockArrow"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_c

    .line 669
    new-instance v7, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v7, v0}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;-><init>(Ljava/io/File;)V

    .line 670
    .local v7, "blockArrow":Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v9

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v13, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v14, v0

    move-object/from16 v10, p1

    move/from16 v11, p3

    move/from16 v12, p4

    invoke-virtual/range {v7 .. v14}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 673
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 674
    goto/16 :goto_0

    .end local v7    # "blockArrow":Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BlockShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_d

    .line 676
    new-instance v8, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v8, v0}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;-><init>(Ljava/io/File;)V

    .line 677
    .local v8, "blockShapes":Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v10

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v14, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v15, v0

    move-object/from16 v11, p1

    move/from16 v12, p3

    move/from16 v13, p4

    invoke-virtual/range {v8 .. v15}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 680
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 681
    goto/16 :goto_0

    .end local v8    # "blockShapes":Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BraceShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 683
    new-instance v9, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v9, v0}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;-><init>(Ljava/io/File;)V

    .line 684
    .local v9, "braceShape":Lcom/samsung/thumbnail/office/point/shapes/BraceShape;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v11

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v15, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v16, v0

    move-object/from16 v12, p1

    move/from16 v13, p3

    move/from16 v14, p4

    invoke-virtual/range {v9 .. v16}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 687
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 688
    goto/16 :goto_0

    .end local v9    # "braceShape":Lcom/samsung/thumbnail/office/point/shapes/BraceShape;
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BracketShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_f

    .line 690
    new-instance v10, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v10, v0}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;-><init>(Ljava/io/File;)V

    .line 691
    .local v10, "bracketShapes":Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v16, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v17, v0

    move-object/from16 v13, p1

    move/from16 v14, p3

    move/from16 v15, p4

    invoke-virtual/range {v10 .. v17}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 694
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 695
    goto/16 :goto_0

    .end local v10    # "bracketShapes":Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "CurvedBlockArrow"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_10

    .line 697
    new-instance v11, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v11, v0}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;-><init>(Ljava/io/File;)V

    .line 699
    .local v11, "curvedBlockArrow":Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v14, p1

    move/from16 v15, p3

    move/from16 v16, p4

    invoke-virtual/range {v11 .. v18}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 702
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 703
    goto/16 :goto_0

    .end local v11    # "curvedBlockArrow":Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "ArrowCallout"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_11

    .line 705
    new-instance v12, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;-><init>(Ljava/io/File;)V

    .line 706
    .local v12, "arrowCallout":Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v19, v0

    move-object/from16 v15, p1

    move/from16 v16, p3

    move/from16 v17, p4

    invoke-virtual/range {v12 .. v19}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 709
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 710
    goto/16 :goto_0

    .end local v12    # "arrowCallout":Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "FlowChartProcess"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_12

    .line 712
    new-instance v13, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v13, v0}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;-><init>(Ljava/io/File;)V

    .line 714
    .local v13, "flowChartShapes":Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v16, p1

    move/from16 v17, p3

    move/from16 v18, p4

    invoke-virtual/range {v13 .. v20}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 717
    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 718
    goto/16 :goto_0

    .end local v13    # "flowChartShapes":Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "IrregularSeal"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_13

    .line 720
    new-instance v14, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v14, v0}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;-><init>(Ljava/io/File;)V

    .line 721
    .local v14, "irregularSeal":Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v16

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v17, p1

    move/from16 v18, p3

    move/from16 v19, p4

    invoke-virtual/range {v14 .. v21}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 724
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 725
    goto/16 :goto_0

    .end local v14    # "irregularSeal":Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "StarShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_14

    .line 727
    new-instance v15, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v15, v0}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;-><init>(Ljava/io/File;)V

    .line 728
    .local v15, "starShapes":Lcom/samsung/thumbnail/office/point/shapes/StarShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v21, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v22, v0

    move-object/from16 v18, p1

    move/from16 v19, p3

    move/from16 v20, p4

    invoke-virtual/range {v15 .. v22}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 731
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 732
    goto/16 :goto_0

    .end local v15    # "starShapes":Lcom/samsung/thumbnail/office/point/shapes/StarShapes;
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Ribbbonhapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_15

    .line 734
    new-instance v16, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;-><init>(Ljava/io/File;)V

    .line 735
    .local v16, "ribbonShapes":Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v18

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v19, p1

    move/from16 v20, p3

    move/from16 v21, p4

    invoke-virtual/range {v16 .. v23}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 738
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 739
    goto/16 :goto_0

    .end local v16    # "ribbonShapes":Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;
    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "ScrollShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_16

    .line 741
    new-instance v17, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;-><init>(Ljava/io/File;)V

    .line 742
    .local v17, "scrollShapes":Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v19

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p4

    invoke-virtual/range {v17 .. v24}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 745
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 746
    goto/16 :goto_0

    .end local v17    # "scrollShapes":Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;
    :cond_16
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "WaveShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_17

    .line 748
    new-instance v18, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;-><init>(Ljava/io/File;)V

    .line 749
    .local v18, "waveShapes":Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v20

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v24, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v21, p1

    move/from16 v22, p3

    move/from16 v23, p4

    invoke-virtual/range {v18 .. v25}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 752
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 753
    goto/16 :goto_0

    .end local v18    # "waveShapes":Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;
    :cond_17
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "WedgeCallouts"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_18

    .line 755
    new-instance v19, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;-><init>(Ljava/io/File;)V

    .line 756
    .local v19, "wedgeCallouts":Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v21

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v25, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v26, v0

    move-object/from16 v22, p1

    move/from16 v23, p3

    move/from16 v24, p4

    invoke-virtual/range {v19 .. v26}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 759
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 760
    goto/16 :goto_0

    .end local v19    # "wedgeCallouts":Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;
    :cond_18
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Callout1"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_19

    .line 762
    new-instance v20, Lcom/samsung/thumbnail/office/point/shapes/Callout1;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;-><init>(Ljava/io/File;)V

    .line 763
    .local v20, "callout1":Lcom/samsung/thumbnail/office/point/shapes/Callout1;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v22

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v26, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v23, p1

    move/from16 v24, p3

    move/from16 v25, p4

    invoke-virtual/range {v20 .. v27}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 766
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 767
    goto/16 :goto_0

    .end local v20    # "callout1":Lcom/samsung/thumbnail/office/point/shapes/Callout1;
    :cond_19
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Callout2"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_1a

    .line 769
    new-instance v21, Lcom/samsung/thumbnail/office/point/shapes/Callout2;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;-><init>(Ljava/io/File;)V

    .line 770
    .local v21, "callout2":Lcom/samsung/thumbnail/office/point/shapes/Callout2;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v22

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v23

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move-object/from16 v24, p1

    move/from16 v25, p3

    move/from16 v26, p4

    invoke-virtual/range {v21 .. v28}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 773
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 774
    goto/16 :goto_0

    .end local v21    # "callout2":Lcom/samsung/thumbnail/office/point/shapes/Callout2;
    :cond_1a
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Callout3"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_1b

    .line 776
    new-instance v22, Lcom/samsung/thumbnail/office/point/shapes/Callout3;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;-><init>(Ljava/io/File;)V

    .line 777
    .local v22, "callout3":Lcom/samsung/thumbnail/office/point/shapes/Callout3;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v24

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v28, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v25, p1

    move/from16 v26, p3

    move/from16 v27, p4

    invoke-virtual/range {v22 .. v29}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 780
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 781
    goto/16 :goto_0

    .end local v22    # "callout3":Lcom/samsung/thumbnail/office/point/shapes/Callout3;
    :cond_1b
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BentConnector"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_1c

    .line 783
    new-instance v23, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mConnectorShapeIndex:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v28, v0

    move-object/from16 v0, v23

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;-><init>(ILjava/io/File;)V

    .line 785
    .local v23, "bentConnector":Lcom/samsung/thumbnail/office/point/shapes/BentConnector;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v24

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v25

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v29

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v30

    move-object/from16 v26, p1

    move/from16 v27, p3

    move/from16 v28, p4

    invoke-virtual/range {v23 .. v30}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;ILandroid/graphics/Canvas;IIFF)V

    .line 788
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 789
    goto/16 :goto_0

    .end local v23    # "bentConnector":Lcom/samsung/thumbnail/office/point/shapes/BentConnector;
    :cond_1c
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "ActionButtonShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 791
    new-instance v25, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;-><init>(Ljava/io/File;)V

    .line 793
    .local v25, "actionButtonShapes":Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    move-result-object v27

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v28

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->drawHWPFShapes(Lcom/samsung/thumbnail/office/word/WordShapeInfo;I)V

    goto/16 :goto_0

    .line 800
    .end local v25    # "actionButtonShapes":Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;
    :pswitch_1
    const/16 v60, 0x0

    .line 801
    .local v60, "pptAutoShape":Lorg/apache/poi/hslf/model/AutoShape;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->shape:Lorg/apache/poi/hslf/model/Shape;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    instance-of v0, v0, Lorg/apache/poi/hslf/model/AutoShape;

    move/from16 v27, v0

    if-eqz v27, :cond_7

    .line 802
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->shape:Lorg/apache/poi/hslf/model/Shape;

    move-object/from16 v60, v0

    .end local v60    # "pptAutoShape":Lorg/apache/poi/hslf/model/AutoShape;
    check-cast v60, Lorg/apache/poi/hslf/model/AutoShape;

    .line 803
    .restart local v60    # "pptAutoShape":Lorg/apache/poi/hslf/model/AutoShape;
    new-instance v59, Lorg/apache/poi/hslf/model/Fill;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->shape:Lorg/apache/poi/hslf/model/Shape;

    move-object/from16 v27, v0

    move-object/from16 v0, v59

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lorg/apache/poi/hslf/model/Fill;-><init>(Lorg/apache/poi/hslf/model/Shape;)V

    .line 804
    .local v59, "fill_shape":Lorg/apache/poi/hslf/model/Fill;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->shape:Lorg/apache/poi/hslf/model/Shape;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Shape;->getShapeName()Ljava/lang/String;

    move-result-object v63

    .line 805
    .local v63, "shapeName":Ljava/lang/String;
    if-eqz v63, :cond_a

    .line 807
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->shape:Lorg/apache/poi/hslf/model/Shape;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Shape;->getRotation()I

    move-result v27

    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v28, v0

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    .line 809
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    move-wide/from16 v28, v0

    move-object/from16 v24, p0

    move-object/from16 v25, p1

    invoke-direct/range {v24 .. v29}, Lcom/samsung/thumbnail/customview/word/ShapesController;->rotateCanvas(Landroid/graphics/Canvas;FFJ)V

    .line 811
    const-string/jumbo v27, "Rectangle"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_1e

    .line 812
    invoke-virtual/range {v59 .. v59}, Lorg/apache/poi/hslf/model/Fill;->getNofillpropertyvalue()I

    move-result v61

    .line 813
    .local v61, "propertyval":I
    const/high16 v27, 0x100000

    move/from16 v0, v61

    move/from16 v1, v27

    if-eq v0, v1, :cond_1d

    const v27, 0x100010

    move/from16 v0, v61

    move/from16 v1, v27

    if-ne v0, v1, :cond_7

    .line 815
    :cond_1d
    new-instance v6, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v6, v0}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;-><init>(Ljava/io/File;)V

    .line 816
    .restart local v6    # "basicShapes":Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v29, v0

    move-object/from16 v24, v6

    move-object/from16 v25, v60

    move-object/from16 v27, p1

    invoke-virtual/range {v24 .. v29}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 819
    .end local v6    # "basicShapes":Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
    .end local v61    # "propertyval":I
    :cond_1e
    const-string/jumbo v27, "StraightConnector1"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_1f

    .line 820
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->shape:Lorg/apache/poi/hslf/model/Shape;

    move-object/from16 v27, v0

    const/16 v28, 0x1c0

    invoke-virtual/range {v27 .. v28}, Lorg/apache/poi/hslf/model/Shape;->getEscherProperty(S)I

    move-result v56

    .line 822
    .local v56, "colorValue":I
    if-eqz v56, :cond_7

    .line 823
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->shape:Lorg/apache/poi/hslf/model/Shape;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/hslf/model/Shape;->getLineColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v55

    .line 824
    .local v55, "color":Lorg/apache/poi/java/awt/Color;
    if-eqz v55, :cond_7

    goto/16 :goto_0

    .line 828
    .end local v55    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v56    # "colorValue":I
    :cond_1f
    const-string/jumbo v27, "BentConnector3"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_20

    const-string/jumbo v27, "BentConnector2"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_20

    const-string/jumbo v27, "CurvedConnector3"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_21

    .line 831
    :cond_20
    new-instance v24, Lcom/samsung/thumbnail/office/point/shapes/Connector;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Connector;-><init>(Ljava/io/File;)V

    .line 832
    .local v24, "connector":Lcom/samsung/thumbnail/office/point/shapes/Connector;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v29, v0

    move-object/from16 v25, v60

    move-object/from16 v27, p1

    invoke-virtual/range {v24 .. v29}, Lcom/samsung/thumbnail/office/point/shapes/Connector;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 834
    .end local v24    # "connector":Lcom/samsung/thumbnail/office/point/shapes/Connector;
    :cond_21
    const-string/jumbo v27, "RoundRectangle"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_22

    const-string/jumbo v27, "Diamond"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_22

    const-string/jumbo v27, "Ellipse"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_22

    const-string/jumbo v27, "IsocelesTriangle"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_22

    const-string/jumbo v27, "RightTriangle"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_22

    const-string/jumbo v27, "Parallelogram"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_22

    const-string/jumbo v27, "Pentagon"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_22

    const-string/jumbo v27, "Hexagon"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_22

    const-string/jumbo v27, "Octagon"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_22

    const-string/jumbo v27, "Plus"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_22

    const-string/jumbo v27, "Can"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_23

    .line 845
    :cond_22
    new-instance v6, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v6, v0}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;-><init>(Ljava/io/File;)V

    .line 846
    .restart local v6    # "basicShapes":Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v28, v0

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v28, v0

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v30, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v60

    move-object/from16 v28, p1

    invoke-virtual/range {v25 .. v30}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 848
    .end local v6    # "basicShapes":Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
    :cond_23
    const-string/jumbo v27, "HomePlate"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_24

    const-string/jumbo v27, "Chevron"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_24

    const-string/jumbo v27, "Bevel"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_24

    const-string/jumbo v27, "Plaque"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_24

    const-string/jumbo v27, "Cube"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_24

    const-string/jumbo v27, "Moon"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_24

    const-string/jumbo v27, "FoldedCorner"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_24

    const-string/jumbo v27, "LightningBolt"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_24

    const-string/jumbo v27, "Sun"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_24

    const-string/jumbo v27, "SmileyFace"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_25

    .line 858
    :cond_24
    new-instance v8, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v8, v0}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;-><init>(Ljava/io/File;)V

    .line 859
    .restart local v8    # "blockShapes":Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v28, v0

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v28, v0

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v30, v0

    move-object/from16 v25, v8

    move-object/from16 v26, v60

    move-object/from16 v28, p1

    invoke-virtual/range {v25 .. v30}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 861
    .end local v8    # "blockShapes":Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;
    :cond_25
    const-string/jumbo v27, "ActionButtonHome"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_26

    .line 862
    new-instance v25, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;-><init>(Ljava/io/File;)V

    .line 864
    .restart local v25    # "actionButtonShapes":Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v28, v0

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v28, v0

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v30, v0

    move-object/from16 v26, v60

    move-object/from16 v28, p1

    invoke-virtual/range {v25 .. v30}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 866
    .end local v25    # "actionButtonShapes":Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;
    :cond_26
    const-string/jumbo v27, "DownArrow"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_27

    const-string/jumbo v27, "UpArrow"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_27

    const-string/jumbo v27, "Arrow"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_27

    const-string/jumbo v27, "Arrow"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_27

    const-string/jumbo v27, "LeftArrow"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_27

    const-string/jumbo v27, "LeftRightArrow"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_27

    const-string/jumbo v27, "UpDownArrow"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_27

    const-string/jumbo v27, "NotchedRightArrow"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_28

    .line 874
    :cond_27
    new-instance v7, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v7, v0}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;-><init>(Ljava/io/File;)V

    .line 875
    .restart local v7    # "blockArrow":Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v26, v7

    move-object/from16 v27, v60

    move-object/from16 v29, p1

    invoke-virtual/range {v26 .. v31}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 877
    .end local v7    # "blockArrow":Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;
    :cond_28
    const-string/jumbo v27, "LeftArrowCallout"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_29

    const-string/jumbo v27, "DownArrowCallout"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_29

    const-string/jumbo v27, "RightArrowCallout"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_29

    const-string/jumbo v27, "UpArrowCallout"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_29

    const-string/jumbo v27, "LeftRightArrowCallout"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_2a

    .line 882
    :cond_29
    new-instance v12, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;-><init>(Ljava/io/File;)V

    .line 883
    .restart local v12    # "arrowCallout":Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v26, v12

    move-object/from16 v27, v60

    move-object/from16 v29, p1

    invoke-virtual/range {v26 .. v31}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 885
    .end local v12    # "arrowCallout":Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;
    :cond_2a
    const-string/jumbo v27, "BracketPair"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_2b

    const-string/jumbo v27, "LeftBracket"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_2b

    const-string/jumbo v27, "RightBracket"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_2c

    .line 888
    :cond_2b
    new-instance v10, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v10, v0}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;-><init>(Ljava/io/File;)V

    .line 889
    .restart local v10    # "bracketShapes":Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v26, v10

    move-object/from16 v27, v60

    move-object/from16 v29, p1

    invoke-virtual/range {v26 .. v31}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 891
    .end local v10    # "bracketShapes":Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;
    :cond_2c
    const-string/jumbo v27, "BracePair"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_2d

    const-string/jumbo v27, "LeftBrace"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_2d

    const-string/jumbo v27, "RightBrace"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_2e

    .line 894
    :cond_2d
    new-instance v9, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v9, v0}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;-><init>(Ljava/io/File;)V

    .line 895
    .restart local v9    # "braceShape":Lcom/samsung/thumbnail/office/point/shapes/BraceShape;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v26, v9

    move-object/from16 v27, v60

    move-object/from16 v29, p1

    invoke-virtual/range {v26 .. v31}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 897
    .end local v9    # "braceShape":Lcom/samsung/thumbnail/office/point/shapes/BraceShape;
    :cond_2e
    const-string/jumbo v27, "CurvedRightArrow"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_2f

    const-string/jumbo v27, "CurvedLeftArrow"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_2f

    const-string/jumbo v27, "CurvedUpArrow"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_2f

    const-string/jumbo v27, "CurvedDownArrow"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_30

    .line 901
    :cond_2f
    new-instance v11, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v11, v0}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;-><init>(Ljava/io/File;)V

    .line 903
    .restart local v11    # "curvedBlockArrow":Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v26, v11

    move-object/from16 v27, v60

    move-object/from16 v29, p1

    invoke-virtual/range {v26 .. v31}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 905
    .end local v11    # "curvedBlockArrow":Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;
    :cond_30
    const-string/jumbo v27, "FlowChartProcess"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartAlternateProcess"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartInputOutput"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartDecision"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartPredefinedProcess"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartInternalStorage"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartTerminator"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartPreparation"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartManualInput"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartManualOperation"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartConnector"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartOffpageConnector"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartPunchedCard"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartSummingJunction"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartOr"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartCollate"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartSort"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartExtract"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartMerge"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartOnlineStorage"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartDelay"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartMagneticTape"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartMagneticDisk"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartMagneticDrum"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartDisplay"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartPunchedTape"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartDocument"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_31

    const-string/jumbo v27, "FlowChartMultidocument"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_32

    .line 939
    :cond_31
    new-instance v13, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v13, v0}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;-><init>(Ljava/io/File;)V

    .line 941
    .restart local v13    # "flowChartShapes":Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v26, v13

    move-object/from16 v27, v60

    move-object/from16 v29, p1

    invoke-virtual/range {v26 .. v31}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 943
    .end local v13    # "flowChartShapes":Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;
    :cond_32
    const-string/jumbo v27, "IrregularSeal1"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_33

    const-string/jumbo v27, "IrregularSeal2"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_34

    .line 945
    :cond_33
    new-instance v14, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v14, v0}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;-><init>(Ljava/io/File;)V

    .line 946
    .restart local v14    # "irregularSeal":Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v26, v14

    move-object/from16 v27, v60

    move-object/from16 v29, p1

    invoke-virtual/range {v26 .. v31}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 948
    .end local v14    # "irregularSeal":Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;
    :cond_34
    const-string/jumbo v27, "Star4"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_35

    const-string/jumbo v27, "Star8"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_35

    const-string/jumbo v27, "Star16"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_35

    const-string/jumbo v27, "Star24"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_35

    const-string/jumbo v27, "Star32"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_35

    const-string/jumbo v27, "Star"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_35

    const-string/jumbo v27, "StarShapes"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_36

    .line 955
    :cond_35
    new-instance v26, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    invoke-direct/range {v26 .. v27}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;-><init>(Ljava/io/File;)V

    .line 956
    .local v26, "starShape":Lcom/samsung/thumbnail/office/point/shapes/StarShapes;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v27, v60

    move-object/from16 v29, p1

    invoke-virtual/range {v26 .. v31}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 958
    .end local v26    # "starShape":Lcom/samsung/thumbnail/office/point/shapes/StarShapes;
    :cond_36
    const-string/jumbo v27, "Ribbon2"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_37

    const-string/jumbo v27, "Ribbon"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_37

    const-string/jumbo v27, "EllipseRibbon2"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_37

    const-string/jumbo v27, "EllipseRibbon"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_38

    .line 962
    :cond_37
    new-instance v16, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;-><init>(Ljava/io/File;)V

    .line 963
    .restart local v16    # "ribbonShapes":Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v32, v0

    move-object/from16 v27, v16

    move-object/from16 v28, v60

    move-object/from16 v30, p1

    invoke-virtual/range {v27 .. v32}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 965
    .end local v16    # "ribbonShapes":Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;
    :cond_38
    const-string/jumbo v27, "VerticalScroll"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_39

    const-string/jumbo v27, "HorizontalScroll"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_3a

    .line 967
    :cond_39
    new-instance v17, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;-><init>(Ljava/io/File;)V

    .line 968
    .restart local v17    # "scrollShapes":Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v32, v0

    move-object/from16 v27, v17

    move-object/from16 v28, v60

    move-object/from16 v30, p1

    invoke-virtual/range {v27 .. v32}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 970
    .end local v17    # "scrollShapes":Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;
    :cond_3a
    const-string/jumbo v27, "Wave"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_3b

    const-string/jumbo v27, "DoubleWave"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_3c

    .line 972
    :cond_3b
    new-instance v18, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;-><init>(Ljava/io/File;)V

    .line 973
    .restart local v18    # "waveShapes":Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v32, v0

    move-object/from16 v27, v18

    move-object/from16 v28, v60

    move-object/from16 v30, p1

    invoke-virtual/range {v27 .. v32}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 975
    .end local v18    # "waveShapes":Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;
    :cond_3c
    const-string/jumbo v27, "WedgeRectCallout"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_3d

    const-string/jumbo v27, "WedgeRRectCallout"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_3d

    const-string/jumbo v27, "WedgeEllipseCallout"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_3d

    const-string/jumbo v27, "CloudCallout"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_3e

    .line 979
    :cond_3d
    new-instance v19, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;-><init>(Ljava/io/File;)V

    .line 980
    .restart local v19    # "wedgeCallouts":Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v32, v0

    move-object/from16 v27, v19

    move-object/from16 v28, v60

    move-object/from16 v30, p1

    invoke-virtual/range {v27 .. v32}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 982
    .end local v19    # "wedgeCallouts":Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;
    :cond_3e
    const-string/jumbo v27, "BorderCallout1"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_3f

    const-string/jumbo v27, "Callout1"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_3f

    const-string/jumbo v27, "AccentCallout1"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_3f

    const-string/jumbo v27, "AccentBorderCallout1"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_40

    .line 986
    :cond_3f
    new-instance v20, Lcom/samsung/thumbnail/office/point/shapes/Callout1;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;-><init>(Ljava/io/File;)V

    .line 987
    .restart local v20    # "callout1":Lcom/samsung/thumbnail/office/point/shapes/Callout1;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v32, v0

    move-object/from16 v27, v20

    move-object/from16 v28, v60

    move-object/from16 v30, p1

    invoke-virtual/range {v27 .. v32}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 989
    .end local v20    # "callout1":Lcom/samsung/thumbnail/office/point/shapes/Callout1;
    :cond_40
    const-string/jumbo v27, "BorderCallout2"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_41

    const-string/jumbo v27, "Callout2"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_41

    const-string/jumbo v27, "AccentCallout2"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_41

    const-string/jumbo v27, "AccentBorderCallout2"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_42

    .line 993
    :cond_41
    new-instance v21, Lcom/samsung/thumbnail/office/point/shapes/Callout2;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;-><init>(Ljava/io/File;)V

    .line 994
    .restart local v21    # "callout2":Lcom/samsung/thumbnail/office/point/shapes/Callout2;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v32, v0

    move-object/from16 v27, v21

    move-object/from16 v28, v60

    move-object/from16 v30, p1

    invoke-virtual/range {v27 .. v32}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 996
    .end local v21    # "callout2":Lcom/samsung/thumbnail/office/point/shapes/Callout2;
    :cond_42
    const-string/jumbo v27, "BorderCallout3"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_43

    const-string/jumbo v27, "Callout3"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_43

    const-string/jumbo v27, "AccentCallout3"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_43

    const-string/jumbo v27, "AccentBorderCallout3"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_44

    .line 1000
    :cond_43
    new-instance v22, Lcom/samsung/thumbnail/office/point/shapes/Callout3;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;-><init>(Ljava/io/File;)V

    .line 1001
    .restart local v22    # "callout3":Lcom/samsung/thumbnail/office/point/shapes/Callout3;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v32, v0

    move-object/from16 v27, v22

    move-object/from16 v28, v60

    move-object/from16 v30, p1

    invoke-virtual/range {v27 .. v32}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->drawShape(Lorg/apache/poi/hslf/model/AutoShape;ILandroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 1003
    .end local v22    # "callout3":Lcom/samsung/thumbnail/office/point/shapes/Callout3;
    :cond_44
    const-string/jumbo v27, "NotPrimitive"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 1004
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v28, v0

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v60

    move-object/from16 v2, p1

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->createNonPrimitiveShapes(Lorg/apache/poi/hslf/model/AutoShape;Landroid/graphics/Canvas;II)V

    goto/16 :goto_0

    .line 1011
    .end local v59    # "fill_shape":Lorg/apache/poi/hslf/model/Fill;
    .end local v60    # "pptAutoShape":Lorg/apache/poi/hslf/model/AutoShape;
    .end local v63    # "shapeName":Ljava/lang/String;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1013
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v27

    invoke-static/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->rotationAngelToDegrees16_16(I)I

    move-result v62

    .line 1015
    .local v62, "rotation":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->isFlipV()Z

    move-result v32

    .line 1017
    .local v32, "flipV":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeXvalue()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-float v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    .line 1018
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeYvalue()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-float v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    .line 1020
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-float v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 1021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeHeight()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-float v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 1023
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getTextRotation()I

    move-result v27

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    .line 1027
    if-eqz v32, :cond_45

    .line 1028
    move/from16 v0, v62

    add-int/lit16 v0, v0, 0xb4

    move/from16 v62, v0

    .line 1030
    :cond_45
    move/from16 v0, v62

    int-to-long v0, v0

    move-wide/from16 v28, v0

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    .line 1033
    if-eqz v62, :cond_46

    .line 1034
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 1035
    move/from16 v0, v62

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v29, v0

    const/high16 v30, 0x40000000    # 2.0f

    div-float v29, v29, v30

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v30, v0

    const/high16 v31, 0x40000000    # 2.0f

    div-float v30, v30, v31

    add-float v29, v29, v30

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1039
    :cond_46
    const/16 v65, 0x0

    .line 1040
    .local v65, "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_49

    .line 1041
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v65

    .line 1048
    :cond_47
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v27

    if-eqz v27, :cond_48

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Rounded Rectangle"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_48

    .line 1051
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    const-string/jumbo v28, "roundRect"

    invoke-virtual/range {v27 .. v28}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setPrstShapeName(Ljava/lang/String;)V

    .line 1052
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v27

    sget-object v28, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ROUND_RECT:Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    invoke-virtual/range {v27 .. v28}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setPrstName(Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;)V

    .line 1056
    :cond_48
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getPrstName()Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    move-result-object v27

    if-eqz v27, :cond_61

    .line 1057
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getPrstName()Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;

    move-result-object v58

    .line 1059
    .local v58, "eShapetype":Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;
    sget-object v27, Lcom/samsung/thumbnail/customview/word/ShapesController$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$dml$EDrawMLShapeTypes:[I

    invoke-virtual/range {v58 .. v58}, Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;->ordinal()I

    move-result v28

    aget v27, v27, v28

    packed-switch v27, :pswitch_data_1

    .line 1524
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape()Z

    move-result v27

    if-eqz v27, :cond_7

    .line 1525
    new-instance v27, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v28, "Shape is not supported"

    invoke-direct/range {v27 .. v28}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v27

    .line 1042
    .end local v58    # "eShapetype":Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;
    :cond_49
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_47

    .line 1043
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;->getSlideLayout()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideLayout;->getSlideMaster()Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v65

    goto/16 :goto_3

    .line 1076
    .restart local v58    # "eShapetype":Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;
    :pswitch_3
    new-instance v6, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v6, v0}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;-><init>(Ljava/io/File;)V

    .line 1079
    .restart local v6    # "basicShapes":Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_4a

    .line 1080
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v31, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v32, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    .end local v32    # "flipV":Z
    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v33, v0

    move-object/from16 v27, v6

    move-object/from16 v28, p1

    invoke-virtual/range {v27 .. v33}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1085
    .restart local v32    # "flipV":Z
    :cond_4a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1087
    invoke-virtual/range {v65 .. v65}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getThemeName()Ljava/lang/String;

    move-result-object v27

    if-eqz v27, :cond_4e

    .line 1088
    invoke-virtual/range {v65 .. v65}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getThemeName()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Aspect"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_4b

    .line 1090
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v27

    if-eqz v27, :cond_4b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Rounded Rectangle 9"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_4b

    .line 1099
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->getXDocColor()Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->clear()V

    .line 1103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFillProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;

    move-result-object v27

    const-string/jumbo v28, "C0C0C0"

    invoke-virtual/range {v27 .. v28}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;->setForeColor(Ljava/lang/String;)V

    .line 1108
    :cond_4b
    invoke-virtual/range {v65 .. v65}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getThemeName()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Paper"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_4d

    .line 1110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v27

    if-eqz v27, :cond_4d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Straight Connector 7"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_4c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Straight Connector 12"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_4d

    .line 1120
    :cond_4c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getLineProps()Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;

    move-result-object v27

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/LineProperties;->setFillProp(Lcom/samsung/thumbnail/office/ooxml/dml/theme/FillProperty;)V

    .line 1123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    const-string/jumbo v28, "lt1"

    invoke-virtual/range {v27 .. v28}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setLineRefColor(Ljava/lang/String;)V

    .line 1127
    :cond_4d
    const-string/jumbo v27, "Pushpin"

    invoke-virtual/range {v65 .. v65}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getThemeName()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_4e

    .line 1129
    const-string/jumbo v27, "Rectangle 11"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->autoShape:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFAutoShape;->getShapeProperties()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_4e

    .line 1133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    const-string/jumbo v28, "lt1"

    invoke-virtual/range {v27 .. v28}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->setFillRefColor(Ljava/lang/String;)V

    .line 1137
    :cond_4e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v31, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v33, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v34, v0

    move-object/from16 v27, v6

    move-object/from16 v28, p1

    invoke-virtual/range {v27 .. v34}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;ZII)V

    goto/16 :goto_0

    .line 1151
    .end local v6    # "basicShapes":Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
    :pswitch_4
    new-instance v7, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v7, v0}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;-><init>(Ljava/io/File;)V

    .line 1152
    .restart local v7    # "blockArrow":Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_4f

    .line 1153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v37, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v38, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v39, v0

    move-object/from16 v33, v7

    move-object/from16 v34, p1

    invoke-virtual/range {v33 .. v39}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1158
    :cond_4f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1159
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v37, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v38, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v39, v0

    move-object/from16 v33, v7

    move-object/from16 v34, p1

    invoke-virtual/range {v33 .. v39}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1171
    .end local v7    # "blockArrow":Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;
    :pswitch_5
    new-instance v33, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v33

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;-><init>(Ljava/io/File;)V

    .line 1172
    .local v33, "arrow":Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_50

    .line 1173
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v37, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v38, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v39, v0

    move-object/from16 v34, p1

    invoke-virtual/range {v33 .. v39}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1176
    :cond_50
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v37, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v38, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v39, v0

    move-object/from16 v34, p1

    invoke-virtual/range {v33 .. v39}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1211
    .end local v33    # "arrow":Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;
    :pswitch_6
    new-instance v34, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;-><init>(Ljava/io/File;)V

    .line 1213
    .local v34, "flowChart":Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_51

    .line 1214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v38, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v39, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v35, p1

    invoke-virtual/range {v34 .. v40}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1219
    :cond_51
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v38, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v39, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v40, v0

    move-object/from16 v35, p1

    invoke-virtual/range {v34 .. v40}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1232
    .end local v34    # "flowChart":Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;
    :pswitch_7
    new-instance v35, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;-><init>(Ljava/io/File;)V

    .line 1234
    .local v35, "irregularSealShapes":Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_52

    .line 1235
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v39, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v40, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v41, v0

    move-object/from16 v36, p1

    invoke-virtual/range {v35 .. v41}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1240
    :cond_52
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v39, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v40, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v41, v0

    move-object/from16 v36, p1

    invoke-virtual/range {v35 .. v41}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1254
    .end local v35    # "irregularSealShapes":Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;
    :pswitch_8
    new-instance v36, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v36

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;-><init>(Ljava/io/File;)V

    .line 1256
    .local v36, "curvedArrow":Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_53

    .line 1257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v40, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v42, v0

    move-object/from16 v37, p1

    invoke-virtual/range {v36 .. v42}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1262
    :cond_53
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1263
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v40, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v41, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v42, v0

    move-object/from16 v37, p1

    invoke-virtual/range {v36 .. v42}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1280
    .end local v36    # "curvedArrow":Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;
    :pswitch_9
    new-instance v37, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v37

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;-><init>(Ljava/io/File;)V

    .line 1281
    .local v37, "bolckShape":Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_54

    .line 1282
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v41, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v42, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v43, v0

    move-object/from16 v38, p1

    invoke-virtual/range {v37 .. v43}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1287
    :cond_54
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1288
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v41, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v42, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v43, v0

    move-object/from16 v38, p1

    invoke-virtual/range {v37 .. v43}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1299
    .end local v37    # "bolckShape":Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;
    :pswitch_a
    new-instance v25, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;-><init>(Ljava/io/File;)V

    .line 1301
    .restart local v25    # "actionButtonShapes":Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_55

    .line 1302
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v42, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    move-object/from16 v38, v25

    move-object/from16 v39, p1

    invoke-virtual/range {v38 .. v44}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1307
    :cond_55
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1308
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v42, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    move-object/from16 v38, v25

    move-object/from16 v39, p1

    invoke-virtual/range {v38 .. v44}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1320
    .end local v25    # "actionButtonShapes":Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;
    :pswitch_b
    new-instance v38, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v38

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;-><init>(Ljava/io/File;)V

    .line 1322
    .local v38, "bracketShape":Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_56

    .line 1323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v42, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    move-object/from16 v39, p1

    invoke-virtual/range {v38 .. v44}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1328
    :cond_56
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1329
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v42, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    move-object/from16 v39, p1

    invoke-virtual/range {v38 .. v44}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1339
    .end local v38    # "bracketShape":Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;
    :pswitch_c
    new-instance v9, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v9, v0}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;-><init>(Ljava/io/File;)V

    .line 1340
    .restart local v9    # "braceShape":Lcom/samsung/thumbnail/office/point/shapes/BraceShape;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_57

    .line 1341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v9

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1346
    :cond_57
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v9

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1362
    .end local v9    # "braceShape":Lcom/samsung/thumbnail/office/point/shapes/BraceShape;
    :pswitch_d
    new-instance v26, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    invoke-direct/range {v26 .. v27}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;-><init>(Ljava/io/File;)V

    .line 1363
    .restart local v26    # "starShape":Lcom/samsung/thumbnail/office/point/shapes/StarShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_58

    .line 1364
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v26

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1369
    :cond_58
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1370
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v26

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1381
    .end local v26    # "starShape":Lcom/samsung/thumbnail/office/point/shapes/StarShapes;
    :pswitch_e
    new-instance v16, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;-><init>(Ljava/io/File;)V

    .line 1382
    .restart local v16    # "ribbonShapes":Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_59

    .line 1383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v16

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1388
    :cond_59
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1389
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v16

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1398
    .end local v16    # "ribbonShapes":Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;
    :pswitch_f
    new-instance v17, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;-><init>(Ljava/io/File;)V

    .line 1399
    .restart local v17    # "scrollShapes":Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_5a

    .line 1400
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v17

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1405
    :cond_5a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v17

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1415
    .end local v17    # "scrollShapes":Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;
    :pswitch_10
    new-instance v18, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;-><init>(Ljava/io/File;)V

    .line 1416
    .restart local v18    # "waveShapes":Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_5b

    .line 1417
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v18

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1422
    :cond_5b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1423
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v18

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1434
    .end local v18    # "waveShapes":Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;
    :pswitch_11
    new-instance v19, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;-><init>(Ljava/io/File;)V

    .line 1436
    .restart local v19    # "wedgeCallouts":Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_5c

    .line 1437
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v19

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1442
    :cond_5c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1443
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v19

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1454
    .end local v19    # "wedgeCallouts":Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;
    :pswitch_12
    new-instance v20, Lcom/samsung/thumbnail/office/point/shapes/Callout1;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;-><init>(Ljava/io/File;)V

    .line 1455
    .restart local v20    # "callout1":Lcom/samsung/thumbnail/office/point/shapes/Callout1;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_5d

    .line 1456
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v20

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1459
    :cond_5d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v20

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1469
    .end local v20    # "callout1":Lcom/samsung/thumbnail/office/point/shapes/Callout1;
    :pswitch_13
    new-instance v21, Lcom/samsung/thumbnail/office/point/shapes/Callout2;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;-><init>(Ljava/io/File;)V

    .line 1470
    .restart local v21    # "callout2":Lcom/samsung/thumbnail/office/point/shapes/Callout2;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_5e

    .line 1471
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v21

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1474
    :cond_5e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v21

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1484
    .end local v21    # "callout2":Lcom/samsung/thumbnail/office/point/shapes/Callout2;
    :pswitch_14
    new-instance v22, Lcom/samsung/thumbnail/office/point/shapes/Callout3;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;-><init>(Ljava/io/File;)V

    .line 1485
    .restart local v22    # "callout3":Lcom/samsung/thumbnail/office/point/shapes/Callout3;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_5f

    .line 1486
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v22

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1489
    :cond_5f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v22

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1497
    .end local v22    # "callout3":Lcom/samsung/thumbnail/office/point/shapes/Callout3;
    :pswitch_15
    new-instance v23, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;-><init>(Ljava/io/File;)V

    .line 1499
    .restart local v23    # "bentConnector":Lcom/samsung/thumbnail/office/point/shapes/BentConnector;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v27, v0

    if-eqz v27, :cond_60

    .line 1500
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlide:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v23

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlide;II)V

    goto/16 :goto_0

    .line 1505
    :cond_60
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1506
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeCount:I

    move/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfSlideMaster:Lcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v39, v23

    move-object/from16 v40, p1

    invoke-virtual/range {v39 .. v45}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->drawXSLFShape(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;ILcom/samsung/thumbnail/office/ooxml/pptx/XSLFSlideMaster;II)V

    goto/16 :goto_0

    .line 1516
    .end local v23    # "bentConnector":Lcom/samsung/thumbnail/office/point/shapes/BentConnector;
    :pswitch_16
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape()Z

    move-result v27

    if-eqz v27, :cond_7

    .line 1517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getPathDescriptor()Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    move-result-object v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v42

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v43

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getSDRatio()F

    move-result v44

    move-object/from16 v39, p0

    move-object/from16 v40, p1

    invoke-direct/range {v39 .. v44}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawPaths(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;FFF)V

    goto/16 :goto_0

    .line 1530
    .end local v58    # "eShapetype":Lcom/samsung/thumbnail/office/ooxml/dml/EDrawMLShapeTypes;
    :cond_61
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeName()Ljava/lang/String;

    move-result-object v63

    .line 1531
    .restart local v63    # "shapeName":Ljava/lang/String;
    if-eqz v63, :cond_62

    const-string/jumbo v27, "Freeform"

    move-object/from16 v0, v63

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_62

    .line 1532
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v65

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawCustomGeom(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    goto/16 :goto_0

    .line 1533
    :cond_62
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape()Z

    move-result v27

    if-eqz v27, :cond_7

    .line 1534
    new-instance v27, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v28, "Shape is not supported"

    invoke-direct/range {v27 .. v28}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v27

    .line 1542
    .end local v32    # "flipV":Z
    .end local v62    # "rotation":I
    .end local v63    # "shapeName":Ljava/lang/String;
    .end local v65    # "theme":Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;
    :pswitch_17
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v27

    if-eqz v27, :cond_63

    .line 1543
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getAllParagraph()Ljava/util/ArrayList;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    .line 1544
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextBoxContents:Lcom/samsung/thumbnail/customview/word/TextBoxController;

    move-object/from16 v27, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getTxtBxContents()Ljava/util/ArrayList;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/samsung/thumbnail/customview/word/TextBoxController;->setContents(Ljava/util/ArrayList;)V

    .line 1545
    const-string/jumbo v27, "bottom-to-top"

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getOrientParam()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_64

    .line 1547
    const-wide/16 v28, -0x5a

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    .line 1554
    :cond_63
    :goto_4
    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v42, v0

    move/from16 v0, p4

    int-to-float v0, v0

    move/from16 v43, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    move-wide/from16 v44, v0

    move-object/from16 v40, p0

    move-object/from16 v41, p1

    invoke-direct/range {v40 .. v45}, Lcom/samsung/thumbnail/customview/word/ShapesController;->rotateCanvas(Landroid/graphics/Canvas;FFJ)V

    .line 1555
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getTextRotation()I

    move-result v27

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    .line 1558
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BasicShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_65

    .line 1559
    new-instance v6, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v6, v0}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;-><init>(Ljava/io/File;)V

    .line 1560
    .restart local v6    # "basicShapes":Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getFlipValue()Ljava/lang/String;

    move-result-object v47

    move-object/from16 v39, v6

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    move/from16 v48, p5

    invoke-virtual/range {v39 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIIILjava/lang/String;Z)V

    .line 1564
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1565
    goto/16 :goto_0

    .line 1548
    .end local v6    # "basicShapes":Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
    :cond_64
    const-string/jumbo v27, "top-to-bottom"

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getOrientParam()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_63

    .line 1550
    const-wide/16 v28, 0x5a

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    goto/16 :goto_4

    .line 1565
    :cond_65
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BlockArrow"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_66

    .line 1567
    new-instance v7, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v7, v0}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;-><init>(Ljava/io/File;)V

    .line 1568
    .restart local v7    # "blockArrow":Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v7

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1571
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1572
    goto/16 :goto_0

    .end local v7    # "blockArrow":Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;
    :cond_66
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BlockShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_67

    .line 1574
    new-instance v8, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v8, v0}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;-><init>(Ljava/io/File;)V

    .line 1575
    .restart local v8    # "blockShapes":Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v8

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1578
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1579
    goto/16 :goto_0

    .end local v8    # "blockShapes":Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;
    :cond_67
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BraceShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_68

    .line 1581
    new-instance v9, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v9, v0}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;-><init>(Ljava/io/File;)V

    .line 1582
    .restart local v9    # "braceShape":Lcom/samsung/thumbnail/office/point/shapes/BraceShape;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v9

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1585
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1586
    goto/16 :goto_0

    .end local v9    # "braceShape":Lcom/samsung/thumbnail/office/point/shapes/BraceShape;
    :cond_68
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BracketShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_69

    .line 1588
    new-instance v10, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v10, v0}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;-><init>(Ljava/io/File;)V

    .line 1589
    .restart local v10    # "bracketShapes":Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v10

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1592
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1593
    goto/16 :goto_0

    .end local v10    # "bracketShapes":Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;
    :cond_69
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "CurvedBlockArrow"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_6a

    .line 1595
    new-instance v11, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v11, v0}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;-><init>(Ljava/io/File;)V

    .line 1597
    .restart local v11    # "curvedBlockArrow":Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v11

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1600
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1601
    goto/16 :goto_0

    .end local v11    # "curvedBlockArrow":Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;
    :cond_6a
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "ArrowCallout"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_6b

    .line 1603
    new-instance v12, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;-><init>(Ljava/io/File;)V

    .line 1604
    .restart local v12    # "arrowCallout":Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v12

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1607
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1608
    goto/16 :goto_0

    .end local v12    # "arrowCallout":Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;
    :cond_6b
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "FlowChartProcess"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_6c

    .line 1610
    new-instance v13, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v13, v0}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;-><init>(Ljava/io/File;)V

    .line 1612
    .restart local v13    # "flowChartShapes":Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v13

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1615
    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1616
    goto/16 :goto_0

    .end local v13    # "flowChartShapes":Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;
    :cond_6c
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "IrregularSeal"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_6d

    .line 1618
    new-instance v14, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v14, v0}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;-><init>(Ljava/io/File;)V

    .line 1619
    .restart local v14    # "irregularSeal":Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v14

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1622
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1623
    goto/16 :goto_0

    .end local v14    # "irregularSeal":Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;
    :cond_6d
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "StarShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_6e

    .line 1625
    new-instance v15, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v15, v0}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;-><init>(Ljava/io/File;)V

    .line 1626
    .restart local v15    # "starShapes":Lcom/samsung/thumbnail/office/point/shapes/StarShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v15

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1629
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1630
    goto/16 :goto_0

    .end local v15    # "starShapes":Lcom/samsung/thumbnail/office/point/shapes/StarShapes;
    :cond_6e
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Ribbbonhapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_6f

    .line 1632
    new-instance v16, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;-><init>(Ljava/io/File;)V

    .line 1633
    .restart local v16    # "ribbonShapes":Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v16

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1636
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1637
    goto/16 :goto_0

    .end local v16    # "ribbonShapes":Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;
    :cond_6f
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "ScrollShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_70

    .line 1639
    new-instance v17, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;-><init>(Ljava/io/File;)V

    .line 1640
    .restart local v17    # "scrollShapes":Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v17

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1643
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1644
    goto/16 :goto_0

    .end local v17    # "scrollShapes":Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;
    :cond_70
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "WaveShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_71

    .line 1646
    new-instance v18, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;-><init>(Ljava/io/File;)V

    .line 1647
    .restart local v18    # "waveShapes":Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v18

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1650
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1651
    goto/16 :goto_0

    .end local v18    # "waveShapes":Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;
    :cond_71
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "WedgeCallouts"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_72

    .line 1653
    new-instance v19, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;-><init>(Ljava/io/File;)V

    .line 1654
    .restart local v19    # "wedgeCallouts":Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v19

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1657
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1658
    goto/16 :goto_0

    .end local v19    # "wedgeCallouts":Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;
    :cond_72
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Callout1"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_73

    .line 1660
    new-instance v20, Lcom/samsung/thumbnail/office/point/shapes/Callout1;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;-><init>(Ljava/io/File;)V

    .line 1661
    .restart local v20    # "callout1":Lcom/samsung/thumbnail/office/point/shapes/Callout1;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v20

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1664
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1665
    goto/16 :goto_0

    .end local v20    # "callout1":Lcom/samsung/thumbnail/office/point/shapes/Callout1;
    :cond_73
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Callout2"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_74

    .line 1667
    new-instance v21, Lcom/samsung/thumbnail/office/point/shapes/Callout2;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;-><init>(Ljava/io/File;)V

    .line 1668
    .restart local v21    # "callout2":Lcom/samsung/thumbnail/office/point/shapes/Callout2;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v21

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1671
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1672
    goto/16 :goto_0

    .end local v21    # "callout2":Lcom/samsung/thumbnail/office/point/shapes/Callout2;
    :cond_74
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Callout3"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_75

    .line 1674
    new-instance v22, Lcom/samsung/thumbnail/office/point/shapes/Callout3;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;-><init>(Ljava/io/File;)V

    .line 1675
    .restart local v22    # "callout3":Lcom/samsung/thumbnail/office/point/shapes/Callout3;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    move-object/from16 v39, v22

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1678
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1679
    goto/16 :goto_0

    .end local v22    # "callout3":Lcom/samsung/thumbnail/office/point/shapes/Callout3;
    :cond_75
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "textOnPath"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_76

    .line 1681
    new-instance v39, Lcom/samsung/thumbnail/office/point/wordart/TextOnPath;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mPath:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v28, v0

    move-object/from16 v0, v39

    move-object/from16 v1, p2

    move-object/from16 v2, v27

    move-object/from16 v3, v28

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/office/point/wordart/TextOnPath;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)V

    .line 1683
    .local v39, "textOnPath":Lcom/samsung/thumbnail/office/point/wordart/TextOnPath;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v40

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v42, p1

    move/from16 v43, p3

    move/from16 v44, p4

    invoke-virtual/range {v39 .. v46}, Lcom/samsung/thumbnail/office/point/wordart/TextOnPath;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIII)V

    .line 1686
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/office/point/wordart/TextOnPath;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1687
    goto/16 :goto_0

    .end local v39    # "textOnPath":Lcom/samsung/thumbnail/office/point/wordart/TextOnPath;
    :cond_76
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BentConnector"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_77

    .line 1689
    new-instance v23, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mConnectorShapeIndex:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v28, v0

    move-object/from16 v0, v23

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;-><init>(ILjava/io/File;)V

    .line 1691
    .restart local v23    # "bentConnector":Lcom/samsung/thumbnail/office/point/shapes/BentConnector;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v41

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v42

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v47, v0

    move-object/from16 v40, v23

    move-object/from16 v43, p1

    move/from16 v44, p3

    move/from16 v45, p4

    invoke-virtual/range {v40 .. v47}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->drawXWPFShapes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;ILandroid/graphics/Canvas;IIFF)V

    .line 1694
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1695
    goto/16 :goto_0

    .end local v23    # "bentConnector":Lcom/samsung/thumbnail/office/point/shapes/BentConnector;
    :cond_77
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "NonPrimitive"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 1697
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape()Z

    move-result v27

    if-eqz v27, :cond_7

    .line 1698
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getPathDescriptor()Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;

    move-result-object v42

    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v43, v0

    move/from16 v0, p4

    int-to-float v0, v0

    move/from16 v44, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getSDRatio()F

    move-result v45

    move-object/from16 v40, p0

    move-object/from16 v41, p1

    invoke-direct/range {v40 .. v45}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawPaths(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/dml/handlers/path/PathDescriptor;FFF)V

    goto/16 :goto_0

    .line 1706
    :pswitch_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v27

    if-eqz v27, :cond_78

    .line 1707
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 1708
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v29, v0

    const/high16 v30, 0x40000000    # 2.0f

    div-float v29, v29, v30

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v30, v0

    const/high16 v31, 0x40000000    # 2.0f

    div-float v30, v30, v31

    add-float v29, v29, v30

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1713
    :cond_78
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BasicShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_79

    .line 1714
    new-instance v6, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v6, v0}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;-><init>(Ljava/io/File;)V

    .line 1715
    .restart local v6    # "basicShapes":Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v6

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1718
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1719
    goto/16 :goto_0

    .end local v6    # "basicShapes":Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
    :cond_79
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BlockArrow"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_7a

    .line 1721
    new-instance v7, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v7, v0}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;-><init>(Ljava/io/File;)V

    .line 1722
    .restart local v7    # "blockArrow":Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v7

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1725
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1726
    goto/16 :goto_0

    .end local v7    # "blockArrow":Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;
    :cond_7a
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BlockShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_7b

    .line 1728
    new-instance v8, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v8, v0}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;-><init>(Ljava/io/File;)V

    .line 1729
    .restart local v8    # "blockShapes":Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v8

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1732
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1733
    goto/16 :goto_0

    .end local v8    # "blockShapes":Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;
    :cond_7b
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BraceShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_7c

    .line 1735
    new-instance v9, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v9, v0}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;-><init>(Ljava/io/File;)V

    .line 1736
    .restart local v9    # "braceShape":Lcom/samsung/thumbnail/office/point/shapes/BraceShape;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v9

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1739
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1740
    goto/16 :goto_0

    .end local v9    # "braceShape":Lcom/samsung/thumbnail/office/point/shapes/BraceShape;
    :cond_7c
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BracketShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_7d

    .line 1742
    new-instance v10, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v10, v0}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;-><init>(Ljava/io/File;)V

    .line 1743
    .restart local v10    # "bracketShapes":Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v10

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1746
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1747
    goto/16 :goto_0

    .end local v10    # "bracketShapes":Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;
    :cond_7d
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "CurvedBlockArrow"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_7e

    .line 1749
    new-instance v11, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v11, v0}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;-><init>(Ljava/io/File;)V

    .line 1751
    .restart local v11    # "curvedBlockArrow":Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v11

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1754
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1755
    goto/16 :goto_0

    .end local v11    # "curvedBlockArrow":Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;
    :cond_7e
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "ArrowCallout"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_7f

    .line 1757
    new-instance v12, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;-><init>(Ljava/io/File;)V

    .line 1758
    .restart local v12    # "arrowCallout":Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v12

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1761
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1762
    goto/16 :goto_0

    .end local v12    # "arrowCallout":Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;
    :cond_7f
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "FlowChartProcess"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_80

    .line 1764
    new-instance v13, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v13, v0}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;-><init>(Ljava/io/File;)V

    .line 1766
    .restart local v13    # "flowChartShapes":Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v13

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1769
    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1770
    goto/16 :goto_0

    .end local v13    # "flowChartShapes":Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;
    :cond_80
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "IrregularSeal"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_81

    .line 1772
    new-instance v14, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v14, v0}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;-><init>(Ljava/io/File;)V

    .line 1773
    .restart local v14    # "irregularSeal":Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v14

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1776
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1777
    goto/16 :goto_0

    .end local v14    # "irregularSeal":Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;
    :cond_81
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "StarShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_82

    .line 1779
    new-instance v15, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v15, v0}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;-><init>(Ljava/io/File;)V

    .line 1780
    .restart local v15    # "starShapes":Lcom/samsung/thumbnail/office/point/shapes/StarShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v15

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1783
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1784
    goto/16 :goto_0

    .end local v15    # "starShapes":Lcom/samsung/thumbnail/office/point/shapes/StarShapes;
    :cond_82
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Ribbbonhapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_83

    .line 1786
    new-instance v16, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;-><init>(Ljava/io/File;)V

    .line 1787
    .restart local v16    # "ribbonShapes":Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v16

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1790
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1791
    goto/16 :goto_0

    .end local v16    # "ribbonShapes":Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;
    :cond_83
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "ScrollShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_84

    .line 1793
    new-instance v17, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;-><init>(Ljava/io/File;)V

    .line 1794
    .restart local v17    # "scrollShapes":Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v17

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1797
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1798
    goto/16 :goto_0

    .end local v17    # "scrollShapes":Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;
    :cond_84
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "WaveShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_85

    .line 1800
    new-instance v18, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;-><init>(Ljava/io/File;)V

    .line 1801
    .restart local v18    # "waveShapes":Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v18

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1804
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1805
    goto/16 :goto_0

    .end local v18    # "waveShapes":Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;
    :cond_85
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "WedgeCallouts"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_86

    .line 1807
    new-instance v19, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;-><init>(Ljava/io/File;)V

    .line 1808
    .restart local v19    # "wedgeCallouts":Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v19

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1811
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1812
    goto/16 :goto_0

    .end local v19    # "wedgeCallouts":Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;
    :cond_86
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Callout1"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_87

    .line 1814
    new-instance v20, Lcom/samsung/thumbnail/office/point/shapes/Callout1;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;-><init>(Ljava/io/File;)V

    .line 1815
    .restart local v20    # "callout1":Lcom/samsung/thumbnail/office/point/shapes/Callout1;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v20

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1818
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1819
    goto/16 :goto_0

    .end local v20    # "callout1":Lcom/samsung/thumbnail/office/point/shapes/Callout1;
    :cond_87
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Callout2"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_88

    .line 1821
    new-instance v21, Lcom/samsung/thumbnail/office/point/shapes/Callout2;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;-><init>(Ljava/io/File;)V

    .line 1822
    .restart local v21    # "callout2":Lcom/samsung/thumbnail/office/point/shapes/Callout2;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v21

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1825
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1826
    goto/16 :goto_0

    .end local v21    # "callout2":Lcom/samsung/thumbnail/office/point/shapes/Callout2;
    :cond_88
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Callout3"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_89

    .line 1828
    new-instance v22, Lcom/samsung/thumbnail/office/point/shapes/Callout3;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;-><init>(Ljava/io/File;)V

    .line 1829
    .restart local v22    # "callout3":Lcom/samsung/thumbnail/office/point/shapes/Callout3;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v22

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1832
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1833
    goto/16 :goto_0

    .end local v22    # "callout3":Lcom/samsung/thumbnail/office/point/shapes/Callout3;
    :cond_89
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BentConnector"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_8a

    .line 1835
    new-instance v23, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mConnectorShapeIndex:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v28, v0

    move-object/from16 v0, v23

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;-><init>(ILjava/io/File;)V

    .line 1837
    .restart local v23    # "bentConnector":Lcom/samsung/thumbnail/office/point/shapes/BentConnector;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v23

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 1840
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1841
    goto/16 :goto_0

    .end local v23    # "bentConnector":Lcom/samsung/thumbnail/office/point/shapes/BentConnector;
    :cond_8a
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "ActionButtonShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_8b

    .line 1843
    new-instance v25, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;-><init>(Ljava/io/File;)V

    .line 1845
    .restart local v25    # "actionButtonShapes":Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIndex:I

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    move/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    move/from16 v48, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v49, v0

    move-object/from16 v40, v25

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v49}, Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;->drawXSSExcelShape(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIFFLcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    goto/16 :goto_0

    .line 1848
    .end local v25    # "actionButtonShapes":Lcom/samsung/thumbnail/office/point/shapes/ActionButtonShapes;
    :cond_8b
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Freeform"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 1850
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->xslfShapeInfo:Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mDrawingObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v29

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawCustomGeom(Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;Landroid/graphics/Canvas;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    goto/16 :goto_0

    .line 1860
    :pswitch_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getRotationAngle()I

    move-result v27

    if-eqz v27, :cond_8c

    .line 1861
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 1862
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;->getRotationAngle()I

    move-result v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v28

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v29

    const/high16 v30, 0x40000000    # 2.0f

    div-float v29, v29, v30

    add-float v28, v28, v29

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v29

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v30

    const/high16 v31, 0x40000000    # 2.0f

    div-float v30, v30, v31

    add-float v29, v29, v30

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1867
    :cond_8c
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BasicShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_8d

    .line 1868
    new-instance v6, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v6, v0}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;-><init>(Ljava/io/File;)V

    .line 1869
    .restart local v6    # "basicShapes":Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v6

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1874
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1875
    goto/16 :goto_0

    .end local v6    # "basicShapes":Lcom/samsung/thumbnail/office/point/shapes/BasicShapes;
    :cond_8d
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "ArrowCallout"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_8e

    .line 1877
    new-instance v12, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v12, v0}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;-><init>(Ljava/io/File;)V

    .line 1878
    .restart local v12    # "arrowCallout":Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v12

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1883
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1884
    goto/16 :goto_0

    .end local v12    # "arrowCallout":Lcom/samsung/thumbnail/office/point/shapes/ArrowCallout;
    :cond_8e
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BlockArrow"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_8f

    .line 1886
    new-instance v7, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v7, v0}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;-><init>(Ljava/io/File;)V

    .line 1887
    .restart local v7    # "blockArrow":Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v7

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1892
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1893
    goto/16 :goto_0

    .end local v7    # "blockArrow":Lcom/samsung/thumbnail/office/point/shapes/BlockArrow;
    :cond_8f
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BlockShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_90

    .line 1895
    new-instance v8, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v8, v0}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;-><init>(Ljava/io/File;)V

    .line 1896
    .restart local v8    # "blockShapes":Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v8

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1901
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1902
    goto/16 :goto_0

    .end local v8    # "blockShapes":Lcom/samsung/thumbnail/office/point/shapes/BlockShapes;
    :cond_90
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BraceShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_91

    .line 1904
    new-instance v9, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v9, v0}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;-><init>(Ljava/io/File;)V

    .line 1905
    .restart local v9    # "braceShape":Lcom/samsung/thumbnail/office/point/shapes/BraceShape;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v9

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1910
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/point/shapes/BraceShape;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1911
    goto/16 :goto_0

    .end local v9    # "braceShape":Lcom/samsung/thumbnail/office/point/shapes/BraceShape;
    :cond_91
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BracketShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_92

    .line 1913
    new-instance v10, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v10, v0}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;-><init>(Ljava/io/File;)V

    .line 1914
    .restart local v10    # "bracketShapes":Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v10

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1919
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1920
    goto/16 :goto_0

    .end local v10    # "bracketShapes":Lcom/samsung/thumbnail/office/point/shapes/BracketShapes;
    :cond_92
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Ribbbonhapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_93

    .line 1922
    new-instance v16, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;-><init>(Ljava/io/File;)V

    .line 1923
    .restart local v16    # "ribbonShapes":Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v16

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1928
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1929
    goto/16 :goto_0

    .end local v16    # "ribbonShapes":Lcom/samsung/thumbnail/office/point/shapes/RibbonShapes;
    :cond_93
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Callout1"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_94

    .line 1931
    new-instance v20, Lcom/samsung/thumbnail/office/point/shapes/Callout1;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;-><init>(Ljava/io/File;)V

    .line 1932
    .restart local v20    # "callout1":Lcom/samsung/thumbnail/office/point/shapes/Callout1;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v20

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1935
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/point/shapes/Callout1;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1936
    goto/16 :goto_0

    .end local v20    # "callout1":Lcom/samsung/thumbnail/office/point/shapes/Callout1;
    :cond_94
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Callout2"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_95

    .line 1938
    new-instance v21, Lcom/samsung/thumbnail/office/point/shapes/Callout2;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;-><init>(Ljava/io/File;)V

    .line 1939
    .restart local v21    # "callout2":Lcom/samsung/thumbnail/office/point/shapes/Callout2;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v21

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1942
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/point/shapes/Callout2;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1943
    goto/16 :goto_0

    .end local v21    # "callout2":Lcom/samsung/thumbnail/office/point/shapes/Callout2;
    :cond_95
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "Callout3"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_96

    .line 1945
    new-instance v22, Lcom/samsung/thumbnail/office/point/shapes/Callout3;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;-><init>(Ljava/io/File;)V

    .line 1946
    .restart local v22    # "callout3":Lcom/samsung/thumbnail/office/point/shapes/Callout3;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v22

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1949
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/point/shapes/Callout3;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1951
    goto/16 :goto_0

    .end local v22    # "callout3":Lcom/samsung/thumbnail/office/point/shapes/Callout3;
    :cond_96
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "CurvedBlockArrow"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_97

    .line 1953
    new-instance v11, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v11, v0}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;-><init>(Ljava/io/File;)V

    .line 1955
    .restart local v11    # "curvedBlockArrow":Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v11

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1960
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1961
    goto/16 :goto_0

    .end local v11    # "curvedBlockArrow":Lcom/samsung/thumbnail/office/point/shapes/CurvedBlockArrow;
    :cond_97
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "FlowChartProcess"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_98

    .line 1963
    new-instance v13, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v13, v0}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;-><init>(Ljava/io/File;)V

    .line 1965
    .restart local v13    # "flowChartShapes":Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v13

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1970
    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1971
    goto/16 :goto_0

    .end local v13    # "flowChartShapes":Lcom/samsung/thumbnail/office/point/shapes/FlowChartShapes;
    :cond_98
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "IrregularSeal"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_99

    .line 1973
    new-instance v14, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v14, v0}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;-><init>(Ljava/io/File;)V

    .line 1974
    .restart local v14    # "irregularSeal":Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v14

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1979
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1980
    goto/16 :goto_0

    .end local v14    # "irregularSeal":Lcom/samsung/thumbnail/office/point/shapes/IrregularSeal;
    :cond_99
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "ScrollShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_9a

    .line 1982
    new-instance v17, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;-><init>(Ljava/io/File;)V

    .line 1983
    .restart local v17    # "scrollShapes":Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v17

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1988
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1989
    goto/16 :goto_0

    .end local v17    # "scrollShapes":Lcom/samsung/thumbnail/office/point/shapes/ScrollShapes;
    :cond_9a
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "StarShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_9b

    .line 1991
    new-instance v15, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-direct {v15, v0}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;-><init>(Ljava/io/File;)V

    .line 1992
    .restart local v15    # "starShapes":Lcom/samsung/thumbnail/office/point/shapes/StarShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v15

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 1997
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/point/shapes/StarShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 1998
    goto/16 :goto_0

    .end local v15    # "starShapes":Lcom/samsung/thumbnail/office/point/shapes/StarShapes;
    :cond_9b
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "WaveShapes"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_9c

    .line 2000
    new-instance v18, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;-><init>(Ljava/io/File;)V

    .line 2001
    .restart local v18    # "waveShapes":Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v18

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 2006
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 2007
    goto/16 :goto_0

    .end local v18    # "waveShapes":Lcom/samsung/thumbnail/office/point/shapes/WaveShapes;
    :cond_9c
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "WedgeCallouts"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_9d

    .line 2009
    new-instance v19, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v27, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;-><init>(Ljava/io/File;)V

    .line 2010
    .restart local v19    # "wedgeCallouts":Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v19

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 2015
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 2016
    goto/16 :goto_0

    .end local v19    # "wedgeCallouts":Lcom/samsung/thumbnail/office/point/shapes/WedgeCallouts;
    :cond_9d
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeType()Ljava/lang/String;

    move-result-object v27

    const-string/jumbo v28, "BentConnector"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 2018
    new-instance v23, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mConnectorShapeIndex:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->folderPath:Ljava/io/File;

    move-object/from16 v28, v0

    move-object/from16 v0, v23

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;-><init>(ILjava/io/File;)V

    .line 2020
    .restart local v23    # "bentConnector":Lcom/samsung/thumbnail/office/point/shapes/BentConnector;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeInfo:Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;

    move-object/from16 v43, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeCount()I

    move-result v44

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v45, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v46, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v47

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v48

    move-object/from16 v40, v23

    move-object/from16 v41, p1

    move-object/from16 v42, p2

    invoke-virtual/range {v40 .. v48}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->drawHSSFShapes(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/office/excel/ExcelShapeInfo;IIIFF)V

    .line 2025
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/office/point/shapes/BentConnector;->getTextBoxArea()Landroid/graphics/RectF;

    move-result-object v64

    .line 2026
    goto/16 :goto_0

    .line 2052
    .end local v23    # "bentConnector":Lcom/samsung/thumbnail/office/point/shapes/BentConnector;
    .restart local v41    # "mTextBoxContentAL":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    .restart local v46    # "dummyCanvas":Landroid/graphics/Canvas;
    .restart local v66    # "totalTextHeight":I
    .restart local v67    # "verticalAlignOffset":I
    :cond_9e
    invoke-virtual/range {v64 .. v64}, Landroid/graphics/RectF;->height()F

    move-result v27

    move/from16 v0, v66

    int-to-float v0, v0

    move/from16 v28, v0

    sub-float v27, v27, v28

    const/high16 v28, 0x40000000    # 2.0f

    div-float v27, v27, v28

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v67, v0

    goto/16 :goto_1

    .line 2054
    :cond_9f
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getVerticalAlignmentElement()Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-result-object v27

    sget-object v28, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_8

    .line 2055
    invoke-virtual/range {v64 .. v64}, Landroid/graphics/RectF;->height()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v66

    move/from16 v1, v27

    if-le v0, v1, :cond_a0

    .line 2056
    const/16 v67, 0x0

    goto/16 :goto_1

    .line 2058
    :cond_a0
    invoke-virtual/range {v64 .. v64}, Landroid/graphics/RectF;->height()F

    move-result v27

    move/from16 v0, v66

    int-to-float v0, v0

    move/from16 v28, v0

    sub-float v27, v27, v28

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v67, v0

    goto/16 :goto_1

    .line 2071
    .end local v46    # "dummyCanvas":Landroid/graphics/Canvas;
    .end local v66    # "totalTextHeight":I
    .end local v67    # "verticalAlignOffset":I
    :cond_a1
    const/16 v49, 0x0

    .line 2072
    .local v49, "xPos":I
    const/16 v50, 0x0

    .line 2073
    .local v50, "yPos":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v27

    const/high16 v28, 0x41a00000    # 20.0f

    sub-float v27, v27, v28

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v51, v0

    .line 2075
    .local v51, "textAreaWidth":I
    sget-object v27, Lcom/samsung/thumbnail/customview/word/ShapesController$1;->$SwitchMap$com$samsung$thumbnail$customview$CanvasElementPart$DocumentType:[I

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->ordinal()I

    move-result v28

    aget v27, v27, v28

    packed-switch v27, :pswitch_data_2

    .line 2093
    :pswitch_1a
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v49, v0

    .line 2094
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v50, v0

    .line 2098
    :goto_5
    if-eqz v64, :cond_a2

    .line 2099
    move-object/from16 v0, v64

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    add-int v50, v50, v27

    .line 2100
    move-object/from16 v0, v64

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    add-int v49, v49, v27

    .line 2101
    move-object/from16 v0, v64

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v27, v0

    move-object/from16 v0, v64

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v28, v0

    sub-float v27, v27, v28

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v51, v0

    .line 2104
    :cond_a2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape()Z

    move-result v27

    if-eqz v27, :cond_a4

    .line 2105
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v27

    sget-object v28, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_a3

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v27

    sget-object v28, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_a4

    :cond_a3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    move/from16 v27, v0

    if-eqz v27, :cond_a4

    .line 2107
    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/customview/word/ShapesController;->restoreCanvas(Landroid/graphics/Canvas;)V

    .line 2112
    :cond_a4
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v27

    sget-object v28, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_a8

    .line 2113
    new-instance v46, Landroid/graphics/Canvas;

    invoke-direct/range {v46 .. v46}, Landroid/graphics/Canvas;-><init>()V

    .line 2114
    .restart local v46    # "dummyCanvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    move-object/from16 v48, v0

    move-object/from16 v47, p0

    move-object/from16 v52, p6

    move-object/from16 v53, v46

    invoke-direct/range {v47 .. v53}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawTextDummy(Ljava/util/ArrayList;IIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Landroid/graphics/Canvas;)I

    move-result v53

    .local v53, "textHeight":I
    move-object/from16 v47, p0

    move-object/from16 v48, p1

    move-object/from16 v52, p6

    .line 2116
    invoke-direct/range {v47 .. v53}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawText(Landroid/graphics/Canvas;IIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;I)V

    .line 2124
    .end local v46    # "dummyCanvas":Landroid/graphics/Canvas;
    .end local v53    # "textHeight":I
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape()Z

    move-result v27

    if-eqz v27, :cond_a6

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape()Z

    move-result v27

    if-eqz v27, :cond_a7

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v27

    sget-object v28, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_a5

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v27

    sget-object v28, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_a7

    :cond_a5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextRotationAngle:I

    move/from16 v27, v0

    if-nez v27, :cond_a7

    .line 2127
    :cond_a6
    invoke-direct/range {p0 .. p1}, Lcom/samsung/thumbnail/customview/word/ShapesController;->restoreCanvas(Landroid/graphics/Canvas;)V

    .line 2130
    :cond_a7
    sget v27, Lcom/samsung/thumbnail/customview/word/ShapesController;->INITIAL_YVAL:I

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/ShapesController;->y:I

    goto/16 :goto_2

    .line 2078
    :pswitch_1b
    move/from16 v49, p3

    .line 2079
    move/from16 v50, p4

    .line 2080
    goto/16 :goto_5

    .line 2118
    :cond_a8
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v27

    sget-object v28, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-ne v0, v1, :cond_a9

    .line 2119
    move-object/from16 v0, p0

    move/from16 v1, v49

    move/from16 v2, v50

    move/from16 v3, v51

    move-object/from16 v4, p6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawTextPPT(IIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    goto :goto_6

    :cond_a9
    move-object/from16 v47, p0

    move-object/from16 v48, p1

    move-object/from16 v52, p6

    .line 2121
    invoke-direct/range {v47 .. v52}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawText(Landroid/graphics/Canvas;IIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    goto :goto_6

    .line 656
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch

    .line 1059
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_f
        :pswitch_f
        :pswitch_10
        :pswitch_10
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_15
        :pswitch_15
        :pswitch_16
        :pswitch_16
    .end packed-switch

    .line 2075
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_1b
        :pswitch_1a
        :pswitch_1a
        :pswitch_1b
    .end packed-switch
.end method

.method public getAllParagraph()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 339
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParagraphArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getAllTextBoxContents()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 349
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTextBoxContents:Lcom/samsung/thumbnail/customview/word/TextBoxController;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/TextBoxController;->getContents()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getAutoShape()Lorg/apache/poi/hslf/model/AutoShape;
    .locals 1

    .prologue
    .line 2904
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mAutoShape:Lorg/apache/poi/hslf/model/AutoShape;

    return-object v0
.end method

.method public getFlipValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mFlipValue:Ljava/lang/String;

    return-object v0
.end method

.method public getFooterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3022
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mFooterName:Ljava/lang/String;

    return-object v0
.end method

.method public getHeaderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3010
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHeaderName:Ljava/lang/String;

    return-object v0
.end method

.method public getLeftMargin()F
    .locals 1

    .prologue
    .line 2940
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    return v0
.end method

.method public getParaNumber()I
    .locals 1

    .prologue
    .line 2916
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParaNumber:I

    return v0
.end method

.method public getRotationAngle()J
    .locals 2

    .prologue
    .line 3002
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mRotationAngle:J

    return-wide v0
.end method

.method public getShapeCount()I
    .locals 1

    .prologue
    .line 2924
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    return v0
.end method

.method public getShapeHeight()F
    .locals 1

    .prologue
    .line 2972
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    return v0
.end method

.method public getShapeStringId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3006
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeStringId:Ljava/lang/String;

    return-object v0
.end method

.method public getShapeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2892
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeType:Ljava/lang/String;

    return-object v0
.end method

.method public getShapeWidth()F
    .locals 1

    .prologue
    .line 2964
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    return v0
.end method

.method public getTextPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3034
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getTopMargin()F
    .locals 1

    .prologue
    .line 2948
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    return v0
.end method

.method public getWordShapeInfo()Lcom/samsung/thumbnail/office/word/WordShapeInfo;
    .locals 1

    .prologue
    .line 2936
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->shapeInfo:Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    return-object v0
.end method

.method public getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .locals 1

    .prologue
    .line 2912
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mXWPFShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    return-object v0
.end method

.method public getZorder()J
    .locals 2

    .prologue
    .line 2956
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mZorder:J

    return-wide v0
.end method

.method public isDiagramShape()Z
    .locals 1

    .prologue
    .line 3038
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape:Z

    return v0
.end method

.method public isFooter()Z
    .locals 1

    .prologue
    .line 2998
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIsFooter:Z

    return v0
.end method

.method public isHeader()Z
    .locals 1

    .prologue
    .line 2994
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIsHeader:Z

    return v0
.end method

.method public setAutoShape(Lorg/apache/poi/hslf/model/AutoShape;)V
    .locals 0
    .param p1, "autoShape"    # Lorg/apache/poi/hslf/model/AutoShape;

    .prologue
    .line 2900
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mAutoShape:Lorg/apache/poi/hslf/model/AutoShape;

    .line 2901
    return-void
.end method

.method public setDiagramShape(Z)V
    .locals 0
    .param p1, "isDiagramShape"    # Z

    .prologue
    .line 3042
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->isDiagramShape:Z

    .line 3043
    return-void
.end method

.method public setFooterName(Ljava/lang/String;)V
    .locals 0
    .param p1, "footerName"    # Ljava/lang/String;

    .prologue
    .line 3018
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mFooterName:Ljava/lang/String;

    .line 3019
    return-void
.end method

.method public setHeaderName(Ljava/lang/String;)V
    .locals 0
    .param p1, "headerName"    # Ljava/lang/String;

    .prologue
    .line 3014
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mHeaderName:Ljava/lang/String;

    .line 3015
    return-void
.end method

.method public setIsFooter(Z)V
    .locals 1
    .param p1, "isFooter"    # Z

    .prologue
    .line 2990
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIsFooter:Z

    .line 2991
    return-void
.end method

.method public setIsHeader(Z)V
    .locals 1
    .param p1, "isHeader"    # Z

    .prologue
    .line 2986
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mIsHeader:Z

    .line 2987
    return-void
.end method

.method public setLeftMargin(F)V
    .locals 0
    .param p1, "mLeftMargin"    # F

    .prologue
    .line 2944
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mLeftMargin:F

    .line 2945
    return-void
.end method

.method public setParaNumber(I)V
    .locals 0
    .param p1, "mParaNumber"    # I

    .prologue
    .line 2920
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mParaNumber:I

    .line 2921
    return-void
.end method

.method public setShapeCount(I)V
    .locals 0
    .param p1, "mShapeCount"    # I

    .prologue
    .line 2928
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeCount:I

    .line 2929
    return-void
.end method

.method public setShapeHeight(F)V
    .locals 0
    .param p1, "mShapeHeight"    # F

    .prologue
    .line 2976
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeHeight:F

    .line 2977
    return-void
.end method

.method public setShapeInfo(Lcom/samsung/thumbnail/office/word/WordShapeInfo;)V
    .locals 0
    .param p1, "shapeInfo"    # Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    .prologue
    .line 2932
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->shapeInfo:Lcom/samsung/thumbnail/office/word/WordShapeInfo;

    .line 2933
    return-void
.end method

.method public setShapeStringId(Ljava/lang/String;)V
    .locals 0
    .param p1, "mShapeStringId"    # Ljava/lang/String;

    .prologue
    .line 3026
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeStringId:Ljava/lang/String;

    .line 3027
    return-void
.end method

.method public setShapeType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mShapeType"    # Ljava/lang/String;

    .prologue
    .line 2896
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeType:Ljava/lang/String;

    .line 2897
    return-void
.end method

.method public setShapeWidth(F)V
    .locals 0
    .param p1, "mShapeWidth"    # F

    .prologue
    .line 2968
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mShapeWidth:F

    .line 2969
    return-void
.end method

.method public setTextPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "textPath"    # Ljava/lang/String;

    .prologue
    .line 3030
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mPath:Ljava/lang/String;

    .line 3031
    return-void
.end method

.method public setTopMargin(F)V
    .locals 0
    .param p1, "mTopMargin"    # F

    .prologue
    .line 2952
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mTopMargin:F

    .line 2953
    return-void
.end method

.method public setXWPFShape(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V
    .locals 0
    .param p1, "mShape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .prologue
    .line 2908
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mXWPFShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .line 2909
    return-void
.end method

.method public setZorder(J)V
    .locals 1
    .param p1, "mZorder"    # J

    .prologue
    .line 2960
    iput-wide p1, p0, Lcom/samsung/thumbnail/customview/word/ShapesController;->mZorder:J

    .line 2961
    return-void
.end method
