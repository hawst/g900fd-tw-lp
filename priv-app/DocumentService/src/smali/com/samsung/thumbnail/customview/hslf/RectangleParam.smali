.class public Lcom/samsung/thumbnail/customview/hslf/RectangleParam;
.super Lcom/samsung/thumbnail/customview/CanvasElementPart;
.source "RectangleParam.java"


# instance fields
.field bottom:I

.field color:I

.field left:I

.field right:I

.field top:I


# direct methods
.method public constructor <init>(IIIII)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "color"    # I

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 14
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->left:I

    .line 15
    iput p2, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->top:I

    .line 16
    iput p4, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->bottom:I

    .line 17
    iput p3, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->right:I

    .line 18
    iput p5, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->color:I

    .line 19
    return-void
.end method


# virtual methods
.method public getBottom()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->bottom:I

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->color:I

    return v0
.end method

.method public getLeft()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->left:I

    return v0
.end method

.method public getRight()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->right:I

    return v0
.end method

.method public getTop()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->top:I

    return v0
.end method

.method public setBottom(I)V
    .locals 0
    .param p1, "bottom"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->bottom:I

    .line 51
    return-void
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->color:I

    .line 59
    return-void
.end method

.method public setLeft(I)V
    .locals 0
    .param p1, "left"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->left:I

    .line 27
    return-void
.end method

.method public setRight(I)V
    .locals 0
    .param p1, "right"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->right:I

    .line 43
    return-void
.end method

.method public setTop(I)V
    .locals 0
    .param p1, "top"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->top:I

    .line 35
    return-void
.end method
