.class public Lcom/samsung/thumbnail/customview/hslf/CustomController;
.super Ljava/lang/Object;
.source "CustomController.java"


# instance fields
.field private elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 0
    .param p1, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/CustomController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 16
    return-void
.end method


# virtual methods
.method public drawBackGroundContent(ZLandroid/graphics/Bitmap;IIII)V
    .locals 2
    .param p1, "isBGImg"    # Z
    .param p2, "bgImage"    # Landroid/graphics/Bitmap;
    .param p3, "color"    # I
    .param p4, "slideNumber"    # I
    .param p5, "fillstyle"    # I
    .param p6, "fgColor"    # I

    .prologue
    .line 36
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;

    invoke-direct {v0, p1, p2, p3, p6}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;-><init>(ZLandroid/graphics/Bitmap;II)V

    .line 38
    .local v0, "bgContent":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 39
    invoke-virtual {v0, p4}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setSlideNumber(I)V

    .line 40
    invoke-virtual {v0, p5}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->setFillStyle(I)V

    .line 41
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/hslf/CustomController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 42
    return-void
.end method

.method public drawBitMap(ILandroid/graphics/Bitmap;IIII)V
    .locals 6
    .param p1, "slideNumber"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "left"    # I
    .param p4, "top"    # I
    .param p5, "width"    # I
    .param p6, "height"    # I

    .prologue
    .line 46
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;-><init>(Landroid/graphics/Bitmap;IIII)V

    .line 48
    .local v0, "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 49
    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setSlideNumber(I)V

    .line 50
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/hslf/CustomController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 51
    return-void
.end method

.method public drawLine(IIIIIDI)V
    .locals 10
    .param p1, "startX"    # I
    .param p2, "startY"    # I
    .param p3, "endX"    # I
    .param p4, "endY"    # I
    .param p5, "lineColor"    # I
    .param p6, "lineWidth"    # D
    .param p8, "slideNumber"    # I

    .prologue
    .line 73
    new-instance v2, Lcom/samsung/thumbnail/customview/hslf/LineParam;

    iget-object v3, p0, Lcom/samsung/thumbnail/customview/hslf/CustomController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    int-to-double v4, p1

    invoke-virtual {v3, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v4

    double-to-int v3, v4

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/thumbnail/customview/hslf/CustomController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v4

    int-to-double v6, p2

    invoke-virtual {v4, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v4

    double-to-int v4, v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/thumbnail/customview/hslf/CustomController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v5

    int-to-double v6, p3

    invoke-virtual {v5, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v6

    double-to-int v5, v6

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/thumbnail/customview/hslf/CustomController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v6

    int-to-double v8, p4

    invoke-virtual {v6, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v6

    double-to-int v6, v6

    int-to-float v6, v6

    move-wide/from16 v0, p6

    double-to-float v8, v0

    move v7, p5

    invoke-direct/range {v2 .. v8}, Lcom/samsung/thumbnail/customview/hslf/LineParam;-><init>(FFFFIF)V

    .line 79
    .local v2, "lineParam":Lcom/samsung/thumbnail/customview/hslf/LineParam;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 80
    move/from16 v0, p8

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->setSlideNumber(I)V

    .line 81
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/hslf/CustomController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 82
    return-void
.end method

.method public drawRect(IIIIII)V
    .locals 8
    .param p1, "top"    # I
    .param p2, "left"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "color"    # I
    .param p6, "slideNumber"    # I

    .prologue
    .line 56
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/hslf/CustomController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    int-to-double v4, p1

    invoke-virtual {v3, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v4

    double-to-int v2, v4

    .line 57
    .local v2, "rectTopMargin":I
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/hslf/CustomController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    int-to-double v4, p2

    invoke-virtual {v3, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v4

    double-to-int v1, v4

    .line 58
    .local v1, "rectLeftMargin":I
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/hslf/CustomController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    int-to-double v4, p4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v4

    double-to-int v6, v4

    .line 59
    .local v6, "rectBottomMargin":I
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/hslf/CustomController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    int-to-double v4, p3

    invoke-virtual {v3, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v4

    double-to-int v7, v4

    .line 61
    .local v7, "rectRightMargin":I
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;

    add-int v3, v7, v1

    add-int v4, v6, v2

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;-><init>(IIIII)V

    .line 64
    .local v0, "rectParam":Lcom/samsung/thumbnail/customview/hslf/RectangleParam;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 65
    invoke-virtual {v0, p6}, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->setSlideNumber(I)V

    .line 66
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/hslf/CustomController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 68
    return-void
.end method

.method public drawText(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;DDDDI)V
    .locals 17
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "textColor"    # I
    .param p3, "fontSize"    # Ljava/lang/String;
    .param p4, "allignment"    # Ljava/lang/String;
    .param p5, "xCoOrdinate"    # D
    .param p7, "yCoOrdinate"    # D
    .param p9, "width"    # D
    .param p11, "height"    # D
    .param p13, "slideNumber"    # I

    .prologue
    .line 27
    new-instance v3, Lcom/samsung/thumbnail/customview/hslf/TextContent;

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p2

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move-wide/from16 v12, p9

    move-wide/from16 v14, p11

    invoke-direct/range {v3 .. v15}, Lcom/samsung/thumbnail/customview/hslf/TextContent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IDDDD)V

    .line 29
    .local v3, "textContent":Lcom/samsung/thumbnail/customview/hslf/TextContent;
    sget-object v2, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 30
    move/from16 v0, p13

    invoke-virtual {v3, v0}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->setSlideNumber(I)V

    .line 31
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/hslf/CustomController;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 32
    return-void
.end method
