.class synthetic Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable$1;
.super Ljava/lang/Object;
.source "XWPFCanvasTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$samsung$thumbnail$office$ooxml$word$XWPFStyle$EStyleType:[I

.field static final synthetic $SwitchMap$com$samsung$thumbnail$office$ooxml$word$table$XWPFTable$EWidthType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1051
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->values()[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XWPFStyle$EStyleType:[I

    :try_start_0
    sget-object v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XWPFStyle$EStyleType:[I

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->PARAGRAPH:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_0
    :try_start_1
    sget-object v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XWPFStyle$EStyleType:[I

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->CHARACTER:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_1
    :try_start_2
    sget-object v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XWPFStyle$EStyleType:[I

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->TABLE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    :try_start_3
    sget-object v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XWPFStyle$EStyleType:[I

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->NUMBERING:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    .line 722
    :goto_3
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->values()[Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$table$XWPFTable$EWidthType:[I

    :try_start_4
    sget-object v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$table$XWPFTable$EWidthType:[I

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->DXA:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_4
    :try_start_5
    sget-object v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$table$XWPFTable$EWidthType:[I

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->PCT:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_5
    return-void

    :catch_0
    move-exception v0

    goto :goto_5

    :catch_1
    move-exception v0

    goto :goto_4

    .line 1051
    :catch_2
    move-exception v0

    goto :goto_3

    :catch_3
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v0

    goto :goto_1

    :catch_5
    move-exception v0

    goto :goto_0
.end method
