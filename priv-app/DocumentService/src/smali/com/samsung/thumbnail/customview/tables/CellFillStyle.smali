.class public Lcom/samsung/thumbnail/customview/tables/CellFillStyle;
.super Ljava/lang/Object;
.source "CellFillStyle.java"


# instance fields
.field private fillColor:Lorg/apache/poi/java/awt/Color;

.field private gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

.field private isGradientFill:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->isGradientFill:Z

    return-void
.end method


# virtual methods
.method public getGradientFill()Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    return-object v0
.end method

.method public getfillColor()Lorg/apache/poi/java/awt/Color;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->fillColor:Lorg/apache/poi/java/awt/Color;

    return-object v0
.end method

.method public isGradientFill()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->isGradientFill:Z

    return v0
.end method

.method public setGradient(Z)V
    .locals 0
    .param p1, "isgradientFill"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->isGradientFill:Z

    .line 36
    return-void
.end method

.method public setGradientFill(Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;)V
    .locals 0
    .param p1, "gradientFill"    # Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->gradientFill:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    .line 28
    return-void
.end method

.method public setfillColor(Lorg/apache/poi/java/awt/Color;)V
    .locals 0
    .param p1, "fillcolor"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->fillColor:Lorg/apache/poi/java/awt/Color;

    .line 20
    return-void
.end method
