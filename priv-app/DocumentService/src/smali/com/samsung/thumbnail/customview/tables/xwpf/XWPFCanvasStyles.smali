.class public Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;
.super Lorg/apache/poi/POIXMLDocumentPart;
.source "XWPFCanvasStyles.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles$1;
    }
.end annotation


# instance fields
.field private defCharProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

.field private defParaProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

.field private defParaStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

.field private defTableStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

.field private excelfontsizechk:Z

.field private isPpt:Z

.field private lHOnlyForBody:Z

.field private styles:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Lorg/apache/poi/POIXMLDocumentPart;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->isPpt:Z

    .line 101
    return-void
.end method

.method public constructor <init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    .locals 1
    .param p1, "part"    # Lorg/apache/poi/openxml4j/opc/PackagePart;
    .param p2, "rel"    # Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/POIXMLDocumentPart;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->isPpt:Z

    .line 90
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->onDocumentRead()V

    .line 91
    return-void
.end method


# virtual methods
.method public addMargin(Ljava/lang/StringBuilder;)V
    .locals 0
    .param p1, "style"    # Ljava/lang/StringBuilder;

    .prologue
    .line 299
    return-void
.end method

.method public addStyle(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)V
    .locals 2
    .param p1, "style"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .prologue
    .line 862
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->styles:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getStyleId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 863
    return-void
.end method

.method public addStyleProp(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)Ljava/lang/String;
    .locals 8
    .param p1, "style"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .prologue
    .line 228
    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x100

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 229
    .local v5, "value":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getStyleType()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    move-result-object v4

    .line 230
    .local v4, "type":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;
    if-nez v4, :cond_0

    .line 231
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->TABLE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    .line 233
    :cond_0
    const/4 v1, 0x0

    .line 234
    .local v1, "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    const/4 v0, 0x0

    .line 235
    .local v0, "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    const/4 v3, 0x0

    .line 236
    .local v3, "tblProp":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;
    const/4 v2, 0x0

    .line 238
    .local v2, "tblCellProp":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    sget-object v6, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XWPFStyle$EStyleType:[I

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 259
    :goto_0
    :pswitch_0
    if-eqz v0, :cond_1

    .line 260
    invoke-virtual {p0, v0, v5}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->createCharacterStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Ljava/lang/StringBuilder;)Ljava/lang/String;

    .line 263
    :cond_1
    if-eqz v1, :cond_2

    .line 264
    invoke-virtual {p0, v1, v5}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->createParagraphStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Ljava/lang/StringBuilder;)Ljava/lang/String;

    .line 267
    :cond_2
    if-eqz v3, :cond_3

    .line 268
    invoke-virtual {p0, v3, v5}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->createTableStyle(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;Ljava/lang/StringBuilder;)Ljava/lang/String;

    .line 271
    :cond_3
    if-eqz v2, :cond_4

    .line 272
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 280
    :cond_4
    invoke-virtual {p0, v5}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->addMargin(Ljava/lang/StringBuilder;)V

    .line 282
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 240
    :pswitch_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    .line 241
    goto :goto_0

    .line 243
    :pswitch_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v0

    .line 244
    goto :goto_0

    .line 246
    :pswitch_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    .line 247
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v0

    .line 248
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getTblProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v3

    .line 249
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCellProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v2

    .line 251
    goto :goto_0

    .line 238
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public concatStyleAttr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 845
    const-string/jumbo v0, ""

    .line 846
    .local v0, "val":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 847
    invoke-virtual {v0, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 848
    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 849
    return-object v0
.end method

.method public createCharacterStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 12
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p2, "style"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v11, 0x3b

    const/high16 v10, 0x42c80000    # 100.0f

    const/high16 v9, 0x42ac0000    # 86.0f

    const/high16 v8, 0x40000000    # 2.0f

    .line 368
    if-nez p1, :cond_0

    .line 369
    const-string/jumbo v6, ""

    .line 708
    :goto_0
    return-object v6

    .line 395
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v0

    .line 396
    .local v0, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    if-eqz v0, :cond_1

    .line 401
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getThemeColor()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 402
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    .line 404
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getThemeColor()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->displayRgbfromThemeandtint(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    .line 406
    .local v1, "output":Lorg/apache/poi/java/awt/Color;
    invoke-virtual {v1}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v6

    const v7, 0xffffff

    and-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    .line 408
    .local v4, "tintcolor":Ljava/lang/String;
    const-string/jumbo v6, "ffffff"

    invoke-virtual {v4, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_1

    .line 409
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x6

    if-ge v6, v7, :cond_1

    .line 410
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 426
    .end local v1    # "output":Lorg/apache/poi/java/awt/Color;
    .end local v4    # "tintcolor":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v6

    if-lez v6, :cond_9

    iget-boolean v6, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->excelfontsizechk:Z

    if-nez v6, :cond_9

    iget-boolean v6, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->isPpt:Z

    if-nez v6, :cond_9

    .line 428
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v6

    int-to-float v6, v6

    div-float v2, v6, v8

    .line 431
    .local v2, "size":F
    mul-float v6, v2, v9

    div-float v2, v6, v10

    .line 449
    .end local v2    # "size":F
    :cond_2
    :goto_1
    iget-boolean v6, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->excelfontsizechk:Z

    if-eqz v6, :cond_3

    iget-boolean v6, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->lHOnlyForBody:Z

    if-eqz v6, :cond_3

    .line 451
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v6

    int-to-float v2, v6

    .line 453
    .restart local v2    # "size":F
    const/4 v6, 0x0

    cmpl-float v6, v2, v6

    if-lez v6, :cond_a

    .line 478
    .end local v2    # "size":F
    :cond_3
    :goto_2
    iget-boolean v6, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->isPpt:Z

    if-eqz v6, :cond_4

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getIsBaseline()Z

    move-result v6

    if-nez v6, :cond_4

    .line 479
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v6

    int-to-float v6, v6

    div-float v2, v6, v8

    .line 480
    .restart local v2    # "size":F
    mul-float v6, v2, v9

    div-float/2addr v6, v10

    .line 488
    .end local v2    # "size":F
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getIsBaseline()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 514
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBaseline()I

    move-result v6

    if-nez v6, :cond_5

    .line 515
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v6

    int-to-float v6, v6

    div-float v2, v6, v8

    .line 516
    .restart local v2    # "size":F
    mul-float v6, v2, v9

    div-float/2addr v6, v10

    .line 556
    .end local v2    # "size":F
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getUnderlineProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    move-result-object v5

    .line 557
    .local v5, "underline":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    const/4 v3, 0x0

    .line 558
    .local v3, "textDec":Ljava/lang/String;
    if-eqz v5, :cond_6

    .line 559
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->createUnderlineStyle()Ljava/lang/String;

    move-result-object v3

    .line 561
    const-string/jumbo v6, "text-decoration:"

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 562
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 563
    invoke-virtual {p2, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 564
    const/4 v3, 0x0

    .line 580
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStrike()Z

    move-result v6

    if-nez v6, :cond_7

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getDStrike()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 581
    :cond_7
    if-eqz v3, :cond_8

    .line 582
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " line-through"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 583
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 584
    invoke-virtual {p2, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 585
    const/4 v3, 0x0

    .line 708
    :cond_8
    :goto_3
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 441
    .end local v3    # "textDec":Ljava/lang/String;
    .end local v5    # "underline":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    :cond_9
    iget-boolean v6, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->lHOnlyForBody:Z

    if-nez v6, :cond_2

    iget-boolean v6, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->isPpt:Z

    if-nez v6, :cond_2

    goto/16 :goto_1

    .line 466
    .restart local v2    # "size":F
    :cond_a
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v6

    int-to-float v6, v6

    div-float v2, v6, v8

    .line 469
    mul-float v6, v2, v9

    div-float/2addr v6, v10

    goto/16 :goto_2

    .line 592
    .end local v2    # "size":F
    .restart local v3    # "textDec":Ljava/lang/String;
    .restart local v5    # "underline":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    :cond_b
    if-eqz v3, :cond_8

    .line 593
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 594
    invoke-virtual {p2, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public createParagraphStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 6
    .param p1, "paraProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .param p2, "val"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v5, -0x1

    .line 724
    if-nez p1, :cond_0

    .line 725
    const-string/jumbo v4, ""

    .line 841
    :goto_0
    return-object v4

    .line 728
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 741
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v3

    .line 742
    .local v3, "spaceProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;
    if-eqz v3, :cond_4

    .line 743
    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v4

    if-eq v4, v5, :cond_2

    .line 753
    :cond_2
    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getAfter()I

    move-result v4

    if-eq v4, v5, :cond_3

    .line 761
    :cond_3
    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getBefore()I

    move-result v4

    if-eq v4, v5, :cond_4

    .line 770
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v1

    .line 771
    .local v1, "bordersProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;
    const/4 v0, 0x0

    .line 772
    .local v0, "border":Z
    if-eqz v1, :cond_5

    .line 773
    invoke-virtual {v1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->createBorderStyle(Ljava/lang/StringBuilder;)V

    .line 774
    const/4 v0, 0x1

    .line 777
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v2

    .line 778
    .local v2, "indProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;
    if-eqz v2, :cond_8

    .line 790
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getRightInd()I

    move-result v4

    if-eqz v4, :cond_6

    .line 798
    :cond_6
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getFirstInd()I

    move-result v4

    if-eqz v4, :cond_7

    .line 806
    :cond_7
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getHangingInd()I

    move-result v4

    if-eqz v4, :cond_8

    .line 818
    :cond_8
    if-eqz v0, :cond_9

    .line 824
    const/4 v0, 0x0

    .line 841
    :cond_9
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public createTableStyle(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 1
    .param p1, "tableProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;
    .param p2, "tableStyle"    # Ljava/lang/StringBuilder;

    .prologue
    .line 339
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 340
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->createBorderStyle(Ljava/lang/StringBuilder;)V

    .line 343
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 349
    :cond_1
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefCharProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->defCharProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    return-object v0
.end method

.method public getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->defParaStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    return-object v0
.end method

.method public getDefTableStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->defTableStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    return-object v0
.end method

.method getParaShadingColor(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;)Ljava/lang/String;
    .locals 6
    .param p1, "paraShading"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    .prologue
    .line 305
    const-string/jumbo v0, "000000"

    .line 306
    .local v0, "autoColor":Ljava/lang/String;
    const-string/jumbo v3, "ffffff"

    .line 307
    .local v3, "defaultFill":Ljava/lang/String;
    const/4 v1, 0x0

    .line 309
    .local v1, "bgColor":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getPattern()Ljava/lang/String;

    move-result-object v4

    .line 310
    .local v4, "wVal":Ljava/lang/String;
    const-string/jumbo v5, "clear"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 312
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getFill()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 313
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getFill()Ljava/lang/String;

    move-result-object v1

    .line 334
    :cond_0
    :goto_0
    return-object v1

    .line 315
    :cond_1
    move-object v1, v3

    goto :goto_0

    .line 318
    :cond_2
    const-string/jumbo v5, "solid"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 320
    const/4 v2, 0x0

    .line 321
    .local v2, "color":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 322
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    .line 324
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->isAuto()Z

    move-result v5

    if-nez v5, :cond_5

    :cond_4
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 327
    :cond_5
    move-object v1, v0

    goto :goto_0

    .line 329
    :cond_6
    move-object v1, v2

    goto :goto_0
.end method

.method public getStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .locals 1
    .param p1, "styleID"    # Ljava/lang/String;

    .prologue
    .line 960
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->styles:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    return-object v0
.end method

.method public getStyles()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 866
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->styles:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method protected onDocumentRead()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v2, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->styles:Ljava/util/LinkedHashMap;

    .line 147
    const/4 v0, 0x0

    .line 150
    .local v0, "is":Ljava/io/InputStream;
    :try_start_0
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleParser;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleParser;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/IXWPFStyleUpdate;Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;)V

    .line 151
    .local v1, "styleParser":Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleParser;
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 152
    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleParser;->parse(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 170
    :cond_0
    return-void

    .line 154
    .end local v1    # "styleParser":Lcom/samsung/thumbnail/office/ooxml/word/style/XDocStyleParser;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 155
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v2
.end method

.method public setCurrStyleLineHeight(Ljava/lang/String;)V
    .locals 0
    .param p1, "currStyleLineHight"    # Ljava/lang/String;

    .prologue
    .line 136
    return-void
.end method

.method public setDefaultCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 0
    .param p1, "defCharProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->defCharProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 111
    return-void
.end method

.method public setDefaultParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V
    .locals 0
    .param p1, "defParaProperties"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->defParaProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .line 106
    return-void
.end method

.method public setIsPPt(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 718
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->isPpt:Z

    .line 719
    return-void
.end method

.method public setexcelfontsizechk(Z)V
    .locals 1
    .param p1, "val"    # Z

    .prologue
    .line 713
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->lHOnlyForBody:Z

    .line 714
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->excelfontsizechk:Z

    .line 715
    return-void
.end method

.method public updateStyleTree()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 872
    iget-object v6, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->styles:Ljava/util/LinkedHashMap;

    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v5

    .line 873
    .local v5, "styleCollection":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;>;"
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 878
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 879
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 881
    .local v4, "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    if-eqz v4, :cond_0

    .line 887
    :goto_0
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getBasedOn()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 888
    iget-object v6, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->styles:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getBasedOn()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 891
    .local v3, "parentStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    invoke-virtual {v4, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->setParentStyle(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)V

    .line 893
    if-eqz v3, :cond_0

    .line 897
    move-object v4, v3

    .line 898
    goto :goto_0

    .line 900
    .end local v3    # "parentStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .end local v4    # "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_1
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 902
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;>;"
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 903
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 905
    .restart local v4    # "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    if-eqz v4, :cond_2

    .line 909
    sget-object v6, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XWPFStyle$EStyleType:[I

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getStyleType()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 913
    :pswitch_1
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getDefault()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 915
    iput-object v4, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->defParaStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 917
    :cond_3
    iget-object v6, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->defCharProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    iget-object v7, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->defParaProperties:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    invoke-virtual {v4, v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->mergeWithParent(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    goto :goto_1

    .line 927
    :pswitch_2
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getDefault()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 929
    iput-object v4, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->defTableStyle:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 930
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v0

    .line 931
    .local v0, "defParaStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v6

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->mergeWithParent(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    goto :goto_1

    .line 934
    .end local v0    # "defParaStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_4
    invoke-virtual {v4, v8, v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->mergeWithParent(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    goto :goto_1

    .line 950
    .end local v4    # "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_5
    return-void

    .line 909
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public writeStyles()V
    .locals 10

    .prologue
    const/16 v9, 0xc

    const/4 v8, 0x1

    .line 174
    iget-object v6, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->styles:Ljava/util/LinkedHashMap;

    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v5

    .line 175
    .local v5, "styleCollection":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;>;"
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 177
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 178
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    .line 183
    .local v4, "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    invoke-virtual {p0, v4}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->addStyleProp(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)Ljava/lang/String;

    .line 189
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getStyleType()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    move-result-object v6

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->TABLE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 190
    new-array v1, v9, [Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    const/4 v6, 0x0

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->FIRST_ROW:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    sget-object v6, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->LAST_ROW:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v6, v1, v8

    const/4 v6, 0x2

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->FIRST_COL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/4 v6, 0x3

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->LAST_COL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/4 v6, 0x4

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND1_VERT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/4 v6, 0x5

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND2_VERT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/4 v6, 0x6

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND1_HORZ:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/4 v6, 0x7

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND2_HORZ:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/16 v6, 0x8

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->NE_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/16 v6, 0x9

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->NW_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/16 v6, 0xa

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->SE_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    const/16 v6, 0xb

    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->SW_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    aput-object v7, v1, v6

    .line 204
    .local v1, "eTblStyleOverrideTypes":[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v9, :cond_0

    .line 205
    aget-object v6, v1, v2

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getTblStyleProp(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 206
    aget-object v6, v1, v2

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getTblStyleProp(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v0

    .line 208
    .local v0, "currentStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    if-nez v0, :cond_2

    .line 204
    .end local v0    # "currentStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 210
    .restart local v0    # "currentStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_2
    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->addStyleProp(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;)Ljava/lang/String;

    goto :goto_1

    .line 224
    .end local v0    # "currentStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .end local v1    # "eTblStyleOverrideTypes":[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    .end local v2    # "i":I
    .end local v4    # "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_3
    iput-boolean v8, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->lHOnlyForBody:Z

    .line 225
    return-void
.end method
