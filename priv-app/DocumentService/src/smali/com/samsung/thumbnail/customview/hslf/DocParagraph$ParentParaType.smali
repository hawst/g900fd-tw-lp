.class public final enum Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;
.super Ljava/lang/Enum;
.source "DocParagraph.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ParentParaType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

.field public static final enum DOCUMENT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

.field public static final enum SHAPE:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

.field public static final enum TABLE:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 101
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    const-string/jumbo v1, "DOCUMENT"

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->DOCUMENT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    const-string/jumbo v1, "TABLE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->TABLE:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    const-string/jumbo v1, "SHAPE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->SHAPE:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    .line 100
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    sget-object v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->DOCUMENT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->TABLE:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->SHAPE:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->$VALUES:[Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 100
    const-class v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->$VALUES:[Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    return-object v0
.end method
