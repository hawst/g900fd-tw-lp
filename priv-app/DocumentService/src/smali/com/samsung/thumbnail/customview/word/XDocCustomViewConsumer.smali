.class public Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;
.super Ljava/lang/Object;
.source "XDocCustomViewConsumer.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer$1;
    }
.end annotation


# static fields
.field public static final CONST_DEVICE_WIDTH:I = 0x258

.field public static final HDR_FDR_DEF_HEIGHT:I = 0x5a

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field private static final TAG:Ljava/lang/String; = "XDocCustomViewConsumer"

.field public static final WIDTH:Ljava/lang/String; = "width"


# instance fields
.field private bIsDiagram:Z

.field private backgroundColor:I

.field private bodyElements:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;",
            ">;"
        }
    .end annotation
.end field

.field private breakPage:Z

.field private chartBuilder:Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;

.field private defTabStopPos:I

.field private elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

.field private folderName:Ljava/lang/String;

.field private folderPath:Ljava/io/File;

.field private hdrFdrLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeaderFooter;",
            ">;"
        }
    .end annotation
.end field

.field private isCoverPage:Z

.field private isHyperlink:Z

.field private isInGroup:Z

.field private isParaCreated:Z

.field private isShape:Z

.field private isTable:Z

.field private isTextBox:Z

.field private mBuCount:I

.field private mBuLvlState:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mBuValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field private mContext:Landroid/content/Context;

.field private mCurrentShapeId:Ljava/lang/String;

.field private mCustomView:Lcom/samsung/thumbnail/customview/word/CustomView;

.field private mDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

.field mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

.field private mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

.field private mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

.field public mFooterHeight:I

.field private mFooterParaNum:I

.field private mGroupFlipValue:Ljava/lang/String;

.field private mGroupHorAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

.field private mGroupHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

.field private mGroupMarginLeft:F

.field private mGroupMarginTop:F

.field private mGroupRotationAngle:J

.field private mGroupVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

.field private mGroupVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

.field private mGroupXAnchor:Ljava/lang/String;

.field private mGroupYAnchor:Ljava/lang/String;

.field private mGroupZorder:J

.field public mHeaderHeight:I

.field private mHeaderParaNum:I

.field private mNormalParaNum:I

.field private mParaStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;",
            ">;"
        }
    .end annotation
.end field

.field private mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

.field private mPreLvl:I

.field private mPreNumFmt:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

.field private mPreNumId:I

.field private mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

.field private mShapeCount:I

.field private mShapeFlipValue:Ljava/lang/String;

.field private mShapeHeight:F

.field private mShapeHorAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

.field private mShapeHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

.field private mShapeMarginLeft:F

.field private mShapeMarginTop:F

.field mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

.field private mShapeRotationAngle:J

.field private mShapeVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

.field private mShapeVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

.field private mShapeWidth:F

.field private mShapeZorder:J

.field private mTable:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;",
            ">;"
        }
    .end annotation
.end field

.field private mTableCell:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;",
            ">;"
        }
    .end annotation
.end field

.field private mTableRow:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;",
            ">;"
        }
    .end annotation
.end field

.field private paraNumber:I

.field public pgHeight:F

.field private run:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;",
            ">;"
        }
    .end annotation
.end field

.field private sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

.field private sectPrLst:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;",
            ">;"
        }
    .end annotation
.end field

.field private text:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CustomView;)V
    .locals 2
    .param p1, "doc"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;
    .param p2, "mContext"    # Landroid/content/Context;
    .param p3, "customView"    # Lcom/samsung/thumbnail/customview/word/CustomView;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPreNumId:I

    .line 148
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isTable:Z

    .line 158
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->bIsDiagram:Z

    .line 196
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->breakPage:Z

    .line 226
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    .line 227
    iput-object p2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mContext:Landroid/content/Context;

    .line 228
    iput-object p3, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCustomView:Lcom/samsung/thumbnail/customview/word/CustomView;

    .line 229
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->init()V

    .line 230
    return-void
.end method

.method private addEmptyRun()V
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 1273
    new-instance v0, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 1274
    .local v0, "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 1275
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v1

    if-eq v1, v2, :cond_2

    .line 1277
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 1300
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v1, :cond_5

    .line 1301
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 1305
    :cond_1
    :goto_1
    return-void

    .line 1281
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefCharProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v1

    if-eq v1, v2, :cond_3

    .line 1283
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefCharProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    goto :goto_0

    .line 1288
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v1

    const-string/jumbo v2, "Normal"

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1289
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v1

    const-string/jumbo v2, "Normal"

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1291
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v2

    const-string/jumbo v3, "Normal"

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    goto/16 :goto_0

    .line 1297
    :cond_4
    const/high16 v1, 0x41300000    # 11.0f

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    goto/16 :goto_0

    .line 1302
    :cond_5
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v1, :cond_1

    .line 1303
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1
.end method

.method private applyCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/customview/word/Run;)V
    .locals 7
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p2, "drawRun"    # Lcom/samsung/thumbnail/customview/word/Run;

    .prologue
    const/16 v6, 0x23

    const/4 v2, 0x1

    .line 2517
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_c

    .line 2518
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBold()Z

    move-result v4

    invoke-virtual {p2, v3, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setTypeface(Ljava/lang/String;Z)V

    .line 2523
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2525
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v4, 0x23

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p2, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2532
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getShadeColor()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2533
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getShadeColor()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getFill()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setShadingColor(Ljava/lang/String;)V

    .line 2536
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getHighlightColor()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2537
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getHighlightColor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setHighLightedColor(Ljava/lang/String;)V

    .line 2540
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v3

    if-lez v3, :cond_4

    .line 2542
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v1, v3, v4

    .line 2543
    .local v1, "fontSize":F
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    float-to-double v4, v1

    invoke-virtual {v3, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual {p2, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 2546
    .end local v1    # "fontSize":F
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStyleId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStyleId()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "Hyperlink"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2548
    invoke-virtual {p2, v2}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 2549
    const v3, -0xffff01

    invoke-virtual {p2, v3}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 2552
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getUnderlineProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getUnderlineProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 2554
    invoke-virtual {p2, v2}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 2556
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStrike()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getDStrike()Z

    move-result v3

    if-eqz v3, :cond_d

    :cond_7
    :goto_2
    invoke-virtual {p2, v2}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 2558
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getItalic()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2559
    invoke-virtual {p2}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 2562
    :cond_8
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getCaps()Z

    move-result v2

    if-nez v2, :cond_9

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getSmallCaps()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2563
    :cond_9
    invoke-virtual {p2}, Lcom/samsung/thumbnail/customview/word/Run;->setCAPLetterON()V

    .line 2566
    :cond_a
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getVertAlign()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 2567
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getVertAlign()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/samsung/thumbnail/customview/word/Run;->setVertivalAlignment(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 2569
    :cond_b
    return-void

    .line 2519
    :cond_c
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBold()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2520
    invoke-virtual {p2}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    goto/16 :goto_0

    .line 2527
    :catch_0
    move-exception v0

    .line 2528
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "XDocCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2556
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_d
    const/4 v2, 0x0

    goto :goto_2
.end method

.method private applyGroupPropertiesToShape()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 441
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 442
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupMarginLeft:F

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    .line 444
    :cond_0
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 445
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupMarginTop:F

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    .line 449
    :cond_1
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupZorder:J

    iput-wide v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    .line 450
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupRotationAngle:J

    iput-wide v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeRotationAngle:J

    .line 451
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupHorAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeHorAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 452
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    .line 453
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 454
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    .line 455
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupFlipValue:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeFlipValue:Ljava/lang/String;

    .line 456
    return-void
.end method

.method private applyParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/customview/word/Run;)V
    .locals 7
    .param p1, "paraProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .param p2, "drawRun"    # Lcom/samsung/thumbnail/customview/word/Run;

    .prologue
    const/high16 v6, 0x43700000    # 240.0f

    const/high16 v5, 0x41a00000    # 20.0f

    const/high16 v4, -0x40800000    # -1.0f

    .line 2580
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2581
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getLeftInd()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2, v2}, Lcom/samsung/thumbnail/customview/word/Run;->setLeftIndent(I)V

    .line 2583
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getRightInd()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2, v2}, Lcom/samsung/thumbnail/customview/word/Run;->setRightIndent(I)V

    .line 2585
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getFirstInd()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2, v2}, Lcom/samsung/thumbnail/customview/word/Run;->setFirstLineIndent(I)V

    .line 2589
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2591
    const/4 v0, 0x0

    .line 2593
    .local v0, "lineRule":Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLineRule()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 2594
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLineRule()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "auto"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2596
    sget-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;->AUTO:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    .line 2608
    :cond_1
    :goto_0
    iget-boolean v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v2, :cond_8

    .line 2609
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineRule(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;)V

    .line 2615
    .end local v0    # "lineRule":Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v2

    if-eqz v2, :cond_3

    .line 2617
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v2

    int-to-float v2, v2

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_a

    .line 2618
    iget-boolean v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v2, :cond_9

    .line 2619
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    .line 2650
    :cond_3
    :goto_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getStyleId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2652
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getStyleId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v1

    .line 2654
    .local v1, "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    if-eqz v1, :cond_4

    .line 2656
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2657
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->applyCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 2667
    .end local v1    # "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_4
    return-void

    .line 2597
    .restart local v0    # "lineRule":Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLineRule()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "atLeast"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2599
    sget-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;->AT_LEAST:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    goto :goto_0

    .line 2600
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLineRule()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "exact"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2602
    sget-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;->EXACTLY:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    goto/16 :goto_0

    .line 2605
    :cond_7
    sget-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;->NONE:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    goto/16 :goto_0

    .line 2611
    :cond_8
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineRule(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;)V

    goto/16 :goto_1

    .line 2621
    .end local v0    # "lineRule":Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;
    :cond_9
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v2, :cond_3

    .line 2622
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    goto :goto_2

    .line 2626
    :cond_a
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLineRule()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;

    move-result-object v2

    if-eqz v2, :cond_d

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLineRule()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "atLeast"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLineRule()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty$LineRule;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "exact"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2632
    :cond_b
    iget-boolean v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v2, :cond_c

    .line 2633
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    goto/16 :goto_2

    .line 2635
    :cond_c
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v2, :cond_3

    .line 2636
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    goto/16 :goto_2

    .line 2640
    :cond_d
    iget-boolean v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v2, :cond_e

    .line 2641
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v6

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    goto/16 :goto_2

    .line 2643
    :cond_e
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v2, :cond_3

    .line 2644
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v6

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    goto/16 :goto_2
.end method

.method private applyStyleParameters(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;Ljava/lang/String;Z)V
    .locals 52
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "style"    # Ljava/lang/String;
    .param p3, "isGroupOrShapeStyle"    # Z

    .prologue
    .line 2938
    const/16 v31, 0x0

    .line 2939
    .local v31, "shapeMarginLeft":F
    const/16 v32, 0x0

    .line 2940
    .local v32, "shapeMarginTop":F
    const/16 v33, 0x0

    .line 2941
    .local v33, "shapeWidth":F
    const/16 v30, 0x0

    .line 2942
    .local v30, "shapeHeight":F
    const-wide/16 v36, 0x0

    .line 2943
    .local v36, "shapeZorder":J
    const-wide/16 v34, 0x0

    .line 2944
    .local v34, "shapeRotationAngle":J
    const/4 v10, 0x0

    .line 2945
    .local v10, "horiAlignEle":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    const/4 v9, 0x0

    .line 2946
    .local v9, "horRelEle":Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;
    const/16 v44, 0x0

    .line 2947
    .local v44, "vertAlignEle":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    const/16 v45, 0x0

    .line 2948
    .local v45, "vertRelEle":Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
    const/16 v29, 0x0

    .line 2949
    .local v29, "shapeFlipValue":Ljava/lang/String;
    const/4 v6, 0x0

    .line 2950
    .local v6, "doTwipsConversion":Z
    const/4 v14, 0x0

    .line 2951
    .local v14, "leftPerCent":F
    const/16 v42, 0x0

    .line 2953
    .local v42, "topPerCent":F
    if-nez p1, :cond_1

    if-nez p3, :cond_1

    .line 3241
    :cond_0
    :goto_0
    return-void

    .line 2957
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    move-object/from16 v48, v0

    sget-object v49, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->FOOTER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    move-object/from16 v0, v48

    move-object/from16 v1, v49

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    move-object/from16 v48, v0

    sget-object v49, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->HEADER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    move-object/from16 v0, v48

    move-object/from16 v1, v49

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/office/word/Page;->isCoverPage()Z

    move-result v48

    if-eqz v48, :cond_3

    .line 2960
    :cond_2
    const/4 v6, 0x1

    .line 2964
    :cond_3
    if-eqz p2, :cond_34

    .line 2965
    const-string/jumbo v48, ";"

    move-object/from16 v0, p2

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v38

    .line 2966
    .local v38, "styleArray":[Ljava/lang/String;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    move-object/from16 v0, v38

    array-length v0, v0

    move/from16 v48, v0

    move/from16 v0, v48

    if-ge v11, v0, :cond_30

    .line 2967
    aget-object v48, v38, v11

    const-string/jumbo v49, "margin-left:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_5

    .line 2968
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    .line 2970
    .local v12, "leftMargin":Ljava/lang/String;
    invoke-static {v12, v6}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getValuesFromStyle(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    .line 2972
    invoke-static {v12}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Float;->floatValue()F

    move-result v15

    .line 2973
    .local v15, "lmargin":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v48

    float-to-int v0, v15

    move/from16 v49, v0

    invoke-virtual/range {v48 .. v49}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v48

    move/from16 v0, v48

    int-to-float v0, v0

    move/from16 v31, v0

    .line 2966
    .end local v12    # "leftMargin":Ljava/lang/String;
    .end local v15    # "lmargin":F
    :cond_4
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 2975
    :cond_5
    aget-object v48, v38, v11

    const-string/jumbo v49, "left:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_6

    const/16 v48, 0x0

    cmpl-float v48, v48, v31

    if-nez v48, :cond_6

    const/16 v48, 0x0

    cmpl-float v48, v14, v48

    if-nez v48, :cond_6

    .line 2977
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    .line 2979
    .restart local v12    # "leftMargin":Ljava/lang/String;
    invoke-static {v12, v6}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getValuesFromStyle(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    .line 2981
    invoke-static {v12}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Float;->floatValue()F

    move-result v15

    .line 2982
    .restart local v15    # "lmargin":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v48

    float-to-int v0, v15

    move/from16 v49, v0

    invoke-virtual/range {v48 .. v49}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v48

    move/from16 v0, v48

    int-to-float v0, v0

    move/from16 v31, v0

    .line 2984
    goto :goto_2

    .end local v12    # "leftMargin":Ljava/lang/String;
    .end local v15    # "lmargin":F
    :cond_6
    aget-object v48, v38, v11

    const-string/jumbo v49, "margin-top:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_7

    .line 2985
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v40

    .line 2987
    .local v40, "topMargin":Ljava/lang/String;
    move-object/from16 v0, v40

    invoke-static {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getValuesFromStyle(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v40

    .line 2989
    invoke-static/range {v40 .. v40}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Float;->floatValue()F

    move-result v39

    .line 2990
    .local v39, "tmargin":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v48

    move/from16 v0, v39

    float-to-int v0, v0

    move/from16 v49, v0

    invoke-virtual/range {v48 .. v49}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v48

    move/from16 v0, v48

    int-to-float v0, v0

    move/from16 v32, v0

    .line 2992
    goto/16 :goto_2

    .end local v39    # "tmargin":F
    .end local v40    # "topMargin":Ljava/lang/String;
    :cond_7
    aget-object v48, v38, v11

    const-string/jumbo v49, "top:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_8

    const/16 v48, 0x0

    cmpl-float v48, v48, v32

    if-nez v48, :cond_8

    const/16 v48, 0x0

    cmpl-float v48, v42, v48

    if-nez v48, :cond_8

    .line 2994
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v40

    .line 2996
    .restart local v40    # "topMargin":Ljava/lang/String;
    move-object/from16 v0, v40

    invoke-static {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getValuesFromStyle(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v40

    .line 2998
    invoke-static/range {v40 .. v40}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Float;->floatValue()F

    move-result v39

    .line 2999
    .restart local v39    # "tmargin":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v48

    move/from16 v0, v39

    float-to-int v0, v0

    move/from16 v49, v0

    invoke-virtual/range {v48 .. v49}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v48

    move/from16 v0, v48

    int-to-float v0, v0

    move/from16 v32, v0

    .line 3001
    goto/16 :goto_2

    .end local v39    # "tmargin":F
    .end local v40    # "topMargin":Ljava/lang/String;
    :cond_8
    aget-object v48, v38, v11

    const-string/jumbo v49, "width:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_9

    .line 3002
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v28

    .line 3004
    .local v28, "sWidth":Ljava/lang/String;
    move-object/from16 v0, v28

    invoke-static {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getValuesFromStyle(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v28

    .line 3006
    invoke-static/range {v28 .. v28}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Float;->floatValue()F

    move-result v46

    .line 3007
    .local v46, "width":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v48

    move/from16 v0, v46

    float-to-int v0, v0

    move/from16 v49, v0

    invoke-virtual/range {v48 .. v49}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v48

    move/from16 v0, v48

    int-to-float v0, v0

    move/from16 v33, v0

    .line 3009
    goto/16 :goto_2

    .end local v28    # "sWidth":Ljava/lang/String;
    .end local v46    # "width":F
    :cond_9
    aget-object v48, v38, v11

    const-string/jumbo v49, "height:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_a

    .line 3010
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v23

    .line 3012
    .local v23, "sHeight":Ljava/lang/String;
    move-object/from16 v0, v23

    invoke-static {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getValuesFromStyle(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v23

    .line 3014
    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 3015
    .local v8, "height":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v48

    float-to-int v0, v8

    move/from16 v49, v0

    invoke-virtual/range {v48 .. v49}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v48

    move/from16 v0, v48

    int-to-float v0, v0

    move/from16 v30, v0

    .line 3017
    goto/16 :goto_2

    .end local v8    # "height":F
    .end local v23    # "sHeight":Ljava/lang/String;
    :cond_a
    aget-object v48, v38, v11

    const-string/jumbo v49, "z-index:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_b

    .line 3018
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v47

    .line 3020
    .local v47, "zOrder":Ljava/lang/String;
    invoke-static/range {v47 .. v47}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Long;->longValue()J

    move-result-wide v36

    .line 3021
    goto/16 :goto_2

    .end local v47    # "zOrder":Ljava/lang/String;
    :cond_b
    aget-object v48, v38, v11

    const-string/jumbo v49, "rotation:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_e

    .line 3022
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v22

    .line 3024
    .local v22, "rotationAngle":Ljava/lang/String;
    const-string/jumbo v48, "fd"

    move-object/from16 v0, v22

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v48

    if-nez v48, :cond_c

    .line 3025
    invoke-static/range {v22 .. v22}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Long;->longValue()J

    move-result-wide v34

    goto/16 :goto_2

    .line 3026
    :cond_c
    const-string/jumbo v48, "fd"

    move-object/from16 v0, v22

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_4

    .line 3028
    const/16 v48, 0x0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v49

    add-int/lit8 v49, v49, -0x2

    move-object/from16 v0, v22

    move/from16 v1, v48

    move/from16 v2, v49

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Long;->longValue()J

    move-result-wide v48

    const-wide/32 v50, 0xfde8

    div-long v20, v48, v50

    .line 3030
    .local v20, "rotAngle":J
    :goto_3
    const-wide/16 v48, 0x168

    cmp-long v48, v20, v48

    if-lez v48, :cond_d

    .line 3031
    const-wide/16 v20, -0x168

    goto :goto_3

    .line 3032
    :cond_d
    move-wide/from16 v34, v20

    goto/16 :goto_2

    .line 3034
    .end local v20    # "rotAngle":J
    .end local v22    # "rotationAngle":Ljava/lang/String;
    :cond_e
    aget-object v48, v38, v11

    const-string/jumbo v49, "mso-position-horizontal:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_12

    .line 3035
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v25

    .line 3037
    .local v25, "sHoriAlignEle":Ljava/lang/String;
    const-string/jumbo v48, "Left"

    move-object/from16 v0, v48

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_f

    .line 3038
    sget-object v10, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->LEFT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    goto/16 :goto_2

    .line 3039
    :cond_f
    const-string/jumbo v48, "Center"

    move-object/from16 v0, v48

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_10

    .line 3040
    sget-object v10, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    goto/16 :goto_2

    .line 3041
    :cond_10
    const-string/jumbo v48, "Right"

    move-object/from16 v0, v48

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_11

    .line 3042
    sget-object v10, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->RIGHT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    goto/16 :goto_2

    .line 3043
    :cond_11
    const-string/jumbo v48, "outside"

    move-object/from16 v0, v48

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_4

    .line 3044
    sget-object v10, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->RIGHT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    goto/16 :goto_2

    .line 3046
    .end local v25    # "sHoriAlignEle":Ljava/lang/String;
    :cond_12
    aget-object v48, v38, v11

    const-string/jumbo v49, "mso-position-vertical:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_17

    .line 3047
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v27

    .line 3049
    .local v27, "sVertAlignEle":Ljava/lang/String;
    const-string/jumbo v48, "Top"

    move-object/from16 v0, v48

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_13

    .line 3050
    sget-object v44, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->TOP:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    goto/16 :goto_2

    .line 3051
    :cond_13
    const-string/jumbo v48, "Center"

    move-object/from16 v0, v48

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_14

    .line 3052
    sget-object v44, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    goto/16 :goto_2

    .line 3053
    :cond_14
    const-string/jumbo v48, "Bottom"

    move-object/from16 v0, v48

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_15

    .line 3054
    sget-object v44, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    goto/16 :goto_2

    .line 3055
    :cond_15
    const-string/jumbo v48, "Inside"

    move-object/from16 v0, v48

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_16

    .line 3056
    sget-object v44, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->INSIDE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    goto/16 :goto_2

    .line 3057
    :cond_16
    const-string/jumbo v48, "Outside"

    move-object/from16 v0, v48

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_4

    .line 3058
    sget-object v44, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->OUTSIDE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    goto/16 :goto_2

    .line 3060
    .end local v27    # "sVertAlignEle":Ljava/lang/String;
    :cond_17
    aget-object v48, v38, v11

    const-string/jumbo v49, "mso-position-horizontal-relative:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_22

    .line 3062
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v24

    .line 3064
    .local v24, "sHorRelEle":Ljava/lang/String;
    const-string/jumbo v48, "Margin"

    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_18

    .line 3065
    sget-object v9, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    goto/16 :goto_2

    .line 3066
    :cond_18
    const-string/jumbo v48, "Page"

    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_19

    .line 3067
    sget-object v9, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->PAGE:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    goto/16 :goto_2

    .line 3068
    :cond_19
    const-string/jumbo v48, "Column"

    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-nez v48, :cond_1a

    const-string/jumbo v48, "text"

    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_1b

    .line 3070
    :cond_1a
    sget-object v9, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->COLUMN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    goto/16 :goto_2

    .line 3071
    :cond_1b
    const-string/jumbo v48, "Character"

    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_1c

    .line 3072
    sget-object v9, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->CHARACTER:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    goto/16 :goto_2

    .line 3073
    :cond_1c
    const-string/jumbo v48, "Left Margin"

    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-nez v48, :cond_1d

    const-string/jumbo v48, "left-margin-area"

    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_1e

    .line 3076
    :cond_1d
    sget-object v9, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->LEFT_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    goto/16 :goto_2

    .line 3077
    :cond_1e
    const-string/jumbo v48, "Right Margin"

    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-nez v48, :cond_1f

    const-string/jumbo v48, "right-margin-area"

    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_20

    .line 3080
    :cond_1f
    sget-object v9, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->RIGHT_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    goto/16 :goto_2

    .line 3081
    :cond_20
    const-string/jumbo v48, "Inside Margin"

    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_21

    .line 3082
    sget-object v9, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->INSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    goto/16 :goto_2

    .line 3083
    :cond_21
    const-string/jumbo v48, "Outside Margin"

    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_4

    .line 3084
    sget-object v9, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->OUTSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    goto/16 :goto_2

    .line 3086
    .end local v24    # "sHorRelEle":Ljava/lang/String;
    :cond_22
    aget-object v48, v38, v11

    const-string/jumbo v49, "mso-position-vertical-relative:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_2b

    .line 3088
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v26

    .line 3090
    .local v26, "sVerRelEle":Ljava/lang/String;
    const-string/jumbo v48, "Margin"

    move-object/from16 v0, v48

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_23

    .line 3091
    sget-object v45, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    goto/16 :goto_2

    .line 3092
    :cond_23
    const-string/jumbo v48, "Page"

    move-object/from16 v0, v48

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_24

    .line 3093
    sget-object v45, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->PAGE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    goto/16 :goto_2

    .line 3094
    :cond_24
    const-string/jumbo v48, "Line"

    move-object/from16 v0, v48

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_25

    .line 3095
    sget-object v45, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->LINE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    goto/16 :goto_2

    .line 3096
    :cond_25
    const-string/jumbo v48, "Top Margin"

    move-object/from16 v0, v48

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-nez v48, :cond_26

    const-string/jumbo v48, "top-margin-area"

    move-object/from16 v0, v48

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_27

    .line 3098
    :cond_26
    sget-object v45, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->TOP_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    goto/16 :goto_2

    .line 3099
    :cond_27
    const-string/jumbo v48, "Bottom Margin"

    move-object/from16 v0, v48

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-nez v48, :cond_28

    const-string/jumbo v48, "bottom-margin-area"

    move-object/from16 v0, v48

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_29

    .line 3102
    :cond_28
    sget-object v45, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->BOTTOM_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    goto/16 :goto_2

    .line 3103
    :cond_29
    const-string/jumbo v48, "Inside Margin"

    move-object/from16 v0, v48

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_2a

    .line 3104
    sget-object v45, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->INSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    goto/16 :goto_2

    .line 3105
    :cond_2a
    const-string/jumbo v48, "Outside Margin"

    move-object/from16 v0, v48

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_4

    .line 3106
    sget-object v45, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->OUTSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    goto/16 :goto_2

    .line 3108
    .end local v26    # "sVerRelEle":Ljava/lang/String;
    :cond_2b
    aget-object v48, v38, v11

    const-string/jumbo v49, "flip:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_2c

    .line 3109
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v29

    goto/16 :goto_2

    .line 3111
    :cond_2c
    aget-object v48, v38, v11

    const-string/jumbo v49, "mso-top-percent:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_2d

    const/16 v48, 0x0

    cmpl-float v48, v32, v48

    if-nez v48, :cond_2d

    .line 3113
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v41

    .line 3117
    .local v41, "topMgn":Ljava/lang/String;
    :try_start_0
    invoke-static/range {v41 .. v41}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v48

    const/high16 v49, 0x447a0000    # 1000.0f

    div-float v42, v48, v49

    goto/16 :goto_2

    .line 3118
    :catch_0
    move-exception v7

    .line 3119
    .local v7, "e":Ljava/lang/NumberFormatException;
    const/16 v42, 0x0

    goto/16 :goto_2

    .line 3121
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    .end local v41    # "topMgn":Ljava/lang/String;
    :cond_2d
    aget-object v48, v38, v11

    const-string/jumbo v49, "mso-left-percent:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_2e

    const/16 v48, 0x0

    cmpl-float v48, v31, v48

    if-nez v48, :cond_2e

    .line 3123
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 3127
    .local v13, "leftMgn":Ljava/lang/String;
    :try_start_1
    invoke-static {v13}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Float;->floatValue()F
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v48

    const/high16 v49, 0x447a0000    # 1000.0f

    div-float v14, v48, v49

    goto/16 :goto_2

    .line 3128
    :catch_1
    move-exception v7

    .line 3129
    .restart local v7    # "e":Ljava/lang/NumberFormatException;
    const/4 v14, 0x0

    goto/16 :goto_2

    .line 3131
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    .end local v13    # "leftMgn":Ljava/lang/String;
    :cond_2e
    aget-object v48, v38, v11

    const-string/jumbo v49, "v-text-anchor:"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v48

    if-eqz v48, :cond_4

    .line 3132
    aget-object v48, v38, v11

    aget-object v49, v38, v11

    const-string/jumbo v50, ":"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    invoke-virtual/range {v48 .. v49}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v43

    .line 3134
    .local v43, "vTextAnchor":Ljava/lang/String;
    if-eqz p1, :cond_4

    .line 3135
    const-string/jumbo v48, "middle"

    move-object/from16 v0, v48

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_2f

    .line 3136
    sget-object v48, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto/16 :goto_2

    .line 3137
    :cond_2f
    const-string/jumbo v48, "bottom"

    move-object/from16 v0, v48

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_4

    .line 3138
    sget-object v48, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto/16 :goto_2

    .line 3146
    .end local v43    # "vTextAnchor":Ljava/lang/String;
    :cond_30
    const/16 v48, 0x0

    cmpl-float v48, v14, v48

    if-nez v48, :cond_31

    const/16 v48, 0x0

    cmpl-float v48, v42, v48

    if-eqz v48, :cond_33

    .line 3147
    :cond_31
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v48

    move-wide/from16 v0, v48

    double-to-float v5, v0

    .line 3149
    .local v5, "customViewWidth":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v48

    move-wide/from16 v0, v48

    double-to-float v4, v0

    .line 3151
    .local v4, "customViewHeight":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/office/word/Page;->getRightMargin()F

    move-result v18

    .line 3153
    .local v18, "pageRightMargin":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v17

    .line 3155
    .local v17, "pageLeftMargin":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/office/word/Page;->getTopMargin()F

    move-result v19

    .line 3157
    .local v19, "pageTopMargin":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/office/word/Page;->getBottomMargin()F

    move-result v16

    .line 3160
    .local v16, "pageBottomMargin":F
    if-eqz v9, :cond_32

    const/16 v48, 0x0

    cmpl-float v48, v31, v48

    if-nez v48, :cond_32

    .line 3161
    sget-object v48, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->RIGHT_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    move-object/from16 v0, v48

    if-ne v9, v0, :cond_36

    .line 3162
    mul-float v48, v14, v18

    sub-float v49, v5, v18

    add-float v31, v48, v49

    .line 3176
    :cond_32
    :goto_4
    if-eqz v45, :cond_33

    const/16 v48, 0x0

    cmpl-float v48, v32, v48

    if-nez v48, :cond_33

    .line 3177
    sget-object v48, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->TOP_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    if-ne v0, v1, :cond_39

    .line 3178
    mul-float v32, v42, v19

    .line 3194
    .end local v4    # "customViewHeight":F
    .end local v5    # "customViewWidth":F
    .end local v16    # "pageBottomMargin":F
    .end local v17    # "pageLeftMargin":F
    .end local v18    # "pageRightMargin":F
    .end local v19    # "pageTopMargin":F
    :cond_33
    :goto_5
    if-eqz p3, :cond_3c

    .line 3195
    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupMarginLeft:F

    .line 3196
    move/from16 v0, v32

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupMarginTop:F

    .line 3199
    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupZorder:J

    .line 3200
    move-wide/from16 v0, v34

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupRotationAngle:J

    .line 3201
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupHorAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 3202
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    .line 3203
    move-object/from16 v0, v44

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 3204
    move-object/from16 v0, v45

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    .line 3205
    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupFlipValue:Ljava/lang/String;

    .line 3221
    .end local v11    # "i":I
    .end local v38    # "styleArray":[Ljava/lang/String;
    :cond_34
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/office/word/Page;->isCoverPage()Z

    move-result v48

    if-eqz v48, :cond_35

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    move-wide/from16 v48, v0

    const-wide/16 v50, 0x0

    cmp-long v48, v48, v50

    if-nez v48, :cond_35

    .line 3224
    const-wide/16 v48, 0x1

    move-wide/from16 v0, v48

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    .line 3226
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isInGroup:Z

    move/from16 v48, v0

    if-eqz v48, :cond_35

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupZorder:J

    move-wide/from16 v48, v0

    const-wide/16 v50, 0x0

    cmp-long v48, v48, v50

    if-eqz v48, :cond_35

    .line 3227
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupZorder:J

    move-wide/from16 v48, v0

    move-wide/from16 v0, v48

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    .line 3232
    :cond_35
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isInGroup:Z

    move/from16 v48, v0

    if-eqz v48, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v48, v0

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/samsung/thumbnail/office/word/Page;->isCoverPage()Z

    move-result v48

    if-nez v48, :cond_0

    if-nez p3, :cond_0

    .line 3235
    if-nez v6, :cond_3d

    .line 3236
    invoke-direct/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->applyGroupPropertiesToShape()V

    goto/16 :goto_0

    .line 3164
    .restart local v4    # "customViewHeight":F
    .restart local v5    # "customViewWidth":F
    .restart local v11    # "i":I
    .restart local v16    # "pageBottomMargin":F
    .restart local v17    # "pageLeftMargin":F
    .restart local v18    # "pageRightMargin":F
    .restart local v19    # "pageTopMargin":F
    .restart local v38    # "styleArray":[Ljava/lang/String;
    :cond_36
    sget-object v48, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->LEFT_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    move-object/from16 v0, v48

    if-ne v9, v0, :cond_37

    .line 3165
    mul-float v31, v14, v17

    goto/16 :goto_4

    .line 3166
    :cond_37
    sget-object v48, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    move-object/from16 v0, v48

    if-ne v9, v0, :cond_38

    .line 3167
    sub-float v48, v5, v17

    sub-float v48, v48, v18

    mul-float v48, v48, v14

    add-float v31, v17, v48

    goto/16 :goto_4

    .line 3172
    :cond_38
    mul-float v31, v14, v5

    goto/16 :goto_4

    .line 3179
    :cond_39
    sget-object v48, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->BOTTOM_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    if-ne v0, v1, :cond_3a

    .line 3180
    mul-float v48, v42, v16

    sub-float v49, v4, v16

    add-float v32, v48, v49

    goto/16 :goto_5

    .line 3182
    :cond_3a
    sget-object v48, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    if-ne v0, v1, :cond_3b

    .line 3183
    sub-float v48, v4, v19

    sub-float v48, v48, v16

    mul-float v48, v48, v42

    add-float v32, v19, v48

    goto/16 :goto_5

    .line 3188
    :cond_3b
    mul-float v32, v42, v4

    goto/16 :goto_5

    .line 3207
    .end local v4    # "customViewHeight":F
    .end local v5    # "customViewWidth":F
    .end local v16    # "pageBottomMargin":F
    .end local v17    # "pageLeftMargin":F
    .end local v18    # "pageRightMargin":F
    .end local v19    # "pageTopMargin":F
    :cond_3c
    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    .line 3208
    move/from16 v0, v32

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    .line 3209
    move/from16 v0, v33

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeWidth:F

    .line 3210
    move/from16 v0, v30

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeHeight:F

    .line 3211
    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    .line 3212
    move-wide/from16 v0, v34

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeRotationAngle:J

    .line 3213
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeHorAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 3214
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    .line 3215
    move-object/from16 v0, v44

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 3216
    move-object/from16 v0, v45

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    .line 3217
    move-object/from16 v0, v29

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeFlipValue:Ljava/lang/String;

    goto/16 :goto_6

    .line 3238
    .end local v11    # "i":I
    .end local v38    # "styleArray":[Ljava/lang/String;
    :cond_3d
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupZorder:J

    move-wide/from16 v48, v0

    move-wide/from16 v0, v48

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    goto/16 :goto_0
.end method

.method private calcPageSize()V
    .locals 6

    .prologue
    const/16 v5, 0x5a

    const/high16 v2, 0x44460000    # 792.0f

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v3, 0x442f0000    # 700.0f

    .line 2442
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    if-eqz v1, :cond_4

    .line 2443
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getPgHeight()F

    move-result v1

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    .line 2448
    :goto_0
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_5

    .line 2449
    iput v3, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    .line 2461
    :cond_0
    :goto_1
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mHeaderHeight:I

    if-le v1, v5, :cond_1

    .line 2462
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    iget v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mHeaderHeight:I

    add-int/lit8 v2, v2, -0x5a

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    .line 2465
    :cond_1
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFooterHeight:I

    if-le v1, v5, :cond_2

    .line 2466
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    iget v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFooterHeight:I

    add-int/lit8 v2, v2, -0x5a

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    .line 2478
    :cond_2
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    if-nez v1, :cond_3

    .line 2479
    const/high16 v1, 0x44190000    # 612.0f

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    .line 2506
    :cond_3
    return-void

    .line 2445
    :cond_4
    iput v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    goto :goto_0

    .line 2450
    :cond_5
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 2451
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    const v2, 0x3f624dd3    # 0.884f

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    .line 2453
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_0

    .line 2454
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    sub-float v0, v1, v3

    .line 2455
    .local v0, "val":F
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mHeaderHeight:I

    int-to-float v1, v1

    div-float v2, v0, v4

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mHeaderHeight:I

    .line 2456
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFooterHeight:I

    int-to-float v1, v1

    div-float v2, v0, v4

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFooterHeight:I

    .line 2457
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    add-float/2addr v1, v0

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->pgHeight:F

    goto :goto_1
.end method

.method private createBullet(Lcom/samsung/thumbnail/customview/word/Run;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;Z)V
    .locals 30
    .param p1, "drawRun"    # Lcom/samsung/thumbnail/customview/word/Run;
    .param p2, "bulletText"    # Ljava/lang/String;
    .param p3, "paraProps"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .param p4, "paraStyle"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .param p5, "isFromStyle"    # Z

    .prologue
    .line 1311
    const/4 v12, 0x0

    .line 1312
    .local v12, "leftIndent":I
    const/4 v9, 0x0

    .line 1313
    .local v9, "hangingIndent":I
    const/16 v16, 0x0

    .line 1314
    .local v16, "numID":I
    const/4 v13, 0x0

    .line 1316
    .local v13, "lvl":I
    if-eqz p5, :cond_1

    .line 1317
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getNumbering()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;->getNumID()I

    move-result v16

    .line 1318
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getNumbering()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;->getLvl()I

    move-result v13

    .line 1324
    :goto_0
    if-nez v16, :cond_2

    .line 1585
    :cond_0
    :goto_1
    return-void

    .line 1320
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getNumbering()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;->getNumID()I

    move-result v16

    .line 1321
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getNumbering()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;->getLvl()I

    move-result v13

    goto :goto_0

    .line 1328
    :cond_2
    const/16 v19, 0x0

    .line 1331
    .local v19, "pStyle":Ljava/lang/String;
    const-string/jumbo v7, ""

    .line 1333
    .local v7, "bullet":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getNumbering()Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;

    move-result-object v18

    .line 1334
    .local v18, "numbering":Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;
    if-nez v18, :cond_15

    const/4 v15, 0x0

    .line 1335
    .local v15, "num":Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;
    :goto_2
    if-nez v15, :cond_16

    const/16 v26, 0x0

    :goto_3
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1336
    .local v2, "abstractId":I
    if-nez v18, :cond_17

    const/4 v3, 0x0

    .line 1339
    .local v3, "abstractNum":Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;
    :goto_4
    if-eqz v3, :cond_4

    .line 1340
    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->getNumStyleLink()Ljava/lang/String;

    move-result-object v19

    .line 1341
    if-eqz v19, :cond_4

    .line 1343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v26

    if-eqz v26, :cond_4

    .line 1344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v26

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->getNumStyleLink()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v20

    .line 1347
    .local v20, "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet()Z

    move-result v26

    if-eqz v26, :cond_4

    .line 1348
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getNumbering()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocNumberingProperty;->getNumID()I

    move-result v26

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->getNum(I)Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;

    move-result-object v15

    .line 1351
    if-eqz v15, :cond_3

    .line 1352
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->getAbstractNumId()I

    move-result v2

    .line 1353
    :cond_3
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->getAbstractNum(I)Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;

    move-result-object v3

    .line 1359
    .end local v20    # "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    :cond_4
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, "-"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1361
    .local v11, "key":Ljava/lang/String;
    const/16 v17, 0x0

    .line 1362
    .local v17, "numLvl":Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;
    const/4 v14, 0x0

    .line 1364
    .local v14, "lvlOverride":Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;
    if-eqz v15, :cond_5

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->getLvlOverride()Ljava/util/ArrayList;

    move-result-object v26

    if-eqz v26, :cond_5

    .line 1365
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->getLvlOverride()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "lvlOverride":Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;
    check-cast v14, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;

    .line 1368
    .restart local v14    # "lvlOverride":Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;
    :cond_5
    if-eqz v14, :cond_18

    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->getLvl()Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    move-result-object v26

    if-eqz v26, :cond_18

    .line 1369
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->getLvlOverride()Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->getLvl()Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    move-result-object v17

    .line 1373
    :cond_6
    :goto_5
    if-eqz v17, :cond_0

    .line 1375
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->getNumFmt()Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    move-result-object v8

    .line 1376
    .local v8, "curNumFmt":Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v21

    .line 1379
    .local v21, "prop":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    if-eqz v21, :cond_8

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v26

    if-eqz v26, :cond_8

    .line 1380
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getLeftInd()I

    move-result v26

    if-eqz v26, :cond_7

    .line 1381
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getLeftInd()I

    move-result v12

    .line 1382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v26

    add-int/lit16 v0, v12, -0x168

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v12

    .line 1385
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-object/from16 v27, v0

    int-to-float v0, v12

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getPageList()Ljava/util/ArrayList;

    move-result-object v26

    const/16 v29, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v26

    add-float v26, v26, v28

    move-object/from16 v0, v27

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBulletXPos(F)V

    .line 1390
    :cond_7
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getHangingInd()I

    move-result v26

    if-eqz v26, :cond_8

    .line 1391
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getHangingInd()I

    move-result v9

    .line 1392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v26

    invoke-static {v9}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v9

    .line 1398
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBulletSpaceStatus()Z

    move-result v26

    if-nez v26, :cond_b

    .line 1399
    if-eqz p3, :cond_a

    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v26

    if-eqz v26, :cond_a

    .line 1400
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getLeftInd()I

    move-result v26

    if-eqz v26, :cond_9

    .line 1401
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getLeftInd()I

    move-result v12

    .line 1402
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v26

    add-int/lit16 v0, v12, -0x168

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v12

    .line 1407
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-object/from16 v26, v0

    int-to-float v0, v12

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBulletXPos(F)V

    .line 1410
    :cond_9
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getHangingInd()I

    move-result v26

    if-eqz v26, :cond_a

    .line 1411
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getHangingInd()I

    move-result v9

    .line 1412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v26

    invoke-static {v9}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v9

    .line 1419
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v26

    const/16 v27, 0x0

    cmpl-float v26, v26, v27

    if-nez v26, :cond_19

    .line 1420
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-object/from16 v26, v0

    add-int v27, v12, v9

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 1427
    :cond_b
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPreNumFmt:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    move-object/from16 v26, v0

    if-eqz v26, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPreNumFmt:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    if-eq v0, v8, :cond_c

    .line 1428
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    .line 1431
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPreNumId:I

    move/from16 v26, v0

    const/16 v27, -0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_d

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPreNumId:I

    move/from16 v26, v0

    move/from16 v0, v26

    move/from16 v1, v16

    if-eq v0, v1, :cond_d

    .line 1432
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    .line 1433
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuLvlState:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    if-eqz v26, :cond_d

    .line 1434
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuLvlState:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    .line 1437
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPreLvl:I

    move/from16 v26, v0

    move/from16 v0, v26

    if-eq v0, v13, :cond_e

    .line 1438
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPreLvl:I

    move/from16 v26, v0

    move/from16 v0, v26

    if-ge v0, v13, :cond_1a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPreNumId:I

    move/from16 v26, v0

    move/from16 v0, v26

    move/from16 v1, v16

    if-ne v0, v1, :cond_1a

    .line 1439
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    .line 1446
    :cond_e
    :goto_7
    const/16 v23, 0x1

    .line 1447
    .local v23, "start":I
    if-eqz v14, :cond_1b

    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->isStartOverride()Z

    move-result v26

    if-eqz v26, :cond_1b

    .line 1448
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;->getStartOverride()I

    move-result v23

    .line 1452
    :goto_8
    const/16 v26, 0x1

    move/from16 v0, v23

    move/from16 v1, v26

    if-le v0, v1, :cond_f

    .line 1453
    add-int/lit8 v22, v23, -0x1

    .line 1454
    .local v22, "st":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuLvlState:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    if-eqz v26, :cond_1c

    .line 1455
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuLvlState:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v26

    add-int/lit8 v26, v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    .line 1461
    .end local v22    # "st":I
    :cond_f
    :goto_9
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->getText()Ljava/lang/String;

    move-result-object v5

    .line 1463
    .local v5, "buText":Ljava/lang/String;
    const/4 v6, 0x1

    .line 1464
    .local v6, "buTextCheck":Z
    sget-object v26, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->BULLET:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    move-object/from16 v0, v26

    if-eq v8, v0, :cond_12

    .line 1466
    invoke-static {v5}, Lcom/samsung/thumbnail/office/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_1d

    .line 1467
    const/4 v6, 0x0

    .line 1474
    :cond_10
    :goto_a
    if-eqz v6, :cond_12

    .line 1475
    invoke-static {v5}, Lcom/samsung/thumbnail/office/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v26

    if-nez v26, :cond_11

    const-string/jumbo v26, "%"

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_1e

    .line 1476
    :cond_11
    const/4 v6, 0x0

    .line 1495
    :cond_12
    const-string/jumbo v4, ""

    .line 1496
    .local v4, "buHtml":Ljava/lang/String;
    sget-object v26, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$numbering$ENumberFormat:[I

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->ordinal()I

    move-result v27

    aget v26, v26, v27

    packed-switch v26, :pswitch_data_0

    .line 1531
    :goto_b
    :pswitch_0
    if-eqz v6, :cond_13

    .line 1532
    invoke-virtual {v7, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1534
    :cond_13
    :goto_c
    if-eqz v6, :cond_14

    .line 1535
    invoke-static {v5}, Lcom/samsung/thumbnail/office/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v26

    if-nez v26, :cond_14

    const-string/jumbo v26, "%"

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_20

    .line 1552
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBulletSpaceStatus()Z

    move-result v26

    if-nez v26, :cond_24

    .line 1553
    sget-object v26, Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;->BULLET:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    move-object/from16 v0, v26

    if-eq v8, v0, :cond_21

    invoke-static {v5}, Lcom/samsung/thumbnail/office/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v26

    if-nez v26, :cond_21

    .line 1555
    invoke-virtual {v7, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1556
    invoke-virtual/range {p1 .. p2}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 1557
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBulletChar(Ljava/lang/String;)V

    .line 1558
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-object/from16 v26, v0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBulletPaint(Landroid/text/TextPaint;)V

    .line 1559
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    move/from16 v26, v0

    add-int/lit8 v26, v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    .line 1579
    .end local v5    # "buText":Ljava/lang/String;
    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuValues:Ljava/util/HashMap;

    move-object/from16 v26, v0

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string/jumbo v28, "-"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1580
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuLvlState:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v11, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1581
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPreNumFmt:Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;

    .line 1582
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPreNumId:I

    .line 1583
    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPreLvl:I

    .line 1584
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBulletSpaceStatus(Z)V

    goto/16 :goto_1

    .line 1334
    .end local v2    # "abstractId":I
    .end local v3    # "abstractNum":Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;
    .end local v4    # "buHtml":Ljava/lang/String;
    .end local v6    # "buTextCheck":Z
    .end local v8    # "curNumFmt":Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;
    .end local v11    # "key":Ljava/lang/String;
    .end local v14    # "lvlOverride":Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;
    .end local v15    # "num":Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;
    .end local v17    # "numLvl":Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;
    .end local v21    # "prop":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .end local v23    # "start":I
    :cond_15
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->getNum(I)Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;

    move-result-object v15

    goto/16 :goto_2

    .line 1335
    .restart local v15    # "num":Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;
    :cond_16
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNum;->getAbstractNumId()I

    move-result v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    goto/16 :goto_3

    .line 1336
    .restart local v2    # "abstractId":I
    :cond_17
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumbering;->getAbstractNum(I)Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;

    move-result-object v3

    goto/16 :goto_4

    .line 1370
    .restart local v3    # "abstractNum":Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;
    .restart local v11    # "key":Ljava/lang/String;
    .restart local v14    # "lvlOverride":Lcom/samsung/thumbnail/office/ooxml/word/XWPFNumLvlOverride;
    .restart local v17    # "numLvl":Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;
    :cond_18
    if-eqz v3, :cond_6

    .line 1371
    invoke-virtual {v3, v13}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFAbstractNum;->getNumLvl(I)Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;

    move-result-object v17

    goto/16 :goto_5

    .line 1422
    .restart local v8    # "curNumFmt":Lcom/samsung/thumbnail/office/ooxml/word/numbering/ENumberFormat;
    .restart local v21    # "prop":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLeftMargin()F

    move-result v27

    int-to-float v0, v9

    move/from16 v28, v0

    add-float v27, v27, v28

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    goto/16 :goto_6

    .line 1440
    :cond_1a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPreLvl:I

    move/from16 v26, v0

    move/from16 v0, v26

    if-le v0, v13, :cond_e

    .line 1441
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuLvlState:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    if-eqz v26, :cond_e

    .line 1442
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuLvlState:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    goto/16 :goto_7

    .line 1450
    .restart local v23    # "start":I
    :cond_1b
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/word/XDocNumLvl;->getStart()I

    move-result v23

    goto/16 :goto_8

    .line 1457
    .restart local v22    # "st":I
    :cond_1c
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    goto/16 :goto_9

    .line 1468
    .end local v22    # "st":I
    .restart local v5    # "buText":Ljava/lang/String;
    .restart local v6    # "buTextCheck":Z
    :cond_1d
    const-string/jumbo v26, "%"

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_10

    .line 1469
    const/4 v6, 0x0

    .line 1470
    invoke-virtual {v7, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1471
    const/4 v5, 0x0

    goto/16 :goto_a

    .line 1480
    :cond_1e
    const/16 v26, 0x25

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v10

    .line 1481
    .local v10, "index":I
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v5, v0, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1482
    add-int/lit8 v26, v10, 0x1

    add-int/lit8 v27, v10, 0x2

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v26

    add-int/lit8 v24, v26, -0x1

    .line 1484
    .local v24, "val":I
    add-int/lit8 v26, v10, 0x2

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1485
    move/from16 v0, v24

    if-ge v0, v13, :cond_12

    .line 1488
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, "-"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 1489
    .local v25, "valKey":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuValues:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    if-eqz v26, :cond_10

    .line 1490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuValues:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_a

    .line 1498
    .end local v10    # "index":I
    .end local v24    # "val":I
    .end local v25    # "valKey":Ljava/lang/String;
    .restart local v4    # "buHtml":Ljava/lang/String;
    :pswitch_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    move/from16 v26, v0

    add-int/lit8 v26, v26, 0x1

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 1499
    goto/16 :goto_b

    .line 1501
    :pswitch_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    move/from16 v26, v0

    add-int/lit8 v24, v26, 0x1

    .line 1502
    .restart local v24    # "val":I
    const/16 v26, 0xa

    move/from16 v0, v24

    move/from16 v1, v26

    if-ge v0, v1, :cond_1f

    .line 1503
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "0"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_b

    .line 1505
    :cond_1f
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 1507
    goto/16 :goto_b

    .line 1509
    .end local v24    # "val":I
    :pswitch_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    move/from16 v26, v0

    const/16 v27, 0x14

    invoke-static/range {v26 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getApha(II)Ljava/lang/String;

    move-result-object v4

    .line 1511
    goto/16 :goto_b

    .line 1513
    :pswitch_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    move/from16 v26, v0

    const/16 v27, 0xa

    invoke-static/range {v26 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getApha(II)Ljava/lang/String;

    move-result-object v4

    .line 1515
    goto/16 :goto_b

    .line 1517
    :pswitch_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    move/from16 v26, v0

    add-int/lit8 v26, v26, 0x1

    const/16 v27, 0x14

    invoke-static/range {v26 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getRomanDigit(II)Ljava/lang/String;

    move-result-object v4

    .line 1519
    goto/16 :goto_b

    .line 1521
    :pswitch_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuCount:I

    move/from16 v26, v0

    add-int/lit8 v26, v26, 0x1

    const/16 v27, 0xa

    invoke-static/range {v26 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getRomanDigit(II)Ljava/lang/String;

    move-result-object v4

    .line 1523
    goto/16 :goto_b

    .line 1539
    :cond_20
    const/16 v26, 0x25

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v10

    .line 1540
    .restart local v10    # "index":I
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v5, v0, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1541
    add-int/lit8 v26, v10, 0x1

    add-int/lit8 v27, v10, 0x2

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v26

    add-int/lit8 v24, v26, -0x1

    .line 1542
    .restart local v24    # "val":I
    add-int/lit8 v26, v10, 0x2

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1543
    move/from16 v0, v24

    if-ge v0, v13, :cond_14

    .line 1546
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string/jumbo v27, "-"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 1547
    .restart local v25    # "valKey":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuValues:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    if-eqz v26, :cond_13

    .line 1548
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuValues:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_c

    .line 1563
    .end local v10    # "index":I
    .end local v24    # "val":I
    .end local v25    # "valKey":Ljava/lang/String;
    :cond_21
    invoke-virtual/range {p1 .. p2}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 1566
    if-eqz v5, :cond_22

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-lt v0, v1, :cond_22

    .line 1567
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/String;->charAt(I)C

    move-result v27

    const/16 v28, 0x7f

    move/from16 v0, v27

    move/from16 v1, v28

    if-gt v0, v1, :cond_23

    const/16 v27, 0x0

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/String;->charAt(I)C

    move-result v27

    const/16 v28, 0x6f

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_23

    .end local v5    # "buText":Ljava/lang/String;
    :goto_e
    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBulletChar(Ljava/lang/String;)V

    .line 1573
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-object/from16 v26, v0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBulletPaint(Landroid/text/TextPaint;)V

    goto/16 :goto_d

    .line 1567
    .restart local v5    # "buText":Ljava/lang/String;
    :cond_23
    const/16 v27, 0x0

    move/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/String;->charAt(I)C

    move-result v27

    const/16 v28, 0x0

    invoke-static/range {v27 .. v28}, Lcom/samsung/thumbnail/util/Utils;->getUnicodeForBullet(IZ)Ljava/lang/String;

    move-result-object v5

    goto :goto_e

    .line 1576
    :cond_24
    invoke-virtual/range {p1 .. p2}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    goto/16 :goto_d

    .line 1496
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
    .end packed-switch
.end method

.method private drawShape(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V
    .locals 27
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .prologue
    .line 1619
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v25

    .line 1621
    .local v25, "shapeProp":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-result-object v5

    if-nez v5, :cond_1

    .line 1984
    :cond_0
    :goto_0
    return-void

    .line 1625
    :cond_1
    sget-object v5, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$shape$XWPFShapeStyle$EShapeType:[I

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1935
    :cond_2
    :goto_1
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-result-object v26

    .line 1936
    .local v26, "shapeType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    move-object/from16 v0, v26

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->getType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)Ljava/lang/String;

    move-result-object v23

    .line 1937
    .local v23, "shapeName":Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Lcom/samsung/thumbnail/util/Utils;->getShapeCategory(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1939
    .local v4, "shapeCategory":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->bIsDiagram:Z

    if-eqz v5, :cond_6c

    const-string/jumbo v5, "trapezium"

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string/jumbo v5, "trapezoid"

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string/jumbo v5, "blockArc"

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string/jumbo v5, "arc"

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string/jumbo v5, "unknown"

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6c

    .line 1945
    :cond_3
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v6, "Shape is not supported"

    invoke-direct {v5, v6}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1627
    .end local v4    # "shapeCategory":Ljava/lang/String;
    .end local v23    # "shapeName":Ljava/lang/String;
    .end local v26    # "shapeType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getShapeType(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v2

    .line 1628
    .local v2, "shapType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->merge(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    .line 1630
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getPath()Ljava/lang/String;

    move-result-object v24

    .line 1632
    .local v24, "shapePath":Ljava/lang/String;
    const-string/jumbo v5, "m@0,l,21600r21600,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1633
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->TRIANGLE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto :goto_1

    .line 1634
    :cond_4
    const-string/jumbo v5, "m,l,21600r21600,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1635
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTTRIANGLE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1636
    :cond_5
    const-string/jumbo v5, "m10800,l,10800,10800,21600,21600,10800xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1638
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->DIAMOND:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1639
    :cond_6
    const-string/jumbo v5, "m@0,l,21600@1,21600,21600,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1640
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->PARALLELOGRAM:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1641
    :cond_7
    const-string/jumbo v5, "m@0,l0@0,0@2@0,21600@1,21600,21600@2,21600@0@1,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1643
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->OCTAGON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1644
    :cond_8
    const-string/jumbo v5, "m10800,qx0@1l0@2qy10800,21600,21600@2l21600@1qy10800,xem0@1qy10800@0,21600@1nfe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1646
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CAN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1647
    :cond_9
    const-string/jumbo v5, "m@0,l@0@0,0@0,0@2@0@2@0,21600@1,21600@1@2,21600@2,21600@0@1@0@1,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1649
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->PLUS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1650
    :cond_a
    const-string/jumbo v5, "m@0,l,10800@0,21600@1,21600,21600,10800@1,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1652
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->HEXAGON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1653
    :cond_b
    const-string/jumbo v5, "m10800,l,8259,4200,21600r13200,l21600,8259xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 1655
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->PENTAGON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1656
    :cond_c
    const-string/jumbo v5, "m,l21600,21600e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 1657
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LINE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1658
    :cond_d
    const-string/jumbo v5, "m@0,qy0@0l0@2qx@0,21600l@1,21600qy21600@2l21600@0qx@1,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1660
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->PLAQUE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1661
    :cond_e
    const-string/jumbo v5, "m21600,qx,10800,21600,21600wa@0@10@6@11,21600,21600,21600,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 1663
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->MOON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1664
    :cond_f
    const-string/jumbo v5, "m@0,l0@0,,21600@1,21600,21600@2,21600,xem0@0nfl@1@0,21600,em@1@0nfl@1,21600e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 1666
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CUBE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1667
    :cond_10
    const-string/jumbo v5, "m,l,21600@0,21600,21600@0,21600,xem@0,21600nfl@3@5c@7@9@11@13,21600@0e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 1669
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FOLDEDCORNER:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1670
    :cond_11
    const-string/jumbo v5, "m,l,21600r21600,l21600,xem@0@0nfl@0@2@1@2@1@0xem,nfl@0@0em,21600nfl@0@2em21600,21600nfl@1@2em21600,nfl@1@0e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 1672
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BEVEL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1673
    :cond_12
    const-string/jumbo v5, "m10800,qx,10800,10800,21600,21600,10800,10800,xem7340,6445qx6215,7570,7340,8695,8465,7570,7340,6445xnfem14260,6445qx13135,7570,14260,8695,15385,7570,14260,6445xnfem4960@0c8853@3,12747@3,16640@0nfe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 1676
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->SMILEYFACE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1677
    :cond_13
    const-string/jumbo v5, "m8472,l,3890,7602,8382,5022,9705r7200,4192l10012,14915r11588,6685l14767,12877r1810,-870l11050,6797r1810,-717xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 1679
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LIGHTNINGBOLT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1680
    :cond_14
    const-string/jumbo v5, "m21600,10800l@15@14@15@18xem18436,3163l@17@12@16@13xem10800,l@14@10@18@10xem3163,3163l@12@13@13@12xem,10800l@10@18@10@14xem3163,18436l@13@16@12@17xem10800,21600l@18@15@14@15xem18436,18436l@16@17@17@16xem10800@19qx@19,10800,10800@20@20,10800,10800@19xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 1682
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->SUN:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1683
    :cond_15
    const-string/jumbo v5, "m@0,qx0@0l0@2qy@0,21600l@1,21600qx21600@2l21600@0qy@1,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 1685
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTALTERNATEPROCESS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1686
    :cond_16
    const-string/jumbo v5, "m@0,l,0@1,10800,,21600@0,21600,21600,10800xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 1688
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CHEVRON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1689
    :cond_17
    const-string/jumbo v5, "m@0,l,,,21600@0,21600,21600,10800xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 1690
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->HOMEPLATE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1691
    :cond_18
    const-string/jumbo v5, "m21600,qx10800@0l10800@2qy0@11,10800@3l10800@1qy21600,21600e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 1693
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTBRACE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1694
    :cond_19
    const-string/jumbo v5, "m,qx10800@0l10800@2qy21600@11,10800@3l10800@1qy,21600e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 1696
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTBRACE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1697
    :cond_1a
    const-string/jumbo v5, "m@9,nfqx@0@0l@0@7qy0@4@0@8l@0@6qy@9,21600em@10,nfqx@5@0l@5@7qy21600@4@5@8l@5@6qy@10,21600em@9,nsqx@0@0l@0@7qy0@4@0@8l@0@6qy@9,21600l@10,21600qx@5@6l@5@8qy21600@4@5@7l@5@0qy@10,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 1699
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BRACEPAIR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1700
    :cond_1b
    const-string/jumbo v5, "m21600,10800qy18019,21600l3581,21600qx,10800,3581,l18019,qx21600,10800xem18019,21600nfqx14438,10800,18019,e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 1702
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMAGNETICDRUM:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1703
    :cond_1c
    const-string/jumbo v5, "m4321,l21600,,17204,21600,,21600xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 1704
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTINPUTOUTPUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1705
    :cond_1d
    const-string/jumbo v5, "m,l,21600r21600,l21600,xem2610,nfl2610,21600em18990,nfl18990,21600e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 1707
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTPREDEFINEDPROCESS:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1708
    :cond_1e
    const-string/jumbo v5, "m,l,21600r21600,l21600,xem4236,nfl4236,21600em,4236nfl21600,4236e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 1710
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTINTERNALSTORAGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1711
    :cond_1f
    const-string/jumbo v5, "m,20172v945,400,1887,628,2795,913c3587,21312,4342,21370,5060,21597v2037,,2567,-227,3095,-285c8722,21197,9325,20970,9855,20800v490,-228,945,-400,1472,-740c11817,19887,12347,19660,12875,19375v567,-228,1095,-513,1700,-740c15177,18462,15782,18122,16537,17950v718,-113,1398,-398,2228,-513c19635,17437,20577,17322,21597,17322l21597,,,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_20

    .line 1713
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTDOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1714
    :cond_20
    const-string/jumbo v5, "m,20465v810,317,1620,452,2397,725c3077,21325,3790,21417,4405,21597v1620,,2202,-180,2657,-272c7580,21280,8002,21010,8455,20917v422,-135,810,-405,1327,-542c10205,20150,10657,19967,11080,19742v517,-182,970,-407,1425,-590c13087,19017,13605,18745,14255,18610v615,-180,1262,-318,1942,-408c16975,18202,17785,18022,18595,18022r,-1670l19192,16252r808,l20000,14467r722,-75l21597,14392,21597,,2972,r,1815l1532,1815r,1860l,3675,,20465xem1532,3675nfl18595,3675r,12677em2972,1815nfl20000,1815r,12652e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_21

    .line 1716
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMULTIDOCUMENT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1717
    :cond_21
    const-string/jumbo v5, "m3475,qx,10800,3475,21600l18125,21600qx21600,10800,18125,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_22

    .line 1719
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTTERMINATOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1720
    :cond_22
    const-string/jumbo v5, "m4353,l17214,r4386,10800l17214,21600r-12861,l,10800xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_23

    .line 1722
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTPREPARATION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1723
    :cond_23
    const-string/jumbo v5, "m,4292l21600,r,21600l,21600xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_24

    .line 1724
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMANUALINPUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1725
    :cond_24
    const-string/jumbo v5, "m,l21600,,17240,21600r-12880,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_25

    .line 1726
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMANUALOPERATION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1727
    :cond_25
    const-string/jumbo v5, "m10800,qx,10800,10800,21600,21600,10800,10800,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_26

    .line 1729
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTCONNECTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1730
    :cond_26
    const-string/jumbo v5, "m,l21600,r,17255l10800,21600,,17255xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_27

    .line 1732
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTOFFPAGECONNECTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1733
    :cond_27
    const-string/jumbo v5, "m4321,l21600,r,21600l,21600,,4338xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_28

    .line 1734
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTPUNCHEDCARD:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1735
    :cond_28
    const-string/jumbo v5, "m21597,19450v-225,-558,-750,-1073,-1650,-1545c18897,17605,17585,17347,16197,17260v-1500,87,-2700,345,-3787,645c11472,18377,10910,18892,10800,19450v-188,515,-750,1075,-1613,1460c8100,21210,6825,21425,5400,21597,3937,21425,2700,21210,1612,20910,675,20525,150,19965,,19450l,2147v150,558,675,1073,1612,1460c2700,3950,3937,4165,5400,4337,6825,4165,8100,3950,9187,3607v863,-387,1425,-902,1613,-1460c10910,1632,11472,1072,12410,600,13497,300,14697,85,16197,v1388,85,2700,300,3750,600c20847,1072,21372,1632,21597,2147xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_29

    .line 1737
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTPUNCHEDTAPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1738
    :cond_29
    const-string/jumbo v5, "m10800,qx,10800,10800,21600,21600,10800,10800,xem3163,3163nfl18437,18437em3163,18437nfl18437,3163e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2a

    .line 1740
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTSUMMINGJUNCTION:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1741
    :cond_2a
    const-string/jumbo v5, "m10800,qx,10800,10800,21600,21600,10800,10800,xem,10800nfl21600,10800em10800,nfl10800,21600e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2b

    .line 1743
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTOR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1744
    :cond_2b
    const-string/jumbo v5, "m21600,21600l,21600,21600,,,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2c

    .line 1745
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTCOLLATE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1746
    :cond_2c
    const-string/jumbo v5, "m10800,l,10800,10800,21600,21600,10800xem,10800nfl21600,10800e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2d

    .line 1748
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTSORT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1749
    :cond_2d
    const-string/jumbo v5, "m10800,l21600,21600,,21600xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2e

    .line 1750
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTEXTRACT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1751
    :cond_2e
    const-string/jumbo v5, "m,l21600,,10800,21600xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2f

    .line 1752
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMERGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1753
    :cond_2f
    const-string/jumbo v5, "m3600,21597c2662,21202,1837,20075,1087,18440,487,16240,75,13590,,10770,75,8007,487,5412,1087,3045,1837,1465,2662,337,3600,l21597,v-937,337,-1687,1465,-2512,3045c18485,5412,18072,8007,17997,10770v75,2820,488,5470,1088,7670c19910,20075,20660,21202,21597,21597xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_30

    .line 1755
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTONLINESTORAGE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1756
    :cond_30
    const-string/jumbo v5, "m10800,qx21600,10800,10800,21600l,21600,,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_31

    .line 1758
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTDELAY:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1759
    :cond_31
    const-string/jumbo v5, "ar,,21600,21600,18685,18165,10677,21597l20990,21597r,-3432xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_32

    .line 1761
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMAGNETICTAPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1762
    :cond_32
    const-string/jumbo v5, "m10800,qx,3391l,18209qy10800,21600,21600,18209l21600,3391qy10800,xem,3391nfqy10800,6782,21600,3391e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 1764
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTMAGNETICDISK:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1765
    :cond_33
    const-string/jumbo v5, "m17955,v862,282,1877,1410,2477,3045c21035,5357,21372,7895,21597,10827v-225,2763,-562,5300,-1165,7613c19832,20132,18817,21260,17955,21597r-14388,l,10827,3567,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_34

    .line 1767
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->FLOWCHARTDISPLAY:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1768
    :cond_34
    const-string/jumbo v5, "m21600,qx0@0l0@1qy21600,21600e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_35

    .line 1769
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTBRACKET:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1770
    :cond_35
    const-string/jumbo v5, "m,qx21600@0l21600@1qy,21600e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_36

    .line 1771
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTBRACKET:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1772
    :cond_36
    const-string/jumbo v5, "m@0,nfqx0@0l0@2qy@0,21600em@1,nfqx21600@0l21600@2qy@1,21600em@0,nsqx0@0l0@2qy@0,21600l@1,21600qx21600@2l21600@0qy@1,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_37

    .line 1774
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BRACKETPAIR:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1775
    :cond_37
    const-string/jumbo v5, "m,l,21600@0,21600@0@5@2@5@2@4,21600,10800@2@1@2@3@0@3@0,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_38

    .line 1777
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1778
    :cond_38
    const-string/jumbo v5, "m@0,l@0@3@2@3@2@1,,10800@2@4@2@5@0@5@0,21600,21600,21600,21600,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_39

    .line 1780
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1781
    :cond_39
    const-string/jumbo v5, "m0@0l@3@0@3@2@1@2,10800,0@4@2@5@2@5@0,21600@0,21600,21600,,21600xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3a

    .line 1783
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->UPARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1784
    :cond_3a
    const-string/jumbo v5, "m,l21600,,21600@0@5@0@5@2@4@2,10800,21600@1@2@3@2@3@0,0@0xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3b

    .line 1786
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->DOWNARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1787
    :cond_3b
    const-string/jumbo v5, "m@0,l@0@3@2@3@2@1,,10800@2@4@2@5@0@5@0,21600@8,21600@8@5@9@5@9@4,21600,10800@9@1@9@3@8@3@8,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3c

    .line 1789
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTRIGHTARROWCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1790
    :cond_3c
    const-string/jumbo v5, "ar,0@23@3@22,,0@4,0@15@23@1,0@7@2@13l@2@14@22@8@2@12wa,0@23@3@2@11@26@17,0@15@23@1@26@17@22@15xear,0@23@3,0@4@26@17nfe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3d

    .line 1792
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDRIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1793
    :cond_3d
    const-string/jumbo v5, "wr@22,0@21@3,,0@21@4@22@14@21@1@21@7@2@12l@2@13,0@8@2@11at@22,0@21@3@2@10@24@16@22@14@21@1@24@16,0@14xear@22@14@21@1@21@7@24@16nfe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3e

    .line 1795
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDLEFTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1796
    :cond_3e
    const-string/jumbo v5, "ar0@22@3@21,,0@4@21@14@22@1@21@7@21@12@2l@13@2@8,0@11@2wa0@22@3@21@10@2@16@24@14@22@1@21@16@24@14,xewr@14@22@1@21@7@21@16@24nfe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3f

    .line 1798
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDUPARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1799
    :cond_3f
    const-string/jumbo v5, "wr,0@3@23,0@22@4,0@15,0@1@23@7,0@13@2l@14@2@8@22@12@2at,0@3@23@11@2@17@26@15,0@1@23@17@26@15@22xewr,0@3@23@4,0@17@26nfe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_40

    .line 1801
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDDOWNARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1802
    :cond_40
    const-string/jumbo v5, "m@0,l@0@1,0@1,0@2@0@2@0,21600,21600,10800xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_41

    .line 1804
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1805
    :cond_41
    const-string/jumbo v5, "m@0,l@0@1,21600@1,21600@2@0@2@0,21600,,10800xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_42

    .line 1807
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1808
    :cond_42
    const-string/jumbo v5, "m0@0l@1@0@1,21600@2,21600@2@0,21600@0,10800,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_43

    .line 1810
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->UPARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1811
    :cond_43
    const-string/jumbo v5, "m0@0l@1@0@1,0@2,0@2@0,21600@0,10800,21600xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_44

    .line 1813
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->DOWNARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1814
    :cond_44
    const-string/jumbo v5, "m@0,l@0@1,0@1@5,10800,0@2@0@2@0,21600,21600,10800xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_45

    .line 1816
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->NOTCHEDRIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1817
    :cond_45
    const-string/jumbo v5, "m,10800l@0,21600@0@3@2@3@2,21600,21600,10800@2,0@2@1@0@1@0,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_46

    .line 1819
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->LEFTRIGHTARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1820
    :cond_46
    const-string/jumbo v5, "m10800,l21600@0@3@0@3@2,21600@2,10800,21600,0@2@1@2@1@0,0@0xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_47

    .line 1822
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->UPDOWNARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1823
    :cond_47
    const-string/jumbo v5, "m10800,l8280,8259,,8259r6720,5146l4200,21600r6600,-5019l17400,21600,14880,13405,21600,8259r-8280,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_48

    .line 1825
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STARSHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1826
    :cond_48
    const-string/jumbo v5, "m,c7200@0,14400@0,21600,m,21600c7200@1,14400@1,21600,21600e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_49

    .line 1828
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STARSHAPES:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1829
    :cond_49
    const-string/jumbo v5, "m10800,5800l8352,2295,7312,6320,370,2295,4627,7617,,8615r3722,3160l135,14587r5532,-650l4762,17617,7715,15627r770,5973l10532,14935r2715,4802l14020,14457r4125,3638l16837,12942r4763,348l17607,10475,21097,8137,16702,7315,18380,4457r-4225,868l14522,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4a

    .line 1831
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->IRREGULARSEAL1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1832
    :cond_4a
    const-string/jumbo v5, "m11462,4342l9722,1887,8550,6382,4502,3625r870,4192l1172,8270r2763,3322l,12877r3330,2493l1285,17825r3520,415l4917,21600,7527,18125r1173,1587l9872,17370r1740,1472l12180,15935r2762,1435l14640,14350r4237,1282l16380,12310r1890,-1020l16985,9402,21600,6645,16380,6532,18007,3172,14525,5777,14790,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4b

    .line 1834
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->IRREGULARSEAL2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1835
    :cond_4b
    const-string/jumbo v5, "m21600,10800l@2@3,10800,0@3@3,,10800@3@2,10800,21600@2@2xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4c

    .line 1837
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR4:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1838
    :cond_4c
    const-string/jumbo v5, "m21600,10800l@3@6,18436,3163@4@5,10800,0@6@5,3163,3163@5@6,,10800@5@4,3163,18436@6@3,10800,21600@4@3,18436,18436@3@4xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4d

    .line 1840
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR8:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1841
    :cond_4d
    const-string/jumbo v5, "m21600,10800l@5@10,20777,6667@7@12,18436,3163@8@11,14932,822@6@9,10800,0@10@9,6667,822@12@11,3163,3163@11@12,822,6667@9@10,,10800@9@6,822,14932@11@8,3163,18436@12@7,6667,20777@10@5,10800,21600@6@5,14932,20777@8@7,18436,18436@7@8,20777,14932@5@6xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4e

    .line 1843
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR16:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1844
    :cond_4e
    const-string/jumbo v5, "m21600,10800l@7@14,21232,8005@9@16,20153,5400@11@18,18437,3163@12@17,16200,1447@10@15,13595,368@8@13,10800,0@14@13,8005,368@16@15,5400,1447@18@17,3163,3163@17@18,1447,5400@15@16,368,8005@13@14,,10800@13@8,368,13595@15@10,1447,16200@17@12,3163,18437@18@11,5400,20153@16@9,8005,21232@14@7,10800,21600@8@7,13595,21232@10@9,16200,20153@12@11,18437,18437@11@12,20153,16200@9@10,21232,13595@7@8xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4f

    .line 1846
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR24:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1847
    :cond_4f
    const-string/jumbo v5, "m21600,10800l@9@18,21392,8693@11@20,20777,6667@13@22,19780,4800@15@24,18436,3163@16@23,16800,1820@14@21,14932,822@12@19,12907,208@10@17,10800,0@18@17,8693,208@20@19,6667,822@22@21,4800,1820@24@23,3163,3163@23@24,1820,4800@21@22,822,6667@19@20,208,8693@17@18,,10800@17@10,208,12907@19@12,822,14932@21@14,1820,16800@23@16,3163,18436@24@15,4800,19780@22@13,6667,20777@20@11,8693,21392@18@9,10800,21600@10@9,12907,21392@12@11,14932,20777@14@13,16800,19780@16@15,18436,18436@15@16,19780,16800@13@14,20777,14932@11@12,21392,12907@9@10xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_50

    .line 1849
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->STAR32:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1850
    :cond_50
    const-string/jumbo v5, "m0@29l@3@29qx@4@19l@4@10@5@10@5@19qy@6@29l@28@29@26@22@28@23@9@23@9@24qy@8,l@1,qx@0@24l@0@23,0@23,2700@22xem@4@19nfqy@3@20l@1@20qx@0@21@1@10l@4@10em@5@19nfqy@6@20l@8@20qx@9@21@8@10l@5@10em@0@21nfl@0@23em@9@21nfl@9@23e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_51

    .line 1852
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIBBON2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1853
    :cond_51
    const-string/jumbo v5, "m,l@3,qx@4@11l@4@10@5@10@5@11qy@6,l@21,0@19@15@21@16@9@16@9@17qy@8@22l@1@22qx@0@17l@0@16,0@16,2700@15xem@4@11nfqy@3@12l@1@12qx@0@13@1@10l@4@10em@5@11nfqy@6@12l@8@12qx@9@13@8@10l@5@10em@0@13nfl@0@16em@9@13nfl@9@16e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_52

    .line 1855
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RIBBON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1856
    :cond_52
    const-string/jumbo v5, "wr@9@34@8@35,0@24@0@23@9,0@8@11@0@22@19@22@9@34@8@35@19@23@3@24l@7@36@3@4at@9@31@8@32@3@4@18@30@9@1@8@33@18@28@17@28@9@31@8@32@17@30,0@4l@5@36xear@9@1@8@33@17@28@0@29nfl@17@30ewr@9@1@8@33@18@28@19@29nfl@18@30em@0@23nfl@0@29em@19@23nfl@19@29e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_53

    .line 1858
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ELLIPSERIBBON2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1859
    :cond_53
    const-string/jumbo v5, "ar@9@38@8@37,0@27@0@26@9@13@8@4@0@25@22@25@9@38@8@37@22@26@3@27l@7@40@3,wa@9@35@8@10@3,0@21@33@9@36@8@1@21@31@20@31@9@35@8@10@20@33,,l@5@40xewr@9@36@8@1@20@31@0@32nfl@20@33ear@9@36@8@1@21@31@22@32nfl@21@33em@0@26nfl@0@32em@22@26nfl@22@32e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_54

    .line 1861
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ELLIPSERIBBON:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1862
    :cond_54
    const-string/jumbo v5, "m@5,qx@1@2l@1@0@2@0qx0@7@2,21600l@9,21600qx@10@7l@10@1@11@1qx21600@2@11,xem@5,nfqx@6@2@5@1@4@3@5@2l@6@2em@5@1nfl@10@1em@2,21600nfqx@1@7l@1@0em@2@0nfqx@3@8@2@7l@1@7e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_55

    .line 1864
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->VERTICALSCROLL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1865
    :cond_55
    const-string/jumbo v5, "m0@5qy@2@1l@0@1@0@2qy@7,,21600@2l21600@9qy@7@10l@1@10@1@11qy@2,21600,0@11xem0@5nfqy@2@6@1@5@3@4@2@5l@2@6em@1@5nfl@1@10em21600@2nfqy@7@1l@0@1em@0@2nfqy@8@3@7@2l@7@1e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_56

    .line 1867
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->HORIZONTALSCROLL:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1868
    :cond_56
    const-string/jumbo v5, "m@28@0c@27@1@26@3@25@0l@21@4c@22@5@23@6@24@4xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_57

    .line 1870
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->WAVE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1871
    :cond_57
    const-string/jumbo v5, "m@43@0c@42@1@41@3@40@0@39@1@38@3@37@0l@30@4c@31@5@32@6@33@4@34@5@35@6@36@4xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_58

    .line 1873
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->DOUBLEWAVE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1874
    :cond_58
    const-string/jumbo v5, "m@7,l@8,m@5,21600l@6,21600e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_59

    .line 1875
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RECT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1876
    :cond_59
    const-string/jumbo v5, "m,l,21600r21600,l21600,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5a

    .line 1877
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->RECT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1878
    :cond_5a
    const-string/jumbo v5, "al10800,10800,10800,10800@2@14e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5b

    .line 1879
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->TEXTONPATH:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1880
    :cond_5b
    const-string/jumbo v5, "m,l0@8@12@24,0@9,,21600@6,21600@15@27@7,21600,21600,21600,21600@9@18@30,21600@8,21600,0@7,0@21@33@6,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5c

    .line 1882
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->WEDGERECTCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1883
    :cond_5c
    const-string/jumbo v5, "m3600,qx,3600l0@8@12@24,0@9,,18000qy3600,21600l@6,21600@15@27@7,21600,18000,21600qx21600,18000l21600@9@18@30,21600@8,21600,3600qy18000,l@7,0@21@33@6,xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5d

    .line 1885
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->WEDGEROUNDRECTCALLOUT:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1886
    :cond_5d
    const-string/jumbo v5, "m@0@1l@2@3nfem,l21600,r,21600l,21600xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5e

    .line 1888
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDERCALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1889
    :cond_5e
    const-string/jumbo v5, "m@0@1l@2@3@4@5nfem,l21600,r,21600l,21600xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5f

    .line 1891
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDERCALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1892
    :cond_5f
    const-string/jumbo v5, "m@0@1l@2@3@4@5@6@7nfem,l21600,r,21600l,21600xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_60

    .line 1894
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BORDERCALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1895
    :cond_60
    const-string/jumbo v5, "m@0@1l@2@3nfem@2,l@2,21600nfem,l21600,r,21600l,21600nsxe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_61

    .line 1897
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTCALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1898
    :cond_61
    const-string/jumbo v5, "m@0@1l@2@3@4@5nfem@4,l@4,21600nfem,l21600,r,21600l,21600nsxe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_62

    .line 1900
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTCALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1901
    :cond_62
    const-string/jumbo v5, "m@0@1l@2@3@4@5@6@7nfem@6,l@6,21600nfem,l21600,r,21600l,21600nsxe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_63

    .line 1903
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTCALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1904
    :cond_63
    const-string/jumbo v5, "m@0@1l@2@3nfem,l21600,r,21600l,21600nsxe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_64

    .line 1906
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1907
    :cond_64
    const-string/jumbo v5, "m@0@1l@2@3@4@5nfem,l21600,r,21600l,21600nsxe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_65

    .line 1909
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1910
    :cond_65
    const-string/jumbo v5, "m@0@1l@2@3@4@5@6@7nfem,l21600,r,21600l,21600nsxe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_66

    .line 1912
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1913
    :cond_66
    const-string/jumbo v5, "m@0@1l@2@3nfem@2,l@2,21600nfem,l21600,r,21600l,21600xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_67

    .line 1915
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTBORDERCALLOUT1:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1916
    :cond_67
    const-string/jumbo v5, "m@0@1l@2@3@4@5nfem@4,l@4,21600nfem,l21600,r,21600l,21600xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_68

    .line 1918
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTBORDERCALLOUT2:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1919
    :cond_68
    const-string/jumbo v5, "m@0@1l@2@3@4@5@6@7nfem@6,l@6,21600nfem,l21600,r,21600l,21600xe"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_69

    .line 1921
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->ACCENTBORDERCALLOUT3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1922
    :cond_69
    const-string/jumbo v5, "m,l@0,0@0,21600,21600,21600e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6a

    .line 1923
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->BENTCONNECTOR3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1924
    :cond_6a
    const-string/jumbo v5, "m,c@0,0@1,5400@1,10800@1,16200@2,21600,21600,21600e"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6b

    .line 1926
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->CURVEDCONNECTOR3:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1927
    :cond_6b
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_2

    .line 1928
    sget-object v5, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->UPDOWNARROW:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setShapeType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)V

    goto/16 :goto_1

    .line 1946
    .end local v2    # "shapType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .end local v24    # "shapePath":Ljava/lang/String;
    .restart local v4    # "shapeCategory":Ljava/lang/String;
    .restart local v23    # "shapeName":Ljava/lang/String;
    .restart local v26    # "shapeType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    :cond_6c
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1952
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isInGroup:Z

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setGroupShape(Z)V

    .line 1955
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeRotationAngle:J

    const-wide/16 v8, -0xb4

    cmp-long v5, v6, v8

    if-nez v5, :cond_6d

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->bIsDiagram:Z

    if-nez v5, :cond_6d

    .line 1956
    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeRotationAngle:J

    .line 1959
    :cond_6d
    new-instance v3, Lcom/samsung/thumbnail/customview/word/ShapesController;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->paraNumber:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeCount:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    move-object/from16 v0, p0

    iget v10, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeWidth:F

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeHeight:F

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeRotationAngle:J

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeHorAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeFlipValue:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v22, v0

    move-object/from16 v5, p1

    invoke-direct/range {v3 .. v22}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;IIFFFFJJLcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;Ljava/lang/String;Ljava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1966
    .local v3, "shapeCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    sget-object v6, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->HEADER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    if-ne v5, v6, :cond_70

    .line 1967
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setIsHeader(Z)V

    .line 1968
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getHeader()Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;->getHeaderName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setHeaderName(Ljava/lang/String;)V

    .line 1974
    :cond_6e
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCurrentShapeId:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setShapeStringId(Ljava/lang/String;)V

    .line 1976
    const-string/jumbo v5, "textOnPath"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6f

    .line 1977
    const-string/jumbo v5, "al10800,10800,10800,10800@2@14e"

    invoke-virtual {v3, v5}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setTextPath(Ljava/lang/String;)V

    .line 1980
    :cond_6f
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->bIsDiagram:Z

    invoke-virtual {v3, v5}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setDiagramShape(Z)V

    .line 1981
    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v3, v5}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 1983
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v5, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_0

    .line 1969
    :cond_70
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    sget-object v6, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->FOOTER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    if-ne v5, v6, :cond_6e

    .line 1970
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setIsFooter(Z)V

    .line 1971
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getFooter()Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;->getFooterName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/samsung/thumbnail/customview/word/ShapesController;->setFooterName(Ljava/lang/String;)V

    goto :goto_2

    .line 1625
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private drawShapePicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V
    .locals 7
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .prologue
    .line 2112
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getRefId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2113
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    invoke-direct {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    .line 2114
    .local v0, "pict":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v2

    .line 2115
    .local v2, "shapeProp":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getRefId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setImageRelId(Ljava/lang/String;)V

    .line 2116
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-result-object v3

    .line 2117
    .local v3, "shapeType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    invoke-virtual {v3, v3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->getType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)Ljava/lang/String;

    move-result-object v1

    .line 2118
    .local v1, "shapeName":Ljava/lang/String;
    const-string/jumbo v4, "ellipse"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "oval"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "circle"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2121
    :cond_0
    const-string/jumbo v4, "ellipse"

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setEnclosedSpName(Ljava/lang/String;)V

    .line 2123
    :cond_1
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->PARAGRAPH:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    iget v5, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->paraNumber:I

    const/4 v6, 0x0

    invoke-virtual {p0, v0, v4, v5, v6}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->writePicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;IZ)V

    .line 2125
    .end local v0    # "pict":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;
    .end local v1    # "shapeName":Ljava/lang/String;
    .end local v2    # "shapeProp":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;
    .end local v3    # "shapeType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    :cond_2
    return-void
.end method

.method private filterText(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 2677
    const-string/jumbo v0, "&#39;"

    const-string/jumbo v1, "\'"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2678
    const-string/jumbo v0, "&#34;"

    const-string/jumbo v1, "\""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2679
    const-string/jumbo v0, "&quot;"

    const-string/jumbo v1, "\""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2680
    const-string/jumbo v0, "&amp;"

    const-string/jumbo v1, "&"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2681
    const-string/jumbo v0, "&lt;"

    const-string/jumbo v1, "<"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2682
    const-string/jumbo v0, "&gt;"

    const-string/jumbo v1, ">"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2683
    const-string/jumbo v0, "&nbsp;"

    const-string/jumbo v1, " "

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2684
    const-string/jumbo v0, "&apos;"

    const-string/jumbo v1, "\'"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2685
    return-object p1
.end method

.method private getPictData(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;Ljava/util/List;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    .locals 20
    .param p1, "picture"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;",
            ">;)",
            "Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;"
        }
    .end annotation

    .prologue
    .line 2853
    .local p2, "relList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;>;"
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 2854
    .local v11, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;>;"
    const/4 v15, 0x0

    .line 2855
    .local v15, "rel":Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;
    const/4 v10, 0x0

    .line 2856
    .local v10, "imgPath":Ljava/lang/String;
    const/4 v12, 0x0

    .line 2858
    .local v12, "picData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2859
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "rel":Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;
    check-cast v15, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;

    .line 2860
    .restart local v15    # "rel":Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getRelId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2861
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->getTarget()Ljava/lang/String;

    move-result-object v10

    .line 2867
    :cond_1
    if-nez v10, :cond_2

    move-object v13, v12

    .line 2932
    .end local v12    # "picData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    .local v13, "picData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    :goto_0
    return-object v13

    .line 2871
    .end local v13    # "picData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    .restart local v12    # "picData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    :cond_2
    const/16 v2, 0x6d

    invoke-virtual {v10, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {v10, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 2873
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getFileInputStream()Ljava/io/FileInputStream;

    move-result-object v9

    .line 2875
    .local v9, "fileInputStream":Ljava/io/FileInputStream;
    const/16 v17, 0x0

    .line 2877
    .local v17, "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    :try_start_0
    new-instance v18, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;

    new-instance v2, Ljava/util/zip/ZipInputStream;

    invoke-direct {v2, v9}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v18

    invoke-direct {v0, v10, v2}, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;-><init>(Ljava/lang/String;Ljava/util/zip/ZipInputStream;)V
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2879
    .end local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .local v18, "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    :try_start_1
    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;->getFakeZipEntry()Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;

    move-result-object v19

    .line 2881
    .local v19, "zipEntry":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    const/4 v3, 0x0

    .line 2882
    .local v3, "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    const/4 v1, 0x0

    .line 2883
    .local v1, "packRel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    if-eqz v19, :cond_5

    .line 2884
    invoke-static/range {v19 .. v19}, Lorg/apache/poi/openxml4j/opc/ZipPackage;->buildPartName(Ljava/util/zip/ZipEntry;)Lorg/apache/poi/openxml4j/opc/PackagePartName;

    move-result-object v14

    .line 2888
    .local v14, "pkgPartName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    if-eqz v14, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v2

    iget-object v2, v2, Lorg/apache/poi/openxml4j/opc/OPCPackage;->contentTypeManager:Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;

    invoke-virtual {v2, v14}, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->getContentType(Lorg/apache/poi/openxml4j/opc/PackagePartName;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 2891
    new-instance v3, Lorg/apache/poi/openxml4j/opc/ZipPackagePart;

    .end local v3    # "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v4

    iget-object v4, v4, Lorg/apache/poi/openxml4j/opc/OPCPackage;->contentTypeManager:Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;

    invoke-virtual {v4, v14}, Lorg/apache/poi/openxml4j/opc/internal/ContentTypeManager;->getContentType(Lorg/apache/poi/openxml4j/opc/PackagePartName;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-direct {v3, v2, v0, v14, v4}, Lorg/apache/poi/openxml4j/opc/ZipPackagePart;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Ljava/util/zip/ZipEntry;Lorg/apache/poi/openxml4j/opc/PackagePartName;Ljava/lang/String;)V

    .line 2897
    .restart local v3    # "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    const/16 v16, 0x0

    .line 2898
    .local v16, "targetModeAttr":Ljava/lang/String;
    if-eqz v15, :cond_3

    .line 2899
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->getTargeMode()Ljava/lang/String;

    move-result-object v16

    .line 2902
    :cond_3
    sget-object v5, Lorg/apache/poi/openxml4j/opc/TargetMode;->INTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;

    .line 2903
    .local v5, "targetMode":Lorg/apache/poi/openxml4j/opc/TargetMode;
    if-eqz v16, :cond_4

    .line 2904
    const-string/jumbo v2, "internal"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    sget-object v5, Lorg/apache/poi/openxml4j/opc/TargetMode;->INTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;

    .line 2908
    :cond_4
    :goto_1
    new-instance v1, Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    .end local v1    # "packRel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object v2

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->getTarget()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/poi/openxml4j/opc/PackagingURIHelper;->toURI(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v4

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/relationship/CTRelationship;->getType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getRelId()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lorg/apache/poi/openxml4j/opc/PackageRelationship;-><init>(Lorg/apache/poi/openxml4j/opc/OPCPackage;Lorg/apache/poi/openxml4j/opc/PackagePart;Ljava/net/URI;Lorg/apache/poi/openxml4j/opc/TargetMode;Ljava/lang/String;Ljava/lang/String;)V

    .line 2915
    .end local v5    # "targetMode":Lorg/apache/poi/openxml4j/opc/TargetMode;
    .end local v14    # "pkgPartName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .end local v16    # "targetModeAttr":Ljava/lang/String;
    .restart local v1    # "packRel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    :cond_5
    if-eqz v3, :cond_6

    if-eqz v1, :cond_6

    .line 2916
    new-instance v13, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    invoke-direct {v13, v3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;-><init>(Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/openxml4j/opc/PackageRelationship;)V
    :try_end_1
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v12    # "picData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    .restart local v13    # "picData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    move-object v12, v13

    .line 2923
    .end local v13    # "picData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    .restart local v12    # "picData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    :cond_6
    if-eqz v9, :cond_a

    .line 2925
    :try_start_2
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object/from16 v17, v18

    .end local v1    # "packRel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .end local v3    # "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    .end local v18    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .end local v19    # "zipEntry":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    .restart local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    :cond_7
    :goto_2
    move-object v13, v12

    .line 2932
    .end local v12    # "picData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    .restart local v13    # "picData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    goto/16 :goto_0

    .line 2904
    .end local v13    # "picData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    .end local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v1    # "packRel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .restart local v3    # "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    .restart local v5    # "targetMode":Lorg/apache/poi/openxml4j/opc/TargetMode;
    .restart local v12    # "picData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    .restart local v14    # "pkgPartName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .restart local v16    # "targetModeAttr":Ljava/lang/String;
    .restart local v18    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v19    # "zipEntry":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    :cond_8
    :try_start_3
    sget-object v5, Lorg/apache/poi/openxml4j/opc/TargetMode;->EXTERNAL:Lorg/apache/poi/openxml4j/opc/TargetMode;
    :try_end_3
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/net/URISyntaxException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 2926
    .end local v5    # "targetMode":Lorg/apache/poi/openxml4j/opc/TargetMode;
    .end local v14    # "pkgPartName":Lorg/apache/poi/openxml4j/opc/PackagePartName;
    .end local v16    # "targetModeAttr":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 2927
    .local v8, "e":Ljava/io/IOException;
    const-string/jumbo v2, "XDocCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v17, v18

    .line 2928
    .end local v18    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    goto :goto_2

    .line 2918
    .end local v1    # "packRel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .end local v3    # "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v19    # "zipEntry":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    :catch_1
    move-exception v8

    .line 2919
    .local v8, "e":Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;
    :goto_3
    :try_start_4
    const-string/jumbo v2, "XDocCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2923
    if-eqz v9, :cond_7

    .line 2925
    :try_start_5
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    .line 2926
    :catch_2
    move-exception v8

    .line 2927
    .local v8, "e":Ljava/io/IOException;
    const-string/jumbo v2, "XDocCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 2920
    .end local v8    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v8

    .line 2921
    .local v8, "e":Ljava/net/URISyntaxException;
    :goto_4
    :try_start_6
    const-string/jumbo v2, "XDocCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2923
    if-eqz v9, :cond_7

    .line 2925
    :try_start_7
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_2

    .line 2926
    :catch_4
    move-exception v8

    .line 2927
    .local v8, "e":Ljava/io/IOException;
    const-string/jumbo v2, "XDocCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2923
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    :goto_5
    if-eqz v9, :cond_9

    .line 2925
    :try_start_8
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 2928
    :cond_9
    :goto_6
    throw v2

    .line 2926
    :catch_5
    move-exception v8

    .line 2927
    .restart local v8    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "XDocCustomViewConsumer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 2923
    .end local v8    # "e":Ljava/io/IOException;
    .end local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v18    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    :catchall_1
    move-exception v2

    move-object/from16 v17, v18

    .end local v18    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    goto :goto_5

    .line 2920
    .end local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v18    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    :catch_6
    move-exception v8

    move-object/from16 v17, v18

    .end local v18    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    goto :goto_4

    .line 2918
    .end local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v18    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    :catch_7
    move-exception v8

    move-object/from16 v17, v18

    .end local v18    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    goto/16 :goto_3

    .end local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v1    # "packRel":Lorg/apache/poi/openxml4j/opc/PackageRelationship;
    .restart local v3    # "zipPackPart":Lorg/apache/poi/openxml4j/opc/ZipPackagePart;
    .restart local v18    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v19    # "zipEntry":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource$FakeZipEntry;
    :cond_a
    move-object/from16 v17, v18

    .end local v18    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    .restart local v17    # "zipArchive":Lorg/apache/poi/openxml4j/util/ZipInputStreamZipEntrySource;
    goto/16 :goto_2
.end method

.method private init()V
    .locals 6

    .prologue
    const/16 v5, 0x24

    const/4 v4, 0x0

    .line 236
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->folderName:Ljava/lang/String;

    .line 237
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->folderName:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->folderPath:Ljava/io/File;

    .line 238
    new-instance v1, Ljava/lang/StringBuilder;

    const/high16 v2, 0x100000

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->text:Ljava/lang/StringBuilder;

    .line 239
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->bodyElements:Ljava/util/ArrayList;

    .line 240
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->sectPrLst:Ljava/util/ArrayList;

    .line 241
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->hdrFdrLst:Ljava/util/ArrayList;

    .line 242
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuLvlState:Ljava/util/HashMap;

    .line 243
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mBuValues:Ljava/util/HashMap;

    .line 244
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;-><init>()V

    .line 245
    .local v0, "slideDimention":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    new-instance v1, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v1, v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;-><init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 246
    new-instance v1, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    .line 247
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    .line 248
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    .line 249
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTable:Ljava/util/Stack;

    .line 250
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableRow:Ljava/util/Stack;

    .line 251
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableCell:Ljava/util/Stack;

    .line 253
    iput v4, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mHeaderParaNum:I

    .line 254
    iput v4, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFooterParaNum:I

    .line 255
    iput v4, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mNormalParaNum:I

    .line 257
    const/16 v1, 0x2d0

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->defTabStopPos:I

    .line 265
    iput v5, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mHeaderHeight:I

    .line 266
    iput v5, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFooterHeight:I

    .line 268
    return-void
.end method

.method private processParagraph(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V
    .locals 13
    .param p1, "paragraph"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    .prologue
    const/4 v12, -0x1

    const/4 v11, 0x1

    .line 639
    if-nez p1, :cond_1

    .line 914
    :cond_0
    :goto_0
    return-void

    .line 644
    :cond_1
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isParaCreated:Z

    .line 645
    const/4 v5, 0x0

    .line 646
    .local v5, "isPgBr":Z
    new-instance v6, Lcom/samsung/thumbnail/customview/word/Paragraph;

    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v6, v7}, Lcom/samsung/thumbnail/customview/word/Paragraph;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 648
    .local v6, "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    sget-object v7, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 651
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    if-eqz v7, :cond_8

    .line 652
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getPgBrBefStatus()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 653
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v7, v11}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setPgBrBefStatus(Z)V

    .line 655
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v2

    .line 657
    .local v2, "bdrProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;
    if-eqz v2, :cond_8

    .line 658
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v4

    .line 659
    .local v4, "bdrs":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;>;"
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 660
    .local v0, "bdrKeySet":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 661
    .local v1, "bdrKeySetIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;>;"
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 662
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    .line 663
    .local v3, "bdrType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    sget-object v7, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XDocBorder$EBorderType:[I

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    goto :goto_1

    .line 665
    :pswitch_0
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v7, :cond_4

    .line 666
    iget-object v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    invoke-virtual {v8, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    goto :goto_1

    .line 667
    :cond_4
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v7, :cond_3

    .line 668
    iget-object v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    invoke-virtual {v8, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    goto :goto_1

    .line 672
    :pswitch_1
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v7, :cond_5

    .line 673
    iget-object v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    invoke-virtual {v8, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    goto :goto_1

    .line 674
    :cond_5
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v7, :cond_3

    .line 675
    iget-object v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    invoke-virtual {v8, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    goto :goto_1

    .line 679
    :pswitch_2
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v7, :cond_6

    .line 680
    iget-object v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    invoke-virtual {v8, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    goto :goto_1

    .line 681
    :cond_6
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v7, :cond_3

    .line 682
    iget-object v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    invoke-virtual {v8, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    goto :goto_1

    .line 686
    :pswitch_3
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v7, :cond_7

    .line 687
    iget-object v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    invoke-virtual {v8, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    goto/16 :goto_1

    .line 688
    :cond_7
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v7, :cond_3

    .line 689
    iget-object v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    invoke-virtual {v8, v7}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    goto/16 :goto_1

    .line 700
    .end local v0    # "bdrKeySet":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;>;"
    .end local v1    # "bdrKeySetIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;>;"
    .end local v2    # "bdrProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;
    .end local v3    # "bdrType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .end local v4    # "bdrs":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;>;"
    :cond_8
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    if-eqz v7, :cond_b

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v7

    if-eqz v7, :cond_b

    .line 703
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getFirstInd()I

    move-result v7

    if-eqz v7, :cond_9

    .line 704
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    iget-object v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v8

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getFirstInd()I

    move-result v9

    int-to-float v9, v9

    invoke-static {v9}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(F)F

    move-result v9

    float-to-int v9, v9

    invoke-virtual {v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setFirstLineMargin(F)V

    .line 711
    :cond_9
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getHangingInd()I

    move-result v7

    if-eqz v7, :cond_a

    .line 712
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    iget-object v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v9

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getHangingInd()I

    move-result v10

    int-to-float v10, v10

    invoke-static {v10}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(F)F

    move-result v10

    float-to-int v10, v10

    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v9

    neg-int v9, v9

    invoke-virtual {v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setFirstLineMargin(F)V

    .line 726
    :cond_a
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    iget-object v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v8

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getLeftInd()I

    move-result v9

    int-to-float v9, v9

    invoke-static {v9}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(F)F

    move-result v9

    float-to-int v9, v9

    invoke-virtual {v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftMargin(F)V

    .line 731
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    iget-object v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v8

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getRightInd()I

    move-result v9

    int-to-float v9, v9

    invoke-static {v9}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(F)F

    move-result v9

    float-to-int v9, v9

    invoke-virtual {v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightMargin(F)V

    .line 738
    :cond_b
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->isHeader()Z

    move-result v7

    if-eqz v7, :cond_c

    .line 739
    invoke-virtual {v6, v11}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setIsHeader(Z)V

    .line 740
    iget v7, v6, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginTop:I

    div-int/lit8 v7, v7, 0x2

    iput v7, v6, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginTop:I

    .line 743
    :cond_c
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->isFooter()Z

    move-result v7

    if-eqz v7, :cond_d

    .line 744
    invoke-virtual {v6, v11}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setIsFooter(Z)V

    .line 747
    :cond_d
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-eqz v7, :cond_e

    .line 748
    iget v7, v6, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginLeft:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    const/high16 v9, 0x41200000    # 10.0f

    add-float/2addr v8, v9

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v6, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginLeft:I

    .line 750
    iget v7, v6, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginTop:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v6, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginTop:I

    .line 753
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v8

    iget v7, v6, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginLeft:I

    int-to-double v10, v7

    sub-double/2addr v8, v10

    iget v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeWidth:F

    float-to-double v10, v7

    sub-double/2addr v8, v10

    double-to-int v7, v8

    iput v7, v6, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginRight:I

    .line 755
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v8

    iget v7, v6, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginTop:I

    int-to-double v10, v7

    sub-double/2addr v8, v10

    iget v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeHeight:F

    float-to-double v10, v7

    sub-double/2addr v8, v10

    double-to-int v7, v8

    iput v7, v6, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginBottom:I

    .line 757
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCurrentShapeId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setShapeId(Ljava/lang/String;)V

    .line 760
    :cond_e
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    if-eqz v7, :cond_f

    .line 761
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setHorizontalAlignment(Ljava/lang/String;)V

    .line 764
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_13

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "right"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 767
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v7, :cond_12

    .line 768
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 797
    :cond_f
    :goto_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    if-eqz v7, :cond_1f

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v7

    if-eqz v7, :cond_1f

    .line 800
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getAfter()I

    move-result v7

    if-eq v7, v12, :cond_1a

    .line 801
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getAfter()I

    move-result v7

    div-int/lit8 v7, v7, 0x14

    iput v7, v6, Lcom/samsung/thumbnail/customview/word/Paragraph;->spacingBottom:I

    .line 803
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v7, :cond_19

    .line 805
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getAfter()I

    move-result v8

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBottomSpacing(F)V

    .line 835
    :cond_10
    :goto_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getBefore()I

    move-result v7

    if-eq v7, v12, :cond_1d

    .line 836
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getBefore()I

    move-result v7

    invoke-static {v7}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v7

    float-to-int v7, v7

    iput v7, v6, Lcom/samsung/thumbnail/customview/word/Paragraph;->spacingTop:I

    .line 839
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v7, :cond_1c

    .line 840
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getBefore()I

    move-result v8

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTopSpacing(F)V

    .line 909
    :cond_11
    :goto_4
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    invoke-direct {p0, v6, p1, v7}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->processRun(Lcom/samsung/thumbnail/customview/word/Paragraph;Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;)Z

    move-result v5

    .line 911
    if-eqz v5, :cond_0

    goto/16 :goto_0

    .line 769
    :cond_12
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v7, :cond_f

    .line 770
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_2

    .line 772
    :cond_13
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_15

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "center"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 775
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v7, :cond_14

    .line 776
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_2

    .line 777
    :cond_14
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v7, :cond_f

    .line 778
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_2

    .line 780
    :cond_15
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_17

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "justify"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_17

    .line 783
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v7, :cond_16

    .line 784
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->JUSTIFY:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_2

    .line 785
    :cond_16
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v7, :cond_f

    .line 786
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->JUSTIFY:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_2

    .line 789
    :cond_17
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v7, :cond_18

    .line 790
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_2

    .line 791
    :cond_18
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v7, :cond_f

    .line 792
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    sget-object v8, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto/16 :goto_2

    .line 808
    :cond_19
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v7, :cond_10

    .line 810
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getAfter()I

    move-result v8

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBottomSpacing(F)V

    goto/16 :goto_3

    .line 814
    :cond_1a
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v7

    if-eqz v7, :cond_10

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    if-eqz v7, :cond_10

    .line 817
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v7

    if-eqz v7, :cond_10

    .line 819
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v7, :cond_1b

    .line 820
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getAfter()I

    move-result v8

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBottomSpacing(F)V

    goto/16 :goto_3

    .line 825
    :cond_1b
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v7, :cond_10

    .line 826
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getAfter()I

    move-result v8

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBottomSpacing(F)V

    goto/16 :goto_3

    .line 843
    :cond_1c
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v7, :cond_11

    .line 844
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getBefore()I

    move-result v8

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTopSpacing(F)V

    goto/16 :goto_4

    .line 848
    :cond_1d
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v7

    if-eqz v7, :cond_11

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    if-eqz v7, :cond_11

    .line 851
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v7

    if-eqz v7, :cond_11

    .line 853
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v7, :cond_1e

    .line 854
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getBefore()I

    move-result v8

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTopSpacing(F)V

    goto/16 :goto_4

    .line 858
    :cond_1e
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v7, :cond_11

    .line 859
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getBefore()I

    move-result v8

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTopSpacing(F)V

    goto/16 :goto_4

    .line 868
    :cond_1f
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v7

    if-eqz v7, :cond_11

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v7

    if-eqz v7, :cond_11

    .line 870
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getLineSpacing()F

    move-result v8

    float-to-int v8, v8

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    .line 874
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v7

    if-eqz v7, :cond_20

    .line 876
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v7, :cond_21

    .line 877
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getAfter()I

    move-result v8

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBottomSpacing(F)V

    .line 891
    :cond_20
    :goto_5
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v7

    if-eqz v7, :cond_11

    .line 893
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v7, :cond_22

    .line 894
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getBefore()I

    move-result v8

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTopSpacing(F)V

    goto/16 :goto_4

    .line 882
    :cond_21
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v7, :cond_20

    .line 883
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getAfter()I

    move-result v8

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBottomSpacing(F)V

    goto :goto_5

    .line 898
    :cond_22
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v7, :cond_11

    .line 899
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getBefore()I

    move-result v8

    invoke-static {v8}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTopSpacing(F)V

    goto/16 :goto_4

    .line 663
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private processRun(Lcom/samsung/thumbnail/customview/word/Paragraph;Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;)Z
    .locals 40
    .param p1, "canvasPara"    # Lcom/samsung/thumbnail/customview/word/Paragraph;
    .param p2, "para"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .param p3, "elementType"    # Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    .prologue
    .line 919
    const/16 v17, 0x0

    .line 920
    .local v17, "hasPgBr":Z
    const/16 v16, 0x0

    .line 921
    .local v16, "hasInLineComp":Z
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->paraNumber:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->paraNumber:I

    .line 923
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_3

    .line 924
    invoke-direct/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->addEmptyRun()V

    .line 926
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v4, :cond_5

    .line 927
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 931
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v4, :cond_6

    .line 932
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 936
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v4, :cond_7

    .line 937
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 941
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v4, :cond_8

    .line 942
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 947
    :cond_3
    :goto_3
    const/16 v28, 0x0

    .line 948
    .local v28, "shadeProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getShadeColor()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 950
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getShadeColor()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    move-result-object v28

    .line 952
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v4, :cond_9

    .line 953
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getFill()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBackgroundColor(Ljava/lang/String;)V

    .line 958
    :cond_4
    :goto_4
    const/16 v21, 0x0

    .local v21, "j":I
    :goto_5
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v21

    if-ge v0, v4, :cond_30

    .line 959
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    .line 960
    .local v26, "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    new-instance v5, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct {v5}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 964
    .local v5, "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isLastRenderedPgBr()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 965
    const/16 v17, 0x1

    .line 968
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->breakPage:Z

    move/from16 v18, v17

    .line 1269
    .end local v5    # "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v17    # "hasPgBr":Z
    .end local v26    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .local v18, "hasPgBr":I
    :goto_6
    return v18

    .line 928
    .end local v18    # "hasPgBr":I
    .end local v21    # "j":I
    .end local v28    # "shadeProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
    .restart local v17    # "hasPgBr":Z
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v4, :cond_0

    .line 929
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    goto/16 :goto_0

    .line 933
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v4, :cond_1

    .line 934
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    goto/16 :goto_1

    .line 938
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v4, :cond_2

    .line 939
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    goto/16 :goto_2

    .line 943
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v4, :cond_3

    .line 944
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    goto/16 :goto_3

    .line 954
    .restart local v28    # "shadeProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v4, :cond_4

    .line 955
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getFill()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBackgroundColor(Ljava/lang/String;)V

    goto :goto_4

    .line 972
    .restart local v5    # "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    .restart local v21    # "j":I
    .restart local v26    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    :cond_a
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->hasTab()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 973
    const/4 v4, 0x1

    invoke-virtual {v5, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setTabStatus(Z)V

    .line 976
    :cond_b
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v4

    if-eqz v4, :cond_c

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getHighlightColor()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 978
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getHighlightColor()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setHighLightedColor(Ljava/lang/String;)V

    .line 983
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v4

    const-wide/high16 v36, 0x4026000000000000L    # 11.0

    move-wide/from16 v0, v36

    invoke-virtual {v4, v0, v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-float v4, v0

    invoke-virtual {v5, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 986
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isParaCreated:Z

    if-nez v4, :cond_d

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isHyperlink:Z

    if-nez v4, :cond_d

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isShape()Z

    move-result v4

    if-nez v4, :cond_d

    .line 987
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isParaCreated:Z

    .line 990
    :cond_d
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1a

    .line 992
    const/16 v16, 0x1

    .line 994
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getFldSmplInstr()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_f

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getFldSmplInstr()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "PAGE"

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 996
    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->setFldSmplInstr(Ljava/lang/String;)V

    .line 1094
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v4, :cond_19

    .line 1095
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 1256
    :cond_e
    :goto_8
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isPageBreak()Z

    move-result v4

    if-eqz v4, :cond_2f

    .line 1257
    const/16 v17, 0x1

    .line 1260
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->breakPage:Z

    move/from16 v18, v17

    .line 1261
    .restart local v18    # "hasPgBr":I
    goto/16 :goto_6

    .line 999
    .end local v18    # "hasPgBr":I
    :cond_f
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v7

    .line 1000
    .local v7, "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    if-eqz v7, :cond_12

    .line 1001
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v5}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->applyParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 1003
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v10

    .line 1005
    .local v10, "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    if-eqz v10, :cond_11

    .line 1006
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v5}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->applyCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 1007
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStyleId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_10

    .line 1008
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v4

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStyleId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v33

    .line 1011
    .local v33, "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    if-eqz v33, :cond_10

    .line 1015
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v4

    if-eqz v4, :cond_10

    .line 1016
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->applyCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 1058
    .end local v10    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .end local v33    # "style":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_10
    :goto_9
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v4

    if-eqz v4, :cond_18

    .line 1059
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getStyleId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v8

    .line 1062
    .local v8, "currStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    if-eqz v8, :cond_16

    .line 1063
    if-eqz v7, :cond_14

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 1064
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->filterText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->createBullet(Lcom/samsung/thumbnail/customview/word/Run;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;Z)V

    goto/16 :goto_7

    .line 1022
    .end local v8    # "currStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .restart local v10    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v13

    .line 1024
    .local v13, "defStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    if-eqz v13, :cond_10

    .line 1025
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v4

    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getStyleId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v8

    .line 1028
    .restart local v8    # "currStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v4

    if-eqz v4, :cond_10

    .line 1029
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->applyCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/customview/word/Run;)V

    goto :goto_9

    .line 1035
    .end local v8    # "currStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .end local v10    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .end local v13    # "defStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getDefParaStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v13

    .line 1037
    .restart local v13    # "defStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    if-eqz v13, :cond_10

    .line 1039
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v4

    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getStyleId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v8

    .line 1041
    .restart local v8    # "currStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v23

    .line 1043
    .local v23, "paraProp1":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    if-eqz v23, :cond_10

    .line 1044
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v5}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->applyParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 1045
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v4

    if-eqz v4, :cond_13

    .line 1046
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->applyCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/customview/word/Run;)V

    goto/16 :goto_9

    .line 1047
    :cond_13
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v4

    if-eqz v4, :cond_10

    .line 1048
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->applyCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/customview/word/Run;)V

    goto/16 :goto_9

    .line 1067
    .end local v13    # "defStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .end local v23    # "paraProp1":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    :cond_14
    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v4

    if-eqz v4, :cond_15

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getParaProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 1069
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->filterText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x1

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->createBullet(Lcom/samsung/thumbnail/customview/word/Run;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;Z)V

    goto/16 :goto_7

    .line 1073
    :cond_15
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->filterText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/thumbnail/util/Utils;->getDirectionalText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1078
    :cond_16
    if-eqz v7, :cond_17

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->isBullet()Z

    move-result v4

    if-eqz v4, :cond_17

    .line 1079
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->filterText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->createBullet(Lcom/samsung/thumbnail/customview/word/Run;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;Z)V

    goto/16 :goto_7

    .line 1083
    :cond_17
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->filterText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/thumbnail/util/Utils;->getDirectionalText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1089
    .end local v8    # "currStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_18
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->filterText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/thumbnail/util/Utils;->getDirectionalText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1096
    .end local v7    # "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    :cond_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v4, :cond_e

    .line 1097
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_8

    .line 1098
    :cond_1a
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isPict()Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 1100
    const/16 v22, 0x0

    .local v22, "k":I
    :goto_a
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getPictures()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v22

    if-ge v0, v4, :cond_e

    .line 1101
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getPictures()Ljava/util/ArrayList;

    move-result-object v4

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->isInLine()Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 1102
    const/16 v16, 0x1

    .line 1103
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getPictures()Ljava/util/ArrayList;

    move-result-object v4

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->paraNumber:I

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v4, v1, v6, v9}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->writePicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;IZ)V

    .line 1100
    :cond_1b
    add-int/lit8 v22, v22, 0x1

    goto :goto_a

    .line 1107
    .end local v22    # "k":I
    :cond_1c
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isShape()Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 1109
    const/16 v22, 0x0

    .restart local v22    # "k":I
    :goto_b
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getShapes()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v22

    if-ge v0, v4, :cond_e

    .line 1110
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getShapes()Ljava/util/ArrayList;

    move-result-object v4

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .line 1111
    .local v27, "runShape":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    if-eqz v27, :cond_1e

    .line 1112
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStyle()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1, v4, v6}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->applyStyleParameters(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;Ljava/lang/String;Z)V

    .line 1114
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    move-wide/from16 v36, v0

    const-wide/16 v38, 0x0

    cmp-long v4, v36, v38

    if-nez v4, :cond_1d

    .line 1116
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getGrpZorder()J

    move-result-wide v36

    const-wide/16 v38, 0x0

    cmp-long v4, v36, v38

    if-nez v4, :cond_1d

    .line 1117
    const/16 v16, 0x1

    .line 1118
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->drawShape(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    .line 1119
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->drawShapePicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    .line 1122
    :cond_1d
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeCount:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeCount:I

    .line 1109
    :cond_1e
    add-int/lit8 v22, v22, 0x1

    goto :goto_b

    .line 1125
    .end local v22    # "k":I
    .end local v27    # "runShape":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    :cond_1f
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isChart()Z

    move-result v4

    if-eqz v4, :cond_21

    .line 1126
    const/16 v16, 0x1

    .line 1127
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getChartRelId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getChartById(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;

    move-result-object v11

    .line 1128
    .local v11, "chart":Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->parseChart()V

    .line 1129
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getPictures()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    .line 1130
    .local v24, "pic":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v4

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getWidth()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v11, v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->setWidth(F)V

    .line 1132
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v4

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getHeight()F

    move-result v6

    float-to-int v6, v6

    int-to-double v0, v6

    move-wide/from16 v36, v0

    move-wide/from16 v0, v36

    invoke-virtual {v4, v0, v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v11, v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;->setHeight(F)V

    .line 1134
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->chartBuilder:Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;

    if-nez v4, :cond_20

    .line 1135
    new-instance v4, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->chartBuilder:Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;

    .line 1137
    :cond_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->chartBuilder:Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v11, v6}, Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChartBuilder;->drawChartForCanvas(Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    goto/16 :goto_8

    .line 1138
    .end local v11    # "chart":Lcom/samsung/thumbnail/office/ooxml/word/chart/XWPFChart;
    .end local v24    # "pic":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;
    :cond_21
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->isDiagram()Z

    move-result v4

    if-eqz v4, :cond_2d

    .line 1140
    const/16 v16, 0x1

    .line 1141
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->bIsDiagram:Z

    .line 1143
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getDataModelId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getDiagramDataModelId(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;

    move-result-object v12

    .line 1145
    .local v12, "dataModel":Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;->parseDataModel()V

    .line 1147
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;->getDiagramRelId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getDiagramRelId(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    move-result-object v14

    .line 1149
    .local v14, "diagram":Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getTheme()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v4

    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v14, v12, v4, v6}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->parseDiagram(Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 1152
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getPictures()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    .line 1153
    .restart local v24    # "pic":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v4

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getWidth()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v4, v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v34

    .line 1155
    .local v34, "width":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v4

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getHeight()F

    move-result v6

    float-to-int v6, v6

    int-to-double v0, v6

    move-wide/from16 v36, v0

    move-wide/from16 v0, v36

    invoke-virtual {v4, v0, v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v19

    .line 1158
    .local v19, "height":I
    new-instance v15, Lcom/samsung/thumbnail/customview/word/DiagramController;

    move/from16 v0, v34

    move/from16 v1, v19

    invoke-direct {v15, v0, v1}, Lcom/samsung/thumbnail/customview/word/DiagramController;-><init>(II)V

    .line 1160
    .local v15, "diagramController":Lcom/samsung/thumbnail/customview/word/DiagramController;
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->isInLine()Z

    move-result v4

    if-eqz v4, :cond_22

    .line 1161
    const/4 v4, 0x1

    invoke-virtual {v15, v4}, Lcom/samsung/thumbnail/customview/word/DiagramController;->setIsInLineStatus(Z)V

    .line 1166
    :cond_22
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    .line 1168
    const/4 v4, 0x1

    invoke-virtual {v15, v4}, Lcom/samsung/thumbnail/customview/word/DiagramController;->setBehindDoc(I)V

    .line 1174
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_c
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->getDiagramShapeAL()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v20

    if-ge v0, v4, :cond_2c

    .line 1175
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->getDiagramShapeAL()Ljava/util/ArrayList;

    move-result-object v4

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .line 1176
    .local v29, "shape":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v31

    .line 1177
    .local v31, "shapeProp":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-float v4, v0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getWordHeight()J

    move-result-wide v36

    move-wide/from16 v0, v36

    long-to-float v6, v0

    div-float/2addr v4, v6

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setSDRatio(F)V

    .line 1183
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->updateShapeParamentersDgm(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    .line 1188
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getRefId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_29

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getAdjVal()I

    move-result v4

    if-eqz v4, :cond_29

    .line 1190
    const-wide/16 v36, -0x1

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    .line 1197
    :goto_d
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getRelativeHeight()J

    move-result-wide v36

    const-wide/16 v38, 0x0

    cmp-long v4, v36, v38

    if-eqz v4, :cond_23

    .line 1198
    const/4 v4, 0x0

    invoke-virtual {v15, v4}, Lcom/samsung/thumbnail/customview/word/DiagramController;->setBehindDoc(I)V

    .line 1199
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getBehindDocState()Z

    move-result v4

    if-eqz v4, :cond_2a

    .line 1200
    const-wide/16 v36, -0x1

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    .line 1207
    :cond_23
    :goto_e
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->drawShape(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    .line 1212
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->getDiagramShapeAL()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, v20

    if-ne v0, v4, :cond_24

    .line 1213
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v15}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 1216
    :cond_24
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getRefId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_28

    .line 1217
    new-instance v25, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeWidth()J

    move-result-wide v36

    move-wide/from16 v0, v36

    long-to-float v4, v0

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeHeight()J

    move-result-wide v36

    move-wide/from16 v0, v36

    long-to-float v6, v0

    move-object/from16 v0, v25

    invoke-direct {v0, v4, v6}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;-><init>(FF)V

    .line 1220
    .local v25, "pict":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;
    const/4 v4, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setVMLType(Z)V

    .line 1221
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getRefId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setImageRelId(Ljava/lang/String;)V

    .line 1223
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    move-wide/from16 v36, v0

    const-wide/16 v38, -0x1

    cmp-long v4, v36, v38

    if-eqz v4, :cond_25

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getBehindDocState()Z

    move-result v4

    if-eqz v4, :cond_2b

    .line 1224
    :cond_25
    const/4 v4, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setBehindDoc(Z)V

    .line 1229
    :goto_f
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-result-object v32

    .line 1230
    .local v32, "shapeType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    move-object/from16 v0, v32

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->getType(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;)Ljava/lang/String;

    move-result-object v30

    .line 1231
    .local v30, "shapeName":Ljava/lang/String;
    const-string/jumbo v4, "ellipse"

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_26

    const-string/jumbo v4, "oval"

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_26

    const-string/jumbo v4, "circle"

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_27

    .line 1234
    :cond_26
    const-string/jumbo v4, "ellipse"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setEnclosedSpName(Ljava/lang/String;)V

    .line 1237
    :cond_27
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->PARAGRAPH:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->paraNumber:I

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v4, v6, v9}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->writePicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;IZ)V

    .line 1174
    .end local v25    # "pict":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;
    .end local v30    # "shapeName":Ljava/lang/String;
    .end local v32    # "shapeType":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;
    :cond_28
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_c

    .line 1192
    :cond_29
    const-wide/16 v36, 0x1

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    goto/16 :goto_d

    .line 1202
    :cond_2a
    const-wide/16 v36, 0x1

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    goto/16 :goto_e

    .line 1226
    .restart local v25    # "pict":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;
    :cond_2b
    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setBehindDoc(Z)V

    goto :goto_f

    .line 1242
    .end local v25    # "pict":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;
    .end local v29    # "shape":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .end local v31    # "shapeProp":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;
    :cond_2c
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->bIsDiagram:Z

    .line 1243
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    goto/16 :goto_8

    .line 1244
    .end local v12    # "dataModel":Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDataModel;
    .end local v14    # "diagram":Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;
    .end local v15    # "diagramController":Lcom/samsung/thumbnail/customview/word/DiagramController;
    .end local v19    # "height":I
    .end local v20    # "i":I
    .end local v24    # "pic":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;
    .end local v34    # "width":I
    :cond_2d
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->hasTab()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1246
    const/16 v16, 0x1

    .line 1247
    const-string/jumbo v4, ""

    invoke-virtual {v5, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 1248
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v4, :cond_2e

    .line 1249
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_8

    .line 1250
    :cond_2e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v4, :cond_e

    .line 1251
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_8

    .line 958
    :cond_2f
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_5

    .line 1265
    .end local v5    # "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v26    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    :cond_30
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_31

    if-nez v16, :cond_31

    .line 1266
    invoke-direct/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->addEmptyRun()V

    :cond_31
    move/from16 v18, v17

    .line 1269
    .restart local v18    # "hasPgBr":I
    goto/16 :goto_6
.end method

.method private resetRunProp()V
    .locals 2

    .prologue
    .line 2436
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->text:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2437
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 2438
    return-void
.end method

.method private startBody()V
    .locals 0

    .prologue
    .line 2077
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->calcPageSize()V

    .line 2078
    return-void
.end method

.method private updateBitmapParam(Landroid/graphics/Bitmap;Lcom/samsung/thumbnail/customview/hslf/SlideDimention;FFLcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;)Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .locals 8
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "sd"    # Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .param p3, "width"    # F
    .param p4, "height"    # F
    .param p5, "picture"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    .prologue
    const/4 v6, 0x0

    .line 3246
    const/4 v0, 0x0

    .line 3248
    .local v0, "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    invoke-virtual {p5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getHorizontalOffsetPos()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    .line 3250
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    invoke-virtual {p5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getVerticalOffsetPos()F

    move-result v2

    float-to-int v2, v2

    int-to-double v2, v2

    invoke-virtual {p2, v2, v3}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    .line 3253
    float-to-int v1, p3

    invoke-virtual {p2, v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v4

    .line 3254
    .local v4, "newWidth":I
    float-to-int v1, p4

    int-to-double v2, v1

    invoke-virtual {p2, v2, v3}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v5

    .line 3255
    .local v5, "newHeight":I
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/word/Page;->getLeftMargin()F

    move-result v1

    float-to-int v7, v1

    .line 3258
    .local v7, "pageLeftMargin":I
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    cmpg-float v1, v1, v6

    if-gez v1, :cond_1

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    cmpg-float v1, v1, v6

    if-gez v1, :cond_1

    .line 3259
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .end local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    float-to-int v1, v1

    mul-int/lit8 v1, v1, -0x1

    sub-int v2, v7, v1

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    float-to-int v3, v1

    iget v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->paraNumber:I

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;-><init>(Landroid/graphics/Bitmap;IIIII)V

    .line 3287
    .restart local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    :cond_0
    :goto_0
    return-object v0

    .line 3262
    :cond_1
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    cmpg-float v1, v1, v6

    if-gez v1, :cond_2

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    cmpl-float v1, v1, v6

    if-ltz v1, :cond_2

    .line 3263
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .end local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    float-to-int v1, v1

    mul-int/lit8 v1, v1, -0x1

    sub-int v2, v7, v1

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    float-to-int v3, v1

    iget v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->paraNumber:I

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;-><init>(Landroid/graphics/Bitmap;IIIII)V

    .restart local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    goto :goto_0

    .line 3266
    :cond_2
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    cmpl-float v1, v1, v6

    if-lez v1, :cond_3

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    cmpg-float v1, v1, v6

    if-gez v1, :cond_3

    .line 3267
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .end local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    float-to-int v1, v1

    add-int v2, v7, v1

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    float-to-int v3, v1

    iget v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->paraNumber:I

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;-><init>(Landroid/graphics/Bitmap;IIIII)V

    .restart local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    goto :goto_0

    .line 3270
    :cond_3
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    cmpl-float v1, v1, v6

    if-lez v1, :cond_4

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    cmpl-float v1, v1, v6

    if-lez v1, :cond_4

    .line 3271
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .end local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    float-to-int v1, v1

    add-int v2, v7, v1

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    float-to-int v3, v1

    iget v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->paraNumber:I

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;-><init>(Landroid/graphics/Bitmap;IIIII)V

    .restart local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    goto :goto_0

    .line 3274
    :cond_4
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    cmpl-float v1, v1, v6

    if-ltz v1, :cond_5

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    cmpl-float v1, v1, v6

    if-lez v1, :cond_5

    .line 3275
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .end local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    float-to-int v1, v1

    add-int v2, v7, v1

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    float-to-int v3, v1

    iget v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->paraNumber:I

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;-><init>(Landroid/graphics/Bitmap;IIIII)V

    .restart local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    goto :goto_0

    .line 3278
    :cond_5
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    cmpl-float v1, v1, v6

    if-lez v1, :cond_6

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    cmpl-float v1, v1, v6

    if-ltz v1, :cond_6

    .line 3279
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .end local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    float-to-int v1, v1

    add-int v2, v7, v1

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    float-to-int v3, v1

    iget v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->paraNumber:I

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;-><init>(Landroid/graphics/Bitmap;IIIII)V

    .restart local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    goto/16 :goto_0

    .line 3282
    :cond_6
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    cmpl-float v1, v1, v6

    if-nez v1, :cond_0

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    cmpl-float v1, v1, v6

    if-nez v1, :cond_0

    .line 3283
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .end local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    float-to-int v2, v1

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    float-to-int v3, v1

    iget v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->paraNumber:I

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;-><init>(Landroid/graphics/Bitmap;IIIII)V

    .restart local v0    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    goto/16 :goto_0
.end method

.method private updateShapeParamentersDgm(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V
    .locals 5
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .prologue
    const/4 v4, 0x0

    .line 1588
    if-nez p1, :cond_1

    .line 1614
    :cond_0
    :goto_0
    return-void

    .line 1591
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    .line 1593
    .local v0, "shapeProp":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;
    if-eqz v0, :cond_0

    .line 1597
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeXvalue()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    .line 1599
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeYvalue()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    .line 1601
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeWidth()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeWidth:F

    .line 1603
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeHeight()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeHeight:F

    .line 1607
    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getRotation()I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeRotationAngle:J

    .line 1609
    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->ABSOLUTE:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeHorAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 1610
    iput-object v4, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    .line 1611
    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->ABSOLUTE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 1612
    iput-object v4, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    .line 1613
    iput-object v4, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeFlipValue:Ljava/lang/String;

    goto :goto_0
.end method

.method private writePicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;FF)Z
    .locals 21
    .param p1, "picture"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    .param p2, "pict"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 2780
    const/4 v14, 0x0

    .line 2781
    .local v14, "fos":Ljava/io/FileOutputStream;
    const/16 v19, 0x0

    .line 2783
    .local v19, "wmfFos":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->getFileName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_12
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_b
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v12

    .line 2784
    .local v12, "fileName":Ljava/lang/String;
    if-nez v12, :cond_2

    .line 2785
    const/4 v4, 0x0

    .line 2834
    if-eqz v14, :cond_0

    .line 2836
    :try_start_1
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2840
    :cond_0
    :goto_0
    if-eqz v19, :cond_1

    .line 2842
    :try_start_2
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2847
    .end local v12    # "fileName":Ljava/lang/String;
    :cond_1
    :goto_1
    return v4

    .line 2837
    .restart local v12    # "fileName":Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 2838
    .local v11, "e":Ljava/io/IOException;
    const-string/jumbo v5, "XDocCustomViewConsumer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2843
    .end local v11    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v11

    .line 2844
    .restart local v11    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "XDocCustomViewConsumer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2786
    .end local v11    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->getPictureType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 2825
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->folderName:Ljava/lang/String;

    invoke-static {v4, v5, v12, v6}, Lorg/apache/poi/util/IOUtils;->writeToFile(Ljava/io/InputStream;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_12
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_b
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2827
    const/4 v4, 0x0

    .line 2834
    if-eqz v14, :cond_3

    .line 2836
    :try_start_4
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_8

    .line 2840
    :cond_3
    :goto_2
    if-eqz v19, :cond_1

    .line 2842
    :try_start_5
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 2843
    :catch_2
    move-exception v11

    .line 2844
    .restart local v11    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "XDocCustomViewConsumer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2788
    .end local v11    # "e":Ljava/io/IOException;
    :pswitch_0
    :try_start_6
    new-instance v15, Ljava/io/FileOutputStream;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->folderPath:Ljava/io/File;

    invoke-direct {v4, v5, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v15, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_12
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_b
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2790
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .local v15, "fos":Ljava/io/FileOutputStream;
    :try_start_7
    new-instance v17, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->getData()[B

    move-result-object v4

    move-object/from16 v0, v17

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-direct {v0, v4, v1, v2}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;-><init>([BFF)V

    .line 2792
    .local v17, "readEMFFile":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawImage()Landroid/graphics/Bitmap;

    move-result-object v10

    .line 2793
    .local v10, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v10, :cond_4

    .line 2794
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x19

    invoke-virtual {v10, v4, v5, v15}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_13
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_10
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2796
    :cond_4
    const/4 v4, 0x1

    .line 2834
    if-eqz v15, :cond_5

    .line 2836
    :try_start_8
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 2840
    :cond_5
    :goto_3
    if-eqz v19, :cond_6

    .line 2842
    :try_start_9
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    :cond_6
    :goto_4
    move-object v14, v15

    .line 2845
    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_1

    .line 2837
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v11

    .line 2838
    .restart local v11    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "XDocCustomViewConsumer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 2843
    .end local v11    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v11

    .line 2844
    .restart local v11    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "XDocCustomViewConsumer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 2798
    .end local v10    # "bitmap":Landroid/graphics/Bitmap;
    .end local v11    # "e":Ljava/io/IOException;
    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .end local v17    # "readEMFFile":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    :pswitch_1
    :try_start_a
    new-instance v20, Ljava/io/FileOutputStream;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->folderPath:Ljava/io/File;

    invoke-direct {v4, v5, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_12
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_b
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 2800
    .end local v19    # "wmfFos":Ljava/io/FileOutputStream;
    .local v20, "wmfFos":Ljava/io/FileOutputStream;
    :try_start_b
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->getData()[B

    move-result-object v4

    if-eqz v4, :cond_7

    .line 2801
    new-instance v18, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->getData()[B

    move-result-object v4

    move-object/from16 v0, v18

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-direct {v0, v4, v1, v2}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;-><init>([BFF)V

    .line 2803
    .local v18, "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawImage()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2805
    .local v3, "wmfBitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->flipTheImage()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2806
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 2807
    .local v8, "matrix":Landroid/graphics/Matrix;
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, -0x40800000    # -1.0f

    invoke-virtual {v8, v4, v5}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 2808
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    const/4 v9, 0x1

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 2812
    .local v13, "flippedBitmap":Landroid/graphics/Bitmap;
    if-eqz v13, :cond_7

    .line 2813
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x19

    move-object/from16 v0, v20

    invoke-virtual {v13, v4, v5, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_11
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 2822
    .end local v3    # "wmfBitmap":Landroid/graphics/Bitmap;
    .end local v8    # "matrix":Landroid/graphics/Matrix;
    .end local v13    # "flippedBitmap":Landroid/graphics/Bitmap;
    .end local v18    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    :cond_7
    :goto_5
    const/4 v4, 0x1

    .line 2834
    if-eqz v14, :cond_8

    .line 2836
    :try_start_c
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6

    .line 2840
    :cond_8
    :goto_6
    if-eqz v20, :cond_9

    .line 2842
    :try_start_d
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7

    :cond_9
    :goto_7
    move-object/from16 v19, v20

    .line 2845
    .end local v20    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v19    # "wmfFos":Ljava/io/FileOutputStream;
    goto/16 :goto_1

    .line 2816
    .end local v19    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v3    # "wmfBitmap":Landroid/graphics/Bitmap;
    .restart local v18    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    .restart local v20    # "wmfFos":Ljava/io/FileOutputStream;
    :cond_a
    if-eqz v3, :cond_7

    .line 2817
    :try_start_e
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x19

    move-object/from16 v0, v20

    invoke-virtual {v3, v4, v5, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_e
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_e} :catch_5
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_11
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    goto :goto_5

    .line 2829
    .end local v3    # "wmfBitmap":Landroid/graphics/Bitmap;
    .end local v18    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    :catch_5
    move-exception v11

    move-object/from16 v19, v20

    .line 2830
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v20    # "wmfFos":Ljava/io/FileOutputStream;
    .local v11, "e":Ljava/io/FileNotFoundException;
    .restart local v19    # "wmfFos":Ljava/io/FileOutputStream;
    :goto_8
    :try_start_f
    const-string/jumbo v4, "XDocCustomViewConsumer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 2834
    if-eqz v14, :cond_b

    .line 2836
    :try_start_10
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_9

    .line 2840
    .end local v11    # "e":Ljava/io/FileNotFoundException;
    :cond_b
    :goto_9
    if-eqz v19, :cond_c

    .line 2842
    :try_start_11
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a

    .line 2847
    :cond_c
    :goto_a
    const/4 v4, 0x1

    goto/16 :goto_1

    .line 2837
    .end local v19    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v12    # "fileName":Ljava/lang/String;
    .restart local v20    # "wmfFos":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v11

    .line 2838
    .local v11, "e":Ljava/io/IOException;
    const-string/jumbo v5, "XDocCustomViewConsumer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 2843
    .end local v11    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v11

    .line 2844
    .restart local v11    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "XDocCustomViewConsumer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 2837
    .end local v11    # "e":Ljava/io/IOException;
    .end local v20    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v19    # "wmfFos":Ljava/io/FileOutputStream;
    :catch_8
    move-exception v11

    .line 2838
    .restart local v11    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "XDocCustomViewConsumer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2837
    .end local v12    # "fileName":Ljava/lang/String;
    .local v11, "e":Ljava/io/FileNotFoundException;
    :catch_9
    move-exception v11

    .line 2838
    .local v11, "e":Ljava/io/IOException;
    const-string/jumbo v4, "XDocCustomViewConsumer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9

    .line 2843
    .end local v11    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v11

    .line 2844
    .restart local v11    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "XDocCustomViewConsumer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    .line 2831
    .end local v11    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v16

    .line 2832
    .local v16, "ioe":Ljava/io/IOException;
    :goto_b
    :try_start_12
    const-string/jumbo v4, "XDocCustomViewConsumer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 2834
    if-eqz v14, :cond_d

    .line 2836
    :try_start_13
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_d

    .line 2840
    :cond_d
    :goto_c
    if-eqz v19, :cond_c

    .line 2842
    :try_start_14
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_c

    goto/16 :goto_a

    .line 2843
    :catch_c
    move-exception v11

    .line 2844
    .restart local v11    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "XDocCustomViewConsumer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    .line 2837
    .end local v11    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v11

    .line 2838
    .restart local v11    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "XDocCustomViewConsumer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 2834
    .end local v11    # "e":Ljava/io/IOException;
    .end local v16    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_d
    if-eqz v14, :cond_e

    .line 2836
    :try_start_15
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_e

    .line 2840
    :cond_e
    :goto_e
    if-eqz v19, :cond_f

    .line 2842
    :try_start_16
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_f

    .line 2845
    :cond_f
    :goto_f
    throw v4

    .line 2837
    :catch_e
    move-exception v11

    .line 2838
    .restart local v11    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "XDocCustomViewConsumer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_e

    .line 2843
    .end local v11    # "e":Ljava/io/IOException;
    :catch_f
    move-exception v11

    .line 2844
    .restart local v11    # "e":Ljava/io/IOException;
    const-string/jumbo v5, "XDocCustomViewConsumer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v11}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_f

    .line 2834
    .end local v11    # "e":Ljava/io/IOException;
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "fileName":Ljava/lang/String;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    goto :goto_d

    .end local v19    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v20    # "wmfFos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v4

    move-object/from16 v19, v20

    .end local v20    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v19    # "wmfFos":Ljava/io/FileOutputStream;
    goto :goto_d

    .line 2831
    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catch_10
    move-exception v16

    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_b

    .end local v19    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v20    # "wmfFos":Ljava/io/FileOutputStream;
    :catch_11
    move-exception v16

    move-object/from16 v19, v20

    .end local v20    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v19    # "wmfFos":Ljava/io/FileOutputStream;
    goto/16 :goto_b

    .line 2829
    .end local v12    # "fileName":Ljava/lang/String;
    :catch_12
    move-exception v11

    goto/16 :goto_8

    .end local v14    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "fileName":Ljava/lang/String;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    :catch_13
    move-exception v11

    move-object v14, v15

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .restart local v14    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_8

    .line 2786
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public addBodyElements(Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;)V
    .locals 1
    .param p1, "bodyElement"    # Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;

    .prologue
    .line 2260
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    if-nez v0, :cond_0

    .line 2261
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->bodyElements:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2265
    :goto_0
    return-void

    .line 2263
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->addTxtBoxContent(Lcom/samsung/thumbnail/office/ooxml/word/IBodyElement;)V

    goto :goto_0
.end method

.method public addBreak(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;)V
    .locals 2
    .param p1, "br"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;

    .prologue
    .line 2099
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isTable:Z

    if-nez v0, :cond_0

    .line 2100
    if-eqz p1, :cond_0

    .line 2101
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;->getType()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak$EBrType;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak;->getType()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak$EBrType;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak$EBrType;->PAGE:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBreak$EBrType;

    if-ne v0, v1, :cond_1

    .line 2102
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->setPageBreak()V

    .line 2106
    :goto_0
    const/4 p1, 0x0

    .line 2109
    :cond_0
    return-void

    .line 2104
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->text:Ljava/lang/StringBuilder;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public addChart(Ljava/lang/String;)V
    .locals 1
    .param p1, "rId"    # Ljava/lang/String;

    .prologue
    .line 2423
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    if-eqz v0, :cond_0

    .line 2424
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->setChartRelId(Ljava/lang/String;)V

    .line 2426
    :cond_0
    return-void
.end method

.method public addDataModelId(Ljava/lang/String;)V
    .locals 1
    .param p1, "rId"    # Ljava/lang/String;

    .prologue
    .line 3365
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    if-eqz v0, :cond_0

    .line 3366
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->setDataModelRelId(Ljava/lang/String;)V

    .line 3368
    :cond_0
    return-void
.end method

.method public addDiagramId(Ljava/lang/String;)V
    .locals 1
    .param p1, "rId"    # Ljava/lang/String;

    .prologue
    .line 3358
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    if-eqz v0, :cond_0

    .line 3359
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->addDiagramId(Ljava/lang/String;)V

    .line 3361
    :cond_0
    return-void
.end method

.method public addGroupShapeAnchor(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "xAnchor"    # Ljava/lang/String;
    .param p2, "yAnchor"    # Ljava/lang/String;

    .prologue
    .line 3304
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupXAnchor:Ljava/lang/String;

    .line 3305
    iput-object p2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupYAnchor:Ljava/lang/String;

    .line 3306
    return-void
.end method

.method public addGroupStyle(Ljava/lang/String;)V
    .locals 2
    .param p1, "grpStyle"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 3310
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isInGroup:Z

    .line 3311
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->applyStyleParameters(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;Ljava/lang/String;Z)V

    .line 3312
    return-void
.end method

.method public addPTabCharacter(Ljava/lang/String;)V
    .locals 3
    .param p1, "alignment"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 2028
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2029
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->setTabStatus(Z)V

    .line 2037
    :goto_0
    return-void

    .line 2032
    :cond_0
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;-><init>()V

    .line 2033
    .local v0, "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 2034
    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->setTabStatus(Z)V

    .line 2035
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->addRun(Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;)V

    goto :goto_0
.end method

.method public addSectPr(Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;)V
    .locals 13
    .param p1, "sectPr"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    .line 2136
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->sectPrLst:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2138
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 2140
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v5

    .line 2142
    .local v5, "slideDimention":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    iget v7, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v8, v7

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setSlideWidth(D)V

    .line 2143
    iget v7, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v8, v7

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setSlideHeight(D)V

    .line 2145
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getPgWidth()F

    move-result v7

    float-to-int v7, v7

    int-to-long v8, v7

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setWordWidth(J)V

    .line 2146
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getPgHeight()F

    move-result v7

    float-to-int v7, v7

    int-to-long v8, v7

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setWordHeight(J)V

    .line 2148
    const/4 v6, 0x0

    .line 2149
    .local v6, "width":I
    const/4 v2, 0x0

    .line 2151
    .local v2, "height":I
    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideWidth()D

    move-result-wide v8

    double-to-int v7, v8

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideHeight()D

    move-result-wide v8

    double-to-int v8, v8

    if-ge v7, v8, :cond_0

    .line 2153
    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideWidth()D

    move-result-wide v8

    double-to-int v6, v8

    .line 2154
    int-to-long v8, v6

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getWordHeight()J

    move-result-wide v10

    mul-long/2addr v8, v10

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getWordWidth()J

    move-result-wide v10

    div-long/2addr v8, v10

    long-to-int v2, v8

    .line 2162
    :goto_0
    int-to-double v8, v6

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setCustomViewWidth(D)V

    .line 2163
    int-to-double v8, v2

    invoke-virtual {v5, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->setCustomViewHeight(D)V

    .line 2165
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getHeader()F

    move-result v8

    float-to-double v8, v8

    invoke-virtual {v7, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v7

    sput v7, Lcom/samsung/thumbnail/customview/word/Param;->HEADER_START_POINT:I

    .line 2167
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getFooter()F

    move-result v8

    float-to-double v8, v8

    invoke-virtual {v7, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v7

    sput v7, Lcom/samsung/thumbnail/customview/word/Param;->FOOTER_START_POINT:I

    .line 2175
    sget v7, Lcom/samsung/thumbnail/customview/word/Param;->HEADER_START_POINT:I

    iput v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mHeaderHeight:I

    .line 2176
    sget v7, Lcom/samsung/thumbnail/customview/word/Param;->FOOTER_START_POINT:I

    iput v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFooterHeight:I

    .line 2179
    new-instance v4, Lcom/samsung/thumbnail/office/word/Page;

    invoke-direct {v4}, Lcom/samsung/thumbnail/office/word/Page;-><init>()V

    .line 2180
    .local v4, "page":Lcom/samsung/thumbnail/office/word/Page;
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getLeft()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/word/Page;->setLeftMargin(F)V

    .line 2182
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getRight()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/word/Page;->setRightMargin(F)V

    .line 2184
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getTop()F

    move-result v8

    float-to-int v8, v8

    int-to-double v8, v8

    invoke-virtual {v7, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/word/Page;->setTopMargin(F)V

    .line 2186
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getBottom()F

    move-result v8

    float-to-int v8, v8

    int-to-double v8, v8

    invoke-virtual {v7, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/word/Page;->setBottomMargin(F)V

    .line 2188
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getTop()F

    move-result v8

    div-float/2addr v8, v12

    float-to-int v8, v8

    int-to-double v8, v8

    invoke-virtual {v7, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/word/Page;->setHeaderStartPoint(F)V

    .line 2190
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getBottom()F

    move-result v8

    div-float/2addr v8, v12

    float-to-int v8, v8

    int-to-double v8, v8

    invoke-virtual {v7, v8, v9}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/word/Page;->setFooterStartPoint(F)V

    .line 2192
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getLeftBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/word/Page;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 2193
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getTopBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/word/Page;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 2194
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getRightBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/word/Page;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 2195
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getBottomBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/word/Page;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V

    .line 2197
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getPgBorDisplay()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/word/Page;->setPgBorDisplay(Ljava/lang/String;)V

    .line 2198
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->getPgBorOffset()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/word/Page;->setPgBorOffset(Ljava/lang/String;)V

    .line 2200
    iget-boolean v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isCoverPage:Z

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/word/Page;->setIsCoverPage(Z)V

    .line 2201
    iget v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->backgroundColor:I

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/word/Page;->setBackgroundColor(I)V

    .line 2202
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7, v4}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->addPage(Lcom/samsung/thumbnail/office/word/Page;)V

    .line 2203
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->sectPrLst:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->sectPrLst:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    iget-object v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v7, v8}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;->setCanvasElementCreater(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 2205
    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v8

    double-to-int v7, v8

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v8

    double-to-int v8, v8

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2210
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2211
    .local v1, "canvas":Landroid/graphics/Canvas;
    const/4 v7, -0x1

    invoke-virtual {v1, v7}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 2213
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    invoke-virtual {v7, v1}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->setCanvas(Landroid/graphics/Canvas;)V

    .line 2214
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->setCanvasBitmap(Landroid/graphics/Bitmap;)V

    .line 2224
    return-void

    .line 2157
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v4    # "page":Lcom/samsung/thumbnail/office/word/Page;
    :cond_0
    const-wide v8, 0x3fe999999999999aL    # 0.8

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideHeight()D

    move-result-wide v10

    mul-double/2addr v8, v10

    double-to-int v2, v8

    .line 2158
    int-to-long v8, v2

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getWordWidth()J

    move-result-wide v10

    mul-long/2addr v8, v10

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getWordHeight()J

    move-result-wide v10

    div-long/2addr v8, v10

    long-to-int v6, v8

    goto/16 :goto_0
.end method

.method public addShape(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;J)V
    .locals 4
    .param p1, "shape"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;
    .param p2, "shapeZOrder"    # J

    .prologue
    .line 412
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .line 413
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    .line 415
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getShapeType()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;->SHAPETYPE:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeStyle$EShapeType;

    if-eq v0, v1, :cond_2

    .line 416
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->addShape(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    .line 421
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCurrentShapeId:Ljava/lang/String;

    .line 424
    iput-wide p2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    .line 425
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/word/Page;->isCoverPage()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 428
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    .line 431
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isInGroup:Z

    if-eqz v0, :cond_1

    .line 433
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    iget-wide v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupZorder:J

    invoke-virtual {v0, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->setGrpZorder(J)V

    .line 435
    :cond_1
    return-void

    .line 418
    :cond_2
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->addShapeTypes(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    goto :goto_0
.end method

.method public addTabCharacter(Ljava/lang/String;I)V
    .locals 3
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "position"    # I

    .prologue
    .line 2007
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2008
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->setTabStatus(Z)V

    .line 2019
    :cond_0
    if-eqz p1, :cond_1

    .line 2020
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/TabValues;

    invoke-static {p2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v1

    float-to-int v1, v1

    invoke-direct {v0, p1, v1}, Lcom/samsung/thumbnail/customview/hslf/TabValues;-><init>(Ljava/lang/String;I)V

    .line 2022
    .local v0, "tabVal":Lcom/samsung/thumbnail/customview/hslf/TabValues;
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addTabContents(Lcom/samsung/thumbnail/customview/hslf/TabValues;)V

    .line 2024
    .end local v0    # "tabVal":Lcom/samsung/thumbnail/customview/hslf/TabValues;
    :cond_1
    return-void
.end method

.method public calcRowSpan(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V
    .locals 2
    .param p1, "cellProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    .prologue
    .line 2321
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableRow:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2322
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableRow:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getGridSpan()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->addCellCount(I)V

    .line 2323
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTable:Ljava/util/Stack;

    if-nez v0, :cond_1

    .line 2348
    :cond_1
    return-void
.end method

.method public endCell()V
    .locals 2

    .prologue
    .line 2359
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableCell:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2360
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableCell:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->getCellProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2361
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableCell:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->getCellProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->calcRowSpan(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V

    .line 2366
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableCell:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 2367
    return-void

    .line 2363
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableRow:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2364
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableRow:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->addCellCount(I)V

    goto :goto_0
.end method

.method public endDocument()V
    .locals 1

    .prologue
    .line 2084
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isTable:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCustomView:Lcom/samsung/thumbnail/customview/word/CustomView;

    if-eqz v0, :cond_0

    .line 2085
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCustomView:Lcom/samsung/thumbnail/customview/word/CustomView;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CustomView;->Load()V

    .line 2095
    :cond_0
    return-void
.end method

.method public endFooter()V
    .locals 2

    .prologue
    .line 2417
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getFooter()Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->bodyElements:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;->setBodyElements(Ljava/util/ArrayList;)V

    .line 2418
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->hdrFdrLst:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getFooter()Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2419
    return-void
.end method

.method public endGroupStyle()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 3317
    iget-object v5, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getContents()Ljava/util/ArrayList;

    move-result-object v0

    .line 3318
    .local v0, "docArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 3319
    .local v1, "docArraySize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_1

    .line 3320
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .line 3321
    .local v2, "elePart":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    instance-of v5, v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    if-eqz v5, :cond_0

    .line 3322
    check-cast v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "elePart":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getXWPFShape()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v4

    .line 3324
    .local v4, "shapeProp":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->isGroupShape()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3325
    iget-object v5, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupXAnchor:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setxAnchorRef(Ljava/lang/String;)V

    .line 3326
    iget-object v5, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupYAnchor:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->setyAnchorRef(Ljava/lang/String;)V

    .line 3319
    .end local v4    # "shapeProp":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3331
    :cond_1
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isInGroup:Z

    .line 3333
    iput-object v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupXAnchor:Ljava/lang/String;

    .line 3334
    iput-object v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupYAnchor:Ljava/lang/String;

    .line 3335
    iput v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupMarginLeft:F

    .line 3336
    iput v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupMarginLeft:F

    .line 3337
    iput v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupMarginTop:F

    .line 3338
    iput v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupMarginTop:F

    .line 3341
    iput-wide v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupZorder:J

    .line 3342
    iput-wide v8, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupRotationAngle:J

    .line 3343
    iput-object v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupHorAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 3344
    iput-object v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 3345
    iput-object v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupHorRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    .line 3346
    iput-object v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupVertRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    .line 3347
    iput-object v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupFlipValue:Ljava/lang/String;

    .line 3348
    iput v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mGroupMarginTop:F

    .line 3349
    return-void
.end method

.method public endHeader()V
    .locals 2

    .prologue
    .line 2410
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getHeader()Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->bodyElements:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;->setBodyElements(Ljava/util/ArrayList;)V

    .line 2411
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->hdrFdrLst:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getHeader()Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2412
    return-void
.end method

.method public endHyperLink()V
    .locals 0

    .prologue
    .line 2255
    return-void
.end method

.method public endPara()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 550
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->processParagraph(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V

    .line 551
    iget-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v1, :cond_6

    .line 552
    iget-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isTable:Z

    if-nez v1, :cond_5

    .line 553
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/word/Page;->addElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 566
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isTable:Z

    if-nez v1, :cond_3

    .line 567
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 568
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v0

    .line 570
    .local v0, "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getKeepLinesStatus()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getKeepNxtStatus()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 572
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iput-boolean v3, v1, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    .line 574
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getPgBrBefStatus()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 577
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    .line 580
    .end local v0    # "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    :cond_2
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->drawOnCanvas(Landroid/content/Context;)Z

    .line 583
    iget-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->breakPage:Z

    if-eqz v1, :cond_3

    .line 584
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iput-boolean v3, v1, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    .line 587
    :cond_3
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 588
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 589
    :cond_4
    return-void

    .line 556
    :cond_5
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableCell:Ljava/util/Stack;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableCell:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 557
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableCell:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;

    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->addPara(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    goto :goto_0

    .line 560
    :cond_6
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v1, :cond_0

    .line 561
    iget-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isTextBox:Z

    if-eqz v1, :cond_7

    .line 562
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->addTxtBoxContent(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    goto/16 :goto_0

    .line 564
    :cond_7
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    goto/16 :goto_0
.end method

.method public endPicture()V
    .locals 5

    .prologue
    .line 316
    const/4 v0, 0x0

    .line 317
    .local v0, "id":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    if-eqz v2, :cond_0

    .line 318
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getId()Ljava/lang/String;

    move-result-object v0

    .line 320
    :cond_0
    const/4 v1, 0x0

    .line 322
    .local v1, "isWaterMarkImage":Z
    if-eqz v0, :cond_1

    .line 323
    const-string/jumbo v2, "Watermark"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 324
    const/4 v1, 0x1

    .line 328
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->isInLine()Z

    move-result v2

    if-nez v2, :cond_2

    .line 329
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    iget-object v3, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    iget v4, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->paraNumber:I

    invoke-virtual {p0, v2, v3, v4, v1}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->writePicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;IZ)V

    .line 332
    :cond_2
    return-void
.end method

.method public endRow()V
    .locals 1

    .prologue
    .line 2294
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableRow:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2295
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableRow:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 2297
    :cond_0
    return-void
.end method

.method public endShape()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 461
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/word/Page;->isCoverPage()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStyle()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, v4}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->applyStyleParameters(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;Ljava/lang/String;Z)V

    .line 465
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->drawShape(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    .line 466
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->drawShapePicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    .line 476
    :cond_1
    :goto_0
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeCount:I

    .line 477
    iput-boolean v4, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    .line 478
    iput-object v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    .line 479
    iput v5, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginLeft:F

    .line 480
    iput v5, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeMarginTop:F

    .line 481
    iput-wide v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    .line 482
    iput-object v6, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCurrentShapeId:Ljava/lang/String;

    .line 483
    return-void

    .line 467
    :cond_2
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeZorder:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 469
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getGrpZorder()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 470
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->getShapeProp()Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShapeProp;->getStyle()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, v4}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->applyStyleParameters(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;Ljava/lang/String;Z)V

    .line 472
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->drawShape(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    .line 473
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->drawShapePicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    goto :goto_0
.end method

.method public endTable()V
    .locals 5

    .prologue
    .line 2375
    new-instance v1, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;

    invoke-direct {v1}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;-><init>()V

    .line 2377
    .local v1, "newCanvasTable":Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->HEADER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    if-ne v2, v3, :cond_2

    .line 2378
    sget-object v2, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->setTableType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 2386
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTable:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2387
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTable:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    iget-object v4, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    iget-object v3, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTable:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v3

    invoke-virtual {v1, v2, p0, v4, v3}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->createNewTable(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V

    .line 2391
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v2

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->getCurrentTable()Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/word/Page;->addElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 2393
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTable:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2400
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTable:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2401
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isTable:Z

    .line 2402
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    iget-object v3, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;->drawOnCanvas(Landroid/content/Context;)Z

    .line 2403
    return-void

    .line 2379
    :cond_2
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->FOOTER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    if-ne v2, v3, :cond_3

    .line 2380
    sget-object v2, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->setTableType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    goto :goto_0

    .line 2382
    :cond_3
    sget-object v2, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->NORMAL:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->setTableType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    goto :goto_0

    .line 2396
    :catch_0
    move-exception v0

    .line 2397
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "XDocCustomViewConsumer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public endText()V
    .locals 0

    .prologue
    .line 503
    return-void
.end method

.method public endTextBox()V
    .locals 1

    .prologue
    .line 605
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isTextBox:Z

    .line 606
    return-void
.end method

.method public endTextBoxContents()V
    .locals 0

    .prologue
    .line 616
    return-void
.end method

.method public endTextRun()V
    .locals 2

    .prologue
    .line 2043
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2044
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isTable:Z

    if-eqz v0, :cond_2

    .line 2045
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    if-nez v0, :cond_0

    .line 2059
    :goto_0
    return-void

    .line 2048
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->text:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2049
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->text:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->setText(Ljava/lang/String;)V

    .line 2051
    :cond_1
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->resetRunProp()V

    goto :goto_0

    .line 2055
    :cond_2
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->text:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_3

    .line 2056
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->text:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->setText(Ljava/lang/String;)V

    .line 2058
    :cond_3
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->resetRunProp()V

    goto :goto_0
.end method

.method public getCanvas()Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFirstPageCanvas:Lcom/samsung/thumbnail/customview/word/FirstPageCanvas;

    return-object v0
.end method

.method public getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .locals 1

    .prologue
    .line 3299
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    return-object v0
.end method

.method public getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    return-object v0
.end method

.method public getElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    return-object v0
.end method

.method public getFolderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->folderName:Ljava/lang/String;

    return-object v0
.end method

.method public getFolderPath()Ljava/io/File;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->folderPath:Ljava/io/File;

    return-object v0
.end method

.method public lastRenderedPageBreak()V
    .locals 2

    .prologue
    .line 2130
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->setLastRenderedPgBr(Z)V

    .line 2131
    return-void
.end method

.method public setBackgroundColor(Ljava/lang/String;)V
    .locals 2
    .param p1, "colorStr"    # Ljava/lang/String;

    .prologue
    .line 1988
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1989
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->backgroundColor:I

    .line 1990
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v0

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->backgroundColor:I

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/word/Page;->setBackgroundColor(I)V

    .line 1993
    :cond_0
    return-void
.end method

.method public setCharacterProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 1
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 2235
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->setCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 2236
    return-void
.end method

.method public setDefaultTabStop(Ljava/lang/String;)V
    .locals 1
    .param p1, "tabStopPos"    # Ljava/lang/String;

    .prologue
    .line 3292
    invoke-static {p1}, Lcom/samsung/thumbnail/util/Utils;->isValidInteger(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3293
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->defTabStopPos:I

    .line 3295
    :cond_0
    return-void
.end method

.method public setDocPartGalleryVal(Ljava/lang/String;)V
    .locals 2
    .param p1, "docPartGalleryVal"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 620
    if-eqz p1, :cond_0

    const-string/jumbo v0, "Cover Pages"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 622
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isCoverPage:Z

    .line 623
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/word/Page;->setIsCoverPage(Z)V

    .line 625
    :cond_0
    return-void
.end method

.method public setDocPr(ILjava/lang/String;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 3354
    return-void
.end method

.method public setDrSpeNme(Ljava/lang/String;)V
    .locals 1
    .param p1, "shapeName"    # Ljava/lang/String;

    .prologue
    .line 370
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    if-nez v0, :cond_0

    .line 373
    :goto_0
    return-void

    .line 372
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setEnclosedSpName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setEffectExtent(IIII)V
    .locals 1
    .param p1, "l"    # I
    .param p2, "r"    # I
    .param p3, "b"    # I
    .param p4, "t"    # I

    .prologue
    .line 378
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    if-nez v0, :cond_0

    .line 381
    :goto_0
    return-void

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setEffectExtent(IIII)V

    goto :goto_0
.end method

.method public setElementType(Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;)V
    .locals 0
    .param p1, "elementType"    # Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    .prologue
    .line 297
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    .line 298
    return-void
.end method

.method public setFldSimple(Ljava/lang/String;)V
    .locals 1
    .param p1, "instr"    # Ljava/lang/String;

    .prologue
    .line 2431
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->setFldSmplInstr(Ljava/lang/String;)V

    .line 2432
    return-void
.end method

.method public setImageRelId(Ljava/lang/String;)V
    .locals 1
    .param p1, "imgRelId"    # Ljava/lang/String;

    .prologue
    .line 337
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    if-nez v0, :cond_0

    .line 341
    :goto_0
    return-void

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setImageRelId(Ljava/lang/String;)V

    .line 340
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->setPic()V

    goto :goto_0
.end method

.method public setParaCharProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V
    .locals 1
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .prologue
    .line 2241
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->setCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    .line 2242
    return-void
.end method

.method public setParagraphProperty(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V
    .locals 1
    .param p1, "paraProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    .prologue
    .line 2229
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->setParaProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;)V

    .line 2230
    return-void
.end method

.method public setPictAnchorProp(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "relativeHeight"    # Ljava/lang/String;
    .param p2, "behindDoc"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 386
    const-wide/16 v2, 0x0

    .line 387
    .local v2, "relHeight":J
    const/4 v0, 0x0

    .line 390
    .local v0, "behDoc":I
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 391
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 396
    :goto_0
    iget-object v4, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    invoke-virtual {v4, v2, v3}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setRelativeHeight(J)V

    .line 397
    if-nez v0, :cond_1

    .line 398
    iget-object v4, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setBehindDoc(Z)V

    .line 401
    :cond_0
    :goto_1
    return-void

    .line 392
    :catch_0
    move-exception v1

    .line 393
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v4, "XDocCustomViewConsumer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 399
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    if-ne v0, v7, :cond_0

    .line 400
    iget-object v4, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    invoke-virtual {v4, v7}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setBehindDoc(Z)V

    goto :goto_1
.end method

.method public setPictPosOffset(Ljava/lang/String;J)V
    .locals 4
    .param p1, "offsetRef"    # Ljava/lang/String;
    .param p2, "offsetVal"    # J

    .prologue
    .line 405
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    invoke-static {p2, p3}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->toPoints(J)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, p1, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setPictPosOffset(Ljava/lang/String;F)V

    .line 407
    return-void
.end method

.method public setPictureExtent(II)V
    .locals 1
    .param p1, "cx"    # I
    .param p2, "cy"    # I

    .prologue
    .line 355
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    if-nez v0, :cond_0

    .line 358
    :goto_0
    return-void

    .line 357
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setExtent(II)V

    goto :goto_0
.end method

.method public setPictureType(Ljava/lang/String;)V
    .locals 2
    .param p1, "imageType"    # Ljava/lang/String;

    .prologue
    .line 629
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 630
    const-string/jumbo v0, "inline"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 631
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setInLineStatus(Z)V

    .line 635
    :cond_0
    :goto_0
    return-void

    .line 632
    :cond_1
    const-string/jumbo v0, "anchor"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setInLineStatus(Z)V

    goto :goto_0
.end method

.method public setRotation(I)V
    .locals 1
    .param p1, "rotation"    # I

    .prologue
    .line 363
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    if-nez v0, :cond_0

    .line 366
    :goto_0
    return-void

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setRotation(I)V

    goto :goto_0
.end method

.method public setTblCellProperty(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V
    .locals 1
    .param p1, "cellProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    .prologue
    .line 2316
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableCell:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2317
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableCell:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->setCellProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;)V

    .line 2318
    :cond_0
    return-void
.end method

.method public setTblGridColWidth(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2280
    .local p1, "gridColWidthArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTable:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2281
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTable:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->setGridColWidthArray(Ljava/util/ArrayList;)V

    .line 2282
    :cond_0
    return-void
.end method

.method public setTblProperty(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V
    .locals 1
    .param p1, "tableProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    .prologue
    .line 2309
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTable:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2310
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTable:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->setTableProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V

    .line 2312
    :cond_0
    return-void
.end method

.method public setTblRowProperty(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;)V
    .locals 1
    .param p1, "rowProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    .prologue
    .line 2352
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableRow:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2353
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableRow:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->setTblRowProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;)V

    .line 2354
    :cond_0
    return-void
.end method

.method public setTxtBxRotParam(Ljava/lang/String;)V
    .locals 1
    .param p1, "rotParam"    # Ljava/lang/String;

    .prologue
    .line 593
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    if-eqz v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;->setOrientParam(Ljava/lang/String;)V

    .line 596
    :cond_0
    return-void
.end method

.method public setWrapSquareProp(Ljava/lang/String;)V
    .locals 2
    .param p1, "wrapVal"    # Ljava/lang/String;

    .prologue
    .line 345
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    if-nez v0, :cond_0

    .line 350
    :goto_0
    return-void

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->setInLineStatus(Z)V

    goto :goto_0
.end method

.method public startCell()V
    .locals 2

    .prologue
    .line 2301
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableRow:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2302
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableCell:Ljava/util/Stack;

    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 2303
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableRow:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableCell:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->addTableCell(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;)V

    .line 2305
    :cond_0
    return-void
.end method

.method public startDocument()V
    .locals 2

    .prologue
    .line 2064
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->bodyElements:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2065
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->PARAGRAPH:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    if-ne v0, v1, :cond_1

    .line 2066
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->sectPrLst:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2067
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->sectPrLst:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->sectPrLst:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->sectPr:Lcom/samsung/thumbnail/office/ooxml/word/XWPFSectPr;

    .line 2069
    :cond_0
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->startBody()V

    .line 2072
    :cond_1
    return-void
.end method

.method public startHyperLink(Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;)V
    .locals 1
    .param p1, "hyperlinkRun"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFHyperlinkRun;

    .prologue
    .line 2247
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->addRun(Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;)V

    .line 2248
    return-void
.end method

.method public startPara()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 509
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 512
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v0, :cond_2

    .line 513
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 514
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->defTabStopPos:I

    invoke-static {v1}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setDefTabSpacing(I)V

    .line 519
    :goto_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->HEADER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    if-ne v0, v1, :cond_3

    .line 520
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->setIsHeader(Z)V

    .line 521
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v0, :cond_0

    .line 522
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    sget-object v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParaType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 523
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mHeaderParaNum:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mHeaderParaNum:I

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParaNumber(I)V

    .line 541
    :cond_0
    :goto_1
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isTable:Z

    if-eqz v0, :cond_1

    .line 542
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableCell:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 543
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableCell:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->addPara(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;)V

    .line 546
    :cond_1
    return-void

    .line 517
    :cond_2
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    goto :goto_0

    .line 525
    :cond_3
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->FOOTER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    if-ne v0, v1, :cond_4

    .line 526
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->setIsFooter(Z)V

    .line 527
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    sget-object v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParaType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 529
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFooterParaNum:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mFooterParaNum:I

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParaNumber(I)V

    goto :goto_1

    .line 532
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isShape:Z

    if-nez v0, :cond_5

    .line 533
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    sget-object v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->NORMAL:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParaType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 534
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mNormalParaNum:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mNormalParaNum:I

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParaNumber(I)V

    goto :goto_1

    .line 536
    :cond_5
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShapeParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    sget-object v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->NORMAL:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParaType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    goto :goto_1
.end method

.method public startPicture()V
    .locals 2

    .prologue
    .line 308
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mShape:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFShape;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    .line 309
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mPicture:Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->addPicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;)V

    .line 310
    return-void
.end method

.method public startRow()V
    .locals 2

    .prologue
    .line 2287
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableRow:Ljava/util/Stack;

    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 2288
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTable:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableRow:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2289
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTable:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTableRow:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->addRow(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;)V

    .line 2290
    :cond_0
    return-void
.end method

.method public startTable()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2269
    iput-boolean v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isTable:Z

    .line 2270
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mTable:Ljava/util/Stack;

    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;-><init>(Z)V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 2272
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->HEADER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->elementType:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->FOOTER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    if-ne v0, v1, :cond_0

    .line 2276
    :cond_0
    return-void
.end method

.method public startTextBox()V
    .locals 1

    .prologue
    .line 600
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->isTextBox:Z

    .line 601
    return-void
.end method

.method public startTextBoxContents()V
    .locals 0

    .prologue
    .line 611
    return-void
.end method

.method public startTextRun()V
    .locals 3

    .prologue
    .line 1997
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2002
    :goto_0
    return-void

    .line 1999
    :cond_0
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;-><init>()V

    .line 2000
    .local v0, "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 2001
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mParaStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->run:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->addRun(Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;)V

    goto :goto_0
.end method

.method public text([CII)V
    .locals 3
    .param p1, "content"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    .line 489
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    .line 491
    .local v0, "chars":Ljava/lang/String;
    const-string/jumbo v1, "&nbsp;&nbsp;&nbsp;&nbsp;"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 492
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->text:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 497
    :goto_0
    return-void

    .line 495
    :cond_0
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->text:Ljava/lang/StringBuilder;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method protected writePicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;IZ)V
    .locals 14
    .param p1, "picture"    # Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;
    .param p2, "type"    # Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;
    .param p3, "paraNumber"    # I
    .param p4, "isWaterMarkImage"    # Z

    .prologue
    .line 2691
    const/4 v10, 0x0

    .line 2692
    .local v10, "pictureData":Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->HEADER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_2

    .line 2693
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getHeader()Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;->getRelList()Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getPictData(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;Ljava/util/List;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    move-result-object v10

    .line 2706
    :cond_0
    :goto_0
    if-nez v10, :cond_5

    .line 2766
    :cond_1
    :goto_1
    return-void

    .line 2695
    :cond_2
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->FOOTER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_3

    .line 2696
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getFooter()Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;->getRelList()Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getPictData(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;Ljava/util/List;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    move-result-object v10

    goto :goto_0

    .line 2698
    :cond_3
    iget-boolean v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->bIsDiagram:Z

    if-eqz v2, :cond_4

    .line 2699
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->getRelList()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2700
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDiagram:Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/diagram/XWPFDiagram;->getRelList()Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getPictData(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;Ljava/util/List;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    move-result-object v10

    goto :goto_0

    .line 2703
    :cond_4
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getDocRelList()Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getPictData(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;Ljava/util/List;)Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;

    move-result-object v10

    goto :goto_0

    .line 2709
    :cond_5
    const/high16 v5, 0x42200000    # 40.0f

    .line 2710
    .local v5, "width":F
    const/high16 v6, 0x42200000    # 40.0f

    .line 2712
    .local v6, "height":F
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getWidth()F

    move-result v5

    .line 2713
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getHeight()F

    move-result v6

    .line 2715
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v4

    .line 2717
    .local v4, "sd":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    float-to-int v2, v5

    invoke-virtual {v4, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v2

    int-to-float v2, v2

    float-to-int v7, v6

    int-to-double v12, v7

    invoke-virtual {v4, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v7

    int-to-float v7, v7

    invoke-direct {p0, v10, p1, v2, v7}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->writePicture(Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;FF)Z

    move-result v9

    .line 2721
    .local v9, "isWMFOrEMF":Z
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->folderPath:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v7, "/"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 2722
    .local v11, "srcPath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPictureData;->getFileName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 2726
    const/4 v3, 0x0

    .line 2727
    .local v3, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v9, :cond_8

    .line 2728
    float-to-int v2, v5

    invoke-virtual {v4, v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v2

    float-to-int v7, v6

    int-to-double v12, v7

    invoke-virtual {v4, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v7

    invoke-static {v11, v2, v7}, Lcom/samsung/thumbnail/util/Utils;->decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v3

    :goto_2
    move-object v2, p0

    move-object v7, p1

    .line 2740
    invoke-direct/range {v2 .. v7}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->updateBitmapParam(Landroid/graphics/Bitmap;Lcom/samsung/thumbnail/customview/hslf/SlideDimention;FFLcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;)Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    move-result-object v8

    .line 2743
    .local v8, "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    if-eqz v8, :cond_1

    .line 2744
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->HEADER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_9

    .line 2745
    const/4 v2, 0x1

    invoke-virtual {v8, v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setIsHeaderImage(Z)V

    .line 2746
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getHeader()Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFHeader;->getHeaderName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setHeaderName(Ljava/lang/String;)V

    .line 2754
    :cond_6
    :goto_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->isInLine()Z

    move-result v2

    if-nez v2, :cond_7

    .line 2755
    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setIsInLineStatus(Z)V

    .line 2757
    :cond_7
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getRotation()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v8, v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setRotationAngle(I)V

    .line 2758
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setEnclosedName(Ljava/lang/String;)V

    .line 2759
    move/from16 v0, p4

    invoke-virtual {v8, v0}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setWaterMarkImage(Z)V

    .line 2760
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getRelativeHeight()J

    move-result-wide v12

    invoke-virtual {v8, v12, v13}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setRelativeHeight(J)V

    .line 2761
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/shape/XWPFPicture;->getBehindDocState()Z

    move-result v2

    invoke-virtual {v8, v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setIsBehindDocState(Z)V

    .line 2762
    sget-object v2, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v8, v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 2764
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocParagraph:Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-virtual {v2, v8}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2735
    .end local v8    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    :cond_8
    float-to-int v2, v5

    float-to-int v7, v6

    invoke-static {v11, v2, v7}, Lcom/samsung/thumbnail/util/Utils;->decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_2

    .line 2748
    .restart local v8    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    :cond_9
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;->FOOTER:Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    move-object/from16 v0, p2

    if-ne v0, v2, :cond_6

    .line 2749
    const/4 v2, 0x1

    invoke-virtual {v8, v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setIsFooterImage(Z)V

    .line 2750
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->mDocument:Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getFooter()Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFFooter;->getFooterName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setFooterName(Ljava/lang/String;)V

    goto :goto_3
.end method
