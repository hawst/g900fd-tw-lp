.class public Lcom/samsung/thumbnail/customview/word/Param;
.super Ljava/lang/Object;
.source "Param.java"


# static fields
.field public static final FONTSIZE_MULTIPLIER:F = 1.8f

.field public static FOOTER_START_POINT:I = 0x0

.field public static HEADER_START_POINT:I = 0x0

.field public static final LINE_SPACING:I = 0xc

.field public static MARGIN_BOTTOM:I = 0x0

.field public static MARGIN_LEFT:I = 0x0

.field public static MARGIN_RIGHT:I = 0x0

.field public static MARGIN_TOP:I = 0x0

.field public static final PARA_SPACING:I = 0x5


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5
    sput v0, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    .line 6
    sput v0, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_RIGHT:I

    .line 7
    sput v0, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    .line 8
    sput v0, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_BOTTOM:I

    .line 10
    sput v0, Lcom/samsung/thumbnail/customview/word/Param;->HEADER_START_POINT:I

    .line 11
    sput v0, Lcom/samsung/thumbnail/customview/word/Param;->FOOTER_START_POINT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMarginBottom()I
    .locals 1

    .prologue
    .line 46
    sget v0, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    return v0
.end method

.method public getMarginLeft()I
    .locals 1

    .prologue
    .line 22
    sget v0, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    return v0
.end method

.method public getMarginRight()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    return v0
.end method

.method public getMarginTop()I
    .locals 1

    .prologue
    .line 38
    sget v0, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    return v0
.end method

.method public setMarginBottom(I)V
    .locals 0
    .param p1, "pageBottomMargin"    # I

    .prologue
    .line 42
    sput p1, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    .line 43
    return-void
.end method

.method public setMarginLeft(I)V
    .locals 0
    .param p1, "pageLeftMargin"    # I

    .prologue
    .line 18
    sput p1, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    .line 19
    return-void
.end method

.method public setMarginRight(I)V
    .locals 0
    .param p1, "pageRightMargin"    # I

    .prologue
    .line 26
    sput p1, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    .line 27
    return-void
.end method

.method public setMarginTop(I)V
    .locals 0
    .param p1, "pageTopMargin"    # I

    .prologue
    .line 34
    sput p1, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    .line 35
    return-void
.end method
