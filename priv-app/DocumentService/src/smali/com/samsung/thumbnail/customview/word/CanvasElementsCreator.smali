.class public Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
.super Ljava/lang/Object;
.source "CanvasElementsCreator.java"


# instance fields
.field public mCanvasElementsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/CanvasElementPart;",
            ">;"
        }
    .end annotation
.end field

.field private mSlideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

.field private pageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/word/Page;",
            ">;"
        }
    .end annotation
.end field

.field public stopParsing:Z


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V
    .locals 1
    .param p1, "slideDimention"    # Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->pageList:Ljava/util/ArrayList;

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    .line 21
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->mSlideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    .line 22
    return-void
.end method


# virtual methods
.method public AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V
    .locals 1
    .param p1, "element"    # Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->mCanvasElementsList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->mCanvasElementsList:Ljava/util/ArrayList;

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->mCanvasElementsList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    return-void
.end method

.method public addPage(Lcom/samsung/thumbnail/office/word/Page;)V
    .locals 1
    .param p1, "page"    # Lcom/samsung/thumbnail/office/word/Page;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->pageList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->pageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->pageList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    :cond_0
    return-void
.end method

.method public getCanvasElementsList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/CanvasElementPart;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->mCanvasElementsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPageList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/office/word/Page;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->pageList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRecentPage()Lcom/samsung/thumbnail/office/word/Page;
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->pageList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->pageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->pageList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->pageList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/word/Page;

    .line 58
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRecentPara()Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->mCanvasElementsList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->mCanvasElementsList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->mCanvasElementsList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .line 41
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->mSlideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    return-object v0
.end method
