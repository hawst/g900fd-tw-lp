.class public Lcom/samsung/thumbnail/customview/CanvasElementPart;
.super Ljava/lang/Object;
.source "CanvasElementPart.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;,
        Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;,
        Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;,
        Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;,
        Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;
    }
.end annotation


# instance fields
.field public documentType:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

.field private mDrawnOnCanvas:Z

.field private mSlideNumber:I

.field private mVerRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

.field private mVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/CanvasElementPart;->documentType:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    .line 28
    return-void
.end method


# virtual methods
.method public getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/CanvasElementPart;->documentType:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    return-object v0
.end method

.method public getDrawnStatus()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/CanvasElementPart;->mDrawnOnCanvas:Z

    return v0
.end method

.method public getSlideNumber()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/samsung/thumbnail/customview/CanvasElementPart;->mSlideNumber:I

    return v0
.end method

.method public getVerticalAlignmentElement()Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/CanvasElementPart;->mVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    return-object v0
.end method

.method public getVerticalRelativeElement()Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/CanvasElementPart;->mVerRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    return-object v0
.end method

.method public setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V
    .locals 0
    .param p1, "docType"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/CanvasElementPart;->documentType:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    .line 100
    return-void
.end method

.method public setDrawnStatus(Z)V
    .locals 0
    .param p1, "drawnStatus"    # Z

    .prologue
    .line 112
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/CanvasElementPart;->mDrawnOnCanvas:Z

    .line 113
    return-void
.end method

.method public setSlideNumber(I)V
    .locals 0
    .param p1, "mSlideNumber"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/samsung/thumbnail/customview/CanvasElementPart;->mSlideNumber:I

    .line 135
    return-void
.end method

.method public setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V
    .locals 0
    .param p1, "verAlignEle"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/CanvasElementPart;->mVertAlignEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    .line 59
    return-void
.end method

.method public setVerticalRelativeElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;)V
    .locals 0
    .param p1, "verRelEle"    # Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/CanvasElementPart;->mVerRelEle:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    .line 84
    return-void
.end method
