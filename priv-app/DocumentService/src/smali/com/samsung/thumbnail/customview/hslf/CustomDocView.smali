.class public Lcom/samsung/thumbnail/customview/hslf/CustomDocView;
.super Landroid/view/View;
.source "CustomDocView.java"


# instance fields
.field private drawObj:Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;

.field private elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field public mPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 1
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/util/AttributeSet;
    .param p3, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->drawObj:Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;

    .line 45
    iput-object p3, p0, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 46
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->init()V

    .line 47
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 77
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->mPaint:Landroid/graphics/Paint;

    .line 78
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->mPaint:Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 80
    new-instance v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;

    new-instance v1, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-direct {v1}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;-><init>()V

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;-><init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->drawObj:Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;

    .line 82
    return-void
.end method


# virtual methods
.method public clearView(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 1
    .param p1, "elementCeator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 97
    if-eqz p1, :cond_0

    .line 98
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getCanvasElementsList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->invalidate()V

    .line 102
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->drawObj:Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->elementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0, p1, p0, v1}, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->handleDraw(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/hslf/CustomDocView;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 88
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 89
    return-void
.end method
