.class public Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;
.super Ljava/lang/Object;
.source "HSLFCanvasDraw.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "HSLFCanvasDraw"


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCanvas:Landroid/graphics/Canvas;

.field public mPaint:Landroid/graphics/Paint;

.field private slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

.field private y:I


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V
    .locals 2
    .param p1, "slideDimention"    # Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->y:I

    .line 56
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    .line 57
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 58
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    .line 59
    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "slideDimention"    # Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->y:I

    .line 62
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    .line 63
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 64
    iput-object p2, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mBitmap:Landroid/graphics/Bitmap;

    .line 65
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    .line 66
    return-void
.end method

.method private drawBitMap(Landroid/graphics/Bitmap;IIII)V
    .locals 8
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    .line 341
    if-eqz p1, :cond_0

    .line 342
    const/4 v1, 0x0

    .line 345
    .local v1, "scalableBitmap":Landroid/graphics/Bitmap;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    int-to-double v4, p4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget-object v3, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    int-to-double v4, p5

    invoke-virtual {v3, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v4

    double-to-int v3, v4

    const/4 v4, 0x0

    invoke-static {p1, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 351
    :goto_0
    if-eqz v1, :cond_1

    .line 352
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    int-to-double v4, p2

    invoke-virtual {v3, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v4

    double-to-int v3, v4

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    int-to-double v6, p3

    invoke-virtual {v4, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v4

    double-to-int v4, v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 359
    .end local v1    # "scalableBitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_1
    return-void

    .line 348
    .restart local v1    # "scalableBitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v0

    .line 349
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "HSLFCanvasDraw"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 356
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    int-to-double v4, p2

    invoke-virtual {v3, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v4

    double-to-int v3, v4

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    int-to-double v6, p3

    invoke-virtual {v4, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v4

    double-to-int v4, v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, p1, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method private drawParagraph(Ljava/lang/String;)V
    .locals 0
    .param p1, "TextToBeWritten"    # Ljava/lang/String;

    .prologue
    .line 322
    return-void
.end method

.method private drawRect(IIIII)V
    .locals 6
    .param p1, "top"    # I
    .param p2, "left"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "color"    # I

    .prologue
    .line 377
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p5}, Landroid/graphics/Paint;->setColor(I)V

    .line 378
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    int-to-float v1, p2

    int-to-float v2, p1

    int-to-float v3, p3

    int-to-float v4, p4

    iget-object v5, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 379
    return-void
.end method

.method private drawText(Lcom/samsung/thumbnail/customview/hslf/TextContent;Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "text"    # Lcom/samsung/thumbnail/customview/hslf/TextContent;
    .param p2, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    .line 287
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 288
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getFontSize()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 289
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getTextColor()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 291
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getWidth()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getxCoOrdinate()F

    move-result v3

    float-to-double v6, v3

    add-double/2addr v4, v6

    double-to-float v1, v4

    .line 292
    .local v1, "canvasWidth":F
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    .line 293
    .local v0, "blackTextWidth":F
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getWidth()D

    move-result-wide v4

    div-double/2addr v4, v10

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getxCoOrdinate()F

    move-result v3

    float-to-double v6, v3

    add-double/2addr v4, v6

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v0, v3

    float-to-double v6, v3

    sub-double/2addr v4, v6

    double-to-float v2, v4

    .line 296
    .local v2, "startPositionX":F
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getAllignment()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "center"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 297
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getyCoOrdinate()F

    move-result v5

    float-to-double v6, v5

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getHeight()D

    move-result-wide v8

    div-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-float v5, v6

    iget-object v6, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 309
    :goto_0
    return-void

    .line 300
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getAllignment()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "right"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 301
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getText()Ljava/lang/String;

    move-result-object v4

    sub-float v5, v1, v0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getyCoOrdinate()F

    move-result v6

    float-to-double v6, v6

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getHeight()D

    move-result-wide v8

    div-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-float v6, v6

    iget-object v7, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 305
    :cond_1
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getxCoOrdinate()F

    move-result v5

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getyCoOrdinate()F

    move-result v6

    float-to-double v6, v6

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getHeight()D

    move-result-wide v8

    div-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-float v6, v6

    iget-object v7, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method


# virtual methods
.method public getBitmapFromCanvas()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public handleDraw(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/hslf/CustomDocView;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 25
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "customDocView"    # Lcom/samsung/thumbnail/customview/hslf/CustomDocView;
    .param p3, "elementCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 165
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    .line 166
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v5}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 167
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getCanvasElementsList()Ljava/util/ArrayList;

    move-result-object v15

    .line 170
    .local v15, "customContents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    if-eqz v15, :cond_a

    .line 171
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 172
    .local v13, "contentSize":I
    const/16 v24, 0x0

    .local v24, "x1":I
    :goto_0
    move/from16 v0, v24

    if-ge v0, v13, :cond_a

    .line 174
    move/from16 v0, v24

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .line 176
    .local v14, "customContent":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    instance-of v4, v14, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    if-eqz v4, :cond_1

    move-object v10, v14

    .line 177
    check-cast v10, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .line 178
    .local v10, "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getSlideNumber()I

    move-result v4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->getId()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 179
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v6

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getTopCoOrdinate()I

    move-result v7

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v8

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v9

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIII)V

    .line 172
    .end local v10    # "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    :cond_0
    :goto_1
    add-int/lit8 v24, v24, 0x1

    goto :goto_0

    .line 183
    :cond_1
    instance-of v4, v14, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;

    if-eqz v4, :cond_2

    move-object/from16 v21, v14

    .line 184
    check-cast v21, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;

    .line 185
    .local v21, "rectParam":Lcom/samsung/thumbnail/customview/hslf/RectangleParam;
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->getSlideNumber()I

    move-result v4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->getId()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 186
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->getTop()I

    move-result v5

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->getLeft()I

    move-result v6

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->getRight()I

    move-result v7

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->getBottom()I

    move-result v8

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->getColor()I

    move-result v9

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->drawRect(IIIII)V

    goto :goto_1

    .line 190
    .end local v21    # "rectParam":Lcom/samsung/thumbnail/customview/hslf/RectangleParam;
    :cond_2
    instance-of v4, v14, Lcom/samsung/thumbnail/customview/hslf/LineParam;

    if-eqz v4, :cond_3

    move-object/from16 v17, v14

    .line 191
    check-cast v17, Lcom/samsung/thumbnail/customview/hslf/LineParam;

    .line 192
    .local v17, "lineParam":Lcom/samsung/thumbnail/customview/hslf/LineParam;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->getSlideNumber()I

    move-result v4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->getId()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 193
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->getLineColor()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 194
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->getLineWidth()F

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 195
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->getStartX()F

    move-result v5

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->getStartY()F

    move-result v6

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->getEndX()F

    move-result v7

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->getEndY()F

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 200
    .end local v17    # "lineParam":Lcom/samsung/thumbnail/customview/hslf/LineParam;
    :cond_3
    instance-of v4, v14, Lcom/samsung/thumbnail/customview/hslf/TextContent;

    if-eqz v4, :cond_4

    move-object/from16 v23, v14

    .line 201
    check-cast v23, Lcom/samsung/thumbnail/customview/hslf/TextContent;

    .line 202
    .local v23, "textContent":Lcom/samsung/thumbnail/customview/hslf/TextContent;
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/customview/hslf/TextContent;->getSlideNumber()I

    move-result v4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->getId()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 203
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v4}, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->drawText(Lcom/samsung/thumbnail/customview/hslf/TextContent;Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 204
    .end local v23    # "textContent":Lcom/samsung/thumbnail/customview/hslf/TextContent;
    :cond_4
    instance-of v4, v14, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;

    if-eqz v4, :cond_7

    move-object v11, v14

    .line 205
    check-cast v11, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;

    .line 206
    .local v11, "bg":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    invoke-virtual {v11}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getSlideNumber()I

    move-result v4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->getId()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 207
    invoke-virtual {v11}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getIsBackgroundImage()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 208
    invoke-virtual {v11}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getBGBitMap()Landroid/graphics/Bitmap;

    move-result-object v12

    .line 209
    .local v12, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v12, :cond_0

    .line 210
    const/16 v22, 0x0

    .line 215
    .local v22, "scalableBitmap":Landroid/graphics/Bitmap;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideWidth()D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getObjectWidth(D)D

    move-result-wide v4

    double-to-int v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideHeight()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getObjectHeight(D)D

    move-result-wide v6

    double-to-int v5, v6

    const/4 v6, 0x0

    invoke-static {v12, v4, v5, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v22

    .line 229
    :goto_2
    new-instance v18, Landroid/graphics/Matrix;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Matrix;-><init>()V

    .line 230
    .local v18, "mtx":Landroid/graphics/Matrix;
    if-eqz v22, :cond_5

    .line 231
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    const/4 v5, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v4, v0, v1, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 225
    .end local v18    # "mtx":Landroid/graphics/Matrix;
    :catch_0
    move-exception v16

    .line 226
    .local v16, "e":Ljava/lang/Exception;
    const-string/jumbo v4, "HSLFCanvasDraw"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 233
    .end local v16    # "e":Ljava/lang/Exception;
    .restart local v18    # "mtx":Landroid/graphics/Matrix;
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v11}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getBGBitMap()Landroid/graphics/Bitmap;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v4, v5, v0, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 238
    .end local v12    # "bitmap":Landroid/graphics/Bitmap;
    .end local v18    # "mtx":Landroid/graphics/Matrix;
    .end local v22    # "scalableBitmap":Landroid/graphics/Bitmap;
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v11}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getBGColor()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_1

    .line 241
    .end local v11    # "bg":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    :cond_7
    instance-of v4, v14, Lcom/samsung/thumbnail/customview/word/Paragraph;

    if-eqz v4, :cond_0

    move-object/from16 v19, v14

    .line 243
    check-cast v19, Lcom/samsung/thumbnail/customview/word/Paragraph;

    .line 244
    .local v19, "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getSlideNumber()I

    move-result v4

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/hslf/CustomDocView;->getId()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 245
    const/16 v20, 0x0

    .line 246
    .local v20, "r":Landroid/graphics/Rect;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 247
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v4

    sget-object v5, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 248
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->y:I

    .line 249
    new-instance v20, Landroid/graphics/Rect;

    .end local v20    # "r":Landroid/graphics/Rect;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginLeft()I

    move-result v4

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginTop()I

    move-result v5

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginRight()I

    move-result v6

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginBottom()I

    move-result v7

    move-object/from16 v0, v20

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 265
    .restart local v20    # "r":Landroid/graphics/Rect;
    :goto_3
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->y:I

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setYValue(I)V

    .line 266
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, v19

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v4, v2}, Lcom/samsung/thumbnail/customview/word/Paragraph;->handleDrawTextPara(Lcom/samsung/thumbnail/customview/word/Paragraph;Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 267
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getYValue()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->y:I

    goto/16 :goto_1

    .line 254
    :cond_8
    new-instance v20, Landroid/graphics/Rect;

    .end local v20    # "r":Landroid/graphics/Rect;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginLeft()I

    move-result v4

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginTop()I

    move-result v5

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginRight()I

    move-result v6

    add-int/lit8 v6, v6, 0x3c

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginBottom()I

    move-result v7

    move-object/from16 v0, v20

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v20    # "r":Landroid/graphics/Rect;
    goto :goto_3

    .line 260
    :cond_9
    new-instance v20, Landroid/graphics/Rect;

    .end local v20    # "r":Landroid/graphics/Rect;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginLeft()I

    move-result v4

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginTop()I

    move-result v5

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginRight()I

    move-result v6

    add-int/lit8 v6, v6, 0x3c

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginBottom()I

    move-result v7

    move-object/from16 v0, v20

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v20    # "r":Landroid/graphics/Rect;
    goto :goto_3

    .line 273
    .end local v13    # "contentSize":I
    .end local v14    # "customContent":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .end local v19    # "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    .end local v20    # "r":Landroid/graphics/Rect;
    .end local v24    # "x1":I
    :cond_a
    return-void
.end method

.method public handleDraw(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Landroid/content/Context;)V
    .locals 32
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "elementCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    .line 71
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v4}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 72
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getCanvasElementsList()Ljava/util/ArrayList;

    move-result-object v22

    .line 74
    .local v22, "customContents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    if-eqz v22, :cond_9

    .line 75
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v19

    .line 76
    .local v19, "contentSize":I
    const/16 v31, 0x0

    .local v31, "x1":I
    :goto_0
    move/from16 v0, v31

    move/from16 v1, v19

    if-ge v0, v1, :cond_9

    .line 77
    move-object/from16 v0, v22

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .line 78
    .local v21, "customContent":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    move-object/from16 v0, v21

    instance-of v3, v0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    if-eqz v3, :cond_1

    move-object/from16 v16, v21

    .line 79
    check-cast v16, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .line 80
    .local v16, "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getLeftCoOrdinate()I

    move-result v4

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getTopCoOrdinate()I

    move-result v5

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v6

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v7

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->drawBitMap(Landroid/graphics/Bitmap;IIII)V

    .line 76
    .end local v16    # "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    :cond_0
    :goto_1
    add-int/lit8 v31, v31, 0x1

    goto :goto_0

    .line 83
    :cond_1
    move-object/from16 v0, v21

    instance-of v3, v0, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;

    if-eqz v3, :cond_2

    move-object/from16 v28, v21

    .line 84
    check-cast v28, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;

    .line 85
    .local v28, "rectParam":Lcom/samsung/thumbnail/customview/hslf/RectangleParam;
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->getTop()I

    move-result v3

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->getLeft()I

    move-result v4

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->getRight()I

    move-result v5

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->getBottom()I

    move-result v6

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/RectangleParam;->getColor()I

    move-result v7

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->drawRect(IIIII)V

    goto :goto_1

    .line 88
    .end local v28    # "rectParam":Lcom/samsung/thumbnail/customview/hslf/RectangleParam;
    :cond_2
    move-object/from16 v0, v21

    instance-of v3, v0, Lcom/samsung/thumbnail/customview/hslf/LineParam;

    if-eqz v3, :cond_3

    move-object/from16 v24, v21

    .line 89
    check-cast v24, Lcom/samsung/thumbnail/customview/hslf/LineParam;

    .line 90
    .local v24, "lineParam":Lcom/samsung/thumbnail/customview/hslf/LineParam;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->getLineColor()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 91
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->getLineWidth()F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 92
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->getStartX()F

    move-result v3

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->getStartY()F

    move-result v4

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->getEndX()F

    move-result v5

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/hslf/LineParam;->getEndY()F

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 95
    .end local v24    # "lineParam":Lcom/samsung/thumbnail/customview/hslf/LineParam;
    :cond_3
    move-object/from16 v0, v21

    instance-of v3, v0, Lcom/samsung/thumbnail/customview/hslf/TextContent;

    if-eqz v3, :cond_4

    move-object/from16 v30, v21

    .line 96
    check-cast v30, Lcom/samsung/thumbnail/customview/hslf/TextContent;

    .line 97
    .local v30, "textContent":Lcom/samsung/thumbnail/customview/hslf/TextContent;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v3}, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->drawText(Lcom/samsung/thumbnail/customview/hslf/TextContent;Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 98
    .end local v30    # "textContent":Lcom/samsung/thumbnail/customview/hslf/TextContent;
    :cond_4
    move-object/from16 v0, v21

    instance-of v3, v0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;

    if-eqz v3, :cond_8

    move-object/from16 v17, v21

    .line 99
    check-cast v17, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;

    .line 100
    .local v17, "bg":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getIsBackgroundImage()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 101
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getBGBitMap()Landroid/graphics/Bitmap;

    move-result-object v18

    .line 102
    .local v18, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v18, :cond_0

    .line 103
    const/16 v29, 0x0

    .line 108
    .local v29, "scalableBitmap":Landroid/graphics/Bitmap;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideWidth()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v4

    double-to-int v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideHeight()D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v4

    double-to-int v4, v4

    const/4 v5, 0x0

    move-object/from16 v0, v18

    invoke-static {v0, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v29

    .line 119
    :goto_2
    new-instance v25, Landroid/graphics/Matrix;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Matrix;-><init>()V

    .line 120
    .local v25, "mtx":Landroid/graphics/Matrix;
    if-eqz v29, :cond_5

    .line 121
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    const/4 v4, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-virtual {v3, v0, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 116
    .end local v25    # "mtx":Landroid/graphics/Matrix;
    :catch_0
    move-exception v23

    .line 117
    .local v23, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "HSLFCanvasDraw"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 123
    .end local v23    # "e":Ljava/lang/Exception;
    .restart local v25    # "mtx":Landroid/graphics/Matrix;
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getBGBitMap()Landroid/graphics/Bitmap;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v3, v4, v0, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 127
    .end local v18    # "bitmap":Landroid/graphics/Bitmap;
    .end local v25    # "mtx":Landroid/graphics/Matrix;
    .end local v29    # "scalableBitmap":Landroid/graphics/Bitmap;
    :cond_6
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getFillStyle()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7

    .line 128
    new-instance v27, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideWidth()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjWidth(D)D

    move-result-wide v6

    double-to-int v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->slideDimention:Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getSlideHeight()D

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getPptObjHeight(D)D

    move-result-wide v6

    double-to-int v6, v6

    move-object/from16 v0, v27

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 135
    .local v27, "r":Landroid/graphics/Rect;
    const-wide v4, 0x4056800000000000L    # 90.0

    move-object/from16 v0, v27

    invoke-static {v0, v4, v5}, Lcom/samsung/thumbnail/util/Utils;->setPolarCoordinateOnRectangle(Landroid/graphics/Rect;D)[F

    move-result-object v20

    .line 138
    .local v20, "coordinates":[F
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getFGColor()I

    move-result v8

    .line 140
    .local v8, "baseColor":I
    new-instance v2, Landroid/graphics/LinearGradient;

    const/4 v3, 0x0

    aget v3, v20, v3

    const/4 v4, 0x1

    aget v4, v20, v4

    const/4 v5, 0x2

    aget v5, v20, v5

    const/4 v6, 0x3

    aget v6, v20, v6

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getBGColor()I

    move-result v7

    sget-object v9, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v2 .. v9}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    .line 145
    .local v2, "linearGradientShader":Landroid/graphics/LinearGradient;
    new-instance v26, Landroid/graphics/Paint;

    invoke-direct/range {v26 .. v26}, Landroid/graphics/Paint;-><init>()V

    .line 146
    .local v26, "paintNew":Landroid/graphics/Paint;
    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 147
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 150
    .end local v2    # "linearGradientShader":Landroid/graphics/LinearGradient;
    .end local v8    # "baseColor":I
    .end local v20    # "coordinates":[F
    .end local v26    # "paintNew":Landroid/graphics/Paint;
    .end local v27    # "r":Landroid/graphics/Rect;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/draw/HSLFCanvasDraw;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->getBGColor()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    goto/16 :goto_1

    .line 153
    .end local v17    # "bg":Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
    :cond_8
    move-object/from16 v0, v21

    instance-of v3, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;

    if-eqz v3, :cond_0

    move-object/from16 v9, v21

    .line 154
    check-cast v9, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .line 155
    .local v9, "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getLeftMargin()F

    move-result v3

    float-to-int v12, v3

    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getTopMargin()F

    move-result v3

    float-to-int v13, v3

    const/4 v14, 0x0

    move-object/from16 v10, p1

    move-object/from16 v11, p3

    move-object/from16 v15, p2

    invoke-virtual/range {v9 .. v15}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawShape(Landroid/graphics/Canvas;Landroid/content/Context;IIZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    goto/16 :goto_1

    .line 161
    .end local v9    # "spCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .end local v19    # "contentSize":I
    .end local v21    # "customContent":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .end local v31    # "x1":I
    :cond_9
    return-void
.end method
