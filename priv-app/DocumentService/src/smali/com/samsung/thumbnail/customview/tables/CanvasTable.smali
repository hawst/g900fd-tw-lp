.class public Lcom/samsung/thumbnail/customview/tables/CanvasTable;
.super Lcom/samsung/thumbnail/customview/CanvasElementPart;
.source "CanvasTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/customview/tables/CanvasTable$1;
    }
.end annotation


# static fields
.field private static final EXCEL_DEFAULT_BORDERCOLOR:I = -0xbbbbbc

.field private static final PATH_STYLE_DASH:F = 4.0f

.field private static final PATH_STYLE_DOT:F = 1.0f

.field private static final PATH_STYLE_GAP_DASH:F = 2.0f

.field private static final PATH_STYLE_GAP_DOT:F = 1.0f


# instance fields
.field private isGridDisplay:Z

.field private isHeightCalculated:Z

.field private mCurrentRow:Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

.field private mMergedCellsHandler:Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;

.field private mRowList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;",
            ">;"
        }
    .end annotation
.end field

.field private mTblWidth:I

.field private mTotalHeight:F

.field private showHeaders:Z

.field private tableNum:I

.field private tableType:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

.field private tableXPos:F

.field private tableYPos:F

.field private tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

.field private xPos:Ljava/lang/String;

.field private yPos:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 98
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mMergedCellsHandler:Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;

    .line 38
    iput v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableXPos:F

    .line 39
    iput v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableYPos:F

    .line 51
    iput-boolean v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->isGridDisplay:Z

    .line 56
    iput-boolean v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->showHeaders:Z

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->isHeightCalculated:Z

    .line 102
    return-void
.end method

.method private displayText(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Landroid/graphics/Canvas;Z)F
    .locals 21
    .param p1, "cellToDisplay"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "isMergedCell"    # Z

    .prologue
    .line 518
    const/high16 v9, -0x40800000    # -1.0f

    .line 519
    .local v9, "height":F
    new-instance v14, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v14, v2}, Landroid/graphics/Paint;-><init>(I)V

    .line 520
    .local v14, "paintnew":Landroid/graphics/Paint;
    const/high16 v2, -0x1000000

    invoke-virtual {v14, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 521
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v14, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 523
    const/4 v11, 0x0

    .line 525
    .local v11, "indentVal":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextIndent()I

    move-result v2

    if-eqz v2, :cond_1

    .line 526
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "left"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "center"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "right"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 532
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextIndent()I

    move-result v2

    mul-int/lit8 v11, v2, 0xa

    .line 539
    :cond_1
    const/16 v19, 0x0

    .line 551
    .local v19, "verticalAlignOffset":F
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellText()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 552
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v3

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v3, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v7

    add-float/2addr v6, v7

    add-float v6, v6, v19

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v6, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 558
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getParagraphList()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 559
    const/4 v10, 0x0

    .line 560
    .local v10, "i":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getParagraphList()Ljava/util/ArrayList;

    move-result-object v16

    .line 562
    .local v16, "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 563
    .local v20, "verticalAlignParaOffset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v12, 0x0

    .line 564
    .local v12, "j":I
    const/4 v15, 0x0

    .line 565
    .local v15, "paraHeight":I
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 567
    .local v17, "paraSize":I
    const/4 v12, 0x0

    :goto_0
    move/from16 v0, v17

    if-ge v12, v0, :cond_3

    .line 568
    move/from16 v0, v19

    float-to-int v2, v0

    add-int/2addr v2, v15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Paragraph;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getTextHeightofPara()I

    move-result v2

    add-int/2addr v15, v2

    .line 567
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 572
    :cond_3
    const/4 v10, 0x0

    :goto_1
    move/from16 v0, v17

    if-ge v10, v0, :cond_7

    .line 573
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/customview/word/Paragraph;

    .line 575
    .local v1, "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v7

    add-float/2addr v6, v7

    float-to-int v6, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v8

    add-float/2addr v7, v8

    float-to-int v7, v7

    invoke-direct {v4, v2, v3, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 582
    .local v4, "r":Landroid/graphics/Rect;
    float-to-int v2, v9

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setYValue(I)V

    .line 583
    invoke-virtual {v1, v11}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setActualTextIndent(I)V

    .line 585
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getWrapContent()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 586
    const/4 v5, 0x0

    iget v2, v4, Landroid/graphics/Rect;->right:I

    iget v3, v4, Landroid/graphics/Rect;->left:I

    sub-int v6, v2, v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v8, 0x0

    move-object v2, v1

    move-object/from16 v3, p2

    invoke-virtual/range {v1 .. v8}, Lcom/samsung/thumbnail/customview/word/Paragraph;->paintText(Lcom/samsung/thumbnail/customview/word/Paragraph;Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Rect;IIZ)V

    .line 612
    :goto_2
    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getYValue()I

    move-result v2

    int-to-float v9, v2

    .line 572
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 590
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v2

    float-to-int v13, v2

    .line 591
    .local v13, "left":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v3

    add-float/2addr v2, v3

    float-to-int v0, v2

    move/from16 v18, v0

    .line 594
    .local v18, "right":I
    if-nez p3, :cond_6

    .line 595
    move-object/from16 v0, p1

    iget v2, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableLeft:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_5

    .line 596
    move-object/from16 v0, p1

    iget v13, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableLeft:I

    .line 599
    :cond_5
    move-object/from16 v0, p1

    iget v2, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableRight:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_6

    .line 600
    move-object/from16 v0, p1

    iget v0, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableRight:I

    move/from16 v18, v0

    .line 604
    :cond_6
    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v6

    add-float/2addr v3, v6

    float-to-int v3, v3

    move/from16 v0, v18

    invoke-direct {v5, v13, v2, v0, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 609
    .local v5, "availRect":Landroid/graphics/Rect;
    const/4 v6, -0x1

    move/from16 v0, v19

    float-to-int v7, v0

    const/4 v8, 0x0

    move-object v2, v1

    move-object/from16 v3, p2

    invoke-virtual/range {v1 .. v8}, Lcom/samsung/thumbnail/customview/word/Paragraph;->paintText(Lcom/samsung/thumbnail/customview/word/Paragraph;Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Rect;IIZ)V

    goto :goto_2

    .line 617
    .end local v1    # "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    .end local v4    # "r":Landroid/graphics/Rect;
    .end local v5    # "availRect":Landroid/graphics/Rect;
    .end local v10    # "i":I
    .end local v12    # "j":I
    .end local v13    # "left":I
    .end local v15    # "paraHeight":I
    .end local v16    # "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    .end local v17    # "paraSize":I
    .end local v18    # "right":I
    .end local v20    # "verticalAlignParaOffset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_7
    return v9
.end method

.method private drawCellFillProperties(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 30
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "tempCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p3, "pathTable"    # Landroid/graphics/Path;
    .param p4, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 1864
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    move-result-object v15

    .line 1865
    .local v15, "fillStyle":Lcom/samsung/thumbnail/customview/tables/CellFillStyle;
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->isGradientFill()Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1867
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getGradientFill()Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    move-result-object v16

    .line 1868
    .local v16, "gradFill":Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->getStopLst()Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v21

    .line 1870
    .local v21, "stopPositions":I
    move/from16 v0, v21

    new-array v10, v0, [F

    .line 1871
    .local v10, "pos":[F
    move/from16 v0, v21

    new-array v9, v0, [I

    .line 1873
    .local v9, "col":[I
    const/16 v17, 0x0

    .line 1874
    .local v17, "i":I
    const/16 v17, 0x0

    :goto_0
    move/from16 v0, v17

    move/from16 v1, v21

    if-ge v0, v1, :cond_0

    .line 1875
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->getStopLst()Ljava/util/ArrayList;

    move-result-object v11

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;->getPosition()D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-float v11, v0

    aput v11, v10, v17

    .line 1876
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v26, "#"

    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->getStopLst()Ljava/util/ArrayList;

    move-result-object v11

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v11

    aput v11, v9, v17

    .line 1874
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 1880
    :cond_0
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->getType()Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    move-result-object v11

    sget-object v26, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->LINEAR:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1882
    new-instance v20, Landroid/graphics/Rect;

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v11

    float-to-int v11, v11

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v26

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v26, v0

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    add-int v27, v27, v28

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v29

    move/from16 v0, v29

    float-to-int v0, v0

    move/from16 v29, v0

    add-int v28, v28, v29

    move-object/from16 v0, v20

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-direct {v0, v11, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1888
    .local v20, "r":Landroid/graphics/Rect;
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->getDegree()D

    move-result-wide v26

    move-object/from16 v0, v20

    move-wide/from16 v1, v26

    invoke-static {v0, v1, v2}, Lcom/samsung/thumbnail/util/Utils;->setPolarCoordinateOnRectangle(Landroid/graphics/Rect;D)[F

    move-result-object v14

    .line 1891
    .local v14, "coordinates":[F
    new-instance v4, Landroid/graphics/LinearGradient;

    const/4 v11, 0x0

    aget v5, v14, v11

    const/4 v11, 0x1

    aget v6, v14, v11

    const/4 v11, 0x2

    aget v7, v14, v11

    const/4 v11, 0x3

    aget v8, v14, v11

    sget-object v11, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v4 .. v11}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 1895
    .local v4, "linearGradientShader":Landroid/graphics/LinearGradient;
    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1943
    .end local v4    # "linearGradientShader":Landroid/graphics/LinearGradient;
    .end local v14    # "coordinates":[F
    .end local v20    # "r":Landroid/graphics/Rect;
    :cond_1
    :goto_1
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1955
    .end local v9    # "col":[I
    .end local v10    # "pos":[F
    .end local v16    # "gradFill":Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;
    .end local v17    # "i":I
    .end local v21    # "stopPositions":I
    :cond_2
    :goto_2
    return-void

    .line 1897
    .restart local v9    # "col":[I
    .restart local v10    # "pos":[F
    .restart local v16    # "gradFill":Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;
    .restart local v17    # "i":I
    .restart local v21    # "stopPositions":I
    :cond_3
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->getType()Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    move-result-object v11

    sget-object v26, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->PATH:Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;

    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$EGradientType;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1900
    const-wide/16 v24, 0x0

    .local v24, "top":D
    const-wide/16 v12, 0x0

    .local v12, "bottom":D
    const-wide/16 v22, 0x0

    .local v22, "right":D
    const-wide/16 v18, 0x0

    .line 1902
    .local v18, "left":D
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->getTop()D

    move-result-wide v24

    .line 1903
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->getBottom()D

    move-result-wide v12

    .line 1904
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->getRight()D

    move-result-wide v22

    .line 1905
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->getLeft()D

    move-result-wide v18

    .line 1907
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v11

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v26

    mul-float v11, v11, v26

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v26

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v27

    mul-float v26, v26, v27

    add-float v11, v11, v26

    float-to-double v0, v11

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-float v8, v0

    .line 1913
    .local v8, "radius":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v6

    .line 1914
    .local v6, "x":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v7

    .line 1916
    .local v7, "y":F
    const-wide/16 v26, 0x0

    cmpl-double v11, v24, v26

    if-nez v11, :cond_5

    const-wide/16 v26, 0x0

    cmpl-double v11, v12, v26

    if-nez v11, :cond_5

    .line 1917
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v6

    .line 1918
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v7

    .line 1938
    :cond_4
    :goto_3
    new-instance v5, Landroid/graphics/RadialGradient;

    sget-object v11, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v5 .. v11}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 1940
    .local v5, "radialGradientShader":Landroid/graphics/RadialGradient;
    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto :goto_1

    .line 1919
    .end local v5    # "radialGradientShader":Landroid/graphics/RadialGradient;
    :cond_5
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    cmpl-double v11, v24, v26

    if-nez v11, :cond_6

    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    cmpl-double v11, v12, v26

    if-nez v11, :cond_6

    .line 1920
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v6

    .line 1921
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v11

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v26

    add-float v7, v11, v26

    goto :goto_3

    .line 1922
    :cond_6
    const-wide/high16 v26, 0x3fe0000000000000L    # 0.5

    cmpl-double v11, v24, v26

    if-nez v11, :cond_7

    const-wide/high16 v26, 0x3fe0000000000000L    # 0.5

    cmpl-double v11, v12, v26

    if-nez v11, :cond_7

    const-wide/high16 v26, 0x3fe0000000000000L    # 0.5

    cmpl-double v11, v22, v26

    if-nez v11, :cond_7

    const-wide/high16 v26, 0x3fe0000000000000L    # 0.5

    cmpl-double v11, v18, v26

    if-nez v11, :cond_7

    .line 1924
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v11

    const/high16 v26, 0x40000000    # 2.0f

    div-float v11, v11, v26

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v26

    mul-float v11, v11, v26

    const/high16 v26, 0x40000000    # 2.0f

    div-float v11, v11, v26

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v26

    const/high16 v27, 0x40000000    # 2.0f

    div-float v26, v26, v27

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v27

    mul-float v26, v26, v27

    const/high16 v27, 0x40000000    # 2.0f

    div-float v26, v26, v27

    add-float v11, v11, v26

    float-to-double v0, v11

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-float v8, v0

    .line 1928
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v11

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v26

    const/high16 v27, 0x40000000    # 2.0f

    div-float v26, v26, v27

    add-float v6, v11, v26

    .line 1929
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v11

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v26

    const/high16 v27, 0x40000000    # 2.0f

    div-float v26, v26, v27

    add-float v7, v11, v26

    goto/16 :goto_3

    .line 1930
    :cond_7
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    cmpl-double v11, v24, v26

    if-nez v11, :cond_8

    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    cmpl-double v11, v12, v26

    if-nez v11, :cond_8

    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    cmpl-double v11, v22, v26

    if-nez v11, :cond_8

    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    cmpl-double v11, v18, v26

    if-nez v11, :cond_8

    .line 1931
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v11

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v26

    add-float v6, v11, v26

    .line 1932
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v11

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v26

    add-float v7, v11, v26

    goto/16 :goto_3

    .line 1933
    :cond_8
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    cmpl-double v11, v22, v26

    if-nez v11, :cond_4

    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    cmpl-double v11, v18, v26

    if-nez v11, :cond_4

    .line 1934
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v11

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v26

    add-float v6, v11, v26

    .line 1935
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v7

    goto/16 :goto_3

    .line 1946
    .end local v6    # "x":F
    .end local v7    # "y":F
    .end local v8    # "radius":F
    .end local v9    # "col":[I
    .end local v10    # "pos":[F
    .end local v12    # "bottom":D
    .end local v16    # "gradFill":Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;
    .end local v17    # "i":I
    .end local v18    # "left":D
    .end local v21    # "stopPositions":I
    .end local v22    # "right":D
    .end local v24    # "top":D
    :cond_9
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v11

    if-eqz v11, :cond_2

    .line 1948
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v11

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v26

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v27

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v28

    move-object/from16 v0, p4

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v0, v11, v1, v2, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1952
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_2
.end method

.method private drawCellParagraph(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 23
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 1496
    const/16 v22, 0x0

    .line 1497
    .local v22, "y":I
    const/16 v21, 0x0

    .line 1499
    .local v21, "x":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 1500
    .local v20, "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellList()Ljava/util/ArrayList;

    move-result-object v19

    .line 1501
    .local v19, "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 1502
    .local v18, "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellMergeValue()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 1505
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getDocParaList()Ljava/util/ArrayList;

    move-result-object v14

    .line 1507
    .local v14, "docParaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v3

    float-to-int v0, v3

    move/from16 v22, v0

    .line 1508
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v3

    float-to-int v0, v3

    move/from16 v21, v0

    .line 1510
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v15, v3, :cond_1

    .line 1511
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 1512
    .local v2, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->TABLE:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParentType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;)V

    .line 1513
    move/from16 v0, v22

    int-to-float v3, v0

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTopSpacing()F

    move-result v4

    add-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 v22, v0

    .line 1515
    move/from16 v0, v22

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    .line 1516
    move/from16 v0, v21

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 1517
    new-instance v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;

    invoke-direct {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;-><init>()V

    .line 1519
    .local v1, "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "normal"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1522
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v7

    float-to-int v6, v7

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual/range {v1 .. v7}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I

    .line 1525
    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v7

    add-float/2addr v5, v7

    float-to-int v5, v5

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v7

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v8

    add-float/2addr v7, v8

    float-to-int v7, v7

    invoke-direct {v6, v3, v4, v5, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1532
    .local v6, "rect":Landroid/graphics/Rect;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v3

    float-to-int v7, v3

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v3

    float-to-int v8, v3

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v10

    const/4 v11, 0x0

    move-object v3, v2

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v12, p3

    invoke-virtual/range {v2 .. v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1589
    .end local v6    # "rect":Landroid/graphics/Rect;
    :cond_2
    :goto_1
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getY()I

    move-result v22

    .line 1590
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-eq v15, v3, :cond_3

    .line 1591
    move/from16 v0, v22

    int-to-float v3, v0

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBottomSpacing()F

    move-result v4

    add-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 v22, v0

    .line 1510
    :cond_3
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_0

    .line 1538
    :cond_4
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "btLr"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1541
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, -0x1

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v12, v3

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v13, v3

    move-object v7, v1

    move-object v8, v2

    invoke-virtual/range {v7 .. v13}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I

    .line 1544
    const/high16 v3, -0x3d4c0000    # -90.0f

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1546
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1547
    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v7

    add-float/2addr v5, v7

    float-to-int v5, v5

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v7

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v8

    add-float/2addr v7, v8

    float-to-int v7, v7

    invoke-direct {v6, v3, v4, v5, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1554
    .restart local v6    # "rect":Landroid/graphics/Rect;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v7, v3

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v8, v3

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v10

    const/4 v11, 0x0

    move-object v3, v2

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v12, p3

    invoke-virtual/range {v2 .. v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1559
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1560
    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    goto/16 :goto_1

    .line 1563
    .end local v6    # "rect":Landroid/graphics/Rect;
    :cond_5
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "tbRl"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1566
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, -0x1

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v12, v3

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v13, v3

    move-object v7, v1

    move-object v8, v2

    invoke-virtual/range {v7 .. v13}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I

    .line 1569
    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1571
    const/4 v3, 0x0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v4

    neg-float v4, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1572
    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v7

    add-float/2addr v5, v7

    float-to-int v5, v5

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v7

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v8

    add-float/2addr v7, v8

    float-to-int v7, v7

    invoke-direct {v6, v3, v4, v5, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1579
    .restart local v6    # "rect":Landroid/graphics/Rect;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v7, v3

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v8, v3

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v10

    const/4 v11, 0x0

    move-object v3, v2

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v12, p3

    invoke-virtual/range {v2 .. v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1584
    const/4 v3, 0x0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1585
    const/high16 v3, -0x3d4c0000    # -90.0f

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    goto/16 :goto_1

    .line 1595
    .end local v1    # "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    .end local v2    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v6    # "rect":Landroid/graphics/Rect;
    .end local v14    # "docParaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    .end local v15    # "i":I
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v18    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v19    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v20    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    :cond_6
    return-void
.end method

.method private drawCellParagraph(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 18
    .param p1, "tempCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 1600
    const/16 v17, 0x0

    .line 1601
    .local v17, "y":I
    const/16 v16, 0x0

    .line 1603
    .local v16, "x":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getDocParaList()Ljava/util/ArrayList;

    move-result-object v14

    .line 1605
    .local v14, "docParaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v3

    float-to-int v0, v3

    move/from16 v17, v0

    .line 1606
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v3

    float-to-int v0, v3

    move/from16 v16, v0

    .line 1608
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v15, v3, :cond_4

    .line 1609
    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 1610
    .local v2, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->TABLE:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParentType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;)V

    .line 1611
    move/from16 v0, v17

    int-to-float v3, v0

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTopSpacing()F

    move-result v4

    add-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 v17, v0

    .line 1613
    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    .line 1614
    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 1615
    new-instance v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;

    invoke-direct {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;-><init>()V

    .line 1617
    .local v1, "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "normal"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1619
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v7

    float-to-int v6, v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual/range {v1 .. v7}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I

    .line 1622
    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v7

    add-float/2addr v5, v7

    float-to-int v5, v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v8

    add-float/2addr v7, v8

    float-to-int v7, v7

    invoke-direct {v6, v3, v4, v5, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1627
    .local v6, "rect":Landroid/graphics/Rect;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v3

    float-to-int v7, v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v3

    float-to-int v8, v3

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v10

    const/4 v11, 0x0

    move-object v3, v2

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v12, p4

    invoke-virtual/range {v2 .. v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1672
    .end local v6    # "rect":Landroid/graphics/Rect;
    :cond_0
    :goto_1
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getY()I

    move-result v17

    .line 1673
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-eq v15, v3, :cond_1

    .line 1674
    move/from16 v0, v17

    int-to-float v3, v0

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBottomSpacing()F

    move-result v4

    add-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 v17, v0

    .line 1608
    :cond_1
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_0

    .line 1631
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "btLr"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1632
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, -0x1

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v12, v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v13, v3

    move-object v7, v1

    move-object v8, v2

    invoke-virtual/range {v7 .. v13}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I

    .line 1635
    const/high16 v3, -0x3d4c0000    # -90.0f

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1637
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1638
    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v7

    add-float/2addr v5, v7

    float-to-int v5, v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v8

    add-float/2addr v7, v8

    float-to-int v7, v7

    invoke-direct {v6, v3, v4, v5, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1643
    .restart local v6    # "rect":Landroid/graphics/Rect;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v7, v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v8, v3

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v10

    const/4 v11, 0x0

    move-object v3, v2

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v12, p4

    invoke-virtual/range {v2 .. v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1648
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1649
    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    goto/16 :goto_1

    .line 1651
    .end local v6    # "rect":Landroid/graphics/Rect;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "tbRl"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1652
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, -0x1

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v12, v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v13, v3

    move-object v7, v1

    move-object v8, v2

    invoke-virtual/range {v7 .. v13}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I

    .line 1655
    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1657
    const/4 v3, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v4

    neg-float v4, v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1658
    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v7

    add-float/2addr v5, v7

    float-to-int v5, v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v8

    add-float/2addr v7, v8

    float-to-int v7, v7

    invoke-direct {v6, v3, v4, v5, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1663
    .restart local v6    # "rect":Landroid/graphics/Rect;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v7, v3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v3

    float-to-int v8, v3

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v10

    const/4 v11, 0x0

    move-object v3, v2

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v12, p4

    invoke-virtual/range {v2 .. v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1667
    const/4 v3, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1668
    const/high16 v3, -0x3d4c0000    # -90.0f

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    goto/16 :goto_1

    .line 1676
    .end local v1    # "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    .end local v2    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v6    # "rect":Landroid/graphics/Rect;
    :cond_4
    return-void
.end method

.method private drawLeftRightBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "height"    # F
    .param p5, "border"    # Lcom/samsung/thumbnail/customview/tables/TableBorder;

    .prologue
    const/4 v9, 0x6

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 821
    new-instance v5, Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-direct {v5, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 822
    .local v5, "paint":Landroid/graphics/Paint;
    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 823
    sget-object v0, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 826
    if-nez p5, :cond_1

    .line 828
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->isGridDisplay:Z

    if-eqz v0, :cond_0

    .line 829
    const v0, -0xbbbbbc

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 830
    const v0, 0x3dcccccd    # 0.1f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 831
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 918
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 836
    :cond_1
    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v0

    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v1

    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v2

    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v3

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 840
    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v0

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_2

    .line 841
    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 844
    :cond_2
    if-eqz p5, :cond_3

    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v0, v1, :cond_3

    .line 845
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 848
    :cond_3
    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 849
    sget-object v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XDocBorder$EBorderStyle:[I

    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 914
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 851
    :pswitch_1
    sub-float v1, p2, v6

    sub-float v3, p2, v6

    add-float v4, p3, p4

    move-object v0, p1

    move v2, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 852
    add-float v1, p2, v6

    add-float v3, p2, v6

    add-float v4, p3, p4

    move-object v0, p1

    move v2, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 858
    :pswitch_2
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 861
    :pswitch_3
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 864
    :pswitch_4
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v7, [F

    fill-array-data v1, :array_0

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 866
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 869
    :pswitch_5
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 872
    :pswitch_6
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v7, [F

    fill-array-data v1, :array_1

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 874
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 877
    :pswitch_7
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 880
    :pswitch_8
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 883
    :pswitch_9
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v7, [F

    fill-array-data v1, :array_2

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 885
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 888
    :pswitch_a
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v8, [F

    fill-array-data v1, :array_3

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 891
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 894
    :pswitch_b
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v9, [F

    fill-array-data v1, :array_4

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 898
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 901
    :pswitch_c
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v8, [F

    fill-array-data v1, :array_5

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 904
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 907
    :pswitch_d
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v9, [F

    fill-array-data v1, :array_6

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 911
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 849
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 864
    :array_0
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
    .end array-data

    .line 872
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 883
    :array_2
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
    .end array-data

    .line 888
    :array_3
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data

    .line 894
    :array_4
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data

    .line 901
    :array_5
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data

    .line 907
    :array_6
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data
.end method

.method private drawLeftRightBorderForWord(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;Landroid/graphics/Paint;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "height"    # F
    .param p5, "border"    # Lcom/samsung/thumbnail/customview/tables/TableBorder;
    .param p6, "paint"    # Landroid/graphics/Paint;

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 940
    if-nez p5, :cond_1

    .line 1014
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 944
    :cond_1
    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 945
    sget-object v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XDocBorder$EBorderStyle:[I

    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1010
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 947
    :pswitch_1
    sub-float v1, p2, v6

    sub-float v3, p2, v6

    add-float v4, p3, p4

    move-object v0, p1

    move v2, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 948
    add-float v1, p2, v6

    add-float v3, p2, v6

    add-float v4, p3, p4

    move-object v0, p1

    move v2, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 954
    :pswitch_2
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 957
    :pswitch_3
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 960
    :pswitch_4
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v3, [F

    fill-array-data v1, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 962
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 965
    :pswitch_5
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 968
    :pswitch_6
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v3, [F

    fill-array-data v1, :array_1

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 970
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 973
    :pswitch_7
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 976
    :pswitch_8
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 979
    :pswitch_9
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v3, [F

    fill-array-data v1, :array_2

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 981
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 984
    :pswitch_a
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v4, [F

    fill-array-data v1, :array_3

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 987
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 990
    :pswitch_b
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v5, [F

    fill-array-data v1, :array_4

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 994
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 997
    :pswitch_c
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v4, [F

    fill-array-data v1, :array_5

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1000
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 1003
    :pswitch_d
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v5, [F

    fill-array-data v1, :array_6

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1007
    add-float v4, p3, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 945
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 960
    :array_0
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
    .end array-data

    .line 968
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 979
    :array_2
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
    .end array-data

    .line 984
    :array_3
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data

    .line 990
    :array_4
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data

    .line 997
    :array_5
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data

    .line 1003
    :array_6
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data
.end method

.method private drawMergedCells(Landroid/graphics/Canvas;)V
    .locals 34
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1679
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mMergedCellsHandler:Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->getMergedCellList()Ljava/util/ArrayList;

    move-result-object v33

    .line 1682
    .local v33, "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    if-nez v33, :cond_1

    .line 1779
    :cond_0
    return-void

    .line 1686
    :cond_1
    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v29

    .local v29, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 1689
    .local v32, "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellMergeValue()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_2

    .line 1692
    new-instance v31, Landroid/graphics/Path;

    invoke-direct/range {v31 .. v31}, Landroid/graphics/Path;-><init>()V

    .line 1693
    .local v31, "pathTable":Landroid/graphics/Path;
    new-instance v30, Landroid/graphics/Paint;

    const/4 v5, 0x1

    move-object/from16 v0, v30

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    .line 1694
    .local v30, "paintnew":Landroid/graphics/Paint;
    const/4 v5, 0x0

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1696
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v6

    move-object/from16 v0, v31

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1697
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v6

    move-object/from16 v0, v31

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1699
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v6

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v7

    add-float/2addr v6, v7

    move-object/from16 v0, v31

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1701
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v6

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v7

    add-float/2addr v6, v7

    move-object/from16 v0, v31

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1703
    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Path;->close()V

    .line 1706
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v32

    move-object/from16 v3, v31

    move-object/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawCellFillProperties(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1712
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v22

    .line 1713
    .local v22, "left":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v28

    .line 1714
    .local v28, "right":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v16

    .line 1715
    .local v16, "bottom":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v10

    .line 1730
    .local v10, "top":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getGridDisplay()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1731
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v7

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v8

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v9

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-direct/range {v5 .. v10}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawTopBottomBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1734
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v13

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v6

    add-float v14, v5, v6

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v15

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    invoke-direct/range {v11 .. v16}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawTopBottomBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1738
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v19

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v20

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v21

    move-object/from16 v17, p0

    move-object/from16 v18, p1

    invoke-direct/range {v17 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawLeftRightBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1741
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v6

    add-float v25, v5, v6

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v26

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v27

    move-object/from16 v23, p0

    move-object/from16 v24, p1

    invoke-direct/range {v23 .. v28}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawLeftRightBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1776
    :cond_3
    :goto_1
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->displayText(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Landroid/graphics/Canvas;Z)F

    goto/16 :goto_0

    .line 1746
    :cond_4
    if-eqz v22, :cond_5

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->name()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "SINGLE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 1748
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v19

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v20

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v21

    move-object/from16 v17, p0

    move-object/from16 v18, p1

    invoke-direct/range {v17 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawLeftRightBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1752
    :cond_5
    if-eqz v28, :cond_6

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->name()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "SINGLE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 1754
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v6

    add-float v25, v5, v6

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v26

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v27

    move-object/from16 v23, p0

    move-object/from16 v24, p1

    invoke-direct/range {v23 .. v28}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawLeftRightBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1758
    :cond_6
    if-eqz v10, :cond_7

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->name()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "SINGLE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1760
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v7

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v8

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v9

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-direct/range {v5 .. v10}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawTopBottomBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1765
    :cond_7
    if-eqz v16, :cond_3

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->name()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "SINGLE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1767
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v13

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v6

    add-float v14, v5, v6

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v15

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    invoke-direct/range {v11 .. v16}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawTopBottomBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    goto/16 :goto_1
.end method

.method private drawMergedCellsForWord(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 35
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 1783
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mMergedCellsHandler:Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->getMergedCellList()Ljava/util/ArrayList;

    move-result-object v34

    .line 1786
    .local v34, "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    if-nez v34, :cond_1

    .line 1860
    :cond_0
    return-void

    .line 1790
    :cond_1
    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v30

    .local v30, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 1793
    .local v33, "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellMergeValue()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_2

    .line 1796
    new-instance v32, Landroid/graphics/Path;

    invoke-direct/range {v32 .. v32}, Landroid/graphics/Path;-><init>()V

    .line 1797
    .local v32, "pathTable":Landroid/graphics/Path;
    new-instance v31, Landroid/graphics/Paint;

    const/4 v5, 0x1

    move-object/from16 v0, v31

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    .line 1798
    .local v31, "paintnew":Landroid/graphics/Paint;
    const/4 v5, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1800
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v6

    move-object/from16 v0, v32

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1801
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v6

    move-object/from16 v0, v32

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1803
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v6

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v7

    add-float/2addr v6, v7

    move-object/from16 v0, v32

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1805
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v6

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v7

    add-float/2addr v6, v7

    move-object/from16 v0, v32

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1807
    invoke-virtual/range {v32 .. v32}, Landroid/graphics/Path;->close()V

    .line 1810
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    move-result-object v29

    .line 1811
    .local v29, "fillStyle":Lcom/samsung/thumbnail/customview/tables/CellFillStyle;
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 1812
    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1814
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v5

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v6

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v7

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v8

    move-object/from16 v0, v31

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1818
    move-object/from16 v0, p1

    move-object/from16 v1, v32

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1825
    :cond_3
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v22

    .line 1826
    .local v22, "left":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v28

    .line 1827
    .local v28, "right":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v16

    .line 1828
    .local v16, "bottom":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v10

    .line 1830
    .local v10, "top":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    if-nez v22, :cond_4

    if-nez v28, :cond_4

    if-nez v10, :cond_4

    if-nez v16, :cond_4

    .line 1831
    new-instance v31, Landroid/graphics/Paint;

    .end local v31    # "paintnew":Landroid/graphics/Paint;
    const/4 v5, 0x1

    move-object/from16 v0, v31

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    .line 1835
    .restart local v31    # "paintnew":Landroid/graphics/Paint;
    const v5, -0xbbbbbc

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 1836
    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1837
    const/4 v5, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1838
    move-object/from16 v0, p1

    move-object/from16 v1, v32

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1858
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawCellParagraph(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    goto/16 :goto_0

    .line 1841
    :cond_4
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v7

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v8

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v9

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-direct/range {v5 .. v10}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawTopBottomBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1844
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v13

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v6

    add-float v14, v5, v6

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v15

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    invoke-direct/range {v11 .. v16}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawTopBottomBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1848
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v19

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v20

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v21

    move-object/from16 v17, p0

    move-object/from16 v18, p1

    invoke-direct/range {v17 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawLeftRightBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1851
    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v6

    add-float v25, v5, v6

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v26

    invoke-virtual/range {v33 .. v33}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v27

    move-object/from16 v23, p0

    move-object/from16 v24, p1

    invoke-direct/range {v23 .. v28}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawLeftRightBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    goto :goto_1
.end method

.method private drawNormalCells(Landroid/graphics/Canvas;)V
    .locals 41
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1018
    const/16 v32, 0x0

    .line 1019
    .local v32, "currentCellWithText":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    const/16 v29, -0x1

    .line 1020
    .local v29, "availableLeft":I
    const/16 v30, -0x1

    .line 1021
    .local v30, "availableRight":I
    const/16 v31, 0x0

    .line 1022
    .local v31, "cellCount":I
    const/16 v37, 0x0

    .line 1024
    .local v37, "rowCount":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v33

    :cond_0
    :goto_0
    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_11

    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 1025
    .local v40, "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellList()Ljava/util/ArrayList;

    move-result-object v39

    .line 1027
    .local v39, "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    add-int/lit8 v37, v37, 0x1

    .line 1028
    const/16 v29, -0x1

    .line 1029
    const/16 v30, -0x1

    .line 1030
    const/16 v31, 0x0

    .line 1032
    invoke-virtual/range {v39 .. v39}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v34

    .local v34, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_10

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 1035
    .local v38, "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    add-int/lit8 v31, v31, 0x1

    .line 1036
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellMergeValue()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 1038
    if-eqz v32, :cond_2

    .line 1039
    move/from16 v0, v29

    move-object/from16 v1, v32

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableLeft:I

    .line 1040
    move/from16 v0, v30

    move-object/from16 v1, v32

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableRight:I

    .line 1041
    const/16 v32, 0x0

    .line 1043
    :cond_2
    const/16 v29, -0x1

    .line 1044
    const/16 v30, -0x1

    .line 1045
    goto :goto_1

    .line 1048
    :cond_3
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHideCell()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1049
    if-eqz v32, :cond_4

    .line 1050
    move/from16 v0, v29

    move-object/from16 v1, v32

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableLeft:I

    .line 1051
    move/from16 v0, v30

    move-object/from16 v1, v32

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableRight:I

    .line 1052
    const/16 v32, 0x0

    .line 1054
    :cond_4
    const/16 v29, -0x1

    .line 1055
    const/16 v30, -0x1

    .line 1056
    goto :goto_1

    .line 1059
    :cond_5
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->isEmptyText()Z

    move-result v5

    if-nez v5, :cond_7

    .line 1060
    if-eqz v32, :cond_6

    .line 1061
    move/from16 v0, v29

    move-object/from16 v1, v32

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableLeft:I

    .line 1062
    move/from16 v0, v30

    move-object/from16 v1, v32

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableRight:I

    .line 1063
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v6

    add-float/2addr v5, v6

    float-to-int v0, v5

    move/from16 v29, v0

    .line 1068
    :cond_6
    move-object/from16 v32, v38

    .line 1071
    :cond_7
    const/4 v5, -0x1

    move/from16 v0, v29

    if-ne v0, v5, :cond_8

    .line 1072
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    float-to-int v0, v5

    move/from16 v29, v0

    .line 1073
    move/from16 v30, v29

    .line 1075
    :cond_8
    const/4 v5, -0x1

    move/from16 v0, v30

    if-eq v0, v5, :cond_9

    .line 1076
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v5

    float-to-int v5, v5

    add-int v30, v30, v5

    .line 1079
    :cond_9
    new-instance v36, Landroid/graphics/Path;

    invoke-direct/range {v36 .. v36}, Landroid/graphics/Path;-><init>()V

    .line 1080
    .local v36, "pathTable":Landroid/graphics/Path;
    new-instance v35, Landroid/graphics/Paint;

    const/4 v5, 0x1

    move-object/from16 v0, v35

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    .line 1081
    .local v35, "paintNew":Landroid/graphics/Paint;
    const/4 v5, 0x0

    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1082
    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1085
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v6

    move-object/from16 v0, v36

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1086
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v6

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v7

    add-float/2addr v6, v7

    move-object/from16 v0, v36

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1088
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v6

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v7

    add-float/2addr v6, v7

    move-object/from16 v0, v36

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1090
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v6

    move-object/from16 v0, v36

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1092
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v6

    move-object/from16 v0, v36

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1093
    invoke-virtual/range {v36 .. v36}, Landroid/graphics/Path;->close()V

    .line 1096
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v38

    move-object/from16 v3, v36

    move-object/from16 v4, v35

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawCellFillProperties(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1098
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v22

    .line 1099
    .local v22, "left":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v28

    .line 1100
    .local v28, "right":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v16

    .line 1101
    .local v16, "bottom":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v10

    .line 1114
    .local v10, "top":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {v40 .. v40}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getGridDisplay()Z

    move-result v5

    if-nez v5, :cond_a

    const/4 v5, 0x1

    move/from16 v0, v37

    if-eq v0, v5, :cond_a

    const/4 v5, 0x1

    move/from16 v0, v31

    if-ne v0, v5, :cond_c

    .line 1116
    :cond_a
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v7

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v8

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v9

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-direct/range {v5 .. v10}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawTopBottomBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1120
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v13

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v6

    add-float v14, v5, v6

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v15

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    invoke-direct/range {v11 .. v16}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawTopBottomBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1124
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->drawLeftBorder()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1125
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v19

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v20

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v21

    move-object/from16 v17, p0

    move-object/from16 v18, p1

    invoke-direct/range {v17 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawLeftRightBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1130
    :cond_b
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->drawRightBorder()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1131
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v6

    add-float v25, v5, v6

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v26

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v27

    move-object/from16 v23, p0

    move-object/from16 v24, p1

    invoke-direct/range {v23 .. v28}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawLeftRightBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    goto/16 :goto_1

    .line 1136
    :cond_c
    const/4 v5, 0x1

    move/from16 v0, v31

    if-eq v0, v5, :cond_1

    const/4 v5, 0x1

    move/from16 v0, v37

    if-le v0, v5, :cond_1

    .line 1137
    if-eqz v22, :cond_d

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->name()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "SINGLE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 1140
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v19

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v20

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v21

    move-object/from16 v17, p0

    move-object/from16 v18, p1

    invoke-direct/range {v17 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawLeftRightBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1144
    :cond_d
    if-eqz v28, :cond_e

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->name()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "SINGLE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 1147
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v5

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v6

    add-float v25, v5, v6

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v26

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v27

    move-object/from16 v23, p0

    move-object/from16 v24, p1

    invoke-direct/range {v23 .. v28}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawLeftRightBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1152
    :cond_e
    if-eqz v10, :cond_f

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->name()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "SINGLE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 1155
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v7

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v8

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v9

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    invoke-direct/range {v5 .. v10}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawTopBottomBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1160
    :cond_f
    if-eqz v16, :cond_1

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->name()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "SINGLE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1163
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v13

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v6

    add-float v14, v5, v6

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v15

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    invoke-direct/range {v11 .. v16}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawTopBottomBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    goto/16 :goto_1

    .line 1174
    .end local v10    # "top":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    .end local v16    # "bottom":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    .end local v22    # "left":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    .end local v28    # "right":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    .end local v35    # "paintNew":Landroid/graphics/Paint;
    .end local v36    # "pathTable":Landroid/graphics/Path;
    .end local v38    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    :cond_10
    if-eqz v32, :cond_0

    .line 1175
    move/from16 v0, v29

    move-object/from16 v1, v32

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableLeft:I

    .line 1176
    move/from16 v0, v30

    move-object/from16 v1, v32

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableRight:I

    .line 1177
    const/16 v32, 0x0

    goto/16 :goto_0

    .line 1180
    .end local v34    # "i$":Ljava/util/Iterator;
    .end local v39    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v40    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    :cond_11
    return-void
.end method

.method private drawNormalCellsForWord(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 44
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 1185
    const/16 v38, 0x0

    .line 1186
    .local v38, "currentCellWithText":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    const/16 v36, -0x1

    .line 1187
    .local v36, "availableLeft":I
    const/16 v37, -0x1

    .line 1190
    .local v37, "availableRight":I
    const/16 v29, 0x0

    .local v29, "rowNum":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v29

    if-ge v0, v4, :cond_19

    .line 1191
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    move/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 1192
    .local v43, "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    invoke-virtual/range {v43 .. v43}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellList()Ljava/util/ArrayList;

    move-result-object v35

    .line 1194
    .local v35, "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    const/16 v36, -0x1

    .line 1195
    const/16 v37, -0x1

    .line 1198
    const/16 v30, 0x0

    .local v30, "colNum":I
    :goto_1
    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v30

    if-ge v0, v4, :cond_17

    .line 1200
    move-object/from16 v0, v35

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 1203
    .local v42, "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellMergeValue()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1205
    if-eqz v38, :cond_0

    .line 1206
    move/from16 v0, v36

    move-object/from16 v1, v38

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableLeft:I

    .line 1207
    move/from16 v0, v37

    move-object/from16 v1, v38

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableRight:I

    .line 1208
    const/16 v38, 0x0

    .line 1210
    :cond_0
    const/16 v36, -0x1

    .line 1211
    const/16 v37, -0x1

    .line 1198
    :cond_1
    :goto_2
    add-int/lit8 v30, v30, 0x1

    goto :goto_1

    .line 1215
    :cond_2
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHideCell()Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    .line 1216
    if-eqz v38, :cond_3

    .line 1217
    move/from16 v0, v36

    move-object/from16 v1, v38

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableLeft:I

    .line 1218
    move/from16 v0, v37

    move-object/from16 v1, v38

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableRight:I

    .line 1219
    const/16 v38, 0x0

    .line 1221
    :cond_3
    const/16 v36, -0x1

    .line 1222
    const/16 v37, -0x1

    .line 1223
    goto :goto_2

    .line 1226
    :cond_4
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->isEmptyText()Z

    move-result v4

    if-nez v4, :cond_6

    .line 1227
    if-eqz v38, :cond_5

    .line 1228
    move/from16 v0, v36

    move-object/from16 v1, v38

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableLeft:I

    .line 1229
    move/from16 v0, v37

    move-object/from16 v1, v38

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableRight:I

    .line 1230
    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    invoke-virtual/range {v38 .. v38}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v36, v0

    .line 1235
    :cond_5
    move-object/from16 v38, v42

    .line 1238
    :cond_6
    const/4 v4, -0x1

    move/from16 v0, v36

    if-ne v0, v4, :cond_7

    .line 1239
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    float-to-int v0, v4

    move/from16 v36, v0

    .line 1240
    move/from16 v37, v36

    .line 1242
    :cond_7
    const/4 v4, -0x1

    move/from16 v0, v37

    if-eq v0, v4, :cond_8

    .line 1243
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v4

    float-to-int v4, v4

    add-int v37, v37, v4

    .line 1246
    :cond_8
    new-instance v41, Landroid/graphics/Path;

    invoke-direct/range {v41 .. v41}, Landroid/graphics/Path;-><init>()V

    .line 1247
    .local v41, "pathTable":Landroid/graphics/Path;
    new-instance v40, Landroid/graphics/Paint;

    const/4 v4, 0x1

    move-object/from16 v0, v40

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    .line 1248
    .local v40, "paintNew":Landroid/graphics/Paint;
    const/4 v4, 0x0

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1249
    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1252
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v4

    if-eqz v4, :cond_9

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/word/Page;->isCoverPage()Z

    move-result v4

    if-nez v4, :cond_9

    .line 1254
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    move-object/from16 v0, v41

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1255
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v6

    add-float/2addr v5, v6

    move-object/from16 v0, v41

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1257
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v6

    add-float/2addr v5, v6

    move-object/from16 v0, v41

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1260
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    move-object/from16 v0, v41

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1263
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    move-object/from16 v0, v41

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1264
    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Path;->close()V

    .line 1267
    :cond_9
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    move-result-object v39

    .line 1268
    .local v39, "fillStyle":Lcom/samsung/thumbnail/customview/tables/CellFillStyle;
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 1269
    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v4

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v5

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    invoke-virtual/range {v39 .. v39}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v7

    move-object/from16 v0, v40

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1276
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    move-object/from16 v0, v41

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1277
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v6

    add-float/2addr v5, v6

    move-object/from16 v0, v41

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1279
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v6

    add-float/2addr v5, v6

    move-object/from16 v0, v41

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1282
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    move-object/from16 v0, v41

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1285
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    move-object/from16 v0, v41

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1286
    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Path;->close()V

    .line 1288
    move-object/from16 v0, p1

    move-object/from16 v1, v41

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1291
    :cond_a
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v21

    .line 1292
    .local v21, "left":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v27

    .line 1293
    .local v27, "right":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v15

    .line 1294
    .local v15, "bottom":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v9

    .line 1347
    .local v9, "top":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v4

    if-eqz v4, :cond_e

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getRecentPage()Lcom/samsung/thumbnail/office/word/Page;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/word/Page;->isCoverPage()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1348
    if-eqz v9, :cond_b

    .line 1349
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v6

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v7

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v8

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-direct/range {v4 .. v9}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawTopBottomBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1353
    :cond_b
    if-eqz v15, :cond_c

    .line 1354
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v12

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v4

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v5

    add-float v13, v4, v5

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v14

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    invoke-direct/range {v10 .. v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawTopBottomBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1361
    :cond_c
    if-eqz v21, :cond_d

    .line 1362
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v18

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v19

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v20

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    invoke-direct/range {v16 .. v21}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawLeftRightBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    .line 1366
    :cond_d
    if-eqz v27, :cond_1

    .line 1367
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v5

    add-float v24, v4, v5

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v25

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v26

    move-object/from16 v22, p0

    move-object/from16 v23, p1

    invoke-direct/range {v22 .. v27}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawLeftRightBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V

    goto/16 :goto_2

    .line 1374
    :cond_e
    new-instance v31, Landroid/graphics/Paint;

    const/4 v4, 0x1

    move-object/from16 v0, v31

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    .line 1375
    .local v31, "leftBorPt":Landroid/graphics/Paint;
    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1378
    if-eqz v21, :cond_f

    .line 1379
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v4

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v5

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v7

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1383
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_f

    .line 1384
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v4

    float-to-double v4, v4

    const-wide/high16 v6, 0x3ff4000000000000L    # 1.25

    cmpg-double v4, v4, v6

    if-gez v4, :cond_13

    const/high16 v4, 0x3fa00000    # 1.25f

    :goto_3
    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1389
    :cond_f
    new-instance v32, Landroid/graphics/Paint;

    const/4 v4, 0x1

    move-object/from16 v0, v32

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    .line 1390
    .local v32, "topBorPt":Landroid/graphics/Paint;
    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1393
    if-eqz v9, :cond_10

    .line 1394
    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v4

    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v5

    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v7

    move-object/from16 v0, v32

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1398
    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_10

    .line 1399
    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v4

    float-to-double v4, v4

    const-wide/high16 v6, 0x3ff4000000000000L    # 1.25

    cmpg-double v4, v4, v6

    if-gez v4, :cond_14

    const/high16 v4, 0x3fa00000    # 1.25f

    :goto_4
    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1404
    :cond_10
    new-instance v34, Landroid/graphics/Paint;

    const/4 v4, 0x1

    move-object/from16 v0, v34

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    .line 1405
    .local v34, "rightBorPt":Landroid/graphics/Paint;
    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1408
    if-eqz v27, :cond_11

    .line 1409
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v4

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v5

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v7

    move-object/from16 v0, v34

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1413
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_11

    .line 1414
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v4

    float-to-double v4, v4

    const-wide/high16 v6, 0x3ff4000000000000L    # 1.25

    cmpg-double v4, v4, v6

    if-gez v4, :cond_15

    const/high16 v4, 0x3fa00000    # 1.25f

    :goto_5
    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1419
    :cond_11
    new-instance v33, Landroid/graphics/Paint;

    const/4 v4, 0x1

    move-object/from16 v0, v33

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    .line 1420
    .local v33, "bottomBorPt":Landroid/graphics/Paint;
    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1423
    if-eqz v15, :cond_12

    .line 1424
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v4

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v5

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v6

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v7

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1428
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_12

    .line 1429
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v4

    float-to-double v4, v4

    const-wide/high16 v6, 0x3ff4000000000000L    # 1.25

    cmpg-double v4, v4, v6

    if-gez v4, :cond_16

    const/high16 v4, 0x3fa00000    # 1.25f

    :goto_6
    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :cond_12
    move-object/from16 v28, p0

    .line 1434
    invoke-direct/range {v28 .. v35}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->fillTablePaint(IILandroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Ljava/util/ArrayList;)V

    .line 1438
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v6

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v7

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v8

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v10, v32

    invoke-direct/range {v4 .. v10}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawTopBottomBorderForWord(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;Landroid/graphics/Paint;)V

    .line 1443
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v12

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v4

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v5

    add-float v13, v4, v5

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v14

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v16, v33

    invoke-direct/range {v10 .. v16}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawTopBottomBorderForWord(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;Landroid/graphics/Paint;)V

    .line 1448
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v18

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v19

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v20

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    move-object/from16 v22, v31

    invoke-direct/range {v16 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawLeftRightBorderForWord(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;Landroid/graphics/Paint;)V

    .line 1453
    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v5

    add-float v24, v4, v5

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v25

    invoke-virtual/range {v42 .. v42}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v26

    move-object/from16 v22, p0

    move-object/from16 v23, p1

    move-object/from16 v28, v34

    invoke-direct/range {v22 .. v28}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawLeftRightBorderForWord(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1384
    .end local v32    # "topBorPt":Landroid/graphics/Paint;
    .end local v33    # "bottomBorPt":Landroid/graphics/Paint;
    .end local v34    # "rightBorPt":Landroid/graphics/Paint;
    :cond_13
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v4

    goto/16 :goto_3

    .line 1399
    .restart local v32    # "topBorPt":Landroid/graphics/Paint;
    :cond_14
    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v4

    goto/16 :goto_4

    .line 1414
    .restart local v34    # "rightBorPt":Landroid/graphics/Paint;
    :cond_15
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v4

    goto/16 :goto_5

    .line 1429
    .restart local v33    # "bottomBorPt":Landroid/graphics/Paint;
    :cond_16
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v4

    goto/16 :goto_6

    .line 1467
    .end local v9    # "top":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    .end local v15    # "bottom":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    .end local v21    # "left":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    .end local v27    # "right":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    .end local v31    # "leftBorPt":Landroid/graphics/Paint;
    .end local v32    # "topBorPt":Landroid/graphics/Paint;
    .end local v33    # "bottomBorPt":Landroid/graphics/Paint;
    .end local v34    # "rightBorPt":Landroid/graphics/Paint;
    .end local v39    # "fillStyle":Lcom/samsung/thumbnail/customview/tables/CellFillStyle;
    .end local v40    # "paintNew":Landroid/graphics/Paint;
    .end local v41    # "pathTable":Landroid/graphics/Path;
    .end local v42    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    :cond_17
    if-eqz v38, :cond_18

    .line 1468
    move/from16 v0, v36

    move-object/from16 v1, v38

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableLeft:I

    .line 1469
    move/from16 v0, v37

    move-object/from16 v1, v38

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->availableRight:I

    .line 1470
    const/16 v38, 0x0

    .line 1190
    :cond_18
    add-int/lit8 v29, v29, 0x1

    goto/16 :goto_0

    .line 1473
    .end local v30    # "colNum":I
    .end local v35    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v43    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    :cond_19
    return-void
.end method

.method private drawNormalCellsText(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1476
    iget-object v5, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 1477
    .local v4, "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellList()Ljava/util/ArrayList;

    move-result-object v3

    .line 1479
    .local v3, "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 1482
    .local v2, "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellMergeValue()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    .line 1485
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHideCell()Z

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_1

    .line 1488
    const/4 v5, 0x0

    invoke-direct {p0, v2, p1, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->displayText(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Landroid/graphics/Canvas;Z)F

    goto :goto_0

    .line 1492
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v3    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v4    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    :cond_2
    return-void
.end method

.method private drawTopBottomBorder(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "width"    # F
    .param p5, "border"    # Lcom/samsung/thumbnail/customview/tables/TableBorder;

    .prologue
    const/4 v9, 0x6

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 623
    new-instance v5, Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-direct {v5, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 624
    .local v5, "paint":Landroid/graphics/Paint;
    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 625
    sget-object v0, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 628
    if-nez p5, :cond_1

    .line 630
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->isGridDisplay:Z

    if-eqz v0, :cond_0

    .line 631
    const v0, -0xbbbbbc

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 632
    const v0, 0x3dcccccd    # 0.1f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 633
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 720
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 638
    :cond_1
    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v0

    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v1

    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v2

    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v3

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 642
    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v0

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_2

    .line 643
    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 646
    :cond_2
    if-eqz p5, :cond_3

    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v0, v1, :cond_3

    .line 647
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 650
    :cond_3
    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 651
    sget-object v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XDocBorder$EBorderStyle:[I

    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 716
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 653
    :pswitch_1
    sub-float v2, p3, v6

    add-float v3, p2, p4

    sub-float v4, p3, v6

    move-object v0, p1

    move v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 654
    add-float v2, p3, v6

    add-float v3, p2, p4

    add-float v4, p3, v6

    move-object v0, p1

    move v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 660
    :pswitch_2
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 663
    :pswitch_3
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 666
    :pswitch_4
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v7, [F

    fill-array-data v1, :array_0

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 668
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 671
    :pswitch_5
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 674
    :pswitch_6
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v7, [F

    fill-array-data v1, :array_1

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 676
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 679
    :pswitch_7
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 682
    :pswitch_8
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 685
    :pswitch_9
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v7, [F

    fill-array-data v1, :array_2

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 687
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 690
    :pswitch_a
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v8, [F

    fill-array-data v1, :array_3

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 693
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 696
    :pswitch_b
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v9, [F

    fill-array-data v1, :array_4

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 700
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 703
    :pswitch_c
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v8, [F

    fill-array-data v1, :array_5

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 706
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 709
    :pswitch_d
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v9, [F

    fill-array-data v1, :array_6

    invoke-direct {v0, v1, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 713
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 651
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 666
    :array_0
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
    .end array-data

    .line 674
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 685
    :array_2
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
    .end array-data

    .line 690
    :array_3
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data

    .line 696
    :array_4
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data

    .line 703
    :array_5
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data

    .line 709
    :array_6
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data
.end method

.method private drawTopBottomBorderForWord(Landroid/graphics/Canvas;FFFLcom/samsung/thumbnail/customview/tables/TableBorder;Landroid/graphics/Paint;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "width"    # F
    .param p5, "border"    # Lcom/samsung/thumbnail/customview/tables/TableBorder;
    .param p6, "paint"    # Landroid/graphics/Paint;

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 742
    if-nez p5, :cond_1

    .line 816
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 746
    :cond_1
    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 747
    sget-object v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XDocBorder$EBorderStyle:[I

    invoke-virtual {p5}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 812
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 749
    :pswitch_1
    sub-float v2, p3, v6

    add-float v3, p2, p4

    sub-float v4, p3, v6

    move-object v0, p1

    move v1, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 750
    add-float v2, p3, v6

    add-float v3, p2, p4

    add-float v4, p3, v6

    move-object v0, p1

    move v1, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 756
    :pswitch_2
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 759
    :pswitch_3
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 762
    :pswitch_4
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v3, [F

    fill-array-data v1, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 764
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 767
    :pswitch_5
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 770
    :pswitch_6
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v3, [F

    fill-array-data v1, :array_1

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 772
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 775
    :pswitch_7
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 778
    :pswitch_8
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 781
    :pswitch_9
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v3, [F

    fill-array-data v1, :array_2

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 783
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 786
    :pswitch_a
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v4, [F

    fill-array-data v1, :array_3

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 789
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 792
    :pswitch_b
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v5, [F

    fill-array-data v1, :array_4

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 796
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 799
    :pswitch_c
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v4, [F

    fill-array-data v1, :array_5

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 802
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 805
    :pswitch_d
    new-instance v0, Landroid/graphics/DashPathEffect;

    new-array v1, v5, [F

    fill-array-data v1, :array_6

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {p6, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 809
    add-float v3, p2, p4

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p3

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 747
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 762
    :array_0
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
    .end array-data

    .line 770
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 781
    :array_2
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
    .end array-data

    .line 786
    :array_3
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data

    .line 792
    :array_4
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data

    .line 799
    :array_5
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data

    .line 805
    :array_6
    .array-data 4
        0x40800000    # 4.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
    .end array-data
.end method

.method private fillTablePaint(IILandroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Ljava/util/ArrayList;)V
    .locals 12
    .param p1, "rowNum"    # I
    .param p2, "colNum"    # I
    .param p3, "leftBorPt"    # Landroid/graphics/Paint;
    .param p4, "topBorPt"    # Landroid/graphics/Paint;
    .param p5, "bottomBorPt"    # Landroid/graphics/Paint;
    .param p6, "rightBorPt"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/graphics/Paint;",
            "Landroid/graphics/Paint;",
            "Landroid/graphics/Paint;",
            "Landroid/graphics/Paint;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2017
    .local p7, "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    iget-object v10, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 2018
    iget-object v10, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 2020
    .local v2, "borders":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 2021
    .local v5, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;>;"
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 2022
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    .line 2023
    .local v1, "borType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    sget-object v10, Lcom/samsung/thumbnail/customview/tables/CanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XDocBorder$EBorderType:[I

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    goto :goto_0

    .line 2025
    :pswitch_0
    iget-object v10, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 2027
    .local v8, "topBor":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v10

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v10, v11, :cond_0

    .line 2031
    if-nez p1, :cond_0

    .line 2032
    const/4 v10, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 2036
    .end local v8    # "topBor":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :pswitch_1
    iget-object v10, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 2038
    .local v3, "botBor":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v10

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v10, v11, :cond_0

    .line 2042
    iget-object v10, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ne p1, v10, :cond_0

    .line 2043
    const/4 v10, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v10}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 2048
    .end local v3    # "botBor":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :pswitch_2
    iget-object v10, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 2050
    .local v7, "rightBor":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v10

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v10, v11, :cond_0

    .line 2054
    invoke-virtual/range {p7 .. p7}, Ljava/util/ArrayList;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ne p2, v10, :cond_0

    .line 2055
    const/4 v10, 0x0

    move-object/from16 v0, p6

    invoke-virtual {v0, v10}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_0

    .line 2060
    .end local v7    # "rightBor":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :pswitch_3
    iget-object v10, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 2062
    .local v6, "leftBor":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v10

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v10, v11, :cond_0

    .line 2066
    if-nez p2, :cond_0

    .line 2067
    const/4 v10, 0x0

    invoke-virtual {p3, v10}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_0

    .line 2073
    .end local v6    # "leftBor":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :pswitch_4
    iget-object v10, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 2075
    .local v4, "horBor":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v10

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v10, v11, :cond_0

    .line 2080
    iget-object v10, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-eq p1, v10, :cond_1

    .line 2081
    const/4 v10, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 2082
    :cond_1
    if-eqz p1, :cond_0

    .line 2083
    const/4 v10, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_0

    .line 2087
    .end local v4    # "horBor":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :pswitch_5
    iget-object v10, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 2089
    .local v9, "verBor":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v10

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    if-ne v10, v11, :cond_0

    .line 2094
    invoke-virtual/range {p7 .. p7}, Ljava/util/ArrayList;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-eq p2, v10, :cond_2

    .line 2095
    const/4 v10, 0x0

    move-object/from16 v0, p6

    invoke-virtual {v0, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 2096
    :cond_2
    if-eqz p2, :cond_0

    .line 2097
    const/4 v10, 0x0

    invoke-virtual {p3, v10}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_0

    .line 2106
    .end local v1    # "borType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    .end local v2    # "borders":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;>;"
    .end local v5    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;>;"
    .end local v9    # "verBor":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_3
    return-void

    .line 2023
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public calculateHeight()F
    .locals 27

    .prologue
    .line 175
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->isHeightCalculated:Z

    .line 177
    const/16 v24, 0x0

    .line 204
    .local v24, "totalHeight":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 205
    .local v22, "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellList()Ljava/util/ArrayList;

    move-result-object v21

    .line 206
    .local v21, "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getRowHeight()F

    move-result v18

    .line 207
    .local v18, "rowHeight":F
    move/from16 v7, v18

    .line 209
    .local v7, "finalRowHeight":F
    const/4 v11, 0x0

    .line 210
    .local v11, "isWrapContent":Z
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 211
    .local v20, "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->calculateTextDimensions()V

    .line 213
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v6

    .line 215
    .local v6, "cellheight":F
    cmpg-float v25, v7, v6

    if-gez v25, :cond_1

    .line 216
    move v7, v6

    .line 218
    :cond_1
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getWrapContent()Z

    move-result v25

    if-eqz v25, :cond_0

    .line 220
    const/4 v11, 0x1

    goto :goto_1

    .line 223
    .end local v6    # "cellheight":F
    .end local v20    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    :cond_2
    if-nez v11, :cond_3

    .line 228
    :cond_3
    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->updateRowHeight(F)V

    .line 229
    add-float v24, v24, v7

    .line 230
    goto :goto_0

    .line 236
    .end local v7    # "finalRowHeight":F
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v11    # "isWrapContent":Z
    .end local v18    # "rowHeight":F
    .end local v21    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v22    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableXPos:F

    .line 237
    .local v2, "XValues":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableYPos:F

    .line 238
    .local v3, "YValues":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 239
    .restart local v22    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellList()Ljava/util/ArrayList;

    move-result-object v21

    .line 241
    .restart local v21    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .restart local v9    # "i$":Ljava/util/Iterator;
    :cond_5
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 242
    .restart local v20    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHideCell()Z

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_5

    .line 246
    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellX(F)V

    .line 247
    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellY(F)V

    .line 248
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v25

    add-float v2, v2, v25

    .line 249
    goto :goto_3

    .line 251
    .end local v20    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableXPos:F

    .line 252
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getRowHeight()F

    move-result v25

    add-float v3, v3, v25

    .line 253
    goto :goto_2

    .line 257
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v21    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v22    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_8
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_11

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 258
    .restart local v22    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellList()Ljava/util/ArrayList;

    move-result-object v21

    .line 260
    .restart local v21    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v19

    .line 261
    .local v19, "size":I
    const/4 v12, 0x0

    .local v12, "k":I
    :goto_4
    move/from16 v0, v19

    if-ge v12, v0, :cond_8

    .line 262
    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 264
    .restart local v20    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHideCell()Z

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_a

    if-nez v12, :cond_9

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->showHeaders:Z

    move/from16 v25, v0

    if-nez v25, :cond_a

    :cond_9
    add-int/lit8 v25, v19, -0x1

    move/from16 v0, v25

    if-lt v12, v0, :cond_b

    .line 261
    :cond_a
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    .line 268
    :cond_b
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getSingleLineTextWidth()F

    move-result v23

    .line 269
    .local v23, "textWidth":F
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v5

    .line 270
    .local v5, "cellWidth":F
    cmpl-float v25, v23, v5

    if-lez v25, :cond_a

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getShrinkToFit()Z

    move-result v25

    if-nez v25, :cond_a

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getWrapContent()Z

    move-result v25

    if-nez v25, :cond_a

    .line 272
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v25

    if-eqz v25, :cond_e

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v25

    const-string/jumbo v26, "left"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_e

    .line 275
    add-int/lit8 v17, v12, 0x1

    .line 276
    .local v17, "nextCellCount":I
    :cond_c
    :goto_5
    add-int/lit8 v25, v19, -0x1

    move/from16 v0, v17

    move/from16 v1, v25

    if-gt v0, v1, :cond_a

    .line 277
    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 279
    .local v16, "nextCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    add-int/lit8 v17, v17, 0x1

    .line 280
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHideCell()Z

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_c

    .line 282
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getParagraphList()Ljava/util/ArrayList;

    move-result-object v25

    if-eqz v25, :cond_d

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getParagraphList()Ljava/util/ArrayList;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    if-nez v25, :cond_a

    .line 284
    :cond_d
    const/16 v25, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setIsDrawRightBorder(Z)V

    .line 285
    const/16 v25, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setIsDrawLeftBorder(Z)V

    .line 286
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v25

    add-float v5, v5, v25

    .line 287
    cmpg-float v25, v23, v5

    if-ltz v25, :cond_a

    .line 290
    move-object/from16 v20, v16

    .line 294
    goto :goto_5

    .line 295
    .end local v16    # "nextCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v17    # "nextCellCount":I
    :cond_e
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v25

    if-eqz v25, :cond_a

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v25

    const-string/jumbo v26, "right"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 298
    add-int/lit8 v14, v12, -0x1

    .line 299
    .local v14, "lastCellCount":I
    :cond_f
    :goto_6
    if-ltz v14, :cond_a

    .line 300
    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 302
    .local v13, "lastCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    add-int/lit8 v14, v14, -0x1

    .line 303
    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHideCell()Z

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_f

    .line 305
    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getParagraphList()Ljava/util/ArrayList;

    move-result-object v25

    if-eqz v25, :cond_10

    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getParagraphList()Ljava/util/ArrayList;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    if-nez v25, :cond_a

    .line 307
    :cond_10
    const/16 v25, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setIsDrawLeftBorder(Z)V

    .line 308
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setIsDrawRightBorder(Z)V

    .line 309
    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v25

    add-float v5, v5, v25

    .line 310
    cmpg-float v25, v23, v5

    if-ltz v25, :cond_a

    .line 313
    move-object/from16 v20, v13

    .line 317
    goto :goto_6

    .line 325
    .end local v5    # "cellWidth":F
    .end local v12    # "k":I
    .end local v13    # "lastCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v14    # "lastCellCount":I
    .end local v19    # "size":I
    .end local v20    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v21    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v22    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    .end local v23    # "textWidth":F
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_12
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_15

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 326
    .restart local v22    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellList()Ljava/util/ArrayList;

    move-result-object v21

    .line 327
    .restart local v21    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 329
    .local v4, "cellSize":I
    const/4 v10, 0x0

    .local v10, "indexMerge":I
    :goto_7
    if-ge v10, v4, :cond_12

    .line 330
    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 332
    .restart local v20    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellMergeValue()Ljava/lang/String;

    move-result-object v15

    .line 334
    .local v15, "mergeVal":Ljava/lang/String;
    if-eqz v15, :cond_13

    .line 335
    const-string/jumbo v25, "restart"

    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_14

    .line 336
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mMergedCellsHandler:Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->CreateNewCellMergeList(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 329
    :cond_13
    :goto_8
    add-int/lit8 v10, v10, 0x1

    goto :goto_7

    .line 337
    :cond_14
    const-string/jumbo v25, "continue"

    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_13

    .line 338
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mMergedCellsHandler:Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->addToCellMergeList(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    goto :goto_8

    .line 343
    .end local v4    # "cellSize":I
    .end local v10    # "indexMerge":I
    .end local v15    # "mergeVal":Ljava/lang/String;
    .end local v20    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v21    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v22    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mMergedCellsHandler:Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->mergeVerticalCells()V

    .line 345
    return v24
.end method

.method public calculateHeightForWord(Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)F
    .locals 35
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 356
    const/16 v33, 0x0

    .line 357
    .local v33, "x":I
    const/16 v34, 0x0

    .line 359
    .local v34, "y":I
    new-instance v16, Landroid/graphics/Canvas;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Canvas;-><init>()V

    .line 360
    .local v16, "c":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 361
    .local v31, "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellList()Ljava/util/ArrayList;

    move-result-object v30

    .line 362
    .local v30, "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    const/16 v25, 0x0

    .line 363
    .local v25, "maxCellHeight":I
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .local v23, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 364
    .local v29, "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getDocParaList()Ljava/util/ArrayList;

    move-result-object v19

    .line 366
    .local v19, "docParaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v4

    float-to-int v0, v4

    move/from16 v34, v0

    .line 367
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    float-to-int v0, v4

    move/from16 v33, v0

    .line 369
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "normal"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 372
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v28

    .line 373
    .local v28, "size":I
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_1
    move/from16 v0, v21

    move/from16 v1, v28

    if-ge v0, v1, :cond_3

    .line 374
    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 375
    .local v3, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    sget-object v4, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->TABLE:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setParentType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;)V

    .line 376
    move/from16 v0, v34

    int-to-float v4, v0

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTopSpacing()F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v34, v0

    .line 378
    move/from16 v0, v34

    invoke-virtual {v3, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    .line 379
    move/from16 v0, v33

    invoke-virtual {v3, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 380
    new-instance v2, Lcom/samsung/thumbnail/customview/word/LineBreaker;

    invoke-direct {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;-><init>()V

    .line 381
    .local v2, "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v8

    float-to-int v7, v8

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I

    .line 385
    new-instance v7, Landroid/graphics/Rect;

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v6

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v8

    add-float/2addr v6, v8

    float-to-int v6, v6

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v8

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v9

    add-float/2addr v8, v9

    float-to-int v8, v8

    invoke-direct {v7, v4, v5, v6, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 392
    .local v7, "rect":Landroid/graphics/Rect;
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v4

    float-to-int v8, v4

    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v4

    float-to-int v9, v4

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x0

    move-object v4, v3

    move-object/from16 v5, v16

    move-object/from16 v6, p1

    move-object/from16 v13, p2

    invoke-virtual/range {v3 .. v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 398
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getY()I

    move-result v34

    .line 399
    add-int/lit8 v4, v28, -0x1

    move/from16 v0, v21

    if-eq v0, v4, :cond_2

    .line 400
    move/from16 v0, v34

    int-to-float v4, v0

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBottomSpacing()F

    move-result v5

    add-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v34, v0

    .line 373
    :cond_2
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_1

    .line 403
    .end local v2    # "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    .end local v3    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v7    # "rect":Landroid/graphics/Rect;
    :cond_3
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v4

    float-to-int v4, v4

    sub-int v34, v34, v4

    .line 404
    if-eqz v25, :cond_4

    move/from16 v0, v25

    move/from16 v1, v34

    if-ge v0, v1, :cond_1

    .line 405
    :cond_4
    move/from16 v25, v34

    goto/16 :goto_0

    .line 410
    .end local v19    # "docParaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    .end local v21    # "i":I
    .end local v28    # "size":I
    .end local v29    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    :cond_5
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 411
    .local v17, "cellSize":I
    const/16 v21, 0x0

    .restart local v21    # "i":I
    :goto_2
    move/from16 v0, v21

    move/from16 v1, v17

    if-ge v0, v1, :cond_0

    .line 412
    move-object/from16 v0, v30

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v4

    move/from16 v0, v25

    int-to-float v5, v0

    cmpg-float v4, v4, v5

    if-gez v4, :cond_6

    .line 413
    move-object/from16 v0, v30

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move/from16 v0, v25

    int-to-float v5, v0

    invoke-virtual {v4, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellHeight(F)V

    .line 411
    :cond_6
    add-int/lit8 v21, v21, 0x1

    goto :goto_2

    .line 417
    .end local v17    # "cellSize":I
    .end local v21    # "i":I
    .end local v23    # "i$":Ljava/util/Iterator;
    .end local v25    # "maxCellHeight":I
    .end local v30    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v31    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    :cond_7
    const/16 v32, 0x0

    .line 424
    .local v32, "totalHeight":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_3
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 425
    .restart local v31    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellList()Ljava/util/ArrayList;

    move-result-object v30

    .line 426
    .restart local v30    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getRowHeight()F

    move-result v27

    .line 427
    .local v27, "rowHeight":F
    move/from16 v20, v27

    .line 429
    .local v20, "finalRowHeight":F
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .restart local v23    # "i$":Ljava/util/Iterator;
    :cond_8
    :goto_4
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 430
    .restart local v29    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v18

    .line 431
    .local v18, "cellheight":F
    cmpg-float v4, v20, v18

    if-gez v4, :cond_8

    .line 432
    move/from16 v20, v18

    goto :goto_4

    .line 436
    .end local v18    # "cellheight":F
    .end local v29    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    :cond_9
    move-object/from16 v0, v31

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->updateRowHeight(F)V

    goto :goto_3

    .line 444
    .end local v20    # "finalRowHeight":F
    .end local v23    # "i$":Ljava/util/Iterator;
    .end local v27    # "rowHeight":F
    .end local v30    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v31    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_5
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 445
    .restart local v31    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellList()Ljava/util/ArrayList;

    move-result-object v30

    .line 446
    .restart local v30    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getRowHeight()F

    move-result v27

    .line 447
    .restart local v27    # "rowHeight":F
    move/from16 v20, v27

    .line 449
    .restart local v20    # "finalRowHeight":F
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .restart local v23    # "i$":Ljava/util/Iterator;
    :cond_b
    :goto_6
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 450
    .restart local v29    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->calculateTextDimensions()V

    .line 452
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v18

    .line 454
    .restart local v18    # "cellheight":F
    cmpg-float v4, v20, v18

    if-gez v4, :cond_b

    .line 455
    move/from16 v20, v18

    goto :goto_6

    .line 459
    .end local v18    # "cellheight":F
    .end local v29    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    :cond_c
    move-object/from16 v0, v31

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->updateRowHeight(F)V

    .line 460
    add-float v32, v32, v20

    .line 461
    goto :goto_5

    .line 467
    .end local v20    # "finalRowHeight":F
    .end local v23    # "i$":Ljava/util/Iterator;
    .end local v27    # "rowHeight":F
    .end local v30    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v31    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    :cond_d
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableXPos:F

    .line 468
    .local v14, "XValues":F
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableYPos:F

    .line 469
    .local v15, "YValues":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_7
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 470
    .restart local v31    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellList()Ljava/util/ArrayList;

    move-result-object v30

    .line 472
    .restart local v30    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .restart local v23    # "i$":Ljava/util/Iterator;
    :cond_e
    :goto_8
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 473
    .restart local v29    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHideCell()Z

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_e

    .line 477
    move-object/from16 v0, v29

    invoke-virtual {v0, v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellX(F)V

    .line 478
    move-object/from16 v0, v29

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellY(F)V

    .line 479
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v4

    add-float/2addr v14, v4

    .line 480
    goto :goto_8

    .line 482
    .end local v29    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    :cond_f
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableXPos:F

    .line 483
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getRowHeight()F

    move-result v4

    add-float/2addr v15, v4

    .line 484
    goto :goto_7

    .line 488
    .end local v23    # "i$":Ljava/util/Iterator;
    .end local v30    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v31    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "i$":Ljava/util/Iterator;
    :cond_11
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_14

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 489
    .restart local v31    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellList()Ljava/util/ArrayList;

    move-result-object v30

    .line 490
    .restart local v30    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 491
    .restart local v17    # "cellSize":I
    const/16 v24, 0x0

    .local v24, "indexMerge":I
    :goto_9
    move/from16 v0, v24

    move/from16 v1, v17

    if-ge v0, v1, :cond_11

    .line 492
    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 494
    .restart local v29    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual/range {v29 .. v29}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellMergeValue()Ljava/lang/String;

    move-result-object v26

    .line 496
    .local v26, "mergeVal":Ljava/lang/String;
    if-eqz v26, :cond_12

    .line 497
    const-string/jumbo v4, "restart"

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 498
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mMergedCellsHandler:Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->CreateNewCellMergeList(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 491
    :cond_12
    :goto_a
    add-int/lit8 v24, v24, 0x1

    goto :goto_9

    .line 499
    :cond_13
    const-string/jumbo v4, "continue"

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 500
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mMergedCellsHandler:Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->addToCellMergeList(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    goto :goto_a

    .line 506
    .end local v17    # "cellSize":I
    .end local v24    # "indexMerge":I
    .end local v26    # "mergeVal":Ljava/lang/String;
    .end local v29    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v30    # "tempCellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v31    # "tempRow":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mMergedCellsHandler:Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->mergeVerticalCellsForWord()V

    .line 507
    move/from16 v0, v32

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mTotalHeight:F

    .line 508
    return v32
.end method

.method public calculateWidth()F
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mTblWidth:I

    int-to-float v0, v0

    return v0
.end method

.method public createNewRow()V
    .locals 2

    .prologue
    .line 121
    new-instance v0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mCurrentRow:Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    .line 122
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mCurrentRow:Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    return-void
.end method

.method public getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mCurrentRow:Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    return-object v0
.end method

.method public getGridDisplay()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->isGridDisplay:Z

    return v0
.end method

.method public getRow(I)Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    return-object v0
.end method

.method public getRowList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mRowList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTableHeight()F
    .locals 1

    .prologue
    .line 513
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mTotalHeight:F

    return v0
.end method

.method public getTableNumber()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableNum:I

    return v0
.end method

.method public getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    return-object v0
.end method

.method public getTableType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableType:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    return-object v0
.end method

.method public getTableX()F
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableXPos:F

    return v0
.end method

.method public getTableY()F
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableYPos:F

    return v0
.end method

.method public getXPos()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->xPos:Ljava/lang/String;

    return-object v0
.end method

.method public getYPos()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->yPos:Ljava/lang/String;

    return-object v0
.end method

.method public isHeightCalculated()Z
    .locals 1

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->isHeightCalculated:Z

    return v0
.end method

.method public paint(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1962
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawNormalCells(Landroid/graphics/Canvas;)V

    .line 1963
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawNormalCellsText(Landroid/graphics/Canvas;)V

    .line 1969
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawMergedCells(Landroid/graphics/Canvas;)V

    .line 1970
    return-void
.end method

.method public paint(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 1978
    invoke-virtual {p0, p2, p3}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->calculateHeightForWord(Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)F

    .line 1979
    invoke-direct {p0, p1, p3}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawNormalCellsForWord(Landroid/graphics/Canvas;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1980
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawCellParagraph(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1986
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->drawMergedCellsForWord(Landroid/graphics/Canvas;Landroid/content/Context;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1987
    return-void
.end method

.method public setGridDisplay(Z)V
    .locals 0
    .param p1, "isGridDisplay"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->isGridDisplay:Z

    .line 84
    return-void
.end method

.method public setHeadersDisplay(Z)V
    .locals 0
    .param p1, "showHeaders"    # Z

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->showHeaders:Z

    .line 88
    return-void
.end method

.method public setTableNumber(I)V
    .locals 0
    .param p1, "tableNum"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableNum:I

    .line 114
    return-void
.end method

.method public setTableProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V
    .locals 0
    .param p1, "tblProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tblProp:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    .line 163
    return-void
.end method

.method public setTableType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V
    .locals 0
    .param p1, "paraType"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableType:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    .line 106
    return-void
.end method

.method public setTableWidth(I)V
    .locals 0
    .param p1, "tblWidth"    # I

    .prologue
    .line 1990
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->mTblWidth:I

    .line 1991
    return-void
.end method

.method public setTableX(F)V
    .locals 0
    .param p1, "x"    # F

    .prologue
    .line 138
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableXPos:F

    .line 139
    return-void
.end method

.method public setTableY(F)V
    .locals 0
    .param p1, "y"    # F

    .prologue
    .line 146
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->tableYPos:F

    .line 147
    return-void
.end method

.method public setXPos(Ljava/lang/String;)V
    .locals 0
    .param p1, "xPos"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->xPos:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setYPos(Ljava/lang/String;)V
    .locals 0
    .param p1, "yPos"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->yPos:Ljava/lang/String;

    .line 68
    return-void
.end method
