.class public Lcom/samsung/thumbnail/customview/word/LineBreaker;
.super Ljava/lang/Object;
.source "LineBreaker.java"


# instance fields
.field lengthOfThisLine:F

.field private lineLength:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mEllipsisLength:F

.field private mEllipsisLengthMore:F

.field private mIsEllipsisRequired:Z

.field private mLastLineLength:F

.field private mNoOfLines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[I>;"
        }
    .end annotation
.end field

.field mlineElement:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/ParaLine;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mIsEllipsisRequired:Z

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lineLength:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mlineElement:Ljava/util/ArrayList;

    .line 40
    return-void
.end method


# virtual methods
.method public breakText(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;II)I
    .locals 27
    .param p2, "ellipsis"    # Ljava/lang/String;
    .param p3, "ellipsisMore"    # Ljava/lang/String;
    .param p4, "maxLines"    # I
    .param p5, "maxWidth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/Run;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II)I"
        }
    .end annotation

    .prologue
    .line 555
    .local p1, "runArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V

    .line 556
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mIsEllipsisRequired:Z

    .line 557
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mLastLineLength:F

    .line 558
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLength:F

    .line 559
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLengthMore:F

    .line 560
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 561
    .local v17, "runSize":I
    move/from16 v0, v17

    new-array v8, v0, [Ljava/lang/String;

    .line 562
    .local v8, "in":[Ljava/lang/String;
    move/from16 v0, v17

    new-array v0, v0, [Landroid/text/TextPaint;

    move-object/from16 v19, v0

    .line 564
    .local v19, "textPaintArray":[Landroid/text/TextPaint;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    move/from16 v0, v17

    if-ge v7, v0, :cond_3

    .line 565
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v23

    aput-object v23, v8, v7

    .line 566
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v23

    aput-object v23, v19, v7

    .line 571
    const/16 v23, -0x1

    move/from16 v0, p5

    move/from16 v1, v23

    if-ne v0, v1, :cond_0

    .line 572
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [I

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/16 v26, 0x0

    aput v26, v24, v25

    const/16 v25, 0x1

    aget-object v26, v8, v7

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    aput v26, v24, v25

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573
    aget-object v23, v19, v7

    aget-object v24, v8, v7

    const/16 v25, 0x0

    aget-object v26, v8, v7

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    invoke-virtual/range {v23 .. v26}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v23

    const/high16 v24, 0x3f000000    # 0.5f

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    .line 762
    :goto_1
    return v23

    .line 579
    :cond_0
    if-eqz p2, :cond_1

    .line 580
    aget-object v23, v19, v7

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLength:F

    .line 582
    :cond_1
    if-eqz p3, :cond_2

    .line 583
    aget-object v23, v19, v7

    move-object/from16 v0, v23

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLengthMore:F

    .line 564
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 589
    :cond_3
    const/4 v15, -0x1

    .line 590
    .local v15, "posStartThisLine":I
    const/4 v10, 0x0

    .line 591
    .local v10, "lengthOfThisLine":F
    const/4 v6, 0x1

    .line 592
    .local v6, "breakWords":Z
    const/4 v14, 0x0

    .line 593
    .local v14, "pos":I
    const/16 v20, 0x0

    .line 594
    .local v20, "totalCharCount":I
    const/4 v11, 0x0

    .line 595
    .local v11, "longStrAdj":I
    const/4 v7, 0x0

    :goto_2
    move/from16 v0, v17

    if-ge v7, v0, :cond_17

    .line 596
    if-eqz v8, :cond_4

    aget-object v23, v8, v7

    if-eqz v23, :cond_4

    array-length v0, v8

    move/from16 v23, v0

    move/from16 v0, v23

    if-lt v7, v0, :cond_5

    .line 597
    :cond_4
    const/16 v23, 0x0

    goto :goto_1

    .line 601
    :cond_5
    aget-object v23, v19, v7

    aget-object v24, v8, v7

    const/16 v25, 0x0

    aget-object v26, v8, v7

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    invoke-static/range {v23 .. v26}, Lcom/samsung/thumbnail/customview/word/Run;->getTextWidths(Landroid/text/TextPaint;Ljava/lang/String;II)[F

    move-result-object v22

    .line 603
    .local v22, "widths":[F
    const/4 v14, 0x0

    .line 604
    aget-object v23, v8, v7

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v9

    .line 606
    .local v9, "inLength":I
    :goto_3
    if-ge v14, v9, :cond_7

    .line 608
    const/16 v23, -0x1

    move/from16 v0, v23

    if-ne v15, v0, :cond_6

    .line 609
    move/from16 v15, v20

    .line 612
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v23

    move/from16 v0, v23

    move/from16 v1, p4

    if-ne v0, v1, :cond_9

    .line 613
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mIsEllipsisRequired:Z

    .line 745
    :cond_7
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mIsEllipsisRequired:Z

    move/from16 v23, v0

    if-eqz v23, :cond_8

    .line 746
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v24

    add-int/lit8 v24, v24, -0x1

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [I

    .line 747
    .local v13, "pairLast":[I
    aget-object v23, v8, v7

    const/16 v24, 0x0

    aget v24, v13, v24

    const/16 v25, 0x1

    aget v25, v13, v25

    add-int/lit8 v25, v25, 0x1

    invoke-virtual/range {v23 .. v25}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v18

    .line 749
    .local v18, "subSequence":Ljava/lang/CharSequence;
    aget-object v23, v19, v7

    const/16 v24, 0x0

    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->length()I

    move-result v25

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mLastLineLength:F

    .line 595
    .end local v13    # "pairLast":[I
    .end local v18    # "subSequence":Ljava/lang/CharSequence;
    :cond_8
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    .line 617
    :cond_9
    aget v21, v22, v14

    .line 618
    .local v21, "widthOfChar":F
    const/4 v12, 0x0

    .line 622
    .local v12, "newLineRequired":Z
    aget-object v23, v8, v7

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/String;->charAt(I)C

    move-result v23

    const/16 v24, 0xa

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_c

    .line 623
    const/4 v12, 0x1

    .line 631
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [I

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput v15, v24, v25

    const/16 v25, 0x1

    add-int/lit8 v26, v20, -0x1

    aput v26, v24, v25

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 706
    :cond_a
    :goto_5
    if-eqz v12, :cond_16

    .line 709
    const/4 v15, -0x1

    .line 712
    const/4 v10, 0x0

    .line 716
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v23

    add-int/lit8 v24, p4, -0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_b

    .line 717
    move/from16 v0, p5

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLength:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLengthMore:F

    move/from16 v25, v0

    add-float v24, v24, v25

    sub-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 p5, v0

    .line 723
    const/4 v6, 0x0

    .line 739
    :cond_b
    :goto_6
    add-int/lit8 v14, v14, 0x1

    .line 740
    add-int/lit8 v20, v20, 0x1

    .line 742
    goto/16 :goto_3

    .line 633
    :cond_c
    add-float v23, v10, v21

    move/from16 v0, p5

    int-to-float v0, v0

    move/from16 v24, v0

    cmpl-float v23, v23, v24

    if-ltz v23, :cond_a

    .line 634
    const/4 v12, 0x1

    .line 636
    aget-object v23, v8, v7

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/String;->charAt(I)C

    move-result v23

    const/16 v24, 0x20

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_d

    if-nez v6, :cond_e

    .line 645
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [I

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput v15, v24, v25

    const/16 v25, 0x1

    aput v20, v24, v25

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 649
    :cond_e
    const/4 v4, 0x0

    .line 650
    .local v4, "backWidth":F
    const/4 v5, 0x0

    .line 651
    .local v5, "backWidthIsMore":Z
    move/from16 v16, v20

    .line 652
    .local v16, "refPos":I
    add-float v10, v10, v21

    .line 654
    :goto_7
    if-ltz v14, :cond_f

    aget-object v23, v8, v7

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/String;->charAt(I)C

    move-result v23

    const/16 v24, 0x20

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_f

    .line 656
    aget v23, v22, v14

    add-float v4, v4, v23

    .line 657
    move/from16 v0, p5

    int-to-float v0, v0

    move/from16 v23, v0

    cmpl-float v23, v4, v23

    if-ltz v23, :cond_10

    .line 659
    const/4 v5, 0x1

    .line 667
    :cond_f
    if-nez v5, :cond_11

    .line 668
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [I

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput v15, v24, v25

    const/16 v25, 0x1

    aput v20, v24, v25

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 670
    sub-int v11, v20, v15

    .line 699
    :goto_8
    const/16 v23, 0x1

    move/from16 v0, p4

    move/from16 v1, v23

    if-ne v0, v1, :cond_a

    goto/16 :goto_4

    .line 662
    :cond_10
    add-int/lit8 v14, v14, -0x1

    .line 663
    add-int/lit8 v20, v20, -0x1

    goto :goto_7

    .line 672
    :cond_11
    sub-int v23, v15, v20

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-gt v0, v1, :cond_12

    .line 675
    move/from16 v0, p5

    int-to-float v0, v0

    move/from16 v23, v0

    cmpl-float v23, v21, v23

    if-lez v23, :cond_13

    .line 676
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [I

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput v15, v24, v25

    const/16 v25, 0x1

    add-int/lit8 v26, v16, -0x1

    aput v26, v24, v25

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 678
    add-int/lit8 v20, v20, 0x1

    .line 679
    add-int/lit8 v14, v14, 0x1

    .line 680
    add-int/lit8 v16, v16, 0x1

    .line 689
    :cond_12
    :goto_9
    move/from16 v0, p5

    int-to-float v0, v0

    move/from16 v23, v0

    cmpl-float v23, v4, v23

    if-lez v23, :cond_15

    .line 690
    add-int/lit8 v20, v16, -0x1

    .line 691
    sub-int v23, v20, v11

    add-int/lit8 v14, v23, -0x1

    .line 696
    :goto_a
    const/16 v16, 0x0

    .line 697
    const/4 v12, 0x1

    goto :goto_8

    .line 681
    :cond_13
    move/from16 v0, p5

    int-to-float v0, v0

    move/from16 v23, v0

    cmpl-float v23, v4, v23

    if-lez v23, :cond_14

    .line 682
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [I

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput v15, v24, v25

    const/16 v25, 0x1

    add-int/lit8 v26, v16, -0x1

    aput v26, v24, v25

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 685
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [I

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput v15, v24, v25

    const/16 v25, 0x1

    aput v16, v24, v25

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 693
    :cond_15
    move/from16 v20, v16

    .line 694
    sub-int v14, v20, v11

    goto :goto_a

    .line 726
    .end local v4    # "backWidth":F
    .end local v5    # "backWidthIsMore":Z
    .end local v16    # "refPos":I
    :cond_16
    add-float v10, v10, v21

    .line 730
    aget-object v23, v8, v7

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v23

    add-int/lit8 v23, v23, -0x1

    move/from16 v0, v23

    if-ne v14, v0, :cond_b

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v23

    add-int/lit8 v23, v23, -0x1

    move/from16 v0, v23

    if-ne v7, v0, :cond_b

    .line 732
    add-int/lit8 v20, v20, 0x1

    .line 733
    add-int/lit8 v14, v14, 0x1

    .line 734
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [I

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput v15, v24, v25

    const/16 v25, 0x1

    aput v20, v24, v25

    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 756
    .end local v9    # "inLength":I
    .end local v12    # "newLineRequired":Z
    .end local v21    # "widthOfChar":F
    .end local v22    # "widths":[F
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v23

    if-nez v23, :cond_18

    .line 757
    const/16 v23, 0x0

    goto/16 :goto_1

    .line 758
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_19

    .line 760
    const/16 v23, 0x0

    goto/16 :goto_1

    :cond_19
    move/from16 v23, p5

    .line 762
    goto/16 :goto_1
.end method

.method public breakTextIntoLines(Ljava/lang/String;ILandroid/text/TextPaint;)I
    .locals 7
    .param p1, "in"    # Ljava/lang/String;
    .param p2, "maxWid"    # I
    .param p3, "tPaint"    # Landroid/text/TextPaint;

    .prologue
    const/4 v2, 0x0

    .line 44
    const/4 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move v5, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->breakTextIntoLines(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/text/TextPaint;)I

    move-result v0

    return v0
.end method

.method public breakTextIntoLines(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/text/TextPaint;)I
    .locals 17
    .param p1, "in"    # Ljava/lang/String;
    .param p2, "ellipsisText"    # Ljava/lang/String;
    .param p3, "MoreEllipsis"    # Ljava/lang/String;
    .param p4, "maximumLines"    # I
    .param p5, "maximumWidth"    # I
    .param p6, "tPaint"    # Landroid/text/TextPaint;

    .prologue
    .line 77
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    .line 78
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lineLength:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    .line 79
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mIsEllipsisRequired:Z

    .line 80
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mLastLineLength:F

    .line 81
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLength:F

    .line 82
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLengthMore:F

    .line 83
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 85
    .local v4, "length":I
    const/4 v13, -0x1

    move/from16 v0, p5

    if-ne v0, v13, :cond_0

    .line 86
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    const/4 v14, 0x2

    new-array v14, v14, [I

    const/4 v15, 0x0

    const/16 v16, 0x0

    aput v16, v14, v15

    const/4 v15, 0x1

    aput v4, v14, v15

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    const/4 v13, 0x0

    move-object/from16 v0, p6

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13, v4}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v13

    const/high16 v14, 0x3f000000    # 0.5f

    add-float/2addr v13, v14

    float-to-int v13, v13

    .line 183
    :goto_0
    return v13

    .line 90
    :cond_0
    if-eqz p2, :cond_1

    .line 91
    move-object/from16 v0, p6

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLength:F

    .line 93
    :cond_1
    if-eqz p3, :cond_2

    .line 94
    move-object/from16 v0, p6

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLengthMore:F

    .line 97
    :cond_2
    const/4 v8, -0x1

    .line 98
    .local v8, "posStartThisLine":I
    const/4 v3, 0x1

    .line 99
    .local v3, "breakWords":Z
    const/4 v7, 0x0

    .line 103
    .local v7, "pos":I
    const/4 v13, 0x0

    move-object/from16 v0, p6

    move-object/from16 v1, p1

    invoke-static {v0, v1, v13, v4}, Lcom/samsung/thumbnail/customview/word/Run;->getTextWidths(Landroid/text/TextPaint;Ljava/lang/String;II)[F

    move-result-object v12

    .line 104
    .local v12, "widths":[F
    :goto_1
    if-ge v7, v4, :cond_4

    .line 106
    const/4 v13, -0x1

    if-ne v8, v13, :cond_3

    .line 107
    move v8, v7

    .line 110
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    move/from16 v0, p4

    if-ne v13, v0, :cond_6

    .line 111
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mIsEllipsisRequired:Z

    .line 170
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mIsEllipsisRequired:Z

    if-eqz v13, :cond_5

    .line 171
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [I

    .line 172
    .local v6, "pairLast":[I
    const/4 v13, 0x0

    aget v13, v6, v13

    const/4 v14, 0x1

    aget v14, v6, v14

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v10

    .line 174
    .local v10, "subSequence":Ljava/lang/CharSequence;
    const/4 v13, 0x0

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v14

    move-object/from16 v0, p6

    invoke-virtual {v0, v10, v13, v14}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mLastLineLength:F

    .line 178
    .end local v6    # "pairLast":[I
    .end local v10    # "subSequence":Ljava/lang/CharSequence;
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-nez v13, :cond_10

    .line 179
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 115
    :cond_6
    aget v11, v12, v7

    .line 116
    .local v11, "widthOfChar":F
    const/4 v5, 0x0

    .line 118
    .local v5, "newLineRequired":Z
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v13

    const/16 v14, 0xa

    if-ne v13, v14, :cond_9

    .line 119
    const/4 v5, 0x1

    .line 121
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    const/4 v14, 0x2

    new-array v14, v14, [I

    const/4 v15, 0x0

    aput v8, v14, v15

    const/4 v15, 0x1

    add-int/lit8 v16, v7, -0x1

    aput v16, v14, v15

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_7
    :goto_3
    if-eqz v5, :cond_f

    .line 152
    const/4 v8, -0x1

    .line 154
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 156
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    add-int/lit8 v14, p4, -0x1

    if-ne v13, v14, :cond_8

    .line 157
    move/from16 v0, p5

    int-to-float v13, v0

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLength:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLengthMore:F

    add-float/2addr v14, v15

    sub-float/2addr v13, v14

    float-to-int v0, v13

    move/from16 p5, v0

    .line 158
    const/4 v3, 0x0

    .line 167
    :cond_8
    :goto_4
    add-int/lit8 v7, v7, 0x1

    .line 168
    goto/16 :goto_1

    .line 122
    :cond_9
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    add-float/2addr v13, v11

    move/from16 v0, p5

    int-to-float v14, v0

    cmpl-float v13, v13, v14

    if-ltz v13, :cond_7

    .line 123
    const/4 v5, 0x1

    .line 124
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v13

    const/16 v14, 0x20

    if-eq v13, v14, :cond_a

    if-nez v3, :cond_b

    .line 125
    :cond_a
    add-int/lit8 v7, v7, -0x1

    .line 127
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    const/4 v14, 0x2

    new-array v14, v14, [I

    const/4 v15, 0x0

    aput v8, v14, v15

    const/4 v15, 0x1

    aput v7, v14, v15

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 129
    :cond_b
    const/4 v2, 0x0

    .line 130
    .local v2, "backWidth":F
    move v9, v7

    .line 132
    .local v9, "refPos":I
    :goto_5
    if-ltz v7, :cond_c

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v13

    const/16 v14, 0x20

    if-eq v13, v14, :cond_c

    .line 134
    aget v13, v12, v7

    add-float/2addr v2, v13

    .line 135
    move/from16 v0, p5

    int-to-float v13, v0

    cmpl-float v13, v2, v13

    if-ltz v13, :cond_d

    .line 136
    move v7, v9

    .line 143
    :cond_c
    if-eq v7, v9, :cond_e

    .line 144
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    const/4 v14, 0x2

    new-array v14, v14, [I

    const/4 v15, 0x0

    aput v8, v14, v15

    const/4 v15, 0x1

    aput v7, v14, v15

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 139
    :cond_d
    add-int/lit8 v7, v7, -0x1

    goto :goto_5

    .line 145
    :cond_e
    const/4 v13, 0x1

    move/from16 v0, p4

    if-ne v0, v13, :cond_7

    goto/16 :goto_2

    .line 161
    .end local v2    # "backWidth":F
    .end local v9    # "refPos":I
    :cond_f
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    add-float/2addr v13, v11

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 163
    add-int/lit8 v13, v4, -0x1

    if-ne v7, v13, :cond_8

    .line 164
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    const/4 v14, 0x2

    new-array v14, v14, [I

    const/4 v15, 0x0

    aput v8, v14, v15

    const/4 v15, 0x1

    aput v7, v14, v15

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 180
    .end local v5    # "newLineRequired":Z
    .end local v11    # "widthOfChar":F
    :cond_10
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_11

    .line 181
    const/4 v13, 0x0

    move-object/from16 v0, p6

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13, v4}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v13

    const/high16 v14, 0x3f000000    # 0.5f

    add-float/2addr v13, v14

    float-to-int v13, v13

    goto/16 :goto_0

    :cond_11
    move/from16 v13, p5

    .line 183
    goto/16 :goto_0
.end method

.method public calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I
    .locals 36
    .param p1, "paragraph"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .param p2, "ellipsis"    # Ljava/lang/String;
    .param p3, "ellipsisMore"    # Ljava/lang/String;
    .param p4, "maxLines"    # I
    .param p5, "paraWidth"    # I
    .param p6, "firstLineWidth"    # I

    .prologue
    .line 205
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getContents()Ljava/util/ArrayList;

    move-result-object v22

    .line 206
    .local v22, "runArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    move/from16 v14, p6

    .line 208
    .local v14, "maxWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->clear()V

    .line 209
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mIsEllipsisRequired:Z

    .line 210
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mLastLineLength:F

    .line 211
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLength:F

    .line 212
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLengthMore:F

    .line 213
    const/4 v8, 0x0

    .line 214
    .local v8, "currentParaLineIndex":I
    const-string/jumbo v10, ""

    .line 215
    .local v10, "in":Ljava/lang/String;
    const/16 v26, 0x0

    .line 218
    .local v26, "textPaint":Landroid/text/TextPaint;
    const/16 v19, -0x1

    .line 220
    .local v19, "posStartThisLine":I
    const/4 v7, 0x1

    .line 221
    .local v7, "breakWords":Z
    const/16 v18, 0x0

    .line 222
    .local v18, "pos":I
    const/16 v28, 0x0

    .line 224
    .local v28, "totalCharCount":I
    new-instance v13, Lcom/samsung/thumbnail/customview/word/ParaLine;

    invoke-direct {v13}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 225
    .local v13, "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/16 v31, 0x1

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsText(Z)V

    .line 226
    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    .line 228
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v23

    .line 229
    .local v23, "runSize":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTabContents()Ljava/util/ArrayList;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v25

    .line 230
    .local v25, "tabSize":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    move/from16 v0, v23

    if-ge v9, v0, :cond_20

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lineLength:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v31

    if-eqz v31, :cond_0

    .line 233
    move/from16 v14, p5

    .line 235
    :cond_0
    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .line 237
    .local v21, "run":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    move-object/from16 v0, v21

    instance-of v0, v0, Lcom/samsung/thumbnail/customview/word/Run;

    move/from16 v31, v0

    if-eqz v31, :cond_18

    move-object/from16 v27, v21

    .line 239
    check-cast v27, Lcom/samsung/thumbnail/customview/word/Run;

    .line 240
    .local v27, "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v10

    .line 241
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v26

    .line 243
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/word/Run;->hasTab()Z

    move-result v31

    if-eqz v31, :cond_1

    .line 244
    if-eqz v25, :cond_4

    .line 245
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_1
    move/from16 v0, v25

    if-ge v11, v0, :cond_1

    .line 246
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTabContents()Ljava/util/ArrayList;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/samsung/thumbnail/customview/hslf/TabValues;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/hslf/TabValues;->getPosition()I

    move-result v31

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    cmpg-float v31, v32, v31

    if-gez v31, :cond_3

    .line 248
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTabContents()Ljava/util/ArrayList;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/samsung/thumbnail/customview/hslf/TabValues;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/hslf/TabValues;->getPosition()I

    move-result v31

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 250
    const/16 v31, 0x1

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setHasTab(Z)V

    .line 251
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTabContents()Ljava/util/ArrayList;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/samsung/thumbnail/customview/hslf/TabValues;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/hslf/TabValues;->getPosition()I

    move-result v31

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setTabIndex(F)V

    .line 262
    .end local v11    # "j":I
    :cond_1
    :goto_2
    const/4 v12, 0x0

    .line 264
    .local v12, "length":I
    if-eqz v10, :cond_2

    .line 265
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v12

    .line 270
    :cond_2
    const/16 v31, -0x1

    move/from16 v0, v31

    if-ne v14, v0, :cond_5

    .line 271
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v32

    new-array v0, v0, [I

    move-object/from16 v32, v0

    const/16 v33, 0x0

    const/16 v34, 0x0

    aput v34, v32, v33

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 272
    const/16 v31, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v31

    invoke-virtual {v0, v10, v1, v12}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v31

    const/high16 v32, 0x3f000000    # 0.5f

    add-float v31, v31, v32

    move/from16 v0, v31

    float-to-int v0, v0

    move/from16 v31, v0

    .line 546
    .end local v12    # "length":I
    .end local v21    # "run":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .end local v27    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    :goto_3
    return v31

    .line 245
    .restart local v11    # "j":I
    .restart local v21    # "run":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .restart local v27    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 257
    .end local v11    # "j":I
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v31, v0

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getDefTabSpacing()I

    move-result v32

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v32, v0

    add-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 258
    const/16 v31, 0x1

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setHasTab(Z)V

    .line 259
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getDefTabSpacing()I

    move-result v31

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setTabIndex(F)V

    goto :goto_2

    .line 277
    .restart local v12    # "length":I
    :cond_5
    if-eqz p2, :cond_6

    .line 278
    move-object/from16 v0, v26

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLength:F

    .line 280
    :cond_6
    if-eqz p3, :cond_7

    .line 281
    move-object/from16 v0, v26

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLengthMore:F

    .line 284
    :cond_7
    if-eqz v10, :cond_b

    if-eqz v26, :cond_b

    .line 287
    const/16 v31, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v31

    invoke-static {v0, v10, v1, v12}, Lcom/samsung/thumbnail/customview/word/Run;->getTextWidths(Landroid/text/TextPaint;Ljava/lang/String;II)[F

    move-result-object v30

    .line 289
    .local v30, "widths":[F
    const/16 v18, 0x0

    .line 290
    if-eqz v30, :cond_8

    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v31, v0

    if-nez v31, :cond_8

    .line 291
    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setEndIndex(I)V

    .line 292
    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 293
    invoke-virtual {v13, v8}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mlineElement:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 295
    new-instance v13, Lcom/samsung/thumbnail/customview/word/ParaLine;

    .end local v13    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-direct {v13}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 296
    .restart local v13    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/16 v31, 0x1

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsText(Z)V

    .line 297
    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    .line 300
    :cond_8
    :goto_4
    move/from16 v0, v18

    if-ge v0, v12, :cond_a

    .line 302
    const/16 v31, -0x1

    move/from16 v0, v19

    move/from16 v1, v31

    if-ne v0, v1, :cond_9

    .line 303
    move/from16 v19, v28

    .line 306
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v31

    move/from16 v0, v31

    move/from16 v1, p4

    if-ne v0, v1, :cond_c

    .line 307
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mIsEllipsisRequired:Z

    .line 461
    :cond_a
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mIsEllipsisRequired:Z

    move/from16 v31, v0

    if-eqz v31, :cond_b

    .line 462
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/util/ArrayList;->size()I

    move-result v32

    add-int/lit8 v32, v32, -0x1

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, [I

    .line 463
    .local v16, "pairLast":[I
    const/16 v31, 0x0

    aget v31, v16, v31

    const/16 v32, 0x1

    aget v32, v16, v32

    add-int/lit8 v32, v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v24

    .line 465
    .local v24, "subSequence":Ljava/lang/CharSequence;
    const/16 v31, 0x0

    invoke-interface/range {v24 .. v24}, Ljava/lang/CharSequence;->length()I

    move-result v32

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    move/from16 v2, v31

    move/from16 v3, v32

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mLastLineLength:F

    .line 230
    .end local v12    # "length":I
    .end local v16    # "pairLast":[I
    .end local v21    # "run":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .end local v24    # "subSequence":Ljava/lang/CharSequence;
    .end local v27    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v30    # "widths":[F
    :cond_b
    :goto_6
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 311
    .restart local v12    # "length":I
    .restart local v21    # "run":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .restart local v27    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    .restart local v30    # "widths":[F
    :cond_c
    aget v29, v30, v18

    .line 312
    .local v29, "widthOfChar":F
    const/4 v15, 0x0

    .line 316
    .local v15, "newLineRequired":Z
    move/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/String;->charAt(I)C

    move-result v31

    const/16 v32, 0xa

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_f

    .line 317
    const/4 v15, 0x1

    .line 326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    const/16 v32, 0x2

    move/from16 v0, v32

    new-array v0, v0, [I

    move-object/from16 v32, v0

    const/16 v33, 0x0

    aput v19, v32, v33

    const/16 v33, 0x1

    add-int/lit8 v34, v28, -0x1

    aput v34, v32, v33

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lineLength:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    invoke-static/range {v32 .. v32}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 329
    add-int/lit8 v8, v8, 0x1

    .line 330
    move/from16 v14, p5

    .line 427
    :cond_d
    :goto_7
    if-eqz v15, :cond_17

    .line 428
    const/16 v19, -0x1

    .line 430
    const/16 v31, 0x0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v31

    add-int/lit8 v32, p4, -0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_e

    .line 433
    int-to-float v0, v14

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLength:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLengthMore:F

    move/from16 v33, v0

    add-float v32, v32, v33

    sub-float v31, v31, v32

    move/from16 v0, v31

    float-to-int v14, v0

    .line 434
    const/4 v7, 0x0

    .line 456
    :cond_e
    :goto_8
    add-int/lit8 v18, v18, 0x1

    .line 457
    add-int/lit8 v28, v28, 0x1

    .line 459
    goto/16 :goto_4

    .line 331
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v31, v0

    add-float v31, v31, v29

    int-to-float v0, v14

    move/from16 v32, v0

    cmpl-float v31, v31, v32

    if-ltz v31, :cond_d

    .line 332
    const/4 v15, 0x1

    .line 335
    move/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/String;->charAt(I)C

    move-result v31

    const/16 v32, 0x20

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_10

    if-nez v7, :cond_11

    .line 345
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    const/16 v32, 0x2

    move/from16 v0, v32

    new-array v0, v0, [I

    move-object/from16 v32, v0

    const/16 v33, 0x0

    aput v19, v32, v33

    const/16 v33, 0x1

    aput v28, v32, v33

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lineLength:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    add-float v32, v32, v29

    invoke-static/range {v32 .. v32}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    move/from16 v14, p5

    .line 350
    move/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setEndIndex(I)V

    .line 351
    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 352
    invoke-virtual {v13, v8}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mlineElement:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    new-instance v13, Lcom/samsung/thumbnail/customview/word/ParaLine;

    .end local v13    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-direct {v13}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 355
    .restart local v13    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/16 v31, 0x1

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsText(Z)V

    .line 356
    add-int/lit8 v31, v18, 0x1

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    .line 357
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_7

    .line 360
    :cond_11
    const/4 v4, 0x0

    .line 361
    .local v4, "backWidth":F
    const/4 v5, 0x0

    .line 362
    .local v5, "backWidthIsMore":Z
    move/from16 v20, v28

    .line 363
    .local v20, "refPos":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v31, v0

    add-float v31, v31, v29

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 365
    :goto_9
    if-ltz v18, :cond_12

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/String;->charAt(I)C

    move-result v31

    const/16 v32, 0x20

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_12

    .line 367
    aget v31, v30, v18

    add-float v4, v4, v31

    .line 368
    int-to-float v0, v14

    move/from16 v31, v0

    cmpl-float v31, v4, v31

    if-ltz v31, :cond_13

    .line 370
    const/4 v5, 0x1

    .line 378
    :cond_12
    if-nez v5, :cond_14

    .line 379
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    const/16 v32, 0x2

    move/from16 v0, v32

    new-array v0, v0, [I

    move-object/from16 v32, v0

    const/16 v33, 0x0

    aput v19, v32, v33

    const/16 v33, 0x1

    aput v28, v32, v33

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 381
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lineLength:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    sub-float v32, v32, v4

    invoke-static/range {v32 .. v32}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 383
    move/from16 v14, p5

    .line 385
    move/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setEndIndex(I)V

    .line 386
    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 387
    invoke-virtual {v13, v8}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mlineElement:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 389
    new-instance v13, Lcom/samsung/thumbnail/customview/word/ParaLine;

    .end local v13    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-direct {v13}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 390
    .restart local v13    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/16 v31, 0x1

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsText(Z)V

    .line 391
    add-int/lit8 v31, v18, 0x1

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    .line 392
    add-int/lit8 v8, v8, 0x1

    .line 420
    :goto_a
    const/16 v31, 0x1

    move/from16 v0, p4

    move/from16 v1, v31

    if-ne v0, v1, :cond_d

    goto/16 :goto_5

    .line 373
    :cond_13
    add-int/lit8 v18, v18, -0x1

    .line 374
    add-int/lit8 v28, v28, -0x1

    goto/16 :goto_9

    .line 394
    :cond_14
    sub-int v31, v19, v28

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-gt v0, v1, :cond_16

    .line 397
    if-gtz v18, :cond_15

    .line 398
    sub-int v20, v20, v28

    .line 399
    move/from16 v28, v20

    .line 402
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    const/16 v32, 0x2

    move/from16 v0, v32

    new-array v0, v0, [I

    move-object/from16 v32, v0

    const/16 v33, 0x0

    aput v19, v32, v33

    const/16 v33, 0x1

    aput v20, v32, v33

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lineLength:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    invoke-static/range {v32 .. v32}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 405
    move/from16 v14, p5

    .line 406
    move/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setEndIndex(I)V

    .line 407
    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 408
    invoke-virtual {v13, v8}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mlineElement:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 410
    new-instance v13, Lcom/samsung/thumbnail/customview/word/ParaLine;

    .end local v13    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-direct {v13}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 411
    .restart local v13    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/16 v31, 0x1

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsText(Z)V

    .line 412
    move/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    .line 413
    add-int/lit8 v8, v8, 0x1

    .line 415
    :cond_16
    move/from16 v28, v20

    .line 416
    move/from16 v18, v28

    .line 417
    const/16 v20, 0x0

    .line 418
    const/4 v15, 0x1

    goto :goto_a

    .line 437
    .end local v4    # "backWidth":F
    .end local v5    # "backWidthIsMore":Z
    .end local v20    # "refPos":I
    :cond_17
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v31, v0

    add-float v31, v31, v29

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 439
    add-int/lit8 v31, v12, -0x1

    move/from16 v0, v18

    move/from16 v1, v31

    if-ne v0, v1, :cond_e

    .line 440
    add-int/lit8 v28, v28, 0x1

    .line 441
    add-int/lit8 v18, v18, 0x1

    .line 442
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    const/16 v32, 0x2

    move/from16 v0, v32

    new-array v0, v0, [I

    move-object/from16 v32, v0

    const/16 v33, 0x0

    aput v19, v32, v33

    const/16 v33, 0x1

    aput v28, v32, v33

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445
    move/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setEndIndex(I)V

    .line 446
    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 447
    invoke-virtual {v13, v8}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mlineElement:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    new-instance v13, Lcom/samsung/thumbnail/customview/word/ParaLine;

    .end local v13    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-direct {v13}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 450
    .restart local v13    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/16 v31, 0x1

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsText(Z)V

    .line 451
    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-virtual {v13, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    goto/16 :goto_8

    .line 469
    .end local v12    # "length":I
    .end local v15    # "newLineRequired":Z
    .end local v27    # "textRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v29    # "widthOfChar":F
    .end local v30    # "widths":[F
    :cond_18
    move-object/from16 v0, v21

    instance-of v0, v0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    move/from16 v31, v0

    if-eqz v31, :cond_1a

    move-object/from16 v6, v21

    .line 470
    check-cast v6, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .line 471
    .local v6, "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isInLine()Z

    move-result v31

    if-eqz v31, :cond_b

    .line 472
    new-instance v17, Lcom/samsung/thumbnail/customview/word/ParaLine;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 473
    .local v17, "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/16 v31, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsBitmap(Z)V

    .line 474
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v31, v0

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v32

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v32, v0

    add-float v31, v31, v32

    int-to-float v0, v14

    move/from16 v32, v0

    cmpl-float v31, v31, v32

    if-ltz v31, :cond_19

    .line 475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lineLength:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    invoke-static/range {v32 .. v32}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 476
    move/from16 v14, p5

    .line 477
    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v31

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 478
    add-int/lit8 v8, v8, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 483
    :goto_b
    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mlineElement:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 480
    :cond_19
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v31, v0

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v32

    move/from16 v0, v32

    int-to-float v0, v0

    move/from16 v32, v0

    add-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 481
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    goto :goto_b

    .line 486
    .end local v6    # "bc":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v17    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :cond_1a
    move-object/from16 v0, v21

    instance-of v0, v0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    move/from16 v31, v0

    if-eqz v31, :cond_1c

    .line 487
    new-instance v17, Lcom/samsung/thumbnail/customview/word/ParaLine;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 488
    .restart local v17    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/16 v31, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsChart(Z)V

    .line 489
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    move-object/from16 v31, v21

    check-cast v31, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v31

    add-float v31, v31, v32

    int-to-float v0, v14

    move/from16 v32, v0

    cmpl-float v31, v31, v32

    if-ltz v31, :cond_1b

    .line 490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lineLength:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    invoke-static/range {v32 .. v32}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 491
    move/from16 v14, p5

    move-object/from16 v31, v21

    .line 492
    check-cast v31, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 493
    add-int/lit8 v8, v8, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 498
    :goto_c
    check-cast v21, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    .end local v21    # "run":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 499
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mlineElement:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 495
    .restart local v21    # "run":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    :cond_1b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    move-object/from16 v31, v21

    check-cast v31, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v31

    add-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 496
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    goto :goto_c

    .line 500
    .end local v17    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :cond_1c
    move-object/from16 v0, v21

    instance-of v0, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;

    move/from16 v31, v0

    if-eqz v31, :cond_1e

    move-object/from16 v31, v21

    .line 501
    check-cast v31, Lcom/samsung/thumbnail/customview/word/ShapesController;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getZorder()J

    move-result-wide v32

    const-wide/16 v34, 0x0

    cmp-long v31, v32, v34

    if-nez v31, :cond_b

    .line 502
    new-instance v17, Lcom/samsung/thumbnail/customview/word/ParaLine;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 503
    .restart local v17    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/16 v31, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsShape(Z)V

    .line 504
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    move-object/from16 v31, v21

    check-cast v31, Lcom/samsung/thumbnail/customview/word/ShapesController;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v31

    add-float v31, v31, v32

    int-to-float v0, v14

    move/from16 v32, v0

    cmpl-float v31, v31, v32

    if-ltz v31, :cond_1d

    .line 506
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lineLength:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    invoke-static/range {v32 .. v32}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 507
    move/from16 v14, p5

    move-object/from16 v31, v21

    .line 508
    check-cast v31, Lcom/samsung/thumbnail/customview/word/ShapesController;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 510
    add-int/lit8 v8, v8, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 516
    :goto_d
    check-cast v21, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v21    # "run":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mlineElement:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 512
    .restart local v21    # "run":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    move-object/from16 v31, v21

    check-cast v31, Lcom/samsung/thumbnail/customview/word/ShapesController;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v31

    add-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 514
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    goto :goto_d

    .line 519
    .end local v17    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :cond_1e
    move-object/from16 v0, v21

    instance-of v0, v0, Lcom/samsung/thumbnail/customview/word/DiagramController;

    move/from16 v31, v0

    if-eqz v31, :cond_b

    .line 520
    new-instance v17, Lcom/samsung/thumbnail/customview/word/ParaLine;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 521
    .restart local v17    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/16 v31, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsDiagram(Z)V

    .line 522
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    move-object/from16 v31, v21

    check-cast v31, Lcom/samsung/thumbnail/customview/word/DiagramController;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/word/DiagramController;->getWidth()I

    move-result v31

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    add-float v31, v31, v32

    int-to-float v0, v14

    move/from16 v32, v0

    cmpl-float v31, v31, v32

    if-ltz v31, :cond_1f

    .line 523
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lineLength:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    invoke-static/range {v32 .. v32}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 524
    move/from16 v14, p5

    move-object/from16 v31, v21

    .line 525
    check-cast v31, Lcom/samsung/thumbnail/customview/word/DiagramController;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/word/DiagramController;->getWidth()I

    move-result v31

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 526
    add-int/lit8 v8, v8, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 531
    :goto_e
    check-cast v21, Lcom/samsung/thumbnail/customview/word/DiagramController;

    .end local v21    # "run":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 532
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mlineElement:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 528
    .restart local v21    # "run":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    :cond_1f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    move-object/from16 v31, v21

    check-cast v31, Lcom/samsung/thumbnail/customview/word/DiagramController;

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/word/DiagramController;->getWidth()I

    move-result v31

    move/from16 v0, v31

    int-to-float v0, v0

    move/from16 v31, v0

    add-float v31, v31, v32

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    .line 529
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    goto :goto_e

    .line 536
    .end local v17    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .end local v21    # "run":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lineLength:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lengthOfThisLine:F

    move/from16 v32, v0

    invoke-static/range {v32 .. v32}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 537
    move/from16 v14, p5

    .line 540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v31

    if-nez v31, :cond_21

    .line 541
    const/16 v31, 0x0

    goto/16 :goto_3

    .line 542
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v31

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_22

    .line 544
    const/16 v31, 0x0

    goto/16 :goto_3

    :cond_22
    move/from16 v31, v14

    .line 546
    goto/16 :goto_3
.end method

.method public getLengthEllipsisMore()F
    .locals 1

    .prologue
    .line 784
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLengthMore:F

    return v0
.end method

.method public getLengthLastEllipsizedLine()F
    .locals 1

    .prologue
    .line 776
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mLastLineLength:F

    return v0
.end method

.method public getLengthLastEllipsizedLinePlusEllipsis()F
    .locals 2

    .prologue
    .line 780
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mLastLineLength:F

    iget v1, p0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mEllipsisLength:F

    add-float/2addr v0, v1

    return v0
.end method

.method public getLineElementOfPara()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/ParaLine;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mlineElement:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLineLengthOfPara()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->lineLength:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLines()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<[I>;"
        }
    .end annotation

    .prologue
    .line 772
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mNoOfLines:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRequiredEllipsis()Z
    .locals 1

    .prologue
    .line 768
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/LineBreaker;->mIsEllipsisRequired:Z

    return v0
.end method
