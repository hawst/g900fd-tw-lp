.class public Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
.super Lcom/samsung/thumbnail/customview/CanvasElementPart;
.source "DocParagraph.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/customview/hslf/DocParagraph$1;,
        Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;,
        Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;,
        Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;,
        Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;
    }
.end annotation


# static fields
.field private static final mMinBottomSpacing:I = 0x2


# instance fields
.field private backgroundColor:Ljava/lang/String;

.field private bottomBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

.field private bulletChar:Ljava/lang/String;

.field private bulletPaint:Landroid/text/TextPaint;

.field private bulletSpaceAdded:Z

.field private bulletXPos:F

.field private firstLineMargin:F

.field private leftBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

.field private leftMargin:F

.field private lineRule:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

.field private mAlignment:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

.field private mBottomSpacing:F

.field private mContentArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/CanvasElementPart;",
            ">;"
        }
    .end annotation
.end field

.field private mDefTabSpacing:I

.field private mLineSpacing:F

.field private mParaType:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

.field private mParentType:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

.field private mTabSpacing:I

.field private mTabValueArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/TabValues;",
            ">;"
        }
    .end annotation
.end field

.field private mTopSpacing:F

.field private paraNumber:I

.field private paraTextHeight:I

.field private pgBrBefStatus:Z

.field private rightBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

.field private rightMargin:F

.field private topBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

.field private x:I

.field private y:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 266
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 54
    const/16 v0, 0x2d0

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mDefTabSpacing:I

    .line 69
    iput v1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    iput v1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 72
    const-string/jumbo v0, "\u2022"

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->bulletChar:Ljava/lang/String;

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->bulletXPos:F

    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->paraTextHeight:I

    .line 267
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mContentArray:Ljava/util/ArrayList;

    .line 268
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mTabValueArray:Ljava/util/ArrayList;

    .line 269
    const v0, 0x3f933333    # 1.15f

    iput v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mLineSpacing:F

    .line 270
    sget-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;->AUTO:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->lineRule:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    .line 271
    return-void
.end method

.method private calculateLineSpacing(FLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)F
    .locals 12
    .param p1, "fontSize"    # F
    .param p2, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    const-wide/high16 v10, 0x3ff8000000000000L    # 1.5

    const-wide v8, 0x3ff2666666666666L    # 1.15

    const/high16 v6, 0x40000000    # 2.0f

    .line 804
    const/4 v1, 0x0

    .line 807
    .local v1, "lineSpaceValue":F
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineRule()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    move-result-object v0

    .line 808
    .local v0, "lineRule":Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;
    if-nez v0, :cond_0

    .line 811
    div-float v3, p1, v6

    float-to-double v4, v3

    mul-double/2addr v4, v8

    double-to-float v1, v4

    move v2, v1

    .line 843
    .end local v1    # "lineSpaceValue":F
    .local v2, "lineSpaceValue":F
    :goto_0
    return v2

    .line 815
    .end local v2    # "lineSpaceValue":F
    .restart local v1    # "lineSpaceValue":F
    :cond_0
    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$1;->$SwitchMap$com$samsung$thumbnail$customview$hslf$DocParagraph$LineRuleType:[I

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 840
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v3

    div-float v4, p1, v6

    mul-float v1, v3, v4

    :cond_1
    :goto_1
    move v2, v1

    .line 843
    .end local v1    # "lineSpaceValue":F
    .restart local v2    # "lineSpaceValue":F
    goto :goto_0

    .line 817
    .end local v2    # "lineSpaceValue":F
    .restart local v1    # "lineSpaceValue":F
    :pswitch_0
    div-float v3, p1, v6

    float-to-double v4, v3

    mul-double/2addr v4, v8

    double-to-float v1, v4

    .line 818
    goto :goto_1

    .line 820
    :pswitch_1
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v3

    float-to-double v4, v3

    cmpg-double v3, v4, v10

    if-gez v3, :cond_2

    .line 821
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v3

    div-float v4, p1, v6

    mul-float v1, v3, v4

    goto :goto_1

    .line 822
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v3

    float-to-double v4, v3

    cmpl-double v3, v4, v10

    if-ltz v3, :cond_1

    .line 823
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v3

    mul-float v1, v3, p1

    goto :goto_1

    .line 827
    :pswitch_2
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v3

    const/high16 v4, 0x41400000    # 12.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    .line 828
    invoke-virtual {p2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    const-wide/high16 v4, 0x4028000000000000L    # 12.0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v6

    goto :goto_1

    .line 831
    :cond_3
    invoke-virtual {p2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v4

    float-to-double v4, v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v6

    .line 833
    goto :goto_1

    .line 835
    :pswitch_3
    invoke-virtual {p2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getLineSpacing()F

    move-result v4

    float-to-double v4, v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjHeight(D)I

    move-result v3

    int-to-float v1, v3

    .line 837
    goto :goto_1

    .line 815
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private drawCurrentLine(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IFZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 47
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "rect"    # Landroid/graphics/Rect;
    .param p5, "paraWidth"    # I
    .param p6, "maxFontSize"    # F
    .param p7, "isHidden"    # Z
    .param p8, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/ParaLine;",
            ">;",
            "Landroid/graphics/Canvas;",
            "Landroid/content/Context;",
            "Landroid/graphics/Rect;",
            "IFZ",
            "Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;",
            ")V"
        }
    .end annotation

    .prologue
    .line 871
    .local p1, "currentLineArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    .line 874
    .local v11, "x":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 877
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v46

    .line 878
    .local v46, "widthIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    const/16 v45, 0x0

    .line 879
    .local v45, "totalCharLength":F
    :cond_1
    :goto_0
    invoke-interface/range {v46 .. v46}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 881
    :try_start_0
    invoke-interface/range {v46 .. v46}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/thumbnail/customview/word/ParaLine;

    .line 882
    .local v30, "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isText()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 883
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v32

    check-cast v32, Lcom/samsung/thumbnail/customview/word/Run;

    .line 886
    .local v32, "currentRun":Lcom/samsung/thumbnail/customview/word/Run;
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getStartIndex()I

    move-result v43

    .line 887
    .local v43, "startIndex":I
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getEndIndex()I

    move-result v34

    .line 888
    .local v34, "endIndex":I
    move/from16 v0, v34

    move/from16 v1, v43

    if-eq v0, v1, :cond_1

    .line 892
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v3

    move/from16 v0, v43

    move/from16 v1, v34

    invoke-static {v2, v3, v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->getTextWidths(Landroid/text/TextPaint;Ljava/lang/String;II)[F

    move-result-object v31

    .line 903
    .local v31, "currentLineWidthArray":[F
    const/16 v38, 0x0

    .local v38, "i":I
    :goto_1
    move-object/from16 v0, v31

    array-length v2, v0

    move/from16 v0, v38

    if-ge v0, v2, :cond_1

    .line 904
    aget v2, v31, v38

    add-float v45, v45, v2

    .line 903
    add-int/lit8 v38, v38, 0x1

    goto :goto_1

    .line 907
    .end local v31    # "currentLineWidthArray":[F
    .end local v32    # "currentRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v34    # "endIndex":I
    .end local v38    # "i":I
    .end local v43    # "startIndex":I
    :cond_2
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isBitmap()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 908
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v2

    int-to-float v2, v2

    add-float v45, v45, v2

    goto :goto_0

    .line 910
    :cond_3
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isShape()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 911
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v2

    add-float v45, v45, v2

    goto :goto_0

    .line 913
    :cond_4
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isChart()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 914
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v2

    add-float v45, v45, v2

    goto/16 :goto_0

    .line 916
    :cond_5
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isDiagram()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 917
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/DiagramController;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/DiagramController;->getWidth()I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    int-to-float v2, v2

    add-float v45, v45, v2

    goto/16 :goto_0

    .line 921
    .end local v30    # "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :catch_0
    move-exception v33

    .line 922
    .local v33, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v33 .. v33}, Ljava/lang/ArrayIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 925
    .end local v33    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 926
    int-to-float v2, v11

    move/from16 v0, p5

    int-to-float v3, v0

    sub-float v3, v3, v45

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v11, v2

    .line 932
    .end local v45    # "totalCharLength":F
    .end local v46    # "widthIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    :cond_7
    :goto_2
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v39

    .line 933
    .local v39, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    const/16 v36, 0x1

    .line 934
    .local v36, "firstRun":Z
    :cond_8
    :goto_3
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_27

    .line 936
    :try_start_1
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/samsung/thumbnail/customview/word/ParaLine;

    .line 937
    .restart local v30    # "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isText()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 939
    const-string/jumbo v44, ""

    .line 940
    .local v44, "textInLine":Ljava/lang/String;
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_9

    .line 942
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getStartIndex()I

    move-result v3

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getEndIndex()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v44

    .line 946
    if-eqz v36, :cond_9

    .line 948
    const/16 v36, 0x0

    .line 952
    :cond_9
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getStartIndex()I

    move-result v4

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getEndIndex()I

    move-result v5

    invoke-virtual {v3, v2, v4, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v35

    .line 960
    .local v35, "endXpos":F
    if-nez p7, :cond_10

    .line 961
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-eq v2, v3, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v2, v3, :cond_b

    .line 963
    :cond_a
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v2

    const/16 v3, 0x4d

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 966
    :cond_b
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getStartIndex()I

    move-result v2

    if-nez v2, :cond_c

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->hasTab()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 968
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getTabIndex()F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, p4

    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v3

    if-ge v11, v2, :cond_12

    .line 969
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getTabIndex()F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, p4

    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int v11, v2, v3

    .line 974
    :cond_c
    :goto_4
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Run;->getVerticalalignment()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUPERSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    if-ne v2, v3, :cond_d

    .line 976
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, p6, v3

    float-to-int v3, v3

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 980
    :cond_d
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Run;->getShadingColor()Ljava/lang/String;

    move-result-object v42

    .line 983
    .local v42, "shadeColor":Ljava/lang/String;
    if-eqz v42, :cond_e

    invoke-virtual/range {v44 .. v44}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_e

    .line 985
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 986
    .local v7, "p":Landroid/graphics/Paint;
    invoke-static/range {v42 .. v42}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->stringToColor(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v29

    .line 988
    .local v29, "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 990
    int-to-float v3, v11

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    sub-float v4, v2, p6

    int-to-float v2, v11

    add-float v5, v2, v35

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, p6, v6

    add-float/2addr v6, v2

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 996
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v29    # "color":Lorg/apache/poi/java/awt/Color;
    :cond_e
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/Run;->getHighLightedColorKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/word/Run;->getHighLightedColor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 1002
    .local v37, "highlightedColor":Ljava/lang/String;
    if-eqz v37, :cond_f

    invoke-virtual/range {v44 .. v44}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_f

    .line 1004
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1005
    .restart local v7    # "p":Landroid/graphics/Paint;
    invoke-static/range {v37 .. v37}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->stringToColor(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v29

    .line 1007
    .restart local v29    # "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual/range {v29 .. v29}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1009
    int-to-float v3, v11

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    sub-float v4, v2, p6

    int-to-float v2, v11

    add-float v5, v2, v35

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, p6, v6

    add-float/2addr v6, v2

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1013
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v29    # "color":Lorg/apache/poi/java/awt/Color;
    :cond_f
    int-to-float v3, v11

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v4, v2

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v2

    move-object/from16 v0, p2

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v3, v4, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1016
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Run;->getVerticalalignment()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUPERSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    if-ne v2, v3, :cond_10

    .line 1018
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, p6, v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_3

    .line 1021
    .end local v37    # "highlightedColor":Ljava/lang/String;
    .end local v42    # "shadeColor":Ljava/lang/String;
    :cond_10
    int-to-float v2, v11

    add-float v2, v2, v35

    float-to-int v11, v2

    .line 1022
    goto/16 :goto_3

    .line 927
    .end local v30    # "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .end local v35    # "endXpos":F
    .end local v36    # "firstRun":Z
    .end local v39    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .end local v44    # "textInLine":Ljava/lang/String;
    .restart local v45    # "totalCharLength":F
    .restart local v46    # "widthIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 928
    int-to-float v2, v11

    move/from16 v0, p5

    int-to-float v3, v0

    sub-float v3, v3, v45

    add-float/2addr v2, v3

    float-to-int v11, v2

    goto/16 :goto_2

    .line 971
    .end local v45    # "totalCharLength":F
    .end local v46    # "widthIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v30    # "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .restart local v35    # "endXpos":F
    .restart local v36    # "firstRun":Z
    .restart local v39    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v44    # "textInLine":Ljava/lang/String;
    :cond_12
    :try_start_2
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getTabIndex()F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v11, v2

    goto/16 :goto_4

    .line 1022
    .end local v35    # "endXpos":F
    .end local v44    # "textInLine":Ljava/lang/String;
    :cond_13
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isBitmap()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 1023
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v27

    check-cast v27, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .line 1026
    .local v27, "bpContent":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v2

    if-eqz v2, :cond_8

    .line 1027
    const/16 v41, 0x0

    .line 1030
    .local v41, "scalableBitmap":Landroid/graphics/Bitmap;
    :try_start_3
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v3

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v4

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v41

    .line 1038
    :goto_5
    :try_start_4
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v2

    if-eqz v2, :cond_14

    .line 1039
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Canvas;->save()I

    .line 1040
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v11

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    int-to-float v4, v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1046
    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    move/from16 v40, v0

    .line 1047
    .local v40, "preVal":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v3

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 1049
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1050
    .restart local v7    # "p":Landroid/graphics/Paint;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-eq v2, v3, :cond_15

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v2, v3, :cond_17

    .line 1052
    :cond_15
    const/16 v2, 0x7f

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1053
    const/4 v2, -0x1

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1055
    if-eqz v41, :cond_16

    .line 1056
    int-to-float v3, v11

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v4, v2

    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    add-int/2addr v2, v11

    int-to-float v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    add-int/2addr v2, v6

    int-to-float v6, v2

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1059
    :cond_16
    const/16 v2, 0x32

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1062
    :cond_17
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_18

    const-string/jumbo v2, "rect"

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 1065
    :cond_18
    if-eqz v41, :cond_1b

    .line 1066
    int-to-float v2, v11

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v3, v3

    move-object/from16 v0, p2

    move-object/from16 v1, v41

    invoke-virtual {v0, v1, v2, v3, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1093
    :cond_19
    :goto_6
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getRotationAngle()I
    :try_end_4
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_3

    move-result v2

    if-eqz v2, :cond_1a

    .line 1095
    :try_start_5
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Canvas;->restore()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1102
    :cond_1a
    :goto_7
    :try_start_6
    move/from16 v0, v40

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 1103
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v2

    add-int/2addr v11, v2

    goto/16 :goto_3

    .line 1034
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v40    # "preVal":I
    :catch_1
    move-exception v33

    .line 1035
    .local v33, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v33 .. v33}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_5

    .line 1225
    .end local v27    # "bpContent":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v30    # "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .end local v33    # "e":Ljava/lang/Exception;
    .end local v41    # "scalableBitmap":Landroid/graphics/Bitmap;
    :catch_2
    move-exception v33

    .line 1226
    .local v33, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v33 .. v33}, Ljava/lang/ArrayIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1068
    .end local v33    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v7    # "p":Landroid/graphics/Paint;
    .restart local v27    # "bpContent":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .restart local v30    # "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .restart local v40    # "preVal":I
    .restart local v41    # "scalableBitmap":Landroid/graphics/Bitmap;
    :cond_1b
    :try_start_7
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v2

    int-to-float v3, v11

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v4, v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_7
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_6

    .line 1227
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v27    # "bpContent":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v30    # "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .end local v40    # "preVal":I
    .end local v41    # "scalableBitmap":Landroid/graphics/Bitmap;
    :catch_3
    move-exception v33

    .line 1228
    .local v33, "e":Ljava/lang/StringIndexOutOfBoundsException;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1072
    .end local v33    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    .restart local v7    # "p":Landroid/graphics/Paint;
    .restart local v27    # "bpContent":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .restart local v30    # "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .restart local v40    # "preVal":I
    .restart local v41    # "scalableBitmap":Landroid/graphics/Bitmap;
    :cond_1c
    :try_start_8
    const-string/jumbo v2, "ellipse"

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getEnclosedSpName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 1074
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1075
    if-eqz v41, :cond_1d

    .line 1076
    new-instance v2, Landroid/graphics/BitmapShader;

    sget-object v3, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v4, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    move-object/from16 v0, v41

    invoke-direct {v2, v0, v3, v4}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1086
    :goto_8
    int-to-float v2, v11

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v3, v3

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_6

    .line 1081
    :cond_1d
    new-instance v2, Landroid/graphics/BitmapShader;

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getBitMap()Landroid/graphics/Bitmap;

    move-result-object v3

    sget-object v4, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v5, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-direct {v2, v3, v4, v5}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto :goto_8

    .line 1096
    :catch_4
    move-exception v33

    .line 1097
    .local v33, "e":Ljava/lang/Exception;
    const-string/jumbo v2, "DocumentService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v33 .. v33}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 1108
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v27    # "bpContent":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v33    # "e":Ljava/lang/Exception;
    .end local v40    # "preVal":I
    .end local v41    # "scalableBitmap":Landroid/graphics/Bitmap;
    :cond_1e
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isShape()Z

    move-result v2

    if-eqz v2, :cond_21

    .line 1109
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v8

    check-cast v8, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .line 1111
    .local v8, "shapeCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    move/from16 v40, v0

    .line 1112
    .restart local v40    # "preVal":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 1113
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-eq v2, v3, :cond_1f

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v2, v3, :cond_20

    .line 1115
    :cond_1f
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    const/4 v13, 0x1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v14, p8

    invoke-virtual/range {v8 .. v14}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawShape(Landroid/graphics/Canvas;Landroid/content/Context;IIZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1120
    :goto_9
    move/from16 v0, v40

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 1121
    int-to-float v2, v11

    invoke-virtual {v8}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v3

    add-float/2addr v2, v3

    float-to-int v11, v2

    .line 1122
    goto/16 :goto_3

    .line 1118
    :cond_20
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    const/4 v13, 0x0

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v14, p8

    invoke-virtual/range {v8 .. v14}, Lcom/samsung/thumbnail/customview/word/ShapesController;->drawShape(Landroid/graphics/Canvas;Landroid/content/Context;IIZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    goto :goto_9

    .line 1122
    .end local v8    # "shapeCont":Lcom/samsung/thumbnail/customview/word/ShapesController;
    .end local v40    # "preVal":I
    :cond_21
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isChart()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1124
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v28

    check-cast v28, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    .line 1126
    .local v28, "chart":Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v2

    if-eqz v2, :cond_26

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 1128
    move/from16 v0, p5

    int-to-float v2, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v3

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v11, v2

    .line 1134
    :cond_22
    :goto_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    move/from16 v40, v0

    .line 1135
    .restart local v40    # "preVal":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 1136
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1137
    .restart local v7    # "p":Landroid/graphics/Paint;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->HEADER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-eq v2, v3, :cond_23

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-ne v2, v3, :cond_24

    .line 1139
    :cond_23
    const/16 v2, 0x7f

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1141
    :cond_24
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1220
    :cond_25
    :goto_b
    :pswitch_0
    move/from16 v0, v40

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 1221
    int-to-float v2, v11

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v3

    add-float/2addr v2, v3

    float-to-int v11, v2

    goto/16 :goto_3

    .line 1129
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v40    # "preVal":I
    :cond_26
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v2

    if-eqz v2, :cond_22

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 1131
    move/from16 v0, p5

    int-to-float v2, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v11, v2

    goto :goto_a

    .line 1144
    .restart local v7    # "p":Landroid/graphics/Paint;
    .restart local v40    # "preVal":I
    :pswitch_1
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getColumnBarChart()Lorg/achartengine/chart/AbstractChart;

    move-result-object v9

    .line 1145
    .local v9, "abChart":Lorg/achartengine/chart/AbstractChart;
    if-eqz v9, :cond_25

    .line 1146
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v2

    float-to-int v13, v2

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v2

    float-to-int v14, v2

    move-object/from16 v10, p2

    move-object v15, v7

    invoke-virtual/range {v9 .. v15}, Lorg/achartengine/chart/AbstractChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto :goto_b

    .line 1151
    .end local v9    # "abChart":Lorg/achartengine/chart/AbstractChart;
    :pswitch_2
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getDoughnutChart()Lorg/achartengine/chart/DoughnutChart;

    move-result-object v12

    .line 1152
    .local v12, "doughnutChart":Lorg/achartengine/chart/DoughnutChart;
    if-eqz v12, :cond_25

    .line 1153
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v2

    float-to-int v0, v2

    move/from16 v16, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v2

    float-to-int v0, v2

    move/from16 v17, v0

    move-object/from16 v13, p2

    move v14, v11

    move-object/from16 v18, v7

    invoke-virtual/range {v12 .. v18}, Lorg/achartengine/chart/DoughnutChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto :goto_b

    .line 1159
    .end local v12    # "doughnutChart":Lorg/achartengine/chart/DoughnutChart;
    :pswitch_3
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getPieChart()Lorg/achartengine/chart/PieChart;

    move-result-object v13

    .line 1160
    .local v13, "pieChart":Lorg/achartengine/chart/PieChart;
    if-eqz v13, :cond_25

    .line 1161
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    move/from16 v16, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v2

    float-to-int v0, v2

    move/from16 v17, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v2

    float-to-int v0, v2

    move/from16 v18, v0

    move-object/from16 v14, p2

    move v15, v11

    move-object/from16 v19, v7

    invoke-virtual/range {v13 .. v19}, Lorg/achartengine/chart/PieChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto/16 :goto_b

    .line 1166
    .end local v13    # "pieChart":Lorg/achartengine/chart/PieChart;
    :pswitch_4
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getScatterChart()Lorg/achartengine/chart/XYChart;

    move-result-object v14

    .line 1167
    .local v14, "scatterChart":Lorg/achartengine/chart/XYChart;
    if-eqz v14, :cond_25

    .line 1168
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    move/from16 v17, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v2

    float-to-int v0, v2

    move/from16 v18, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v2

    float-to-int v0, v2

    move/from16 v19, v0

    move-object/from16 v15, p2

    move/from16 v16, v11

    move-object/from16 v20, v7

    invoke-virtual/range {v14 .. v20}, Lorg/achartengine/chart/XYChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto/16 :goto_b

    .line 1173
    .end local v14    # "scatterChart":Lorg/achartengine/chart/XYChart;
    :pswitch_5
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getBubbleChart()Lorg/achartengine/chart/XYChart;

    move-result-object v15

    .line 1174
    .local v15, "bubbleChart":Lorg/achartengine/chart/XYChart;
    if-eqz v15, :cond_25

    .line 1175
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    move/from16 v18, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v2

    float-to-int v0, v2

    move/from16 v19, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v2

    float-to-int v0, v2

    move/from16 v20, v0

    move-object/from16 v16, p2

    move/from16 v17, v11

    move-object/from16 v21, v7

    invoke-virtual/range {v15 .. v21}, Lorg/achartengine/chart/XYChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto/16 :goto_b

    .line 1181
    .end local v15    # "bubbleChart":Lorg/achartengine/chart/XYChart;
    :pswitch_6
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getAreaChart()Lorg/achartengine/chart/TimeChart;

    move-result-object v16

    .line 1182
    .local v16, "timeChart":Lorg/achartengine/chart/TimeChart;
    if-eqz v16, :cond_25

    .line 1183
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    move/from16 v19, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v2

    float-to-int v0, v2

    move/from16 v20, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v2

    float-to-int v0, v2

    move/from16 v21, v0

    move-object/from16 v17, p2

    move/from16 v18, v11

    move-object/from16 v22, v7

    invoke-virtual/range {v16 .. v22}, Lorg/achartengine/chart/TimeChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto/16 :goto_b

    .line 1188
    .end local v16    # "timeChart":Lorg/achartengine/chart/TimeChart;
    :pswitch_7
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getLineChart()Lorg/achartengine/chart/XYChart;

    move-result-object v17

    .line 1189
    .local v17, "lineChart":Lorg/achartengine/chart/XYChart;
    if-eqz v17, :cond_25

    .line 1190
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    move/from16 v20, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v2

    float-to-int v0, v2

    move/from16 v21, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v2

    float-to-int v0, v2

    move/from16 v22, v0

    move-object/from16 v18, p2

    move/from16 v19, v11

    move-object/from16 v23, v7

    invoke-virtual/range {v17 .. v23}, Lorg/achartengine/chart/XYChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto/16 :goto_b

    .line 1195
    .end local v17    # "lineChart":Lorg/achartengine/chart/XYChart;
    :pswitch_8
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getRadarChart()Lorg/achartengine/chart/RadarChart;

    move-result-object v18

    .line 1196
    .local v18, "radarChart":Lorg/achartengine/chart/RadarChart;
    if-eqz v18, :cond_25

    .line 1197
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    move/from16 v21, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v2

    float-to-int v0, v2

    move/from16 v22, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v2

    float-to-int v0, v2

    move/from16 v23, v0

    move-object/from16 v19, p2

    move/from16 v20, v11

    move-object/from16 v24, v7

    invoke-virtual/range {v18 .. v24}, Lorg/achartengine/chart/RadarChart;->drawChart(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto/16 :goto_b

    .line 1202
    .end local v18    # "radarChart":Lorg/achartengine/chart/RadarChart;
    :pswitch_9
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getHorizontalBarChart()Lorg/achartengine/chart/ColumnChart;

    move-result-object v19

    .line 1204
    .local v19, "colChart":Lorg/achartengine/chart/ColumnChart;
    if-eqz v19, :cond_25

    .line 1205
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    move/from16 v22, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v2

    float-to-int v0, v2

    move/from16 v23, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v2

    float-to-int v0, v2

    move/from16 v24, v0

    move-object/from16 v20, p2

    move/from16 v21, v11

    move-object/from16 v25, v7

    invoke-virtual/range {v19 .. v25}, Lorg/achartengine/chart/ColumnChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    goto/16 :goto_b

    .line 1210
    .end local v19    # "colChart":Lorg/achartengine/chart/ColumnChart;
    :pswitch_a
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getStockChart()Lorg/achartengine/chart/RangeBarChart;

    move-result-object v20

    .line 1211
    .local v20, "stockChart":Lorg/achartengine/chart/RangeBarChart;
    if-eqz v20, :cond_25

    .line 1212
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    move/from16 v23, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v2

    float-to-int v0, v2

    move/from16 v24, v0

    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v2

    float-to-int v0, v2

    move/from16 v25, v0

    move-object/from16 v21, p2

    move/from16 v22, v11

    move-object/from16 v26, v7

    invoke-virtual/range {v20 .. v26}, Lorg/achartengine/chart/RangeBarChart;->draw(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V
    :try_end_8
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_b

    .line 1231
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v20    # "stockChart":Lorg/achartengine/chart/RangeBarChart;
    .end local v28    # "chart":Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    .end local v30    # "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .end local v40    # "preVal":I
    :cond_27
    return-void

    .line 1141
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_a
        :pswitch_8
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method public DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILcom/samsung/thumbnail/customview/word/LineBreaker;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 24
    .param p1, "paragraph"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "rect"    # Landroid/graphics/Rect;
    .param p5, "paraWidth"    # I
    .param p6, "firstLineWidth"    # I
    .param p7, "lb"    # Lcom/samsung/thumbnail/customview/word/LineBreaker;
    .param p8, "isHidden"    # Z
    .param p9, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 677
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 678
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 680
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 681
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    .line 683
    :cond_1
    const/4 v13, 0x0

    .line 685
    .local v13, "lineWidth":I
    invoke-virtual/range {p7 .. p7}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v23

    .line 686
    .local v23, "size":I
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_0
    move/from16 v0, v18

    move/from16 v1, v23

    if-ge v0, v1, :cond_f

    .line 688
    if-nez v18, :cond_4

    .line 689
    move/from16 v13, p6

    .line 694
    :goto_1
    const/4 v14, 0x0

    .line 695
    .local v14, "maxFontSize":F
    const/16 v20, 0x0

    .line 696
    .local v20, "maxImgHeight":F
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 697
    .local v9, "currentLineRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    invoke-virtual/range {p7 .. p7}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineElementOfPara()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .line 698
    .local v19, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    :cond_2
    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 699
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/thumbnail/customview/word/ParaLine;

    .line 700
    .local v21, "paraLineItem":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getLineNumber()I

    move-result v2

    move/from16 v0, v18

    if-ne v2, v0, :cond_2

    .line 701
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isText()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 702
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/TextPaint;->getTextSize()F

    move-result v2

    cmpg-float v2, v14, v2

    if-gtz v2, :cond_3

    .line 705
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/TextPaint;->getTextSize()F

    move-result v14

    .line 735
    :cond_3
    :goto_3
    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 691
    .end local v9    # "currentLineRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .end local v14    # "maxFontSize":F
    .end local v19    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .end local v20    # "maxImgHeight":F
    .end local v21    # "paraLineItem":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :cond_4
    move/from16 v13, p5

    goto :goto_1

    .line 709
    .restart local v9    # "currentLineRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v14    # "maxFontSize":F
    .restart local v19    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v20    # "maxImgHeight":F
    .restart local v21    # "paraLineItem":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :cond_5
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isBitmap()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 710
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, v20, v2

    if-gtz v2, :cond_3

    .line 712
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v2

    int-to-float v0, v2

    move/from16 v20, v0

    goto :goto_3

    .line 715
    :cond_6
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isChart()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 716
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v2

    cmpg-float v2, v20, v2

    if-gtz v2, :cond_3

    .line 718
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v20

    goto :goto_3

    .line 720
    :cond_7
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isShape()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 721
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v2

    cmpg-float v2, v20, v2

    if-gtz v2, :cond_3

    .line 723
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v20

    goto :goto_3

    .line 725
    :cond_8
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isDiagram()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 726
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/DiagramController;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/DiagramController;->getBehindDocState()I

    move-result v2

    if-eqz v2, :cond_3

    .line 728
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/DiagramController;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/DiagramController;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, v20, v2

    if-gtz v2, :cond_3

    .line 730
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/DiagramController;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/DiagramController;->getHeight()I

    move-result v2

    int-to-float v0, v2

    move/from16 v20, v0

    goto/16 :goto_3

    .line 739
    .end local v21    # "paraLineItem":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :cond_9
    const/4 v2, 0x0

    cmpl-float v2, v20, v2

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->lineRule:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->lineRule:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;->EXACTLY:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    if-eq v2, v3, :cond_a

    .line 741
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    add-float v2, v2, v20

    float-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 742
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->lineRule:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->lineRule:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;->EXACTLY:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    if-eq v2, v3, :cond_b

    .line 743
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    add-float/2addr v2, v14

    float-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 745
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    move/from16 v22, v0

    .line 747
    .local v22, "prevXval":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBackgroundColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 748
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 749
    .local v7, "p":Landroid/graphics/Paint;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBackgroundColor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->stringToColor(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v17

    .line 751
    .local v17, "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual/range {v17 .. v17}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 753
    add-int/lit8 v2, v23, -0x1

    move/from16 v0, v18

    if-eq v0, v2, :cond_10

    .line 754
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    int-to-float v3, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    sub-float v4, v2, v14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    add-int v2, v2, p5

    int-to-float v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    move-object/from16 v1, p9

    invoke-direct {v0, v14, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->calculateLineSpacing(FLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)F

    move-result v6

    add-float/2addr v2, v6

    add-float v6, v2, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 767
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v17    # "color":Lorg/apache/poi/java/awt/Color;
    :cond_c
    :goto_4
    move/from16 v0, p6

    if-ne v13, v0, :cond_d

    .line 768
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    sub-int v3, p5, p6

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    .line 771
    :cond_d
    if-nez v18, :cond_e

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBulletSpaceStatus()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 772
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBulletChar()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBulletXPos()F

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v4, v4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBulletPaint()Landroid/text/TextPaint;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_e
    move-object/from16 v8, p0

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move/from16 v15, p8

    move-object/from16 v16, p9

    .line 777
    invoke-direct/range {v8 .. v16}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->drawCurrentLine(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IFZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 779
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    .line 780
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-double v4, v2

    invoke-virtual/range {p9 .. p9}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v2

    move-object/from16 v0, p4

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    int-to-double v10, v6

    sub-double v10, v2, v10

    invoke-virtual/range {p9 .. p9}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getPageList()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/office/word/Page;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/word/Page;->getFooterStartPoint()F

    move-result v2

    float-to-double v2, v2

    sub-double v2, v10, v2

    cmpl-double v2, v4, v2

    if-ltz v2, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-eq v2, v3, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParentType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->DOCUMENT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    if-ne v2, v3, :cond_11

    .line 785
    const/4 v2, 0x1

    move-object/from16 v0, p9

    iput-boolean v2, v0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    .line 790
    .end local v9    # "currentLineRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .end local v14    # "maxFontSize":F
    .end local v19    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .end local v20    # "maxImgHeight":F
    .end local v22    # "prevXval":I
    :cond_f
    return-void

    .line 762
    .restart local v7    # "p":Landroid/graphics/Paint;
    .restart local v9    # "currentLineRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v14    # "maxFontSize":F
    .restart local v17    # "color":Lorg/apache/poi/java/awt/Color;
    .restart local v19    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v20    # "maxImgHeight":F
    .restart local v22    # "prevXval":I
    :cond_10
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    int-to-float v3, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    sub-float v4, v2, v14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    add-int v2, v2, p5

    int-to-float v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    add-float v6, v2, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 788
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v17    # "color":Lorg/apache/poi/java/awt/Color;
    :cond_11
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    move-object/from16 v1, p9

    invoke-direct {v0, v14, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->calculateLineSpacing(FLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)F

    move-result v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 686
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_0
.end method

.method public DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 42
    .param p1, "paragraph"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "rect"    # Landroid/graphics/Rect;
    .param p5, "paraWidth"    # I
    .param p6, "firstLineWidth"    # I
    .param p9, "isHidden"    # Z
    .param p10, "elementsCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            "Landroid/graphics/Canvas;",
            "Landroid/content/Context;",
            "Landroid/graphics/Rect;",
            "II",
            "Ljava/util/List",
            "<[I>;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;Z",
            "Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;",
            ")V"
        }
    .end annotation

    .prologue
    .line 367
    .local p7, "lines":Ljava/util/List;, "Ljava/util/List<[I>;"
    .local p8, "paraLineLength":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    const/16 v23, 0x0

    .line 369
    .local v23, "currentParaLineIndex":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getContents()Ljava/util/ArrayList;

    move-result-object v36

    .line 370
    .local v36, "runArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/CanvasElementPart;>;"
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 372
    .local v29, "lineElement":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    move-object/from16 v0, p8

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v22

    .line 373
    .local v22, "currentLineLength":F
    const/16 v21, 0x0

    .line 375
    .local v21, "currentLineIndexLength":F
    invoke-virtual/range {p8 .. p8}, Ljava/util/ArrayList;->size()I

    move-result v33

    .line 376
    .local v33, "paraLineSize":I
    invoke-virtual/range {v36 .. v36}, Ljava/util/ArrayList;->size()I

    move-result v38

    .line 377
    .local v38, "size":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTabContents()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v40

    .line 378
    .local v40, "tabContSize":I
    const/16 v27, 0x0

    .local v27, "j":I
    :goto_0
    move/from16 v0, v27

    move/from16 v1, v38

    if-ge v0, v1, :cond_15

    .line 380
    move-object/from16 v0, v36

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .line 382
    .local v18, "canElePart":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    move-object/from16 v0, v18

    instance-of v2, v0, Lcom/samsung/thumbnail/customview/word/Run;

    if-eqz v2, :cond_8

    move-object/from16 v35, v18

    .line 383
    check-cast v35, Lcom/samsung/thumbnail/customview/word/Run;

    .line 385
    .local v35, "run":Lcom/samsung/thumbnail/customview/word/Run;
    new-instance v28, Lcom/samsung/thumbnail/customview/word/ParaLine;

    invoke-direct/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 386
    .local v28, "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/4 v2, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsText(Z)V

    .line 387
    const/4 v2, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    .line 389
    invoke-virtual/range {v35 .. v35}, Lcom/samsung/thumbnail/customview/word/Run;->hasTab()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 390
    if-eqz v40, :cond_5

    .line 391
    const/16 v25, 0x0

    .local v25, "i":I
    :goto_1
    move/from16 v0, v25

    move/from16 v1, v40

    if-ge v0, v1, :cond_0

    .line 392
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTabContents()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/TabValues;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/TabValues;->getPosition()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, v21, v2

    if-gez v2, :cond_4

    .line 394
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTabContents()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/TabValues;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/TabValues;->getPosition()I

    move-result v2

    int-to-float v0, v2

    move/from16 v21, v0

    .line 396
    const/4 v2, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setHasTab(Z)V

    .line 397
    move-object/from16 v0, v28

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setTabIndex(F)V

    .line 407
    .end local v25    # "i":I
    :cond_0
    :goto_2
    const/16 v41, 0x0

    .line 408
    .local v41, "widthArray":[F
    invoke-virtual/range {v35 .. v35}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v39

    .line 409
    .local v39, "strCont":Ljava/lang/String;
    if-eqz v39, :cond_1

    .line 413
    invoke-virtual/range {v35 .. v35}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->length()I

    move-result v4

    move-object/from16 v0, v39

    invoke-static {v2, v0, v3, v4}, Lcom/samsung/thumbnail/customview/word/Run;->getTextWidths(Landroid/text/TextPaint;Ljava/lang/String;II)[F

    move-result-object v41

    .line 416
    :cond_1
    if-eqz v41, :cond_2

    move-object/from16 v0, v41

    array-length v2, v0

    if-nez v2, :cond_2

    .line 417
    const/4 v2, 0x0

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setEndIndex(I)V

    .line 418
    move-object/from16 v0, v28

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 419
    move-object/from16 v0, v28

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 420
    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    :cond_2
    if-eqz v41, :cond_9

    .line 424
    const/16 v25, 0x0

    .restart local v25    # "i":I
    :goto_3
    move-object/from16 v0, v41

    array-length v2, v0

    move/from16 v0, v25

    if-ge v0, v2, :cond_9

    .line 426
    aget v2, v41, v25

    add-float v2, v2, v21

    cmpg-float v2, v2, v22

    if-gtz v2, :cond_6

    .line 427
    aget v2, v41, v25

    add-float v21, v21, v2

    .line 428
    move-object/from16 v0, v41

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    move/from16 v0, v25

    if-ne v0, v2, :cond_3

    .line 429
    add-int/lit8 v2, v25, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setEndIndex(I)V

    .line 430
    move-object/from16 v0, v28

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 431
    move-object/from16 v0, v28

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 432
    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 433
    new-instance v28, Lcom/samsung/thumbnail/customview/word/ParaLine;

    .end local v28    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-direct/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 434
    .restart local v28    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/4 v2, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsText(Z)V

    .line 435
    add-int/lit8 v2, v25, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    .line 424
    :cond_3
    :goto_4
    add-int/lit8 v25, v25, 0x1

    goto :goto_3

    .line 391
    .end local v39    # "strCont":Ljava/lang/String;
    .end local v41    # "widthArray":[F
    :cond_4
    add-int/lit8 v25, v25, 0x1

    goto/16 :goto_1

    .line 402
    .end local v25    # "i":I
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getDefTabSpacing()I

    move-result v2

    int-to-float v2, v2

    add-float v21, v21, v2

    .line 403
    const/4 v2, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setHasTab(Z)V

    .line 404
    move-object/from16 v0, v28

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setTabIndex(F)V

    goto/16 :goto_2

    .line 439
    .restart local v25    # "i":I
    .restart local v39    # "strCont":Ljava/lang/String;
    .restart local v41    # "widthArray":[F
    :cond_6
    move-object/from16 v0, v28

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setEndIndex(I)V

    .line 440
    move-object/from16 v0, v28

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 441
    move-object/from16 v0, v28

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 442
    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 443
    new-instance v28, Lcom/samsung/thumbnail/customview/word/ParaLine;

    .end local v28    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-direct/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 444
    .restart local v28    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/4 v2, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsText(Z)V

    .line 445
    move-object/from16 v0, v28

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    .line 446
    add-int/lit8 v2, v23, 0x1

    move/from16 v0, v33

    if-ge v2, v0, :cond_7

    .line 447
    add-int/lit8 v23, v23, 0x1

    move-object/from16 v0, p8

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v22

    .line 449
    :cond_7
    const/16 v21, 0x0

    .line 450
    aget v2, v41, v25

    add-float v21, v21, v2

    goto :goto_4

    .line 454
    .end local v25    # "i":I
    .end local v28    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .end local v35    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v39    # "strCont":Ljava/lang/String;
    .end local v41    # "widthArray":[F
    :cond_8
    move-object/from16 v0, v18

    instance-of v2, v0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    if-eqz v2, :cond_c

    move-object/from16 v17, v18

    .line 455
    check-cast v17, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    .line 457
    .local v17, "bitmapContent":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isInLine()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 458
    new-instance v31, Lcom/samsung/thumbnail/customview/word/ParaLine;

    invoke-direct/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 459
    .local v31, "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/4 v2, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsBitmap(Z)V

    .line 460
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v2

    int-to-float v2, v2

    add-float v2, v2, v21

    cmpg-float v2, v2, v22

    if-gtz v2, :cond_a

    .line 462
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v2

    int-to-float v2, v2

    add-float v21, v21, v2

    .line 464
    move-object/from16 v0, v31

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 473
    :goto_5
    move-object/from16 v0, v31

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 474
    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 378
    .end local v17    # "bitmapContent":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v31    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :cond_9
    :goto_6
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_0

    .line 466
    .restart local v17    # "bitmapContent":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .restart local v31    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :cond_a
    add-int/lit8 v2, v23, 0x1

    move/from16 v0, v33

    if-ge v2, v0, :cond_b

    .line 467
    add-int/lit8 v2, v23, 0x1

    move-object/from16 v0, p8

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v22

    .line 469
    :cond_b
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getWidthOfBitmap()I

    move-result v2

    int-to-float v0, v2

    move/from16 v21, v0

    .line 471
    add-int/lit8 v23, v23, 0x1

    move-object/from16 v0, v31

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    goto :goto_5

    .line 477
    .end local v17    # "bitmapContent":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .end local v31    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :cond_c
    move-object/from16 v0, v18

    instance-of v2, v0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    if-eqz v2, :cond_f

    move-object/from16 v19, v18

    .line 478
    check-cast v19, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    .line 480
    .local v19, "chartContent":Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    new-instance v31, Lcom/samsung/thumbnail/customview/word/ParaLine;

    invoke-direct/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 481
    .restart local v31    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/4 v2, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsChart(Z)V

    .line 482
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v2

    add-float v2, v2, v21

    cmpg-float v2, v2, v22

    if-gtz v2, :cond_d

    .line 483
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v2

    add-float v21, v21, v2

    .line 484
    move-object/from16 v0, v31

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 492
    :goto_7
    move-object/from16 v0, v31

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 493
    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 486
    :cond_d
    add-int/lit8 v2, v23, 0x1

    move/from16 v0, v33

    if-ge v2, v0, :cond_e

    .line 487
    add-int/lit8 v2, v23, 0x1

    move-object/from16 v0, p8

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v22

    .line 489
    :cond_e
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartWidth()F

    move-result v21

    .line 490
    add-int/lit8 v23, v23, 0x1

    move-object/from16 v0, v31

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    goto :goto_7

    .line 495
    .end local v19    # "chartContent":Lcom/samsung/thumbnail/customview/hslf/ChartContents;
    .end local v31    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :cond_f
    move-object/from16 v0, v18

    instance-of v2, v0, Lcom/samsung/thumbnail/customview/word/ShapesController;

    if-eqz v2, :cond_12

    move-object/from16 v37, v18

    .line 496
    check-cast v37, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .line 498
    .local v37, "shapeController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    invoke-virtual/range {v37 .. v37}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getZorder()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_9

    .line 499
    new-instance v31, Lcom/samsung/thumbnail/customview/word/ParaLine;

    invoke-direct/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 500
    .restart local v31    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/4 v2, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsShape(Z)V

    .line 501
    invoke-virtual/range {v37 .. v37}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v2

    add-float v2, v2, v21

    cmpg-float v2, v2, v22

    if-gtz v2, :cond_10

    .line 503
    invoke-virtual/range {v37 .. v37}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v2

    add-float v21, v21, v2

    .line 505
    move-object/from16 v0, v31

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 514
    :goto_8
    move-object/from16 v0, v31

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 515
    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 507
    :cond_10
    add-int/lit8 v2, v23, 0x1

    move/from16 v0, v33

    if-ge v2, v0, :cond_11

    .line 508
    add-int/lit8 v2, v23, 0x1

    move-object/from16 v0, p8

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v22

    .line 510
    :cond_11
    invoke-virtual/range {v37 .. v37}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeWidth()F

    move-result v21

    .line 512
    add-int/lit8 v23, v23, 0x1

    move-object/from16 v0, v31

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    goto :goto_8

    .line 517
    .end local v31    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .end local v37    # "shapeController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_12
    move-object/from16 v0, v18

    instance-of v2, v0, Lcom/samsung/thumbnail/customview/word/DiagramController;

    if-eqz v2, :cond_9

    move-object/from16 v24, v18

    .line 518
    check-cast v24, Lcom/samsung/thumbnail/customview/word/DiagramController;

    .line 520
    .local v24, "diagramController":Lcom/samsung/thumbnail/customview/word/DiagramController;
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/word/DiagramController;->isInLine()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 521
    new-instance v31, Lcom/samsung/thumbnail/customview/word/ParaLine;

    invoke-direct/range {v31 .. v31}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 522
    .restart local v31    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/4 v2, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setIsDiagram(Z)V

    .line 523
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/word/DiagramController;->getWidth()I

    move-result v2

    int-to-float v2, v2

    add-float v2, v2, v21

    cmpg-float v2, v2, v22

    if-gtz v2, :cond_13

    .line 524
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/word/DiagramController;->getWidth()I

    move-result v2

    int-to-float v2, v2

    add-float v21, v21, v2

    .line 525
    move-object/from16 v0, v31

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 533
    :goto_9
    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 534
    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 527
    :cond_13
    add-int/lit8 v2, v23, 0x1

    move/from16 v0, v33

    if-ge v2, v0, :cond_14

    .line 528
    add-int/lit8 v2, v23, 0x1

    move-object/from16 v0, p8

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v22

    .line 530
    :cond_14
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/word/DiagramController;->getWidth()I

    move-result v2

    int-to-float v0, v2

    move/from16 v21, v0

    .line 531
    add-int/lit8 v23, v23, 0x1

    move-object/from16 v0, v31

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    goto :goto_9

    .line 539
    .end local v18    # "canElePart":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .end local v24    # "diagramController":Lcom/samsung/thumbnail/customview/word/DiagramController;
    .end local v31    # "paraLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :cond_15
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_16

    .line 540
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 542
    :cond_16
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_17

    .line 543
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    .line 545
    :cond_17
    const/4 v13, 0x0

    .line 547
    .local v13, "lineWidth":I
    const/16 v25, 0x0

    .restart local v25    # "i":I
    :goto_a
    move/from16 v0, v25

    move/from16 v1, v33

    if-ge v0, v1, :cond_26

    .line 549
    if-nez v25, :cond_1a

    .line 550
    move/from16 v13, p6

    .line 555
    :goto_b
    const/4 v14, 0x0

    .line 556
    .local v14, "maxFontSize":F
    const/16 v30, 0x0

    .line 558
    .local v30, "maxImgHeight":F
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 559
    .local v9, "currentLineRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v26

    .line 560
    .local v26, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    :cond_18
    :goto_c
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 561
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Lcom/samsung/thumbnail/customview/word/ParaLine;

    .line 562
    .local v32, "paraLineItem":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getLineNumber()I

    move-result v2

    move/from16 v0, v25

    if-ne v2, v0, :cond_18

    .line 563
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isText()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 564
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/TextPaint;->getTextSize()F

    move-result v2

    cmpg-float v2, v14, v2

    if-gtz v2, :cond_19

    .line 570
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/TextPaint;->getTextSize()F

    move-result v14

    .line 596
    :cond_19
    :goto_d
    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 552
    .end local v9    # "currentLineRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .end local v14    # "maxFontSize":F
    .end local v26    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .end local v30    # "maxImgHeight":F
    .end local v32    # "paraLineItem":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :cond_1a
    move/from16 v13, p5

    goto :goto_b

    .line 574
    .restart local v9    # "currentLineRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v14    # "maxFontSize":F
    .restart local v26    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v30    # "maxImgHeight":F
    .restart local v32    # "paraLineItem":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :cond_1b
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isBitmap()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 575
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, v30, v2

    if-gtz v2, :cond_19

    .line 577
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->getHeightOfBitmap()I

    move-result v2

    int-to-float v0, v2

    move/from16 v30, v0

    goto :goto_d

    .line 580
    :cond_1c
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isChart()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 581
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v2

    cmpg-float v2, v30, v2

    if-gtz v2, :cond_19

    .line 583
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/ChartContents;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->getChartHeight()F

    move-result v30

    goto :goto_d

    .line 585
    :cond_1d
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isShape()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 586
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v2

    cmpg-float v2, v30, v2

    if-gtz v2, :cond_19

    .line 588
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/ShapesController;->getShapeHeight()F

    move-result v30

    goto :goto_d

    .line 590
    :cond_1e
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->isDiagram()Z

    move-result v2

    if-eqz v2, :cond_19

    .line 591
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/DiagramController;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/DiagramController;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v2, v30, v2

    if-gtz v2, :cond_19

    .line 593
    invoke-virtual/range {v32 .. v32}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;

    move-result-object v2

    check-cast v2, Lcom/samsung/thumbnail/customview/word/DiagramController;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/DiagramController;->getHeight()I

    move-result v2

    int-to-float v0, v2

    move/from16 v30, v0

    goto/16 :goto_d

    .line 600
    .end local v32    # "paraLineItem":Lcom/samsung/thumbnail/customview/word/ParaLine;
    :cond_1f
    const/4 v2, 0x0

    cmpl-float v2, v30, v2

    if-eqz v2, :cond_20

    .line 601
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    add-float v2, v2, v30

    float-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 602
    :cond_20
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-eq v2, v3, :cond_21

    .line 603
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    add-float/2addr v2, v14

    float-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 605
    :cond_21
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    move/from16 v34, v0

    .line 607
    .local v34, "prevXval":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBackgroundColor()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_22

    .line 608
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 609
    .local v7, "p":Landroid/graphics/Paint;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBackgroundColor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->stringToColor(Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v20

    .line 611
    .local v20, "color":Lorg/apache/poi/java/awt/Color;
    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/java/awt/Color;->getAlpha()I

    move-result v2

    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/java/awt/Color;->getRed()I

    move-result v3

    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/java/awt/Color;->getGreen()I

    move-result v4

    invoke-virtual/range {v20 .. v20}, Lorg/apache/poi/java/awt/Color;->getBlue()I

    move-result v5

    invoke-virtual {v7, v2, v3, v4, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 613
    add-int/lit8 v2, v33, -0x1

    move/from16 v0, v25

    if-eq v0, v2, :cond_25

    .line 614
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    int-to-float v3, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    sub-float v4, v2, v14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    add-int v2, v2, p5

    int-to-float v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    move-object/from16 v1, p10

    invoke-direct {v0, v14, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->calculateLineSpacing(FLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)F

    move-result v6

    add-float/2addr v2, v6

    add-float v6, v2, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 627
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v20    # "color":Lorg/apache/poi/java/awt/Color;
    :cond_22
    :goto_e
    move/from16 v0, p6

    if-ne v13, v0, :cond_23

    .line 628
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    sub-int v3, p5, p6

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    :cond_23
    move-object/from16 v8, p0

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move/from16 v15, p9

    move-object/from16 v16, p10

    .line 631
    invoke-direct/range {v8 .. v16}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->drawCurrentLine(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IFZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 633
    move/from16 v0, v34

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    .line 634
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-double v2, v2

    invoke-virtual/range {p10 .. p10}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v4

    move-object/from16 v0, p4

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    int-to-double v10, v6

    sub-double/2addr v4, v10

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_24

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;->FOOTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    if-eq v2, v3, :cond_24

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getParentType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;->DOCUMENT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    if-ne v2, v3, :cond_24

    .line 637
    const/4 v2, 0x1

    move-object/from16 v0, p10

    iput-boolean v2, v0, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    .line 639
    :cond_24
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    move-object/from16 v1, p10

    invoke-direct {v0, v14, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->calculateLineSpacing(FLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)F

    move-result v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 547
    add-int/lit8 v25, v25, 0x1

    goto/16 :goto_a

    .line 622
    .restart local v7    # "p":Landroid/graphics/Paint;
    .restart local v20    # "color":Lorg/apache/poi/java/awt/Color;
    :cond_25
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    int-to-float v3, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    sub-float v4, v2, v14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    add-int v2, v2, p5

    int-to-float v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    int-to-float v2, v2

    add-float v6, v2, v14

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_e

    .line 641
    .end local v7    # "p":Landroid/graphics/Paint;
    .end local v9    # "currentLineRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .end local v14    # "maxFontSize":F
    .end local v20    # "color":Lorg/apache/poi/java/awt/Color;
    .end local v26    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .end local v30    # "maxImgHeight":F
    .end local v34    # "prevXval":I
    :cond_26
    if-eqz p9, :cond_27

    .line 642
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTextHeight(I)V

    .line 643
    :cond_27
    return-void
.end method

.method public addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V
    .locals 1
    .param p1, "element"    # Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .prologue
    .line 274
    if-eqz p1, :cond_0

    .line 275
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mContentArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    :cond_0
    return-void
.end method

.method public addTabContents(Lcom/samsung/thumbnail/customview/hslf/TabValues;)V
    .locals 1
    .param p1, "tabVal"    # Lcom/samsung/thumbnail/customview/hslf/TabValues;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mTabValueArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    return-void
.end method

.method public clearContents()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mContentArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 292
    return-void
.end method

.method public getAlignment()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mAlignment:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    return-object v0
.end method

.method public getBackgroundColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->backgroundColor:Ljava/lang/String;

    return-object v0
.end method

.method public getBottomBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->bottomBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    return-object v0
.end method

.method public getBottomSpacing()F
    .locals 2

    .prologue
    .line 225
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mBottomSpacing:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 226
    const/high16 v0, 0x40000000    # 2.0f

    .line 227
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mBottomSpacing:F

    goto :goto_0
.end method

.method public getBulletChar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->bulletChar:Ljava/lang/String;

    return-object v0
.end method

.method public getBulletPaint()Landroid/text/TextPaint;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->bulletPaint:Landroid/text/TextPaint;

    return-object v0
.end method

.method public getBulletSpaceStatus()Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->bulletSpaceAdded:Z

    return v0
.end method

.method public getBulletXPos()F
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->bulletXPos:F

    return v0
.end method

.method public getContents()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/CanvasElementPart;",
            ">;"
        }
    .end annotation

    .prologue
    .line 279
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mContentArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDefTabSpacing()I
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mDefTabSpacing:I

    return v0
.end method

.method public getFirstLineMargin()F
    .locals 1

    .prologue
    .line 251
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->firstLineMargin:F

    return v0
.end method

.method public getLeftBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->leftBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    return-object v0
.end method

.method public getLeftMargin()F
    .locals 1

    .prologue
    .line 295
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->leftMargin:F

    return v0
.end method

.method public getLineRule()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->lineRule:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    return-object v0
.end method

.method public getLineSpacing()F
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mLineSpacing:F

    return v0
.end method

.method public getParaNumber()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->paraNumber:I

    return v0
.end method

.method public getParaType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mParaType:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    return-object v0
.end method

.method public getParentType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mParentType:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    return-object v0
.end method

.method public getRightBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->rightBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    return-object v0
.end method

.method public getRightMargin()F
    .locals 1

    .prologue
    .line 303
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->rightMargin:F

    return v0
.end method

.method public getTabContents()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/TabValues;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mTabValueArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTabSpacing()I
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mTabSpacing:I

    return v0
.end method

.method public getTextHeight()I
    .locals 1

    .prologue
    .line 1238
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->paraTextHeight:I

    return v0
.end method

.method public getTopBorder()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->topBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    return-object v0
.end method

.method public getTopSpacing()F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 215
    iget v1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mTopSpacing:F

    cmpg-float v1, v1, v0

    if-gez v1, :cond_0

    .line 217
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mTopSpacing:F

    goto :goto_0
.end method

.method public getX()I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    return v0
.end method

.method public getY()I
    .locals 1

    .prologue
    .line 327
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    return v0
.end method

.method public isPgBrBefStatus()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->pgBrBefStatus:Z

    return v0
.end method

.method public setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V
    .locals 0
    .param p1, "paraAlignment"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mAlignment:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    .line 260
    return-void
.end method

.method public setBackgroundColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "backgroundColor"    # Ljava/lang/String;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->backgroundColor:Ljava/lang/String;

    .line 175
    return-void
.end method

.method public setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 0
    .param p1, "bottomBorder"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->bottomBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 139
    return-void
.end method

.method public setBottomSpacing(F)V
    .locals 0
    .param p1, "bottomSpacing"    # F

    .prologue
    .line 231
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mBottomSpacing:F

    .line 232
    return-void
.end method

.method public setBulletChar(Ljava/lang/String;)V
    .locals 0
    .param p1, "bulletChar"    # Ljava/lang/String;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->bulletChar:Ljava/lang/String;

    .line 159
    return-void
.end method

.method public setBulletPaint(Landroid/text/TextPaint;)V
    .locals 0
    .param p1, "bulletPaint"    # Landroid/text/TextPaint;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->bulletPaint:Landroid/text/TextPaint;

    .line 171
    return-void
.end method

.method public setBulletSpaceStatus(Z)V
    .locals 0
    .param p1, "bulletSpaceStatus"    # Z

    .prologue
    .line 182
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->bulletSpaceAdded:Z

    .line 183
    return-void
.end method

.method public setBulletXPos(F)V
    .locals 0
    .param p1, "bullXPos"    # F

    .prologue
    .line 150
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->bulletXPos:F

    .line 151
    return-void
.end method

.method public setDefTabSpacing(I)V
    .locals 0
    .param p1, "defTabSpacing"    # I

    .prologue
    .line 239
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mDefTabSpacing:I

    .line 240
    return-void
.end method

.method public setFirstLineMargin(F)V
    .locals 0
    .param p1, "firstLineMargin"    # F

    .prologue
    .line 255
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->firstLineMargin:F

    .line 256
    return-void
.end method

.method public setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 0
    .param p1, "leftBorder"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->leftBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 127
    return-void
.end method

.method public setLeftMargin(F)V
    .locals 0
    .param p1, "leftMargin"    # F

    .prologue
    .line 299
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->leftMargin:F

    .line 300
    return-void
.end method

.method public setLineRule(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;)V
    .locals 0
    .param p1, "lineRule"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->lineRule:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$LineRuleType;

    .line 143
    return-void
.end method

.method public setLineSpacing(F)V
    .locals 1
    .param p1, "lineSpacing"    # F

    .prologue
    .line 210
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 211
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mLineSpacing:F

    .line 212
    :cond_0
    return-void
.end method

.method public setParaNumber(I)V
    .locals 0
    .param p1, "paraNumber"    # I

    .prologue
    .line 202
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->paraNumber:I

    .line 203
    return-void
.end method

.method public setParaType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V
    .locals 0
    .param p1, "paraType"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    .prologue
    .line 315
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mParaType:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    .line 316
    return-void
.end method

.method public setParentType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;)V
    .locals 0
    .param p1, "parentType"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mParentType:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParentParaType;

    .line 191
    return-void
.end method

.method public setPgBrBefStatus(Z)V
    .locals 0
    .param p1, "pgBrBefStatus"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->pgBrBefStatus:Z

    .line 90
    return-void
.end method

.method public setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 0
    .param p1, "rightBorder"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->rightBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 135
    return-void
.end method

.method public setRightMargin(F)V
    .locals 0
    .param p1, "rightMargin"    # F

    .prologue
    .line 307
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->rightMargin:F

    .line 308
    return-void
.end method

.method public setTabSpacing(I)V
    .locals 0
    .param p1, "tabSpacing"    # I

    .prologue
    .line 247
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mTabSpacing:I

    .line 248
    return-void
.end method

.method public setTextHeight(I)V
    .locals 0
    .param p1, "paraHeight"    # I

    .prologue
    .line 1234
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->paraTextHeight:I

    .line 1235
    return-void
.end method

.method public setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;)V
    .locals 0
    .param p1, "topBorder"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->topBorder:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 131
    return-void
.end method

.method public setTopSpacing(F)V
    .locals 0
    .param p1, "topSpacing"    # F

    .prologue
    .line 221
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->mTopSpacing:F

    .line 222
    return-void
.end method

.method public setX(I)V
    .locals 0
    .param p1, "x"    # I

    .prologue
    .line 323
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->x:I

    .line 324
    return-void
.end method

.method public setY(I)V
    .locals 0
    .param p1, "y"    # I

    .prologue
    .line 331
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->y:I

    .line 332
    return-void
.end method
