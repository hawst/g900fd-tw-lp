.class public Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;
.super Ljava/lang/Object;
.source "XExcelCustomViewConsumer.java"


# static fields
.field private static final ACTUAL_CELL_HEIGHT:I = 0x2e824

.field private static final CELL_HEIGHT_IN_INCHES:I = 0xf

.field private static final COLOR_BLACK:Ljava/lang/String; = "#000000"

.field private static final DEFAULT_ADJUST_BORDERWIDTH:F = 0.1f

.field private static final DEFAULT_ADJUST_FONTSIZE:F = 1.5f

.field private static final DEFAULT_BORDER_COLOR:Ljava/lang/String; = "8b8989"

.field private static final DEFAULT_BORDER_COLOROBJ:Lorg/apache/poi/java/awt/Color;

.field private static final DEFAULT_BORDER_WIDTH:I = 0x1

.field public static final DEFAULT_CELL_HEIGHT:I = 0x14

.field private static final DEFAULT_CELL_WIDTH:I = 0x46

.field private static final DEFAULT_TEXTINSHAPE_COLOR:I = -0x1

.field private static final DEFAULT_WIDTH_XLSX:Ljava/lang/Double;

.field private static final FIRST_CELL_WIDTH:I = 0x19

.field private static final FIRST_COLUMN_HEIGHT:I = 0xf

.field private static final FIRST_COLUMN_WIDTH:I = 0x19

.field public static final MEASURED_CELL_HEIGHT:I = 0x1e

.field public static final ONECELLHEIGHT:I = 0x2e824

.field public static final ONECELLWIDTH:I = 0x94d40

.field private static final TAG:Ljava/lang/String; = "XExcelCustomViewConsumer"


# instance fields
.field public ROW_DISPLAY:I

.field private charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

.field private colcount:I

.field private drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

.field private excelVariable:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

.field private folderName:Ljava/lang/String;

.field private folderPath:Ljava/io/File;

.field private hyperlinkcount:I

.field private isThumbnailPreview:Z

.field private mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field private mContext:Landroid/content/Context;

.field private mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

.field private mCustomView:Lcom/samsung/thumbnail/customview/word/CustomView;

.field private mMaxColumn:I

.field private mMaxRow:I

.field private mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

.field private mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

.field private numericcolumcount:I

.field public previousrow:I

.field public rowattr:Z

.field public rowid1:I

.field public rowid2:I

.field private rprcount:I

.field private themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

.field private underlines:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x89

    .line 135
    const-wide/high16 v0, 0x4022000000000000L    # 9.0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_WIDTH_XLSX:Ljava/lang/Double;

    .line 141
    new-instance v0, Lorg/apache/poi/java/awt/Color;

    const/16 v1, 0x8b

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    sput-object v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_BORDER_COLOROBJ:Lorg/apache/poi/java/awt/Color;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;Lcom/samsung/thumbnail/customview/word/CustomView;Landroid/content/Context;ZLcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;)V
    .locals 5
    .param p1, "workBookObj"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;
    .param p2, "customView"    # Lcom/samsung/thumbnail/customview/word/CustomView;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "isThumbnail"    # Z
    .param p5, "var"    # Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    .line 100
    iput v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numericcolumcount:I

    .line 101
    iput v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rowid2:I

    iput v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rowid1:I

    .line 102
    iput v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    .line 103
    iput v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxRow:I

    .line 109
    iput v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->hyperlinkcount:I

    .line 111
    iput v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    .line 113
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->ROW_DISPLAY:I

    .line 161
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->isThumbnailPreview:Z

    .line 170
    iput-object p5, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->excelVariable:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    .line 171
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    .line 172
    iput-object p2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCustomView:Lcom/samsung/thumbnail/customview/word/CustomView;

    .line 173
    iput-object p3, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mContext:Landroid/content/Context;

    .line 174
    iput-boolean p4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->isThumbnailPreview:Z

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderName:Ljava/lang/String;

    .line 176
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderName:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    .line 177
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->init()V

    .line 178
    return-void
.end method

.method static alpha()[Ljava/lang/String;
    .locals 7

    .prologue
    .line 488
    const/16 v5, 0x2bf

    new-array v4, v5, [Ljava/lang/String;

    .line 489
    .local v4, "result":[Ljava/lang/String;
    const/4 v5, 0x0

    const-string/jumbo v6, ""

    aput-object v6, v4, v5

    .line 490
    const/4 v3, 0x0

    .local v3, "j":I
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    const/16 v5, 0x2bd

    if-gt v3, v5, :cond_1

    .line 491
    rem-int/lit8 v5, v3, 0x1a

    add-int/lit8 v5, v5, 0x41

    int-to-char v0, v5

    .line 492
    .local v0, "a":C
    div-int/lit8 v5, v3, 0x1a

    add-int/lit8 v5, v5, 0x41

    add-int/lit8 v5, v5, -0x1

    int-to-char v1, v5

    .line 494
    .local v1, "b":C
    div-int/lit8 v5, v3, 0x1a

    const/4 v6, 0x1

    if-lt v5, v6, :cond_0

    .line 495
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    .line 490
    :goto_1
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 497
    :cond_0
    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    goto :goto_1

    .line 500
    .end local v0    # "a":C
    .end local v1    # "b":C
    :cond_1
    return-object v4
.end method

.method private applyCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/customview/word/Run;)V
    .locals 4
    .param p1, "charProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .param p2, "drawRun"    # Lcom/samsung/thumbnail/customview/word/Run;

    .prologue
    .line 2824
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 2825
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBold()Z

    move-result v2

    invoke-virtual {p2, v1, v2}, Lcom/samsung/thumbnail/customview/word/Run;->setTypeface(Ljava/lang/String;Z)V

    .line 2829
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2831
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2837
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v1

    if-lez v1, :cond_2

    .line 2838
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getFontSize()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v1, v2

    invoke-virtual {p2, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 2842
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStyleId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStyleId()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "Hyperlink"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getUnderlineProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getUnderlineProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 2846
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setUnderlineText(Z)V

    .line 2847
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStrike()Z

    move-result v1

    invoke-virtual {p2, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setStrikeThruText(Z)V

    .line 2848
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getItalic()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2849
    invoke-virtual {p2}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 2851
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getVertAlign()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 2852
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getVertAlign()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setVertivalAlignment(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 2853
    :cond_7
    return-void

    .line 2826
    :cond_8
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBold()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2827
    invoke-virtual {p2}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    goto/16 :goto_0

    .line 2833
    :catch_0
    move-exception v0

    .line 2834
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v1, "XExcelCustomViewConsumer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private applyParaProp(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/customview/word/Run;)V
    .locals 3
    .param p1, "canvasPara"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .param p2, "paraProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .param p3, "drawRun"    # Lcom/samsung/thumbnail/customview/word/Run;

    .prologue
    const/high16 v2, 0x43700000    # 240.0f

    .line 2857
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2858
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getLeftInd()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p3, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setLeftIndent(I)V

    .line 2860
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getRightInd()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p3, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setRightIndent(I)V

    .line 2862
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getIndentation()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocIndentationProperty;->getFirstInd()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twipsMeasure(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p3, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setFirstLineIndent(I)V

    .line 2865
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2867
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v0

    int-to-float v0, v0

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 2868
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, -0x40800000    # -1.0f

    mul-float/2addr v0, v1

    invoke-virtual {p1, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    .line 2890
    :cond_1
    :goto_0
    return-void

    .line 2870
    :cond_2
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_3

    .line 2871
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    invoke-virtual {p1, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    goto :goto_0

    .line 2874
    :cond_3
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getLine()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setLineSpacing(F)V

    goto :goto_0
.end method

.method private applyProperWidth(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V
    .locals 6
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "columnindex"    # I

    .prologue
    .line 395
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v2, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetCellWidth(I)Ljava/lang/Double;

    move-result-object v1

    .line 397
    .local v1, "width":Ljava/lang/Double;
    if-nez v1, :cond_0

    .line 398
    const/high16 v2, 0x428c0000    # 70.0f

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 404
    :goto_0
    return-void

    .line 400
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide v4, 0x4051800000000000L    # 70.0

    mul-double/2addr v2, v4

    sget-object v4, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_WIDTH_XLSX:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 401
    .local v0, "actualwidth":F
    invoke-virtual {p1, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    goto :goto_0
.end method

.method private apply_firstrowtopborderstyle(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 3
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    .line 320
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v1, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_BORDER_COLOROBJ:Lorg/apache/poi/java/awt/Color;

    const v2, 0x3dcccccd    # 0.1f

    invoke-virtual {p1, v0, v1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 322
    return-void
.end method

.method private apply_table03(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V
    .locals 9
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "columnindex"    # I

    .prologue
    const/16 v5, 0xe7

    const v8, 0x3dcccccd    # 0.1f

    .line 427
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    const/16 v4, 0xe3

    invoke-direct {v2, v5, v4, v5}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 429
    .local v2, "fillColor":Lorg/apache/poi/java/awt/Color;
    sget-object v1, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_BORDER_COLOROBJ:Lorg/apache/poi/java/awt/Color;

    .line 432
    .local v1, "borderColor":Lorg/apache/poi/java/awt/Color;
    iget-object v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v4, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetCellWidth(I)Ljava/lang/Double;

    move-result-object v3

    .line 434
    .local v3, "width":Ljava/lang/Double;
    if-nez v3, :cond_0

    .line 435
    const/high16 v4, 0x428c0000    # 70.0f

    invoke-virtual {p1, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 441
    :goto_0
    const/high16 v4, 0x41a00000    # 20.0f

    invoke-virtual {p1, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellHeight(F)V

    .line 443
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    .line 445
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {p1, v4, v1, v8}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 447
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {p1, v4, v1, v8}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 449
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {p1, v4, v1, v8}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 451
    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {p1, v4, v1, v8}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 453
    return-void

    .line 437
    :cond_0
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide v6, 0x4051800000000000L    # 70.0

    mul-double/2addr v4, v6

    sget-object v6, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_WIDTH_XLSX:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    div-double/2addr v4, v6

    double-to-float v0, v4

    .line 438
    .local v0, "actualwidth":F
    invoke-virtual {p1, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    goto :goto_0
.end method

.method private apply_tbl_first_cell(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 5
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    const/16 v4, 0xc6

    const v3, 0x3dcccccd    # 0.1f

    .line 333
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    const/16 v2, 0xc3

    invoke-direct {v1, v4, v2, v4}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 335
    .local v1, "fillColor":Lorg/apache/poi/java/awt/Color;
    sget-object v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_BORDER_COLOROBJ:Lorg/apache/poi/java/awt/Color;

    .line 337
    .local v0, "borderColor":Lorg/apache/poi/java/awt/Color;
    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 338
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellHeight(F)V

    .line 340
    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    .line 342
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {p1, v2, v0, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 344
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {p1, v2, v0, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 346
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {p1, v2, v0, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 348
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {p1, v2, v0, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 350
    return-void
.end method

.method private apply_tbl_first_column(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 5
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    const/16 v4, 0xde

    const v3, 0x3dcccccd    # 0.1f

    .line 361
    new-instance v1, Lorg/apache/poi/java/awt/Color;

    const/16 v2, 0xdf

    invoke-direct {v1, v4, v2, v4}, Lorg/apache/poi/java/awt/Color;-><init>(III)V

    .line 363
    .local v1, "fillColor":Lorg/apache/poi/java/awt/Color;
    sget-object v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_BORDER_COLOROBJ:Lorg/apache/poi/java/awt/Color;

    .line 365
    .local v0, "borderColor":Lorg/apache/poi/java/awt/Color;
    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 366
    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellHeight(F)V

    .line 368
    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    .line 372
    const-string/jumbo v2, "center"

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setHorizontalAlignment(Ljava/lang/String;)V

    .line 373
    const-string/jumbo v2, "bottom"

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setVerticalAlignment(Ljava/lang/String;)V

    .line 375
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {p1, v2, v0, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 377
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {p1, v2, v0, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 379
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {p1, v2, v0, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 381
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {p1, v2, v0, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 383
    return-void
.end method

.method private canShowGridLine(Ljava/lang/String;)Z
    .locals 1
    .param p1, "style"    # Ljava/lang/String;

    .prologue
    .line 1088
    if-eqz p1, :cond_0

    const-string/jumbo v0, "nil"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowGridlines()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1091
    :cond_0
    const/4 v0, 0x0

    .line 1093
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private drawCharts(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3052
    .local p1, "topCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p2, "bottomCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p3, "rightCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local p4, "leftCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 3053
    .local v0, "objChartDisplay":Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/thumbnail/office/ooxml/excel/chart/ChartDisplay;->drawChartForCanvas(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 3056
    return-void
.end method

.method private filterText(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 2942
    const-string/jumbo v0, "&#39;"

    const-string/jumbo v1, "\'"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2943
    const-string/jumbo v0, "&#34;"

    const-string/jumbo v1, "\""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2944
    const-string/jumbo v0, "&quot;"

    const-string/jumbo v1, "\""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2945
    const-string/jumbo v0, "&amp;"

    const-string/jumbo v1, "&"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2946
    const-string/jumbo v0, "&lt;"

    const-string/jumbo v1, "<"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2947
    const-string/jumbo v0, "&gt;"

    const-string/jumbo v1, ">"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2948
    const-string/jumbo v0, "&nbsp;"

    const-string/jumbo v1, " "

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2949
    const-string/jumbo v0, "&apos;"

    const-string/jumbo v1, "\'"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2950
    return-object p1
.end method

.method private getCharacterProperties(I)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .locals 12
    .param p1, "textstyleid"    # I

    .prologue
    const/4 v11, 0x1

    .line 1527
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;-><init>()V

    .line 1528
    .local v7, "underlinesTemp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;
    const/4 v0, 0x1

    .line 1529
    .local v0, "bold":Z
    const/4 v4, 0x1

    .line 1530
    .local v4, "italic":Z
    const/4 v5, 0x0

    .line 1531
    .local v5, "textsz":I
    move v3, p1

    .line 1532
    .local v3, "i":I
    const-string/jumbo v8, ""

    .line 1533
    .local v8, "value":Ljava/lang/String;
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;-><init>()V

    .line 1535
    .local v1, "charPropTemp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExSharedStringTextBOLD()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExSharedStringTextBOLD()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-le v9, v3, :cond_6

    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExSharedStringTextBOLD()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string/jumbo v10, "true"

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_6

    .line 1540
    const/4 v0, 0x1

    .line 1545
    :goto_0
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextUnderline()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 1546
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextUnderline()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string/jumbo v10, "underline"

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_0

    .line 1548
    const-string/jumbo v9, "single"

    invoke-static {v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->setStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;)V

    .line 1551
    :cond_0
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextUnderline()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextUnderline()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string/jumbo v10, "double"

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_1

    .line 1555
    const-string/jumbo v9, "double"

    invoke-static {v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->setStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;)V

    .line 1558
    :cond_1
    invoke-virtual {v1, v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setUnderlineProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;)V

    .line 1561
    :cond_2
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextSize()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v9

    float-to-int v5, v9

    .line 1564
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getStyleTextVerticalAlign()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getStyleTextVerticalAlign()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string/jumbo v10, "superscript"

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_7

    .line 1567
    const-string/jumbo v9, "superscript"

    invoke-static {v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setVertAlign(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 1568
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;

    move-result-object v9

    invoke-virtual {v9, v11}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->setexcelfontsizechk(Z)V

    .line 1569
    invoke-virtual {v1, v5}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontSize(I)V

    .line 1589
    :goto_1
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextStrikethrough()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_a

    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextStrikethrough()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-le v9, v3, :cond_a

    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextStrikethrough()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string/jumbo v10, "true"

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_a

    .line 1594
    invoke-virtual {v1, v11}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setStrike(Z)V

    .line 1599
    :goto_2
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextItalic()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_b

    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextItalic()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-le v9, v3, :cond_b

    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextItalic()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string/jumbo v10, "true"

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_b

    .line 1603
    const/4 v4, 0x1

    .line 1608
    :goto_3
    invoke-virtual {v1, v4}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setItalic(Z)V

    .line 1609
    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setBold(Z)V

    .line 1613
    new-instance v2, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v2}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 1614
    .local v2, "docColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextColorRGB()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_c

    .line 1615
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextColorRGB()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "value":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 1616
    .restart local v8    # "value":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_3

    .line 1617
    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 1618
    invoke-virtual {v2, v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1644
    :cond_3
    :goto_4
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextColorTint()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_4

    .line 1645
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextColorTint()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "value":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 1646
    .restart local v8    # "value":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_4

    .line 1647
    invoke-virtual {v2, v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setTint(Ljava/lang/String;)V

    .line 1650
    :cond_4
    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 1653
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextFontname()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 1654
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextFontname()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v1, v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontName(Ljava/lang/String;)V

    .line 1657
    :cond_5
    return-object v1

    .line 1542
    .end local v2    # "docColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1572
    :cond_7
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getStyleTextVerticalAlign()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getStyleTextVerticalAlign()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string/jumbo v10, "subscript"

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_8

    .line 1576
    const-string/jumbo v9, "subscript"

    invoke-static {v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setVertAlign(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 1577
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;

    move-result-object v9

    invoke-virtual {v9, v11}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->setexcelfontsizechk(Z)V

    .line 1578
    invoke-virtual {v1, v5}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontSize(I)V

    goto/16 :goto_1

    .line 1579
    :cond_8
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextSize()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_9

    .line 1583
    invoke-virtual {v1, v5}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontSize(I)V

    goto/16 :goto_1

    .line 1586
    :cond_9
    const/16 v9, 0xb

    invoke-virtual {v1, v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontSize(I)V

    goto/16 :goto_1

    .line 1596
    :cond_a
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setStrike(Z)V

    goto/16 :goto_2

    .line 1605
    :cond_b
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 1622
    .restart local v2    # "docColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    :cond_c
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextColorTheme()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 1623
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleTextColorTheme()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "value":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 1626
    .restart local v8    # "value":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_3

    .line 1627
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 1629
    .local v6, "themeid":I
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getThemeObj()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v9

    if-eqz v9, :cond_d

    .line 1630
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getThemeObj()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getExcelColorSchema()Ljava/util/ArrayList;

    move-result-object v9

    invoke-direct {p0, v6}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCorrectThemeClr(I)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "value":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 1640
    .restart local v8    # "value":Ljava/lang/String;
    :goto_5
    invoke-virtual {v2, v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setThemeColor(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1633
    :cond_d
    const-string/jumbo v8, "000000"

    goto :goto_5
.end method

.method private getCorrectThemeClr(I)I
    .locals 1
    .param p1, "theme"    # I

    .prologue
    .line 681
    if-ltz p1, :cond_0

    const/4 v0, 0x4

    if-ge p1, v0, :cond_0

    .line 682
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_1

    .line 683
    add-int/lit8 p1, p1, 0x1

    .line 689
    .end local p1    # "theme":I
    :cond_0
    :goto_0
    return p1

    .line 685
    .restart local p1    # "theme":I
    :cond_1
    add-int/lit8 p1, p1, -0x1

    goto :goto_0
.end method

.method private imageColumWidth(I)I
    .locals 8
    .param p1, "columNum"    # I

    .prologue
    .line 408
    const/4 v2, 0x0

    .line 410
    .local v2, "totalWidth":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_1

    .line 412
    iget-object v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v4, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetCellWidth(I)Ljava/lang/Double;

    move-result-object v3

    .line 414
    .local v3, "width":Ljava/lang/Double;
    if-nez v3, :cond_0

    .line 416
    add-int/lit8 v2, v2, 0x46

    .line 410
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 418
    :cond_0
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide v6, 0x4051800000000000L    # 70.0

    mul-double/2addr v4, v6

    sget-object v6, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_WIDTH_XLSX:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    div-double/2addr v4, v6

    double-to-float v0, v4

    .line 419
    .local v0, "actualwidth":F
    int-to-float v4, v2

    add-float/2addr v4, v0

    float-to-int v2, v4

    goto :goto_1

    .line 423
    .end local v0    # "actualwidth":F
    .end local v3    # "width":Ljava/lang/Double;
    :cond_1
    add-int/lit8 v4, v2, 0x19

    return v4
.end method

.method private init()V
    .locals 2

    .prologue
    .line 202
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;-><init>()V

    .line 203
    .local v0, "slideDimention":Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
    new-instance v1, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v1, v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;-><init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 204
    return-void
.end method

.method private prepareDrawings()V
    .locals 24

    .prologue
    .line 1779
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getThemeObj()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    .line 1781
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v2, :cond_0

    .line 1782
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->setTheme(Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;)V

    .line 1783
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorSchema()Ljava/util/Map;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->addThemeColorSchema(Ljava/util/Map;)V

    .line 1786
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getCellAnchor()Ljava/util/ArrayList;

    move-result-object v9

    .line 1789
    .local v9, "cellAnchorList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;>;"
    const/4 v14, 0x0

    .line 1790
    .local v14, "topCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v8, 0x0

    .line 1791
    .local v8, "bottomCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v13, 0x0

    .line 1792
    .local v13, "rightCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v11, 0x0

    .line 1794
    .local v11, "leftCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v10, 0x0

    .local v10, "k":I
    :goto_0
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v10, v2, :cond_7

    .line 1795
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    .line 1796
    .local v7, "cellAnchor":Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
    const/4 v4, 0x0

    .line 1797
    .local v4, "bottomCoordinate":I
    const/4 v6, 0x0

    .line 1798
    .local v6, "rightCoordinate":I
    const/4 v3, 0x0

    .line 1799
    .local v3, "topCoordinate":I
    const/4 v5, 0x0

    .line 1801
    .local v5, "leftCoordinate":I
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getFromRow()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getRowList()Ljava/util/ArrayList;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getRowPosition(ILjava/util/ArrayList;)F

    move-result v2

    float-to-int v3, v2

    .line 1804
    int-to-float v2, v3

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getFromRowOff()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v20, v0

    const v21, 0x483a0900    # 190500.0f

    div-float v20, v20, v21

    const/high16 v21, 0x41f00000    # 30.0f

    mul-float v20, v20, v21

    add-float v2, v2, v20

    float-to-int v3, v2

    .line 1806
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getFromCol()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->imageColumWidth(I)I

    move-result v5

    .line 1807
    int-to-float v2, v5

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getFromColOff()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v20, v0

    const v21, 0x4914d400    # 609600.0f

    div-float v20, v20, v21

    const/high16 v21, 0x428c0000    # 70.0f

    mul-float v20, v20, v21

    add-float v2, v2, v20

    float-to-int v5, v2

    .line 1810
    instance-of v2, v7, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;

    if-eqz v2, :cond_3

    move-object v15, v7

    .line 1811
    check-cast v15, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;

    .line 1813
    .local v15, "twoCellAnchor":Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->getToRow()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getRowList()Ljava/util/ArrayList;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getRowPosition(ILjava/util/ArrayList;)F

    move-result v2

    float-to-int v4, v2

    .line 1816
    int-to-float v2, v4

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->getToRowOff()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v20, v0

    const v21, 0x483a0900    # 190500.0f

    div-float v20, v20, v21

    const/high16 v21, 0x41f00000    # 30.0f

    mul-float v20, v20, v21

    add-float v2, v2, v20

    float-to-int v4, v2

    .line 1819
    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->getToCol()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->imageColumWidth(I)I

    move-result v6

    .line 1820
    int-to-float v2, v6

    invoke-virtual {v15}, Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;->getToColOff()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v20, v0

    const v21, 0x4914d400    # 609600.0f

    div-float v20, v20, v21

    const/high16 v21, 0x428c0000    # 70.0f

    mul-float v20, v20, v21

    add-float v2, v2, v20

    float-to-int v6, v2

    .line 1835
    .end local v15    # "twoCellAnchor":Lcom/samsung/thumbnail/office/ooxml/excel/TwoCellAnchor;
    :cond_1
    :goto_1
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getType()Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    move-result-object v2

    sget-object v20, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->PICTURE:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    move-object/from16 v0, v20

    if-ne v2, v0, :cond_4

    move-object/from16 v2, p0

    .line 1836
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawPictures(IIIILcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    .line 1794
    :cond_2
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 1823
    :cond_3
    instance-of v2, v7, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;

    if-eqz v2, :cond_1

    move-object v12, v7

    .line 1824
    check-cast v12, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;

    .line 1826
    .local v12, "oneCellAnchor":Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;->getCY()J

    move-result-wide v18

    .line 1827
    .local v18, "yval":J
    move-wide/from16 v0, v18

    long-to-float v2, v0

    const v20, 0x483a0900    # 190500.0f

    div-float v2, v2, v20

    const/high16 v20, 0x41f00000    # 30.0f

    mul-float v2, v2, v20

    float-to-int v2, v2

    add-int v4, v3, v2

    .line 1830
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;->getCX()J

    move-result-wide v16

    .line 1831
    .local v16, "xval":J
    const-wide/32 v20, 0x94d40

    div-long v20, v16, v20

    const-wide/16 v22, 0x46

    mul-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v2, v0

    add-int v6, v5, v2

    goto :goto_1

    .line 1838
    .end local v12    # "oneCellAnchor":Lcom/samsung/thumbnail/office/ooxml/excel/OneCellAnchor;
    .end local v16    # "xval":J
    .end local v18    # "yval":J
    :cond_4
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getType()Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    move-result-object v2

    sget-object v20, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->SHAPE:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    move-object/from16 v0, v20

    if-ne v2, v0, :cond_5

    move-object/from16 v2, p0

    .line 1839
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawShapes(IIIILcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V

    goto :goto_2

    .line 1841
    :cond_5
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getType()Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    move-result-object v2

    sget-object v20, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;->CHART:Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor$EAnchorObjectType;

    move-object/from16 v0, v20

    if-ne v2, v0, :cond_2

    .line 1842
    if-nez v14, :cond_6

    .line 1843
    new-instance v14, Ljava/util/ArrayList;

    .end local v14    # "topCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 1844
    .restart local v14    # "topCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "bottomCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1845
    .restart local v8    # "bottomCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v13, Ljava/util/ArrayList;

    .end local v13    # "rightCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1846
    .restart local v13    # "rightCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v11, Ljava/util/ArrayList;

    .end local v11    # "leftCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1848
    .restart local v11    # "leftCoordinates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_6
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1849
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1850
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1851
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1854
    .end local v3    # "topCoordinate":I
    .end local v4    # "bottomCoordinate":I
    .end local v5    # "leftCoordinate":I
    .end local v6    # "rightCoordinate":I
    .end local v7    # "cellAnchor":Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    iget v2, v2, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->chartcnt:I

    if-lez v2, :cond_8

    if-eqz v14, :cond_8

    .line 1856
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v8, v11, v13}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawCharts(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 1859
    :cond_8
    return-void
.end method

.method private processParagraph(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .locals 6
    .param p1, "paragraph"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .param p2, "shapeProp"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    const/4 v5, -0x1

    const/high16 v4, 0x41a00000    # 20.0f

    const/high16 v3, 0x40a00000    # 5.0f

    .line 2894
    if-nez p1, :cond_0

    .line 2895
    const/4 v0, 0x0

    .line 2938
    :goto_0
    return-object v0

    .line 2897
    :cond_0
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    invoke-direct {v0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 2905
    .local v0, "para":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2906
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "right"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2909
    sget-object v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    .line 2918
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2921
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getAfter()I

    move-result v1

    if-eq v1, v5, :cond_4

    .line 2922
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getAfter()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBottomSpacing(F)V

    .line 2928
    :goto_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getBefore()I

    move-result v1

    if-eq v1, v5, :cond_5

    .line 2929
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getSpacing()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocSpacingProperty;->getBefore()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTopSpacing(F)V

    .line 2936
    :cond_2
    :goto_3
    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->processRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    goto :goto_0

    .line 2911
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;->getJustify()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "center"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2914
    sget-object v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setAlignment(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;)V

    goto :goto_1

    .line 2925
    :cond_4
    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setBottomSpacing(F)V

    goto :goto_2

    .line 2932
    :cond_5
    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setTopSpacing(F)V

    goto :goto_3
.end method

.method private processRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V
    .locals 11
    .param p1, "canvasPara"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .param p2, "para"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .param p3, "shapeProp"    # Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    .prologue
    .line 2955
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_0
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v5, v9, :cond_6

    .line 2956
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    .line 2957
    .local v7, "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    new-instance v3, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct {v3}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 2961
    .local v3, "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    const/4 v9, -0x1

    invoke-virtual {v3, v9}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 2962
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F

    move-result v4

    .line 2963
    .local v4, "fontSize":F
    const/high16 v9, 0x3fc00000    # 1.5f

    mul-float/2addr v4, v9

    .line 2964
    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 2966
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 2974
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getFldSmplInstr()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getFldSmplInstr()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "PAGE"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2976
    const/4 v9, 0x0

    invoke-virtual {p2, v9}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->setFldSmplInstr(Ljava/lang/String;)V

    .line 3041
    :goto_1
    invoke-virtual {p1, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->addContent(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 2955
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2979
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getParaProperty()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;

    move-result-object v6

    .line 2980
    .local v6, "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    if-eqz v6, :cond_2

    .line 2981
    invoke-direct {p0, p1, v6, v3}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->applyParaProp(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 2983
    :cond_2
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v0

    .line 2985
    .local v0, "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    if-eqz v0, :cond_3

    .line 2986
    invoke-direct {p0, v0, v3}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->applyCharProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 3004
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v9

    if-nez v9, :cond_5

    .line 3009
    :cond_4
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getFontRef()Ljava/lang/String;

    move-result-object v2

    .line 3011
    .local v2, "defaultcolor":Ljava/lang/String;
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_5

    .line 3012
    invoke-virtual {p0, v2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getColorSchemakeyval(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 3013
    .local v8, "temp":Ljava/lang/String;
    if-eqz v8, :cond_5

    .line 3014
    const/high16 v9, -0x1000000

    const/16 v10, 0x10

    invoke-static {v8, v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v10

    or-int v1, v9, v10

    .line 3016
    .local v1, "colorr":I
    invoke-virtual {v3, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 3039
    .end local v1    # "colorr":I
    .end local v2    # "defaultcolor":Ljava/lang/String;
    .end local v8    # "temp":Ljava/lang/String;
    :cond_5
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->filterText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    goto :goto_1

    .line 3044
    .end local v0    # "charProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    .end local v3    # "drawRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v4    # "fontSize":F
    .end local v6    # "paraProp":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocParaProperties;
    .end local v7    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    :cond_6
    return-void
.end method

.method private setHeightToRows()V
    .locals 10

    .prologue
    .line 1720
    iget-object v6, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getRowHeights()Ljava/util/HashMap;

    move-result-object v4

    .line 1722
    .local v4, "rowHeights":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Double;>;"
    if-nez v4, :cond_1

    .line 1744
    :cond_0
    return-void

    .line 1726
    :cond_1
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1729
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Double;>;>;"
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1730
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1731
    .local v2, "pairs":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Double;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    .line 1733
    .local v0, "height":Ljava/lang/Double;
    iget-object v6, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getRowList()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1734
    .local v5, "size":I
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-ge v6, v5, :cond_2

    .line 1737
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v7, v6}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getRow(I)Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v3

    .line 1739
    .local v3, "row":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    if-eqz v3, :cond_2

    .line 1740
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide/high16 v8, 0x4034000000000000L    # 20.0

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x402e000000000000L    # 15.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 1741
    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v6

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->setRowHeight(F)V

    goto :goto_0
.end method

.method private text(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Lcom/samsung/thumbnail/customview/word/Run;)V
    .locals 4
    .param p1, "newCanvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "inNewRun"    # Lcom/samsung/thumbnail/customview/word/Run;

    .prologue
    .line 3235
    move-object v1, p3

    .line 3236
    .local v1, "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    if-eqz v2, :cond_2

    .line 3237
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getStyleId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 3251
    :cond_0
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v2, v3, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->createCharacterStyleForCanvas(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 3258
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getVertAlign()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUPERSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    if-ne v2, v3, :cond_3

    .line 3261
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUPERSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/word/Run;->setVertivalAlignment(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 3268
    :cond_1
    :goto_0
    invoke-virtual {v1, p2}, Lcom/samsung/thumbnail/customview/word/Run;->setRunText(Ljava/lang/String;)V

    .line 3271
    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F

    move-result v0

    .line 3272
    .local v0, "fontSize":F
    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v2

    .line 3273
    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 3278
    .end local v0    # "fontSize":F
    :cond_2
    return-void

    .line 3262
    :cond_3
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getVertAlign()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUBSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    if-ne v2, v3, :cond_1

    .line 3265
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUBSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/customview/word/Run;->setVertivalAlignment(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    goto :goto_0
.end method


# virtual methods
.method public SetAlignment(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;IILjava/lang/String;)V
    .locals 4
    .param p1, "cell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "alignindex"    # I
    .param p3, "index"    # I
    .param p4, "text"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 791
    const-string/jumbo v0, ""

    .line 792
    .local v0, "align":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v1

    .line 793
    .local v1, "excelStyle":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFWraptext()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge p2, v2, :cond_4

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFWraptext()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 795
    invoke-virtual {p1, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setWrapContent(Z)V

    .line 801
    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getAlingmentHorizontal()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 802
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getAlingmentHorizontal()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "align":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 803
    .restart local v0    # "align":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 804
    const-string/jumbo v2, "centerContinuous"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_5

    .line 806
    const-string/jumbo v2, "center"

    invoke-virtual {p0, p1, v2, p4}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->alignCellText(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    :cond_1
    :goto_1
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getAlingmentVertical()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 827
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getAlingmentVertical()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "align":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 828
    .restart local v0    # "align":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 829
    const-string/jumbo v2, "top"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_8

    .line 830
    const-string/jumbo v2, "top"

    invoke-virtual {p0, p1, v2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->alignCellTextVertical(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;)V

    .line 850
    :cond_2
    :goto_2
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getStyleTextAlignmentIndent()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 851
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getStyleTextAlignmentIndent()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTextIndent(I)V

    .line 854
    :cond_3
    return-void

    .line 796
    :cond_4
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getShrinkToFit()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge p2, v2, :cond_0

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getShrinkToFit()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 798
    invoke-virtual {p1, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setShrinkToFit(Z)V

    goto/16 :goto_0

    .line 809
    :cond_5
    invoke-virtual {p0, p1, v0, p4}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->alignCellText(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 813
    :cond_6
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetTextDataType()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string/jumbo v3, "string"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 817
    const-string/jumbo v0, "left"

    .line 823
    :goto_3
    invoke-virtual {p0, p1, v0, p4}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->alignCellText(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 819
    :cond_7
    const-string/jumbo v0, "right"

    goto :goto_3

    .line 831
    :cond_8
    const-string/jumbo v2, "center"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_9

    .line 832
    const-string/jumbo v2, "center"

    invoke-virtual {p0, p1, v2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->alignCellTextVertical(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;)V

    goto :goto_2

    .line 833
    :cond_9
    const-string/jumbo v2, "distributed"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_a

    .line 834
    const-string/jumbo v2, "bottom"

    invoke-virtual {p0, p1, v2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->alignCellTextVertical(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;)V

    goto :goto_2

    .line 835
    :cond_a
    const-string/jumbo v2, "justify"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_b

    .line 836
    const-string/jumbo v2, "bottom"

    invoke-virtual {p0, p1, v2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->alignCellTextVertical(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 838
    :cond_b
    const-string/jumbo v2, "bottom"

    invoke-virtual {p0, p1, v2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->alignCellTextVertical(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 842
    :cond_c
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetTextDataType()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string/jumbo v3, "string"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 844
    const-string/jumbo v0, "bottom"

    .line 847
    :cond_d
    invoke-virtual {p0, p1, v0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->alignCellTextVertical(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Z)V
    .locals 4
    .param p1, "newCanvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "showGrid"    # Z

    .prologue
    const v3, 0x3dcccccd    # 0.1f

    .line 257
    if-eqz p2, :cond_3

    .line 258
    const/4 v0, 0x0

    .line 260
    .local v0, "border":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v0

    .line 262
    if-nez v0, :cond_0

    .line 263
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_BORDER_COLOROBJ:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 267
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v0

    .line 269
    if-nez v0, :cond_1

    .line 270
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_BORDER_COLOROBJ:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 274
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v0

    .line 276
    if-nez v0, :cond_2

    .line 277
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_BORDER_COLOROBJ:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 281
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v0

    .line 283
    if-nez v0, :cond_3

    .line 284
    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v2, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_BORDER_COLOROBJ:Lorg/apache/poi/java/awt/Color;

    invoke-virtual {p1, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 288
    .end local v0    # "border":Lcom/samsung/thumbnail/customview/tables/TableBorder;
    :cond_3
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->addCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 289
    return-void
.end method

.method public addtextonshape(Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;Lcom/samsung/thumbnail/customview/word/ShapesController;)V
    .locals 6
    .param p1, "cellAnchor"    # Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;
    .param p2, "shapesController"    # Lcom/samsung/thumbnail/customview/word/ShapesController;

    .prologue
    .line 2783
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getPara()Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;

    move-result-object v2

    .line 2785
    .local v2, "excelpara":Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;
    if-eqz v2, :cond_0

    .line 2787
    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;->getParaList()Ljava/util/List;

    move-result-object v1

    .line 2789
    .local v1, "excelParalist":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 2813
    .end local v1    # "excelParalist":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    :cond_0
    return-void

    .line 2792
    .restart local v1    # "excelParalist":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    :cond_1
    const/4 v3, 0x0

    .local v3, "paracount":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 2793
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getShapeProperty()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->processParagraph(Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    move-result-object v0

    .line 2796
    .local v0, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    if-eqz v0, :cond_2

    .line 2799
    const-string/jumbo v4, "ctr"

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->getvAlign()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2800
    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    .line 2809
    :goto_1
    invoke-virtual {p2, v0}, Lcom/samsung/thumbnail/customview/word/ShapesController;->addParagraph(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V

    .line 2792
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2801
    :cond_3
    const-string/jumbo v4, "b"

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/drawing/XExcelShapePara;->getBodyProp()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFTextShape$BodyProperty;->getvAlign()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2803
    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->BOTTOM:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto :goto_1

    .line 2805
    :cond_4
    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;->TOP:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setVerticalAlignmentElement(Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalAlignmentElement;)V

    goto :goto_1
.end method

.method public alignCellText(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "cell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "align"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 887
    if-eqz p2, :cond_1

    .line 889
    invoke-static {p3}, Lcom/samsung/thumbnail/util/Utils;->isRTLText(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 890
    const-string/jumbo p2, "right"

    .line 892
    :cond_0
    invoke-virtual {p1, p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setHorizontalAlignment(Ljava/lang/String;)V

    .line 894
    :cond_1
    return-void
.end method

.method public alignCellTextVertical(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;)V
    .locals 0
    .param p1, "cell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "align"    # Ljava/lang/String;

    .prologue
    .line 897
    if-eqz p2, :cond_0

    .line 898
    invoke-virtual {p1, p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setVerticalAlignment(Ljava/lang/String;)V

    .line 900
    :cond_0
    return-void
.end method

.method public alphaRow()V
    .locals 8

    .prologue
    .line 541
    invoke-static {}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->alpha()[Ljava/lang/String;

    move-result-object v3

    .line 543
    .local v3, "header":[Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->createNewRow()V

    .line 545
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget v7, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    if-ge v4, v7, :cond_3

    .line 547
    new-instance v5, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct {v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    .line 549
    .local v5, "newCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    if-nez v4, :cond_2

    .line 550
    invoke-direct {p0, v5}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->apply_tbl_first_cell(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 559
    :goto_1
    if-eqz v4, :cond_1

    .line 560
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->textStyleforBlankRowandColumn()V

    .line 561
    const-string/jumbo v7, "bottom"

    invoke-virtual {v5, v7}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setVerticalAlignment(Ljava/lang/String;)V

    .line 562
    aget-object v7, v3, v4

    if-eqz v7, :cond_1

    aget-object v7, v3, v4

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_1

    .line 563
    new-instance v0, Lcom/samsung/thumbnail/customview/word/Paragraph;

    iget-object v7, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v0, v7}, Lcom/samsung/thumbnail/customview/word/Paragraph;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 565
    .local v0, "currentPara":Lcom/samsung/thumbnail/customview/word/Paragraph;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 566
    .local v1, "currentParaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    new-instance v6, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct {v6}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 567
    .local v6, "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    const-string/jumbo v2, "center"

    .line 568
    .local v2, "hAlign":Ljava/lang/String;
    aget-object v7, v3, v4

    invoke-static {v7}, Lcom/samsung/thumbnail/util/Utils;->isRTLText(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 569
    const-string/jumbo v2, "right"

    .line 571
    :cond_0
    invoke-virtual {v5, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setHorizontalAlignment(Ljava/lang/String;)V

    .line 573
    aget-object v7, v3, v4

    invoke-direct {p0, v5, v7, v6}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->text(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 574
    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/customview/word/Paragraph;->AddRun(Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 575
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 576
    invoke-virtual {v5, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setParagraphList(Ljava/util/ArrayList;)V

    .line 580
    .end local v0    # "currentPara":Lcom/samsung/thumbnail/customview/word/Paragraph;
    .end local v1    # "currentParaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    .end local v2    # "hAlign":Ljava/lang/String;
    .end local v6    # "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_1
    const/4 v7, 0x1

    invoke-virtual {p0, v5, v7}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Z)V

    .line 545
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 552
    :cond_2
    invoke-direct {p0, v5, v4}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->apply_table03(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V

    goto :goto_1

    .line 587
    .end local v5    # "newCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    :cond_3
    return-void
.end method

.method public blankRowandColumnDisplay(II)V
    .locals 2
    .param p1, "previousrow"    # I
    .param p2, "k"    # I

    .prologue
    .line 3145
    :goto_0
    iget v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->ROW_DISPLAY:I

    if-gt p1, v1, :cond_2

    .line 3146
    const/4 v0, 0x0

    .line 3149
    .local v0, "colid":I
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->createNewRow()V

    .line 3152
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowHeaders()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3153
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numericCol()V

    .line 3154
    add-int/lit8 v0, v0, 0x1

    .line 3157
    :cond_0
    :goto_1
    iget v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    if-ge v0, v1, :cond_1

    .line 3159
    invoke-virtual {p0, p1, v0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawColumn(II)V

    .line 3160
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3164
    :cond_1
    add-int/lit8 p1, p1, 0x1

    .line 3165
    goto :goto_0

    .line 3167
    .end local v0    # "colid":I
    :cond_2
    return-void
.end method

.method public cellAttributesProperties(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V
    .locals 21
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "fillid"    # I

    .prologue
    .line 1251
    if-nez p2, :cond_1

    .line 1325
    :cond_0
    :goto_0
    return-void

    .line 1254
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getFills()Ljava/util/ArrayList;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 1255
    .local v8, "fillListSize":I
    move/from16 v0, p2

    if-le v8, v0, :cond_0

    .line 1259
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getFill(I)Lcom/samsung/thumbnail/office/ooxml/excel/IFill;

    move-result-object v7

    .line 1260
    .local v7, "fill":Lcom/samsung/thumbnail/office/ooxml/excel/IFill;
    instance-of v0, v7, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    move/from16 v19, v0

    if-eqz v19, :cond_7

    move-object v9, v7

    .line 1261
    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    .line 1263
    .local v9, "gradientFill":Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->getStopLst()Ljava/util/ArrayList;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 1264
    .local v14, "stops":I
    const/4 v11, 0x0

    .line 1265
    .local v11, "i":I
    const/4 v11, 0x0

    :goto_1
    if-ge v11, v14, :cond_6

    .line 1266
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;->getStopLst()Ljava/util/ArrayList;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;

    .line 1267
    .local v13, "stop":Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;
    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v4

    .line 1268
    .local v4, "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    if-nez v4, :cond_3

    .line 1265
    :cond_2
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 1271
    :cond_3
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_4

    .line 1272
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v2

    .line 1273
    .local v2, "actColor":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v19

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    .line 1274
    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1275
    invoke-virtual {v13, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    goto :goto_2

    .line 1277
    .end local v2    # "actColor":Ljava/lang/String;
    :cond_4
    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getThemeColor()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_2

    .line 1278
    invoke-virtual {v13}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getThemeColor()Ljava/lang/String;

    move-result-object v15

    .line 1279
    .local v15, "theme":Ljava/lang/String;
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 1280
    .local v16, "themeid":I
    const-string/jumbo v17, "FFFFFF"

    .line 1281
    .local v17, "themergbclr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getThemeObj()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v19

    if-eqz v19, :cond_5

    .line 1282
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getThemeObj()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getExcelColorSchema()Ljava/util/ArrayList;

    move-result-object v19

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCorrectThemeClr(I)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "themergbclr":Ljava/lang/String;
    check-cast v17, Ljava/lang/String;

    .line 1285
    .restart local v17    # "themergbclr":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1286
    invoke-virtual {v13, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    goto :goto_2

    .line 1289
    .end local v4    # "color":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v13    # "stop":Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill$Stop;
    .end local v15    # "theme":Ljava/lang/String;
    .end local v16    # "themeid":I
    .end local v17    # "themergbclr":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;)V

    goto/16 :goto_0

    .line 1291
    .end local v9    # "gradientFill":Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;
    .end local v11    # "i":I
    .end local v14    # "stops":I
    :cond_7
    instance-of v0, v7, Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;

    move/from16 v19, v0

    if-eqz v19, :cond_0

    move-object v12, v7

    .line 1292
    check-cast v12, Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;

    .line 1293
    .local v12, "patternFill":Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;->getFGColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v19

    if-eqz v19, :cond_0

    .line 1294
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/PatternFill;->getFGColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v6

    .line 1295
    .local v6, "fgColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_8

    .line 1296
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x2

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 1297
    .local v10, "hexcolorcode":Ljava/lang/String;
    const/16 v19, 0x10

    move/from16 v0, v19

    invoke-static {v10, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    .line 1298
    .local v5, "colorvalue":I
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v3, v5}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 1299
    .local v3, "bgColor":Lorg/apache/poi/java/awt/Color;
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    goto/16 :goto_0

    .line 1300
    .end local v3    # "bgColor":Lorg/apache/poi/java/awt/Color;
    .end local v5    # "colorvalue":I
    .end local v10    # "hexcolorcode":Ljava/lang/String;
    :cond_8
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getThemeColor()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_0

    .line 1302
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getThemeColor()Ljava/lang/String;

    move-result-object v15

    .line 1303
    .restart local v15    # "theme":Ljava/lang/String;
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 1304
    .restart local v16    # "themeid":I
    const-string/jumbo v17, "FFFFFF"

    .line 1305
    .restart local v17    # "themergbclr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getThemeObj()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v19

    if-eqz v19, :cond_9

    .line 1306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getThemeObj()Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getExcelColorSchema()Ljava/util/ArrayList;

    move-result-object v19

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCorrectThemeClr(I)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "themergbclr":Ljava/lang/String;
    check-cast v17, Ljava/lang/String;

    .line 1310
    .restart local v17    # "themergbclr":Ljava/lang/String;
    :cond_9
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_a

    .line 1311
    invoke-virtual {v6}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getTint()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->displayRgbfromThemeandtint(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/poi/java/awt/Color;

    move-result-object v18

    .line 1314
    .local v18, "tintThemeColor":Lorg/apache/poi/java/awt/Color;
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    invoke-virtual/range {v18 .. v18}, Lorg/apache/poi/java/awt/Color;->getRGB()I

    move-result v19

    const v20, 0xffffff

    and-int v19, v19, v20

    move/from16 v0, v19

    invoke-direct {v3, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 1316
    .restart local v3    # "bgColor":Lorg/apache/poi/java/awt/Color;
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    goto/16 :goto_0

    .line 1318
    .end local v3    # "bgColor":Lorg/apache/poi/java/awt/Color;
    .end local v18    # "tintThemeColor":Lorg/apache/poi/java/awt/Color;
    :cond_a
    const/16 v19, 0x10

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    .line 1319
    .restart local v5    # "colorvalue":I
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v3, v5}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 1320
    .restart local v3    # "bgColor":Lorg/apache/poi/java/awt/Color;
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    goto/16 :goto_0
.end method

.method public cellBorderStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .locals 2
    .param p1, "cellborder"    # Ljava/lang/String;

    .prologue
    .line 862
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 863
    .local v0, "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    const-string/jumbo v1, "nil"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 864
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NIL:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 865
    :cond_0
    const-string/jumbo v1, "thin"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 866
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 867
    :cond_1
    const-string/jumbo v1, "thick"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "medium"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 869
    :cond_2
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->THICK:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 870
    :cond_3
    const-string/jumbo v1, "mediumDashed"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_4

    const-string/jumbo v1, "slantDashDot"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_4

    const-string/jumbo v1, "dashDot"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 873
    :cond_4
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DASHED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 874
    :cond_5
    const-string/jumbo v1, "mediumDashDot"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_6

    const-string/jumbo v1, "dashed"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_6

    const-string/jumbo v1, "mediumDashDotDot"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_6

    const-string/jumbo v1, "dotted"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_6

    const-string/jumbo v1, "hair"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_7

    .line 879
    :cond_6
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOTTED:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 880
    :cond_7
    const-string/jumbo v1, "double"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_8

    .line 881
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->DOUBLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 883
    :cond_8
    return-object v0
.end method

.method public checkCurrentcolumWithPreviousColumn(II)I
    .locals 1
    .param p1, "currentcolum"    # I
    .param p2, "previouscolum"    # I

    .prologue
    .line 665
    :goto_0
    if-le p1, p2, :cond_0

    .line 669
    iget v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    invoke-virtual {p0, v0, p2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawColumn(II)V

    .line 670
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 673
    :cond_0
    return p2
.end method

.method public checkPreviousColumnwithTotalColum(I)I
    .locals 1
    .param p1, "previouscolum"    # I

    .prologue
    .line 1706
    :goto_0
    iget v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    if-ge p1, v0, :cond_0

    .line 1709
    iget v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    invoke-virtual {p0, v0, p1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawColumn(II)V

    .line 1710
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 1712
    :cond_0
    return p1
.end method

.method public displaynumberofblankrow(IIII)V
    .locals 2
    .param p1, "chartrow"    # I
    .param p2, "picrow"    # I
    .param p3, "shaperow"    # I
    .param p4, "previousrow"    # I

    .prologue
    .line 3096
    if-ge p4, p2, :cond_0

    .line 3097
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p0, p4, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawlastblankrowcolumn(II)I

    move-result p4

    .line 3100
    :cond_0
    if-ge p4, p1, :cond_1

    .line 3101
    add-int/lit8 v1, p1, 0x1

    invoke-virtual {p0, p4, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawlastblankrowcolumn(II)I

    move-result p4

    .line 3104
    :cond_1
    if-ge p4, p3, :cond_2

    .line 3105
    add-int/lit8 v1, p3, 0x1

    invoke-virtual {p0, p4, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawlastblankrowcolumn(II)I

    move-result p4

    .line 3108
    :cond_2
    iget v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->ROW_DISPLAY:I

    if-ge p4, v1, :cond_3

    .line 3109
    const/4 v0, 0x0

    .line 3110
    .local v0, "k":I
    invoke-virtual {p0, p4, v0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->blankRowandColumnDisplay(II)V

    .line 3112
    .end local v0    # "k":I
    :cond_3
    return-void
.end method

.method public drawBlankTable()V
    .locals 6

    .prologue
    .line 2270
    const/4 v0, 0x0

    .local v0, "chartrow":I
    const/4 v2, 0x0

    .local v2, "picrow":I
    const/4 v3, 0x0

    .line 2273
    .local v3, "shaperow":I
    iget-boolean v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->isThumbnailPreview:Z

    if-nez v4, :cond_1

    .line 2291
    iget v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->displaynumberofblankrow(IIII)V

    .line 2312
    :cond_0
    return-void

    .line 2295
    :cond_1
    const/16 v4, 0x14

    iput v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxRow:I

    .line 2298
    :goto_0
    iget v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    iget v5, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxRow:I

    if-gt v4, v5, :cond_0

    .line 2299
    const/4 v1, 0x0

    .line 2300
    .local v1, "colid":I
    iget-object v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->createNewRow()V

    .line 2301
    iget-object v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowHeaders()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2302
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numericCol()V

    .line 2303
    add-int/lit8 v1, v1, 0x1

    .line 2305
    :cond_2
    :goto_1
    iget v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    if-ge v1, v4, :cond_3

    .line 2306
    iget v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    invoke-virtual {p0, v4, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawColumn(II)V

    .line 2307
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2309
    :cond_3
    iget v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    goto :goto_0
.end method

.method public drawColumn(II)V
    .locals 9
    .param p1, "previousrow"    # I
    .param p2, "columnId"    # I

    .prologue
    const/high16 v8, 0x41a00000    # 20.0f

    .line 515
    new-instance v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct {v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    .line 517
    .local v1, "newCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3, p2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetCellWidth(I)Ljava/lang/Double;

    move-result-object v2

    .line 519
    .local v2, "width":Ljava/lang/Double;
    if-nez v2, :cond_1

    .line 520
    const/high16 v3, 0x428c0000    # 70.0f

    invoke-virtual {v1, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 526
    :goto_0
    const/4 v3, 0x1

    if-ne p1, v3, :cond_2

    .line 527
    invoke-virtual {v1, v8}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellHeight(F)V

    .line 528
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowGridlines()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 529
    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    sget-object v4, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_BORDER_COLOROBJ:Lorg/apache/poi/java/awt/Color;

    const v5, 0x3dcccccd    # 0.1f

    invoke-virtual {v1, v3, v4, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 537
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowGridlines()Z

    move-result v3

    invoke-virtual {p0, v1, v3}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Z)V

    .line 538
    return-void

    .line 522
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide v6, 0x4051800000000000L    # 70.0

    mul-double/2addr v4, v6

    sget-object v3, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->DEFAULT_WIDTH_XLSX:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    div-double/2addr v4, v6

    double-to-float v0, v4

    .line 523
    .local v0, "actualwidth":F
    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    goto :goto_0

    .line 533
    .end local v0    # "actualwidth":F
    :cond_2
    invoke-virtual {v1, v8}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellHeight(F)V

    goto :goto_1
.end method

.method public drawMergedTable()V
    .locals 20

    .prologue
    .line 1145
    const-string/jumbo v8, ":"

    .line 1147
    .local v8, "delims":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->excelVariable:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->mergecount:I

    move/from16 v18, v0

    if-lez v18, :cond_d

    .line 1148
    const/4 v10, 0x0

    .local v10, "i":I
    const/4 v4, 0x0

    .local v4, "chkrow1":I
    const/4 v2, 0x0

    .local v2, "chkcol1":I
    const/4 v5, 0x0

    .local v5, "chkrow2":I
    const/4 v3, 0x0

    .line 1150
    .local v3, "chkcol2":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->excelVariable:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->mergecount:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v10, v0, :cond_d

    .line 1151
    const/4 v9, 0x0

    .line 1152
    .local v9, "firststr":Ljava/lang/String;
    const/4 v14, 0x0

    .line 1153
    .local v14, "secondstr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetMergecell()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-le v0, v10, :cond_6

    .line 1154
    new-instance v15, Ljava/util/StringTokenizer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetMergecell()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v15, v0, v8}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    .local v15, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v15}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    .line 1157
    invoke-virtual {v15}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v14

    .line 1159
    const/4 v13, 0x0

    .line 1161
    .local v13, "rowmergeLocal":Z
    if-eqz v9, :cond_6

    if-eqz v14, :cond_6

    .line 1162
    const/4 v11, 0x0

    .local v11, "iCount":I
    const/4 v12, 0x0

    .line 1163
    .local v12, "jCount":I
    const-string/jumbo v18, "[^\\d]"

    const-string/jumbo v19, ""

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 1166
    const-string/jumbo v18, "[^\\d]"

    const-string/jumbo v19, ""

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1170
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numColumn(Ljava/lang/String;)I

    move-result v2

    .line 1171
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numColumn(Ljava/lang/String;)I

    move-result v3

    .line 1173
    if-eq v4, v5, :cond_0

    .line 1174
    const/4 v13, 0x1

    .line 1178
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->isThumbnailPreview:Z

    move/from16 v18, v0

    if-eqz v18, :cond_4

    .line 1179
    const/16 v18, 0x14

    move/from16 v0, v18

    if-ge v4, v0, :cond_1

    const/16 v18, 0x10

    move/from16 v0, v18

    if-lt v2, v0, :cond_2

    .line 1180
    :cond_1
    add-int/lit8 v10, v10, 0x1

    .line 1181
    goto/16 :goto_0

    .line 1183
    :cond_2
    const/16 v18, 0x14

    move/from16 v0, v18

    if-le v5, v0, :cond_3

    .line 1184
    const/16 v5, 0x14

    .line 1186
    :cond_3
    const/16 v18, 0x10

    move/from16 v0, v18

    if-le v3, v0, :cond_4

    .line 1187
    const/16 v3, 0x10

    .line 1192
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowHeaders()Z

    move-result v18

    if-nez v18, :cond_5

    .line 1195
    add-int/lit8 v4, v4, -0x1

    .line 1196
    add-int/lit8 v5, v5, -0x1

    .line 1197
    add-int/lit8 v2, v2, -0x1

    .line 1198
    add-int/lit8 v3, v3, -0x1

    .line 1201
    :cond_5
    move v11, v4

    :goto_1
    if-gt v11, v5, :cond_6

    .line 1202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getRow(I)Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v7

    .line 1204
    .local v7, "currentRowTemp":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    const/16 v17, 0x0

    .line 1205
    .local v17, "totalWidth":F
    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellCount()I

    move-result v18

    move/from16 v0, v18

    if-gt v0, v2, :cond_7

    .line 1242
    .end local v7    # "currentRowTemp":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    .end local v11    # "iCount":I
    .end local v12    # "jCount":I
    .end local v13    # "rowmergeLocal":Z
    .end local v15    # "st":Ljava/util/StringTokenizer;
    .end local v17    # "totalWidth":F
    :cond_6
    add-int/lit8 v10, v10, 0x1

    .line 1243
    goto/16 :goto_0

    .line 1210
    .restart local v7    # "currentRowTemp":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    .restart local v11    # "iCount":I
    .restart local v12    # "jCount":I
    .restart local v13    # "rowmergeLocal":Z
    .restart local v15    # "st":Ljava/util/StringTokenizer;
    .restart local v17    # "totalWidth":F
    :cond_7
    invoke-virtual {v7, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCell(I)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v16

    .line 1213
    .local v16, "startingCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v0, v13, :cond_8

    .line 1214
    if-ne v4, v11, :cond_a

    .line 1215
    const-string/jumbo v18, "restart"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellMergeValue(Ljava/lang/String;)V

    .line 1219
    :goto_2
    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setMergedCellKey(Ljava/lang/String;)V

    .line 1222
    :cond_8
    move v12, v2

    :goto_3
    if-gt v12, v3, :cond_9

    .line 1223
    invoke-virtual {v7}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCellCount()I

    move-result v18

    move/from16 v0, v18

    if-gt v0, v12, :cond_b

    .line 1237
    :cond_9
    invoke-virtual/range {v16 .. v17}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 1201
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 1217
    :cond_a
    const-string/jumbo v18, "continue"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellMergeValue(Ljava/lang/String;)V

    goto :goto_2

    .line 1228
    :cond_b
    invoke-virtual {v7, v12}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getCell(I)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    move-result-object v6

    .line 1230
    .local v6, "currentCellTemp":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual {v6}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v18

    add-float v17, v17, v18

    .line 1232
    if-eq v12, v2, :cond_c

    .line 1233
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setHideCell(Z)V

    .line 1222
    :cond_c
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 1245
    .end local v2    # "chkcol1":I
    .end local v3    # "chkcol2":I
    .end local v4    # "chkrow1":I
    .end local v5    # "chkrow2":I
    .end local v6    # "currentCellTemp":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v7    # "currentRowTemp":Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
    .end local v9    # "firststr":Ljava/lang/String;
    .end local v10    # "i":I
    .end local v11    # "iCount":I
    .end local v12    # "jCount":I
    .end local v13    # "rowmergeLocal":Z
    .end local v14    # "secondstr":Ljava/lang/String;
    .end local v15    # "st":Ljava/util/StringTokenizer;
    .end local v16    # "startingCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v17    # "totalWidth":F
    :cond_d
    return-void
.end method

.method public drawPictures(IIIILcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V
    .locals 34
    .param p1, "top"    # I
    .param p2, "bottom"    # I
    .param p3, "left"    # I
    .param p4, "right"    # I
    .param p5, "cellAnchor"    # Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    .prologue
    .line 2340
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;->getAllPictureData()Ljava/util/HashMap;

    move-result-object v29

    .line 2342
    .local v29, "pictRidMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;>;"
    const/16 v27, 0x0

    .line 2343
    .local v27, "picNames":Ljava/lang/String;
    const/4 v9, 0x0

    .line 2345
    .local v9, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getBlipId()Ljava/lang/String;

    move-result-object v21

    .line 2346
    .local v21, "imageRid":Ljava/lang/String;
    move-object/from16 v0, v29

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;

    .line 2347
    .local v26, "picData":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;
    if-nez v26, :cond_1

    .line 2469
    :cond_0
    :goto_0
    return-void

    .line 2350
    :cond_1
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;->getFileName()Ljava/lang/String;

    move-result-object v27

    .line 2352
    if-eqz v27, :cond_0

    .line 2353
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getShapeProperty()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v28

    .line 2357
    .local v28, "picProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v27

    .line 2358
    const-string/jumbo v3, ".wmf"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2360
    const/16 v32, 0x0

    .line 2361
    .local v32, "wmfFos":Ljava/io/FileOutputStream;
    const/16 v22, 0x0

    .line 2363
    .local v22, "is":Ljava/io/InputStream;
    :try_start_0
    new-instance v33, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "DRAW"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v33

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_f
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2366
    .end local v32    # "wmfFos":Ljava/io/FileOutputStream;
    .local v33, "wmfFos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v23, Ljava/io/FileInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-direct {v0, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_d
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2368
    .end local v22    # "is":Ljava/io/InputStream;
    .local v23, "is":Ljava/io/InputStream;
    :try_start_2
    new-instance v31, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;

    invoke-static/range {v23 .. v23}, Lorg/apache/poi/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v3

    move-object/from16 v0, v31

    invoke-direct {v0, v3}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;-><init>([B)V

    .line 2372
    .local v31, "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeHeight()J

    move-result-wide v4

    long-to-float v14, v4

    .line 2374
    .local v14, "actHeight":F
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getShapeWidth()J

    move-result-wide v4

    long-to-float v15, v4

    .line 2376
    .local v15, "actWidth":F
    move-object/from16 v0, v31

    invoke-virtual {v0, v14}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->setActHeight(F)V

    .line 2377
    move-object/from16 v0, v31

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->setActWidth(F)V

    .line 2379
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->drawImage()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2381
    .local v2, "wmfBbitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;->flipTheImage()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2382
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 2383
    .local v7, "matrix":Landroid/graphics/Matrix;
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v7, v3, v4}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 2384
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 2388
    .local v19, "flippedBitmap":Landroid/graphics/Bitmap;
    if-eqz v19, :cond_2

    .line 2389
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v19

    move-object/from16 v1, v33

    invoke-virtual {v0, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2398
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v19    # "flippedBitmap":Landroid/graphics/Bitmap;
    :cond_2
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/DRAW"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_e
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v9

    .line 2406
    if-eqz v23, :cond_3

    .line 2408
    :try_start_3
    invoke-virtual/range {v23 .. v23}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 2414
    :cond_3
    :goto_2
    if-eqz v33, :cond_e

    .line 2416
    :try_start_4
    invoke-virtual/range {v33 .. v33}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    move-object/from16 v22, v23

    .end local v23    # "is":Ljava/io/InputStream;
    .restart local v22    # "is":Ljava/io/InputStream;
    move-object/from16 v32, v33

    .line 2462
    .end local v2    # "wmfBbitmap":Landroid/graphics/Bitmap;
    .end local v14    # "actHeight":F
    .end local v15    # "actWidth":F
    .end local v22    # "is":Ljava/io/InputStream;
    .end local v31    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    .end local v33    # "wmfFos":Ljava/io/FileOutputStream;
    :cond_4
    :goto_3
    new-instance v8, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;

    sub-int v12, p4, p3

    sub-int v13, p2, p1

    move/from16 v10, p3

    move/from16 v11, p1

    invoke-direct/range {v8 .. v13}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;-><init>(Landroid/graphics/Bitmap;IIII)V

    .line 2465
    .local v8, "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getRotation()I

    move-result v3

    invoke-virtual {v8, v3}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setRotationAngle(I)V

    .line 2466
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v8, v3}, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->setDocumentType(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;)V

    .line 2467
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v8}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_0

    .line 2392
    .end local v8    # "bitmapContents":Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
    .restart local v2    # "wmfBbitmap":Landroid/graphics/Bitmap;
    .restart local v14    # "actHeight":F
    .restart local v15    # "actWidth":F
    .restart local v23    # "is":Ljava/io/InputStream;
    .restart local v31    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    .restart local v33    # "wmfFos":Ljava/io/FileOutputStream;
    :cond_5
    if-eqz v2, :cond_2

    .line 2393
    :try_start_5
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    move-object/from16 v0, v33

    invoke-virtual {v2, v3, v4, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_e
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_1

    .line 2400
    .end local v2    # "wmfBbitmap":Landroid/graphics/Bitmap;
    .end local v14    # "actHeight":F
    .end local v15    # "actWidth":F
    .end local v31    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    :catch_0
    move-exception v17

    move-object/from16 v22, v23

    .end local v23    # "is":Ljava/io/InputStream;
    .restart local v22    # "is":Ljava/io/InputStream;
    move-object/from16 v32, v33

    .line 2401
    .end local v33    # "wmfFos":Ljava/io/FileOutputStream;
    .local v17, "e":Ljava/io/FileNotFoundException;
    .restart local v32    # "wmfFos":Ljava/io/FileOutputStream;
    :goto_4
    :try_start_6
    const-string/jumbo v3, "XExcelCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2406
    if-eqz v22, :cond_6

    .line 2408
    :try_start_7
    invoke-virtual/range {v22 .. v22}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 2414
    .end local v17    # "e":Ljava/io/FileNotFoundException;
    :cond_6
    :goto_5
    if-eqz v32, :cond_4

    .line 2416
    :try_start_8
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_3

    .line 2417
    :catch_1
    move-exception v17

    .line 2418
    .local v17, "e":Ljava/io/IOException;
    const-string/jumbo v3, "XExcelCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 2409
    .end local v17    # "e":Ljava/io/IOException;
    .end local v22    # "is":Ljava/io/InputStream;
    .end local v32    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v2    # "wmfBbitmap":Landroid/graphics/Bitmap;
    .restart local v14    # "actHeight":F
    .restart local v15    # "actWidth":F
    .restart local v23    # "is":Ljava/io/InputStream;
    .restart local v31    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    .restart local v33    # "wmfFos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v17

    .line 2410
    .restart local v17    # "e":Ljava/io/IOException;
    const-string/jumbo v3, "XExcelCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2417
    .end local v17    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v17

    .line 2418
    .restart local v17    # "e":Ljava/io/IOException;
    const-string/jumbo v3, "XExcelCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v22, v23

    .end local v23    # "is":Ljava/io/InputStream;
    .restart local v22    # "is":Ljava/io/InputStream;
    move-object/from16 v32, v33

    .line 2419
    .end local v33    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v32    # "wmfFos":Ljava/io/FileOutputStream;
    goto/16 :goto_3

    .line 2409
    .end local v2    # "wmfBbitmap":Landroid/graphics/Bitmap;
    .end local v14    # "actHeight":F
    .end local v15    # "actWidth":F
    .end local v31    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    .local v17, "e":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v17

    .line 2410
    .local v17, "e":Ljava/io/IOException;
    const-string/jumbo v3, "XExcelCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 2402
    .end local v17    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v17

    .line 2403
    .restart local v17    # "e":Ljava/io/IOException;
    :goto_6
    :try_start_9
    const-string/jumbo v3, "XExcelCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 2406
    if-eqz v22, :cond_7

    .line 2408
    :try_start_a
    invoke-virtual/range {v22 .. v22}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 2414
    :cond_7
    :goto_7
    if-eqz v32, :cond_4

    .line 2416
    :try_start_b
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    goto/16 :goto_3

    .line 2417
    :catch_6
    move-exception v17

    .line 2418
    const-string/jumbo v3, "XExcelCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 2409
    :catch_7
    move-exception v17

    .line 2410
    const-string/jumbo v3, "XExcelCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 2406
    .end local v17    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_8
    if-eqz v22, :cond_8

    .line 2408
    :try_start_c
    invoke-virtual/range {v22 .. v22}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    .line 2414
    :cond_8
    :goto_9
    if-eqz v32, :cond_9

    .line 2416
    :try_start_d
    invoke-virtual/range {v32 .. v32}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9

    .line 2419
    :cond_9
    :goto_a
    throw v3

    .line 2409
    :catch_8
    move-exception v17

    .line 2410
    .restart local v17    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "XExcelCustomViewConsumer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 2417
    .end local v17    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v17

    .line 2418
    .restart local v17    # "e":Ljava/io/IOException;
    const-string/jumbo v4, "XExcelCustomViewConsumer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 2422
    .end local v17    # "e":Ljava/io/IOException;
    .end local v22    # "is":Ljava/io/InputStream;
    .end local v32    # "wmfFos":Ljava/io/FileOutputStream;
    :cond_a
    const-string/jumbo v3, ".emf"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 2423
    move-object/from16 v20, v27

    .line 2424
    .local v20, "htmlImgFileName":Ljava/lang/String;
    const/16 v24, 0x0

    .line 2425
    .local v24, "out":Ljava/io/FileOutputStream;
    const/16 v16, 0x0

    .line 2428
    .local v16, "data":[B
    :try_start_e
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v22

    .line 2429
    .restart local v22    # "is":Ljava/io/InputStream;
    if-nez v22, :cond_c

    .line 2430
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFPictureData;->getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getEntryName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getActualFile()Ljava/io/File;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getInputStream(Ljava/lang/String;Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v22

    .line 2433
    invoke-static/range {v22 .. v22}, Lorg/apache/poi/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_a

    move-result-object v16

    .line 2441
    .end local v22    # "is":Ljava/io/InputStream;
    :goto_b
    if-eqz v16, :cond_4

    .line 2443
    :try_start_f
    new-instance v25, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v0, v20

    invoke-direct {v3, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_b

    .line 2445
    .end local v24    # "out":Ljava/io/FileOutputStream;
    .local v25, "out":Ljava/io/FileOutputStream;
    :try_start_10
    new-instance v30, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;

    sub-int v3, p4, p3

    int-to-float v3, v3

    sub-int v4, p2, p1

    int-to-float v4, v4

    move-object/from16 v0, v30

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v3, v4}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;-><init>([BFF)V

    .line 2447
    .local v30, "readEMFFile":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;->drawImage()Landroid/graphics/Bitmap;

    move-result-object v9

    .line 2448
    if-eqz v9, :cond_b

    .line 2449
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x19

    move-object/from16 v0, v25

    invoke-virtual {v9, v3, v4, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 2450
    :cond_b
    invoke-virtual/range {v25 .. v25}, Ljava/io/FileOutputStream;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_c

    move-object/from16 v24, v25

    .line 2453
    .end local v25    # "out":Ljava/io/FileOutputStream;
    .restart local v24    # "out":Ljava/io/FileOutputStream;
    goto/16 :goto_3

    .line 2435
    .end local v30    # "readEMFFile":Lcom/samsung/thumbnail/office/emf/EmfDrawingRecords/ReadRawFiles;
    .restart local v22    # "is":Ljava/io/InputStream;
    :cond_c
    :try_start_11
    invoke-virtual/range {v22 .. v22}, Ljava/io/InputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a

    goto :goto_b

    .line 2437
    .end local v22    # "is":Ljava/io/InputStream;
    :catch_a
    move-exception v18

    .line 2438
    .local v18, "e1":Ljava/io/IOException;
    const-string/jumbo v3, "XExcelCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 2451
    .end local v18    # "e1":Ljava/io/IOException;
    :catch_b
    move-exception v17

    .line 2452
    .local v17, "e":Ljava/lang/Exception;
    :goto_c
    const-string/jumbo v3, "XExcelCustomViewConsumer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 2457
    .end local v16    # "data":[B
    .end local v17    # "e":Ljava/lang/Exception;
    .end local v20    # "htmlImgFileName":Ljava/lang/String;
    .end local v24    # "out":Ljava/io/FileOutputStream;
    :cond_d
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sub-int v4, p4, p3

    sub-int v5, p2, p1

    invoke-static {v3, v4, v5}, Lcom/samsung/thumbnail/util/Utils;->decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v9

    goto/16 :goto_3

    .line 2451
    .restart local v16    # "data":[B
    .restart local v20    # "htmlImgFileName":Ljava/lang/String;
    .restart local v25    # "out":Ljava/io/FileOutputStream;
    :catch_c
    move-exception v17

    move-object/from16 v24, v25

    .end local v25    # "out":Ljava/io/FileOutputStream;
    .restart local v24    # "out":Ljava/io/FileOutputStream;
    goto :goto_c

    .line 2406
    .end local v16    # "data":[B
    .end local v20    # "htmlImgFileName":Ljava/lang/String;
    .end local v24    # "out":Ljava/io/FileOutputStream;
    .restart local v22    # "is":Ljava/io/InputStream;
    .restart local v33    # "wmfFos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v3

    move-object/from16 v32, v33

    .end local v33    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v32    # "wmfFos":Ljava/io/FileOutputStream;
    goto/16 :goto_8

    .end local v22    # "is":Ljava/io/InputStream;
    .end local v32    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v23    # "is":Ljava/io/InputStream;
    .restart local v33    # "wmfFos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v3

    move-object/from16 v22, v23

    .end local v23    # "is":Ljava/io/InputStream;
    .restart local v22    # "is":Ljava/io/InputStream;
    move-object/from16 v32, v33

    .end local v33    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v32    # "wmfFos":Ljava/io/FileOutputStream;
    goto/16 :goto_8

    .line 2402
    .end local v32    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v33    # "wmfFos":Ljava/io/FileOutputStream;
    :catch_d
    move-exception v17

    move-object/from16 v32, v33

    .end local v33    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v32    # "wmfFos":Ljava/io/FileOutputStream;
    goto/16 :goto_6

    .end local v22    # "is":Ljava/io/InputStream;
    .end local v32    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v23    # "is":Ljava/io/InputStream;
    .restart local v33    # "wmfFos":Ljava/io/FileOutputStream;
    :catch_e
    move-exception v17

    move-object/from16 v22, v23

    .end local v23    # "is":Ljava/io/InputStream;
    .restart local v22    # "is":Ljava/io/InputStream;
    move-object/from16 v32, v33

    .end local v33    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v32    # "wmfFos":Ljava/io/FileOutputStream;
    goto/16 :goto_6

    .line 2400
    :catch_f
    move-exception v17

    goto/16 :goto_4

    .end local v32    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v33    # "wmfFos":Ljava/io/FileOutputStream;
    :catch_10
    move-exception v17

    move-object/from16 v32, v33

    .end local v33    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v32    # "wmfFos":Ljava/io/FileOutputStream;
    goto/16 :goto_4

    .end local v22    # "is":Ljava/io/InputStream;
    .end local v32    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v2    # "wmfBbitmap":Landroid/graphics/Bitmap;
    .restart local v14    # "actHeight":F
    .restart local v15    # "actWidth":F
    .restart local v23    # "is":Ljava/io/InputStream;
    .restart local v31    # "readWMFFile":Lcom/samsung/thumbnail/office/wmf/WmfControlRecords/ReadWmfRawFile;
    .restart local v33    # "wmfFos":Ljava/io/FileOutputStream;
    :cond_e
    move-object/from16 v22, v23

    .end local v23    # "is":Ljava/io/InputStream;
    .restart local v22    # "is":Ljava/io/InputStream;
    move-object/from16 v32, v33

    .end local v33    # "wmfFos":Ljava/io/FileOutputStream;
    .restart local v32    # "wmfFos":Ljava/io/FileOutputStream;
    goto/16 :goto_3
.end method

.method public drawShapes(IIIILcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;)V
    .locals 28
    .param p1, "top"    # I
    .param p2, "bottom"    # I
    .param p3, "left"    # I
    .param p4, "right"    # I
    .param p5, "cellAnchor"    # Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;

    .prologue
    .line 2479
    const/4 v6, 0x0

    .line 2482
    .local v6, "i":I
    const/4 v11, 0x0

    .line 2484
    .local v11, "connectorShapesIndex":I
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getShapeProperty()Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;

    move-result-object v14

    .line 2485
    .local v14, "shapeProp":Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;
    if-nez v14, :cond_1

    .line 2778
    :cond_0
    :goto_0
    return-void

    .line 2489
    :cond_1
    const-string/jumbo v27, ""

    .line 2491
    .local v27, "shapeName":Ljava/lang/String;
    invoke-virtual {v14}, Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;->getPrstShapeName()Ljava/lang/String;

    move-result-object v27

    .line 2493
    if-eqz v27, :cond_27

    .line 2494
    const/4 v2, 0x0

    .line 2497
    .local v2, "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    const-string/jumbo v3, "StraightConnector1"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "Line"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "BentConnector3"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "BentConnector2"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "CurvedConnector3"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2502
    :cond_2
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v4, "BentConnector"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move/from16 v7, p1

    move/from16 v8, p3

    move/from16 v9, p2

    move/from16 v10, p4

    invoke-direct/range {v2 .. v14}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2506
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 2761
    :cond_3
    :goto_1
    if-eqz v2, :cond_0

    .line 2762
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->addtextonshape(Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;Lcom/samsung/thumbnail/customview/word/ShapesController;)V

    goto :goto_0

    .line 2511
    :cond_4
    const-string/jumbo v3, "ActionButtonHome"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2516
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "ActionButtonShapes"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2520
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto :goto_1

    .line 2526
    :cond_5
    const-string/jumbo v3, "rect"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "Rectangle"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "RoundRectangle"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "Diamond"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "Ellipse"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "IsocelesTriangle"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "RightTriangle"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "Parallelogram"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "Pentagon"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "Hexagon"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "Octagon"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "Plus"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "mathPlus"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "Can"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "roundRect"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "Triangle"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "rtTriangle"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2547
    :cond_6
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "BasicShapes"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2551
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2553
    :cond_7
    const-string/jumbo v3, "HomePlate"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string/jumbo v3, "Chevron"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string/jumbo v3, "Bevel"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string/jumbo v3, "Plaque"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string/jumbo v3, "Cube"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string/jumbo v3, "Moon"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string/jumbo v3, "FoldedCorner"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string/jumbo v3, "LightningBolt"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string/jumbo v3, "Sun"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string/jumbo v3, "SmileyFace"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2564
    :cond_8
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "BlockShapes"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2568
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2570
    :cond_9
    const-string/jumbo v3, "DownArrow"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string/jumbo v3, "UpArrow"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string/jumbo v3, "Arrow"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string/jumbo v3, "Arrow"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string/jumbo v3, "LeftArrow"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string/jumbo v3, "LeftRightArrow"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string/jumbo v3, "UpDownArrow"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string/jumbo v3, "NotchedRightArrow"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string/jumbo v3, "RightArrow"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 2579
    :cond_a
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "BlockArrow"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2583
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2585
    :cond_b
    const-string/jumbo v3, "LeftArrowCallout"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "DownArrowCallout"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "RightArrowCallout"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "UpArrowCallout"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string/jumbo v3, "LeftRightArrowCallout"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 2590
    :cond_c
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "ArrowCallout"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2594
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2595
    :cond_d
    const-string/jumbo v3, "BracketPair"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "LeftBracket"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string/jumbo v3, "RightBracket"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 2598
    :cond_e
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "BracketShapes"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2602
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2604
    :cond_f
    const-string/jumbo v3, "BracePair"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10

    const-string/jumbo v3, "LeftBrace"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10

    const-string/jumbo v3, "RightBrace"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 2607
    :cond_10
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "BraceShapes"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2611
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2615
    :cond_11
    const-string/jumbo v3, "CurvedRightArrow"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string/jumbo v3, "CurvedLeftArrow"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string/jumbo v3, "CurvedUpArrow"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string/jumbo v3, "CurvedDownArrow"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 2619
    :cond_12
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "CurvedBlockArrow"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2623
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2627
    :cond_13
    const-string/jumbo v3, "FlowChartProcess"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartAlternateProcess"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartInputOutput"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartDecision"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartPredefinedProcess"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartInternalStorage"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartTerminator"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartPreparation"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartManualInput"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartManualOperation"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartConnector"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartOffpageConnector"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartPunchedCard"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartSummingJunction"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartOr"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartCollate"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartSort"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartExtract"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartMerge"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartOnlineStorage"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartDelay"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartMagneticTape"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartMagneticDisk"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartMagneticDrum"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartDisplay"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartPunchedTape"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartDocument"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string/jumbo v3, "FlowChartMultidocument"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 2656
    :cond_14
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "FlowChartProcess"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2660
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2661
    :cond_15
    const-string/jumbo v3, "IrregularSeal1"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_16

    const-string/jumbo v3, "IrregularSeal2"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 2664
    :cond_16
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "IrregularSeal"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2668
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2670
    :cond_17
    const-string/jumbo v3, "Star4"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_18

    const-string/jumbo v3, "Star8"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_18

    const-string/jumbo v3, "Star16"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_18

    const-string/jumbo v3, "Star24"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_18

    const-string/jumbo v3, "Star32"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_18

    const-string/jumbo v3, "Star"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_18

    const-string/jumbo v3, "StarShapes"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 2678
    :cond_18
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "StarShapes"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2682
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2685
    :cond_19
    const-string/jumbo v3, "Ribbon2"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1a

    const-string/jumbo v3, "Ribbon"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1a

    const-string/jumbo v3, "EllipseRibbon"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1a

    const-string/jumbo v3, "EllipseRibbon2"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 2693
    :cond_1a
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "Ribbbonhapes"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2697
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2698
    :cond_1b
    const-string/jumbo v3, "VerticalScroll"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1c

    const-string/jumbo v3, "HorizontalScroll"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 2701
    :cond_1c
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "ScrollShapes"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2705
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2707
    :cond_1d
    const-string/jumbo v3, "Wave"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1e

    const-string/jumbo v3, "DoubleWave"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 2710
    :cond_1e
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "WaveShapes"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2714
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2716
    :cond_1f
    const-string/jumbo v3, "WedgeRectCallout"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_20

    const-string/jumbo v3, "WedgeRRectCallout"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_20

    const-string/jumbo v3, "wedgeRoundRectCallout"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 2720
    :cond_20
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "WedgeCallouts"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2724
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2726
    :cond_21
    const-string/jumbo v3, "BorderCallout1"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "Callout1"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "AccentCallout1"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_22

    const-string/jumbo v3, "AccentBorderCallout1"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_23

    .line 2731
    :cond_22
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "Callout1"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2735
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2737
    :cond_23
    const-string/jumbo v3, "BorderCallout2"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_24

    const-string/jumbo v3, "Callout2"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_24

    const-string/jumbo v3, "AccentCallout2"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_24

    const-string/jumbo v3, "AccentBorderCallout2"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_25

    .line 2742
    :cond_24
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "Callout2"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2746
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2748
    :cond_25
    const-string/jumbo v3, "BorderCallout3"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_26

    const-string/jumbo v3, "Callout3"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_26

    const-string/jumbo v3, "AccentCallout3"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_26

    const-string/jumbo v3, "AccentBorderCallout3"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2753
    :cond_26
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "Callout3"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2757
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_1

    .line 2769
    .end local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    :cond_27
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/thumbnail/office/ooxml/excel/CellAnchor;->getName()Ljava/lang/String;

    move-result-object v27

    .line 2770
    if-eqz v27, :cond_0

    const-string/jumbo v3, "Freeform"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2771
    new-instance v2, Lcom/samsung/thumbnail/customview/word/ShapesController;

    sget-object v16, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v17, "Freeform"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v25, v0

    move-object v15, v2

    move/from16 v19, v6

    move/from16 v20, p1

    move/from16 v21, p3

    move/from16 v22, p2

    move/from16 v23, p4

    move-object/from16 v26, v14

    invoke-direct/range {v15 .. v26}, Lcom/samsung/thumbnail/customview/word/ShapesController;-><init>(Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;IIIIILjava/io/File;Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;Lcom/samsung/thumbnail/office/ooxml/pptx/shapes/XSLFShapeInfo;)V

    .line 2775
    .restart local v2    # "shapesController":Lcom/samsung/thumbnail/customview/word/ShapesController;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3, v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    goto/16 :goto_0
.end method

.method public drawSheet()V
    .locals 2

    .prologue
    .line 1756
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawTable()V

    .line 1759
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawBlankTable()V

    .line 1762
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawMergedTable()V

    .line 1764
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->setHeightToRows()V

    .line 1767
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->AddElement(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V

    .line 1769
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->calculateHeight()F

    .line 1773
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    if-eqz v0, :cond_0

    .line 1774
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->prepareDrawings()V

    .line 1776
    :cond_0
    return-void
.end method

.method public drawTable()V
    .locals 26

    .prologue
    .line 1862
    const/16 v19, 0x1

    .line 1863
    .local v19, "previouscolum":I
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    .line 1864
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rowid1:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rowid2:I

    .line 1865
    const/4 v6, 0x0

    .line 1866
    .local v6, "i":I
    const/16 v23, 0x0

    .line 1867
    .local v23, "storecurrentrow":I
    const/4 v15, 0x0

    .line 1869
    .local v15, "colnum":I
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numericcolumcount:I

    .line 1871
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->excelVariable:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    iget v0, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->countc:I

    move/from16 v20, v0

    .line 1874
    .local v20, "rowlength":I
    const/16 v3, 0x14

    move/from16 v0, v20

    if-le v0, v3, :cond_0

    .line 1875
    add-int/lit8 v20, v20, -0x1

    .line 1881
    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    .line 1884
    new-instance v3, Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    const/4 v9, 0x0

    invoke-direct {v3, v9}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    .line 1885
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowGridlines()Z

    move-result v9

    invoke-virtual {v3, v9}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setGridDisplay(Z)V

    .line 1886
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowHeaders()Z

    move-result v9

    invoke-virtual {v3, v9}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setHeadersDisplay(Z)V

    .line 1912
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->isThumbnailPreview:Z

    if-eqz v3, :cond_1

    .line 1915
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getMAXRowColumnMergedForThumbnail()V

    .line 1945
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->isThumbnailPreview:Z

    if-eqz v3, :cond_5

    .line 1946
    const/16 v3, 0x10

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    .line 1959
    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowHeaders()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1960
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->alphaRow()V

    .line 1963
    :cond_3
    const/4 v8, 0x0

    .local v8, "fillid":I
    const/16 v24, 0x0

    .line 1964
    .local v24, "styleindex":I
    :goto_1
    move/from16 v0, v20

    if-ge v6, v0, :cond_4

    .line 1965
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetRowid()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gt v3, v6, :cond_7

    .line 2266
    :cond_4
    :goto_2
    return-void

    .line 1948
    .end local v8    # "fillid":I
    .end local v24    # "styleindex":I
    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->excelVariable:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    iget v9, v9, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->columcount:I

    if-ge v3, v9, :cond_6

    .line 1949
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->excelVariable:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    iget v3, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->columcount:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    .line 1952
    :cond_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    const/16 v9, 0xf

    if-gt v3, v9, :cond_2

    .line 1953
    const/16 v3, 0xf

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    goto :goto_0

    .line 1968
    .restart local v8    # "fillid":I
    .restart local v24    # "styleindex":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetRowid()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 1970
    .local v16, "currentrow":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->excelVariable:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    iget v3, v3, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->columcount:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->colcount:I

    .line 1972
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    .line 1974
    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    move/from16 v0, v23

    if-ge v3, v0, :cond_b

    .line 1979
    const/4 v15, 0x0

    .line 1980
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawTableRow(I)V

    .line 1983
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowHeaders()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1984
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numericCol()V

    .line 1985
    add-int/lit8 v15, v15, 0x1

    .line 1989
    :cond_9
    :goto_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    if-ge v15, v3, :cond_a

    .line 1991
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v15}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawColumn(II)V

    .line 1992
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 1996
    :cond_a
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    .line 1997
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->isThumbnailPreview:Z

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    const/16 v9, 0x14

    if-le v3, v9, :cond_8

    goto/16 :goto_2

    .line 2006
    :cond_b
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawTableRow(I)V

    .line 2008
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowHeaders()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 2009
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numericCol()V

    .line 2012
    :cond_c
    const/16 v25, 0x0

    .line 2013
    .local v25, "textstyleid":I
    move/from16 v17, v6

    .line 2014
    .local v17, "j":I
    const/4 v7, 0x0

    .line 2015
    .local v7, "currentcolum":I
    const/4 v15, 0x0

    .line 2016
    const-string/jumbo v5, ""

    .line 2019
    .local v5, "text":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetRowid()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v9, v17, 0x1

    if-le v3, v9, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetRowid()Ljava/util/ArrayList;

    move-result-object v3

    add-int/lit8 v9, v17, 0x1

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetRowid()Ljava/util/ArrayList;

    move-result-object v9

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    .line 2024
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetColumnid()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_d

    .line 2025
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetColumnid()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numColumn(Ljava/lang/String;)I

    move-result v7

    .line 2032
    :goto_4
    move/from16 v0, v19

    if-ge v0, v7, :cond_e

    .line 2035
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawColumn(II)V

    .line 2036
    add-int/lit8 v19, v19, 0x1

    goto :goto_4

    .line 2029
    :cond_d
    const/4 v7, 0x0

    goto :goto_4

    .line 2041
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getTextFromSheettextid(I)Ljava/lang/String;

    move-result-object v5

    .line 2043
    new-instance v4, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct {v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    .line 2045
    .local v4, "newCanvasCellTemp":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    const/4 v8, 0x0

    .line 2046
    const/16 v24, 0x0

    .line 2048
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_f

    .line 2049
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v24

    .line 2053
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFFillid()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    move/from16 v0, v24

    if-le v3, v0, :cond_10

    .line 2055
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFFillid()Ljava/util/ArrayList;

    move-result-object v3

    move/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_10

    .line 2057
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFFillid()Ljava/util/ArrayList;

    move-result-object v3

    move/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 2063
    :cond_10
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v8}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->cellAttributesProperties(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V

    move-object/from16 v3, p0

    .line 2065
    invoke-virtual/range {v3 .. v8}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->tableBorder(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;III)V

    .line 2067
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    const/4 v9, 0x1

    if-ne v3, v9, :cond_11

    .line 2070
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->apply_firstrowtopborderstyle(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 2073
    :cond_11
    add-int/lit8 v19, v19, 0x1

    .line 2075
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->setHyperlink(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V

    .line 2078
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getTextStyleid(I)I

    move-result v25

    .line 2080
    const/16 v22, -0x1

    .line 2081
    .local v22, "siID":I
    if-eqz v5, :cond_12

    const-string/jumbo v3, "~"

    invoke-virtual {v5, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 2084
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v3, v6}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getSIID(I)I

    move-result v22

    .line 2087
    :cond_12
    const/4 v3, -0x1

    move/from16 v0, v22

    if-ne v0, v3, :cond_16

    .line 2088
    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v4, v5, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->setTextSyle(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;I)V

    .line 2096
    :goto_5
    add-int/lit8 v3, v19, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->applyProperWidth(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V

    .line 2098
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowGridlines()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v3}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Z)V

    .line 2216
    .end local v4    # "newCanvasCellTemp":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v22    # "siID":I
    :cond_13
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->checkPreviousColumnwithTotalColum(I)I

    move-result v19

    .line 2218
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rowattr:Z

    const/4 v9, 0x1

    if-ne v3, v9, :cond_24

    .line 2220
    :cond_14
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rowid1:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rowid2:I

    if-ge v3, v9, :cond_22

    .line 2221
    const/16 v18, 0x1

    .line 2224
    .local v18, "k":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->createNewRow()V

    .line 2227
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowHeaders()Z

    move-result v3

    if-eqz v3, :cond_15

    .line 2228
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numericCol()V

    .line 2229
    add-int/lit8 v18, v18, 0x1

    .line 2232
    :cond_15
    :goto_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    move/from16 v0, v18

    if-ge v0, v3, :cond_21

    .line 2234
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawColumn(II)V

    .line 2235
    add-int/lit8 v18, v18, 0x1

    goto :goto_6

    .line 2092
    .end local v18    # "k":I
    .restart local v4    # "newCanvasCellTemp":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .restart local v22    # "siID":I
    :cond_16
    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v22

    invoke-virtual {v0, v4, v5, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->setTextSyle(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;II)V

    goto :goto_5

    .line 2104
    .end local v4    # "newCanvasCellTemp":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v22    # "siID":I
    :cond_17
    :goto_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetRowid()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v9, v17, 0x1

    if-le v3, v9, :cond_18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetRowid()Ljava/util/ArrayList;

    move-result-object v3

    add-int/lit8 v9, v17, 0x1

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetRowid()Ljava/util/ArrayList;

    move-result-object v9

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 2107
    add-int/lit8 v17, v17, 0x1

    goto :goto_7

    .line 2110
    :cond_18
    move v12, v6

    .local v12, "z":I
    :goto_8
    move/from16 v0, v17

    if-gt v12, v0, :cond_1e

    .line 2113
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetColumnid()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numColumn(Ljava/lang/String;)I

    move-result v7

    .line 2116
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getTextFromSheettextid(I)Ljava/lang/String;

    move-result-object v5

    .line 2122
    new-instance v10, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct {v10}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    .line 2125
    .local v10, "newCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v7, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->checkCurrentcolumWithPreviousColumn(II)I

    move-result v19

    .line 2128
    const/16 v24, 0x0

    .line 2129
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_19

    .line 2130
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v24

    .line 2133
    :cond_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFFillid()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    move/from16 v0, v24

    if-le v3, v0, :cond_1a

    .line 2135
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFFillid()Ljava/util/ArrayList;

    move-result-object v3

    move/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1a

    .line 2138
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFFillid()Ljava/util/ArrayList;

    move-result-object v3

    move/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 2145
    :cond_1a
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v8}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->cellAttributesProperties(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V

    move-object/from16 v9, p0

    move-object v11, v5

    move v13, v7

    move v14, v8

    .line 2146
    invoke-virtual/range {v9 .. v14}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->tableBorder(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;III)V

    .line 2147
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    const/4 v9, 0x1

    if-ne v3, v9, :cond_1b

    .line 2148
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->apply_firstrowtopborderstyle(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 2151
    :cond_1b
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v10, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->setHyperlink(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V

    .line 2152
    add-int/lit8 v19, v19, 0x1

    .line 2156
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getTextStyleid(I)I

    move-result v25

    .line 2158
    const/16 v22, -0x1

    .line 2159
    .restart local v22    # "siID":I
    if-eqz v5, :cond_1c

    const-string/jumbo v3, "~"

    invoke-virtual {v5, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 2163
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v3, v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getSIID(I)I

    move-result v22

    .line 2166
    :cond_1c
    const/4 v3, -0x1

    move/from16 v0, v22

    if-ne v0, v3, :cond_1d

    .line 2167
    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v10, v5, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->setTextSyle(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;I)V

    .line 2179
    :goto_9
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v10, v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellHeight(F)V

    .line 2183
    add-int/lit8 v3, v19, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v3}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->applyProperWidth(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V

    .line 2185
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowGridlines()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v3}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Z)V

    .line 2189
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v7, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->checkCurrentcolumWithPreviousColumn(II)I

    move-result v19

    .line 2110
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_8

    .line 2171
    :cond_1d
    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v22

    invoke-virtual {v0, v10, v5, v1, v2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->setTextSyle(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;II)V

    goto :goto_9

    .line 2195
    .end local v10    # "newCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v22    # "siID":I
    :cond_1e
    const/16 v21, 0x0

    .line 2196
    .local v21, "rowspan":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetRowspan()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1f

    .line 2197
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetRowspan()Ljava/util/ArrayList;

    move-result-object v3

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    .line 2201
    :cond_1f
    :goto_a
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->colcount:I

    move/from16 v0, v21

    if-ge v0, v3, :cond_20

    .line 2202
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawColumn(II)V

    .line 2203
    add-int/lit8 v21, v21, 0x1

    .line 2204
    add-int/lit8 v19, v19, 0x1

    goto :goto_a

    .line 2208
    :cond_20
    :goto_b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->colcount:I

    move/from16 v0, v19

    if-ge v0, v3, :cond_13

    .line 2211
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawColumn(II)V

    .line 2212
    add-int/lit8 v19, v19, 0x1

    goto :goto_b

    .line 2239
    .end local v12    # "z":I
    .end local v21    # "rowspan":I
    .restart local v18    # "k":I
    :cond_21
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rowid2:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rowid1:I

    sub-int/2addr v3, v9

    add-int v6, v17, v3

    .line 2240
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rowid1:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rowid1:I

    .line 2241
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    .line 2242
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->isThumbnailPreview:Z

    if-eqz v3, :cond_14

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    const/16 v9, 0x14

    if-le v3, v9, :cond_14

    goto/16 :goto_2

    .line 2248
    .end local v18    # "k":I
    :cond_22
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rowattr:Z

    .line 2256
    :goto_c
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    .line 2257
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->isThumbnailPreview:Z

    if-eqz v3, :cond_23

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->previousrow:I

    const/16 v9, 0x14

    if-gt v3, v9, :cond_4

    .line 2262
    :cond_23
    const/16 v19, 0x1

    .line 2263
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rowid2:I

    .line 2264
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rowid1:I

    goto/16 :goto_1

    .line 2250
    :cond_24
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->checkPreviousColumnwithTotalColum(I)I

    move-result v19

    .line 2253
    add-int/lit8 v6, v17, 0x1

    goto :goto_c
.end method

.method public drawTableRow(I)V
    .locals 1
    .param p1, "rowNumber"    # I

    .prologue
    .line 3223
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->createNewRow()V

    .line 3224
    return-void
.end method

.method public drawlastblankrowcolumn(II)I
    .locals 2
    .param p1, "previousrow"    # I
    .param p2, "row"    # I

    .prologue
    .line 3115
    :goto_0
    if-gt p1, p2, :cond_2

    .line 3117
    const/4 v0, 0x0

    .line 3120
    .local v0, "colid":I
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->createNewRow()V

    .line 3121
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->canShowHeaders()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3122
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numericCol()V

    .line 3123
    add-int/lit8 v0, v0, 0x1

    .line 3126
    :cond_0
    :goto_1
    iget v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    if-ge v0, v1, :cond_1

    .line 3127
    invoke-virtual {p0, p1, v0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawColumn(II)V

    .line 3128
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3132
    :cond_1
    add-int/lit8 p1, p1, 0x1

    .line 3134
    goto :goto_0

    .line 3135
    .end local v0    # "colid":I
    :cond_2
    return p1
.end method

.method public getBorderColor(ILjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "cellborderid"    # I
    .param p2, "border"    # Ljava/lang/String;

    .prologue
    .line 697
    const/4 v0, 0x0

    .line 698
    .local v0, "bordercolor":Ljava/lang/String;
    const-string/jumbo v2, "left"

    invoke-virtual {p2, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    .line 699
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellLeftBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 701
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellLeftBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 716
    .restart local v0    # "bordercolor":Ljava/lang/String;
    :cond_0
    :goto_0
    const-string/jumbo v2, "rgt"

    invoke-virtual {p2, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 718
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellRightBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 720
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellRightBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 737
    .restart local v0    # "bordercolor":Ljava/lang/String;
    :cond_1
    :goto_1
    const-string/jumbo v2, "top"

    invoke-virtual {p2, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    .line 738
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellTopBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 740
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellTopBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 755
    .restart local v0    # "bordercolor":Ljava/lang/String;
    :cond_2
    :goto_2
    const-string/jumbo v2, "bottom"

    invoke-virtual {p2, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    .line 756
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellBottomBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 758
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellBottomBorderRGBColor()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 781
    .restart local v0    # "bordercolor":Ljava/lang/String;
    :cond_3
    :goto_3
    return-object v0

    .line 704
    :cond_4
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellLeftBorderThemeColor()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 706
    .restart local v0    # "bordercolor":Ljava/lang/String;
    if-eqz v0, :cond_5

    .line 707
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 708
    .local v1, "theme":I
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCorrectThemeClr(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getColorSchemRGBval(I)Ljava/lang/String;

    move-result-object v0

    .line 709
    goto/16 :goto_0

    .line 711
    .end local v1    # "theme":I
    :cond_5
    const-string/jumbo v0, "8b8989"

    goto/16 :goto_0

    .line 724
    :cond_6
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellRightBorderThemeColor()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 727
    .restart local v0    # "bordercolor":Ljava/lang/String;
    if-eqz v0, :cond_7

    .line 728
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 729
    .restart local v1    # "theme":I
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCorrectThemeClr(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getColorSchemRGBval(I)Ljava/lang/String;

    move-result-object v0

    .line 730
    goto/16 :goto_1

    .line 732
    .end local v1    # "theme":I
    :cond_7
    const-string/jumbo v0, "8b8989"

    goto/16 :goto_1

    .line 743
    :cond_8
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellTopBorderThemeColor()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 745
    .restart local v0    # "bordercolor":Ljava/lang/String;
    if-eqz v0, :cond_9

    .line 746
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 747
    .restart local v1    # "theme":I
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCorrectThemeClr(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getColorSchemRGBval(I)Ljava/lang/String;

    move-result-object v0

    .line 748
    goto/16 :goto_2

    .line 750
    .end local v1    # "theme":I
    :cond_9
    const-string/jumbo v0, "8b8989"

    goto/16 :goto_2

    .line 761
    :cond_a
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellBottomBorderThemeColor()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bordercolor":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 764
    .restart local v0    # "bordercolor":Ljava/lang/String;
    if-eqz v0, :cond_b

    .line 765
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 766
    .restart local v1    # "theme":I
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-direct {p0, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCorrectThemeClr(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getColorSchemRGBval(I)Ljava/lang/String;

    move-result-object v0

    .line 767
    goto/16 :goto_3

    .line 769
    .end local v1    # "theme":I
    :cond_b
    const-string/jumbo v0, "8b8989"

    goto/16 :goto_3
.end method

.method public getCanvas()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    return-object v0
.end method

.method public getCellborderid(I)I
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 3173
    const/4 v0, 0x0

    .line 3174
    .local v0, "borderid":I
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, p1, :cond_2

    .line 3176
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3177
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 3179
    :cond_0
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFBorderId()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3181
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFBorderId()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 3189
    :goto_0
    return v0

    .line 3184
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 3187
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getColorSchemakeyval(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 3063
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    if-eqz v0, :cond_0

    .line 3064
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->themeobj:Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/dml/theme/Theme;->getColorSchema()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3066
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCustomView()Lcom/samsung/thumbnail/customview/word/CustomView;
    .locals 1

    .prologue
    .line 3227
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCustomView:Lcom/samsung/thumbnail/customview/word/CustomView;

    return-object v0
.end method

.method public getFolderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderName:Ljava/lang/String;

    return-object v0
.end method

.method public getFolderPath()Ljava/io/File;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->folderPath:Ljava/io/File;

    return-object v0
.end method

.method public getMAXRowColumnMergedForThumbnail()V
    .locals 11

    .prologue
    .line 1103
    const-string/jumbo v4, ":"

    .line 1105
    .local v4, "delims":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->excelVariable:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    iget v9, v9, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->mergecount:I

    if-lez v9, :cond_2

    .line 1106
    const/4 v6, 0x0

    .local v6, "i":I
    const/4 v2, 0x0

    .local v2, "chkrow1":I
    const/4 v0, 0x0

    .local v0, "chkcol1":I
    const/4 v3, 0x0

    .local v3, "chkrow2":I
    const/4 v1, 0x0

    .line 1108
    .local v1, "chkcol2":I
    :goto_0
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->excelVariable:Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;

    iget v9, v9, Lcom/samsung/thumbnail/office/ooxml/excel/display/ExcelVariables;->mergecount:I

    if-ge v6, v9, :cond_2

    .line 1109
    const/4 v5, 0x0

    .line 1110
    .local v5, "firststr":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1111
    .local v7, "secondstr":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetMergecell()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-le v9, v6, :cond_1

    .line 1112
    new-instance v8, Ljava/util/StringTokenizer;

    iget-object v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetMergecell()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-direct {v8, v9, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114
    .local v8, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    .line 1115
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    .line 1117
    if-eqz v5, :cond_1

    if-eqz v7, :cond_1

    .line 1118
    const-string/jumbo v9, "[^\\d]"

    const-string/jumbo v10, ""

    invoke-virtual {v5, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1121
    const-string/jumbo v9, "[^\\d]"

    const-string/jumbo v10, ""

    invoke-virtual {v7, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1125
    invoke-virtual {p0, v5}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numColumn(Ljava/lang/String;)I

    move-result v0

    .line 1126
    invoke-virtual {p0, v7}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numColumn(Ljava/lang/String;)I

    move-result v1

    .line 1128
    const/16 v9, 0x14

    if-ge v2, v9, :cond_1

    const/16 v9, 0x10

    if-ge v0, v9, :cond_1

    .line 1130
    iget v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxRow:I

    if-le v3, v9, :cond_0

    .line 1131
    iput v3, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxRow:I

    .line 1133
    :cond_0
    iget v9, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    if-le v1, v9, :cond_1

    .line 1134
    iput v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mMaxColumn:I

    .line 1139
    .end local v8    # "st":Ljava/util/StringTokenizer;
    :cond_1
    add-int/lit8 v6, v6, 0x1

    .line 1140
    goto :goto_0

    .line 1142
    .end local v0    # "chkcol1":I
    .end local v1    # "chkcol2":I
    .end local v2    # "chkrow1":I
    .end local v3    # "chkrow2":I
    .end local v5    # "firststr":Ljava/lang/String;
    .end local v6    # "i":I
    .end local v7    # "secondstr":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public getRowPosition(ILjava/util/ArrayList;)F
    .locals 4
    .param p1, "rowNumber"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;",
            ">;)F"
        }
    .end annotation

    .prologue
    .line 2316
    .local p2, "mRowList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;>;"
    const/4 v2, 0x0

    .line 2317
    .local v2, "totalHeight":F
    const/4 v0, 0x0

    .line 2319
    .local v0, "rowCounter":I
    :goto_0
    if-gt v0, p1, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 2320
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getRowHeight()F

    move-result v1

    .line 2321
    .local v1, "rowHeight":F
    add-float/2addr v2, v1

    .line 2322
    add-int/lit8 v0, v0, 0x1

    .line 2323
    goto :goto_0

    .line 2325
    .end local v1    # "rowHeight":F
    :cond_0
    if-ge v0, p1, :cond_1

    .line 2326
    :goto_1
    if-gt v0, p1, :cond_1

    .line 2327
    const/high16 v3, 0x41f00000    # 30.0f

    add-float/2addr v2, v3

    .line 2328
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2332
    :cond_1
    return v2
.end method

.method public getTextStyleid(I)I
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 3196
    const/4 v0, 0x0

    .line 3197
    .local v0, "textstyleid":I
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, p1, :cond_2

    .line 3198
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3199
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 3203
    :cond_0
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFFontid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3205
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFFontid()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 3213
    :goto_0
    return v0

    .line 3208
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 3211
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public numColumn(Ljava/lang/String;)I
    .locals 5
    .param p1, "column"    # Ljava/lang/String;

    .prologue
    const/16 v0, 0x1a

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 593
    if-eqz p1, :cond_34

    .line 594
    const-string/jumbo v2, "[^\\[A-Z]"

    const-string/jumbo v3, ""

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 599
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v1, :cond_1

    .line 656
    :cond_0
    :goto_0
    return v0

    .line 602
    :cond_1
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x41

    if-eq v2, v3, :cond_2

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x61

    if-ne v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 603
    goto :goto_0

    .line 604
    :cond_3
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x42

    if-eq v2, v3, :cond_4

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x62

    if-ne v2, v3, :cond_5

    .line 605
    :cond_4
    const/4 v0, 0x2

    goto :goto_0

    .line 606
    :cond_5
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x43

    if-eq v2, v3, :cond_6

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x63

    if-ne v2, v3, :cond_7

    .line 607
    :cond_6
    const/4 v0, 0x3

    goto :goto_0

    .line 608
    :cond_7
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x44

    if-eq v2, v3, :cond_8

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x64

    if-ne v2, v3, :cond_9

    .line 609
    :cond_8
    const/4 v0, 0x4

    goto :goto_0

    .line 610
    :cond_9
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x45

    if-eq v2, v3, :cond_a

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x65

    if-ne v2, v3, :cond_b

    .line 611
    :cond_a
    const/4 v0, 0x5

    goto :goto_0

    .line 612
    :cond_b
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x46

    if-eq v2, v3, :cond_c

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x66

    if-ne v2, v3, :cond_d

    .line 613
    :cond_c
    const/4 v0, 0x6

    goto :goto_0

    .line 614
    :cond_d
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x47

    if-eq v2, v3, :cond_e

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x67

    if-ne v2, v3, :cond_f

    .line 615
    :cond_e
    const/4 v0, 0x7

    goto :goto_0

    .line 616
    :cond_f
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x48

    if-eq v2, v3, :cond_10

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x68

    if-ne v2, v3, :cond_11

    .line 617
    :cond_10
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 618
    :cond_11
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x49

    if-eq v2, v3, :cond_12

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x69

    if-ne v2, v3, :cond_13

    .line 619
    :cond_12
    const/16 v0, 0x9

    goto/16 :goto_0

    .line 620
    :cond_13
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x4a

    if-eq v2, v3, :cond_14

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x6a

    if-ne v2, v3, :cond_15

    .line 621
    :cond_14
    const/16 v0, 0xa

    goto/16 :goto_0

    .line 622
    :cond_15
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x4b

    if-eq v2, v3, :cond_16

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x6b

    if-ne v2, v3, :cond_17

    .line 623
    :cond_16
    const/16 v0, 0xb

    goto/16 :goto_0

    .line 624
    :cond_17
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x4c

    if-eq v2, v3, :cond_18

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x6c

    if-ne v2, v3, :cond_19

    .line 625
    :cond_18
    const/16 v0, 0xc

    goto/16 :goto_0

    .line 626
    :cond_19
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x4d

    if-eq v2, v3, :cond_1a

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x6d

    if-ne v2, v3, :cond_1b

    .line 627
    :cond_1a
    const/16 v0, 0xd

    goto/16 :goto_0

    .line 628
    :cond_1b
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x4e

    if-eq v2, v3, :cond_1c

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x6e

    if-ne v2, v3, :cond_1d

    .line 629
    :cond_1c
    const/16 v0, 0xe

    goto/16 :goto_0

    .line 630
    :cond_1d
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x4f

    if-eq v2, v3, :cond_1e

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x6f

    if-ne v2, v3, :cond_1f

    .line 631
    :cond_1e
    const/16 v0, 0xf

    goto/16 :goto_0

    .line 632
    :cond_1f
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x50

    if-eq v2, v3, :cond_20

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x70

    if-ne v2, v3, :cond_21

    .line 633
    :cond_20
    const/16 v0, 0x10

    goto/16 :goto_0

    .line 634
    :cond_21
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x51

    if-eq v2, v3, :cond_22

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x71

    if-ne v2, v3, :cond_23

    .line 635
    :cond_22
    const/16 v0, 0x11

    goto/16 :goto_0

    .line 636
    :cond_23
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x52

    if-eq v2, v3, :cond_24

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x72

    if-ne v2, v3, :cond_25

    .line 637
    :cond_24
    const/16 v0, 0x12

    goto/16 :goto_0

    .line 638
    :cond_25
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x53

    if-eq v2, v3, :cond_26

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x73

    if-ne v2, v3, :cond_27

    .line 639
    :cond_26
    const/16 v0, 0x13

    goto/16 :goto_0

    .line 640
    :cond_27
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x54

    if-eq v2, v3, :cond_28

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x74

    if-ne v2, v3, :cond_29

    .line 641
    :cond_28
    const/16 v0, 0x14

    goto/16 :goto_0

    .line 642
    :cond_29
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x55

    if-eq v2, v3, :cond_2a

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x75

    if-ne v2, v3, :cond_2b

    .line 643
    :cond_2a
    const/16 v0, 0x15

    goto/16 :goto_0

    .line 644
    :cond_2b
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x56

    if-eq v2, v3, :cond_2c

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x76

    if-ne v2, v3, :cond_2d

    .line 645
    :cond_2c
    const/16 v0, 0x16

    goto/16 :goto_0

    .line 646
    :cond_2d
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x57

    if-eq v2, v3, :cond_2e

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x77

    if-ne v2, v3, :cond_2f

    .line 647
    :cond_2e
    const/16 v0, 0x17

    goto/16 :goto_0

    .line 648
    :cond_2f
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x58

    if-eq v2, v3, :cond_30

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x78

    if-ne v2, v3, :cond_31

    .line 649
    :cond_30
    const/16 v0, 0x18

    goto/16 :goto_0

    .line 650
    :cond_31
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x59

    if-eq v2, v3, :cond_32

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x79

    if-ne v2, v3, :cond_33

    .line 651
    :cond_32
    const/16 v0, 0x19

    goto/16 :goto_0

    .line 652
    :cond_33
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x5a

    if-eq v2, v3, :cond_0

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x7a

    if-eq v2, v3, :cond_0

    :cond_34
    move v0, v1

    .line 656
    goto/16 :goto_0
.end method

.method public numericCol()V
    .locals 5

    .prologue
    .line 460
    new-instance v2, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct {v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    .line 462
    .local v2, "newCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-direct {p0, v2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->apply_tbl_first_column(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 468
    iget v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numericcolumcount:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 469
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->textStyleforBlankRowandColumn()V

    .line 470
    :cond_0
    new-instance v0, Lcom/samsung/thumbnail/customview/word/Paragraph;

    iget-object v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v0, v4}, Lcom/samsung/thumbnail/customview/word/Paragraph;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 471
    .local v0, "currentPara":Lcom/samsung/thumbnail/customview/word/Paragraph;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 472
    .local v1, "currentParaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    new-instance v3, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct {v3}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 473
    .local v3, "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    iget v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numericcolumcount:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v4, v3}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->text(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 474
    invoke-virtual {v0, v3}, Lcom/samsung/thumbnail/customview/word/Paragraph;->AddRun(Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 475
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 476
    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setParagraphList(Ljava/util/ArrayList;)V

    .line 477
    iget v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numericcolumcount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->numericcolumcount:I

    .line 480
    const/4 v4, 0x1

    invoke-virtual {p0, v2, v4}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->addXSSFCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Z)V

    .line 481
    return-void
.end method

.method public processSheet(Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;Landroid/content/Context;)V
    .locals 1
    .param p1, "workSheet"    # Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;
    .param p2, "mContex"    # Landroid/content/Context;

    .prologue
    .line 292
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    .line 293
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getdrawingobj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawingobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFDrawing;

    .line 295
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->drawSheet()V

    .line 296
    return-void
.end method

.method public returnMaxArrayVal(Ljava/util/ArrayList;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 3076
    .local p1, "shapetorows":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 3077
    .local v1, "max":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 3079
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 3080
    .local v3, "value":Ljava/lang/String;
    if-eqz v3, :cond_0

    const-string/jumbo v4, "ext"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 3081
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 3082
    .local v2, "score":I
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 3077
    .end local v2    # "score":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3085
    .end local v3    # "value":Ljava/lang/String;
    :cond_1
    return v1
.end method

.method public setHyperlink(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;I)V
    .locals 3
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "i"    # I

    .prologue
    .line 1333
    const-string/jumbo v1, ""

    .line 1334
    .local v1, "xfid":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFid()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1335
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellXFid()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "xfid":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 1338
    .restart local v1    # "xfid":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellStyleName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1340
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string/jumbo v2, "Hyperlink"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    .line 1366
    iget v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->hyperlinkcount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->hyperlinkcount:I

    .line 1372
    :cond_1
    return-void
.end method

.method public setRPrSharedstringStyletoText(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;II)V
    .locals 20
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "siID"    # I
    .param p4, "textStyleID"    # I

    .prologue
    .line 1381
    const/4 v8, 0x0

    .line 1382
    .local v8, "i":I
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    invoke-direct/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->underlines:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    .line 1383
    const-string/jumbo v14, ""

    .line 1384
    .local v14, "str":Ljava/lang/String;
    new-instance v13, Ljava/util/StringTokenizer;

    const-string/jumbo v18, "~"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-direct {v13, v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1386
    .local v13, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v17

    .line 1387
    .local v17, "totaltoken":I
    const/4 v15, 0x0

    .line 1389
    .local v15, "textsz":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getSIStringDetails(I)Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;

    move-result-object v12

    .line 1392
    .local v12, "siDetails":Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;
    if-nez v12, :cond_0

    .line 1524
    :goto_0
    return-void

    .line 1396
    :cond_0
    new-instance v4, Lcom/samsung/thumbnail/customview/word/Paragraph;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Lcom/samsung/thumbnail/customview/word/Paragraph;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1397
    .local v4, "currentPara":Lcom/samsung/thumbnail/customview/word/Paragraph;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getShrinkToFit()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 1398
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setShrinkToFit(Z)V

    .line 1400
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1402
    .local v5, "currentParaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCharacterProperties(I)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v6

    .line 1404
    .local v6, "defaultProperties":Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    .line 1405
    :goto_1
    move/from16 v0, v17

    if-ge v8, v0, :cond_e

    .line 1406
    invoke-virtual {v13}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v14

    .line 1407
    const-string/jumbo v18, "\n"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    .line 1409
    .local v10, "newlinechk":Z
    new-instance v9, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct {v9}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 1411
    .local v9, "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    if-nez v10, :cond_d

    .line 1412
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getRprStyles()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_c

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getRprStyles()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Boolean;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v18

    if-eqz v18, :cond_c

    .line 1415
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-direct/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 1417
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleFontSize()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_2

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleFontSize()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_2

    .line 1422
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleFontSize()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 1426
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontSize(I)V

    .line 1429
    :cond_2
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleBold()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_3

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleBold()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_3

    .line 1432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setBold(Z)V

    .line 1435
    :cond_3
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleUnderline()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_4

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleUnderline()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_4

    .line 1438
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->underlines:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    move-object/from16 v18, v0

    const-string/jumbo v19, "single"

    invoke-static/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;->setStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline$EUnderline;)V

    .line 1439
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->underlines:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setUnderlineProp(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocUnderline;)V

    .line 1442
    :cond_4
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleItalic()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_5

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleItalic()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_5

    .line 1445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setItalic(Z)V

    .line 1448
    :cond_5
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleStrike()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_6

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringStyleStrike()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_6

    .line 1451
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setStrike(Z)V

    .line 1455
    :cond_6
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringTextColorRGB()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_a

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringTextColorRGB()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_a

    .line 1459
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringTextColorRGB()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 1462
    .local v11, "rgb":Ljava/lang/String;
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 1463
    .local v7, "docColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    invoke-virtual {v7, v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1464
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 1483
    .end local v7    # "docColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v11    # "rgb":Ljava/lang/String;
    :cond_7
    :goto_2
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getStyleTextVerticalAlign()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_b

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getStyleTextVerticalAlign()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_b

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getStyleTextVerticalAlign()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    const-string/jumbo v19, "superscript"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v18

    if-nez v18, :cond_b

    .line 1489
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v18, v0

    const-string/jumbo v19, "superscript"

    invoke-static/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setVertAlign(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 1491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;

    move-result-object v18

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->setexcelfontsizechk(Z)V

    .line 1492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontSize(I)V

    .line 1510
    :cond_8
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    .line 1515
    :goto_4
    if-eqz v14, :cond_9

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v18

    if-eqz v18, :cond_9

    .line 1516
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v14, v9}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->text(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 1517
    invoke-virtual {v4, v9}, Lcom/samsung/thumbnail/customview/word/Paragraph;->AddRun(Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 1519
    :cond_9
    add-int/lit8 v8, v8, 0x1

    .line 1520
    goto/16 :goto_1

    .line 1465
    :cond_a
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringTextColorTheme()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_7

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringTextColorTheme()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_7

    .line 1473
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getExSharedStringTextColorTheme()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1476
    .local v2, "colTheme":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 1477
    .local v16, "theme":I
    new-instance v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 1478
    .restart local v7    # "docColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCorrectThemeClr(I)I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getColorSchemRGBval(I)Ljava/lang/String;

    move-result-object v3

    .line 1479
    .local v3, "color":Ljava/lang/String;
    invoke-virtual {v7, v3}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 1480
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    goto/16 :goto_2

    .line 1495
    .end local v2    # "colTheme":Ljava/lang/String;
    .end local v3    # "color":Ljava/lang/String;
    .end local v7    # "docColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    .end local v16    # "theme":I
    :cond_b
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getStyleTextVerticalAlign()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_8

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getStyleTextVerticalAlign()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_8

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelSharedStringSI;->getStyleTextVerticalAlign()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->rprcount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    const-string/jumbo v19, "subscript"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v18

    if-nez v18, :cond_8

    .line 1501
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v18, v0

    const-string/jumbo v19, "subscript"

    invoke-static/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->convert(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setVertAlign(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V

    .line 1503
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;

    move-result-object v18

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyles;->setexcelfontsizechk(Z)V

    .line 1504
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontSize(I)V

    goto/16 :goto_3

    .line 1507
    :cond_c
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v0, v18

    invoke-direct {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    goto/16 :goto_3

    .line 1512
    :cond_d
    new-instance v18, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-object/from16 v0, v18

    invoke-direct {v0, v6}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;-><init>(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    goto/16 :goto_4

    .line 1522
    .end local v9    # "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v10    # "newlinechk":Z
    :cond_e
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1523
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setParagraphList(Ljava/util/ArrayList;)V

    goto/16 :goto_0
.end method

.method public setTextSyle(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;I)V
    .locals 1
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "textstyleid"    # I

    .prologue
    .line 1666
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->setTextSyle(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;II)V

    .line 1667
    return-void
.end method

.method public setTextSyle(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;II)V
    .locals 5
    .param p1, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "textstyleid"    # I
    .param p4, "siID"    # I

    .prologue
    .line 1672
    const/4 v0, 0x0

    .line 1673
    .local v0, "chktext":Z
    if-eqz p2, :cond_0

    .line 1674
    const-string/jumbo v4, "~"

    invoke-virtual {p2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 1675
    :cond_0
    if-eqz v0, :cond_2

    .line 1676
    invoke-virtual {p0, p1, p2, p4, p3}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->setRPrSharedstringStyletoText(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;II)V

    .line 1699
    :cond_1
    :goto_0
    return-void

    .line 1679
    :cond_2
    invoke-direct {p0, p3}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCharacterProperties(I)Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 1681
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    .line 1682
    new-instance v1, Lcom/samsung/thumbnail/customview/word/Paragraph;

    iget-object v4, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mCanvasElementsCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-direct {v1, v4}, Lcom/samsung/thumbnail/customview/word/Paragraph;-><init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 1683
    .local v1, "currentPara":Lcom/samsung/thumbnail/customview/word/Paragraph;
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getShrinkToFit()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1684
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setShrinkToFit(Z)V

    .line 1686
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1687
    .local v2, "currentParaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    new-instance v3, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-direct {v3}, Lcom/samsung/thumbnail/customview/word/Run;-><init>()V

    .line 1688
    .local v3, "newRun":Lcom/samsung/thumbnail/customview/word/Run;
    invoke-direct {p0, p1, p2, v3}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->text(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 1689
    invoke-virtual {v1, v3}, Lcom/samsung/thumbnail/customview/word/Paragraph;->AddRun(Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 1690
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1691
    invoke-virtual {p1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setParagraphList(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public tableBorder(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;III)V
    .locals 19
    .param p1, "cell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "i"    # I
    .param p4, "column"    # I
    .param p5, "fillID"    # I

    .prologue
    .line 905
    const/4 v10, 0x0

    .local v10, "cellborder":Ljava/lang/String;
    const/4 v12, 0x0

    .line 908
    .local v12, "cellcolor":Ljava/lang/String;
    const/4 v5, 0x0

    .line 909
    .local v5, "alignindex":I
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getCellborderid(I)I

    move-result v11

    .line 910
    .local v11, "cellborderid":I
    const-string/jumbo v14, ""

    .local v14, "leftstr":Ljava/lang/String;
    const-string/jumbo v15, ""

    .local v15, "rgtstr":Ljava/lang/String;
    const-string/jumbo v16, ""

    .local v16, "topstr":Ljava/lang/String;
    const-string/jumbo v9, ""

    .line 911
    .local v9, "bottom":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellLeftBorderstyle()Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "cellborder":Ljava/lang/String;
    check-cast v10, Ljava/lang/String;

    .line 913
    .restart local v10    # "cellborder":Ljava/lang/String;
    const/4 v6, 0x0

    .line 915
    .local v6, "borderColor":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->canShowGridLine(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 916
    const-string/jumbo v14, "left"

    .line 917
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v14}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getBorderColor(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 918
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 920
    .local v7, "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    if-eqz v10, :cond_0

    .line 922
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->cellBorderStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v7

    .line 923
    if-nez v12, :cond_0

    .line 925
    const-string/jumbo v12, "8b8989"

    .line 929
    :cond_0
    const/16 v17, 0x10

    move/from16 v0, v17

    invoke-static {v12, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v13

    .line 930
    .local v13, "colorvalue":I
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    .end local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    invoke-direct {v6, v13}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 934
    .restart local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    const v8, 0x3dcccccd    # 0.1f

    .line 936
    .local v8, "borderwidth":F
    sget-object v17, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NIL:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-object/from16 v0, v17

    if-ne v7, v0, :cond_1

    if-eqz p5, :cond_2

    :cond_1
    const/16 v17, 0x1

    move/from16 v0, p4

    move/from16 v1, v17

    if-ne v0, v1, :cond_f

    .line 939
    :cond_2
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 940
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    .end local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    const-string/jumbo v17, "8b8989"

    const/16 v18, 0x10

    invoke-static/range {v17 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v17

    move/from16 v0, v17

    invoke-direct {v6, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 945
    .restart local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6, v8}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 946
    const/4 v6, 0x0

    .line 949
    .end local v7    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v8    # "borderwidth":F
    .end local v13    # "colorvalue":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellRightBorderstyle()Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "cellborder":Ljava/lang/String;
    check-cast v10, Ljava/lang/String;

    .line 952
    .restart local v10    # "cellborder":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->canShowGridLine(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 953
    const-string/jumbo v15, "rgt"

    .line 954
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 956
    .restart local v7    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v15}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getBorderColor(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 957
    if-eqz v10, :cond_4

    .line 960
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->cellBorderStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v7

    .line 961
    if-nez v12, :cond_4

    .line 962
    const-string/jumbo v12, "8b8989"

    .line 966
    :cond_4
    const/16 v17, 0x10

    move/from16 v0, v17

    invoke-static {v12, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v13

    .line 967
    .restart local v13    # "colorvalue":I
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    .end local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    invoke-direct {v6, v13}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 971
    .restart local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    const v8, 0x3dcccccd    # 0.1f

    .line 973
    .restart local v8    # "borderwidth":F
    sget-object v17, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NIL:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-object/from16 v0, v17

    if-ne v7, v0, :cond_10

    if-nez p5, :cond_10

    .line 976
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 977
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    .end local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    const-string/jumbo v17, "8b8989"

    const/16 v18, 0x10

    invoke-static/range {v17 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v17

    move/from16 v0, v17

    invoke-direct {v6, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 982
    .restart local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6, v8}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 983
    const/4 v6, 0x0

    .line 986
    .end local v7    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v8    # "borderwidth":F
    .end local v13    # "colorvalue":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellTopBorderstyle()Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "cellborder":Ljava/lang/String;
    check-cast v10, Ljava/lang/String;

    .line 989
    .restart local v10    # "cellborder":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->canShowGridLine(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 990
    const-string/jumbo v16, "top"

    .line 991
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 993
    .restart local v7    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v11, v1}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getBorderColor(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 994
    if-eqz v10, :cond_6

    .line 995
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->cellBorderStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v7

    .line 996
    if-nez v12, :cond_6

    .line 997
    const-string/jumbo v12, "8b8989"

    .line 1001
    :cond_6
    const/16 v17, 0x10

    move/from16 v0, v17

    invoke-static {v12, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v13

    .line 1002
    .restart local v13    # "colorvalue":I
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    .end local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    invoke-direct {v6, v13}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 1007
    .restart local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    const v8, 0x3dcccccd    # 0.1f

    .line 1009
    .restart local v8    # "borderwidth":F
    sget-object v17, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NIL:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-object/from16 v0, v17

    if-ne v7, v0, :cond_7

    if-eqz p5, :cond_8

    :cond_7
    const/16 v17, 0x1

    move/from16 v0, p3

    move/from16 v1, v17

    if-ne v0, v1, :cond_11

    .line 1010
    :cond_8
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 1011
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    .end local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    const-string/jumbo v17, "8b8989"

    const/16 v18, 0x10

    invoke-static/range {v17 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v17

    move/from16 v0, v17

    invoke-direct {v6, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 1016
    .restart local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6, v8}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 1017
    const/4 v6, 0x0

    .line 1020
    .end local v7    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v8    # "borderwidth":F
    .end local v13    # "colorvalue":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorkBookObj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFWorkbook;->getStyleObj()Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFExcelStyle;->getExStyleCellBottomBorderstyle()Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "cellborder":Ljava/lang/String;
    check-cast v10, Ljava/lang/String;

    .line 1023
    .restart local v10    # "cellborder":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->canShowGridLine(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 1024
    const-string/jumbo v9, "bottom"

    .line 1025
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 1027
    .restart local v7    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v9}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->getBorderColor(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1028
    if-eqz v10, :cond_a

    .line 1031
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->cellBorderStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v7

    .line 1032
    if-nez v12, :cond_a

    .line 1033
    const-string/jumbo v12, "8b8989"

    .line 1037
    :cond_a
    const/16 v17, 0x10

    move/from16 v0, v17

    invoke-static {v12, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v13

    .line 1038
    .restart local v13    # "colorvalue":I
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    .end local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    invoke-direct {v6, v13}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 1043
    .restart local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    const v8, 0x3dcccccd    # 0.1f

    .line 1045
    .restart local v8    # "borderwidth":F
    sget-object v17, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NIL:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-object/from16 v0, v17

    if-ne v7, v0, :cond_b

    if-eqz p5, :cond_c

    :cond_b
    if-nez p3, :cond_12

    .line 1046
    :cond_c
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 1047
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    .end local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    const-string/jumbo v17, "8b8989"

    const/16 v18, 0x10

    invoke-static/range {v17 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v17

    move/from16 v0, v17

    invoke-direct {v6, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 1052
    .restart local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6, v8}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 1053
    const/4 v6, 0x0

    .line 1055
    .end local v7    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v8    # "borderwidth":F
    .end local v13    # "colorvalue":I
    :cond_d
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const/16 v18, 0x20

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const/16 v18, 0x20

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const/16 v18, 0x20

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1058
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    if-eqz v17, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    move/from16 v1, p3

    if-le v0, v1, :cond_13

    .line 1060
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetStyleid()Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1062
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    move-object/from16 v3, p2

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->SetAlignment(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;IILjava/lang/String;)V

    .line 1078
    :cond_e
    :goto_4
    return-void

    .line 943
    .restart local v7    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .restart local v8    # "borderwidth":F
    .restart local v13    # "colorvalue":I
    :cond_f
    const/high16 v17, 0x40a00000    # 5.0f

    mul-float v8, v8, v17

    goto/16 :goto_0

    .line 980
    :cond_10
    const/high16 v17, 0x40a00000    # 5.0f

    mul-float v8, v8, v17

    goto/16 :goto_1

    .line 1014
    :cond_11
    const/high16 v17, 0x40a00000    # 5.0f

    mul-float v8, v8, v17

    goto/16 :goto_2

    .line 1050
    :cond_12
    const/high16 v17, 0x40a00000    # 5.0f

    mul-float v8, v8, v17

    goto/16 :goto_3

    .line 1065
    .end local v7    # "borderStyle":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v8    # "borderwidth":F
    .end local v13    # "colorvalue":I
    :cond_13
    const-string/jumbo v4, ""

    .line 1066
    .local v4, "align":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetTextDataType()Ljava/util/ArrayList;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    move/from16 v1, p3

    if-le v0, v1, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetTextDataType()Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    if-eqz v17, :cond_e

    .line 1069
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->mWorksheetobj:Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/thumbnail/office/ooxml/excel/XSSFSheet;->getExWorkSheetTextDataType()Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    const-string/jumbo v18, "string"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_14

    .line 1071
    const-string/jumbo v4, "left"

    .line 1075
    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v4, v2}, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->alignCellText(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 1073
    :cond_14
    const-string/jumbo v4, "right"

    goto :goto_5
.end method

.method public textStyleforBlankRowandColumn()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 303
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-direct {v1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;-><init>()V

    iput-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    .line 304
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setUndreline(Z)V

    .line 305
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setItalic(Z)V

    .line 306
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setBold(Z)V

    .line 307
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontSize(I)V

    .line 308
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;-><init>()V

    .line 309
    .local v0, "docColor":Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;
    const-string/jumbo v1, "#000000"

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->setColor(Ljava/lang/String;)V

    .line 310
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    invoke-virtual {v1, v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setColor(Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;)V

    .line 311
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/excel/XExcelCustomViewConsumer;->charProp:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    const-string/jumbo v2, "Calibri"

    invoke-virtual {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->setFontName(Ljava/lang/String;)V

    .line 315
    return-void
.end method
