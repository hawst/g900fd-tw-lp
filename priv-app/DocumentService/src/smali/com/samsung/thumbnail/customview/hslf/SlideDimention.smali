.class public Lcom/samsung/thumbnail/customview/hslf/SlideDimention;
.super Ljava/lang/Object;
.source "SlideDimention.java"


# instance fields
.field customViewHeight:D

.field customViewWidth:D

.field mPPTHeight:J

.field mPPTWidth:J

.field mWordHeight:J

.field mWordWidth:J

.field private showOnlyFirstSlide:Z

.field slideHeight:D

.field slideWidth:D


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->showOnlyFirstSlide:Z

    .line 46
    return-void
.end method


# virtual methods
.method public getCustomViewHeight()D
    .locals 2

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->customViewHeight:D

    return-wide v0
.end method

.method public getCustomViewWidth()D
    .locals 2

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->customViewWidth:D

    return-wide v0
.end method

.method public getDocHeight(D)D
    .locals 5
    .param p1, "actualHeight"    # D

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->customViewHeight:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mWordHeight:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 76
    .end local p1    # "actualHeight":D
    :cond_0
    :goto_0
    return-wide p1

    .restart local p1    # "actualHeight":D
    :cond_1
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->customViewHeight:D

    mul-double/2addr v0, p1

    iget-wide v2, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mWordHeight:J

    long-to-double v2, v2

    div-double p1, v0, v2

    goto :goto_0
.end method

.method public getDocObjHeight(D)I
    .locals 5
    .param p1, "docObjHeight"    # D

    .prologue
    .line 152
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->customViewHeight:D

    mul-double/2addr v0, p1

    iget-wide v2, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mWordHeight:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public getDocObjWidth(I)I
    .locals 4
    .param p1, "docObjWidth"    # I

    .prologue
    .line 148
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->customViewWidth:D

    int-to-double v2, p1

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mWordWidth:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public getDocWidth(D)D
    .locals 5
    .param p1, "actualWidth"    # D

    .prologue
    .line 80
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->customViewWidth:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mWordWidth:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 83
    .end local p1    # "actualWidth":D
    :cond_0
    :goto_0
    return-wide p1

    .restart local p1    # "actualWidth":D
    :cond_1
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->customViewWidth:D

    mul-double/2addr v0, p1

    iget-wide v2, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mWordWidth:J

    long-to-double v2, v2

    div-double p1, v0, v2

    goto :goto_0
.end method

.method public getObjectHeight(D)D
    .locals 2
    .param p1, "actualHeight"    # D

    .prologue
    .line 132
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getObjectHeight(I)I
    .locals 1
    .param p1, "actualHeight"    # I

    .prologue
    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method public getObjectWidth(D)D
    .locals 2
    .param p1, "actualWidth"    # D

    .prologue
    .line 127
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getObjectWidth(I)I
    .locals 1
    .param p1, "actualWidth"    # I

    .prologue
    .line 138
    const/4 v0, 0x0

    return v0
.end method

.method public getPPTHeight()J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mPPTHeight:J

    return-wide v0
.end method

.method public getPPTWidth()J
    .locals 2

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mPPTWidth:J

    return-wide v0
.end method

.method public getPptObjHeight(D)D
    .locals 5
    .param p1, "objHeight"    # D

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->customViewHeight:D

    mul-double/2addr v0, p1

    iget-wide v2, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mPPTHeight:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    double-to-int v0, v0

    int-to-double v0, v0

    return-wide v0
.end method

.method public getPptObjWidth(D)D
    .locals 5
    .param p1, "objWidth"    # D

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->customViewWidth:D

    mul-double/2addr v0, p1

    iget-wide v2, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mPPTWidth:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    double-to-int v0, v0

    int-to-double v0, v0

    return-wide v0
.end method

.method public getSlideHeight()D
    .locals 2

    .prologue
    .line 118
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->slideHeight:D

    return-wide v0
.end method

.method public getSlideWidth()D
    .locals 2

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->slideWidth:D

    return-wide v0
.end method

.method public getWordHeight()J
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mWordHeight:J

    return-wide v0
.end method

.method public getWordLeftMargin(I)I
    .locals 4
    .param p1, "actualLeftMargin"    # I

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->customViewWidth:D

    int-to-double v2, p1

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mWordWidth:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public getWordTopMargin(I)I
    .locals 4
    .param p1, "actualTopMargin"    # I

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->customViewHeight:D

    int-to-double v2, p1

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mWordHeight:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public getWordWidth()J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mWordWidth:J

    return-wide v0
.end method

.method public isShowOnlyFirstSlide()Z
    .locals 1

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->showOnlyFirstSlide:Z

    return v0
.end method

.method public setCustomViewHeight(D)V
    .locals 1
    .param p1, "customViewHeight"    # D

    .prologue
    .line 106
    iput-wide p1, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->customViewHeight:D

    .line 107
    return-void
.end method

.method public setCustomViewWidth(D)V
    .locals 1
    .param p1, "customViewWidth"    # D

    .prologue
    .line 98
    iput-wide p1, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->customViewWidth:D

    .line 99
    return-void
.end method

.method public setPPTHeight(J)V
    .locals 1
    .param p1, "pptHeight"    # J

    .prologue
    .line 27
    iput-wide p1, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mPPTHeight:J

    .line 28
    return-void
.end method

.method public setPPTWidth(J)V
    .locals 1
    .param p1, "pptWidth"    # J

    .prologue
    .line 19
    iput-wide p1, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mPPTWidth:J

    .line 20
    return-void
.end method

.method public setShowOnlyFirstSlide(Z)V
    .locals 0
    .param p1, "showOnlyFirstSlide"    # Z

    .prologue
    .line 160
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->showOnlyFirstSlide:Z

    .line 161
    return-void
.end method

.method public setSlideHeight(D)V
    .locals 1
    .param p1, "height"    # D

    .prologue
    .line 110
    iput-wide p1, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->slideHeight:D

    .line 111
    return-void
.end method

.method public setSlideWidth(D)V
    .locals 1
    .param p1, "width"    # D

    .prologue
    .line 114
    iput-wide p1, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->slideWidth:D

    .line 115
    return-void
.end method

.method public setWordHeight(J)V
    .locals 1
    .param p1, "mWordHeight"    # J

    .prologue
    .line 57
    iput-wide p1, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mWordHeight:J

    .line 58
    return-void
.end method

.method public setWordWidth(J)V
    .locals 1
    .param p1, "mWordWidth"    # J

    .prologue
    .line 49
    iput-wide p1, p0, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->mWordWidth:J

    .line 50
    return-void
.end method
