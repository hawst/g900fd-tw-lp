.class public Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;
.super Ljava/lang/Object;
.source "CanvasTableMergedCells.java"


# instance fields
.field private mMergedCellList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;",
            ">;"
        }
    .end annotation
.end field

.field private rowSpanCellProp:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->mMergedCellList:Ljava/util/ArrayList;

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->rowSpanCellProp:Ljava/util/Map;

    .line 24
    return-void
.end method


# virtual methods
.method public CreateNewCellMergeList(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 3
    .param p1, "mergedCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .local v0, "cellsToMerge":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->rowSpanCellProp:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getMergedCellKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    return-void
.end method

.method public addToCellMergeList(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 3
    .param p1, "mergedCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    .line 33
    iget-object v1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->rowSpanCellProp:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getMergedCellKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 35
    .local v0, "cellsToMerge":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    :cond_0
    return-void
.end method

.method public getMergedCellList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 364
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->mMergedCellList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public mergeVerticalCells()V
    .locals 22

    .prologue
    .line 185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->rowSpanCellProp:Ljava/util/Map;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v8

    .line 187
    .local v8, "keys":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;>;"
    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 189
    .local v7, "keyIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_16

    .line 190
    new-instance v9, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct {v9}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    .line 191
    .local v9, "mergedCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    const/4 v15, 0x0

    .line 192
    .local v15, "totalHeight":F
    const/16 v16, 0x0

    .line 193
    .local v16, "totalWidth":F
    const/16 v17, 0x0

    .line 194
    .local v17, "x":F
    const/16 v18, 0x0

    .line 195
    .local v18, "y":F
    new-instance v13, Ljava/lang/StringBuilder;

    const/16 v19, 0x100

    move/from16 v0, v19

    invoke-direct {v13, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 197
    .local v13, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v10, 0x0

    .line 198
    .local v10, "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    const/4 v5, 0x0

    .line 200
    .local v5, "count":I
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    .line 202
    .local v4, "cellsToMerge":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    if-eqz v4, :cond_0

    .line 206
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHideCell()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 207
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setHideCell(Z)V

    .line 210
    :cond_1
    invoke-virtual {v9}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHideCell()Z

    move-result v19

    if-nez v19, :cond_8

    .line 211
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getWrapContent()Z

    move-result v19

    if-eqz v19, :cond_2

    .line 212
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setWrapContent(Z)V

    .line 215
    :cond_2
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getShrinkToFit()Z

    move-result v19

    if-eqz v19, :cond_3

    .line 216
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setShrinkToFit(Z)V

    .line 219
    :cond_3
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextIndent()I

    move-result v19

    if-eqz v19, :cond_4

    .line 220
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextIndent()I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTextIndent(I)V

    .line 224
    :cond_4
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextRotation()S

    move-result v19

    if-eqz v19, :cond_5

    .line 225
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextRotation()S

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTextRotation(S)V

    .line 229
    :cond_5
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_6

    .line 230
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setHorizontalAlignment(Ljava/lang/String;)V

    .line 234
    :cond_6
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getVerticalAlignment()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_7

    .line 235
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getVerticalAlignment()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setVerticalAlignment(Ljava/lang/String;)V

    .line 239
    :cond_7
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    move-result-object v19

    if-eqz v19, :cond_8

    .line 240
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v19

    if-eqz v19, :cond_8

    .line 242
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v19

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    .line 249
    :cond_8
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 250
    .local v12, "size":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-ge v6, v12, :cond_14

    .line 251
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 252
    .local v14, "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellText()Ljava/lang/String;

    move-result-object v3

    .line 254
    .local v3, "cellText":Ljava/lang/String;
    if-nez v6, :cond_b

    .line 255
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v17

    .line 256
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v18

    .line 265
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v19

    if-eqz v19, :cond_9

    .line 266
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v19

    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v20

    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v9, v0, v1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 272
    :cond_9
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v19

    if-eqz v19, :cond_a

    .line 273
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v19

    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v20

    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v9, v0, v1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 279
    :cond_a
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    move-result-object v19

    if-eqz v19, :cond_b

    .line 280
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->isGradientFill()Z

    move-result v19

    if-eqz v19, :cond_13

    .line 281
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getGradientFill()Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lcom/samsung/thumbnail/office/ooxml/excel/GradientFill;)V

    .line 293
    :cond_b
    :goto_2
    add-int/lit8 v19, v12, -0x1

    move/from16 v0, v19

    if-ne v6, v0, :cond_d

    .line 295
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v19

    if-eqz v19, :cond_c

    .line 296
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v19

    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v20

    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v9, v0, v1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 302
    :cond_c
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v19

    if-eqz v19, :cond_d

    .line 303
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v19

    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v20

    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v9, v0, v1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 310
    :cond_d
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getParagraphList()Ljava/util/ArrayList;

    move-result-object v19

    if-eqz v19, :cond_f

    .line 311
    if-nez v10, :cond_e

    .line 312
    new-instance v10, Ljava/util/ArrayList;

    .end local v10    # "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 315
    .restart local v10    # "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    :cond_e
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getParagraphList()Ljava/util/ArrayList;

    move-result-object v11

    .line 318
    .local v11, "paraListTemp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 322
    .end local v11    # "paraListTemp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    :cond_f
    if-eqz v3, :cond_11

    .line 323
    if-eqz v5, :cond_10

    .line 324
    const/16 v19, 0x20

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 326
    :cond_10
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    add-int/lit8 v5, v5, 0x1

    .line 330
    :cond_11
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v19

    add-float v15, v15, v19

    .line 332
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v19

    cmpg-float v19, v16, v19

    if-gez v19, :cond_12

    .line 333
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v16

    .line 250
    :cond_12
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 284
    :cond_13
    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v19

    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->isGradientFill()Z

    move-result v20

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    goto/16 :goto_2

    .line 340
    .end local v3    # "cellText":Ljava/lang/String;
    .end local v14    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    :cond_14
    if-eqz v10, :cond_15

    .line 341
    invoke-virtual {v9, v10}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setParagraphList(Ljava/util/ArrayList;)V

    .line 344
    :cond_15
    invoke-virtual {v9, v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellHeight(F)V

    .line 345
    move/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 346
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellText(Ljava/lang/String;)V

    .line 347
    move/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellX(F)V

    .line 348
    move/from16 v0, v18

    invoke-virtual {v9, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellY(F)V

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->mMergedCellList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 361
    .end local v4    # "cellsToMerge":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v5    # "count":I
    .end local v6    # "i":I
    .end local v9    # "mergedCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v10    # "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Paragraph;>;"
    .end local v12    # "size":I
    .end local v13    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v15    # "totalHeight":F
    .end local v16    # "totalWidth":F
    .end local v17    # "x":F
    .end local v18    # "y":F
    :cond_16
    return-void
.end method

.method public mergeVerticalCellsForWord()V
    .locals 23

    .prologue
    .line 42
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->rowSpanCellProp:Ljava/util/Map;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v9

    .line 44
    .local v9, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 46
    .local v8, "keyIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_10

    .line 47
    new-instance v10, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct {v10}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    .line 48
    .local v10, "mergedCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    const/16 v16, 0x0

    .line 49
    .local v16, "totalHeight":F
    const/16 v17, 0x0

    .line 50
    .local v17, "totalWidth":F
    const/16 v18, 0x0

    .line 51
    .local v18, "x":F
    const/16 v19, 0x0

    .line 52
    .local v19, "y":F
    new-instance v14, Ljava/lang/StringBuilder;

    const/16 v20, 0x100

    move/from16 v0, v20

    invoke-direct {v14, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 54
    .local v14, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v11, 0x0

    .line 56
    .local v11, "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    const/4 v5, 0x0

    .line 58
    .local v5, "count":I
    const/4 v4, 0x0

    .line 60
    .local v4, "cellsToMerge":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 61
    .local v7, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->rowSpanCellProp:Ljava/util/Map;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "cellsToMerge":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    check-cast v4, Ljava/util/ArrayList;

    .line 63
    .restart local v4    # "cellsToMerge":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    if-eqz v4, :cond_0

    .line 66
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 67
    .local v13, "size":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-ge v6, v13, :cond_c

    .line 68
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 69
    .local v15, "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellText()Ljava/lang/String;

    move-result-object v3

    .line 71
    .local v3, "cellText":Ljava/lang/String;
    if-nez v6, :cond_5

    .line 72
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellX()F

    move-result v18

    .line 73
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellY()F

    move-result v19

    .line 82
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v20

    if-eqz v20, :cond_1

    .line 83
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v20

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v21

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getLeftBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v22

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v10, v0, v1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 89
    :cond_1
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v20

    if-eqz v20, :cond_2

    .line 90
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v20

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v21

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getRightBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v22

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v10, v0, v1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 96
    :cond_2
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v20

    if-eqz v20, :cond_3

    .line 97
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v20

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v21

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getBottomBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v22

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v10, v0, v1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 103
    :cond_3
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v20

    if-eqz v20, :cond_4

    .line 104
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v20

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v21

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTopBorder()Lcom/samsung/thumbnail/customview/tables/TableBorder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/TableBorder;->getBorderWidth()F

    move-result v22

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v10, v0, v1, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 110
    :cond_4
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    move-result-object v20

    if-eqz v20, :cond_5

    .line 111
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->getfillColor()Lorg/apache/poi/java/awt/Color;

    move-result-object v20

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellFillStyle()Lcom/samsung/thumbnail/customview/tables/CellFillStyle;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/tables/CellFillStyle;->isGradientFill()Z

    move-result v21

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    .line 118
    :cond_5
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getDocParagraphList()Ljava/util/ArrayList;

    move-result-object v20

    if-eqz v20, :cond_7

    .line 119
    if-nez v11, :cond_6

    .line 120
    new-instance v11, Ljava/util/ArrayList;

    .end local v11    # "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 123
    .restart local v11    # "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    :cond_6
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getDocParagraphList()Ljava/util/ArrayList;

    move-result-object v12

    .line 126
    .local v12, "paraListTemp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 130
    .end local v12    # "paraListTemp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    :cond_7
    if-eqz v3, :cond_9

    .line 131
    if-eqz v5, :cond_8

    .line 132
    const/16 v20, 0x20

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 134
    :cond_8
    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    add-int/lit8 v5, v5, 0x1

    .line 138
    :cond_9
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellHeight()F

    move-result v20

    add-float v16, v16, v20

    .line 140
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v20

    cmpg-float v20, v17, v20

    if-gez v20, :cond_a

    .line 141
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getCellWidth()F

    move-result v17

    .line 145
    :cond_a
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v20

    if-eqz v20, :cond_b

    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v20

    const-string/jumbo v21, "normal"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-nez v20, :cond_b

    .line 148
    invoke-virtual {v15}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTextDirection(Ljava/lang/String;)V

    .line 67
    :cond_b
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 152
    .end local v3    # "cellText":Ljava/lang/String;
    .end local v15    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    :cond_c
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v20

    if-nez v20, :cond_d

    .line 153
    const-string/jumbo v20, "normal"

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTextDirection(Ljava/lang/String;)V

    .line 156
    :cond_d
    if-eqz v11, :cond_e

    .line 157
    invoke-virtual {v10, v11}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setDocParagraphList(Ljava/util/ArrayList;)V

    .line 160
    :cond_e
    move/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellHeight(F)V

    .line 161
    move/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 162
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v20

    if-eqz v20, :cond_f

    .line 163
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getTextDirection()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTextDirection(Ljava/lang/String;)V

    .line 167
    :goto_2
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellText(Ljava/lang/String;)V

    .line 168
    move/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellX(F)V

    .line 169
    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellY(F)V

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/CanvasTableMergedCells;->mMergedCellList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 166
    :cond_f
    const-string/jumbo v20, "normal"

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTextDirection(Ljava/lang/String;)V

    goto :goto_2

    .line 181
    .end local v4    # "cellsToMerge":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    .end local v5    # "count":I
    .end local v6    # "i":I
    .end local v7    # "key":Ljava/lang/String;
    .end local v10    # "mergedCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .end local v11    # "paraList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    .end local v13    # "size":I
    .end local v14    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v16    # "totalHeight":F
    .end local v17    # "totalWidth":F
    .end local v18    # "x":F
    .end local v19    # "y":F
    :cond_10
    return-void
.end method
