.class public final enum Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
.super Ljava/lang/Enum;
.source "CanvasElementPart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/customview/CanvasElementPart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HorizontalAlignmentElement"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

.field public static final enum ABSOLUTE:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

.field public static final enum CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

.field public static final enum LEFT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

.field public static final enum RIGHT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    const-string/jumbo v1, "LEFT"

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->LEFT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    const-string/jumbo v1, "CENTERED"

    invoke-direct {v0, v1, v3}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    const-string/jumbo v1, "RIGHT"

    invoke-direct {v0, v1, v4}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->RIGHT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    const-string/jumbo v1, "ABSOLUTE"

    invoke-direct {v0, v1, v5}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->ABSOLUTE:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    .line 40
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->LEFT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->CENTERED:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->RIGHT:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->ABSOLUTE:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->$VALUES:[Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 40
    const-class v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->$VALUES:[Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalAlignmentElement;

    return-object v0
.end method
