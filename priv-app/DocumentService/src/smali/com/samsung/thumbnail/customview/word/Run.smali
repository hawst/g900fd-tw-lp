.class public Lcom/samsung/thumbnail/customview/word/Run;
.super Lcom/samsung/thumbnail/customview/CanvasElementPart;
.source "Run.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final MAX_CHAR_COUNT_IN_RUN:I = 0x5


# instance fields
.field align:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

.field firstLineIndent:I

.field private hasTab:Z

.field private highlightedColor:Ljava/lang/String;

.field isCAPSLetterOn:Z

.field isSMALLLetterOn:Z

.field leftIndent:I

.field lineSpacing:I

.field mRunText:Ljava/lang/String;

.field mTextPaint:Landroid/text/TextPaint;

.field rightIndent:I

.field private shadeColor:Ljava/lang/String;

.field typeFace:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 19
    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->isCAPSLetterOn:Z

    .line 21
    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->isSMALLLetterOn:Z

    .line 39
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/word/Run;->init()V

    .line 40
    return-void
.end method

.method public static final getTextWidths(Landroid/text/TextPaint;Ljava/lang/String;II)[F
    .locals 12
    .param p0, "textPaint"    # Landroid/text/TextPaint;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 246
    const/4 v9, 0x0

    .line 247
    .local v9, "widths":[F
    sub-int v3, p3, p2

    .line 248
    .local v3, "length":I
    const/4 v10, 0x5

    if-le v3, v10, :cond_4

    .line 249
    move v4, v3

    .line 250
    .local v4, "lengthPending":I
    rem-int/lit8 v10, v3, 0x5

    if-nez v10, :cond_1

    div-int/lit8 v5, v3, 0x5

    .line 252
    .local v5, "numberofiterations":I
    :goto_0
    new-array v8, v5, [[F

    .line 253
    .local v8, "widthArray":[[F
    const/4 v0, 0x0

    .line 254
    .local v0, "count":I
    move v6, p2

    .line 256
    .local v6, "stringIndex":I
    :cond_0
    const/4 v10, 0x5

    if-lt v4, v10, :cond_2

    const/4 v7, 0x5

    .line 258
    .local v7, "toProcess":I
    :goto_1
    new-array v10, v7, [F

    aput-object v10, v8, v0

    .line 259
    add-int v10, v6, v7

    aget-object v11, v8, v0

    invoke-virtual {p0, p1, v6, v10, v11}, Landroid/text/TextPaint;->getTextWidths(Ljava/lang/String;II[F)I

    .line 261
    add-int/lit8 v0, v0, 0x1

    .line 262
    add-int/2addr v6, v7

    .line 263
    sub-int/2addr v4, v7

    .line 264
    if-gtz v4, :cond_0

    .line 265
    const/4 v0, 0x0

    .line 266
    new-array v9, v3, [F

    .line 267
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v5, :cond_5

    .line 268
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_3
    aget-object v10, v8, v1

    array-length v10, v10

    if-ge v2, v10, :cond_3

    .line 269
    aget-object v10, v8, v1

    aget v10, v10, v2

    aput v10, v9, v0

    .line 270
    add-int/lit8 v0, v0, 0x1

    .line 268
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 250
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "j":I
    .end local v5    # "numberofiterations":I
    .end local v6    # "stringIndex":I
    .end local v7    # "toProcess":I
    .end local v8    # "widthArray":[[F
    :cond_1
    div-int/lit8 v10, v3, 0x5

    add-int/lit8 v5, v10, 0x1

    goto :goto_0

    .restart local v0    # "count":I
    .restart local v5    # "numberofiterations":I
    .restart local v6    # "stringIndex":I
    .restart local v8    # "widthArray":[[F
    :cond_2
    move v7, v4

    .line 256
    goto :goto_1

    .line 267
    .restart local v1    # "i":I
    .restart local v2    # "j":I
    .restart local v7    # "toProcess":I
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 274
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "j":I
    .end local v4    # "lengthPending":I
    .end local v5    # "numberofiterations":I
    .end local v6    # "stringIndex":I
    .end local v7    # "toProcess":I
    .end local v8    # "widthArray":[[F
    :cond_4
    new-array v9, v3, [F

    .line 275
    invoke-virtual {p0, p1, p2, p3, v9}, Landroid/text/TextPaint;->getTextWidths(Ljava/lang/String;II[F)I

    .line 277
    :cond_5
    return-object v9
.end method

.method private init()V
    .locals 2

    .prologue
    .line 44
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    .line 45
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 46
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    const/high16 v1, 0x41100000    # 9.0f

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 47
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 48
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 49
    const/16 v0, 0xc

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->lineSpacing:I

    .line 50
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 234
    const/4 v2, 0x0

    .line 236
    .local v2, "run":Lcom/samsung/thumbnail/customview/word/Run;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/samsung/thumbnail/customview/word/Run;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_0
    return-object v2

    .line 237
    :catch_0
    move-exception v1

    .line 238
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    const-string/jumbo v3, "DocumentService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/CloneNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getFirstLineIndent()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->firstLineIndent:I

    return v0
.end method

.method public getHighLightedColor(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-static {p1}, Lcom/samsung/thumbnail/office/ThumbnailUtils;->getHighLightColor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHighLightedColorKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->highlightedColor:Ljava/lang/String;

    return-object v0
.end method

.method public getLeftIndent()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->leftIndent:I

    return v0
.end method

.method public getLineSpacing()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->lineSpacing:I

    return v0
.end method

.method public getRightIndent()I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->rightIndent:I

    return v0
.end method

.method public getRunText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mRunText:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 95
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->isSMALLLetterOn:Z

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mRunText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mRunText:Ljava/lang/String;

    .line 98
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->isCAPSLetterOn:Z

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mRunText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mRunText:Ljava/lang/String;

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mRunText:Ljava/lang/String;

    return-object v0
.end method

.method public getShadingColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->shadeColor:Ljava/lang/String;

    return-object v0
.end method

.method public getTextPaint()Landroid/text/TextPaint;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    return-object v0
.end method

.method public getTextSize()F
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextSize()F

    move-result v0

    return v0
.end method

.method public getVerticalalignment()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->align:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    return-object v0
.end method

.method public hasTab()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->hasTab:Z

    return v0
.end method

.method public isEmptyText()Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mRunText:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mRunText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 54
    :cond_0
    const/4 v0, 0x1

    .line 56
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCAPLetterON()V
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->isCAPSLetterOn:Z

    .line 210
    return-void
.end method

.method public setColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 116
    return-void
.end method

.method public setFirstLineIndent(I)V
    .locals 0
    .param p1, "firstLineIndent"    # I

    .prologue
    .line 205
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Run;->firstLineIndent:I

    .line 206
    return-void
.end method

.method public setFontStyle(I)V
    .locals 0
    .param p1, "fontStyle"    # I

    .prologue
    .line 158
    packed-switch p1, :pswitch_data_0

    .line 174
    :pswitch_0
    return-void

    .line 158
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setHighLightedColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "highLightedColor"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/Run;->highlightedColor:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setLeftIndent(I)V
    .locals 0
    .param p1, "leftIndent"    # I

    .prologue
    .line 189
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Run;->leftIndent:I

    .line 190
    return-void
.end method

.method public setLineSpacing(I)V
    .locals 0
    .param p1, "lineSpacing"    # I

    .prologue
    .line 181
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Run;->lineSpacing:I

    .line 182
    return-void
.end method

.method public setRightIndent(I)V
    .locals 0
    .param p1, "rightIndent"    # I

    .prologue
    .line 197
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Run;->rightIndent:I

    .line 198
    return-void
.end method

.method public setRunText(Ljava/lang/String;)V
    .locals 0
    .param p1, "runText"    # Ljava/lang/String;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/Run;->mRunText:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public setSMALLLetterON()V
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->isSMALLLetterOn:Z

    .line 214
    return-void
.end method

.method public setShadingColor(Ljava/lang/String;)V
    .locals 0
    .param p1, "shadeColor"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/Run;->shadeColor:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public setStrikeThruText(Z)V
    .locals 1
    .param p1, "strikeThruText"    # Z

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setStrikeThruText(Z)V

    .line 145
    return-void
.end method

.method public setTabStatus(Z)V
    .locals 0
    .param p1, "hasTab"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/word/Run;->hasTab:Z

    .line 82
    return-void
.end method

.method public setTextBold()V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-static {v1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 141
    return-void
.end method

.method public setTextItalics()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    const/high16 v1, -0x41800000    # -0.25f

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSkewX(F)V

    .line 137
    return-void
.end method

.method public setTextPaint(Landroid/text/TextPaint;)V
    .locals 0
    .param p1, "mTextPaint"    # Landroid/text/TextPaint;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    .line 112
    return-void
.end method

.method public setTextSize(F)V
    .locals 1
    .param p1, "textSize"    # F

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 121
    return-void
.end method

.method public setTypeface(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "fontName"    # Ljava/lang/String;
    .param p2, "bold"    # Z

    .prologue
    .line 148
    if-eqz p2, :cond_0

    .line 149
    const/4 v0, 0x1

    invoke-static {p1, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->typeFace:Landroid/graphics/Typeface;

    .line 152
    :goto_0
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/word/Run;->typeFace:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 153
    return-void

    .line 151
    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->typeFace:Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method public setUnderlineText(Z)V
    .locals 1
    .param p1, "underlineText"    # Z

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 132
    return-void
.end method

.method public setVertivalAlignment(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;)V
    .locals 3
    .param p1, "align"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 217
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUBSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    if-ne p1, v0, :cond_0

    .line 218
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/Run;->align:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    .line 219
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F

    move-result v1

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 221
    :cond_0
    sget-object v0, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUPERSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    if-ne p1, v0, :cond_1

    .line 222
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/Run;->align:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    .line 223
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Run;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F

    move-result v1

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 225
    :cond_1
    return-void
.end method
