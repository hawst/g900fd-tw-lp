.class public Lcom/samsung/thumbnail/customview/hslf/LineParam;
.super Lcom/samsung/thumbnail/customview/CanvasElementPart;
.source "LineParam.java"


# instance fields
.field private endX:F

.field private endY:F

.field private lineColor:I

.field private lineWidth:F

.field private startX:F

.field private startY:F


# direct methods
.method public constructor <init>(FFFFIF)V
    .locals 0
    .param p1, "startX"    # F
    .param p2, "startY"    # F
    .param p3, "endX"    # F
    .param p4, "endY"    # F
    .param p5, "lineColor"    # I
    .param p6, "lineWidth"    # F

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 16
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->startX:F

    .line 17
    iput p2, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->startY:F

    .line 18
    iput p3, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->endX:F

    .line 19
    iput p4, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->endY:F

    .line 20
    iput p5, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->lineColor:I

    .line 21
    iput p6, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->lineWidth:F

    .line 22
    return-void
.end method


# virtual methods
.method public getEndX()F
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->endX:F

    return v0
.end method

.method public getEndY()F
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->endY:F

    return v0
.end method

.method public getLineColor()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->lineColor:I

    return v0
.end method

.method public getLineWidth()F
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->lineWidth:F

    return v0
.end method

.method public getStartX()F
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->startX:F

    return v0
.end method

.method public getStartY()F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->startY:F

    return v0
.end method

.method public setEndX(F)V
    .locals 0
    .param p1, "endX"    # F

    .prologue
    .line 45
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->endX:F

    .line 46
    return-void
.end method

.method public setEndY(F)V
    .locals 0
    .param p1, "endY"    # F

    .prologue
    .line 53
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->endY:F

    .line 54
    return-void
.end method

.method public setLineColor(I)V
    .locals 0
    .param p1, "lineColor"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->lineColor:I

    .line 62
    return-void
.end method

.method public setLineWidth(I)V
    .locals 1
    .param p1, "lineWidth"    # I

    .prologue
    .line 69
    int-to-float v0, p1

    iput v0, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->lineWidth:F

    .line 70
    return-void
.end method

.method public setStartX(F)V
    .locals 0
    .param p1, "startX"    # F

    .prologue
    .line 29
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->startX:F

    .line 30
    return-void
.end method

.method public setStartY(F)V
    .locals 0
    .param p1, "startY"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/LineParam;->startY:F

    .line 38
    return-void
.end method
