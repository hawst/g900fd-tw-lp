.class public Lcom/samsung/thumbnail/customview/hslf/BitmapContents;
.super Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
.source "BitmapContents.java"


# instance fields
.field private bitMap:Landroid/graphics/Bitmap;

.field private footerName:Ljava/lang/String;

.field private headerName:Ljava/lang/String;

.field private heightOfBitmap:I

.field private isBehindDoc:Z

.field private isFooterImage:Z

.field private isHeaderImage:Z

.field private isInLine:Z

.field private isWaterMarkImage:Z

.field private leftCoOrdinate:I

.field private mEnclosedSpName:Ljava/lang/String;

.field private paraNum:I

.field private relativeHeight:J

.field private rotationAngle:I

.field private topCoOrdinate:I

.field private widthOfBitmap:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;IIII)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "leftCoOrdinate"    # I
    .param p3, "topCoOrdinate"    # I
    .param p4, "widthOfBitmap"    # I
    .param p5, "heightOfBitmap"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isInLine:Z

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isBehindDoc:Z

    .line 27
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->bitMap:Landroid/graphics/Bitmap;

    .line 28
    iput p2, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->leftCoOrdinate:I

    .line 29
    iput p3, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->topCoOrdinate:I

    .line 30
    iput p4, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->widthOfBitmap:I

    .line 31
    iput p5, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->heightOfBitmap:I

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;IIIII)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "leftCoOrdinate"    # I
    .param p3, "topCoOrdinate"    # I
    .param p4, "widthOfBitmap"    # I
    .param p5, "heightOfBitmap"    # I
    .param p6, "paraNum"    # I

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isInLine:Z

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isBehindDoc:Z

    .line 36
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->bitMap:Landroid/graphics/Bitmap;

    .line 37
    iput p2, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->leftCoOrdinate:I

    .line 38
    iput p3, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->topCoOrdinate:I

    .line 39
    iput p4, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->widthOfBitmap:I

    .line 40
    iput p5, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->heightOfBitmap:I

    .line 41
    iput p6, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->paraNum:I

    .line 42
    return-void
.end method


# virtual methods
.method public getBitMap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->bitMap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getEnclosedSpName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->mEnclosedSpName:Ljava/lang/String;

    return-object v0
.end method

.method public getFooterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->footerName:Ljava/lang/String;

    return-object v0
.end method

.method public getHeaderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->headerName:Ljava/lang/String;

    return-object v0
.end method

.method public getHeightOfBitmap()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->heightOfBitmap:I

    return v0
.end method

.method public getLeftCoOrdinate()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->leftCoOrdinate:I

    return v0
.end method

.method public getParaNumber()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->paraNum:I

    return v0
.end method

.method public getRelativeHeight()J
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->relativeHeight:J

    return-wide v0
.end method

.method public getRotationAngle()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->rotationAngle:I

    return v0
.end method

.method public getTopCoOrdinate()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->topCoOrdinate:I

    return v0
.end method

.method public getWidthOfBitmap()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->widthOfBitmap:I

    return v0
.end method

.method public isBehindDoc()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isBehindDoc:Z

    return v0
.end method

.method public isFooterImage()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isFooterImage:Z

    return v0
.end method

.method public isHeaderImage()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isHeaderImage:Z

    return v0
.end method

.method public isInLine()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isInLine:Z

    return v0
.end method

.method public isWaterMarkImage()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isWaterMarkImage:Z

    return v0
.end method

.method public setBitMap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitMap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->bitMap:Landroid/graphics/Bitmap;

    .line 122
    return-void
.end method

.method public setEnclosedName(Ljava/lang/String;)V
    .locals 0
    .param p1, "enclosedSpName"    # Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->mEnclosedSpName:Ljava/lang/String;

    .line 162
    return-void
.end method

.method public setFooterName(Ljava/lang/String;)V
    .locals 0
    .param p1, "footerName"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->footerName:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public setHeaderName(Ljava/lang/String;)V
    .locals 0
    .param p1, "headerName"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->headerName:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setHeightOfBitmap(I)V
    .locals 0
    .param p1, "heightOfBitmap"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->heightOfBitmap:I

    .line 114
    return-void
.end method

.method public setIsBehindDocState(Z)V
    .locals 0
    .param p1, "isBehindDoc"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isBehindDoc:Z

    .line 54
    return-void
.end method

.method public setIsFooterImage(Z)V
    .locals 0
    .param p1, "isFooterImage"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isFooterImage:Z

    .line 90
    return-void
.end method

.method public setIsHeaderImage(Z)V
    .locals 0
    .param p1, "isHeaderImage"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isHeaderImage:Z

    .line 74
    return-void
.end method

.method public setIsInLineStatus(Z)V
    .locals 0
    .param p1, "isInLine"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isInLine:Z

    .line 66
    return-void
.end method

.method public setLeftCoOrdinate(I)V
    .locals 0
    .param p1, "leftCoOrdinate"    # I

    .prologue
    .line 129
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->leftCoOrdinate:I

    .line 130
    return-void
.end method

.method public setParaNumber(I)V
    .locals 0
    .param p1, "paraNumber"    # I

    .prologue
    .line 149
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->paraNum:I

    .line 150
    return-void
.end method

.method public setRelativeHeight(J)V
    .locals 1
    .param p1, "relativeHeight"    # J

    .prologue
    .line 49
    iput-wide p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->relativeHeight:J

    .line 50
    return-void
.end method

.method public setRotationAngle(I)V
    .locals 0
    .param p1, "rotation"    # I

    .prologue
    .line 157
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->rotationAngle:I

    .line 158
    return-void
.end method

.method public setTopCoOrdinate(I)V
    .locals 0
    .param p1, "topCoOrdinate"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->topCoOrdinate:I

    .line 138
    return-void
.end method

.method public setWaterMarkImage(Z)V
    .locals 0
    .param p1, "isWaterMarkImage"    # Z

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->isWaterMarkImage:Z

    .line 146
    return-void
.end method

.method public setWidthOfBitmap(I)V
    .locals 0
    .param p1, "widthOfBitmap"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/BitmapContents;->widthOfBitmap:I

    .line 106
    return-void
.end method
