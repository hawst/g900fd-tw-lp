.class public Lcom/samsung/thumbnail/customview/word/Paragraph;
.super Lcom/samsung/thumbnail/customview/CanvasElementPart;
.source "Paragraph.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

.field horizontalAlignment:Ljava/lang/String;

.field private isFooter:Z

.field private isHeader:Z

.field lineSpacing:F

.field public mAppendLine:Z

.field public mLastmeasure:I

.field mRunList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/Run;",
            ">;"
        }
    .end annotation
.end field

.field mShapeId:Ljava/lang/String;

.field marginBottom:I

.field marginLeft:I

.field marginRight:I

.field marginTop:I

.field private nonWrapTextWidth:F

.field private offset:I

.field private shrinkToFit:Z

.field private singleLineMaxFont:F

.field spacingBottom:I

.field spacingTop:I

.field private textActualIndent:I

.field private textHeightofPara:I

.field private textIndent:I

.field private textRotation:S

.field tp:Landroid/text/TextPaint;

.field verticalAlignment:Ljava/lang/String;

.field private x:I

.field private y:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/thumbnail/customview/word/Paragraph;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 4
    .param p1, "elementCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    const/4 v3, 0x5

    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 19
    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mAppendLine:Z

    .line 20
    iput v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    .line 40
    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->shrinkToFit:Z

    .line 41
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->nonWrapTextWidth:F

    .line 42
    iput v1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->singleLineMaxFont:F

    .line 44
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mShapeId:Ljava/lang/String;

    .line 51
    iput v2, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    iput v2, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 59
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mRunList:Ljava/util/ArrayList;

    .line 61
    const v0, 0x3f933333    # 1.15f

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->lineSpacing:F

    .line 62
    iput v3, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->spacingTop:I

    .line 63
    iput v3, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->spacingBottom:I

    .line 65
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->tp:Landroid/text/TextPaint;

    .line 67
    return-void
.end method

.method private DrawTextForRun(Lcom/samsung/thumbnail/customview/word/Run;Ljava/lang/String;Landroid/graphics/Canvas;Landroid/text/TextPaint;ZLandroid/graphics/Rect;)V
    .locals 24
    .param p1, "run"    # Lcom/samsung/thumbnail/customview/word/Run;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "tp"    # Landroid/text/TextPaint;
    .param p5, "newPara"    # Z
    .param p6, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 409
    const/16 v23, 0x0

    .line 410
    .local v23, "width":I
    invoke-virtual/range {p4 .. p4}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    float-to-int v11, v3

    .line 412
    .local v11, "ascent":I
    const/4 v12, 0x0

    .line 414
    .local v12, "canvasWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v3

    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 417
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v4

    move-object/from16 v0, p6

    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-double v6, v3

    sub-double/2addr v4, v6

    double-to-int v12, v4

    .line 425
    :goto_0
    const/16 v19, 0x0

    .line 427
    .local v19, "oneLine":Z
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 428
    neg-int v3, v11

    move-object/from16 v0, p6

    iget v4, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 430
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    .line 647
    :cond_1
    :goto_1
    return-void

    .line 420
    .end local v19    # "oneLine":Z
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v4

    move-object/from16 v0, p6

    iget v3, v0, Landroid/graphics/Rect;->right:I

    int-to-double v6, v3

    sub-double/2addr v4, v6

    move-object/from16 v0, p6

    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-double v6, v3

    sub-double/2addr v4, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getLeftIndent()I

    move-result v3

    int-to-double v6, v3

    sub-double/2addr v4, v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getRightIndent()I

    move-result v3

    int-to-double v6, v3

    sub-double/2addr v4, v6

    double-to-int v12, v4

    goto :goto_0

    .line 433
    .restart local v19    # "oneLine":Z
    :cond_3
    new-instance v2, Lcom/samsung/thumbnail/customview/word/LineBreaker;

    invoke-direct {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;-><init>()V

    .line 434
    .local v2, "breaker":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    .local v10, "arr$":[C
    array-length v0, v10

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    :goto_2
    move/from16 v0, v16

    if-ge v15, v0, :cond_4

    aget-char v13, v10, v15

    .line 435
    .local v13, "charChar":C
    const/16 v3, 0xb

    if-ne v13, v3, :cond_5

    .line 436
    const/16 p5, 0x1

    .line 437
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    int-to-float v3, v3

    neg-int v4, v11

    int-to-float v4, v4

    invoke-virtual/range {p4 .. p4}, Landroid/text/TextPaint;->descent()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/thumbnail/customview/word/Paragraph;->calculateLineSpacing(F)F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 439
    move-object/from16 v0, p2

    invoke-static {v0, v13}, Lcom/samsung/thumbnail/office/util/StringUtils;->remove(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object p2

    .line 446
    .end local v13    # "charChar":C
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mAppendLine:Z

    if-eqz v3, :cond_6

    if-nez p5, :cond_6

    .line 449
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    move-object/from16 v0, p6

    iget v4, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 450
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    sub-int v7, v12, v3

    move-object/from16 v3, p2

    move-object/from16 v8, p4

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->breakTextIntoLines(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/text/TextPaint;)I

    move-result v23

    .line 452
    const/16 v19, 0x1

    .line 462
    :goto_3
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v18

    .line 466
    .local v18, "lines":Ljava/util/List;, "Ljava/util/List<[I>;"
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_4
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v3

    if-ge v14, v3, :cond_1

    .line 469
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Canvas;->getHeight()I

    move-result v4

    move-object/from16 v0, p6

    iget v5, v0, Landroid/graphics/Rect;->top:I

    sget v6, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    add-int/2addr v5, v6

    sget v6, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_BOTTOM:I

    add-int/2addr v5, v6

    sub-int/2addr v4, v5

    if-le v3, v4, :cond_7

    .line 471
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    goto/16 :goto_1

    .line 434
    .end local v14    # "i":I
    .end local v18    # "lines":Ljava/util/List;, "Ljava/util/List<[I>;"
    .restart local v13    # "charChar":C
    :cond_5
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 455
    .end local v13    # "charChar":C
    :cond_6
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    .line 457
    move-object/from16 v0, p6

    iget v3, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 458
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-virtual {v2, v0, v12, v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->breakTextIntoLines(Ljava/lang/String;ILandroid/text/TextPaint;)I

    move-result v23

    .line 459
    const/16 v19, 0x1

    goto :goto_3

    .line 476
    .restart local v14    # "i":I
    .restart local v18    # "lines":Ljava/util/List;, "Ljava/util/List<[I>;"
    :cond_7
    move-object/from16 v0, v18

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, [I

    .line 478
    .local v20, "pair":[I
    const/4 v3, 0x0

    aget v3, v20, v3

    const/4 v4, 0x1

    aget v4, v20, v4

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    .line 482
    .local v22, "runStr":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v3

    if-eqz v3, :cond_11

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v3

    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 484
    invoke-virtual/range {p4 .. p4}, Landroid/text/TextPaint;->getTextAlign()Landroid/graphics/Paint$Align;

    move-result-object v3

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    if-ne v3, v4, :cond_f

    .line 485
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    invoke-virtual/range {p6 .. p6}, Landroid/graphics/Rect;->width()I

    move-result v4

    shr-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p4

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v5

    float-to-int v5, v5

    shr-int/lit8 v5, v5, 0x1

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, -0xf

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 518
    :cond_8
    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getVerticalalignment()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v3

    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUPERSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    if-ne v3, v4, :cond_15

    .line 519
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    int-to-float v3, v3

    invoke-virtual/range {p4 .. p4}, Landroid/text/TextPaint;->getTextSize()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 522
    :cond_9
    :goto_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    if-gtz v3, :cond_a

    .line 523
    move-object/from16 v0, p6

    iget v3, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 525
    :cond_a
    const/4 v3, 0x0

    aget v5, v20, v3

    const/4 v3, 0x1

    aget v3, v20, v3

    add-int/lit8 v6, v3, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getLeftIndent()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v7, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    int-to-float v8, v3

    move-object/from16 v3, p3

    move-object/from16 v4, p2

    move-object/from16 v9, p4

    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 529
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getVerticalalignment()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v3

    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUPERSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    if-ne v3, v4, :cond_16

    .line 530
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    int-to-float v3, v3

    invoke-virtual/range {p4 .. p4}, Landroid/text/TextPaint;->getTextSize()F

    move-result v4

    add-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 533
    :cond_b
    :goto_7
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    const/4 v4, 0x0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v5

    move-object/from16 v0, p4

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v4, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v4

    float-to-int v4, v4

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    .line 539
    if-eqz v19, :cond_1d

    .line 541
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    if-ge v3, v12, :cond_1c

    .line 546
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move-object/from16 v0, p6

    iget v4, v0, Landroid/graphics/Rect;->left:I

    if-ne v3, v4, :cond_c

    .line 547
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 548
    :cond_c
    const-string/jumbo v3, "\r"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v3

    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 550
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    int-to-float v3, v3

    neg-int v4, v11

    int-to-float v4, v4

    invoke-virtual/range {p4 .. p4}, Landroid/text/TextPaint;->descent()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/thumbnail/customview/word/Paragraph;->calculateLineSpacing(F)F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 554
    :cond_d
    const/4 v3, 0x1

    aget v3, v20, v3

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 557
    if-eqz p2, :cond_e

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_17

    .line 558
    :cond_e
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mAppendLine:Z

    goto/16 :goto_1

    .line 488
    :cond_f
    invoke-virtual/range {p4 .. p4}, Landroid/text/TextPaint;->getTextAlign()Landroid/graphics/Paint$Align;

    move-result-object v3

    sget-object v4, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    if-ne v3, v4, :cond_10

    .line 489
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    invoke-virtual/range {p6 .. p6}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p4

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v5

    sub-float/2addr v4, v5

    const/high16 v5, 0x41f00000    # 30.0f

    sub-float/2addr v4, v5

    float-to-int v4, v4

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    goto/16 :goto_5

    .line 491
    :cond_10
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    goto/16 :goto_5

    .line 494
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 496
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v3

    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 497
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "center"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 498
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move-object/from16 v0, p6

    iget v4, v0, Landroid/graphics/Rect;->right:I

    shr-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p4

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v5

    float-to-int v5, v5

    shr-int/lit8 v5, v5, 0x1

    sub-int/2addr v4, v5

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    goto/16 :goto_5

    .line 500
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "right"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 502
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move-object/from16 v0, p6

    iget v4, v0, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    move-object/from16 v0, p4

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v5

    sub-float/2addr v4, v5

    float-to-int v4, v4

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    goto/16 :goto_5

    .line 506
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v3

    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 507
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "center"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 508
    shr-int/lit8 v3, v12, 0x1

    move-object/from16 v0, p4

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v4

    float-to-int v4, v4

    shr-int/lit8 v4, v4, 0x1

    sub-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    goto/16 :goto_5

    .line 511
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "right"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 513
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    int-to-float v4, v12

    move-object/from16 v0, p4

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v5

    sub-float/2addr v4, v5

    float-to-int v4, v4

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    goto/16 :goto_5

    .line 520
    :cond_15
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getVerticalalignment()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v3

    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUBSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    if-ne v3, v4, :cond_9

    .line 521
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    invoke-virtual/range {p4 .. p4}, Landroid/text/TextPaint;->getTextSize()F

    move-result v4

    float-to-int v4, v4

    shr-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    goto/16 :goto_6

    .line 531
    :cond_16
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getVerticalalignment()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v3

    sget-object v4, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUBSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    if-ne v3, v4, :cond_b

    .line 532
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    invoke-virtual/range {p4 .. p4}, Landroid/text/TextPaint;->getTextSize()F

    move-result v4

    float-to-int v4, v4

    shr-int/lit8 v4, v4, 0x1

    sub-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    goto/16 :goto_7

    .line 562
    :cond_17
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mAppendLine:Z

    .line 563
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    sub-int v7, v12, v3

    move-object/from16 v3, p2

    move-object/from16 v8, p4

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->breakTextIntoLines(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/text/TextPaint;)I

    move-result v23

    .line 566
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    move-object/from16 v0, p6

    iget v4, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 568
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v17

    .line 569
    .local v17, "line":Ljava/util/List;, "Ljava/util/List<[I>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1b

    .line 570
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, [I

    .line 571
    .local v21, "pair1":[I
    const/4 v3, 0x0

    aget v3, v21, v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_18

    const/4 v3, 0x1

    aget v3, v21, v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_19

    .line 575
    :cond_18
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    .line 577
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 578
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    int-to-float v3, v3

    neg-int v4, v11

    int-to-float v4, v4

    invoke-virtual/range {p4 .. p4}, Landroid/text/TextPaint;->descent()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/thumbnail/customview/word/Paragraph;->calculateLineSpacing(F)F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 580
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-virtual {v2, v0, v12, v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->breakTextIntoLines(Ljava/lang/String;ILandroid/text/TextPaint;)I

    move-result v23

    .line 582
    const/16 v19, 0x0

    .line 583
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v18

    .line 600
    .end local v21    # "pair1":[I
    :cond_19
    :goto_8
    const/4 v14, -0x1

    .line 466
    .end local v17    # "line":Ljava/util/List;, "Ljava/util/List<[I>;"
    :cond_1a
    :goto_9
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_4

    .line 590
    .restart local v17    # "line":Ljava/util/List;, "Ljava/util/List<[I>;"
    :cond_1b
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    .line 592
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 593
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    int-to-float v3, v3

    neg-int v4, v11

    int-to-float v4, v4

    invoke-virtual/range {p4 .. p4}, Landroid/text/TextPaint;->descent()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/thumbnail/customview/word/Paragraph;->calculateLineSpacing(F)F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 595
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-virtual {v2, v0, v12, v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->breakTextIntoLines(Ljava/lang/String;ILandroid/text/TextPaint;)I

    move-result v23

    .line 597
    const/16 v19, 0x0

    .line 598
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v18

    goto :goto_8

    .line 604
    .end local v17    # "line":Ljava/util/List;, "Ljava/util/List<[I>;"
    :cond_1c
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    .line 605
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mAppendLine:Z

    .line 607
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 608
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    int-to-float v3, v3

    neg-int v4, v11

    int-to-float v4, v4

    invoke-virtual/range {p4 .. p4}, Landroid/text/TextPaint;->descent()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/thumbnail/customview/word/Paragraph;->calculateLineSpacing(F)F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 611
    const/4 v3, 0x1

    aget v3, v20, v3

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 614
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-virtual {v2, v0, v12, v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->breakTextIntoLines(Ljava/lang/String;ILandroid/text/TextPaint;)I

    move-result v23

    .line 615
    const/16 v19, 0x0

    .line 616
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v18

    .line 617
    const/4 v14, -0x1

    .line 618
    goto :goto_9

    .line 626
    :cond_1d
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v14, v3, :cond_1e

    .line 627
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mLastmeasure:I

    if-ge v3, v12, :cond_1e

    .line 628
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mAppendLine:Z

    .line 629
    const-string/jumbo v3, "\r"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v3

    sget-object v4, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v3, v4}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 631
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    int-to-float v3, v3

    neg-int v4, v11

    int-to-float v4, v4

    invoke-virtual/range {p4 .. p4}, Landroid/text/TextPaint;->descent()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/thumbnail/customview/word/Paragraph;->calculateLineSpacing(F)F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    goto/16 :goto_1

    .line 638
    :cond_1e
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    int-to-float v3, v3

    neg-int v4, v11

    int-to-float v4, v4

    invoke-virtual/range {p4 .. p4}, Landroid/text/TextPaint;->descent()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/thumbnail/customview/word/Paragraph;->calculateLineSpacing(F)F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 640
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    int-to-double v4, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v6

    sget v3, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_BOTTOM:I

    int-to-double v8, v3

    sub-double/2addr v6, v8

    cmpl-double v3, v4, v6

    if-ltz v3, :cond_1a

    .line 643
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    goto/16 :goto_1
.end method

.method private DrawTextForRun(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V
    .locals 6
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "rect"    # Landroid/graphics/Rect;
    .param p4, "paraWidth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/Run;",
            ">;",
            "Landroid/graphics/Canvas;",
            "Landroid/graphics/Rect;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 651
    .local p1, "runArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/thumbnail/customview/word/Paragraph;->DrawTextForRun(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Rect;IZ)V

    .line 652
    return-void
.end method

.method private DrawTextForRun(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Rect;IZ)V
    .locals 30
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "rect"    # Landroid/graphics/Rect;
    .param p4, "paraWidth"    # I
    .param p5, "isHidden"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/Run;",
            ">;",
            "Landroid/graphics/Canvas;",
            "Landroid/graphics/Rect;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 656
    .local p1, "runArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    new-instance v4, Lcom/samsung/thumbnail/customview/word/LineBreaker;

    invoke-direct {v4}, Lcom/samsung/thumbnail/customview/word/LineBreaker;-><init>()V

    .line 657
    .local v4, "breaker":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, -0x1

    move-object/from16 v5, p1

    move/from16 v9, p4

    invoke-virtual/range {v4 .. v9}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->breakText(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;II)I

    .line 659
    invoke-virtual {v4}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v18

    .line 660
    .local v18, "lines":Ljava/util/List;, "Ljava/util/List<[I>;"
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 662
    .local v16, "lineElement":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    const/16 v26, 0x0

    .line 663
    .local v26, "runArrayIndex":I
    const/4 v11, 0x0

    .line 664
    .local v11, "currentPendingRunIndex":I
    const/16 v22, 0x0

    .line 666
    .local v22, "pendingline":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 667
    .local v14, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/Run;>;"
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v5

    if-ge v12, v5, :cond_5

    .line 669
    move-object/from16 v0, v18

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [I

    .line 671
    .local v17, "lineLenArray":[I
    if-eqz v22, :cond_4

    .line 672
    const-string/jumbo v24, ""

    .line 673
    .local v24, "remainingText":Ljava/lang/String;
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x0

    aget v6, v17, v6

    if-ge v5, v6, :cond_2

    .line 674
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getStartIndex()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v24

    .line 681
    :cond_0
    :goto_1
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x1

    aget v6, v17, v6

    const/4 v7, 0x0

    aget v7, v17, v7

    sub-int/2addr v6, v7

    if-le v5, v6, :cond_3

    .line 682
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getStartIndex()I

    move-result v5

    const/4 v6, 0x1

    aget v6, v17, v6

    const/4 v7, 0x0

    aget v7, v17, v7

    sub-int/2addr v6, v7

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setEndIndex(I)V

    .line 684
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getStartIndex()I

    move-result v5

    const/4 v6, 0x1

    aget v6, v17, v6

    const/4 v7, 0x0

    aget v7, v17, v7

    sub-int/2addr v6, v7

    add-int/lit8 v6, v6, 0x1

    add-int v11, v5, v6

    .line 686
    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 688
    new-instance v21, Lcom/samsung/thumbnail/customview/word/ParaLine;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 689
    .local v21, "penLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    .line 690
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setRun(Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 691
    add-int/lit8 v5, v12, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 692
    const/4 v5, 0x1

    aget v5, v17, v5

    sub-int v5, v5, v26

    add-int v26, v26, v5

    .line 693
    move-object/from16 v22, v21

    .line 667
    .end local v21    # "penLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .end local v24    # "remainingText":Ljava/lang/String;
    :cond_1
    :goto_2
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 677
    .restart local v24    # "remainingText":Ljava/lang/String;
    :cond_2
    if-ltz v11, :cond_0

    .line 678
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v24

    goto :goto_1

    .line 696
    :cond_3
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v11

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setEndIndex(I)V

    .line 698
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v5

    add-int v26, v26, v5

    .line 699
    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 702
    .end local v24    # "remainingText":Ljava/lang/String;
    :cond_4
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_a

    .line 753
    .end local v17    # "lineLenArray":[I
    :cond_5
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_6

    .line 754
    sget v5, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 756
    :cond_6
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_7

    .line 757
    sget v5, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 759
    :cond_7
    const/4 v12, 0x0

    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v5

    if-ge v12, v5, :cond_11

    .line 760
    const/16 v19, 0x0

    .line 762
    .local v19, "maxFontSize":F
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 763
    .local v10, "currentLineRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .line 764
    .local v13, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    :cond_8
    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_f

    .line 765
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/thumbnail/customview/word/ParaLine;

    .line 766
    .local v20, "paraLineItem":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getLineNumber()I

    move-result v5

    if-ne v5, v12, :cond_8

    .line 767
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v5

    invoke-virtual {v5}, Landroid/text/TextPaint;->getTextSize()F

    move-result v5

    cmpl-float v5, v19, v5

    if-eqz v5, :cond_9

    .line 772
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v5

    invoke-virtual {v5}, Landroid/text/TextPaint;->getTextSize()F

    move-result v19

    .line 775
    :cond_9
    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 704
    .end local v10    # "currentLineRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .end local v13    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .end local v19    # "maxFontSize":F
    .end local v20    # "paraLineItem":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .restart local v17    # "lineLenArray":[I
    :cond_a
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/samsung/thumbnail/customview/word/Run;

    .line 706
    .local v25, "run":Lcom/samsung/thumbnail/customview/word/Run;
    new-instance v15, Lcom/samsung/thumbnail/customview/word/ParaLine;

    invoke-direct {v15}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 707
    .local v15, "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int v5, v5, v26

    add-int/lit8 v5, v5, -0x1

    const/4 v6, 0x1

    aget v6, v17, v6

    if-ge v5, v6, :cond_b

    .line 708
    const/4 v5, 0x0

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    .line 709
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setEndIndex(I)V

    .line 710
    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setRun(Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 711
    invoke-virtual {v15, v12}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 712
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int v26, v26, v5

    .line 713
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 714
    const/4 v11, 0x0

    .line 715
    const/16 v22, 0x0

    .line 748
    :goto_5
    const/4 v5, 0x1

    aget v5, v17, v5

    move/from16 v0, v26

    if-eq v0, v5, :cond_1

    .line 749
    add-int/lit8 v12, v12, -0x1

    goto/16 :goto_2

    .line 716
    :cond_b
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int v5, v5, v26

    add-int/lit8 v5, v5, -0x1

    const/4 v6, 0x1

    aget v6, v17, v6

    if-ne v5, v6, :cond_c

    .line 717
    const/4 v5, 0x0

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    .line 718
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setEndIndex(I)V

    .line 719
    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setRun(Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 720
    invoke-virtual {v15, v12}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 721
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    add-int v26, v26, v5

    .line 722
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 723
    const/4 v11, 0x0

    .line 724
    const/16 v22, 0x0

    goto :goto_5

    .line 726
    :cond_c
    const/4 v5, 0x0

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    .line 727
    const/4 v5, 0x1

    aget v5, v17, v5

    sub-int v5, v5, v26

    if-gez v5, :cond_d

    .line 728
    add-int/lit8 v12, v12, 0x1

    .line 729
    move-object/from16 v0, v18

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "lineLenArray":[I
    check-cast v17, [I

    .line 731
    .restart local v17    # "lineLenArray":[I
    :cond_d
    const/4 v5, 0x1

    aget v5, v17, v5

    sub-int v5, v5, v26

    invoke-virtual {v15, v5}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setEndIndex(I)V

    .line 732
    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setRun(Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 733
    invoke-virtual {v15, v12}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 734
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 736
    new-instance v21, Lcom/samsung/thumbnail/customview/word/ParaLine;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/thumbnail/customview/word/ParaLine;-><init>()V

    .line 737
    .restart local v21    # "penLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    const/4 v5, 0x1

    aget v5, v17, v5

    sub-int v5, v5, v26

    if-gez v5, :cond_e

    .line 738
    const/4 v5, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    .line 741
    :goto_6
    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setRun(Lcom/samsung/thumbnail/customview/word/Run;)V

    .line 742
    add-int/lit8 v5, v12, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setLineNumber(I)V

    .line 743
    const/4 v5, 0x1

    aget v5, v17, v5

    sub-int v11, v5, v26

    .line 744
    const/4 v5, 0x1

    aget v5, v17, v5

    sub-int v5, v5, v26

    add-int v26, v26, v5

    .line 745
    move-object/from16 v22, v21

    goto/16 :goto_5

    .line 740
    :cond_e
    const/4 v5, 0x1

    aget v5, v17, v5

    sub-int v5, v5, v26

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lcom/samsung/thumbnail/customview/word/ParaLine;->setStartIndex(I)V

    goto :goto_6

    .line 782
    .end local v15    # "line":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .end local v17    # "lineLenArray":[I
    .end local v21    # "penLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .end local v25    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    .restart local v10    # "currentLineRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v13    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v19    # "maxFontSize":F
    :cond_f
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    int-to-float v5, v5

    add-float v5, v5, v19

    float-to-int v5, v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 783
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    .line 784
    .local v23, "prevXval":I
    move-object/from16 v0, p3

    iget v5, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 785
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-direct {v0, v10, v1, v2, v3}, Lcom/samsung/thumbnail/customview/word/Paragraph;->drawCurrentLine(Ljava/util/ArrayList;Landroid/graphics/Canvas;IZ)V

    .line 786
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 790
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v5

    if-eqz v5, :cond_12

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v5

    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v5

    sget-object v6, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v5, v6}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 793
    :cond_10
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    int-to-double v6, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewHeight()D

    move-result-wide v8

    sget v5, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_BOTTOM:I

    int-to-double v0, v5

    move-wide/from16 v28, v0

    sub-double v8, v8, v28

    cmpl-double v5, v6, v8

    if-ltz v5, :cond_12

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->isFooter()Z

    move-result v5

    if-nez v5, :cond_12

    .line 796
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->stopParsing:Z

    .line 806
    .end local v10    # "currentLineRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .end local v13    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .end local v19    # "maxFontSize":F
    .end local v23    # "prevXval":I
    :cond_11
    return-void

    .line 803
    .restart local v10    # "currentLineRun":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v13    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v19    # "maxFontSize":F
    .restart local v23    # "prevXval":I
    :cond_12
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    int-to-float v5, v5

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->calculateLineSpacing(F)F

    move-result v6

    add-float/2addr v5, v6

    float-to-int v5, v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 804
    sget v5, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 759
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_3
.end method

.method private DrawTextForRunSingleLine(Landroid/graphics/Rect;Landroid/graphics/Rect;Ljava/util/ArrayList;Landroid/graphics/Canvas;Z)V
    .locals 27
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "availableRect"    # Landroid/graphics/Rect;
    .param p4, "canvas"    # Landroid/graphics/Canvas;
    .param p5, "isHidden"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Rect;",
            "Landroid/graphics/Rect;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/Run;",
            ">;",
            "Landroid/graphics/Canvas;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 818
    .local p3, "runArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    if-eqz p3, :cond_0

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v23

    if-nez v23, :cond_1

    .line 1118
    :cond_0
    :goto_0
    return-void

    .line 822
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    move/from16 v23, v0

    const/16 v24, -0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_2

    .line 823
    sget v23, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 825
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    const/16 v24, -0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_3

    .line 826
    sget v23, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 828
    :cond_3
    const/16 v21, 0x0

    .line 830
    .local v21, "totalCharLength":F
    const/4 v15, 0x0

    .line 831
    .local v15, "maxFontSize":F
    if-eqz p5, :cond_a

    .line 833
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .line 834
    .local v22, "widthIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/Run;>;"
    :cond_4
    :goto_1
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_8

    .line 836
    :try_start_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/thumbnail/customview/word/Run;

    .line 838
    .local v10, "currentRun":Lcom/samsung/thumbnail/customview/word/Run;
    float-to-int v0, v15

    move/from16 v23, v0

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/text/TextPaint;->getTextSize()F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_5

    .line 840
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/text/TextPaint;->getTextSize()F

    move-result v15

    .line 848
    :cond_5
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v23

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x0

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    invoke-static/range {v23 .. v26}, Lcom/samsung/thumbnail/customview/word/Run;->getTextWidths(Landroid/text/TextPaint;Ljava/lang/String;II)[F

    move-result-object v9

    .line 852
    .local v9, "currentLineWidthArray":[F
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    array-length v0, v9

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v13, v0, :cond_6

    .line 853
    aget v23, v9, v13

    add-float v21, v21, v23

    .line 852
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 855
    :cond_6
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setNonWrapTextWidth(F)V

    .line 861
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getVerticalalignment()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v23

    sget-object v24, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUBSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_7

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getVerticalalignment()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v23

    sget-object v24, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUPERSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_4

    .line 863
    :cond_7
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F

    move-result v23

    const/high16 v24, 0x40400000    # 3.0f

    mul-float v23, v23, v24

    const/high16 v24, 0x40800000    # 4.0f

    div-float v23, v23, v24

    add-float v15, v15, v23

    .line 864
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setSingleLineMaxFont(F)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 871
    .end local v9    # "currentLineWidthArray":[F
    .end local v10    # "currentRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v13    # "i":I
    :catch_0
    move-exception v11

    .line 872
    .local v11, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string/jumbo v23, "DocumentService"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v25, "Exception: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v11}, Ljava/lang/ArrayIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 876
    .end local v11    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setSingleLineMaxFont(F)V

    .line 879
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getSingleLineMaxFont()F

    move-result v24

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 1116
    :cond_9
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getSingleLineMaxFont()F

    move-result v24

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->calculateLineSpacing(F)F

    move-result v24

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 1117
    sget v23, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    goto/16 :goto_0

    .line 881
    .end local v22    # "widthIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/Run;>;"
    :cond_a
    const/4 v5, 0x1

    .line 882
    .local v5, "ALIGN_CENTER":I
    const/4 v7, 0x2

    .line 883
    .local v7, "ALIGN_RIGHT":I
    const/4 v6, 0x3

    .line 884
    .local v6, "ALIGN_LEFT":I
    const/4 v8, 0x3

    .line 886
    .local v8, "alignment":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v23

    if-eqz v23, :cond_b

    .line 887
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v23

    const-string/jumbo v24, "center"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_f

    .line 888
    const/4 v8, 0x1

    .line 894
    :cond_b
    :goto_4
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .line 895
    .restart local v22    # "widthIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/Run;>;"
    :cond_c
    :goto_5
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_11

    .line 897
    :try_start_1
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/thumbnail/customview/word/Run;

    .line 899
    .restart local v10    # "currentRun":Lcom/samsung/thumbnail/customview/word/Run;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getShrinkToFit()Z

    move-result v23

    if-eqz v23, :cond_d

    .line 900
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v24, v0

    sub-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getNonWrapTextWidth()F

    move-result v24

    cmpg-float v23, v23, v24

    if-gez v23, :cond_d

    .line 901
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/text/TextPaint;->getTextSize()F

    move-result v12

    .line 903
    .local v12, "fontSize":F
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v24, v0

    sub-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v12

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getNonWrapTextWidth()F

    move-result v24

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getActualTextIndent()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    add-float v24, v24, v25

    div-float v23, v23, v24

    move/from16 v0, v23

    invoke-virtual {v10, v0}, Lcom/samsung/thumbnail/customview/word/Run;->setTextSize(F)V

    .line 907
    float-to-int v0, v15

    move/from16 v23, v0

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/text/TextPaint;->getTextSize()F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_d

    .line 909
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/text/TextPaint;->getTextSize()F

    move-result v15

    .line 911
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setSingleLineMaxFont(F)V

    .line 918
    .end local v12    # "fontSize":F
    :cond_d
    const/16 v23, 0x2

    move/from16 v0, v23

    if-eq v8, v0, :cond_e

    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v8, v0, :cond_c

    .line 925
    :cond_e
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v23

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x0

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    invoke-static/range {v23 .. v26}, Lcom/samsung/thumbnail/customview/word/Run;->getTextWidths(Landroid/text/TextPaint;Ljava/lang/String;II)[F

    move-result-object v9

    .line 930
    .restart local v9    # "currentLineWidthArray":[F
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_6
    array-length v0, v9

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v13, v0, :cond_10

    .line 931
    aget v23, v9, v13
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    add-float v21, v21, v23

    .line 930
    add-int/lit8 v13, v13, 0x1

    goto :goto_6

    .line 889
    .end local v9    # "currentLineWidthArray":[F
    .end local v10    # "currentRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v13    # "i":I
    .end local v22    # "widthIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/Run;>;"
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v23

    const-string/jumbo v24, "right"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 890
    const/4 v8, 0x2

    goto/16 :goto_4

    .line 933
    .restart local v9    # "currentLineWidthArray":[F
    .restart local v10    # "currentRun":Lcom/samsung/thumbnail/customview/word/Run;
    .restart local v13    # "i":I
    .restart local v22    # "widthIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/Run;>;"
    :cond_10
    :try_start_2
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->setNonWrapTextWidth(F)V
    :try_end_2
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_5

    .line 935
    .end local v9    # "currentLineWidthArray":[F
    .end local v10    # "currentRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v13    # "i":I
    :catch_1
    move-exception v11

    .line 936
    .restart local v11    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string/jumbo v23, "DocumentService"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v25, "Exception: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v11}, Ljava/lang/ArrayIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 944
    .end local v11    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getSingleLineMaxFont()F

    move-result v23

    const/16 v24, 0x0

    cmpl-float v23, v23, v24

    if-eqz v23, :cond_12

    .line 945
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getSingleLineMaxFont()F

    move-result v24

    const/high16 v25, 0x40400000    # 3.0f

    mul-float v24, v24, v25

    const/high16 v25, 0x40800000    # 4.0f

    div-float v24, v24, v25

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 948
    :cond_12
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .line 950
    const/16 v17, 0x0

    .line 951
    .local v17, "startDisplayText":Z
    const/16 v19, 0x0

    .line 952
    .local v19, "stopDisplayText":Z
    const/16 v23, 0x3

    move/from16 v0, v23

    if-ne v8, v0, :cond_18

    .line 953
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getActualTextIndent()I

    move-result v24

    add-int v23, v23, v24

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 954
    const/16 v17, 0x1

    .line 962
    :goto_7
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 964
    .local v14, "initialXValue":I
    :cond_13
    :goto_8
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_17

    .line 966
    :try_start_3
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/thumbnail/customview/word/Run;

    .line 972
    .restart local v10    # "currentRun":Lcom/samsung/thumbnail/customview/word/Run;
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v23

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x0

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    invoke-static/range {v23 .. v26}, Lcom/samsung/thumbnail/customview/word/Run;->getTextWidths(Landroid/text/TextPaint;Ljava/lang/String;II)[F

    move-result-object v9

    .line 976
    .restart local v9    # "currentLineWidthArray":[F
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v20

    .line 977
    .local v20, "textInLine":Ljava/lang/String;
    const/4 v13, 0x0

    .line 979
    .restart local v13    # "i":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 981
    const/4 v13, 0x0

    :goto_9
    array-length v0, v9

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v13, v0, :cond_14

    .line 982
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    aget v24, v9, v13

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 984
    const/16 v23, 0x3

    move/from16 v0, v23

    if-ne v8, v0, :cond_1b

    .line 986
    if-eqz p2, :cond_27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_27

    .line 988
    if-lez v13, :cond_1a

    .line 989
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v1, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    .line 996
    :goto_a
    const/16 v19, 0x1

    .line 1080
    :cond_14
    :goto_b
    if-eqz v17, :cond_16

    if-eqz v20, :cond_16

    .line 1083
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    move/from16 v16, v0

    .line 1085
    .local v16, "modifiedY":I
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getVerticalalignment()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v23

    sget-object v24, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUBSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_28

    .line 1086
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F

    move-result v23

    const/high16 v24, 0x40400000    # 3.0f

    mul-float v23, v23, v24

    const/high16 v24, 0x40800000    # 4.0f

    div-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    add-int v16, v16, v23

    .line 1092
    :cond_15
    :goto_c
    int-to-float v0, v14

    move/from16 v23, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v24, v0

    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v25

    move-object/from16 v0, p4

    move-object/from16 v1, v20

    move/from16 v2, v23

    move/from16 v3, v24

    move-object/from16 v4, v25

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    :try_end_3
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_2

    .line 1096
    .end local v16    # "modifiedY":I
    :cond_16
    if-eqz v19, :cond_13

    .line 1109
    .end local v9    # "currentLineWidthArray":[F
    .end local v10    # "currentRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v13    # "i":I
    .end local v20    # "textInLine":Ljava/lang/String;
    :cond_17
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getSingleLineMaxFont()F

    move-result v23

    const/16 v24, 0x0

    cmpl-float v23, v23, v24

    if-eqz v23, :cond_9

    .line 1110
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getSingleLineMaxFont()F

    move-result v24

    const/high16 v25, 0x40800000    # 4.0f

    div-float v24, v24, v25

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    goto/16 :goto_3

    .line 955
    .end local v14    # "initialXValue":I
    :cond_18
    const/16 v23, 0x2

    move/from16 v0, v23

    if-ne v8, v0, :cond_19

    .line 956
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getNonWrapTextWidth()F

    move-result v24

    sub-float v23, v23, v24

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getActualTextIndent()I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    sub-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    goto/16 :goto_7

    .line 959
    :cond_19
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getNonWrapTextWidth()F

    move-result v24

    sub-float v23, v23, v24

    const/high16 v24, 0x40000000    # 2.0f

    div-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    goto/16 :goto_7

    .line 992
    .restart local v9    # "currentLineWidthArray":[F
    .restart local v10    # "currentRun":Lcom/samsung/thumbnail/customview/word/Run;
    .restart local v13    # "i":I
    .restart local v14    # "initialXValue":I
    .restart local v20    # "textInLine":Ljava/lang/String;
    :cond_1a
    const/16 v20, 0x0

    goto/16 :goto_a

    .line 999
    :cond_1b
    const/16 v23, 0x2

    move/from16 v0, v23

    if-ne v8, v0, :cond_21

    .line 1001
    if-eqz p2, :cond_1e

    :try_start_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_1e

    if-nez v17, :cond_1e

    .line 1003
    const/16 v17, 0x1

    .line 1004
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    aget v24, v9, v13

    sub-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v14, v0

    .line 1005
    move/from16 v18, v13

    .line 1006
    .local v18, "startIVal":I
    :cond_1c
    add-int/lit8 v23, v13, 0x1

    array-length v0, v9

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_1d

    .line 1007
    add-int/lit8 v13, v13, 0x1

    .line 1008
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    aget v24, v9, v13

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 1009
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_1c

    .line 1014
    :cond_1d
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v23

    add-int/lit8 v24, v13, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v18

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    .line 1016
    goto/16 :goto_b

    .line 1018
    .end local v18    # "startIVal":I
    :cond_1e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_20

    const/16 v23, 0x1

    move/from16 v0, v17

    move/from16 v1, v23

    if-ne v0, v1, :cond_20

    .line 1019
    const/16 v19, 0x1

    .line 1020
    if-lez v13, :cond_1f

    .line 1021
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v1, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    goto/16 :goto_b

    .line 1024
    :cond_1f
    const/16 v20, 0x0

    .line 1028
    goto/16 :goto_b

    .line 1029
    :cond_20
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_27

    if-nez v17, :cond_27

    .line 1031
    const/16 v19, 0x1

    .line 1032
    const/16 v20, 0x0

    .line 1033
    goto/16 :goto_b

    .line 1038
    :cond_21
    if-eqz p2, :cond_24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-le v0, v1, :cond_24

    if-nez v17, :cond_24

    .line 1040
    const/16 v17, 0x1

    .line 1041
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    aget v24, v9, v13

    sub-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v14, v0

    .line 1042
    move/from16 v18, v13

    .line 1043
    .restart local v18    # "startIVal":I
    :cond_22
    add-int/lit8 v23, v13, 0x1

    array-length v0, v9

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_23

    .line 1044
    add-int/lit8 v13, v13, 0x1

    .line 1045
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    aget v24, v9, v13

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 1047
    if-eqz p2, :cond_22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_22

    .line 1053
    :cond_23
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v23

    add-int/lit8 v24, v13, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v18

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    .line 1055
    goto/16 :goto_b

    .line 1058
    .end local v18    # "startIVal":I
    :cond_24
    if-eqz p2, :cond_26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_26

    const/16 v23, 0x1

    move/from16 v0, v17

    move/from16 v1, v23

    if-ne v0, v1, :cond_26

    .line 1060
    const/16 v19, 0x1

    .line 1061
    if-lez v13, :cond_25

    .line 1062
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v0, v1, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    goto/16 :goto_b

    .line 1065
    :cond_25
    const/16 v20, 0x0

    .line 1069
    goto/16 :goto_b

    .line 1071
    :cond_26
    if-eqz p2, :cond_27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    move/from16 v23, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_27

    if-nez v17, :cond_27

    .line 1073
    const/16 v19, 0x1

    .line 1074
    const/16 v20, 0x0

    .line 1075
    goto/16 :goto_b

    .line 981
    :cond_27
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_9

    .line 1087
    .restart local v16    # "modifiedY":I
    :cond_28
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getVerticalalignment()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v23

    sget-object v24, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUPERSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_15

    .line 1088
    invoke-virtual {v10}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F
    :try_end_4
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_2

    move-result v23

    const/high16 v24, 0x40400000    # 3.0f

    mul-float v23, v23, v24

    const/high16 v24, 0x40000000    # 2.0f

    div-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    sub-int v16, v16, v23

    goto/16 :goto_c

    .line 1099
    .end local v9    # "currentLineWidthArray":[F
    .end local v10    # "currentRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v13    # "i":I
    .end local v16    # "modifiedY":I
    .end local v20    # "textInLine":Ljava/lang/String;
    :catch_2
    move-exception v11

    .line 1100
    .restart local v11    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string/jumbo v23, "DocumentService"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v25, "Exception: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v11}, Ljava/lang/ArrayIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8
.end method

.method private calculateLineSpacing(F)F
    .locals 6
    .param p1, "fontSize"    # F

    .prologue
    .line 1473
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getLineSpacing()F

    move-result v0

    float-to-double v0, v0

    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, p1, v3

    float-to-double v4, v3

    invoke-virtual {v2, v4, v5}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocWidth(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private drawCurrentLine(Ljava/util/ArrayList;Landroid/graphics/Canvas;IZ)V
    .locals 17
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "paraWidth"    # I
    .param p4, "isHidden"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/ParaLine;",
            ">;",
            "Landroid/graphics/Canvas;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 1126
    .local p1, "currentLineArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "center"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "right"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1129
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 1130
    .local v11, "widthIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    const/4 v10, 0x0

    .line 1131
    .local v10, "totalCharLength":F
    :cond_1
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1133
    :try_start_0
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/customview/word/ParaLine;

    .line 1134
    .local v1, "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v3

    .line 1146
    .local v3, "currentRun":Lcom/samsung/thumbnail/customview/word/Run;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v12

    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getStartIndex()I

    move-result v14

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getEndIndex()I

    move-result v15

    invoke-static {v12, v13, v14, v15}, Lcom/samsung/thumbnail/customview/word/Run;->getTextWidths(Landroid/text/TextPaint;Ljava/lang/String;II)[F

    move-result-object v2

    .line 1152
    .local v2, "currentLineWidthArray":[F
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    array-length v12, v2

    if-ge v6, v12, :cond_1

    .line 1153
    aget v12, v2, v6
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    add-float/2addr v10, v12

    .line 1152
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1156
    .end local v1    # "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .end local v2    # "currentLineWidthArray":[F
    .end local v3    # "currentRun":Lcom/samsung/thumbnail/customview/word/Run;
    .end local v6    # "i":I
    :catch_0
    move-exception v4

    .line 1157
    .local v4, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string/jumbo v12, "DocumentService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Exception: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v4}, Ljava/lang/ArrayIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1160
    .end local v4    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_2
    const/4 v12, -0x1

    move/from16 v0, p3

    if-eq v0, v12, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "center"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1162
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    int-to-float v12, v12

    move/from16 v0, p3

    int-to-float v13, v0

    sub-float/2addr v13, v10

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    add-float/2addr v12, v13

    float-to-int v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 1169
    .end local v10    # "totalCharLength":F
    .end local v11    # "widthIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    :cond_3
    :goto_2
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 1170
    .local v7, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    const/4 v5, 0x1

    .line 1171
    .local v5, "firstRun":Z
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1173
    :try_start_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/customview/word/ParaLine;

    .line 1174
    .restart local v1    # "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getStartIndex()I

    move-result v13

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getEndIndex()I

    move-result v14

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 1179
    .local v9, "textInLine":Ljava/lang/String;
    if-eqz v5, :cond_4

    .line 1180
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 1181
    const/4 v5, 0x0

    .line 1183
    :cond_4
    if-nez p4, :cond_6

    .line 1185
    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 1187
    .local v8, "modifiedY":I
    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/Run;->getVerticalalignment()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v12

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUBSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    if-ne v12, v13, :cond_8

    .line 1188
    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F

    move-result v12

    const/high16 v13, 0x40400000    # 3.0f

    mul-float/2addr v12, v13

    const/high16 v13, 0x40800000    # 4.0f

    div-float/2addr v12, v13

    float-to-int v12, v12

    add-int/2addr v8, v12

    .line 1194
    :cond_5
    :goto_4
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    int-to-float v12, v12

    int-to-float v13, v8

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v12, v13, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1198
    .end local v8    # "modifiedY":I
    :cond_6
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    int-to-float v12, v12

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v13

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getStartIndex()I

    move-result v15

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getEndIndex()I

    move-result v16

    invoke-virtual/range {v13 .. v16}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v13

    add-float/2addr v12, v13

    float-to-int v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_3

    .line 1204
    .end local v1    # "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .end local v9    # "textInLine":Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 1205
    .restart local v4    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string/jumbo v12, "DocumentService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Exception: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v4}, Ljava/lang/ArrayIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1163
    .end local v4    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    .end local v5    # "firstRun":Z
    .end local v7    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v10    # "totalCharLength":F
    .restart local v11    # "widthIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    :cond_7
    const/4 v12, -0x1

    move/from16 v0, p3

    if-eq v0, v12, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v12

    const-string/jumbo v13, "right"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1165
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    int-to-float v12, v12

    move/from16 v0, p3

    int-to-float v13, v0

    sub-float/2addr v13, v10

    add-float/2addr v12, v13

    float-to-int v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    goto/16 :goto_2

    .line 1189
    .end local v10    # "totalCharLength":F
    .end local v11    # "widthIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v1    # "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .restart local v5    # "firstRun":Z
    .restart local v7    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/word/ParaLine;>;"
    .restart local v8    # "modifiedY":I
    .restart local v9    # "textInLine":Ljava/lang/String;
    :cond_8
    :try_start_2
    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/Run;->getVerticalalignment()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    move-result-object v12

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;->SUPERSCRIPT:Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocProperty$EVerticalAlign;

    if-ne v12, v13, :cond_5

    .line 1190
    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/ParaLine;->getRun()Lcom/samsung/thumbnail/customview/word/Run;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/customview/word/Run;->getTextSize()F
    :try_end_2
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v12

    const/high16 v13, 0x40400000    # 3.0f

    mul-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    float-to-int v12, v12

    sub-int/2addr v8, v12

    goto/16 :goto_4

    .line 1206
    .end local v1    # "currentLine":Lcom/samsung/thumbnail/customview/word/ParaLine;
    .end local v8    # "modifiedY":I
    .end local v9    # "textInLine":Ljava/lang/String;
    :catch_2
    move-exception v4

    .line 1207
    .local v4, "e":Ljava/lang/StringIndexOutOfBoundsException;
    const-string/jumbo v12, "DocumentService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "Exception: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v4}, Ljava/lang/StringIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1210
    .end local v4    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_9
    return-void
.end method


# virtual methods
.method public AddRun(Lcom/samsung/thumbnail/customview/word/Run;)V
    .locals 1
    .param p1, "r"    # Lcom/samsung/thumbnail/customview/word/Run;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mRunList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "compObj"    # Ljava/lang/Object;

    .prologue
    .line 1440
    if-nez p1, :cond_1

    .line 1441
    const/4 v0, 0x0

    .line 1460
    :cond_0
    :goto_0
    return v0

    .line 1442
    :cond_1
    const/4 v0, 0x0

    .line 1444
    .local v0, "isEqual":Z
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 1445
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 1447
    check-cast v1, Lcom/samsung/thumbnail/customview/word/Paragraph;

    .line 1448
    .local v1, "para":Lcom/samsung/thumbnail/customview/word/Paragraph;
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginLeft()I

    move-result v2

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginLeft()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginRight()I

    move-result v2

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginRight()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginTop()I

    move-result v2

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginTop()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginBottom()I

    move-result v2

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getMarginBottom()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v2

    sget-object v3, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getSlideNumber()I

    move-result v2

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getSlideNumber()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 1456
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getActualTextIndent()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->textActualIndent:I

    return v0
.end method

.method public getHorizontalAlignment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->horizontalAlignment:Ljava/lang/String;

    return-object v0
.end method

.method public getLineSpacing()F
    .locals 1

    .prologue
    .line 1464
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->lineSpacing:F

    return v0
.end method

.method public getMarginBottom()I
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginBottom:I

    return v0
.end method

.method public getMarginLeft()I
    .locals 1

    .prologue
    .line 182
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginLeft:I

    return v0
.end method

.method public getMarginRight()I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginRight:I

    return v0
.end method

.method public getMarginTop()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginTop:I

    return v0
.end method

.method public getNonWrapTextWidth()F
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->nonWrapTextWidth:F

    return v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->offset:I

    return v0
.end method

.method public getRunList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/word/Run;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mRunList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getShapeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mShapeId:Ljava/lang/String;

    return-object v0
.end method

.method public getShrinkToFit()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->shrinkToFit:Z

    return v0
.end method

.method public getSingleLineMaxFont()F
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->singleLineMaxFont:F

    return v0
.end method

.method public getSpacingBottom()I
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->spacingBottom:I

    return v0
.end method

.method public getSpacingTop()I
    .locals 1

    .prologue
    .line 214
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->spacingTop:I

    return v0
.end method

.method public getTextHeightofPara()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->textHeightofPara:I

    return v0
.end method

.method public getTextIndent()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->textIndent:I

    return v0
.end method

.method public getTextRotation()S
    .locals 1

    .prologue
    .line 146
    iget-short v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->textRotation:S

    return v0
.end method

.method public getVerticalAlignment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->verticalAlignment:Ljava/lang/String;

    return-object v0
.end method

.method public getXValue()I
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    return v0
.end method

.method public getYValue()I
    .locals 1

    .prologue
    .line 244
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    return v0
.end method

.method public handleDrawTextPara(Lcom/samsung/thumbnail/customview/word/Paragraph;Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 12
    .param p1, "para"    # Lcom/samsung/thumbnail/customview/word/Paragraph;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 276
    if-nez p1, :cond_1

    .line 399
    :cond_0
    :goto_0
    return-void

    .line 280
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v9

    .line 282
    .local v9, "rList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    const/4 v10, 0x0

    .line 283
    .local v10, "runSize":I
    if-eqz v9, :cond_2

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    .line 284
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->tp:Landroid/text/TextPaint;

    .line 285
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 288
    :cond_2
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 290
    iget v0, p3, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getSpacingTop()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 292
    :cond_3
    if-eqz v10, :cond_0

    .line 296
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 297
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 298
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "center"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 300
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->tp:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 309
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 316
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    sget v1, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    if-ge v0, v1, :cond_5

    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->isHeader:Z

    if-nez v0, :cond_5

    .line 317
    sget v0, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_TOP:I

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 318
    :cond_5
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getSpacingTop()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 322
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v0

    sget v3, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    sget v4, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_RIGHT:I

    add-int/2addr v3, v4

    int-to-double v4, v3

    sub-double/2addr v0, v4

    double-to-int v8, v0

    .line 324
    .local v8, "paraWidth":I
    invoke-direct {p0, v9, p2, p3, v8}, Lcom/samsung/thumbnail/customview/word/Paragraph;->DrawTextForRun(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V

    .line 391
    .end local v8    # "paraWidth":I
    :cond_6
    :goto_2
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getSpacingBottom()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    goto/16 :goto_0

    .line 303
    :cond_7
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getHorizontalAlignment()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "right"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 305
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->tp:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_1

    .line 307
    :cond_8
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->tp:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    goto :goto_1

    .line 339
    :cond_9
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getDocumentType()Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 341
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getSpacingTop()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 342
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->elementCreator:Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v0

    sget v3, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_LEFT:I

    sget v4, Lcom/samsung/thumbnail/customview/word/Param;->MARGIN_RIGHT:I

    add-int/2addr v3, v4

    int-to-double v4, v3

    sub-double/2addr v0, v4

    double-to-int v8, v0

    .line 344
    .restart local v8    # "paraWidth":I
    invoke-direct {p0, v9, p2, p3, v8}, Lcom/samsung/thumbnail/customview/word/Paragraph;->DrawTextForRun(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V

    goto :goto_2

    .line 371
    .end local v8    # "paraWidth":I
    :cond_a
    const/4 v11, 0x0

    .local v11, "x11":I
    :goto_3
    if-ge v11, v10, :cond_6

    .line 373
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/Run;->getRunText()Ljava/lang/String;

    move-result-object v2

    .line 374
    .local v2, "text":Ljava/lang/String;
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/customview/word/Run;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/Run;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->tp:Landroid/text/TextPaint;

    .line 377
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_c

    .line 371
    :cond_b
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 381
    :cond_c
    :try_start_0
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/customview/word/Run;

    iget-object v4, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->tp:Landroid/text/TextPaint;

    if-nez v11, :cond_d

    const/4 v5, 0x1

    :goto_5
    move-object v0, p0

    move-object v3, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/samsung/thumbnail/customview/word/Paragraph;->DrawTextForRun(Lcom/samsung/thumbnail/customview/word/Run;Ljava/lang/String;Landroid/graphics/Canvas;Landroid/text/TextPaint;ZLandroid/graphics/Rect;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 383
    :catch_0
    move-exception v7

    .line 384
    .local v7, "e":Ljava/lang/Exception;
    const-string/jumbo v0, "DocumentService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Exception: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 381
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_d
    const/4 v5, 0x0

    goto :goto_5
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 1433
    sget-boolean v0, Lcom/samsung/thumbnail/customview/word/Paragraph;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "hashCode not designed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1434
    :cond_0
    const/16 v0, 0x2a

    return v0
.end method

.method public isEmptyText()Z
    .locals 5

    .prologue
    .line 248
    const/4 v2, 0x1

    .line 250
    .local v2, "ret":Z
    invoke-virtual {p0}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v1

    .line 251
    .local v1, "rList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    if-nez v1, :cond_0

    .line 252
    const/4 v4, 0x1

    .line 262
    :goto_0
    return v4

    .line 255
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/thumbnail/customview/word/Run;

    .line 256
    .local v3, "runs":Lcom/samsung/thumbnail/customview/word/Run;
    invoke-virtual {v3}, Lcom/samsung/thumbnail/customview/word/Run;->isEmptyText()Z

    move-result v4

    if-nez v4, :cond_1

    .line 257
    const/4 v2, 0x0

    .end local v3    # "runs":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_2
    move v4, v2

    .line 262
    goto :goto_0
.end method

.method public isFooter()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->isFooter:Z

    return v0
.end method

.method public isHeader()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->isHeader:Z

    return v0
.end method

.method public paintText(Lcom/samsung/thumbnail/customview/word/Paragraph;Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Rect;IIZ)V
    .locals 9
    .param p1, "para"    # Lcom/samsung/thumbnail/customview/word/Paragraph;
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "rect"    # Landroid/graphics/Rect;
    .param p4, "availableRect"    # Landroid/graphics/Rect;
    .param p5, "paraWidth"    # I
    .param p6, "offset"    # I
    .param p7, "hide"    # Z

    .prologue
    .line 1217
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->tp:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->ascent()F

    move-result v0

    float-to-int v8, v0

    .line 1219
    .local v8, "ascent":I
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 1220
    neg-int v0, v8

    iget v2, p3, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 1222
    :cond_0
    if-nez p1, :cond_1

    .line 1267
    :goto_0
    return-void

    .line 1225
    :cond_1
    if-nez p7, :cond_2

    .line 1226
    iput p6, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->offset:I

    .line 1227
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    iget v2, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->offset:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 1230
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/customview/word/Paragraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v1

    .line 1233
    .local v1, "rList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/word/Run;>;"
    const/4 v0, -0x1

    if-eq p5, v0, :cond_3

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p5

    move/from16 v5, p7

    .line 1234
    invoke-direct/range {v0 .. v5}, Lcom/samsung/thumbnail/customview/word/Paragraph;->DrawTextForRun(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Rect;IZ)V

    .line 1265
    :goto_1
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    neg-int v2, v8

    add-int/lit8 v2, v2, 0x5

    add-int/2addr v0, v2

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 1266
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->textHeightofPara:I

    goto :goto_0

    :cond_3
    move-object v2, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, v1

    move-object v6, p2

    move/from16 v7, p7

    .line 1236
    invoke-direct/range {v2 .. v7}, Lcom/samsung/thumbnail/customview/word/Paragraph;->DrawTextForRunSingleLine(Landroid/graphics/Rect;Landroid/graphics/Rect;Ljava/util/ArrayList;Landroid/graphics/Canvas;Z)V

    goto :goto_1
.end method

.method public setActualTextIndent(I)V
    .locals 0
    .param p1, "actualTextIndent"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->textActualIndent:I

    .line 135
    return-void
.end method

.method public setHorizontalAlignment(Ljava/lang/String;)V
    .locals 0
    .param p1, "alignment"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->horizontalAlignment:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setIsFooter(Z)V
    .locals 0
    .param p1, "isFooter"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->isFooter:Z

    .line 79
    return-void
.end method

.method public setIsHeader(Z)V
    .locals 0
    .param p1, "isHeader"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->isHeader:Z

    .line 71
    return-void
.end method

.method public setLineSpacing(F)V
    .locals 0
    .param p1, "lineSpacing"    # F

    .prologue
    .line 1468
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->lineSpacing:F

    .line 1469
    return-void
.end method

.method public setMarginBottom(I)V
    .locals 0
    .param p1, "marginBottom"    # I

    .prologue
    .line 210
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginBottom:I

    .line 211
    return-void
.end method

.method public setMarginLeft(I)V
    .locals 0
    .param p1, "marginLeft"    # I

    .prologue
    .line 186
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginLeft:I

    .line 187
    return-void
.end method

.method public setMarginRight(I)V
    .locals 0
    .param p1, "marginRight"    # I

    .prologue
    .line 194
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginRight:I

    .line 195
    return-void
.end method

.method public setMarginTop(I)V
    .locals 0
    .param p1, "marginTop"    # I

    .prologue
    .line 202
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->marginTop:I

    .line 203
    return-void
.end method

.method public setNonWrapTextWidth(F)V
    .locals 0
    .param p1, "totalCharLength"    # F

    .prologue
    .line 158
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->nonWrapTextWidth:F

    .line 159
    return-void
.end method

.method public setOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->offset:I

    .line 95
    return-void
.end method

.method public setShapeId(Ljava/lang/String;)V
    .locals 0
    .param p1, "mShapeId"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->mShapeId:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setShrinkToFit(Z)V
    .locals 0
    .param p1, "shrinkToFit"    # Z

    .prologue
    .line 150
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->shrinkToFit:Z

    .line 151
    return-void
.end method

.method public setSingleLineMaxFont(F)V
    .locals 0
    .param p1, "singleLineMaxFont"    # F

    .prologue
    .line 166
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->singleLineMaxFont:F

    .line 167
    return-void
.end method

.method public setSpacingBottom(I)V
    .locals 0
    .param p1, "spacingBottom"    # I

    .prologue
    .line 226
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->spacingBottom:I

    .line 227
    return-void
.end method

.method public setSpacingTop(I)V
    .locals 0
    .param p1, "spacingTop"    # I

    .prologue
    .line 218
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->spacingTop:I

    .line 219
    return-void
.end method

.method public setTextHeightofPara(I)V
    .locals 0
    .param p1, "textHeightofPara"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->textHeightofPara:I

    .line 87
    return-void
.end method

.method public setTextIndent(I)V
    .locals 0
    .param p1, "textIndent"    # I

    .prologue
    .line 126
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->textIndent:I

    .line 127
    return-void
.end method

.method public setTextRotation(S)V
    .locals 0
    .param p1, "textRotation"    # S

    .prologue
    .line 142
    iput-short p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->textRotation:S

    .line 143
    return-void
.end method

.method public setVerticalAlignment(Ljava/lang/String;)V
    .locals 0
    .param p1, "alignment"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->verticalAlignment:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public setXValue(I)V
    .locals 0
    .param p1, "xval"    # I

    .prologue
    .line 232
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->x:I

    .line 233
    return-void
.end method

.method public setYValue(I)V
    .locals 0
    .param p1, "yval"    # I

    .prologue
    .line 240
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/Paragraph;->y:I

    .line 241
    return-void
.end method
