.class public Lcom/samsung/thumbnail/customview/word/DiagramController;
.super Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
.source "DiagramController.java"


# instance fields
.field private behindDoc:I

.field private height:I

.field private isInLine:Z

.field private width:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v0, 0x1

    .line 13
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    .line 9
    iput v0, p0, Lcom/samsung/thumbnail/customview/word/DiagramController;->behindDoc:I

    .line 11
    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/DiagramController;->isInLine:Z

    .line 14
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/DiagramController;->width:I

    .line 15
    iput p2, p0, Lcom/samsung/thumbnail/customview/word/DiagramController;->height:I

    .line 16
    return-void
.end method


# virtual methods
.method public getBehindDocState()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/DiagramController;->behindDoc:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/DiagramController;->height:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/DiagramController;->width:I

    return v0
.end method

.method public isInLine()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/DiagramController;->isInLine:Z

    return v0
.end method

.method public setBehindDoc(I)V
    .locals 0
    .param p1, "behindDoc"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/DiagramController;->behindDoc:I

    .line 40
    return-void
.end method

.method public setIsInLineStatus(Z)V
    .locals 0
    .param p1, "isInLine"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/word/DiagramController;->isInLine:Z

    .line 32
    return-void
.end method
