.class Lcom/samsung/thumbnail/customview/word/CustomView$ScaleListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "CustomView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/customview/word/CustomView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/thumbnail/customview/word/CustomView;


# direct methods
.method private constructor <init>(Lcom/samsung/thumbnail/customview/word/CustomView;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/CustomView$ScaleListener;->this$0:Lcom/samsung/thumbnail/customview/word/CustomView;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/thumbnail/customview/word/CustomView;Lcom/samsung/thumbnail/customview/word/CustomView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/thumbnail/customview/word/CustomView;
    .param p2, "x1"    # Lcom/samsung/thumbnail/customview/word/CustomView$1;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/customview/word/CustomView$ScaleListener;-><init>(Lcom/samsung/thumbnail/customview/word/CustomView;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView$ScaleListener;->this$0:Lcom/samsung/thumbnail/customview/word/CustomView;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    # *= operator for: Lcom/samsung/thumbnail/customview/word/CustomView;->mScaleFactor:F
    invoke-static {v0, v1}, Lcom/samsung/thumbnail/customview/word/CustomView;->access$132(Lcom/samsung/thumbnail/customview/word/CustomView;F)F

    .line 70
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView$ScaleListener;->this$0:Lcom/samsung/thumbnail/customview/word/CustomView;

    const v1, 0x3dcccccd    # 0.1f

    iget-object v2, p0, Lcom/samsung/thumbnail/customview/word/CustomView$ScaleListener;->this$0:Lcom/samsung/thumbnail/customview/word/CustomView;

    # getter for: Lcom/samsung/thumbnail/customview/word/CustomView;->mScaleFactor:F
    invoke-static {v2}, Lcom/samsung/thumbnail/customview/word/CustomView;->access$100(Lcom/samsung/thumbnail/customview/word/CustomView;)F

    move-result v2

    const/high16 v3, 0x40a00000    # 5.0f

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    # setter for: Lcom/samsung/thumbnail/customview/word/CustomView;->mScaleFactor:F
    invoke-static {v0, v1}, Lcom/samsung/thumbnail/customview/word/CustomView;->access$102(Lcom/samsung/thumbnail/customview/word/CustomView;F)F

    .line 72
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/CustomView$ScaleListener;->this$0:Lcom/samsung/thumbnail/customview/word/CustomView;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/customview/word/CustomView;->invalidate()V

    .line 73
    const/4 v0, 0x1

    return v0
.end method
