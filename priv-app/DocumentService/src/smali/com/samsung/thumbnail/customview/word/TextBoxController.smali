.class public Lcom/samsung/thumbnail/customview/word/TextBoxController;
.super Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;
.source "TextBoxController.java"


# instance fields
.field private mContentArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;"
        }
    .end annotation
.end field

.field private x:I

.field private y:I


# direct methods
.method public constructor <init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V
    .locals 1
    .param p1, "slideDimention"    # Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    .prologue
    const/4 v0, -0x1

    .line 21
    invoke-direct {p0, p1}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;-><init>(Lcom/samsung/thumbnail/customview/hslf/SlideDimention;)V

    .line 17
    iput v0, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->y:I

    .line 18
    iput v0, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->x:I

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->mContentArray:Ljava/util/ArrayList;

    .line 23
    return-void
.end method


# virtual methods
.method public addContent(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;)V
    .locals 1
    .param p1, "element"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .prologue
    .line 26
    if-eqz p1, :cond_0

    .line 27
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->mContentArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    :cond_0
    return-void
.end method

.method public drawTextBoxContents(Landroid/graphics/Canvas;Landroid/content/Context;IIIILcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V
    .locals 15
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "startXpos"    # I
    .param p4, "startYpos"    # I
    .param p5, "widthOfTextBox"    # I
    .param p6, "heightOfTextBox"    # I
    .param p7, "elementCreator"    # Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    .prologue
    .line 43
    iget-object v3, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->mContentArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 45
    .local v14, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    add-int/lit8 v3, p3, 0xa

    iput v3, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->x:I

    .line 46
    add-int/lit8 v3, p4, 0xa

    iput v3, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->y:I

    .line 48
    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 50
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .line 52
    .local v13, "canElePart":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    instance-of v3, v13, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    if-eqz v3, :cond_0

    move-object v2, v13

    .line 53
    check-cast v2, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 54
    .local v2, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    iget v3, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->y:I

    int-to-float v3, v3

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getTopSpacing()F

    move-result v4

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->y:I

    .line 56
    iget v3, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->y:I

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setY(I)V

    .line 57
    iget v3, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->x:I

    invoke-virtual {v2, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->setX(I)V

    .line 59
    new-instance v1, Lcom/samsung/thumbnail/customview/word/LineBreaker;

    invoke-direct {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;-><init>()V

    .line 60
    .local v1, "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, -0x1

    move/from16 v6, p5

    move/from16 v7, p5

    invoke-virtual/range {v1 .. v7}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->calculateRunSpace(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Ljava/lang/String;Ljava/lang/String;III)I

    .line 65
    new-instance v6, Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->x:I

    iget v4, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->y:I

    iget v5, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->y:I

    move/from16 v0, p5

    invoke-direct {v6, v3, v4, v0, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 66
    .local v6, "rect":Landroid/graphics/Rect;
    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLines()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v1}, Lcom/samsung/thumbnail/customview/word/LineBreaker;->getLineLengthOfPara()Ljava/util/ArrayList;

    move-result-object v10

    const/4 v11, 0x0

    move-object v3, v2

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v7, p5

    move/from16 v8, p5

    move-object/from16 v12, p7

    invoke-virtual/range {v2 .. v12}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->DrawParaRun(Lcom/samsung/thumbnail/customview/hslf/DocParagraph;Landroid/graphics/Canvas;Landroid/content/Context;Landroid/graphics/Rect;IILjava/util/List;Ljava/util/ArrayList;ZLcom/samsung/thumbnail/customview/word/CanvasElementsCreator;)V

    .line 70
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getY()I

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->y:I

    .line 71
    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getX()I

    move-result v3

    iput v3, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->x:I

    .line 72
    iget v3, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->y:I

    int-to-float v3, v3

    invoke-virtual {v2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getBottomSpacing()F

    move-result v4

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->y:I

    goto :goto_0

    .line 75
    .end local v1    # "lb":Lcom/samsung/thumbnail/customview/word/LineBreaker;
    .end local v2    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v6    # "rect":Landroid/graphics/Rect;
    .end local v13    # "canElePart":Lcom/samsung/thumbnail/customview/CanvasElementPart;
    :cond_1
    const/4 v3, -0x1

    iput v3, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->y:I

    .line 76
    return-void
.end method

.method public getContents()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->mContentArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setContents(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/hslf/DocParagraph;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "txtBxContArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/hslf/DocParagraph;>;"
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->mContentArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 32
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/TextBoxController;->mContentArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 33
    return-void
.end method
