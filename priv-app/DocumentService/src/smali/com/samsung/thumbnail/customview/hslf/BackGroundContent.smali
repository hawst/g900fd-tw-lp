.class public Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;
.super Lcom/samsung/thumbnail/customview/CanvasElementPart;
.source "BackGroundContent.java"


# static fields
.field public static final FILL_STYLE_NONE:I = 0x0

.field public static final FILL_STYLE_PATTERN:I = 0x2

.field public static final FILL_STYLE_SHADE:I = 0x1


# instance fields
.field private bgColor:I

.field private bitMap:Landroid/graphics/Bitmap;

.field private fgColor:I

.field private isBackgroundImage:Z

.field private mFillStyle:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 13
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->isBackgroundImage:Z

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->fgColor:I

    .line 16
    iput v1, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->mFillStyle:I

    .line 19
    return-void
.end method

.method public constructor <init>(ZLandroid/graphics/Bitmap;II)V
    .locals 2
    .param p1, "isBackgroundImage"    # Z
    .param p2, "bitMap"    # Landroid/graphics/Bitmap;
    .param p3, "bgColor"    # I
    .param p4, "fgColor"    # I

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 13
    iput-boolean v1, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->isBackgroundImage:Z

    .line 15
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->fgColor:I

    .line 16
    iput v1, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->mFillStyle:I

    .line 23
    iput-object p2, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->bitMap:Landroid/graphics/Bitmap;

    .line 24
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->isBackgroundImage:Z

    .line 25
    iput p3, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->bgColor:I

    .line 26
    iput p4, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->fgColor:I

    .line 27
    return-void
.end method


# virtual methods
.method public getBGBitMap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->bitMap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getBGColor()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->bgColor:I

    return v0
.end method

.method public getFGColor()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->fgColor:I

    return v0
.end method

.method public getFillStyle()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->mFillStyle:I

    return v0
.end method

.method public getIsBackgroundImage()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->isBackgroundImage:Z

    return v0
.end method

.method public setBGBitMap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitMap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->bitMap:Landroid/graphics/Bitmap;

    .line 67
    return-void
.end method

.method public setBGColor(I)V
    .locals 0
    .param p1, "bgColor"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->bgColor:I

    .line 31
    return-void
.end method

.method public setFGColor(I)V
    .locals 0
    .param p1, "bgColor"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->fgColor:I

    .line 39
    return-void
.end method

.method public setFillStyle(I)V
    .locals 0
    .param p1, "style"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->mFillStyle:I

    .line 47
    return-void
.end method

.method public setIsBackgroundImage(Z)V
    .locals 0
    .param p1, "isPresent"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/hslf/BackGroundContent;->isBackgroundImage:Z

    .line 55
    return-void
.end method
