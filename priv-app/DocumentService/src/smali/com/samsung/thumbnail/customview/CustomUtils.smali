.class public Lcom/samsung/thumbnail/customview/CustomUtils;
.super Ljava/lang/Object;
.source "CustomUtils.java"


# static fields
.field public static final APK_EXTEN:Ljava/lang/String; = "apk"

.field public static final ARROW:Ljava/lang/String; = "\u00d8"

.field public static final BOLD:Ljava/lang/String; = "bold"

.field public static final BORDER_BOTTOM:Ljava/lang/String; = "border-bottom:"

.field public static final BORDER_BOTTOM_COLOR:Ljava/lang/String; = "border-bottom-color:"

.field public static final BORDER_BOTTOM_STYLE:Ljava/lang/String; = "border-bottom-style:"

.field public static final BORDER_BOTTOM_WIDTH:Ljava/lang/String; = "border-bottom-width:"

.field public static final BORDER_LEFT:Ljava/lang/String; = "border-left:"

.field public static final BORDER_LEFT_COLOR:Ljava/lang/String; = "border-left-color:"

.field public static final BORDER_LEFT_STYLE:Ljava/lang/String; = "border-left-style:"

.field public static final BORDER_LEFT_WIDTH:Ljava/lang/String; = "border-left-width:"

.field public static final BORDER_RIGHT:Ljava/lang/String; = "border-right:"

.field public static final BORDER_RIGHT_COLOR:Ljava/lang/String; = "border-right-color:"

.field public static final BORDER_RIGHT_STYLE:Ljava/lang/String; = "border-right-style:"

.field public static final BORDER_RIGHT_WIDTH:Ljava/lang/String; = "border-right-width:"

.field public static final BORDER_TOP:Ljava/lang/String; = "border-top:"

.field public static final BORDER_TOP_COLOR:Ljava/lang/String; = "border-top-color:"

.field public static final BORDER_TOP_STYLE:Ljava/lang/String; = "border-top-style:"

.field public static final BORDER_TOP_WIDTH:Ljava/lang/String; = "border-top-width:"

.field public static final BORDER_WIDTH:Ljava/lang/String; = "border-width:"

.field public static final CAPITALIZE:Ljava/lang/String; = "capitalize"

.field public static final CHECK_MARK:Ljava/lang/String; = "\u00fc"

.field public static final DASHED:Ljava/lang/String; = "dashed"

.field public static final DOTTED:Ljava/lang/String; = "dotted"

.field public static final DOTTED_SOLID:Ljava/lang/String; = "dotted solid"

.field public static final DOTTED_SOLID_DOCUBLE:Ljava/lang/String; = "dotted solid double dashed"

.field public static final DOUBLE:Ljava/lang/String; = "double"

.field public static final EXCEL_2007_ACTION:Ljava/lang/String; = "android.siso.EXCEL"

.field public static final EXCEL_EXTEN_2003:Ljava/lang/String; = "xls"

.field public static final EXCEL_EXTEN_2007:Ljava/lang/String; = "xlsx"

.field public static final EXCEL_VIEWER_ACTION:Ljava/lang/String; = "com.siso.office.EXCEL"

.field public static final FILE_EXTENSION:Ljava/lang/String; = "file-exten"

.field public static final FILE_PATH:Ljava/lang/String; = "file-path"

.field public static final FILLED_SQUARE:Ljava/lang/String; = "\u00a7"

.field public static final FONT_XX_SMALL:Ljava/lang/String; = "xx-small"

.field public static final GROOVE:Ljava/lang/String; = "groove"

.field public static final HALLOW_ROUND:Ljava/lang/String; = "o"

.field public static final HALLOW_SQUARE:Ljava/lang/String; = "q"

.field public static final HEIGHT_STYLE:Ljava/lang/String; = "height:"

.field public static final HREF_PPT_ATTR:Ljava/lang/String; = "href:"

.field public static final INSET:Ljava/lang/String; = "inset"

.field public static final ITALIC:Ljava/lang/String; = "italic"

.field public static final LETTER_SPACING:Ljava/lang/String; = "letter-spacing:"

.field public static final LINE_THROUGH:Ljava/lang/String; = "line-through"

.field public static final MARGIN:Ljava/lang/String; = "margin:"

.field public static final MARGIN_BOTTOM:Ljava/lang/String; = "margin-bottom:"

.field public static final MARGIN_LEFT:Ljava/lang/String; = "margin-left:"

.field public static final MARGIN_RIGHT:Ljava/lang/String; = "margin-right:"

.field public static final MARGIN_TOP:Ljava/lang/String; = "margin-top:"

.field public static final MIN_WIDTH:Ljava/lang/String; = "min-width:"

.field public static final MP3_EXTEN:Ljava/lang/String; = "mp3"

.field public static final NONE:Ljava/lang/String; = "none"

.field public static final OUTSET:Ljava/lang/String; = "outset"

.field public static final PADDING:Ljava/lang/String; = "padding:"

.field public static final PADDING_BOTTOM:Ljava/lang/String; = "padding-bottom:"

.field public static final PADDING_LEFT:Ljava/lang/String; = "padding-left:"

.field public static final PADDING_RIGHT:Ljava/lang/String; = "padding-right:"

.field public static final PADDING_TOP:Ljava/lang/String; = "padding-top:"

.field public static final PARA_BORDER_COLOR:Ljava/lang/String; = "border-color:"

.field public static final PARA_BORDER_STYLE:Ljava/lang/String; = "border-style:"

.field public static final PDF_EXTEN:Ljava/lang/String; = "pdf"

.field public static final PDF_VIEWER_ACTION:Ljava/lang/String; = "com.siso.office.PDF"

.field public static final PIC_BMP_EXTEN:Ljava/lang/String; = "bmp"

.field public static final PIC_GIF_EXTEN:Ljava/lang/String; = "gif"

.field public static final PIC_JPG_EXTEN:Ljava/lang/String; = "jpg"

.field public static final PIC_PNG_EXTEN:Ljava/lang/String; = "png"

.field public static final POINT_EXTEN_2003:Ljava/lang/String; = "ppt"

.field public static final POINT_EXTEN_2007:Ljava/lang/String; = "pptx"

.field public static final POINT_VIEWER_ACTION:Ljava/lang/String; = "com.siso.office.POINT"

.field public static final RIDGE:Ljava/lang/String; = "ridge"

.field public static final SD_CARD_LOCATION:Ljava/lang/String; = "/sdcard/"

.field public static final SMALL_CAPS:Ljava/lang/String; = "small-caps"

.field public static final SOLID:Ljava/lang/String; = "solid"

.field public static final STAR:Ljava/lang/String; = "v"

.field public static final STRIKE:Ljava/lang/String; = "strike"

.field public static final SUB:Ljava/lang/String; = "sub"

.field public static final SUB_END:Ljava/lang/String; = "</sub>"

.field public static final SUB_START:Ljava/lang/String; = "<sub>"

.field public static final SUPER:Ljava/lang/String; = "super"

.field public static final SUP_END:Ljava/lang/String; = "</sup>"

.field public static final SUP_START:Ljava/lang/String; = "<sup>"

.field public static final TAB_CHAR:Ljava/lang/String; = "&nbsp;&nbsp;&nbsp;&nbsp;"

.field public static final TEXT_ALIGN:Ljava/lang/String; = "text-align:"

.field public static final TEXT_DECORATION:Ljava/lang/String; = "text-decoration:"

.field public static final TEXT_EXTEN:Ljava/lang/String; = "txt"

.field public static final TEXT_INDENT:Ljava/lang/String; = "text-indent:"

.field public static final TEXT_OUTLINE:Ljava/lang/String; = "text-outline:"

.field public static final TEXT_SHADOW:Ljava/lang/String; = "text-shadow:"

.field public static final TEXT_TRANSFORM:Ljava/lang/String; = "text-transform:"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final UNDERLINE:Ljava/lang/String; = "underline"

.field public static final UPPERCASE:Ljava/lang/String; = "uppercase"

.field public static final VERTICAL_ALIGN:Ljava/lang/String; = "vertical-align:"

.field public static final VERTICAL_ALIGN_BOTTOM:Ljava/lang/String; = "bottom"

.field public static final VERTICAL_ALIGN_MIDDLE:Ljava/lang/String; = "middle"

.field public static final VERTICAL_ALIGN_TOP:Ljava/lang/String; = "top"

.field public static final VIEW_ACTION:Ljava/lang/String; = "android.intent.action.VIEW"

.field public static final WAV_EXTEN:Ljava/lang/String; = "wav"

.field public static final WIDTH_STYLE:Ljava/lang/String; = "width:"

.field public static final WORD_EXTEN_2003:Ljava/lang/String; = "doc"

.field public static final WORD_EXTEN_2007:Ljava/lang/String; = "docx"

.field public static final WORD_SPACING:Ljava/lang/String; = "word-spacing:"

.field public static final WORD_VIEWER_ACTION:Ljava/lang/String; = "com.siso.office.WORD"

.field private static bulletMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 128
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/thumbnail/customview/CustomUtils;->bulletMap:Ljava/util/Map;

    .line 139
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/thumbnail/customview/CustomUtils;->bulletMap:Ljava/util/Map;

    .line 140
    sget-object v0, Lcom/samsung/thumbnail/customview/CustomUtils;->bulletMap:Ljava/util/Map;

    const-string/jumbo v1, "o"

    const-string/jumbo v2, "&#9675;"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/samsung/thumbnail/customview/CustomUtils;->bulletMap:Ljava/util/Map;

    const-string/jumbo v1, "\u00a7"

    const-string/jumbo v2, "&#9642;"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/samsung/thumbnail/customview/CustomUtils;->bulletMap:Ljava/util/Map;

    const-string/jumbo v1, "q"

    const-string/jumbo v2, "&#9643"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/samsung/thumbnail/customview/CustomUtils;->bulletMap:Ljava/util/Map;

    const-string/jumbo v1, "v"

    const-string/jumbo v2, "&#9733"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/samsung/thumbnail/customview/CustomUtils;->bulletMap:Ljava/util/Map;

    const-string/jumbo v1, "\u00d8"

    const-string/jumbo v2, "&#9654"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/samsung/thumbnail/customview/CustomUtils;->bulletMap:Ljava/util/Map;

    const-string/jumbo v1, "\u00fc"

    const-string/jumbo v2, "&#10003"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getDecimalCodeForBullet(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "unicode"    # Ljava/lang/String;

    .prologue
    .line 155
    const-class v1, Lcom/samsung/thumbnail/customview/CustomUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/thumbnail/customview/CustomUtils;->bulletMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
