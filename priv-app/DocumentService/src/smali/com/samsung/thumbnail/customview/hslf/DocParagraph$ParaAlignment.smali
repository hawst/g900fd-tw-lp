.class public final enum Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;
.super Ljava/lang/Enum;
.source "DocParagraph.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ParaAlignment"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

.field public static final enum CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

.field public static final enum JUSTIFY:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

.field public static final enum LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

.field public static final enum RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    const-string/jumbo v1, "LEFT"

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    const-string/jumbo v1, "RIGHT"

    invoke-direct {v0, v1, v3}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    const-string/jumbo v1, "CENTER"

    invoke-direct {v0, v1, v4}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    new-instance v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    const-string/jumbo v1, "JUSTIFY"

    invoke-direct {v0, v1, v5}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->JUSTIFY:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    .line 92
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    sget-object v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->LEFT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->RIGHT:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->CENTER:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->JUSTIFY:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->$VALUES:[Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 92
    const-class v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->$VALUES:[Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaAlignment;

    return-object v0
.end method
