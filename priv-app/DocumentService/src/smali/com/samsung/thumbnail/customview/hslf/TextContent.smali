.class public Lcom/samsung/thumbnail/customview/hslf/TextContent;
.super Lcom/samsung/thumbnail/customview/CanvasElementPart;
.source "TextContent.java"


# instance fields
.field private height:D

.field private mAllignment:Ljava/lang/String;

.field private mFontSize:Ljava/lang/String;

.field private mText:Ljava/lang/String;

.field private mTextColor:I

.field private width:D

.field private xCoOrdinate:D

.field private yCoOrdinate:D


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IDDDD)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "fontSize"    # Ljava/lang/String;
    .param p3, "Allignment"    # Ljava/lang/String;
    .param p4, "textColor"    # I
    .param p5, "xCoOrdinate"    # D
    .param p7, "yCoOrdinate"    # D
    .param p9, "width"    # D
    .param p11, "height"    # D

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/CanvasElementPart;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->mText:Ljava/lang/String;

    .line 20
    iput-object p2, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->mFontSize:Ljava/lang/String;

    .line 21
    iput-object p3, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->mAllignment:Ljava/lang/String;

    .line 22
    iput p4, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->mTextColor:I

    .line 23
    iput-wide p5, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->xCoOrdinate:D

    .line 24
    iput-wide p7, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->yCoOrdinate:D

    .line 25
    iput-wide p9, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->width:D

    .line 26
    iput-wide p11, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->height:D

    .line 27
    return-void
.end method


# virtual methods
.method public getAllignment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->mAllignment:Ljava/lang/String;

    return-object v0
.end method

.method public getFontSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->mFontSize:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()D
    .locals 2

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->height:D

    return-wide v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->mTextColor:I

    return v0
.end method

.method public getWidth()D
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->width:D

    return-wide v0
.end method

.method public getxCoOrdinate()F
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->xCoOrdinate:D

    double-to-float v0, v0

    return v0
.end method

.method public getyCoOrdinate()F
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->yCoOrdinate:D

    double-to-float v0, v0

    return v0
.end method

.method public setAllignment(Ljava/lang/String;)V
    .locals 0
    .param p1, "mAllignment"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->mAllignment:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setFontSize(Ljava/lang/String;)V
    .locals 0
    .param p1, "mFontSize"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->mFontSize:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setHeight(D)V
    .locals 1
    .param p1, "height"    # D

    .prologue
    .line 90
    iput-wide p1, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->height:D

    .line 91
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1, "mText"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->mText:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setTextColor(I)V
    .locals 0
    .param p1, "mTextColor"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->mTextColor:I

    .line 35
    return-void
.end method

.method public setWidth(D)V
    .locals 1
    .param p1, "width"    # D

    .prologue
    .line 82
    iput-wide p1, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->width:D

    .line 83
    return-void
.end method

.method public setxCoOrdinate(D)V
    .locals 1
    .param p1, "xCoOrdinate"    # D

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->xCoOrdinate:D

    .line 67
    return-void
.end method

.method public setyCoOrdinate(D)V
    .locals 1
    .param p1, "yCoOrdinate"    # D

    .prologue
    .line 74
    iput-wide p1, p0, Lcom/samsung/thumbnail/customview/hslf/TextContent;->yCoOrdinate:D

    .line 75
    return-void
.end method
