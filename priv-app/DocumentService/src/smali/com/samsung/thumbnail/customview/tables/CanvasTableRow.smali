.class public Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;
.super Ljava/lang/Object;
.source "CanvasTableRow.java"


# instance fields
.field private gridDisplay:Z

.field private mCellList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;",
            ">;"
        }
    .end annotation
.end field

.field private mRowHeight:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->mCellList:Ljava/util/ArrayList;

    .line 12
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->gridDisplay:Z

    .line 16
    const/high16 v0, 0x41f00000    # 30.0f

    iput v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->mRowHeight:F

    .line 18
    return-void
.end method


# virtual methods
.method public addCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 1
    .param p1, "newCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->mCellList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    return-void
.end method

.method public getCell(I)Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->mCellList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    return-object v0
.end method

.method public getCellCount()I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->mCellList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getCellList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->mCellList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGridDisplay()Z
    .locals 1

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->gridDisplay:Z

    return v0
.end method

.method public getRowHeight()F
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->mRowHeight:F

    return v0
.end method

.method public setCellList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "cellList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;>;"
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->mCellList:Ljava/util/ArrayList;

    .line 30
    return-void
.end method

.method public setGridDisplay(Z)V
    .locals 0
    .param p1, "isGridDisplay"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->gridDisplay:Z

    .line 26
    return-void
.end method

.method public setRowHeight(F)V
    .locals 0
    .param p1, "rowHeight"    # F

    .prologue
    .line 45
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->mRowHeight:F

    .line 46
    return-void
.end method

.method public updateRowHeight(F)V
    .locals 3
    .param p1, "height"    # F

    .prologue
    .line 53
    iget-object v2, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->mCellList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .line 54
    .local v1, "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    invoke-virtual {v1, p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellHeight(F)V

    goto :goto_0

    .line 56
    .end local v1    # "tempCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    :cond_0
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->mRowHeight:F

    .line 57
    return-void
.end method
