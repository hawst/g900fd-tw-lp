.class public final enum Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;
.super Ljava/lang/Enum;
.source "CanvasElementPart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/customview/CanvasElementPart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HorizontalRelativeElement"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

.field public static final enum CHARACTER:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

.field public static final enum COLUMN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

.field public static final enum INSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

.field public static final enum LEFT_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

.field public static final enum MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

.field public static final enum OUTSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

.field public static final enum PAGE:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

.field public static final enum RIGHT_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 66
    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    const-string/jumbo v1, "MARGIN"

    invoke-direct {v0, v1, v3}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    const-string/jumbo v1, "PAGE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->PAGE:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    const-string/jumbo v1, "COLUMN"

    invoke-direct {v0, v1, v5}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->COLUMN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    const-string/jumbo v1, "CHARACTER"

    invoke-direct {v0, v1, v6}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->CHARACTER:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    const-string/jumbo v1, "LEFT_MARGIN"

    invoke-direct {v0, v1, v7}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->LEFT_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    const-string/jumbo v1, "RIGHT_MARGIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->RIGHT_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    const-string/jumbo v1, "INSIDE_MARGIN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->INSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    const-string/jumbo v1, "OUTSIDE_MARGIN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->OUTSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    .line 65
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->PAGE:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->COLUMN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->CHARACTER:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->LEFT_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->RIGHT_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->INSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->OUTSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->$VALUES:[Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 65
    const-class v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->$VALUES:[Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/customview/CanvasElementPart$HorizontalRelativeElement;

    return-object v0
.end method
