.class public final enum Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;
.super Ljava/lang/Enum;
.source "CanvasElementPart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/customview/CanvasElementPart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DocumentType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

.field public static final enum DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

.field public static final enum DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

.field public static final enum PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

.field public static final enum PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

.field public static final enum TXT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

.field public static final enum XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

.field public static final enum XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 37
    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v1, "DOC"

    invoke-direct {v0, v1, v3}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v1, "DOCX"

    invoke-direct {v0, v1, v4}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v1, "XLS"

    invoke-direct {v0, v1, v5}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v1, "XLSX"

    invoke-direct {v0, v1, v6}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v1, "PPT"

    invoke-direct {v0, v1, v7}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v1, "PPTX"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    const-string/jumbo v1, "TXT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->TXT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    .line 36
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOC:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->DOCX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLS:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->XLSX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->PPTX:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->TXT:Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->$VALUES:[Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 36
    const-class v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->$VALUES:[Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/customview/CanvasElementPart$DocumentType;

    return-object v0
.end method
