.class public Lcom/samsung/thumbnail/customview/tables/TableBorder;
.super Ljava/lang/Object;
.source "TableBorder.java"


# instance fields
.field private borderColor:Lorg/apache/poi/java/awt/Color;

.field private borderStyle:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

.field private borderWidth:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBorderColor()Lorg/apache/poi/java/awt/Color;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/TableBorder;->borderColor:Lorg/apache/poi/java/awt/Color;

    return-object v0
.end method

.method public getBorderStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/TableBorder;->borderStyle:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    return-object v0
.end method

.method public getBorderWidth()F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/TableBorder;->borderWidth:F

    return v0
.end method

.method public setBorderColor(Lorg/apache/poi/java/awt/Color;)V
    .locals 0
    .param p1, "color"    # Lorg/apache/poi/java/awt/Color;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/TableBorder;->borderColor:Lorg/apache/poi/java/awt/Color;

    .line 23
    return-void
.end method

.method public setBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;)V
    .locals 0
    .param p1, "borderstyle"    # Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .prologue
    .line 14
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/TableBorder;->borderStyle:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 15
    return-void
.end method

.method public setBorderWidth(F)V
    .locals 0
    .param p1, "width"    # F

    .prologue
    .line 30
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/TableBorder;->borderWidth:F

    .line 31
    return-void
.end method
