.class public Lcom/samsung/thumbnail/customview/word/ParaLine;
.super Ljava/lang/Object;
.source "ParaLine.java"


# instance fields
.field endIndex:I

.field hasTab:Z

.field isBitmap:Z

.field isChart:Z

.field isDiagram:Z

.field isShape:Z

.field isText:Z

.field lineNumber:I

.field mCanElePart:Lcom/samsung/thumbnail/customview/CanvasElementPart;

.field run:Lcom/samsung/thumbnail/customview/word/Run;

.field startIndex:I

.field tabIndex:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCanvasElementPart()Lcom/samsung/thumbnail/customview/CanvasElementPart;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->mCanElePart:Lcom/samsung/thumbnail/customview/CanvasElementPart;

    return-object v0
.end method

.method public getEndIndex()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->endIndex:I

    return v0
.end method

.method public getLineNumber()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->lineNumber:I

    return v0
.end method

.method public getRun()Lcom/samsung/thumbnail/customview/word/Run;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->run:Lcom/samsung/thumbnail/customview/word/Run;

    return-object v0
.end method

.method public getStartIndex()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->startIndex:I

    return v0
.end method

.method public getTabIndex()F
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->tabIndex:F

    return v0
.end method

.method public hasTab()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->hasTab:Z

    return v0
.end method

.method public isBitmap()Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->isBitmap:Z

    return v0
.end method

.method public isChart()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->isChart:Z

    return v0
.end method

.method public isDiagram()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->isDiagram:Z

    return v0
.end method

.method public isShape()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->isShape:Z

    return v0
.end method

.method public isText()Z
    .locals 1

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->isText:Z

    return v0
.end method

.method public setCanvasElementPart(Lcom/samsung/thumbnail/customview/CanvasElementPart;)V
    .locals 0
    .param p1, "canElePart"    # Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->mCanElePart:Lcom/samsung/thumbnail/customview/CanvasElementPart;

    .line 93
    return-void
.end method

.method public setEndIndex(I)V
    .locals 1
    .param p1, "endIndex"    # I

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->startIndex:I

    if-ge p1, v0, :cond_0

    .line 82
    iget v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->startIndex:I

    iput v0, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->endIndex:I

    .line 85
    :goto_0
    return-void

    .line 84
    :cond_0
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->endIndex:I

    goto :goto_0
.end method

.method public setHasTab(Z)V
    .locals 0
    .param p1, "hasTab"    # Z

    .prologue
    .line 100
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->hasTab:Z

    .line 101
    return-void
.end method

.method public setIsBitmap(Z)V
    .locals 0
    .param p1, "isBitmap"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->isBitmap:Z

    .line 34
    return-void
.end method

.method public setIsChart(Z)V
    .locals 0
    .param p1, "isChart"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->isChart:Z

    .line 50
    return-void
.end method

.method public setIsDiagram(Z)V
    .locals 0
    .param p1, "isDiagram"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->isDiagram:Z

    .line 42
    return-void
.end method

.method public setIsShape(Z)V
    .locals 0
    .param p1, "isShape"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->isShape:Z

    .line 58
    return-void
.end method

.method public setIsText(Z)V
    .locals 0
    .param p1, "isText"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->isText:Z

    .line 26
    return-void
.end method

.method public setLineNumber(I)V
    .locals 0
    .param p1, "lineNumber"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->lineNumber:I

    .line 66
    return-void
.end method

.method public setRun(Lcom/samsung/thumbnail/customview/word/Run;)V
    .locals 0
    .param p1, "run"    # Lcom/samsung/thumbnail/customview/word/Run;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->run:Lcom/samsung/thumbnail/customview/word/Run;

    .line 118
    return-void
.end method

.method public setStartIndex(I)V
    .locals 0
    .param p1, "startIndex"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->startIndex:I

    .line 74
    return-void
.end method

.method public setTabIndex(F)V
    .locals 0
    .param p1, "tabIndex"    # F

    .prologue
    .line 108
    iput p1, p0, Lcom/samsung/thumbnail/customview/word/ParaLine;->tabIndex:F

    .line 109
    return-void
.end method
