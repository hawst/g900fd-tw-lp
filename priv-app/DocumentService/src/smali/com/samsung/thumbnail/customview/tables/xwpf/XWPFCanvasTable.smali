.class public Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;
.super Ljava/lang/Object;
.source "XWPFCanvasTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable$1;
    }
.end annotation


# static fields
.field public static final BORDER_WIDTHDEFAULTVALUE:F = 1.05f

.field public static final DEFAULT_ROW_HEIGHT:F = 330.0f


# instance fields
.field private box:I

.field private colNo:I

.field private consumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

.field private mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

.field private noOfCols:I

.field private noOfRows:I

.field private parentTableStruct:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

.field private rowNo:I

.field private tableNum:I

.field private tableType:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    return-void
.end method

.method private processTblCellCondFormat(Ljava/util/List;Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;)Ljava/util/List;
    .locals 3
    .param p2, "cell"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;",
            ">;",
            "Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1171
    .local p1, "overrideTypesRow":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1174
    .local v1, "overrideTypes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;>;"
    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1176
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->getCellProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 1177
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->getCellProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getCNFStyle()Ljava/lang/String;

    move-result-object v0

    .line 1179
    .local v0, "cnfStyle":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string/jumbo v2, "^\\d{2}1.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1180
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->FIRST_COL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1182
    :cond_0
    if-eqz v0, :cond_1

    const-string/jumbo v2, "^\\d{3}1.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1183
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->LAST_COL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1185
    :cond_1
    if-eqz v0, :cond_2

    const-string/jumbo v2, "^\\d{4}1.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1186
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND1_VERT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1188
    :cond_2
    if-eqz v0, :cond_3

    const-string/jumbo v2, "^\\d{5}1.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1189
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND2_VERT:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1191
    :cond_3
    if-eqz v0, :cond_4

    const-string/jumbo v2, "^\\d{8}1.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1192
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->NE_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194
    :cond_4
    if-eqz v0, :cond_5

    const-string/jumbo v2, "^\\d{9}1.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1195
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->NW_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1197
    :cond_5
    if-eqz v0, :cond_6

    const-string/jumbo v2, "^\\d{10}1.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1198
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->SE_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1200
    :cond_6
    if-eqz v0, :cond_7

    const-string/jumbo v2, "^\\d{11}1.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1201
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->SW_CELL:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1205
    .end local v0    # "cnfStyle":Ljava/lang/String;
    :cond_7
    return-object v1
.end method

.method private processTblRowCondFormat(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;)Ljava/util/List;
    .locals 3
    .param p1, "row"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1145
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1146
    .local v1, "overrideTypes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;>;"
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->getRowProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1147
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->getRowProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;->getCNFStyle()Ljava/lang/String;

    move-result-object v0

    .line 1149
    .local v0, "cnfStyle":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string/jumbo v2, "^1.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1150
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->FIRST_ROW:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1152
    :cond_0
    if-eqz v0, :cond_1

    const-string/jumbo v2, "^\\d1.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1153
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->LAST_ROW:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1155
    :cond_1
    if-eqz v0, :cond_2

    const-string/jumbo v2, "^\\d{6}1.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1156
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND1_HORZ:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1158
    :cond_2
    if-eqz v0, :cond_3

    const-string/jumbo v2, "^\\d{7}1.*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1159
    sget-object v2, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->BAND2_HORZ:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1163
    .end local v0    # "cnfStyle":Ljava/lang/String;
    :cond_3
    return-object v1
.end method

.method private startCanvasTable(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V
    .locals 2
    .param p1, "tableProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->parentTableStruct:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 160
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 162
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->createTableBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;)V

    .line 164
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_H:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_V:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_H:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_V:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 174
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 178
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->RIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 183
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 188
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->LEFT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 193
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->box:I

    .line 262
    :cond_1
    :goto_0
    return-void

    .line 196
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_V:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_V:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 202
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 206
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->RIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 211
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 216
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->LEFT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 221
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->box:I

    goto :goto_0

    .line 224
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_H:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_H:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->NONE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 230
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 234
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->RIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 239
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 244
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->LEFT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 249
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->box:I

    goto/16 :goto_0

    .line 251
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->box:I

    goto/16 :goto_0
.end method

.method private writeTableCell(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;IILjava/util/List;)V
    .locals 37
    .param p1, "row"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;
    .param p2, "cellPos"    # I
    .param p3, "indexnumber"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 655
    .local p4, "rowStylesList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;>;"
    new-instance v24, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    invoke-direct/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;-><init>()V

    .line 660
    .local v24, "newCanvasCell":Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;
    const-string/jumbo v34, "top"

    move-object/from16 v0, v24

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setVerticalAlignment(Ljava/lang/String;)V

    .line 664
    const/16 v34, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setWrapContent(Z)V

    .line 667
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->getCellList()Ljava/util/List;

    move-result-object v34

    move-object/from16 v0, v34

    move/from16 v1, p2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;

    .line 670
    .local v9, "cell":Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->getCellProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v34

    if-eqz v34, :cond_0

    .line 671
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->getCellProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getGridSpan()I

    move-result v34

    move-object/from16 v0, v24

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellGradSpan(I)V

    .line 672
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->getCellProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getTextDirection()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v24

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTextDirection(Ljava/lang/String;)V

    .line 676
    :cond_0
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v35, ""

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, v34

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v24

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setMergedCellKey(Ljava/lang/String;)V

    .line 683
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->getCellProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v10

    .line 686
    .local v10, "cellProp":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    if-eqz v10, :cond_13

    .line 687
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v34

    if-eqz v34, :cond_1

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v34

    if-eqz v34, :cond_1

    .line 692
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v18

    .line 693
    .local v18, "hexcolorcode":Ljava/lang/String;
    const/4 v15, 0x0

    .line 696
    .local v15, "fillColor":Lorg/apache/poi/java/awt/Color;
    const/16 v34, 0x10

    :try_start_0
    move-object/from16 v0, v18

    move/from16 v1, v34

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v11

    .line 697
    .local v11, "colorvalue":I
    new-instance v16, Lorg/apache/poi/java/awt/Color;

    move-object/from16 v0, v16

    invoke-direct {v0, v11}, Lorg/apache/poi/java/awt/Color;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v15    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .local v16, "fillColor":Lorg/apache/poi/java/awt/Color;
    move-object/from16 v15, v16

    .line 702
    .end local v11    # "colorvalue":I
    .end local v16    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .restart local v15    # "fillColor":Lorg/apache/poi/java/awt/Color;
    :goto_0
    const/16 v34, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v34

    invoke-virtual {v0, v15, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    .line 705
    .end local v15    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .end local v18    # "hexcolorcode":Ljava/lang/String;
    :cond_1
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getVMerge()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    move-result-object v34

    if-eqz v34, :cond_2

    .line 707
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getVMerge()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    move-result-object v34

    sget-object v35, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->CONTINUE:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    if-ne v0, v1, :cond_11

    .line 708
    sget-object v34, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->CONTINUE:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->xmlValue()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v24

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellMergeValue(Ljava/lang/String;)V

    .line 718
    :cond_2
    :goto_1
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getWidthType()Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    move-result-object v34

    if-eqz v34, :cond_3

    .line 720
    const/16 v33, 0x0

    .line 722
    .local v33, "widthVal":F
    sget-object v34, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$table$XWPFTable$EWidthType:[I

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getWidthType()Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->ordinal()I

    move-result v35

    aget v34, v34, v35

    packed-switch v34, :pswitch_data_0

    .line 737
    :goto_2
    move-object/from16 v0, v24

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellWidth(F)V

    .line 742
    .end local v33    # "widthVal":F
    :cond_3
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v34

    if-eqz v34, :cond_12

    .line 744
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->parentTableStruct:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v34

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v35

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->createCellBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 763
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->box:I

    move/from16 v34, v0

    const/16 v35, 0x2

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_5

    .line 767
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    const/16 v34, 0x0

    move/from16 v0, v34

    invoke-direct {v6, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 771
    .local v6, "borderColor":Lorg/apache/poi/java/awt/Color;
    sget-object v34, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const v35, 0x3f866666    # 1.05f

    move-object/from16 v0, v24

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v6, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 773
    sget-object v34, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const v35, 0x3f866666    # 1.05f

    move-object/from16 v0, v24

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v6, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 776
    .end local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->box:I

    move/from16 v34, v0

    const/16 v35, 0x3

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_6

    .line 777
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    const/16 v34, 0x0

    move/from16 v0, v34

    invoke-direct {v6, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 779
    .restart local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    sget-object v34, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const v35, 0x3f866666    # 1.05f

    move-object/from16 v0, v24

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v6, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 781
    sget-object v34, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    const v35, 0x3f866666    # 1.05f

    move-object/from16 v0, v24

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v6, v2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 785
    .end local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->parentTableStruct:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v34

    if-eqz v34, :cond_c

    .line 786
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->parentTableStruct:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v34

    sget-object v35, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_H:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    if-eqz v34, :cond_9

    .line 789
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    const/16 v34, 0x0

    move/from16 v0, v34

    invoke-direct {v6, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 791
    .restart local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 792
    .local v7, "borderType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    const v8, 0x3f866666    # 1.05f

    .line 794
    .local v8, "borderWidth":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->parentTableStruct:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v34

    sget-object v35, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_H:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 798
    .local v5, "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v34

    if-eqz v34, :cond_7

    .line 799
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v7

    .line 803
    :cond_7
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v34

    if-eqz v34, :cond_8

    .line 804
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->isAuto()Z

    move-result v34

    if-nez v34, :cond_8

    .line 806
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v18

    .line 807
    .restart local v18    # "hexcolorcode":Ljava/lang/String;
    const/16 v34, 0x10

    move-object/from16 v0, v18

    move/from16 v1, v34

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v11

    .line 808
    .restart local v11    # "colorvalue":I
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    .end local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    invoke-direct {v6, v11}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 813
    .end local v11    # "colorvalue":I
    .end local v18    # "hexcolorcode":Ljava/lang/String;
    .restart local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :cond_8
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v34

    if-ltz v34, :cond_9

    .line 814
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v30

    .line 823
    .local v30, "size":I
    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPoint(F)F

    .line 834
    .end local v5    # "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .end local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    .end local v7    # "borderType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v8    # "borderWidth":F
    .end local v30    # "size":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->parentTableStruct:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v34

    sget-object v35, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_V:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    if-eqz v34, :cond_c

    .line 837
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    const/16 v34, 0x0

    move/from16 v0, v34

    invoke-direct {v6, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 839
    .restart local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 840
    .restart local v7    # "borderType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    const v8, 0x3f866666    # 1.05f

    .line 842
    .restart local v8    # "borderWidth":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->parentTableStruct:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v34

    sget-object v35, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_V:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 846
    .restart local v5    # "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v34

    if-eqz v34, :cond_a

    .line 847
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v7

    .line 851
    :cond_a
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v34

    if-eqz v34, :cond_b

    .line 852
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->isAuto()Z

    move-result v34

    if-nez v34, :cond_b

    .line 854
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v18

    .line 855
    .restart local v18    # "hexcolorcode":Ljava/lang/String;
    const/16 v34, 0x10

    move-object/from16 v0, v18

    move/from16 v1, v34

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v11

    .line 856
    .restart local v11    # "colorvalue":I
    new-instance v6, Lorg/apache/poi/java/awt/Color;

    .end local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    invoke-direct {v6, v11}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 861
    .end local v11    # "colorvalue":I
    .end local v18    # "hexcolorcode":Ljava/lang/String;
    .restart local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :cond_b
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v34

    if-ltz v34, :cond_c

    .line 862
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v30

    .line 870
    .restart local v30    # "size":I
    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPoint(F)F

    .line 881
    .end local v5    # "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .end local v6    # "borderColor":Lorg/apache/poi/java/awt/Color;
    .end local v7    # "borderType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v8    # "borderWidth":F
    .end local v30    # "size":I
    :cond_c
    if-eqz v10, :cond_e

    .line 883
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getRowSpan()I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-le v0, v1, :cond_d

    .line 889
    :cond_d
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getGridSpan()I

    move-result v34

    const/16 v35, 0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-le v0, v1, :cond_e

    .line 901
    :cond_e
    new-instance v23, Ljava/lang/StringBuilder;

    const/16 v34, 0x100

    move-object/from16 v0, v23

    move/from16 v1, v34

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 906
    .local v23, "mstrBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->getParaList()Ljava/util/List;

    move-result-object v27

    .line 908
    .local v27, "paraList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_4
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v34

    move/from16 v0, v19

    move/from16 v1, v34

    if-ge v0, v1, :cond_15

    .line 909
    move-object/from16 v0, v27

    move/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;

    .line 914
    .local v26, "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->size()I

    move-result v34

    if-nez v34, :cond_f

    .line 918
    :cond_f
    invoke-virtual/range {v26 .. v26}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;->getRunList()Ljava/util/ArrayList;

    move-result-object v29

    .line 920
    .local v29, "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    const/16 v21, 0x0

    .local v21, "j":I
    :goto_5
    invoke-interface/range {v29 .. v29}, Ljava/util/List;->size()I

    move-result v34

    move/from16 v0, v21

    move/from16 v1, v34

    if-ge v0, v1, :cond_14

    .line 921
    move-object/from16 v0, v29

    move/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;

    .line 923
    .local v28, "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v34

    if-eqz v34, :cond_10

    .line 925
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;->getText()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v23

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 920
    :cond_10
    add-int/lit8 v21, v21, 0x1

    goto :goto_5

    .line 698
    .end local v19    # "i":I
    .end local v21    # "j":I
    .end local v23    # "mstrBuilder":Ljava/lang/StringBuilder;
    .end local v26    # "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .end local v27    # "paraList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    .end local v28    # "run":Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;
    .end local v29    # "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    .restart local v15    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .restart local v18    # "hexcolorcode":Ljava/lang/String;
    :catch_0
    move-exception v14

    .line 699
    .local v14, "e":Ljava/lang/Exception;
    const-string/jumbo v34, "DocumentService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v36, "Exception: "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual {v14}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 709
    .end local v14    # "e":Ljava/lang/Exception;
    .end local v15    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .end local v18    # "hexcolorcode":Ljava/lang/String;
    :cond_11
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getVMerge()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    move-result-object v34

    sget-object v35, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->RESTART:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    if-ne v0, v1, :cond_2

    .line 710
    sget-object v34, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->RESTART:Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties$VMerge;->xmlValue()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v24

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellMergeValue(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 724
    .restart local v33    # "widthVal":F
    :pswitch_0
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getWidth()I

    move-result v34

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(F)F

    move-result v33

    .line 727
    goto/16 :goto_2

    .line 729
    :pswitch_1
    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getWidth()I

    move-result v34

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v34, v0

    invoke-static/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->fiftiethsToPoints(F)F

    move-result v33

    .line 732
    goto/16 :goto_2

    .line 747
    .end local v33    # "widthVal":F
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->parentTableStruct:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v34

    if-eqz v34, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->box:I

    move/from16 v34, v0

    if-nez v34, :cond_4

    .line 750
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->parentTableStruct:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v34

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->createCellBorderStyleFromParent(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    goto/16 :goto_3

    .line 756
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->parentTableStruct:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v34

    if-eqz v34, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->box:I

    move/from16 v34, v0

    if-nez v34, :cond_4

    .line 759
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->parentTableStruct:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v34

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->createCellBorderStyleFromParent(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    goto/16 :goto_3

    .line 908
    .restart local v19    # "i":I
    .restart local v21    # "j":I
    .restart local v23    # "mstrBuilder":Ljava/lang/StringBuilder;
    .restart local v26    # "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .restart local v27    # "paraList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;>;"
    .restart local v29    # "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    :cond_14
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_4

    .line 938
    .end local v21    # "j":I
    .end local v26    # "para":Lcom/samsung/thumbnail/office/ooxml/word/XWPFParagraph;
    .end local v29    # "runList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFRun;>;"
    :cond_15
    invoke-virtual {v9}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->getDocParaList()Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v24

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->addDocParaList(Ljava/util/ArrayList;)V

    .line 939
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v24

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellText(Ljava/lang/String;)V

    .line 944
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->consumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->parentTableStruct:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getStyle()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v12

    .line 949
    .local v12, "currentStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    if-eqz v12, :cond_1a

    .line 950
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v12, v1}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->applyStyleToCell(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 954
    const/16 v19, 0x0

    :goto_6
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getDocParagraphList()Ljava/util/ArrayList;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v19

    move/from16 v1, v34

    if-ge v0, v1, :cond_1a

    .line 955
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getDocParagraphList()Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 957
    .local v13, "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    const/16 v21, 0x0

    .restart local v21    # "j":I
    :goto_7
    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getContents()Ljava/util/ArrayList;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v21

    move/from16 v1, v34

    if-ge v0, v1, :cond_19

    .line 958
    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getContents()Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/samsung/thumbnail/customview/word/Run;

    .line 960
    .local v28, "run":Lcom/samsung/thumbnail/customview/word/Run;
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v34

    if-eqz v34, :cond_16

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v34

    if-eqz v34, :cond_16

    .line 962
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v17

    .line 965
    .local v17, "fontColor":Ljava/lang/String;
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v35, "#"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v34 .. v34}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v34

    move-object/from16 v0, v28

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 970
    .end local v17    # "fontColor":Ljava/lang/String;
    :cond_16
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v34

    if-eqz v34, :cond_17

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBold()Z

    move-result v34

    if-eqz v34, :cond_17

    .line 972
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    .line 974
    :cond_17
    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v34

    if-eqz v34, :cond_18

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getItalic()Z

    move-result v34

    if-eqz v34, :cond_18

    .line 976
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 957
    :cond_18
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_7

    .line 954
    .end local v28    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_19
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_6

    .line 987
    .end local v13    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v21    # "j":I
    :cond_1a
    if-eqz v12, :cond_20

    .line 989
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 990
    .local v25, "overrideTypes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1, v9}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->processTblCellCondFormat(Ljava/util/List;Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;)Ljava/util/List;

    move-result-object v25

    .line 992
    invoke-static {}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;->values()[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;

    move-result-object v4

    .local v4, "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    array-length v0, v4

    move/from16 v22, v0

    .local v22, "len$":I
    const/16 v20, 0x0

    .local v20, "i$":I
    :goto_8
    move/from16 v0, v20

    move/from16 v1, v22

    if-ge v0, v1, :cond_20

    aget-object v32, v4, v20

    .line 994
    .local v32, "type":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    move-object/from16 v0, v25

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_1f

    .line 995
    move-object/from16 v0, v32

    invoke-virtual {v12, v0}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getTblStyleProp(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v31

    .line 998
    .local v31, "styleToApply":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->applyStyleToCell(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 1001
    const/16 v19, 0x0

    :goto_9
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getDocParagraphList()Ljava/util/ArrayList;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v19

    move/from16 v1, v34

    if-ge v0, v1, :cond_1f

    .line 1003
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->getDocParagraphList()Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;

    .line 1005
    .restart local v13    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    const/16 v21, 0x0

    .restart local v21    # "j":I
    :goto_a
    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getContents()Ljava/util/ArrayList;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->size()I

    move-result v34

    move/from16 v0, v21

    move/from16 v1, v34

    if-ge v0, v1, :cond_1e

    .line 1006
    invoke-virtual {v13}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;->getContents()Ljava/util/ArrayList;

    move-result-object v34

    move-object/from16 v0, v34

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/samsung/thumbnail/customview/word/Run;

    .line 1008
    .restart local v28    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v34

    if-eqz v34, :cond_1b

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v34

    if-eqz v34, :cond_1b

    .line 1010
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v17

    .line 1012
    .restart local v17    # "fontColor":Ljava/lang/String;
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v35, "#"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v34 .. v34}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v34

    move-object/from16 v0, v28

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/word/Run;->setColor(I)V

    .line 1017
    .end local v17    # "fontColor":Ljava/lang/String;
    :cond_1b
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v34

    if-eqz v34, :cond_1c

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getBold()Z

    move-result v34

    if-eqz v34, :cond_1c

    .line 1019
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/word/Run;->setTextBold()V

    .line 1021
    :cond_1c
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v34

    if-eqz v34, :cond_1d

    invoke-virtual/range {v31 .. v31}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCharProp()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocCharProperties;->getItalic()Z

    move-result v34

    if-eqz v34, :cond_1d

    .line 1023
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/thumbnail/customview/word/Run;->setTextItalics()V

    .line 1005
    :cond_1d
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_a

    .line 1002
    .end local v28    # "run":Lcom/samsung/thumbnail/customview/word/Run;
    :cond_1e
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_9

    .line 992
    .end local v13    # "docPara":Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
    .end local v21    # "j":I
    .end local v31    # "styleToApply":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    :cond_1f
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_8

    .line 1037
    .end local v4    # "arr$":[Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    .end local v20    # "i$":I
    .end local v22    # "len$":I
    .end local v25    # "overrideTypes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;>;"
    .end local v32    # "type":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->addCellToRow(Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 1039
    return-void

    .line 722
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public applyStyleToCell(Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 11
    .param p1, "style"    # Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .param p2, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    .line 1044
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getStyleType()Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    move-result-object v7

    .line 1045
    .local v7, "type":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;
    if-nez v7, :cond_0

    .line 1046
    sget-object v7, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->TABLE:Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;

    .line 1048
    :cond_0
    const/4 v6, 0x0

    .line 1049
    .local v6, "tblProp":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;
    const/4 v5, 0x0

    .line 1051
    .local v5, "tblCellProp":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    sget-object v8, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable$1;->$SwitchMap$com$samsung$thumbnail$office$ooxml$word$XWPFStyle$EStyleType:[I

    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$EStyleType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 1082
    :goto_0
    :pswitch_0
    if-eqz v6, :cond_1

    .line 1084
    invoke-virtual {p0, v6, p2}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->createApplyTableStyle(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 1087
    :cond_1
    if-eqz v5, :cond_3

    .line 1089
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 1091
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v8

    invoke-virtual {p0, v8, p2}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->createCellBorderStyleFromParent(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 1093
    :cond_2
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 1101
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v4

    .line 1102
    .local v4, "hexcolorcode":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1104
    .local v2, "fillColor":Lorg/apache/poi/java/awt/Color;
    const/16 v8, 0x10

    :try_start_0
    invoke-static {v4, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 1105
    .local v0, "colorvalue":I
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v3, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .local v3, "fillColor":Lorg/apache/poi/java/awt/Color;
    move-object v2, v3

    .line 1110
    .end local v0    # "colorvalue":I
    .end local v3    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .restart local v2    # "fillColor":Lorg/apache/poi/java/awt/Color;
    :goto_1
    const/4 v8, 0x0

    invoke-virtual {p2, v2, v8}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    .line 1115
    .end local v2    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .end local v4    # "hexcolorcode":Ljava/lang/String;
    :cond_3
    return-void

    .line 1063
    :pswitch_1
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getTblProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v6

    .line 1064
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;->getCellProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v5

    .line 1066
    goto :goto_0

    .line 1106
    .restart local v2    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .restart local v4    # "hexcolorcode":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1107
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v8, "DocumentService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1051
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public createApplyTableStyle(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 8
    .param p1, "tableProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;
    .param p2, "canvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    .line 1121
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1122
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getBoders()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    move-result-object v5

    invoke-virtual {p0, v5, p2}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->createCellBorderStyleFromParent(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V

    .line 1125
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getFill()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1128
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getShade()Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocShadingProperty;->getFill()Ljava/lang/String;

    move-result-object v4

    .line 1129
    .local v4, "hexcolorcode":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1131
    .local v2, "fillColor":Lorg/apache/poi/java/awt/Color;
    const/16 v5, 0x10

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 1132
    .local v0, "colorvalue":I
    new-instance v3, Lorg/apache/poi/java/awt/Color;

    invoke-direct {v3, v0}, Lorg/apache/poi/java/awt/Color;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .local v3, "fillColor":Lorg/apache/poi/java/awt/Color;
    move-object v2, v3

    .line 1137
    .end local v0    # "colorvalue":I
    .end local v3    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .restart local v2    # "fillColor":Lorg/apache/poi/java/awt/Color;
    :goto_0
    const/4 v5, 0x0

    invoke-virtual {p2, v2, v5}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setCellFillStyle(Lorg/apache/poi/java/awt/Color;Z)V

    .line 1140
    .end local v2    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .end local v4    # "hexcolorcode":Ljava/lang/String;
    :cond_1
    return-void

    .line 1133
    .restart local v2    # "fillColor":Lorg/apache/poi/java/awt/Color;
    .restart local v4    # "hexcolorcode":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1134
    .local v1, "e":Ljava/lang/Exception;
    const-string/jumbo v5, "DocumentService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public createCellBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 14
    .param p1, "tableBorders"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;
    .param p2, "borders"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;
    .param p3, "newCanvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    .line 404
    const/4 v12, 0x4

    new-array v6, v12, [Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    const/4 v12, 0x0

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v13, v6, v12

    const/4 v12, 0x1

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->RIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v13, v6, v12

    const/4 v12, 0x2

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v13, v6, v12

    const/4 v12, 0x3

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->LEFT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v13, v6, v12

    .line 407
    .local v6, "eBorders":[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    new-instance v9, Ljava/lang/StringBuilder;

    const/16 v12, 0x100

    invoke-direct {v9, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 408
    .local v9, "padding":Ljava/lang/StringBuilder;
    const-string/jumbo v12, "padding:"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    const/4 v12, 0x4

    if-ge v8, v12, :cond_f

    .line 411
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v12

    aget-object v13, v6, v8

    invoke-virtual {v12, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 412
    .local v1, "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    const/4 v11, 0x0

    .line 414
    .local v11, "tblBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 415
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v12

    aget-object v13, v6, v8

    invoke-virtual {v12, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "tblBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    check-cast v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 417
    .restart local v11    # "tblBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_0
    if-eqz v1, :cond_8

    .line 418
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    const/4 v12, 0x0

    invoke-direct {v2, v12}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 420
    .local v2, "borderColor":Lorg/apache/poi/java/awt/Color;
    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 421
    .local v3, "borderType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    const v4, 0x3f866666    # 1.05f

    .line 424
    .local v4, "borderWidth":F
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v12

    if-eqz v12, :cond_1

    .line 425
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v3

    .line 429
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v12

    if-eqz v12, :cond_2

    .line 430
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->isAuto()Z

    move-result v12

    if-nez v12, :cond_2

    .line 432
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v7

    .line 433
    .local v7, "hexcolorcode":Ljava/lang/String;
    const/16 v12, 0x10

    invoke-static {v7, v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    .line 434
    .local v5, "colorvalue":I
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    .end local v2    # "borderColor":Lorg/apache/poi/java/awt/Color;
    invoke-direct {v2, v5}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 440
    .end local v5    # "colorvalue":I
    .end local v7    # "hexcolorcode":Ljava/lang/String;
    .restart local v2    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v12

    if-ltz v12, :cond_3

    .line 441
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v10

    .line 448
    .local v10, "size":I
    int-to-float v12, v10

    invoke-static {v12}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPoint(F)F

    move-result v4

    .line 451
    .end local v10    # "size":I
    :cond_3
    aget-object v12, v6, v8

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    if-ne v12, v13, :cond_5

    .line 452
    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 410
    .end local v2    # "borderColor":Lorg/apache/poi/java/awt/Color;
    .end local v3    # "borderType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v4    # "borderWidth":F
    :cond_4
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 454
    .restart local v2    # "borderColor":Lorg/apache/poi/java/awt/Color;
    .restart local v3    # "borderType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .restart local v4    # "borderWidth":F
    :cond_5
    aget-object v12, v6, v8

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    if-ne v12, v13, :cond_6

    .line 455
    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto :goto_1

    .line 457
    :cond_6
    aget-object v12, v6, v8

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->LEFT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    if-ne v12, v13, :cond_7

    .line 458
    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto :goto_1

    .line 460
    :cond_7
    aget-object v12, v6, v8

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->RIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    if-ne v12, v13, :cond_4

    .line 461
    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto :goto_1

    .line 470
    .end local v2    # "borderColor":Lorg/apache/poi/java/awt/Color;
    .end local v3    # "borderType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v4    # "borderWidth":F
    :cond_8
    if-eqz v11, :cond_4

    .line 472
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    const/4 v12, 0x0

    invoke-direct {v2, v12}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 474
    .restart local v2    # "borderColor":Lorg/apache/poi/java/awt/Color;
    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 475
    .restart local v3    # "borderType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    const v4, 0x3f866666    # 1.05f

    .line 477
    .restart local v4    # "borderWidth":F
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v12

    if-eqz v12, :cond_9

    .line 478
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v3

    .line 481
    :cond_9
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v12

    if-eqz v12, :cond_a

    .line 482
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->isAuto()Z

    move-result v12

    if-nez v12, :cond_a

    .line 484
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v7

    .line 485
    .restart local v7    # "hexcolorcode":Ljava/lang/String;
    const/16 v12, 0x10

    invoke-static {v7, v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    .line 486
    .restart local v5    # "colorvalue":I
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    .end local v2    # "borderColor":Lorg/apache/poi/java/awt/Color;
    invoke-direct {v2, v5}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 490
    .end local v5    # "colorvalue":I
    .end local v7    # "hexcolorcode":Ljava/lang/String;
    .restart local v2    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :cond_a
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v12

    if-ltz v12, :cond_b

    .line 491
    invoke-virtual {v11}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v10

    .line 499
    .restart local v10    # "size":I
    int-to-float v12, v10

    invoke-static {v12}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPoint(F)F

    move-result v4

    .line 502
    .end local v10    # "size":I
    :cond_b
    aget-object v12, v6, v8

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    if-ne v12, v13, :cond_c

    .line 503
    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto :goto_1

    .line 505
    :cond_c
    aget-object v12, v6, v8

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    if-ne v12, v13, :cond_d

    .line 506
    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto/16 :goto_1

    .line 508
    :cond_d
    aget-object v12, v6, v8

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->LEFT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    if-ne v12, v13, :cond_e

    .line 509
    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto/16 :goto_1

    .line 511
    :cond_e
    aget-object v12, v6, v8

    sget-object v13, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->RIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    if-ne v12, v13, :cond_4

    .line 512
    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto/16 :goto_1

    .line 526
    .end local v1    # "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .end local v2    # "borderColor":Lorg/apache/poi/java/awt/Color;
    .end local v3    # "borderType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v4    # "borderWidth":F
    .end local v11    # "tblBorder":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_f
    return-void
.end method

.method public createCellBorderStyleFromParent(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;)V
    .locals 12
    .param p1, "tableBorders"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;
    .param p2, "newCanvasCell"    # Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;

    .prologue
    .line 530
    const/4 v10, 0x6

    new-array v6, v10, [Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    const/4 v10, 0x0

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v11, v6, v10

    const/4 v10, 0x1

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->RIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v11, v6, v10

    const/4 v10, 0x2

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v11, v6, v10

    const/4 v10, 0x3

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->LEFT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v11, v6, v10

    const/4 v10, 0x4

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_H:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v11, v6, v10

    const/4 v10, 0x5

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_V:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    aput-object v11, v6, v10

    .line 538
    .local v6, "eBorders":[Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;
    const/4 v0, 0x0

    .line 539
    .local v0, "applyBorder":Z
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    const/4 v10, 0x6

    if-ge v8, v10, :cond_1

    .line 540
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v10

    aget-object v11, v6, v8

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 541
    .local v1, "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    if-eqz v1, :cond_0

    .line 542
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 543
    const/4 v0, 0x1

    .line 539
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 547
    .end local v1    # "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    :cond_1
    const/4 v8, 0x0

    :goto_1
    const/4 v10, 0x6

    if-ge v8, v10, :cond_14

    .line 549
    const/4 v2, 0x0

    .line 550
    .local v2, "borderColor":Lorg/apache/poi/java/awt/Color;
    sget-object v3, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;->SINGLE:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    .line 551
    .local v3, "borderType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    const v4, 0x3f866666    # 1.05f

    .line 553
    .local v4, "borderWidth":F
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;->getBorders()Ljava/util/HashMap;

    move-result-object v10

    aget-object v11, v6, v8

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;

    .line 555
    .restart local v1    # "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    if-eqz v1, :cond_5

    .line 557
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v10

    if-eqz v10, :cond_2

    .line 558
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getStyle()Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;

    move-result-object v3

    .line 561
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 562
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->isAuto()Z

    move-result v10

    if-nez v10, :cond_6

    .line 564
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getColor()Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/thumbnail/office/ooxml/word/XDocColor;->getColor()Ljava/lang/String;

    move-result-object v7

    .line 565
    .local v7, "hexcolorcode":Ljava/lang/String;
    const/16 v10, 0x10

    invoke-static {v7, v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    .line 566
    .local v5, "colorvalue":I
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    .end local v2    # "borderColor":Lorg/apache/poi/java/awt/Color;
    invoke-direct {v2, v5}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .line 574
    .end local v5    # "colorvalue":I
    .end local v7    # "hexcolorcode":Ljava/lang/String;
    .restart local v2    # "borderColor":Lorg/apache/poi/java/awt/Color;
    :cond_3
    :goto_2
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v10

    if-ltz v10, :cond_4

    .line 575
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;->getSize()I

    move-result v9

    .line 583
    .local v9, "size":I
    int-to-float v10, v9

    invoke-static {v10}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->eightsOfPoint(F)F

    move-result v4

    .line 586
    .end local v9    # "size":I
    :cond_4
    aget-object v10, v6, v8

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->TOP:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    if-ne v10, v11, :cond_8

    .line 587
    if-eqz v2, :cond_7

    .line 588
    invoke-virtual {p2, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 547
    :cond_5
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 571
    :cond_6
    new-instance v2, Lorg/apache/poi/java/awt/Color;

    .end local v2    # "borderColor":Lorg/apache/poi/java/awt/Color;
    const/4 v10, 0x0

    invoke-direct {v2, v10}, Lorg/apache/poi/java/awt/Color;-><init>(I)V

    .restart local v2    # "borderColor":Lorg/apache/poi/java/awt/Color;
    goto :goto_2

    .line 590
    :cond_7
    if-eqz v0, :cond_5

    .line 591
    invoke-virtual {p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorderToNull()V

    goto :goto_3

    .line 592
    :cond_8
    aget-object v10, v6, v8

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->BOTTOM:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    if-ne v10, v11, :cond_a

    .line 593
    if-eqz v2, :cond_9

    .line 594
    invoke-virtual {p2, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto :goto_3

    .line 596
    :cond_9
    if-eqz v0, :cond_5

    .line 597
    invoke-virtual {p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorderToNull()V

    goto :goto_3

    .line 598
    :cond_a
    aget-object v10, v6, v8

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->LEFT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    if-ne v10, v11, :cond_c

    .line 599
    if-eqz v2, :cond_b

    .line 600
    invoke-virtual {p2, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto :goto_3

    .line 602
    :cond_b
    if-eqz v0, :cond_5

    .line 603
    invoke-virtual {p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorderToNull()V

    goto :goto_3

    .line 604
    :cond_c
    aget-object v10, v6, v8

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->RIGHT:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    if-ne v10, v11, :cond_e

    .line 605
    if-eqz v2, :cond_d

    .line 606
    invoke-virtual {p2, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto :goto_3

    .line 608
    :cond_d
    if-eqz v0, :cond_5

    .line 609
    invoke-virtual {p2}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorderToNull()V

    goto :goto_3

    .line 610
    :cond_e
    aget-object v10, v6, v8

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_H:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    if-ne v10, v11, :cond_11

    .line 611
    if-eqz v2, :cond_5

    .line 612
    iget v10, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->rowNo:I

    if-nez v10, :cond_f

    .line 613
    invoke-virtual {p2, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto :goto_3

    .line 615
    :cond_f
    iget v10, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->rowNo:I

    iget v11, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->noOfRows:I

    add-int/lit8 v11, v11, -0x1

    if-ne v10, v11, :cond_10

    .line 616
    invoke-virtual {p2, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto :goto_3

    .line 619
    :cond_10
    invoke-virtual {p2, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setTopBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 621
    invoke-virtual {p2, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setBottomBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto :goto_3

    .line 625
    :cond_11
    aget-object v10, v6, v8

    sget-object v11, Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;->INSIDE_V:Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderType;

    if-ne v10, v11, :cond_5

    .line 626
    if-eqz v2, :cond_5

    .line 627
    iget v10, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->colNo:I

    if-nez v10, :cond_12

    .line 628
    invoke-virtual {p2, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto :goto_3

    .line 630
    :cond_12
    iget v10, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->colNo:I

    iget v11, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->noOfCols:I

    add-int/lit8 v11, v11, -0x1

    if-ne v10, v11, :cond_13

    .line 631
    invoke-virtual {p2, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto/16 :goto_3

    .line 634
    :cond_13
    invoke-virtual {p2, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setLeftBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    .line 636
    invoke-virtual {p2, v3, v2, v4}, Lcom/samsung/thumbnail/customview/tables/CanvasTableCell;->setRightBorder(Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;Lorg/apache/poi/java/awt/Color;F)V

    goto/16 :goto_3

    .line 649
    .end local v1    # "border":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder;
    .end local v2    # "borderColor":Lorg/apache/poi/java/awt/Color;
    .end local v3    # "borderType":Lcom/samsung/thumbnail/office/ooxml/word/XDocBorder$EBorderStyle;
    .end local v4    # "borderWidth":F
    :cond_14
    return-void
.end method

.method public createNewTable(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;)V
    .locals 2
    .param p1, "parent"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;
    .param p2, "htmlConsumer"    # Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;
    .param p3, "type"    # Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    .prologue
    .line 110
    new-instance v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    .line 111
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->tableType:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 112
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->writeTable(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;)V

    .line 113
    return-void
.end method

.method public createNewTable(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V
    .locals 2
    .param p1, "parent"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;
    .param p2, "htmlConsumer"    # Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;
    .param p3, "type"    # Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;
    .param p4, "tblProp"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    .prologue
    .line 118
    new-instance v0, Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    .line 119
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    iget-object v1, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->tableType:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 120
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {p4}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getXPosition()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setXPos(Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {p4}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getYPosition()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setYPos(Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v0, p4}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableProp(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V

    .line 123
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->writeTable(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;)V

    .line 124
    return-void
.end method

.method public createTableBorderStyle(Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;)V
    .locals 0
    .param p1, "bordersProp"    # Lcom/samsung/thumbnail/office/ooxml/word/properties/XDocBordersProperty;

    .prologue
    .line 106
    return-void
.end method

.method public getCurrentTable()Lcom/samsung/thumbnail/customview/tables/CanvasTable;
    .locals 1

    .prologue
    .line 1210
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    return-object v0
.end method

.method public getTableNumber()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->tableNum:I

    return v0
.end method

.method public getTableType()Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->tableType:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    return-object v0
.end method

.method public setTableNumber(I)V
    .locals 1
    .param p1, "tableNum"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->tableNum:I

    .line 138
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableNumber(I)V

    .line 140
    :cond_0
    return-void
.end method

.method public setTableType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V
    .locals 1
    .param p1, "paraType"    # Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->tableType:Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;

    .line 128
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    invoke-virtual {v0, p1}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableType(Lcom/samsung/thumbnail/customview/hslf/DocParagraph$ParaType;)V

    .line 130
    :cond_0
    return-void
.end method

.method public writeTable(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;)V
    .locals 26
    .param p1, "parent"    # Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;
    .param p2, "htmlConsumer"    # Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;
    .param p3, "type"    # Lcom/samsung/thumbnail/office/ooxml/word/EBodyElemntType;

    .prologue
    .line 268
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->consumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    .line 269
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->parentTableStruct:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;

    .line 271
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getGridColWidthArray()Ljava/util/ArrayList;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 272
    .local v10, "gridSize":I
    if-eqz v10, :cond_1

    .line 273
    const/16 v18, 0x0

    .line 274
    .local v18, "tableWidth":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    if-ge v13, v10, :cond_0

    .line 275
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getGridColWidthArray()Ljava/util/ArrayList;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    add-int v18, v18, v22

    .line 274
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 277
    :cond_0
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(F)F

    move-result v22

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-double v0, v0

    move-wide/from16 v22, v0

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getCustomViewWidth()D

    move-result-wide v24

    cmpg-double v22, v22, v24

    if-gtz v22, :cond_1

    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v22, v0

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v23

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v24, v0

    invoke-static/range {v24 .. v24}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(F)F

    move-result v24

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->setTableWidth(I)V

    .line 289
    .end local v13    # "i":I
    .end local v18    # "tableWidth":I
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v22

    if-eqz v22, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getStyle()Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_2

    .line 302
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->startCanvasTable(Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;)V

    .line 305
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getRows()Ljava/util/List;

    move-result-object v17

    .line 307
    .local v17, "tableRows":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->noOfRows:I

    .line 308
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->noOfRows:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v13, v0, :cond_b

    .line 310
    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->rowNo:I

    .line 313
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->createNewRow()V

    .line 315
    move-object/from16 v0, v17

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;

    .line 317
    .local v19, "tblRow":Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->getRowProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;

    move-result-object v16

    .line 320
    .local v16, "rowProp":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getTableProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblProperties;->getStyle()Ljava/lang/String;

    move-result-object v4

    .line 321
    .local v4, "StyleId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->consumer:Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getDocument()Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/office/ooxml/word/XWPFDocument;->getCanvasStyle()Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasStyles;->getStyle(Ljava/lang/String;)Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;

    move-result-object v9

    .line 326
    .local v9, "currentStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 327
    .local v15, "overrideTypes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;>;"
    if-eqz v9, :cond_3

    .line 328
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->processTblRowCondFormat(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;)Ljava/util/List;

    move-result-object v15

    .line 332
    :cond_3
    if-eqz v16, :cond_7

    .line 333
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;->getHeight()I

    move-result v22

    if-lez v22, :cond_6

    .line 335
    const/high16 v12, 0x41a00000    # 20.0f

    .line 338
    .local v12, "height":F
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;->getHeight()I

    move-result v22

    const/16 v23, 0x14

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_4

    .line 339
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;->getHeight()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(F)F

    move-result v12

    .line 341
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->setRowHeight(F)V

    .line 356
    .end local v12    # "height":F
    :goto_2
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->getCellList()Ljava/util/List;

    move-result-object v6

    .line 358
    .local v6, "cellLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;>;"
    const/16 v20, 0x0

    .line 359
    .local v20, "totalIndexNumber":I
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->noOfCols:I

    .line 360
    const/4 v14, 0x0

    .local v14, "j":I
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->noOfCols:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v14, v0, :cond_a

    .line 361
    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->colNo:I

    .line 362
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;->getCellList()Ljava/util/List;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;

    .line 364
    .local v5, "cell":Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;
    invoke-virtual {v5}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;->getCellProp()Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;

    move-result-object v7

    .line 366
    .local v7, "cellProp":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    if-eqz v7, :cond_9

    .line 367
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->setWidth(I)V

    .line 368
    sget-object v22, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;->DXA:Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->setWidthType(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable$EWidthType;)V

    .line 369
    const/4 v8, 0x0

    .line 371
    .local v8, "cellWidth":I
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getGridSpan()I

    move-result v11

    .line 372
    .local v11, "gridSpan":I
    const/16 v21, 0x0

    .local v21, "x":I
    :goto_4
    move/from16 v0, v21

    if-ge v0, v11, :cond_8

    .line 373
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getGridColWidthArray()Ljava/util/ArrayList;

    move-result-object v22

    if-eqz v22, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getGridColWidthArray()Ljava/util/ArrayList;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    add-int v23, v20, v21

    move/from16 v0, v22

    move/from16 v1, v23

    if-lt v0, v1, :cond_5

    .line 375
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getWidth()I

    move-result v23

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTable;->getGridColWidthArray()Ljava/util/ArrayList;

    move-result-object v22

    add-int v24, v20, v21

    move-object/from16 v0, v22

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    add-int v22, v22, v23

    add-int v8, v8, v22

    .line 372
    :cond_5
    add-int/lit8 v21, v21, 0x1

    goto :goto_4

    .line 346
    .end local v5    # "cell":Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;
    .end local v6    # "cellLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;>;"
    .end local v7    # "cellProp":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    .end local v8    # "cellWidth":I
    .end local v11    # "gridSpan":I
    .end local v14    # "j":I
    .end local v20    # "totalIndexNumber":I
    .end local v21    # "x":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v22

    const/high16 v23, 0x41a00000    # 20.0f

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->setRowHeight(F)V

    goto/16 :goto_2

    .line 352
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v22

    const/high16 v23, 0x43a50000    # 330.0f

    invoke-static/range {v23 .. v23}, Lcom/samsung/thumbnail/office/ooxml/util/OOXMLUtil;->twentiethsToPoints(F)F

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->setRowHeight(F)V

    goto/16 :goto_2

    .line 380
    .restart local v5    # "cell":Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;
    .restart local v6    # "cellLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;>;"
    .restart local v7    # "cellProp":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    .restart local v8    # "cellWidth":I
    .restart local v11    # "gridSpan":I
    .restart local v14    # "j":I
    .restart local v20    # "totalIndexNumber":I
    .restart local v21    # "x":I
    :cond_8
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/thumbnail/customview/word/XDocCustomViewConsumer;->getCanvasElementCreator()Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/word/CanvasElementsCreator;->getSlideDimention()Lcom/samsung/thumbnail/customview/hslf/SlideDimention;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Lcom/samsung/thumbnail/customview/hslf/SlideDimention;->getDocObjWidth(I)I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->setWidth(I)V

    .line 383
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v14, v2, v15}, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->writeTableCell(Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;IILjava/util/List;)V

    .line 384
    invoke-virtual {v7}, Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;->getGridSpan()I

    move-result v22

    add-int v20, v20, v22

    .line 360
    .end local v8    # "cellWidth":I
    .end local v11    # "gridSpan":I
    .end local v21    # "x":I
    :goto_5
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_3

    .line 387
    :cond_9
    add-int/lit8 v20, v20, 0x1

    goto :goto_5

    .line 394
    .end local v5    # "cell":Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;
    .end local v7    # "cellProp":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblCellProperties;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/thumbnail/customview/tables/xwpf/XWPFCanvasTable;->mCurrentTable:Lcom/samsung/thumbnail/customview/tables/CanvasTable;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/customview/tables/CanvasTable;->getCurrentRow()Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->getRowHeight()F

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/thumbnail/customview/tables/CanvasTableRow;->updateRowHeight(F)V

    .line 308
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    .line 399
    .end local v4    # "StyleId":Ljava/lang/String;
    .end local v6    # "cellLst":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableCell;>;"
    .end local v9    # "currentStyle":Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle;
    .end local v14    # "j":I
    .end local v15    # "overrideTypes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/thumbnail/office/ooxml/word/XWPFStyle$ETblStyleOverrideType;>;"
    .end local v16    # "rowProp":Lcom/samsung/thumbnail/office/ooxml/word/table/XDocTblRowProperties;
    .end local v19    # "tblRow":Lcom/samsung/thumbnail/office/ooxml/word/table/XWPFTableRow;
    .end local v20    # "totalIndexNumber":I
    :cond_b
    return-void
.end method
