.class public Lcom/samsung/thumbnail/customview/hslf/ChartContents;
.super Lcom/samsung/thumbnail/customview/hslf/DocParagraph;
.source "ChartContents.java"


# instance fields
.field bubbleChart:Lorg/achartengine/chart/XYChart;

.field chartHeight:F

.field chartType:I

.field chartWidth:F

.field columnBarChart:Lorg/achartengine/chart/AbstractChart;

.field columnChart:Lorg/achartengine/chart/ColumnChart;

.field height:I

.field mAreaChart:Lorg/achartengine/chart/TimeChart;

.field mDoughnutChart:Lorg/achartengine/chart/DoughnutChart;

.field mRadarChart:Lorg/achartengine/chart/RadarChart;

.field mStockChart:Lorg/achartengine/chart/RangeBarChart;

.field pieChart:Lorg/achartengine/chart/PieChart;

.field scatterChart:Lorg/achartengine/chart/XYChart;

.field width:I

.field x:I

.field xyChart:Lorg/achartengine/chart/XYChart;

.field y:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/samsung/thumbnail/customview/hslf/DocParagraph;-><init>()V

    return-void
.end method


# virtual methods
.method public getAreaChart()Lorg/achartengine/chart/TimeChart;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->mAreaChart:Lorg/achartengine/chart/TimeChart;

    return-object v0
.end method

.method public getBubbleChart()Lorg/achartengine/chart/XYChart;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->bubbleChart:Lorg/achartengine/chart/XYChart;

    return-object v0
.end method

.method public getChartHeight()F
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->chartHeight:F

    return v0
.end method

.method public getChartType()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->chartType:I

    return v0
.end method

.method public getChartWidth()F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->chartWidth:F

    return v0
.end method

.method public getColumnBarChart()Lorg/achartengine/chart/AbstractChart;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->columnBarChart:Lorg/achartengine/chart/AbstractChart;

    return-object v0
.end method

.method public getDoughnutChart()Lorg/achartengine/chart/DoughnutChart;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->mDoughnutChart:Lorg/achartengine/chart/DoughnutChart;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->height:I

    return v0
.end method

.method public getHorizontalBarChart()Lorg/achartengine/chart/ColumnChart;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->columnChart:Lorg/achartengine/chart/ColumnChart;

    return-object v0
.end method

.method public getLineChart()Lorg/achartengine/chart/XYChart;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->xyChart:Lorg/achartengine/chart/XYChart;

    return-object v0
.end method

.method public getPieChart()Lorg/achartengine/chart/PieChart;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->pieChart:Lorg/achartengine/chart/PieChart;

    return-object v0
.end method

.method public getRadarChart()Lorg/achartengine/chart/RadarChart;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->mRadarChart:Lorg/achartengine/chart/RadarChart;

    return-object v0
.end method

.method public getScatterChart()Lorg/achartengine/chart/XYChart;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->scatterChart:Lorg/achartengine/chart/XYChart;

    return-object v0
.end method

.method public getStockChart()Lorg/achartengine/chart/RangeBarChart;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->mStockChart:Lorg/achartengine/chart/RangeBarChart;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->width:I

    return v0
.end method

.method public getX()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->x:I

    return v0
.end method

.method public getY()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->y:I

    return v0
.end method

.method public setAreaChart(Lorg/achartengine/chart/TimeChart;)V
    .locals 0
    .param p1, "areaChart"    # Lorg/achartengine/chart/TimeChart;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->mAreaChart:Lorg/achartengine/chart/TimeChart;

    .line 123
    return-void
.end method

.method public setBubbleChart(Lorg/achartengine/chart/XYChart;)V
    .locals 0
    .param p1, "bubbleChart"    # Lorg/achartengine/chart/XYChart;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->bubbleChart:Lorg/achartengine/chart/XYChart;

    .line 91
    return-void
.end method

.method public setChartHeight(F)V
    .locals 0
    .param p1, "chartHeight"    # F

    .prologue
    .line 46
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->chartHeight:F

    .line 47
    return-void
.end method

.method public setChartType(I)V
    .locals 0
    .param p1, "chartType"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->chartType:I

    .line 51
    return-void
.end method

.method public setChartWidth(F)V
    .locals 0
    .param p1, "chartWidth"    # F

    .prologue
    .line 38
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->chartWidth:F

    .line 39
    return-void
.end method

.method public setColumnBarChart(Lorg/achartengine/chart/AbstractChart;)V
    .locals 0
    .param p1, "barChart"    # Lorg/achartengine/chart/AbstractChart;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->columnBarChart:Lorg/achartengine/chart/AbstractChart;

    .line 59
    return-void
.end method

.method public setDoughnutChart(Lorg/achartengine/chart/DoughnutChart;)V
    .locals 0
    .param p1, "doughnutChart"    # Lorg/achartengine/chart/DoughnutChart;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->mDoughnutChart:Lorg/achartengine/chart/DoughnutChart;

    .line 83
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 158
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->height:I

    .line 159
    return-void
.end method

.method public setHorizontalBarChart(Lorg/achartengine/chart/ColumnChart;)V
    .locals 0
    .param p1, "columnChart"    # Lorg/achartengine/chart/ColumnChart;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->columnChart:Lorg/achartengine/chart/ColumnChart;

    .line 111
    return-void
.end method

.method public setLineChart(Lorg/achartengine/chart/XYChart;)V
    .locals 0
    .param p1, "xyChart"    # Lorg/achartengine/chart/XYChart;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->xyChart:Lorg/achartengine/chart/XYChart;

    .line 67
    return-void
.end method

.method public setPieChart(Lorg/achartengine/chart/PieChart;)V
    .locals 0
    .param p1, "pieChart"    # Lorg/achartengine/chart/PieChart;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->pieChart:Lorg/achartengine/chart/PieChart;

    .line 75
    return-void
.end method

.method public setRadarChart(Lorg/achartengine/chart/RadarChart;)V
    .locals 0
    .param p1, "radarChart"    # Lorg/achartengine/chart/RadarChart;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->mRadarChart:Lorg/achartengine/chart/RadarChart;

    .line 163
    return-void
.end method

.method public setScatterChart(Lorg/achartengine/chart/XYChart;)V
    .locals 0
    .param p1, "scatterChart"    # Lorg/achartengine/chart/XYChart;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->scatterChart:Lorg/achartengine/chart/XYChart;

    .line 99
    return-void
.end method

.method public setStockChart(Lorg/achartengine/chart/RangeBarChart;)V
    .locals 0
    .param p1, "stockChart"    # Lorg/achartengine/chart/RangeBarChart;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->mStockChart:Lorg/achartengine/chart/RangeBarChart;

    .line 115
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 150
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->width:I

    .line 151
    return-void
.end method

.method public setX(I)V
    .locals 0
    .param p1, "x"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->x:I

    .line 135
    return-void
.end method

.method public setY(I)V
    .locals 0
    .param p1, "y"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/samsung/thumbnail/customview/hslf/ChartContents;->y:I

    .line 143
    return-void
.end method
