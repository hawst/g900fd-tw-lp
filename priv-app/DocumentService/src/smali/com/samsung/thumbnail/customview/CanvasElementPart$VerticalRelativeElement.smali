.class public final enum Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
.super Ljava/lang/Enum;
.source "CanvasElementPart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/thumbnail/customview/CanvasElementPart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VerticalRelativeElement"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

.field public static final enum BOTTOM_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

.field public static final enum INSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

.field public static final enum LINE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

.field public static final enum MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

.field public static final enum OUTSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

.field public static final enum PAGE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

.field public static final enum TOP_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 79
    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    const-string/jumbo v1, "MARGIN"

    invoke-direct {v0, v1, v3}, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    const-string/jumbo v1, "PAGE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->PAGE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    const-string/jumbo v1, "LINE"

    invoke-direct {v0, v1, v5}, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->LINE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    const-string/jumbo v1, "TOP_MARGIN"

    invoke-direct {v0, v1, v6}, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->TOP_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    const-string/jumbo v1, "BOTTOM_MARGIN"

    invoke-direct {v0, v1, v7}, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->BOTTOM_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    const-string/jumbo v1, "INSIDE_MARGIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->INSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    new-instance v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    const-string/jumbo v1, "OUTSIDE_MARGIN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->OUTSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    .line 78
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->PAGE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->LINE:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->TOP_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->BOTTOM_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->INSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->OUTSIDE_MARGIN:Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->$VALUES:[Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 78
    const-class v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    return-object v0
.end method

.method public static values()[Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->$VALUES:[Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    invoke-virtual {v0}, [Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/thumbnail/customview/CanvasElementPart$VerticalRelativeElement;

    return-object v0
.end method
