.class public abstract Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;
.super Ljava/lang/Object;
.source "OOXMLElementHandler.java"

# interfaces
.implements Lcom/samsung/thumbnail/office/ooxml/IElementHandler;


# instance fields
.field protected expectedNameSpaceId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkElement(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)Z
    .locals 8
    .param p0, "orgEleName"    # Ljava/lang/String;
    .param p1, "elementName"    # Ljava/lang/String;
    .param p2, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p3, "nameSpace"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .prologue
    .line 63
    const/4 v2, 0x0

    .line 64
    .local v2, "isAvailable":Z
    if-nez p3, :cond_1

    .line 65
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 66
    const/4 v2, 0x1

    :cond_0
    move v3, v2

    .line 104
    .end local v2    # "isAvailable":Z
    .local v3, "isAvailable":I
    :goto_0
    return v3

    .line 71
    .end local v3    # "isAvailable":I
    .restart local v2    # "isAvailable":Z
    :cond_1
    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getPrefix()Ljava/lang/String;

    move-result-object v5

    .line 74
    .local v5, "prefix":Ljava/lang/String;
    if-nez v5, :cond_4

    .line 75
    invoke-virtual {p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getDefaultNameSpace()Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    .line 77
    .local v0, "defNS":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    if-eqz v0, :cond_2

    invoke-virtual {p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getID()I

    move-result v6

    invoke-virtual {v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getID()I

    move-result v7

    if-eq v6, v7, :cond_2

    move v3, v2

    .line 78
    .restart local v3    # "isAvailable":I
    goto :goto_0

    .line 81
    .end local v3    # "isAvailable":I
    :cond_2
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 82
    const/4 v2, 0x1

    :cond_3
    move v3, v2

    .line 84
    .restart local v3    # "isAvailable":I
    goto :goto_0

    .line 86
    .end local v0    # "defNS":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .end local v3    # "isAvailable":I
    :cond_4
    const/4 v6, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 87
    .local v4, "pref":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {p0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 89
    .local v1, "ele":Ljava/lang/String;
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 90
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    :cond_5
    move v3, v2

    .line 104
    .restart local v3    # "isAvailable":I
    goto :goto_0
.end method

.method public static stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)Ljava/lang/String;
    .locals 4
    .param p0, "eleName"    # Ljava/lang/String;
    .param p1, "nameSpace"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    .prologue
    .line 109
    const/4 v1, 0x0

    .line 111
    .local v1, "stripedName":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 112
    move-object v1, p0

    move-object v2, v1

    .line 128
    .end local v1    # "stripedName":Ljava/lang/String;
    .local v2, "stripedName":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 116
    .end local v2    # "stripedName":Ljava/lang/String;
    .restart local v1    # "stripedName":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getPrefix()Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "prefix":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 118
    move-object v1, p0

    move-object v2, v1

    .line 119
    .end local v1    # "stripedName":Ljava/lang/String;
    .restart local v2    # "stripedName":Ljava/lang/String;
    goto :goto_0

    .line 122
    .end local v2    # "stripedName":Ljava/lang/String;
    .restart local v1    # "stripedName":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 123
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 124
    .end local v1    # "stripedName":Ljava/lang/String;
    .restart local v2    # "stripedName":Ljava/lang/String;
    goto :goto_0

    .line 127
    .end local v2    # "stripedName":Ljava/lang/String;
    .restart local v1    # "stripedName":Ljava/lang/String;
    :cond_2
    move-object v1, p0

    move-object v2, v1

    .line 128
    .end local v1    # "stripedName":Ljava/lang/String;
    .restart local v2    # "stripedName":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public charactersHandler(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;[CII)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "content"    # [C
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    .line 15
    return-void
.end method

.method public checkElement(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Z
    .locals 2
    .param p1, "orgEleName"    # Ljava/lang/String;
    .param p2, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->getElementName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->getExpectedNS(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v1

    invoke-static {p1, v0, p2, v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->checkElement(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)Z

    move-result v0

    return v0
.end method

.method public getAttrForNS(Lorg/xml/sax/Attributes;Ljava/lang/String;ILcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;
    .locals 3
    .param p1, "attrs"    # Lorg/xml/sax/Attributes;
    .param p2, "attrName"    # Ljava/lang/String;
    .param p3, "nsId"    # I
    .param p4, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 31
    invoke-virtual {p4, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getNameSpaceById(I)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getPrefix()Ljava/lang/String;

    move-result-object v0

    .line 32
    .local v0, "attName":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 35
    invoke-interface {p1, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "attrs"    # Lorg/xml/sax/Attributes;
    .param p2, "attrName"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-interface {p1, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAttrValue(Lorg/xml/sax/Attributes;Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;
    .locals 5
    .param p1, "attrs"    # Lorg/xml/sax/Attributes;
    .param p2, "attrName"    # Ljava/lang/String;
    .param p3, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 41
    invoke-interface {p1, p2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "attrVal":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 43
    invoke-virtual {p0, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->getExpectedNS(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v1

    .line 44
    .local v1, "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    if-eqz v1, :cond_0

    .line 45
    invoke-virtual {v1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;->getPrefix()Ljava/lang/String;

    move-result-object v2

    .line 46
    .local v2, "prefix":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-interface {p1, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    .end local v1    # "nameSpace":Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
    .end local v2    # "prefix":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getElementName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getExpectedNS(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;
.end method

.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 138
    if-eqz p1, :cond_0

    .line 139
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->popHandler()Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;

    .line 144
    :cond_0
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 19
    return-void
.end method

.method public stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Ljava/lang/String;
    .locals 1
    .param p1, "eleName"    # Ljava/lang/String;
    .param p2, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;

    .prologue
    .line 132
    invoke-virtual {p0, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->getExpectedNS(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;)Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLElementHandler;->stripElementName(Ljava/lang/String;Lcom/samsung/thumbnail/office/ooxml/OOXMLNameSpace;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
