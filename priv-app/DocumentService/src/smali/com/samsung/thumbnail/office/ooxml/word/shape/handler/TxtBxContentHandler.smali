.class public Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/TxtBxContentHandler;
.super Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;
.source "TxtBxContentHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    const-string/jumbo v0, "txbxContent"

    invoke-direct {p0, v0}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;-><init>(Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    .line 22
    new-instance v0, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaHandler;

    invoke-direct {v0}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaHandler;-><init>()V

    .line 23
    .local v0, "paraHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocParaHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/TxtBxContentHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "p"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    new-instance v1, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;

    const/16 v2, 0x64

    invoke-direct {v1, v2}, Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;-><init>(I)V

    .line 26
    .local v1, "sdtHandler":Lcom/samsung/thumbnail/office/ooxml/word/document/XDocSdtHandler;
    iget-object v2, p0, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/TxtBxContentHandler;->handlerMap:Ljava/util/HashMap;

    const-string/jumbo v3, "sdt"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    return-void
.end method


# virtual methods
.method public handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V
    .locals 1
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleEndElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->endTextBoxContents()V

    .line 47
    return-void
.end method

.method public handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 0
    .param p1, "ooxmlParser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "eleName"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 40
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->handleStartElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 41
    return-void
.end method

.method public startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "parser"    # Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;
    .param p2, "element"    # Ljava/lang/String;
    .param p3, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 32
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/thumbnail/office/ooxml/OOXMLFixedElementHandler;->startParsingElement(Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 33
    invoke-direct {p0}, Lcom/samsung/thumbnail/office/ooxml/word/shape/handler/TxtBxContentHandler;->init()V

    .line 34
    invoke-virtual {p1}, Lcom/samsung/thumbnail/office/ooxml/OOXMLParser;->getConsumer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;

    invoke-interface {v0}, Lcom/samsung/thumbnail/office/ooxml/word/IXDocConsumer;->startTextBoxContents()V

    .line 35
    return-void
.end method
